/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:29:43 AM
**        * FROM NATURAL SUBPROGRAM : Mdmn361a
************************************************************
**        * FILE NAME            : Mdmn361a.java
**        * CLASS NAME           : Mdmn361a
**        * INSTANCE NAME        : Mdmn361a
************************************************************
************************************************************************
* PROGRAM NAME : MDMN361A - PIN EXPANSION VERSION OF MDMN360A
* DESCRIPTION  : MDM RETRIEVE CURRENT AND RESIDENTIAL FUTURE ADDRESS
*                THIS SUBPROGRAM IS CALLED TO GET MDM ADDRESS INFO.
*                DESIGNED AS A PART OF:
*                   RESIDENTIAL ADDRESS PROJECT
*
* WRITTEN BY   : DON MEADE
* DATE WRITTEN : MAY 12, 2017
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
************************************************************************

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mdmn361a extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    public DbsRecord parameters;
    private PdaMdma361 pdaMdma361;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Max_In;
    private DbsField pnd_Max_Out;
    private DbsField pnd_Data_In;

    private DbsGroup pnd_Data_In__R_Field_1;
    private DbsField pnd_Data_In_Pnd_I_Ssn;
    private DbsField pnd_Data_In_Pnd_I_Pin;
    private DbsField pnd_Data_In_Pnd_I_Xref_Ind;
    private DbsField pnd_Data_In_Pnd_I_Party_Fltr;
    private DbsField pnd_Data_In_Pnd_I_Party_Inq_Lvl;
    private DbsField pnd_Data_In_Pnd_I_Privpref_Fltr;
    private DbsField pnd_Data_In_Pnd_I_Address_Fltr;
    private DbsField pnd_Data_In_Pnd_I_Pin_A12;

    private DbsGroup pnd_Data_In__R_Field_2;
    private DbsField pnd_Data_In_Pnd_I_Pin_N12;

    private DbsGroup pnd_Data_In__R_Field_3;
    private DbsField pnd_Data_In_Pnd_I_Pin_Zeroes;
    private DbsField pnd_Data_In_Pnd_I_Pin_N7;

    private DbsGroup pnd_Data_In__R_Field_4;
    private DbsField pnd_Data_In_Pnd_I_Pin_A7;
    private DbsField pnd_Data_In_Pnd_I_Pin_Blanks;
    private DbsField pnd_Return;

    private DbsGroup pnd_Return__R_Field_5;
    private DbsField pnd_Return_Pnd_Rtrn_Code;
    private DbsField pnd_Return_Pnd_Rtrn_Text;
    private DbsField pnd_Data_Out;

    private DbsGroup pnd_Data_Out__R_Field_6;

    private DbsGroup pnd_Data_Out_Pnd_Temp_Data;
    private DbsField pnd_Data_Out_Pnd_O_Ssn;
    private DbsField pnd_Data_Out_Pnd_O_Pin;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_Last_Name;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_First_Name;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_Middle_Name;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_Prefix;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_Suffix;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Permanent_Ind;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Type_Code;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_1;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_2;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_3;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_4;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_City;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_St_Prov;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip_Code;

    private DbsGroup pnd_Data_Out__R_Field_7;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip1_5;

    private DbsGroup pnd_Data_Out__R_Field_8;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip1_7;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Country;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Postal_Data;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Geographic_Code;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Status_Code;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Status_Date;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Override_Code;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Last_Update_Date;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Res_Adr_Last_Update_Time;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Start_Date;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Type_Code;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_1;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_2;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_3;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_4;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_City;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_St_Prov;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip_Code;

    private DbsGroup pnd_Data_Out__R_Field_9;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip1_5;

    private DbsGroup pnd_Data_Out__R_Field_10;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip1_7;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Country;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Postal_Data;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Geographic_Code;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Status_Code;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Status_Date;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Override_Code;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Last_Update_Date;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Res_Adr_Last_Update_Time;
    private DbsField pnd_Data_Out_Pnd_O_Pin_A12;

    private DbsGroup pnd_Data_Out__R_Field_11;
    private DbsField pnd_Data_Out_Pnd_O_Pin_N7;
    private DbsField pnd_Data_Out_Pnd_O_Pin_Blanks;

    private DbsGroup pnd_Data_Out__R_Field_12;
    private DbsField pnd_Data_Out_Pnd_O_Pin_N12;
    private DbsField pnd_City_State_Zip;
    private DbsField pnd_Save_Code;
    private DbsField pnd_Save_Text;
    private DbsField pnd_Pin_All_0s;
    private DbsField pnd_Soc_All_0s;
    private DbsField pnd_Pin_All_9s;
    private DbsField pnd_Soc_All_9s;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        // parameters
        parameters = new DbsRecord();
        pdaMdma361 = new PdaMdma361(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Max_In = localVariables.newFieldInRecord("pnd_Max_In", "#MAX-IN", FieldType.NUMERIC, 5);
        pnd_Max_Out = localVariables.newFieldInRecord("pnd_Max_Out", "#MAX-OUT", FieldType.NUMERIC, 5);
        pnd_Data_In = localVariables.newFieldArrayInRecord("pnd_Data_In", "#DATA-IN", FieldType.STRING, 1, new DbsArrayController(1, 32560));

        pnd_Data_In__R_Field_1 = localVariables.newGroupInRecord("pnd_Data_In__R_Field_1", "REDEFINE", pnd_Data_In);
        pnd_Data_In_Pnd_I_Ssn = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Ssn", "#I-SSN", FieldType.STRING, 9);
        pnd_Data_In_Pnd_I_Pin = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Pin", "#I-PIN", FieldType.STRING, 7);
        pnd_Data_In_Pnd_I_Xref_Ind = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Xref_Ind", "#I-XREF-IND", FieldType.STRING, 1);
        pnd_Data_In_Pnd_I_Party_Fltr = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Party_Fltr", "#I-PARTY-FLTR", FieldType.STRING, 8);
        pnd_Data_In_Pnd_I_Party_Inq_Lvl = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Party_Inq_Lvl", "#I-PARTY-INQ-LVL", FieldType.NUMERIC, 
            3);
        pnd_Data_In_Pnd_I_Privpref_Fltr = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Privpref_Fltr", "#I-PRIVPREF-FLTR", FieldType.STRING, 
            23);
        pnd_Data_In_Pnd_I_Address_Fltr = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Address_Fltr", "#I-ADDRESS-FLTR", FieldType.STRING, 
            9);
        pnd_Data_In_Pnd_I_Pin_A12 = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Pin_A12", "#I-PIN-A12", FieldType.STRING, 12);

        pnd_Data_In__R_Field_2 = pnd_Data_In__R_Field_1.newGroupInGroup("pnd_Data_In__R_Field_2", "REDEFINE", pnd_Data_In_Pnd_I_Pin_A12);
        pnd_Data_In_Pnd_I_Pin_N12 = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_I_Pin_N12", "#I-PIN-N12", FieldType.NUMERIC, 12);

        pnd_Data_In__R_Field_3 = pnd_Data_In__R_Field_1.newGroupInGroup("pnd_Data_In__R_Field_3", "REDEFINE", pnd_Data_In_Pnd_I_Pin_A12);
        pnd_Data_In_Pnd_I_Pin_Zeroes = pnd_Data_In__R_Field_3.newFieldInGroup("pnd_Data_In_Pnd_I_Pin_Zeroes", "#I-PIN-ZEROES", FieldType.NUMERIC, 5);
        pnd_Data_In_Pnd_I_Pin_N7 = pnd_Data_In__R_Field_3.newFieldInGroup("pnd_Data_In_Pnd_I_Pin_N7", "#I-PIN-N7", FieldType.NUMERIC, 7);

        pnd_Data_In__R_Field_4 = pnd_Data_In__R_Field_1.newGroupInGroup("pnd_Data_In__R_Field_4", "REDEFINE", pnd_Data_In_Pnd_I_Pin_A12);
        pnd_Data_In_Pnd_I_Pin_A7 = pnd_Data_In__R_Field_4.newFieldInGroup("pnd_Data_In_Pnd_I_Pin_A7", "#I-PIN-A7", FieldType.STRING, 7);
        pnd_Data_In_Pnd_I_Pin_Blanks = pnd_Data_In__R_Field_4.newFieldInGroup("pnd_Data_In_Pnd_I_Pin_Blanks", "#I-PIN-BLANKS", FieldType.STRING, 5);
        pnd_Return = localVariables.newFieldArrayInRecord("pnd_Return", "#RETURN", FieldType.STRING, 1, new DbsArrayController(1, 200));

        pnd_Return__R_Field_5 = localVariables.newGroupInRecord("pnd_Return__R_Field_5", "REDEFINE", pnd_Return);
        pnd_Return_Pnd_Rtrn_Code = pnd_Return__R_Field_5.newFieldInGroup("pnd_Return_Pnd_Rtrn_Code", "#RTRN-CODE", FieldType.STRING, 4);
        pnd_Return_Pnd_Rtrn_Text = pnd_Return__R_Field_5.newFieldInGroup("pnd_Return_Pnd_Rtrn_Text", "#RTRN-TEXT", FieldType.STRING, 80);
        pnd_Data_Out = localVariables.newFieldArrayInRecord("pnd_Data_Out", "#DATA-OUT", FieldType.STRING, 1, new DbsArrayController(1, 32360));

        pnd_Data_Out__R_Field_6 = localVariables.newGroupInRecord("pnd_Data_Out__R_Field_6", "REDEFINE", pnd_Data_Out);

        pnd_Data_Out_Pnd_Temp_Data = pnd_Data_Out__R_Field_6.newGroupInGroup("pnd_Data_Out_Pnd_Temp_Data", "#TEMP-DATA");
        pnd_Data_Out_Pnd_O_Ssn = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Ssn", "#O-SSN", FieldType.NUMERIC, 9);
        pnd_Data_Out_Pnd_O_Pin = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin", "#O-PIN", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Preferred_Last_Name = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_Last_Name", "#O-PREFERRED-LAST-NAME", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Preferred_First_Name = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_First_Name", "#O-PREFERRED-FIRST-NAME", 
            FieldType.STRING, 30);
        pnd_Data_Out_Pnd_O_Preferred_Middle_Name = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_Middle_Name", "#O-PREFERRED-MIDDLE-NAME", 
            FieldType.STRING, 30);
        pnd_Data_Out_Pnd_O_Preferred_Prefix = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_Prefix", "#O-PREFERRED-PREFIX", 
            FieldType.STRING, 8);
        pnd_Data_Out_Pnd_O_Preferred_Suffix = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_Suffix", "#O-PREFERRED-SUFFIX", 
            FieldType.STRING, 8);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Permanent_Ind = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Permanent_Ind", "#O-ACTV-RES-ADR-PERMANENT-IND", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Type_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Type_Code", "#O-ACTV-RES-ADR-TYPE-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_1 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_1", "#O-ACTV-RES-ADR-LINE-1", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_2 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_2", "#O-ACTV-RES-ADR-LINE-2", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_3 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_3", "#O-ACTV-RES-ADR-LINE-3", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_4 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_4", "#O-ACTV-RES-ADR-LINE-4", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_City = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_City", "#O-ACTV-RES-ADR-CITY", 
            FieldType.STRING, 25);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_St_Prov = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_St_Prov", "#O-ACTV-RES-ADR-ST-PROV", 
            FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip_Code", "#O-ACTV-RES-ADR-ZIP-CODE", 
            FieldType.STRING, 10);

        pnd_Data_Out__R_Field_7 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_7", "REDEFINE", pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip_Code);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip1_5 = pnd_Data_Out__R_Field_7.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip1_5", "#O-ACTV-RES-ADR-ZIP1-5", 
            FieldType.STRING, 5);

        pnd_Data_Out__R_Field_8 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_8", "REDEFINE", pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip_Code);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip1_7 = pnd_Data_Out__R_Field_8.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip1_7", "#O-ACTV-RES-ADR-ZIP1-7", 
            FieldType.STRING, 7);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Country = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Country", "#O-ACTV-RES-ADR-COUNTRY", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Postal_Data = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Postal_Data", "#O-ACTV-RES-ADR-POSTAL-DATA", 
            FieldType.STRING, 32);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Geographic_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Geographic_Code", 
            "#O-ACTV-RES-ADR-GEOGRAPHIC-CODE", FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Status_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Status_Code", "#O-ACTV-RES-ADR-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Status_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Status_Date", "#O-ACTV-RES-ADR-STATUS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Override_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Override_Code", "#O-ACTV-RES-ADR-OVERRIDE-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Last_Update_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Last_Update_Date", 
            "#O-ACTV-RES-ADR-LAST-UPDATE-DATE", FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Actv_Res_Adr_Last_Update_Time = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Res_Adr_Last_Update_Time", 
            "#O-ACTV-RES-ADR-LAST-UPDATE-TIME", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Start_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Start_Date", "#O-FUTR-RES-ADR-START-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Type_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Type_Code", "#O-FUTR-RES-ADR-TYPE-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_1 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_1", "#O-FUTR-RES-ADR-LINE-1", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_2 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_2", "#O-FUTR-RES-ADR-LINE-2", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_3 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_3", "#O-FUTR-RES-ADR-LINE-3", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_4 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_4", "#O-FUTR-RES-ADR-LINE-4", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_City = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_City", "#O-FUTR-RES-ADR-CITY", 
            FieldType.STRING, 25);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_St_Prov = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_St_Prov", "#O-FUTR-RES-ADR-ST-PROV", 
            FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip_Code", "#O-FUTR-RES-ADR-ZIP-CODE", 
            FieldType.STRING, 10);

        pnd_Data_Out__R_Field_9 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_9", "REDEFINE", pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip_Code);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip1_5 = pnd_Data_Out__R_Field_9.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip1_5", "#O-FUTR-RES-ADR-ZIP1-5", 
            FieldType.STRING, 5);

        pnd_Data_Out__R_Field_10 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_10", "REDEFINE", pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip_Code);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip1_7 = pnd_Data_Out__R_Field_10.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip1_7", "#O-FUTR-RES-ADR-ZIP1-7", 
            FieldType.STRING, 7);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Country = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Country", "#O-FUTR-RES-ADR-COUNTRY", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Postal_Data = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Postal_Data", "#O-FUTR-RES-ADR-POSTAL-DATA", 
            FieldType.STRING, 32);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Geographic_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Geographic_Code", 
            "#O-FUTR-RES-ADR-GEOGRAPHIC-CODE", FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Status_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Status_Code", "#O-FUTR-RES-ADR-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Status_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Status_Date", "#O-FUTR-RES-ADR-STATUS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Override_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Override_Code", "#O-FUTR-RES-ADR-OVERRIDE-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Last_Update_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Last_Update_Date", 
            "#O-FUTR-RES-ADR-LAST-UPDATE-DATE", FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Futr_Res_Adr_Last_Update_Time = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Res_Adr_Last_Update_Time", 
            "#O-FUTR-RES-ADR-LAST-UPDATE-TIME", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Pin_A12 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_A12", "#O-PIN-A12", FieldType.STRING, 12);

        pnd_Data_Out__R_Field_11 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_11", "REDEFINE", pnd_Data_Out_Pnd_O_Pin_A12);
        pnd_Data_Out_Pnd_O_Pin_N7 = pnd_Data_Out__R_Field_11.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_N7", "#O-PIN-N7", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Pin_Blanks = pnd_Data_Out__R_Field_11.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_Blanks", "#O-PIN-BLANKS", FieldType.STRING, 5);

        pnd_Data_Out__R_Field_12 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_12", "REDEFINE", pnd_Data_Out_Pnd_O_Pin_A12);
        pnd_Data_Out_Pnd_O_Pin_N12 = pnd_Data_Out__R_Field_12.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_N12", "#O-PIN-N12", FieldType.NUMERIC, 12);
        pnd_City_State_Zip = localVariables.newFieldInRecord("pnd_City_State_Zip", "#CITY-STATE-ZIP", FieldType.STRING, 35);
        pnd_Save_Code = localVariables.newFieldInRecord("pnd_Save_Code", "#SAVE-CODE", FieldType.STRING, 4);
        pnd_Save_Text = localVariables.newFieldInRecord("pnd_Save_Text", "#SAVE-TEXT", FieldType.STRING, 80);
        pnd_Pin_All_0s = localVariables.newFieldInRecord("pnd_Pin_All_0s", "#PIN-ALL-0S", FieldType.STRING, 12);
        pnd_Soc_All_0s = localVariables.newFieldInRecord("pnd_Soc_All_0s", "#SOC-ALL-0S", FieldType.STRING, 9);
        pnd_Pin_All_9s = localVariables.newFieldInRecord("pnd_Pin_All_9s", "#PIN-ALL-9S", FieldType.STRING, 12);
        pnd_Soc_All_9s = localVariables.newFieldInRecord("pnd_Soc_All_9s", "#SOC-ALL-9S", FieldType.STRING, 9);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Max_In.setInitialValue(72);
        pnd_Max_Out.setInitialValue(692);
        pnd_Pin_All_0s.setInitialValue("000000000000");
        pnd_Soc_All_0s.setInitialValue("000000000");
        pnd_Pin_All_9s.setInitialValue("999999999999");
        pnd_Soc_All_9s.setInitialValue("999999999");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Mdmn361a() throws Exception
    {
        super("Mdmn361a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  ASSIGN DEFAULTS
        short decideConditionsMet260 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #MDMA361.#I-SSN = 0 OR #MDMA361.#I-SSN-A9 = ' '
        if (condition(pdaMdma361.getPnd_Mdma361_Pnd_I_Ssn().equals(getZero()) || pdaMdma361.getPnd_Mdma361_Pnd_I_Ssn_A9().equals(" ")))
        {
            decideConditionsMet260++;
            pdaMdma361.getPnd_Mdma361_Pnd_I_Ssn_A9().setValue("000000000");                                                                                               //Natural: ASSIGN #MDMA361.#I-SSN-A9 := '000000000'
        }                                                                                                                                                                 //Natural: WHEN #MDMA361.#I-PIN-N12 = 0 OR #MDMA361.#I-PIN-A12 = ' '
        if (condition(pdaMdma361.getPnd_Mdma361_Pnd_I_Pin_N12().equals(getZero()) || pdaMdma361.getPnd_Mdma361_Pnd_I_Pin_A12().equals(" ")))
        {
            decideConditionsMet260++;
            pdaMdma361.getPnd_Mdma361_Pnd_I_Pin_A12().setValue("000000000000");                                                                                           //Natural: ASSIGN #MDMA361.#I-PIN-A12 := '000000000000'
        }                                                                                                                                                                 //Natural: WHEN #MDMA361.#I-PARTY-FLTR = ' '
        if (condition(pdaMdma361.getPnd_Mdma361_Pnd_I_Party_Fltr().equals(" ")))
        {
            decideConditionsMet260++;
            pdaMdma361.getPnd_Mdma361_Pnd_I_Party_Fltr().setValue("ACTIVE");                                                                                              //Natural: ASSIGN #MDMA361.#I-PARTY-FLTR := 'ACTIVE'
        }                                                                                                                                                                 //Natural: WHEN #MDMA361.#I-PRIVPREF-FLTR = ' '
        if (condition(pdaMdma361.getPnd_Mdma361_Pnd_I_Privpref_Fltr().equals(" ")))
        {
            decideConditionsMet260++;
            pdaMdma361.getPnd_Mdma361_Pnd_I_Privpref_Fltr().setValue("PARTICIPANT PREFERENCE");                                                                           //Natural: ASSIGN #MDMA361.#I-PRIVPREF-FLTR := 'PARTICIPANT PREFERENCE'
        }                                                                                                                                                                 //Natural: WHEN #MDMA361.#I-PARTY-INQ-LVL = 0
        if (condition(pdaMdma361.getPnd_Mdma361_Pnd_I_Party_Inq_Lvl().equals(getZero())))
        {
            decideConditionsMet260++;
            pdaMdma361.getPnd_Mdma361_Pnd_I_Party_Inq_Lvl().setValue(207);                                                                                                //Natural: ASSIGN #MDMA361.#I-PARTY-INQ-LVL := 207
        }                                                                                                                                                                 //Natural: WHEN #MDMA361.#I-ADDRESS-FLTR = ' '
        if (condition(pdaMdma361.getPnd_Mdma361_Pnd_I_Address_Fltr().equals(" ")))
        {
            decideConditionsMet260++;
            pdaMdma361.getPnd_Mdma361_Pnd_I_Address_Fltr().setValue("RESIDALL");                                                                                          //Natural: ASSIGN #MDMA361.#I-ADDRESS-FLTR := 'RESIDALL'
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet260 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition((pdaMdma361.getPnd_Mdma361_Pnd_I_Ssn().equals(getZero()) && pdaMdma361.getPnd_Mdma361_Pnd_I_Pin_N12().equals(getZero())) || (pdaMdma361.getPnd_Mdma361_Pnd_I_Ssn().equals(999999999)  //Natural: IF ( #MDMA361.#I-SSN = 0 AND #MDMA361.#I-PIN-N12 = 0 ) OR ( #MDMA361.#I-SSN = 999999999 AND #MDMA361.#I-PIN-N12 = 999999999999 ) OR ( #MDMA361.#I-SSN = 0 AND #MDMA361.#I-PIN-N12 = 999999999999 ) OR ( #MDMA361.#I-SSN = 999999999 AND #MDMA361.#I-PIN-N12 = 0 )
            && pdaMdma361.getPnd_Mdma361_Pnd_I_Pin_N12().equals(new DbsDecimal("999999999999"))) || (pdaMdma361.getPnd_Mdma361_Pnd_I_Ssn().equals(getZero()) 
            && pdaMdma361.getPnd_Mdma361_Pnd_I_Pin_N12().equals(new DbsDecimal("999999999999"))) || (pdaMdma361.getPnd_Mdma361_Pnd_I_Ssn().equals(999999999) 
            && pdaMdma361.getPnd_Mdma361_Pnd_I_Pin_N12().equals(getZero()))))
        {
            pdaMdma361.getPnd_Mdma361_Pnd_O_Return_Code().setValue("9999");                                                                                               //Natural: ASSIGN #O-RETURN-CODE := '9999'
            pdaMdma361.getPnd_Mdma361_Pnd_O_Return_Text().setValue("PIN and SSN must not be all 0s or all 9s");                                                           //Natural: ASSIGN #O-RETURN-TEXT := 'PIN and SSN must not be all 0s or all 9s'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FETCH-MDMP0010
        sub_Fetch_Mdmp0010();
        if (condition(Global.isEscape())) {return;}
        //*  CHECK IF PIN IS KEY
        //*  TRY IF THIS IS XREF
        if (condition(pnd_Return_Pnd_Rtrn_Code.notEquals("0000") && pdaMdma361.getPnd_Mdma361_Pnd_I_Ssn_A9().equals("000000000")))                                        //Natural: IF #RTRN-CODE NE '0000' AND #MDMA361.#I-SSN-A9 = '000000000'
        {
            pnd_Save_Code.setValue(pnd_Return_Pnd_Rtrn_Code);                                                                                                             //Natural: ASSIGN #SAVE-CODE := #RTRN-CODE
            pnd_Save_Text.setValue(pnd_Return_Pnd_Rtrn_Text);                                                                                                             //Natural: ASSIGN #SAVE-TEXT := #RTRN-TEXT
            pdaMdma361.getPnd_Mdma361_Pnd_I_Xref_Ind().setValue("Y");                                                                                                     //Natural: ASSIGN #MDMA361.#I-XREF-IND := 'Y'
                                                                                                                                                                          //Natural: PERFORM FETCH-MDMP0010
            sub_Fetch_Mdmp0010();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Pnd_Rtrn_Code.notEquals("0000")))                                                                                                    //Natural: IF #RTRN-CODE NE '0000'
            {
                pnd_Return_Pnd_Rtrn_Code.setValue(pnd_Save_Code);                                                                                                         //Natural: ASSIGN #RTRN-CODE := #SAVE-CODE
                pnd_Return_Pnd_Rtrn_Text.setValue(pnd_Save_Text);                                                                                                         //Natural: ASSIGN #RTRN-TEXT := #SAVE-TEXT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaMdma361.getPnd_Mdma361_Pnd_O_Return_Code().setValue(pnd_Return_Pnd_Rtrn_Code);                                                                                 //Natural: ASSIGN #O-RETURN-CODE := #RTRN-CODE
        pdaMdma361.getPnd_Mdma361_Pnd_O_Return_Text().setValue(pnd_Return_Pnd_Rtrn_Text);                                                                                 //Natural: ASSIGN #O-RETURN-TEXT := #RTRN-TEXT
        pdaMdma361.getPnd_Mdma361_Pnd_O_Pda_Data().setValuesByName(pnd_Data_Out_Pnd_Temp_Data);                                                                           //Natural: MOVE BY NAME #TEMP-DATA TO #O-PDA-DATA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FETCH-MDMP0010
        //*  PIN EXPAN - CONVERT 7-BYTE NUMERIC PIN WITH LEADING ZEROES TO ALPHA
        //*  WITH TRAILING BLANKS TO MATCH MDM's format
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DERIVE-MAINFRAME-FIELDS
        //*  PIN EXPANSION - CONVERT 7-BYTE PIN FORMAT BACK TO NUMERIC
    }
    private void sub_Fetch_Mdmp0010() throws Exception                                                                                                                    //Natural: FETCH-MDMP0010
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        gdaMdmg0001.getPnd_Pnd_Mdmg0001().reset();                                                                                                                        //Natural: RESET ##MDMG0001
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Date_Sent().setValueEdited(Global.getDATX(),new ReportEditMask("YYYY-MM-DD"));                                        //Natural: MOVE EDITED *DATX ( EM = YYYY-MM-DD ) TO ##MSG-DATE-SENT
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Time_Sent().setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                          //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO ##MSG-TIME-SENT
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Guid().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "mfsync::", Global.getINIT_PROGRAM(),                 //Natural: COMPRESS 'mfsync::' *INIT-PROGRAM #MDMA361.#I-PIN-A12 #MDMA361.#I-SSN-A9 ##MSG-DATE-SENT ##MSG-TIME-SENT INTO ##MSG-GUID LEAVING NO
            pdaMdma361.getPnd_Mdma361_Pnd_I_Pin_A12(), pdaMdma361.getPnd_Mdma361_Pnd_I_Ssn_A9(), gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Date_Sent(), 
            gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Time_Sent()));
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Ping_Ind().setValue("N");                                                                                             //Natural: ASSIGN ##MSG-PING-IND := 'N'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Sender_Appl_Id().setValue("LEGACY");                                                                                      //Natural: ASSIGN ##SENDER-APPL-ID := 'LEGACY'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Appl_Id().setValue("MDM");                                                                                            //Natural: ASSIGN ##TGT-APPL-ID := 'MDM'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Module_Code().setValue("N");                                                                                          //Natural: ASSIGN ##TGT-MODULE-CODE := 'N'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Module_Name().setValue("MDMN361");                                                                                    //Natural: ASSIGN ##TGT-MODULE-NAME := 'MDMN361'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Ctr().setValue(1);                                                                                                    //Natural: ASSIGN ##PDA-CTR := 1
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length().setValue(pnd_Max_In);                                                                                        //Natural: ASSIGN ##PDA-LENGTH := #MAX-IN
        pnd_Data_In_Pnd_I_Address_Fltr.setValue(pdaMdma361.getPnd_Mdma361_Pnd_I_Address_Fltr());                                                                          //Natural: ASSIGN #DATA-IN.#I-ADDRESS-FLTR := #MDMA361.#I-ADDRESS-FLTR
        pnd_Data_In_Pnd_I_Ssn.setValue(pdaMdma361.getPnd_Mdma361_Pnd_I_Ssn_A9());                                                                                         //Natural: ASSIGN #DATA-IN.#I-SSN := #MDMA361.#I-SSN-A9
        pnd_Data_In_Pnd_I_Xref_Ind.setValue(pdaMdma361.getPnd_Mdma361_Pnd_I_Xref_Ind());                                                                                  //Natural: ASSIGN #DATA-IN.#I-XREF-IND := #MDMA361.#I-XREF-IND
        pnd_Data_In_Pnd_I_Party_Fltr.setValue(pdaMdma361.getPnd_Mdma361_Pnd_I_Party_Fltr());                                                                              //Natural: ASSIGN #DATA-IN.#I-PARTY-FLTR := #MDMA361.#I-PARTY-FLTR
        pnd_Data_In_Pnd_I_Party_Inq_Lvl.setValue(pdaMdma361.getPnd_Mdma361_Pnd_I_Party_Inq_Lvl());                                                                        //Natural: ASSIGN #DATA-IN.#I-PARTY-INQ-LVL := #MDMA361.#I-PARTY-INQ-LVL
        pnd_Data_In_Pnd_I_Privpref_Fltr.setValue(pdaMdma361.getPnd_Mdma361_Pnd_I_Privpref_Fltr());                                                                        //Natural: ASSIGN #DATA-IN.#I-PRIVPREF-FLTR := #MDMA361.#I-PRIVPREF-FLTR
        pnd_Data_In_Pnd_I_Pin.setValue(0);                                                                                                                                //Natural: ASSIGN #DATA-IN.#I-PIN := 0
        pnd_Data_In_Pnd_I_Pin_A12.setValue(pdaMdma361.getPnd_Mdma361_Pnd_I_Pin_A12());                                                                                    //Natural: ASSIGN #DATA-IN.#I-PIN-A12 := #MDMA361.#I-PIN-A12
        if (condition(pnd_Data_In_Pnd_I_Pin_Zeroes.equals(getZero()) && pnd_Data_In_Pnd_I_Pin_N7.notEquals(getZero())))                                                   //Natural: IF #DATA-IN.#I-PIN-ZEROES = 0 AND #DATA-IN.#I-PIN-N7 NE 0
        {
            pnd_Data_In_Pnd_I_Pin_A7.setValue(pnd_Data_In_Pnd_I_Pin_N7);                                                                                                  //Natural: ASSIGN #DATA-IN.#I-PIN-A7 := #DATA-IN.#I-PIN-N7
            pnd_Data_In_Pnd_I_Pin_Blanks.setValue("     ");                                                                                                               //Natural: ASSIGN #DATA-IN.#I-PIN-BLANKS := '     '
        }                                                                                                                                                                 //Natural: END-IF
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_In().getValue("*").setValue(pnd_Data_In.getValue("*"));                                                              //Natural: ASSIGN ##DATA-IN ( * ) := #DATA-IN ( * )
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "=",gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Guid());                                                                                //Natural: WRITE '=' ##MSG-GUID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0010"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0010'
        if (condition(Global.isEscape())) return;
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Data Resp",gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue("*"));                                                     //Natural: WRITE 'Data Resp' ##DATA-RESPONSE ( * )
            if (Global.isEscape()) return;
            if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length().notEquals(pnd_Max_Out)))                                                                   //Natural: IF ##PDA-LENGTH NE #MAX-OUT
            {
                getReports().write(0, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length(),"Different PDA Length",pnd_Max_Out);                                           //Natural: WRITE ##PDA-LENGTH 'Different PDA Length' #MAX-OUT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue("*"));                                                         //Natural: ASSIGN #RETURN ( * ) := ##DATA-RESPONSE ( * )
        pnd_Data_Out.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Out().getValue("*"));                                                            //Natural: ASSIGN #DATA-OUT ( * ) := ##DATA-OUT ( * )
                                                                                                                                                                          //Natural: PERFORM DERIVE-MAINFRAME-FIELDS
        sub_Derive_Mainframe_Fields();
        if (condition(Global.isEscape())) {return;}
        //*  FETCH-MDMP0010
    }
    private void sub_Derive_Mainframe_Fields() throws Exception                                                                                                           //Natural: DERIVE-MAINFRAME-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_City_State_Zip.reset();                                                                                                                                       //Natural: RESET #CITY-STATE-ZIP
        //*  FOREIGN
        //*  CANADIAN
        short decideConditionsMet380 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #DATA-OUT.#O-ACTV-RES-ADR-TYPE-CODE = 'F'
        if (condition(pnd_Data_Out_Pnd_O_Actv_Res_Adr_Type_Code.equals("F")))
        {
            decideConditionsMet380++;
            pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip_Code.setValue("FORGN");                                                                                                   //Natural: ASSIGN #DATA-OUT.#O-ACTV-RES-ADR-ZIP-CODE := 'FORGN'
            pnd_Data_Out_Pnd_O_Actv_Res_Adr_Postal_Data.setValue("FORGN");                                                                                                //Natural: ASSIGN #DATA-OUT.#O-ACTV-RES-ADR-POSTAL-DATA := 'FORGN'
        }                                                                                                                                                                 //Natural: WHEN #DATA-OUT.#O-ACTV-RES-ADR-TYPE-CODE = 'C' OR #DATA-OUT.#O-ACTV-RES-ADR-COUNTRY = 'CANADA' OR = 'CA'
        if (condition(pnd_Data_Out_Pnd_O_Actv_Res_Adr_Type_Code.equals("C") || pnd_Data_Out_Pnd_O_Actv_Res_Adr_Country.equals("CANADA") || pnd_Data_Out_Pnd_O_Actv_Res_Adr_Country.equals("CA")))
        {
            decideConditionsMet380++;
            //*  NOT APPLICABLE
            //*  UNKNOWN
            if (condition(pnd_Data_Out_Pnd_O_Actv_Res_Adr_City.notEquals("N/A") && pnd_Data_Out_Pnd_O_Actv_Res_Adr_St_Prov.notEquals("UN")))                              //Natural: IF #DATA-OUT.#O-ACTV-RES-ADR-CITY NE 'N/A' AND #DATA-OUT.#O-ACTV-RES-ADR-ST-PROV NE 'UN'
            {
                pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Data_Out_Pnd_O_Actv_Res_Adr_City, pnd_Data_Out_Pnd_O_Actv_Res_Adr_St_Prov, pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip1_7)); //Natural: COMPRESS #DATA-OUT.#O-ACTV-RES-ADR-CITY #DATA-OUT.#O-ACTV-RES-ADR-ST-PROV #DATA-OUT.#O-ACTV-RES-ADR-ZIP1-7 INTO #CITY-STATE-ZIP
                //*  DOMESTIC
            }                                                                                                                                                             //Natural: END-IF
            pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip_Code.setValue("CANAD");                                                                                                   //Natural: ASSIGN #DATA-OUT.#O-ACTV-RES-ADR-ZIP-CODE := 'CANAD'
            pnd_Data_Out_Pnd_O_Actv_Res_Adr_Postal_Data.setValue("CANAD");                                                                                                //Natural: ASSIGN #DATA-OUT.#O-ACTV-RES-ADR-POSTAL-DATA := 'CANAD'
            pnd_Data_Out_Pnd_O_Actv_Res_Adr_Country.setValue("CANADA");                                                                                                   //Natural: ASSIGN #DATA-OUT.#O-ACTV-RES-ADR-COUNTRY := 'CANADA'
        }                                                                                                                                                                 //Natural: WHEN #DATA-OUT.#O-ACTV-RES-ADR-TYPE-CODE = 'U' OR = 'D' OR #DATA-OUT.#O-ACTV-RES-ADR-COUNTRY = 'USA' OR = 'US'
        if (condition(pnd_Data_Out_Pnd_O_Actv_Res_Adr_Type_Code.equals("U") || pnd_Data_Out_Pnd_O_Actv_Res_Adr_Type_Code.equals("D") || pnd_Data_Out_Pnd_O_Actv_Res_Adr_Country.equals("USA") 
            || pnd_Data_Out_Pnd_O_Actv_Res_Adr_Country.equals("US")))
        {
            decideConditionsMet380++;
            //*  NOT APPLICABLE
            //*  UNKNOWN
            if (condition(pnd_Data_Out_Pnd_O_Actv_Res_Adr_City.notEquals("N/A") && pnd_Data_Out_Pnd_O_Actv_Res_Adr_St_Prov.notEquals("UN")))                              //Natural: IF #DATA-OUT.#O-ACTV-RES-ADR-CITY NE 'N/A' AND #DATA-OUT.#O-ACTV-RES-ADR-ST-PROV NE 'UN'
            {
                pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Data_Out_Pnd_O_Actv_Res_Adr_City, pnd_Data_Out_Pnd_O_Actv_Res_Adr_St_Prov, pnd_Data_Out_Pnd_O_Actv_Res_Adr_Zip1_5)); //Natural: COMPRESS #DATA-OUT.#O-ACTV-RES-ADR-CITY #DATA-OUT.#O-ACTV-RES-ADR-ST-PROV #DATA-OUT.#O-ACTV-RES-ADR-ZIP1-5 INTO #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Data_Out_Pnd_O_Actv_Res_Adr_Country.setValue("USA");                                                                                                      //Natural: ASSIGN #DATA-OUT.#O-ACTV-RES-ADR-COUNTRY := 'USA'
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet380 > 0))
        {
            short decideConditionsMet402 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #DATA-OUT.#O-ACTV-RES-ADR-LINE-1 = ' '
            if (condition(pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_1.equals(" ")))
            {
                decideConditionsMet402++;
                pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_1.setValue(pnd_City_State_Zip);                                                                                      //Natural: ASSIGN #DATA-OUT.#O-ACTV-RES-ADR-LINE-1 := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-ACTV-RES-ADR-LINE-2 = ' '
            else if (condition(pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_2.equals(" ")))
            {
                decideConditionsMet402++;
                pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_2.setValue(pnd_City_State_Zip);                                                                                      //Natural: ASSIGN #DATA-OUT.#O-ACTV-RES-ADR-LINE-2 := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-ACTV-RES-ADR-LINE-3 = ' '
            else if (condition(pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_3.equals(" ")))
            {
                decideConditionsMet402++;
                pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_3.setValue(pnd_City_State_Zip);                                                                                      //Natural: ASSIGN #DATA-OUT.#O-ACTV-RES-ADR-LINE-3 := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-ACTV-RES-ADR-LINE-4 = ' '
            else if (condition(pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_4.equals(" ")))
            {
                decideConditionsMet402++;
                pnd_Data_Out_Pnd_O_Actv_Res_Adr_Line_4.setValue(pnd_City_State_Zip);                                                                                      //Natural: ASSIGN #DATA-OUT.#O-ACTV-RES-ADR-LINE-4 := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet380 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_1.notEquals(" ")))                                                                                             //Natural: IF #DATA-OUT.#O-FUTR-RES-ADR-LINE-1 NE ' '
        {
            pnd_City_State_Zip.reset();                                                                                                                                   //Natural: RESET #CITY-STATE-ZIP
            //*  FOREIGN
            //*  CANADIAN
            short decideConditionsMet423 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #DATA-OUT.#O-FUTR-RES-ADR-TYPE-CODE = 'F'
            if (condition(pnd_Data_Out_Pnd_O_Futr_Res_Adr_Type_Code.equals("F")))
            {
                decideConditionsMet423++;
                pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip_Code.setValue("FORGN");                                                                                               //Natural: ASSIGN #DATA-OUT.#O-FUTR-RES-ADR-ZIP-CODE := 'FORGN'
                pnd_Data_Out_Pnd_O_Futr_Res_Adr_Postal_Data.setValue("FORGN");                                                                                            //Natural: ASSIGN #DATA-OUT.#O-FUTR-RES-ADR-POSTAL-DATA := 'FORGN'
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-FUTR-RES-ADR-TYPE-CODE = 'C' OR #DATA-OUT.#O-FUTR-RES-ADR-COUNTRY = 'CANADA' OR = 'CA'
            if (condition(pnd_Data_Out_Pnd_O_Futr_Res_Adr_Type_Code.equals("C") || pnd_Data_Out_Pnd_O_Futr_Res_Adr_Country.equals("CANADA") || pnd_Data_Out_Pnd_O_Futr_Res_Adr_Country.equals("CA")))
            {
                decideConditionsMet423++;
                //*  NOT APPLICABLE
                //*  UNKNOWN
                if (condition(pnd_Data_Out_Pnd_O_Futr_Res_Adr_City.notEquals("N/A") && pnd_Data_Out_Pnd_O_Futr_Res_Adr_St_Prov.notEquals("UN")))                          //Natural: IF #DATA-OUT.#O-FUTR-RES-ADR-CITY NE 'N/A' AND #DATA-OUT.#O-FUTR-RES-ADR-ST-PROV NE 'UN'
                {
                    pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Data_Out_Pnd_O_Futr_Res_Adr_City, pnd_Data_Out_Pnd_O_Futr_Res_Adr_St_Prov, pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip1_7)); //Natural: COMPRESS #DATA-OUT.#O-FUTR-RES-ADR-CITY #DATA-OUT.#O-FUTR-RES-ADR-ST-PROV #DATA-OUT.#O-FUTR-RES-ADR-ZIP1-7 INTO #CITY-STATE-ZIP
                    //*  DOMESTIC
                }                                                                                                                                                         //Natural: END-IF
                pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip_Code.setValue("CANAD");                                                                                               //Natural: ASSIGN #DATA-OUT.#O-FUTR-RES-ADR-ZIP-CODE := 'CANAD'
                pnd_Data_Out_Pnd_O_Futr_Res_Adr_Postal_Data.setValue("CANAD");                                                                                            //Natural: ASSIGN #DATA-OUT.#O-FUTR-RES-ADR-POSTAL-DATA := 'CANAD'
                pnd_Data_Out_Pnd_O_Futr_Res_Adr_Country.setValue("CANADA");                                                                                               //Natural: ASSIGN #DATA-OUT.#O-FUTR-RES-ADR-COUNTRY := 'CANADA'
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-FUTR-RES-ADR-TYPE-CODE = 'U' OR = 'D' OR #DATA-OUT.#O-FUTR-RES-ADR-COUNTRY = 'USA' OR = 'US'
            if (condition(pnd_Data_Out_Pnd_O_Futr_Res_Adr_Type_Code.equals("U") || pnd_Data_Out_Pnd_O_Futr_Res_Adr_Type_Code.equals("D") || pnd_Data_Out_Pnd_O_Futr_Res_Adr_Country.equals("USA") 
                || pnd_Data_Out_Pnd_O_Futr_Res_Adr_Country.equals("US")))
            {
                decideConditionsMet423++;
                //*  NOT APPLICABLE
                //*  UNKNOWN
                if (condition(pnd_Data_Out_Pnd_O_Futr_Res_Adr_City.notEquals("N/A") && pnd_Data_Out_Pnd_O_Futr_Res_Adr_St_Prov.notEquals("UN")))                          //Natural: IF #DATA-OUT.#O-FUTR-RES-ADR-CITY NE 'N/A' AND #DATA-OUT.#O-FUTR-RES-ADR-ST-PROV NE 'UN'
                {
                    pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Data_Out_Pnd_O_Futr_Res_Adr_City, pnd_Data_Out_Pnd_O_Futr_Res_Adr_St_Prov, pnd_Data_Out_Pnd_O_Futr_Res_Adr_Zip1_5)); //Natural: COMPRESS #DATA-OUT.#O-FUTR-RES-ADR-CITY #DATA-OUT.#O-FUTR-RES-ADR-ST-PROV #DATA-OUT.#O-FUTR-RES-ADR-ZIP1-5 INTO #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Data_Out_Pnd_O_Futr_Res_Adr_Country.setValue("USA");                                                                                                  //Natural: ASSIGN #DATA-OUT.#O-FUTR-RES-ADR-COUNTRY := 'USA'
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet423 > 0))
            {
                short decideConditionsMet445 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #DATA-OUT.#O-FUTR-RES-ADR-LINE-1 = ' '
                if (condition(pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_1.equals(" ")))
                {
                    decideConditionsMet445++;
                    pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_1.setValue(pnd_City_State_Zip);                                                                                  //Natural: ASSIGN #DATA-OUT.#O-FUTR-RES-ADR-LINE-1 := #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: WHEN #DATA-OUT.#O-FUTR-RES-ADR-LINE-2 = ' '
                else if (condition(pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_2.equals(" ")))
                {
                    decideConditionsMet445++;
                    pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_2.setValue(pnd_City_State_Zip);                                                                                  //Natural: ASSIGN #DATA-OUT.#O-FUTR-RES-ADR-LINE-2 := #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: WHEN #DATA-OUT.#O-FUTR-RES-ADR-LINE-3 = ' '
                else if (condition(pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_3.equals(" ")))
                {
                    decideConditionsMet445++;
                    pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_3.setValue(pnd_City_State_Zip);                                                                                  //Natural: ASSIGN #DATA-OUT.#O-FUTR-RES-ADR-LINE-3 := #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: WHEN #DATA-OUT.#O-FUTR-RES-ADR-LINE-4 = ' '
                else if (condition(pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_4.equals(" ")))
                {
                    decideConditionsMet445++;
                    pnd_Data_Out_Pnd_O_Futr_Res_Adr_Line_4.setValue(pnd_City_State_Zip);                                                                                  //Natural: ASSIGN #DATA-OUT.#O-FUTR-RES-ADR-LINE-4 := #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet423 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Data_Out_Pnd_O_Pin.setValue(0);                                                                                                                               //Natural: ASSIGN #DATA-OUT.#O-PIN := 0
        if (condition(pnd_Data_Out_Pnd_O_Pin_Blanks.equals(" ") && pnd_Data_Out_Pnd_O_Pin_A12.notEquals(" ")))                                                            //Natural: IF #DATA-OUT.#O-PIN-BLANKS = ' ' AND #O-PIN-A12 NE ' '
        {
            pnd_Data_Out_Pnd_O_Pin_N12.setValue(pnd_Data_Out_Pnd_O_Pin_N7);                                                                                               //Natural: ASSIGN #DATA-OUT.#O-PIN-N12 := #DATA-OUT.#O-PIN-N7
        }                                                                                                                                                                 //Natural: END-IF
        //*  DERIVE-MAINFRAME-FIELDS
    }

    //
}
