/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:32:39 AM
**        * FROM NATURAL SUBPROGRAM : Nasn8003
************************************************************
**        * FILE NAME            : Nasn8003.java
**        * CLASS NAME           : Nasn8003
**        * INSTANCE NAME        : Nasn8003
************************************************************
**********************************************************************
************************************************************************
* PGM NAME    : NASN8003
* DESCRIPTION : PROCESS CWF NAS UNIT CODE TABLE (TABLE ID 003)
*               THIS FUNCTION WILL LOAD TABLE ID 003 TO BE PROCESSED BY
*               CALLING PROGRAM.
* WRITTEN BY  : FRANCIS ENDAYA
* DATE WRITTEN: 7/2001
************************************************************************
*                                                                      *

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nasn8003 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaNasa8003 pdaNasa8003;
    private LdaNasl810v ldaNasl810v;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Areas;
    private DbsField pnd_Work_Areas_Pnd_Rec_Found;
    private DbsField pnd_Work_Areas_Pnd_Wk_Data_Key;

    private DbsGroup pnd_Work_Areas__R_Field_1;
    private DbsField pnd_Work_Areas_Pnd_Wk_Unit;

    private DbsGroup pnd_Work_Areas__R_Field_2;
    private DbsField pnd_Work_Areas_Pnd_Wk_Unit_Code;
    private DbsField pnd_Work_Areas_Pnd_Wk_Region;
    private DbsField pnd_Work_Areas_Pnd_Wk_Designation;
    private DbsField pnd_Work_Areas_Pnd_Wk_Filler;
    private DbsField pnd_Work_Areas_Pnd_Wk_Maint_Sw;
    private DbsField pnd_Work_Areas_Pnd_Wk_Stat_Sw;
    private DbsField pnd_Work_Areas_Pnd_Wk_Ia_Work_Sw;
    private DbsField pnd_Work_Areas_Pnd_Wk_Verification_Sw;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaNasl810v = new LdaNasl810v();
        registerRecord(ldaNasl810v);
        registerRecord(ldaNasl810v.getVw_table_View());

        // parameters
        parameters = new DbsRecord();
        pdaNasa8003 = new PdaNasa8003(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Areas = localVariables.newGroupInRecord("pnd_Work_Areas", "#WORK-AREAS");
        pnd_Work_Areas_Pnd_Rec_Found = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Rec_Found", "#REC-FOUND", FieldType.BOOLEAN, 1);
        pnd_Work_Areas_Pnd_Wk_Data_Key = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Data_Key", "#WK-DATA-KEY", FieldType.STRING, 15);

        pnd_Work_Areas__R_Field_1 = pnd_Work_Areas.newGroupInGroup("pnd_Work_Areas__R_Field_1", "REDEFINE", pnd_Work_Areas_Pnd_Wk_Data_Key);
        pnd_Work_Areas_Pnd_Wk_Unit = pnd_Work_Areas__R_Field_1.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Unit", "#WK-UNIT", FieldType.STRING, 7);

        pnd_Work_Areas__R_Field_2 = pnd_Work_Areas__R_Field_1.newGroupInGroup("pnd_Work_Areas__R_Field_2", "REDEFINE", pnd_Work_Areas_Pnd_Wk_Unit);
        pnd_Work_Areas_Pnd_Wk_Unit_Code = pnd_Work_Areas__R_Field_2.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Unit_Code", "#WK-UNIT-CODE", FieldType.STRING, 
            5);
        pnd_Work_Areas_Pnd_Wk_Region = pnd_Work_Areas__R_Field_2.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Region", "#WK-REGION", FieldType.STRING, 1);
        pnd_Work_Areas_Pnd_Wk_Designation = pnd_Work_Areas__R_Field_2.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Designation", "#WK-DESIGNATION", FieldType.STRING, 
            1);
        pnd_Work_Areas_Pnd_Wk_Filler = pnd_Work_Areas__R_Field_1.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Filler", "#WK-FILLER", FieldType.STRING, 4);
        pnd_Work_Areas_Pnd_Wk_Maint_Sw = pnd_Work_Areas__R_Field_1.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Maint_Sw", "#WK-MAINT-SW", FieldType.STRING, 
            1);
        pnd_Work_Areas_Pnd_Wk_Stat_Sw = pnd_Work_Areas__R_Field_1.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Stat_Sw", "#WK-STAT-SW", FieldType.STRING, 1);
        pnd_Work_Areas_Pnd_Wk_Ia_Work_Sw = pnd_Work_Areas__R_Field_1.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Ia_Work_Sw", "#WK-IA-WORK-SW", FieldType.STRING, 
            1);
        pnd_Work_Areas_Pnd_Wk_Verification_Sw = pnd_Work_Areas__R_Field_1.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Verification_Sw", "#WK-VERIFICATION-SW", 
            FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaNasl810v.initializeValues();

        localVariables.reset();
        pnd_Work_Areas_Pnd_Rec_Found.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Nasn8003() throws Exception
    {
        super("Nasn8003");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  ***********************************************************
        //*  *****   M A I N    R O U T I N E   ************************
        pdaNasa8003.getPnd_Nasa8003_Pnd_Msg().reset();                                                                                                                    //Natural: RESET #MSG
        //*  DETERMINE UNIT CODE
        if (condition(pdaNasa8003.getPnd_Nasa8003_Pnd_Action_Cde().equals("U")))                                                                                          //Natural: IF #NASA8003.#ACTION-CDE EQ 'U'
        {
            ldaNasl810v.getVw_table_View().startDatabaseFind                                                                                                              //Natural: FIND TABLE-VIEW WITH TABLE-VIEW.TABLE-ID EQ 003
            (
            "FN1",
            new Wc[] { new Wc("TABLE_ID", "=", 3, WcType.WITH) }
            );
            FN1:
            while (condition(ldaNasl810v.getVw_table_View().readNextRow("FN1", true)))
            {
                ldaNasl810v.getVw_table_View().setIfNotFoundControlFlag(false);
                if (condition(ldaNasl810v.getVw_table_View().getAstCOUNTER().equals(0)))                                                                                  //Natural: IF NO RECORDS FOUND
                {
                    pdaNasa8003.getPnd_Nasa8003_Pnd_Msg().setValue(" No records found ");                                                                                 //Natural: ASSIGN #MSG := ' No records found '
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Work_Areas_Pnd_Wk_Data_Key.setValue(ldaNasl810v.getTable_View_Data_Key().getSubstring(1,15));                                                         //Natural: MOVE SUBSTR ( TABLE-VIEW.DATA-KEY,1,15 ) TO #WK-DATA-KEY
                short decideConditionsMet90 = 0;                                                                                                                          //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #NASA8003.#MAINTENANCE-SW EQ 'Y'
                if (condition(pdaNasa8003.getPnd_Nasa8003_Pnd_Maintenance_Sw().equals("Y")))
                {
                    decideConditionsMet90++;
                    if (condition(pnd_Work_Areas_Pnd_Wk_Maint_Sw.equals("Y") && pnd_Work_Areas_Pnd_Wk_Designation.equals("D")))                                           //Natural: IF #WK-MAINT-SW EQ 'Y' AND #WK-DESIGNATION EQ 'D'
                    {
                                                                                                                                                                          //Natural: PERFORM C1000-MOVE-UNIT-TO-PDA
                        sub_C1000_Move_Unit_To_Pda();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FN1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FN1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NASA8003.#GENERIC-SW EQ 'Y'
                else if (condition(pdaNasa8003.getPnd_Nasa8003_Pnd_Generic_Sw().equals("Y")))
                {
                    decideConditionsMet90++;
                    if (condition(pnd_Work_Areas_Pnd_Wk_Maint_Sw.equals("Y") && pnd_Work_Areas_Pnd_Wk_Designation.equals(" ")))                                           //Natural: IF #WK-MAINT-SW EQ 'Y' AND #WK-DESIGNATION EQ ' '
                    {
                                                                                                                                                                          //Natural: PERFORM C1000-MOVE-UNIT-TO-PDA
                        sub_C1000_Move_Unit_To_Pda();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FN1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FN1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NASA8003.#RESEARCH-SW EQ 'Y'
                else if (condition(pdaNasa8003.getPnd_Nasa8003_Pnd_Research_Sw().equals("Y")))
                {
                    decideConditionsMet90++;
                    if (condition(pnd_Work_Areas_Pnd_Wk_Stat_Sw.equals("Y")))                                                                                             //Natural: IF #WK-STAT-SW EQ 'Y'
                    {
                                                                                                                                                                          //Natural: PERFORM C1000-MOVE-UNIT-TO-PDA
                        sub_C1000_Move_Unit_To_Pda();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FN1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FN1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NASA8003.#IA-WORK-SW EQ 'Y'
                else if (condition(pdaNasa8003.getPnd_Nasa8003_Pnd_Ia_Work_Sw().equals("Y")))
                {
                    decideConditionsMet90++;
                    if (condition(pnd_Work_Areas_Pnd_Wk_Ia_Work_Sw.equals("Y")))                                                                                          //Natural: IF #WK-IA-WORK-SW EQ 'Y'
                    {
                                                                                                                                                                          //Natural: PERFORM C1000-MOVE-UNIT-TO-PDA
                        sub_C1000_Move_Unit_To_Pda();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FN1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FN1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NASA8003.#VERIFICATION-SW EQ 'Y'
                else if (condition(pdaNasa8003.getPnd_Nasa8003_Pnd_Verification_Sw().equals("Y")))
                {
                    decideConditionsMet90++;
                    if (condition(pnd_Work_Areas_Pnd_Wk_Verification_Sw.equals("Y")))                                                                                     //Natural: IF #WK-VERIFICATION-SW EQ 'Y'
                    {
                                                                                                                                                                          //Natural: PERFORM C1000-MOVE-UNIT-TO-PDA
                        sub_C1000_Move_Unit_To_Pda();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FN1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FN1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pdaNasa8003.getPnd_Nasa8003_Pnd_Msg().setValue(" No active switch found. ");                                                                          //Natural: ASSIGN #NASA8003.#MSG := ' No active switch found. '
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            //*  DETERMINE ACCESS ALLOWED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Areas_Pnd_Rec_Found.resetInitial();                                                                                                                  //Natural: RESET INITIAL #REC-FOUND
            ldaNasl810v.getVw_table_View().startDatabaseFind                                                                                                              //Natural: FIND TABLE-VIEW WITH TABLE-VIEW.TABLE-ID EQ 003
            (
            "FN2",
            new Wc[] { new Wc("TABLE_ID", "=", 3, WcType.WITH) }
            );
            FN2:
            while (condition(ldaNasl810v.getVw_table_View().readNextRow("FN2", true)))
            {
                ldaNasl810v.getVw_table_View().setIfNotFoundControlFlag(false);
                if (condition(ldaNasl810v.getVw_table_View().getAstCOUNTER().equals(0)))                                                                                  //Natural: IF NO RECORDS FOUND
                {
                    pdaNasa8003.getPnd_Nasa8003_Pnd_Msg().setValue(" No records found ");                                                                                 //Natural: ASSIGN #MSG := ' No records found '
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Work_Areas_Pnd_Wk_Data_Key.setValue(ldaNasl810v.getTable_View_Data_Key().getSubstring(1,15));                                                         //Natural: MOVE SUBSTR ( TABLE-VIEW.DATA-KEY,1,15 ) TO #WK-DATA-KEY
                if (condition(pnd_Work_Areas_Pnd_Wk_Unit.equals(pdaNasa8003.getPnd_Nasa8003_Pnd_Unit_Code_Input())))                                                      //Natural: IF #WK-UNIT EQ #UNIT-CODE-INPUT
                {
                    pnd_Work_Areas_Pnd_Rec_Found.setValue(true);                                                                                                          //Natural: ASSIGN #REC-FOUND := TRUE
                    pdaNasa8003.getPnd_Nasa8003_Pnd_Maintenance_Sw().setValue("N");                                                                                       //Natural: ASSIGN #NASA8003.#MAINTENANCE-SW := 'N'
                    pdaNasa8003.getPnd_Nasa8003_Pnd_Research_Sw().setValue("N");                                                                                          //Natural: ASSIGN #NASA8003.#RESEARCH-SW := 'N'
                    pdaNasa8003.getPnd_Nasa8003_Pnd_Ia_Work_Sw().setValue("N");                                                                                           //Natural: ASSIGN #NASA8003.#IA-WORK-SW := 'N'
                    pdaNasa8003.getPnd_Nasa8003_Pnd_Verification_Sw().setValue("N");                                                                                      //Natural: ASSIGN #NASA8003.#VERIFICATION-SW := 'N'
                    short decideConditionsMet140 = 0;                                                                                                                     //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #WK-MAINT-SW EQ 'Y'
                    if (condition(pnd_Work_Areas_Pnd_Wk_Maint_Sw.equals("Y")))
                    {
                        decideConditionsMet140++;
                        pdaNasa8003.getPnd_Nasa8003_Pnd_Maintenance_Sw().setValue("Y");                                                                                   //Natural: ASSIGN #NASA8003.#MAINTENANCE-SW := 'Y'
                    }                                                                                                                                                     //Natural: WHEN #WK-STAT-SW EQ 'Y'
                    if (condition(pnd_Work_Areas_Pnd_Wk_Stat_Sw.equals("Y")))
                    {
                        decideConditionsMet140++;
                        pdaNasa8003.getPnd_Nasa8003_Pnd_Research_Sw().setValue("Y");                                                                                      //Natural: ASSIGN #NASA8003.#RESEARCH-SW := 'Y'
                    }                                                                                                                                                     //Natural: WHEN #WK-IA-WORK-SW EQ 'Y'
                    if (condition(pnd_Work_Areas_Pnd_Wk_Ia_Work_Sw.equals("Y")))
                    {
                        decideConditionsMet140++;
                        pdaNasa8003.getPnd_Nasa8003_Pnd_Ia_Work_Sw().setValue("Y");                                                                                       //Natural: ASSIGN #NASA8003.#IA-WORK-SW := 'Y'
                    }                                                                                                                                                     //Natural: WHEN #WK-VERIFICATION-SW EQ 'Y'
                    if (condition(pnd_Work_Areas_Pnd_Wk_Verification_Sw.equals("Y")))
                    {
                        decideConditionsMet140++;
                        pdaNasa8003.getPnd_Nasa8003_Pnd_Verification_Sw().setValue("Y");                                                                                  //Natural: ASSIGN #NASA8003.#VERIFICATION-SW := 'Y'
                    }                                                                                                                                                     //Natural: WHEN NONE
                    if (condition(decideConditionsMet140 == 0))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            if (condition(! (pnd_Work_Areas_Pnd_Rec_Found.getBoolean())))                                                                                                 //Natural: IF NOT #REC-FOUND
            {
                pdaNasa8003.getPnd_Nasa8003_Pnd_Msg().setValue(DbsUtil.compress(" This Unit code does not exist on table,", pdaNasa8003.getPnd_Nasa8003_Pnd_Unit_Code_Input())); //Natural: COMPRESS ' This Unit code does not exist on table,' #UNIT-CODE-INPUT INTO #NASA8003.#MSG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C1000-MOVE-UNIT-TO-PDA
        //* ******************************************************************
    }
    private void sub_C1000_Move_Unit_To_Pda() throws Exception                                                                                                            //Natural: C1000-MOVE-UNIT-TO-PDA
    {
        if (BLNatReinput.isReinput()) return;

        pdaNasa8003.getPnd_Nasa8003_Pnd_Unit_Code_Return().setValue(pnd_Work_Areas_Pnd_Wk_Unit);                                                                          //Natural: ASSIGN #NASA8003.#UNIT-CODE-RETURN := #WK-UNIT
        pdaNasa8003.getPnd_Nasa8003_Pnd_Unit_Desc().setValue(ldaNasl810v.getTable_View_Data_Description().getSubstring(1,50));                                            //Natural: MOVE SUBSTR ( TABLE-VIEW.DATA-DESCRIPTION,1,50 ) TO #NASA8003.#UNIT-DESC
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
        //*  C1000-MOVE-UNIT-TO-PDA
    }

    //
}
