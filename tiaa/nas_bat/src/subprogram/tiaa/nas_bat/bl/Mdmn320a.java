/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:29:29 AM
**        * FROM NATURAL SUBPROGRAM : Mdmn320a
************************************************************
**        * FILE NAME            : Mdmn320a.java
**        * CLASS NAME           : Mdmn320a
**        * INSTANCE NAME        : Mdmn320a
************************************************************
************************************************************************
* PROGRAM NAME : MDMN320A - CLONE OF MDMN320 TO FETCH MDMP0010
* DESCRIPTION  : MDM RETRIEVE ACTIVE, FUTURE, TEMPORARYADDRESS
*                THIS SUBPROGRAM IS CALLED TO GET MDM ADDRESS INFO.
* WRITTEN BY   : DENNIS DURAN
* DATE WRITTEN : JUNE 11, 2009
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
*
* 08/08/2014  S. ZAKI   FIX ON THE SERVICE SIDE FOR CREF CONTRACT BLANK
*                       SETTING CONTRACTINQUIRYLEVEL=3 INSTEAD OF 103
*                       LABEL: SZBLKCRF
* 06/21/2017  D. MEADE  PIN EXPANSION. SEE PINE COMMENTS.
* 09/06/2017  D. MEADE  SET TIAA-CREF IND TO 'I' FOR ITD CONTRACTS.
*                       SEE "ITD" COMMENTS.
************************************************************************

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mdmn320a extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    public DbsRecord parameters;
    private PdaMdma321 pdaMdma321;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Max_In;
    private DbsField pnd_Max_Out;
    private DbsField pnd_Data_In;

    private DbsGroup pnd_Data_In__R_Field_1;
    private DbsField pnd_Data_In_Pnd_I_Tiaa_Cref_Ind;
    private DbsField pnd_Data_In_Pnd_I_Contract_Number;
    private DbsField pnd_Data_In_Pnd_I_Payee_Code_A2;
    private DbsField pnd_Data_In_Pnd_I_Address_Usage;
    private DbsField pnd_Data_In_Pnd_I_Party_Inq_Lvl;
    private DbsField pnd_Data_In_Pnd_I_Address_Fltr;
    private DbsField pnd_Data_In_Pnd_I_Cntrct_Inq_Lvl;
    private DbsField pnd_Data_In_Pnd_I_Cntrct_Role_Fltr;
    private DbsField pnd_Return;

    private DbsGroup pnd_Return__R_Field_2;
    private DbsField pnd_Return_Pnd_Rtrn_Code;
    private DbsField pnd_Return_Pnd_Rtrn_Text;
    private DbsField pnd_Data_Out;

    private DbsGroup pnd_Data_Out__R_Field_3;

    private DbsGroup pnd_Data_Out_Pnd_Temp_Data;
    private DbsField pnd_Data_Out_Pnd_O_Ssn;
    private DbsField pnd_Data_Out_Pnd_O_Pin;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_Last_Name;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_First_Name;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_Middle_Name;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_Prefix;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_Suffix;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Number;
    private DbsField pnd_Data_Out_Pnd_O_Payee_Code;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Status_Code;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Status_Year;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Issue_Date;
    private DbsField pnd_Data_Out_Pnd_O_Line_Of_Business;
    private DbsField pnd_Data_Out_Pnd_O_Cref_Number;
    private DbsField pnd_Data_Out_Pnd_O_Cref_Issued_Ind;
    private DbsField pnd_Data_Out_Pnd_O_Ownership_Code;
    private DbsField pnd_Data_Out_Pnd_O_Option_Code;
    private DbsField pnd_Data_Out_Pnd_O_Plan_Code;
    private DbsField pnd_Data_Out_Pnd_O_Fund_Code;
    private DbsField pnd_Data_Out_Pnd_O_Social_Code;
    private DbsField pnd_Data_Out_Pnd_O_Custodial_Agreement;
    private DbsField pnd_Data_Out_Pnd_O_Aas_Ind;
    private DbsField pnd_Data_Out_Pnd_O_Platform_Ind;
    private DbsField pnd_Data_Out_Pnd_O_Da_Multi_Plan_Ind;
    private DbsField pnd_Data_Out_Pnd_O_Tnt_Tiaa_Mailed;
    private DbsField pnd_Data_Out_Pnd_O_Tnt_Cref_Mailed;
    private DbsField pnd_Data_Out_Pnd_O_Default_Enrollment_Ind;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Add_Date;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Last_Update_Date;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Last_Update_Time;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Last_Update_Userid;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Permanent_Ind;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Type_Code;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Line_1;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Line_2;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Line_3;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Line_4;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_City;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_St_Prov;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Zip_Code;

    private DbsGroup pnd_Data_Out__R_Field_4;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Zip1_5;

    private DbsGroup pnd_Data_Out__R_Field_5;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Zip1_7;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Country;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Check_Saving_Ind;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Bank_Account_Number;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Bank_Aba_Eft_Number;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Eft_Pay_Type;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Postal_Data;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Geographic_Code;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Status_Code;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Status_Date;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Override_Code;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Last_Update_Date;
    private DbsField pnd_Data_Out_Pnd_O_Actv_Address_Last_Update_Time;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Start_Date;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Type_Code;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Line_1;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Line_2;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Line_3;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Line_4;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_City;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_St_Prov;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Zip_Code;

    private DbsGroup pnd_Data_Out__R_Field_6;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Zip1_5;

    private DbsGroup pnd_Data_Out__R_Field_7;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Zip1_7;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Country;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Check_Saving_Ind;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Bank_Account_Number;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Bank_Aba_Eft_Number;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Eft_Pay_Type;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Postal_Data;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Geographic_Code;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Status_Code;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Status_Date;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Override_Code;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Last_Update_Date;
    private DbsField pnd_Data_Out_Pnd_O_Futr_Address_Last_Update_Time;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Start_Date;

    private DbsGroup pnd_Data_Out__R_Field_8;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Start_Yyyy;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Start_Mmdd;

    private DbsGroup pnd_Data_Out__R_Field_9;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Start_Mm;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Start_Dd;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_End_Date;

    private DbsGroup pnd_Data_Out__R_Field_10;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_End_Yyyy;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_End_Mmdd;

    private DbsGroup pnd_Data_Out__R_Field_11;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_End_Mm;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_End_Dd;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Annual_Ind;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Type_Code;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Line_1;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Line_2;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Line_3;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Line_4;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_City;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_St_Prov;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Zip_Code;

    private DbsGroup pnd_Data_Out__R_Field_12;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Zip1_5;

    private DbsGroup pnd_Data_Out__R_Field_13;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Zip1_7;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Country;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Check_Saving_Ind;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Bank_Account_Number;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Bank_Aba_Eft_Number;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Eft_Pay_Type;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Postal_Data;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Geographic_Code;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Status_Code;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Status_Date;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Override_Code;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Last_Update_Date;
    private DbsField pnd_Data_Out_Pnd_O_Temp_Address_Last_Update_Time;
    private DbsField pnd_Data_Out_Pnd_O_Pin_A12;

    private DbsGroup pnd_Data_Out__R_Field_14;
    private DbsField pnd_Data_Out_Pnd_O_Pin_N7;
    private DbsField pnd_Data_Out_Pnd_O_Pin_Blanks;

    private DbsGroup pnd_Data_Out__R_Field_15;
    private DbsField pnd_Data_Out_Pnd_O_Pin_N12;
    private DbsField pnd_City_State_Zip;
    private DbsField pnd_Save_Code;
    private DbsField pnd_Save_Text;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        // parameters
        parameters = new DbsRecord();
        pdaMdma321 = new PdaMdma321(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Max_In = localVariables.newFieldInRecord("pnd_Max_In", "#MAX-IN", FieldType.NUMERIC, 5);
        pnd_Max_Out = localVariables.newFieldInRecord("pnd_Max_Out", "#MAX-OUT", FieldType.NUMERIC, 5);
        pnd_Data_In = localVariables.newFieldArrayInRecord("pnd_Data_In", "#DATA-IN", FieldType.STRING, 1, new DbsArrayController(1, 32560));

        pnd_Data_In__R_Field_1 = localVariables.newGroupInRecord("pnd_Data_In__R_Field_1", "REDEFINE", pnd_Data_In);
        pnd_Data_In_Pnd_I_Tiaa_Cref_Ind = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Tiaa_Cref_Ind", "#I-TIAA-CREF-IND", FieldType.STRING, 
            1);
        pnd_Data_In_Pnd_I_Contract_Number = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Contract_Number", "#I-CONTRACT-NUMBER", FieldType.STRING, 
            10);
        pnd_Data_In_Pnd_I_Payee_Code_A2 = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Payee_Code_A2", "#I-PAYEE-CODE-A2", FieldType.STRING, 
            2);
        pnd_Data_In_Pnd_I_Address_Usage = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Address_Usage", "#I-ADDRESS-USAGE", FieldType.STRING, 
            1);
        pnd_Data_In_Pnd_I_Party_Inq_Lvl = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Party_Inq_Lvl", "#I-PARTY-INQ-LVL", FieldType.NUMERIC, 
            3);
        pnd_Data_In_Pnd_I_Address_Fltr = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Address_Fltr", "#I-ADDRESS-FLTR", FieldType.STRING, 
            9);
        pnd_Data_In_Pnd_I_Cntrct_Inq_Lvl = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Cntrct_Inq_Lvl", "#I-CNTRCT-INQ-LVL", FieldType.NUMERIC, 
            3);
        pnd_Data_In_Pnd_I_Cntrct_Role_Fltr = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Cntrct_Role_Fltr", "#I-CNTRCT-ROLE-FLTR", FieldType.STRING, 
            8);
        pnd_Return = localVariables.newFieldArrayInRecord("pnd_Return", "#RETURN", FieldType.STRING, 1, new DbsArrayController(1, 200));

        pnd_Return__R_Field_2 = localVariables.newGroupInRecord("pnd_Return__R_Field_2", "REDEFINE", pnd_Return);
        pnd_Return_Pnd_Rtrn_Code = pnd_Return__R_Field_2.newFieldInGroup("pnd_Return_Pnd_Rtrn_Code", "#RTRN-CODE", FieldType.STRING, 4);
        pnd_Return_Pnd_Rtrn_Text = pnd_Return__R_Field_2.newFieldInGroup("pnd_Return_Pnd_Rtrn_Text", "#RTRN-TEXT", FieldType.STRING, 80);
        pnd_Data_Out = localVariables.newFieldArrayInRecord("pnd_Data_Out", "#DATA-OUT", FieldType.STRING, 1, new DbsArrayController(1, 32360));

        pnd_Data_Out__R_Field_3 = localVariables.newGroupInRecord("pnd_Data_Out__R_Field_3", "REDEFINE", pnd_Data_Out);

        pnd_Data_Out_Pnd_Temp_Data = pnd_Data_Out__R_Field_3.newGroupInGroup("pnd_Data_Out_Pnd_Temp_Data", "#TEMP-DATA");
        pnd_Data_Out_Pnd_O_Ssn = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Ssn", "#O-SSN", FieldType.NUMERIC, 9);
        pnd_Data_Out_Pnd_O_Pin = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin", "#O-PIN", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Preferred_Last_Name = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_Last_Name", "#O-PREFERRED-LAST-NAME", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Preferred_First_Name = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_First_Name", "#O-PREFERRED-FIRST-NAME", 
            FieldType.STRING, 30);
        pnd_Data_Out_Pnd_O_Preferred_Middle_Name = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_Middle_Name", "#O-PREFERRED-MIDDLE-NAME", 
            FieldType.STRING, 30);
        pnd_Data_Out_Pnd_O_Preferred_Prefix = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_Prefix", "#O-PREFERRED-PREFIX", 
            FieldType.STRING, 8);
        pnd_Data_Out_Pnd_O_Preferred_Suffix = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_Suffix", "#O-PREFERRED-SUFFIX", 
            FieldType.STRING, 8);
        pnd_Data_Out_Pnd_O_Contract_Number = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Number", "#O-CONTRACT-NUMBER", FieldType.STRING, 
            10);
        pnd_Data_Out_Pnd_O_Payee_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Payee_Code", "#O-PAYEE-CODE", FieldType.NUMERIC, 
            2);
        pnd_Data_Out_Pnd_O_Contract_Status_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Status_Code", "#O-CONTRACT-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Contract_Status_Year = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Status_Year", "#O-CONTRACT-STATUS-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Data_Out_Pnd_O_Contract_Issue_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Issue_Date", "#O-CONTRACT-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Line_Of_Business = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Line_Of_Business", "#O-LINE-OF-BUSINESS", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Cref_Number = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Cref_Number", "#O-CREF-NUMBER", FieldType.STRING, 
            10);
        pnd_Data_Out_Pnd_O_Cref_Issued_Ind = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Cref_Issued_Ind", "#O-CREF-ISSUED-IND", FieldType.STRING, 
            1);
        pnd_Data_Out_Pnd_O_Ownership_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Ownership_Code", "#O-OWNERSHIP-CODE", FieldType.STRING, 
            1);
        pnd_Data_Out_Pnd_O_Option_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Option_Code", "#O-OPTION-CODE", FieldType.STRING, 
            2);
        pnd_Data_Out_Pnd_O_Plan_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Plan_Code", "#O-PLAN-CODE", FieldType.STRING, 5);
        pnd_Data_Out_Pnd_O_Fund_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Fund_Code", "#O-FUND-CODE", FieldType.STRING, 3);
        pnd_Data_Out_Pnd_O_Social_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Social_Code", "#O-SOCIAL-CODE", FieldType.STRING, 
            3);
        pnd_Data_Out_Pnd_O_Custodial_Agreement = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Custodial_Agreement", "#O-CUSTODIAL-AGREEMENT", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Aas_Ind = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Aas_Ind", "#O-AAS-IND", FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Platform_Ind = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Platform_Ind", "#O-PLATFORM-IND", FieldType.STRING, 
            1);
        pnd_Data_Out_Pnd_O_Da_Multi_Plan_Ind = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Da_Multi_Plan_Ind", "#O-DA-MULTI-PLAN-IND", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Tnt_Tiaa_Mailed = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Tnt_Tiaa_Mailed", "#O-TNT-TIAA-MAILED", FieldType.STRING, 
            1);
        pnd_Data_Out_Pnd_O_Tnt_Cref_Mailed = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Tnt_Cref_Mailed", "#O-TNT-CREF-MAILED", FieldType.STRING, 
            1);
        pnd_Data_Out_Pnd_O_Default_Enrollment_Ind = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Default_Enrollment_Ind", "#O-DEFAULT-ENROLLMENT-IND", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Contract_Add_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Add_Date", "#O-CONTRACT-ADD-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Contract_Last_Update_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Last_Update_Date", "#O-CONTRACT-LAST-UPDATE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Contract_Last_Update_Time = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Last_Update_Time", "#O-CONTRACT-LAST-UPDATE-TIME", 
            FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Contract_Last_Update_Userid = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Last_Update_Userid", 
            "#O-CONTRACT-LAST-UPDATE-USERID", FieldType.STRING, 8);
        pnd_Data_Out_Pnd_O_Actv_Address_Permanent_Ind = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Permanent_Ind", "#O-ACTV-ADDRESS-PERMANENT-IND", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Actv_Address_Type_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Type_Code", "#O-ACTV-ADDRESS-TYPE-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Actv_Address_Line_1 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Line_1", "#O-ACTV-ADDRESS-LINE-1", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Actv_Address_Line_2 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Line_2", "#O-ACTV-ADDRESS-LINE-2", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Actv_Address_Line_3 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Line_3", "#O-ACTV-ADDRESS-LINE-3", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Actv_Address_Line_4 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Line_4", "#O-ACTV-ADDRESS-LINE-4", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Actv_Address_City = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_City", "#O-ACTV-ADDRESS-CITY", 
            FieldType.STRING, 25);
        pnd_Data_Out_Pnd_O_Actv_Address_St_Prov = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_St_Prov", "#O-ACTV-ADDRESS-ST-PROV", 
            FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Actv_Address_Zip_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Zip_Code", "#O-ACTV-ADDRESS-ZIP-CODE", 
            FieldType.STRING, 10);

        pnd_Data_Out__R_Field_4 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_4", "REDEFINE", pnd_Data_Out_Pnd_O_Actv_Address_Zip_Code);
        pnd_Data_Out_Pnd_O_Actv_Address_Zip1_5 = pnd_Data_Out__R_Field_4.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Zip1_5", "#O-ACTV-ADDRESS-ZIP1-5", 
            FieldType.STRING, 5);

        pnd_Data_Out__R_Field_5 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_5", "REDEFINE", pnd_Data_Out_Pnd_O_Actv_Address_Zip_Code);
        pnd_Data_Out_Pnd_O_Actv_Address_Zip1_7 = pnd_Data_Out__R_Field_5.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Zip1_7", "#O-ACTV-ADDRESS-ZIP1-7", 
            FieldType.STRING, 7);
        pnd_Data_Out_Pnd_O_Actv_Address_Country = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Country", "#O-ACTV-ADDRESS-COUNTRY", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Actv_Check_Saving_Ind = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Check_Saving_Ind", "#O-ACTV-CHECK-SAVING-IND", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Actv_Bank_Account_Number = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Bank_Account_Number", "#O-ACTV-BANK-ACCOUNT-NUMBER", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Actv_Bank_Aba_Eft_Number = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Bank_Aba_Eft_Number", "#O-ACTV-BANK-ABA-EFT-NUMBER", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Actv_Eft_Pay_Type = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Eft_Pay_Type", "#O-ACTV-EFT-PAY-TYPE", 
            FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Actv_Address_Postal_Data = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Postal_Data", "#O-ACTV-ADDRESS-POSTAL-DATA", 
            FieldType.STRING, 32);
        pnd_Data_Out_Pnd_O_Actv_Address_Geographic_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Geographic_Code", 
            "#O-ACTV-ADDRESS-GEOGRAPHIC-CODE", FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Actv_Address_Status_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Status_Code", "#O-ACTV-ADDRESS-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Actv_Address_Status_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Status_Date", "#O-ACTV-ADDRESS-STATUS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Actv_Address_Override_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Override_Code", "#O-ACTV-ADDRESS-OVERRIDE-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Actv_Address_Last_Update_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Last_Update_Date", 
            "#O-ACTV-ADDRESS-LAST-UPDATE-DATE", FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Actv_Address_Last_Update_Time = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Actv_Address_Last_Update_Time", 
            "#O-ACTV-ADDRESS-LAST-UPDATE-TIME", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Futr_Address_Start_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Start_Date", "#O-FUTR-ADDRESS-START-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Futr_Address_Type_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Type_Code", "#O-FUTR-ADDRESS-TYPE-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Futr_Address_Line_1 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Line_1", "#O-FUTR-ADDRESS-LINE-1", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Futr_Address_Line_2 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Line_2", "#O-FUTR-ADDRESS-LINE-2", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Futr_Address_Line_3 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Line_3", "#O-FUTR-ADDRESS-LINE-3", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Futr_Address_Line_4 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Line_4", "#O-FUTR-ADDRESS-LINE-4", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Futr_Address_City = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_City", "#O-FUTR-ADDRESS-CITY", 
            FieldType.STRING, 25);
        pnd_Data_Out_Pnd_O_Futr_Address_St_Prov = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_St_Prov", "#O-FUTR-ADDRESS-ST-PROV", 
            FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Futr_Address_Zip_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Zip_Code", "#O-FUTR-ADDRESS-ZIP-CODE", 
            FieldType.STRING, 10);

        pnd_Data_Out__R_Field_6 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_6", "REDEFINE", pnd_Data_Out_Pnd_O_Futr_Address_Zip_Code);
        pnd_Data_Out_Pnd_O_Futr_Address_Zip1_5 = pnd_Data_Out__R_Field_6.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Zip1_5", "#O-FUTR-ADDRESS-ZIP1-5", 
            FieldType.STRING, 5);

        pnd_Data_Out__R_Field_7 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_7", "REDEFINE", pnd_Data_Out_Pnd_O_Futr_Address_Zip_Code);
        pnd_Data_Out_Pnd_O_Futr_Address_Zip1_7 = pnd_Data_Out__R_Field_7.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Zip1_7", "#O-FUTR-ADDRESS-ZIP1-7", 
            FieldType.STRING, 7);
        pnd_Data_Out_Pnd_O_Futr_Address_Country = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Country", "#O-FUTR-ADDRESS-COUNTRY", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Futr_Check_Saving_Ind = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Check_Saving_Ind", "#O-FUTR-CHECK-SAVING-IND", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Futr_Bank_Account_Number = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Bank_Account_Number", "#O-FUTR-BANK-ACCOUNT-NUMBER", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Futr_Bank_Aba_Eft_Number = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Bank_Aba_Eft_Number", "#O-FUTR-BANK-ABA-EFT-NUMBER", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Futr_Eft_Pay_Type = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Eft_Pay_Type", "#O-FUTR-EFT-PAY-TYPE", 
            FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Futr_Address_Postal_Data = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Postal_Data", "#O-FUTR-ADDRESS-POSTAL-DATA", 
            FieldType.STRING, 32);
        pnd_Data_Out_Pnd_O_Futr_Address_Geographic_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Geographic_Code", 
            "#O-FUTR-ADDRESS-GEOGRAPHIC-CODE", FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Futr_Address_Status_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Status_Code", "#O-FUTR-ADDRESS-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Futr_Address_Status_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Status_Date", "#O-FUTR-ADDRESS-STATUS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Futr_Address_Override_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Override_Code", "#O-FUTR-ADDRESS-OVERRIDE-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Futr_Address_Last_Update_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Last_Update_Date", 
            "#O-FUTR-ADDRESS-LAST-UPDATE-DATE", FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Futr_Address_Last_Update_Time = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Futr_Address_Last_Update_Time", 
            "#O-FUTR-ADDRESS-LAST-UPDATE-TIME", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Temp_Address_Start_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Start_Date", "#O-TEMP-ADDRESS-START-DATE", 
            FieldType.NUMERIC, 8);

        pnd_Data_Out__R_Field_8 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_8", "REDEFINE", pnd_Data_Out_Pnd_O_Temp_Address_Start_Date);
        pnd_Data_Out_Pnd_O_Temp_Address_Start_Yyyy = pnd_Data_Out__R_Field_8.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Start_Yyyy", "#O-TEMP-ADDRESS-START-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Data_Out_Pnd_O_Temp_Address_Start_Mmdd = pnd_Data_Out__R_Field_8.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Start_Mmdd", "#O-TEMP-ADDRESS-START-MMDD", 
            FieldType.NUMERIC, 4);

        pnd_Data_Out__R_Field_9 = pnd_Data_Out__R_Field_8.newGroupInGroup("pnd_Data_Out__R_Field_9", "REDEFINE", pnd_Data_Out_Pnd_O_Temp_Address_Start_Mmdd);
        pnd_Data_Out_Pnd_O_Temp_Address_Start_Mm = pnd_Data_Out__R_Field_9.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Start_Mm", "#O-TEMP-ADDRESS-START-MM", 
            FieldType.NUMERIC, 2);
        pnd_Data_Out_Pnd_O_Temp_Address_Start_Dd = pnd_Data_Out__R_Field_9.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Start_Dd", "#O-TEMP-ADDRESS-START-DD", 
            FieldType.NUMERIC, 2);
        pnd_Data_Out_Pnd_O_Temp_Address_End_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_End_Date", "#O-TEMP-ADDRESS-END-DATE", 
            FieldType.NUMERIC, 8);

        pnd_Data_Out__R_Field_10 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_10", "REDEFINE", pnd_Data_Out_Pnd_O_Temp_Address_End_Date);
        pnd_Data_Out_Pnd_O_Temp_Address_End_Yyyy = pnd_Data_Out__R_Field_10.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_End_Yyyy", "#O-TEMP-ADDRESS-END-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Data_Out_Pnd_O_Temp_Address_End_Mmdd = pnd_Data_Out__R_Field_10.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_End_Mmdd", "#O-TEMP-ADDRESS-END-MMDD", 
            FieldType.NUMERIC, 4);

        pnd_Data_Out__R_Field_11 = pnd_Data_Out__R_Field_10.newGroupInGroup("pnd_Data_Out__R_Field_11", "REDEFINE", pnd_Data_Out_Pnd_O_Temp_Address_End_Mmdd);
        pnd_Data_Out_Pnd_O_Temp_Address_End_Mm = pnd_Data_Out__R_Field_11.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_End_Mm", "#O-TEMP-ADDRESS-END-MM", 
            FieldType.NUMERIC, 2);
        pnd_Data_Out_Pnd_O_Temp_Address_End_Dd = pnd_Data_Out__R_Field_11.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_End_Dd", "#O-TEMP-ADDRESS-END-DD", 
            FieldType.NUMERIC, 2);
        pnd_Data_Out_Pnd_O_Temp_Address_Annual_Ind = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Annual_Ind", "#O-TEMP-ADDRESS-ANNUAL-IND", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Temp_Address_Type_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Type_Code", "#O-TEMP-ADDRESS-TYPE-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Temp_Address_Line_1 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Line_1", "#O-TEMP-ADDRESS-LINE-1", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Temp_Address_Line_2 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Line_2", "#O-TEMP-ADDRESS-LINE-2", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Temp_Address_Line_3 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Line_3", "#O-TEMP-ADDRESS-LINE-3", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Temp_Address_Line_4 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Line_4", "#O-TEMP-ADDRESS-LINE-4", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Temp_Address_City = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_City", "#O-TEMP-ADDRESS-CITY", 
            FieldType.STRING, 25);
        pnd_Data_Out_Pnd_O_Temp_Address_St_Prov = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_St_Prov", "#O-TEMP-ADDRESS-ST-PROV", 
            FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Temp_Address_Zip_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Zip_Code", "#O-TEMP-ADDRESS-ZIP-CODE", 
            FieldType.STRING, 10);

        pnd_Data_Out__R_Field_12 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_12", "REDEFINE", pnd_Data_Out_Pnd_O_Temp_Address_Zip_Code);
        pnd_Data_Out_Pnd_O_Temp_Address_Zip1_5 = pnd_Data_Out__R_Field_12.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Zip1_5", "#O-TEMP-ADDRESS-ZIP1-5", 
            FieldType.STRING, 5);

        pnd_Data_Out__R_Field_13 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_13", "REDEFINE", pnd_Data_Out_Pnd_O_Temp_Address_Zip_Code);
        pnd_Data_Out_Pnd_O_Temp_Address_Zip1_7 = pnd_Data_Out__R_Field_13.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Zip1_7", "#O-TEMP-ADDRESS-ZIP1-7", 
            FieldType.STRING, 7);
        pnd_Data_Out_Pnd_O_Temp_Address_Country = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Country", "#O-TEMP-ADDRESS-COUNTRY", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Temp_Check_Saving_Ind = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Check_Saving_Ind", "#O-TEMP-CHECK-SAVING-IND", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Temp_Bank_Account_Number = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Bank_Account_Number", "#O-TEMP-BANK-ACCOUNT-NUMBER", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Temp_Bank_Aba_Eft_Number = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Bank_Aba_Eft_Number", "#O-TEMP-BANK-ABA-EFT-NUMBER", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Temp_Eft_Pay_Type = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Eft_Pay_Type", "#O-TEMP-EFT-PAY-TYPE", 
            FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Temp_Address_Postal_Data = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Postal_Data", "#O-TEMP-ADDRESS-POSTAL-DATA", 
            FieldType.STRING, 32);
        pnd_Data_Out_Pnd_O_Temp_Address_Geographic_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Geographic_Code", 
            "#O-TEMP-ADDRESS-GEOGRAPHIC-CODE", FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Temp_Address_Status_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Status_Code", "#O-TEMP-ADDRESS-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Temp_Address_Status_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Status_Date", "#O-TEMP-ADDRESS-STATUS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Temp_Address_Override_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Override_Code", "#O-TEMP-ADDRESS-OVERRIDE-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Temp_Address_Last_Update_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Last_Update_Date", 
            "#O-TEMP-ADDRESS-LAST-UPDATE-DATE", FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Temp_Address_Last_Update_Time = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Temp_Address_Last_Update_Time", 
            "#O-TEMP-ADDRESS-LAST-UPDATE-TIME", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Pin_A12 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_A12", "#O-PIN-A12", FieldType.STRING, 12);

        pnd_Data_Out__R_Field_14 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_14", "REDEFINE", pnd_Data_Out_Pnd_O_Pin_A12);
        pnd_Data_Out_Pnd_O_Pin_N7 = pnd_Data_Out__R_Field_14.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_N7", "#O-PIN-N7", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Pin_Blanks = pnd_Data_Out__R_Field_14.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_Blanks", "#O-PIN-BLANKS", FieldType.STRING, 5);

        pnd_Data_Out__R_Field_15 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_15", "REDEFINE", pnd_Data_Out_Pnd_O_Pin_A12);
        pnd_Data_Out_Pnd_O_Pin_N12 = pnd_Data_Out__R_Field_15.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_N12", "#O-PIN-N12", FieldType.NUMERIC, 12);
        pnd_City_State_Zip = localVariables.newFieldInRecord("pnd_City_State_Zip", "#CITY-STATE-ZIP", FieldType.STRING, 35);
        pnd_Save_Code = localVariables.newFieldInRecord("pnd_Save_Code", "#SAVE-CODE", FieldType.STRING, 4);
        pnd_Save_Text = localVariables.newFieldInRecord("pnd_Save_Text", "#SAVE-TEXT", FieldType.STRING, 80);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Max_In.setInitialValue(37);
        pnd_Max_Out.setInitialValue(1289);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Mdmn320a() throws Exception
    {
        super("Mdmn320a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  ASSIGN DEFAULTS
        //*  ITD
        //*  DEFAULT TO TIAA
        //*  DEFAULT TO CORRESPONDENCE
        short decideConditionsMet415 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN SUBSTRING ( #MDMA321.#I-CONTRACT-NUMBER,1,3 ) = 'ITD'
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_I_Contract_Number().getSubstring(1,3).equals("ITD")))
        {
            decideConditionsMet415++;
            pdaMdma321.getPnd_Mdma321_Pnd_I_Tiaa_Cref_Ind().setValue("I");                                                                                                //Natural: ASSIGN #MDMA321.#I-TIAA-CREF-IND := 'I'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#I-TIAA-CREF-IND = ' '
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_I_Tiaa_Cref_Ind().equals(" ")))
        {
            decideConditionsMet415++;
            pdaMdma321.getPnd_Mdma321_Pnd_I_Tiaa_Cref_Ind().setValue("T");                                                                                                //Natural: ASSIGN #MDMA321.#I-TIAA-CREF-IND := 'T'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#I-PAYEE-CODE = 0 OR #MDMA321.#I-PAYEE-CODE-A2 = ' '
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_I_Payee_Code().equals(getZero()) || pdaMdma321.getPnd_Mdma321_Pnd_I_Payee_Code_A2().equals(" ")))
        {
            decideConditionsMet415++;
            pdaMdma321.getPnd_Mdma321_Pnd_I_Payee_Code_A2().setValue("00");                                                                                               //Natural: ASSIGN #MDMA321.#I-PAYEE-CODE-A2 := '00'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#I-ADDRESS-USAGE = ' '
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_I_Address_Usage().equals(" ")))
        {
            decideConditionsMet415++;
            pdaMdma321.getPnd_Mdma321_Pnd_I_Address_Usage().setValue("O");                                                                                                //Natural: ASSIGN #MDMA321.#I-ADDRESS-USAGE := 'O'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#I-PARTY-INQ-LVL = 0
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_I_Party_Inq_Lvl().equals(getZero())))
        {
            decideConditionsMet415++;
            pdaMdma321.getPnd_Mdma321_Pnd_I_Party_Inq_Lvl().setValue(207);                                                                                                //Natural: ASSIGN #MDMA321.#I-PARTY-INQ-LVL := 207
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#I-ADDRESS-FLTR = ' '
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_I_Address_Fltr().equals(" ")))
        {
            decideConditionsMet415++;
            pdaMdma321.getPnd_Mdma321_Pnd_I_Address_Fltr().setValue("ALL");                                                                                               //Natural: ASSIGN #MDMA321.#I-ADDRESS-FLTR := 'ALL'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#I-CNTRCT-INQ-LVL = 0
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_I_Cntrct_Inq_Lvl().equals(getZero())))
        {
            decideConditionsMet415++;
            pdaMdma321.getPnd_Mdma321_Pnd_I_Cntrct_Inq_Lvl().setValue(3);                                                                                                 //Natural: ASSIGN #MDMA321.#I-CNTRCT-INQ-LVL := 3
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#I-CNTRCT-ROLE-FLTR = ' '
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_I_Cntrct_Role_Fltr().equals(" ")))
        {
            decideConditionsMet415++;
            pdaMdma321.getPnd_Mdma321_Pnd_I_Cntrct_Role_Fltr().setValue("ALL");                                                                                           //Natural: ASSIGN #MDMA321.#I-CNTRCT-ROLE-FLTR := 'ALL'
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet415 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM FETCH-MDMP0010
        sub_Fetch_Mdmp0010();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Pnd_Rtrn_Code.notEquals("0000")))                                                                                                        //Natural: IF #RTRN-CODE NE '0000'
        {
            pnd_Save_Code.setValue(pnd_Return_Pnd_Rtrn_Code);                                                                                                             //Natural: ASSIGN #SAVE-CODE := #RTRN-CODE
            pnd_Save_Text.setValue(pnd_Return_Pnd_Rtrn_Text);                                                                                                             //Natural: ASSIGN #SAVE-TEXT := #RTRN-TEXT
            //*  ITD
            //*  TRY IF THIS IS CREF
            if (condition(pdaMdma321.getPnd_Mdma321_Pnd_I_Tiaa_Cref_Ind().notEquals("I")))                                                                                //Natural: IF #MDMA321.#I-TIAA-CREF-IND NE 'I'
            {
                pdaMdma321.getPnd_Mdma321_Pnd_I_Tiaa_Cref_Ind().setValue("C");                                                                                            //Natural: ASSIGN #MDMA321.#I-TIAA-CREF-IND := 'C'
                                                                                                                                                                          //Natural: PERFORM FETCH-MDMP0010
                sub_Fetch_Mdmp0010();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Return_Pnd_Rtrn_Code.notEquals("0000")))                                                                                                //Natural: IF #RTRN-CODE NE '0000'
                {
                    pnd_Return_Pnd_Rtrn_Code.setValue(pnd_Save_Code);                                                                                                     //Natural: ASSIGN #RTRN-CODE := #SAVE-CODE
                    pnd_Return_Pnd_Rtrn_Text.setValue(pnd_Save_Text);                                                                                                     //Natural: ASSIGN #RTRN-TEXT := #SAVE-TEXT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaMdma321.getPnd_Mdma321_Pnd_O_Return_Code().setValue(pnd_Return_Pnd_Rtrn_Code);                                                                                 //Natural: ASSIGN #O-RETURN-CODE := #RTRN-CODE
        pdaMdma321.getPnd_Mdma321_Pnd_O_Return_Text().setValue(pnd_Return_Pnd_Rtrn_Text);                                                                                 //Natural: ASSIGN #O-RETURN-TEXT := #RTRN-TEXT
        pdaMdma321.getPnd_Mdma321_Pnd_O_Pda_Data().setValuesByName(pnd_Data_Out_Pnd_Temp_Data);                                                                           //Natural: MOVE BY NAME #TEMP-DATA TO #O-PDA-DATA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FETCH-MDMP0010
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DERIVE-MAINFRAME-FIELDS
        //*  PIN EXPANSION - CONVERT 7-BYTE PIN FROM ALPHA TO NUMERIC
    }
    private void sub_Fetch_Mdmp0010() throws Exception                                                                                                                    //Natural: FETCH-MDMP0010
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        gdaMdmg0001.getPnd_Pnd_Mdmg0001().reset();                                                                                                                        //Natural: RESET ##MDMG0001
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Date_Sent().setValueEdited(Global.getDATX(),new ReportEditMask("YYYY-MM-DD"));                                        //Natural: MOVE EDITED *DATX ( EM = YYYY-MM-DD ) TO ##MSG-DATE-SENT
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Time_Sent().setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                          //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO ##MSG-TIME-SENT
        //*  PINE
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Guid().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "mfsync::", Global.getINIT_PROGRAM(),                 //Natural: COMPRESS 'mfsync::' *INIT-PROGRAM #MDMA321.#I-TIAA-CREF-IND #MDMA321.#I-CONTRACT-NUMBER #MDMA321.#I-PAYEE-CODE-A2 ##MSG-DATE-SENT ##MSG-TIME-SENT INTO ##MSG-GUID LEAVING NO
            pdaMdma321.getPnd_Mdma321_Pnd_I_Tiaa_Cref_Ind(), pdaMdma321.getPnd_Mdma321_Pnd_I_Contract_Number(), pdaMdma321.getPnd_Mdma321_Pnd_I_Payee_Code_A2(), 
            gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Date_Sent(), gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Time_Sent()));
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Ping_Ind().setValue("N");                                                                                             //Natural: ASSIGN ##MSG-PING-IND := 'N'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Sender_Appl_Id().setValue("LEGACY");                                                                                      //Natural: ASSIGN ##SENDER-APPL-ID := 'LEGACY'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Appl_Id().setValue("MDM");                                                                                            //Natural: ASSIGN ##TGT-APPL-ID := 'MDM'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Module_Code().setValue("N");                                                                                          //Natural: ASSIGN ##TGT-MODULE-CODE := 'N'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Module_Name().setValue("MDMN321");                                                                                    //Natural: ASSIGN ##TGT-MODULE-NAME := 'MDMN321'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Ctr().setValue(1);                                                                                                    //Natural: ASSIGN ##PDA-CTR := 1
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length().setValue(pnd_Max_In);                                                                                        //Natural: ASSIGN ##PDA-LENGTH := #MAX-IN
        pnd_Data_In_Pnd_I_Tiaa_Cref_Ind.setValue(pdaMdma321.getPnd_Mdma321_Pnd_I_Tiaa_Cref_Ind());                                                                        //Natural: ASSIGN #DATA-IN.#I-TIAA-CREF-IND := #MDMA321.#I-TIAA-CREF-IND
        pnd_Data_In_Pnd_I_Contract_Number.setValue(pdaMdma321.getPnd_Mdma321_Pnd_I_Contract_Number());                                                                    //Natural: ASSIGN #DATA-IN.#I-CONTRACT-NUMBER := #MDMA321.#I-CONTRACT-NUMBER
        pnd_Data_In_Pnd_I_Payee_Code_A2.setValue(pdaMdma321.getPnd_Mdma321_Pnd_I_Payee_Code_A2());                                                                        //Natural: ASSIGN #DATA-IN.#I-PAYEE-CODE-A2 := #MDMA321.#I-PAYEE-CODE-A2
        pnd_Data_In_Pnd_I_Address_Usage.setValue(pdaMdma321.getPnd_Mdma321_Pnd_I_Address_Usage());                                                                        //Natural: ASSIGN #DATA-IN.#I-ADDRESS-USAGE := #MDMA321.#I-ADDRESS-USAGE
        pnd_Data_In_Pnd_I_Party_Inq_Lvl.setValue(pdaMdma321.getPnd_Mdma321_Pnd_I_Party_Inq_Lvl());                                                                        //Natural: ASSIGN #DATA-IN.#I-PARTY-INQ-LVL := #MDMA321.#I-PARTY-INQ-LVL
        pnd_Data_In_Pnd_I_Address_Fltr.setValue(pdaMdma321.getPnd_Mdma321_Pnd_I_Address_Fltr());                                                                          //Natural: ASSIGN #DATA-IN.#I-ADDRESS-FLTR := #MDMA321.#I-ADDRESS-FLTR
        pnd_Data_In_Pnd_I_Cntrct_Inq_Lvl.setValue(pdaMdma321.getPnd_Mdma321_Pnd_I_Cntrct_Inq_Lvl());                                                                      //Natural: ASSIGN #DATA-IN.#I-CNTRCT-INQ-LVL := #MDMA321.#I-CNTRCT-INQ-LVL
        pnd_Data_In_Pnd_I_Cntrct_Role_Fltr.setValue(pdaMdma321.getPnd_Mdma321_Pnd_I_Cntrct_Role_Fltr());                                                                  //Natural: ASSIGN #DATA-IN.#I-CNTRCT-ROLE-FLTR := #MDMA321.#I-CNTRCT-ROLE-FLTR
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_In().getValue("*").setValue(pnd_Data_In.getValue("*"));                                                              //Natural: ASSIGN ##DATA-IN ( * ) := #DATA-IN ( * )
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "=",gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Guid());                                                                                //Natural: WRITE '=' ##MSG-GUID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0010"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0010'
        if (condition(Global.isEscape())) return;
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Data Resp",gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue("*"));                                                     //Natural: WRITE 'Data Resp' ##DATA-RESPONSE ( * )
            if (Global.isEscape()) return;
            if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length().notEquals(pnd_Max_Out)))                                                                   //Natural: IF ##PDA-LENGTH NE #MAX-OUT
            {
                getReports().write(0, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length(),"Different PDA Length",pnd_Max_Out);                                           //Natural: WRITE ##PDA-LENGTH 'Different PDA Length' #MAX-OUT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue("*"));                                                         //Natural: ASSIGN #RETURN ( * ) := ##DATA-RESPONSE ( * )
        pnd_Data_Out.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Out().getValue("*"));                                                            //Natural: ASSIGN #DATA-OUT ( * ) := ##DATA-OUT ( * )
                                                                                                                                                                          //Natural: PERFORM DERIVE-MAINFRAME-FIELDS
        sub_Derive_Mainframe_Fields();
        if (condition(Global.isEscape())) {return;}
        //*  FETCH-MDMP0010
    }
    private void sub_Derive_Mainframe_Fields() throws Exception                                                                                                           //Natural: DERIVE-MAINFRAME-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Data_Out_Pnd_O_Contract_Status_Code.equals(" ") || pnd_Data_Out_Pnd_O_Contract_Status_Code.equals("H") || pnd_Data_Out_Pnd_O_Contract_Status_Code.equals("Y"))) //Natural: IF #DATA-OUT.#O-CONTRACT-STATUS-CODE = ' ' OR = 'H' OR = 'Y'
        {
            pnd_Data_Out_Pnd_O_Contract_Status_Code.setValue(" ");                                                                                                        //Natural: ASSIGN #DATA-OUT.#O-CONTRACT-STATUS-CODE := ' '
            pnd_Data_Out_Pnd_O_Contract_Status_Year.setValue(0);                                                                                                          //Natural: ASSIGN #DATA-OUT.#O-CONTRACT-STATUS-YEAR := 0
        }                                                                                                                                                                 //Natural: END-IF
        pnd_City_State_Zip.reset();                                                                                                                                       //Natural: RESET #CITY-STATE-ZIP
        //*  FOREIGN
        //*  CANADIAN
        short decideConditionsMet534 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #DATA-OUT.#O-ACTV-ADDRESS-TYPE-CODE = 'F'
        if (condition(pnd_Data_Out_Pnd_O_Actv_Address_Type_Code.equals("F")))
        {
            decideConditionsMet534++;
            pnd_Data_Out_Pnd_O_Actv_Address_Zip_Code.setValue("FORGN");                                                                                                   //Natural: ASSIGN #DATA-OUT.#O-ACTV-ADDRESS-ZIP-CODE := 'FORGN'
            pnd_Data_Out_Pnd_O_Actv_Address_Postal_Data.setValue("FORGN");                                                                                                //Natural: ASSIGN #DATA-OUT.#O-ACTV-ADDRESS-POSTAL-DATA := 'FORGN'
        }                                                                                                                                                                 //Natural: WHEN #DATA-OUT.#O-ACTV-ADDRESS-TYPE-CODE = 'C' OR #DATA-OUT.#O-ACTV-ADDRESS-COUNTRY = 'CANADA' OR = 'CA'
        if (condition(pnd_Data_Out_Pnd_O_Actv_Address_Type_Code.equals("C") || pnd_Data_Out_Pnd_O_Actv_Address_Country.equals("CANADA") || pnd_Data_Out_Pnd_O_Actv_Address_Country.equals("CA")))
        {
            decideConditionsMet534++;
            //*  NOT APPLICABLE
            //*  UNKNOWN
            if (condition(pnd_Data_Out_Pnd_O_Actv_Address_City.notEquals("N/A") && pnd_Data_Out_Pnd_O_Actv_Address_St_Prov.notEquals("UN")))                              //Natural: IF #DATA-OUT.#O-ACTV-ADDRESS-CITY NE 'N/A' AND #DATA-OUT.#O-ACTV-ADDRESS-ST-PROV NE 'UN'
            {
                pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Data_Out_Pnd_O_Actv_Address_City, pnd_Data_Out_Pnd_O_Actv_Address_St_Prov, pnd_Data_Out_Pnd_O_Actv_Address_Zip1_7)); //Natural: COMPRESS #DATA-OUT.#O-ACTV-ADDRESS-CITY #DATA-OUT.#O-ACTV-ADDRESS-ST-PROV #DATA-OUT.#O-ACTV-ADDRESS-ZIP1-7 INTO #CITY-STATE-ZIP
                //*  DOMESTIC
            }                                                                                                                                                             //Natural: END-IF
            pnd_Data_Out_Pnd_O_Actv_Address_Zip_Code.setValue("CANAD");                                                                                                   //Natural: ASSIGN #DATA-OUT.#O-ACTV-ADDRESS-ZIP-CODE := 'CANAD'
            pnd_Data_Out_Pnd_O_Actv_Address_Postal_Data.setValue("CANAD");                                                                                                //Natural: ASSIGN #DATA-OUT.#O-ACTV-ADDRESS-POSTAL-DATA := 'CANAD'
            pnd_Data_Out_Pnd_O_Actv_Address_Country.setValue("CANADA");                                                                                                   //Natural: ASSIGN #DATA-OUT.#O-ACTV-ADDRESS-COUNTRY := 'CANADA'
        }                                                                                                                                                                 //Natural: WHEN #DATA-OUT.#O-ACTV-ADDRESS-TYPE-CODE = 'U' OR = 'D' OR #DATA-OUT.#O-ACTV-ADDRESS-COUNTRY = 'USA' OR = 'US'
        if (condition(pnd_Data_Out_Pnd_O_Actv_Address_Type_Code.equals("U") || pnd_Data_Out_Pnd_O_Actv_Address_Type_Code.equals("D") || pnd_Data_Out_Pnd_O_Actv_Address_Country.equals("USA") 
            || pnd_Data_Out_Pnd_O_Actv_Address_Country.equals("US")))
        {
            decideConditionsMet534++;
            //*  NOT APPLICABLE
            //*  UNKNOWN
            if (condition(pnd_Data_Out_Pnd_O_Actv_Address_City.notEquals("N/A") && pnd_Data_Out_Pnd_O_Actv_Address_St_Prov.notEquals("UN")))                              //Natural: IF #DATA-OUT.#O-ACTV-ADDRESS-CITY NE 'N/A' AND #DATA-OUT.#O-ACTV-ADDRESS-ST-PROV NE 'UN'
            {
                pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Data_Out_Pnd_O_Actv_Address_City, pnd_Data_Out_Pnd_O_Actv_Address_St_Prov));                             //Natural: COMPRESS #DATA-OUT.#O-ACTV-ADDRESS-CITY #DATA-OUT.#O-ACTV-ADDRESS-ST-PROV INTO #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Data_Out_Pnd_O_Line_Of_Business.notEquals("D") && pnd_Data_Out_Pnd_O_Actv_Address_Zip1_5.greater("00000") && DbsUtil.maskMatches(pnd_Data_Out_Pnd_O_Actv_Address_Zip1_5, //Natural: IF #DATA-OUT.#O-LINE-OF-BUSINESS NE 'D' AND ( #DATA-OUT.#O-ACTV-ADDRESS-ZIP1-5 > '00000' AND #DATA-OUT.#O-ACTV-ADDRESS-ZIP1-5 = MASK ( 99999 ) )
                "99999")))
            {
                pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_City_State_Zip, pnd_Data_Out_Pnd_O_Actv_Address_Zip1_5));                                                //Natural: COMPRESS #CITY-STATE-ZIP #DATA-OUT.#O-ACTV-ADDRESS-ZIP1-5 INTO #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Data_Out_Pnd_O_Actv_Address_Country.setValue("USA");                                                                                                      //Natural: ASSIGN #DATA-OUT.#O-ACTV-ADDRESS-COUNTRY := 'USA'
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet534 > 0))
        {
            short decideConditionsMet559 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #DATA-OUT.#O-ACTV-ADDRESS-LINE-1 = ' '
            if (condition(pnd_Data_Out_Pnd_O_Actv_Address_Line_1.equals(" ")))
            {
                decideConditionsMet559++;
                pnd_Data_Out_Pnd_O_Actv_Address_Line_1.setValue(pnd_City_State_Zip);                                                                                      //Natural: ASSIGN #DATA-OUT.#O-ACTV-ADDRESS-LINE-1 := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-ACTV-ADDRESS-LINE-2 = ' '
            else if (condition(pnd_Data_Out_Pnd_O_Actv_Address_Line_2.equals(" ")))
            {
                decideConditionsMet559++;
                pnd_Data_Out_Pnd_O_Actv_Address_Line_2.setValue(pnd_City_State_Zip);                                                                                      //Natural: ASSIGN #DATA-OUT.#O-ACTV-ADDRESS-LINE-2 := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-ACTV-ADDRESS-LINE-3 = ' '
            else if (condition(pnd_Data_Out_Pnd_O_Actv_Address_Line_3.equals(" ")))
            {
                decideConditionsMet559++;
                pnd_Data_Out_Pnd_O_Actv_Address_Line_3.setValue(pnd_City_State_Zip);                                                                                      //Natural: ASSIGN #DATA-OUT.#O-ACTV-ADDRESS-LINE-3 := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-ACTV-ADDRESS-LINE-4 = ' '
            else if (condition(pnd_Data_Out_Pnd_O_Actv_Address_Line_4.equals(" ")))
            {
                decideConditionsMet559++;
                pnd_Data_Out_Pnd_O_Actv_Address_Line_4.setValue(pnd_City_State_Zip);                                                                                      //Natural: ASSIGN #DATA-OUT.#O-ACTV-ADDRESS-LINE-4 := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet534 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Data_Out_Pnd_O_Temp_Address_Line_1.notEquals(" ")))                                                                                             //Natural: IF #DATA-OUT.#O-TEMP-ADDRESS-LINE-1 NE ' '
        {
            pnd_City_State_Zip.reset();                                                                                                                                   //Natural: RESET #CITY-STATE-ZIP
            //*  FOREIGN
            //*  CANADIAN
            short decideConditionsMet579 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #DATA-OUT.#O-TEMP-ADDRESS-TYPE-CODE = 'F'
            if (condition(pnd_Data_Out_Pnd_O_Temp_Address_Type_Code.equals("F")))
            {
                decideConditionsMet579++;
                pnd_Data_Out_Pnd_O_Temp_Address_Zip_Code.setValue("FORGN");                                                                                               //Natural: ASSIGN #DATA-OUT.#O-TEMP-ADDRESS-ZIP-CODE := 'FORGN'
                pnd_Data_Out_Pnd_O_Temp_Address_Postal_Data.setValue("FORGN");                                                                                            //Natural: ASSIGN #DATA-OUT.#O-TEMP-ADDRESS-POSTAL-DATA := 'FORGN'
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-TEMP-ADDRESS-TYPE-CODE = 'C' OR #DATA-OUT.#O-TEMP-ADDRESS-COUNTRY = 'CANADA' OR = 'CA'
            if (condition(pnd_Data_Out_Pnd_O_Temp_Address_Type_Code.equals("C") || pnd_Data_Out_Pnd_O_Temp_Address_Country.equals("CANADA") || pnd_Data_Out_Pnd_O_Temp_Address_Country.equals("CA")))
            {
                decideConditionsMet579++;
                //*  NOT APPLICABLE
                //*  UNKNOWN
                if (condition(pnd_Data_Out_Pnd_O_Temp_Address_City.notEquals("N/A") && pnd_Data_Out_Pnd_O_Temp_Address_St_Prov.notEquals("UN")))                          //Natural: IF #DATA-OUT.#O-TEMP-ADDRESS-CITY NE 'N/A' AND #DATA-OUT.#O-TEMP-ADDRESS-ST-PROV NE 'UN'
                {
                    pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Data_Out_Pnd_O_Temp_Address_City, pnd_Data_Out_Pnd_O_Temp_Address_St_Prov, pnd_Data_Out_Pnd_O_Temp_Address_Zip1_7)); //Natural: COMPRESS #DATA-OUT.#O-TEMP-ADDRESS-CITY #DATA-OUT.#O-TEMP-ADDRESS-ST-PROV #DATA-OUT.#O-TEMP-ADDRESS-ZIP1-7 INTO #CITY-STATE-ZIP
                    //*  DOMESTIC
                }                                                                                                                                                         //Natural: END-IF
                pnd_Data_Out_Pnd_O_Temp_Address_Zip_Code.setValue("CANAD");                                                                                               //Natural: ASSIGN #DATA-OUT.#O-TEMP-ADDRESS-ZIP-CODE := 'CANAD'
                pnd_Data_Out_Pnd_O_Temp_Address_Postal_Data.setValue("CANAD");                                                                                            //Natural: ASSIGN #DATA-OUT.#O-TEMP-ADDRESS-POSTAL-DATA := 'CANAD'
                pnd_Data_Out_Pnd_O_Temp_Address_Country.setValue("CANADA");                                                                                               //Natural: ASSIGN #DATA-OUT.#O-TEMP-ADDRESS-COUNTRY := 'CANADA'
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-TEMP-ADDRESS-TYPE-CODE = 'U' OR = 'D' OR #DATA-OUT.#O-TEMP-ADDRESS-COUNTRY = 'USA' OR = 'US'
            if (condition(pnd_Data_Out_Pnd_O_Temp_Address_Type_Code.equals("U") || pnd_Data_Out_Pnd_O_Temp_Address_Type_Code.equals("D") || pnd_Data_Out_Pnd_O_Temp_Address_Country.equals("USA") 
                || pnd_Data_Out_Pnd_O_Temp_Address_Country.equals("US")))
            {
                decideConditionsMet579++;
                //*  NOT APPLICABLE
                //*  UNKNOWN
                if (condition(pnd_Data_Out_Pnd_O_Temp_Address_City.notEquals("N/A") && pnd_Data_Out_Pnd_O_Temp_Address_St_Prov.notEquals("UN")))                          //Natural: IF #DATA-OUT.#O-TEMP-ADDRESS-CITY NE 'N/A' AND #DATA-OUT.#O-TEMP-ADDRESS-ST-PROV NE 'UN'
                {
                    pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Data_Out_Pnd_O_Temp_Address_City, pnd_Data_Out_Pnd_O_Temp_Address_St_Prov));                         //Natural: COMPRESS #DATA-OUT.#O-TEMP-ADDRESS-CITY #DATA-OUT.#O-TEMP-ADDRESS-ST-PROV INTO #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Data_Out_Pnd_O_Line_Of_Business.notEquals("D") && pnd_Data_Out_Pnd_O_Temp_Address_Zip1_5.greater("00000") && DbsUtil.maskMatches(pnd_Data_Out_Pnd_O_Temp_Address_Zip1_5, //Natural: IF #DATA-OUT.#O-LINE-OF-BUSINESS NE 'D' AND ( #DATA-OUT.#O-TEMP-ADDRESS-ZIP1-5 > '00000' AND #DATA-OUT.#O-TEMP-ADDRESS-ZIP1-5 = MASK ( 99999 ) )
                    "99999")))
                {
                    pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_City_State_Zip, pnd_Data_Out_Pnd_O_Temp_Address_Zip1_5));                                            //Natural: COMPRESS #CITY-STATE-ZIP #DATA-OUT.#O-TEMP-ADDRESS-ZIP1-5 INTO #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Data_Out_Pnd_O_Temp_Address_Country.setValue("USA");                                                                                                  //Natural: ASSIGN #DATA-OUT.#O-TEMP-ADDRESS-COUNTRY := 'USA'
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet579 > 0))
            {
                short decideConditionsMet604 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #DATA-OUT.#O-TEMP-ADDRESS-LINE-1 = ' '
                if (condition(pnd_Data_Out_Pnd_O_Temp_Address_Line_1.equals(" ")))
                {
                    decideConditionsMet604++;
                    pnd_Data_Out_Pnd_O_Temp_Address_Line_1.setValue(pnd_City_State_Zip);                                                                                  //Natural: ASSIGN #DATA-OUT.#O-TEMP-ADDRESS-LINE-1 := #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: WHEN #DATA-OUT.#O-TEMP-ADDRESS-LINE-2 = ' '
                else if (condition(pnd_Data_Out_Pnd_O_Temp_Address_Line_2.equals(" ")))
                {
                    decideConditionsMet604++;
                    pnd_Data_Out_Pnd_O_Temp_Address_Line_2.setValue(pnd_City_State_Zip);                                                                                  //Natural: ASSIGN #DATA-OUT.#O-TEMP-ADDRESS-LINE-2 := #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: WHEN #DATA-OUT.#O-TEMP-ADDRESS-LINE-3 = ' '
                else if (condition(pnd_Data_Out_Pnd_O_Temp_Address_Line_3.equals(" ")))
                {
                    decideConditionsMet604++;
                    pnd_Data_Out_Pnd_O_Temp_Address_Line_3.setValue(pnd_City_State_Zip);                                                                                  //Natural: ASSIGN #DATA-OUT.#O-TEMP-ADDRESS-LINE-3 := #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: WHEN #DATA-OUT.#O-TEMP-ADDRESS-LINE-4 = ' '
                else if (condition(pnd_Data_Out_Pnd_O_Temp_Address_Line_4.equals(" ")))
                {
                    decideConditionsMet604++;
                    pnd_Data_Out_Pnd_O_Temp_Address_Line_4.setValue(pnd_City_State_Zip);                                                                                  //Natural: ASSIGN #DATA-OUT.#O-TEMP-ADDRESS-LINE-4 := #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet579 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_Data_Out_Pnd_O_Temp_Address_End_Mmdd.greater(getZero()) && pnd_Data_Out_Pnd_O_Temp_Address_End_Mmdd.less(pnd_Data_Out_Pnd_O_Temp_Address_Start_Mmdd)  //Natural: IF #DATA-OUT.#O-TEMP-ADDRESS-END-MMDD > 0 AND #DATA-OUT.#O-TEMP-ADDRESS-END-MMDD < #DATA-OUT.#O-TEMP-ADDRESS-START-MMDD AND #DATA-OUT.#O-TEMP-ADDRESS-END-YYYY = #DATA-OUT.#O-TEMP-ADDRESS-START-YYYY
                && pnd_Data_Out_Pnd_O_Temp_Address_End_Yyyy.equals(pnd_Data_Out_Pnd_O_Temp_Address_Start_Yyyy)))
            {
                //*  ADD 1 YEAR TO END DATE
                pnd_Data_Out_Pnd_O_Temp_Address_End_Yyyy.nadd(1);                                                                                                         //Natural: ADD 1 TO #O-TEMP-ADDRESS-END-YYYY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Data_Out_Pnd_O_Futr_Address_Line_1.notEquals(" ")))                                                                                             //Natural: IF #DATA-OUT.#O-FUTR-ADDRESS-LINE-1 NE ' '
        {
            pnd_City_State_Zip.reset();                                                                                                                                   //Natural: RESET #CITY-STATE-ZIP
            //*  FOREIGN
            //*  CANADIAN
            short decideConditionsMet630 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #DATA-OUT.#O-FUTR-ADDRESS-TYPE-CODE = 'F'
            if (condition(pnd_Data_Out_Pnd_O_Futr_Address_Type_Code.equals("F")))
            {
                decideConditionsMet630++;
                pnd_Data_Out_Pnd_O_Futr_Address_Zip_Code.setValue("FORGN");                                                                                               //Natural: ASSIGN #DATA-OUT.#O-FUTR-ADDRESS-ZIP-CODE := 'FORGN'
                pnd_Data_Out_Pnd_O_Futr_Address_Postal_Data.setValue("FORGN");                                                                                            //Natural: ASSIGN #DATA-OUT.#O-FUTR-ADDRESS-POSTAL-DATA := 'FORGN'
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-FUTR-ADDRESS-TYPE-CODE = 'C' OR #DATA-OUT.#O-FUTR-ADDRESS-COUNTRY = 'CANADA' OR = 'CA'
            if (condition(pnd_Data_Out_Pnd_O_Futr_Address_Type_Code.equals("C") || pnd_Data_Out_Pnd_O_Futr_Address_Country.equals("CANADA") || pnd_Data_Out_Pnd_O_Futr_Address_Country.equals("CA")))
            {
                decideConditionsMet630++;
                //*  NOT APPLICABLE
                //*  UNKNOWN
                if (condition(pnd_Data_Out_Pnd_O_Futr_Address_City.notEquals("N/A") && pnd_Data_Out_Pnd_O_Futr_Address_St_Prov.notEquals("UN")))                          //Natural: IF #DATA-OUT.#O-FUTR-ADDRESS-CITY NE 'N/A' AND #DATA-OUT.#O-FUTR-ADDRESS-ST-PROV NE 'UN'
                {
                    pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Data_Out_Pnd_O_Futr_Address_City, pnd_Data_Out_Pnd_O_Futr_Address_St_Prov, pnd_Data_Out_Pnd_O_Futr_Address_Zip1_7)); //Natural: COMPRESS #DATA-OUT.#O-FUTR-ADDRESS-CITY #DATA-OUT.#O-FUTR-ADDRESS-ST-PROV #DATA-OUT.#O-FUTR-ADDRESS-ZIP1-7 INTO #CITY-STATE-ZIP
                    //*  DOMESTIC
                }                                                                                                                                                         //Natural: END-IF
                pnd_Data_Out_Pnd_O_Futr_Address_Zip_Code.setValue("CANAD");                                                                                               //Natural: ASSIGN #DATA-OUT.#O-FUTR-ADDRESS-ZIP-CODE := 'CANAD'
                pnd_Data_Out_Pnd_O_Futr_Address_Postal_Data.setValue("CANAD");                                                                                            //Natural: ASSIGN #DATA-OUT.#O-FUTR-ADDRESS-POSTAL-DATA := 'CANAD'
                pnd_Data_Out_Pnd_O_Futr_Address_Country.setValue("CANADA");                                                                                               //Natural: ASSIGN #DATA-OUT.#O-FUTR-ADDRESS-COUNTRY := 'CANADA'
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-FUTR-ADDRESS-TYPE-CODE = 'U' OR = 'D' OR #DATA-OUT.#O-FUTR-ADDRESS-COUNTRY = 'USA' OR = 'US'
            if (condition(pnd_Data_Out_Pnd_O_Futr_Address_Type_Code.equals("U") || pnd_Data_Out_Pnd_O_Futr_Address_Type_Code.equals("D") || pnd_Data_Out_Pnd_O_Futr_Address_Country.equals("USA") 
                || pnd_Data_Out_Pnd_O_Futr_Address_Country.equals("US")))
            {
                decideConditionsMet630++;
                //*  NOT APPLICABLE
                //*  UNKNOWN
                if (condition(pnd_Data_Out_Pnd_O_Futr_Address_City.notEquals("N/A") && pnd_Data_Out_Pnd_O_Futr_Address_St_Prov.notEquals("UN")))                          //Natural: IF #DATA-OUT.#O-FUTR-ADDRESS-CITY NE 'N/A' AND #DATA-OUT.#O-FUTR-ADDRESS-ST-PROV NE 'UN'
                {
                    pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Data_Out_Pnd_O_Futr_Address_City, pnd_Data_Out_Pnd_O_Futr_Address_St_Prov));                         //Natural: COMPRESS #DATA-OUT.#O-FUTR-ADDRESS-CITY #DATA-OUT.#O-FUTR-ADDRESS-ST-PROV INTO #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Data_Out_Pnd_O_Line_Of_Business.notEquals("D") && pnd_Data_Out_Pnd_O_Futr_Address_Zip1_5.greater("00000") && DbsUtil.maskMatches(pnd_Data_Out_Pnd_O_Futr_Address_Zip1_5, //Natural: IF #DATA-OUT.#O-LINE-OF-BUSINESS NE 'D' AND ( #DATA-OUT.#O-FUTR-ADDRESS-ZIP1-5 > '00000' AND #DATA-OUT.#O-FUTR-ADDRESS-ZIP1-5 = MASK ( 99999 ) )
                    "99999")))
                {
                    pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_City_State_Zip, pnd_Data_Out_Pnd_O_Futr_Address_Zip1_5));                                            //Natural: COMPRESS #CITY-STATE-ZIP #DATA-OUT.#O-FUTR-ADDRESS-ZIP1-5 INTO #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Data_Out_Pnd_O_Futr_Address_Country.setValue("USA");                                                                                                  //Natural: ASSIGN #DATA-OUT.#O-FUTR-ADDRESS-COUNTRY := 'USA'
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet630 > 0))
            {
                short decideConditionsMet655 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #DATA-OUT.#O-FUTR-ADDRESS-LINE-1 = ' '
                if (condition(pnd_Data_Out_Pnd_O_Futr_Address_Line_1.equals(" ")))
                {
                    decideConditionsMet655++;
                    pnd_Data_Out_Pnd_O_Futr_Address_Line_1.setValue(pnd_City_State_Zip);                                                                                  //Natural: ASSIGN #DATA-OUT.#O-FUTR-ADDRESS-LINE-1 := #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: WHEN #DATA-OUT.#O-FUTR-ADDRESS-LINE-2 = ' '
                else if (condition(pnd_Data_Out_Pnd_O_Futr_Address_Line_2.equals(" ")))
                {
                    decideConditionsMet655++;
                    pnd_Data_Out_Pnd_O_Futr_Address_Line_2.setValue(pnd_City_State_Zip);                                                                                  //Natural: ASSIGN #DATA-OUT.#O-FUTR-ADDRESS-LINE-2 := #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: WHEN #DATA-OUT.#O-FUTR-ADDRESS-LINE-3 = ' '
                else if (condition(pnd_Data_Out_Pnd_O_Futr_Address_Line_3.equals(" ")))
                {
                    decideConditionsMet655++;
                    pnd_Data_Out_Pnd_O_Futr_Address_Line_3.setValue(pnd_City_State_Zip);                                                                                  //Natural: ASSIGN #DATA-OUT.#O-FUTR-ADDRESS-LINE-3 := #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: WHEN #DATA-OUT.#O-FUTR-ADDRESS-LINE-4 = ' '
                else if (condition(pnd_Data_Out_Pnd_O_Futr_Address_Line_4.equals(" ")))
                {
                    decideConditionsMet655++;
                    pnd_Data_Out_Pnd_O_Futr_Address_Line_4.setValue(pnd_City_State_Zip);                                                                                  //Natural: ASSIGN #DATA-OUT.#O-FUTR-ADDRESS-LINE-4 := #CITY-STATE-ZIP
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet630 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Data_Out_Pnd_O_Pin.setValue(0);                                                                                                                               //Natural: ASSIGN #DATA-OUT.#O-PIN := 0
        if (condition(pnd_Data_Out_Pnd_O_Pin_Blanks.equals(" ") && pnd_Data_Out_Pnd_O_Pin_A12.notEquals(" ")))                                                            //Natural: IF #DATA-OUT.#O-PIN-BLANKS = ' ' AND #O-PIN-A12 NE ' '
        {
            pnd_Data_Out_Pnd_O_Pin_N12.setValue(pnd_Data_Out_Pnd_O_Pin_N7);                                                                                               //Natural: ASSIGN #DATA-OUT.#O-PIN-N12 := #DATA-OUT.#O-PIN-N7
        }                                                                                                                                                                 //Natural: END-IF
        //*  DERIVE-MAINFRAME-FIELDS
    }

    //
}
