/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:28:51 AM
**        * FROM NATURAL SUBPROGRAM : Mdmn191
************************************************************
**        * FILE NAME            : Mdmn191.java
**        * CLASS NAME           : Mdmn191
**        * INSTANCE NAME        : Mdmn191
************************************************************
************************************************************************
* SUBPROGRAM : MDMN191
* *********************************************************************
* DATE       : JUNE 23, 2017
*
* FUNCTION   : CLONE OF NASN191 FOR 12-BYTE PIN PROCESSING. WILL BE
*              CALLED FROM NASB* BATCH PROGRAMS.
*
* REVISIONS
* DATE        BY        DESCRIPTION
*
************************************************************************

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mdmn191 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaMdma190 pdaMdma190;
    private PdaNasa191 pdaNasa191;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;
    private PdaErla1000 pdaErla1000;
    private PdaCwfa8010 pdaCwfa8010;
    private PdaCwfa6510 pdaCwfa6510;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_Cdms_In_Routing;
    private DbsField pnd_Route_To_Next_Unit;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaErla1000 = new PdaErla1000(localVariables);
        pdaCwfa8010 = new PdaCwfa8010(localVariables);
        pdaCwfa6510 = new PdaCwfa6510(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaMdma190 = new PdaMdma190(parameters);
        pdaNasa191 = new PdaNasa191(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Cdms_In_Routing = localVariables.newFieldInRecord("pnd_Cdms_In_Routing", "#CDMS-IN-ROUTING", FieldType.STRING, 1);
        pnd_Route_To_Next_Unit = localVariables.newFieldInRecord("pnd_Route_To_Next_Unit", "#ROUTE-TO-NEXT-UNIT", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Mdmn191() throws Exception
    {
        super("Mdmn191");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        if (condition(pdaMdma190.getMdma190_Pnd_Gda_Slo_Exists().equals("Y")))                                                                                            //Natural: IF MDMA190.#GDA-SLO-EXISTS = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-CWF-RTN
            sub_Update_Cwf_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM STORE-CWF-RTN
            sub_Store_Cwf_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaMdma190.getMdma190_Pnd_Error_Message().notEquals(" ")))                                                                                          //Natural: IF MDMA190.#ERROR-MESSAGE NE ' '
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            pdaErla1000.getErla1000_Err_Pgm_Name().setValue(Global.getPROGRAM());                                                                                         //Natural: ASSIGN ERLA1000.ERR-PGM-NAME := *PROGRAM
            pdaErla1000.getErla1000_Err_Pgm_Level().setValue(Global.getLEVEL());                                                                                          //Natural: ASSIGN ERLA1000.ERR-PGM-LEVEL := *LEVEL
            pdaErla1000.getErla1000_Err_Nbr().setValue(Global.getERROR_NR());                                                                                             //Natural: ASSIGN ERLA1000.ERR-NBR := *ERROR-NR
            pdaErla1000.getErla1000_Err_Line_Nbr().setValue(Global.getERROR_LINE());                                                                                      //Natural: ASSIGN ERLA1000.ERR-LINE-NBR := *ERROR-LINE
            pdaErla1000.getErla1000_Err_Status_Cde().setValue("U");                                                                                                       //Natural: ASSIGN ERLA1000.ERR-STATUS-CDE := 'U'
            pdaErla1000.getErla1000_Err_Type_Cde().setValue("N");                                                                                                         //Natural: ASSIGN ERLA1000.ERR-TYPE-CDE := 'N'
            //*  ERLA1000.ERR-NOTES      := MDMN190.#ERROR-MESSAGE
            pdaErla1000.getErla1000_Err_Notes().setValue(DbsUtil.compress("PIN", pdaMdma190.getMdma190_Pnd_Wpid_Pin(), pdaMdma190.getMdma190_Pnd_Error_Message()));       //Natural: COMPRESS 'PIN' MDMA190.#WPID-PIN MDMA190.#ERROR-MESSAGE INTO ERLA1000.ERR-NOTES
            DbsUtil.callnat(Erln1000.class , getCurrentProcessState(), pdaErla1000.getErla1000());                                                                        //Natural: CALLNAT 'ERLN1000' ERLA1000
            if (condition(Global.isEscape())) return;
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaMdma190.getMdma190_Pnd_Et_Ind().equals("Y")))                                                                                                //Natural: IF MDMA190.#ET-IND = 'Y'
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //* *                    UPDATE CWF RTN                                  **
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CWF-RTN
        //* ***********************************************************************
        //* *                    STORE CWF RTN                                   **
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-CWF-RTN
    }
    private void sub_Update_Cwf_Rtn() throws Exception                                                                                                                    //Natural: UPDATE-CWF-RTN
    {
        if (BLNatReinput.isReinput()) return;

        pdaCwfa8010.getCwfa8010_Action_Cde().setValue("MO");                                                                                                              //Natural: MOVE 'MO' TO CWFA8010.ACTION-CDE
        pdaCwfa8010.getCwfa8010_System().setValue(pdaMdma190.getMdma190_Pnd_Wpid_System());                                                                               //Natural: MOVE MDMA190.#WPID-SYSTEM TO CWFA8010.SYSTEM
        pdaCwfa8010.getCwfa8010_Rqst_Log_Dte_Tme().setValue(pdaMdma190.getMdma190_Pnd_Slo_Rqst_Log_Dte_Tme());                                                            //Natural: MOVE MDMA190.#SLO-RQST-LOG-DTE-TME TO CWFA8010.RQST-LOG-DTE-TME
        pdaCwfa8010.getCwfa8010_Pin_Nbr().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Pin());                                                                                 //Natural: MOVE MDMA190.#WPID-PIN TO CWFA8010.PIN-NBR
        pdaCwfa8010.getCwfa8010_Oprtr_Cde().setValue(pdaMdma190.getMdma190_Pnd_Gda_User_Id());                                                                            //Natural: MOVE MDMA190.#GDA-USER-ID TO CWFA8010.OPRTR-CDE
        pdaCwfa8010.getCwfa8010_Wpid_Vldte_Ind().setValue("Y");                                                                                                           //Natural: MOVE 'Y' TO CWFA8010.WPID-VLDTE-IND
        pdaCwfa8010.getCwfa8010_Oprtr_Unit_Cde().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Unit());                                                                         //Natural: MOVE MDMA190.#WPID-UNIT TO CWFA8010.OPRTR-UNIT-CDE
        pdaCwfa8010.getCwfa8010_Spcl_Hndlng_Msg().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Special_Handling_Text());                                                       //Natural: MOVE MDMA190.#WPID-SPECIAL-HANDLING-TEXT TO CWFA8010.SPCL-HNDLNG-MSG
        //*  IF NEXT UNIT IS FILLED IN, THEN THIS WORK REQUEST REQUIRES
        //*  VERIFICATION AND MUST BE ROUTED TO CDCOR. IF NOT, THEN WE MUST REVIEW
        //*  THE STANDARD ROUTING TO DECIDE IF THIS NEEDS TO BE ROUTED TO ANOTHER
        //*  UNIT, OR IT CAN BE CORPORATLY CLOSED
        //*  IF THE REVIEW OF THE STANDARD ROUTING INDICATES THE CDMS IS NOT PART
        //*  OF THIS WORK NORMALLY, THE PROGRAM CANNOT DETERMINE WHETHER THE WORK
        //*  REQUEST SHOULD BE CLOSED, OR WHERE IT SHOULD BE ROUTED TO. THE
        //*  WORK REQUEST WILL BE UPDATED WITH A STATUS 7600 - IN UNIT BUT
        //*  MUST BE MANUALLY ROUTED.
        if (condition(pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit().equals("     ")))                                                                                        //Natural: IF MDMA190.#WPID-NEXT-UNIT = '     '
        {
            pdaCwfa6510.getCwfa6510_Pnd_Wpid().setValue(pdaMdma190.getMdma190_Pnd_Wpid());                                                                                //Natural: MOVE MDMA190.#WPID TO CWFA6510.#WPID
            DbsUtil.callnat(Cwfn6510.class , getCurrentProcessState(), pdaCwfa6510.getCwfa6510(), pdaCwfpda_M.getMsg_Info_Sub());                                         //Natural: CALLNAT 'CWFN6510' CWFA6510 MSG-INFO-SUB
            if (condition(Global.isEscape())) return;
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO CWFA6510.#SUM-OF-UNITS-IN-ROUTING
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaCwfa6510.getCwfa6510_Pnd_Sum_Of_Units_In_Routing())); pnd_I.nadd(1))
            {
                if (condition(pdaCwfa6510.getCwfa6510_Pnd_Units_In_Routing().getValue(pnd_I).equals("CDMSA") || pdaCwfa6510.getCwfa6510_Pnd_Units_In_Routing().getValue(pnd_I).equals("CDCOR")  //Natural: IF CWFA6510.#UNITS-IN-ROUTING ( #I ) = 'CDMSA' OR = 'CDCOR' OR = 'CDMSR'
                    || pdaCwfa6510.getCwfa6510_Pnd_Units_In_Routing().getValue(pnd_I).equals("CDMSR")))
                {
                    pnd_Cdms_In_Routing.setValue("Y");                                                                                                                    //Natural: MOVE 'Y' TO #CDMS-IN-ROUTING
                    //*  MEANS THAT THIS IS THE LAST UNIT IN ROUTING
                    if (condition(pnd_I.equals(20)))                                                                                                                      //Natural: IF #I = 20
                    {
                        pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit().reset();                                                                                               //Natural: RESET MDMA190.#WPID-NEXT-UNIT
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Route_To_Next_Unit.setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #ROUTE-TO-NEXT-UNIT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Route_To_Next_Unit.equals("Y")))                                                                                                    //Natural: IF #ROUTE-TO-NEXT-UNIT = 'Y'
                    {
                        pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit().setValue(pdaCwfa6510.getCwfa6510_Pnd_Units_In_Routing().getValue(pnd_I));                              //Natural: MOVE CWFA6510.#UNITS-IN-ROUTING ( #I ) TO MDMA190.#WPID-NEXT-UNIT
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(pnd_Cdms_In_Routing.equals(" ")))                                                                                                               //Natural: IF #CDMS-IN-ROUTING = ' '
            {
                pdaMdma190.getMdma190_Pnd_Wpid_Status().setValue("7600");                                                                                                 //Natural: MOVE '7600' TO MDMA190.#WPID-STATUS
                pdaMdma190.getMdma190_Pnd_Wpid_Priority_Cde().setValue("9");                                                                                              //Natural: MOVE '9' TO MDMA190.#WPID-PRIORITY-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfa8010.getCwfa8010_Unit_Cde().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit());                                                                          //Natural: MOVE MDMA190.#WPID-NEXT-UNIT TO CWFA8010.UNIT-CDE
        pdaCwfa8010.getCwfa8010_Status_Cde().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Status());                                                                           //Natural: MOVE MDMA190.#WPID-STATUS TO CWFA8010.STATUS-CDE
        pdaCwfa8010.getCwfa8010_Due_Dte_Prty_Cde().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Priority_Cde());                                                               //Natural: MOVE MDMA190.#WPID-PRIORITY-CDE TO CWFA8010.DUE-DTE-PRTY-CDE
        //*  IF THERE IS A NEXT UNIT TO BE ROUTED TO, THEN THE STATUS MUST BE
        //*  WITHIN 7000 TO 7499. IF IT ISN'T, THEN 7100 WILL BE USED. IF
        //*  THERE IS NOT A NEXT UNIT, AND THE STATUS WAS NOT SET BY THE CALLING
        //*  PROGRAM, THEN THE WORK IS COMPLETE. IF THE WORK WAS COMPLETED BY THE
        //*  VERIFICATION UNIT, THEN IT IS CLOSED WITH AN 8001, ELSE CLOSE IT
        //*  WITH AN 8000.
        //*  IF THEN STATUS IS 7990, THIS MEANS THAT A NEXT UNIT WAS NOT
        //*  SENT BY THE CALLING PROGRAM, AND THAT A SEARCH OF THE STANDARD ROUTING
        //*  COULD NOT DETERMINE WHAT SHOULD BE DONE NEXT WITH THE WORK REQUEST
        if (condition(pdaCwfa8010.getCwfa8010_Unit_Cde().equals(" ")))                                                                                                    //Natural: IF CWFA8010.UNIT-CDE = ' '
        {
            if (condition(pdaMdma190.getMdma190_Pnd_Wpid_Status().equals(" ")))                                                                                           //Natural: IF MDMA190.#WPID-STATUS = ' '
            {
                //*  CLOSE WITH VERIFY COMPLETE
                if (condition(pdaCwfa8010.getCwfa8010_Oprtr_Cde().equals("CDCOR")))                                                                                       //Natural: IF CWFA8010.OPRTR-CDE = 'CDCOR'
                {
                    pdaCwfa8010.getCwfa8010_Status_Cde().setValue("8001");                                                                                                //Natural: MOVE '8001' TO CWFA8010.STATUS-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaCwfa8010.getCwfa8010_Status_Cde().setValue("8000");                                                                                                //Natural: MOVE '8000' TO CWFA8010.STATUS-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaMdma190.getMdma190_Pnd_Wpid_Status().greaterOrEqual("7000") && pdaMdma190.getMdma190_Pnd_Wpid_Status().lessOrEqual("7499")))                 //Natural: IF MDMA190.#WPID-STATUS = '7000' THRU '7499'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCwfa8010.getCwfa8010_Status_Cde().setValue("7100");                                                                                                    //Natural: MOVE '7100' TO CWFA8010.STATUS-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfa8010.getCwfa8010_Status_Cde().greaterOrEqual("8000") && pdaCwfa8010.getCwfa8010_Status_Cde().lessOrEqual("8799")))                           //Natural: IF CWFA8010.STATUS-CDE = '8000' THRU '8799'
        {
            pdaCwfa8010.getCwfa8010_Crprte_Status_Ind().setValue("9");                                                                                                    //Natural: MOVE '9' TO CWFA8010.CRPRTE-STATUS-IND
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Cwfn8010.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaCwfpda_D.getDialog_Info_Sub(),            //Natural: CALLNAT 'CWFN8010' MSG-INFO-SUB PASS-SUB DIALOG-INFO-SUB CWFA8010
            pdaCwfa8010.getCwfa8010());
        if (condition(Global.isEscape())) return;
        //*   MSG-INFO-SUB.##MSG-NR NE 0
        //*    OR MSG-INFO-SUB.##RETURN-CODE = 'E'
        pdaMdma190.getMdma190_Pnd_Error_Message().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                    //Natural: MOVE MSG-INFO-SUB.##MSG TO MDMA190.#ERROR-MESSAGE
        if (condition(pdaMdma190.getMdma190_Pnd_Error_Message().equals(" ")))                                                                                             //Natural: IF MDMA190.#ERROR-MESSAGE = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfa8010.getCwfa8010_Status_Cde().setValue("7990");                                                                                                        //Natural: MOVE '7990' TO CWFA8010.STATUS-CDE
            DbsUtil.callnat(Cwfn8010.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaCwfpda_D.getDialog_Info_Sub(),        //Natural: CALLNAT 'CWFN8010' MSG-INFO-SUB PASS-SUB DIALOG-INFO-SUB CWFA8010
                pdaCwfa8010.getCwfa8010());
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Store_Cwf_Rtn() throws Exception                                                                                                                     //Natural: STORE-CWF-RTN
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa9120.getEfsa9120_Action().setValue("AR");                                                                                                                  //Natural: MOVE 'AR' TO EFSA9120.ACTION
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: MOVE 'P' TO EFSA9120.CABINET-PREFIX
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Pin());                                                                                 //Natural: MOVE MDMA190.#WPID-PIN TO EFSA9120.PIN-NBR
        pdaEfsa9120.getEfsa9120_Tiaa_Rcvd_Dte().getValue(1).setValue(Global.getDATN());                                                                                   //Natural: MOVE *DATN TO EFSA9120.TIAA-RCVD-DTE ( 1 )
        pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue(pdaMdma190.getMdma190_Pnd_Wpid());                                                                            //Natural: MOVE MDMA190.#WPID TO EFSA9120.WPID ( 1 )
        pdaEfsa9120.getEfsa9120_Unit_Cde().getValue(1).setValue(pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit());                                                              //Natural: MOVE MDMA190.#WPID-NEXT-UNIT TO EFSA9120.UNIT-CDE ( 1 )
        pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).setValue(pdaMdma190.getMdma190_Pnd_Wpid_Status());                                                               //Natural: MOVE MDMA190.#WPID-STATUS TO EFSA9120.STATUS-CDE ( 1 )
        pdaEfsa9120.getEfsa9120_Multi_Rqst_Ind().getValue(1).setValue("0");                                                                                               //Natural: MOVE '0' TO EFSA9120.MULTI-RQST-IND ( 1 )
        pdaEfsa9120.getEfsa9120_Gen_Fldr_Ind().getValue(1).setValue("N");                                                                                                 //Natural: MOVE 'N' TO EFSA9120.GEN-FLDR-IND ( 1 )
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue(pdaMdma190.getMdma190_Pnd_Gda_User_Id());                                                                    //Natural: MOVE MDMA190.#GDA-USER-ID TO EFSA9120.RQST-ENTRY-OP-CDE
        pdaEfsa9120.getEfsa9120_System().setValue(pdaMdma190.getMdma190_Pnd_Wpid_System());                                                                               //Natural: MOVE MDMA190.#WPID-SYSTEM TO EFSA9120.SYSTEM
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("I");                                                                                                          //Natural: MOVE 'I' TO EFSA9120.RQST-ORIGIN-CDE
        pdaEfsa9120.getEfsa9120_Spcl_Handling_Txt().getValue(1).setValue(pdaMdma190.getMdma190_Pnd_Wpid_Special_Handling_Text());                                         //Natural: MOVE MDMA190.#WPID-SPECIAL-HANDLING-TEXT TO EFSA9120.SPCL-HANDLING-TXT ( 1 )
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Unit());                                                                   //Natural: MOVE MDMA190.#WPID-UNIT TO EFSA9120.RQST-ORIGIN-UNIT-CDE
        pdaEfsa9120.getEfsa9120_Wpid_Validate_Ind().getValue(1).setValue("Y");                                                                                            //Natural: MOVE 'Y' TO EFSA9120.WPID-VALIDATE-IND ( 1 )
        pdaEfsa9120.getEfsa9120_Wpid_Validate_Ind().getValue(1).setValue("Y");                                                                                            //Natural: MOVE 'Y' TO EFSA9120.WPID-VALIDATE-IND ( 1 )
        if (condition(pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).greaterOrEqual("8000") && pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).lessOrEqual("8799")))   //Natural: IF EFSA9120.STATUS-CDE ( 1 ) = '8000' THRU '8799'
        {
            pdaEfsa9120.getEfsa9120_Corp_Status().getValue(1).setValue("9");                                                                                              //Natural: MOVE '9' TO EFSA9120.CORP-STATUS ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  FE0701 START
        //*  RKG0702
        //*  FE1201
        //*  RKG0103
        //*  MIKE WEBER
        //*  GOPAZ
        //*  GUY S
        if (condition(pdaMdma190.getMdma190_Pnd_Gda_User_Id().equals("NASB510") || pdaMdma190.getMdma190_Pnd_Gda_User_Id().equals("NASB491") || pdaMdma190.getMdma190_Pnd_Gda_User_Id().equals("NASB750")  //Natural: IF MDMA190.#GDA-USER-ID EQ 'NASB510' OR EQ 'NASB491' OR EQ 'NASB750' OR EQ 'NASB751' OR EQ 'NASB752' OR EQ 'NASN205' OR EQ 'NASB891' OR EQ 'MDMCWF'
            || pdaMdma190.getMdma190_Pnd_Gda_User_Id().equals("NASB751") || pdaMdma190.getMdma190_Pnd_Gda_User_Id().equals("NASB752") || pdaMdma190.getMdma190_Pnd_Gda_User_Id().equals("NASN205") 
            || pdaMdma190.getMdma190_Pnd_Gda_User_Id().equals("NASB891") || pdaMdma190.getMdma190_Pnd_Gda_User_Id().equals("MDMCWF")))
        {
            pdaEfsa9120.getEfsa9120_Action().setValue(pdaNasa191.getNasa191_Pnd_Action());                                                                                //Natural: ASSIGN EFSA9120.ACTION := NASA191.#ACTION
            pdaEfsa9120.getEfsa9120_Doc_Category().setValue(pdaNasa191.getNasa191_Pnd_Doc_Category());                                                                    //Natural: ASSIGN EFSA9120.DOC-CATEGORY := NASA191.#DOC-CATEGORY
            pdaEfsa9120.getEfsa9120_Doc_Class().setValue(pdaNasa191.getNasa191_Pnd_Doc_Class());                                                                          //Natural: ASSIGN EFSA9120.DOC-CLASS := NASA191.#DOC-CLASS
            pdaEfsa9120.getEfsa9120_Doc_Direction().setValue(pdaNasa191.getNasa191_Pnd_Doc_Direction());                                                                  //Natural: ASSIGN EFSA9120.DOC-DIRECTION := NASA191.#DOC-DIRECTION
            pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue(pdaNasa191.getNasa191_Pnd_Doc_Format_Cde());                                                                //Natural: ASSIGN EFSA9120.DOC-FORMAT-CDE := NASA191.#DOC-FORMAT-CDE
            pdaEfsa9120.getEfsa9120_Doc_Retention_Cde().setValue(pdaNasa191.getNasa191_Pnd_Doc_Retention_Cde());                                                          //Natural: ASSIGN EFSA9120.DOC-RETENTION-CDE := NASA191.#DOC-RETENTION-CDE
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(1,":",60).setValue(pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(1,":",60));                                      //Natural: ASSIGN EFSA9120.DOC-TEXT ( 1:60 ) := NASA191.#DOC-TEXT ( 1:60 )
            pdaEfsa9120.getEfsa9120_Last_Page_Flag().setValue(pdaNasa191.getNasa191_Pnd_Last_Page_Flag());                                                                //Natural: ASSIGN EFSA9120.LAST-PAGE-FLAG := NASA191.#LAST-PAGE-FLAG
            pdaEfsa9120.getEfsa9120_Batch_Nbr().setValue(pdaNasa191.getNasa191_Pnd_Batch_Nbr());                                                                          //Natural: ASSIGN EFSA9120.BATCH-NBR := NASA191.#BATCH-NBR
            pdaEfsa9120.getEfsa9120_Doc_Specific().reset();                                                                                                               //Natural: RESET EFSA9120.DOC-SPECIFIC
            //*  FE0701 END
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                    //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        pdaMdma190.getMdma190_Pnd_Error_Message().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                    //Natural: MOVE MSG-INFO-SUB.##MSG TO MDMA190.#ERROR-MESSAGE
        if (condition(pdaMdma190.getMdma190_Pnd_Error_Message().equals(" ")))                                                                                             //Natural: IF MDMA190.#ERROR-MESSAGE = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).setValue("7990");                                                                                            //Natural: MOVE '7990' TO EFSA9120.STATUS-CDE ( 1 )
            DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
