/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:29:19 AM
**        * FROM NATURAL SUBPROGRAM : Mdmn300a
************************************************************
**        * FILE NAME            : Mdmn300a.java
**        * CLASS NAME           : Mdmn300a
**        * INSTANCE NAME        : Mdmn300a
************************************************************
************************************************************************
* PROGRAM NAME : MDMN300A - CLONE OF MDMN300 TO FETCH MDMP0010
* DESCRIPTION  : MDM ADD/UPDATE PARTICIPAN/CONTRACT INFO
*                THIS SUBPROGRAM REPLACES CORN200 TO ADD/UPDT PH/CNTRCT
* WRITTEN BY   : DENNIS DURAN
* DATE WRITTEN : JUNE 11, 2009
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
************************************************************************

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mdmn300a extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    public DbsRecord parameters;
    private PdaMdma300 pdaMdma300;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Max_In;
    private DbsField pnd_Max_Out;
    private DbsField pnd_Data_In;

    private DbsGroup pnd_Data_In__R_Field_1;

    private DbsGroup pnd_Data_In_Pnd_Temp_Ph;
    private DbsField pnd_Data_In_Pnd_Requestor;
    private DbsField pnd_Data_In_Pnd_Function_Code;
    private DbsField pnd_Data_In_Pnd_Pin_A7;

    private DbsGroup pnd_Data_In__R_Field_2;
    private DbsField pnd_Data_In_Pnd_Pin;
    private DbsField pnd_Data_In_Pnd_Soc_Sec_Nbr_A9;

    private DbsGroup pnd_Data_In__R_Field_3;
    private DbsField pnd_Data_In_Pnd_Soc_Sec_Nbr;
    private DbsField pnd_Data_In_Pnd_Foreign_Soc_Sec_Nbr;

    private DbsGroup pnd_Data_In__R_Field_4;
    private DbsField pnd_Data_In_Pnd_Foreign_Soc_Sec_Nbr_A9;
    private DbsField pnd_Data_In_Pnd_Negative_Election_Code;
    private DbsField pnd_Data_In_Pnd_Date_Of_Birth;
    private DbsField pnd_Data_In_Pnd_Date_Of_Death;
    private DbsField pnd_Data_In_Pnd_Sex_Code;
    private DbsField pnd_Data_In_Pnd_Occupation;
    private DbsField pnd_Data_In_Pnd_Prefix;
    private DbsField pnd_Data_In_Pnd_Last_Name;

    private DbsGroup pnd_Data_In__R_Field_5;
    private DbsField pnd_Data_In_Pnd_Last_Name_Char;
    private DbsField pnd_Data_In_Pnd_First_Name;

    private DbsGroup pnd_Data_In__R_Field_6;
    private DbsField pnd_Data_In_Pnd_First_Name_Char;
    private DbsField pnd_Data_In_Pnd_Middle_Name;

    private DbsGroup pnd_Data_In__R_Field_7;
    private DbsField pnd_Data_In_Pnd_Middle_Name_Char;
    private DbsField pnd_Data_In_Pnd_Suffix;
    private DbsField pnd_Data_In_Pnd_Tlc_Category;
    private DbsField pnd_Data_In_Pnd_Tlc_Area_Of_Origin;
    private DbsField pnd_Data_In_Pnd_Mail_Code;
    private DbsField pnd_Data_In_Pnd_Mail_Area_Of_Origin;
    private DbsField pnd_Data_In_Pnd_Contract_Table_Count;

    private DbsGroup pnd_Data_In_Pnd_Temp_Cn;
    private DbsField pnd_Data_In_Pnd_Contract;
    private DbsField pnd_Data_In_Pnd_Issue_Date;
    private DbsField pnd_Data_In_Pnd_Status_Code;
    private DbsField pnd_Data_In_Pnd_Status_Year;
    private DbsField pnd_Data_In_Pnd_Payee_Code;
    private DbsField pnd_Data_In_Pnd_Cref_Contract;
    private DbsField pnd_Data_In_Pnd_Cref_Issued_Ind;
    private DbsField pnd_Data_In_Pnd_Da_Ownership_Code;
    private DbsField pnd_Data_In_Pnd_Ia_Option_Code;
    private DbsField pnd_Data_In_Pnd_Ins_Plan_Code;
    private DbsField pnd_Data_In_Pnd_Mf_Social_Cde;
    private DbsField pnd_Data_In_Pnd_Cntrct_Universal_Data;
    private DbsField pnd_Return;

    private DbsGroup pnd_Return__R_Field_8;
    private DbsField pnd_Return_Pnd_Rtrn_Code;

    private DbsGroup pnd_Return__R_Field_9;
    private DbsField pnd_Return_Pnd_Rtrn_Code_N4;
    private DbsField pnd_Return_Pnd_Rtrn_Text;
    private DbsField pnd_Data_Out;

    private DbsGroup pnd_Data_Out__R_Field_10;

    private DbsGroup pnd_Data_Out_Pnd_Temp_Data;
    private DbsField pnd_Data_Out_Pnd_Current_Pin;
    private DbsField pnd_Data_Out_Pnd_Current_Soc_Sec_Nbr;
    private DbsField pnd_Data_Out_Pnd_Current_Prefix;
    private DbsField pnd_Data_Out_Pnd_Current_Last_Name;
    private DbsField pnd_Data_Out_Pnd_Current_First_Name;
    private DbsField pnd_Data_Out_Pnd_Current_Middle_Name;
    private DbsField pnd_Data_Out_Pnd_Current_Suffix;
    private DbsField pnd_Data_Out_Pnd_Current_Date_Of_Birth;
    private DbsField pnd_Data_Out_Pnd_Current_Sex_Code;
    private DbsField pnd_Save_Code;
    private DbsField pnd_Save_Function_Code;
    private DbsField pnd_Save_Text;
    private DbsField pnd_X;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        // parameters
        parameters = new DbsRecord();
        pdaMdma300 = new PdaMdma300(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Max_In = localVariables.newFieldInRecord("pnd_Max_In", "#MAX-IN", FieldType.NUMERIC, 5);
        pnd_Max_Out = localVariables.newFieldInRecord("pnd_Max_Out", "#MAX-OUT", FieldType.NUMERIC, 5);
        pnd_Data_In = localVariables.newFieldArrayInRecord("pnd_Data_In", "#DATA-IN", FieldType.STRING, 1, new DbsArrayController(1, 32560));

        pnd_Data_In__R_Field_1 = localVariables.newGroupInRecord("pnd_Data_In__R_Field_1", "REDEFINE", pnd_Data_In);

        pnd_Data_In_Pnd_Temp_Ph = pnd_Data_In__R_Field_1.newGroupInGroup("pnd_Data_In_Pnd_Temp_Ph", "#TEMP-PH");
        pnd_Data_In_Pnd_Requestor = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Requestor", "#REQUESTOR", FieldType.STRING, 8);
        pnd_Data_In_Pnd_Function_Code = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Function_Code", "#FUNCTION-CODE", FieldType.STRING, 3);
        pnd_Data_In_Pnd_Pin_A7 = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Pin_A7", "#PIN-A7", FieldType.STRING, 7);

        pnd_Data_In__R_Field_2 = pnd_Data_In_Pnd_Temp_Ph.newGroupInGroup("pnd_Data_In__R_Field_2", "REDEFINE", pnd_Data_In_Pnd_Pin_A7);
        pnd_Data_In_Pnd_Pin = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_Pin", "#PIN", FieldType.NUMERIC, 7);
        pnd_Data_In_Pnd_Soc_Sec_Nbr_A9 = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Soc_Sec_Nbr_A9", "#SOC-SEC-NBR-A9", FieldType.STRING, 
            9);

        pnd_Data_In__R_Field_3 = pnd_Data_In_Pnd_Temp_Ph.newGroupInGroup("pnd_Data_In__R_Field_3", "REDEFINE", pnd_Data_In_Pnd_Soc_Sec_Nbr_A9);
        pnd_Data_In_Pnd_Soc_Sec_Nbr = pnd_Data_In__R_Field_3.newFieldInGroup("pnd_Data_In_Pnd_Soc_Sec_Nbr", "#SOC-SEC-NBR", FieldType.NUMERIC, 9);
        pnd_Data_In_Pnd_Foreign_Soc_Sec_Nbr = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Foreign_Soc_Sec_Nbr", "#FOREIGN-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);

        pnd_Data_In__R_Field_4 = pnd_Data_In_Pnd_Temp_Ph.newGroupInGroup("pnd_Data_In__R_Field_4", "REDEFINE", pnd_Data_In_Pnd_Foreign_Soc_Sec_Nbr);
        pnd_Data_In_Pnd_Foreign_Soc_Sec_Nbr_A9 = pnd_Data_In__R_Field_4.newFieldInGroup("pnd_Data_In_Pnd_Foreign_Soc_Sec_Nbr_A9", "#FOREIGN-SOC-SEC-NBR-A9", 
            FieldType.STRING, 9);
        pnd_Data_In_Pnd_Negative_Election_Code = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Negative_Election_Code", "#NEGATIVE-ELECTION-CODE", 
            FieldType.NUMERIC, 3);
        pnd_Data_In_Pnd_Date_Of_Birth = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Date_Of_Birth", "#DATE-OF-BIRTH", FieldType.NUMERIC, 
            8);
        pnd_Data_In_Pnd_Date_Of_Death = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Date_Of_Death", "#DATE-OF-DEATH", FieldType.NUMERIC, 
            8);
        pnd_Data_In_Pnd_Sex_Code = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Sex_Code", "#SEX-CODE", FieldType.STRING, 1);
        pnd_Data_In_Pnd_Occupation = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Occupation", "#OCCUPATION", FieldType.STRING, 10);
        pnd_Data_In_Pnd_Prefix = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Prefix", "#PREFIX", FieldType.STRING, 8);
        pnd_Data_In_Pnd_Last_Name = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 30);

        pnd_Data_In__R_Field_5 = pnd_Data_In_Pnd_Temp_Ph.newGroupInGroup("pnd_Data_In__R_Field_5", "REDEFINE", pnd_Data_In_Pnd_Last_Name);
        pnd_Data_In_Pnd_Last_Name_Char = pnd_Data_In__R_Field_5.newFieldArrayInGroup("pnd_Data_In_Pnd_Last_Name_Char", "#LAST-NAME-CHAR", FieldType.STRING, 
            1, new DbsArrayController(1, 30));
        pnd_Data_In_Pnd_First_Name = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 30);

        pnd_Data_In__R_Field_6 = pnd_Data_In_Pnd_Temp_Ph.newGroupInGroup("pnd_Data_In__R_Field_6", "REDEFINE", pnd_Data_In_Pnd_First_Name);
        pnd_Data_In_Pnd_First_Name_Char = pnd_Data_In__R_Field_6.newFieldArrayInGroup("pnd_Data_In_Pnd_First_Name_Char", "#FIRST-NAME-CHAR", FieldType.STRING, 
            1, new DbsArrayController(1, 30));
        pnd_Data_In_Pnd_Middle_Name = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Middle_Name", "#MIDDLE-NAME", FieldType.STRING, 30);

        pnd_Data_In__R_Field_7 = pnd_Data_In_Pnd_Temp_Ph.newGroupInGroup("pnd_Data_In__R_Field_7", "REDEFINE", pnd_Data_In_Pnd_Middle_Name);
        pnd_Data_In_Pnd_Middle_Name_Char = pnd_Data_In__R_Field_7.newFieldArrayInGroup("pnd_Data_In_Pnd_Middle_Name_Char", "#MIDDLE-NAME-CHAR", FieldType.STRING, 
            1, new DbsArrayController(1, 30));
        pnd_Data_In_Pnd_Suffix = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Suffix", "#SUFFIX", FieldType.STRING, 8);
        pnd_Data_In_Pnd_Tlc_Category = pnd_Data_In_Pnd_Temp_Ph.newFieldArrayInGroup("pnd_Data_In_Pnd_Tlc_Category", "#TLC-CATEGORY", FieldType.STRING, 
            2, new DbsArrayController(1, 8));
        pnd_Data_In_Pnd_Tlc_Area_Of_Origin = pnd_Data_In_Pnd_Temp_Ph.newFieldArrayInGroup("pnd_Data_In_Pnd_Tlc_Area_Of_Origin", "#TLC-AREA-OF-ORIGIN", 
            FieldType.STRING, 3, new DbsArrayController(1, 8));
        pnd_Data_In_Pnd_Mail_Code = pnd_Data_In_Pnd_Temp_Ph.newFieldArrayInGroup("pnd_Data_In_Pnd_Mail_Code", "#MAIL-CODE", FieldType.STRING, 2, new DbsArrayController(1, 
            25));
        pnd_Data_In_Pnd_Mail_Area_Of_Origin = pnd_Data_In_Pnd_Temp_Ph.newFieldArrayInGroup("pnd_Data_In_Pnd_Mail_Area_Of_Origin", "#MAIL-AREA-OF-ORIGIN", 
            FieldType.STRING, 3, new DbsArrayController(1, 25));
        pnd_Data_In_Pnd_Contract_Table_Count = pnd_Data_In_Pnd_Temp_Ph.newFieldInGroup("pnd_Data_In_Pnd_Contract_Table_Count", "#CONTRACT-TABLE-COUNT", 
            FieldType.NUMERIC, 2);

        pnd_Data_In_Pnd_Temp_Cn = pnd_Data_In__R_Field_1.newGroupInGroup("pnd_Data_In_Pnd_Temp_Cn", "#TEMP-CN");
        pnd_Data_In_Pnd_Contract = pnd_Data_In_Pnd_Temp_Cn.newFieldInGroup("pnd_Data_In_Pnd_Contract", "#CONTRACT", FieldType.STRING, 10);
        pnd_Data_In_Pnd_Issue_Date = pnd_Data_In_Pnd_Temp_Cn.newFieldInGroup("pnd_Data_In_Pnd_Issue_Date", "#ISSUE-DATE", FieldType.NUMERIC, 8);
        pnd_Data_In_Pnd_Status_Code = pnd_Data_In_Pnd_Temp_Cn.newFieldInGroup("pnd_Data_In_Pnd_Status_Code", "#STATUS-CODE", FieldType.STRING, 1);
        pnd_Data_In_Pnd_Status_Year = pnd_Data_In_Pnd_Temp_Cn.newFieldInGroup("pnd_Data_In_Pnd_Status_Year", "#STATUS-YEAR", FieldType.NUMERIC, 4);
        pnd_Data_In_Pnd_Payee_Code = pnd_Data_In_Pnd_Temp_Cn.newFieldInGroup("pnd_Data_In_Pnd_Payee_Code", "#PAYEE-CODE", FieldType.STRING, 2);
        pnd_Data_In_Pnd_Cref_Contract = pnd_Data_In_Pnd_Temp_Cn.newFieldInGroup("pnd_Data_In_Pnd_Cref_Contract", "#CREF-CONTRACT", FieldType.STRING, 10);
        pnd_Data_In_Pnd_Cref_Issued_Ind = pnd_Data_In_Pnd_Temp_Cn.newFieldInGroup("pnd_Data_In_Pnd_Cref_Issued_Ind", "#CREF-ISSUED-IND", FieldType.STRING, 
            1);
        pnd_Data_In_Pnd_Da_Ownership_Code = pnd_Data_In_Pnd_Temp_Cn.newFieldInGroup("pnd_Data_In_Pnd_Da_Ownership_Code", "#DA-OWNERSHIP-CODE", FieldType.STRING, 
            1);
        pnd_Data_In_Pnd_Ia_Option_Code = pnd_Data_In_Pnd_Temp_Cn.newFieldInGroup("pnd_Data_In_Pnd_Ia_Option_Code", "#IA-OPTION-CODE", FieldType.STRING, 
            2);
        pnd_Data_In_Pnd_Ins_Plan_Code = pnd_Data_In_Pnd_Temp_Cn.newFieldInGroup("pnd_Data_In_Pnd_Ins_Plan_Code", "#INS-PLAN-CODE", FieldType.STRING, 5);
        pnd_Data_In_Pnd_Mf_Social_Cde = pnd_Data_In_Pnd_Temp_Cn.newFieldInGroup("pnd_Data_In_Pnd_Mf_Social_Cde", "#MF-SOCIAL-CDE", FieldType.STRING, 3);
        pnd_Data_In_Pnd_Cntrct_Universal_Data = pnd_Data_In_Pnd_Temp_Cn.newFieldInGroup("pnd_Data_In_Pnd_Cntrct_Universal_Data", "#CNTRCT-UNIVERSAL-DATA", 
            FieldType.STRING, 150);
        pnd_Return = localVariables.newFieldArrayInRecord("pnd_Return", "#RETURN", FieldType.STRING, 1, new DbsArrayController(1, 200));

        pnd_Return__R_Field_8 = localVariables.newGroupInRecord("pnd_Return__R_Field_8", "REDEFINE", pnd_Return);
        pnd_Return_Pnd_Rtrn_Code = pnd_Return__R_Field_8.newFieldInGroup("pnd_Return_Pnd_Rtrn_Code", "#RTRN-CODE", FieldType.STRING, 4);

        pnd_Return__R_Field_9 = pnd_Return__R_Field_8.newGroupInGroup("pnd_Return__R_Field_9", "REDEFINE", pnd_Return_Pnd_Rtrn_Code);
        pnd_Return_Pnd_Rtrn_Code_N4 = pnd_Return__R_Field_9.newFieldInGroup("pnd_Return_Pnd_Rtrn_Code_N4", "#RTRN-CODE-N4", FieldType.NUMERIC, 4);
        pnd_Return_Pnd_Rtrn_Text = pnd_Return__R_Field_8.newFieldInGroup("pnd_Return_Pnd_Rtrn_Text", "#RTRN-TEXT", FieldType.STRING, 80);
        pnd_Data_Out = localVariables.newFieldArrayInRecord("pnd_Data_Out", "#DATA-OUT", FieldType.STRING, 1, new DbsArrayController(1, 32360));

        pnd_Data_Out__R_Field_10 = localVariables.newGroupInRecord("pnd_Data_Out__R_Field_10", "REDEFINE", pnd_Data_Out);

        pnd_Data_Out_Pnd_Temp_Data = pnd_Data_Out__R_Field_10.newGroupInGroup("pnd_Data_Out_Pnd_Temp_Data", "#TEMP-DATA");
        pnd_Data_Out_Pnd_Current_Pin = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_Current_Pin", "#CURRENT-PIN", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_Current_Soc_Sec_Nbr = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_Current_Soc_Sec_Nbr", "#CURRENT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Data_Out_Pnd_Current_Prefix = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_Current_Prefix", "#CURRENT-PREFIX", FieldType.STRING, 
            8);
        pnd_Data_Out_Pnd_Current_Last_Name = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_Current_Last_Name", "#CURRENT-LAST-NAME", FieldType.STRING, 
            30);
        pnd_Data_Out_Pnd_Current_First_Name = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_Current_First_Name", "#CURRENT-FIRST-NAME", 
            FieldType.STRING, 30);
        pnd_Data_Out_Pnd_Current_Middle_Name = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_Current_Middle_Name", "#CURRENT-MIDDLE-NAME", 
            FieldType.STRING, 30);
        pnd_Data_Out_Pnd_Current_Suffix = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_Current_Suffix", "#CURRENT-SUFFIX", FieldType.STRING, 
            8);
        pnd_Data_Out_Pnd_Current_Date_Of_Birth = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_Current_Date_Of_Birth", "#CURRENT-DATE-OF-BIRTH", 
            FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_Current_Sex_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_Current_Sex_Code", "#CURRENT-SEX-CODE", FieldType.STRING, 
            1);
        pnd_Save_Code = localVariables.newFieldInRecord("pnd_Save_Code", "#SAVE-CODE", FieldType.STRING, 4);
        pnd_Save_Function_Code = localVariables.newFieldInRecord("pnd_Save_Function_Code", "#SAVE-FUNCTION-CODE", FieldType.STRING, 3);
        pnd_Save_Text = localVariables.newFieldInRecord("pnd_Save_Text", "#SAVE-TEXT", FieldType.STRING, 80);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Max_In.setInitialValue(536);
        pnd_Max_Out.setInitialValue(131);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Mdmn300a() throws Exception
    {
        super("Mdmn300a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  ASSIGN DEFAULTS
        short decideConditionsMet296 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #MDMA300.#PIN = 0 OR #MDMA300.#PIN-A7 = ' '
        if (condition(pdaMdma300.getPnd_Mdma300_Pnd_Pin().equals(getZero()) || pdaMdma300.getPnd_Mdma300_Pnd_Pin_A7().equals(" ")))
        {
            decideConditionsMet296++;
            pdaMdma300.getPnd_Mdma300_Pnd_Pin_A7().setValue("0000000");                                                                                                   //Natural: ASSIGN #MDMA300.#PIN-A7 := '0000000'
        }                                                                                                                                                                 //Natural: WHEN #MDMA300.#SOC-SEC-NBR = 0 OR #MDMA300.#SOC-SEC-NBR-A9 = ' '
        if (condition(pdaMdma300.getPnd_Mdma300_Pnd_Soc_Sec_Nbr().equals(getZero()) || pdaMdma300.getPnd_Mdma300_Pnd_Soc_Sec_Nbr_A9().equals(" ")))
        {
            decideConditionsMet296++;
            pdaMdma300.getPnd_Mdma300_Pnd_Soc_Sec_Nbr_A9().setValue("000000000");                                                                                         //Natural: ASSIGN #MDMA300.#SOC-SEC-NBR-A9 := '000000000'
        }                                                                                                                                                                 //Natural: WHEN #MDMA300.#FOREIGN-SOC-SEC-NBR = 0 OR #MDMA300.#FOREIGN-SOC-SEC-NBR-A9 = ' '
        if (condition(pdaMdma300.getPnd_Mdma300_Pnd_Foreign_Soc_Sec_Nbr().equals(getZero()) || pdaMdma300.getPnd_Mdma300_Pnd_Foreign_Soc_Sec_Nbr_A9().equals(" ")))
        {
            decideConditionsMet296++;
            pdaMdma300.getPnd_Mdma300_Pnd_Foreign_Soc_Sec_Nbr_A9().setValue("000000000");                                                                                 //Natural: ASSIGN #MDMA300.#FOREIGN-SOC-SEC-NBR-A9 := '000000000'
        }                                                                                                                                                                 //Natural: WHEN #MDMA300.#CONTRACT-TABLE-COUNT = 0
        if (condition(pdaMdma300.getPnd_Mdma300_Pnd_Contract_Table_Count().equals(getZero())))
        {
            decideConditionsMet296++;
            pdaMdma300.getPnd_Mdma300_Pnd_Contract_Table_Count().setValue(1);                                                                                             //Natural: ASSIGN #MDMA300.#CONTRACT-TABLE-COUNT := 1
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet296 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Save_Function_Code.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Function_Code());                                                                                   //Natural: ASSIGN #SAVE-FUNCTION-CODE := #MDMA300.#FUNCTION-CODE
        gdaMdmg0001.getPnd_Pnd_Mdmg0001().reset();                                                                                                                        //Natural: RESET ##MDMG0001
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Date_Sent().setValueEdited(Global.getDATX(),new ReportEditMask("YYYY-MM-DD"));                                        //Natural: MOVE EDITED *DATX ( EM = YYYY-MM-DD ) TO ##MSG-DATE-SENT
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Time_Sent().setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                          //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO ##MSG-TIME-SENT
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Guid().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "mfsync::", Global.getINIT_PROGRAM(),                 //Natural: COMPRESS 'mfsync::' *INIT-PROGRAM #MDMA300.#PIN-A7 #MDMA300.#SOC-SEC-NBR-A9 ##MSG-DATE-SENT ##MSG-TIME-SENT INTO ##MSG-GUID LEAVING NO
            pdaMdma300.getPnd_Mdma300_Pnd_Pin_A7(), pdaMdma300.getPnd_Mdma300_Pnd_Soc_Sec_Nbr_A9(), gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Date_Sent(), 
            gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Time_Sent()));
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Ping_Ind().setValue("N");                                                                                             //Natural: ASSIGN ##MSG-PING-IND := 'N'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Sender_Appl_Id().setValue("LEGACY");                                                                                      //Natural: ASSIGN ##SENDER-APPL-ID := 'LEGACY'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Appl_Id().setValue("MDM");                                                                                            //Natural: ASSIGN ##TGT-APPL-ID := 'MDM'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Module_Code().setValue("N");                                                                                          //Natural: ASSIGN ##TGT-MODULE-CODE := 'N'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Module_Name().setValue("MDMN300A");                                                                                   //Natural: ASSIGN ##TGT-MODULE-NAME := 'MDMN300A'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Ctr().setValue(1);                                                                                                    //Natural: ASSIGN ##PDA-CTR := 1
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length().setValue(pnd_Max_In);                                                                                        //Natural: ASSIGN ##PDA-LENGTH := #MAX-IN
        pnd_Data_In_Pnd_Temp_Ph.setValuesByName(pdaMdma300.getPnd_Mdma300());                                                                                             //Natural: MOVE BY NAME #MDMA300 TO #TEMP-PH
        FOR01:                                                                                                                                                            //Natural: FOR #X = 1 TO #MDMA300.#CONTRACT-TABLE-COUNT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pdaMdma300.getPnd_Mdma300_Pnd_Contract_Table_Count())); pnd_X.nadd(1))
        {
            pnd_Data_In_Pnd_Contract.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Contract().getValue(pnd_X));                                                                  //Natural: ASSIGN #DATA-IN.#CONTRACT := #MDMA300.#CONTRACT ( #X )
            pnd_Data_In_Pnd_Issue_Date.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Issue_Date().getValue(pnd_X));                                                              //Natural: ASSIGN #DATA-IN.#ISSUE-DATE := #MDMA300.#ISSUE-DATE ( #X )
            pnd_Data_In_Pnd_Status_Code.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Status_Code().getValue(pnd_X));                                                            //Natural: ASSIGN #DATA-IN.#STATUS-CODE := #MDMA300.#STATUS-CODE ( #X )
            pnd_Data_In_Pnd_Status_Year.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Status_Year().getValue(pnd_X));                                                            //Natural: ASSIGN #DATA-IN.#STATUS-YEAR := #MDMA300.#STATUS-YEAR ( #X )
            pnd_Data_In_Pnd_Payee_Code.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Payee_Code().getValue(pnd_X));                                                              //Natural: ASSIGN #DATA-IN.#PAYEE-CODE := #MDMA300.#PAYEE-CODE ( #X )
            pnd_Data_In_Pnd_Cref_Contract.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Cref_Contract().getValue(pnd_X));                                                        //Natural: ASSIGN #DATA-IN.#CREF-CONTRACT := #MDMA300.#CREF-CONTRACT ( #X )
            pnd_Data_In_Pnd_Cref_Issued_Ind.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Cref_Issued_Ind().getValue(pnd_X));                                                    //Natural: ASSIGN #DATA-IN.#CREF-ISSUED-IND := #MDMA300.#CREF-ISSUED-IND ( #X )
            pnd_Data_In_Pnd_Da_Ownership_Code.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Da_Ownership_Code().getValue(pnd_X));                                                //Natural: ASSIGN #DATA-IN.#DA-OWNERSHIP-CODE := #MDMA300.#DA-OWNERSHIP-CODE ( #X )
            pnd_Data_In_Pnd_Ia_Option_Code.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Ia_Option_Code().getValue(pnd_X));                                                      //Natural: ASSIGN #DATA-IN.#IA-OPTION-CODE := #MDMA300.#IA-OPTION-CODE ( #X )
            pnd_Data_In_Pnd_Ins_Plan_Code.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Ins_Plan_Code().getValue(pnd_X));                                                        //Natural: ASSIGN #DATA-IN.#INS-PLAN-CODE := #MDMA300.#INS-PLAN-CODE ( #X )
            pnd_Data_In_Pnd_Mf_Social_Cde.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Mf_Social_Cde().getValue(pnd_X));                                                        //Natural: ASSIGN #DATA-IN.#MF-SOCIAL-CDE := #MDMA300.#MF-SOCIAL-CDE ( #X )
            pnd_Data_In_Pnd_Cntrct_Universal_Data.setValue(pdaMdma300.getPnd_Mdma300_Pnd_Cntrct_Universal_Data().getValue(pnd_X));                                        //Natural: ASSIGN #DATA-IN.#CNTRCT-UNIVERSAL-DATA := #MDMA300.#CNTRCT-UNIVERSAL-DATA ( #X )
            gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_In().getValue("*").setValue(pnd_Data_In.getValue("*"));                                                          //Natural: ASSIGN ##DATA-IN ( * ) := #DATA-IN ( * )
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, "=",gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Guid());                                                                            //Natural: WRITE '=' ##MSG-GUID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0010"), getCurrentProcessState());                                                                                  //Natural: FETCH RETURN 'MDMP0010'
            if (condition(Global.isEscape())) return;
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, "Data Resp",gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue("*"));                                                 //Natural: WRITE 'Data Resp' ##DATA-RESPONSE ( * )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length().notEquals(pnd_Max_Out)))                                                               //Natural: IF ##PDA-LENGTH NE #MAX-OUT
                {
                    getReports().write(0, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length(),"Different PDA Length",pnd_Max_Out);                                       //Natural: WRITE ##PDA-LENGTH 'Different PDA Length' #MAX-OUT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Return.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue("*"));                                                     //Natural: ASSIGN #RETURN ( * ) := ##DATA-RESPONSE ( * )
            pnd_Data_Out.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Out().getValue("*"));                                                        //Natural: ASSIGN #DATA-OUT ( * ) := ##DATA-OUT ( * )
            pdaMdma300.getPnd_Mdma300_Pnd_Return_Code().setValue(pnd_Return_Pnd_Rtrn_Code_N4);                                                                            //Natural: ASSIGN #RETURN-CODE := #RTRN-CODE-N4
            pdaMdma300.getPnd_Mdma300_Pnd_Return_Text().setValue(pnd_Return_Pnd_Rtrn_Text);                                                                               //Natural: ASSIGN #RETURN-TEXT := #RTRN-TEXT
            if (condition(pnd_Save_Function_Code.equals("003") || pnd_Save_Function_Code.equals("060") || pnd_Save_Function_Code.equals("061") || pnd_Save_Function_Code.equals("062"))) //Natural: IF #SAVE-FUNCTION-CODE = '003' OR = '060' OR = '061' OR = '062'
            {
                pdaMdma300.getPnd_Mdma300_Pnd_New_Pin().setValue(pnd_Data_Out_Pnd_Current_Pin);                                                                           //Natural: ASSIGN #MDMA300.#NEW-PIN := #DATA-OUT.#CURRENT-PIN
                pdaMdma300.getPnd_Mdma300_Pnd_New_Record_Type().setValue(1);                                                                                              //Natural: ASSIGN #MDMA300.#NEW-RECORD-TYPE := 01
            }                                                                                                                                                             //Natural: END-IF
            pdaMdma300.getPnd_Mdma300_Pnd_Current_Pin().getValue(pnd_X).setValue(pnd_Data_Out_Pnd_Current_Pin);                                                           //Natural: ASSIGN #MDMA300.#CURRENT-PIN ( #X ) := #DATA-OUT.#CURRENT-PIN
            pdaMdma300.getPnd_Mdma300_Pnd_Current_Record_Type().getValue(pnd_X).setValue(1);                                                                              //Natural: ASSIGN #MDMA300.#CURRENT-RECORD-TYPE ( #X ) := 01
            pdaMdma300.getPnd_Mdma300_Pnd_Current_Soc_Sec_Nbr().getValue(pnd_X).setValue(pnd_Data_Out_Pnd_Current_Soc_Sec_Nbr);                                           //Natural: ASSIGN #MDMA300.#CURRENT-SOC-SEC-NBR ( #X ) := #DATA-OUT.#CURRENT-SOC-SEC-NBR
            pdaMdma300.getPnd_Mdma300_Pnd_Current_Prefix().getValue(pnd_X).setValue(pnd_Data_Out_Pnd_Current_Prefix);                                                     //Natural: ASSIGN #MDMA300.#CURRENT-PREFIX ( #X ) := #DATA-OUT.#CURRENT-PREFIX
            pdaMdma300.getPnd_Mdma300_Pnd_Current_Last_Name().getValue(pnd_X).setValue(pnd_Data_Out_Pnd_Current_Last_Name);                                               //Natural: ASSIGN #MDMA300.#CURRENT-LAST-NAME ( #X ) := #DATA-OUT.#CURRENT-LAST-NAME
            pdaMdma300.getPnd_Mdma300_Pnd_Current_First_Name().getValue(pnd_X).setValue(pnd_Data_Out_Pnd_Current_First_Name);                                             //Natural: ASSIGN #MDMA300.#CURRENT-FIRST-NAME ( #X ) := #DATA-OUT.#CURRENT-FIRST-NAME
            pdaMdma300.getPnd_Mdma300_Pnd_Current_Middle_Name().getValue(pnd_X).setValue(pnd_Data_Out_Pnd_Current_Middle_Name);                                           //Natural: ASSIGN #MDMA300.#CURRENT-MIDDLE-NAME ( #X ) := #DATA-OUT.#CURRENT-MIDDLE-NAME
            pdaMdma300.getPnd_Mdma300_Pnd_Current_Suffix().getValue(pnd_X).setValue(pnd_Data_Out_Pnd_Current_Suffix);                                                     //Natural: ASSIGN #MDMA300.#CURRENT-SUFFIX ( #X ) := #DATA-OUT.#CURRENT-SUFFIX
            pdaMdma300.getPnd_Mdma300_Pnd_Current_Date_Of_Birth().getValue(pnd_X).setValue(pnd_Data_Out_Pnd_Current_Date_Of_Birth);                                       //Natural: ASSIGN #MDMA300.#CURRENT-DATE-OF-BIRTH ( #X ) := #DATA-OUT.#CURRENT-DATE-OF-BIRTH
            pdaMdma300.getPnd_Mdma300_Pnd_Current_Sex_Code().getValue(pnd_X).setValue(pnd_Data_Out_Pnd_Current_Sex_Code);                                                 //Natural: ASSIGN #MDMA300.#CURRENT-SEX-CODE ( #X ) := #DATA-OUT.#CURRENT-SEX-CODE
            if (condition(pnd_Return_Pnd_Rtrn_Code_N4.notEquals(getZero())))                                                                                              //Natural: IF #RTRN-CODE-N4 NE 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //
}
