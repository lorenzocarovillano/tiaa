/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:07:54 PM
**        * FROM NATURAL LDA     : NASL747A
************************************************************
**        * FILE NAME            : LdaNasl747a.java
**        * CLASS NAME           : LdaNasl747a
**        * INSTANCE NAME        : LdaNasl747a
************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaNasl747a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Bad_Output;
    private DbsGroup pnd_Bad_Output_Pnd_Tiaa_Input;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Name;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Line_1;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Line_2;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Line_3;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Line_4;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_City_State;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4;
    private DbsGroup pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef1;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Zip_5;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Zip_4;
    private DbsGroup pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef2;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4_Num;
    private DbsField pnd_Bad_Output_Pnd_Filler;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Nmbr;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_Cde;
    private DbsGroup pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_CdeRedef3;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_Cde_N;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_Nmbr;
    private DbsGroup pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_NmbrRedef4;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_Numeric;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_X;
    private DbsGroup pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_XRedef5;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_Numeric;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Pin_Base_Addr_Ind;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Ssn;
    private DbsGroup pnd_Bad_Output_Pnd_Bad_Addr_SsnRedef6;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Ssn_Numeric;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Dob;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Vip_High_Accum_Ind;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt_A;
    private DbsGroup pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt_ARedef7;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Filler;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Pin_A12;
    private DbsGroup pnd_Bad_Output_Pnd_Bad_Addr_Pin_A12Redef8;
    private DbsField pnd_Bad_Output_Pnd_Bad_Addr_Pin_N12;
    private DbsGroup pnd_Bad_Output_Pnd_Vendor_Info;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Unknown;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Deceased_Info;
    private DbsField pnd_Bad_Output_Pnd_Vendor_First_Name;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Middle_Init;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Last_Name;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Suffix;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Street_Addr;
    private DbsField pnd_Bad_Output_Pnd_Vendor_City;
    private DbsField pnd_Bad_Output_Pnd_Vendor_State;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Zip_Code;
    private DbsGroup pnd_Bad_Output_Pnd_Vendor_Zip_CodeRedef9;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Zip_5;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Zip_Filler;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Zip_Plus_4;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Phone_Num;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Dual_Anme;
    private DbsField pnd_Bad_Output_Pnd_Vendor_First_Date;
    private DbsField pnd_Bad_Output_Pnd_Vendor_Last_Date;
    private DbsField pnd_Bad_Output_Pnd_Vendor_New_Line;
    private DbsGroup pnd_Bad_Output_Pnd_Tiaa_Output;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Sort_Key;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1;
    private DbsGroup pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1Redef10;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1_A30;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1_Filler;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line2;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line3;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line4;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line5;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4;
    private DbsGroup pnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4Redef11;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Zip;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Zip_4;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Carrier_Rte;
    private DbsField pnd_Bad_Output_Pnd_Tiaa_Std_Walk_Seq;

    public DbsGroup getPnd_Bad_Output() { return pnd_Bad_Output; }

    public DbsGroup getPnd_Bad_Output_Pnd_Tiaa_Input() { return pnd_Bad_Output_Pnd_Tiaa_Input; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Name() { return pnd_Bad_Output_Pnd_Bad_Addr_Name; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Line_1() { return pnd_Bad_Output_Pnd_Bad_Addr_Line_1; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Line_2() { return pnd_Bad_Output_Pnd_Bad_Addr_Line_2; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Line_3() { return pnd_Bad_Output_Pnd_Bad_Addr_Line_3; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Line_4() { return pnd_Bad_Output_Pnd_Bad_Addr_Line_4; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_City_State() { return pnd_Bad_Output_Pnd_Bad_Addr_City_State; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4() { return pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4; }

    public DbsGroup getPnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef1() { return pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef1; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Zip_5() { return pnd_Bad_Output_Pnd_Bad_Addr_Zip_5; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Zip_4() { return pnd_Bad_Output_Pnd_Bad_Addr_Zip_4; }

    public DbsGroup getPnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef2() { return pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef2; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4_Num() { return pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4_Num; }

    public DbsField getPnd_Bad_Output_Pnd_Filler() { return pnd_Bad_Output_Pnd_Filler; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Nmbr() { return pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Nmbr; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_Cde() { return pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_Cde; }

    public DbsGroup getPnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_CdeRedef3() { return pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_CdeRedef3; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_Cde_N() { return pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_Cde_N; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_Nmbr() { return pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_Nmbr; }

    public DbsGroup getPnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_NmbrRedef4() { return pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_NmbrRedef4; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_Numeric() { return pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_Numeric; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_X() { return pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_X; }

    public DbsGroup getPnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_XRedef5() { return pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_XRedef5; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_Numeric() { return pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_Numeric; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Pin_Base_Addr_Ind() { return pnd_Bad_Output_Pnd_Bad_Addr_Pin_Base_Addr_Ind; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Ssn() { return pnd_Bad_Output_Pnd_Bad_Addr_Ssn; }

    public DbsGroup getPnd_Bad_Output_Pnd_Bad_Addr_SsnRedef6() { return pnd_Bad_Output_Pnd_Bad_Addr_SsnRedef6; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Ssn_Numeric() { return pnd_Bad_Output_Pnd_Bad_Addr_Ssn_Numeric; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Dob() { return pnd_Bad_Output_Pnd_Bad_Addr_Dob; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Vip_High_Accum_Ind() { return pnd_Bad_Output_Pnd_Bad_Addr_Vip_High_Accum_Ind; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt_A() { return pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt_A; }

    public DbsGroup getPnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt_ARedef7() { return pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt_ARedef7; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt() { return pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Filler() { return pnd_Bad_Output_Pnd_Bad_Addr_Filler; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Pin_A12() { return pnd_Bad_Output_Pnd_Bad_Addr_Pin_A12; }

    public DbsGroup getPnd_Bad_Output_Pnd_Bad_Addr_Pin_A12Redef8() { return pnd_Bad_Output_Pnd_Bad_Addr_Pin_A12Redef8; }

    public DbsField getPnd_Bad_Output_Pnd_Bad_Addr_Pin_N12() { return pnd_Bad_Output_Pnd_Bad_Addr_Pin_N12; }

    public DbsGroup getPnd_Bad_Output_Pnd_Vendor_Info() { return pnd_Bad_Output_Pnd_Vendor_Info; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Unknown() { return pnd_Bad_Output_Pnd_Vendor_Unknown; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Deceased_Info() { return pnd_Bad_Output_Pnd_Vendor_Deceased_Info; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_First_Name() { return pnd_Bad_Output_Pnd_Vendor_First_Name; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Middle_Init() { return pnd_Bad_Output_Pnd_Vendor_Middle_Init; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Last_Name() { return pnd_Bad_Output_Pnd_Vendor_Last_Name; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Suffix() { return pnd_Bad_Output_Pnd_Vendor_Suffix; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Street_Addr() { return pnd_Bad_Output_Pnd_Vendor_Street_Addr; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_City() { return pnd_Bad_Output_Pnd_Vendor_City; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_State() { return pnd_Bad_Output_Pnd_Vendor_State; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Zip_Code() { return pnd_Bad_Output_Pnd_Vendor_Zip_Code; }

    public DbsGroup getPnd_Bad_Output_Pnd_Vendor_Zip_CodeRedef9() { return pnd_Bad_Output_Pnd_Vendor_Zip_CodeRedef9; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Zip_5() { return pnd_Bad_Output_Pnd_Vendor_Zip_5; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Zip_Filler() { return pnd_Bad_Output_Pnd_Vendor_Zip_Filler; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Zip_Plus_4() { return pnd_Bad_Output_Pnd_Vendor_Zip_Plus_4; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Phone_Num() { return pnd_Bad_Output_Pnd_Vendor_Phone_Num; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Dual_Anme() { return pnd_Bad_Output_Pnd_Vendor_Dual_Anme; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_First_Date() { return pnd_Bad_Output_Pnd_Vendor_First_Date; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_Last_Date() { return pnd_Bad_Output_Pnd_Vendor_Last_Date; }

    public DbsField getPnd_Bad_Output_Pnd_Vendor_New_Line() { return pnd_Bad_Output_Pnd_Vendor_New_Line; }

    public DbsGroup getPnd_Bad_Output_Pnd_Tiaa_Output() { return pnd_Bad_Output_Pnd_Tiaa_Output; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Sort_Key() { return pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Sort_Key; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1() { return pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1; }

    public DbsGroup getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1Redef10() { return pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1Redef10; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1_A30() { return pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1_A30; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1_Filler() { return pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1_Filler; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line2() { return pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line2; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line3() { return pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line3; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line4() { return pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line4; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line5() { return pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line5; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4() { return pnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4; }

    public DbsGroup getPnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4Redef11() { return pnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4Redef11; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Zip() { return pnd_Bad_Output_Pnd_Tiaa_Std_Zip; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Zip_4() { return pnd_Bad_Output_Pnd_Tiaa_Std_Zip_4; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Carrier_Rte() { return pnd_Bad_Output_Pnd_Tiaa_Std_Carrier_Rte; }

    public DbsField getPnd_Bad_Output_Pnd_Tiaa_Std_Walk_Seq() { return pnd_Bad_Output_Pnd_Tiaa_Std_Walk_Seq; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Bad_Output = newGroupInRecord("pnd_Bad_Output", "#BAD-OUTPUT");
        pnd_Bad_Output_Pnd_Tiaa_Input = pnd_Bad_Output.newGroupInGroup("pnd_Bad_Output_Pnd_Tiaa_Input", "#TIAA-INPUT");
        pnd_Bad_Output_Pnd_Bad_Addr_Name = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Name", "#BAD-ADDR-NAME", FieldType.STRING, 
            35);
        pnd_Bad_Output_Pnd_Bad_Addr_Line_1 = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Line_1", "#BAD-ADDR-LINE-1", FieldType.STRING, 
            35);
        pnd_Bad_Output_Pnd_Bad_Addr_Line_2 = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Line_2", "#BAD-ADDR-LINE-2", FieldType.STRING, 
            35);
        pnd_Bad_Output_Pnd_Bad_Addr_Line_3 = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Line_3", "#BAD-ADDR-LINE-3", FieldType.STRING, 
            35);
        pnd_Bad_Output_Pnd_Bad_Addr_Line_4 = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Line_4", "#BAD-ADDR-LINE-4", FieldType.STRING, 
            35);
        pnd_Bad_Output_Pnd_Bad_Addr_City_State = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_City_State", "#BAD-ADDR-CITY-STATE", 
            FieldType.STRING, 35);
        pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4 = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4", "#BAD-ADDR-ZIP-PLUS-4", 
            FieldType.STRING, 9);
        pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef1 = pnd_Bad_Output_Pnd_Tiaa_Input.newGroupInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef1", "Redefines", 
            pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4);
        pnd_Bad_Output_Pnd_Bad_Addr_Zip_5 = pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef1.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Zip_5", "#BAD-ADDR-ZIP-5", 
            FieldType.STRING, 5);
        pnd_Bad_Output_Pnd_Bad_Addr_Zip_4 = pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef1.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Zip_4", "#BAD-ADDR-ZIP-4", 
            FieldType.STRING, 4);
        pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef2 = pnd_Bad_Output_Pnd_Tiaa_Input.newGroupInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef2", "Redefines", 
            pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4);
        pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4_Num = pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4Redef2.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4_Num", 
            "#BAD-ADDR-ZIP-PLUS-4-NUM", FieldType.NUMERIC, 9);
        pnd_Bad_Output_Pnd_Filler = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Nmbr = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Nmbr", "#BAD-ADDR-CNTRCT-NMBR", 
            FieldType.STRING, 10);
        pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_Cde = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_Cde", "#BAD-ADDR-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_CdeRedef3 = pnd_Bad_Output_Pnd_Tiaa_Input.newGroupInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_CdeRedef3", 
            "Redefines", pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_Cde);
        pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_Cde_N = pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_CdeRedef3.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Cntrct_Payee_Cde_N", 
            "#BAD-ADDR-CNTRCT-PAYEE-CDE-N", FieldType.NUMERIC, 2);
        pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_Nmbr = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_Nmbr", "#BAD-ADDR-PH-UNQUE-ID-NMBR", 
            FieldType.STRING, 7);
        pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_NmbrRedef4 = pnd_Bad_Output_Pnd_Tiaa_Input.newGroupInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_NmbrRedef4", 
            "Redefines", pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_Nmbr);
        pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_Numeric = pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_NmbrRedef4.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Ph_Unque_Id_Numeric", 
            "#BAD-ADDR-PH-UNQUE-ID-NUMERIC", FieldType.NUMERIC, 7);
        pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_X = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_X", "#BAD-ADDR-SYS-RUN-DATE-X", 
            FieldType.STRING, 8);
        pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_XRedef5 = pnd_Bad_Output_Pnd_Tiaa_Input.newGroupInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_XRedef5", 
            "Redefines", pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_X);
        pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_Numeric = pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_XRedef5.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Sys_Run_Date_Numeric", 
            "#BAD-ADDR-SYS-RUN-DATE-NUMERIC", FieldType.NUMERIC, 8);
        pnd_Bad_Output_Pnd_Bad_Addr_Pin_Base_Addr_Ind = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Pin_Base_Addr_Ind", 
            "#BAD-ADDR-PIN-BASE-ADDR-IND", FieldType.STRING, 1);
        pnd_Bad_Output_Pnd_Bad_Addr_Ssn = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Ssn", "#BAD-ADDR-SSN", FieldType.STRING, 
            9);
        pnd_Bad_Output_Pnd_Bad_Addr_SsnRedef6 = pnd_Bad_Output_Pnd_Tiaa_Input.newGroupInGroup("pnd_Bad_Output_Pnd_Bad_Addr_SsnRedef6", "Redefines", pnd_Bad_Output_Pnd_Bad_Addr_Ssn);
        pnd_Bad_Output_Pnd_Bad_Addr_Ssn_Numeric = pnd_Bad_Output_Pnd_Bad_Addr_SsnRedef6.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Ssn_Numeric", "#BAD-ADDR-SSN-NUMERIC", 
            FieldType.NUMERIC, 9);
        pnd_Bad_Output_Pnd_Bad_Addr_Dob = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Dob", "#BAD-ADDR-DOB", FieldType.STRING, 
            8);
        pnd_Bad_Output_Pnd_Bad_Addr_Vip_High_Accum_Ind = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Vip_High_Accum_Ind", 
            "#BAD-ADDR-VIP-HIGH-ACCUM-IND", FieldType.STRING, 2);
        pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt_A = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt_A", "#BAD-ADDR-REC-CNT-A", 
            FieldType.STRING, 9);
        pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt_ARedef7 = pnd_Bad_Output_Pnd_Tiaa_Input.newGroupInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt_ARedef7", "Redefines", 
            pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt_A);
        pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt = pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt_ARedef7.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt", "#BAD-ADDR-REC-CNT", 
            FieldType.NUMERIC, 9);
        pnd_Bad_Output_Pnd_Bad_Addr_Filler = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Filler", "#BAD-ADDR-FILLER", FieldType.STRING, 
            32);
        pnd_Bad_Output_Pnd_Bad_Addr_Pin_A12 = pnd_Bad_Output_Pnd_Tiaa_Input.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Pin_A12", "#BAD-ADDR-PIN-A12", 
            FieldType.STRING, 12);
        pnd_Bad_Output_Pnd_Bad_Addr_Pin_A12Redef8 = pnd_Bad_Output_Pnd_Tiaa_Input.newGroupInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Pin_A12Redef8", "Redefines", 
            pnd_Bad_Output_Pnd_Bad_Addr_Pin_A12);
        pnd_Bad_Output_Pnd_Bad_Addr_Pin_N12 = pnd_Bad_Output_Pnd_Bad_Addr_Pin_A12Redef8.newFieldInGroup("pnd_Bad_Output_Pnd_Bad_Addr_Pin_N12", "#BAD-ADDR-PIN-N12", 
            FieldType.NUMERIC, 12);
        pnd_Bad_Output_Pnd_Vendor_Info = pnd_Bad_Output.newGroupInGroup("pnd_Bad_Output_Pnd_Vendor_Info", "#VENDOR-INFO");
        pnd_Bad_Output_Pnd_Vendor_Unknown = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Unknown", "#VENDOR-UNKNOWN", FieldType.STRING, 
            9);
        pnd_Bad_Output_Pnd_Vendor_Deceased_Info = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Deceased_Info", "#VENDOR-DECEASED-INFO", 
            FieldType.STRING, 8);
        pnd_Bad_Output_Pnd_Vendor_First_Name = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_First_Name", "#VENDOR-FIRST-NAME", 
            FieldType.STRING, 30);
        pnd_Bad_Output_Pnd_Vendor_Middle_Init = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Middle_Init", "#VENDOR-MIDDLE-INIT", 
            FieldType.STRING, 1);
        pnd_Bad_Output_Pnd_Vendor_Last_Name = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Last_Name", "#VENDOR-LAST-NAME", 
            FieldType.STRING, 30);
        pnd_Bad_Output_Pnd_Vendor_Suffix = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Suffix", "#VENDOR-SUFFIX", FieldType.STRING, 
            5);
        pnd_Bad_Output_Pnd_Vendor_Street_Addr = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Street_Addr", "#VENDOR-STREET-ADDR", 
            FieldType.STRING, 50);
        pnd_Bad_Output_Pnd_Vendor_City = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_City", "#VENDOR-CITY", FieldType.STRING, 
            28);
        pnd_Bad_Output_Pnd_Vendor_State = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_State", "#VENDOR-STATE", FieldType.STRING, 
            2);
        pnd_Bad_Output_Pnd_Vendor_Zip_Code = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Zip_Code", "#VENDOR-ZIP-CODE", 
            FieldType.STRING, 10);
        pnd_Bad_Output_Pnd_Vendor_Zip_CodeRedef9 = pnd_Bad_Output_Pnd_Vendor_Info.newGroupInGroup("pnd_Bad_Output_Pnd_Vendor_Zip_CodeRedef9", "Redefines", 
            pnd_Bad_Output_Pnd_Vendor_Zip_Code);
        pnd_Bad_Output_Pnd_Vendor_Zip_5 = pnd_Bad_Output_Pnd_Vendor_Zip_CodeRedef9.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Zip_5", "#VENDOR-ZIP-5", 
            FieldType.STRING, 5);
        pnd_Bad_Output_Pnd_Vendor_Zip_Filler = pnd_Bad_Output_Pnd_Vendor_Zip_CodeRedef9.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Zip_Filler", "#VENDOR-ZIP-FILLER", 
            FieldType.STRING, 1);
        pnd_Bad_Output_Pnd_Vendor_Zip_Plus_4 = pnd_Bad_Output_Pnd_Vendor_Zip_CodeRedef9.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Zip_Plus_4", "#VENDOR-ZIP-PLUS-4", 
            FieldType.STRING, 4);
        pnd_Bad_Output_Pnd_Vendor_Phone_Num = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Phone_Num", "#VENDOR-PHONE-NUM", 
            FieldType.STRING, 10);
        pnd_Bad_Output_Pnd_Vendor_Dual_Anme = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Dual_Anme", "#VENDOR-DUAL-ANME", 
            FieldType.STRING, 30);
        pnd_Bad_Output_Pnd_Vendor_First_Date = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_First_Date", "#VENDOR-FIRST-DATE", 
            FieldType.STRING, 8);
        pnd_Bad_Output_Pnd_Vendor_Last_Date = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_Last_Date", "#VENDOR-LAST-DATE", 
            FieldType.STRING, 8);
        pnd_Bad_Output_Pnd_Vendor_New_Line = pnd_Bad_Output_Pnd_Vendor_Info.newFieldInGroup("pnd_Bad_Output_Pnd_Vendor_New_Line", "#VENDOR-NEW-LINE", 
            FieldType.STRING, 2);
        pnd_Bad_Output_Pnd_Tiaa_Output = pnd_Bad_Output.newGroupInGroup("pnd_Bad_Output_Pnd_Tiaa_Output", "#TIAA-OUTPUT");
        pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Sort_Key = pnd_Bad_Output_Pnd_Tiaa_Output.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Sort_Key", "#TIAA-STD-ADDR-SORT-KEY", 
            FieldType.STRING, 42);
        pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1 = pnd_Bad_Output_Pnd_Tiaa_Output.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1", "#TIAA-STD-ADDR-LINE1", 
            FieldType.STRING, 35);
        pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1Redef10 = pnd_Bad_Output_Pnd_Tiaa_Output.newGroupInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1Redef10", 
            "Redefines", pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1);
        pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1_A30 = pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1Redef10.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1_A30", 
            "#TIAA-STD-ADDR-LINE1-A30", FieldType.STRING, 30);
        pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1_Filler = pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1Redef10.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1_Filler", 
            "#TIAA-STD-ADDR-LINE1-FILLER", FieldType.STRING, 5);
        pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line2 = pnd_Bad_Output_Pnd_Tiaa_Output.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line2", "#TIAA-STD-ADDR-LINE2", 
            FieldType.STRING, 35);
        pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line3 = pnd_Bad_Output_Pnd_Tiaa_Output.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line3", "#TIAA-STD-ADDR-LINE3", 
            FieldType.STRING, 35);
        pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line4 = pnd_Bad_Output_Pnd_Tiaa_Output.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line4", "#TIAA-STD-ADDR-LINE4", 
            FieldType.STRING, 35);
        pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line5 = pnd_Bad_Output_Pnd_Tiaa_Output.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line5", "#TIAA-STD-ADDR-LINE5", 
            FieldType.STRING, 35);
        pnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4 = pnd_Bad_Output_Pnd_Tiaa_Output.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4", "#TIAA-STD-ZIP-PLUS4", 
            FieldType.STRING, 9);
        pnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4Redef11 = pnd_Bad_Output_Pnd_Tiaa_Output.newGroupInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4Redef11", 
            "Redefines", pnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4);
        pnd_Bad_Output_Pnd_Tiaa_Std_Zip = pnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4Redef11.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Zip", "#TIAA-STD-ZIP", 
            FieldType.STRING, 5);
        pnd_Bad_Output_Pnd_Tiaa_Std_Zip_4 = pnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4Redef11.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Zip_4", "#TIAA-STD-ZIP-4", 
            FieldType.STRING, 4);
        pnd_Bad_Output_Pnd_Tiaa_Std_Carrier_Rte = pnd_Bad_Output_Pnd_Tiaa_Output.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Carrier_Rte", "#TIAA-STD-CARRIER-RTE", 
            FieldType.STRING, 4);
        pnd_Bad_Output_Pnd_Tiaa_Std_Walk_Seq = pnd_Bad_Output_Pnd_Tiaa_Output.newFieldInGroup("pnd_Bad_Output_Pnd_Tiaa_Std_Walk_Seq", "#TIAA-STD-WALK-SEQ", 
            FieldType.STRING, 4);

        this.setRecordName("LdaNasl747a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaNasl747a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
