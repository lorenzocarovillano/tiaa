/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:21:49 PM
**        * FROM NATURAL PDA     : MDMA361
************************************************************
**        * FILE NAME            : PdaMdma361.java
**        * CLASS NAME           : PdaMdma361
**        * INSTANCE NAME        : PdaMdma361
************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaMdma361 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Mdma361;
    private DbsField pnd_Mdma361_Pnd_I_Section;
    private DbsGroup pnd_Mdma361_Pnd_I_SectionRedef1;
    private DbsField pnd_Mdma361_Pnd_I_Ssn_A9;
    private DbsGroup pnd_Mdma361_Pnd_I_Ssn_A9Redef2;
    private DbsField pnd_Mdma361_Pnd_I_Ssn;
    private DbsField pnd_Mdma361_Pnd_I_Pin_A7;
    private DbsGroup pnd_Mdma361_Pnd_I_Pin_A7Redef3;
    private DbsField pnd_Mdma361_Pnd_I_Pin;
    private DbsField pnd_Mdma361_Pnd_I_Xref_Ind;
    private DbsField pnd_Mdma361_Pnd_I_Party_Fltr;
    private DbsField pnd_Mdma361_Pnd_I_Party_Inq_Lvl;
    private DbsField pnd_Mdma361_Pnd_I_Privpref_Fltr;
    private DbsField pnd_Mdma361_Pnd_I_Address_Fltr;
    private DbsField pnd_Mdma361_Pnd_I_Pin_A12;
    private DbsGroup pnd_Mdma361_Pnd_I_Pin_A12Redef4;
    private DbsField pnd_Mdma361_Pnd_I_Pin_N12;
    private DbsGroup pnd_Mdma361_Pnd_O_Return_Section;
    private DbsField pnd_Mdma361_Pnd_O_Return_Code;
    private DbsField pnd_Mdma361_Pnd_O_Return_Text;
    private DbsField pnd_Mdma361_Pnd_O_Data_Section;
    private DbsGroup pnd_Mdma361_Pnd_O_Data_SectionRedef5;
    private DbsGroup pnd_Mdma361_Pnd_O_Pda_Data;
    private DbsField pnd_Mdma361_Pnd_O_Ssn;
    private DbsField pnd_Mdma361_Pnd_O_Pin;
    private DbsField pnd_Mdma361_Pnd_O_Preferred_Last_Name;
    private DbsField pnd_Mdma361_Pnd_O_Preferred_First_Name;
    private DbsField pnd_Mdma361_Pnd_O_Preferred_Middle_Name;
    private DbsField pnd_Mdma361_Pnd_O_Preferred_Prefix;
    private DbsField pnd_Mdma361_Pnd_O_Preferred_Suffix;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Permanent_Ind;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Type_Code;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_1;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_2;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_3;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_4;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_City;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_St_Prov;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Zip_Code;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Country;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Postal_Data;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Geographic_Code;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Status_Code;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Status_Date;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Override_Code;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Last_Update_Date;
    private DbsField pnd_Mdma361_Pnd_O_Actv_Res_Adr_Last_Update_Time;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Start_Date;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Type_Code;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_1;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_2;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_3;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_4;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_City;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_St_Prov;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Zip_Code;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Country;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Postal_Data;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Geographic_Code;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Status_Code;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Status_Date;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Override_Code;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Date;
    private DbsField pnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Time;
    private DbsField pnd_Mdma361_Pnd_O_Pin_N12;

    public DbsGroup getPnd_Mdma361() { return pnd_Mdma361; }

    public DbsField getPnd_Mdma361_Pnd_I_Section() { return pnd_Mdma361_Pnd_I_Section; }

    public DbsGroup getPnd_Mdma361_Pnd_I_SectionRedef1() { return pnd_Mdma361_Pnd_I_SectionRedef1; }

    public DbsField getPnd_Mdma361_Pnd_I_Ssn_A9() { return pnd_Mdma361_Pnd_I_Ssn_A9; }

    public DbsGroup getPnd_Mdma361_Pnd_I_Ssn_A9Redef2() { return pnd_Mdma361_Pnd_I_Ssn_A9Redef2; }

    public DbsField getPnd_Mdma361_Pnd_I_Ssn() { return pnd_Mdma361_Pnd_I_Ssn; }

    public DbsField getPnd_Mdma361_Pnd_I_Pin_A7() { return pnd_Mdma361_Pnd_I_Pin_A7; }

    public DbsGroup getPnd_Mdma361_Pnd_I_Pin_A7Redef3() { return pnd_Mdma361_Pnd_I_Pin_A7Redef3; }

    public DbsField getPnd_Mdma361_Pnd_I_Pin() { return pnd_Mdma361_Pnd_I_Pin; }

    public DbsField getPnd_Mdma361_Pnd_I_Xref_Ind() { return pnd_Mdma361_Pnd_I_Xref_Ind; }

    public DbsField getPnd_Mdma361_Pnd_I_Party_Fltr() { return pnd_Mdma361_Pnd_I_Party_Fltr; }

    public DbsField getPnd_Mdma361_Pnd_I_Party_Inq_Lvl() { return pnd_Mdma361_Pnd_I_Party_Inq_Lvl; }

    public DbsField getPnd_Mdma361_Pnd_I_Privpref_Fltr() { return pnd_Mdma361_Pnd_I_Privpref_Fltr; }

    public DbsField getPnd_Mdma361_Pnd_I_Address_Fltr() { return pnd_Mdma361_Pnd_I_Address_Fltr; }

    public DbsField getPnd_Mdma361_Pnd_I_Pin_A12() { return pnd_Mdma361_Pnd_I_Pin_A12; }

    public DbsGroup getPnd_Mdma361_Pnd_I_Pin_A12Redef4() { return pnd_Mdma361_Pnd_I_Pin_A12Redef4; }

    public DbsField getPnd_Mdma361_Pnd_I_Pin_N12() { return pnd_Mdma361_Pnd_I_Pin_N12; }

    public DbsGroup getPnd_Mdma361_Pnd_O_Return_Section() { return pnd_Mdma361_Pnd_O_Return_Section; }

    public DbsField getPnd_Mdma361_Pnd_O_Return_Code() { return pnd_Mdma361_Pnd_O_Return_Code; }

    public DbsField getPnd_Mdma361_Pnd_O_Return_Text() { return pnd_Mdma361_Pnd_O_Return_Text; }

    public DbsField getPnd_Mdma361_Pnd_O_Data_Section() { return pnd_Mdma361_Pnd_O_Data_Section; }

    public DbsGroup getPnd_Mdma361_Pnd_O_Data_SectionRedef5() { return pnd_Mdma361_Pnd_O_Data_SectionRedef5; }

    public DbsGroup getPnd_Mdma361_Pnd_O_Pda_Data() { return pnd_Mdma361_Pnd_O_Pda_Data; }

    public DbsField getPnd_Mdma361_Pnd_O_Ssn() { return pnd_Mdma361_Pnd_O_Ssn; }

    public DbsField getPnd_Mdma361_Pnd_O_Pin() { return pnd_Mdma361_Pnd_O_Pin; }

    public DbsField getPnd_Mdma361_Pnd_O_Preferred_Last_Name() { return pnd_Mdma361_Pnd_O_Preferred_Last_Name; }

    public DbsField getPnd_Mdma361_Pnd_O_Preferred_First_Name() { return pnd_Mdma361_Pnd_O_Preferred_First_Name; }

    public DbsField getPnd_Mdma361_Pnd_O_Preferred_Middle_Name() { return pnd_Mdma361_Pnd_O_Preferred_Middle_Name; }

    public DbsField getPnd_Mdma361_Pnd_O_Preferred_Prefix() { return pnd_Mdma361_Pnd_O_Preferred_Prefix; }

    public DbsField getPnd_Mdma361_Pnd_O_Preferred_Suffix() { return pnd_Mdma361_Pnd_O_Preferred_Suffix; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Permanent_Ind() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Permanent_Ind; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Type_Code() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Type_Code; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_1() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_1; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_2() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_2; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_3() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_3; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_4() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_4; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_City() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_City; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_St_Prov() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_St_Prov; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Zip_Code() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Zip_Code; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Country() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Country; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Postal_Data() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Postal_Data; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Geographic_Code() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Geographic_Code; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Status_Code() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Status_Code; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Status_Date() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Status_Date; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Override_Code() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Override_Code; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Last_Update_Date() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Last_Update_Date; }

    public DbsField getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Last_Update_Time() { return pnd_Mdma361_Pnd_O_Actv_Res_Adr_Last_Update_Time; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Start_Date() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Start_Date; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Type_Code() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Type_Code; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_1() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_1; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_2() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_2; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_3() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_3; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_4() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_4; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_City() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_City; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_St_Prov() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_St_Prov; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Zip_Code() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Zip_Code; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Country() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Country; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Postal_Data() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Postal_Data; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Geographic_Code() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Geographic_Code; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Status_Code() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Status_Code; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Status_Date() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Status_Date; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Override_Code() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Override_Code; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Date() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Date; }

    public DbsField getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Time() { return pnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Time; }

    public DbsField getPnd_Mdma361_Pnd_O_Pin_N12() { return pnd_Mdma361_Pnd_O_Pin_N12; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Mdma361 = dbsRecord.newGroupInRecord("pnd_Mdma361", "#MDMA361");
        pnd_Mdma361.setParameterOption(ParameterOption.ByReference);
        pnd_Mdma361_Pnd_I_Section = pnd_Mdma361.newFieldArrayInGroup("pnd_Mdma361_Pnd_I_Section", "#I-SECTION", FieldType.STRING, 1, new DbsArrayController(1,
            72));
        pnd_Mdma361_Pnd_I_SectionRedef1 = pnd_Mdma361.newGroupInGroup("pnd_Mdma361_Pnd_I_SectionRedef1", "Redefines", pnd_Mdma361_Pnd_I_Section);
        pnd_Mdma361_Pnd_I_Ssn_A9 = pnd_Mdma361_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma361_Pnd_I_Ssn_A9", "#I-SSN-A9", FieldType.STRING, 9);
        pnd_Mdma361_Pnd_I_Ssn_A9Redef2 = pnd_Mdma361_Pnd_I_SectionRedef1.newGroupInGroup("pnd_Mdma361_Pnd_I_Ssn_A9Redef2", "Redefines", pnd_Mdma361_Pnd_I_Ssn_A9);
        pnd_Mdma361_Pnd_I_Ssn = pnd_Mdma361_Pnd_I_Ssn_A9Redef2.newFieldInGroup("pnd_Mdma361_Pnd_I_Ssn", "#I-SSN", FieldType.NUMERIC, 9);
        pnd_Mdma361_Pnd_I_Pin_A7 = pnd_Mdma361_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma361_Pnd_I_Pin_A7", "#I-PIN-A7", FieldType.STRING, 7);
        pnd_Mdma361_Pnd_I_Pin_A7Redef3 = pnd_Mdma361_Pnd_I_SectionRedef1.newGroupInGroup("pnd_Mdma361_Pnd_I_Pin_A7Redef3", "Redefines", pnd_Mdma361_Pnd_I_Pin_A7);
        pnd_Mdma361_Pnd_I_Pin = pnd_Mdma361_Pnd_I_Pin_A7Redef3.newFieldInGroup("pnd_Mdma361_Pnd_I_Pin", "#I-PIN", FieldType.NUMERIC, 7);
        pnd_Mdma361_Pnd_I_Xref_Ind = pnd_Mdma361_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma361_Pnd_I_Xref_Ind", "#I-XREF-IND", FieldType.STRING, 1);
        pnd_Mdma361_Pnd_I_Party_Fltr = pnd_Mdma361_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma361_Pnd_I_Party_Fltr", "#I-PARTY-FLTR", FieldType.STRING, 
            8);
        pnd_Mdma361_Pnd_I_Party_Inq_Lvl = pnd_Mdma361_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma361_Pnd_I_Party_Inq_Lvl", "#I-PARTY-INQ-LVL", FieldType.NUMERIC, 
            3);
        pnd_Mdma361_Pnd_I_Privpref_Fltr = pnd_Mdma361_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma361_Pnd_I_Privpref_Fltr", "#I-PRIVPREF-FLTR", FieldType.STRING, 
            23);
        pnd_Mdma361_Pnd_I_Address_Fltr = pnd_Mdma361_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma361_Pnd_I_Address_Fltr", "#I-ADDRESS-FLTR", FieldType.STRING, 
            9);
        pnd_Mdma361_Pnd_I_Pin_A12 = pnd_Mdma361_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma361_Pnd_I_Pin_A12", "#I-PIN-A12", FieldType.STRING, 12);
        pnd_Mdma361_Pnd_I_Pin_A12Redef4 = pnd_Mdma361_Pnd_I_SectionRedef1.newGroupInGroup("pnd_Mdma361_Pnd_I_Pin_A12Redef4", "Redefines", pnd_Mdma361_Pnd_I_Pin_A12);
        pnd_Mdma361_Pnd_I_Pin_N12 = pnd_Mdma361_Pnd_I_Pin_A12Redef4.newFieldInGroup("pnd_Mdma361_Pnd_I_Pin_N12", "#I-PIN-N12", FieldType.NUMERIC, 12);
        pnd_Mdma361_Pnd_O_Return_Section = pnd_Mdma361.newGroupInGroup("pnd_Mdma361_Pnd_O_Return_Section", "#O-RETURN-SECTION");
        pnd_Mdma361_Pnd_O_Return_Code = pnd_Mdma361_Pnd_O_Return_Section.newFieldInGroup("pnd_Mdma361_Pnd_O_Return_Code", "#O-RETURN-CODE", FieldType.STRING, 
            4);
        pnd_Mdma361_Pnd_O_Return_Text = pnd_Mdma361_Pnd_O_Return_Section.newFieldInGroup("pnd_Mdma361_Pnd_O_Return_Text", "#O-RETURN-TEXT", FieldType.STRING, 
            80);
        pnd_Mdma361_Pnd_O_Data_Section = pnd_Mdma361.newFieldArrayInGroup("pnd_Mdma361_Pnd_O_Data_Section", "#O-DATA-SECTION", FieldType.STRING, 1, new 
            DbsArrayController(1,692));
        pnd_Mdma361_Pnd_O_Data_SectionRedef5 = pnd_Mdma361.newGroupInGroup("pnd_Mdma361_Pnd_O_Data_SectionRedef5", "Redefines", pnd_Mdma361_Pnd_O_Data_Section);
        pnd_Mdma361_Pnd_O_Pda_Data = pnd_Mdma361_Pnd_O_Data_SectionRedef5.newGroupInGroup("pnd_Mdma361_Pnd_O_Pda_Data", "#O-PDA-DATA");
        pnd_Mdma361_Pnd_O_Ssn = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Ssn", "#O-SSN", FieldType.NUMERIC, 9);
        pnd_Mdma361_Pnd_O_Pin = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Pin", "#O-PIN", FieldType.NUMERIC, 7);
        pnd_Mdma361_Pnd_O_Preferred_Last_Name = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Preferred_Last_Name", "#O-PREFERRED-LAST-NAME", 
            FieldType.STRING, 35);
        pnd_Mdma361_Pnd_O_Preferred_First_Name = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Preferred_First_Name", "#O-PREFERRED-FIRST-NAME", 
            FieldType.STRING, 30);
        pnd_Mdma361_Pnd_O_Preferred_Middle_Name = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Preferred_Middle_Name", "#O-PREFERRED-MIDDLE-NAME", 
            FieldType.STRING, 30);
        pnd_Mdma361_Pnd_O_Preferred_Prefix = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Preferred_Prefix", "#O-PREFERRED-PREFIX", FieldType.STRING, 
            8);
        pnd_Mdma361_Pnd_O_Preferred_Suffix = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Preferred_Suffix", "#O-PREFERRED-SUFFIX", FieldType.STRING, 
            8);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Permanent_Ind = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Permanent_Ind", "#O-ACTV-RES-ADR-PERMANENT-IND", 
            FieldType.STRING, 1);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Type_Code = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Type_Code", "#O-ACTV-RES-ADR-TYPE-CODE", 
            FieldType.STRING, 1);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_1 = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_1", "#O-ACTV-RES-ADR-LINE-1", 
            FieldType.STRING, 35);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_2 = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_2", "#O-ACTV-RES-ADR-LINE-2", 
            FieldType.STRING, 35);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_3 = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_3", "#O-ACTV-RES-ADR-LINE-3", 
            FieldType.STRING, 35);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_4 = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_4", "#O-ACTV-RES-ADR-LINE-4", 
            FieldType.STRING, 35);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_City = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_City", "#O-ACTV-RES-ADR-CITY", 
            FieldType.STRING, 25);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_St_Prov = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_St_Prov", "#O-ACTV-RES-ADR-ST-PROV", 
            FieldType.STRING, 2);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Zip_Code = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Zip_Code", "#O-ACTV-RES-ADR-ZIP-CODE", 
            FieldType.STRING, 10);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Country = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Country", "#O-ACTV-RES-ADR-COUNTRY", 
            FieldType.STRING, 35);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Postal_Data = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Postal_Data", "#O-ACTV-RES-ADR-POSTAL-DATA", 
            FieldType.STRING, 32);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Geographic_Code = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Geographic_Code", 
            "#O-ACTV-RES-ADR-GEOGRAPHIC-CODE", FieldType.STRING, 2);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Status_Code = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Status_Code", "#O-ACTV-RES-ADR-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Status_Date = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Status_Date", "#O-ACTV-RES-ADR-STATUS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Override_Code = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Override_Code", "#O-ACTV-RES-ADR-OVERRIDE-CODE", 
            FieldType.STRING, 1);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Last_Update_Date = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Last_Update_Date", 
            "#O-ACTV-RES-ADR-LAST-UPDATE-DATE", FieldType.NUMERIC, 8);
        pnd_Mdma361_Pnd_O_Actv_Res_Adr_Last_Update_Time = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Actv_Res_Adr_Last_Update_Time", 
            "#O-ACTV-RES-ADR-LAST-UPDATE-TIME", FieldType.NUMERIC, 7);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Start_Date = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Start_Date", "#O-FUTR-RES-ADR-START-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Type_Code = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Type_Code", "#O-FUTR-RES-ADR-TYPE-CODE", 
            FieldType.STRING, 1);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_1 = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_1", "#O-FUTR-RES-ADR-LINE-1", 
            FieldType.STRING, 35);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_2 = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_2", "#O-FUTR-RES-ADR-LINE-2", 
            FieldType.STRING, 35);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_3 = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_3", "#O-FUTR-RES-ADR-LINE-3", 
            FieldType.STRING, 35);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_4 = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_4", "#O-FUTR-RES-ADR-LINE-4", 
            FieldType.STRING, 35);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_City = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_City", "#O-FUTR-RES-ADR-CITY", 
            FieldType.STRING, 25);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_St_Prov = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_St_Prov", "#O-FUTR-RES-ADR-ST-PROV", 
            FieldType.STRING, 2);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Zip_Code = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Zip_Code", "#O-FUTR-RES-ADR-ZIP-CODE", 
            FieldType.STRING, 10);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Country = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Country", "#O-FUTR-RES-ADR-COUNTRY", 
            FieldType.STRING, 35);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Postal_Data = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Postal_Data", "#O-FUTR-RES-ADR-POSTAL-DATA", 
            FieldType.STRING, 32);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Geographic_Code = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Geographic_Code", 
            "#O-FUTR-RES-ADR-GEOGRAPHIC-CODE", FieldType.STRING, 2);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Status_Code = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Status_Code", "#O-FUTR-RES-ADR-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Status_Date = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Status_Date", "#O-FUTR-RES-ADR-STATUS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Override_Code = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Override_Code", "#O-FUTR-RES-ADR-OVERRIDE-CODE", 
            FieldType.STRING, 1);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Date = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Date", 
            "#O-FUTR-RES-ADR-LAST-UPDATE-DATE", FieldType.NUMERIC, 8);
        pnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Time = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Time", 
            "#O-FUTR-RES-ADR-LAST-UPDATE-TIME", FieldType.NUMERIC, 7);
        pnd_Mdma361_Pnd_O_Pin_N12 = pnd_Mdma361_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma361_Pnd_O_Pin_N12", "#O-PIN-N12", FieldType.NUMERIC, 12);

        dbsRecord.reset();
    }

    // Constructors
    public PdaMdma361(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

