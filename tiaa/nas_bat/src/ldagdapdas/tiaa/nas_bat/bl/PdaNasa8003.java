/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:21:57 PM
**        * FROM NATURAL PDA     : NASA8003
************************************************************
**        * FILE NAME            : PdaNasa8003.java
**        * CLASS NAME           : PdaNasa8003
**        * INSTANCE NAME        : PdaNasa8003
************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaNasa8003 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Nasa8003;
    private DbsField pnd_Nasa8003_Pnd_Action_Cde;
    private DbsField pnd_Nasa8003_Pnd_Unit_Code_Input;
    private DbsGroup pnd_Nasa8003_Pnd_Unit_Code_InputRedef1;
    private DbsField pnd_Nasa8003_Pnd_Unit_Code;
    private DbsField pnd_Nasa8003_Pnd_Region;
    private DbsField pnd_Nasa8003_Pnd_Spcl_Designation;
    private DbsField pnd_Nasa8003_Pnd_Unit_Code_Return;
    private DbsField pnd_Nasa8003_Pnd_Unit_Desc;
    private DbsField pnd_Nasa8003_Pnd_Maintenance_Sw;
    private DbsField pnd_Nasa8003_Pnd_Research_Sw;
    private DbsField pnd_Nasa8003_Pnd_Ia_Work_Sw;
    private DbsField pnd_Nasa8003_Pnd_Verification_Sw;
    private DbsField pnd_Nasa8003_Pnd_Generic_Sw;
    private DbsField pnd_Nasa8003_Pnd_Msg;

    public DbsGroup getPnd_Nasa8003() { return pnd_Nasa8003; }

    public DbsField getPnd_Nasa8003_Pnd_Action_Cde() { return pnd_Nasa8003_Pnd_Action_Cde; }

    public DbsField getPnd_Nasa8003_Pnd_Unit_Code_Input() { return pnd_Nasa8003_Pnd_Unit_Code_Input; }

    public DbsGroup getPnd_Nasa8003_Pnd_Unit_Code_InputRedef1() { return pnd_Nasa8003_Pnd_Unit_Code_InputRedef1; }

    public DbsField getPnd_Nasa8003_Pnd_Unit_Code() { return pnd_Nasa8003_Pnd_Unit_Code; }

    public DbsField getPnd_Nasa8003_Pnd_Region() { return pnd_Nasa8003_Pnd_Region; }

    public DbsField getPnd_Nasa8003_Pnd_Spcl_Designation() { return pnd_Nasa8003_Pnd_Spcl_Designation; }

    public DbsField getPnd_Nasa8003_Pnd_Unit_Code_Return() { return pnd_Nasa8003_Pnd_Unit_Code_Return; }

    public DbsField getPnd_Nasa8003_Pnd_Unit_Desc() { return pnd_Nasa8003_Pnd_Unit_Desc; }

    public DbsField getPnd_Nasa8003_Pnd_Maintenance_Sw() { return pnd_Nasa8003_Pnd_Maintenance_Sw; }

    public DbsField getPnd_Nasa8003_Pnd_Research_Sw() { return pnd_Nasa8003_Pnd_Research_Sw; }

    public DbsField getPnd_Nasa8003_Pnd_Ia_Work_Sw() { return pnd_Nasa8003_Pnd_Ia_Work_Sw; }

    public DbsField getPnd_Nasa8003_Pnd_Verification_Sw() { return pnd_Nasa8003_Pnd_Verification_Sw; }

    public DbsField getPnd_Nasa8003_Pnd_Generic_Sw() { return pnd_Nasa8003_Pnd_Generic_Sw; }

    public DbsField getPnd_Nasa8003_Pnd_Msg() { return pnd_Nasa8003_Pnd_Msg; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Nasa8003 = dbsRecord.newGroupInRecord("pnd_Nasa8003", "#NASA8003");
        pnd_Nasa8003.setParameterOption(ParameterOption.ByReference);
        pnd_Nasa8003_Pnd_Action_Cde = pnd_Nasa8003.newFieldInGroup("pnd_Nasa8003_Pnd_Action_Cde", "#ACTION-CDE", FieldType.STRING, 1);
        pnd_Nasa8003_Pnd_Unit_Code_Input = pnd_Nasa8003.newFieldInGroup("pnd_Nasa8003_Pnd_Unit_Code_Input", "#UNIT-CODE-INPUT", FieldType.STRING, 7);
        pnd_Nasa8003_Pnd_Unit_Code_InputRedef1 = pnd_Nasa8003.newGroupInGroup("pnd_Nasa8003_Pnd_Unit_Code_InputRedef1", "Redefines", pnd_Nasa8003_Pnd_Unit_Code_Input);
        pnd_Nasa8003_Pnd_Unit_Code = pnd_Nasa8003_Pnd_Unit_Code_InputRedef1.newFieldInGroup("pnd_Nasa8003_Pnd_Unit_Code", "#UNIT-CODE", FieldType.STRING, 
            5);
        pnd_Nasa8003_Pnd_Region = pnd_Nasa8003_Pnd_Unit_Code_InputRedef1.newFieldInGroup("pnd_Nasa8003_Pnd_Region", "#REGION", FieldType.STRING, 1);
        pnd_Nasa8003_Pnd_Spcl_Designation = pnd_Nasa8003_Pnd_Unit_Code_InputRedef1.newFieldInGroup("pnd_Nasa8003_Pnd_Spcl_Designation", "#SPCL-DESIGNATION", 
            FieldType.STRING, 1);
        pnd_Nasa8003_Pnd_Unit_Code_Return = pnd_Nasa8003.newFieldInGroup("pnd_Nasa8003_Pnd_Unit_Code_Return", "#UNIT-CODE-RETURN", FieldType.STRING, 7);
        pnd_Nasa8003_Pnd_Unit_Desc = pnd_Nasa8003.newFieldInGroup("pnd_Nasa8003_Pnd_Unit_Desc", "#UNIT-DESC", FieldType.STRING, 50);
        pnd_Nasa8003_Pnd_Maintenance_Sw = pnd_Nasa8003.newFieldInGroup("pnd_Nasa8003_Pnd_Maintenance_Sw", "#MAINTENANCE-SW", FieldType.STRING, 1);
        pnd_Nasa8003_Pnd_Research_Sw = pnd_Nasa8003.newFieldInGroup("pnd_Nasa8003_Pnd_Research_Sw", "#RESEARCH-SW", FieldType.STRING, 1);
        pnd_Nasa8003_Pnd_Ia_Work_Sw = pnd_Nasa8003.newFieldInGroup("pnd_Nasa8003_Pnd_Ia_Work_Sw", "#IA-WORK-SW", FieldType.STRING, 1);
        pnd_Nasa8003_Pnd_Verification_Sw = pnd_Nasa8003.newFieldInGroup("pnd_Nasa8003_Pnd_Verification_Sw", "#VERIFICATION-SW", FieldType.STRING, 1);
        pnd_Nasa8003_Pnd_Generic_Sw = pnd_Nasa8003.newFieldInGroup("pnd_Nasa8003_Pnd_Generic_Sw", "#GENERIC-SW", FieldType.STRING, 1);
        pnd_Nasa8003_Pnd_Msg = pnd_Nasa8003.newFieldInGroup("pnd_Nasa8003_Pnd_Msg", "#MSG", FieldType.STRING, 80);

        dbsRecord.reset();
    }

    // Constructors
    public PdaNasa8003(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

