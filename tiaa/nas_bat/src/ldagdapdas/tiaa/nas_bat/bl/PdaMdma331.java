/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:21:48 PM
**        * FROM NATURAL PDA     : MDMA331
************************************************************
**        * FILE NAME            : PdaMdma331.java
**        * CLASS NAME           : PdaMdma331
**        * INSTANCE NAME        : PdaMdma331
************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaMdma331 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Mdma331;
    private DbsField pnd_Mdma331_Pnd_I_Section;
    private DbsGroup pnd_Mdma331_Pnd_I_SectionRedef1;
    private DbsField pnd_Mdma331_Pnd_I_Tiaa_Cref_Ind;
    private DbsField pnd_Mdma331_Pnd_I_Contract_Number;
    private DbsField pnd_Mdma331_Pnd_I_Payee_Code_A2;
    private DbsGroup pnd_Mdma331_Pnd_I_Payee_Code_A2Redef2;
    private DbsField pnd_Mdma331_Pnd_I_Payee_Code;
    private DbsField pnd_Mdma331_Pnd_I_Address_Usage;
    private DbsField pnd_Mdma331_Pnd_I_Inquire_Date;
    private DbsField pnd_Mdma331_Pnd_I_Inquire_Time;
    private DbsGroup pnd_Mdma331_Pnd_I_Inquire_TimeRedef3;
    private DbsField pnd_Mdma331_Pnd_I_Inquire_Hour;
    private DbsField pnd_Mdma331_Pnd_I_Inquire_Colon1;
    private DbsField pnd_Mdma331_Pnd_I_Inquire_Minute;
    private DbsField pnd_Mdma331_Pnd_I_Inquire_Colon2;
    private DbsField pnd_Mdma331_Pnd_I_Inquire_Second;
    private DbsGroup pnd_Mdma331_Pnd_O_Return_Section;
    private DbsField pnd_Mdma331_Pnd_O_Return_Code;
    private DbsField pnd_Mdma331_Pnd_O_Return_Text;
    private DbsField pnd_Mdma331_Pnd_O_Data_Section;
    private DbsGroup pnd_Mdma331_Pnd_O_Data_SectionRedef4;
    private DbsGroup pnd_Mdma331_Pnd_O_Pda_Data;
    private DbsField pnd_Mdma331_Pnd_O_Ssn;
    private DbsField pnd_Mdma331_Pnd_O_Pin;
    private DbsField pnd_Mdma331_Pnd_O_Preferred_Last_Name;
    private DbsField pnd_Mdma331_Pnd_O_Preferred_First_Name;
    private DbsField pnd_Mdma331_Pnd_O_Preferred_Middle_Name;
    private DbsField pnd_Mdma331_Pnd_O_Preferred_Prefix;
    private DbsField pnd_Mdma331_Pnd_O_Preferred_Suffix;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Type_Code;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Line_1;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Line_2;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Line_3;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Line_4;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_City;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_St_Prov;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Zip_Code;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Country;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Check_Saving_Ind;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Bank_Account_Number;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Bank_Aba_Eft_Number;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Eft_Pay_Type;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Postal_Data;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Geographic_Code;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Status_Code;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Status_Date;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Override_Code;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Last_Update_Date;
    private DbsField pnd_Mdma331_Pnd_O_Prvs_Address_Last_Update_Time;
    private DbsField pnd_Mdma331_Pnd_O_Line_Of_Business;
    private DbsField pnd_Mdma331_Pnd_O_Contract_Status_Code;
    private DbsField pnd_Mdma331_Pnd_O_Contract_Status_Year;
    private DbsField pnd_Mdma331_Pnd_O_Pin_N12;

    public DbsGroup getPnd_Mdma331() { return pnd_Mdma331; }

    public DbsField getPnd_Mdma331_Pnd_I_Section() { return pnd_Mdma331_Pnd_I_Section; }

    public DbsGroup getPnd_Mdma331_Pnd_I_SectionRedef1() { return pnd_Mdma331_Pnd_I_SectionRedef1; }

    public DbsField getPnd_Mdma331_Pnd_I_Tiaa_Cref_Ind() { return pnd_Mdma331_Pnd_I_Tiaa_Cref_Ind; }

    public DbsField getPnd_Mdma331_Pnd_I_Contract_Number() { return pnd_Mdma331_Pnd_I_Contract_Number; }

    public DbsField getPnd_Mdma331_Pnd_I_Payee_Code_A2() { return pnd_Mdma331_Pnd_I_Payee_Code_A2; }

    public DbsGroup getPnd_Mdma331_Pnd_I_Payee_Code_A2Redef2() { return pnd_Mdma331_Pnd_I_Payee_Code_A2Redef2; }

    public DbsField getPnd_Mdma331_Pnd_I_Payee_Code() { return pnd_Mdma331_Pnd_I_Payee_Code; }

    public DbsField getPnd_Mdma331_Pnd_I_Address_Usage() { return pnd_Mdma331_Pnd_I_Address_Usage; }

    public DbsField getPnd_Mdma331_Pnd_I_Inquire_Date() { return pnd_Mdma331_Pnd_I_Inquire_Date; }

    public DbsField getPnd_Mdma331_Pnd_I_Inquire_Time() { return pnd_Mdma331_Pnd_I_Inquire_Time; }

    public DbsGroup getPnd_Mdma331_Pnd_I_Inquire_TimeRedef3() { return pnd_Mdma331_Pnd_I_Inquire_TimeRedef3; }

    public DbsField getPnd_Mdma331_Pnd_I_Inquire_Hour() { return pnd_Mdma331_Pnd_I_Inquire_Hour; }

    public DbsField getPnd_Mdma331_Pnd_I_Inquire_Colon1() { return pnd_Mdma331_Pnd_I_Inquire_Colon1; }

    public DbsField getPnd_Mdma331_Pnd_I_Inquire_Minute() { return pnd_Mdma331_Pnd_I_Inquire_Minute; }

    public DbsField getPnd_Mdma331_Pnd_I_Inquire_Colon2() { return pnd_Mdma331_Pnd_I_Inquire_Colon2; }

    public DbsField getPnd_Mdma331_Pnd_I_Inquire_Second() { return pnd_Mdma331_Pnd_I_Inquire_Second; }

    public DbsGroup getPnd_Mdma331_Pnd_O_Return_Section() { return pnd_Mdma331_Pnd_O_Return_Section; }

    public DbsField getPnd_Mdma331_Pnd_O_Return_Code() { return pnd_Mdma331_Pnd_O_Return_Code; }

    public DbsField getPnd_Mdma331_Pnd_O_Return_Text() { return pnd_Mdma331_Pnd_O_Return_Text; }

    public DbsField getPnd_Mdma331_Pnd_O_Data_Section() { return pnd_Mdma331_Pnd_O_Data_Section; }

    public DbsGroup getPnd_Mdma331_Pnd_O_Data_SectionRedef4() { return pnd_Mdma331_Pnd_O_Data_SectionRedef4; }

    public DbsGroup getPnd_Mdma331_Pnd_O_Pda_Data() { return pnd_Mdma331_Pnd_O_Pda_Data; }

    public DbsField getPnd_Mdma331_Pnd_O_Ssn() { return pnd_Mdma331_Pnd_O_Ssn; }

    public DbsField getPnd_Mdma331_Pnd_O_Pin() { return pnd_Mdma331_Pnd_O_Pin; }

    public DbsField getPnd_Mdma331_Pnd_O_Preferred_Last_Name() { return pnd_Mdma331_Pnd_O_Preferred_Last_Name; }

    public DbsField getPnd_Mdma331_Pnd_O_Preferred_First_Name() { return pnd_Mdma331_Pnd_O_Preferred_First_Name; }

    public DbsField getPnd_Mdma331_Pnd_O_Preferred_Middle_Name() { return pnd_Mdma331_Pnd_O_Preferred_Middle_Name; }

    public DbsField getPnd_Mdma331_Pnd_O_Preferred_Prefix() { return pnd_Mdma331_Pnd_O_Preferred_Prefix; }

    public DbsField getPnd_Mdma331_Pnd_O_Preferred_Suffix() { return pnd_Mdma331_Pnd_O_Preferred_Suffix; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Type_Code() { return pnd_Mdma331_Pnd_O_Prvs_Address_Type_Code; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Line_1() { return pnd_Mdma331_Pnd_O_Prvs_Address_Line_1; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Line_2() { return pnd_Mdma331_Pnd_O_Prvs_Address_Line_2; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Line_3() { return pnd_Mdma331_Pnd_O_Prvs_Address_Line_3; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Line_4() { return pnd_Mdma331_Pnd_O_Prvs_Address_Line_4; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_City() { return pnd_Mdma331_Pnd_O_Prvs_Address_City; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_St_Prov() { return pnd_Mdma331_Pnd_O_Prvs_Address_St_Prov; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Zip_Code() { return pnd_Mdma331_Pnd_O_Prvs_Address_Zip_Code; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Country() { return pnd_Mdma331_Pnd_O_Prvs_Address_Country; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Check_Saving_Ind() { return pnd_Mdma331_Pnd_O_Prvs_Check_Saving_Ind; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Bank_Account_Number() { return pnd_Mdma331_Pnd_O_Prvs_Bank_Account_Number; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Bank_Aba_Eft_Number() { return pnd_Mdma331_Pnd_O_Prvs_Bank_Aba_Eft_Number; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Eft_Pay_Type() { return pnd_Mdma331_Pnd_O_Prvs_Eft_Pay_Type; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Postal_Data() { return pnd_Mdma331_Pnd_O_Prvs_Address_Postal_Data; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Geographic_Code() { return pnd_Mdma331_Pnd_O_Prvs_Address_Geographic_Code; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Status_Code() { return pnd_Mdma331_Pnd_O_Prvs_Address_Status_Code; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Status_Date() { return pnd_Mdma331_Pnd_O_Prvs_Address_Status_Date; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Override_Code() { return pnd_Mdma331_Pnd_O_Prvs_Address_Override_Code; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Last_Update_Date() { return pnd_Mdma331_Pnd_O_Prvs_Address_Last_Update_Date; }

    public DbsField getPnd_Mdma331_Pnd_O_Prvs_Address_Last_Update_Time() { return pnd_Mdma331_Pnd_O_Prvs_Address_Last_Update_Time; }

    public DbsField getPnd_Mdma331_Pnd_O_Line_Of_Business() { return pnd_Mdma331_Pnd_O_Line_Of_Business; }

    public DbsField getPnd_Mdma331_Pnd_O_Contract_Status_Code() { return pnd_Mdma331_Pnd_O_Contract_Status_Code; }

    public DbsField getPnd_Mdma331_Pnd_O_Contract_Status_Year() { return pnd_Mdma331_Pnd_O_Contract_Status_Year; }

    public DbsField getPnd_Mdma331_Pnd_O_Pin_N12() { return pnd_Mdma331_Pnd_O_Pin_N12; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Mdma331 = dbsRecord.newGroupInRecord("pnd_Mdma331", "#MDMA331");
        pnd_Mdma331.setParameterOption(ParameterOption.ByReference);
        pnd_Mdma331_Pnd_I_Section = pnd_Mdma331.newFieldArrayInGroup("pnd_Mdma331_Pnd_I_Section", "#I-SECTION", FieldType.STRING, 1, new DbsArrayController(1,
            32));
        pnd_Mdma331_Pnd_I_SectionRedef1 = pnd_Mdma331.newGroupInGroup("pnd_Mdma331_Pnd_I_SectionRedef1", "Redefines", pnd_Mdma331_Pnd_I_Section);
        pnd_Mdma331_Pnd_I_Tiaa_Cref_Ind = pnd_Mdma331_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma331_Pnd_I_Tiaa_Cref_Ind", "#I-TIAA-CREF-IND", FieldType.STRING, 
            1);
        pnd_Mdma331_Pnd_I_Contract_Number = pnd_Mdma331_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma331_Pnd_I_Contract_Number", "#I-CONTRACT-NUMBER", 
            FieldType.STRING, 10);
        pnd_Mdma331_Pnd_I_Payee_Code_A2 = pnd_Mdma331_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma331_Pnd_I_Payee_Code_A2", "#I-PAYEE-CODE-A2", FieldType.STRING, 
            2);
        pnd_Mdma331_Pnd_I_Payee_Code_A2Redef2 = pnd_Mdma331_Pnd_I_SectionRedef1.newGroupInGroup("pnd_Mdma331_Pnd_I_Payee_Code_A2Redef2", "Redefines", 
            pnd_Mdma331_Pnd_I_Payee_Code_A2);
        pnd_Mdma331_Pnd_I_Payee_Code = pnd_Mdma331_Pnd_I_Payee_Code_A2Redef2.newFieldInGroup("pnd_Mdma331_Pnd_I_Payee_Code", "#I-PAYEE-CODE", FieldType.NUMERIC, 
            2);
        pnd_Mdma331_Pnd_I_Address_Usage = pnd_Mdma331_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma331_Pnd_I_Address_Usage", "#I-ADDRESS-USAGE", FieldType.STRING, 
            1);
        pnd_Mdma331_Pnd_I_Inquire_Date = pnd_Mdma331_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma331_Pnd_I_Inquire_Date", "#I-INQUIRE-DATE", FieldType.STRING, 
            10);
        pnd_Mdma331_Pnd_I_Inquire_Time = pnd_Mdma331_Pnd_I_SectionRedef1.newFieldInGroup("pnd_Mdma331_Pnd_I_Inquire_Time", "#I-INQUIRE-TIME", FieldType.STRING, 
            8);
        pnd_Mdma331_Pnd_I_Inquire_TimeRedef3 = pnd_Mdma331_Pnd_I_SectionRedef1.newGroupInGroup("pnd_Mdma331_Pnd_I_Inquire_TimeRedef3", "Redefines", pnd_Mdma331_Pnd_I_Inquire_Time);
        pnd_Mdma331_Pnd_I_Inquire_Hour = pnd_Mdma331_Pnd_I_Inquire_TimeRedef3.newFieldInGroup("pnd_Mdma331_Pnd_I_Inquire_Hour", "#I-INQUIRE-HOUR", FieldType.NUMERIC, 
            2);
        pnd_Mdma331_Pnd_I_Inquire_Colon1 = pnd_Mdma331_Pnd_I_Inquire_TimeRedef3.newFieldInGroup("pnd_Mdma331_Pnd_I_Inquire_Colon1", "#I-INQUIRE-COLON1", 
            FieldType.STRING, 1);
        pnd_Mdma331_Pnd_I_Inquire_Minute = pnd_Mdma331_Pnd_I_Inquire_TimeRedef3.newFieldInGroup("pnd_Mdma331_Pnd_I_Inquire_Minute", "#I-INQUIRE-MINUTE", 
            FieldType.NUMERIC, 2);
        pnd_Mdma331_Pnd_I_Inquire_Colon2 = pnd_Mdma331_Pnd_I_Inquire_TimeRedef3.newFieldInGroup("pnd_Mdma331_Pnd_I_Inquire_Colon2", "#I-INQUIRE-COLON2", 
            FieldType.STRING, 1);
        pnd_Mdma331_Pnd_I_Inquire_Second = pnd_Mdma331_Pnd_I_Inquire_TimeRedef3.newFieldInGroup("pnd_Mdma331_Pnd_I_Inquire_Second", "#I-INQUIRE-SECOND", 
            FieldType.NUMERIC, 2);
        pnd_Mdma331_Pnd_O_Return_Section = pnd_Mdma331.newGroupInGroup("pnd_Mdma331_Pnd_O_Return_Section", "#O-RETURN-SECTION");
        pnd_Mdma331_Pnd_O_Return_Code = pnd_Mdma331_Pnd_O_Return_Section.newFieldInGroup("pnd_Mdma331_Pnd_O_Return_Code", "#O-RETURN-CODE", FieldType.STRING, 
            4);
        pnd_Mdma331_Pnd_O_Return_Text = pnd_Mdma331_Pnd_O_Return_Section.newFieldInGroup("pnd_Mdma331_Pnd_O_Return_Text", "#O-RETURN-TEXT", FieldType.STRING, 
            80);
        pnd_Mdma331_Pnd_O_Data_Section = pnd_Mdma331.newFieldArrayInGroup("pnd_Mdma331_Pnd_O_Data_Section", "#O-DATA-SECTION", FieldType.STRING, 1, new 
            DbsArrayController(1,490));
        pnd_Mdma331_Pnd_O_Data_SectionRedef4 = pnd_Mdma331.newGroupInGroup("pnd_Mdma331_Pnd_O_Data_SectionRedef4", "Redefines", pnd_Mdma331_Pnd_O_Data_Section);
        pnd_Mdma331_Pnd_O_Pda_Data = pnd_Mdma331_Pnd_O_Data_SectionRedef4.newGroupInGroup("pnd_Mdma331_Pnd_O_Pda_Data", "#O-PDA-DATA");
        pnd_Mdma331_Pnd_O_Ssn = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Ssn", "#O-SSN", FieldType.NUMERIC, 9);
        pnd_Mdma331_Pnd_O_Pin = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Pin", "#O-PIN", FieldType.NUMERIC, 7);
        pnd_Mdma331_Pnd_O_Preferred_Last_Name = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Preferred_Last_Name", "#O-PREFERRED-LAST-NAME", 
            FieldType.STRING, 35);
        pnd_Mdma331_Pnd_O_Preferred_First_Name = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Preferred_First_Name", "#O-PREFERRED-FIRST-NAME", 
            FieldType.STRING, 30);
        pnd_Mdma331_Pnd_O_Preferred_Middle_Name = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Preferred_Middle_Name", "#O-PREFERRED-MIDDLE-NAME", 
            FieldType.STRING, 30);
        pnd_Mdma331_Pnd_O_Preferred_Prefix = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Preferred_Prefix", "#O-PREFERRED-PREFIX", FieldType.STRING, 
            8);
        pnd_Mdma331_Pnd_O_Preferred_Suffix = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Preferred_Suffix", "#O-PREFERRED-SUFFIX", FieldType.STRING, 
            8);
        pnd_Mdma331_Pnd_O_Prvs_Address_Type_Code = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Type_Code", "#O-PRVS-ADDRESS-TYPE-CODE", 
            FieldType.STRING, 1);
        pnd_Mdma331_Pnd_O_Prvs_Address_Line_1 = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Line_1", "#O-PRVS-ADDRESS-LINE-1", 
            FieldType.STRING, 35);
        pnd_Mdma331_Pnd_O_Prvs_Address_Line_2 = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Line_2", "#O-PRVS-ADDRESS-LINE-2", 
            FieldType.STRING, 35);
        pnd_Mdma331_Pnd_O_Prvs_Address_Line_3 = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Line_3", "#O-PRVS-ADDRESS-LINE-3", 
            FieldType.STRING, 35);
        pnd_Mdma331_Pnd_O_Prvs_Address_Line_4 = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Line_4", "#O-PRVS-ADDRESS-LINE-4", 
            FieldType.STRING, 35);
        pnd_Mdma331_Pnd_O_Prvs_Address_City = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_City", "#O-PRVS-ADDRESS-CITY", 
            FieldType.STRING, 25);
        pnd_Mdma331_Pnd_O_Prvs_Address_St_Prov = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_St_Prov", "#O-PRVS-ADDRESS-ST-PROV", 
            FieldType.STRING, 2);
        pnd_Mdma331_Pnd_O_Prvs_Address_Zip_Code = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Zip_Code", "#O-PRVS-ADDRESS-ZIP-CODE", 
            FieldType.STRING, 10);
        pnd_Mdma331_Pnd_O_Prvs_Address_Country = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Country", "#O-PRVS-ADDRESS-COUNTRY", 
            FieldType.STRING, 35);
        pnd_Mdma331_Pnd_O_Prvs_Check_Saving_Ind = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Check_Saving_Ind", "#O-PRVS-CHECK-SAVING-IND", 
            FieldType.STRING, 1);
        pnd_Mdma331_Pnd_O_Prvs_Bank_Account_Number = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Bank_Account_Number", "#O-PRVS-BANK-ACCOUNT-NUMBER", 
            FieldType.STRING, 35);
        pnd_Mdma331_Pnd_O_Prvs_Bank_Aba_Eft_Number = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Bank_Aba_Eft_Number", "#O-PRVS-BANK-ABA-EFT-NUMBER", 
            FieldType.STRING, 35);
        pnd_Mdma331_Pnd_O_Prvs_Eft_Pay_Type = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Eft_Pay_Type", "#O-PRVS-EFT-PAY-TYPE", 
            FieldType.STRING, 2);
        pnd_Mdma331_Pnd_O_Prvs_Address_Postal_Data = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Postal_Data", "#O-PRVS-ADDRESS-POSTAL-DATA", 
            FieldType.STRING, 32);
        pnd_Mdma331_Pnd_O_Prvs_Address_Geographic_Code = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Geographic_Code", 
            "#O-PRVS-ADDRESS-GEOGRAPHIC-CODE", FieldType.STRING, 2);
        pnd_Mdma331_Pnd_O_Prvs_Address_Status_Code = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Status_Code", "#O-PRVS-ADDRESS-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Mdma331_Pnd_O_Prvs_Address_Status_Date = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Status_Date", "#O-PRVS-ADDRESS-STATUS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Mdma331_Pnd_O_Prvs_Address_Override_Code = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Override_Code", "#O-PRVS-ADDRESS-OVERRIDE-CODE", 
            FieldType.STRING, 1);
        pnd_Mdma331_Pnd_O_Prvs_Address_Last_Update_Date = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Last_Update_Date", 
            "#O-PRVS-ADDRESS-LAST-UPDATE-DATE", FieldType.NUMERIC, 8);
        pnd_Mdma331_Pnd_O_Prvs_Address_Last_Update_Time = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Prvs_Address_Last_Update_Time", 
            "#O-PRVS-ADDRESS-LAST-UPDATE-TIME", FieldType.NUMERIC, 7);
        pnd_Mdma331_Pnd_O_Line_Of_Business = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Line_Of_Business", "#O-LINE-OF-BUSINESS", FieldType.STRING, 
            1);
        pnd_Mdma331_Pnd_O_Contract_Status_Code = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Contract_Status_Code", "#O-CONTRACT-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Mdma331_Pnd_O_Contract_Status_Year = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Contract_Status_Year", "#O-CONTRACT-STATUS-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Mdma331_Pnd_O_Pin_N12 = pnd_Mdma331_Pnd_O_Pda_Data.newFieldInGroup("pnd_Mdma331_Pnd_O_Pin_N12", "#O-PIN-N12", FieldType.NUMERIC, 12);

        dbsRecord.reset();
    }

    // Constructors
    public PdaMdma331(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

