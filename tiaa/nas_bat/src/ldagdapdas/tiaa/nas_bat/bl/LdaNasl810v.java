/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:07:54 PM
**        * FROM NATURAL LDA     : NASL810V
************************************************************
**        * FILE NAME            : LdaNasl810v.java
**        * CLASS NAME           : LdaNasl810v
**        * INSTANCE NAME        : LdaNasl810v
************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaNasl810v extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_table_View;
    private DbsField table_View_Table_Id;
    private DbsField table_View_Data_Key;
    private DbsField table_View_Data_Description;
    private DbsField table_View_Last_Add_Userid;
    private DbsField table_View_Last_Add_Date_Time;
    private DbsField table_View_Last_Updt_Date_Time;
    private DbsField table_View_Last_Updt_Pgm_Name;
    private DbsField table_View_Last_Updt_Userid;
    private DbsField table_View_Last_Updt_Termid;

    public DataAccessProgramView getVw_table_View() { return vw_table_View; }

    public DbsField getTable_View_Table_Id() { return table_View_Table_Id; }

    public DbsField getTable_View_Data_Key() { return table_View_Data_Key; }

    public DbsField getTable_View_Data_Description() { return table_View_Data_Description; }

    public DbsField getTable_View_Last_Add_Userid() { return table_View_Last_Add_Userid; }

    public DbsField getTable_View_Last_Add_Date_Time() { return table_View_Last_Add_Date_Time; }

    public DbsField getTable_View_Last_Updt_Date_Time() { return table_View_Last_Updt_Date_Time; }

    public DbsField getTable_View_Last_Updt_Pgm_Name() { return table_View_Last_Updt_Pgm_Name; }

    public DbsField getTable_View_Last_Updt_Userid() { return table_View_Last_Updt_Userid; }

    public DbsField getTable_View_Last_Updt_Termid() { return table_View_Last_Updt_Termid; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_table_View = new DataAccessProgramView(new NameInfo("vw_table_View", "TABLE-VIEW"), "NAS_REFERENCE_TABLE_VIEW", "NAS_REF_TABLES");
        table_View_Table_Id = vw_table_View.getRecord().newFieldInGroup("table_View_Table_Id", "TABLE-ID", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TABLE_ID");
        table_View_Data_Key = vw_table_View.getRecord().newFieldInGroup("table_View_Data_Key", "DATA-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "DATA_KEY");
        table_View_Data_Description = vw_table_View.getRecord().newFieldInGroup("table_View_Data_Description", "DATA-DESCRIPTION", FieldType.STRING, 80, 
            RepeatingFieldStrategy.None, "DATA_DESCRIPTION");
        table_View_Last_Add_Userid = vw_table_View.getRecord().newFieldInGroup("table_View_Last_Add_Userid", "LAST-ADD-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_ADD_USERID");
        table_View_Last_Add_Date_Time = vw_table_View.getRecord().newFieldInGroup("table_View_Last_Add_Date_Time", "LAST-ADD-DATE-TIME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "LAST_ADD_DATE_TIME");
        table_View_Last_Updt_Date_Time = vw_table_View.getRecord().newFieldInGroup("table_View_Last_Updt_Date_Time", "LAST-UPDT-DATE-TIME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "LAST_UPDT_DATE_TIME");
        table_View_Last_Updt_Pgm_Name = vw_table_View.getRecord().newFieldInGroup("table_View_Last_Updt_Pgm_Name", "LAST-UPDT-PGM-NAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_UPDT_PGM_NAME");
        table_View_Last_Updt_Userid = vw_table_View.getRecord().newFieldInGroup("table_View_Last_Updt_Userid", "LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "LAST_UPDT_USERID");
        table_View_Last_Updt_Termid = vw_table_View.getRecord().newFieldInGroup("table_View_Last_Updt_Termid", "LAST-UPDT-TERMID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "LAST_UPDT_TERMID");

        this.setRecordName("LdaNasl810v");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_table_View.reset();
    }

    // Constructor
    public LdaNasl810v() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
