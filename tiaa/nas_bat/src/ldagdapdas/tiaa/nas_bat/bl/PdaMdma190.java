/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:21:44 PM
**        * FROM NATURAL PDA     : MDMA190
************************************************************
**        * FILE NAME            : PdaMdma190.java
**        * CLASS NAME           : PdaMdma190
**        * INSTANCE NAME        : PdaMdma190
************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaMdma190 extends PdaBase
{
    // Properties
    private DbsGroup mdma190;
    private DbsField mdma190_Pnd_Gda_User_Id;
    private DbsField mdma190_Pnd_Gda_Slo_Exists;
    private DbsField mdma190_Pnd_Slo_Rqst_Log_Dte_Tme;
    private DbsField mdma190_Pnd_Wpid;
    private DbsField mdma190_Pnd_Wpid_Pin;
    private DbsField mdma190_Pnd_Wpid_Unit;
    private DbsField mdma190_Pnd_Wpid_Next_Unit;
    private DbsField mdma190_Pnd_Wpid_Status;
    private DbsField mdma190_Pnd_Wpid_Special_Handling_Text;
    private DbsField mdma190_Pnd_Wpid_Priority_Cde;
    private DbsField mdma190_Pnd_Error_Message;
    private DbsField mdma190_Pnd_Wpid_System;
    private DbsField mdma190_Pnd_Et_Ind;

    public DbsGroup getMdma190() { return mdma190; }

    public DbsField getMdma190_Pnd_Gda_User_Id() { return mdma190_Pnd_Gda_User_Id; }

    public DbsField getMdma190_Pnd_Gda_Slo_Exists() { return mdma190_Pnd_Gda_Slo_Exists; }

    public DbsField getMdma190_Pnd_Slo_Rqst_Log_Dte_Tme() { return mdma190_Pnd_Slo_Rqst_Log_Dte_Tme; }

    public DbsField getMdma190_Pnd_Wpid() { return mdma190_Pnd_Wpid; }

    public DbsField getMdma190_Pnd_Wpid_Pin() { return mdma190_Pnd_Wpid_Pin; }

    public DbsField getMdma190_Pnd_Wpid_Unit() { return mdma190_Pnd_Wpid_Unit; }

    public DbsField getMdma190_Pnd_Wpid_Next_Unit() { return mdma190_Pnd_Wpid_Next_Unit; }

    public DbsField getMdma190_Pnd_Wpid_Status() { return mdma190_Pnd_Wpid_Status; }

    public DbsField getMdma190_Pnd_Wpid_Special_Handling_Text() { return mdma190_Pnd_Wpid_Special_Handling_Text; }

    public DbsField getMdma190_Pnd_Wpid_Priority_Cde() { return mdma190_Pnd_Wpid_Priority_Cde; }

    public DbsField getMdma190_Pnd_Error_Message() { return mdma190_Pnd_Error_Message; }

    public DbsField getMdma190_Pnd_Wpid_System() { return mdma190_Pnd_Wpid_System; }

    public DbsField getMdma190_Pnd_Et_Ind() { return mdma190_Pnd_Et_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        mdma190 = dbsRecord.newGroupInRecord("mdma190", "MDMA190");
        mdma190.setParameterOption(ParameterOption.ByReference);
        mdma190_Pnd_Gda_User_Id = mdma190.newFieldInGroup("mdma190_Pnd_Gda_User_Id", "#GDA-USER-ID", FieldType.STRING, 8);
        mdma190_Pnd_Gda_Slo_Exists = mdma190.newFieldInGroup("mdma190_Pnd_Gda_Slo_Exists", "#GDA-SLO-EXISTS", FieldType.STRING, 1);
        mdma190_Pnd_Slo_Rqst_Log_Dte_Tme = mdma190.newFieldInGroup("mdma190_Pnd_Slo_Rqst_Log_Dte_Tme", "#SLO-RQST-LOG-DTE-TME", FieldType.STRING, 15);
        mdma190_Pnd_Wpid = mdma190.newFieldInGroup("mdma190_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        mdma190_Pnd_Wpid_Pin = mdma190.newFieldInGroup("mdma190_Pnd_Wpid_Pin", "#WPID-PIN", FieldType.NUMERIC, 12);
        mdma190_Pnd_Wpid_Unit = mdma190.newFieldInGroup("mdma190_Pnd_Wpid_Unit", "#WPID-UNIT", FieldType.STRING, 8);
        mdma190_Pnd_Wpid_Next_Unit = mdma190.newFieldInGroup("mdma190_Pnd_Wpid_Next_Unit", "#WPID-NEXT-UNIT", FieldType.STRING, 8);
        mdma190_Pnd_Wpid_Status = mdma190.newFieldInGroup("mdma190_Pnd_Wpid_Status", "#WPID-STATUS", FieldType.STRING, 4);
        mdma190_Pnd_Wpid_Special_Handling_Text = mdma190.newFieldInGroup("mdma190_Pnd_Wpid_Special_Handling_Text", "#WPID-SPECIAL-HANDLING-TEXT", FieldType.STRING, 
            45);
        mdma190_Pnd_Wpid_Priority_Cde = mdma190.newFieldInGroup("mdma190_Pnd_Wpid_Priority_Cde", "#WPID-PRIORITY-CDE", FieldType.STRING, 1);
        mdma190_Pnd_Error_Message = mdma190.newFieldInGroup("mdma190_Pnd_Error_Message", "#ERROR-MESSAGE", FieldType.STRING, 79);
        mdma190_Pnd_Wpid_System = mdma190.newFieldInGroup("mdma190_Pnd_Wpid_System", "#WPID-SYSTEM", FieldType.STRING, 8);
        mdma190_Pnd_Et_Ind = mdma190.newFieldInGroup("mdma190_Pnd_Et_Ind", "#ET-IND", FieldType.STRING, 1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaMdma190(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

