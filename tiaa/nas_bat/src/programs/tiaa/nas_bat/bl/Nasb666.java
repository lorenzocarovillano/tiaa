/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:59:54 PM
**        * FROM NATURAL PROGRAM : Nasb666
************************************************************
**        * FILE NAME            : Nasb666.java
**        * CLASS NAME           : Nasb666
**        * INSTANCE NAME        : Nasb666
************************************************************
************************************************************************
* PROGRAM NAME: NASB666
* AUTHOR      : MICHAEL WEBER
* DATE        : SEPT 2, 2003
* DESCRIPTION : LETTER PRE PROCESSOR
* HISTORY
* R. GO-PAZ  08/25/10
* 05/11/2012  GOPAZ  REMOVE HARDCODED DATE
* 08/21/2014  DURAND CREATE WPID=TA HTA FOR CREF PARTICIPANT REGULATORY
*                    RETURNED MAIL PROCESS 2015 Q1
* 06/28/2017  MEADED PIN EXPANSION - SEE PINE COMMENTS.
*                    ALSO COMMENTED OUT IA READ FOR DOD PER SALIE G-P.
************************************************************************

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nasb666 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaNasl747a ldaNasl747a;
    private PdaMdma190 pdaMdma190;
    private PdaNasa191 pdaNasa191;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ia_Contract;

    private DbsGroup pnd_Switches_Flags;
    private DbsField pnd_Switches_Flags_Pnd_Error_Ind;
    private DbsField pnd_Pin_Table;
    private DbsField pnd_Pin_Last;
    private DbsField pnd_Work_Cnt;
    private DbsField pnd_Repeat_Cnt;
    private DbsField pnd_Count_All;
    private DbsField pnd_Count_Restart;
    private DbsField pnd_Count_Edits;
    private DbsField pnd_Count_Bypass;
    private DbsField pnd_Count_Bypass_Pin;
    private DbsField pnd_Count_Written;
    private DbsField pnd_Pin_Cnt;
    private DbsField pnd_Error;
    private DbsField pnd_W_Seq_Num;
    private DbsField pnd_W_Current_Pin_Used;
    private DbsField pnd_Index;
    private DbsField pnd_I_Ndx;
    private DbsField pnd_J_Ndx;
    private DbsField pnd_W_Doc_Text;

    private DbsGroup pnd_W_Doc_Text__R_Field_1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Program;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Fill1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Title1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Fill2;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Area1;

    private DbsGroup pnd_W_Doc_Text__R_Field_2;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Date;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Fill1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Title1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Fill2;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Time;
    private DbsField pnd_Wrk_Cwf_Date;

    private DbsGroup pnd_Wrk_Cwf_Date__R_Field_3;
    private DbsField pnd_Wrk_Cwf_Date_Pnd_Wrk_Cwf_Date_Yyyy;
    private DbsField pnd_Wrk_Cwf_Date_Pnd_Wrk_Cwf_Date_Mm;
    private DbsField pnd_Wrk_Cwf_Date_Pnd_Wrk_Cwf_Date_Dd;
    private DbsField pnd_Wrk_Date_Edited;
    private DbsField pnd_Date_A;
    private DbsField pnd_Old_New_Addr_Line_1;
    private DbsField pnd_Old_New_Addr_Line_2;
    private DbsField pnd_Old_New_Addr_Line_3;
    private DbsField pnd_Old_Bad_Addr_Line_1;
    private DbsField pnd_Old_Bad_Addr_Line_2;
    private DbsField pnd_Old_Bad_Addr_Line_3;
    private DbsField pnd_Old_Bad_Addr_Line_4;
    private DbsField pnd_Old_Bad_Addr_Line_5;
    private DbsField pnd_Wrk_Business_Date;

    private DbsGroup pnd_Wrk_Business_Date__R_Field_4;
    private DbsField pnd_Wrk_Business_Date_Pnd_Wrk_Business_Date_Mm;
    private DbsField pnd_Wrk_Business_Date_Pnd_Wrk_Business_Date_Dd;
    private DbsField pnd_Wrk_Business_Date_Pnd_Wrk_Business_Date_Yyyy;
    private DbsField pnd_Read;
    private DbsField pnd_Update_Cnt;
    private DbsField pnd_Error_Cnt;
    private DbsField pnd_Old_City_State;

    private DbsGroup pnd_Old_City_State__R_Field_5;
    private DbsField pnd_Old_City_State_Pnd_Old_City;
    private DbsField pnd_Old_City_State_Pnd_Old_State;
    private DbsField pnd_Compress_Old_City_State;

    private DbsGroup pnd_Bypass_Out;
    private DbsField pnd_Bypass_Out_Pnd_Bypass_Pin_O;
    private DbsField pnd_Bypass_Out_Pnd_Filler;
    private DbsField pnd_Remarks;
    private DbsField pnd_Same_Address;
    private DbsField pnd_Dod_Vendor;
    private DbsField pnd_No_Address;
    private DbsField pnd_Qs;
    private DbsField pnd_Old_Address;
    private DbsField pnd_New_Address;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaNasl747a = new LdaNasl747a();
        registerRecord(ldaNasl747a);
        localVariables = new DbsRecord();
        pdaMdma190 = new PdaMdma190(localVariables);
        pdaNasa191 = new PdaNasa191(localVariables);

        // Local Variables
        pnd_Ia_Contract = localVariables.newFieldInRecord("pnd_Ia_Contract", "#IA-CONTRACT", FieldType.STRING, 10);

        pnd_Switches_Flags = localVariables.newGroupInRecord("pnd_Switches_Flags", "#SWITCHES-FLAGS");
        pnd_Switches_Flags_Pnd_Error_Ind = pnd_Switches_Flags.newFieldInGroup("pnd_Switches_Flags_Pnd_Error_Ind", "#ERROR-IND", FieldType.STRING, 1);
        pnd_Pin_Table = localVariables.newFieldArrayInRecord("pnd_Pin_Table", "#PIN-TABLE", FieldType.NUMERIC, 12, new DbsArrayController(1, 200));
        pnd_Pin_Last = localVariables.newFieldInRecord("pnd_Pin_Last", "#PIN-LAST", FieldType.NUMERIC, 12);
        pnd_Work_Cnt = localVariables.newFieldInRecord("pnd_Work_Cnt", "#WORK-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Repeat_Cnt = localVariables.newFieldInRecord("pnd_Repeat_Cnt", "#REPEAT-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Count_All = localVariables.newFieldInRecord("pnd_Count_All", "#COUNT-ALL", FieldType.PACKED_DECIMAL, 9);
        pnd_Count_Restart = localVariables.newFieldInRecord("pnd_Count_Restart", "#COUNT-RESTART", FieldType.PACKED_DECIMAL, 9);
        pnd_Count_Edits = localVariables.newFieldInRecord("pnd_Count_Edits", "#COUNT-EDITS", FieldType.PACKED_DECIMAL, 9);
        pnd_Count_Bypass = localVariables.newFieldInRecord("pnd_Count_Bypass", "#COUNT-BYPASS", FieldType.PACKED_DECIMAL, 9);
        pnd_Count_Bypass_Pin = localVariables.newFieldInRecord("pnd_Count_Bypass_Pin", "#COUNT-BYPASS-PIN", FieldType.PACKED_DECIMAL, 9);
        pnd_Count_Written = localVariables.newFieldInRecord("pnd_Count_Written", "#COUNT-WRITTEN", FieldType.PACKED_DECIMAL, 9);
        pnd_Pin_Cnt = localVariables.newFieldInRecord("pnd_Pin_Cnt", "#PIN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Error = localVariables.newFieldInRecord("pnd_Error", "#ERROR", FieldType.STRING, 79);
        pnd_W_Seq_Num = localVariables.newFieldInRecord("pnd_W_Seq_Num", "#W-SEQ-NUM", FieldType.PACKED_DECIMAL, 9);
        pnd_W_Current_Pin_Used = localVariables.newFieldInRecord("pnd_W_Current_Pin_Used", "#W-CURRENT-PIN-USED", FieldType.NUMERIC, 12);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.NUMERIC, 3);
        pnd_I_Ndx = localVariables.newFieldInRecord("pnd_I_Ndx", "#I-NDX", FieldType.NUMERIC, 3);
        pnd_J_Ndx = localVariables.newFieldInRecord("pnd_J_Ndx", "#J-NDX", FieldType.NUMERIC, 3);
        pnd_W_Doc_Text = localVariables.newFieldInRecord("pnd_W_Doc_Text", "#W-DOC-TEXT", FieldType.STRING, 80);

        pnd_W_Doc_Text__R_Field_1 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_1", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln1_Program = pnd_W_Doc_Text__R_Field_1.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Program", "#W-LN1-PROGRAM", FieldType.STRING, 
            7);
        pnd_W_Doc_Text_Pnd_W_Ln1_Fill1 = pnd_W_Doc_Text__R_Field_1.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Fill1", "#W-LN1-FILL1", FieldType.STRING, 
            19);
        pnd_W_Doc_Text_Pnd_W_Ln1_Title1 = pnd_W_Doc_Text__R_Field_1.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Title1", "#W-LN1-TITLE1", FieldType.STRING, 
            23);
        pnd_W_Doc_Text_Pnd_W_Ln1_Fill2 = pnd_W_Doc_Text__R_Field_1.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Fill2", "#W-LN1-FILL2", FieldType.STRING, 
            19);
        pnd_W_Doc_Text_Pnd_W_Ln1_Area1 = pnd_W_Doc_Text__R_Field_1.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Area1", "#W-LN1-AREA1", FieldType.STRING, 
            12);

        pnd_W_Doc_Text__R_Field_2 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_2", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln2_Date = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Date", "#W-LN2-DATE", FieldType.STRING, 10);
        pnd_W_Doc_Text_Pnd_W_Ln2_Fill1 = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Fill1", "#W-LN2-FILL1", FieldType.STRING, 
            13);
        pnd_W_Doc_Text_Pnd_W_Ln2_Title1 = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Title1", "#W-LN2-TITLE1", FieldType.STRING, 
            33);
        pnd_W_Doc_Text_Pnd_W_Ln2_Fill2 = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Fill2", "#W-LN2-FILL2", FieldType.STRING, 
            13);
        pnd_W_Doc_Text_Pnd_W_Ln2_Time = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Time", "#W-LN2-TIME", FieldType.STRING, 11);
        pnd_Wrk_Cwf_Date = localVariables.newFieldInRecord("pnd_Wrk_Cwf_Date", "#WRK-CWF-DATE", FieldType.NUMERIC, 8);

        pnd_Wrk_Cwf_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Wrk_Cwf_Date__R_Field_3", "REDEFINE", pnd_Wrk_Cwf_Date);
        pnd_Wrk_Cwf_Date_Pnd_Wrk_Cwf_Date_Yyyy = pnd_Wrk_Cwf_Date__R_Field_3.newFieldInGroup("pnd_Wrk_Cwf_Date_Pnd_Wrk_Cwf_Date_Yyyy", "#WRK-CWF-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Wrk_Cwf_Date_Pnd_Wrk_Cwf_Date_Mm = pnd_Wrk_Cwf_Date__R_Field_3.newFieldInGroup("pnd_Wrk_Cwf_Date_Pnd_Wrk_Cwf_Date_Mm", "#WRK-CWF-DATE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Wrk_Cwf_Date_Pnd_Wrk_Cwf_Date_Dd = pnd_Wrk_Cwf_Date__R_Field_3.newFieldInGroup("pnd_Wrk_Cwf_Date_Pnd_Wrk_Cwf_Date_Dd", "#WRK-CWF-DATE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Wrk_Date_Edited = localVariables.newFieldInRecord("pnd_Wrk_Date_Edited", "#WRK-DATE-EDITED", FieldType.STRING, 10);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 10);
        pnd_Old_New_Addr_Line_1 = localVariables.newFieldInRecord("pnd_Old_New_Addr_Line_1", "#OLD-NEW-ADDR-LINE-1", FieldType.STRING, 35);
        pnd_Old_New_Addr_Line_2 = localVariables.newFieldInRecord("pnd_Old_New_Addr_Line_2", "#OLD-NEW-ADDR-LINE-2", FieldType.STRING, 35);
        pnd_Old_New_Addr_Line_3 = localVariables.newFieldInRecord("pnd_Old_New_Addr_Line_3", "#OLD-NEW-ADDR-LINE-3", FieldType.STRING, 35);
        pnd_Old_Bad_Addr_Line_1 = localVariables.newFieldInRecord("pnd_Old_Bad_Addr_Line_1", "#OLD-BAD-ADDR-LINE-1", FieldType.STRING, 35);
        pnd_Old_Bad_Addr_Line_2 = localVariables.newFieldInRecord("pnd_Old_Bad_Addr_Line_2", "#OLD-BAD-ADDR-LINE-2", FieldType.STRING, 35);
        pnd_Old_Bad_Addr_Line_3 = localVariables.newFieldInRecord("pnd_Old_Bad_Addr_Line_3", "#OLD-BAD-ADDR-LINE-3", FieldType.STRING, 35);
        pnd_Old_Bad_Addr_Line_4 = localVariables.newFieldInRecord("pnd_Old_Bad_Addr_Line_4", "#OLD-BAD-ADDR-LINE-4", FieldType.STRING, 35);
        pnd_Old_Bad_Addr_Line_5 = localVariables.newFieldInRecord("pnd_Old_Bad_Addr_Line_5", "#OLD-BAD-ADDR-LINE-5", FieldType.STRING, 35);
        pnd_Wrk_Business_Date = localVariables.newFieldInRecord("pnd_Wrk_Business_Date", "#WRK-BUSINESS-DATE", FieldType.STRING, 8);

        pnd_Wrk_Business_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Wrk_Business_Date__R_Field_4", "REDEFINE", pnd_Wrk_Business_Date);
        pnd_Wrk_Business_Date_Pnd_Wrk_Business_Date_Mm = pnd_Wrk_Business_Date__R_Field_4.newFieldInGroup("pnd_Wrk_Business_Date_Pnd_Wrk_Business_Date_Mm", 
            "#WRK-BUSINESS-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Wrk_Business_Date_Pnd_Wrk_Business_Date_Dd = pnd_Wrk_Business_Date__R_Field_4.newFieldInGroup("pnd_Wrk_Business_Date_Pnd_Wrk_Business_Date_Dd", 
            "#WRK-BUSINESS-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Wrk_Business_Date_Pnd_Wrk_Business_Date_Yyyy = pnd_Wrk_Business_Date__R_Field_4.newFieldInGroup("pnd_Wrk_Business_Date_Pnd_Wrk_Business_Date_Yyyy", 
            "#WRK-BUSINESS-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Read = localVariables.newFieldInRecord("pnd_Read", "#READ", FieldType.PACKED_DECIMAL, 9);
        pnd_Update_Cnt = localVariables.newFieldInRecord("pnd_Update_Cnt", "#UPDATE-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Error_Cnt = localVariables.newFieldInRecord("pnd_Error_Cnt", "#ERROR-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Old_City_State = localVariables.newFieldInRecord("pnd_Old_City_State", "#OLD-CITY-STATE", FieldType.STRING, 35);

        pnd_Old_City_State__R_Field_5 = localVariables.newGroupInRecord("pnd_Old_City_State__R_Field_5", "REDEFINE", pnd_Old_City_State);
        pnd_Old_City_State_Pnd_Old_City = pnd_Old_City_State__R_Field_5.newFieldInGroup("pnd_Old_City_State_Pnd_Old_City", "#OLD-CITY", FieldType.STRING, 
            33);
        pnd_Old_City_State_Pnd_Old_State = pnd_Old_City_State__R_Field_5.newFieldInGroup("pnd_Old_City_State_Pnd_Old_State", "#OLD-STATE", FieldType.STRING, 
            2);
        pnd_Compress_Old_City_State = localVariables.newFieldInRecord("pnd_Compress_Old_City_State", "#COMPRESS-OLD-CITY-STATE", FieldType.STRING, 35);

        pnd_Bypass_Out = localVariables.newGroupInRecord("pnd_Bypass_Out", "#BYPASS-OUT");
        pnd_Bypass_Out_Pnd_Bypass_Pin_O = pnd_Bypass_Out.newFieldInGroup("pnd_Bypass_Out_Pnd_Bypass_Pin_O", "#BYPASS-PIN-O", FieldType.NUMERIC, 12);
        pnd_Bypass_Out_Pnd_Filler = pnd_Bypass_Out.newFieldInGroup("pnd_Bypass_Out_Pnd_Filler", "#FILLER", FieldType.STRING, 73);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 12);
        pnd_Same_Address = localVariables.newFieldInRecord("pnd_Same_Address", "#SAME-ADDRESS", FieldType.PACKED_DECIMAL, 9);
        pnd_Dod_Vendor = localVariables.newFieldInRecord("pnd_Dod_Vendor", "#DOD-VENDOR", FieldType.PACKED_DECIMAL, 9);
        pnd_No_Address = localVariables.newFieldInRecord("pnd_No_Address", "#NO-ADDRESS", FieldType.PACKED_DECIMAL, 9);
        pnd_Qs = localVariables.newFieldInRecord("pnd_Qs", "#QS", FieldType.PACKED_DECIMAL, 9);
        pnd_Old_Address = localVariables.newFieldInRecord("pnd_Old_Address", "#OLD-ADDRESS", FieldType.STRING, 35);
        pnd_New_Address = localVariables.newFieldInRecord("pnd_New_Address", "#NEW-ADDRESS", FieldType.STRING, 35);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaNasl747a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Nasb666() throws Exception
    {
        super("Nasb666");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* *
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 55;//Natural: FORMAT ( 1 ) LS = 132 PS = 55;//Natural: FORMAT ( 2 ) LS = 132 PS = 55
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  OPEN MQ TO FACILITATE MDM CALL                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*  READ BATCH RESEARCH LETTER EXTRACT
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #BAD-OUTPUT
        while (condition(getWorkFiles().read(1, ldaNasl747a.getPnd_Bad_Output())))
        {
            pnd_Read.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #READ
            //*  FOR PINE, REFORMAT INPUT ALPHA PIN TO NUMERIC
            ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Pin_N12().compute(new ComputeParameters(false, ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Pin_N12()),              //Natural: ASSIGN #BAD-ADDR-PIN-N12 := VAL ( #BAD-ADDR-PIN-A12 )
                ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Pin_A12().val());
            pnd_Compress_Old_City_State.reset();                                                                                                                          //Natural: RESET #COMPRESS-OLD-CITY-STATE
            pnd_Old_New_Addr_Line_1.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1());                                                                    //Natural: ASSIGN #OLD-NEW-ADDR-LINE-1 := #TIAA-STD-ADDR-LINE1
            if (condition(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line3().greater(" ")))                                                                          //Natural: IF #TIAA-STD-ADDR-LINE3 > ' '
            {
                pnd_Old_New_Addr_Line_3.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line3());                                                                //Natural: ASSIGN #OLD-NEW-ADDR-LINE-3 := #TIAA-STD-ADDR-LINE3
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Old_New_Addr_Line_2.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line2());                                                                //Natural: ASSIGN #OLD-NEW-ADDR-LINE-2 := #TIAA-STD-ADDR-LINE2
            }                                                                                                                                                             //Natural: END-IF
            pnd_Old_City_State.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_City_State());                                                                         //Natural: ASSIGN #OLD-CITY-STATE := #BAD-ADDR-CITY-STATE
            pnd_Old_Bad_Addr_Line_1.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Line_1());                                                                        //Natural: ASSIGN #OLD-BAD-ADDR-LINE-1 := #BAD-ADDR-LINE-1
            pnd_Old_Bad_Addr_Line_2.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Line_2());                                                                        //Natural: ASSIGN #OLD-BAD-ADDR-LINE-2 := #BAD-ADDR-LINE-2
            pnd_Old_Bad_Addr_Line_3.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Line_3());                                                                        //Natural: ASSIGN #OLD-BAD-ADDR-LINE-3 := #BAD-ADDR-LINE-3
            pnd_Old_Bad_Addr_Line_4.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Line_4());                                                                        //Natural: ASSIGN #OLD-BAD-ADDR-LINE-4 := #BAD-ADDR-LINE-4
            pnd_Compress_Old_City_State.setValue(DbsUtil.compress(pnd_Old_City_State_Pnd_Old_City, ",", pnd_Old_City_State_Pnd_Old_State));                               //Natural: COMPRESS #OLD-CITY ',' #OLD-STATE TO #COMPRESS-OLD-CITY-STATE
            pnd_Old_Bad_Addr_Line_5.setValue(pnd_Compress_Old_City_State);                                                                                                //Natural: ASSIGN #OLD-BAD-ADDR-LINE-5 := #COMPRESS-OLD-CITY-STATE
            pnd_Remarks.reset();                                                                                                                                          //Natural: RESET #REMARKS
            short decideConditionsMet367 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #VENDOR-STREET-ADDR = ' '
            if (condition(ldaNasl747a.getPnd_Bad_Output_Pnd_Vendor_Street_Addr().equals(" ")))
            {
                decideConditionsMet367++;
                pnd_Remarks.setValue("No Address");                                                                                                                       //Natural: ASSIGN #REMARKS := 'No Address'
                pnd_No_Address.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NO-ADDRESS
            }                                                                                                                                                             //Natural: WHEN #TIAA-STD-CARRIER-RTE = ' '
            if (condition(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Carrier_Rte().equals(" ")))
            {
                decideConditionsMet367++;
                pnd_Remarks.setValue("Did not pass QS");                                                                                                                  //Natural: ASSIGN #REMARKS := 'Did not pass QS'
                pnd_Qs.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #QS
            }                                                                                                                                                             //Natural: WHEN #VENDOR-DECEASED-INFO NE ' '
            if (condition(ldaNasl747a.getPnd_Bad_Output_Pnd_Vendor_Deceased_Info().notEquals(" ")))
            {
                decideConditionsMet367++;
                pnd_Remarks.setValue("DOD Accurint");                                                                                                                     //Natural: ASSIGN #REMARKS := 'DOD Accurint'
                pnd_Dod_Vendor.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #DOD-VENDOR
            }                                                                                                                                                             //Natural: WHEN #BAD-ADDR-ZIP-PLUS-4 = #TIAA-STD-ZIP-PLUS4
            if (condition(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4().equals(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4())))
            {
                decideConditionsMet367++;
                pnd_Remarks.setValue("Same Address");                                                                                                                     //Natural: ASSIGN #REMARKS := 'Same Address'
                pnd_Same_Address.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #SAME-ADDRESS
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet367 > 0))
            {
                if (condition(pnd_Remarks.greater(" ")))                                                                                                                  //Natural: IF #REMARKS GT ' '
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-REPORT
                    sub_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet367 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  CHECK IF PIN IS DOD IN IA - REMOVED FOR PINE (AFTER CHANGING TO
            //*  USE VAL FOR ALPHA/NUMERIC COMPARISON).
            //*  #IA-PH-UNIQUE-ID-NBR := VAL (#BAD-ADDR-PH-UNQUE-ID-NMBR)
            //*  RESET #DOD
            //*  READ (1)IAPIN BY PIN-CNTRCT-PAYEE-KEY STARTING FROM #IA-SUPER
            //*    IF CPR-ID-NBR NE VAL (#BAD-ADDR-PH-UNQUE-ID-NMBR)
            //*      ESCAPE BOTTOM
            //*    END-IF
            //*    #IA-CONTRACT :=  CNTRCT-PART-PPCN-NBR
            //*    FIND IADOD WITH CNTRCT-PPCN-NBR = #IA-CONTRACT
            //*      IF NO RECORDS FOUND
            //*        ESCAPE ROUTINE
            //*      END-NOREC
            //*      IF CNTRCT-FIRST-ANNT-DOD-DTE GT 0 OR
            //*          CNTRCT-SCND-ANNT-DOD-DTE GT 0
            //*        #DOD:=TRUE
            //*        ADD 1 TO #IA-DOD
            //*        #REMARKS := 'IA DOD'
            //*      END-IF
            //*    END-FIND
            //*  END-READ
            //*  IF #DOD
            //*    PERFORM ERROR-REPORT
            //*  ELSE
            //*  RESTRUCTURE ADDRESS FOR CWF FOLDER
            if (condition(pnd_Old_Bad_Addr_Line_4.greater("  ")))                                                                                                         //Natural: IF #OLD-BAD-ADDR-LINE-4 > '  '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Old_Bad_Addr_Line_3.greater("  ")))                                                                                                     //Natural: IF #OLD-BAD-ADDR-LINE-3 > '  '
                {
                    pnd_Old_Bad_Addr_Line_4.setValue(pnd_Compress_Old_City_State);                                                                                        //Natural: MOVE #COMPRESS-OLD-CITY-STATE TO #OLD-BAD-ADDR-LINE-4
                    pnd_Old_Bad_Addr_Line_5.reset();                                                                                                                      //Natural: RESET #OLD-BAD-ADDR-LINE-5
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Old_Bad_Addr_Line_2.greater("  ")))                                                                                                 //Natural: IF #OLD-BAD-ADDR-LINE-2 > '  '
                    {
                        pnd_Old_Bad_Addr_Line_3.setValue(pnd_Compress_Old_City_State);                                                                                    //Natural: MOVE #COMPRESS-OLD-CITY-STATE TO #OLD-BAD-ADDR-LINE-3
                        pnd_Old_Bad_Addr_Line_4.reset();                                                                                                                  //Natural: RESET #OLD-BAD-ADDR-LINE-4 #OLD-BAD-ADDR-LINE-5
                        pnd_Old_Bad_Addr_Line_5.reset();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Old_Bad_Addr_Line_1.greater("  ")))                                                                                             //Natural: IF #OLD-BAD-ADDR-LINE-1 > '  '
                        {
                            pnd_Old_Bad_Addr_Line_2.setValue(pnd_Compress_Old_City_State);                                                                                //Natural: MOVE #COMPRESS-OLD-CITY-STATE TO #OLD-BAD-ADDR-LINE-2
                            pnd_Old_Bad_Addr_Line_3.reset();                                                                                                              //Natural: RESET #OLD-BAD-ADDR-LINE-3 #OLD-BAD-ADDR-LINE-4 #OLD-BAD-ADDR-LINE-5
                            pnd_Old_Bad_Addr_Line_4.reset();
                            pnd_Old_Bad_Addr_Line_5.reset();
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*    END-IF
                //*  CREATE CWF
                                                                                                                                                                          //Natural: PERFORM C2000-CWF-CREATION
                sub_C2000_Cwf_Creation();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM UPDATE-REPORT
                sub_Update_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   COMPRESS #BAD-ADDR-PH-UNQUE-ID-NMBR #TIAA-STD-ADDR-LINE1
                //*   PINE: HAD TO TRUNCATE ADDRESS PORTION OF KEY TO FIT 12 BYTE PIN
                ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Sort_Key().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Pin_N12(),  //Natural: COMPRESS #BAD-ADDR-PIN-N12 #TIAA-STD-ADDR-LINE1-A30 INTO #TIAA-STD-ADDR-SORT-KEY LEAVING NO SPACE
                    ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1_A30()));
                getWorkFiles().write(3, false, ldaNasl747a.getPnd_Bad_Output());                                                                                          //Natural: WRITE WORK FILE 3 #BAD-OUTPUT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C2000-CWF-CREATION
        //* ***********************************************************************
        //*  MDMA190.#GDA-USER-ID    := 'NASB752'
        //* ************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C3000-MOVE-DOC-TEXT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C3100-WRITE-CONTRACT-LINE
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-REPORT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-REPORT
        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,"Total records read                       ",pnd_Read, new ReportEditMask ("Z,ZZZ,Z99"));                               //Natural: WRITE ( 1 ) 'Total records read                       ' #READ ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        //*  WRITE (1) 'Total records with IA DOD                ' /* PINE
        //*   #ERROR-CNT (EM=Z,ZZZ,Z99)
        getReports().write(1, ReportOption.NOTITLE,"Total records with Accurint DOD          ",pnd_Dod_Vendor, new ReportEditMask ("Z,ZZZ,Z99"));                         //Natural: WRITE ( 1 ) 'Total records with Accurint DOD          ' #DOD-VENDOR ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total records with No address - Accurint ",pnd_No_Address, new ReportEditMask ("Z,ZZZ,Z99"));                         //Natural: WRITE ( 1 ) 'Total records with No address - Accurint ' #NO-ADDRESS ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total records with Same address          ",pnd_Same_Address, new ReportEditMask ("Z,ZZZ,Z99"));                       //Natural: WRITE ( 1 ) 'Total records with Same address          ' #SAME-ADDRESS ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total records failed QS                  ",pnd_Qs, new ReportEditMask ("Z,ZZZ,Z99"));                                 //Natural: WRITE ( 1 ) 'Total records failed QS                  ' #QS ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Records Written out                ",pnd_Update_Cnt, new ReportEditMask ("Z,ZZZ,Z99"));                         //Natural: WRITE ( 1 ) 'Total Records Written out                ' #UPDATE-CNT ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total records read                       ",pnd_Read, new ReportEditMask ("Z,ZZZ,Z99"));                               //Natural: WRITE ( 2 ) 'Total records read                       ' #READ ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        //*  WRITE (2) 'Total records with IA DOD                ' /* PINE
        //*   #IA-DOD    (EM=Z,ZZZ,Z99)
        getReports().write(2, ReportOption.NOTITLE,"Total records with Accurint DOD          ",pnd_Dod_Vendor, new ReportEditMask ("Z,ZZZ,Z99"));                         //Natural: WRITE ( 2 ) 'Total records with Accurint DOD          ' #DOD-VENDOR ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total records with No address - Accurint ",pnd_No_Address, new ReportEditMask ("Z,ZZZ,Z99"));                         //Natural: WRITE ( 2 ) 'Total records with No address - Accurint ' #NO-ADDRESS ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total records with Same address          ",pnd_Same_Address, new ReportEditMask ("Z,ZZZ,Z99"));                       //Natural: WRITE ( 2 ) 'Total records with Same address          ' #SAME-ADDRESS ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total records failed QS                  ",pnd_Qs, new ReportEditMask ("Z,ZZZ,Z99"));                                 //Natural: WRITE ( 1 ) 'Total records failed QS                  ' #QS ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total Records Written out                ",pnd_Update_Cnt, new ReportEditMask ("Z,ZZZ,Z99"));                         //Natural: WRITE ( 2 ) 'Total Records Written out                ' #UPDATE-CNT ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
    }
    //*  PINE - USED CORRECT PGM NAME
    //*  PINE
    private void sub_C2000_Cwf_Creation() throws Exception                                                                                                                //Natural: C2000-CWF-CREATION
    {
        if (BLNatReinput.isReinput()) return;

        pdaMdma190.getMdma190_Pnd_Et_Ind().setValue("N");                                                                                                                 //Natural: ASSIGN MDMA190.#ET-IND := 'N'
        pdaMdma190.getMdma190_Pnd_Wpid_System().setValue("NAS");                                                                                                          //Natural: ASSIGN MDMA190.#WPID-SYSTEM := 'NAS'
        pdaMdma190.getMdma190_Pnd_Gda_User_Id().setValue("NASB666");                                                                                                      //Natural: ASSIGN MDMA190.#GDA-USER-ID := 'NASB666'
        pdaMdma190.getMdma190_Pnd_Gda_Slo_Exists().setValue("N");                                                                                                         //Natural: ASSIGN MDMA190.#GDA-SLO-EXISTS := 'N'
        pdaMdma190.getMdma190_Pnd_Wpid_Pin().setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Pin_N12());                                                              //Natural: ASSIGN MDMA190.#WPID-PIN := #BAD-ADDR-PIN-N12
        pdaMdma190.getMdma190_Pnd_Wpid_Status().setValue("8000");                                                                                                         //Natural: ASSIGN MDMA190.#WPID-STATUS := '8000'
        pdaMdma190.getMdma190_Pnd_Wpid_Unit().setValue("CDMSA D");                                                                                                        //Natural: ASSIGN MDMA190.#WPID-UNIT := MDMA190.#WPID-NEXT-UNIT := 'CDMSA D'
        pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit().setValue("CDMSA D");
        //*  CREF PARTICIPANT REGULATORY
        //*  ADDRESS CHANGE TNG LETTER
        if (condition(Global.getDATN().greaterOrEqual(20150101) && Global.getDATN().lessOrEqual(20150531)))                                                               //Natural: IF *DATN = 20150101 THRU 20150531
        {
            pdaMdma190.getMdma190_Pnd_Wpid().setValue("TA HTA");                                                                                                          //Natural: ASSIGN MDMA190.#WPID := 'TA HTA'
            //*  ADDRESS CHANGE (DEFAULT)
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaMdma190.getMdma190_Pnd_Wpid().setValue("TA HV");                                                                                                           //Natural: ASSIGN MDMA190.#WPID := 'TA HV'
        }                                                                                                                                                                 //Natural: END-IF
        pdaNasa191.getNasa191().reset();                                                                                                                                  //Natural: RESET NASA191
        pdaNasa191.getNasa191_Pnd_Action().setValue("AN");                                                                                                                //Natural: ASSIGN NASA191.#ACTION := 'AN'
        pdaNasa191.getNasa191_Pnd_Doc_Category().setValue("M");                                                                                                           //Natural: ASSIGN NASA191.#DOC-CATEGORY := 'M'
        pdaNasa191.getNasa191_Pnd_Doc_Class().setValue("ADD");                                                                                                            //Natural: ASSIGN NASA191.#DOC-CLASS := 'ADD'
        pdaNasa191.getNasa191_Pnd_Doc_Direction().setValue("N");                                                                                                          //Natural: ASSIGN NASA191.#DOC-DIRECTION := 'N'
        pdaNasa191.getNasa191_Pnd_Doc_Format_Cde().setValue("T");                                                                                                         //Natural: ASSIGN NASA191.#DOC-FORMAT-CDE := 'T'
        pdaNasa191.getNasa191_Pnd_Doc_Retention_Cde().setValue("P");                                                                                                      //Natural: ASSIGN NASA191.#DOC-RETENTION-CDE := 'P'
        pdaNasa191.getNasa191_Pnd_Last_Page_Flag().setValue("Y");                                                                                                         //Natural: ASSIGN NASA191.#LAST-PAGE-FLAG := 'Y'
        pdaNasa191.getNasa191_Pnd_Batch_Nbr().setValue(1);                                                                                                                //Natural: ASSIGN NASA191.#BATCH-NBR := 001
                                                                                                                                                                          //Natural: PERFORM C3000-MOVE-DOC-TEXT
        sub_C3000_Move_Doc_Text();
        if (condition(Global.isEscape())) {return;}
        //*  PINE
        DbsUtil.callnat(Mdmn191.class , getCurrentProcessState(), pdaMdma190.getMdma190(), pdaNasa191.getNasa191());                                                      //Natural: CALLNAT 'MDMN191' USING MDMA190 NASA191
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma190.getMdma190_Pnd_Error_Message().notEquals(" ")))                                                                                          //Natural: IF MDMA190.#ERROR-MESSAGE NE ' '
        {
            //*  PINE
            getReports().write(0, "CWF Error -",pdaMdma190.getMdma190_Pnd_Error_Message(),ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Pin_N12());                          //Natural: WRITE 'CWF Error -' MDMA190.#ERROR-MESSAGE #BAD-ADDR-PIN-N12
            if (Global.isEscape()) return;
            pnd_Switches_Flags_Pnd_Error_Ind.setValue(true);                                                                                                              //Natural: ASSIGN #ERROR-IND := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  C1000-CWF-CREATION
    }
    private void sub_C3000_Move_Doc_Text() throws Exception                                                                                                               //Natural: C3000-MOVE-DOC-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        //* * #WK-EFF-DATE-YYYYMM                  := #NEW-EFF-DATE
        //* *  #WRK-EFF-DATE-DD                      := 01
        pnd_Wrk_Cwf_Date_Pnd_Wrk_Cwf_Date_Mm.setValue(pnd_Wrk_Business_Date_Pnd_Wrk_Business_Date_Mm);                                                                    //Natural: MOVE #WRK-BUSINESS-DATE-MM TO #WRK-CWF-DATE-MM
        pnd_Wrk_Cwf_Date_Pnd_Wrk_Cwf_Date_Dd.setValue(pnd_Wrk_Business_Date_Pnd_Wrk_Business_Date_Dd);                                                                    //Natural: MOVE #WRK-BUSINESS-DATE-DD TO #WRK-CWF-DATE-DD
        pnd_Wrk_Cwf_Date_Pnd_Wrk_Cwf_Date_Yyyy.setValue(pnd_Wrk_Business_Date_Pnd_Wrk_Business_Date_Yyyy);                                                                //Natural: MOVE #WRK-BUSINESS-DATE-YYYY TO #WRK-CWF-DATE-YYYY
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(1,":",60).reset();                                                                                                  //Natural: RESET NASA191.#DOC-TEXT ( 1:60 )
        FOR01:                                                                                                                                                            //Natural: FOR #INDEX EQ 1 TO 18
        for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(18)); pnd_Index.nadd(1))
        {
            pnd_W_Doc_Text.reset();                                                                                                                                       //Natural: RESET #W-DOC-TEXT
            short decideConditionsMet548 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #INDEX;//Natural: VALUE 1
            if (condition((pnd_Index.equals(1))))
            {
                decideConditionsMet548++;
                pnd_W_Doc_Text_Pnd_W_Ln1_Program.setValue(Global.getPROGRAM());                                                                                           //Natural: ASSIGN #W-LN1-PROGRAM := *PROGRAM
                pnd_W_Doc_Text_Pnd_W_Ln1_Title1.setValue("Name and Address System");                                                                                      //Natural: ASSIGN #W-LN1-TITLE1 := 'Name and Address System'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Index.equals(2))))
            {
                decideConditionsMet548++;
                pnd_W_Doc_Text_Pnd_W_Ln2_Title1.setValue(" Batch Research Update Processing Report  ");                                                                   //Natural: ASSIGN #W-LN2-TITLE1 := ' Batch Research Update Processing Report  '
                pnd_W_Doc_Text_Pnd_W_Ln2_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                                            //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO #W-LN2-TIME
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Index.equals(3))))
            {
                decideConditionsMet548++;
                pnd_Wrk_Date_Edited.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                    //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #WRK-DATE-EDITED
                setValueToSubstring("on",pnd_W_Doc_Text,32,2);                                                                                                            //Natural: MOVE 'on' TO SUBSTR ( #W-DOC-TEXT,32,2 )
                setValueToSubstring(pnd_Wrk_Date_Edited,pnd_W_Doc_Text,35,10);                                                                                            //Natural: MOVE #WRK-DATE-EDITED TO SUBSTR ( #W-DOC-TEXT,35,10 )
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Index.equals(5))))
            {
                decideConditionsMet548++;
                pnd_W_Doc_Text.setValue("The Post Office returned mail to us, which was mailed to your old address.");                                                    //Natural: ASSIGN #W-DOC-TEXT := 'The Post Office returned mail to us, which was mailed to your old address.'
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_Index.equals(6))))
            {
                decideConditionsMet548++;
                pnd_W_Doc_Text.setValue("After our research, we found a new address and changed our Corporate records to reflect");                                       //Natural: ASSIGN #W-DOC-TEXT := 'After our research, we found a new address and changed our Corporate records to reflect'
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pnd_Index.equals(7))))
            {
                decideConditionsMet548++;
                pnd_W_Doc_Text.setValue("your new address and sent a confirmation letter to that address.");                                                              //Natural: ASSIGN #W-DOC-TEXT := 'your new address and sent a confirmation letter to that address.'
            }                                                                                                                                                             //Natural: VALUE 9
            else if (condition((pnd_Index.equals(9))))
            {
                decideConditionsMet548++;
                setValueToSubstring("New Address",pnd_W_Doc_Text,1,12);                                                                                                   //Natural: MOVE 'New Address' TO SUBSTR ( #W-DOC-TEXT,01,12 )
                setValueToSubstring("Old Address",pnd_W_Doc_Text,46,12);                                                                                                  //Natural: MOVE 'Old Address' TO SUBSTR ( #W-DOC-TEXT,46,12 )
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_Index.equals(10))))
            {
                decideConditionsMet548++;
                setValueToSubstring("___________________________________",pnd_W_Doc_Text,1,35);                                                                           //Natural: MOVE '___________________________________' TO SUBSTR ( #W-DOC-TEXT,1,35 )
                setValueToSubstring("___________________________________",pnd_W_Doc_Text,46,35);                                                                          //Natural: MOVE '___________________________________' TO SUBSTR ( #W-DOC-TEXT,46,35 )
            }                                                                                                                                                             //Natural: VALUE 11
            else if (condition((pnd_Index.equals(11))))
            {
                decideConditionsMet548++;
                setValueToSubstring(pnd_Old_New_Addr_Line_1,pnd_W_Doc_Text,1,35);                                                                                         //Natural: MOVE #OLD-NEW-ADDR-LINE-1 TO SUBSTR ( #W-DOC-TEXT,01,35 )
                setValueToSubstring(pnd_Old_Bad_Addr_Line_1,pnd_W_Doc_Text,46,35);                                                                                        //Natural: MOVE #OLD-BAD-ADDR-LINE-1 TO SUBSTR ( #W-DOC-TEXT,46,35 )
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Index.equals(12))))
            {
                decideConditionsMet548++;
                setValueToSubstring(pnd_Old_New_Addr_Line_2,pnd_W_Doc_Text,1,35);                                                                                         //Natural: MOVE #OLD-NEW-ADDR-LINE-2 TO SUBSTR ( #W-DOC-TEXT,01,35 )
                if (condition(pnd_Old_Bad_Addr_Line_2.greater(" ")))                                                                                                      //Natural: IF #OLD-BAD-ADDR-LINE-2 GT ' '
                {
                    setValueToSubstring(pnd_Old_Bad_Addr_Line_2,pnd_W_Doc_Text,46,35);                                                                                    //Natural: MOVE #OLD-BAD-ADDR-LINE-2 TO SUBSTR ( #W-DOC-TEXT,46,35 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 13
            else if (condition((pnd_Index.equals(13))))
            {
                decideConditionsMet548++;
                setValueToSubstring(pnd_Old_New_Addr_Line_3,pnd_W_Doc_Text,1,35);                                                                                         //Natural: MOVE #OLD-NEW-ADDR-LINE-3 TO SUBSTR ( #W-DOC-TEXT,01,35 )
                if (condition(pnd_Old_Bad_Addr_Line_3.greater(" ")))                                                                                                      //Natural: IF #OLD-BAD-ADDR-LINE-3 GT ' '
                {
                    setValueToSubstring(pnd_Old_Bad_Addr_Line_3,pnd_W_Doc_Text,46,35);                                                                                    //Natural: MOVE #OLD-BAD-ADDR-LINE-3 TO SUBSTR ( #W-DOC-TEXT,46,35 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 14
            else if (condition((pnd_Index.equals(14))))
            {
                decideConditionsMet548++;
                if (condition(pnd_Old_Bad_Addr_Line_4.greater(" ")))                                                                                                      //Natural: IF #OLD-BAD-ADDR-LINE-4 GT ' '
                {
                    setValueToSubstring(pnd_Old_Bad_Addr_Line_4,pnd_W_Doc_Text,46,35);                                                                                    //Natural: MOVE #OLD-BAD-ADDR-LINE-4 TO SUBSTR ( #W-DOC-TEXT,46,35 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 15
            else if (condition((pnd_Index.equals(15))))
            {
                decideConditionsMet548++;
                if (condition(pnd_Old_Bad_Addr_Line_5.greater(" ")))                                                                                                      //Natural: IF #OLD-BAD-ADDR-LINE-5 GT ' '
                {
                    setValueToSubstring(pnd_Old_Bad_Addr_Line_5,pnd_W_Doc_Text,46,35);                                                                                    //Natural: MOVE #OLD-BAD-ADDR-LINE-5 TO SUBSTR ( #W-DOC-TEXT,46,35 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 16
            else if (condition((pnd_Index.equals(16))))
            {
                decideConditionsMet548++;
                setValueToSubstring("The file change was applied on:",pnd_W_Doc_Text,1,31);                                                                               //Natural: MOVE 'The file change was applied on:' TO SUBSTR ( #W-DOC-TEXT,01,31 )
                pnd_Date_A.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                             //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #DATE-A
                setValueToSubstring(pnd_Date_A,pnd_W_Doc_Text,33,10);                                                                                                     //Natural: MOVE #DATE-A TO SUBSTR ( #W-DOC-TEXT,33,10 )
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet548 > 0))
            {
                pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(pnd_Index).setValue(pnd_W_Doc_Text);                                                                        //Natural: ASSIGN NASA191.#DOC-TEXT ( #INDEX ) := #W-DOC-TEXT
                pnd_W_Doc_Text.reset();                                                                                                                                   //Natural: RESET #W-DOC-TEXT
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  C3000-MOVE-DOC-TEXT  /* FE0601 END
    }
    private void sub_C3100_Write_Contract_Line() throws Exception                                                                                                         //Natural: C3100-WRITE-CONTRACT-LINE
    {
        if (BLNatReinput.isReinput()) return;

        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(pnd_Index).setValue(pnd_W_Doc_Text);                                                                                //Natural: ASSIGN NASA191.#DOC-TEXT ( #INDEX ) := #W-DOC-TEXT
        pnd_Index.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #INDEX
        pnd_W_Doc_Text.reset();                                                                                                                                           //Natural: RESET #W-DOC-TEXT
        //*  C3100-WRITE-CONTRACT-LINE
    }
    private void sub_Update_Report() throws Exception                                                                                                                     //Natural: UPDATE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(getReports().getAstLinesLeft(1).less(3)))                                                                                                           //Natural: NEWPAGE ( 1 ) IF LESS THAN 3 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Old_Address.reset();                                                                                                                                          //Natural: RESET #OLD-ADDRESS #NEW-ADDRESS
        pnd_New_Address.reset();
        pnd_Old_Address.setValue(DbsUtil.compress(pnd_Compress_Old_City_State, ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4()));                                 //Natural: COMPRESS #COMPRESS-OLD-CITY-STATE #BAD-ADDR-ZIP-PLUS-4 INTO #OLD-ADDRESS
        pnd_New_Address.setValue(DbsUtil.compress(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line2(), ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4()));      //Natural: COMPRESS #TIAA-STD-ADDR-LINE2 #TIAA-STD-ZIP-PLUS4 INTO #NEW-ADDRESS
        //*  PINE
        getReports().write(1, ReportOption.NOTITLE,ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Pin_N12(),new TabSetting(15),ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Line_1(),new  //Natural: WRITE ( 1 ) #BAD-ADDR-PIN-N12 015T #BAD-ADDR-LINE-1 061T #TIAA-STD-ADDR-LINE1 / 015T #OLD-ADDRESS 061T #NEW-ADDRESS
            TabSetting(61),ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1(),NEWLINE,new TabSetting(15),pnd_Old_Address,new TabSetting(61),pnd_New_Address);
        if (Global.isEscape()) return;
        pnd_Update_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #UPDATE-CNT
    }
    private void sub_Error_Report() throws Exception                                                                                                                      //Natural: ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(getReports().getAstLinesLeft(2).less(3)))                                                                                                           //Natural: NEWPAGE ( 2 ) IF LESS THAN 3 LINES LEFT
        {
            getReports().newPage(2);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Old_Address.reset();                                                                                                                                          //Natural: RESET #OLD-ADDRESS #NEW-ADDRESS
        pnd_New_Address.reset();
        pnd_Old_Address.setValue(DbsUtil.compress(pnd_Compress_Old_City_State, ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4()));                                 //Natural: COMPRESS #COMPRESS-OLD-CITY-STATE #BAD-ADDR-ZIP-PLUS-4 INTO #OLD-ADDRESS
        pnd_New_Address.setValue(DbsUtil.compress(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line2(), ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Zip_Plus4()));      //Natural: COMPRESS #TIAA-STD-ADDR-LINE2 #TIAA-STD-ZIP-PLUS4 INTO #NEW-ADDRESS
        //*  PINE
        getReports().write(2, ReportOption.NOTITLE,ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Pin_N12(),new TabSetting(15),ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Line_1(),new  //Natural: WRITE ( 2 ) #BAD-ADDR-PIN-N12 015T #BAD-ADDR-LINE-1 061T #TIAA-STD-ADDR-LINE1 123T #REMARKS / 015T #OLD-ADDRESS 061T #NEW-ADDRESS
            TabSetting(61),ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1(),new TabSetting(123),pnd_Remarks,NEWLINE,new TabSetting(15),pnd_Old_Address,new 
            TabSetting(61),pnd_New_Address);
        if (Global.isEscape()) return;
        pnd_Error_Cnt.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #ERROR-CNT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(42),"NAS - Batch Reserach CWF - Letter Update Report",new  //Natural: WRITE ( 1 ) NOTITLE 001T *PROGRAM 042T 'NAS - Batch Reserach CWF - Letter Update Report' 121T 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 121T *TIMX ( EM = HH:II:SS�AP ) / 'PIN' 010T 'Old Address' 056T 'New Address' / '-' ( 7 ) 010T '-' ( 35 ) 056T '-' ( 35 )
                        TabSetting(121),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(121),Global.getTIMX(), new ReportEditMask ("HH:II:SS AP"),NEWLINE,"PIN",new TabSetting(10),"Old Address",new 
                        TabSetting(56),"New Address",NEWLINE,"-",new RepeatItem(7),new TabSetting(10),"-",new RepeatItem(35),new TabSetting(56),"-",new 
                        RepeatItem(35));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(42),"NAS - Batch Reserach CWF - Letter Error Report",new  //Natural: WRITE ( 2 ) NOTITLE 001T *PROGRAM 042T 'NAS - Batch Reserach CWF - Letter Error Report' 121T 'Page:' *PAGE-NUMBER ( 2 ) ( EM = ZZZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 121T *TIMX ( EM = HH:II:SS�AP ) / 'PIN' 010T 'Old Address' 056T 'New Address' 117T 'Remarks'/ '-' ( 7 ) 010T '-' ( 35 ) 056T '-' ( 35 ) 117T '-' ( 7 )
                        TabSetting(121),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(121),Global.getTIMX(), new ReportEditMask ("HH:II:SS AP"),NEWLINE,"PIN",new TabSetting(10),"Old Address",new 
                        TabSetting(56),"New Address",new TabSetting(117),"Remarks",NEWLINE,"-",new RepeatItem(7),new TabSetting(10),"-",new RepeatItem(35),new 
                        TabSetting(56),"-",new RepeatItem(35),new TabSetting(117),"-",new RepeatItem(7));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=55");
        Global.format(1, "LS=132 PS=55");
        Global.format(2, "LS=132 PS=55");
    }
}
