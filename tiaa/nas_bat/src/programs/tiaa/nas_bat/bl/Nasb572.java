/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:59:51 PM
**        * FROM NATURAL PROGRAM : Nasb572
************************************************************
**        * FILE NAME            : Nasb572.java
**        * CLASS NAME           : Nasb572
**        * INSTANCE NAME        : Nasb572
************************************************************
***********************************************************************
** PROGRAM    : NASB572
** AUTHOR     : ROSALIE GO-PAZ
** PURPOSE    : ROSTER MAPPING. CREATE CWF REQUEST AND DOC
**              FOR ADDRESS THAT FAILS
** HISTORY    :
**
** SZ         11/10/2011
**            MODIFICATIONS FOR RESIDENTIAL ADDRESS REJECTS
**            CWF ADDRESS UPDATES. RECEIVE TWO NEW FIELDS SYSTEM NAME
**            AND ADDRESS TYPE TO MAKE THE CONTENT OF THE ELECTORIC
**            DOCUMENT DYNAMIC AS OPPOSED TO CURRENT VALUE ROSTER
**            MAPPING.
**            PLEASE LOOK FOR LABEL SZRESADD
**
** SZ         12/03/2014
**            ADDING SEVERAL NEW SOURCES E.G. NIA FOR NEW ISSUE IA,
**            UD FOR UNIFIED DESKTOP, SBL FOR SIEBEL ETC....
**            PLEASE LOOK FOR LABEL SZADDSRC
**            ADDED CONTROL LOGIC WIHT VARIOUS COUNTERS
**            PLEASE LOOK FOR LABEL SZCNTRLS
**
**
** SZ         05/13/2015 PROD ISSUE... DON't reset #LINE(5)
**            AS PER CHAD LAKSHMANAN PASS IT AS IS, AS IT MAY CONTAIN
**            CITY, STATE... OR ANY ADDRESS ELEMENT
**
**            PLEASE LOOK FOR LABEL SZFIX
** APRIL 2016 J BREMER ADD MQ OPEN/CLOSE AS PART OF COR/NAAD TO MDM
**                     REMEDIATION
**
** D.MEADE    01/2017 - PIN EXPANSION. SEE "DM 1/17"
**            FOR INITIAL IMPLEMENTATION, READ IN 12-BYTE PIN BUT
**            PASS 7-BYTE PIN TO NASN191 FOR CWF USE.
**            REMOVED SOME OLD SOHEL ZAKI COMMENTS FOR CLARITY.
** D.MEADE    02/2017 - PIN DEFINED AS ALPHA "DM 2/17"
** D.MEADE    06/2017 - MORE PIN EXPANSION - CALL MDMN191. SEE PINE2
***********************************************************************
*

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nasb572 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaNasa8003 pdaNasa8003;
    private PdaMdma190 pdaMdma190;
    private PdaNasa191 pdaNasa191;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I_Ndx;
    private DbsField pnd_W_Doc_Text;

    private DbsGroup pnd_W_Doc_Text__R_Field_1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Program;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Fill1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Title1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Fill2;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Area1;

    private DbsGroup pnd_W_Doc_Text__R_Field_2;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Date;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Fill1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Title1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Fill2;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Time;
    private DbsField pnd_Work_File1;

    private DbsGroup pnd_Work_File1__R_Field_3;
    private DbsField pnd_Work_File1_Pnd_Wk_Rec_Type;
    private DbsField pnd_Work_File1_Pnd_Wk_Ssn;
    private DbsField pnd_Work_File1_Pnd_Wk_Pin;
    private DbsField pnd_Work_File1_Pnd_Wk_Addr_Mail_Res_Etc;
    private DbsField pnd_Work_File1_Pnd_Wk_Address_Line1;
    private DbsField pnd_Work_File1_Pnd_Wk_Address_Line2;
    private DbsField pnd_Work_File1_Pnd_Wk_Address_Line3;
    private DbsField pnd_Work_File1_Pnd_Wk_Address_Line4;
    private DbsField pnd_Work_File1_Pnd_Wk_Address_Line5;
    private DbsField pnd_Work_File1_Pnd_Wk_Address_City;
    private DbsField pnd_Work_File1_Pnd_Wk_Address_State;
    private DbsField pnd_Work_File1_Pnd_Wk_Address_Zip_Code;
    private DbsField pnd_Work_File1_Pnd_Wk_Address_Country;
    private DbsField pnd_Work_File1_Pnd_Wk_Adr_Update_Date;
    private DbsField pnd_Work_File1_Pnd_Wk_Address_Reason;
    private DbsField pnd_Work_File1_Pnd_Wk_Source;
    private DbsField pnd_Work_File1_Pnd_Wk_Filler2;
    private DbsField pnd_Ctr;
    private DbsField pnd_Read_B4_Restart;
    private DbsField pnd_Record_Count;

    private DbsGroup pnd_Restart_Info;
    private DbsField pnd_Restart_Info_Pnd_Restart_Txt;
    private DbsField pnd_Restart_Info_Pnd_Restart_Ctr;
    private DbsField pnd_Restart_Info_Pnd_Restart_Pin;

    private DbsGroup pnd_Table_Line;
    private DbsField pnd_Table_Line_Pnd_Line;
    private DbsField pnd_Read;
    private DbsField pnd_Wrk_Date_Edited;
    private DbsField pnd_Date_A;
    private DbsField pnd_Wk_Source_Desc;
    private DbsField pnd_Wk_App_Name;
    private DbsField pnd_Wk_Addr_Desc;

    private DbsGroup pnd_Source_Type;
    private DbsField pnd_Source_Type_Pnd_Tbl_Source_Code;
    private DbsField pnd_Source_Type_Pnd_Tbl_Source_Desc;
    private DbsField pnd_Source_Type_Pnd_Tbl_App_Name;

    private DbsGroup pnd_Tbl_Address_Type_Code;
    private DbsField pnd_Tbl_Address_Type_Code_Pnd_Tbl_Address_Code;
    private DbsField pnd_Tbl_Address_Type_Code_Pnd_Tbl_Address_Desc;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Input_Address_Type;
    private DbsField pnd_Source_Found;
    private DbsField pnd_Addr_Type_Found;
    private DbsField pnd_Update_Bypassed;
    private DbsField pnd_Update_Success;
    private DbsField pnd_Update_Error;
    private DbsField pnd_Same_Address;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaNasa8003 = new PdaNasa8003(localVariables);
        pdaMdma190 = new PdaMdma190(localVariables);
        pdaNasa191 = new PdaNasa191(localVariables);

        // Local Variables
        pnd_I_Ndx = localVariables.newFieldInRecord("pnd_I_Ndx", "#I-NDX", FieldType.NUMERIC, 3);
        pnd_W_Doc_Text = localVariables.newFieldInRecord("pnd_W_Doc_Text", "#W-DOC-TEXT", FieldType.STRING, 80);

        pnd_W_Doc_Text__R_Field_1 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_1", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln1_Program = pnd_W_Doc_Text__R_Field_1.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Program", "#W-LN1-PROGRAM", FieldType.STRING, 
            7);
        pnd_W_Doc_Text_Pnd_W_Ln1_Fill1 = pnd_W_Doc_Text__R_Field_1.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Fill1", "#W-LN1-FILL1", FieldType.STRING, 
            19);
        pnd_W_Doc_Text_Pnd_W_Ln1_Title1 = pnd_W_Doc_Text__R_Field_1.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Title1", "#W-LN1-TITLE1", FieldType.STRING, 
            23);
        pnd_W_Doc_Text_Pnd_W_Ln1_Fill2 = pnd_W_Doc_Text__R_Field_1.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Fill2", "#W-LN1-FILL2", FieldType.STRING, 
            19);
        pnd_W_Doc_Text_Pnd_W_Ln1_Area1 = pnd_W_Doc_Text__R_Field_1.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Area1", "#W-LN1-AREA1", FieldType.STRING, 
            12);

        pnd_W_Doc_Text__R_Field_2 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_2", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln2_Date = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Date", "#W-LN2-DATE", FieldType.STRING, 10);
        pnd_W_Doc_Text_Pnd_W_Ln2_Fill1 = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Fill1", "#W-LN2-FILL1", FieldType.STRING, 
            10);
        pnd_W_Doc_Text_Pnd_W_Ln2_Title1 = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Title1", "#W-LN2-TITLE1", FieldType.STRING, 
            40);
        pnd_W_Doc_Text_Pnd_W_Ln2_Fill2 = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Fill2", "#W-LN2-FILL2", FieldType.STRING, 
            8);
        pnd_W_Doc_Text_Pnd_W_Ln2_Time = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Time", "#W-LN2-TIME", FieldType.STRING, 11);
        pnd_Work_File1 = localVariables.newFieldInRecord("pnd_Work_File1", "#WORK-FILE1", FieldType.STRING, 600);

        pnd_Work_File1__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_File1__R_Field_3", "REDEFINE", pnd_Work_File1);
        pnd_Work_File1_Pnd_Wk_Rec_Type = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Rec_Type", "#WK-REC-TYPE", FieldType.STRING, 
            3);
        pnd_Work_File1_Pnd_Wk_Ssn = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Ssn", "#WK-SSN", FieldType.NUMERIC, 9);
        pnd_Work_File1_Pnd_Wk_Pin = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Pin", "#WK-PIN", FieldType.STRING, 12);
        pnd_Work_File1_Pnd_Wk_Addr_Mail_Res_Etc = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Addr_Mail_Res_Etc", "#WK-ADDR-MAIL-RES-ETC", 
            FieldType.STRING, 2);
        pnd_Work_File1_Pnd_Wk_Address_Line1 = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Address_Line1", "#WK-ADDRESS-LINE1", FieldType.STRING, 
            70);
        pnd_Work_File1_Pnd_Wk_Address_Line2 = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Address_Line2", "#WK-ADDRESS-LINE2", FieldType.STRING, 
            70);
        pnd_Work_File1_Pnd_Wk_Address_Line3 = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Address_Line3", "#WK-ADDRESS-LINE3", FieldType.STRING, 
            70);
        pnd_Work_File1_Pnd_Wk_Address_Line4 = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Address_Line4", "#WK-ADDRESS-LINE4", FieldType.STRING, 
            70);
        pnd_Work_File1_Pnd_Wk_Address_Line5 = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Address_Line5", "#WK-ADDRESS-LINE5", FieldType.STRING, 
            70);
        pnd_Work_File1_Pnd_Wk_Address_City = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Address_City", "#WK-ADDRESS-CITY", FieldType.STRING, 
            30);
        pnd_Work_File1_Pnd_Wk_Address_State = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Address_State", "#WK-ADDRESS-STATE", FieldType.STRING, 
            2);
        pnd_Work_File1_Pnd_Wk_Address_Zip_Code = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Address_Zip_Code", "#WK-ADDRESS-ZIP-CODE", 
            FieldType.STRING, 10);
        pnd_Work_File1_Pnd_Wk_Address_Country = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Address_Country", "#WK-ADDRESS-COUNTRY", 
            FieldType.STRING, 50);
        pnd_Work_File1_Pnd_Wk_Adr_Update_Date = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Adr_Update_Date", "#WK-ADR-UPDATE-DATE", 
            FieldType.STRING, 8);
        pnd_Work_File1_Pnd_Wk_Address_Reason = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Address_Reason", "#WK-ADDRESS-REASON", 
            FieldType.STRING, 50);
        pnd_Work_File1_Pnd_Wk_Source = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Source", "#WK-SOURCE", FieldType.STRING, 3);
        pnd_Work_File1_Pnd_Wk_Filler2 = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Filler2", "#WK-FILLER2", FieldType.STRING, 71);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 9);
        pnd_Read_B4_Restart = localVariables.newFieldInRecord("pnd_Read_B4_Restart", "#READ-B4-RESTART", FieldType.NUMERIC, 9);
        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.NUMERIC, 9);

        pnd_Restart_Info = localVariables.newGroupInRecord("pnd_Restart_Info", "#RESTART-INFO");
        pnd_Restart_Info_Pnd_Restart_Txt = pnd_Restart_Info.newFieldInGroup("pnd_Restart_Info_Pnd_Restart_Txt", "#RESTART-TXT", FieldType.STRING, 1);
        pnd_Restart_Info_Pnd_Restart_Ctr = pnd_Restart_Info.newFieldInGroup("pnd_Restart_Info_Pnd_Restart_Ctr", "#RESTART-CTR", FieldType.NUMERIC, 9);
        pnd_Restart_Info_Pnd_Restart_Pin = pnd_Restart_Info.newFieldInGroup("pnd_Restart_Info_Pnd_Restart_Pin", "#RESTART-PIN", FieldType.STRING, 12);

        pnd_Table_Line = localVariables.newGroupArrayInRecord("pnd_Table_Line", "#TABLE-LINE", new DbsArrayController(1, 6));
        pnd_Table_Line_Pnd_Line = pnd_Table_Line.newFieldInGroup("pnd_Table_Line_Pnd_Line", "#LINE", FieldType.STRING, 35);
        pnd_Read = localVariables.newFieldInRecord("pnd_Read", "#READ", FieldType.NUMERIC, 9);
        pnd_Wrk_Date_Edited = localVariables.newFieldInRecord("pnd_Wrk_Date_Edited", "#WRK-DATE-EDITED", FieldType.STRING, 10);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 10);
        pnd_Wk_Source_Desc = localVariables.newFieldInRecord("pnd_Wk_Source_Desc", "#WK-SOURCE-DESC", FieldType.STRING, 40);
        pnd_Wk_App_Name = localVariables.newFieldInRecord("pnd_Wk_App_Name", "#WK-APP-NAME", FieldType.STRING, 27);
        pnd_Wk_Addr_Desc = localVariables.newFieldInRecord("pnd_Wk_Addr_Desc", "#WK-ADDR-DESC", FieldType.STRING, 20);

        pnd_Source_Type = localVariables.newGroupInRecord("pnd_Source_Type", "#SOURCE-TYPE");
        pnd_Source_Type_Pnd_Tbl_Source_Code = pnd_Source_Type.newFieldArrayInGroup("pnd_Source_Type_Pnd_Tbl_Source_Code", "#TBL-SOURCE-CODE", FieldType.STRING, 
            3, new DbsArrayController(1, 50));
        pnd_Source_Type_Pnd_Tbl_Source_Desc = pnd_Source_Type.newFieldArrayInGroup("pnd_Source_Type_Pnd_Tbl_Source_Desc", "#TBL-SOURCE-DESC", FieldType.STRING, 
            40, new DbsArrayController(1, 50));
        pnd_Source_Type_Pnd_Tbl_App_Name = pnd_Source_Type.newFieldArrayInGroup("pnd_Source_Type_Pnd_Tbl_App_Name", "#TBL-APP-NAME", FieldType.STRING, 
            27, new DbsArrayController(1, 50));

        pnd_Tbl_Address_Type_Code = localVariables.newGroupInRecord("pnd_Tbl_Address_Type_Code", "#TBL-ADDRESS-TYPE-CODE");
        pnd_Tbl_Address_Type_Code_Pnd_Tbl_Address_Code = pnd_Tbl_Address_Type_Code.newFieldArrayInGroup("pnd_Tbl_Address_Type_Code_Pnd_Tbl_Address_Code", 
            "#TBL-ADDRESS-CODE", FieldType.STRING, 1, new DbsArrayController(1, 10));
        pnd_Tbl_Address_Type_Code_Pnd_Tbl_Address_Desc = pnd_Tbl_Address_Type_Code.newFieldArrayInGroup("pnd_Tbl_Address_Type_Code_Pnd_Tbl_Address_Desc", 
            "#TBL-ADDRESS-DESC", FieldType.STRING, 30, new DbsArrayController(1, 10));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_Input_Address_Type = localVariables.newFieldInRecord("pnd_Input_Address_Type", "#INPUT-ADDRESS-TYPE", FieldType.STRING, 1);
        pnd_Source_Found = localVariables.newFieldInRecord("pnd_Source_Found", "#SOURCE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Addr_Type_Found = localVariables.newFieldInRecord("pnd_Addr_Type_Found", "#ADDR-TYPE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Update_Bypassed = localVariables.newFieldInRecord("pnd_Update_Bypassed", "#UPDATE-BYPASSED", FieldType.NUMERIC, 9);
        pnd_Update_Success = localVariables.newFieldInRecord("pnd_Update_Success", "#UPDATE-SUCCESS", FieldType.NUMERIC, 9);
        pnd_Update_Error = localVariables.newFieldInRecord("pnd_Update_Error", "#UPDATE-ERROR", FieldType.NUMERIC, 9);
        pnd_Same_Address = localVariables.newFieldInRecord("pnd_Same_Address", "#SAME-ADDRESS", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(1).setInitialValue("ROS");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(2).setInitialValue("ISS");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(3).setInitialValue("MCC");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(4).setInitialValue("INS");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(5).setInitialValue("MF ");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(6).setInitialValue("NCO");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(7).setInitialValue("NIA");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(8).setInitialValue("ONL");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(9).setInitialValue("PA ");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(10).setInitialValue("RBA");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(11).setInitialValue("RES");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(12).setInitialValue("SPK");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(13).setInitialValue("WEB");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(14).setInitialValue("UD ");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(15).setInitialValue("SBL");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(16).setInitialValue("RAW");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(17).setInitialValue("PHH");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(18).setInitialValue("FIS");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(19).setInitialValue("IVR");
        pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(20).setInitialValue("NMD");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(1).setInitialValue("   Roster Mapping Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(2).setInitialValue("    DA New Issue Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(3).setInitialValue("      McCamish Update Request       ");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(4).setInitialValue("   USSI INSURANCE Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(5).setInitialValue("    MUTUAL FUNDS Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(6).setInitialValue("    NCOA PROCESS Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(7).setInitialValue("    NEW IA NEW ISSUES Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(8).setInitialValue("   ON-LINE ADDR CENTER Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(9).setInitialValue("  PERSONAL ANNUITY Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(10).setInitialValue("   RETAIL BRKRAGE ACCT Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(11).setInitialValue("   BATCH RESEARCH Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(12).setInitialValue("        SPARK Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(13).setInitialValue("       ATI WEB Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(14).setInitialValue("   Unified Desktop Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(15).setInitialValue("        Siebel Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(16).setInitialValue("   Retirement At Work Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(17).setInitialValue("     PHH BANKING Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(18).setInitialValue("     FIS BANKING Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(19).setInitialValue("   Automated Phone System Update Request");
        pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(20).setInitialValue("   MDO New Issue Update Request");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(1).setInitialValue("The Roster Mapping");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(2).setInitialValue("The DA New Issue");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(3).setInitialValue("The McCamish");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(4).setInitialValue("The USSI INSURANCE         ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(5).setInitialValue("The MUTUAL FUNDS           ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(6).setInitialValue("The NCOA PROCESS           ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(7).setInitialValue("The NEW IA NEW ISSUES      ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(8).setInitialValue("The ON-LINE ADDR CENTER    ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(9).setInitialValue("The PERSONAL ANNUITY       ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(10).setInitialValue("The RETAIL BRKRAGE ACCT    ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(11).setInitialValue("The BATCH RESEARCH         ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(12).setInitialValue("The SPARK                  ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(13).setInitialValue("The ATI WEB                ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(14).setInitialValue("The Unified Desktop        ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(15).setInitialValue("The Siebel                 ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(16).setInitialValue("The Retirement At Work     ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(17).setInitialValue("The PHH BANKING            ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(18).setInitialValue("The FIS BANKING            ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(19).setInitialValue("The Automated Phone System ");
        pnd_Source_Type_Pnd_Tbl_App_Name.getValue(20).setInitialValue("The MDO New Issue");
        pnd_Tbl_Address_Type_Code_Pnd_Tbl_Address_Code.getValue(1).setInitialValue("M");
        pnd_Tbl_Address_Type_Code_Pnd_Tbl_Address_Code.getValue(2).setInitialValue("R");
        pnd_Tbl_Address_Type_Code_Pnd_Tbl_Address_Desc.getValue(1).setInitialValue("Mailing Address");
        pnd_Tbl_Address_Type_Code_Pnd_Tbl_Address_Desc.getValue(2).setInitialValue("Residential Address");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Nasb572() throws Exception
    {
        super("Nasb572");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //* * 12/04/2014  SZCNTRLS RESETTING CONTROL COUNTERS                                                                                                             //Natural: FORMAT PS = 66 LS = 81
        pnd_Update_Success.reset();                                                                                                                                       //Natural: RESET #UPDATE-SUCCESS #UPDATE-BYPASSED #UPDATE-ERROR #UPDATE-BYPASSED
        pnd_Update_Bypassed.reset();
        pnd_Update_Error.reset();
        pnd_Update_Bypassed.reset();
        //* * 12/04/2014  SZCNTRLS
        //*                                                                                                                                                               //Natural: GET TRANSACTION DATA #RESTART-TXT #RESTART-CTR #RESTART-PIN
        if (condition(pnd_Restart_Info_Pnd_Restart_Txt.notEquals(" ")))                                                                                                   //Natural: IF #RESTART-TXT NE ' '
        {
            getReports().write(0, "---------------  RESTART ---------------");                                                                                            //Natural: WRITE '---------------  RESTART ---------------'
            if (Global.isEscape()) return;
            getReports().write(0, new TabSetting(5),"RESTART DATA : ",NEWLINE,new TabSetting(5),"RESTART TXT         :",pnd_Restart_Info_Pnd_Restart_Txt,NEWLINE,new      //Natural: WRITE 05T 'RESTART DATA : ' / 5T 'RESTART TXT         :' #RESTART-TXT / 5T 'RESTART COUNT       :' #RESTART-CTR / 5T 'RESTART PIN         :' #RESTART-PIN /
                TabSetting(5),"RESTART COUNT       :",pnd_Restart_Info_Pnd_Restart_Ctr,NEWLINE,new TabSetting(5),"RESTART PIN         :",pnd_Restart_Info_Pnd_Restart_Pin,
                NEWLINE);
            if (Global.isEscape()) return;
            getReports().write(0, "------------ END OF RESTART INFO -------------");                                                                                      //Natural: WRITE '------------ END OF RESTART INFO -------------'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-FILE1
        while (condition(getWorkFiles().read(1, pnd_Work_File1)))
        {
            pnd_Read.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #READ
            //* * 12/04/2014  SZCNTRLS ADDED #READ AND #SSN
            //* *             AND SWAPED ORDER OF PIN N REC TYPE
            getReports().write(0, "In",pnd_Read, new NumericLength (6),"Pin",pnd_Work_File1_Pnd_Wk_Pin,"Soc",pnd_Work_File1_Pnd_Wk_Ssn,"Src",pnd_Work_File1_Pnd_Wk_Source, //Natural: WRITE 'In' #READ ( NL = 6 ) 'Pin' #WK-PIN 'Soc' #WK-SSN 'Src' #WK-SOURCE 'Rec Type' #WK-REC-TYPE 'Adr Type' #WK-ADDR-MAIL-RES-ETC
                "Rec Type",pnd_Work_File1_Pnd_Wk_Rec_Type,"Adr Type",pnd_Work_File1_Pnd_Wk_Addr_Mail_Res_Etc);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* * SZDEBUG 05/07/2015
            getReports().write(0, "Raw data from input READ WORK:",NEWLINE,"add ln 1",pnd_Work_File1_Pnd_Wk_Address_Line1,NEWLINE,"add ln 2",pnd_Work_File1_Pnd_Wk_Address_Line2, //Natural: WRITE 'Raw data from input READ WORK:' / 'add ln 1' #WK-ADDRESS-LINE1 / 'add ln 2' #WK-ADDRESS-LINE2 / 'add ln 3' #WK-ADDRESS-LINE3 / 'add ln 4' #WK-ADDRESS-LINE4 / 'add ln 5' #WK-ADDRESS-LINE5 / 'City' #WK-ADDRESS-CITY / 'State' #WK-ADDRESS-STATE / 'Zip' #WK-ADDRESS-ZIP-CODE / 'Country' #WK-ADDRESS-COUNTRY
                NEWLINE,"add ln 3",pnd_Work_File1_Pnd_Wk_Address_Line3,NEWLINE,"add ln 4",pnd_Work_File1_Pnd_Wk_Address_Line4,NEWLINE,"add ln 5",pnd_Work_File1_Pnd_Wk_Address_Line5,
                NEWLINE,"City",pnd_Work_File1_Pnd_Wk_Address_City,NEWLINE,"State",pnd_Work_File1_Pnd_Wk_Address_State,NEWLINE,"Zip",pnd_Work_File1_Pnd_Wk_Address_Zip_Code,
                NEWLINE,"Country",pnd_Work_File1_Pnd_Wk_Address_Country);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* * SZDEBUG 05/07/2015
            //* * 12/04/2014  SZCNTRLS
            if (condition(pnd_Restart_Info_Pnd_Restart_Txt.greater(" ")))                                                                                                 //Natural: IF #RESTART-TXT > ' '
            {
                pnd_Read_B4_Restart.nadd(1);                                                                                                                              //Natural: ADD 1 TO #READ-B4-RESTART
                if (condition(pnd_Read_B4_Restart.lessOrEqual(pnd_Restart_Info_Pnd_Restart_Ctr)))                                                                         //Natural: IF #READ-B4-RESTART LE #RESTART-CTR
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Restart_Info_Pnd_Restart_Txt.setValue(" ");                                                                                                           //Natural: ASSIGN #RESTART-TXT := ' '
                getReports().write(0, "===================================","       First Pin to process",NEWLINE,"       --------------------",NEWLINE,new               //Natural: WRITE '===================================' '       First Pin to process' / '       --------------------' / 5T 'PIN         :' #WK-PIN
                    TabSetting(5),"PIN         :",pnd_Work_File1_Pnd_Wk_Pin);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "===================================");                                                                                             //Natural: WRITE '==================================='
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*   DO NOT CREATE CWF ENTRIES FOR 'ADDRESS IS SAME AS IN MDM'
            if (condition(pnd_Work_File1_Pnd_Wk_Address_Reason.equals("ADDRESS IS SAME AS IN MDM")))                                                                      //Natural: IF #WK-ADDRESS-REASON = 'ADDRESS IS SAME AS IN MDM'
            {
                getReports().write(0, pnd_Work_File1_Pnd_Wk_Pin,"bypassed. ADDRESS IS SAME AS IN MDM");                                                                   //Natural: WRITE #WK-PIN 'bypassed. ADDRESS IS SAME AS IN MDM'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Same_Address.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #SAME-ADDRESS
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Wk_Rec_Type.equals("ADR")))                                                                                                  //Natural: IF #WK-REC-TYPE = 'ADR'
            {
                pdaMdma190.getMdma190_Pnd_Wpid().setValue("TA HV");                                                                                                       //Natural: ASSIGN MDMA190.#WPID := 'TA HV'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Update_Bypassed.nadd(1);                                                                                                                              //Natural: ADD 1 TO #UPDATE-BYPASSED
                getReports().write(0, "Record",pnd_Read, new NumericLength (6),"Pin",pnd_Work_File1_Pnd_Wk_Pin,"bypassed. Rec Type not ADR but is:",pnd_Work_File1_Pnd_Wk_Rec_Type); //Natural: WRITE 'Record' #READ ( NL = 6 ) 'Pin' #WK-PIN 'bypassed. Rec Type not ADR but is:' #WK-REC-TYPE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  PINE2
            }                                                                                                                                                             //Natural: END-IF
            pdaMdma190.getMdma190_Pnd_Et_Ind().setValue("Y");                                                                                                             //Natural: ASSIGN MDMA190.#ET-IND := 'Y'
            pdaMdma190.getMdma190_Pnd_Wpid_System().setValue("MDM");                                                                                                      //Natural: ASSIGN MDMA190.#WPID-SYSTEM := 'MDM'
            pdaMdma190.getMdma190_Pnd_Gda_User_Id().setValue("MDMCWF");                                                                                                   //Natural: ASSIGN MDMA190.#GDA-USER-ID := 'MDMCWF'
            pdaMdma190.getMdma190_Pnd_Wpid_Pin().compute(new ComputeParameters(false, pdaMdma190.getMdma190_Pnd_Wpid_Pin()), pnd_Work_File1_Pnd_Wk_Pin.val());            //Natural: ASSIGN MDMA190.#WPID-PIN := VAL ( #WK-PIN )
            pdaMdma190.getMdma190_Pnd_Wpid_Status().setValue("1000");                                                                                                     //Natural: ASSIGN MDMA190.#WPID-STATUS := '1000'
            pdaMdma190.getMdma190_Pnd_Wpid_Unit().setValue("CDMSA D");                                                                                                    //Natural: ASSIGN MDMA190.#WPID-UNIT := MDMA190.#WPID-NEXT-UNIT := 'CDMSA D'
            pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit().setValue("CDMSA D");
            //*     GET UNIT - CONFIRM WITH SALIE IF CORRECT SW USED
            pdaNasa8003.getPnd_Nasa8003().reset();                                                                                                                        //Natural: RESET #NASA8003
            pdaNasa8003.getPnd_Nasa8003_Pnd_Maintenance_Sw().setValue("Y");                                                                                               //Natural: ASSIGN #NASA8003.#MAINTENANCE-SW := 'Y'
            pdaNasa8003.getPnd_Nasa8003_Pnd_Action_Cde().setValue("U");                                                                                                   //Natural: ASSIGN #NASA8003.#ACTION-CDE := 'U'
            DbsUtil.callnat(Nasn8003.class , getCurrentProcessState(), pdaNasa8003.getPnd_Nasa8003());                                                                    //Natural: CALLNAT 'NASN8003' USING #NASA8003
            if (condition(Global.isEscape())) return;
            pdaMdma190.getMdma190_Pnd_Wpid_Unit().setValue(pdaNasa8003.getPnd_Nasa8003_Pnd_Unit_Code_Return());                                                           //Natural: ASSIGN MDMA190.#WPID-UNIT := #NASA8003.#UNIT-CODE-RETURN
            pdaNasa191.getNasa191().reset();                                                                                                                              //Natural: RESET NASA191
            pdaNasa191.getNasa191_Pnd_Action().setValue("AN");                                                                                                            //Natural: ASSIGN NASA191.#ACTION := 'AN'
            pdaNasa191.getNasa191_Pnd_Doc_Class().setValue("ADD");                                                                                                        //Natural: ASSIGN NASA191.#DOC-CLASS := 'ADD'
            pdaNasa191.getNasa191_Pnd_Doc_Category().setValue("M");                                                                                                       //Natural: ASSIGN NASA191.#DOC-CATEGORY := 'M'
            pdaNasa191.getNasa191_Pnd_Doc_Direction().setValue("N");                                                                                                      //Natural: ASSIGN NASA191.#DOC-DIRECTION := 'N'
            pdaNasa191.getNasa191_Pnd_Doc_Format_Cde().setValue("T");                                                                                                     //Natural: ASSIGN NASA191.#DOC-FORMAT-CDE := 'T'
            pdaNasa191.getNasa191_Pnd_Doc_Retention_Cde().setValue("P");                                                                                                  //Natural: ASSIGN NASA191.#DOC-RETENTION-CDE := 'P'
            pdaNasa191.getNasa191_Pnd_Last_Page_Flag().setValue("Y");                                                                                                     //Natural: ASSIGN NASA191.#LAST-PAGE-FLAG := 'Y'
            pdaNasa191.getNasa191_Pnd_Batch_Nbr().setValue(1);                                                                                                            //Natural: ASSIGN NASA191.#BATCH-NBR := 1
            //* *  SZRESADD 10/31/2011
                                                                                                                                                                          //Natural: PERFORM FORMAT-SOURCE-TYPE-TEXT
            sub_Format_Source_Type_Text();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS-TYPE
            sub_Format_Address_Type();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *  SZRESADD 10/31/2011
                                                                                                                                                                          //Natural: PERFORM MOVE-TO-FREE-TEXT
            sub_Move_To_Free_Text();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  PINE2
            DbsUtil.callnat(Mdmn191.class , getCurrentProcessState(), pdaMdma190.getMdma190(), pdaNasa191.getNasa191());                                                  //Natural: CALLNAT 'MDMN191' USING MDMA190 NASA191
            if (condition(Global.isEscape())) return;
            if (condition(pdaMdma190.getMdma190_Pnd_Error_Message().equals(" ")))                                                                                         //Natural: IF MDMA190.#ERROR-MESSAGE = ' '
            {
                pnd_Ctr.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #CTR
                //* * 12/04/2014  SZCNTRLS
                pnd_Update_Success.nadd(1);                                                                                                                               //Natural: ADD 1 TO #UPDATE-SUCCESS
                //* * 12/04/2014  SZCNTRLS
                if (condition(pnd_Ctr.greater(100)))                                                                                                                      //Natural: IF #CTR > 100
                {
                    pnd_Record_Count.nadd(pnd_Ctr);                                                                                                                       //Natural: ADD #CTR TO #RECORD-COUNT
                    pnd_Ctr.reset();                                                                                                                                      //Natural: RESET #CTR
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION 'X' #RECORD-COUNT #WK-PIN
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* * 12/04/2014  SZCNTRLS
                pnd_Update_Error.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #UPDATE-ERROR
                getReports().write(0, "Update failed for: ","Pin:",pnd_Work_File1_Pnd_Wk_Pin,"Rec Type:",pnd_Work_File1_Pnd_Wk_Rec_Type,"Error Msg:",pdaMdma190.getMdma190_Pnd_Error_Message()); //Natural: WRITE 'Update failed for: ' 'Pin:' #WK-PIN 'Rec Type:' #WK-REC-TYPE 'Error Msg:' MDMA190.#ERROR-MESSAGE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* * 12/04/2014  SZCNTRLS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION ' '
        //* *  SZRESADD 10/31/2011
        //* * 12/04/2014  SZCNTRLS
        getReports().write(0, "End of Job Statistics:",NEWLINE,NEWLINE,"Input Records Read:           ",pnd_Read,NEWLINE,"Restart Count:                ",                //Natural: WRITE 'End of Job Statistics:' / /'Input Records Read:           ' #READ /'Restart Count:                ' #RESTART-CTR /'Bypassed: Rec Type not ADR:   ' #UPDATE-BYPASSED /'Bypassed: same Address in MDM:' #SAME-ADDRESS /'CWF Update Success:           ' #UPDATE-SUCCESS /'CWF Update Error:             ' #UPDATE-ERROR
            pnd_Restart_Info_Pnd_Restart_Ctr,NEWLINE,"Bypassed: Rec Type not ADR:   ",pnd_Update_Bypassed,NEWLINE,"Bypassed: same Address in MDM:",pnd_Same_Address,
            NEWLINE,"CWF Update Success:           ",pnd_Update_Success,NEWLINE,"CWF Update Error:             ",pnd_Update_Error);
        if (Global.isEscape()) return;
        //* * 12/04/2014  SZCNTRLS
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-SOURCE-TYPE-TEXT
        //* * SZADDSRC 12/01/2014 ADDING NEW SOURCES, EXPANDING THE TABLE
        //* * FOR #I 1 10
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-ADDRESS-TYPE
        //* *  SZRESADD 10/31/2011
        //*  -------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TO-FREE-TEXT
        //* *  SZRESADD 11/10/2011
        //* *  SZRESADD 11/10/2011
        //* * FOR #I-NDX EQ 1 TO 18
        //* *  SZRESADD 10/31/2011 USE DYNAMIC FLD VALUE
        //* *    #W-LN2-TITLE1 := '   ROSTER MAPPING UPDATE REQUEST'
        //* *        ' requested an address change '
        //* *    #WK-ADDR-TYPE
        //* *  SZRESADD 10/31/2011
        //* *  SZRESADD 10/31/2011
        //* *  SZRESADD 10/31/2011
        //* *  SZRESADD 11/10/2011
        //* *  SZRESADD 11/10/2011
        //* *  SZRESADD 10/31/2011
        //* *  SZRESADD 10/31/2011
    }
    private void sub_Format_Source_Type_Text() throws Exception                                                                                                           //Natural: FORMAT-SOURCE-TYPE-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Wk_Source_Desc.reset();                                                                                                                                       //Natural: RESET #WK-SOURCE-DESC #WK-APP-NAME
        pnd_Wk_App_Name.reset();
        pnd_Source_Found.setValue(false);                                                                                                                                 //Natural: ASSIGN #SOURCE-FOUND := FALSE
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 50
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(50)); pnd_I.nadd(1))
        {
            //* * SZADDSRC 12/01/2014 ADDING NEW SOURCES, EXPANDING THE TABLE
            if (condition(pnd_Work_File1_Pnd_Wk_Source.equals(pnd_Source_Type_Pnd_Tbl_Source_Code.getValue(pnd_I))))                                                      //Natural: IF #WK-SOURCE = #TBL-SOURCE-CODE ( #I )
            {
                pnd_Source_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #SOURCE-FOUND := TRUE
                pnd_Wk_Source_Desc.setValue(pnd_Source_Type_Pnd_Tbl_Source_Desc.getValue(pnd_I));                                                                         //Natural: ASSIGN #WK-SOURCE-DESC := #TBL-SOURCE-DESC ( #I )
                pnd_Wk_App_Name.setValue(pnd_Source_Type_Pnd_Tbl_App_Name.getValue(pnd_I));                                                                               //Natural: ASSIGN #WK-APP-NAME := #TBL-APP-NAME ( #I )
                //* * SZRESADR FOR TESTING ONLY
                //* *    WRITE 'found match for input...'
                //* *    WRITE   'Source Code' (NE) #TBL-SOURCE-CODE (#I)
                //* * /  'Desc' (BL) #TBL-SOURCE-DESC (#I)
                //* * SZRESADR FOR TESTING ONLY
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(! (pnd_Source_Found.getBoolean())))                                                                                                                 //Natural: IF NOT #SOURCE-FOUND
        {
            getReports().write(0, "No match found for input source: ",pnd_Work_File1_Pnd_Wk_Source," for Pin: ",pnd_Work_File1_Pnd_Wk_Pin);                               //Natural: WRITE 'No match found for input source: ' #WK-SOURCE ' for Pin: ' #WK-PIN
            if (Global.isEscape()) return;
            pnd_Wk_Source_Desc.setValue(pnd_Work_File1_Pnd_Wk_Source);                                                                                                    //Natural: ASSIGN #WK-SOURCE-DESC := #WK-SOURCE
            pnd_Wk_App_Name.setValue(pnd_Work_File1_Pnd_Wk_Source);                                                                                                       //Natural: ASSIGN #WK-APP-NAME := #WK-SOURCE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Address_Type() throws Exception                                                                                                               //Natural: FORMAT-ADDRESS-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Wk_Addr_Desc.reset();                                                                                                                                         //Natural: RESET #WK-ADDR-DESC
        pnd_Addr_Type_Found.setValue(false);                                                                                                                              //Natural: ASSIGN #ADDR-TYPE-FOUND := FALSE
        FOR02:                                                                                                                                                            //Natural: FOR #J 1 10
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(10)); pnd_J.nadd(1))
        {
            if (condition(pnd_Work_File1_Pnd_Wk_Addr_Mail_Res_Etc.equals(pnd_Tbl_Address_Type_Code_Pnd_Tbl_Address_Code.getValue(pnd_J))))                                //Natural: IF #WK-ADDR-MAIL-RES-ETC = #TBL-ADDRESS-CODE ( #J )
            {
                pnd_Addr_Type_Found.setValue(true);                                                                                                                       //Natural: ASSIGN #ADDR-TYPE-FOUND := TRUE
                pnd_Wk_Addr_Desc.setValue(pnd_Tbl_Address_Type_Code_Pnd_Tbl_Address_Desc.getValue(pnd_J));                                                                //Natural: ASSIGN #WK-ADDR-DESC := #TBL-ADDRESS-DESC ( #J )
                //* * SZRESADR FOR TESTING ONLY
                //* *      WRITE 'found match for input...'
                //* *      WRITE  'Addr Code' (YE)  #TBL-ADDRESS-CODE (#J)
                //* */     'Desc' (BL) #TBL-ADDRESS-DESC (#J)
                //* * SZRESADR FOR TESTING ONLY
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(! (pnd_Addr_Type_Found.getBoolean())))                                                                                                              //Natural: IF NOT #ADDR-TYPE-FOUND
        {
            getReports().write(0, "No found match for input address type: ",pnd_Work_File1_Pnd_Wk_Addr_Mail_Res_Etc," for Pin: ",pnd_Work_File1_Pnd_Wk_Pin);              //Natural: WRITE 'No found match for input address type: ' #WK-ADDR-MAIL-RES-ETC ' for Pin: ' #WK-PIN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_To_Free_Text() throws Exception                                                                                                                 //Natural: MOVE-TO-FREE-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Table_Line_Pnd_Line.getValue(1).setValue(pnd_Work_File1_Pnd_Wk_Address_Line1);                                                                                //Natural: ASSIGN #LINE ( 1 ) := #WK-ADDRESS-LINE1
        pnd_Table_Line_Pnd_Line.getValue(2).setValue(pnd_Work_File1_Pnd_Wk_Address_Line2);                                                                                //Natural: ASSIGN #LINE ( 2 ) := #WK-ADDRESS-LINE2
        pnd_Table_Line_Pnd_Line.getValue(3).setValue(pnd_Work_File1_Pnd_Wk_Address_Line3);                                                                                //Natural: ASSIGN #LINE ( 3 ) := #WK-ADDRESS-LINE3
        pnd_Table_Line_Pnd_Line.getValue(4).setValue(pnd_Work_File1_Pnd_Wk_Address_Line4);                                                                                //Natural: ASSIGN #LINE ( 4 ) := #WK-ADDRESS-LINE4
        pnd_Table_Line_Pnd_Line.getValue(5).setValue(pnd_Work_File1_Pnd_Wk_Address_Line5);                                                                                //Natural: ASSIGN #LINE ( 5 ) := #WK-ADDRESS-LINE5
        //* *  SZRESADD 11/10/2011
        if (condition(pnd_Work_File1_Pnd_Wk_Address_Line2.equals(" ")))                                                                                                   //Natural: IF #WK-ADDRESS-LINE2 = ' '
        {
            pnd_Table_Line_Pnd_Line.getValue(2).reset();                                                                                                                  //Natural: RESET #LINE ( 2 ) #LINE ( 3 ) #LINE ( 4 )
            pnd_Table_Line_Pnd_Line.getValue(3).reset();
            pnd_Table_Line_Pnd_Line.getValue(4).reset();
            //* *  SZRESADD 11/10/2011
            //* *  #LINE(5)  /* SZFIX 05/13/2015
            //* *  SZRESADD 11/10/2011
            pnd_Table_Line_Pnd_Line.getValue(2).setValue(DbsUtil.compress(pnd_Work_File1_Pnd_Wk_Address_City, pnd_Work_File1_Pnd_Wk_Address_State, pnd_Work_File1_Pnd_Wk_Address_Zip_Code)); //Natural: COMPRESS #WK-ADDRESS-CITY #WK-ADDRESS-STATE #WK-ADDRESS-ZIP-CODE INTO #LINE ( 2 )
            pnd_Table_Line_Pnd_Line.getValue(3).setValue(pnd_Work_File1_Pnd_Wk_Address_Country);                                                                          //Natural: ASSIGN #LINE ( 3 ) := #WK-ADDRESS-COUNTRY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Work_File1_Pnd_Wk_Address_Line3.equals(" ")))                                                                                               //Natural: IF #WK-ADDRESS-LINE3 = ' '
            {
                pnd_Table_Line_Pnd_Line.getValue(3).reset();                                                                                                              //Natural: RESET #LINE ( 3 ) #LINE ( 4 )
                pnd_Table_Line_Pnd_Line.getValue(4).reset();
                //* *  SZRESADD 11/10/2011
                //* *    #LINE(5)  /* SZFIX 05/13/2015
                //* *  SZRESADD 11/10/2011
                pnd_Table_Line_Pnd_Line.getValue(3).setValue(DbsUtil.compress(pnd_Work_File1_Pnd_Wk_Address_City, pnd_Work_File1_Pnd_Wk_Address_State,                    //Natural: COMPRESS #WK-ADDRESS-CITY #WK-ADDRESS-STATE #WK-ADDRESS-ZIP-CODE INTO #LINE ( 3 )
                    pnd_Work_File1_Pnd_Wk_Address_Zip_Code));
                pnd_Table_Line_Pnd_Line.getValue(4).setValue(pnd_Work_File1_Pnd_Wk_Address_Country);                                                                      //Natural: ASSIGN #LINE ( 4 ) := #WK-ADDRESS-COUNTRY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Work_File1_Pnd_Wk_Address_Line4.equals(" ")))                                                                                           //Natural: IF #WK-ADDRESS-LINE4 = ' '
                {
                    pnd_Table_Line_Pnd_Line.getValue(4).reset();                                                                                                          //Natural: RESET #LINE ( 4 )
                    //* *    #LINE(5) /* SZFIX 05/13/2015
                    pnd_Table_Line_Pnd_Line.getValue(4).setValue(DbsUtil.compress(pnd_Work_File1_Pnd_Wk_Address_City, pnd_Work_File1_Pnd_Wk_Address_State,                //Natural: COMPRESS #WK-ADDRESS-CITY #WK-ADDRESS-STATE #WK-ADDRESS-ZIP-CODE INTO #LINE ( 4 )
                        pnd_Work_File1_Pnd_Wk_Address_Zip_Code));
                    pnd_Table_Line_Pnd_Line.getValue(5).setValue(pnd_Work_File1_Pnd_Wk_Address_Country);                                                                  //Natural: ASSIGN #LINE ( 5 ) := #WK-ADDRESS-COUNTRY
                    //* *  SZRESADD 11/10/2011
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Work_File1_Pnd_Wk_Address_Line5.equals(" ")))                                                                                       //Natural: IF #WK-ADDRESS-LINE5 = ' '
                    {
                        pnd_Table_Line_Pnd_Line.getValue(5).reset();                                                                                                      //Natural: RESET #LINE ( 5 ) #LINE ( 6 )
                        pnd_Table_Line_Pnd_Line.getValue(6).reset();
                        pnd_Table_Line_Pnd_Line.getValue(5).setValue(DbsUtil.compress(pnd_Work_File1_Pnd_Wk_Address_City, pnd_Work_File1_Pnd_Wk_Address_State,            //Natural: COMPRESS #WK-ADDRESS-CITY #WK-ADDRESS-STATE #WK-ADDRESS-ZIP-CODE INTO #LINE ( 5 )
                            pnd_Work_File1_Pnd_Wk_Address_Zip_Code));
                        pnd_Table_Line_Pnd_Line.getValue(6).setValue(pnd_Work_File1_Pnd_Wk_Address_Country);                                                              //Natural: ASSIGN #LINE ( 6 ) := #WK-ADDRESS-COUNTRY
                    }                                                                                                                                                     //Natural: END-IF
                    //* *  SZRESADD 11/10/2011
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "=",pnd_Work_File1_Pnd_Wk_Address_City,"=",pnd_Work_File1_Pnd_Wk_Address_State,"=",pnd_Work_File1_Pnd_Wk_Address_Zip_Code);                 //Natural: WRITE '=' #WK-ADDRESS-CITY '=' #WK-ADDRESS-STATE '='#WK-ADDRESS-ZIP-CODE
        if (Global.isEscape()) return;
        //* * 12/04/2014  SZCNTRLS REPLACES '=' BY LINE #
        getReports().write(0, "Line1",pnd_Table_Line_Pnd_Line.getValue(1));                                                                                               //Natural: WRITE 'Line1' #LINE ( 1 )
        if (Global.isEscape()) return;
        getReports().write(0, "Line2",pnd_Table_Line_Pnd_Line.getValue(2));                                                                                               //Natural: WRITE 'Line2' #LINE ( 2 )
        if (Global.isEscape()) return;
        getReports().write(0, "Line3",pnd_Table_Line_Pnd_Line.getValue(3));                                                                                               //Natural: WRITE 'Line3' #LINE ( 3 )
        if (Global.isEscape()) return;
        getReports().write(0, "Line4",pnd_Table_Line_Pnd_Line.getValue(4));                                                                                               //Natural: WRITE 'Line4' #LINE ( 4 )
        if (Global.isEscape()) return;
        getReports().write(0, "Line5",pnd_Table_Line_Pnd_Line.getValue(5));                                                                                               //Natural: WRITE 'Line5' #LINE ( 5 )
        if (Global.isEscape()) return;
        getReports().write(0, "Line6",pnd_Table_Line_Pnd_Line.getValue(6));                                                                                               //Natural: WRITE 'Line6' #LINE ( 6 )
        if (Global.isEscape()) return;
        //* * WRITE '=' #WK-ADDR-MAIL-RES-ETC
        getReports().write(0, "=",pnd_Work_File1_Pnd_Wk_Address_Reason);                                                                                                  //Natural: WRITE '=' #WK-ADDRESS-REASON
        if (Global.isEscape()) return;
        //* * WRITE '=' #WK-SOURCE
        //* *  SZRESADD 11/10/2011
        //* *  SZRESADD 11/10/2011
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue("*").reset();                                                                                                       //Natural: RESET NASA191.#DOC-TEXT ( * )
        FOR03:                                                                                                                                                            //Natural: FOR #I-NDX EQ 1 TO 19
        for (pnd_I_Ndx.setValue(1); condition(pnd_I_Ndx.lessOrEqual(19)); pnd_I_Ndx.nadd(1))
        {
            //* *  SZRESADD 11/10/2011
            pnd_W_Doc_Text.reset();                                                                                                                                       //Natural: RESET #W-DOC-TEXT
            short decideConditionsMet500 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #I-NDX;//Natural: VALUE 1
            if (condition((pnd_I_Ndx.equals(1))))
            {
                decideConditionsMet500++;
                pnd_W_Doc_Text_Pnd_W_Ln1_Program.setValue(Global.getPROGRAM());                                                                                           //Natural: ASSIGN #W-LN1-PROGRAM := *PROGRAM
                pnd_W_Doc_Text_Pnd_W_Ln1_Title1.setValue("NAME AND ADDRESS SYSTEM");                                                                                      //Natural: ASSIGN #W-LN1-TITLE1 := 'NAME AND ADDRESS SYSTEM'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_I_Ndx.equals(2))))
            {
                decideConditionsMet500++;
                pnd_W_Doc_Text_Pnd_W_Ln2_Title1.setValue(pnd_Wk_Source_Desc);                                                                                             //Natural: ASSIGN #W-LN2-TITLE1 := #WK-SOURCE-DESC
                //* *  SZRESADD 10/31/2011
                pnd_W_Doc_Text_Pnd_W_Ln2_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS' 'AP"));                                                       //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS' 'AP ) TO #W-LN2-TIME
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_I_Ndx.equals(3))))
            {
                decideConditionsMet500++;
                pnd_Wrk_Date_Edited.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                    //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #WRK-DATE-EDITED
                setValueToSubstring("on",pnd_W_Doc_Text,32,2);                                                                                                            //Natural: MOVE 'on' TO SUBSTR ( #W-DOC-TEXT,32,2 )
                setValueToSubstring(pnd_Wrk_Date_Edited,pnd_W_Doc_Text,35,10);                                                                                            //Natural: MOVE #WRK-DATE-EDITED TO SUBSTR ( #W-DOC-TEXT,35,10 )
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_I_Ndx.equals(5))))
            {
                decideConditionsMet500++;
                //* *  SZRESADD 10/31/2011 USE DYNAMIC FLD VALUE
                //* *    #W-DOC-TEXT := 'The Roster Mapping application' -
                pnd_W_Doc_Text.setValue(DbsUtil.compress(pnd_Wk_App_Name, "application requested a", pnd_Wk_Addr_Desc, "change "));                                       //Natural: COMPRESS #WK-APP-NAME 'application requested a' #WK-ADDR-DESC 'change ' INTO #W-DOC-TEXT
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_I_Ndx.equals(6))))
            {
                decideConditionsMet500++;
                pnd_W_Doc_Text.setValue("on the Corporate Name and Address System. Please apply the new address listed ");                                                //Natural: ASSIGN #W-DOC-TEXT := 'on the Corporate Name and Address System. Please apply the new address listed '
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pnd_I_Ndx.equals(7))))
            {
                decideConditionsMet500++;
                pnd_W_Doc_Text.setValue("below and send a confirmation statement to the policyholder if applicable.");                                                    //Natural: ASSIGN #W-DOC-TEXT := 'below and send a confirmation statement to the policyholder if applicable.'
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pnd_I_Ndx.equals(8))))
            {
                decideConditionsMet500++;
                pnd_W_Doc_Text.setValue(" ");                                                                                                                             //Natural: ASSIGN #W-DOC-TEXT := ' '
            }                                                                                                                                                             //Natural: VALUE 9
            else if (condition((pnd_I_Ndx.equals(9))))
            {
                decideConditionsMet500++;
                pnd_W_Doc_Text.setValue(" ");                                                                                                                             //Natural: ASSIGN #W-DOC-TEXT := ' '
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_I_Ndx.equals(10))))
            {
                decideConditionsMet500++;
                setValueToSubstring("New Address",pnd_W_Doc_Text,1,12);                                                                                                   //Natural: MOVE 'New Address' TO SUBSTR ( #W-DOC-TEXT,01,12 )
            }                                                                                                                                                             //Natural: VALUE 11
            else if (condition((pnd_I_Ndx.equals(11))))
            {
                decideConditionsMet500++;
                setValueToSubstring("___________________________________",pnd_W_Doc_Text,1,35);                                                                           //Natural: MOVE '___________________________________' TO SUBSTR ( #W-DOC-TEXT,1,35 )
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_I_Ndx.equals(12))))
            {
                decideConditionsMet500++;
                setValueToSubstring(pnd_Table_Line_Pnd_Line.getValue(1),pnd_W_Doc_Text,1,35);                                                                             //Natural: MOVE #LINE ( 1 ) TO SUBSTR ( #W-DOC-TEXT,01,35 )
            }                                                                                                                                                             //Natural: VALUE 13
            else if (condition((pnd_I_Ndx.equals(13))))
            {
                decideConditionsMet500++;
                setValueToSubstring(pnd_Table_Line_Pnd_Line.getValue(2),pnd_W_Doc_Text,1,35);                                                                             //Natural: MOVE #LINE ( 2 ) TO SUBSTR ( #W-DOC-TEXT,01,35 )
            }                                                                                                                                                             //Natural: VALUE 14
            else if (condition((pnd_I_Ndx.equals(14))))
            {
                decideConditionsMet500++;
                setValueToSubstring(pnd_Table_Line_Pnd_Line.getValue(3),pnd_W_Doc_Text,1,35);                                                                             //Natural: MOVE #LINE ( 3 ) TO SUBSTR ( #W-DOC-TEXT,01,35 )
            }                                                                                                                                                             //Natural: VALUE 15
            else if (condition((pnd_I_Ndx.equals(15))))
            {
                decideConditionsMet500++;
                setValueToSubstring(pnd_Table_Line_Pnd_Line.getValue(4),pnd_W_Doc_Text,1,35);                                                                             //Natural: MOVE #LINE ( 4 ) TO SUBSTR ( #W-DOC-TEXT,01,35 )
            }                                                                                                                                                             //Natural: VALUE 16
            else if (condition((pnd_I_Ndx.equals(16))))
            {
                decideConditionsMet500++;
                setValueToSubstring(pnd_Table_Line_Pnd_Line.getValue(5),pnd_W_Doc_Text,1,35);                                                                             //Natural: MOVE #LINE ( 5 ) TO SUBSTR ( #W-DOC-TEXT,01,35 )
            }                                                                                                                                                             //Natural: VALUE 17
            else if (condition((pnd_I_Ndx.equals(17))))
            {
                decideConditionsMet500++;
                //*  WAS 17 SZRESADD 11/10/2011
                setValueToSubstring(pnd_Table_Line_Pnd_Line.getValue(6),pnd_W_Doc_Text,1,35);                                                                             //Natural: MOVE #LINE ( 6 ) TO SUBSTR ( #W-DOC-TEXT,01,35 )
            }                                                                                                                                                             //Natural: VALUE 18
            else if (condition((pnd_I_Ndx.equals(18))))
            {
                decideConditionsMet500++;
                setValueToSubstring("The address change was requested on:",pnd_W_Doc_Text,1,36);                                                                          //Natural: MOVE 'The address change was requested on:' TO SUBSTR ( #W-DOC-TEXT,01,36 )
                pnd_Date_A.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                             //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #DATE-A
                //*  WAS 18 SZRESADD 11/10/2011
                setValueToSubstring(pnd_Date_A,pnd_W_Doc_Text,38,10);                                                                                                     //Natural: MOVE #DATE-A TO SUBSTR ( #W-DOC-TEXT,38,10 )
            }                                                                                                                                                             //Natural: VALUE 19
            else if (condition((pnd_I_Ndx.equals(19))))
            {
                decideConditionsMet500++;
                pnd_W_Doc_Text.setValue(DbsUtil.compress("Reason: ", pnd_Work_File1_Pnd_Wk_Address_Reason));                                                              //Natural: COMPRESS 'Reason: ' #WK-ADDRESS-REASON INTO #W-DOC-TEXT
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet500 > 0))
            {
                pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(pnd_I_Ndx).setValue(pnd_W_Doc_Text);                                                                        //Natural: ASSIGN NASA191.#DOC-TEXT ( #I-NDX ) := #W-DOC-TEXT
                pnd_W_Doc_Text.reset();                                                                                                                                   //Natural: RESET #W-DOC-TEXT
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=66 LS=81");
    }
}
