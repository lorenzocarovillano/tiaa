/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:59:56 PM
**        * FROM NATURAL PROGRAM : Nasb667
************************************************************
**        * FILE NAME            : Nasb667.java
**        * CLASS NAME           : Nasb667
**        * INSTANCE NAME        : Nasb667
************************************************************
************************************************************************
* PROGRAM NAME: NASB667
* AUTHOR      : JEFF PETERSON
* DATE        : SEPTEMBER, 2003
* DESCRIPTION : NATIONAL CHANGE OF ADDRESS LETTER PROCESS FOR BAD ADDRES
************************************************************************
*    DATE      USERID             DESCRIPTIONS
* 07/01/2004  DURAND    DISPLAY CREF CONTRACT NUMBER IF TNT CONTRACT
* 10/04/2010  GOPAZ     CONTRACTS NOT INCLUDED IN THE LETTER
* 06/28/2017  MEADED    PIN EXPANSION AND COR SUNSETTING. SEE PINE
************************************************************************
*

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nasb667 extends BLNatBase
{
    // Data Areas
    private LdaNasl747a ldaNasl747a;
    private PdaPsta9532 pdaPsta9532;
    private PdaCwfpda_M pdaCwfpda_M;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ppcn_Table;
    private DbsField pnd_Ppcn_Editmask;
    private DbsField pnd_Begin_Of_File;
    private DbsField pnd_Pst_Seq_No;
    private DbsField pnd_Count_Pin;
    private DbsField pnd_Ppcn_Cnt;
    private DbsField pnd_Ppcn_Cnt_2;
    private DbsField pnd_Ppcn_Cnt_22;
    private DbsField pnd_Count_All;

    private DbsGroup pnd_Bypass;
    private DbsField pnd_Bypass_Pnd_Bypass_Pin;
    private DbsField pnd_Bypass_Pnd_Filler;
    private DbsField pnd_Name;
    private DbsField pnd_Mn_Addr1;
    private DbsField pnd_Mn_Addr2;
    private DbsField pnd_Mn_Addr3;
    private DbsField pnd_Mn_Zip_All;

    private DbsGroup pnd_Mn_Zip_All__R_Field_1;
    private DbsField pnd_Mn_Zip_All_Pnd_Mn_Zip_F;
    private DbsField pnd_Mn_Zip_All_Pnd_Mn_Zip;
    private DbsField pnd_Mn_Postnet_Zip;
    private DbsField pnd_Mn_Postnet_Zip4;
    private DbsField pnd_Mn_Postnet_Barcode;
    private DbsField pnd_Mo_Addr1;
    private DbsField pnd_Mo_Addr2;
    private DbsField pnd_Mo_Addr3;
    private DbsField pnd_Mo_Zip_All;

    private DbsGroup pnd_Mo_Zip_All__R_Field_2;
    private DbsField pnd_Mo_Zip_All_Pnd_Mo_Zip_F;
    private DbsField pnd_Mo_Zip_All_Pnd_Mo_Zip;
    private DbsField pnd_Mo_Postnet_Zip;
    private DbsField pnd_Mo_Postnet_Zip4;
    private DbsField pnd_Mo_Postnet_Barcode;
    private DbsField pnd_O_Addr1;
    private DbsField pnd_O_Addr2;
    private DbsField pnd_O_Addr3;
    private DbsField pnd_O_Zip_All;

    private DbsGroup pnd_O_Zip_All__R_Field_3;
    private DbsField pnd_O_Zip_All_Pnd_O_Zip_F;
    private DbsField pnd_O_Zip_All_Pnd_O_Zip;
    private DbsField pnd_N_Addr1;
    private DbsField pnd_N_Addr2;
    private DbsField pnd_N_Addr3;
    private DbsField pnd_N_Zip_All;

    private DbsGroup pnd_N_Zip_All__R_Field_4;
    private DbsField pnd_N_Zip_All_Pnd_N_Zip_F;
    private DbsField pnd_N_Zip_All_Pnd_N_Zip;
    private DbsField pnd_Date_G;

    private DbsGroup pnd_Date_G__R_Field_5;
    private DbsField pnd_Date_G_Pnd_Date_Filler;
    private DbsField pnd_Date_G_Pnd_Date_Label;
    private DbsField pnd_Unique_Id;

    private DbsGroup pnd_Unique_Id__R_Field_6;
    private DbsField pnd_Unique_Id_Pnd_Unique_Id_1;
    private DbsField pnd_Unique_Id_Pnd_Unique_Id_2;
    private DbsField pnd_Unique_Id_Pnd_Unique_Id_3;
    private DbsField pnd_Unique_Id_Pnd_Unique_Id_4;
    private DbsField pnd_Unique_Id_Pnd_Unique_Id_5;
    private DbsField pnd_Unique_Id_Pnd_Unique_Id_6;
    private DbsField pnd_Unique_Id_Pnd_Unique_Id_7;
    private DbsField pnd_Unique_Id_Pnd_Unique_Id_8;
    private DbsField pnd_Unique_Id_Pnd_Unique_Id_9;
    private DbsField pnd_Unique_Id_Pnd_Unique_Id_10;
    private DbsField pnd_Unique_Id_Pnd_Unique_Id_11;
    private DbsField pnd_Unique_Id_Pnd_Unique_Id_12;
    private DbsField xics_Record;

    private DbsGroup xics_File_Dflts;
    private DbsField xics_File_Dflts_Pnd_Ln_Start;
    private DbsField xics_File_Dflts_Pnd_Ln_End;
    private DbsField xics_File_Dflts_Pnd_Comments;
    private DbsField pnd_Letter_Ctr;
    private DbsField pnd_Print;
    private DbsField pnd_Tnt_Cref_Cntrct_Nbr;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Bad_Addr_Pin_N12Old;
    private DbsField readWork01Pnd_Bad_Addr_NameOld;
    private DbsField readWork01Pnd_Tiaa_Std_Addr_Line2Old;
    private DbsField readWork01Pnd_Tiaa_Std_Addr_Line1Old;
    private DbsField readWork01Pnd_Tiaa_Std_ZipOld;
    private DbsField readWork01Pnd_Tiaa_Std_Zip_4Old;
    private DbsField readWork01Pnd_Tiaa_Std_Walk_SeqOld;
    private DbsField readWork01Pnd_Bad_Addr_Line_2Old;
    private DbsField readWork01Pnd_Bad_Addr_Line_1Old;
    private DbsField readWork01Pnd_Bad_Addr_City_StateOld;
    private DbsField readWork01Pnd_Bad_Addr_Zip_Plus_4Old;
    private DbsField readWork01Pnd_Bad_Addr_Rec_CntOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaNasl747a = new LdaNasl747a();
        registerRecord(ldaNasl747a);
        localVariables = new DbsRecord();
        pdaPsta9532 = new PdaPsta9532(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);

        // Local Variables
        pnd_Ppcn_Table = localVariables.newFieldArrayInRecord("pnd_Ppcn_Table", "#PPCN-TABLE", FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_Ppcn_Editmask = localVariables.newFieldInRecord("pnd_Ppcn_Editmask", "#PPCN-EDITMASK", FieldType.STRING, 10);
        pnd_Begin_Of_File = localVariables.newFieldInRecord("pnd_Begin_Of_File", "#BEGIN-OF-FILE", FieldType.STRING, 1);
        pnd_Pst_Seq_No = localVariables.newFieldInRecord("pnd_Pst_Seq_No", "#PST-SEQ-NO", FieldType.PACKED_DECIMAL, 9);
        pnd_Count_Pin = localVariables.newFieldInRecord("pnd_Count_Pin", "#COUNT-PIN", FieldType.PACKED_DECIMAL, 9);
        pnd_Ppcn_Cnt = localVariables.newFieldInRecord("pnd_Ppcn_Cnt", "#PPCN-CNT", FieldType.NUMERIC, 2);
        pnd_Ppcn_Cnt_2 = localVariables.newFieldInRecord("pnd_Ppcn_Cnt_2", "#PPCN-CNT-2", FieldType.NUMERIC, 2);
        pnd_Ppcn_Cnt_22 = localVariables.newFieldInRecord("pnd_Ppcn_Cnt_22", "#PPCN-CNT-22", FieldType.NUMERIC, 2);
        pnd_Count_All = localVariables.newFieldInRecord("pnd_Count_All", "#COUNT-ALL", FieldType.PACKED_DECIMAL, 9);

        pnd_Bypass = localVariables.newGroupInRecord("pnd_Bypass", "#BYPASS");
        pnd_Bypass_Pnd_Bypass_Pin = pnd_Bypass.newFieldInGroup("pnd_Bypass_Pnd_Bypass_Pin", "#BYPASS-PIN", FieldType.NUMERIC, 12);
        pnd_Bypass_Pnd_Filler = pnd_Bypass.newFieldInGroup("pnd_Bypass_Pnd_Filler", "#FILLER", FieldType.STRING, 73);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 35);
        pnd_Mn_Addr1 = localVariables.newFieldInRecord("pnd_Mn_Addr1", "#MN-ADDR1", FieldType.STRING, 35);
        pnd_Mn_Addr2 = localVariables.newFieldInRecord("pnd_Mn_Addr2", "#MN-ADDR2", FieldType.STRING, 35);
        pnd_Mn_Addr3 = localVariables.newFieldInRecord("pnd_Mn_Addr3", "#MN-ADDR3", FieldType.STRING, 35);
        pnd_Mn_Zip_All = localVariables.newFieldInRecord("pnd_Mn_Zip_All", "#MN-ZIP-ALL", FieldType.STRING, 10);

        pnd_Mn_Zip_All__R_Field_1 = localVariables.newGroupInRecord("pnd_Mn_Zip_All__R_Field_1", "REDEFINE", pnd_Mn_Zip_All);
        pnd_Mn_Zip_All_Pnd_Mn_Zip_F = pnd_Mn_Zip_All__R_Field_1.newFieldInGroup("pnd_Mn_Zip_All_Pnd_Mn_Zip_F", "#MN-ZIP-F", FieldType.STRING, 5);
        pnd_Mn_Zip_All_Pnd_Mn_Zip = pnd_Mn_Zip_All__R_Field_1.newFieldInGroup("pnd_Mn_Zip_All_Pnd_Mn_Zip", "#MN-ZIP", FieldType.STRING, 5);
        pnd_Mn_Postnet_Zip = localVariables.newFieldInRecord("pnd_Mn_Postnet_Zip", "#MN-POSTNET-ZIP", FieldType.STRING, 5);
        pnd_Mn_Postnet_Zip4 = localVariables.newFieldInRecord("pnd_Mn_Postnet_Zip4", "#MN-POSTNET-ZIP4", FieldType.STRING, 4);
        pnd_Mn_Postnet_Barcode = localVariables.newFieldInRecord("pnd_Mn_Postnet_Barcode", "#MN-POSTNET-BARCODE", FieldType.STRING, 3);
        pnd_Mo_Addr1 = localVariables.newFieldInRecord("pnd_Mo_Addr1", "#MO-ADDR1", FieldType.STRING, 35);
        pnd_Mo_Addr2 = localVariables.newFieldInRecord("pnd_Mo_Addr2", "#MO-ADDR2", FieldType.STRING, 35);
        pnd_Mo_Addr3 = localVariables.newFieldInRecord("pnd_Mo_Addr3", "#MO-ADDR3", FieldType.STRING, 35);
        pnd_Mo_Zip_All = localVariables.newFieldInRecord("pnd_Mo_Zip_All", "#MO-ZIP-ALL", FieldType.STRING, 10);

        pnd_Mo_Zip_All__R_Field_2 = localVariables.newGroupInRecord("pnd_Mo_Zip_All__R_Field_2", "REDEFINE", pnd_Mo_Zip_All);
        pnd_Mo_Zip_All_Pnd_Mo_Zip_F = pnd_Mo_Zip_All__R_Field_2.newFieldInGroup("pnd_Mo_Zip_All_Pnd_Mo_Zip_F", "#MO-ZIP-F", FieldType.STRING, 5);
        pnd_Mo_Zip_All_Pnd_Mo_Zip = pnd_Mo_Zip_All__R_Field_2.newFieldInGroup("pnd_Mo_Zip_All_Pnd_Mo_Zip", "#MO-ZIP", FieldType.STRING, 5);
        pnd_Mo_Postnet_Zip = localVariables.newFieldInRecord("pnd_Mo_Postnet_Zip", "#MO-POSTNET-ZIP", FieldType.STRING, 5);
        pnd_Mo_Postnet_Zip4 = localVariables.newFieldInRecord("pnd_Mo_Postnet_Zip4", "#MO-POSTNET-ZIP4", FieldType.STRING, 4);
        pnd_Mo_Postnet_Barcode = localVariables.newFieldInRecord("pnd_Mo_Postnet_Barcode", "#MO-POSTNET-BARCODE", FieldType.STRING, 3);
        pnd_O_Addr1 = localVariables.newFieldInRecord("pnd_O_Addr1", "#O-ADDR1", FieldType.STRING, 35);
        pnd_O_Addr2 = localVariables.newFieldInRecord("pnd_O_Addr2", "#O-ADDR2", FieldType.STRING, 35);
        pnd_O_Addr3 = localVariables.newFieldInRecord("pnd_O_Addr3", "#O-ADDR3", FieldType.STRING, 35);
        pnd_O_Zip_All = localVariables.newFieldInRecord("pnd_O_Zip_All", "#O-ZIP-ALL", FieldType.STRING, 10);

        pnd_O_Zip_All__R_Field_3 = localVariables.newGroupInRecord("pnd_O_Zip_All__R_Field_3", "REDEFINE", pnd_O_Zip_All);
        pnd_O_Zip_All_Pnd_O_Zip_F = pnd_O_Zip_All__R_Field_3.newFieldInGroup("pnd_O_Zip_All_Pnd_O_Zip_F", "#O-ZIP-F", FieldType.STRING, 5);
        pnd_O_Zip_All_Pnd_O_Zip = pnd_O_Zip_All__R_Field_3.newFieldInGroup("pnd_O_Zip_All_Pnd_O_Zip", "#O-ZIP", FieldType.STRING, 5);
        pnd_N_Addr1 = localVariables.newFieldInRecord("pnd_N_Addr1", "#N-ADDR1", FieldType.STRING, 35);
        pnd_N_Addr2 = localVariables.newFieldInRecord("pnd_N_Addr2", "#N-ADDR2", FieldType.STRING, 35);
        pnd_N_Addr3 = localVariables.newFieldInRecord("pnd_N_Addr3", "#N-ADDR3", FieldType.STRING, 35);
        pnd_N_Zip_All = localVariables.newFieldInRecord("pnd_N_Zip_All", "#N-ZIP-ALL", FieldType.STRING, 10);

        pnd_N_Zip_All__R_Field_4 = localVariables.newGroupInRecord("pnd_N_Zip_All__R_Field_4", "REDEFINE", pnd_N_Zip_All);
        pnd_N_Zip_All_Pnd_N_Zip_F = pnd_N_Zip_All__R_Field_4.newFieldInGroup("pnd_N_Zip_All_Pnd_N_Zip_F", "#N-ZIP-F", FieldType.STRING, 5);
        pnd_N_Zip_All_Pnd_N_Zip = pnd_N_Zip_All__R_Field_4.newFieldInGroup("pnd_N_Zip_All_Pnd_N_Zip", "#N-ZIP", FieldType.STRING, 5);
        pnd_Date_G = localVariables.newFieldInRecord("pnd_Date_G", "#DATE-G", FieldType.STRING, 20);

        pnd_Date_G__R_Field_5 = localVariables.newGroupInRecord("pnd_Date_G__R_Field_5", "REDEFINE", pnd_Date_G);
        pnd_Date_G_Pnd_Date_Filler = pnd_Date_G__R_Field_5.newFieldInGroup("pnd_Date_G_Pnd_Date_Filler", "#DATE-FILLER", FieldType.STRING, 2);
        pnd_Date_G_Pnd_Date_Label = pnd_Date_G__R_Field_5.newFieldInGroup("pnd_Date_G_Pnd_Date_Label", "#DATE-LABEL", FieldType.STRING, 18);
        pnd_Unique_Id = localVariables.newFieldInRecord("pnd_Unique_Id", "#UNIQUE-ID", FieldType.NUMERIC, 12);

        pnd_Unique_Id__R_Field_6 = localVariables.newGroupInRecord("pnd_Unique_Id__R_Field_6", "REDEFINE", pnd_Unique_Id);
        pnd_Unique_Id_Pnd_Unique_Id_1 = pnd_Unique_Id__R_Field_6.newFieldInGroup("pnd_Unique_Id_Pnd_Unique_Id_1", "#UNIQUE-ID-1", FieldType.NUMERIC, 1);
        pnd_Unique_Id_Pnd_Unique_Id_2 = pnd_Unique_Id__R_Field_6.newFieldInGroup("pnd_Unique_Id_Pnd_Unique_Id_2", "#UNIQUE-ID-2", FieldType.NUMERIC, 1);
        pnd_Unique_Id_Pnd_Unique_Id_3 = pnd_Unique_Id__R_Field_6.newFieldInGroup("pnd_Unique_Id_Pnd_Unique_Id_3", "#UNIQUE-ID-3", FieldType.NUMERIC, 1);
        pnd_Unique_Id_Pnd_Unique_Id_4 = pnd_Unique_Id__R_Field_6.newFieldInGroup("pnd_Unique_Id_Pnd_Unique_Id_4", "#UNIQUE-ID-4", FieldType.NUMERIC, 1);
        pnd_Unique_Id_Pnd_Unique_Id_5 = pnd_Unique_Id__R_Field_6.newFieldInGroup("pnd_Unique_Id_Pnd_Unique_Id_5", "#UNIQUE-ID-5", FieldType.NUMERIC, 1);
        pnd_Unique_Id_Pnd_Unique_Id_6 = pnd_Unique_Id__R_Field_6.newFieldInGroup("pnd_Unique_Id_Pnd_Unique_Id_6", "#UNIQUE-ID-6", FieldType.NUMERIC, 1);
        pnd_Unique_Id_Pnd_Unique_Id_7 = pnd_Unique_Id__R_Field_6.newFieldInGroup("pnd_Unique_Id_Pnd_Unique_Id_7", "#UNIQUE-ID-7", FieldType.NUMERIC, 1);
        pnd_Unique_Id_Pnd_Unique_Id_8 = pnd_Unique_Id__R_Field_6.newFieldInGroup("pnd_Unique_Id_Pnd_Unique_Id_8", "#UNIQUE-ID-8", FieldType.NUMERIC, 1);
        pnd_Unique_Id_Pnd_Unique_Id_9 = pnd_Unique_Id__R_Field_6.newFieldInGroup("pnd_Unique_Id_Pnd_Unique_Id_9", "#UNIQUE-ID-9", FieldType.NUMERIC, 1);
        pnd_Unique_Id_Pnd_Unique_Id_10 = pnd_Unique_Id__R_Field_6.newFieldInGroup("pnd_Unique_Id_Pnd_Unique_Id_10", "#UNIQUE-ID-10", FieldType.NUMERIC, 
            1);
        pnd_Unique_Id_Pnd_Unique_Id_11 = pnd_Unique_Id__R_Field_6.newFieldInGroup("pnd_Unique_Id_Pnd_Unique_Id_11", "#UNIQUE-ID-11", FieldType.NUMERIC, 
            1);
        pnd_Unique_Id_Pnd_Unique_Id_12 = pnd_Unique_Id__R_Field_6.newFieldInGroup("pnd_Unique_Id_Pnd_Unique_Id_12", "#UNIQUE-ID-12", FieldType.NUMERIC, 
            1);
        xics_Record = localVariables.newFieldInRecord("xics_Record", "XICS-RECORD", FieldType.STRING, 80);

        xics_File_Dflts = localVariables.newGroupInRecord("xics_File_Dflts", "XICS-FILE-DFLTS");
        xics_File_Dflts_Pnd_Ln_Start = xics_File_Dflts.newFieldInGroup("xics_File_Dflts_Pnd_Ln_Start", "#LN-START", FieldType.STRING, 1);
        xics_File_Dflts_Pnd_Ln_End = xics_File_Dflts.newFieldInGroup("xics_File_Dflts_Pnd_Ln_End", "#LN-END", FieldType.STRING, 1);
        xics_File_Dflts_Pnd_Comments = xics_File_Dflts.newFieldInGroup("xics_File_Dflts_Pnd_Comments", "#COMMENTS", FieldType.STRING, 4);
        pnd_Letter_Ctr = localVariables.newFieldInRecord("pnd_Letter_Ctr", "#LETTER-CTR", FieldType.NUMERIC, 4);
        pnd_Print = localVariables.newFieldInRecord("pnd_Print", "#PRINT", FieldType.BOOLEAN, 1);
        pnd_Tnt_Cref_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_Tnt_Cref_Cntrct_Nbr", "#TNT-CREF-CNTRCT-NBR", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Bad_Addr_Pin_N12Old = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Bad_Addr_Pin_N12_OLD", "Pnd_Bad_Addr_Pin_N12_OLD", FieldType.NUMERIC, 
            12);
        readWork01Pnd_Bad_Addr_NameOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Bad_Addr_Name_OLD", "Pnd_Bad_Addr_Name_OLD", FieldType.STRING, 
            35);
        readWork01Pnd_Tiaa_Std_Addr_Line2Old = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Tiaa_Std_Addr_Line2_OLD", "Pnd_Tiaa_Std_Addr_Line2_OLD", 
            FieldType.STRING, 35);
        readWork01Pnd_Tiaa_Std_Addr_Line1Old = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Tiaa_Std_Addr_Line1_OLD", "Pnd_Tiaa_Std_Addr_Line1_OLD", 
            FieldType.STRING, 35);
        readWork01Pnd_Tiaa_Std_ZipOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Tiaa_Std_Zip_OLD", "Pnd_Tiaa_Std_Zip_OLD", FieldType.STRING, 
            5);
        readWork01Pnd_Tiaa_Std_Zip_4Old = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Tiaa_Std_Zip_4_OLD", "Pnd_Tiaa_Std_Zip_4_OLD", FieldType.STRING, 
            4);
        readWork01Pnd_Tiaa_Std_Walk_SeqOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Tiaa_Std_Walk_Seq_OLD", "Pnd_Tiaa_Std_Walk_Seq_OLD", 
            FieldType.STRING, 4);
        readWork01Pnd_Bad_Addr_Line_2Old = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Bad_Addr_Line_2_OLD", "Pnd_Bad_Addr_Line_2_OLD", FieldType.STRING, 
            35);
        readWork01Pnd_Bad_Addr_Line_1Old = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Bad_Addr_Line_1_OLD", "Pnd_Bad_Addr_Line_1_OLD", FieldType.STRING, 
            35);
        readWork01Pnd_Bad_Addr_City_StateOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Bad_Addr_City_State_OLD", "Pnd_Bad_Addr_City_State_OLD", 
            FieldType.STRING, 35);
        readWork01Pnd_Bad_Addr_Zip_Plus_4Old = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Bad_Addr_Zip_Plus_4_OLD", "Pnd_Bad_Addr_Zip_Plus_4_OLD", 
            FieldType.STRING, 9);
        readWork01Pnd_Bad_Addr_Rec_CntOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Bad_Addr_Rec_Cnt_OLD", "Pnd_Bad_Addr_Rec_Cnt_OLD", FieldType.NUMERIC, 
            9);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaNasl747a.initializeValues();

        localVariables.reset();
        pnd_Begin_Of_File.setInitialValue("Y");
        xics_File_Dflts_Pnd_Ln_Start.setInitialValue("<");
        xics_File_Dflts_Pnd_Ln_End.setInitialValue(">");
        xics_File_Dflts_Pnd_Comments.setInitialValue("<SK>");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Nasb667() throws Exception
    {
        super("Nasb667");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt0, 0);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 132 PS = 60 SG = OFF ZP = ON;//Natural: FORMAT ( 1 ) LS = 132 PS = 60 SG = OFF ZP = ON
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE
        //*  READ BAD-OUTPUT FROM SORT
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #BAD-OUTPUT
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaNasl747a.getPnd_Bad_Output())))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Count_All.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #COUNT-ALL
            //*  PINE
            pnd_Ppcn_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #PPCN-CNT
            //*  RESET #TNT-CREF-CNTRCT-NBR
            //*  PERFORM READ-COR-FOR-TNT-CNTRCT
            //*  IF #TNT-CREF-CNTRCT-NBR NE ' '
            //*    #PPCN-TABLE (#PPCN-CNT) := #TNT-CREF-CNTRCT-NBR
            //*  ELSE
            //*    #PPCN-TABLE (#PPCN-CNT) := #BAD-ADDR-CNTRCT-NMBR
            //*  END-IF
            //*                                                                                                                                                           //Natural: AT BREAK OF #TIAA-STD-ADDR-SORT-KEY
            readWork01Pnd_Bad_Addr_Pin_N12Old.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Pin_N12());                                                             //Natural: END-WORK
            readWork01Pnd_Bad_Addr_NameOld.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Name());
            readWork01Pnd_Tiaa_Std_Addr_Line2Old.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line2());
            readWork01Pnd_Tiaa_Std_Addr_Line1Old.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Line1());
            readWork01Pnd_Tiaa_Std_ZipOld.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Zip());
            readWork01Pnd_Tiaa_Std_Zip_4Old.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Zip_4());
            readWork01Pnd_Tiaa_Std_Walk_SeqOld.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Walk_Seq());
            readWork01Pnd_Bad_Addr_Line_2Old.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Line_2());
            readWork01Pnd_Bad_Addr_Line_1Old.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Line_1());
            readWork01Pnd_Bad_Addr_City_StateOld.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_City_State());
            readWork01Pnd_Bad_Addr_Zip_Plus_4Old.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Zip_Plus_4());
            readWork01Pnd_Bad_Addr_Rec_CntOld.setValue(ldaNasl747a.getPnd_Bad_Output_Pnd_Bad_Addr_Rec_Cnt());
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, new ColumnSpacing(40),"TOTAL NUMBER OF RECORDS READ   ",pnd_Count_All, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                 //Natural: WRITE 40X 'TOTAL NUMBER OF RECORDS READ   ' #COUNT-ALL ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, new ColumnSpacing(40),"TOTAL NUMBER OF LETTERS        ",pnd_Count_Pin, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                 //Natural: WRITE 40X 'TOTAL NUMBER OF LETTERS        ' #COUNT-PIN ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POSTUP-BARCODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: START1-XICS-FILE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: START2-XICS-FILE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-OLD-LETTER
        //* *
        //* *   SET BAR CODE
        //* *
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-XICS-FILE
    }
    private void sub_Postup_Barcode() throws Exception                                                                                                                    //Natural: POSTUP-BARCODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Pstn9532.class , getCurrentProcessState(), pdaPsta9532.getPsta9532(), pdaCwfpda_M.getMsg_Info_Sub());                                             //Natural: CALLNAT 'PSTN9532' PSTA9532 MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        //*  POSTUP-BARCODE
    }
    private void sub_Start1_Xics_File() throws Exception                                                                                                                  //Natural: START1-XICS-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Comments, " BAD ADDR FILE ", pnd_Count_Pin, " ", Global.getDATN()));     //Natural: COMPRESS #COMMENTS ' BAD ADDR FILE ' #COUNT-PIN ' ' *DATN INTO XICS-RECORD LEAVING NO
        //*  START1-XICS-FILE
    }
    private void sub_Start2_Xics_File() throws Exception                                                                                                                  //Natural: START2-XICS-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Comments, "XICSMET,PFRMT=MET"));                                         //Natural: COMPRESS #COMMENTS 'XICSMET,PFRMT=MET' INTO XICS-RECORD LEAVING NO
        //*  START2-XICS-FILE
    }
    private void sub_Load_Old_Letter() throws Exception                                                                                                                   //Natural: LOAD-OLD-LETTER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *
        //* *   SET UNIQUE ID
        //* *
        if (condition(pnd_Begin_Of_File.equals("Y")))                                                                                                                     //Natural: IF #BEGIN-OF-FILE = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "WIPEOUT", xics_File_Dflts_Pnd_Ln_End));                   //Natural: COMPRESS #LN-START 'WIPEOUT' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#PIN1=", pnd_Unique_Id_Pnd_Unique_Id_1, xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#PIN1=' #UNIQUE-ID-1 #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#PIN2=", pnd_Unique_Id_Pnd_Unique_Id_2, xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#PIN2=' #UNIQUE-ID-2 #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#PIN3=", pnd_Unique_Id_Pnd_Unique_Id_3, xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#PIN3=' #UNIQUE-ID-3 #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#PIN4=", pnd_Unique_Id_Pnd_Unique_Id_4, xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#PIN4=' #UNIQUE-ID-4 #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#PIN5=", pnd_Unique_Id_Pnd_Unique_Id_5, xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#PIN5=' #UNIQUE-ID-5 #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#PIN6=", pnd_Unique_Id_Pnd_Unique_Id_6, xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#PIN6=' #UNIQUE-ID-6 #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#PIN7=", pnd_Unique_Id_Pnd_Unique_Id_7, xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#PIN7=' #UNIQUE-ID-7 #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#PIN8=", pnd_Unique_Id_Pnd_Unique_Id_8, xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#PIN8=' #UNIQUE-ID-8 #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#PIN9=", pnd_Unique_Id_Pnd_Unique_Id_9, xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#PIN9=' #UNIQUE-ID-9 #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#PIN10=", pnd_Unique_Id_Pnd_Unique_Id_10,                     //Natural: COMPRESS #LN-START '#PIN10=' #UNIQUE-ID-10 #LN-END INTO XICS-RECORD LEAVING NO
            xics_File_Dflts_Pnd_Ln_End));
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#PIN11=", pnd_Unique_Id_Pnd_Unique_Id_11,                     //Natural: COMPRESS #LN-START '#PIN11=' #UNIQUE-ID-11 #LN-END INTO XICS-RECORD LEAVING NO
            xics_File_Dflts_Pnd_Ln_End));
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#PIN12=", pnd_Unique_Id_Pnd_Unique_Id_12,                     //Natural: COMPRESS #LN-START '#PIN12=' #UNIQUE-ID-12 #LN-END INTO XICS-RECORD LEAVING NO
            xics_File_Dflts_Pnd_Ln_End));
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        //* *
        //* *   SET BYTE CODE
        //* *
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#BYTE5=0", xics_File_Dflts_Pnd_Ln_End));                      //Natural: COMPRESS #LN-START '#BYTE5=0' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#BYTE4=0", xics_File_Dflts_Pnd_Ln_End));                      //Natural: COMPRESS #LN-START '#BYTE4=0' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#BYTE3=0", xics_File_Dflts_Pnd_Ln_End));                      //Natural: COMPRESS #LN-START '#BYTE3=0' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#BYTE2=0", xics_File_Dflts_Pnd_Ln_End));                      //Natural: COMPRESS #LN-START '#BYTE2=0' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        pdaPsta9532.getPsta9532_In_Zip_5().setValue(pnd_Mn_Postnet_Zip);                                                                                                  //Natural: ASSIGN IN-ZIP-5 := #MN-POSTNET-ZIP
        pdaPsta9532.getPsta9532_In_Zip_4().setValue(pnd_Mn_Postnet_Zip4);                                                                                                 //Natural: ASSIGN IN-ZIP-4 := #MN-POSTNET-ZIP4
        pdaPsta9532.getPsta9532_In_Addrss_Wlk_Rte_Cde().setValue(pnd_Mn_Postnet_Barcode);                                                                                 //Natural: ASSIGN IN-ADDRSS-WLK-RTE-CDE := #MN-POSTNET-BARCODE
                                                                                                                                                                          //Natural: PERFORM POSTUP-BARCODE
        sub_Postup_Barcode();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#POST=", pdaPsta9532.getPsta9532_Full_Postnet_Bar_Cde(),      //Natural: COMPRESS #LN-START '#POST=' PSTA9532.FULL-POSTNET-BAR-CDE #LN-END INTO XICS-RECORD LEAVING NO
            xics_File_Dflts_Pnd_Ln_End));
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        //* *
        //* *   DATE
        //* *
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#LDATE=", pnd_Date_G_Pnd_Date_Label, xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#LDATE=' #DATE-LABEL #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        //* *
        //* *   NAME
        //* *
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#P010=", pnd_Name, xics_File_Dflts_Pnd_Ln_End));              //Natural: COMPRESS #LN-START '#P010=' #NAME #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        //* *
        //* * MAIL TO ADDRESS INFORMATION
        //* *
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#ADD1=", pnd_Mn_Addr1, xics_File_Dflts_Pnd_Ln_End));          //Natural: COMPRESS #LN-START '#ADD1=' #MN-ADDR1 #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Mn_Addr3.greater(" ")))                                                                                                                         //Natural: IF #MN-ADDR3 > ' '
        {
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#ADD2=", pnd_Mn_Addr2, " ", xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#ADD2=' #MN-ADDR2 ' ' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.examine(new ExamineSource(pnd_Mn_Addr3), new ExamineSearch(pnd_Mn_Zip_All_Pnd_Mn_Zip), new ExamineReplace("     "));                                  //Natural: EXAMINE #MN-ADDR3 FOR #MN-ZIP REPLACE WITH '     '
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#ADD3=", pnd_Mn_Addr3, " ", pnd_Mn_Zip_All,               //Natural: COMPRESS #LN-START '#ADD3=' #MN-ADDR3 ' ' #MN-ZIP-ALL #LN-END INTO XICS-RECORD LEAVING NO
                xics_File_Dflts_Pnd_Ln_End));
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.examine(new ExamineSource(pnd_Mn_Addr2), new ExamineSearch(pnd_Mn_Zip_All_Pnd_Mn_Zip), new ExamineReplace("     "));                                  //Natural: EXAMINE #MN-ADDR2 FOR #MN-ZIP REPLACE WITH '     '
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#ADD2=", pnd_Mn_Addr2, " ", pnd_Mn_Zip_All,               //Natural: COMPRESS #LN-START '#ADD2=' #MN-ADDR2 ' ' #MN-ZIP-ALL #LN-END INTO XICS-RECORD LEAVING NO
                xics_File_Dflts_Pnd_Ln_End));
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#ADD3=", xics_File_Dflts_Pnd_Ln_End));                    //Natural: COMPRESS #LN-START '#ADD3=' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#ADD4=", xics_File_Dflts_Pnd_Ln_End));                        //Natural: COMPRESS #LN-START '#ADD4=' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#ADD5=", xics_File_Dflts_Pnd_Ln_End));                        //Natural: COMPRESS #LN-START '#ADD5=' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        //* *
        //* *   PST NUMBER
        //* *
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#X010=", pnd_Pst_Seq_No, xics_File_Dflts_Pnd_Ln_End));        //Natural: COMPRESS #LN-START '#X010='#PST-SEQ-NO #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        //* *
        //* * BAD ADDRESS INFORMATION
        //* *
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#CURAD1=<NL>", pnd_O_Addr1, xics_File_Dflts_Pnd_Ln_End));     //Natural: COMPRESS #LN-START '#CURAD1=<NL>' #O-ADDR1 #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_O_Addr3.greater(" ")))                                                                                                                          //Natural: IF #O-ADDR3 > ' '
        {
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#CURAD2=<NL>", pnd_O_Addr2, " ", xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#CURAD2=<NL>' #O-ADDR2 ' ' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#CURAD3=<NL>", pnd_O_Addr3, " ", pnd_O_Zip_All,           //Natural: COMPRESS #LN-START '#CURAD3=<NL>' #O-ADDR3 ' ' #O-ZIP-ALL #LN-END INTO XICS-RECORD LEAVING NO
                xics_File_Dflts_Pnd_Ln_End));
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#CURAD2=<NL>", pnd_O_Addr2, " ", pnd_O_Zip_All,           //Natural: COMPRESS #LN-START '#CURAD2=<NL>' #O-ADDR2 ' ' #O-ZIP-ALL #LN-END INTO XICS-RECORD LEAVING NO
                xics_File_Dflts_Pnd_Ln_End));
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#CURAD3=<NL>", xics_File_Dflts_Pnd_Ln_End));              //Natural: COMPRESS #LN-START '#CURAD3=<NL>' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#CURAD4=<NL>", xics_File_Dflts_Pnd_Ln_End));                  //Natural: COMPRESS #LN-START '#CURAD4=<NL>' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        //* *
        //* * BODY TEXT
        //* *
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "BEGDOC", xics_File_Dflts_Pnd_Ln_End));                        //Natural: COMPRESS #LN-START 'BEGDOC' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "DFSET", xics_File_Dflts_Pnd_Ln_End));                         //Natural: COMPRESS #LN-START 'DFSET' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "BARLET", xics_File_Dflts_Pnd_Ln_End));                        //Natural: COMPRESS #LN-START 'BARLET' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        //* *
        //* * CURRENT ADDRESS INFORMATION
        //* *
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#NEWAD1=<NL>", pnd_N_Addr1, xics_File_Dflts_Pnd_Ln_End));     //Natural: COMPRESS #LN-START '#NEWAD1=<NL>' #N-ADDR1 #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_N_Addr3.greater(" ")))                                                                                                                          //Natural: IF #N-ADDR3 > ' '
        {
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#NEWAD2=<NL>", pnd_N_Addr2, " ", xics_File_Dflts_Pnd_Ln_End)); //Natural: COMPRESS #LN-START '#NEWAD2=<NL>' #N-ADDR2 ' ' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.examine(new ExamineSource(pnd_N_Addr3), new ExamineSearch(pnd_N_Zip_All_Pnd_N_Zip), new ExamineReplace("     "));                                     //Natural: EXAMINE #N-ADDR3 FOR #N-ZIP REPLACE WITH '     '
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#NEWAD3=<NL>", pnd_N_Addr3, " ", pnd_N_Zip_All,           //Natural: COMPRESS #LN-START '#NEWAD3=<NL>' #N-ADDR3 ' ' #N-ZIP-ALL #LN-END INTO XICS-RECORD LEAVING NO
                xics_File_Dflts_Pnd_Ln_End));
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.examine(new ExamineSource(pnd_N_Addr2), new ExamineSearch(pnd_N_Zip_All_Pnd_N_Zip), new ExamineReplace("     "));                                     //Natural: EXAMINE #N-ADDR2 FOR #N-ZIP REPLACE WITH '     '
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#NEWAD2=<NL>", pnd_N_Addr2, " ", pnd_N_Zip_All,           //Natural: COMPRESS #LN-START '#NEWAD2=<NL>' #N-ADDR2 ' ' #N-ZIP-ALL #LN-END INTO XICS-RECORD LEAVING NO
                xics_File_Dflts_Pnd_Ln_End));
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
            xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#NEWAD3=<NL>", xics_File_Dflts_Pnd_Ln_End));              //Natural: COMPRESS #LN-START '#NEWAD3=<NL>' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
            sub_Write_Xics_File();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "#NEWAD4=<NL>", xics_File_Dflts_Pnd_Ln_End));                  //Natural: COMPRESS #LN-START '#NEWAD4=<NL>' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        //* *
        //* * BODY TEXT
        //* *
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "BARA2", xics_File_Dflts_Pnd_Ln_End));                         //Natural: COMPRESS #LN-START 'BARA2' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "NOTIFY", xics_File_Dflts_Pnd_Ln_End));                        //Natural: COMPRESS #LN-START 'NOTIFY' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "CLOSE", xics_File_Dflts_Pnd_Ln_End));                         //Natural: COMPRESS #LN-START 'CLOSE' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        //* *
        //* * LOAD CONTRACTS
        //* *
        //*  RESET #PPCN-CNT-2 #PPCN-CNT-22
        //*  REPEAT
        //*    ADD 1 TO #PPCN-CNT-2
        //*    ADD 1 TO #PPCN-CNT-22
        //*   IF #PPCN-CNT-2 > 20
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   IF #PPCN-TABLE (#PPCN-CNT-2) = ' '
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   IF #PPCN-CNT-22 = 5
        //*     #PPCN-CNT-22 := 1
        //*   END-IF
        //*   #PPCN-EDITMASK := #PPCN-TABLE (#PPCN-CNT-2)
        //*   COMPRESS #LN-START 'CN' #PPCN-CNT-22 #LN-END #PPCN-EDITMASK
        //*     INTO XICS-RECORD LEAVING NO
        //*   PERFORM WRITE-XICS-FILE
        //*  END-REPEAT
        //* *
        //* * BODY TEXT
        //* *
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "POSTS", xics_File_Dflts_Pnd_Ln_End));                         //Natural: COMPRESS #LN-START 'POSTS' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "DFRES", xics_File_Dflts_Pnd_Ln_End));                         //Natural: COMPRESS #LN-START 'DFRES' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        xics_Record.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, xics_File_Dflts_Pnd_Ln_Start, "ENDDOC", xics_File_Dflts_Pnd_Ln_End));                        //Natural: COMPRESS #LN-START 'ENDDOC' #LN-END INTO XICS-RECORD LEAVING NO
                                                                                                                                                                          //Natural: PERFORM WRITE-XICS-FILE
        sub_Write_Xics_File();
        if (condition(Global.isEscape())) {return;}
        //*  LOAD-OLD-LETTER
    }
    private void sub_Write_Xics_File() throws Exception                                                                                                                   //Natural: WRITE-XICS-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Count_Pin.equals(1) && pnd_Begin_Of_File.equals("Y")))                                                                                          //Natural: IF #COUNT-PIN = 1 AND #BEGIN-OF-FILE = 'Y'
        {
            getReports().write(0, "WORK FILE 02 :");                                                                                                                      //Natural: WRITE 'WORK FILE 02 :'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM START1-XICS-FILE
            sub_Start1_Xics_File();
            if (condition(Global.isEscape())) {return;}
            getWorkFiles().write(2, false, xics_Record);                                                                                                                  //Natural: WRITE WORK FILE 02 XICS-RECORD
            xics_Record.reset();                                                                                                                                          //Natural: RESET XICS-RECORD
                                                                                                                                                                          //Natural: PERFORM START2-XICS-FILE
            sub_Start2_Xics_File();
            if (condition(Global.isEscape())) {return;}
            getWorkFiles().write(2, false, xics_Record);                                                                                                                  //Natural: WRITE WORK FILE 02 XICS-RECORD
            xics_Record.reset();                                                                                                                                          //Natural: RESET XICS-RECORD
            pnd_Begin_Of_File.setValue("N");                                                                                                                              //Natural: ASSIGN #BEGIN-OF-FILE := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Print.getBoolean()))                                                                                                                        //Natural: IF #PRINT
            {
                getReports().write(0, xics_Record);                                                                                                                       //Natural: WRITE XICS-RECORD
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(2, false, xics_Record);                                                                                                                  //Natural: WRITE WORK FILE 02 XICS-RECORD
            xics_Record.reset();                                                                                                                                          //Natural: RESET XICS-RECORD
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-XICS-FILE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(0, new ColumnSpacing(58),"TIAA/CREF",NEWLINE,new ColumnSpacing(10),"RUN DATE  : ",Global.getDATU(),new ColumnSpacing(15),"PENSIONS AND ANNUITIES PROJECT",new  //Natural: WRITE 58X 'TIAA/CREF' / 10X 'RUN DATE  : ' *DATU 15X 'PENSIONS AND ANNUITIES PROJECT' 12X 'PROGRAM ID: ' *PROGRAM / 10X 'RUN TIME  : ' *TIME 16X ' NAME AND ADDRESS REPORT' 15X 'PAGE      : ' *PAGE-NUMBER /// 35X 'Bad Address Letter File Generator for Name and Address' ////
                        ColumnSpacing(12),"PROGRAM ID: ",Global.getPROGRAM(),NEWLINE,new ColumnSpacing(10),"RUN TIME  : ",Global.getTIME(),new ColumnSpacing(16)," NAME AND ADDRESS REPORT",new 
                        ColumnSpacing(15),"PAGE      : ",getReports().getPageNumberDbs(0),NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(35),"Bad Address Letter File Generator for Name and Address",
                        NEWLINE,NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaNasl747a_getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Sort_KeyIsBreak = ldaNasl747a.getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Sort_Key().isBreak(endOfData);
        if (condition(ldaNasl747a_getPnd_Bad_Output_Pnd_Tiaa_Std_Addr_Sort_KeyIsBreak))
        {
            pnd_Unique_Id.setValue(readWork01Pnd_Bad_Addr_Pin_N12Old);                                                                                                    //Natural: ASSIGN #UNIQUE-ID := OLD ( #BAD-ADDR-PIN-N12 )
            pnd_Date_G.setValue(Global.getDATG());                                                                                                                        //Natural: ASSIGN #DATE-G := *DATG
            pnd_Name.setValue(readWork01Pnd_Bad_Addr_NameOld);                                                                                                            //Natural: ASSIGN #NAME := OLD ( #BAD-ADDR-NAME )
            //*                                       NEW ADDRESS
            pnd_Mn_Addr2.setValue(readWork01Pnd_Tiaa_Std_Addr_Line2Old);                                                                                                  //Natural: ASSIGN #MN-ADDR2 := #N-ADDR2 := OLD ( #TIAA-STD-ADDR-LINE2 )
            pnd_N_Addr2.setValue(readWork01Pnd_Tiaa_Std_Addr_Line2Old);
            pnd_Mn_Addr1.setValue(readWork01Pnd_Tiaa_Std_Addr_Line1Old);                                                                                                  //Natural: ASSIGN #MN-ADDR1 := #N-ADDR1 := OLD ( #TIAA-STD-ADDR-LINE1 )
            pnd_N_Addr1.setValue(readWork01Pnd_Tiaa_Std_Addr_Line1Old);
            pnd_Mn_Addr3.reset();                                                                                                                                         //Natural: RESET #MN-ADDR3 #N-ADDR3
            pnd_N_Addr3.reset();
            pnd_Mn_Zip_All_Pnd_Mn_Zip.setValue(readWork01Pnd_Tiaa_Std_ZipOld);                                                                                            //Natural: ASSIGN #MN-ZIP := #N-ZIP := OLD ( #TIAA-STD-ZIP )
            pnd_N_Zip_All_Pnd_N_Zip.setValue(readWork01Pnd_Tiaa_Std_ZipOld);
            pnd_Mn_Postnet_Zip.setValue(readWork01Pnd_Tiaa_Std_ZipOld);                                                                                                   //Natural: ASSIGN #MN-POSTNET-ZIP := OLD ( #TIAA-STD-ZIP )
            pnd_Mn_Postnet_Zip4.setValue(readWork01Pnd_Tiaa_Std_Zip_4Old);                                                                                                //Natural: ASSIGN #MN-POSTNET-ZIP4 := OLD ( #TIAA-STD-ZIP-4 )
            pnd_Mn_Postnet_Barcode.setValue(readWork01Pnd_Tiaa_Std_Walk_SeqOld);                                                                                          //Natural: ASSIGN #MN-POSTNET-BARCODE := OLD ( #TIAA-STD-WALK-SEQ )
            //*                                       OLD ADDRESS
            pnd_Mo_Addr2.setValue(readWork01Pnd_Bad_Addr_Line_2Old);                                                                                                      //Natural: ASSIGN #MO-ADDR2 := #O-ADDR2 := OLD ( #BAD-ADDR-LINE-2 )
            pnd_O_Addr2.setValue(readWork01Pnd_Bad_Addr_Line_2Old);
            pnd_Mo_Addr1.setValue(readWork01Pnd_Bad_Addr_Line_1Old);                                                                                                      //Natural: ASSIGN #MO-ADDR1 := #O-ADDR1 := OLD ( #BAD-ADDR-LINE-1 )
            pnd_O_Addr1.setValue(readWork01Pnd_Bad_Addr_Line_1Old);
            if (condition(pnd_Mo_Addr2.equals(" ")))                                                                                                                      //Natural: IF #MO-ADDR2 = ' '
            {
                pnd_Mo_Addr2.setValue(readWork01Pnd_Bad_Addr_City_StateOld);                                                                                              //Natural: ASSIGN #MO-ADDR2 := #O-ADDR2 := OLD ( #BAD-ADDR-CITY-STATE )
                pnd_O_Addr2.setValue(readWork01Pnd_Bad_Addr_City_StateOld);
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Mo_Addr3.setValue(readWork01Pnd_Bad_Addr_City_StateOld);                                                                                              //Natural: ASSIGN #MO-ADDR3 := #O-ADDR3 := OLD ( #BAD-ADDR-CITY-STATE )
                pnd_O_Addr3.setValue(readWork01Pnd_Bad_Addr_City_StateOld);
            }                                                                                                                                                             //Natural: END-IF
            pnd_Mo_Zip_All_Pnd_Mo_Zip.setValue(readWork01Pnd_Bad_Addr_Zip_Plus_4Old);                                                                                     //Natural: ASSIGN #MO-ZIP := #O-ZIP := OLD ( #BAD-ADDR-ZIP-PLUS-4 )
            pnd_O_Zip_All_Pnd_O_Zip.setValue(readWork01Pnd_Bad_Addr_Zip_Plus_4Old);
            pnd_Pst_Seq_No.setValue(readWork01Pnd_Bad_Addr_Rec_CntOld);                                                                                                   //Natural: ASSIGN #PST-SEQ-NO := OLD ( #BAD-ADDR-REC-CNT )
            pnd_Count_Pin.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #COUNT-PIN
            pnd_Print.reset();                                                                                                                                            //Natural: RESET #PRINT
            pnd_Letter_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #LETTER-CTR
            if (condition(pnd_Letter_Ctr.greaterOrEqual(500)))                                                                                                            //Natural: IF #LETTER-CTR GE 500
            {
                pnd_Print.setValue(true);                                                                                                                                 //Natural: ASSIGN #PRINT := TRUE
                pnd_Letter_Ctr.reset();                                                                                                                                   //Natural: RESET #LETTER-CTR
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOAD-OLD-LETTER
            sub_Load_Old_Letter();
            if (condition(Global.isEscape())) {return;}
            pnd_Ppcn_Table.getValue("*").reset();                                                                                                                         //Natural: RESET #PPCN-TABLE ( * ) #PPCN-CNT-2 #PPCN-CNT
            pnd_Ppcn_Cnt_2.reset();
            pnd_Ppcn_Cnt.reset();
            pnd_Mo_Addr1.reset();                                                                                                                                         //Natural: RESET #MO-ADDR1 #MO-ADDR2 #MO-ADDR3 #MN-ADDR1 #MN-ADDR2 #MN-ADDR3 #N-ADDR1 #N-ADDR2 #N-ADDR3 #O-ADDR1 #O-ADDR2 #O-ADDR3
            pnd_Mo_Addr2.reset();
            pnd_Mo_Addr3.reset();
            pnd_Mn_Addr1.reset();
            pnd_Mn_Addr2.reset();
            pnd_Mn_Addr3.reset();
            pnd_N_Addr1.reset();
            pnd_N_Addr2.reset();
            pnd_N_Addr3.reset();
            pnd_O_Addr1.reset();
            pnd_O_Addr2.reset();
            pnd_O_Addr3.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60 SG=OFF ZP=ON");
        Global.format(1, "LS=132 PS=60 SG=OFF ZP=ON");
    }
}
