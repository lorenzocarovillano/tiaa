/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:59:31 PM
**        * FROM NATURAL PROGRAM : Nasb490k
************************************************************
**        * FILE NAME            : Nasb490k.java
**        * CLASS NAME           : Nasb490k
**        * INSTANCE NAME        : Nasb490k
************************************************************
************************************************************************
* PROGRAM NAME : NASB490K
* DATE WRITTEN : AUG 2017
* CREATED BY   : C.DIMITRIU (COPIED FROM NASB490P)
* DESCRIPTION  : THIS PROGRAM WILL READ WORK FILE CREATED IN MDM AND
*                WILL CALL MDMN* SUBPROGRAMS TO GET ADDRESS INFORMATION.
*                SEND SAME MAILING/NEW ADDRESS IF PREVIOUS ADDRESS=BAD
************************************************************************
* DATE        USERID    DESCRIPTION
* 09/01/2011  DURAND    ADD RESIDENTIAL ADDRESS TO CORRESPONDENCE LETTER
* 09/11/2013  DURAND    PRINT 'CORRESPONDENCE ADDRESS ON FILE' IF CHANGE
*                       IS FOR RESIDENTIAL ADDRESS ONLY
* 01/2017     MEADED    PIN IN INPUT RECORD LENGTHENED FROM 7 TO 12.
*                       FOR INITIAL PIN EXPANSION IMPLEMENTATION,
*                       CONTINUE TO PASS 7-BYTE PIN TO MDMN* AND
*                       WRITE 7-BYTE PIN TO OUTPUT FILE USED BY
*                       NASB491, WHICH CALLS NASN191 TO CREATE CWF
*                       WORK REQUESTS, EFM DOCS.
*                       SEE DM 1/17 COMMENTS
* 02/2017     MEADED    PIN IN INPUT RECORD MUST BE ALPHA AS TRAILING
*                       BLANKS WILL FOLLOW 7-BYTE NUMERIC PIN's.
*                       SEE DM 2/17
* 06/2017     MEADED    AUGUST PIN EXPANSION VERSION. FULL 12-BYTE
*                       FUNCTIONALITY. CALL NEW VERSIONS OF MDMN*
*                       MODULES. SEE PINE COMMENTS.
*                       CALL TO MDMN211 COULD NOT COMPLETELY
*                       REPLACE THE COR LOOKUP, SO COMMENTED OUT.
*                       WILL HAVE TO REPLACE COR LATER.
* 08/13/17    MEADED    CORRECTED BAD CALL TO "MDMN321A"
* 09/17       DIMITRIU  ADDED NEW OUTPUT CONTRACT FIELD - SEE KCD.
* 06/16/19    MEADED    REMOVED COR ACCESS. NO NEED TO CHECK FOR
*                       MAYO, TNT INDICATORS OR DA ICAP TPA/IPRO.
*                       THOSE CONTRACTS WILL NO LONGER BE ON THE
*                       INPUT FILE FROM MDM. MOVED INPUT
*                       C-CONTRACT-2 (WHICH MAY NOT BE POPULATED)
*                       TO OUTPUT CREF NUMBER. UPDATED INPUT RECORD
*                       LENGTH TO CORRESPOND TO 132-BYTE LENGTH OF
*                       INPUT FILE. SEE DJM
* 09/30/19    MEADED    PREVIOUS CHANGE HAD TO BE BACKED OUT. SHOULD
*                       MOVE INPUT C-CONTRACT-2 TO #O-CONTRACT-NO
*                       ONLY IF IT IS POPULATED FOR A NON-RESIDENTIAL
*                       ADDRESS CHANGE. SEE DJM2
************************************************************************

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nasb490k extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma101 pdaMdma101;
    private PdaMdma321 pdaMdma321;
    private PdaMdma331 pdaMdma331;
    private PdaMdma361 pdaMdma361;
    private PdaMdma190 pdaMdma190;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup confirmation_File;
    private DbsField confirmation_File_C_Ph_Unique_Id_Nmbr;
    private DbsField confirmation_File_C_Cntrct_Type;

    private DbsGroup confirmation_File_C_Unique_Address;
    private DbsField confirmation_File_C_Cntrct_Nmbr;

    private DbsGroup confirmation_File__R_Field_1;
    private DbsField confirmation_File_Pnd_C_Cntrct_Nmbr_Prefix;
    private DbsField confirmation_File_Section;
    private DbsField confirmation_File_C_Cntrct_Payee_Cde;
    private DbsField confirmation_File_C_Usage_Type;
    private DbsField confirmation_File_C_Addr_Actvty_Cde;
    private DbsField confirmation_File_C_Actvty_Cde;

    private DbsGroup confirmation_File_C_Original_Data;
    private DbsField confirmation_File_C_Original_Chnge_Dte;

    private DbsGroup confirmation_File__R_Field_2;
    private DbsField confirmation_File_C_Orig_Chnge_Dte_A;
    private DbsField confirmation_File_C_Original_Chnge_Tme;

    private DbsGroup confirmation_File__R_Field_3;
    private DbsField confirmation_File_C_Original_Chnge_Tme_A;
    private DbsField confirmation_File_C_Orig_Chnge_Batch_No;
    private DbsField confirmation_File_C_Addrss_Eff_Dte;
    private DbsField confirmation_File_C_Verify_Ind;
    private DbsField confirmation_File_C_Confirmation_Ind;
    private DbsField confirmation_File_C_Name_Key;
    private DbsField confirmation_File_C_Mit_Wpid;
    private DbsField confirmation_File_C_Mit_Unit_Cde;
    private DbsField confirmation_File_C_Mit_Log_Date_Time;
    private DbsField confirmation_File_C_Mit_Status_Cde;
    private DbsField confirmation_File_C_Contract_2;
    private DbsField confirmation_File_Filler;

    private DbsGroup pnd_Work_Letter_Image_File;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Pin_No;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Contract_No;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Actvty_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Usage;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Change_Dt;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Change_Tm;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Effective_Date;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Effective_Date_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Verify_Ind;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Addr_Type;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Name;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_1;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_3;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Lob;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Contract_Type_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Type;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_6;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Name;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_1;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_3;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_7;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_8;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_9;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Section;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Status_Cde;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Mit_Error;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Compress_Address;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Override;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_1;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_3;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Contract2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Filler;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_10;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Work;
    private DbsField pnd_Addr_Current;
    private DbsField pnd_Addr_New;
    private DbsField pnd_Addr_Mailto;
    private DbsField pnd_Comp_Date_A8;

    private DbsGroup pnd_Comp_Date_A8__R_Field_11;
    private DbsField pnd_Comp_Date_A8_Pnd_Comp_Date;
    private DbsField pnd_Datx;
    private DbsField pnd_Err_Addr_Type;
    private DbsField pnd_Err_Date;
    private DbsField pnd_Err_Letter;
    private DbsField pnd_Err_Msg;
    private DbsField pnd_Err_Prfx;
    private DbsField pnd_Err_Time;
    private DbsField pnd_Err_Tran;

    private DbsGroup pnd_Format_Addr;
    private DbsField pnd_Format_Addr_Pnd_C;
    private DbsField pnd_Format_Addr_Pnd_City_State_Zip;
    private DbsField pnd_Format_Addr_Pnd_In_Addr_Type;
    private DbsField pnd_Format_Addr_Pnd_In_Addr1;
    private DbsField pnd_Format_Addr_Pnd_In_Addr2;
    private DbsField pnd_Format_Addr_Pnd_In_Addr3;
    private DbsField pnd_Format_Addr_Pnd_In_Addr4;
    private DbsField pnd_Format_Addr_Pnd_In_City;
    private DbsField pnd_Format_Addr_Pnd_In_St_Prov;
    private DbsField pnd_Format_Addr_Pnd_In_Zip;

    private DbsGroup pnd_Format_Addr__R_Field_12;
    private DbsField pnd_Format_Addr_Pnd_In_Zip5;
    private DbsField pnd_Format_Addr_Pnd_In_Country;
    private DbsField pnd_Format_Addr_Pnd_Out_Addr;
    private DbsField pnd_Format_Addr_Pnd_S;

    private DbsGroup pnd_Format_Name;
    private DbsField pnd_Format_Name_Pnd_In_Lname;
    private DbsField pnd_Format_Name_Pnd_In_Fname;
    private DbsField pnd_Format_Name_Pnd_In_Mname;

    private DbsGroup pnd_Format_Name__R_Field_13;
    private DbsField pnd_Format_Name_Pnd_In_Mname_1;
    private DbsField pnd_Format_Name_Pnd_In_Prfx;
    private DbsField pnd_Format_Name_Pnd_In_Sffx;
    private DbsField pnd_Format_Name_Pnd_Out_Pref_Name;

    private DbsGroup pnd_Format_Name__R_Field_14;
    private DbsField pnd_Format_Name_Pnd_Out_Name;
    private DbsField pnd_Format_Name_Pnd_Out_Name_36_70;

    private DbsGroup pnd_Group_Address;
    private DbsField pnd_Group_Address_Pnd_Save_Address_Good;
    private DbsField pnd_Group_Address_Pnd_Save_Address_1;
    private DbsField pnd_Group_Address_Pnd_Save_Address_2;
    private DbsField pnd_Group_Address_Pnd_Save_Address_3;
    private DbsField pnd_Group_Address_Pnd_Save_Address_4;
    private DbsField pnd_Group_Address_Pnd_Save_Address_5;
    private DbsField pnd_Group_Address_Pnd_Save_Postal_Data;
    private DbsField pnd_L;
    private DbsField pnd_L_Acct_No;
    private DbsField pnd_L_Date;
    private DbsField pnd_L_Date_F;
    private DbsField pnd_L_Eft_No;
    private DbsField pnd_L_Format_Cd;
    private DbsField pnd_L_Time;
    private DbsField pnd_L_Time_A;

    private DbsGroup pnd_L_Time_A__R_Field_15;
    private DbsField pnd_L_Time_A_Pnd_L_Time_N;
    private DbsField pnd_L_Time_F;
    private DbsField pnd_Previous_Bad;
    private DbsField pnd_Same_Contract;
    private DbsField pnd_Save_Pin;
    private DbsField pnd_Total_Error;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Total_Write;
    private DbsField pnd_X;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma321 = new PdaMdma321(localVariables);
        pdaMdma331 = new PdaMdma331(localVariables);
        pdaMdma361 = new PdaMdma361(localVariables);
        pdaMdma190 = new PdaMdma190(localVariables);

        // Local Variables

        confirmation_File = localVariables.newGroupInRecord("confirmation_File", "CONFIRMATION-FILE");
        confirmation_File_C_Ph_Unique_Id_Nmbr = confirmation_File.newFieldInGroup("confirmation_File_C_Ph_Unique_Id_Nmbr", "C-PH-UNIQUE-ID-NMBR", FieldType.STRING, 
            12);
        confirmation_File_C_Cntrct_Type = confirmation_File.newFieldInGroup("confirmation_File_C_Cntrct_Type", "C-CNTRCT-TYPE", FieldType.STRING, 1);

        confirmation_File_C_Unique_Address = confirmation_File.newGroupInGroup("confirmation_File_C_Unique_Address", "C-UNIQUE-ADDRESS");
        confirmation_File_C_Cntrct_Nmbr = confirmation_File_C_Unique_Address.newFieldInGroup("confirmation_File_C_Cntrct_Nmbr", "C-CNTRCT-NMBR", FieldType.STRING, 
            10);

        confirmation_File__R_Field_1 = confirmation_File_C_Unique_Address.newGroupInGroup("confirmation_File__R_Field_1", "REDEFINE", confirmation_File_C_Cntrct_Nmbr);
        confirmation_File_Pnd_C_Cntrct_Nmbr_Prefix = confirmation_File__R_Field_1.newFieldInGroup("confirmation_File_Pnd_C_Cntrct_Nmbr_Prefix", "#C-CNTRCT-NMBR-PREFIX", 
            FieldType.STRING, 3);
        confirmation_File_Section = confirmation_File_C_Unique_Address.newFieldInGroup("confirmation_File_Section", "SECTION", FieldType.STRING, 1);
        confirmation_File_C_Cntrct_Payee_Cde = confirmation_File_C_Unique_Address.newFieldInGroup("confirmation_File_C_Cntrct_Payee_Cde", "C-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        confirmation_File_C_Usage_Type = confirmation_File_C_Unique_Address.newFieldInGroup("confirmation_File_C_Usage_Type", "C-USAGE-TYPE", FieldType.STRING, 
            2);
        confirmation_File_C_Addr_Actvty_Cde = confirmation_File_C_Unique_Address.newFieldInGroup("confirmation_File_C_Addr_Actvty_Cde", "C-ADDR-ACTVTY-CDE", 
            FieldType.STRING, 2);
        confirmation_File_C_Actvty_Cde = confirmation_File.newFieldInGroup("confirmation_File_C_Actvty_Cde", "C-ACTVTY-CDE", FieldType.STRING, 1);

        confirmation_File_C_Original_Data = confirmation_File.newGroupInGroup("confirmation_File_C_Original_Data", "C-ORIGINAL-DATA");
        confirmation_File_C_Original_Chnge_Dte = confirmation_File_C_Original_Data.newFieldInGroup("confirmation_File_C_Original_Chnge_Dte", "C-ORIGINAL-CHNGE-DTE", 
            FieldType.NUMERIC, 8);

        confirmation_File__R_Field_2 = confirmation_File_C_Original_Data.newGroupInGroup("confirmation_File__R_Field_2", "REDEFINE", confirmation_File_C_Original_Chnge_Dte);
        confirmation_File_C_Orig_Chnge_Dte_A = confirmation_File__R_Field_2.newFieldInGroup("confirmation_File_C_Orig_Chnge_Dte_A", "C-ORIG-CHNGE-DTE-A", 
            FieldType.STRING, 8);
        confirmation_File_C_Original_Chnge_Tme = confirmation_File_C_Original_Data.newFieldInGroup("confirmation_File_C_Original_Chnge_Tme", "C-ORIGINAL-CHNGE-TME", 
            FieldType.NUMERIC, 7);

        confirmation_File__R_Field_3 = confirmation_File_C_Original_Data.newGroupInGroup("confirmation_File__R_Field_3", "REDEFINE", confirmation_File_C_Original_Chnge_Tme);
        confirmation_File_C_Original_Chnge_Tme_A = confirmation_File__R_Field_3.newFieldInGroup("confirmation_File_C_Original_Chnge_Tme_A", "C-ORIGINAL-CHNGE-TME-A", 
            FieldType.STRING, 7);
        confirmation_File_C_Orig_Chnge_Batch_No = confirmation_File_C_Original_Data.newFieldInGroup("confirmation_File_C_Orig_Chnge_Batch_No", "C-ORIG-CHNGE-BATCH-NO", 
            FieldType.NUMERIC, 4);
        confirmation_File_C_Addrss_Eff_Dte = confirmation_File.newFieldInGroup("confirmation_File_C_Addrss_Eff_Dte", "C-ADDRSS-EFF-DTE", FieldType.NUMERIC, 
            8);
        confirmation_File_C_Verify_Ind = confirmation_File.newFieldInGroup("confirmation_File_C_Verify_Ind", "C-VERIFY-IND", FieldType.STRING, 1);
        confirmation_File_C_Confirmation_Ind = confirmation_File.newFieldInGroup("confirmation_File_C_Confirmation_Ind", "C-CONFIRMATION-IND", FieldType.STRING, 
            1);
        confirmation_File_C_Name_Key = confirmation_File.newFieldInGroup("confirmation_File_C_Name_Key", "C-NAME-KEY", FieldType.STRING, 9);
        confirmation_File_C_Mit_Wpid = confirmation_File.newFieldInGroup("confirmation_File_C_Mit_Wpid", "C-MIT-WPID", FieldType.STRING, 6);
        confirmation_File_C_Mit_Unit_Cde = confirmation_File.newFieldInGroup("confirmation_File_C_Mit_Unit_Cde", "C-MIT-UNIT-CDE", FieldType.STRING, 8);
        confirmation_File_C_Mit_Log_Date_Time = confirmation_File.newFieldInGroup("confirmation_File_C_Mit_Log_Date_Time", "C-MIT-LOG-DATE-TIME", FieldType.STRING, 
            15);
        confirmation_File_C_Mit_Status_Cde = confirmation_File.newFieldInGroup("confirmation_File_C_Mit_Status_Cde", "C-MIT-STATUS-CDE", FieldType.STRING, 
            4);
        confirmation_File_C_Contract_2 = confirmation_File.newFieldInGroup("confirmation_File_C_Contract_2", "C-CONTRACT-2", FieldType.STRING, 10);
        confirmation_File_Filler = confirmation_File.newFieldInGroup("confirmation_File_Filler", "FILLER", FieldType.STRING, 20);

        pnd_Work_Letter_Image_File = localVariables.newGroupInRecord("pnd_Work_Letter_Image_File", "#WORK-LETTER-IMAGE-FILE");
        pnd_Work_Letter_Image_File_Pnd_O_Pin_No = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Pin_No", "#O-PIN-NO", FieldType.NUMERIC, 
            12);
        pnd_Work_Letter_Image_File_Pnd_O_Contract_No = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Contract_No", "#O-CONTRACT-NO", 
            FieldType.STRING, 10);
        pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd", "#O-PAYEE-CD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd", 
            "#O-ADDR-ACTVTY-CD", FieldType.STRING, 2);
        pnd_Work_Letter_Image_File_Pnd_O_Actvty_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Actvty_Cd", "#O-ACTVTY-CD", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_Usage = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Usage", "#O-USAGE", FieldType.STRING, 
            2);
        pnd_Work_Letter_Image_File_Pnd_O_Change_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Change_Dt", "#O-CHANGE-DT", 
            FieldType.NUMERIC, 8);
        pnd_Work_Letter_Image_File_Pnd_O_Change_Tm = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Change_Tm", "#O-CHANGE-TM", 
            FieldType.NUMERIC, 7);
        pnd_Work_Letter_Image_File_Pnd_O_Effective_Date = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Effective_Date", 
            "#O-EFFECTIVE-DATE", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_4 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_4", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_Effective_Date);
        pnd_Work_Letter_Image_File_Pnd_O_Effective_Date_A = pnd_Work_Letter_Image_File__R_Field_4.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Effective_Date_A", 
            "#O-EFFECTIVE-DATE-A", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_Verify_Ind = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Verify_Ind", "#O-VERIFY-IND", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_M_Addr_Type = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Addr_Type", "#O-M-ADDR-TYPE", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_M_Name = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Name", "#O-M-NAME", FieldType.STRING, 
            35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_1 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_1", "#O-M-ADDRESS-1", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_2 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_2", "#O-M-ADDRESS-2", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_3 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_3", "#O-M-ADDRESS-3", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_4 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_4", "#O-M-ADDRESS-4", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_5 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_5", "#O-M-ADDRESS-5", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data", 
            "#O-M-POSTAL-DATA", FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Lob = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Lob", "#O-M-LOB", FieldType.STRING, 
            1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Contract_Type_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Contract_Type_Cd", 
            "#O-C-CONTRACT-TYPE-CD", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Type = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Type", "#O-C-ADDR-TYPE", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd", 
            "#O-C-ADDR-FORMAT-CD", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt", 
            "#O-C-ADDR-LAST-CH-DT", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_5 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_5", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt_A = pnd_Work_Letter_Image_File__R_Field_5.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt_A", 
            "#O-C-ADDR-LAST-CH-DT-A", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm", 
            "#O-C-ADDR-LAST-CH-TM", FieldType.NUMERIC, 7);

        pnd_Work_Letter_Image_File__R_Field_6 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_6", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm_A = pnd_Work_Letter_Image_File__R_Field_6.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm_A", 
            "#O-C-ADDR-LAST-CH-TM-A", FieldType.STRING, 7);
        pnd_Work_Letter_Image_File_Pnd_O_C_Name = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Name", "#O-C-NAME", FieldType.STRING, 
            35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_1 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_1", "#O-C-ADDRESS-1", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_2 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_2", "#O-C-ADDRESS-2", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_3 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_3", "#O-C-ADDRESS-3", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_4 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_4", "#O-C-ADDRESS-4", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_5 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_5", "#O-C-ADDRESS-5", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data", 
            "#O-C-POSTAL-DATA", FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind", 
            "#O-C-ANNUAL-VAC-CYCLE-IND", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No", "#O-C-PH-BANK-NO", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd", 
            "#O-C-CHECK-SAVING-CD", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No", 
            "#O-C-ABA-ACCOUNT-NO", FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt", 
            "#O-C-PENDING-ADDR-CH-DT", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_7 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_7", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt);
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt_A = pnd_Work_Letter_Image_File__R_Field_7.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt_A", 
            "#O-C-PENDING-ADDR-CH-DT-A", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt", 
            "#O-C-PENDING-ADDR-RS-DT", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_8 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_8", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt);
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt_A = pnd_Work_Letter_Image_File__R_Field_8.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt_A", 
            "#O-C-PENDING-ADDR-RS-DT-A", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt", 
            "#O-C-PERM-ADDRESS-CH-DT", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_9 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_9", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt);
        pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt_A = pnd_Work_Letter_Image_File__R_Field_9.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt_A", 
            "#O-C-PERM-ADDRESS-CH-DT-A", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_C_Section = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Section", "#O-C-SECTION", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid", "#O-C-MIT-WPID", 
            FieldType.STRING, 6);
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde", 
            "#O-C-MIT-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time", 
            "#O-C-MIT-LOG-DATE-TIME", FieldType.STRING, 15);
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Status_Cde = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Status_Cde", 
            "#O-C-MIT-STATUS-CDE", FieldType.STRING, 4);
        pnd_Work_Letter_Image_File_Pnd_Mit_Error = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Mit_Error", "#MIT-ERROR", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_Compress_Address = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Compress_Address", 
            "#COMPRESS-ADDRESS", FieldType.STRING, 30);
        pnd_Work_Letter_Image_File_Pnd_O_M_Override = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Override", "#O-M-OVERRIDE", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_1 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_1", "#O-R-ADDRESS-1", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_2 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_2", "#O-R-ADDRESS-2", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_3 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_3", "#O-R-ADDRESS-3", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_4 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_4", "#O-R-ADDRESS-4", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_5 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_5", "#O-R-ADDRESS-5", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data", 
            "#O-R-POSTAL-DATA", FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_Contract2 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Contract2", "#O-CONTRACT2", 
            FieldType.STRING, 10);
        pnd_Work_Letter_Image_File_Pnd_O_Filler = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Filler", "#O-FILLER", FieldType.STRING, 
            55);

        pnd_Work_Letter_Image_File__R_Field_10 = localVariables.newGroupInRecord("pnd_Work_Letter_Image_File__R_Field_10", "REDEFINE", pnd_Work_Letter_Image_File);
        pnd_Work_Letter_Image_File_Pnd_Work = pnd_Work_Letter_Image_File__R_Field_10.newFieldArrayInGroup("pnd_Work_Letter_Image_File_Pnd_Work", "#WORK", 
            FieldType.STRING, 1, new DbsArrayController(1, 1000));
        pnd_Addr_Current = localVariables.newFieldInRecord("pnd_Addr_Current", "#ADDR-CURRENT", FieldType.BOOLEAN, 1);
        pnd_Addr_New = localVariables.newFieldInRecord("pnd_Addr_New", "#ADDR-NEW", FieldType.BOOLEAN, 1);
        pnd_Addr_Mailto = localVariables.newFieldInRecord("pnd_Addr_Mailto", "#ADDR-MAILTO", FieldType.BOOLEAN, 1);
        pnd_Comp_Date_A8 = localVariables.newFieldInRecord("pnd_Comp_Date_A8", "#COMP-DATE-A8", FieldType.STRING, 8);

        pnd_Comp_Date_A8__R_Field_11 = localVariables.newGroupInRecord("pnd_Comp_Date_A8__R_Field_11", "REDEFINE", pnd_Comp_Date_A8);
        pnd_Comp_Date_A8_Pnd_Comp_Date = pnd_Comp_Date_A8__R_Field_11.newFieldInGroup("pnd_Comp_Date_A8_Pnd_Comp_Date", "#COMP-DATE", FieldType.NUMERIC, 
            8);
        pnd_Datx = localVariables.newFieldInRecord("pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Err_Addr_Type = localVariables.newFieldInRecord("pnd_Err_Addr_Type", "#ERR-ADDR-TYPE", FieldType.STRING, 9);
        pnd_Err_Date = localVariables.newFieldInRecord("pnd_Err_Date", "#ERR-DATE", FieldType.STRING, 10);
        pnd_Err_Letter = localVariables.newFieldInRecord("pnd_Err_Letter", "#ERR-LETTER", FieldType.BOOLEAN, 1);
        pnd_Err_Msg = localVariables.newFieldInRecord("pnd_Err_Msg", "#ERR-MSG", FieldType.STRING, 65);
        pnd_Err_Prfx = localVariables.newFieldInRecord("pnd_Err_Prfx", "#ERR-PRFX", FieldType.STRING, 10);
        pnd_Err_Time = localVariables.newFieldInRecord("pnd_Err_Time", "#ERR-TIME", FieldType.STRING, 10);
        pnd_Err_Tran = localVariables.newFieldInRecord("pnd_Err_Tran", "#ERR-TRAN", FieldType.STRING, 3);

        pnd_Format_Addr = localVariables.newGroupInRecord("pnd_Format_Addr", "#FORMAT-ADDR");
        pnd_Format_Addr_Pnd_C = pnd_Format_Addr.newFieldInGroup("pnd_Format_Addr_Pnd_C", "#C", FieldType.NUMERIC, 2);
        pnd_Format_Addr_Pnd_City_State_Zip = pnd_Format_Addr.newFieldInGroup("pnd_Format_Addr_Pnd_City_State_Zip", "#CITY-STATE-ZIP", FieldType.STRING, 
            35);
        pnd_Format_Addr_Pnd_In_Addr_Type = pnd_Format_Addr.newFieldInGroup("pnd_Format_Addr_Pnd_In_Addr_Type", "#IN-ADDR-TYPE", FieldType.STRING, 1);
        pnd_Format_Addr_Pnd_In_Addr1 = pnd_Format_Addr.newFieldInGroup("pnd_Format_Addr_Pnd_In_Addr1", "#IN-ADDR1", FieldType.STRING, 35);
        pnd_Format_Addr_Pnd_In_Addr2 = pnd_Format_Addr.newFieldInGroup("pnd_Format_Addr_Pnd_In_Addr2", "#IN-ADDR2", FieldType.STRING, 35);
        pnd_Format_Addr_Pnd_In_Addr3 = pnd_Format_Addr.newFieldInGroup("pnd_Format_Addr_Pnd_In_Addr3", "#IN-ADDR3", FieldType.STRING, 35);
        pnd_Format_Addr_Pnd_In_Addr4 = pnd_Format_Addr.newFieldInGroup("pnd_Format_Addr_Pnd_In_Addr4", "#IN-ADDR4", FieldType.STRING, 35);
        pnd_Format_Addr_Pnd_In_City = pnd_Format_Addr.newFieldInGroup("pnd_Format_Addr_Pnd_In_City", "#IN-CITY", FieldType.STRING, 35);
        pnd_Format_Addr_Pnd_In_St_Prov = pnd_Format_Addr.newFieldInGroup("pnd_Format_Addr_Pnd_In_St_Prov", "#IN-ST-PROV", FieldType.STRING, 35);
        pnd_Format_Addr_Pnd_In_Zip = pnd_Format_Addr.newFieldInGroup("pnd_Format_Addr_Pnd_In_Zip", "#IN-ZIP", FieldType.STRING, 10);

        pnd_Format_Addr__R_Field_12 = pnd_Format_Addr.newGroupInGroup("pnd_Format_Addr__R_Field_12", "REDEFINE", pnd_Format_Addr_Pnd_In_Zip);
        pnd_Format_Addr_Pnd_In_Zip5 = pnd_Format_Addr__R_Field_12.newFieldInGroup("pnd_Format_Addr_Pnd_In_Zip5", "#IN-ZIP5", FieldType.STRING, 5);
        pnd_Format_Addr_Pnd_In_Country = pnd_Format_Addr.newFieldInGroup("pnd_Format_Addr_Pnd_In_Country", "#IN-COUNTRY", FieldType.STRING, 35);
        pnd_Format_Addr_Pnd_Out_Addr = pnd_Format_Addr.newFieldArrayInGroup("pnd_Format_Addr_Pnd_Out_Addr", "#OUT-ADDR", FieldType.STRING, 35, new DbsArrayController(1, 
            5));
        pnd_Format_Addr_Pnd_S = pnd_Format_Addr.newFieldInGroup("pnd_Format_Addr_Pnd_S", "#S", FieldType.NUMERIC, 2);

        pnd_Format_Name = localVariables.newGroupInRecord("pnd_Format_Name", "#FORMAT-NAME");
        pnd_Format_Name_Pnd_In_Lname = pnd_Format_Name.newFieldInGroup("pnd_Format_Name_Pnd_In_Lname", "#IN-LNAME", FieldType.STRING, 35);
        pnd_Format_Name_Pnd_In_Fname = pnd_Format_Name.newFieldInGroup("pnd_Format_Name_Pnd_In_Fname", "#IN-FNAME", FieldType.STRING, 30);
        pnd_Format_Name_Pnd_In_Mname = pnd_Format_Name.newFieldInGroup("pnd_Format_Name_Pnd_In_Mname", "#IN-MNAME", FieldType.STRING, 30);

        pnd_Format_Name__R_Field_13 = pnd_Format_Name.newGroupInGroup("pnd_Format_Name__R_Field_13", "REDEFINE", pnd_Format_Name_Pnd_In_Mname);
        pnd_Format_Name_Pnd_In_Mname_1 = pnd_Format_Name__R_Field_13.newFieldInGroup("pnd_Format_Name_Pnd_In_Mname_1", "#IN-MNAME-1", FieldType.STRING, 
            1);
        pnd_Format_Name_Pnd_In_Prfx = pnd_Format_Name.newFieldInGroup("pnd_Format_Name_Pnd_In_Prfx", "#IN-PRFX", FieldType.STRING, 8);
        pnd_Format_Name_Pnd_In_Sffx = pnd_Format_Name.newFieldInGroup("pnd_Format_Name_Pnd_In_Sffx", "#IN-SFFX", FieldType.STRING, 8);
        pnd_Format_Name_Pnd_Out_Pref_Name = pnd_Format_Name.newFieldInGroup("pnd_Format_Name_Pnd_Out_Pref_Name", "#OUT-PREF-NAME", FieldType.STRING, 70);

        pnd_Format_Name__R_Field_14 = pnd_Format_Name.newGroupInGroup("pnd_Format_Name__R_Field_14", "REDEFINE", pnd_Format_Name_Pnd_Out_Pref_Name);
        pnd_Format_Name_Pnd_Out_Name = pnd_Format_Name__R_Field_14.newFieldInGroup("pnd_Format_Name_Pnd_Out_Name", "#OUT-NAME", FieldType.STRING, 35);
        pnd_Format_Name_Pnd_Out_Name_36_70 = pnd_Format_Name__R_Field_14.newFieldInGroup("pnd_Format_Name_Pnd_Out_Name_36_70", "#OUT-NAME-36-70", FieldType.STRING, 
            35);

        pnd_Group_Address = localVariables.newGroupInRecord("pnd_Group_Address", "#GROUP-ADDRESS");
        pnd_Group_Address_Pnd_Save_Address_Good = pnd_Group_Address.newFieldInGroup("pnd_Group_Address_Pnd_Save_Address_Good", "#SAVE-ADDRESS-GOOD", FieldType.BOOLEAN, 
            1);
        pnd_Group_Address_Pnd_Save_Address_1 = pnd_Group_Address.newFieldInGroup("pnd_Group_Address_Pnd_Save_Address_1", "#SAVE-ADDRESS-1", FieldType.STRING, 
            35);
        pnd_Group_Address_Pnd_Save_Address_2 = pnd_Group_Address.newFieldInGroup("pnd_Group_Address_Pnd_Save_Address_2", "#SAVE-ADDRESS-2", FieldType.STRING, 
            35);
        pnd_Group_Address_Pnd_Save_Address_3 = pnd_Group_Address.newFieldInGroup("pnd_Group_Address_Pnd_Save_Address_3", "#SAVE-ADDRESS-3", FieldType.STRING, 
            35);
        pnd_Group_Address_Pnd_Save_Address_4 = pnd_Group_Address.newFieldInGroup("pnd_Group_Address_Pnd_Save_Address_4", "#SAVE-ADDRESS-4", FieldType.STRING, 
            35);
        pnd_Group_Address_Pnd_Save_Address_5 = pnd_Group_Address.newFieldInGroup("pnd_Group_Address_Pnd_Save_Address_5", "#SAVE-ADDRESS-5", FieldType.STRING, 
            35);
        pnd_Group_Address_Pnd_Save_Postal_Data = pnd_Group_Address.newFieldInGroup("pnd_Group_Address_Pnd_Save_Postal_Data", "#SAVE-POSTAL-DATA", FieldType.STRING, 
            35);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.PACKED_DECIMAL, 3);
        pnd_L_Acct_No = localVariables.newFieldInRecord("pnd_L_Acct_No", "#L-ACCT-NO", FieldType.STRING, 35);
        pnd_L_Date = localVariables.newFieldInRecord("pnd_L_Date", "#L-DATE", FieldType.DATE);
        pnd_L_Date_F = localVariables.newFieldInRecord("pnd_L_Date_F", "#L-DATE-F", FieldType.STRING, 10);
        pnd_L_Eft_No = localVariables.newFieldInRecord("pnd_L_Eft_No", "#L-EFT-NO", FieldType.STRING, 35);
        pnd_L_Format_Cd = localVariables.newFieldInRecord("pnd_L_Format_Cd", "#L-FORMAT-CD", FieldType.STRING, 1);
        pnd_L_Time = localVariables.newFieldInRecord("pnd_L_Time", "#L-TIME", FieldType.TIME);
        pnd_L_Time_A = localVariables.newFieldInRecord("pnd_L_Time_A", "#L-TIME-A", FieldType.STRING, 7);

        pnd_L_Time_A__R_Field_15 = localVariables.newGroupInRecord("pnd_L_Time_A__R_Field_15", "REDEFINE", pnd_L_Time_A);
        pnd_L_Time_A_Pnd_L_Time_N = pnd_L_Time_A__R_Field_15.newFieldInGroup("pnd_L_Time_A_Pnd_L_Time_N", "#L-TIME-N", FieldType.NUMERIC, 7);
        pnd_L_Time_F = localVariables.newFieldInRecord("pnd_L_Time_F", "#L-TIME-F", FieldType.STRING, 8);
        pnd_Previous_Bad = localVariables.newFieldInRecord("pnd_Previous_Bad", "#PREVIOUS-BAD", FieldType.BOOLEAN, 1);
        pnd_Same_Contract = localVariables.newFieldInRecord("pnd_Same_Contract", "#SAME-CONTRACT", FieldType.STRING, 10);
        pnd_Save_Pin = localVariables.newFieldInRecord("pnd_Save_Pin", "#SAVE-PIN", FieldType.STRING, 12);
        pnd_Total_Error = localVariables.newFieldInRecord("pnd_Total_Error", "#TOTAL-ERROR", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Read = localVariables.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Write = localVariables.newFieldInRecord("pnd_Total_Write", "#TOTAL-WRITE", FieldType.PACKED_DECIMAL, 10);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 3);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Addr_Current.setInitialValue(false);
        pnd_Addr_New.setInitialValue(false);
        pnd_Addr_Mailto.setInitialValue(false);
        pnd_Err_Time.setInitialValue("00:00:00:0");
        pls_Trace.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Nasb490k() throws Exception
    {
        super("Nasb490k");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 60 LS = 132 SG = OFF ZP = ON;//Natural: FORMAT ( 1 ) PS = 60 LS = 132 SG = OFF ZP = ON
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        pdaMdma361.getPnd_Mdma361().reset();                                                                                                                              //Natural: RESET #MDMA361
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 CONFIRMATION-FILE
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, confirmation_File)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF C-PH-UNIQUE-ID-NMBR
            pnd_Total_Read.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-READ
            if (condition(! (DbsUtil.maskMatches(confirmation_File_C_Original_Chnge_Tme,"9999999"))))                                                                     //Natural: IF C-ORIGINAL-CHNGE-TME NE MASK ( 9999999 )
            {
                pnd_L_Time_A.setValue(confirmation_File_C_Original_Chnge_Tme_A, MoveOption.RightJustified);                                                               //Natural: MOVE RIGHT JUSTIFIED C-ORIGINAL-CHNGE-TME-A TO #L-TIME-A
                DbsUtil.examine(new ExamineSource(pnd_L_Time_A,true), new ExamineSearch(" "), new ExamineReplace("0"));                                                   //Natural: EXAMINE FULL #L-TIME-A FOR ' ' REPLACE WITH '0'
                confirmation_File_C_Original_Chnge_Tme.setValue(pnd_L_Time_A_Pnd_L_Time_N);                                                                               //Natural: ASSIGN C-ORIGINAL-CHNGE-TME := #L-TIME-N
            }                                                                                                                                                             //Natural: END-IF
            if (condition(confirmation_File_C_Mit_Log_Date_Time.equals(" ")))                                                                                             //Natural: IF C-MIT-LOG-DATE-TIME = ' '
            {
                                                                                                                                                                          //Natural: PERFORM CALL-CWF-PROCESS
                sub_Call_Cwf_Process();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DUMP-INPUT-WORKFILE
            sub_Dump_Input_Workfile();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Work_Letter_Image_File.reset();                                                                                                                           //Natural: RESET #WORK-LETTER-IMAGE-FILE
            pnd_Addr_Mailto.reset();                                                                                                                                      //Natural: RESET #ADDR-MAILTO #ADDR-NEW #ERR-LETTER
            pnd_Addr_New.reset();
            pnd_Err_Letter.reset();
            short decideConditionsMet915 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN C-CNTRCT-NMBR NE ' ' AND ( C-USAGE-TYPE = 'CO' OR = 'CM' )
            if (condition((confirmation_File_C_Cntrct_Nmbr.notEquals(" ") && (confirmation_File_C_Usage_Type.equals("CO") || confirmation_File_C_Usage_Type.equals("CM")))))
            {
                decideConditionsMet915++;
                                                                                                                                                                          //Natural: PERFORM GET-CONFIRMATION-ADDRESS
                sub_Get_Confirmation_Address();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NOT #ERR-LETTER AND ( C-USAGE-TYPE = 'RS' OR ( C-USAGE-TYPE = 'CO' AND C-ADDR-ACTVTY-CDE = 'AP' OR = 'IP' ) )
            if (condition((! (pnd_Err_Letter.getBoolean()) && (confirmation_File_C_Usage_Type.equals("RS") || (confirmation_File_C_Usage_Type.equals("CO") 
                && (confirmation_File_C_Addr_Actvty_Cde.equals("AP") || confirmation_File_C_Addr_Actvty_Cde.equals("IP")))))))
            {
                decideConditionsMet915++;
                //*  PINE
                if (condition(pdaMdma361.getPnd_Mdma361_Pnd_I_Pin_N12().equals(confirmation_File_C_Ph_Unique_Id_Nmbr.val()) && pnd_Group_Address_Pnd_Save_Address_Good.getBoolean())) //Natural: IF #MDMA361.#I-PIN-N12 = VAL ( C-PH-UNIQUE-ID-NMBR ) AND #SAVE-ADDRESS-GOOD
                {
                    pnd_Work_Letter_Image_File_Pnd_O_R_Address_1.setValue(pnd_Group_Address_Pnd_Save_Address_1);                                                          //Natural: ASSIGN #O-R-ADDRESS-1 := #SAVE-ADDRESS-1
                    pnd_Work_Letter_Image_File_Pnd_O_R_Address_2.setValue(pnd_Group_Address_Pnd_Save_Address_2);                                                          //Natural: ASSIGN #O-R-ADDRESS-2 := #SAVE-ADDRESS-2
                    pnd_Work_Letter_Image_File_Pnd_O_R_Address_3.setValue(pnd_Group_Address_Pnd_Save_Address_3);                                                          //Natural: ASSIGN #O-R-ADDRESS-3 := #SAVE-ADDRESS-3
                    pnd_Work_Letter_Image_File_Pnd_O_R_Address_4.setValue(pnd_Group_Address_Pnd_Save_Address_4);                                                          //Natural: ASSIGN #O-R-ADDRESS-4 := #SAVE-ADDRESS-4
                    pnd_Work_Letter_Image_File_Pnd_O_R_Address_5.setValue(pnd_Group_Address_Pnd_Save_Address_5);                                                          //Natural: ASSIGN #O-R-ADDRESS-5 := #SAVE-ADDRESS-5
                    pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data.setValue(pnd_Group_Address_Pnd_Save_Postal_Data);                                                      //Natural: ASSIGN #O-R-POSTAL-DATA := #SAVE-POSTAL-DATA
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM GET-RESIDENTIAL-ADDRESS
                    sub_Get_Residential_Address();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NOT #ERR-LETTER AND C-CNTRCT-NMBR = ' '
            if (condition(! ((pnd_Err_Letter.getBoolean()) && confirmation_File_C_Cntrct_Nmbr.equals(" "))))
            {
                decideConditionsMet915++;
                                                                                                                                                                          //Natural: PERFORM MAIL-TO-BASE-ADDRESS
                sub_Mail_To_Base_Address();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NOT #ERR-LETTER AND C-CNTRCT-NMBR NE ' '
            if (condition(! ((pnd_Err_Letter.getBoolean()) && confirmation_File_C_Cntrct_Nmbr.notEquals(" "))))
            {
                decideConditionsMet915++;
                if (condition((confirmation_File_C_Usage_Type.equals("CO") && (confirmation_File_C_Addr_Actvty_Cde.equals("AP") || confirmation_File_C_Addr_Actvty_Cde.equals("IP"))))) //Natural: IF C-USAGE-TYPE = 'CO' AND C-ADDR-ACTVTY-CDE = 'AP' OR = 'IP'
                {
                                                                                                                                                                          //Natural: PERFORM MAIL-TO-PREVIOUS-ADDRESS
                    sub_Mail_To_Previous_Address();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM MAIL-TO-CURRENT-ADDRESS
                    sub_Mail_To_Current_Address();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet915 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, ReportOption.NOTITLE,"PIN",confirmation_File_C_Ph_Unique_Id_Nmbr,pnd_Addr_Mailto,pnd_Addr_New);                                     //Natural: WRITE 'PIN' C-PH-UNIQUE-ID-NMBR #ADDR-MAILTO #ADDR-NEW
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Addr_Mailto.getBoolean() && pnd_Addr_New.getBoolean()))                                                                                     //Natural: IF #ADDR-MAILTO AND #ADDR-NEW
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
                sub_Write_Work_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL INPUT RECORDS READ           :",pnd_Total_Read, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"TOTAL ERROR                        :",pnd_Total_Error,  //Natural: WRITE ( 1 ) NOTITLE / 'TOTAL INPUT RECORDS READ           :' #TOTAL-READ / 'TOTAL ERROR                        :' #TOTAL-ERROR / 'TOTAL RECORDS WRITTEN IN WORK FILE :' #TOTAL-WRITE
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"TOTAL RECORDS WRITTEN IN WORK FILE :",pnd_Total_Write, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-CWF-PROCESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-ADDR-FORMAT-CODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DUMP-INPUT-WORKFILE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONFIRMATION-ADDRESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RESIDENTIAL-ADDRESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MAIL-TO-BASE-ADDRESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MAIL-TO-CURRENT-ADDRESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MAIL-TO-PREVIOUS-ADDRESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDM-ADDRESS-ACTIVE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDM-ADDRESS-FUTURE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDM-ADDRESS-TEMPORARY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ERROR
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-ADDRESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-NAME
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //* ***********************************************************************
        //*  KCD
        //*  #O-CHANGE-BATCH-NO :=
        //*  #O-EFFECTIVE-DATE  :=
        //*  WHEN #HOLD-TNT-CREF NE ' '
        //*    #O-CONTRACT-NO := #HOLD-TNT-CREF
        //*  WHEN #O-R-FUTR-START-DT NE MASK (YYYYMMDD)
        //*    #O-R-FUTR-START-DT-A := '00000000'
    }
    private void sub_Call_Cwf_Process() throws Exception                                                                                                                  //Natural: CALL-CWF-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE CALL-CWF-PROCESS");                                                                                    //Natural: WRITE 'SUBROUTINE CALL-CWF-PROCESS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  PINE
        pdaMdma190.getMdma190().reset();                                                                                                                                  //Natural: RESET MDMA190
        pdaMdma190.getMdma190_Pnd_Et_Ind().setValue("Y");                                                                                                                 //Natural: ASSIGN MDMA190.#ET-IND := 'Y'
        pdaMdma190.getMdma190_Pnd_Wpid_System().setValue("NAS");                                                                                                          //Natural: ASSIGN MDMA190.#WPID-SYSTEM := 'NAS'
        pdaMdma190.getMdma190_Pnd_Gda_User_Id().setValue("BATCHNAS");                                                                                                     //Natural: ASSIGN MDMA190.#GDA-USER-ID := 'BATCHNAS'
        pdaMdma190.getMdma190_Pnd_Gda_Slo_Exists().setValue("N");                                                                                                         //Natural: ASSIGN MDMA190.#GDA-SLO-EXISTS := 'N'
        pdaMdma190.getMdma190_Pnd_Wpid_Pin().compute(new ComputeParameters(false, pdaMdma190.getMdma190_Pnd_Wpid_Pin()), confirmation_File_C_Ph_Unique_Id_Nmbr.val());    //Natural: ASSIGN MDMA190.#WPID-PIN := VAL ( C-PH-UNIQUE-ID-NMBR )
        pdaMdma190.getMdma190_Pnd_Wpid_Status().setValue("8000");                                                                                                         //Natural: ASSIGN MDMA190.#WPID-STATUS := '8000'
        pdaMdma190.getMdma190_Pnd_Wpid_Unit().setValue("CDMSA D");                                                                                                        //Natural: ASSIGN MDMA190.#WPID-UNIT := 'CDMSA D'
        pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit().setValue(" ");                                                                                                         //Natural: ASSIGN MDMA190.#WPID-NEXT-UNIT := ' '
        pdaMdma190.getMdma190_Pnd_Wpid().setValue("TA HV");                                                                                                               //Natural: ASSIGN MDMA190.#WPID := 'TA HV'
        //*  PINE
        DbsUtil.callnat(Mdmn190.class , getCurrentProcessState(), pdaMdma190.getMdma190());                                                                               //Natural: CALLNAT 'MDMN190' USING MDMA190
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma190.getMdma190_Pnd_Error_Message().notEquals(" ")))                                                                                          //Natural: IF MDMA190.#ERROR-MESSAGE NE ' '
        {
            pnd_Err_Msg.setValue(pdaMdma190.getMdma190_Pnd_Error_Message());                                                                                              //Natural: ASSIGN #ERR-MSG := MDMA190.#ERROR-MESSAGE
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            confirmation_File_C_Mit_Wpid.setValue("TA HV");                                                                                                               //Natural: ASSIGN C-MIT-WPID := 'TA HV'
            confirmation_File_C_Mit_Unit_Cde.setValue("CDMSA D");                                                                                                         //Natural: ASSIGN C-MIT-UNIT-CDE := 'CDMSA D'
            confirmation_File_C_Mit_Log_Date_Time.setValue(pdaMdma190.getMdma190_Pnd_Slo_Rqst_Log_Dte_Tme());                                                             //Natural: ASSIGN C-MIT-LOG-DATE-TIME := MDMA190.#SLO-RQST-LOG-DTE-TME
            confirmation_File_C_Mit_Status_Cde.setValue("8000");                                                                                                          //Natural: ASSIGN C-MIT-STATUS-CDE := '8000'
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-CWF-PROCESS
    }
    private void sub_Determine_Addr_Format_Code() throws Exception                                                                                                        //Natural: DETERMINE-ADDR-FORMAT-CODE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE DETERMINE-ADDR-FORMAT-CODE");                                                                          //Natural: WRITE 'SUBROUTINE DETERMINE-ADDR-FORMAT-CODE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        pnd_L_Format_Cd.reset();                                                                                                                                          //Natural: RESET #L-FORMAT-CD
        short decideConditionsMet1046 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF C-CNTRCT-TYPE;//Natural: VALUE 'D', 'B', 'A'
        if (condition((confirmation_File_C_Cntrct_Type.equals("D") || confirmation_File_C_Cntrct_Type.equals("B") || confirmation_File_C_Cntrct_Type.equals("A"))))
        {
            decideConditionsMet1046++;
            //*  DA/SELF-BILL/CORRESPONDENCE
            if (condition(confirmation_File_C_Usage_Type.equals("SB")))                                                                                                   //Natural: IF C-USAGE-TYPE = 'SB'
            {
                pnd_L_Format_Cd.setValue("I");                                                                                                                            //Natural: ASSIGN #L-FORMAT-CD := 'I'
                //*  DA/RBA/CORRESPONDENCE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_L_Format_Cd.setValue("H");                                                                                                                            //Natural: ASSIGN #L-FORMAT-CD := 'H'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((confirmation_File_C_Cntrct_Type.equals("I"))))
        {
            decideConditionsMet1046++;
            //*  IA/CHECK/CORRESPONDENCE
            short decideConditionsMet1057 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN C-USAGE-TYPE = 'CM'
            if (condition(confirmation_File_C_Usage_Type.equals("CM")))
            {
                decideConditionsMet1057++;
                pnd_L_Format_Cd.setValue("E");                                                                                                                            //Natural: ASSIGN #L-FORMAT-CD := 'E'
                //*  IA/CHECK/BANK/EFT
                if (condition(pnd_L_Acct_No.notEquals(" ") || pnd_L_Eft_No.notEquals(" ")))                                                                               //Natural: IF #L-ACCT-NO NE ' ' OR #L-EFT-NO NE ' '
                {
                    pnd_L_Format_Cd.setValue("C");                                                                                                                        //Natural: ASSIGN #L-FORMAT-CD := 'C'
                    //*  IA/CHECK/BANK/CONSERVATOR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_L_Format_Cd.setValue("D");                                                                                                                        //Natural: ASSIGN #L-FORMAT-CD := 'D'
                    //*  IA/CORR/CORRESPONDENCE
                    //*  IA/CHECK/CORRESP (DEFAULT)
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN C-USAGE-TYPE = 'CO' OR = 'RS'
            else if (condition(confirmation_File_C_Usage_Type.equals("CO") || confirmation_File_C_Usage_Type.equals("RS")))
            {
                decideConditionsMet1057++;
                pnd_L_Format_Cd.setValue("G");                                                                                                                            //Natural: ASSIGN #L-FORMAT-CD := 'G'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_L_Format_Cd.setValue("F");                                                                                                                            //Natural: ASSIGN #L-FORMAT-CD := 'F'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'N'
        else if (condition((confirmation_File_C_Cntrct_Type.equals("N"))))
        {
            decideConditionsMet1046++;
            //*  INS/INSURED/CORRESPONDENCE
            //*  INS/ANNUITANT/CORRESP PA
            //*  INS/INSURED/CORRESPONDENCE
            //*  INS/OWNER/CORRESPONDENCE
            short decideConditionsMet1079 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN C-CNTRCT-PAYEE-CDE = 00
            if (condition(confirmation_File_C_Cntrct_Payee_Cde.equals(0)))
            {
                decideConditionsMet1079++;
                pnd_L_Format_Cd.setValue("A");                                                                                                                            //Natural: ASSIGN #L-FORMAT-CD := 'A'
            }                                                                                                                                                             //Natural: WHEN C-CNTRCT-PAYEE-CDE = 10 AND #MDMA321.#O-LINE-OF-BUSINESS = 'N'
            else if (condition(confirmation_File_C_Cntrct_Payee_Cde.equals(10) && pdaMdma321.getPnd_Mdma321_Pnd_O_Line_Of_Business().equals("N")))
            {
                decideConditionsMet1079++;
                pnd_L_Format_Cd.setValue("A");                                                                                                                            //Natural: ASSIGN #L-FORMAT-CD := 'A'
            }                                                                                                                                                             //Natural: WHEN C-CNTRCT-PAYEE-CDE = 10 THRU 11 AND #MDMA321.#O-LINE-OF-BUSINESS = 'N'
            else if (condition(confirmation_File_C_Cntrct_Payee_Cde.greaterOrEqual(10) && confirmation_File_C_Cntrct_Payee_Cde.lessOrEqual(11) && pdaMdma321.getPnd_Mdma321_Pnd_O_Line_Of_Business().equals("N")))
            {
                decideConditionsMet1079++;
                pnd_L_Format_Cd.setValue("A");                                                                                                                            //Natural: ASSIGN #L-FORMAT-CD := 'A'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_L_Format_Cd.setValue("B");                                                                                                                            //Natural: ASSIGN #L-FORMAT-CD := 'B'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'M', 'F'
        else if (condition((confirmation_File_C_Cntrct_Type.equals("M") || confirmation_File_C_Cntrct_Type.equals("F"))))
        {
            decideConditionsMet1046++;
            //*  INS/MF/RBA OWNER/CORR
            if (condition(confirmation_File_C_Usage_Type.equals("CO") || confirmation_File_C_Usage_Type.equals("RS")))                                                    //Natural: IF C-USAGE-TYPE = 'CO' OR = 'RS'
            {
                pnd_L_Format_Cd.setValue("B");                                                                                                                            //Natural: ASSIGN #L-FORMAT-CD := 'B'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  DETERMINE-ADDR-FORMAT-CODE
    }
    private void sub_Dump_Input_Workfile() throws Exception                                                                                                               //Natural: DUMP-INPUT-WORKFILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(confirmation_File_C_Usage_Type.equals("RS")))                                                                                                       //Natural: IF C-USAGE-TYPE = 'RS'
        {
            confirmation_File_C_Cntrct_Type.reset();                                                                                                                      //Natural: RESET C-CNTRCT-TYPE C-CNTRCT-NMBR
            confirmation_File_C_Cntrct_Nmbr.reset();
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(0, ReportOption.NOTITLE,"PIN",                                                                                                               //Natural: DISPLAY ( 0 ) NOTITLE 'PIN' C-PH-UNIQUE-ID-NMBR 'L' C-CNTRCT-TYPE 'Contract' C-CNTRCT-NMBR 'S' SECTION 'Py' C-CNTRCT-PAYEE-CDE ( EM = 99 ) 'Us' C-USAGE-TYPE 'AA' C-ADDR-ACTVTY-CDE 'Ac' C-ACTVTY-CDE 'OrigDate' C-ORIGINAL-CHNGE-DTE 'OrigTime' C-ORIGINAL-CHNGE-TME 'EffDate' C-ADDRSS-EFF-DTE 'V' C-VERIFY-IND 'C' C-CONFIRMATION-IND 'Name' C-NAME-KEY 'WPID' C-MIT-WPID 'Unit' C-MIT-UNIT-CDE 'Rldt' C-MIT-LOG-DATE-TIME 'St' C-MIT-STATUS-CDE
        		confirmation_File_C_Ph_Unique_Id_Nmbr,"L",
        		confirmation_File_C_Cntrct_Type,"Contract",
        		confirmation_File_C_Cntrct_Nmbr,"S",
        		confirmation_File_Section,"Py",
        		confirmation_File_C_Cntrct_Payee_Cde, new ReportEditMask ("99"),"Us",
        		confirmation_File_C_Usage_Type,"AA",
        		confirmation_File_C_Addr_Actvty_Cde,"Ac",
        		confirmation_File_C_Actvty_Cde,"OrigDate",
        		confirmation_File_C_Original_Chnge_Dte,"OrigTime",
        		confirmation_File_C_Original_Chnge_Tme,"EffDate",
        		confirmation_File_C_Addrss_Eff_Dte,"V",
        		confirmation_File_C_Verify_Ind,"C",
        		confirmation_File_C_Confirmation_Ind,"Name",
        		confirmation_File_C_Name_Key,"WPID",
        		confirmation_File_C_Mit_Wpid,"Unit",
        		confirmation_File_C_Mit_Unit_Cde,"Rldt",
        		confirmation_File_C_Mit_Log_Date_Time,"St",
        		confirmation_File_C_Mit_Status_Cde);
        if (Global.isEscape()) return;
        //*  DUMP-INPUT-WORKFILE
    }
    private void sub_Get_Confirmation_Address() throws Exception                                                                                                          //Natural: GET-CONFIRMATION-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE GET-CONFIRMATION-ADDRESS");                                                                            //Natural: WRITE 'SUBROUTINE GET-CONFIRMATION-ADDRESS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        pdaMdma321.getPnd_Mdma321().reset();                                                                                                                              //Natural: RESET #MDMA321 #ADDR-CURRENT
        pnd_Addr_Current.reset();
        pdaMdma321.getPnd_Mdma321_Pnd_I_Contract_Number().setValue(confirmation_File_C_Cntrct_Nmbr);                                                                      //Natural: ASSIGN #MDMA321.#I-CONTRACT-NUMBER := C-CNTRCT-NMBR
        pdaMdma321.getPnd_Mdma321_Pnd_I_Payee_Code().setValue(confirmation_File_C_Cntrct_Payee_Cde);                                                                      //Natural: ASSIGN #MDMA321.#I-PAYEE-CODE := C-CNTRCT-PAYEE-CDE
        pdaMdma321.getPnd_Mdma321_Pnd_I_Cntrct_Role_Fltr().setValue("ALL");                                                                                               //Natural: ASSIGN #MDMA321.#I-CNTRCT-ROLE-FLTR := 'ALL'
        if (condition(confirmation_File_C_Cntrct_Type.equals("D")))                                                                                                       //Natural: IF C-CNTRCT-TYPE = 'D'
        {
            pdaMdma321.getPnd_Mdma321_Pnd_I_Payee_Code_A2().setValue("01");                                                                                               //Natural: ASSIGN #MDMA321.#I-PAYEE-CODE-A2 := '01'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(confirmation_File_C_Usage_Type.equals("CO")))                                                                                                       //Natural: IF C-USAGE-TYPE = 'CO'
        {
            pnd_Addr_Current.setValue(true);                                                                                                                              //Natural: ASSIGN #ADDR-CURRENT := TRUE
            pdaMdma321.getPnd_Mdma321_Pnd_I_Address_Usage().setValue("O");                                                                                                //Natural: ASSIGN #MDMA321.#I-ADDRESS-USAGE := 'O'
            //*  CM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaMdma321.getPnd_Mdma321_Pnd_I_Address_Usage().setValue("K");                                                                                                //Natural: ASSIGN #MDMA321.#I-ADDRESS-USAGE := 'K'
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Mdmn320a.class , getCurrentProcessState(), pdaMdma321.getPnd_Mdma321());                                                                          //Natural: CALLNAT 'MDMN320A' #MDMA321
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA321.#O-RETURN-CODE NE '0000'
        {
            pnd_Err_Msg.setValue(DbsUtil.compress("N321:", pdaMdma321.getPnd_Mdma321_Pnd_O_Return_Code(), pdaMdma321.getPnd_Mdma321_Pnd_O_Return_Text()));                //Natural: COMPRESS 'N321:' #MDMA321.#O-RETURN-CODE #MDMA321.#O-RETURN-TEXT INTO #ERR-MSG
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, ReportOption.NOTITLE,"N321",pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_1(),pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_1()); //Natural: WRITE 'N321' #MDMA321.#O-ACTV-ADDRESS-LINE-1 #MDMA321.#O-ACTV-ADDRESS-LINE-1
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Err_Letter.getBoolean()))                                                                                                                       //Natural: IF #ERR-LETTER
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Format_Name.reset();                                                                                                                                          //Natural: RESET #FORMAT-NAME
        pnd_Format_Name_Pnd_In_Lname.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Preferred_Last_Name());                                                                     //Natural: ASSIGN #IN-LNAME := #MDMA321.#O-PREFERRED-LAST-NAME
        pnd_Format_Name_Pnd_In_Fname.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Preferred_First_Name());                                                                    //Natural: ASSIGN #IN-FNAME := #MDMA321.#O-PREFERRED-FIRST-NAME
        pnd_Format_Name_Pnd_In_Mname.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Preferred_Middle_Name());                                                                   //Natural: ASSIGN #IN-MNAME := #MDMA321.#O-PREFERRED-MIDDLE-NAME
        pnd_Format_Name_Pnd_In_Prfx.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Preferred_Prefix());                                                                         //Natural: ASSIGN #IN-PRFX := #MDMA321.#O-PREFERRED-PREFIX
        pnd_Format_Name_Pnd_In_Sffx.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Preferred_Suffix());                                                                         //Natural: ASSIGN #IN-SFFX := #MDMA321.#O-PREFERRED-SUFFIX
                                                                                                                                                                          //Natural: PERFORM REFORMAT-NAME
        sub_Reformat_Name();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Letter_Image_File_Pnd_O_C_Name.setValue(pnd_Format_Name_Pnd_Out_Name);                                                                                   //Natural: ASSIGN #O-C-NAME := #OUT-NAME
        short decideConditionsMet1155 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN C-ADDR-ACTVTY-CDE = 'AP' OR = 'IP'
        if (condition(confirmation_File_C_Addr_Actvty_Cde.equals("AP") || confirmation_File_C_Addr_Actvty_Cde.equals("IP")))
        {
            decideConditionsMet1155++;
                                                                                                                                                                          //Natural: PERFORM MDM-ADDRESS-ACTIVE
            sub_Mdm_Address_Active();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN C-ADDR-ACTVTY-CDE = 'F'
        else if (condition(confirmation_File_C_Addr_Actvty_Cde.equals("F")))
        {
            decideConditionsMet1155++;
                                                                                                                                                                          //Natural: PERFORM MDM-ADDRESS-FUTURE
            sub_Mdm_Address_Future();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN C-ADDR-ACTVTY-CDE = 'AV' OR = 'IV'
        else if (condition(confirmation_File_C_Addr_Actvty_Cde.equals("AV") || confirmation_File_C_Addr_Actvty_Cde.equals("IV")))
        {
            decideConditionsMet1155++;
                                                                                                                                                                          //Natural: PERFORM MDM-ADDRESS-TEMPORARY
            sub_Mdm_Address_Temporary();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  GET-CONFIRMATION-ADDRESS
    }
    private void sub_Get_Residential_Address() throws Exception                                                                                                           //Natural: GET-RESIDENTIAL-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE GET-RESIDENTIAL-ADDRESS");                                                                             //Natural: WRITE 'SUBROUTINE GET-RESIDENTIAL-ADDRESS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  PINE
        //*  BLANK = ACTIVE
        //*  ACTIVE & FUTURE
        pdaMdma361.getPnd_Mdma361().reset();                                                                                                                              //Natural: RESET #MDMA361 #GROUP-ADDRESS
        pnd_Group_Address.reset();
        pdaMdma361.getPnd_Mdma361_Pnd_I_Pin_N12().compute(new ComputeParameters(false, pdaMdma361.getPnd_Mdma361_Pnd_I_Pin_N12()), confirmation_File_C_Ph_Unique_Id_Nmbr.val()); //Natural: ASSIGN #MDMA361.#I-PIN-N12 := VAL ( C-PH-UNIQUE-ID-NMBR )
        pdaMdma361.getPnd_Mdma361_Pnd_I_Xref_Ind().setValue(" ");                                                                                                         //Natural: ASSIGN #MDMA361.#I-XREF-IND := ' '
        pdaMdma361.getPnd_Mdma361_Pnd_I_Party_Fltr().setValue("ALL");                                                                                                     //Natural: ASSIGN #MDMA361.#I-PARTY-FLTR := 'ALL'
        pdaMdma361.getPnd_Mdma361_Pnd_I_Party_Inq_Lvl().setValue(207);                                                                                                    //Natural: ASSIGN #MDMA361.#I-PARTY-INQ-LVL := 207
        pdaMdma361.getPnd_Mdma361_Pnd_I_Privpref_Fltr().setValue("PARTICIPANT PREFERENCE");                                                                               //Natural: ASSIGN #MDMA361.#I-PRIVPREF-FLTR := 'PARTICIPANT PREFERENCE'
        pdaMdma361.getPnd_Mdma361_Pnd_I_Address_Fltr().setValue("RESIDALL");                                                                                              //Natural: ASSIGN #MDMA361.#I-ADDRESS-FLTR := 'RESIDALL'
        DbsUtil.callnat(Mdmn361a.class , getCurrentProcessState(), pdaMdma361.getPnd_Mdma361());                                                                          //Natural: CALLNAT 'MDMN361A' #MDMA361
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma361.getPnd_Mdma361_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA361.#O-RETURN-CODE NE '0000'
        {
            pnd_Err_Msg.setValue(DbsUtil.compress("N361:", pdaMdma361.getPnd_Mdma361_Pnd_O_Return_Code(), pdaMdma361.getPnd_Mdma361_Pnd_O_Return_Text()));                //Natural: COMPRESS 'N361:' #MDMA361.#O-RETURN-CODE #MDMA361.#O-RETURN-TEXT INTO #ERR-MSG
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            pnd_Group_Address_Pnd_Save_Address_Good.setValue(true);                                                                                                       //Natural: ASSIGN #SAVE-ADDRESS-GOOD := TRUE
            pnd_Group_Address_Pnd_Save_Address_1.setValue("RESIDENTIAL ADDRESS");                                                                                         //Natural: ASSIGN #SAVE-ADDRESS-1 := #O-R-ADDRESS-1 := 'RESIDENTIAL ADDRESS'
            pnd_Work_Letter_Image_File_Pnd_O_R_Address_1.setValue("RESIDENTIAL ADDRESS");
            pnd_Group_Address_Pnd_Save_Address_2.setValue("NOT ON FILE");                                                                                                 //Natural: ASSIGN #SAVE-ADDRESS-2 := #O-R-ADDRESS-2 := 'NOT ON FILE'
            pnd_Work_Letter_Image_File_Pnd_O_R_Address_2.setValue("NOT ON FILE");
            pnd_Err_Letter.reset();                                                                                                                                       //Natural: RESET #ERR-LETTER
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, ReportOption.NOTITLE,"N361",pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Last_Update_Date(),pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_1(), //Natural: WRITE 'N361' #O-ACTV-RES-ADR-LAST-UPDATE-DATE #MDMA361.#O-ACTV-RES-ADR-LINE-1 #MDMA361.#O-ACTV-RES-ADR-LINE-2
                    pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_2());
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Addr_New.setValue(true);                                                                                                                                      //Natural: ASSIGN #ADDR-NEW := TRUE
        pnd_Comp_Date_A8_Pnd_Comp_Date.setValue(confirmation_File_C_Original_Chnge_Dte);                                                                                  //Natural: ASSIGN #COMP-DATE := C-ORIGINAL-CHNGE-DTE
        if (condition(pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_1().equals(" ") && pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_2().equals(" ")))            //Natural: IF #MDMA361.#O-ACTV-RES-ADR-LINE-1 = ' ' AND #MDMA361.#O-ACTV-RES-ADR-LINE-2 = ' '
        {
            pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_1().setValue("RESIDENTIAL ADDRESS");                                                                        //Natural: ASSIGN #MDMA361.#O-ACTV-RES-ADR-LINE-1 := 'RESIDENTIAL ADDRESS'
            pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_2().setValue("NOT ON FILE");                                                                                //Natural: ASSIGN #MDMA361.#O-ACTV-RES-ADR-LINE-2 := 'NOT ON FILE'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Format_Addr.reset();                                                                                                                                          //Natural: RESET #FORMAT-ADDR
        pnd_Format_Addr_Pnd_In_Addr_Type.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Type_Code());                                                              //Natural: ASSIGN #IN-ADDR-TYPE := #MDMA361.#O-ACTV-RES-ADR-TYPE-CODE
        pnd_Format_Addr_Pnd_In_Addr1.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_1());                                                                     //Natural: ASSIGN #IN-ADDR1 := #MDMA361.#O-ACTV-RES-ADR-LINE-1
        pnd_Format_Addr_Pnd_In_Addr2.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_2());                                                                     //Natural: ASSIGN #IN-ADDR2 := #MDMA361.#O-ACTV-RES-ADR-LINE-2
        pnd_Format_Addr_Pnd_In_Addr3.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_3());                                                                     //Natural: ASSIGN #IN-ADDR3 := #MDMA361.#O-ACTV-RES-ADR-LINE-3
        pnd_Format_Addr_Pnd_In_Addr4.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Line_4());                                                                     //Natural: ASSIGN #IN-ADDR4 := #MDMA361.#O-ACTV-RES-ADR-LINE-4
        pnd_Format_Addr_Pnd_In_City.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_City());                                                                        //Natural: ASSIGN #IN-CITY := #MDMA361.#O-ACTV-RES-ADR-CITY
        pnd_Format_Addr_Pnd_In_St_Prov.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_St_Prov());                                                                  //Natural: ASSIGN #IN-ST-PROV := #MDMA361.#O-ACTV-RES-ADR-ST-PROV
        pnd_Format_Addr_Pnd_In_Zip.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Zip_Code());                                                                     //Natural: ASSIGN #IN-ZIP := #MDMA361.#O-ACTV-RES-ADR-ZIP-CODE
        pnd_Format_Addr_Pnd_In_Country.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Country());                                                                  //Natural: ASSIGN #IN-COUNTRY := #MDMA361.#O-ACTV-RES-ADR-COUNTRY
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
        sub_Reformat_Address();
        if (condition(Global.isEscape())) {return;}
        pnd_Group_Address_Pnd_Save_Address_Good.setValue(true);                                                                                                           //Natural: ASSIGN #SAVE-ADDRESS-GOOD := TRUE
        pnd_Group_Address_Pnd_Save_Address_1.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(1));                                                                          //Natural: ASSIGN #SAVE-ADDRESS-1 := #O-R-ADDRESS-1 := #OUT-ADDR ( 1 )
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_1.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(1));
        pnd_Group_Address_Pnd_Save_Address_2.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(2));                                                                          //Natural: ASSIGN #SAVE-ADDRESS-2 := #O-R-ADDRESS-2 := #OUT-ADDR ( 2 )
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_2.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(2));
        pnd_Group_Address_Pnd_Save_Address_3.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(3));                                                                          //Natural: ASSIGN #SAVE-ADDRESS-3 := #O-R-ADDRESS-3 := #OUT-ADDR ( 3 )
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_3.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(3));
        pnd_Group_Address_Pnd_Save_Address_4.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(4));                                                                          //Natural: ASSIGN #SAVE-ADDRESS-4 := #O-R-ADDRESS-4 := #OUT-ADDR ( 4 )
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_4.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(4));
        pnd_Group_Address_Pnd_Save_Address_5.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(5));                                                                          //Natural: ASSIGN #SAVE-ADDRESS-5 := #O-R-ADDRESS-5 := #OUT-ADDR ( 5 )
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_5.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(5));
        pnd_Group_Address_Pnd_Save_Postal_Data.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Postal_Data());                                                      //Natural: ASSIGN #SAVE-POSTAL-DATA := #O-R-POSTAL-DATA := #MDMA361.#O-ACTV-RES-ADR-POSTAL-DATA
        pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Actv_Res_Adr_Postal_Data());
        if (condition(confirmation_File_C_Usage_Type.equals("RS") && confirmation_File_C_Addr_Actvty_Cde.equals("F ")))                                                   //Natural: IF C-USAGE-TYPE = 'RS' AND C-ADDR-ACTVTY-CDE = 'F '
        {
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, ReportOption.NOTITLE,"N360F",pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Date(),pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_1(), //Natural: WRITE 'N360F' #O-FUTR-RES-ADR-LAST-UPDATE-DATE #MDMA361.#O-FUTR-RES-ADR-LINE-1 #MDMA361.#O-FUTR-RES-ADR-LINE-2
                    pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_2());
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1226 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MDMA361.#O-FUTR-RES-ADR-STATUS-CODE = 'B'
            if (condition(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Status_Code().equals("B")))
            {
                decideConditionsMet1226++;
                pnd_Err_Msg.setValue("Future Residential Marked BAD.");                                                                                                   //Natural: ASSIGN #ERR-MSG := 'Future Residential Marked BAD.'
            }                                                                                                                                                             //Natural: WHEN #MDMA361.#O-FUTR-RES-ADR-STATUS-CODE = 'R'
            else if (condition(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Status_Code().equals("R")))
            {
                decideConditionsMet1226++;
                pnd_Err_Msg.setValue("Future Residential Marked IN-RESEARCH.");                                                                                           //Natural: ASSIGN #ERR-MSG := 'Future Residential Marked IN-RESEARCH.'
            }                                                                                                                                                             //Natural: WHEN #MDMA361.#O-FUTR-RES-ADR-LINE-1 = ' '
            else if (condition(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_1().equals(" ")))
            {
                decideConditionsMet1226++;
                pnd_Err_Msg.setValue("Future Residential Line 1 blank.");                                                                                                 //Natural: ASSIGN #ERR-MSG := 'Future Residential Line 1 blank.'
            }                                                                                                                                                             //Natural: WHEN #MDMA361.#O-FUTR-RES-ADR-LINE-2 = ' '
            else if (condition(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_2().equals(" ")))
            {
                decideConditionsMet1226++;
                pnd_Err_Msg.setValue("Future Residential Line 2 blank.");                                                                                                 //Natural: ASSIGN #ERR-MSG := 'Future Residential Line 2 blank.'
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1226 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
                sub_Print_Error();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Format_Addr.reset();                                                                                                                                      //Natural: RESET #FORMAT-ADDR
            pnd_Format_Addr_Pnd_In_Addr_Type.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Type_Code());                                                          //Natural: ASSIGN #IN-ADDR-TYPE := #MDMA361.#O-FUTR-RES-ADR-TYPE-CODE
            pnd_Format_Addr_Pnd_In_Addr1.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_1());                                                                 //Natural: ASSIGN #IN-ADDR1 := #MDMA361.#O-FUTR-RES-ADR-LINE-1
            pnd_Format_Addr_Pnd_In_Addr2.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_2());                                                                 //Natural: ASSIGN #IN-ADDR2 := #MDMA361.#O-FUTR-RES-ADR-LINE-2
            pnd_Format_Addr_Pnd_In_Addr3.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_3());                                                                 //Natural: ASSIGN #IN-ADDR3 := #MDMA361.#O-FUTR-RES-ADR-LINE-3
            pnd_Format_Addr_Pnd_In_Addr4.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Line_4());                                                                 //Natural: ASSIGN #IN-ADDR4 := #MDMA361.#O-FUTR-RES-ADR-LINE-4
            pnd_Format_Addr_Pnd_In_City.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_City());                                                                    //Natural: ASSIGN #IN-CITY := #MDMA361.#O-FUTR-RES-ADR-CITY
            pnd_Format_Addr_Pnd_In_St_Prov.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_St_Prov());                                                              //Natural: ASSIGN #IN-ST-PROV := #MDMA361.#O-FUTR-RES-ADR-ST-PROV
            pnd_Format_Addr_Pnd_In_Zip.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Zip_Code());                                                                 //Natural: ASSIGN #IN-ZIP := #MDMA361.#O-FUTR-RES-ADR-ZIP-CODE
            pnd_Format_Addr_Pnd_In_Country.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Country());                                                              //Natural: ASSIGN #IN-COUNTRY := #MDMA361.#O-FUTR-RES-ADR-COUNTRY
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
            sub_Reformat_Address();
            if (condition(Global.isEscape())) {return;}
            pnd_Work_Letter_Image_File_Pnd_O_C_Address_1.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(1));                                                              //Natural: ASSIGN #O-C-ADDRESS-1 := #OUT-ADDR ( 1 )
            pnd_Work_Letter_Image_File_Pnd_O_C_Address_2.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(2));                                                              //Natural: ASSIGN #O-C-ADDRESS-2 := #OUT-ADDR ( 2 )
            pnd_Work_Letter_Image_File_Pnd_O_C_Address_3.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(3));                                                              //Natural: ASSIGN #O-C-ADDRESS-3 := #OUT-ADDR ( 3 )
            pnd_Work_Letter_Image_File_Pnd_O_C_Address_4.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(4));                                                              //Natural: ASSIGN #O-C-ADDRESS-4 := #OUT-ADDR ( 4 )
            pnd_Work_Letter_Image_File_Pnd_O_C_Address_5.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(5));                                                              //Natural: ASSIGN #O-C-ADDRESS-5 := #OUT-ADDR ( 5 )
            pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Postal_Data());                                          //Natural: ASSIGN #O-C-POSTAL-DATA := #MDMA361.#O-FUTR-RES-ADR-POSTAL-DATA
            pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt_A.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Start_Date());                                  //Natural: ASSIGN #O-C-PERM-ADDRESS-CH-DT-A := #MDMA361.#O-FUTR-RES-ADR-START-DATE
            pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Date());                                 //Natural: ASSIGN #O-C-ADDR-LAST-CH-DT := #MDMA361.#O-FUTR-RES-ADR-LAST-UPDATE-DATE
            pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm.setValue(pdaMdma361.getPnd_Mdma361_Pnd_O_Futr_Res_Adr_Last_Update_Time());                                 //Natural: ASSIGN #O-C-ADDR-LAST-CH-TM := #MDMA361.#O-FUTR-RES-ADR-LAST-UPDATE-TIME
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-RESIDENTIAL-ADDRESS
    }
    private void sub_Mail_To_Base_Address() throws Exception                                                                                                              //Natural: MAIL-TO-BASE-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE MAIL-TO-BASE-ADDRESS");                                                                                //Natural: WRITE 'SUBROUTINE MAIL-TO-BASE-ADDRESS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  PINE
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().compute(new ComputeParameters(false, pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12()), confirmation_File_C_Ph_Unique_Id_Nmbr.val()); //Natural: ASSIGN #MDMA101.#I-PIN-N12 := VAL ( C-PH-UNIQUE-ID-NMBR )
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA101.#O-RETURN-CODE NE '0000'
        {
            getReports().write(0, ReportOption.NOTITLE,"N101:",confirmation_File_C_Ph_Unique_Id_Nmbr,pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Text());                      //Natural: WRITE 'N101:' C-PH-UNIQUE-ID-NMBR #MDMA101.#O-RETURN-TEXT
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, ReportOption.NOTITLE,"101",pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1(),pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2()); //Natural: WRITE '101' #MDMA101.#O-BASE-ADDRESS-LINE-1 #MDMA101.#O-BASE-ADDRESS-LINE-2
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1284 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MDMA101.#O-BASE-ADDRESS-STATUS-CODE = 'B'
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Status_Code().equals("B")))
        {
            decideConditionsMet1284++;
            pnd_Err_Msg.setValue("Base Address Marked BAD.");                                                                                                             //Natural: ASSIGN #ERR-MSG := 'Base Address Marked BAD.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA101.#O-BASE-ADDRESS-STATUS-CODE = 'R'
        else if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Status_Code().equals("R")))
        {
            decideConditionsMet1284++;
            pnd_Err_Msg.setValue("Base Address Marked IN-RESEARCH.");                                                                                                     //Natural: ASSIGN #ERR-MSG := 'Base Address Marked IN-RESEARCH.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA101.#O-BASE-ADDRESS-LINE-1 = ' '
        else if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1().equals(" ")))
        {
            decideConditionsMet1284++;
            pnd_Err_Msg.setValue("Base Address Line 1 blank.");                                                                                                           //Natural: ASSIGN #ERR-MSG := 'Base Address Line 1 blank.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA101.#O-BASE-ADDRESS-LINE-2 = ' '
        else if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2().equals(" ")))
        {
            decideConditionsMet1284++;
            pnd_Err_Msg.setValue("Base Address Line 2 blank.");                                                                                                           //Natural: ASSIGN #ERR-MSG := 'Base Address Line 2 blank.'
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1284 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Format_Name.reset();                                                                                                                                          //Natural: RESET #FORMAT-NAME
        pnd_Format_Name_Pnd_In_Lname.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Preferred_Last_Name());                                                                     //Natural: ASSIGN #IN-LNAME := #MDMA101.#O-PREFERRED-LAST-NAME
        pnd_Format_Name_Pnd_In_Fname.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Preferred_First_Name());                                                                    //Natural: ASSIGN #IN-FNAME := #MDMA101.#O-PREFERRED-FIRST-NAME
        pnd_Format_Name_Pnd_In_Mname.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Preferred_Middle_Name());                                                                   //Natural: ASSIGN #IN-MNAME := #MDMA101.#O-PREFERRED-MIDDLE-NAME
        pnd_Format_Name_Pnd_In_Prfx.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Preferred_Prefix());                                                                         //Natural: ASSIGN #IN-PRFX := #MDMA101.#O-PREFERRED-PREFIX
        pnd_Format_Name_Pnd_In_Sffx.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Preferred_Suffix());                                                                         //Natural: ASSIGN #IN-SFFX := #MDMA101.#O-PREFERRED-SUFFIX
                                                                                                                                                                          //Natural: PERFORM REFORMAT-NAME
        sub_Reformat_Name();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Letter_Image_File_Pnd_O_M_Name.setValue(pnd_Format_Name_Pnd_Out_Name);                                                                                   //Natural: ASSIGN #O-M-NAME := #OUT-NAME
        pnd_Format_Addr.reset();                                                                                                                                          //Natural: RESET #FORMAT-ADDR
        pnd_Format_Addr_Pnd_In_Addr_Type.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Type_Code());                                                              //Natural: ASSIGN #IN-ADDR-TYPE := #MDMA101.#O-BASE-ADDRESS-TYPE-CODE
        pnd_Format_Addr_Pnd_In_Addr1.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1());                                                                     //Natural: ASSIGN #IN-ADDR1 := #MDMA101.#O-BASE-ADDRESS-LINE-1
        pnd_Format_Addr_Pnd_In_Addr2.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2());                                                                     //Natural: ASSIGN #IN-ADDR2 := #MDMA101.#O-BASE-ADDRESS-LINE-2
        pnd_Format_Addr_Pnd_In_Addr3.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3());                                                                     //Natural: ASSIGN #IN-ADDR3 := #MDMA101.#O-BASE-ADDRESS-LINE-3
        pnd_Format_Addr_Pnd_In_Addr4.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_4());                                                                     //Natural: ASSIGN #IN-ADDR4 := #MDMA101.#O-BASE-ADDRESS-LINE-4
        pnd_Format_Addr_Pnd_In_City.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_City());                                                                        //Natural: ASSIGN #IN-CITY := #MDMA101.#O-BASE-ADDRESS-CITY
        pnd_Format_Addr_Pnd_In_St_Prov.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_St_Prov());                                                                  //Natural: ASSIGN #IN-ST-PROV := #MDMA101.#O-BASE-ADDRESS-ST-PROV
        pnd_Format_Addr_Pnd_In_Zip.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Zip_Code());                                                                     //Natural: ASSIGN #IN-ZIP := #MDMA101.#O-BASE-ADDRESS-ZIP-CODE
        pnd_Format_Addr_Pnd_In_Country.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Country());                                                                  //Natural: ASSIGN #IN-COUNTRY := #MDMA101.#O-BASE-ADDRESS-COUNTRY
        //*  DEFAULT TO INSURANCE
        //*  NO VALUE - DEFAULT TO BLANK
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
        sub_Reformat_Address();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Letter_Image_File_Pnd_O_M_Addr_Type.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Type_Code());                                                  //Natural: ASSIGN #O-M-ADDR-TYPE := #MDMA101.#O-BASE-ADDRESS-TYPE-CODE
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_1.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(1));                                                                  //Natural: ASSIGN #O-M-ADDRESS-1 := #OUT-ADDR ( 1 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_2.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(2));                                                                  //Natural: ASSIGN #O-M-ADDRESS-2 := #OUT-ADDR ( 2 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_3.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(3));                                                                  //Natural: ASSIGN #O-M-ADDRESS-3 := #OUT-ADDR ( 3 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_4.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(4));                                                                  //Natural: ASSIGN #O-M-ADDRESS-4 := #OUT-ADDR ( 4 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_5.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(5));                                                                  //Natural: ASSIGN #O-M-ADDRESS-5 := #OUT-ADDR ( 5 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Postal_Data());                                              //Natural: ASSIGN #O-M-POSTAL-DATA := #MDMA101.#O-BASE-ADDRESS-POSTAL-DATA
        pnd_Work_Letter_Image_File_Pnd_O_M_Lob.setValue("N");                                                                                                             //Natural: ASSIGN #O-M-LOB := 'N'
        pnd_Work_Letter_Image_File_Pnd_O_M_Override.setValue(" ");                                                                                                        //Natural: ASSIGN #O-M-OVERRIDE := ' '
        pnd_Addr_Mailto.setValue(true);                                                                                                                                   //Natural: ASSIGN #ADDR-MAILTO := TRUE
        //*  MAIL-TO-BASE-ADDRESS
    }
    private void sub_Mail_To_Current_Address() throws Exception                                                                                                           //Natural: MAIL-TO-CURRENT-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE MAIL-TO-CURRENT-ADDRESS");                                                                             //Natural: WRITE 'SUBROUTINE MAIL-TO-CURRENT-ADDRESS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        if (condition(! (pnd_Addr_Current.getBoolean())))                                                                                                                 //Natural: IF NOT #ADDR-CURRENT
        {
            pdaMdma321.getPnd_Mdma321().reset();                                                                                                                          //Natural: RESET #MDMA321 #ADDR-CURRENT
            pnd_Addr_Current.reset();
            pdaMdma321.getPnd_Mdma321_Pnd_I_Contract_Number().setValue(confirmation_File_C_Cntrct_Nmbr);                                                                  //Natural: ASSIGN #MDMA321.#I-CONTRACT-NUMBER := C-CNTRCT-NMBR
            pdaMdma321.getPnd_Mdma321_Pnd_I_Payee_Code().setValue(confirmation_File_C_Cntrct_Payee_Cde);                                                                  //Natural: ASSIGN #MDMA321.#I-PAYEE-CODE := C-CNTRCT-PAYEE-CDE
            pdaMdma321.getPnd_Mdma321_Pnd_I_Cntrct_Role_Fltr().setValue("ALL");                                                                                           //Natural: ASSIGN #MDMA321.#I-CNTRCT-ROLE-FLTR := 'ALL'
            pdaMdma321.getPnd_Mdma321_Pnd_I_Address_Usage().setValue("O");                                                                                                //Natural: ASSIGN #MDMA321.#I-ADDRESS-USAGE := 'O'
            pnd_Addr_Current.setValue(true);                                                                                                                              //Natural: ASSIGN #ADDR-CURRENT := TRUE
            if (condition(confirmation_File_C_Cntrct_Type.equals("D")))                                                                                                   //Natural: IF C-CNTRCT-TYPE = 'D'
            {
                pdaMdma321.getPnd_Mdma321_Pnd_I_Payee_Code_A2().setValue("01");                                                                                           //Natural: ASSIGN #MDMA321.#I-PAYEE-CODE-A2 := '01'
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Mdmn320a.class , getCurrentProcessState(), pdaMdma321.getPnd_Mdma321());                                                                      //Natural: CALLNAT 'MDMN320A' #MDMA321
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA321.#O-RETURN-CODE NE '0000'
        {
            pnd_Err_Msg.setValue(DbsUtil.compress("N321:", pdaMdma321.getPnd_Mdma321_Pnd_O_Return_Code(), pdaMdma321.getPnd_Mdma321_Pnd_O_Return_Text()));                //Natural: COMPRESS 'N321:' #MDMA321.#O-RETURN-CODE #MDMA321.#O-RETURN-TEXT INTO #ERR-MSG
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, ReportOption.NOTITLE,"N321",pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_1(),pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_1()); //Natural: WRITE 'N321' #MDMA321.#O-ACTV-ADDRESS-LINE-1 #MDMA321.#O-ACTV-ADDRESS-LINE-1
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FROM CONTRACT
        short decideConditionsMet1363 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MDMA321.#O-ACTV-ADDRESS-STATUS-CODE = 'B'
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Status_Code().equals("B")))
        {
            decideConditionsMet1363++;
            pnd_Err_Msg.setValue("Current Address Marked BAD.");                                                                                                          //Natural: ASSIGN #ERR-MSG := 'Current Address Marked BAD.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#O-ACTV-ADDRESS-STATUS-CODE = 'R'
        else if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Status_Code().equals("R")))
        {
            decideConditionsMet1363++;
            pnd_Err_Msg.setValue("Current Address Marked IN-RESEARCH.");                                                                                                  //Natural: ASSIGN #ERR-MSG := 'Current Address Marked IN-RESEARCH.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#O-CONTRACT-STATUS-CODE > ' '
        else if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Contract_Status_Code().greater(" ")))
        {
            decideConditionsMet1363++;
            pnd_Err_Msg.setValue("Current Address Marked TERMINATED.");                                                                                                   //Natural: ASSIGN #ERR-MSG := 'Current Address Marked TERMINATED.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#O-ACTV-ADDRESS-LINE-1 = ' '
        else if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_1().equals(" ")))
        {
            decideConditionsMet1363++;
            pnd_Err_Msg.setValue("Current Address Line 1 blank.");                                                                                                        //Natural: ASSIGN #ERR-MSG := 'Current Address Line 1 blank.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#O-ACTV-ADDRESS-LINE-2 = ' '
        else if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_2().equals(" ")))
        {
            decideConditionsMet1363++;
            pnd_Err_Msg.setValue("Current Address Line 2 blank.");                                                                                                        //Natural: ASSIGN #ERR-MSG := 'Current Address Line 2 blank.'
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1363 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Format_Name.reset();                                                                                                                                          //Natural: RESET #FORMAT-NAME
        pnd_Format_Name_Pnd_In_Lname.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Preferred_Last_Name());                                                                     //Natural: ASSIGN #IN-LNAME := #MDMA321.#O-PREFERRED-LAST-NAME
        pnd_Format_Name_Pnd_In_Fname.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Preferred_First_Name());                                                                    //Natural: ASSIGN #IN-FNAME := #MDMA321.#O-PREFERRED-FIRST-NAME
        pnd_Format_Name_Pnd_In_Mname.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Preferred_Middle_Name());                                                                   //Natural: ASSIGN #IN-MNAME := #MDMA321.#O-PREFERRED-MIDDLE-NAME
        pnd_Format_Name_Pnd_In_Prfx.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Preferred_Prefix());                                                                         //Natural: ASSIGN #IN-PRFX := #MDMA321.#O-PREFERRED-PREFIX
        pnd_Format_Name_Pnd_In_Sffx.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Preferred_Suffix());                                                                         //Natural: ASSIGN #IN-SFFX := #MDMA321.#O-PREFERRED-SUFFIX
                                                                                                                                                                          //Natural: PERFORM REFORMAT-NAME
        sub_Reformat_Name();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Letter_Image_File_Pnd_O_M_Name.setValue(pnd_Format_Name_Pnd_Out_Name);                                                                                   //Natural: ASSIGN #O-M-NAME := #OUT-NAME
        pnd_Format_Addr.reset();                                                                                                                                          //Natural: RESET #FORMAT-ADDR
        pnd_Format_Addr_Pnd_In_Addr_Type.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Type_Code());                                                              //Natural: ASSIGN #IN-ADDR-TYPE := #MDMA321.#O-ACTV-ADDRESS-TYPE-CODE
        pnd_Format_Addr_Pnd_In_Addr1.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_1());                                                                     //Natural: ASSIGN #IN-ADDR1 := #MDMA321.#O-ACTV-ADDRESS-LINE-1
        pnd_Format_Addr_Pnd_In_Addr2.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_2());                                                                     //Natural: ASSIGN #IN-ADDR2 := #MDMA321.#O-ACTV-ADDRESS-LINE-2
        pnd_Format_Addr_Pnd_In_Addr3.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_3());                                                                     //Natural: ASSIGN #IN-ADDR3 := #MDMA321.#O-ACTV-ADDRESS-LINE-3
        pnd_Format_Addr_Pnd_In_Addr4.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_4());                                                                     //Natural: ASSIGN #IN-ADDR4 := #MDMA321.#O-ACTV-ADDRESS-LINE-4
        pnd_Format_Addr_Pnd_In_City.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_City());                                                                        //Natural: ASSIGN #IN-CITY := #MDMA321.#O-ACTV-ADDRESS-CITY
        pnd_Format_Addr_Pnd_In_St_Prov.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_St_Prov());                                                                  //Natural: ASSIGN #IN-ST-PROV := #MDMA321.#O-ACTV-ADDRESS-ST-PROV
        pnd_Format_Addr_Pnd_In_Zip.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Zip_Code());                                                                     //Natural: ASSIGN #IN-ZIP := #MDMA321.#O-ACTV-ADDRESS-ZIP-CODE
        pnd_Format_Addr_Pnd_In_Country.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Country());                                                                  //Natural: ASSIGN #IN-COUNTRY := #MDMA321.#O-ACTV-ADDRESS-COUNTRY
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
        sub_Reformat_Address();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Letter_Image_File_Pnd_O_M_Addr_Type.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Type_Code());                                                  //Natural: ASSIGN #O-M-ADDR-TYPE := #MDMA321.#O-ACTV-ADDRESS-TYPE-CODE
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_1.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(1));                                                                  //Natural: ASSIGN #O-M-ADDRESS-1 := #OUT-ADDR ( 1 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_2.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(2));                                                                  //Natural: ASSIGN #O-M-ADDRESS-2 := #OUT-ADDR ( 2 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_3.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(3));                                                                  //Natural: ASSIGN #O-M-ADDRESS-3 := #OUT-ADDR ( 3 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_4.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(4));                                                                  //Natural: ASSIGN #O-M-ADDRESS-4 := #OUT-ADDR ( 4 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_5.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(5));                                                                  //Natural: ASSIGN #O-M-ADDRESS-5 := #OUT-ADDR ( 5 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Postal_Data());                                              //Natural: ASSIGN #O-M-POSTAL-DATA := #MDMA321.#O-ACTV-ADDRESS-POSTAL-DATA
        pnd_Work_Letter_Image_File_Pnd_O_M_Lob.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Line_Of_Business());                                                              //Natural: ASSIGN #O-M-LOB := #MDMA321.#O-LINE-OF-BUSINESS
        pnd_Work_Letter_Image_File_Pnd_O_M_Override.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Override_Code());                                               //Natural: ASSIGN #O-M-OVERRIDE := #MDMA321.#O-ACTV-ADDRESS-OVERRIDE-CODE
        pnd_Addr_Mailto.setValue(true);                                                                                                                                   //Natural: ASSIGN #ADDR-MAILTO := TRUE
        //*  MAIL-TO-CURRENT-ADDRESS
    }
    private void sub_Mail_To_Previous_Address() throws Exception                                                                                                          //Natural: MAIL-TO-PREVIOUS-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE MAIL-TO-PREVIOUS-ADDRESS");                                                                            //Natural: WRITE 'SUBROUTINE MAIL-TO-PREVIOUS-ADDRESS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        if (condition(DbsUtil.maskMatches(confirmation_File_C_Original_Chnge_Dte,"YYYYMMDD")))                                                                            //Natural: IF C-ORIGINAL-CHNGE-DTE = MASK ( YYYYMMDD )
        {
            pnd_L_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),confirmation_File_C_Orig_Chnge_Dte_A);                                                               //Natural: MOVE EDITED C-ORIG-CHNGE-DTE-A TO #L-DATE ( EM = YYYYMMDD )
            pnd_L_Date_F.setValueEdited(pnd_L_Date,new ReportEditMask("YYYY-MM-DD"));                                                                                     //Natural: MOVE EDITED #L-DATE ( EM = YYYY-MM-DD ) TO #L-DATE-F
            //*  MDMN320A WILL DO 2 CALLS TO
            //*  GET CURRENT; THEN PRVS ADDRESS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_L_Date_F.setValue("          ");                                                                                                                          //Natural: ASSIGN #L-DATE-F := '          '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(confirmation_File_C_Original_Chnge_Tme_A,"9999999")))                                                                           //Natural: IF C-ORIGINAL-CHNGE-TME-A = MASK ( 9999999 )
        {
            pnd_L_Time.setValueEdited(new ReportEditMask("HHIISST"),confirmation_File_C_Original_Chnge_Tme_A);                                                            //Natural: MOVE EDITED C-ORIGINAL-CHNGE-TME-A TO #L-TIME ( EM = HHIISST )
            pnd_L_Time_F.setValueEdited(pnd_L_Time,new ReportEditMask("HH:II:SS"));                                                                                       //Natural: MOVE EDITED #L-TIME ( EM = HH:II:SS ) TO #L-TIME-F
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_L_Time_F.setValue("        ");                                                                                                                            //Natural: ASSIGN #L-TIME-F := '        '
        }                                                                                                                                                                 //Natural: END-IF
        //*  ALWAYS CORRESPONDENCE
        pdaMdma331.getPnd_Mdma331().reset();                                                                                                                              //Natural: RESET #MDMA331 #ERR-MSG #PREVIOUS-BAD
        pnd_Err_Msg.reset();
        pnd_Previous_Bad.reset();
        pdaMdma331.getPnd_Mdma331_Pnd_I_Contract_Number().setValue(confirmation_File_C_Cntrct_Nmbr);                                                                      //Natural: ASSIGN #MDMA331.#I-CONTRACT-NUMBER := C-CNTRCT-NMBR
        pdaMdma331.getPnd_Mdma331_Pnd_I_Payee_Code().setValue(confirmation_File_C_Cntrct_Payee_Cde);                                                                      //Natural: ASSIGN #MDMA331.#I-PAYEE-CODE := C-CNTRCT-PAYEE-CDE
        pdaMdma331.getPnd_Mdma331_Pnd_I_Address_Usage().setValue("O");                                                                                                    //Natural: ASSIGN #MDMA331.#I-ADDRESS-USAGE := 'O'
        pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Date().setValue(pnd_L_Date_F);                                                                                            //Natural: ASSIGN #MDMA331.#I-INQUIRE-DATE := #L-DATE-F
        pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Time().setValue(pnd_L_Time_F);                                                                                            //Natural: ASSIGN #MDMA331.#I-INQUIRE-TIME := #L-TIME-F
        if (condition(confirmation_File_C_Cntrct_Type.equals("D")))                                                                                                       //Natural: IF C-CNTRCT-TYPE = 'D'
        {
            pdaMdma331.getPnd_Mdma331_Pnd_I_Payee_Code_A2().setValue("01");                                                                                               //Natural: ASSIGN #MDMA331.#I-PAYEE-CODE-A2 := '01'
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Mdmn331a.class , getCurrentProcessState(), pdaMdma331.getPnd_Mdma331());                                                                          //Natural: CALLNAT 'MDMN331A' #MDMA331
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma331.getPnd_Mdma331_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA331.#O-RETURN-CODE NE '0000'
        {
            pnd_Err_Msg.setValue(DbsUtil.compress("N331:", pdaMdma331.getPnd_Mdma331_Pnd_O_Return_Code(), pdaMdma331.getPnd_Mdma331_Pnd_O_Return_Text()));                //Natural: COMPRESS 'N331:' #MDMA331.#O-RETURN-CODE #MDMA331.#O-RETURN-TEXT INTO #ERR-MSG
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, ReportOption.NOTITLE,"N331",pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Line_1(),pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Line_2()); //Natural: WRITE 'N331' #MDMA331.#O-PRVS-ADDRESS-LINE-1 #MDMA331.#O-PRVS-ADDRESS-LINE-2
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET CURRENT ADDRESS
        if (condition(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Status_Code().equals("B")))                                                                            //Natural: IF #MDMA331.#O-PRVS-ADDRESS-STATUS-CODE = 'B'
        {
            pnd_Previous_Bad.setValue(true);                                                                                                                              //Natural: ASSIGN #PREVIOUS-BAD := TRUE
            pdaMdma331.getPnd_Mdma331_Pnd_O_Return_Section().reset();                                                                                                     //Natural: RESET #MDMA331.#O-RETURN-SECTION #MDMA331.#O-DATA-SECTION ( * )
            pdaMdma331.getPnd_Mdma331_Pnd_O_Data_Section().getValue("*").reset();
            pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Date().setValueEdited(Global.getDATX(),new ReportEditMask("YYYY-MM-DD"));                                             //Natural: MOVE EDITED *DATX ( EM = YYYY-MM-DD ) TO #MDMA331.#I-INQUIRE-DATE
            pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Time().setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                               //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO #MDMA331.#I-INQUIRE-TIME
            DbsUtil.callnat(Mdmn331a.class , getCurrentProcessState(), pdaMdma331.getPnd_Mdma331());                                                                      //Natural: CALLNAT 'MDMN331A' #MDMA331
            if (condition(Global.isEscape())) return;
            if (condition(pdaMdma331.getPnd_Mdma331_Pnd_O_Return_Code().notEquals("0000")))                                                                               //Natural: IF #MDMA331.#O-RETURN-CODE NE '0000'
            {
                pnd_Err_Msg.setValue(DbsUtil.compress("N331b:", pdaMdma331.getPnd_Mdma331_Pnd_O_Return_Code(), pdaMdma331.getPnd_Mdma331_Pnd_O_Return_Text()));           //Natural: COMPRESS 'N331b:' #MDMA331.#O-RETURN-CODE #MDMA331.#O-RETURN-TEXT INTO #ERR-MSG
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
                sub_Print_Error();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pls_Trace.getBoolean()))                                                                                                                    //Natural: IF +TRACE
                {
                    getReports().write(0, ReportOption.NOTITLE,"N331b",pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Line_1(),pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Line_2()); //Natural: WRITE 'N331b' #MDMA331.#O-PRVS-ADDRESS-LINE-1 #MDMA331.#O-PRVS-ADDRESS-LINE-2
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1474 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MDMA331.#O-PRVS-ADDRESS-STATUS-CODE = 'B'
        if (condition(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Status_Code().equals("B")))
        {
            decideConditionsMet1474++;
            pnd_Err_Msg.setValue("Old Address Marked BAD.");                                                                                                              //Natural: ASSIGN #ERR-MSG := 'Old Address Marked BAD.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA331.#O-PRVS-ADDRESS-STATUS-CODE = 'R'
        else if (condition(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Status_Code().equals("R")))
        {
            decideConditionsMet1474++;
            pnd_Err_Msg.setValue("Old Address Marked IN-RESEARCH.");                                                                                                      //Natural: ASSIGN #ERR-MSG := 'Old Address Marked IN-RESEARCH.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA331.#O-CONTRACT-STATUS-CODE > ' '
        else if (condition(pdaMdma331.getPnd_Mdma331_Pnd_O_Contract_Status_Code().greater(" ")))
        {
            decideConditionsMet1474++;
            pnd_Err_Msg.setValue("Old Address Marked TERMINATED.");                                                                                                       //Natural: ASSIGN #ERR-MSG := 'Old Address Marked TERMINATED.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA331.#O-PRVS-ADDRESS-LINE-1 = ' '
        else if (condition(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Line_1().equals(" ")))
        {
            decideConditionsMet1474++;
            pnd_Err_Msg.setValue("Old Address Line 1 blank.");                                                                                                            //Natural: ASSIGN #ERR-MSG := 'Old Address Line 1 blank.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA331.#O-PRVS-ADDRESS-LINE-2 = ' '
        else if (condition(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Line_2().equals(" ")))
        {
            decideConditionsMet1474++;
            pnd_Err_Msg.setValue("Old Address Line 2 blank.");                                                                                                            //Natural: ASSIGN #ERR-MSG := 'Old Address Line 2 blank.'
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1474 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Format_Name.reset();                                                                                                                                          //Natural: RESET #FORMAT-NAME
        pnd_Format_Name_Pnd_In_Lname.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Preferred_Last_Name());                                                                     //Natural: ASSIGN #IN-LNAME := #MDMA331.#O-PREFERRED-LAST-NAME
        pnd_Format_Name_Pnd_In_Fname.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Preferred_First_Name());                                                                    //Natural: ASSIGN #IN-FNAME := #MDMA331.#O-PREFERRED-FIRST-NAME
        pnd_Format_Name_Pnd_In_Mname.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Preferred_Middle_Name());                                                                   //Natural: ASSIGN #IN-MNAME := #MDMA331.#O-PREFERRED-MIDDLE-NAME
        pnd_Format_Name_Pnd_In_Prfx.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Preferred_Prefix());                                                                         //Natural: ASSIGN #IN-PRFX := #MDMA331.#O-PREFERRED-PREFIX
        pnd_Format_Name_Pnd_In_Sffx.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Preferred_Suffix());                                                                         //Natural: ASSIGN #IN-SFFX := #MDMA331.#O-PREFERRED-SUFFIX
                                                                                                                                                                          //Natural: PERFORM REFORMAT-NAME
        sub_Reformat_Name();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Letter_Image_File_Pnd_O_M_Name.setValue(pnd_Format_Name_Pnd_Out_Name);                                                                                   //Natural: ASSIGN #O-M-NAME := #OUT-NAME
        pnd_Format_Addr.reset();                                                                                                                                          //Natural: RESET #FORMAT-ADDR
        pnd_Format_Addr_Pnd_In_Addr_Type.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Type_Code());                                                              //Natural: ASSIGN #IN-ADDR-TYPE := #MDMA331.#O-PRVS-ADDRESS-TYPE-CODE
        pnd_Format_Addr_Pnd_In_Addr1.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Line_1());                                                                     //Natural: ASSIGN #IN-ADDR1 := #MDMA331.#O-PRVS-ADDRESS-LINE-1
        pnd_Format_Addr_Pnd_In_Addr2.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Line_2());                                                                     //Natural: ASSIGN #IN-ADDR2 := #MDMA331.#O-PRVS-ADDRESS-LINE-2
        pnd_Format_Addr_Pnd_In_Addr3.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Line_3());                                                                     //Natural: ASSIGN #IN-ADDR3 := #MDMA331.#O-PRVS-ADDRESS-LINE-3
        pnd_Format_Addr_Pnd_In_Addr4.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Line_4());                                                                     //Natural: ASSIGN #IN-ADDR4 := #MDMA331.#O-PRVS-ADDRESS-LINE-4
        pnd_Format_Addr_Pnd_In_City.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_City());                                                                        //Natural: ASSIGN #IN-CITY := #MDMA331.#O-PRVS-ADDRESS-CITY
        pnd_Format_Addr_Pnd_In_St_Prov.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_St_Prov());                                                                  //Natural: ASSIGN #IN-ST-PROV := #MDMA331.#O-PRVS-ADDRESS-ST-PROV
        pnd_Format_Addr_Pnd_In_Zip.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Zip_Code());                                                                     //Natural: ASSIGN #IN-ZIP := #MDMA331.#O-PRVS-ADDRESS-ZIP-CODE
        pnd_Format_Addr_Pnd_In_Country.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Country());                                                                  //Natural: ASSIGN #IN-COUNTRY := #MDMA331.#O-PRVS-ADDRESS-COUNTRY
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
        sub_Reformat_Address();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Letter_Image_File_Pnd_O_M_Addr_Type.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Type_Code());                                                  //Natural: ASSIGN #O-M-ADDR-TYPE := #MDMA331.#O-PRVS-ADDRESS-TYPE-CODE
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_1.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(1));                                                                  //Natural: ASSIGN #O-M-ADDRESS-1 := #OUT-ADDR ( 1 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_2.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(2));                                                                  //Natural: ASSIGN #O-M-ADDRESS-2 := #OUT-ADDR ( 2 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_3.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(3));                                                                  //Natural: ASSIGN #O-M-ADDRESS-3 := #OUT-ADDR ( 3 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_4.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(4));                                                                  //Natural: ASSIGN #O-M-ADDRESS-4 := #OUT-ADDR ( 4 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_5.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(5));                                                                  //Natural: ASSIGN #O-M-ADDRESS-5 := #OUT-ADDR ( 5 )
        pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Postal_Data());                                              //Natural: ASSIGN #O-M-POSTAL-DATA := #MDMA331.#O-PRVS-ADDRESS-POSTAL-DATA
        pnd_Work_Letter_Image_File_Pnd_O_M_Lob.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Line_Of_Business());                                                              //Natural: ASSIGN #O-M-LOB := #MDMA331.#O-LINE-OF-BUSINESS
        pnd_Work_Letter_Image_File_Pnd_O_M_Override.setValue(pdaMdma331.getPnd_Mdma331_Pnd_O_Prvs_Address_Override_Code());                                               //Natural: ASSIGN #O-M-OVERRIDE := #MDMA331.#O-PRVS-ADDRESS-OVERRIDE-CODE
        pnd_Addr_Mailto.setValue(true);                                                                                                                                   //Natural: ASSIGN #ADDR-MAILTO := TRUE
        //*  MAIL-TO-PREVIOUS-ADDRESS
    }
    private void sub_Mdm_Address_Active() throws Exception                                                                                                                //Natural: MDM-ADDRESS-ACTIVE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE MDM-ADDRESS-ACTIVE");                                                                                  //Natural: WRITE 'SUBROUTINE MDM-ADDRESS-ACTIVE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  FROM CONTRACT
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Contract_Status_Code().greater(" ")))                                                                               //Natural: IF #MDMA321.#O-CONTRACT-STATUS-CODE > ' '
        {
            pnd_Err_Msg.setValue("New Address Marked TERMINATED.");                                                                                                       //Natural: ASSIGN #ERR-MSG := 'New Address Marked TERMINATED.'
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_L_Acct_No.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Bank_Account_Number());                                                                               //Natural: ASSIGN #L-ACCT-NO := #MDMA321.#O-ACTV-BANK-ACCOUNT-NUMBER
        pnd_L_Eft_No.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Bank_Aba_Eft_Number());                                                                                //Natural: ASSIGN #L-EFT-NO := #MDMA321.#O-ACTV-BANK-ABA-EFT-NUMBER
                                                                                                                                                                          //Natural: PERFORM DETERMINE-ADDR-FORMAT-CODE
        sub_Determine_Addr_Format_Code();
        if (condition(Global.isEscape())) {return;}
        pnd_Addr_New.setValue(true);                                                                                                                                      //Natural: ASSIGN #ADDR-NEW := TRUE
        pnd_Work_Letter_Image_File_Pnd_O_C_Contract_Type_Cd.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Line_Of_Business());                                                 //Natural: ASSIGN #O-C-CONTRACT-TYPE-CD := #MDMA321.#O-LINE-OF-BUSINESS
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Type.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Type_Code());                                                  //Natural: ASSIGN #O-C-ADDR-TYPE := #MDMA321.#O-ACTV-ADDRESS-TYPE-CODE
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd.setValue(pnd_L_Format_Cd);                                                                                      //Natural: ASSIGN #O-C-ADDR-FORMAT-CD := #L-FORMAT-CD
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Last_Update_Date());                                     //Natural: ASSIGN #O-C-ADDR-LAST-CH-DT := #MDMA321.#O-ACTV-ADDRESS-LAST-UPDATE-DATE
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Last_Update_Time());                                     //Natural: ASSIGN #O-C-ADDR-LAST-CH-TM := #MDMA321.#O-ACTV-ADDRESS-LAST-UPDATE-TIME
        pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Postal_Data());                                              //Natural: ASSIGN #O-C-POSTAL-DATA := #MDMA321.#O-ACTV-ADDRESS-POSTAL-DATA
        pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Bank_Account_Number());                                               //Natural: ASSIGN #O-C-PH-BANK-NO := #MDMA321.#O-ACTV-BANK-ACCOUNT-NUMBER
        pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Bank_Aba_Eft_Number());                                           //Natural: ASSIGN #O-C-ABA-ACCOUNT-NO := #MDMA321.#O-ACTV-BANK-ABA-EFT-NUMBER
        pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Check_Saving_Ind());                                             //Natural: ASSIGN #O-C-CHECK-SAVING-CD := #MDMA321.#O-ACTV-CHECK-SAVING-IND
        pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Annual_Ind());                                      //Natural: ASSIGN #O-C-ANNUAL-VAC-CYCLE-IND := #MDMA321.#O-TEMP-ADDRESS-ANNUAL-IND
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Start_Date());                                        //Natural: ASSIGN #O-C-PENDING-ADDR-CH-DT := #MDMA321.#O-TEMP-ADDRESS-START-DATE
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_End_Date());                                          //Natural: ASSIGN #O-C-PENDING-ADDR-RS-DT := #MDMA321.#O-TEMP-ADDRESS-END-DATE
        pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Start_Date());                                        //Natural: ASSIGN #O-C-PERM-ADDRESS-CH-DT := #MDMA321.#O-FUTR-ADDRESS-START-DATE
        pnd_Format_Addr.reset();                                                                                                                                          //Natural: RESET #FORMAT-ADDR
        pnd_Format_Addr_Pnd_In_Addr_Type.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Type_Code());                                                              //Natural: ASSIGN #IN-ADDR-TYPE := #MDMA321.#O-ACTV-ADDRESS-TYPE-CODE
        pnd_Format_Addr_Pnd_In_Addr1.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_1());                                                                     //Natural: ASSIGN #IN-ADDR1 := #MDMA321.#O-ACTV-ADDRESS-LINE-1
        pnd_Format_Addr_Pnd_In_Addr2.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_2());                                                                     //Natural: ASSIGN #IN-ADDR2 := #MDMA321.#O-ACTV-ADDRESS-LINE-2
        pnd_Format_Addr_Pnd_In_Addr3.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_3());                                                                     //Natural: ASSIGN #IN-ADDR3 := #MDMA321.#O-ACTV-ADDRESS-LINE-3
        pnd_Format_Addr_Pnd_In_Addr4.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Line_4());                                                                     //Natural: ASSIGN #IN-ADDR4 := #MDMA321.#O-ACTV-ADDRESS-LINE-4
        pnd_Format_Addr_Pnd_In_City.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_City());                                                                        //Natural: ASSIGN #IN-CITY := #MDMA321.#O-ACTV-ADDRESS-CITY
        pnd_Format_Addr_Pnd_In_St_Prov.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_St_Prov());                                                                  //Natural: ASSIGN #IN-ST-PROV := #MDMA321.#O-ACTV-ADDRESS-ST-PROV
        pnd_Format_Addr_Pnd_In_Zip.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Zip_Code());                                                                     //Natural: ASSIGN #IN-ZIP := #MDMA321.#O-ACTV-ADDRESS-ZIP-CODE
        pnd_Format_Addr_Pnd_In_Country.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Actv_Address_Country());                                                                  //Natural: ASSIGN #IN-COUNTRY := #MDMA321.#O-ACTV-ADDRESS-COUNTRY
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
        sub_Reformat_Address();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_1.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(1));                                                                  //Natural: ASSIGN #O-C-ADDRESS-1 := #OUT-ADDR ( 1 )
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_2.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(2));                                                                  //Natural: ASSIGN #O-C-ADDRESS-2 := #OUT-ADDR ( 2 )
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_3.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(3));                                                                  //Natural: ASSIGN #O-C-ADDRESS-3 := #OUT-ADDR ( 3 )
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_4.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(4));                                                                  //Natural: ASSIGN #O-C-ADDRESS-4 := #OUT-ADDR ( 4 )
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_5.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(5));                                                                  //Natural: ASSIGN #O-C-ADDRESS-5 := #OUT-ADDR ( 5 )
        //*  MDM-ADDRESS-ACTIVE
    }
    private void sub_Mdm_Address_Future() throws Exception                                                                                                                //Natural: MDM-ADDRESS-FUTURE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE MDM-ADDRESS-FUTURE");                                                                                  //Natural: WRITE 'SUBROUTINE MDM-ADDRESS-FUTURE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  FROM CONTRACT
        short decideConditionsMet1581 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MDMA321.#O-FUTR-ADDRESS-STATUS-CODE = 'B'
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Status_Code().equals("B")))
        {
            decideConditionsMet1581++;
            pnd_Err_Msg.setValue("Future Address Marked BAD.");                                                                                                           //Natural: ASSIGN #ERR-MSG := 'Future Address Marked BAD.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#O-FUTR-ADDRESS-STATUS-CODE = 'R'
        else if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Status_Code().equals("R")))
        {
            decideConditionsMet1581++;
            pnd_Err_Msg.setValue("Future Address Marked IN-RESEARCH.");                                                                                                   //Natural: ASSIGN #ERR-MSG := 'Future Address Marked IN-RESEARCH.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#O-CONTRACT-STATUS-CODE > ' '
        else if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Contract_Status_Code().greater(" ")))
        {
            decideConditionsMet1581++;
            pnd_Err_Msg.setValue("Future Address Marked TERMINATED.");                                                                                                    //Natural: ASSIGN #ERR-MSG := 'Future Address Marked TERMINATED.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#O-FUTR-ADDRESS-LINE-1 = ' '
        else if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Line_1().equals(" ")))
        {
            decideConditionsMet1581++;
            pnd_Err_Msg.setValue("Future Address Line 1 blank.");                                                                                                         //Natural: ASSIGN #ERR-MSG := 'Future Address Line 1 blank.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#O-FUTR-ADDRESS-LINE-2 = ' '
        else if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Line_2().equals(" ")))
        {
            decideConditionsMet1581++;
            pnd_Err_Msg.setValue("Future Address Line 2 blank.");                                                                                                         //Natural: ASSIGN #ERR-MSG := 'Future Address Line 2 blank.'
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1581 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_L_Acct_No.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Bank_Account_Number());                                                                               //Natural: ASSIGN #L-ACCT-NO := #MDMA321.#O-FUTR-BANK-ACCOUNT-NUMBER
        pnd_L_Eft_No.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Bank_Aba_Eft_Number());                                                                                //Natural: ASSIGN #L-EFT-NO := #MDMA321.#O-FUTR-BANK-ABA-EFT-NUMBER
                                                                                                                                                                          //Natural: PERFORM DETERMINE-ADDR-FORMAT-CODE
        sub_Determine_Addr_Format_Code();
        if (condition(Global.isEscape())) {return;}
        pnd_Addr_New.setValue(true);                                                                                                                                      //Natural: ASSIGN #ADDR-NEW := TRUE
        pnd_Work_Letter_Image_File_Pnd_O_C_Contract_Type_Cd.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Line_Of_Business());                                                 //Natural: ASSIGN #O-C-CONTRACT-TYPE-CD := #MDMA321.#O-LINE-OF-BUSINESS
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Type.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Type_Code());                                                  //Natural: ASSIGN #O-C-ADDR-TYPE := #MDMA321.#O-FUTR-ADDRESS-TYPE-CODE
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd.setValue(pnd_L_Format_Cd);                                                                                      //Natural: ASSIGN #O-C-ADDR-FORMAT-CD := #L-FORMAT-CD
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Last_Update_Date());                                     //Natural: ASSIGN #O-C-ADDR-LAST-CH-DT := #MDMA321.#O-FUTR-ADDRESS-LAST-UPDATE-DATE
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Last_Update_Time());                                     //Natural: ASSIGN #O-C-ADDR-LAST-CH-TM := #MDMA321.#O-FUTR-ADDRESS-LAST-UPDATE-TIME
        pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Postal_Data());                                              //Natural: ASSIGN #O-C-POSTAL-DATA := #MDMA321.#O-FUTR-ADDRESS-POSTAL-DATA
        pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Bank_Account_Number());                                               //Natural: ASSIGN #O-C-PH-BANK-NO := #MDMA321.#O-FUTR-BANK-ACCOUNT-NUMBER
        pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Bank_Aba_Eft_Number());                                           //Natural: ASSIGN #O-C-ABA-ACCOUNT-NO := #MDMA321.#O-FUTR-BANK-ABA-EFT-NUMBER
        pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Check_Saving_Ind());                                             //Natural: ASSIGN #O-C-CHECK-SAVING-CD := #MDMA321.#O-FUTR-CHECK-SAVING-IND
        pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Annual_Ind());                                      //Natural: ASSIGN #O-C-ANNUAL-VAC-CYCLE-IND := #MDMA321.#O-TEMP-ADDRESS-ANNUAL-IND
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Start_Date());                                        //Natural: ASSIGN #O-C-PENDING-ADDR-CH-DT := #MDMA321.#O-TEMP-ADDRESS-START-DATE
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_End_Date());                                          //Natural: ASSIGN #O-C-PENDING-ADDR-RS-DT := #MDMA321.#O-TEMP-ADDRESS-END-DATE
        pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Start_Date());                                        //Natural: ASSIGN #O-C-PERM-ADDRESS-CH-DT := #MDMA321.#O-FUTR-ADDRESS-START-DATE
        pnd_Format_Addr.reset();                                                                                                                                          //Natural: RESET #FORMAT-ADDR
        pnd_Format_Addr_Pnd_In_Addr_Type.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Type_Code());                                                              //Natural: ASSIGN #IN-ADDR-TYPE := #MDMA321.#O-FUTR-ADDRESS-TYPE-CODE
        pnd_Format_Addr_Pnd_In_Addr1.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Line_1());                                                                     //Natural: ASSIGN #IN-ADDR1 := #MDMA321.#O-FUTR-ADDRESS-LINE-1
        pnd_Format_Addr_Pnd_In_Addr2.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Line_2());                                                                     //Natural: ASSIGN #IN-ADDR2 := #MDMA321.#O-FUTR-ADDRESS-LINE-2
        pnd_Format_Addr_Pnd_In_Addr3.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Line_3());                                                                     //Natural: ASSIGN #IN-ADDR3 := #MDMA321.#O-FUTR-ADDRESS-LINE-3
        pnd_Format_Addr_Pnd_In_Addr4.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Line_4());                                                                     //Natural: ASSIGN #IN-ADDR4 := #MDMA321.#O-FUTR-ADDRESS-LINE-4
        pnd_Format_Addr_Pnd_In_City.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_City());                                                                        //Natural: ASSIGN #IN-CITY := #MDMA321.#O-FUTR-ADDRESS-CITY
        pnd_Format_Addr_Pnd_In_St_Prov.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_St_Prov());                                                                  //Natural: ASSIGN #IN-ST-PROV := #MDMA321.#O-FUTR-ADDRESS-ST-PROV
        pnd_Format_Addr_Pnd_In_Zip.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Zip_Code());                                                                     //Natural: ASSIGN #IN-ZIP := #MDMA321.#O-FUTR-ADDRESS-ZIP-CODE
        pnd_Format_Addr_Pnd_In_Country.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Country());                                                                  //Natural: ASSIGN #IN-COUNTRY := #MDMA321.#O-FUTR-ADDRESS-COUNTRY
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
        sub_Reformat_Address();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_1.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(1));                                                                  //Natural: ASSIGN #O-C-ADDRESS-1 := #OUT-ADDR ( 1 )
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_2.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(2));                                                                  //Natural: ASSIGN #O-C-ADDRESS-2 := #OUT-ADDR ( 2 )
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_3.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(3));                                                                  //Natural: ASSIGN #O-C-ADDRESS-3 := #OUT-ADDR ( 3 )
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_4.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(4));                                                                  //Natural: ASSIGN #O-C-ADDRESS-4 := #OUT-ADDR ( 4 )
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_5.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(5));                                                                  //Natural: ASSIGN #O-C-ADDRESS-5 := #OUT-ADDR ( 5 )
        //*  MDM-ADDRESS-FUTURE
    }
    private void sub_Mdm_Address_Temporary() throws Exception                                                                                                             //Natural: MDM-ADDRESS-TEMPORARY
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE MDM-ADDRESS-TEMPORARY");                                                                               //Natural: WRITE 'SUBROUTINE MDM-ADDRESS-TEMPORARY'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  FROM CONTRACT
        short decideConditionsMet1642 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MDMA321.#O-TEMP-ADDRESS-STATUS-CODE = 'B'
        if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Status_Code().equals("B")))
        {
            decideConditionsMet1642++;
            pnd_Err_Msg.setValue("Temporary Adress Marked BAD.");                                                                                                         //Natural: ASSIGN #ERR-MSG := 'Temporary Adress Marked BAD.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#O-TEMP-ADDRESS-STATUS-CODE = 'R'
        else if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Status_Code().equals("R")))
        {
            decideConditionsMet1642++;
            pnd_Err_Msg.setValue("Temporary Address Marked IN-RESEARCH.");                                                                                                //Natural: ASSIGN #ERR-MSG := 'Temporary Address Marked IN-RESEARCH.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#O-CONTRACT-STATUS-CODE > ' '
        else if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Contract_Status_Code().greater(" ")))
        {
            decideConditionsMet1642++;
            pnd_Err_Msg.setValue("Temporary Address Marked TERMINATED.");                                                                                                 //Natural: ASSIGN #ERR-MSG := 'Temporary Address Marked TERMINATED.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#O-TEMP-ADDRESS-LINE-1 = ' '
        else if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Line_1().equals(" ")))
        {
            decideConditionsMet1642++;
            pnd_Err_Msg.setValue("Temporary Address Line 1 blank.");                                                                                                      //Natural: ASSIGN #ERR-MSG := 'Temporary Address Line 1 blank.'
        }                                                                                                                                                                 //Natural: WHEN #MDMA321.#O-TEMP-ADDRESS-LINE-2 = ' '
        else if (condition(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Line_2().equals(" ")))
        {
            decideConditionsMet1642++;
            pnd_Err_Msg.setValue("Temporary Address Line 2 blank.");                                                                                                      //Natural: ASSIGN #ERR-MSG := 'Temporary Address Line 2 blank.'
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1642 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_L_Acct_No.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Bank_Account_Number());                                                                               //Natural: ASSIGN #L-ACCT-NO := #MDMA321.#O-TEMP-BANK-ACCOUNT-NUMBER
        pnd_L_Eft_No.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Bank_Aba_Eft_Number());                                                                                //Natural: ASSIGN #L-EFT-NO := #MDMA321.#O-TEMP-BANK-ABA-EFT-NUMBER
                                                                                                                                                                          //Natural: PERFORM DETERMINE-ADDR-FORMAT-CODE
        sub_Determine_Addr_Format_Code();
        if (condition(Global.isEscape())) {return;}
        pnd_Addr_New.setValue(true);                                                                                                                                      //Natural: ASSIGN #ADDR-NEW := TRUE
        pnd_Work_Letter_Image_File_Pnd_O_C_Contract_Type_Cd.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Line_Of_Business());                                                 //Natural: ASSIGN #O-C-CONTRACT-TYPE-CD := #MDMA321.#O-LINE-OF-BUSINESS
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Type.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Type_Code());                                                  //Natural: ASSIGN #O-C-ADDR-TYPE := #MDMA321.#O-TEMP-ADDRESS-TYPE-CODE
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd.setValue(pnd_L_Format_Cd);                                                                                      //Natural: ASSIGN #O-C-ADDR-FORMAT-CD := #L-FORMAT-CD
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Last_Update_Date());                                     //Natural: ASSIGN #O-C-ADDR-LAST-CH-DT := #MDMA321.#O-TEMP-ADDRESS-LAST-UPDATE-DATE
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Last_Update_Time());                                     //Natural: ASSIGN #O-C-ADDR-LAST-CH-TM := #MDMA321.#O-TEMP-ADDRESS-LAST-UPDATE-TIME
        pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Postal_Data());                                              //Natural: ASSIGN #O-C-POSTAL-DATA := #MDMA321.#O-TEMP-ADDRESS-POSTAL-DATA
        pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Bank_Account_Number());                                               //Natural: ASSIGN #O-C-PH-BANK-NO := #MDMA321.#O-TEMP-BANK-ACCOUNT-NUMBER
        pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Bank_Aba_Eft_Number());                                           //Natural: ASSIGN #O-C-ABA-ACCOUNT-NO := #MDMA321.#O-TEMP-BANK-ABA-EFT-NUMBER
        pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Check_Saving_Ind());                                             //Natural: ASSIGN #O-C-CHECK-SAVING-CD := #MDMA321.#O-TEMP-CHECK-SAVING-IND
        pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Annual_Ind());                                      //Natural: ASSIGN #O-C-ANNUAL-VAC-CYCLE-IND := #MDMA321.#O-TEMP-ADDRESS-ANNUAL-IND
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Start_Date());                                        //Natural: ASSIGN #O-C-PENDING-ADDR-CH-DT := #MDMA321.#O-TEMP-ADDRESS-START-DATE
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_End_Date());                                          //Natural: ASSIGN #O-C-PENDING-ADDR-RS-DT := #MDMA321.#O-TEMP-ADDRESS-END-DATE
        pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Futr_Address_Start_Date());                                        //Natural: ASSIGN #O-C-PERM-ADDRESS-CH-DT := #MDMA321.#O-FUTR-ADDRESS-START-DATE
        pnd_Format_Addr.reset();                                                                                                                                          //Natural: RESET #FORMAT-ADDR
        pnd_Format_Addr_Pnd_In_Addr_Type.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Type_Code());                                                              //Natural: ASSIGN #IN-ADDR-TYPE := #MDMA321.#O-TEMP-ADDRESS-TYPE-CODE
        pnd_Format_Addr_Pnd_In_Addr1.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Line_1());                                                                     //Natural: ASSIGN #IN-ADDR1 := #MDMA321.#O-TEMP-ADDRESS-LINE-1
        pnd_Format_Addr_Pnd_In_Addr2.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Line_2());                                                                     //Natural: ASSIGN #IN-ADDR2 := #MDMA321.#O-TEMP-ADDRESS-LINE-2
        pnd_Format_Addr_Pnd_In_Addr3.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Line_3());                                                                     //Natural: ASSIGN #IN-ADDR3 := #MDMA321.#O-TEMP-ADDRESS-LINE-3
        pnd_Format_Addr_Pnd_In_Addr4.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Line_4());                                                                     //Natural: ASSIGN #IN-ADDR4 := #MDMA321.#O-TEMP-ADDRESS-LINE-4
        pnd_Format_Addr_Pnd_In_City.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_City());                                                                        //Natural: ASSIGN #IN-CITY := #MDMA321.#O-TEMP-ADDRESS-CITY
        pnd_Format_Addr_Pnd_In_St_Prov.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_St_Prov());                                                                  //Natural: ASSIGN #IN-ST-PROV := #MDMA321.#O-TEMP-ADDRESS-ST-PROV
        pnd_Format_Addr_Pnd_In_Zip.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Zip_Code());                                                                     //Natural: ASSIGN #IN-ZIP := #MDMA321.#O-TEMP-ADDRESS-ZIP-CODE
        pnd_Format_Addr_Pnd_In_Country.setValue(pdaMdma321.getPnd_Mdma321_Pnd_O_Temp_Address_Country());                                                                  //Natural: ASSIGN #IN-COUNTRY := #MDMA321.#O-TEMP-ADDRESS-COUNTRY
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
        sub_Reformat_Address();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_1.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(1));                                                                  //Natural: ASSIGN #O-C-ADDRESS-1 := #OUT-ADDR ( 1 )
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_2.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(2));                                                                  //Natural: ASSIGN #O-C-ADDRESS-2 := #OUT-ADDR ( 2 )
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_3.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(3));                                                                  //Natural: ASSIGN #O-C-ADDRESS-3 := #OUT-ADDR ( 3 )
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_4.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(4));                                                                  //Natural: ASSIGN #O-C-ADDRESS-4 := #OUT-ADDR ( 4 )
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_5.setValue(pnd_Format_Addr_Pnd_Out_Addr.getValue(5));                                                                  //Natural: ASSIGN #O-C-ADDRESS-5 := #OUT-ADDR ( 5 )
        //*  MDM-ADDRESS-TEMPORARY
    }
    private void sub_Print_Error() throws Exception                                                                                                                       //Natural: PRINT-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Err_Letter.setValue(true);                                                                                                                                    //Natural: ASSIGN #ERR-LETTER := TRUE
        pnd_Err_Date.resetInitial();                                                                                                                                      //Natural: RESET INITIAL #ERR-DATE #ERR-TIME #ADDR-NEW #ADDR-MAILTO
        pnd_Err_Time.resetInitial();
        pnd_Addr_New.resetInitial();
        pnd_Addr_Mailto.resetInitial();
        short decideConditionsMet1699 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN C-ORIG-CHNGE-DTE-A = MASK ( YYYYMMDD )
        if (condition(DbsUtil.maskMatches(confirmation_File_C_Orig_Chnge_Dte_A,"YYYYMMDD")))
        {
            decideConditionsMet1699++;
            pnd_Datx.setValueEdited(new ReportEditMask("YYYYMMDD"),confirmation_File_C_Orig_Chnge_Dte_A);                                                                 //Natural: MOVE EDITED C-ORIG-CHNGE-DTE-A TO #DATX ( EM = YYYYMMDD )
            pnd_Err_Date.setValueEdited(pnd_Datx,new ReportEditMask("MM/DD/YYYY"));                                                                                       //Natural: MOVE EDITED #DATX ( EM = MM/DD/YYYY ) TO #ERR-DATE
        }                                                                                                                                                                 //Natural: WHEN C-ORIGINAL-CHNGE-TME-A = MASK ( 9999999 )
        if (condition(DbsUtil.maskMatches(confirmation_File_C_Original_Chnge_Tme_A,"9999999")))
        {
            decideConditionsMet1699++;
            pnd_L_Time.setValueEdited(new ReportEditMask("HHIISST"),confirmation_File_C_Original_Chnge_Tme_A);                                                            //Natural: MOVE EDITED C-ORIGINAL-CHNGE-TME-A TO #L-TIME ( EM = HHIISST )
            pnd_Err_Time.setValueEdited(pnd_L_Time,new ReportEditMask("HH:II:SS:T"));                                                                                     //Natural: MOVE EDITED #L-TIME ( EM = HH:II:SS:T ) TO #ERR-TIME
        }                                                                                                                                                                 //Natural: WHEN C-ACTVTY-CDE = 'A'
        if (condition(confirmation_File_C_Actvty_Cde.equals("A")))
        {
            decideConditionsMet1699++;
            pnd_Err_Tran.setValue("Add");                                                                                                                                 //Natural: ASSIGN #ERR-TRAN := 'Add'
        }                                                                                                                                                                 //Natural: WHEN C-ACTVTY-CDE = 'C'
        if (condition(confirmation_File_C_Actvty_Cde.equals("C")))
        {
            decideConditionsMet1699++;
            pnd_Err_Tran.setValue("Chg");                                                                                                                                 //Natural: ASSIGN #ERR-TRAN := 'Chg'
        }                                                                                                                                                                 //Natural: WHEN C-ADDR-ACTVTY-CDE = 'AP' OR = 'IP'
        if (condition(confirmation_File_C_Addr_Actvty_Cde.equals("AP") || confirmation_File_C_Addr_Actvty_Cde.equals("IP")))
        {
            decideConditionsMet1699++;
            pnd_Err_Addr_Type.setValue("Permanent");                                                                                                                      //Natural: ASSIGN #ERR-ADDR-TYPE := 'Permanent'
        }                                                                                                                                                                 //Natural: WHEN C-ADDR-ACTVTY-CDE = 'AV' OR = 'IV'
        if (condition(confirmation_File_C_Addr_Actvty_Cde.equals("AV") || confirmation_File_C_Addr_Actvty_Cde.equals("IV")))
        {
            decideConditionsMet1699++;
            pnd_Err_Addr_Type.setValue("Vacation");                                                                                                                       //Natural: ASSIGN #ERR-ADDR-TYPE := 'Vacation'
        }                                                                                                                                                                 //Natural: WHEN C-ADDR-ACTVTY-CDE = 'F'
        if (condition(confirmation_File_C_Addr_Actvty_Cde.equals("F")))
        {
            decideConditionsMet1699++;
            pnd_Err_Addr_Type.setValue("Future");                                                                                                                         //Natural: ASSIGN #ERR-ADDR-TYPE := 'Future'
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1699 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Total_Error.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #TOTAL-ERROR
        getReports().display(1, ReportOption.NOTITLE,"PIN",                                                                                                               //Natural: DISPLAY ( 1 ) NOTITLE 'PIN' C-PH-UNIQUE-ID-NMBR 'Contract.' C-CNTRCT-NMBR 'Py' C-CNTRCT-PAYEE-CDE ( EM = 99 ) 'Usg/Typ' C-USAGE-TYPE 'Activity/Code' #ERR-ADDR-TYPE 'Tran/Type' #ERR-TRAN 'Last Chg/Date' #ERR-DATE 'Last Chg/Time' #ERR-TIME / 'Remarks' #ERR-MSG
        		confirmation_File_C_Ph_Unique_Id_Nmbr,"Contract.",
        		confirmation_File_C_Cntrct_Nmbr,"Py",
        		confirmation_File_C_Cntrct_Payee_Cde, new ReportEditMask ("99"),"Usg/Typ",
        		confirmation_File_C_Usage_Type,"Activity/Code",
        		pnd_Err_Addr_Type,"Tran/Type",
        		pnd_Err_Tran,"Last Chg/Date",
        		pnd_Err_Date,"Last Chg/Time",
        		pnd_Err_Time,NEWLINE,"Remarks",
        		pnd_Err_Msg);
        if (Global.isEscape()) return;
        //*  PRINT-ERROR
    }
    private void sub_Reformat_Address() throws Exception                                                                                                                  //Natural: REFORMAT-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE REFORMAT-ADDRESS");                                                                                    //Natural: WRITE 'SUBROUTINE REFORMAT-ADDRESS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  WRITE '=' #IN-ADDR1 '=' #IN-CITY
        //*  WRITE '=' #IN-ADDR2 '=' #IN-ST-PROV '=' #IN-ADDR-TYPE
        //*  WRITE '=' #IN-ADDR3 '=' #IN-ZIP
        //*  WRITE '=' #IN-ADDR4 '=' #IN-COUNTRY
        pnd_L.reset();                                                                                                                                                    //Natural: RESET #L
        short decideConditionsMet1736 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #IN-ADDR1 NE ' '
        if (condition(pnd_Format_Addr_Pnd_In_Addr1.notEquals(" ")))
        {
            decideConditionsMet1736++;
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L).setValue(pnd_Format_Addr_Pnd_In_Addr1, MoveOption.LeftJustified);                                                //Natural: MOVE LEFT #IN-ADDR1 TO #OUT-ADDR ( #L )
        }                                                                                                                                                                 //Natural: WHEN #IN-ADDR2 NE ' '
        if (condition(pnd_Format_Addr_Pnd_In_Addr2.notEquals(" ")))
        {
            decideConditionsMet1736++;
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L).setValue(pnd_Format_Addr_Pnd_In_Addr2, MoveOption.LeftJustified);                                                //Natural: MOVE LEFT #IN-ADDR2 TO #OUT-ADDR ( #L )
        }                                                                                                                                                                 //Natural: WHEN #IN-ADDR3 NE ' '
        if (condition(pnd_Format_Addr_Pnd_In_Addr3.notEquals(" ")))
        {
            decideConditionsMet1736++;
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L).setValue(pnd_Format_Addr_Pnd_In_Addr3, MoveOption.LeftJustified);                                                //Natural: MOVE LEFT #IN-ADDR3 TO #OUT-ADDR ( #L )
        }                                                                                                                                                                 //Natural: WHEN #IN-ADDR4 NE ' '
        if (condition(pnd_Format_Addr_Pnd_In_Addr4.notEquals(" ")))
        {
            decideConditionsMet1736++;
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L).setValue(pnd_Format_Addr_Pnd_In_Addr4, MoveOption.LeftJustified);                                                //Natural: MOVE LEFT #IN-ADDR4 TO #OUT-ADDR ( #L )
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1736 > 0))
        {
            if (condition(pnd_L.greater(1)))                                                                                                                              //Natural: IF #L > 1
            {
                if (condition(pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L).equals(pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L.getDec().subtract(1)))))                    //Natural: IF #OUT-ADDR ( #L ) = #OUT-ADDR ( #L - 1 )
                {
                    pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L).reset();                                                                                                 //Natural: RESET #OUT-ADDR ( #L )
                    pnd_L.nsubtract(1);                                                                                                                                   //Natural: SUBTRACT 1 FROM #L
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1736 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        DbsUtil.examine(new ExamineSource(pnd_Format_Addr_Pnd_Out_Addr.getValue("*")), new ExamineSearch("  "), new ExamineReplace(" "));                                 //Natural: EXAMINE #OUT-ADDR ( * ) '  ' REPLACE ' '
        pnd_Format_Addr_Pnd_City_State_Zip.reset();                                                                                                                       //Natural: RESET #CITY-STATE-ZIP
        //*  US OR DOMESTIC ADDRESS
        if (condition(pnd_Format_Addr_Pnd_In_Addr_Type.equals("U") || DbsUtil.maskMatches(pnd_Format_Addr_Pnd_In_Zip,"99999")))                                           //Natural: IF #IN-ADDR-TYPE = 'U' OR #IN-ZIP = MASK ( 99999 )
        {
            if (condition(pnd_Format_Addr_Pnd_In_City.notEquals("N/A")))                                                                                                  //Natural: IF #IN-CITY NE 'N/A'
            {
                pnd_Format_Addr_Pnd_City_State_Zip.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Format_Addr_Pnd_In_City, ","));                           //Natural: COMPRESS #IN-CITY ',' INTO #CITY-STATE-ZIP LEAVING NO
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Format_Addr_Pnd_In_St_Prov.equals(" ") || pnd_Format_Addr_Pnd_In_St_Prov.equals("UN"))))                                                 //Natural: IF NOT ( #IN-ST-PROV = ' ' OR = 'UN' )
            {
                pnd_Format_Addr_Pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Format_Addr_Pnd_City_State_Zip, pnd_Format_Addr_Pnd_In_St_Prov, pnd_Format_Addr_Pnd_In_Zip)); //Natural: COMPRESS #CITY-STATE-ZIP #IN-ST-PROV #IN-ZIP INTO #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L)), new ExamineSearch(pnd_Format_Addr_Pnd_In_City), new ExamineGivingNumber(pnd_Format_Addr_Pnd_C)); //Natural: EXAMINE #OUT-ADDR ( #L ) #IN-CITY GIVING NUMBER #C
            DbsUtil.examine(new ExamineSource(pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L)), new ExamineSearch(pnd_Format_Addr_Pnd_In_St_Prov), new ExamineGivingNumber(pnd_Format_Addr_Pnd_S)); //Natural: EXAMINE #OUT-ADDR ( #L ) #IN-ST-PROV GIVING NUMBER #S
            DbsUtil.examine(new ExamineSource(pnd_Format_Addr_Pnd_City_State_Zip), new ExamineSearch("-0000"), new ExamineDelete());                                      //Natural: EXAMINE #CITY-STATE-ZIP '-0000' DELETE
            if (condition(pnd_Format_Addr_Pnd_City_State_Zip.notEquals(" ")))                                                                                             //Natural: IF #CITY-STATE-ZIP NE ' '
            {
                if (condition(pnd_Format_Addr_Pnd_C.greater(getZero()) && pnd_Format_Addr_Pnd_S.greater(getZero())))                                                      //Natural: IF #C > 0 AND #S > 0
                {
                    pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L).reset();                                                                                                 //Natural: RESET #OUT-ADDR ( #L )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_L.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #L
                }                                                                                                                                                         //Natural: END-IF
                pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L).setValue(pnd_Format_Addr_Pnd_City_State_Zip);                                                                //Natural: ASSIGN #OUT-ADDR ( #L ) := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  NON-USA ADDR
        if (condition(pnd_Format_Addr_Pnd_In_Addr_Type.equals("C") || pnd_Format_Addr_Pnd_In_Addr_Type.equals("F") || pnd_Format_Addr_Pnd_In_Zip.equals("CANAD")          //Natural: IF #IN-ADDR-TYPE = 'C' OR = 'F' OR #IN-ZIP = 'CANAD' OR = 'FORGN'
            || pnd_Format_Addr_Pnd_In_Zip.equals("FORGN")))
        {
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            if (condition(pnd_L.lessOrEqual(5) && pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L).equals(" ") && pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L.getDec().subtract(1)).notEquals(pnd_Format_Addr_Pnd_In_Country))) //Natural: IF #L <= 5 AND #OUT-ADDR ( #L ) = ' ' AND #OUT-ADDR ( #L - 1 ) NE #IN-COUNTRY
            {
                pnd_Format_Addr_Pnd_Out_Addr.getValue(pnd_L).setValue(pnd_Format_Addr_Pnd_In_Country, MoveOption.LeftJustified);                                          //Natural: MOVE LEFT #IN-COUNTRY TO #OUT-ADDR ( #L )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '1' #OUT-ADDR (1) '4' #OUT-ADDR (4)
        //*  WRITE '2' #OUT-ADDR (2) '5' #OUT-ADDR (5)
        //*  WRITE '3' #OUT-ADDR (3)
        //*  REFORMAT-ADDRESS
    }
    private void sub_Reformat_Name() throws Exception                                                                                                                     //Natural: REFORMAT-NAME
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE REFORMAT-NAME");                                                                                       //Natural: WRITE 'SUBROUTINE REFORMAT-NAME'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        pnd_Format_Name_Pnd_Out_Pref_Name.setValue(DbsUtil.compress(pnd_Format_Name_Pnd_In_Fname, pnd_Format_Name_Pnd_In_Mname, pnd_Format_Name_Pnd_In_Lname));           //Natural: COMPRESS #IN-FNAME #IN-MNAME #IN-LNAME INTO #OUT-PREF-NAME
        if (condition(pnd_Format_Name_Pnd_Out_Name_36_70.notEquals(" ")))                                                                                                 //Natural: IF #OUT-NAME-36-70 NE ' '
        {
            pnd_Format_Name_Pnd_Out_Pref_Name.setValue(DbsUtil.compress(pnd_Format_Name_Pnd_In_Fname, pnd_Format_Name_Pnd_In_Mname_1, pnd_Format_Name_Pnd_In_Lname));     //Natural: COMPRESS #IN-FNAME #IN-MNAME-1 #IN-LNAME INTO #OUT-PREF-NAME
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1807 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #IN-PRFX;//Natural: VALUE 'DR', 'DR.'
        if (condition((pnd_Format_Name_Pnd_In_Prfx.equals("DR") || pnd_Format_Name_Pnd_In_Prfx.equals("DR."))))
        {
            decideConditionsMet1807++;
            pnd_Format_Name_Pnd_Out_Name.setValue(DbsUtil.compress("DR", pnd_Format_Name_Pnd_Out_Name));                                                                  //Natural: COMPRESS 'DR' #OUT-NAME INTO #OUT-NAME
        }                                                                                                                                                                 //Natural: VALUE 'PROF', 'PROF.', 'PRF', 'PRF.'
        else if (condition((pnd_Format_Name_Pnd_In_Prfx.equals("PROF") || pnd_Format_Name_Pnd_In_Prfx.equals("PROF.") || pnd_Format_Name_Pnd_In_Prfx.equals("PRF") 
            || pnd_Format_Name_Pnd_In_Prfx.equals("PRF."))))
        {
            decideConditionsMet1807++;
            pnd_Format_Name_Pnd_Out_Name.setValue(DbsUtil.compress("PROF", pnd_Format_Name_Pnd_Out_Name));                                                                //Natural: COMPRESS 'PROF' #OUT-NAME INTO #OUT-NAME
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Format_Name_Pnd_In_Sffx.equals("JR") || pnd_Format_Name_Pnd_In_Sffx.equals("SR") || pnd_Format_Name_Pnd_In_Sffx.equals("II")                    //Natural: IF #IN-SFFX = 'JR' OR = 'SR' OR = 'II' OR = 'III' OR = 'IV' OR = 'V' OR = 'VI' OR = 'VII' OR = 'VIII' OR = 'IX' OR = 'X'
            || pnd_Format_Name_Pnd_In_Sffx.equals("III") || pnd_Format_Name_Pnd_In_Sffx.equals("IV") || pnd_Format_Name_Pnd_In_Sffx.equals("V") || pnd_Format_Name_Pnd_In_Sffx.equals("VI") 
            || pnd_Format_Name_Pnd_In_Sffx.equals("VII") || pnd_Format_Name_Pnd_In_Sffx.equals("VIII") || pnd_Format_Name_Pnd_In_Sffx.equals("IX") || pnd_Format_Name_Pnd_In_Sffx.equals("X")))
        {
            pnd_Format_Name_Pnd_Out_Name.setValue(DbsUtil.compress(pnd_Format_Name_Pnd_Out_Name, pnd_Format_Name_Pnd_In_Sffx));                                           //Natural: COMPRESS #OUT-NAME #IN-SFFX INTO #OUT-NAME
        }                                                                                                                                                                 //Natural: END-IF
        //*  REFORMAT-NAME
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE WRITE-WORK-FILE");                                                                                     //Natural: WRITE 'SUBROUTINE WRITE-WORK-FILE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Datx.compute(new ComputeParameters(false, pnd_Datx), Global.getDATX().subtract(1));                                                                           //Natural: ASSIGN #DATX := *DATX - 1
        pnd_Comp_Date_A8.setValueEdited(pnd_Datx,new ReportEditMask("YYYYMMDD"));                                                                                         //Natural: MOVE EDITED #DATX ( EM = YYYYMMDD ) TO #COMP-DATE-A8
        short decideConditionsMet1827 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN C-USAGE-TYPE = 'CO' AND #O-M-ADDRESS-1 = #O-C-ADDRESS-1 AND #O-M-ADDRESS-2 = #O-C-ADDRESS-2 AND #O-M-ADDRESS-3 = #O-C-ADDRESS-3 AND #O-M-ADDRESS-4 = #O-C-ADDRESS-4 AND #O-M-ADDRESS-5 = #O-C-ADDRESS-5 AND #O-M-POSTAL-DATA = #O-C-POSTAL-DATA AND NOT #PREVIOUS-BAD
        if (condition(confirmation_File_C_Usage_Type.equals("CO") && pnd_Work_Letter_Image_File_Pnd_O_M_Address_1.equals(pnd_Work_Letter_Image_File_Pnd_O_C_Address_1) 
            && pnd_Work_Letter_Image_File_Pnd_O_M_Address_2.equals(pnd_Work_Letter_Image_File_Pnd_O_C_Address_2) && pnd_Work_Letter_Image_File_Pnd_O_M_Address_3.equals(pnd_Work_Letter_Image_File_Pnd_O_C_Address_3) 
            && pnd_Work_Letter_Image_File_Pnd_O_M_Address_4.equals(pnd_Work_Letter_Image_File_Pnd_O_C_Address_4) && pnd_Work_Letter_Image_File_Pnd_O_M_Address_5.equals(pnd_Work_Letter_Image_File_Pnd_O_C_Address_5) 
            && pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data.equals(pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data) && ! (pnd_Previous_Bad.getBoolean())))
        {
            decideConditionsMet1827++;
            pnd_Err_Msg.setValue("Same Current & Previous Correspondence");                                                                                               //Natural: ASSIGN #ERR-MSG := 'Same Current & Previous Correspondence'
        }                                                                                                                                                                 //Natural: WHEN #O-C-PENDING-ADDR-CH-DT > 0 AND #O-C-PENDING-ADDR-CH-DT < #COMP-DATE AND C-ADDR-ACTVTY-CDE = 'AV' OR = 'IV'
        else if (condition(((pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt.greater(getZero()) && pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt.less(pnd_Comp_Date_A8_Pnd_Comp_Date)) 
            && (confirmation_File_C_Addr_Actvty_Cde.equals("AV") || confirmation_File_C_Addr_Actvty_Cde.equals("IV")))))
        {
            decideConditionsMet1827++;
            pnd_Err_Msg.setValue(DbsUtil.compress("Temp Start Date", pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt, "< Date", pnd_Comp_Date_A8_Pnd_Comp_Date));   //Natural: COMPRESS 'Temp Start Date' #O-C-PENDING-ADDR-CH-DT '< Date' #COMP-DATE INTO #ERR-MSG
        }                                                                                                                                                                 //Natural: WHEN #O-C-PENDING-ADDR-RS-DT > 0 AND #O-C-PENDING-ADDR-RS-DT < #COMP-DATE AND C-ADDR-ACTVTY-CDE = 'AV' OR = 'IV'
        else if (condition(((pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt.greater(getZero()) && pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt.less(pnd_Comp_Date_A8_Pnd_Comp_Date)) 
            && (confirmation_File_C_Addr_Actvty_Cde.equals("AV") || confirmation_File_C_Addr_Actvty_Cde.equals("IV")))))
        {
            decideConditionsMet1827++;
            pnd_Err_Msg.setValue(DbsUtil.compress("Temp End Date", pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt, "< Date", pnd_Comp_Date_A8_Pnd_Comp_Date));     //Natural: COMPRESS 'Temp End Date' #O-C-PENDING-ADDR-RS-DT '< Date' #COMP-DATE INTO #ERR-MSG
        }                                                                                                                                                                 //Natural: WHEN #O-C-PERM-ADDRESS-CH-DT > 0 AND #O-C-PERM-ADDRESS-CH-DT < #COMP-DATE AND C-ADDR-ACTVTY-CDE = 'F'
        else if (condition(pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt.greater(getZero()) && pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt.less(pnd_Comp_Date_A8_Pnd_Comp_Date) 
            && confirmation_File_C_Addr_Actvty_Cde.equals("F")))
        {
            decideConditionsMet1827++;
            pnd_Err_Msg.setValue(DbsUtil.compress("Future Start Date", pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt, "< Date", pnd_Comp_Date_A8_Pnd_Comp_Date)); //Natural: COMPRESS 'Future Start Date' #O-C-PERM-ADDRESS-CH-DT '< Date' #COMP-DATE INTO #ERR-MSG
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1827 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
            //*  PINE
            //*  KCD ITD  CONTRACT
            //*  KCD CONTAINS DA CONTRACT
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Work_Letter_Image_File_Pnd_O_Pin_No.compute(new ComputeParameters(false, pnd_Work_Letter_Image_File_Pnd_O_Pin_No), confirmation_File_C_Ph_Unique_Id_Nmbr.val()); //Natural: ASSIGN #O-PIN-NO := VAL ( C-PH-UNIQUE-ID-NMBR )
        pnd_Work_Letter_Image_File_Pnd_O_Contract_No.setValue(confirmation_File_C_Cntrct_Nmbr);                                                                           //Natural: ASSIGN #O-CONTRACT-NO := C-CNTRCT-NMBR
        pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd.setValue(confirmation_File_C_Cntrct_Payee_Cde);                                                                         //Natural: ASSIGN #O-PAYEE-CD := C-CNTRCT-PAYEE-CDE
        pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd.setValue(confirmation_File_C_Addr_Actvty_Cde);                                                                    //Natural: ASSIGN #O-ADDR-ACTVTY-CD := C-ADDR-ACTVTY-CDE
        pnd_Work_Letter_Image_File_Pnd_O_Actvty_Cd.setValue(confirmation_File_C_Actvty_Cde);                                                                              //Natural: ASSIGN #O-ACTVTY-CD := C-ACTVTY-CDE
        pnd_Work_Letter_Image_File_Pnd_O_Usage.setValue(confirmation_File_C_Usage_Type);                                                                                  //Natural: ASSIGN #O-USAGE := C-USAGE-TYPE
        pnd_Work_Letter_Image_File_Pnd_O_Change_Dt.setValue(confirmation_File_C_Original_Chnge_Dte);                                                                      //Natural: ASSIGN #O-CHANGE-DT := C-ORIGINAL-CHNGE-DTE
        pnd_Work_Letter_Image_File_Pnd_O_Change_Tm.setValue(confirmation_File_C_Original_Chnge_Tme);                                                                      //Natural: ASSIGN #O-CHANGE-TM := C-ORIGINAL-CHNGE-TME
        pnd_Work_Letter_Image_File_Pnd_O_Verify_Ind.setValue(confirmation_File_C_Verify_Ind);                                                                             //Natural: ASSIGN #O-VERIFY-IND := C-VERIFY-IND
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid.setValue(confirmation_File_C_Mit_Wpid);                                                                               //Natural: ASSIGN #O-C-MIT-WPID := C-MIT-WPID
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde.setValue(confirmation_File_C_Mit_Unit_Cde);                                                                       //Natural: ASSIGN #O-C-MIT-UNIT-CDE := C-MIT-UNIT-CDE
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time.setValue(confirmation_File_C_Mit_Log_Date_Time);                                                             //Natural: ASSIGN #O-C-MIT-LOG-DATE-TIME := C-MIT-LOG-DATE-TIME
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Status_Cde.setValue(confirmation_File_C_Mit_Status_Cde);                                                                   //Natural: ASSIGN #O-C-MIT-STATUS-CDE := C-MIT-STATUS-CDE
        pnd_Work_Letter_Image_File_Pnd_O_Contract2.setValue(confirmation_File_C_Contract_2);                                                                              //Natural: ASSIGN #O-CONTRACT2 := C-CONTRACT-2
        short decideConditionsMet1860 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN C-ADDR-ACTVTY-CDE = 'AP' OR = 'IP'
        if (condition(confirmation_File_C_Addr_Actvty_Cde.equals("AP") || confirmation_File_C_Addr_Actvty_Cde.equals("IP")))
        {
            decideConditionsMet1860++;
            //*  CORRESPONDENCE
            //*  CHECK-MAILING
            //*  RESIDENTIAL
            short decideConditionsMet1865 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF C-USAGE-TYPE;//Natural: VALUE 'CO'
            if (condition((confirmation_File_C_Usage_Type.equals("CO"))))
            {
                decideConditionsMet1865++;
                pnd_Work_Letter_Image_File_Pnd_O_C_Section.setValue("1");                                                                                                 //Natural: ASSIGN #O-C-SECTION := '1'
            }                                                                                                                                                             //Natural: VALUE 'CM'
            else if (condition((confirmation_File_C_Usage_Type.equals("CM"))))
            {
                decideConditionsMet1865++;
                pnd_Work_Letter_Image_File_Pnd_O_C_Section.setValue("2");                                                                                                 //Natural: ASSIGN #O-C-SECTION := '2'
            }                                                                                                                                                             //Natural: VALUE 'RS'
            else if (condition((confirmation_File_C_Usage_Type.equals("RS"))))
            {
                decideConditionsMet1865++;
                pnd_Work_Letter_Image_File_Pnd_O_C_Section.setValue("3");                                                                                                 //Natural: ASSIGN #O-C-SECTION := '3'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN C-ADDR-ACTVTY-CDE = 'F'
        else if (condition(confirmation_File_C_Addr_Actvty_Cde.equals("F")))
        {
            decideConditionsMet1860++;
            //*  CORRESPONDENCE
            //*  CHECK-MAILING
            //*  RESIDENTIAL
            short decideConditionsMet1879 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF C-USAGE-TYPE;//Natural: VALUE 'CO'
            if (condition((confirmation_File_C_Usage_Type.equals("CO"))))
            {
                decideConditionsMet1879++;
                pnd_Work_Letter_Image_File_Pnd_O_C_Section.setValue("4");                                                                                                 //Natural: ASSIGN #O-C-SECTION := '4'
            }                                                                                                                                                             //Natural: VALUE 'CM'
            else if (condition((confirmation_File_C_Usage_Type.equals("CM"))))
            {
                decideConditionsMet1879++;
                pnd_Work_Letter_Image_File_Pnd_O_C_Section.setValue("5");                                                                                                 //Natural: ASSIGN #O-C-SECTION := '5'
            }                                                                                                                                                             //Natural: VALUE 'RS'
            else if (condition((confirmation_File_C_Usage_Type.equals("RS"))))
            {
                decideConditionsMet1879++;
                pnd_Work_Letter_Image_File_Pnd_O_C_Section.setValue("6");                                                                                                 //Natural: ASSIGN #O-C-SECTION := '6'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN C-ADDR-ACTVTY-CDE = 'AV' OR = 'IV'
        else if (condition(confirmation_File_C_Addr_Actvty_Cde.equals("AV") || confirmation_File_C_Addr_Actvty_Cde.equals("IV")))
        {
            decideConditionsMet1860++;
            //*  CORRESPONDENCE
            if (condition(confirmation_File_C_Usage_Type.equals("CO")))                                                                                                   //Natural: IF C-USAGE-TYPE = 'CO'
            {
                pnd_Work_Letter_Image_File_Pnd_O_C_Section.setValue("7");                                                                                                 //Natural: ASSIGN #O-C-SECTION := '7'
                //*  CHECK-MAILING
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Letter_Image_File_Pnd_O_C_Section.setValue("8");                                                                                                 //Natural: ASSIGN #O-C-SECTION := '8'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*   WITH NEW CREF NUMBER.
        //*   RES ADDR IS PIN-LEVEL
        //*   SO DOESN't use cntrct.
        short decideConditionsMet1904 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #O-C-NAME = ' ' AND #O-M-NAME NE ' '
        if (condition(pnd_Work_Letter_Image_File_Pnd_O_C_Name.equals(" ") && pnd_Work_Letter_Image_File_Pnd_O_M_Name.notEquals(" ")))
        {
            decideConditionsMet1904++;
            pnd_Work_Letter_Image_File_Pnd_O_C_Name.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Name);                                                                    //Natural: ASSIGN #O-C-NAME := #O-M-NAME
        }                                                                                                                                                                 //Natural: WHEN C-CONTRACT-2 NE ' ' AND C-USAGE-TYPE NE 'RS'
        if (condition(confirmation_File_C_Contract_2.notEquals(" ") && confirmation_File_C_Usage_Type.notEquals("RS")))
        {
            decideConditionsMet1904++;
            pnd_Work_Letter_Image_File_Pnd_O_Contract_No.setValue(confirmation_File_C_Contract_2);                                                                        //Natural: ASSIGN #O-CONTRACT-NO := C-CONTRACT-2
        }                                                                                                                                                                 //Natural: WHEN #O-EFFECTIVE-DATE NE MASK ( YYYYMMDD )
        if (condition(! ((DbsUtil.maskMatches(pnd_Work_Letter_Image_File_Pnd_O_Effective_Date,"YYYYMMDD")))))
        {
            decideConditionsMet1904++;
            pnd_Work_Letter_Image_File_Pnd_O_Effective_Date_A.setValue("00000000");                                                                                       //Natural: ASSIGN #O-EFFECTIVE-DATE-A := '00000000'
        }                                                                                                                                                                 //Natural: WHEN #O-C-ADDR-LAST-CH-DT NE MASK ( YYYYMMDD )
        if (condition(! ((DbsUtil.maskMatches(pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt,"YYYYMMDD")))))
        {
            decideConditionsMet1904++;
            pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt_A.setValue("00000000");                                                                                    //Natural: ASSIGN #O-C-ADDR-LAST-CH-DT-A := '00000000'
        }                                                                                                                                                                 //Natural: WHEN #O-C-ADDR-LAST-CH-TM NE MASK ( 9999999 )
        if (condition(! ((DbsUtil.maskMatches(pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm,"9999999")))))
        {
            decideConditionsMet1904++;
            pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm_A.setValue("0000000");                                                                                     //Natural: ASSIGN #O-C-ADDR-LAST-CH-TM-A := '0000000'
        }                                                                                                                                                                 //Natural: WHEN #O-C-PENDING-ADDR-CH-DT NE MASK ( YYYYMMDD )
        if (condition(! ((DbsUtil.maskMatches(pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt,"YYYYMMDD")))))
        {
            decideConditionsMet1904++;
            pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt_A.setValue("00000000");                                                                                 //Natural: ASSIGN #O-C-PENDING-ADDR-CH-DT-A := '00000000'
        }                                                                                                                                                                 //Natural: WHEN #O-C-PENDING-ADDR-RS-DT NE MASK ( YYYYMMDD )
        if (condition(! ((DbsUtil.maskMatches(pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt,"YYYYMMDD")))))
        {
            decideConditionsMet1904++;
            pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt_A.setValue("00000000");                                                                                 //Natural: ASSIGN #O-C-PENDING-ADDR-RS-DT-A := '00000000'
        }                                                                                                                                                                 //Natural: WHEN #O-C-PERM-ADDRESS-CH-DT NE MASK ( YYYYMMDD )
        if (condition(! ((DbsUtil.maskMatches(pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt,"YYYYMMDD")))))
        {
            decideConditionsMet1904++;
            pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt_A.setValue("00000000");                                                                                 //Natural: ASSIGN #O-C-PERM-ADDRESS-CH-DT-A := '00000000'
        }                                                                                                                                                                 //Natural: WHEN ( C-USAGE-TYPE = 'CM' AND C-ADDR-ACTVTY-CDE = 'AP' OR = 'IP' ) OR ( C-USAGE-TYPE = 'CO' AND C-ADDR-ACTVTY-CDE = 'AP' OR = 'IP' ) OR ( C-USAGE-TYPE = 'CO' AND C-ADDR-ACTVTY-CDE = 'F' )
        if (condition((((confirmation_File_C_Usage_Type.equals("CM") && (confirmation_File_C_Addr_Actvty_Cde.equals("AP") || confirmation_File_C_Addr_Actvty_Cde.equals("IP"))) 
            || (confirmation_File_C_Usage_Type.equals("CO") && (confirmation_File_C_Addr_Actvty_Cde.equals("AP") || confirmation_File_C_Addr_Actvty_Cde.equals("IP")))) 
            || (confirmation_File_C_Usage_Type.equals("CO") && confirmation_File_C_Addr_Actvty_Cde.equals("F")))))
        {
            decideConditionsMet1904++;
            pnd_Save_Pin.setValue(confirmation_File_C_Ph_Unique_Id_Nmbr);                                                                                                 //Natural: ASSIGN #SAVE-PIN := C-PH-UNIQUE-ID-NMBR
        }                                                                                                                                                                 //Natural: WHEN ( C-USAGE-TYPE = 'CO' AND C-ADDR-ACTVTY-CDE = 'AV' OR = 'IV' ) OR ( C-USAGE-TYPE = 'CM' AND C-ADDR-ACTVTY-CDE = 'AV' OR = 'IV' ) OR ( C-USAGE-TYPE = 'CM' AND C-ADDR-ACTVTY-CDE = 'F' )
        if (condition((((confirmation_File_C_Usage_Type.equals("CO") && (confirmation_File_C_Addr_Actvty_Cde.equals("AV") || confirmation_File_C_Addr_Actvty_Cde.equals("IV"))) 
            || (confirmation_File_C_Usage_Type.equals("CM") && (confirmation_File_C_Addr_Actvty_Cde.equals("AV") || confirmation_File_C_Addr_Actvty_Cde.equals("IV")))) 
            || (confirmation_File_C_Usage_Type.equals("CM") && confirmation_File_C_Addr_Actvty_Cde.equals("F")))))
        {
            decideConditionsMet1904++;
            if (condition(pnd_Save_Pin.equals(confirmation_File_C_Ph_Unique_Id_Nmbr)))                                                                                    //Natural: IF #SAVE-PIN = C-PH-UNIQUE-ID-NMBR
            {
                pnd_Work_Letter_Image_File_Pnd_Mit_Error.setValue("Y");                                                                                                   //Natural: ASSIGN #MIT-ERROR := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN ( C-USAGE-TYPE = 'CO' OR = 'RS' AND C-ADDR-ACTVTY-CDE = 'AP' OR = 'IP' AND #O-R-ADDRESS-1 = ' ' )
        if (condition((((confirmation_File_C_Usage_Type.equals("CO") || confirmation_File_C_Usage_Type.equals("RS")) && (confirmation_File_C_Addr_Actvty_Cde.equals("AP") 
            || confirmation_File_C_Addr_Actvty_Cde.equals("IP"))) && pnd_Work_Letter_Image_File_Pnd_O_R_Address_1.equals(" "))))
        {
            decideConditionsMet1904++;
            pnd_Work_Letter_Image_File_Pnd_O_R_Address_1.setValue("RESIDENTIAL ADDRESS");                                                                                 //Natural: ASSIGN #O-R-ADDRESS-1 := 'RESIDENTIAL ADDRESS'
            pnd_Work_Letter_Image_File_Pnd_O_R_Address_2.setValue("NOT ON FILE");                                                                                         //Natural: ASSIGN #O-R-ADDRESS-2 := 'NOT ON FILE'
            pnd_Work_Letter_Image_File_Pnd_O_R_Address_3.reset();                                                                                                         //Natural: RESET #O-R-ADDRESS-3 #O-R-ADDRESS-4 #O-R-ADDRESS-5 #O-R-POSTAL-DATA
            pnd_Work_Letter_Image_File_Pnd_O_R_Address_4.reset();
            pnd_Work_Letter_Image_File_Pnd_O_R_Address_5.reset();
            pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data.reset();
        }                                                                                                                                                                 //Natural: WHEN ( C-USAGE-TYPE = 'CO' OR = 'RS' AND C-ADDR-ACTVTY-CDE = 'AP' OR = 'IP' AND #O-C-ADDRESS-1 = ' ' )
        if (condition((((confirmation_File_C_Usage_Type.equals("CO") || confirmation_File_C_Usage_Type.equals("RS")) && (confirmation_File_C_Addr_Actvty_Cde.equals("AP") 
            || confirmation_File_C_Addr_Actvty_Cde.equals("IP"))) && pnd_Work_Letter_Image_File_Pnd_O_C_Address_1.equals(" "))))
        {
            decideConditionsMet1904++;
            pnd_Work_Letter_Image_File_Pnd_O_C_Address_1.setValue("CORRESPONDENCE ADDRESS ON FILE");                                                                      //Natural: ASSIGN #O-C-ADDRESS-1 := 'CORRESPONDENCE ADDRESS ON FILE'
            pnd_Work_Letter_Image_File_Pnd_O_C_Address_3.reset();                                                                                                         //Natural: RESET #O-C-ADDRESS-3 #O-C-ADDRESS-4 #O-C-ADDRESS-5 #O-C-POSTAL-DATA
            pnd_Work_Letter_Image_File_Pnd_O_C_Address_4.reset();
            pnd_Work_Letter_Image_File_Pnd_O_C_Address_5.reset();
            pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data.reset();
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1904 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        DbsUtil.examine(new ExamineSource(pnd_Work_Letter_Image_File_Pnd_Work.getValue("*")), new ExamineTranslate(TranslateOption.Upper));                               //Natural: EXAMINE #WORK ( * ) TRANSLATE INTO UPPER CASE
        getReports().write(0, ReportOption.NOTITLE,"PIN",pnd_Work_Letter_Image_File_Pnd_O_Pin_No,"C#",pnd_Work_Letter_Image_File_Pnd_O_Contract_No,"Py",pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd,  //Natural: WRITE ( 0 ) NOTITLE 'PIN' #O-PIN-NO 'C#' #O-CONTRACT-NO 'Py' #O-PAYEE-CD ( EM = 99 ) 'Us' #O-USAGE 'AP' #O-ADDR-ACTVTY-CD 'MZ' #O-M-POSTAL-DATA ( EM = X ( 5 ) ) 'MA' #O-M-ADDRESS-1 ( EM = X ( 30 ) ) 'NZ' #O-C-POSTAL-DATA ( EM = X ( 5 ) ) 'NA' #O-C-ADDRESS-1 ( EM = X ( 30 ) ) 'RA' #O-R-ADDRESS-1 ( EM = X ( 25 ) )
            new ReportEditMask ("99"),"Us",pnd_Work_Letter_Image_File_Pnd_O_Usage,"AP",pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd,"MZ",pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data, 
            new ReportEditMask ("XXXXX"),"MA",pnd_Work_Letter_Image_File_Pnd_O_M_Address_1, new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),"NZ",pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data, 
            new ReportEditMask ("XXXXX"),"NA",pnd_Work_Letter_Image_File_Pnd_O_C_Address_1, new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),"RA",pnd_Work_Letter_Image_File_Pnd_O_R_Address_1, 
            new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXX"));
        if (Global.isEscape()) return;
        getWorkFiles().write(2, false, pnd_Work_Letter_Image_File);                                                                                                       //Natural: WRITE WORK FILE 2 #WORK-LETTER-IMAGE-FILE
        pnd_Total_Write.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #TOTAL-WRITE
        //*  WRITE-WORK-FILE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(54),"Name And Address System",new                     //Natural: WRITE ( 1 ) NOTITLE 001T *PROGRAM 054T 'Name And Address System' 120T 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 048T 'Confirmation Letter Error Validation' 120T *TIMX ( EM = HH:II:SS�AP ) /
                        TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(48),"Confirmation Letter Error Validation",new TabSetting(120),Global.getTIMX(), 
                        new ReportEditMask ("HH:II:SS AP"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean confirmation_File_C_Ph_Unique_Id_NmbrIsBreak = confirmation_File_C_Ph_Unique_Id_Nmbr.isBreak(endOfData);
        if (condition(confirmation_File_C_Ph_Unique_Id_NmbrIsBreak))
        {
            pnd_Work_Letter_Image_File_Pnd_Mit_Error.reset();                                                                                                             //Natural: RESET #MIT-ERROR #SAVE-PIN
            pnd_Save_Pin.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132 SG=OFF ZP=ON");
        Global.format(1, "PS=60 LS=132 SG=OFF ZP=ON");

        getReports().setDisplayColumns(0, ReportOption.NOTITLE,"PIN",
        		confirmation_File_C_Ph_Unique_Id_Nmbr,"L",
        		confirmation_File_C_Cntrct_Type,"Contract",
        		confirmation_File_C_Cntrct_Nmbr,"S",
        		confirmation_File_Section,"Py",
        		confirmation_File_C_Cntrct_Payee_Cde, new ReportEditMask ("99"),"Us",
        		confirmation_File_C_Usage_Type,"AA",
        		confirmation_File_C_Addr_Actvty_Cde,"Ac",
        		confirmation_File_C_Actvty_Cde,"OrigDate",
        		confirmation_File_C_Original_Chnge_Dte,"OrigTime",
        		confirmation_File_C_Original_Chnge_Tme,"EffDate",
        		confirmation_File_C_Addrss_Eff_Dte,"V",
        		confirmation_File_C_Verify_Ind,"C",
        		confirmation_File_C_Confirmation_Ind,"Name",
        		confirmation_File_C_Name_Key,"WPID",
        		confirmation_File_C_Mit_Wpid,"Unit",
        		confirmation_File_C_Mit_Unit_Cde,"Rldt",
        		confirmation_File_C_Mit_Log_Date_Time,"St",
        		confirmation_File_C_Mit_Status_Cde);
        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"PIN",
        		confirmation_File_C_Ph_Unique_Id_Nmbr,"Contract.",
        		confirmation_File_C_Cntrct_Nmbr,"Py",
        		confirmation_File_C_Cntrct_Payee_Cde, new ReportEditMask ("99"),"Usg/Typ",
        		confirmation_File_C_Usage_Type,"Activity/Code",
        		pnd_Err_Addr_Type,"Tran/Type",
        		pnd_Err_Tran,"Last Chg/Date",
        		pnd_Err_Date,"Last Chg/Time",
        		pnd_Err_Time,NEWLINE,"Remarks",
        		pnd_Err_Msg);
    }
}
