/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:59:36 PM
**        * FROM NATURAL PROGRAM : Nasb491k
************************************************************
**        * FILE NAME            : Nasb491k.java
**        * CLASS NAME           : Nasb491k
**        * INSTANCE NAME        : Nasb491k
************************************************************
************************************************************************
* PROGRAM NAME : NASB491K
* CREATED BY   : C.DIMITRIU
* DATE         : AUG 2017
* DESCRIPTION  : THIS JOB READ WORK FILE CREATED ON PROGRAM NASB490
*                THIS JOB WILL CALL CWF PROGRAM TO ROUTED BACK ALL
*                FUTURE AND TEMPORARY BANK PAYMENT ADDRESS CHANGE
************************************************************************
*    DATE     USERID                 DESCRIPTION
* MM/DD/YYYY XXXXXXXX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 10/16/2002 GOPAZ    MODIFY PROGRAM TO FIXED THE INDEX ARRAY PROBLEM
* 10/22/2009 CABIG    REMOVED NASN740 AND REPLACE WITH SYSTEM DATA
* 09/16/2011 DURAND   ADD RESIDENTIAL ADDRESS TO CORRESPONDENCE LETTER
* 06/27/2017 MEADED   PIN EXPANSION - SEE PINE COMMENTS.
************************************************************************

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nasb491k extends BLNatBase
{
    // Data Areas
    private PdaMdma190 pdaMdma190;
    private PdaNasa191 pdaNasa191;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;
    private PdaErla1000 pdaErla1000;
    private PdaCwfa8010 pdaCwfa8010;
    private PdaCwfa6510 pdaCwfa6510;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Letter_Image_File;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Pin_No;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Contract_No;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Actvty_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Usage;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Change_Dt;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Change_Tm;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Effective_Date;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_1;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Effective_Date_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Verify_Ind;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Destination;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Name;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_1;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_3;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Zip_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Zip_Plus_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Carrier_Route;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Walk_Route;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Geo_Code;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Lob;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Contract_Type_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Type;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_3;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Name;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_1;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_3;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_6;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_7;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Section;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Status_Cde;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Mit_Error;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Compress_Address;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Override;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_1;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_3;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Contract2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Filler;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_8;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Work;

    private DbsGroup pnd_Comp_Address;
    private DbsField pnd_Comp_Address_Pnd_Comp_Address_1;
    private DbsField pnd_Comp_Address_Pnd_Comp_Address_2;
    private DbsField pnd_Comp_Address_Pnd_Comp_Address_3;
    private DbsField pnd_Comp_Address_Pnd_Comp_Address_4;
    private DbsField pnd_Comp_Address_Pnd_Comp_Address_5;
    private DbsField pnd_Comp_Address_Pnd_Comp_Postal_Data;
    private DbsField pnd_Error_Ctr;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_First_Sw;
    private DbsField pnd_First_Time;
    private DbsField pnd_I;
    private DbsField pnd_I_Ndx;
    private DbsField pnd_Mit_Pin;
    private DbsField pnd_Name;
    private DbsField pnd_Pin;
    private DbsField pnd_Q;
    private DbsField pnd_S_Pin;
    private DbsField pnd_S_Contract;
    private DbsField pnd_S_Section;
    private DbsField pnd_Save_Address_1;
    private DbsField pnd_Save_Address_2;
    private DbsField pnd_Save_Address_3;
    private DbsField pnd_Save_Address_4;
    private DbsField pnd_Save_Address_5;
    private DbsField pnd_Save_C_Mit_Status_Cde;
    private DbsField pnd_Save_Contract;
    private DbsField pnd_Save_Log;
    private DbsField pnd_Save_Mit;
    private DbsField pnd_Save_Name;
    private DbsField pnd_Save_Payee;
    private DbsField pnd_Save_Pin;
    private DbsField pnd_Save_Postal_Data;
    private DbsField pnd_Save_Unit;
    private DbsField pnd_Save_Wpid;
    private DbsField pnd_W_Doc_Text;

    private DbsGroup pnd_W_Doc_Text__R_Field_9;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Program;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Fill1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Title1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Fill2;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Area1;

    private DbsGroup pnd_W_Doc_Text__R_Field_10;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Date;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Fill1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Title1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Fill2;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Time;

    private DbsGroup pnd_W_Doc_Text__R_Field_11;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln3_Fill1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln3_From;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln3_Last_Date;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln3_Thru;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln3_Date;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln3_Fill3;

    private DbsGroup pnd_W_Doc_Text__R_Field_12;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln4_Fill1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln4_Hold_Title;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln4_Fill2;

    private DbsGroup pnd_W_Doc_Text__R_Field_13;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln6to15_Title;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail;
    private DbsField pnd_W_Hold_Title;
    private DbsField pnd_W_Ln6to15_Detail_Red;

    private DbsGroup pnd_W_Ln6to15_Detail_Red__R_Field_14;
    private DbsField pnd_W_Ln6to15_Detail_Red_Pnd_W_Ln6to15_Col25;
    private DbsField pnd_W_Ln6to15_Detail_Red_Pnd_W_Ln6to15_Col50;
    private DbsField pnd_W_Ln6to15_Detail_Red_Pnd_W_Ln6to15_Col60;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMdma190 = new PdaMdma190(localVariables);
        pdaNasa191 = new PdaNasa191(localVariables);
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaErla1000 = new PdaErla1000(localVariables);
        pdaCwfa8010 = new PdaCwfa8010(localVariables);
        pdaCwfa6510 = new PdaCwfa6510(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);

        // Local Variables

        pnd_Work_Letter_Image_File = localVariables.newGroupInRecord("pnd_Work_Letter_Image_File", "#WORK-LETTER-IMAGE-FILE");
        pnd_Work_Letter_Image_File_Pnd_O_Pin_No = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Pin_No", "#O-PIN-NO", FieldType.NUMERIC, 
            12);
        pnd_Work_Letter_Image_File_Pnd_O_Contract_No = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Contract_No", "#O-CONTRACT-NO", 
            FieldType.STRING, 10);
        pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd", "#O-PAYEE-CD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd", 
            "#O-ADDR-ACTVTY-CD", FieldType.STRING, 2);
        pnd_Work_Letter_Image_File_Pnd_O_Actvty_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Actvty_Cd", "#O-ACTVTY-CD", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_Usage = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Usage", "#O-USAGE", FieldType.STRING, 
            2);
        pnd_Work_Letter_Image_File_Pnd_O_Change_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Change_Dt", "#O-CHANGE-DT", 
            FieldType.NUMERIC, 8);
        pnd_Work_Letter_Image_File_Pnd_O_Change_Tm = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Change_Tm", "#O-CHANGE-TM", 
            FieldType.NUMERIC, 7);
        pnd_Work_Letter_Image_File_Pnd_O_Effective_Date = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Effective_Date", 
            "#O-EFFECTIVE-DATE", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_1 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_1", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_Effective_Date);
        pnd_Work_Letter_Image_File_Pnd_O_Effective_Date_A = pnd_Work_Letter_Image_File__R_Field_1.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Effective_Date_A", 
            "#O-EFFECTIVE-DATE-A", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_Verify_Ind = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Verify_Ind", "#O-VERIFY-IND", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_M_Destination = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Destination", 
            "#O-M-DESTINATION", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_M_Name = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Name", "#O-M-NAME", FieldType.STRING, 
            35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_1 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_1", "#O-M-ADDRESS-1", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_2 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_2", "#O-M-ADDRESS-2", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_3 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_3", "#O-M-ADDRESS-3", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_4 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_4", "#O-M-ADDRESS-4", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_5 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_5", "#O-M-ADDRESS-5", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data", 
            "#O-M-POSTAL-DATA", FieldType.STRING, 35);

        pnd_Work_Letter_Image_File__R_Field_2 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_2", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data);
        pnd_Work_Letter_Image_File_Pnd_Zip_5 = pnd_Work_Letter_Image_File__R_Field_2.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Zip_5", "#ZIP-5", 
            FieldType.STRING, 5);
        pnd_Work_Letter_Image_File_Pnd_Zip_Plus_4 = pnd_Work_Letter_Image_File__R_Field_2.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Zip_Plus_4", 
            "#ZIP-PLUS-4", FieldType.STRING, 4);
        pnd_Work_Letter_Image_File_Pnd_Carrier_Route = pnd_Work_Letter_Image_File__R_Field_2.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Carrier_Route", 
            "#CARRIER-ROUTE", FieldType.STRING, 4);
        pnd_Work_Letter_Image_File_Pnd_Walk_Route = pnd_Work_Letter_Image_File__R_Field_2.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Walk_Route", 
            "#WALK-ROUTE", FieldType.STRING, 4);
        pnd_Work_Letter_Image_File_Pnd_Geo_Code = pnd_Work_Letter_Image_File__R_Field_2.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Geo_Code", "#GEO-CODE", 
            FieldType.STRING, 2);
        pnd_Work_Letter_Image_File_Pnd_O_M_Lob = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Lob", "#O-M-LOB", FieldType.STRING, 
            1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Contract_Type_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Contract_Type_Cd", 
            "#O-C-CONTRACT-TYPE-CD", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Type = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Type", "#O-C-ADDR-TYPE", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd", 
            "#O-C-ADDR-FORMAT-CD", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt", 
            "#O-C-ADDR-LAST-CH-DT", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_3 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_3", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt_A = pnd_Work_Letter_Image_File__R_Field_3.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt_A", 
            "#O-C-ADDR-LAST-CH-DT-A", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm", 
            "#O-C-ADDR-LAST-CH-TM", FieldType.NUMERIC, 7);

        pnd_Work_Letter_Image_File__R_Field_4 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_4", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm_A = pnd_Work_Letter_Image_File__R_Field_4.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm_A", 
            "#O-C-ADDR-LAST-CH-TM-A", FieldType.STRING, 7);
        pnd_Work_Letter_Image_File_Pnd_O_C_Name = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Name", "#O-C-NAME", FieldType.STRING, 
            35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_1 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_1", "#O-C-ADDRESS-1", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_2 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_2", "#O-C-ADDRESS-2", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_3 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_3", "#O-C-ADDRESS-3", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_4 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_4", "#O-C-ADDRESS-4", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_5 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_5", "#O-C-ADDRESS-5", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data", 
            "#O-C-POSTAL-DATA", FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind", 
            "#O-C-ANNUAL-VAC-CYCLE-IND", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No", "#O-C-PH-BANK-NO", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd", 
            "#O-C-CHECK-SAVING-CD", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No", 
            "#O-C-ABA-ACCOUNT-NO", FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt", 
            "#O-C-PENDING-ADDR-CH-DT", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_5 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_5", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt);
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt_A = pnd_Work_Letter_Image_File__R_Field_5.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt_A", 
            "#O-C-PENDING-ADDR-CH-DT-A", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt", 
            "#O-C-PENDING-ADDR-RS-DT", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_6 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_6", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt);
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt_A = pnd_Work_Letter_Image_File__R_Field_6.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt_A", 
            "#O-C-PENDING-ADDR-RS-DT-A", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt", 
            "#O-C-PERM-ADDRESS-CH-DT", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_7 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_7", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt);
        pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt_A = pnd_Work_Letter_Image_File__R_Field_7.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt_A", 
            "#O-C-PERM-ADDRESS-CH-DT-A", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_C_Section = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Section", "#O-C-SECTION", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid", "#O-C-MIT-WPID", 
            FieldType.STRING, 6);
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde", 
            "#O-C-MIT-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time", 
            "#O-C-MIT-LOG-DATE-TIME", FieldType.STRING, 15);
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Status_Cde = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Status_Cde", 
            "#O-C-MIT-STATUS-CDE", FieldType.STRING, 4);
        pnd_Work_Letter_Image_File_Pnd_Mit_Error = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Mit_Error", "#MIT-ERROR", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_Compress_Address = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Compress_Address", 
            "#COMPRESS-ADDRESS", FieldType.STRING, 30);
        pnd_Work_Letter_Image_File_Pnd_O_M_Override = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Override", "#O-M-OVERRIDE", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_1 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_1", "#O-R-ADDRESS-1", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_2 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_2", "#O-R-ADDRESS-2", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_3 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_3", "#O-R-ADDRESS-3", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_4 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_4", "#O-R-ADDRESS-4", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_5 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_5", "#O-R-ADDRESS-5", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data", 
            "#O-R-POSTAL-DATA", FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_Contract2 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Contract2", "#O-CONTRACT2", 
            FieldType.STRING, 10);
        pnd_Work_Letter_Image_File_Pnd_O_Filler = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Filler", "#O-FILLER", FieldType.STRING, 
            55);

        pnd_Work_Letter_Image_File__R_Field_8 = localVariables.newGroupInRecord("pnd_Work_Letter_Image_File__R_Field_8", "REDEFINE", pnd_Work_Letter_Image_File);
        pnd_Work_Letter_Image_File_Pnd_Work = pnd_Work_Letter_Image_File__R_Field_8.newFieldArrayInGroup("pnd_Work_Letter_Image_File_Pnd_Work", "#WORK", 
            FieldType.STRING, 1, new DbsArrayController(1, 1000));

        pnd_Comp_Address = localVariables.newGroupInRecord("pnd_Comp_Address", "#COMP-ADDRESS");
        pnd_Comp_Address_Pnd_Comp_Address_1 = pnd_Comp_Address.newFieldInGroup("pnd_Comp_Address_Pnd_Comp_Address_1", "#COMP-ADDRESS-1", FieldType.STRING, 
            35);
        pnd_Comp_Address_Pnd_Comp_Address_2 = pnd_Comp_Address.newFieldInGroup("pnd_Comp_Address_Pnd_Comp_Address_2", "#COMP-ADDRESS-2", FieldType.STRING, 
            35);
        pnd_Comp_Address_Pnd_Comp_Address_3 = pnd_Comp_Address.newFieldInGroup("pnd_Comp_Address_Pnd_Comp_Address_3", "#COMP-ADDRESS-3", FieldType.STRING, 
            35);
        pnd_Comp_Address_Pnd_Comp_Address_4 = pnd_Comp_Address.newFieldInGroup("pnd_Comp_Address_Pnd_Comp_Address_4", "#COMP-ADDRESS-4", FieldType.STRING, 
            35);
        pnd_Comp_Address_Pnd_Comp_Address_5 = pnd_Comp_Address.newFieldInGroup("pnd_Comp_Address_Pnd_Comp_Address_5", "#COMP-ADDRESS-5", FieldType.STRING, 
            35);
        pnd_Comp_Address_Pnd_Comp_Postal_Data = pnd_Comp_Address.newFieldInGroup("pnd_Comp_Address_Pnd_Comp_Postal_Data", "#COMP-POSTAL-DATA", FieldType.STRING, 
            35);
        pnd_Error_Ctr = localVariables.newFieldInRecord("pnd_Error_Ctr", "#ERROR-CTR", FieldType.PACKED_DECIMAL, 10);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 50);
        pnd_First_Sw = localVariables.newFieldInRecord("pnd_First_Sw", "#FIRST-SW", FieldType.BOOLEAN, 1);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_I_Ndx = localVariables.newFieldInRecord("pnd_I_Ndx", "#I-NDX", FieldType.NUMERIC, 3);
        pnd_Mit_Pin = localVariables.newFieldInRecord("pnd_Mit_Pin", "#MIT-PIN", FieldType.NUMERIC, 12);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 30);
        pnd_Pin = localVariables.newFieldInRecord("pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Q = localVariables.newFieldInRecord("pnd_Q", "#Q", FieldType.NUMERIC, 2);
        pnd_S_Pin = localVariables.newFieldInRecord("pnd_S_Pin", "#S-PIN", FieldType.NUMERIC, 12);
        pnd_S_Contract = localVariables.newFieldInRecord("pnd_S_Contract", "#S-CONTRACT", FieldType.STRING, 10);
        pnd_S_Section = localVariables.newFieldInRecord("pnd_S_Section", "#S-SECTION", FieldType.STRING, 1);
        pnd_Save_Address_1 = localVariables.newFieldInRecord("pnd_Save_Address_1", "#SAVE-ADDRESS-1", FieldType.STRING, 35);
        pnd_Save_Address_2 = localVariables.newFieldInRecord("pnd_Save_Address_2", "#SAVE-ADDRESS-2", FieldType.STRING, 35);
        pnd_Save_Address_3 = localVariables.newFieldInRecord("pnd_Save_Address_3", "#SAVE-ADDRESS-3", FieldType.STRING, 35);
        pnd_Save_Address_4 = localVariables.newFieldInRecord("pnd_Save_Address_4", "#SAVE-ADDRESS-4", FieldType.STRING, 35);
        pnd_Save_Address_5 = localVariables.newFieldInRecord("pnd_Save_Address_5", "#SAVE-ADDRESS-5", FieldType.STRING, 35);
        pnd_Save_C_Mit_Status_Cde = localVariables.newFieldInRecord("pnd_Save_C_Mit_Status_Cde", "#SAVE-C-MIT-STATUS-CDE", FieldType.STRING, 4);
        pnd_Save_Contract = localVariables.newFieldInRecord("pnd_Save_Contract", "#SAVE-CONTRACT", FieldType.STRING, 10);
        pnd_Save_Log = localVariables.newFieldInRecord("pnd_Save_Log", "#SAVE-LOG", FieldType.STRING, 15);
        pnd_Save_Mit = localVariables.newFieldInRecord("pnd_Save_Mit", "#SAVE-MIT", FieldType.STRING, 1);
        pnd_Save_Name = localVariables.newFieldInRecord("pnd_Save_Name", "#SAVE-NAME", FieldType.STRING, 35);
        pnd_Save_Payee = localVariables.newFieldInRecord("pnd_Save_Payee", "#SAVE-PAYEE", FieldType.NUMERIC, 2);
        pnd_Save_Pin = localVariables.newFieldInRecord("pnd_Save_Pin", "#SAVE-PIN", FieldType.NUMERIC, 12);
        pnd_Save_Postal_Data = localVariables.newFieldInRecord("pnd_Save_Postal_Data", "#SAVE-POSTAL-DATA", FieldType.STRING, 35);
        pnd_Save_Unit = localVariables.newFieldInRecord("pnd_Save_Unit", "#SAVE-UNIT", FieldType.STRING, 8);
        pnd_Save_Wpid = localVariables.newFieldInRecord("pnd_Save_Wpid", "#SAVE-WPID", FieldType.STRING, 6);
        pnd_W_Doc_Text = localVariables.newFieldInRecord("pnd_W_Doc_Text", "#W-DOC-TEXT", FieldType.STRING, 80);

        pnd_W_Doc_Text__R_Field_9 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_9", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln1_Program = pnd_W_Doc_Text__R_Field_9.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Program", "#W-LN1-PROGRAM", FieldType.STRING, 
            7);
        pnd_W_Doc_Text_Pnd_W_Ln1_Fill1 = pnd_W_Doc_Text__R_Field_9.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Fill1", "#W-LN1-FILL1", FieldType.STRING, 
            19);
        pnd_W_Doc_Text_Pnd_W_Ln1_Title1 = pnd_W_Doc_Text__R_Field_9.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Title1", "#W-LN1-TITLE1", FieldType.STRING, 
            23);
        pnd_W_Doc_Text_Pnd_W_Ln1_Fill2 = pnd_W_Doc_Text__R_Field_9.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Fill2", "#W-LN1-FILL2", FieldType.STRING, 
            19);
        pnd_W_Doc_Text_Pnd_W_Ln1_Area1 = pnd_W_Doc_Text__R_Field_9.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Area1", "#W-LN1-AREA1", FieldType.STRING, 
            12);

        pnd_W_Doc_Text__R_Field_10 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_10", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln2_Date = pnd_W_Doc_Text__R_Field_10.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Date", "#W-LN2-DATE", FieldType.STRING, 10);
        pnd_W_Doc_Text_Pnd_W_Ln2_Fill1 = pnd_W_Doc_Text__R_Field_10.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Fill1", "#W-LN2-FILL1", FieldType.STRING, 
            13);
        pnd_W_Doc_Text_Pnd_W_Ln2_Title1 = pnd_W_Doc_Text__R_Field_10.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Title1", "#W-LN2-TITLE1", FieldType.STRING, 
            33);
        pnd_W_Doc_Text_Pnd_W_Ln2_Fill2 = pnd_W_Doc_Text__R_Field_10.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Fill2", "#W-LN2-FILL2", FieldType.STRING, 
            13);
        pnd_W_Doc_Text_Pnd_W_Ln2_Time = pnd_W_Doc_Text__R_Field_10.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Time", "#W-LN2-TIME", FieldType.STRING, 11);

        pnd_W_Doc_Text__R_Field_11 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_11", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln3_Fill1 = pnd_W_Doc_Text__R_Field_11.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln3_Fill1", "#W-LN3-FILL1", FieldType.STRING, 
            25);
        pnd_W_Doc_Text_Pnd_W_Ln3_From = pnd_W_Doc_Text__R_Field_11.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln3_From", "#W-LN3-FROM", FieldType.STRING, 5);
        pnd_W_Doc_Text_Pnd_W_Ln3_Last_Date = pnd_W_Doc_Text__R_Field_11.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln3_Last_Date", "#W-LN3-LAST-DATE", FieldType.STRING, 
            10);
        pnd_W_Doc_Text_Pnd_W_Ln3_Thru = pnd_W_Doc_Text__R_Field_11.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln3_Thru", "#W-LN3-THRU", FieldType.STRING, 6);
        pnd_W_Doc_Text_Pnd_W_Ln3_Date = pnd_W_Doc_Text__R_Field_11.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln3_Date", "#W-LN3-DATE", FieldType.STRING, 10);
        pnd_W_Doc_Text_Pnd_W_Ln3_Fill3 = pnd_W_Doc_Text__R_Field_11.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln3_Fill3", "#W-LN3-FILL3", FieldType.STRING, 
            24);

        pnd_W_Doc_Text__R_Field_12 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_12", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln4_Fill1 = pnd_W_Doc_Text__R_Field_12.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln4_Fill1", "#W-LN4-FILL1", FieldType.STRING, 
            21);
        pnd_W_Doc_Text_Pnd_W_Ln4_Hold_Title = pnd_W_Doc_Text__R_Field_12.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln4_Hold_Title", "#W-LN4-HOLD-TITLE", FieldType.STRING, 
            39);
        pnd_W_Doc_Text_Pnd_W_Ln4_Fill2 = pnd_W_Doc_Text__R_Field_12.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln4_Fill2", "#W-LN4-FILL2", FieldType.STRING, 
            20);

        pnd_W_Doc_Text__R_Field_13 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_13", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln6to15_Title = pnd_W_Doc_Text__R_Field_13.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln6to15_Title", "#W-LN6TO15-TITLE", FieldType.STRING, 
            24);
        pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail = pnd_W_Doc_Text__R_Field_13.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail", "#W-LN6TO15-DETAIL", FieldType.STRING, 
            56);
        pnd_W_Hold_Title = localVariables.newFieldInRecord("pnd_W_Hold_Title", "#W-HOLD-TITLE", FieldType.STRING, 39);
        pnd_W_Ln6to15_Detail_Red = localVariables.newFieldInRecord("pnd_W_Ln6to15_Detail_Red", "#W-LN6TO15-DETAIL-RED", FieldType.STRING, 56);

        pnd_W_Ln6to15_Detail_Red__R_Field_14 = localVariables.newGroupInRecord("pnd_W_Ln6to15_Detail_Red__R_Field_14", "REDEFINE", pnd_W_Ln6to15_Detail_Red);
        pnd_W_Ln6to15_Detail_Red_Pnd_W_Ln6to15_Col25 = pnd_W_Ln6to15_Detail_Red__R_Field_14.newFieldInGroup("pnd_W_Ln6to15_Detail_Red_Pnd_W_Ln6to15_Col25", 
            "#W-LN6TO15-COL25", FieldType.STRING, 25);
        pnd_W_Ln6to15_Detail_Red_Pnd_W_Ln6to15_Col50 = pnd_W_Ln6to15_Detail_Red__R_Field_14.newFieldInGroup("pnd_W_Ln6to15_Detail_Red_Pnd_W_Ln6to15_Col50", 
            "#W-LN6TO15-COL50", FieldType.STRING, 10);
        pnd_W_Ln6to15_Detail_Red_Pnd_W_Ln6to15_Col60 = pnd_W_Ln6to15_Detail_Red__R_Field_14.newFieldInGroup("pnd_W_Ln6to15_Detail_Red_Pnd_W_Ln6to15_Col60", 
            "#W-LN6TO15-COL60", FieldType.STRING, 21);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_First_Sw.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Nasb491k() throws Exception
    {
        super("Nasb491k");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 132 SG = OFF ZP = ON
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #WORK-LETTER-IMAGE-FILE
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Work_Letter_Image_File)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #O-PIN-NO
            //*  5=FUTURE 8=TEMPORARY
            //*  C=IA/CHECK/BANK/EFT
            //*  D=IA/CHECK/BANK/CONSRVTOR
            if (condition(((pnd_Work_Letter_Image_File_Pnd_O_C_Section.equals("5") || pnd_Work_Letter_Image_File_Pnd_O_C_Section.equals("8")) && (pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd.equals("C")  //Natural: IF #O-C-SECTION = '5' OR = '8' AND #O-C-ADDR-FORMAT-CD = 'C' OR = 'D'
                || pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd.equals("D")))))
            {
                pnd_Save_Pin.setValue(pnd_Work_Letter_Image_File_Pnd_O_Pin_No);                                                                                           //Natural: ASSIGN #SAVE-PIN := #O-PIN-NO
                pnd_Save_Contract.setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                                                                                 //Natural: ASSIGN #SAVE-CONTRACT := #O-CONTRACT-NO
                pnd_Save_Payee.setValue(pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd);                                                                                       //Natural: ASSIGN #SAVE-PAYEE := #O-PAYEE-CD
                pnd_Save_Name.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Name);                                                                                          //Natural: ASSIGN #SAVE-NAME := #O-M-NAME
                pnd_Save_Address_1.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_1);                                                                                //Natural: ASSIGN #SAVE-ADDRESS-1 := #O-M-ADDRESS-1
                pnd_Save_Address_2.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_2);                                                                                //Natural: ASSIGN #SAVE-ADDRESS-2 := #O-M-ADDRESS-2
                pnd_Save_Address_3.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_3);                                                                                //Natural: ASSIGN #SAVE-ADDRESS-3 := #O-M-ADDRESS-3
                pnd_Save_Address_4.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_4);                                                                                //Natural: ASSIGN #SAVE-ADDRESS-4 := #O-M-ADDRESS-4
                pnd_Save_Address_5.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_5);                                                                                //Natural: ASSIGN #SAVE-ADDRESS-5 := #O-M-ADDRESS-5
                pnd_Save_Postal_Data.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data);                                                                            //Natural: ASSIGN #SAVE-POSTAL-DATA := #O-M-POSTAL-DATA
                pnd_Save_Wpid.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid);                                                                                      //Natural: ASSIGN #SAVE-WPID := #O-C-MIT-WPID
                pnd_Save_Unit.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde);                                                                                  //Natural: ASSIGN #SAVE-UNIT := #O-C-MIT-UNIT-CDE
                pnd_Save_Log.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time);                                                                              //Natural: ASSIGN #SAVE-LOG := #O-C-MIT-LOG-DATE-TIME
                pnd_Save_Mit.setValue(pnd_Work_Letter_Image_File_Pnd_Mit_Error);                                                                                          //Natural: ASSIGN #SAVE-MIT := #MIT-ERROR
                if (condition(pnd_Work_Letter_Image_File_Pnd_Mit_Error.equals(" ")))                                                                                      //Natural: IF #MIT-ERROR = ' '
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-CWF-RTN
                    sub_Update_Cwf_Rtn();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_First_Sw.getBoolean()))                                                                                                                 //Natural: IF #FIRST-SW
                {
                    pnd_S_Pin.setValue(pnd_Work_Letter_Image_File_Pnd_O_Pin_No);                                                                                          //Natural: ASSIGN #S-PIN := #O-PIN-NO
                    pnd_S_Section.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Section);                                                                                   //Natural: ASSIGN #S-SECTION := #O-C-SECTION
                    pnd_S_Contract.setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                                                                                //Natural: ASSIGN #S-CONTRACT := #O-CONTRACT-NO
                    pnd_First_Sw.reset();                                                                                                                                 //Natural: RESET #FIRST-SW
                                                                                                                                                                          //Natural: PERFORM MOVE-COMPRESS-ADDRESS
                    sub_Move_Compress_Address();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getWorkFiles().write(2, false, pnd_Work_Letter_Image_File);                                                                                           //Natural: WRITE WORK FILE 2 #WORK-LETTER-IMAGE-FILE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_S_Pin.equals(pnd_Work_Letter_Image_File_Pnd_O_Pin_No) && pnd_S_Section.equals(pnd_Work_Letter_Image_File_Pnd_O_C_Section)               //Natural: IF #S-PIN = #O-PIN-NO AND #S-SECTION = #O-C-SECTION AND #S-CONTRACT = #O-CONTRACT-NO
                    && pnd_S_Contract.equals(pnd_Work_Letter_Image_File_Pnd_O_Contract_No)))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_S_Pin.setValue(pnd_Work_Letter_Image_File_Pnd_O_Pin_No);                                                                                          //Natural: ASSIGN #S-PIN := #O-PIN-NO
                    pnd_S_Section.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Section);                                                                                   //Natural: ASSIGN #S-SECTION := #O-C-SECTION
                    pnd_S_Contract.setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                                                                                //Natural: ASSIGN #S-CONTRACT := #O-CONTRACT-NO
                                                                                                                                                                          //Natural: PERFORM MOVE-COMPRESS-ADDRESS
                    sub_Move_Compress_Address();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getWorkFiles().write(2, false, pnd_Work_Letter_Image_File);                                                                                           //Natural: WRITE WORK FILE 2 #WORK-LETTER-IMAGE-FILE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Records in Error:",pnd_Error_Ctr, new ReportEditMask ("Z,ZZZ,ZZZ,Z99"));                                        //Natural: WRITE ( 1 ) 'Total Records in Error:' #ERROR-CTR
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-COMPRESS-ADDRESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-DOC-TEXT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-CWF-RTN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CWF-RTN
        //* ***********************************************************************
    }
    private void sub_Error_Report() throws Exception                                                                                                                      //Natural: ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Error_Ctr.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #ERROR-CTR
        getReports().display(1, ReportOption.NOTITLE,"PIN",                                                                                                               //Natural: DISPLAY ( 1 ) NOTITLE 'PIN' #SAVE-PIN 'Name' #SAVE-NAME 'Contract' #SAVE-CONTRACT 'REMARKS' #ERROR-MESSAGE ( EM = X ( 71 ) )
        		pnd_Save_Pin,"Name",
        		pnd_Save_Name,"Contract",
        		pnd_Save_Contract,"REMARKS",
        		pdaMdma190.getMdma190_Pnd_Error_Message(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"));
        if (Global.isEscape()) return;
        //*  ERROR-REPORT
    }
    private void sub_Move_Compress_Address() throws Exception                                                                                                             //Natural: MOVE-COMPRESS-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Comp_Address.reset();                                                                                                                                         //Natural: RESET #COMP-ADDRESS #COMPRESS-ADDRESS
        pnd_Work_Letter_Image_File_Pnd_Compress_Address.reset();
        pnd_Comp_Address_Pnd_Comp_Address_1.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_1);                                                                       //Natural: ASSIGN #COMP-ADDRESS-1 := #O-M-ADDRESS-1
        pnd_Comp_Address_Pnd_Comp_Address_2.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_2);                                                                       //Natural: ASSIGN #COMP-ADDRESS-2 := #O-M-ADDRESS-2
        pnd_Comp_Address_Pnd_Comp_Address_3.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_3);                                                                       //Natural: ASSIGN #COMP-ADDRESS-3 := #O-M-ADDRESS-3
        pnd_Comp_Address_Pnd_Comp_Address_4.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_4);                                                                       //Natural: ASSIGN #COMP-ADDRESS-4 := #O-M-ADDRESS-4
        pnd_Comp_Address_Pnd_Comp_Address_5.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_5);                                                                       //Natural: ASSIGN #COMP-ADDRESS-5 := #O-M-ADDRESS-5
        pnd_Comp_Address_Pnd_Comp_Postal_Data.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data);                                                                   //Natural: ASSIGN #COMP-POSTAL-DATA := #O-M-POSTAL-DATA
        DbsUtil.examine(new ExamineSource(pnd_Comp_Address_Pnd_Comp_Address_1), new ExamineSearch(" "), new ExamineDelete());                                             //Natural: EXAMINE #COMP-ADDRESS-1 ' ' DELETE
        DbsUtil.examine(new ExamineSource(pnd_Comp_Address_Pnd_Comp_Address_2), new ExamineSearch(" "), new ExamineDelete());                                             //Natural: EXAMINE #COMP-ADDRESS-2 ' ' DELETE
        DbsUtil.examine(new ExamineSource(pnd_Comp_Address_Pnd_Comp_Address_3), new ExamineSearch(" "), new ExamineDelete());                                             //Natural: EXAMINE #COMP-ADDRESS-3 ' ' DELETE
        DbsUtil.examine(new ExamineSource(pnd_Comp_Address_Pnd_Comp_Address_4), new ExamineSearch(" "), new ExamineDelete());                                             //Natural: EXAMINE #COMP-ADDRESS-4 ' ' DELETE
        DbsUtil.examine(new ExamineSource(pnd_Comp_Address_Pnd_Comp_Address_5), new ExamineSearch(" "), new ExamineDelete());                                             //Natural: EXAMINE #COMP-ADDRESS-5 ' ' DELETE
        DbsUtil.examine(new ExamineSource(pnd_Comp_Address_Pnd_Comp_Postal_Data), new ExamineSearch(" "), new ExamineDelete());                                           //Natural: EXAMINE #COMP-POSTAL-DATA ' ' DELETE
        //*  STANDARDIZED DOMESTIC ADDRESS
        if (condition(pnd_Work_Letter_Image_File_Pnd_O_M_Destination.equals("U") && pnd_Work_Letter_Image_File_Pnd_O_M_Override.notEquals("Y") && pnd_Work_Letter_Image_File_Pnd_Carrier_Route.notEquals(" ")  //Natural: IF #O-M-DESTINATION = 'U' AND #O-M-OVERRIDE NE 'Y' AND #CARRIER-ROUTE NE ' ' AND #WALK-ROUTE NE ' '
            && pnd_Work_Letter_Image_File_Pnd_Walk_Route.notEquals(" ")))
        {
            pnd_Work_Letter_Image_File_Pnd_Compress_Address.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Letter_Image_File_Pnd_O_Pin_No,             //Natural: COMPRESS #O-PIN-NO #COMP-POSTAL-DATA INTO #COMPRESS-ADDRESS LEAVING NO
                pnd_Comp_Address_Pnd_Comp_Postal_Data));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Letter_Image_File_Pnd_Compress_Address.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Letter_Image_File_Pnd_O_Pin_No,             //Natural: COMPRESS #O-PIN-NO #COMP-ADDRESS-1 #COMP-ADDRESS-2 #COMP-ADDRESS-3 #COMP-ADDRESS-4 #COMP-ADDRESS-5 INTO #COMPRESS-ADDRESS LEAVING NO
                pnd_Comp_Address_Pnd_Comp_Address_1, pnd_Comp_Address_Pnd_Comp_Address_2, pnd_Comp_Address_Pnd_Comp_Address_3, pnd_Comp_Address_Pnd_Comp_Address_4, 
                pnd_Comp_Address_Pnd_Comp_Address_5));
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE-COMPRESS-ADDRESS
    }
    private void sub_Move_Doc_Text() throws Exception                                                                                                                     //Natural: MOVE-DOC-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(1,":",60).reset();                                                                                                  //Natural: RESET NASA191.#DOC-TEXT ( 1:60 )
        FOR01:                                                                                                                                                            //Natural: FOR #I-NDX = 1 TO 13
        for (pnd_I_Ndx.setValue(1); condition(pnd_I_Ndx.lessOrEqual(13)); pnd_I_Ndx.nadd(1))
        {
            pnd_W_Doc_Text.reset();                                                                                                                                       //Natural: RESET #W-DOC-TEXT
            short decideConditionsMet1108 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #I-NDX;//Natural: VALUE 1
            if (condition((pnd_I_Ndx.equals(1))))
            {
                decideConditionsMet1108++;
                pnd_W_Doc_Text_Pnd_W_Ln1_Program.setValue(Global.getPROGRAM());                                                                                           //Natural: ASSIGN #W-LN1-PROGRAM := *PROGRAM
                pnd_W_Doc_Text_Pnd_W_Ln1_Title1.setValue("NAME AND ADDRESS SYSTEM");                                                                                      //Natural: ASSIGN #W-LN1-TITLE1 := 'NAME AND ADDRESS SYSTEM'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_I_Ndx.equals(2))))
            {
                decideConditionsMet1108++;
                pnd_W_Doc_Text_Pnd_W_Ln2_Date.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                          //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #W-LN2-DATE
                pnd_W_Doc_Text_Pnd_W_Ln2_Title1.setValue("NAME ADDRESS LETTER REGISTRY");                                                                                 //Natural: ASSIGN #W-LN2-TITLE1 := 'NAME ADDRESS LETTER REGISTRY'
                pnd_W_Doc_Text_Pnd_W_Ln2_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS' 'AP"));                                                       //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS' 'AP ) TO #W-LN2-TIME
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_I_Ndx.equals(3))))
            {
                decideConditionsMet1108++;
                pnd_W_Doc_Text_Pnd_W_Ln3_From.setValue("FROM ");                                                                                                          //Natural: ASSIGN #W-LN3-FROM := 'FROM '
                pnd_W_Doc_Text_Pnd_W_Ln3_Thru.setValue(" THRU ");                                                                                                         //Natural: ASSIGN #W-LN3-THRU := ' THRU '
                pnd_W_Doc_Text_Pnd_W_Ln3_Last_Date.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                     //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #W-LN3-LAST-DATE
                pnd_W_Doc_Text_Pnd_W_Ln3_Date.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                          //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #W-LN3-DATE
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_I_Ndx.equals(4))))
            {
                decideConditionsMet1108++;
                pnd_W_Doc_Text_Pnd_W_Ln4_Hold_Title.setValue(pnd_W_Hold_Title);                                                                                           //Natural: ASSIGN #W-LN4-HOLD-TITLE := #W-HOLD-TITLE
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_I_Ndx.equals(6))))
            {
                decideConditionsMet1108++;
                pnd_W_Doc_Text_Pnd_W_Ln6to15_Title.setValue("CONTRACT:");                                                                                                 //Natural: ASSIGN #W-LN6TO15-TITLE := 'CONTRACT:'
                pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail.setValue(pnd_Save_Contract);                                                                                          //Natural: ASSIGN #W-LN6TO15-DETAIL := #SAVE-CONTRACT
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pnd_I_Ndx.equals(7))))
            {
                decideConditionsMet1108++;
                pnd_W_Doc_Text_Pnd_W_Ln6to15_Title.setValue("PAYEE:");                                                                                                    //Natural: ASSIGN #W-LN6TO15-TITLE := 'PAYEE:'
                pnd_W_Ln6to15_Detail_Red_Pnd_W_Ln6to15_Col25.setValue(pnd_Save_Payee);                                                                                    //Natural: ASSIGN #W-LN6TO15-COL25 := #SAVE-PAYEE
                pnd_W_Ln6to15_Detail_Red_Pnd_W_Ln6to15_Col50.setValue("USAGE:");                                                                                          //Natural: ASSIGN #W-LN6TO15-COL50 := 'USAGE:'
                pnd_W_Ln6to15_Detail_Red_Pnd_W_Ln6to15_Col60.setValue("CM");                                                                                              //Natural: ASSIGN #W-LN6TO15-COL60 := 'CM'
                pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail.setValue(pnd_W_Ln6to15_Detail_Red);                                                                                   //Natural: ASSIGN #W-LN6TO15-DETAIL := #W-LN6TO15-DETAIL-RED
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pnd_I_Ndx.equals(8))))
            {
                decideConditionsMet1108++;
                pnd_W_Doc_Text_Pnd_W_Ln6to15_Title.setValue("PIN:");                                                                                                      //Natural: ASSIGN #W-LN6TO15-TITLE := 'PIN:'
                pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail.setValue(pnd_Save_Pin);                                                                                               //Natural: ASSIGN #W-LN6TO15-DETAIL := #SAVE-PIN
            }                                                                                                                                                             //Natural: VALUE 9
            else if (condition((pnd_I_Ndx.equals(9))))
            {
                decideConditionsMet1108++;
                pnd_W_Doc_Text_Pnd_W_Ln6to15_Title.setValue("NAME:");                                                                                                     //Natural: ASSIGN #W-LN6TO15-TITLE := 'NAME:'
                pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail.setValue(pnd_Save_Name);                                                                                              //Natural: ASSIGN #W-LN6TO15-DETAIL := #SAVE-NAME
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_I_Ndx.equals(10))))
            {
                decideConditionsMet1108++;
                pnd_W_Doc_Text_Pnd_W_Ln6to15_Title.setValue("ADDRESS:");                                                                                                  //Natural: ASSIGN #W-LN6TO15-TITLE := 'ADDRESS:'
                pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail.setValue(pnd_Save_Address_1);                                                                                         //Natural: ASSIGN #W-LN6TO15-DETAIL := #SAVE-ADDRESS-1
            }                                                                                                                                                             //Natural: VALUE 11
            else if (condition((pnd_I_Ndx.equals(11))))
            {
                decideConditionsMet1108++;
                pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail.setValue(pnd_Save_Address_2);                                                                                         //Natural: ASSIGN #W-LN6TO15-DETAIL := #SAVE-ADDRESS-2
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_I_Ndx.equals(12))))
            {
                decideConditionsMet1108++;
                pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail.setValue(pnd_Save_Address_3);                                                                                         //Natural: ASSIGN #W-LN6TO15-DETAIL := #SAVE-ADDRESS-3
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet1108 > 0))
            {
                pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(pnd_I_Ndx).setValue(pnd_W_Doc_Text);                                                                        //Natural: ASSIGN NASA191.#DOC-TEXT ( #I-NDX ) := #W-DOC-TEXT
                pnd_First_Time.reset();                                                                                                                                   //Natural: RESET #FIRST-TIME
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Work_Letter_Image_File_Pnd_O_M_Address_4.notEquals(" ")))                                                                                       //Natural: IF #O-M-ADDRESS-4 NE ' '
        {
            pnd_I_Ndx.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I-NDX
            pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail.setValue(pnd_Save_Address_4);                                                                                             //Natural: ASSIGN #W-LN6TO15-DETAIL := #SAVE-ADDRESS-4
            pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(pnd_I_Ndx).setValue(pnd_W_Doc_Text);                                                                            //Natural: ASSIGN NASA191.#DOC-TEXT ( #I-NDX ) := #W-DOC-TEXT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_Letter_Image_File_Pnd_O_M_Address_5.notEquals(" ")))                                                                                       //Natural: IF #O-M-ADDRESS-5 NE ' '
        {
            pnd_I_Ndx.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I-NDX
            pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail.setValue(pnd_Save_Address_5);                                                                                             //Natural: ASSIGN #W-LN6TO15-DETAIL := #SAVE-ADDRESS-5
            pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(pnd_I_Ndx).setValue(pnd_W_Doc_Text);                                                                            //Natural: ASSIGN NASA191.#DOC-TEXT ( #I-NDX ) := #W-DOC-TEXT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_I_Ndx.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I-NDX
        pnd_W_Doc_Text_Pnd_W_Ln6to15_Title.setValue("ZIP CODE");                                                                                                          //Natural: ASSIGN #W-LN6TO15-TITLE := 'ZIP CODE'
        pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail.setValue(pnd_Save_Postal_Data);                                                                                               //Natural: ASSIGN #W-LN6TO15-DETAIL := #SAVE-POSTAL-DATA
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(pnd_I_Ndx).setValue(pnd_W_Doc_Text);                                                                                //Natural: ASSIGN NASA191.#DOC-TEXT ( #I-NDX ) := #W-DOC-TEXT
        pnd_I_Ndx.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I-NDX
        pnd_W_Doc_Text_Pnd_W_Ln6to15_Title.setValue("REASON FOR REJECTION:");                                                                                             //Natural: ASSIGN #W-LN6TO15-TITLE := 'REASON FOR REJECTION:'
        pnd_W_Doc_Text_Pnd_W_Ln6to15_Detail.setValue("REQUEST IS A FUTURE/TEMPORAY CHECK MAILING");                                                                       //Natural: ASSIGN #W-LN6TO15-DETAIL := 'REQUEST IS A FUTURE/TEMPORAY CHECK MAILING'
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(pnd_I_Ndx).setValue(pnd_W_Doc_Text);                                                                                //Natural: ASSIGN NASA191.#DOC-TEXT ( #I-NDX ) := #W-DOC-TEXT
        if (condition(pnd_Save_Pin.notEquals(pnd_Mit_Pin)))                                                                                                               //Natural: IF #SAVE-PIN NE #MIT-PIN
        {
            pnd_Mit_Pin.setValue(pnd_Save_Pin);                                                                                                                           //Natural: ASSIGN #MIT-PIN := #SAVE-PIN
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE-DOC-TEXT
    }
    private void sub_Store_Cwf_Rtn() throws Exception                                                                                                                     //Natural: STORE-CWF-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_First_Time.getBoolean()))                                                                                                                       //Natural: IF #FIRST-TIME
        {
            pnd_Mit_Pin.setValue(pnd_Save_Pin);                                                                                                                           //Natural: ASSIGN #MIT-PIN := #SAVE-PIN
            pnd_First_Time.reset();                                                                                                                                       //Natural: RESET #FIRST-TIME
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Mit_Pin.notEquals(pnd_Save_Pin)))                                                                                                               //Natural: IF #MIT-PIN NE #SAVE-PIN
        {
            pnd_Mit_Pin.setValue(pnd_Save_Pin);                                                                                                                           //Natural: ASSIGN #MIT-PIN := #SAVE-PIN
        }                                                                                                                                                                 //Natural: END-IF
        pdaMdma190.getMdma190_Pnd_Et_Ind().setValue("N");                                                                                                                 //Natural: ASSIGN MDMA190.#ET-IND := 'N'
        pdaMdma190.getMdma190_Pnd_Wpid_System().setValue("BATCHNAS");                                                                                                     //Natural: ASSIGN MDMA190.#WPID-SYSTEM := 'BATCHNAS'
        pdaMdma190.getMdma190_Pnd_Gda_User_Id().setValue("NASB491");                                                                                                      //Natural: ASSIGN MDMA190.#GDA-USER-ID := 'NASB491'
        pdaMdma190.getMdma190_Pnd_Gda_Slo_Exists().setValue("N");                                                                                                         //Natural: ASSIGN MDMA190.#GDA-SLO-EXISTS := 'N'
        pdaMdma190.getMdma190_Pnd_Wpid_Pin().setValue(pnd_Save_Pin);                                                                                                      //Natural: ASSIGN MDMA190.#WPID-PIN := #SAVE-PIN
        pdaMdma190.getMdma190_Pnd_Wpid_Status().setValue("8340");                                                                                                         //Natural: ASSIGN MDMA190.#WPID-STATUS := '8340'
        pdaMdma190.getMdma190_Pnd_Wpid_Unit().setValue("CDMSA I");                                                                                                        //Natural: ASSIGN MDMA190.#WPID-UNIT := MDMA190.#WPID-NEXT-UNIT := 'CDMSA I'
        pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit().setValue("CDMSA I");
        pdaMdma190.getMdma190_Pnd_Wpid().setValue("TA HV");                                                                                                               //Natural: ASSIGN MDMA190.#WPID := 'TA HV'
        pdaNasa191.getNasa191().reset();                                                                                                                                  //Natural: RESET NASA191
        pdaNasa191.getNasa191_Pnd_Action().setValue("AN");                                                                                                                //Natural: ASSIGN NASA191.#ACTION := 'AN'
        pdaNasa191.getNasa191_Pnd_Doc_Category().setValue("M");                                                                                                           //Natural: ASSIGN NASA191.#DOC-CATEGORY := 'M'
        pdaNasa191.getNasa191_Pnd_Doc_Class().setValue("ADD");                                                                                                            //Natural: ASSIGN NASA191.#DOC-CLASS := 'ADD'
        pdaNasa191.getNasa191_Pnd_Doc_Direction().setValue("N");                                                                                                          //Natural: ASSIGN NASA191.#DOC-DIRECTION := 'N'
        pdaNasa191.getNasa191_Pnd_Doc_Format_Cde().setValue("T");                                                                                                         //Natural: ASSIGN NASA191.#DOC-FORMAT-CDE := 'T'
        pdaNasa191.getNasa191_Pnd_Doc_Retention_Cde().setValue("P");                                                                                                      //Natural: ASSIGN NASA191.#DOC-RETENTION-CDE := 'P'
        pdaNasa191.getNasa191_Pnd_Last_Page_Flag().setValue("Y");                                                                                                         //Natural: ASSIGN NASA191.#LAST-PAGE-FLAG := 'Y'
                                                                                                                                                                          //Natural: PERFORM MOVE-DOC-TEXT
        sub_Move_Doc_Text();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.callnat(Mdmn191.class , getCurrentProcessState(), pdaMdma190.getMdma190(), pdaNasa191.getNasa191());                                                      //Natural: CALLNAT 'MDMN191' USING MDMA190 NASA191
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma190.getMdma190_Pnd_Error_Message().notEquals(" ")))                                                                                          //Natural: IF MDMA190.#ERROR-MESSAGE NE ' '
        {
            pdaMdma190.getMdma190_Pnd_Error_Message().setValue(pdaMdma190.getMdma190_Pnd_Error_Message());                                                                //Natural: ASSIGN #ERROR-MESSAGE := MDMA190.#ERROR-MESSAGE
                                                                                                                                                                          //Natural: PERFORM ERROR-REPORT
            sub_Error_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  STORE-CWF-RTN
    }
    private void sub_Update_Cwf_Rtn() throws Exception                                                                                                                    //Natural: UPDATE-CWF-RTN
    {
        if (BLNatReinput.isReinput()) return;

        pdaCwfa8010.getCwfa8010_Oprtr_Cde().setValue("NASB491");                                                                                                          //Natural: ASSIGN CWFA8010.OPRTR-CDE := 'NASB491'
        pdaCwfa8010.getCwfa8010_Status_Cde().setValue(8340);                                                                                                              //Natural: ASSIGN CWFA8010.STATUS-CDE := 8340
        pdaCwfa8010.getCwfa8010_Crprte_Status_Ind().setValue(9);                                                                                                          //Natural: ASSIGN CWFA8010.CRPRTE-STATUS-IND := 9
        pdaCwfa8010.getCwfa8010_Action_Cde().setValue("MO");                                                                                                              //Natural: ASSIGN CWFA8010.ACTION-CDE := 'MO'
        pdaCwfa8010.getCwfa8010_System().setValue("BATCHNAS");                                                                                                            //Natural: ASSIGN CWFA8010.SYSTEM := 'BATCHNAS'
        pdaCwfa8010.getCwfa8010_Rqst_Log_Dte_Tme().setValue(pnd_Save_Log);                                                                                                //Natural: ASSIGN CWFA8010.RQST-LOG-DTE-TME := #SAVE-LOG
        pdaCwfa8010.getCwfa8010_Pin_Nbr().setValue(pnd_Save_Pin);                                                                                                         //Natural: ASSIGN CWFA8010.PIN-NBR := #SAVE-PIN
        pdaCwfa8010.getCwfa8010_Wpid_Vldte_Ind().setValue("Y");                                                                                                           //Natural: ASSIGN CWFA8010.WPID-VLDTE-IND := 'Y'
        pdaCwfa8010.getCwfa8010_Oprtr_Unit_Cde().setValue(pnd_Save_Unit);                                                                                                 //Natural: ASSIGN CWFA8010.OPRTR-UNIT-CDE := #SAVE-UNIT
        DbsUtil.callnat(Cwfn8010.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaCwfpda_D.getDialog_Info_Sub(),            //Natural: CALLNAT 'CWFN8010' MSG-INFO-SUB PASS-SUB DIALOG-INFO-SUB CWFA8010
            pdaCwfa8010.getCwfa8010());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(getZero()) || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals("E")))                //Natural: IF MSG-INFO-SUB.##MSG-NR = 0 OR MSG-INFO-SUB.##RETURN-CODE NE 'E'
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaMdma190.getMdma190_Pnd_Error_Message().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                //Natural: ASSIGN #ERROR-MESSAGE := MSG-INFO-SUB.##MSG
                                                                                                                                                                          //Natural: PERFORM ERROR-REPORT
            sub_Error_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  UPDATE-CWF-RTN
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(42),"Name And Address - Confirmation Letter Registry",new  //Natural: WRITE ( 1 ) NOTITLE 001T *PROGRAM 042T 'Name And Address - Confirmation Letter Registry' 120T 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 039T 'Error Report - IA Future/Temporary Check-Mailing Bank' 120T *TIMX ( EM = HH:II:SS�AP ) /
                        TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(39),"Error Report - IA Future/Temporary Check-Mailing Bank",new TabSetting(120),Global.getTIMX(), 
                        new ReportEditMask ("HH:II:SS AP"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Work_Letter_Image_File_Pnd_O_Pin_NoIsBreak = pnd_Work_Letter_Image_File_Pnd_O_Pin_No.isBreak(endOfData);
        if (condition(pnd_Work_Letter_Image_File_Pnd_O_Pin_NoIsBreak))
        {
            if (condition(pnd_Save_Mit.equals("Y")))                                                                                                                      //Natural: IF #SAVE-MIT = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM STORE-CWF-RTN
                sub_Store_Cwf_Rtn();
                if (condition(Global.isEscape())) {return;}
                pnd_Save_Mit.reset();                                                                                                                                     //Natural: RESET #SAVE-MIT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132 SG=OFF ZP=ON");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"PIN",
        		pnd_Save_Pin,"Name",
        		pnd_Save_Name,"Contract",
        		pnd_Save_Contract,"REMARKS",
        		pdaMdma190.getMdma190_Pnd_Error_Message(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"));
    }
}
