/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:59:58 PM
**        * FROM NATURAL PROGRAM : Nasb999
************************************************************
**        * FILE NAME            : Nasb999.java
**        * CLASS NAME           : Nasb999
**        * INSTANCE NAME        : Nasb999
************************************************************
************************************************************************
* PROGRAM NAME : NASB999
* DESCRIPTION  : NAS ADHOC MODULE
* WRITTEN BY   : DENNIS DURAN
* DATE WRITTEN : APRIL 20, 2007
************************************************************************
*
* WE CHOSE THIS METHOD TO AVOID HAVING TO IMPLEMENT A FRESH JCL
* PARM AND PROC EACH TIME WE WISH TO DO ADHOC DATA UPDATES.
*
* NASB999 IS RUN BY JOB P9997NAR. ONCE NASB999 HAS BEEN UPDATED TO
* THE CURRENT PURPOSE AND MOVED TO PROD IT ONLY REMAINS TO REQUEST THAT
* JOB PNA9997R BE RUN. WORKFILE NAME IS 'PNA.COR.NASB999.CORFIX.PIN'
* BUT THIS CAN BE OVERRIDDEN WHEN THE JOB IS EXECUTED.
*
* PLEASE BE CAREFUL TO ENSURE THAT THE CURRENT PURPOSE FOR THIS PROGRAM
* IS COMPLETE BEFORE IMPLEMENTING A NEW ONE.
*
* TO USE THIS PROG FOR SUCH A CHANGE FIRST SAVE IT UNDER ANOTHER NAME,
* SUCH AS NASB999A, NASB999B ETC., DOCUMENT BELOW AS SPECIFIED AND
* REMOVE ALL THE OLD CODE.
************************************************************************
* DOCUMENT HERE USE OF THIS PROG ON THIS OCCASION AND THE NAME UNDER
* WHICH THE PREVIOUS NASB999 HAS BEEN SAVED AS.
************************************************************************
* DATE        USERID    DESCRIPTION
*
* 12/10/2015  MEADED    ADD NEW SIP's for Kathy Orefice.
************************************************************************

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nasb999 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma300 pdaMdma300;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_I_Pin;
    private DbsField pnd_Input_Record_Pnd_I_Fil_1;
    private DbsField pnd_Input_Record_Pnd_I_Ssn;
    private DbsField pnd_Input_Record_Pnd_I_Fil_2;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa;
    private DbsField pnd_Input_Record_Pnd_I_Fil_3;
    private DbsField pnd_Input_Record_Pnd_I_Cref;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_I_Cref_Prefix;
    private DbsField pnd_Input_Record_Pnd_I_Cref_Nbr;
    private DbsField pnd_Input_Record_Pnd_I_Cref_Digit;
    private DbsField pnd_Input_Record_Pnd_I_Fil_4;
    private DbsField pnd_Input_Record_Pnd_I_Issdt;
    private DbsField pnd_Temp_Universal_Data;

    private DbsGroup pnd_Temp_Universal_Data__R_Field_3;
    private DbsField pnd_Temp_Universal_Data_Pnd_T_Cstdl_Agrmnt;
    private DbsField pnd_Temp_Universal_Data_Pnd_T_Aas_Ind;
    private DbsField pnd_Temp_Universal_Data_Pnd_T_Plat_Ind;
    private DbsField pnd_Temp_Universal_Data_Pnd_T_Multi_Ind;
    private DbsField pnd_Temp_Universal_Data_Pnd_T_Tnt1_Ind;
    private DbsField pnd_Temp_Universal_Data_Pnd_T_Tnt2_Ind;
    private DbsField pnd_Temp_Universal_Data_Pnd_T_Prap_Ind;
    private DbsField pnd_Response;

    private DbsGroup pnd_Response__R_Field_4;
    private DbsField pnd_Response_Pnd_Resp_Code;
    private DbsField pnd_Response_Pnd_Resp_Text;
    private DbsField pnd_Added;
    private DbsField pnd_Total_Added;
    private DbsField pnd_Total_Error;
    private DbsField pnd_Total_Read;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma300 = new PdaMdma300(localVariables);

        // Local Variables
        pnd_Input_Record = localVariables.newFieldArrayInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 1, new DbsArrayController(1, 44));

        pnd_Input_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_I_Pin = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_I_Pin", "#I-PIN", FieldType.NUMERIC, 7);
        pnd_Input_Record_Pnd_I_Fil_1 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_I_Fil_1", "#I-FIL-1", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_I_Ssn = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_I_Ssn", "#I-SSN", FieldType.NUMERIC, 9);
        pnd_Input_Record_Pnd_I_Fil_2 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_I_Fil_2", "#I-FIL-2", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_I_Tiaa = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa", "#I-TIAA", FieldType.STRING, 8);
        pnd_Input_Record_Pnd_I_Fil_3 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_I_Fil_3", "#I-FIL-3", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_I_Cref = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_I_Cref", "#I-CREF", FieldType.STRING, 8);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_I_Cref);
        pnd_Input_Record_Pnd_I_Cref_Prefix = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_I_Cref_Prefix", "#I-CREF-PREFIX", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_I_Cref_Nbr = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_I_Cref_Nbr", "#I-CREF-NBR", FieldType.STRING, 
            6);
        pnd_Input_Record_Pnd_I_Cref_Digit = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_I_Cref_Digit", "#I-CREF-DIGIT", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_I_Fil_4 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_I_Fil_4", "#I-FIL-4", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_I_Issdt = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_I_Issdt", "#I-ISSDT", FieldType.NUMERIC, 8);
        pnd_Temp_Universal_Data = localVariables.newFieldInRecord("pnd_Temp_Universal_Data", "#TEMP-UNIVERSAL-DATA", FieldType.STRING, 150);

        pnd_Temp_Universal_Data__R_Field_3 = localVariables.newGroupInRecord("pnd_Temp_Universal_Data__R_Field_3", "REDEFINE", pnd_Temp_Universal_Data);
        pnd_Temp_Universal_Data_Pnd_T_Cstdl_Agrmnt = pnd_Temp_Universal_Data__R_Field_3.newFieldInGroup("pnd_Temp_Universal_Data_Pnd_T_Cstdl_Agrmnt", 
            "#T-CSTDL-AGRMNT", FieldType.STRING, 1);
        pnd_Temp_Universal_Data_Pnd_T_Aas_Ind = pnd_Temp_Universal_Data__R_Field_3.newFieldInGroup("pnd_Temp_Universal_Data_Pnd_T_Aas_Ind", "#T-AAS-IND", 
            FieldType.STRING, 1);
        pnd_Temp_Universal_Data_Pnd_T_Plat_Ind = pnd_Temp_Universal_Data__R_Field_3.newFieldInGroup("pnd_Temp_Universal_Data_Pnd_T_Plat_Ind", "#T-PLAT-IND", 
            FieldType.STRING, 1);
        pnd_Temp_Universal_Data_Pnd_T_Multi_Ind = pnd_Temp_Universal_Data__R_Field_3.newFieldInGroup("pnd_Temp_Universal_Data_Pnd_T_Multi_Ind", "#T-MULTI-IND", 
            FieldType.STRING, 1);
        pnd_Temp_Universal_Data_Pnd_T_Tnt1_Ind = pnd_Temp_Universal_Data__R_Field_3.newFieldInGroup("pnd_Temp_Universal_Data_Pnd_T_Tnt1_Ind", "#T-TNT1-IND", 
            FieldType.STRING, 1);
        pnd_Temp_Universal_Data_Pnd_T_Tnt2_Ind = pnd_Temp_Universal_Data__R_Field_3.newFieldInGroup("pnd_Temp_Universal_Data_Pnd_T_Tnt2_Ind", "#T-TNT2-IND", 
            FieldType.STRING, 1);
        pnd_Temp_Universal_Data_Pnd_T_Prap_Ind = pnd_Temp_Universal_Data__R_Field_3.newFieldInGroup("pnd_Temp_Universal_Data_Pnd_T_Prap_Ind", "#T-PRAP-IND", 
            FieldType.STRING, 1);
        pnd_Response = localVariables.newFieldArrayInRecord("pnd_Response", "#RESPONSE", FieldType.STRING, 1, new DbsArrayController(1, 200));

        pnd_Response__R_Field_4 = localVariables.newGroupInRecord("pnd_Response__R_Field_4", "REDEFINE", pnd_Response);
        pnd_Response_Pnd_Resp_Code = pnd_Response__R_Field_4.newFieldInGroup("pnd_Response_Pnd_Resp_Code", "#RESP-CODE", FieldType.STRING, 4);
        pnd_Response_Pnd_Resp_Text = pnd_Response__R_Field_4.newFieldInGroup("pnd_Response_Pnd_Resp_Text", "#RESP-TEXT", FieldType.STRING, 100);
        pnd_Added = localVariables.newFieldInRecord("pnd_Added", "#ADDED", FieldType.STRING, 1);
        pnd_Total_Added = localVariables.newFieldInRecord("pnd_Total_Added", "#TOTAL-ADDED", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Error = localVariables.newFieldInRecord("pnd_Total_Error", "#TOTAL-ERROR", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Read = localVariables.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.PACKED_DECIMAL, 10);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pls_Trace.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Nasb999() throws Exception
    {
        super("Nasb999");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 60 LS = 200 SG = OFF ZP = ON;//Natural: FORMAT ( 1 ) PS = 60 LS = 200 SG = OFF ZP = ON
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        pnd_Response.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue("*"));                                                       //Natural: ASSIGN #RESPONSE ( * ) := ##DATA-RESPONSE ( * )
        getReports().write(0, ReportOption.NOTITLE,"MDMP0011 :","RC :",pnd_Response_Pnd_Resp_Code,"TEXT :",pnd_Response_Pnd_Resp_Text);                                   //Natural: WRITE 'MDMP0011 :' 'RC :' #RESP-CODE 'TEXT :' #RESP-TEXT
        if (Global.isEscape()) return;
        //*  OR= ' '
        if (condition(pnd_Response_Pnd_Resp_Code.notEquals("0000")))                                                                                                      //Natural: IF #RESP-CODE NE '0000'
        {
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-RECORD ( * )
        while (condition(getWorkFiles().read(1, pnd_Input_Record.getValue("*"))))
        {
            pnd_Total_Read.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-READ
            pdaMdma300.getPnd_Mdma300().reset();                                                                                                                          //Natural: RESET #MDMA300
            pdaMdma300.getPnd_Mdma300_Pnd_Requestor().setValue("OMNIFIX");                                                                                                //Natural: ASSIGN #REQUESTOR := 'OMNIFIX'
            pdaMdma300.getPnd_Mdma300_Pnd_Function_Code().setValue("005");                                                                                                //Natural: ASSIGN #FUNCTION-CODE := '005'
            pdaMdma300.getPnd_Mdma300_Pnd_Pin().setValue(pnd_Input_Record_Pnd_I_Pin);                                                                                     //Natural: ASSIGN #PIN := #I-PIN
            pdaMdma300.getPnd_Mdma300_Pnd_Soc_Sec_Nbr().setValue(pnd_Input_Record_Pnd_I_Ssn);                                                                             //Natural: ASSIGN #SOC-SEC-NBR := #I-SSN
            pdaMdma300.getPnd_Mdma300_Pnd_Da_Ownership_Code().getValue(1).setValue("0");                                                                                  //Natural: ASSIGN #DA-OWNERSHIP-CODE ( 1 ) := '0'
            pdaMdma300.getPnd_Mdma300_Pnd_Contract_Table_Count().setValue(1);                                                                                             //Natural: ASSIGN #CONTRACT-TABLE-COUNT := 1
            pdaMdma300.getPnd_Mdma300_Pnd_Contract().getValue(1).setValue(pnd_Input_Record_Pnd_I_Tiaa);                                                                   //Natural: ASSIGN #CONTRACT ( 1 ) := #I-TIAA
            pdaMdma300.getPnd_Mdma300_Pnd_Cref_Prefix().getValue(1).setValue(pnd_Input_Record_Pnd_I_Cref_Prefix);                                                         //Natural: ASSIGN #CREF-PREFIX ( 1 ) := #I-CREF-PREFIX
            pdaMdma300.getPnd_Mdma300_Pnd_Cref_Nbr().getValue(1).setValue(pnd_Input_Record_Pnd_I_Cref_Nbr);                                                               //Natural: ASSIGN #CREF-NBR ( 1 ) := #I-CREF-NBR
            pdaMdma300.getPnd_Mdma300_Pnd_Cref_Digit().getValue(1).setValue(pnd_Input_Record_Pnd_I_Cref_Digit);                                                           //Natural: ASSIGN #CREF-DIGIT ( 1 ) := #I-CREF-DIGIT
            pdaMdma300.getPnd_Mdma300_Pnd_Cref_Filler().getValue(1).setValue("  ");                                                                                       //Natural: ASSIGN #CREF-FILLER ( 1 ) := '  '
            pdaMdma300.getPnd_Mdma300_Pnd_Cref_Issued_Ind().getValue(1).setValue("Y");                                                                                    //Natural: ASSIGN #CREF-ISSUED-IND ( 1 ) := 'Y'
            pdaMdma300.getPnd_Mdma300_Pnd_Ia_Option_Code().getValue(1).setValue(" ");                                                                                     //Natural: ASSIGN #IA-OPTION-CODE ( 1 ) := ' '
            pdaMdma300.getPnd_Mdma300_Pnd_Ins_Plan_Code().getValue(1).setValue(" ");                                                                                      //Natural: ASSIGN #INS-PLAN-CODE ( 1 ) := ' '
            pdaMdma300.getPnd_Mdma300_Pnd_Mf_Social_Cde().getValue(1).setValue(" ");                                                                                      //Natural: ASSIGN #MF-SOCIAL-CDE ( 1 ) := ' '
            pdaMdma300.getPnd_Mdma300_Pnd_Issue_Date().getValue(1).setValue(pnd_Input_Record_Pnd_I_Issdt);                                                                //Natural: ASSIGN #ISSUE-DATE ( 1 ) := #I-ISSDT
            pdaMdma300.getPnd_Mdma300_Pnd_Status_Code().getValue(1).setValue("H");                                                                                        //Natural: ASSIGN #STATUS-CODE ( 1 ) := 'H'
            pdaMdma300.getPnd_Mdma300_Pnd_Status_Year().getValue(1).setValue(2015);                                                                                       //Natural: ASSIGN #STATUS-YEAR ( 1 ) := 2015
            pdaMdma300.getPnd_Mdma300_Pnd_Payee_Code().getValue(1).setValue("01");                                                                                        //Natural: ASSIGN #PAYEE-CODE ( 1 ) := '01'
            pnd_Temp_Universal_Data.reset();                                                                                                                              //Natural: RESET #TEMP-UNIVERSAL-DATA
            pnd_Temp_Universal_Data_Pnd_T_Cstdl_Agrmnt.setValue(" ");                                                                                                     //Natural: ASSIGN #T-CSTDL-AGRMNT := ' '
            pnd_Temp_Universal_Data_Pnd_T_Aas_Ind.setValue(" ");                                                                                                          //Natural: ASSIGN #T-AAS-IND := ' '
            pnd_Temp_Universal_Data_Pnd_T_Plat_Ind.setValue("S");                                                                                                         //Natural: ASSIGN #T-PLAT-IND := 'S'
            pnd_Temp_Universal_Data_Pnd_T_Multi_Ind.setValue(" ");                                                                                                        //Natural: ASSIGN #T-MULTI-IND := ' '
            pnd_Temp_Universal_Data_Pnd_T_Tnt1_Ind.setValue(" ");                                                                                                         //Natural: ASSIGN #T-TNT1-IND := ' '
            pnd_Temp_Universal_Data_Pnd_T_Tnt2_Ind.setValue(" ");                                                                                                         //Natural: ASSIGN #T-TNT2-IND := ' '
            pnd_Temp_Universal_Data_Pnd_T_Prap_Ind.setValue(" ");                                                                                                         //Natural: ASSIGN #T-PRAP-IND := ' '
            pdaMdma300.getPnd_Mdma300_Pnd_Cntrct_Universal_Data().getValue(1).setValue(pnd_Temp_Universal_Data);                                                          //Natural: ASSIGN #CNTRCT-UNIVERSAL-DATA ( 1 ) := #TEMP-UNIVERSAL-DATA
            pdaMdma300.getPnd_Mdma300_Pnd_Continuation_Code().setValue("N");                                                                                              //Natural: ASSIGN #CONTINUATION-CODE := 'N'
            DbsUtil.callnat(Mdmn300a.class , getCurrentProcessState(), pdaMdma300.getPnd_Mdma300());                                                                      //Natural: CALLNAT 'MDMN300A' #MDMA300
            if (condition(Global.isEscape())) return;
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Code   :",pdaMdma300.getPnd_Mdma300_Pnd_Return_Code(),NEWLINE,"Text   :",pdaMdma300.getPnd_Mdma300_Pnd_Return_Text()); //Natural: WRITE NOTITLE / 'Code   :' #RETURN-CODE / 'Text   :' #RETURN-TEXT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaMdma300.getPnd_Mdma300_Pnd_Return_Code().equals(0)))                                                                                         //Natural: IF #RETURN-CODE = 0000
            {
                pnd_Total_Added.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOTAL-ADDED
                pnd_Added.setValue("Y");                                                                                                                                  //Natural: ASSIGN #ADDED := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Total_Error.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOTAL-ERROR
                pnd_Added.setValue(" ");                                                                                                                                  //Natural: ASSIGN #ADDED := ' '
                getReports().write(0, ReportOption.NOTITLE,"ERROR",pdaMdma300.getPnd_Mdma300_Pnd_Return_Text(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"));   //Natural: WRITE 'ERROR' #RETURN-TEXT ( EM = X ( 30 ) )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, ReportOption.NOTITLE,"SSN",                                                                                                           //Natural: DISPLAY ( 1 ) NOTITLE 'SSN' #I-SSN ( EM = 999-99-9999 ) 'PIN' #I-PIN 'Contract' #I-TIAA 'CREF Cert' #I-CREF 'Issue Date' #I-ISSDT 'Cntrct Added' #ADDED
            		pnd_Input_Record_Pnd_I_Ssn, new ReportEditMask ("999-99-9999"),"PIN",
            		pnd_Input_Record_Pnd_I_Pin,"Contract",
            		pnd_Input_Record_Pnd_I_Tiaa,"CREF Cert",
            		pnd_Input_Record_Pnd_I_Cref,"Issue Date",
            		pnd_Input_Record_Pnd_I_Issdt,"Cntrct Added",
            		pnd_Added);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        getReports().write(1, "Total Input :",pnd_Total_Read, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                                                      //Natural: WRITE ( 1 ) 'Total Input :' #TOTAL-READ
        if (Global.isEscape()) return;
        getReports().write(1, "Total Added :",pnd_Total_Added, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                                                     //Natural: WRITE ( 1 ) 'Total Added :' #TOTAL-ADDED
        if (Global.isEscape()) return;
        getReports().write(1, "Total Error :",pnd_Total_Error, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                                                     //Natural: WRITE ( 1 ) 'Total Error :' #TOTAL-ERROR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=200 SG=OFF ZP=ON");
        Global.format(1, "PS=60 LS=200 SG=OFF ZP=ON");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"SSN",
        		pnd_Input_Record_Pnd_I_Ssn, new ReportEditMask ("999-99-9999"),"PIN",
        		pnd_Input_Record_Pnd_I_Pin,"Contract",
        		pnd_Input_Record_Pnd_I_Tiaa,"CREF Cert",
        		pnd_Input_Record_Pnd_I_Cref,"Issue Date",
        		pnd_Input_Record_Pnd_I_Issdt,"Cntrct Added",
        		pnd_Added);
    }
}
