/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:59:39 PM
**        * FROM NATURAL PROGRAM : Nasb495k
************************************************************
**        * FILE NAME            : Nasb495k.java
**        * CLASS NAME           : Nasb495k
**        * INSTANCE NAME        : Nasb495k
************************************************************
************************************************************************
* PROGRAM    : NASB495K
* CREATED BY : C.DIMITRIU
* DATE       : AUG 2017
* DESCRIPTION: THIS PROGRAM READ WORK FILE FROM NASB491. FORMAT LETTER
*              LAYOUT AND SEND TO POST SYSTEM
************************************************************************
*    DATE      USERID                 DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 09/09/2002  GOPAZ     RESET PSTA9612
* 11/18/2002  GOPAZ     FIXED INDEX ARRAY PROBLEM
* 06/29/2005  GOPAZ     DO NOT SEND UNIT TO POST
*                       ONLY LAST 4 DIGIT OF BANK ACCT WILL BE PRINTED
* 10/19/2009  CABIG     CONVERT LOWER CASE ADDRESS TO UPPER CASE
*                       FOR CONTRACT TYPE D, IF ZIP ALREADY PART OF THE
*                       ADDRESS, ADDRESS-ZIP WILL NOT BE APPEND
* 10/22/2009  CABIG     REMOVED NASN740 AND REPLACE WITH SYSTEM DATE
* 06/27/2017  MEADED    PIN EXPANSION. SEE PINE COMMENTS
* 07/23/2019  MEADED    CALL PSTN9685 (NOT PSTN9685) USING PSTA9612
*                       (NOT PSTA9611) IN BACKOUT ROUTINE. THIS WAS
*                       MISSED IN PIN EXPANSION WORK.
* 01/08/2020  FRANKLR   ADD A DELAY OF 1 SECOND IF THE PREVIOUS PIN IS
*                       SAME AS THE CURRENT PIN BEFORE CALLING PSTN9612.
*                                                   SEE TAG PRB97743
************************************************************************

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nasb495k extends BLNatBase
{
    // Data Areas
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9611 pdaPsta9611;
    private PdaPsta9612 pdaPsta9612;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Prev_Pin_No;
    private DbsField pnd_Hold_Time;
    private DbsField pnd_Time;

    private DbsGroup pnd_Work_Letter_Image_File;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Pin_No;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Contract_No;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Actvty_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Usage;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Change_Dt;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Change_Tm;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Effective_Date;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_1;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Effective_Date_A;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Verify_Ind;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Destination;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Name;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_1;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_3;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Address_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Zip_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Zip_Plus_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Carrier_Route;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Walk_Route;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Geo_Code;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Lob;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Contract_Type_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Type;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Name;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_1;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_3;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Address_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_3;
    private DbsField pnd_Work_Letter_Image_File_Pnd_B_Ccyy;
    private DbsField pnd_Work_Letter_Image_File_Pnd_B_Mm;
    private DbsField pnd_Work_Letter_Image_File_Pnd_B_Dd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_E_Ccyy;
    private DbsField pnd_Work_Letter_Image_File_Pnd_E_Mm;
    private DbsField pnd_Work_Letter_Image_File_Pnd_E_Dd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_R_Ccyy;
    private DbsField pnd_Work_Letter_Image_File_Pnd_R_Mm;
    private DbsField pnd_Work_Letter_Image_File_Pnd_R_Dd;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Section;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Status_Cde;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Mit_Error;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Compress_Address;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_M_Override;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_1;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_3;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_4;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Address_5;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Contract2;
    private DbsField pnd_Work_Letter_Image_File_Pnd_O_Filler;

    private DbsGroup pnd_Work_Letter_Image_File__R_Field_6;
    private DbsField pnd_Work_Letter_Image_File_Pnd_Work;

    private DbsGroup pnd_Co_Group;

    private DbsGroup pnd_Co_Group_Pnd_Co_Address;
    private DbsField pnd_Co_Group_Pnd_Co_Contract;
    private DbsField pnd_Co_Group_Pnd_Co_Address_Compare;
    private DbsField pnd_Co_Group_Pnd_Co_Name;
    private DbsField pnd_Co_Group_Pnd_Co_New_Mail_Addr1;
    private DbsField pnd_Co_Group_Pnd_Co_New_Mail_Addr2;
    private DbsField pnd_Co_Group_Pnd_Co_New_Mail_Addr3;
    private DbsField pnd_Co_Group_Pnd_Co_New_Mail_Addr4;
    private DbsField pnd_Co_Group_Pnd_Co_New_Mail_Addr5;
    private DbsField pnd_Co_Group_Pnd_Co_New_Mail_Zip;
    private DbsField pnd_Co_Group_Pnd_Co_C_Section;
    private DbsField pnd_Co_Group_Pnd_Co_Itd_Contract;

    private DbsGroup pnd_Cm_Group;

    private DbsGroup pnd_Cm_Group_Pnd_Cm_Address;
    private DbsField pnd_Cm_Group_Pnd_Cm_Contract;
    private DbsField pnd_Cm_Group_Pnd_Cm_Address_Compare;
    private DbsField pnd_Cm_Group_Pnd_Cm_Name;
    private DbsField pnd_Cm_Group_Pnd_Cm_Pay_Mail_Ind;
    private DbsField pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr1;
    private DbsField pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr2;
    private DbsField pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr3;
    private DbsField pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr4;
    private DbsField pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr5;
    private DbsField pnd_Cm_Group_Pnd_Cm_Pay_Mail_Zip;
    private DbsField pnd_Cm_Group_Pnd_Cm_Pay_Bnk_Acct_Nbr;
    private DbsField pnd_Cm_Group_Pnd_Cm_Pay_Cs_Ind;
    private DbsField pnd_Cm_Group_Pnd_Cm_Pay_Eft_Nbr;
    private DbsField pnd_Cm_Group_Pnd_Cm_Pay_Bank_Info_Ind;
    private DbsField pnd_Cm_Group_Pnd_Cm_Itd_Contract;

    private DbsGroup pnd_Fco_Group;

    private DbsGroup pnd_Fco_Group_Pnd_Fco_Address;
    private DbsField pnd_Fco_Group_Pnd_Fco_Contract;
    private DbsField pnd_Fco_Group_Pnd_Fco_Name;
    private DbsField pnd_Fco_Group_Pnd_Fco_Mail_Addr1;
    private DbsField pnd_Fco_Group_Pnd_Fco_Mail_Addr2;
    private DbsField pnd_Fco_Group_Pnd_Fco_Mail_Addr3;
    private DbsField pnd_Fco_Group_Pnd_Fco_Mail_Addr4;
    private DbsField pnd_Fco_Group_Pnd_Fco_Mail_Addr5;
    private DbsField pnd_Fco_Group_Pnd_Fco_Mail_Zip;
    private DbsField pnd_Fco_Group_Pnd_Fco_Address_Compare;
    private DbsField pnd_Fco_Group_Pnd_Fco_Begin_Dte;
    private DbsField pnd_Fco_Group_Pnd_Fco_Itd_Contract;

    private DbsGroup pnd_Fcm_Group;

    private DbsGroup pnd_Fcm_Group_Pnd_Fcm_Address;
    private DbsField pnd_Fcm_Group_Pnd_Fcm_Contract;
    private DbsField pnd_Fcm_Group_Pnd_Fcm_Name;
    private DbsField pnd_Fcm_Group_Pnd_Fcm_Pay_Addr1;
    private DbsField pnd_Fcm_Group_Pnd_Fcm_Pay_Addr2;
    private DbsField pnd_Fcm_Group_Pnd_Fcm_Pay_Addr3;
    private DbsField pnd_Fcm_Group_Pnd_Fcm_Pay_Addr4;
    private DbsField pnd_Fcm_Group_Pnd_Fcm_Pay_Addr5;
    private DbsField pnd_Fcm_Group_Pnd_Fcm_Pay_Zip;
    private DbsField pnd_Fcm_Group_Pnd_Fcm_Address_Compare;
    private DbsField pnd_Fcm_Group_Pnd_Fcm_Begin_Dte;
    private DbsField pnd_Fcm_Group_Pnd_Fcm_Itd_Contract;

    private DbsGroup pnd_Frs_Group;

    private DbsGroup pnd_Frs_Group_Pnd_Frs_Address;
    private DbsField pnd_Frs_Group_Pnd_Frs_Contract;
    private DbsField pnd_Frs_Group_Pnd_Frs_Name;
    private DbsField pnd_Frs_Group_Pnd_Frs_Mail_Addr;
    private DbsField pnd_Frs_Group_Pnd_Frs_Mail_Zip;
    private DbsField pnd_Frs_Group_Pnd_Frs_Address_Compare;
    private DbsField pnd_Frs_Group_Pnd_Frs_Begin_Dte;

    private DbsGroup pnd_Rs_Group;

    private DbsGroup pnd_Rs_Group_Pnd_Rs_Address;
    private DbsField pnd_Rs_Group_Pnd_Rs_Name;
    private DbsField pnd_Rs_Group_Pnd_Rs_New_Mail_Addr;
    private DbsField pnd_Rs_Group_Pnd_Rs_New_Mail_Zip;
    private DbsField pnd_Rs_Group_Pnd_Rs_Address_Compare;

    private DbsGroup pnd_Vco_Group;

    private DbsGroup pnd_Vco_Group_Pnd_Vco_Address;
    private DbsField pnd_Vco_Group_Pnd_Vco_Contract;
    private DbsField pnd_Vco_Group_Pnd_Vco_Name;
    private DbsField pnd_Vco_Group_Pnd_Vco_Mail_Addr1;
    private DbsField pnd_Vco_Group_Pnd_Vco_Mail_Addr2;
    private DbsField pnd_Vco_Group_Pnd_Vco_Mail_Addr3;
    private DbsField pnd_Vco_Group_Pnd_Vco_Mail_Addr4;
    private DbsField pnd_Vco_Group_Pnd_Vco_Mail_Addr5;
    private DbsField pnd_Vco_Group_Pnd_Vco_Mail_Zip;
    private DbsField pnd_Vco_Group_Pnd_Vco_Address_Compare;
    private DbsField pnd_Vco_Group_Pnd_Vco_Annual_Cycle_Ind;
    private DbsField pnd_Vco_Group_Pnd_Vco_Begin_Dte;
    private DbsField pnd_Vco_Group_Pnd_Vco_End_Dte;
    private DbsField pnd_Vco_Group_Pnd_Vco_Itd_Contract;

    private DbsGroup pnd_Vcm_Group;

    private DbsGroup pnd_Vcm_Group_Pnd_Vcm_Address;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_Contract;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_Name;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_Pay_Addr1;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_Pay_Addr2;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_Pay_Addr3;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_Pay_Addr4;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_Pay_Addr5;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_Pay_Zip;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_Address_Compare;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_Annual_Cycle_Ind;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_Begin_Dte;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_End_Dte;
    private DbsField pnd_Vcm_Group_Pnd_Vcm_Itd_Contract;
    private DbsField pnd_Address_Compare;

    private DbsGroup pnd_Bank;
    private DbsField pnd_Bank_Pnd_Bank_Acct;

    private DbsGroup pnd_Bank__R_Field_7;
    private DbsField pnd_Bank_Pnd_Bank_Acct_A;
    private DbsField pnd_Bank_Pnd_Eft_Acct;

    private DbsGroup pnd_Bank__R_Field_8;
    private DbsField pnd_Bank_Pnd_Eft_Acct_A;
    private DbsField pnd_Close;
    private DbsField pnd_Close_Cnt;
    private DbsField pnd_Cm_Ctr;
    private DbsField pnd_Cm_Cnt;
    private DbsField pnd_Cm_Idx;
    private DbsField pnd_Cm_Post;
    private DbsField pnd_Cm_Pntr;
    private DbsField pnd_Cm_Reach;
    private DbsField pnd_Cm_Start;
    private DbsField pnd_Cm_Write;
    private DbsField pnd_Co_Cnt;
    private DbsField pnd_Co_Ctr;
    private DbsField pnd_Co_Idx;
    private DbsField pnd_Co_Post;
    private DbsField pnd_Co_Pntr;
    private DbsField pnd_Co_Reach;
    private DbsField pnd_Co_Start;
    private DbsField pnd_Co_Write;
    private DbsField pnd_Contract_Write1;
    private DbsField pnd_Contract_Write2;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_J;
    private DbsField pnd_Counters_Pnd_K;
    private DbsField pnd_Counters_Pnd_L;
    private DbsField pnd_Counters_Pnd_X;
    private DbsField pnd_Counters_Pnd_Y;
    private DbsField pnd_Counters_Pnd_Z;
    private DbsField pnd_Error_Temp1;
    private DbsField pnd_Error_Temp2;
    private DbsField pnd_Error_Work;
    private DbsField pnd_Fcm_Cnt;
    private DbsField pnd_Fcm_Ctr;
    private DbsField pnd_Fcm_Idx;
    private DbsField pnd_Fcm_Post;
    private DbsField pnd_Fcm_Pntr;
    private DbsField pnd_Fcm_Reach;
    private DbsField pnd_Fcm_Start;
    private DbsField pnd_Fcm_Write;
    private DbsField pnd_Fco_Cnt;
    private DbsField pnd_Fco_Ctr;
    private DbsField pnd_Fco_Idx;
    private DbsField pnd_Fco_Post;
    private DbsField pnd_Fco_Pntr;
    private DbsField pnd_Fco_Reach;
    private DbsField pnd_Fco_Start;
    private DbsField pnd_Fco_Write;
    private DbsField pnd_Frs_Cnt;
    private DbsField pnd_Frs_Ctr;
    private DbsField pnd_Frs_Idx;
    private DbsField pnd_Frs_Reach;
    private DbsField pnd_Frs_Start;
    private DbsField pnd_Frs_Write;
    private DbsField pnd_First_Read;
    private DbsField pnd_First_Write;
    private DbsField pnd_Idx;
    private DbsField pnd_Index;
    private DbsField pnd_Letters_Created;
    private DbsField pnd_Loop;
    private DbsField pnd_Max_Reach;

    private DbsGroup pnd_Mit_Array;
    private DbsField pnd_Mit_Array_Pnd_A_Mit_Wpid;
    private DbsField pnd_Mit_Array_Pnd_A_Mit_Unit_Cde;
    private DbsField pnd_Mit_Array_Pnd_A_Mit_Log_Date_Time;
    private DbsField pnd_Mit_Array_Pnd_A_Mit_Status_Cde;
    private DbsField pnd_Mit_Ctr;
    private DbsField pnd_Mit_Pntr;
    private DbsField pnd_Ndx;
    private DbsField pnd_Ndx1;
    private DbsField pnd_Open_Cnt;
    private DbsField pnd_Output_Date;
    private DbsField pnd_Output_Time;
    private DbsField pnd_Post_Cnt;
    private DbsField pnd_Post_Ctr;
    private DbsField pnd_Post_Mmddyyyy;

    private DbsGroup pnd_Post_Mmddyyyy__R_Field_9;
    private DbsField pnd_Post_Mmddyyyy_Pnd_P_Mm;
    private DbsField pnd_Post_Mmddyyyy_Pnd_P_Dd;
    private DbsField pnd_Post_Mmddyyyy_Pnd_P_Ccyy;
    private DbsField pnd_Post_Error;
    private DbsField pnd_Post_Loop;
    private DbsField pnd_Rs_Cnt;
    private DbsField pnd_Rs_Ctr;
    private DbsField pnd_Rs_Idx;
    private DbsField pnd_Rs_Reach;
    private DbsField pnd_Rs_Start;
    private DbsField pnd_Rs_Write;
    private DbsField pnd_Mailto_C_Mit_Log_Date_Time;
    private DbsField pnd_Mailto_C_Mit_Status_Cde;
    private DbsField pnd_Mailto_C_Mit_Unit_Cde;
    private DbsField pnd_Mailto_C_Mit_Wpid;
    private DbsField pnd_Mailto_Compress_Addr;
    private DbsField pnd_Mailto_Lob;
    private DbsField pnd_Mailto_M_Address_1;
    private DbsField pnd_Mailto_M_Address_2;
    private DbsField pnd_Mailto_M_Address_3;
    private DbsField pnd_Mailto_M_Address_4;
    private DbsField pnd_Mailto_M_Address_5;
    private DbsField pnd_Mailto_M_Destination;
    private DbsField pnd_Mailto_M_Name;
    private DbsField pnd_Mailto_M_Postal_Data;
    private DbsField pnd_Mailto_Pin;
    private DbsField pnd_Vcm_Cnt;
    private DbsField pnd_Vcm_Ctr;
    private DbsField pnd_Vcm_Idx;
    private DbsField pnd_Vcm_Post;
    private DbsField pnd_Vcm_Pntr;
    private DbsField pnd_Vcm_Reach;
    private DbsField pnd_Vcm_Start;
    private DbsField pnd_Vcm_Write;
    private DbsField pnd_Vco_Cnt;
    private DbsField pnd_Vco_Ctr;
    private DbsField pnd_Vco_Idx;
    private DbsField pnd_Vco_Post;
    private DbsField pnd_Vco_Pntr;
    private DbsField pnd_Vco_Reach;
    private DbsField pnd_Vco_Start;
    private DbsField pnd_Vco_Write;
    private DbsField pnd_Write_Addr;
    private DbsField pnd_Write_Addr2;

    private DbsGroup sort_Key;
    private DbsField sort_Key_Sort_Key_Lgth;
    private DbsField sort_Key_Sort_Key_Array;

    private DbsGroup pstl6635_Data;
    private DbsField pstl6635_Data_Pstl6635_Rec_Id;
    private DbsField pstl6635_Data_Pstl6635_Rec_Lgth;
    private DbsField pstl6635_Data_Pstl6635_Rec_Occurs;
    private DbsField pstl6635_Data_Pstl6635_Data_Array;

    private DbsGroup pstl6635_Data__R_Field_10;

    private DbsGroup pstl6635_Data_Addr_Chng_Dtls;
    private DbsField pstl6635_Data_Addr_Chng_Type;
    private DbsField pstl6635_Data_Addr_Line;
    private DbsField pstl6635_Data_Addr_Cntrct;
    private DbsField pstl6635_Data_New_Pay_Bank_Info_Ind;
    private DbsField pstl6635_Data_New_Pay_Bnk_Acct_Nbr;
    private DbsField pstl6635_Data_New_Pay_Cs_Ind;
    private DbsField pstl6635_Data_New_Pay_Eft_Nbr;
    private DbsField pstl6635_Data_Bgn_Effct_Dte;
    private DbsField pstl6635_Data_End_Effct_Dte;
    private DbsField pstl6635_Data_Tmp_Annual_Addr_Ind;
    private DbsField pstl6635_Data_Itd_Contract_Out;
    private DbsField pnd_Hold_Contract;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9611 = new PdaPsta9611(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);

        // Local Variables
        pnd_Prev_Pin_No = localVariables.newFieldInRecord("pnd_Prev_Pin_No", "#PREV-PIN-NO", FieldType.NUMERIC, 12);
        pnd_Hold_Time = localVariables.newFieldInRecord("pnd_Hold_Time", "#HOLD-TIME", FieldType.TIME);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);

        pnd_Work_Letter_Image_File = localVariables.newGroupInRecord("pnd_Work_Letter_Image_File", "#WORK-LETTER-IMAGE-FILE");
        pnd_Work_Letter_Image_File_Pnd_O_Pin_No = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Pin_No", "#O-PIN-NO", FieldType.NUMERIC, 
            12);
        pnd_Work_Letter_Image_File_Pnd_O_Contract_No = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Contract_No", "#O-CONTRACT-NO", 
            FieldType.STRING, 10);
        pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd", "#O-PAYEE-CD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd", 
            "#O-ADDR-ACTVTY-CD", FieldType.STRING, 2);
        pnd_Work_Letter_Image_File_Pnd_O_Actvty_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Actvty_Cd", "#O-ACTVTY-CD", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_Usage = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Usage", "#O-USAGE", FieldType.STRING, 
            2);
        pnd_Work_Letter_Image_File_Pnd_O_Change_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Change_Dt", "#O-CHANGE-DT", 
            FieldType.NUMERIC, 8);
        pnd_Work_Letter_Image_File_Pnd_O_Change_Tm = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Change_Tm", "#O-CHANGE-TM", 
            FieldType.NUMERIC, 7);
        pnd_Work_Letter_Image_File_Pnd_O_Effective_Date = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Effective_Date", 
            "#O-EFFECTIVE-DATE", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_1 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_1", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_Effective_Date);
        pnd_Work_Letter_Image_File_Pnd_O_Effective_Date_A = pnd_Work_Letter_Image_File__R_Field_1.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Effective_Date_A", 
            "#O-EFFECTIVE-DATE-A", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_Verify_Ind = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Verify_Ind", "#O-VERIFY-IND", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_M_Destination = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Destination", 
            "#O-M-DESTINATION", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_M_Name = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Name", "#O-M-NAME", FieldType.STRING, 
            35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_1 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_1", "#O-M-ADDRESS-1", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_2 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_2", "#O-M-ADDRESS-2", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_3 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_3", "#O-M-ADDRESS-3", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_4 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_4", "#O-M-ADDRESS-4", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Address_5 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Address_5", "#O-M-ADDRESS-5", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data", 
            "#O-M-POSTAL-DATA", FieldType.STRING, 35);

        pnd_Work_Letter_Image_File__R_Field_2 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_2", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data);
        pnd_Work_Letter_Image_File_Pnd_Zip_5 = pnd_Work_Letter_Image_File__R_Field_2.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Zip_5", "#ZIP-5", 
            FieldType.STRING, 5);
        pnd_Work_Letter_Image_File_Pnd_Zip_Plus_4 = pnd_Work_Letter_Image_File__R_Field_2.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Zip_Plus_4", 
            "#ZIP-PLUS-4", FieldType.STRING, 4);
        pnd_Work_Letter_Image_File_Pnd_Carrier_Route = pnd_Work_Letter_Image_File__R_Field_2.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Carrier_Route", 
            "#CARRIER-ROUTE", FieldType.STRING, 4);
        pnd_Work_Letter_Image_File_Pnd_Walk_Route = pnd_Work_Letter_Image_File__R_Field_2.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Walk_Route", 
            "#WALK-ROUTE", FieldType.STRING, 4);
        pnd_Work_Letter_Image_File_Pnd_Geo_Code = pnd_Work_Letter_Image_File__R_Field_2.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Geo_Code", "#GEO-CODE", 
            FieldType.STRING, 2);
        pnd_Work_Letter_Image_File_Pnd_O_M_Lob = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Lob", "#O-M-LOB", FieldType.STRING, 
            1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Contract_Type_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Contract_Type_Cd", 
            "#O-C-CONTRACT-TYPE-CD", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Type = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Type", "#O-C-ADDR-TYPE", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd", 
            "#O-C-ADDR-FORMAT-CD", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt", 
            "#O-C-ADDR-LAST-CH-DT", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm", 
            "#O-C-ADDR-LAST-CH-TM", FieldType.NUMERIC, 7);
        pnd_Work_Letter_Image_File_Pnd_O_C_Name = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Name", "#O-C-NAME", FieldType.STRING, 
            35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_1 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_1", "#O-C-ADDRESS-1", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_2 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_2", "#O-C-ADDRESS-2", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_3 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_3", "#O-C-ADDRESS-3", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_4 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_4", "#O-C-ADDRESS-4", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Address_5 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Address_5", "#O-C-ADDRESS-5", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data", 
            "#O-C-POSTAL-DATA", FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind", 
            "#O-C-ANNUAL-VAC-CYCLE-IND", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No", "#O-C-PH-BANK-NO", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd", 
            "#O-C-CHECK-SAVING-CD", FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No", 
            "#O-C-ABA-ACCOUNT-NO", FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt", 
            "#O-C-PENDING-ADDR-CH-DT", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_3 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_3", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Ch_Dt);
        pnd_Work_Letter_Image_File_Pnd_B_Ccyy = pnd_Work_Letter_Image_File__R_Field_3.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_B_Ccyy", "#B-CCYY", 
            FieldType.STRING, 4);
        pnd_Work_Letter_Image_File_Pnd_B_Mm = pnd_Work_Letter_Image_File__R_Field_3.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_B_Mm", "#B-MM", FieldType.STRING, 
            2);
        pnd_Work_Letter_Image_File_Pnd_B_Dd = pnd_Work_Letter_Image_File__R_Field_3.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_B_Dd", "#B-DD", FieldType.STRING, 
            2);
        pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt", 
            "#O-C-PENDING-ADDR-RS-DT", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_4 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_4", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Pending_Addr_Rs_Dt);
        pnd_Work_Letter_Image_File_Pnd_E_Ccyy = pnd_Work_Letter_Image_File__R_Field_4.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_E_Ccyy", "#E-CCYY", 
            FieldType.STRING, 4);
        pnd_Work_Letter_Image_File_Pnd_E_Mm = pnd_Work_Letter_Image_File__R_Field_4.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_E_Mm", "#E-MM", FieldType.STRING, 
            2);
        pnd_Work_Letter_Image_File_Pnd_E_Dd = pnd_Work_Letter_Image_File__R_Field_4.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_E_Dd", "#E-DD", FieldType.STRING, 
            2);
        pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt", 
            "#O-C-PERM-ADDRESS-CH-DT", FieldType.NUMERIC, 8);

        pnd_Work_Letter_Image_File__R_Field_5 = pnd_Work_Letter_Image_File.newGroupInGroup("pnd_Work_Letter_Image_File__R_Field_5", "REDEFINE", pnd_Work_Letter_Image_File_Pnd_O_C_Perm_Address_Ch_Dt);
        pnd_Work_Letter_Image_File_Pnd_R_Ccyy = pnd_Work_Letter_Image_File__R_Field_5.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_R_Ccyy", "#R-CCYY", 
            FieldType.STRING, 4);
        pnd_Work_Letter_Image_File_Pnd_R_Mm = pnd_Work_Letter_Image_File__R_Field_5.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_R_Mm", "#R-MM", FieldType.STRING, 
            2);
        pnd_Work_Letter_Image_File_Pnd_R_Dd = pnd_Work_Letter_Image_File__R_Field_5.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_R_Dd", "#R-DD", FieldType.STRING, 
            2);
        pnd_Work_Letter_Image_File_Pnd_O_C_Section = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Section", "#O-C-SECTION", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid", "#O-C-MIT-WPID", 
            FieldType.STRING, 6);
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde", 
            "#O-C-MIT-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time", 
            "#O-C-MIT-LOG-DATE-TIME", FieldType.STRING, 15);
        pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Status_Cde = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Status_Cde", 
            "#O-C-MIT-STATUS-CDE", FieldType.STRING, 4);
        pnd_Work_Letter_Image_File_Pnd_Mit_Error = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Mit_Error", "#MIT-ERROR", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_Compress_Address = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_Compress_Address", 
            "#COMPRESS-ADDRESS", FieldType.STRING, 30);
        pnd_Work_Letter_Image_File_Pnd_O_M_Override = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_M_Override", "#O-M-OVERRIDE", 
            FieldType.STRING, 1);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_1 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_1", "#O-R-ADDRESS-1", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_2 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_2", "#O-R-ADDRESS-2", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_3 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_3", "#O-R-ADDRESS-3", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_4 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_4", "#O-R-ADDRESS-4", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Address_5 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Address_5", "#O-R-ADDRESS-5", 
            FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data", 
            "#O-R-POSTAL-DATA", FieldType.STRING, 35);
        pnd_Work_Letter_Image_File_Pnd_O_Contract2 = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Contract2", "#O-CONTRACT2", 
            FieldType.STRING, 10);
        pnd_Work_Letter_Image_File_Pnd_O_Filler = pnd_Work_Letter_Image_File.newFieldInGroup("pnd_Work_Letter_Image_File_Pnd_O_Filler", "#O-FILLER", FieldType.STRING, 
            55);

        pnd_Work_Letter_Image_File__R_Field_6 = localVariables.newGroupInRecord("pnd_Work_Letter_Image_File__R_Field_6", "REDEFINE", pnd_Work_Letter_Image_File);
        pnd_Work_Letter_Image_File_Pnd_Work = pnd_Work_Letter_Image_File__R_Field_6.newFieldArrayInGroup("pnd_Work_Letter_Image_File_Pnd_Work", "#WORK", 
            FieldType.STRING, 1, new DbsArrayController(1, 1000));

        pnd_Co_Group = localVariables.newGroupInRecord("pnd_Co_Group", "#CO-GROUP");

        pnd_Co_Group_Pnd_Co_Address = pnd_Co_Group.newGroupArrayInGroup("pnd_Co_Group_Pnd_Co_Address", "#CO-ADDRESS", new DbsArrayController(1, 20));
        pnd_Co_Group_Pnd_Co_Contract = pnd_Co_Group_Pnd_Co_Address.newFieldArrayInGroup("pnd_Co_Group_Pnd_Co_Contract", "#CO-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1, 20));
        pnd_Co_Group_Pnd_Co_Address_Compare = pnd_Co_Group_Pnd_Co_Address.newFieldInGroup("pnd_Co_Group_Pnd_Co_Address_Compare", "#CO-ADDRESS-COMPARE", 
            FieldType.STRING, 35);
        pnd_Co_Group_Pnd_Co_Name = pnd_Co_Group_Pnd_Co_Address.newFieldInGroup("pnd_Co_Group_Pnd_Co_Name", "#CO-NAME", FieldType.STRING, 35);
        pnd_Co_Group_Pnd_Co_New_Mail_Addr1 = pnd_Co_Group_Pnd_Co_Address.newFieldInGroup("pnd_Co_Group_Pnd_Co_New_Mail_Addr1", "#CO-NEW-MAIL-ADDR1", FieldType.STRING, 
            35);
        pnd_Co_Group_Pnd_Co_New_Mail_Addr2 = pnd_Co_Group_Pnd_Co_Address.newFieldInGroup("pnd_Co_Group_Pnd_Co_New_Mail_Addr2", "#CO-NEW-MAIL-ADDR2", FieldType.STRING, 
            35);
        pnd_Co_Group_Pnd_Co_New_Mail_Addr3 = pnd_Co_Group_Pnd_Co_Address.newFieldInGroup("pnd_Co_Group_Pnd_Co_New_Mail_Addr3", "#CO-NEW-MAIL-ADDR3", FieldType.STRING, 
            35);
        pnd_Co_Group_Pnd_Co_New_Mail_Addr4 = pnd_Co_Group_Pnd_Co_Address.newFieldInGroup("pnd_Co_Group_Pnd_Co_New_Mail_Addr4", "#CO-NEW-MAIL-ADDR4", FieldType.STRING, 
            35);
        pnd_Co_Group_Pnd_Co_New_Mail_Addr5 = pnd_Co_Group_Pnd_Co_Address.newFieldInGroup("pnd_Co_Group_Pnd_Co_New_Mail_Addr5", "#CO-NEW-MAIL-ADDR5", FieldType.STRING, 
            35);
        pnd_Co_Group_Pnd_Co_New_Mail_Zip = pnd_Co_Group_Pnd_Co_Address.newFieldInGroup("pnd_Co_Group_Pnd_Co_New_Mail_Zip", "#CO-NEW-MAIL-ZIP", FieldType.STRING, 
            35);
        pnd_Co_Group_Pnd_Co_C_Section = pnd_Co_Group_Pnd_Co_Address.newFieldInGroup("pnd_Co_Group_Pnd_Co_C_Section", "#CO-C-SECTION", FieldType.STRING, 
            1);
        pnd_Co_Group_Pnd_Co_Itd_Contract = pnd_Co_Group_Pnd_Co_Address.newFieldArrayInGroup("pnd_Co_Group_Pnd_Co_Itd_Contract", "#CO-ITD-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1, 20));

        pnd_Cm_Group = localVariables.newGroupInRecord("pnd_Cm_Group", "#CM-GROUP");

        pnd_Cm_Group_Pnd_Cm_Address = pnd_Cm_Group.newGroupArrayInGroup("pnd_Cm_Group_Pnd_Cm_Address", "#CM-ADDRESS", new DbsArrayController(1, 20));
        pnd_Cm_Group_Pnd_Cm_Contract = pnd_Cm_Group_Pnd_Cm_Address.newFieldArrayInGroup("pnd_Cm_Group_Pnd_Cm_Contract", "#CM-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1, 20));
        pnd_Cm_Group_Pnd_Cm_Address_Compare = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Address_Compare", "#CM-ADDRESS-COMPARE", 
            FieldType.STRING, 35);
        pnd_Cm_Group_Pnd_Cm_Name = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Name", "#CM-NAME", FieldType.STRING, 35);
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Ind = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Pay_Mail_Ind", "#CM-PAY-MAIL-IND", FieldType.STRING, 
            1);
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr1 = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr1", "#CM-PAY-MAIL-ADDR1", FieldType.STRING, 
            35);
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr2 = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr2", "#CM-PAY-MAIL-ADDR2", FieldType.STRING, 
            35);
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr3 = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr3", "#CM-PAY-MAIL-ADDR3", FieldType.STRING, 
            35);
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr4 = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr4", "#CM-PAY-MAIL-ADDR4", FieldType.STRING, 
            35);
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr5 = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr5", "#CM-PAY-MAIL-ADDR5", FieldType.STRING, 
            35);
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Zip = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Pay_Mail_Zip", "#CM-PAY-MAIL-ZIP", FieldType.STRING, 
            35);
        pnd_Cm_Group_Pnd_Cm_Pay_Bnk_Acct_Nbr = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Pay_Bnk_Acct_Nbr", "#CM-PAY-BNK-ACCT-NBR", 
            FieldType.STRING, 35);
        pnd_Cm_Group_Pnd_Cm_Pay_Cs_Ind = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Pay_Cs_Ind", "#CM-PAY-CS-IND", FieldType.STRING, 
            1);
        pnd_Cm_Group_Pnd_Cm_Pay_Eft_Nbr = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Pay_Eft_Nbr", "#CM-PAY-EFT-NBR", FieldType.STRING, 
            35);
        pnd_Cm_Group_Pnd_Cm_Pay_Bank_Info_Ind = pnd_Cm_Group_Pnd_Cm_Address.newFieldInGroup("pnd_Cm_Group_Pnd_Cm_Pay_Bank_Info_Ind", "#CM-PAY-BANK-INFO-IND", 
            FieldType.STRING, 1);
        pnd_Cm_Group_Pnd_Cm_Itd_Contract = pnd_Cm_Group_Pnd_Cm_Address.newFieldArrayInGroup("pnd_Cm_Group_Pnd_Cm_Itd_Contract", "#CM-ITD-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1, 20));

        pnd_Fco_Group = localVariables.newGroupInRecord("pnd_Fco_Group", "#FCO-GROUP");

        pnd_Fco_Group_Pnd_Fco_Address = pnd_Fco_Group.newGroupArrayInGroup("pnd_Fco_Group_Pnd_Fco_Address", "#FCO-ADDRESS", new DbsArrayController(1, 
            20));
        pnd_Fco_Group_Pnd_Fco_Contract = pnd_Fco_Group_Pnd_Fco_Address.newFieldArrayInGroup("pnd_Fco_Group_Pnd_Fco_Contract", "#FCO-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1, 20));
        pnd_Fco_Group_Pnd_Fco_Name = pnd_Fco_Group_Pnd_Fco_Address.newFieldInGroup("pnd_Fco_Group_Pnd_Fco_Name", "#FCO-NAME", FieldType.STRING, 35);
        pnd_Fco_Group_Pnd_Fco_Mail_Addr1 = pnd_Fco_Group_Pnd_Fco_Address.newFieldInGroup("pnd_Fco_Group_Pnd_Fco_Mail_Addr1", "#FCO-MAIL-ADDR1", FieldType.STRING, 
            35);
        pnd_Fco_Group_Pnd_Fco_Mail_Addr2 = pnd_Fco_Group_Pnd_Fco_Address.newFieldInGroup("pnd_Fco_Group_Pnd_Fco_Mail_Addr2", "#FCO-MAIL-ADDR2", FieldType.STRING, 
            35);
        pnd_Fco_Group_Pnd_Fco_Mail_Addr3 = pnd_Fco_Group_Pnd_Fco_Address.newFieldInGroup("pnd_Fco_Group_Pnd_Fco_Mail_Addr3", "#FCO-MAIL-ADDR3", FieldType.STRING, 
            35);
        pnd_Fco_Group_Pnd_Fco_Mail_Addr4 = pnd_Fco_Group_Pnd_Fco_Address.newFieldInGroup("pnd_Fco_Group_Pnd_Fco_Mail_Addr4", "#FCO-MAIL-ADDR4", FieldType.STRING, 
            35);
        pnd_Fco_Group_Pnd_Fco_Mail_Addr5 = pnd_Fco_Group_Pnd_Fco_Address.newFieldInGroup("pnd_Fco_Group_Pnd_Fco_Mail_Addr5", "#FCO-MAIL-ADDR5", FieldType.STRING, 
            35);
        pnd_Fco_Group_Pnd_Fco_Mail_Zip = pnd_Fco_Group_Pnd_Fco_Address.newFieldInGroup("pnd_Fco_Group_Pnd_Fco_Mail_Zip", "#FCO-MAIL-ZIP", FieldType.STRING, 
            35);
        pnd_Fco_Group_Pnd_Fco_Address_Compare = pnd_Fco_Group_Pnd_Fco_Address.newFieldInGroup("pnd_Fco_Group_Pnd_Fco_Address_Compare", "#FCO-ADDRESS-COMPARE", 
            FieldType.STRING, 35);
        pnd_Fco_Group_Pnd_Fco_Begin_Dte = pnd_Fco_Group_Pnd_Fco_Address.newFieldInGroup("pnd_Fco_Group_Pnd_Fco_Begin_Dte", "#FCO-BEGIN-DTE", FieldType.STRING, 
            8);
        pnd_Fco_Group_Pnd_Fco_Itd_Contract = pnd_Fco_Group_Pnd_Fco_Address.newFieldArrayInGroup("pnd_Fco_Group_Pnd_Fco_Itd_Contract", "#FCO-ITD-CONTRACT", 
            FieldType.STRING, 10, new DbsArrayController(1, 20));

        pnd_Fcm_Group = localVariables.newGroupInRecord("pnd_Fcm_Group", "#FCM-GROUP");

        pnd_Fcm_Group_Pnd_Fcm_Address = pnd_Fcm_Group.newGroupArrayInGroup("pnd_Fcm_Group_Pnd_Fcm_Address", "#FCM-ADDRESS", new DbsArrayController(1, 
            20));
        pnd_Fcm_Group_Pnd_Fcm_Contract = pnd_Fcm_Group_Pnd_Fcm_Address.newFieldArrayInGroup("pnd_Fcm_Group_Pnd_Fcm_Contract", "#FCM-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1, 20));
        pnd_Fcm_Group_Pnd_Fcm_Name = pnd_Fcm_Group_Pnd_Fcm_Address.newFieldInGroup("pnd_Fcm_Group_Pnd_Fcm_Name", "#FCM-NAME", FieldType.STRING, 35);
        pnd_Fcm_Group_Pnd_Fcm_Pay_Addr1 = pnd_Fcm_Group_Pnd_Fcm_Address.newFieldInGroup("pnd_Fcm_Group_Pnd_Fcm_Pay_Addr1", "#FCM-PAY-ADDR1", FieldType.STRING, 
            35);
        pnd_Fcm_Group_Pnd_Fcm_Pay_Addr2 = pnd_Fcm_Group_Pnd_Fcm_Address.newFieldInGroup("pnd_Fcm_Group_Pnd_Fcm_Pay_Addr2", "#FCM-PAY-ADDR2", FieldType.STRING, 
            35);
        pnd_Fcm_Group_Pnd_Fcm_Pay_Addr3 = pnd_Fcm_Group_Pnd_Fcm_Address.newFieldInGroup("pnd_Fcm_Group_Pnd_Fcm_Pay_Addr3", "#FCM-PAY-ADDR3", FieldType.STRING, 
            35);
        pnd_Fcm_Group_Pnd_Fcm_Pay_Addr4 = pnd_Fcm_Group_Pnd_Fcm_Address.newFieldInGroup("pnd_Fcm_Group_Pnd_Fcm_Pay_Addr4", "#FCM-PAY-ADDR4", FieldType.STRING, 
            35);
        pnd_Fcm_Group_Pnd_Fcm_Pay_Addr5 = pnd_Fcm_Group_Pnd_Fcm_Address.newFieldInGroup("pnd_Fcm_Group_Pnd_Fcm_Pay_Addr5", "#FCM-PAY-ADDR5", FieldType.STRING, 
            35);
        pnd_Fcm_Group_Pnd_Fcm_Pay_Zip = pnd_Fcm_Group_Pnd_Fcm_Address.newFieldInGroup("pnd_Fcm_Group_Pnd_Fcm_Pay_Zip", "#FCM-PAY-ZIP", FieldType.STRING, 
            35);
        pnd_Fcm_Group_Pnd_Fcm_Address_Compare = pnd_Fcm_Group_Pnd_Fcm_Address.newFieldInGroup("pnd_Fcm_Group_Pnd_Fcm_Address_Compare", "#FCM-ADDRESS-COMPARE", 
            FieldType.STRING, 35);
        pnd_Fcm_Group_Pnd_Fcm_Begin_Dte = pnd_Fcm_Group_Pnd_Fcm_Address.newFieldInGroup("pnd_Fcm_Group_Pnd_Fcm_Begin_Dte", "#FCM-BEGIN-DTE", FieldType.STRING, 
            8);
        pnd_Fcm_Group_Pnd_Fcm_Itd_Contract = pnd_Fcm_Group_Pnd_Fcm_Address.newFieldArrayInGroup("pnd_Fcm_Group_Pnd_Fcm_Itd_Contract", "#FCM-ITD-CONTRACT", 
            FieldType.STRING, 10, new DbsArrayController(1, 20));

        pnd_Frs_Group = localVariables.newGroupInRecord("pnd_Frs_Group", "#FRS-GROUP");

        pnd_Frs_Group_Pnd_Frs_Address = pnd_Frs_Group.newGroupInGroup("pnd_Frs_Group_Pnd_Frs_Address", "#FRS-ADDRESS");
        pnd_Frs_Group_Pnd_Frs_Contract = pnd_Frs_Group_Pnd_Frs_Address.newFieldInGroup("pnd_Frs_Group_Pnd_Frs_Contract", "#FRS-CONTRACT", FieldType.STRING, 
            10);
        pnd_Frs_Group_Pnd_Frs_Name = pnd_Frs_Group_Pnd_Frs_Address.newFieldInGroup("pnd_Frs_Group_Pnd_Frs_Name", "#FRS-NAME", FieldType.STRING, 35);
        pnd_Frs_Group_Pnd_Frs_Mail_Addr = pnd_Frs_Group_Pnd_Frs_Address.newFieldArrayInGroup("pnd_Frs_Group_Pnd_Frs_Mail_Addr", "#FRS-MAIL-ADDR", FieldType.STRING, 
            35, new DbsArrayController(1, 5));
        pnd_Frs_Group_Pnd_Frs_Mail_Zip = pnd_Frs_Group_Pnd_Frs_Address.newFieldInGroup("pnd_Frs_Group_Pnd_Frs_Mail_Zip", "#FRS-MAIL-ZIP", FieldType.STRING, 
            35);
        pnd_Frs_Group_Pnd_Frs_Address_Compare = pnd_Frs_Group_Pnd_Frs_Address.newFieldInGroup("pnd_Frs_Group_Pnd_Frs_Address_Compare", "#FRS-ADDRESS-COMPARE", 
            FieldType.STRING, 35);
        pnd_Frs_Group_Pnd_Frs_Begin_Dte = pnd_Frs_Group_Pnd_Frs_Address.newFieldInGroup("pnd_Frs_Group_Pnd_Frs_Begin_Dte", "#FRS-BEGIN-DTE", FieldType.STRING, 
            8);

        pnd_Rs_Group = localVariables.newGroupInRecord("pnd_Rs_Group", "#RS-GROUP");

        pnd_Rs_Group_Pnd_Rs_Address = pnd_Rs_Group.newGroupInGroup("pnd_Rs_Group_Pnd_Rs_Address", "#RS-ADDRESS");
        pnd_Rs_Group_Pnd_Rs_Name = pnd_Rs_Group_Pnd_Rs_Address.newFieldInGroup("pnd_Rs_Group_Pnd_Rs_Name", "#RS-NAME", FieldType.STRING, 35);
        pnd_Rs_Group_Pnd_Rs_New_Mail_Addr = pnd_Rs_Group_Pnd_Rs_Address.newFieldArrayInGroup("pnd_Rs_Group_Pnd_Rs_New_Mail_Addr", "#RS-NEW-MAIL-ADDR", 
            FieldType.STRING, 35, new DbsArrayController(1, 5));
        pnd_Rs_Group_Pnd_Rs_New_Mail_Zip = pnd_Rs_Group_Pnd_Rs_Address.newFieldInGroup("pnd_Rs_Group_Pnd_Rs_New_Mail_Zip", "#RS-NEW-MAIL-ZIP", FieldType.STRING, 
            35);
        pnd_Rs_Group_Pnd_Rs_Address_Compare = pnd_Rs_Group_Pnd_Rs_Address.newFieldInGroup("pnd_Rs_Group_Pnd_Rs_Address_Compare", "#RS-ADDRESS-COMPARE", 
            FieldType.STRING, 35);

        pnd_Vco_Group = localVariables.newGroupInRecord("pnd_Vco_Group", "#VCO-GROUP");

        pnd_Vco_Group_Pnd_Vco_Address = pnd_Vco_Group.newGroupArrayInGroup("pnd_Vco_Group_Pnd_Vco_Address", "#VCO-ADDRESS", new DbsArrayController(1, 
            20));
        pnd_Vco_Group_Pnd_Vco_Contract = pnd_Vco_Group_Pnd_Vco_Address.newFieldArrayInGroup("pnd_Vco_Group_Pnd_Vco_Contract", "#VCO-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1, 20));
        pnd_Vco_Group_Pnd_Vco_Name = pnd_Vco_Group_Pnd_Vco_Address.newFieldInGroup("pnd_Vco_Group_Pnd_Vco_Name", "#VCO-NAME", FieldType.STRING, 35);
        pnd_Vco_Group_Pnd_Vco_Mail_Addr1 = pnd_Vco_Group_Pnd_Vco_Address.newFieldInGroup("pnd_Vco_Group_Pnd_Vco_Mail_Addr1", "#VCO-MAIL-ADDR1", FieldType.STRING, 
            35);
        pnd_Vco_Group_Pnd_Vco_Mail_Addr2 = pnd_Vco_Group_Pnd_Vco_Address.newFieldInGroup("pnd_Vco_Group_Pnd_Vco_Mail_Addr2", "#VCO-MAIL-ADDR2", FieldType.STRING, 
            35);
        pnd_Vco_Group_Pnd_Vco_Mail_Addr3 = pnd_Vco_Group_Pnd_Vco_Address.newFieldInGroup("pnd_Vco_Group_Pnd_Vco_Mail_Addr3", "#VCO-MAIL-ADDR3", FieldType.STRING, 
            35);
        pnd_Vco_Group_Pnd_Vco_Mail_Addr4 = pnd_Vco_Group_Pnd_Vco_Address.newFieldInGroup("pnd_Vco_Group_Pnd_Vco_Mail_Addr4", "#VCO-MAIL-ADDR4", FieldType.STRING, 
            35);
        pnd_Vco_Group_Pnd_Vco_Mail_Addr5 = pnd_Vco_Group_Pnd_Vco_Address.newFieldInGroup("pnd_Vco_Group_Pnd_Vco_Mail_Addr5", "#VCO-MAIL-ADDR5", FieldType.STRING, 
            35);
        pnd_Vco_Group_Pnd_Vco_Mail_Zip = pnd_Vco_Group_Pnd_Vco_Address.newFieldInGroup("pnd_Vco_Group_Pnd_Vco_Mail_Zip", "#VCO-MAIL-ZIP", FieldType.STRING, 
            35);
        pnd_Vco_Group_Pnd_Vco_Address_Compare = pnd_Vco_Group_Pnd_Vco_Address.newFieldInGroup("pnd_Vco_Group_Pnd_Vco_Address_Compare", "#VCO-ADDRESS-COMPARE", 
            FieldType.STRING, 35);
        pnd_Vco_Group_Pnd_Vco_Annual_Cycle_Ind = pnd_Vco_Group_Pnd_Vco_Address.newFieldInGroup("pnd_Vco_Group_Pnd_Vco_Annual_Cycle_Ind", "#VCO-ANNUAL-CYCLE-IND", 
            FieldType.STRING, 1);
        pnd_Vco_Group_Pnd_Vco_Begin_Dte = pnd_Vco_Group_Pnd_Vco_Address.newFieldInGroup("pnd_Vco_Group_Pnd_Vco_Begin_Dte", "#VCO-BEGIN-DTE", FieldType.STRING, 
            8);
        pnd_Vco_Group_Pnd_Vco_End_Dte = pnd_Vco_Group_Pnd_Vco_Address.newFieldInGroup("pnd_Vco_Group_Pnd_Vco_End_Dte", "#VCO-END-DTE", FieldType.STRING, 
            8);
        pnd_Vco_Group_Pnd_Vco_Itd_Contract = pnd_Vco_Group_Pnd_Vco_Address.newFieldArrayInGroup("pnd_Vco_Group_Pnd_Vco_Itd_Contract", "#VCO-ITD-CONTRACT", 
            FieldType.STRING, 10, new DbsArrayController(1, 20));

        pnd_Vcm_Group = localVariables.newGroupInRecord("pnd_Vcm_Group", "#VCM-GROUP");

        pnd_Vcm_Group_Pnd_Vcm_Address = pnd_Vcm_Group.newGroupArrayInGroup("pnd_Vcm_Group_Pnd_Vcm_Address", "#VCM-ADDRESS", new DbsArrayController(1, 
            20));
        pnd_Vcm_Group_Pnd_Vcm_Contract = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldArrayInGroup("pnd_Vcm_Group_Pnd_Vcm_Contract", "#VCM-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1, 20));
        pnd_Vcm_Group_Pnd_Vcm_Name = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldInGroup("pnd_Vcm_Group_Pnd_Vcm_Name", "#VCM-NAME", FieldType.STRING, 35);
        pnd_Vcm_Group_Pnd_Vcm_Pay_Addr1 = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldInGroup("pnd_Vcm_Group_Pnd_Vcm_Pay_Addr1", "#VCM-PAY-ADDR1", FieldType.STRING, 
            35);
        pnd_Vcm_Group_Pnd_Vcm_Pay_Addr2 = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldInGroup("pnd_Vcm_Group_Pnd_Vcm_Pay_Addr2", "#VCM-PAY-ADDR2", FieldType.STRING, 
            35);
        pnd_Vcm_Group_Pnd_Vcm_Pay_Addr3 = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldInGroup("pnd_Vcm_Group_Pnd_Vcm_Pay_Addr3", "#VCM-PAY-ADDR3", FieldType.STRING, 
            35);
        pnd_Vcm_Group_Pnd_Vcm_Pay_Addr4 = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldInGroup("pnd_Vcm_Group_Pnd_Vcm_Pay_Addr4", "#VCM-PAY-ADDR4", FieldType.STRING, 
            35);
        pnd_Vcm_Group_Pnd_Vcm_Pay_Addr5 = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldInGroup("pnd_Vcm_Group_Pnd_Vcm_Pay_Addr5", "#VCM-PAY-ADDR5", FieldType.STRING, 
            35);
        pnd_Vcm_Group_Pnd_Vcm_Pay_Zip = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldInGroup("pnd_Vcm_Group_Pnd_Vcm_Pay_Zip", "#VCM-PAY-ZIP", FieldType.STRING, 
            35);
        pnd_Vcm_Group_Pnd_Vcm_Address_Compare = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldInGroup("pnd_Vcm_Group_Pnd_Vcm_Address_Compare", "#VCM-ADDRESS-COMPARE", 
            FieldType.STRING, 35);
        pnd_Vcm_Group_Pnd_Vcm_Annual_Cycle_Ind = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldInGroup("pnd_Vcm_Group_Pnd_Vcm_Annual_Cycle_Ind", "#VCM-ANNUAL-CYCLE-IND", 
            FieldType.STRING, 1);
        pnd_Vcm_Group_Pnd_Vcm_Begin_Dte = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldInGroup("pnd_Vcm_Group_Pnd_Vcm_Begin_Dte", "#VCM-BEGIN-DTE", FieldType.STRING, 
            8);
        pnd_Vcm_Group_Pnd_Vcm_End_Dte = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldInGroup("pnd_Vcm_Group_Pnd_Vcm_End_Dte", "#VCM-END-DTE", FieldType.STRING, 
            8);
        pnd_Vcm_Group_Pnd_Vcm_Itd_Contract = pnd_Vcm_Group_Pnd_Vcm_Address.newFieldArrayInGroup("pnd_Vcm_Group_Pnd_Vcm_Itd_Contract", "#VCM-ITD-CONTRACT", 
            FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_Address_Compare = localVariables.newFieldInRecord("pnd_Address_Compare", "#ADDRESS-COMPARE", FieldType.STRING, 35);

        pnd_Bank = localVariables.newGroupArrayInRecord("pnd_Bank", "#BANK", new DbsArrayController(1, 20));
        pnd_Bank_Pnd_Bank_Acct = pnd_Bank.newFieldInGroup("pnd_Bank_Pnd_Bank_Acct", "#BANK-ACCT", FieldType.STRING, 35);

        pnd_Bank__R_Field_7 = pnd_Bank.newGroupInGroup("pnd_Bank__R_Field_7", "REDEFINE", pnd_Bank_Pnd_Bank_Acct);
        pnd_Bank_Pnd_Bank_Acct_A = pnd_Bank__R_Field_7.newFieldArrayInGroup("pnd_Bank_Pnd_Bank_Acct_A", "#BANK-ACCT-A", FieldType.STRING, 1, new DbsArrayController(1, 
            35));
        pnd_Bank_Pnd_Eft_Acct = pnd_Bank.newFieldInGroup("pnd_Bank_Pnd_Eft_Acct", "#EFT-ACCT", FieldType.STRING, 35);

        pnd_Bank__R_Field_8 = pnd_Bank.newGroupInGroup("pnd_Bank__R_Field_8", "REDEFINE", pnd_Bank_Pnd_Eft_Acct);
        pnd_Bank_Pnd_Eft_Acct_A = pnd_Bank__R_Field_8.newFieldArrayInGroup("pnd_Bank_Pnd_Eft_Acct_A", "#EFT-ACCT-A", FieldType.STRING, 1, new DbsArrayController(1, 
            35));
        pnd_Close = localVariables.newFieldInRecord("pnd_Close", "#CLOSE", FieldType.BOOLEAN, 1);
        pnd_Close_Cnt = localVariables.newFieldInRecord("pnd_Close_Cnt", "#CLOSE-CNT", FieldType.NUMERIC, 7);
        pnd_Cm_Ctr = localVariables.newFieldInRecord("pnd_Cm_Ctr", "#CM-CTR", FieldType.NUMERIC, 3);
        pnd_Cm_Cnt = localVariables.newFieldInRecord("pnd_Cm_Cnt", "#CM-CNT", FieldType.NUMERIC, 2);
        pnd_Cm_Idx = localVariables.newFieldInRecord("pnd_Cm_Idx", "#CM-IDX", FieldType.NUMERIC, 3);
        pnd_Cm_Post = localVariables.newFieldInRecord("pnd_Cm_Post", "#CM-POST", FieldType.BOOLEAN, 1);
        pnd_Cm_Pntr = localVariables.newFieldInRecord("pnd_Cm_Pntr", "#CM-PNTR", FieldType.NUMERIC, 2);
        pnd_Cm_Reach = localVariables.newFieldInRecord("pnd_Cm_Reach", "#CM-REACH", FieldType.STRING, 1);
        pnd_Cm_Start = localVariables.newFieldInRecord("pnd_Cm_Start", "#CM-START", FieldType.NUMERIC, 7);
        pnd_Cm_Write = localVariables.newFieldInRecord("pnd_Cm_Write", "#CM-WRITE", FieldType.BOOLEAN, 1);
        pnd_Co_Cnt = localVariables.newFieldInRecord("pnd_Co_Cnt", "#CO-CNT", FieldType.NUMERIC, 2);
        pnd_Co_Ctr = localVariables.newFieldInRecord("pnd_Co_Ctr", "#CO-CTR", FieldType.NUMERIC, 3);
        pnd_Co_Idx = localVariables.newFieldInRecord("pnd_Co_Idx", "#CO-IDX", FieldType.NUMERIC, 3);
        pnd_Co_Post = localVariables.newFieldInRecord("pnd_Co_Post", "#CO-POST", FieldType.BOOLEAN, 1);
        pnd_Co_Pntr = localVariables.newFieldInRecord("pnd_Co_Pntr", "#CO-PNTR", FieldType.NUMERIC, 2);
        pnd_Co_Reach = localVariables.newFieldInRecord("pnd_Co_Reach", "#CO-REACH", FieldType.STRING, 1);
        pnd_Co_Start = localVariables.newFieldInRecord("pnd_Co_Start", "#CO-START", FieldType.NUMERIC, 7);
        pnd_Co_Write = localVariables.newFieldInRecord("pnd_Co_Write", "#CO-WRITE", FieldType.BOOLEAN, 1);
        pnd_Contract_Write1 = localVariables.newFieldInRecord("pnd_Contract_Write1", "#CONTRACT-WRITE1", FieldType.STRING, 109);
        pnd_Contract_Write2 = localVariables.newFieldInRecord("pnd_Contract_Write2", "#CONTRACT-WRITE2", FieldType.STRING, 109);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_J = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Counters_Pnd_K = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_K", "#K", FieldType.NUMERIC, 3);
        pnd_Counters_Pnd_L = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_L", "#L", FieldType.NUMERIC, 3);
        pnd_Counters_Pnd_X = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_Counters_Pnd_Y = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Y", "#Y", FieldType.NUMERIC, 3);
        pnd_Counters_Pnd_Z = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Z", "#Z", FieldType.NUMERIC, 3);
        pnd_Error_Temp1 = localVariables.newFieldInRecord("pnd_Error_Temp1", "#ERROR-TEMP1", FieldType.STRING, 40);
        pnd_Error_Temp2 = localVariables.newFieldInRecord("pnd_Error_Temp2", "#ERROR-TEMP2", FieldType.STRING, 40);
        pnd_Error_Work = localVariables.newFieldInRecord("pnd_Error_Work", "#ERROR-WORK", FieldType.PACKED_DECIMAL, 10);
        pnd_Fcm_Cnt = localVariables.newFieldInRecord("pnd_Fcm_Cnt", "#FCM-CNT", FieldType.NUMERIC, 2);
        pnd_Fcm_Ctr = localVariables.newFieldInRecord("pnd_Fcm_Ctr", "#FCM-CTR", FieldType.NUMERIC, 3);
        pnd_Fcm_Idx = localVariables.newFieldInRecord("pnd_Fcm_Idx", "#FCM-IDX", FieldType.NUMERIC, 3);
        pnd_Fcm_Post = localVariables.newFieldInRecord("pnd_Fcm_Post", "#FCM-POST", FieldType.BOOLEAN, 1);
        pnd_Fcm_Pntr = localVariables.newFieldInRecord("pnd_Fcm_Pntr", "#FCM-PNTR", FieldType.NUMERIC, 2);
        pnd_Fcm_Reach = localVariables.newFieldInRecord("pnd_Fcm_Reach", "#FCM-REACH", FieldType.STRING, 1);
        pnd_Fcm_Start = localVariables.newFieldInRecord("pnd_Fcm_Start", "#FCM-START", FieldType.NUMERIC, 7);
        pnd_Fcm_Write = localVariables.newFieldInRecord("pnd_Fcm_Write", "#FCM-WRITE", FieldType.BOOLEAN, 1);
        pnd_Fco_Cnt = localVariables.newFieldInRecord("pnd_Fco_Cnt", "#FCO-CNT", FieldType.NUMERIC, 2);
        pnd_Fco_Ctr = localVariables.newFieldInRecord("pnd_Fco_Ctr", "#FCO-CTR", FieldType.NUMERIC, 3);
        pnd_Fco_Idx = localVariables.newFieldInRecord("pnd_Fco_Idx", "#FCO-IDX", FieldType.NUMERIC, 3);
        pnd_Fco_Post = localVariables.newFieldInRecord("pnd_Fco_Post", "#FCO-POST", FieldType.BOOLEAN, 1);
        pnd_Fco_Pntr = localVariables.newFieldInRecord("pnd_Fco_Pntr", "#FCO-PNTR", FieldType.NUMERIC, 2);
        pnd_Fco_Reach = localVariables.newFieldInRecord("pnd_Fco_Reach", "#FCO-REACH", FieldType.STRING, 1);
        pnd_Fco_Start = localVariables.newFieldInRecord("pnd_Fco_Start", "#FCO-START", FieldType.NUMERIC, 7);
        pnd_Fco_Write = localVariables.newFieldInRecord("pnd_Fco_Write", "#FCO-WRITE", FieldType.BOOLEAN, 1);
        pnd_Frs_Cnt = localVariables.newFieldInRecord("pnd_Frs_Cnt", "#FRS-CNT", FieldType.NUMERIC, 2);
        pnd_Frs_Ctr = localVariables.newFieldInRecord("pnd_Frs_Ctr", "#FRS-CTR", FieldType.NUMERIC, 3);
        pnd_Frs_Idx = localVariables.newFieldInRecord("pnd_Frs_Idx", "#FRS-IDX", FieldType.NUMERIC, 3);
        pnd_Frs_Reach = localVariables.newFieldInRecord("pnd_Frs_Reach", "#FRS-REACH", FieldType.STRING, 1);
        pnd_Frs_Start = localVariables.newFieldInRecord("pnd_Frs_Start", "#FRS-START", FieldType.NUMERIC, 7);
        pnd_Frs_Write = localVariables.newFieldInRecord("pnd_Frs_Write", "#FRS-WRITE", FieldType.BOOLEAN, 1);
        pnd_First_Read = localVariables.newFieldInRecord("pnd_First_Read", "#FIRST-READ", FieldType.BOOLEAN, 1);
        pnd_First_Write = localVariables.newFieldInRecord("pnd_First_Write", "#FIRST-WRITE", FieldType.BOOLEAN, 1);
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.NUMERIC, 3);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 3);
        pnd_Letters_Created = localVariables.newFieldInRecord("pnd_Letters_Created", "#LETTERS-CREATED", FieldType.PACKED_DECIMAL, 10);
        pnd_Loop = localVariables.newFieldInRecord("pnd_Loop", "#LOOP", FieldType.NUMERIC, 2);
        pnd_Max_Reach = localVariables.newFieldInRecord("pnd_Max_Reach", "#MAX-REACH", FieldType.BOOLEAN, 1);

        pnd_Mit_Array = localVariables.newGroupArrayInRecord("pnd_Mit_Array", "#MIT-ARRAY", new DbsArrayController(1, 100));
        pnd_Mit_Array_Pnd_A_Mit_Wpid = pnd_Mit_Array.newFieldInGroup("pnd_Mit_Array_Pnd_A_Mit_Wpid", "#A-MIT-WPID", FieldType.STRING, 6);
        pnd_Mit_Array_Pnd_A_Mit_Unit_Cde = pnd_Mit_Array.newFieldInGroup("pnd_Mit_Array_Pnd_A_Mit_Unit_Cde", "#A-MIT-UNIT-CDE", FieldType.STRING, 8);
        pnd_Mit_Array_Pnd_A_Mit_Log_Date_Time = pnd_Mit_Array.newFieldInGroup("pnd_Mit_Array_Pnd_A_Mit_Log_Date_Time", "#A-MIT-LOG-DATE-TIME", FieldType.STRING, 
            15);
        pnd_Mit_Array_Pnd_A_Mit_Status_Cde = pnd_Mit_Array.newFieldInGroup("pnd_Mit_Array_Pnd_A_Mit_Status_Cde", "#A-MIT-STATUS-CDE", FieldType.STRING, 
            4);
        pnd_Mit_Ctr = localVariables.newFieldInRecord("pnd_Mit_Ctr", "#MIT-CTR", FieldType.NUMERIC, 3);
        pnd_Mit_Pntr = localVariables.newFieldInRecord("pnd_Mit_Pntr", "#MIT-PNTR", FieldType.NUMERIC, 2);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 1);
        pnd_Ndx1 = localVariables.newFieldInRecord("pnd_Ndx1", "#NDX1", FieldType.NUMERIC, 2);
        pnd_Open_Cnt = localVariables.newFieldInRecord("pnd_Open_Cnt", "#OPEN-CNT", FieldType.NUMERIC, 7);
        pnd_Output_Date = localVariables.newFieldInRecord("pnd_Output_Date", "#OUTPUT-DATE", FieldType.DATE);
        pnd_Output_Time = localVariables.newFieldInRecord("pnd_Output_Time", "#OUTPUT-TIME", FieldType.NUMERIC, 7);
        pnd_Post_Cnt = localVariables.newFieldInRecord("pnd_Post_Cnt", "#POST-CNT", FieldType.NUMERIC, 2);
        pnd_Post_Ctr = localVariables.newFieldInRecord("pnd_Post_Ctr", "#POST-CTR", FieldType.NUMERIC, 2);
        pnd_Post_Mmddyyyy = localVariables.newFieldInRecord("pnd_Post_Mmddyyyy", "#POST-MMDDYYYY", FieldType.STRING, 8);

        pnd_Post_Mmddyyyy__R_Field_9 = localVariables.newGroupInRecord("pnd_Post_Mmddyyyy__R_Field_9", "REDEFINE", pnd_Post_Mmddyyyy);
        pnd_Post_Mmddyyyy_Pnd_P_Mm = pnd_Post_Mmddyyyy__R_Field_9.newFieldInGroup("pnd_Post_Mmddyyyy_Pnd_P_Mm", "#P-MM", FieldType.STRING, 2);
        pnd_Post_Mmddyyyy_Pnd_P_Dd = pnd_Post_Mmddyyyy__R_Field_9.newFieldInGroup("pnd_Post_Mmddyyyy_Pnd_P_Dd", "#P-DD", FieldType.STRING, 2);
        pnd_Post_Mmddyyyy_Pnd_P_Ccyy = pnd_Post_Mmddyyyy__R_Field_9.newFieldInGroup("pnd_Post_Mmddyyyy_Pnd_P_Ccyy", "#P-CCYY", FieldType.STRING, 4);
        pnd_Post_Error = localVariables.newFieldInRecord("pnd_Post_Error", "#POST-ERROR", FieldType.PACKED_DECIMAL, 10);
        pnd_Post_Loop = localVariables.newFieldInRecord("pnd_Post_Loop", "#POST-LOOP", FieldType.NUMERIC, 3);
        pnd_Rs_Cnt = localVariables.newFieldInRecord("pnd_Rs_Cnt", "#RS-CNT", FieldType.NUMERIC, 2);
        pnd_Rs_Ctr = localVariables.newFieldInRecord("pnd_Rs_Ctr", "#RS-CTR", FieldType.NUMERIC, 3);
        pnd_Rs_Idx = localVariables.newFieldInRecord("pnd_Rs_Idx", "#RS-IDX", FieldType.NUMERIC, 3);
        pnd_Rs_Reach = localVariables.newFieldInRecord("pnd_Rs_Reach", "#RS-REACH", FieldType.STRING, 1);
        pnd_Rs_Start = localVariables.newFieldInRecord("pnd_Rs_Start", "#RS-START", FieldType.NUMERIC, 7);
        pnd_Rs_Write = localVariables.newFieldInRecord("pnd_Rs_Write", "#RS-WRITE", FieldType.BOOLEAN, 1);
        pnd_Mailto_C_Mit_Log_Date_Time = localVariables.newFieldInRecord("pnd_Mailto_C_Mit_Log_Date_Time", "#MAILTO-C-MIT-LOG-DATE-TIME", FieldType.STRING, 
            15);
        pnd_Mailto_C_Mit_Status_Cde = localVariables.newFieldInRecord("pnd_Mailto_C_Mit_Status_Cde", "#MAILTO-C-MIT-STATUS-CDE", FieldType.STRING, 4);
        pnd_Mailto_C_Mit_Unit_Cde = localVariables.newFieldInRecord("pnd_Mailto_C_Mit_Unit_Cde", "#MAILTO-C-MIT-UNIT-CDE", FieldType.STRING, 8);
        pnd_Mailto_C_Mit_Wpid = localVariables.newFieldInRecord("pnd_Mailto_C_Mit_Wpid", "#MAILTO-C-MIT-WPID", FieldType.STRING, 6);
        pnd_Mailto_Compress_Addr = localVariables.newFieldInRecord("pnd_Mailto_Compress_Addr", "#MAILTO-COMPRESS-ADDR", FieldType.STRING, 35);
        pnd_Mailto_Lob = localVariables.newFieldInRecord("pnd_Mailto_Lob", "#MAILTO-LOB", FieldType.STRING, 1);
        pnd_Mailto_M_Address_1 = localVariables.newFieldInRecord("pnd_Mailto_M_Address_1", "#MAILTO-M-ADDRESS-1", FieldType.STRING, 35);
        pnd_Mailto_M_Address_2 = localVariables.newFieldInRecord("pnd_Mailto_M_Address_2", "#MAILTO-M-ADDRESS-2", FieldType.STRING, 35);
        pnd_Mailto_M_Address_3 = localVariables.newFieldInRecord("pnd_Mailto_M_Address_3", "#MAILTO-M-ADDRESS-3", FieldType.STRING, 35);
        pnd_Mailto_M_Address_4 = localVariables.newFieldInRecord("pnd_Mailto_M_Address_4", "#MAILTO-M-ADDRESS-4", FieldType.STRING, 35);
        pnd_Mailto_M_Address_5 = localVariables.newFieldInRecord("pnd_Mailto_M_Address_5", "#MAILTO-M-ADDRESS-5", FieldType.STRING, 35);
        pnd_Mailto_M_Destination = localVariables.newFieldInRecord("pnd_Mailto_M_Destination", "#MAILTO-M-DESTINATION", FieldType.STRING, 1);
        pnd_Mailto_M_Name = localVariables.newFieldInRecord("pnd_Mailto_M_Name", "#MAILTO-M-NAME", FieldType.STRING, 35);
        pnd_Mailto_M_Postal_Data = localVariables.newFieldInRecord("pnd_Mailto_M_Postal_Data", "#MAILTO-M-POSTAL-DATA", FieldType.STRING, 35);
        pnd_Mailto_Pin = localVariables.newFieldInRecord("pnd_Mailto_Pin", "#MAILTO-PIN", FieldType.NUMERIC, 12);
        pnd_Vcm_Cnt = localVariables.newFieldInRecord("pnd_Vcm_Cnt", "#VCM-CNT", FieldType.NUMERIC, 2);
        pnd_Vcm_Ctr = localVariables.newFieldInRecord("pnd_Vcm_Ctr", "#VCM-CTR", FieldType.NUMERIC, 3);
        pnd_Vcm_Idx = localVariables.newFieldInRecord("pnd_Vcm_Idx", "#VCM-IDX", FieldType.NUMERIC, 3);
        pnd_Vcm_Post = localVariables.newFieldInRecord("pnd_Vcm_Post", "#VCM-POST", FieldType.BOOLEAN, 1);
        pnd_Vcm_Pntr = localVariables.newFieldInRecord("pnd_Vcm_Pntr", "#VCM-PNTR", FieldType.NUMERIC, 2);
        pnd_Vcm_Reach = localVariables.newFieldInRecord("pnd_Vcm_Reach", "#VCM-REACH", FieldType.STRING, 1);
        pnd_Vcm_Start = localVariables.newFieldInRecord("pnd_Vcm_Start", "#VCM-START", FieldType.NUMERIC, 7);
        pnd_Vcm_Write = localVariables.newFieldInRecord("pnd_Vcm_Write", "#VCM-WRITE", FieldType.BOOLEAN, 1);
        pnd_Vco_Cnt = localVariables.newFieldInRecord("pnd_Vco_Cnt", "#VCO-CNT", FieldType.NUMERIC, 2);
        pnd_Vco_Ctr = localVariables.newFieldInRecord("pnd_Vco_Ctr", "#VCO-CTR", FieldType.NUMERIC, 3);
        pnd_Vco_Idx = localVariables.newFieldInRecord("pnd_Vco_Idx", "#VCO-IDX", FieldType.NUMERIC, 3);
        pnd_Vco_Post = localVariables.newFieldInRecord("pnd_Vco_Post", "#VCO-POST", FieldType.BOOLEAN, 1);
        pnd_Vco_Pntr = localVariables.newFieldInRecord("pnd_Vco_Pntr", "#VCO-PNTR", FieldType.NUMERIC, 2);
        pnd_Vco_Reach = localVariables.newFieldInRecord("pnd_Vco_Reach", "#VCO-REACH", FieldType.STRING, 1);
        pnd_Vco_Start = localVariables.newFieldInRecord("pnd_Vco_Start", "#VCO-START", FieldType.NUMERIC, 7);
        pnd_Vco_Write = localVariables.newFieldInRecord("pnd_Vco_Write", "#VCO-WRITE", FieldType.BOOLEAN, 1);
        pnd_Write_Addr = localVariables.newFieldArrayInRecord("pnd_Write_Addr", "#WRITE-ADDR", FieldType.STRING, 35, new DbsArrayController(1, 5));
        pnd_Write_Addr2 = localVariables.newFieldArrayInRecord("pnd_Write_Addr2", "#WRITE-ADDR2", FieldType.STRING, 35, new DbsArrayController(1, 5));

        sort_Key = localVariables.newGroupInRecord("sort_Key", "SORT-KEY");
        sort_Key_Sort_Key_Lgth = sort_Key.newFieldInGroup("sort_Key_Sort_Key_Lgth", "SORT-KEY-LGTH", FieldType.NUMERIC, 3);
        sort_Key_Sort_Key_Array = sort_Key.newFieldArrayInGroup("sort_Key_Sort_Key_Array", "SORT-KEY-ARRAY", FieldType.STRING, 1, new DbsArrayController(1, 
            1));

        pstl6635_Data = localVariables.newGroupInRecord("pstl6635_Data", "PSTL6635-DATA");
        pstl6635_Data_Pstl6635_Rec_Id = pstl6635_Data.newFieldInGroup("pstl6635_Data_Pstl6635_Rec_Id", "PSTL6635-REC-ID", FieldType.STRING, 2);
        pstl6635_Data_Pstl6635_Rec_Lgth = pstl6635_Data.newFieldInGroup("pstl6635_Data_Pstl6635_Rec_Lgth", "PSTL6635-REC-LGTH", FieldType.NUMERIC, 5);
        pstl6635_Data_Pstl6635_Rec_Occurs = pstl6635_Data.newFieldInGroup("pstl6635_Data_Pstl6635_Rec_Occurs", "PSTL6635-REC-OCCURS", FieldType.NUMERIC, 
            5);
        pstl6635_Data_Pstl6635_Data_Array = pstl6635_Data.newFieldArrayInGroup("pstl6635_Data_Pstl6635_Data_Array", "PSTL6635-DATA-ARRAY", FieldType.STRING, 
            1, new DbsArrayController(1, 5320));

        pstl6635_Data__R_Field_10 = pstl6635_Data.newGroupInGroup("pstl6635_Data__R_Field_10", "REDEFINE", pstl6635_Data_Pstl6635_Data_Array);

        pstl6635_Data_Addr_Chng_Dtls = pstl6635_Data__R_Field_10.newGroupArrayInGroup("pstl6635_Data_Addr_Chng_Dtls", "ADDR-CHNG-DTLS", new DbsArrayController(1, 
            8));
        pstl6635_Data_Addr_Chng_Type = pstl6635_Data_Addr_Chng_Dtls.newFieldInGroup("pstl6635_Data_Addr_Chng_Type", "ADDR-CHNG-TYPE", FieldType.STRING, 
            1);
        pstl6635_Data_Addr_Line = pstl6635_Data_Addr_Chng_Dtls.newFieldArrayInGroup("pstl6635_Data_Addr_Line", "ADDR-LINE", FieldType.STRING, 35, new 
            DbsArrayController(1, 5));
        pstl6635_Data_Addr_Cntrct = pstl6635_Data_Addr_Chng_Dtls.newFieldArrayInGroup("pstl6635_Data_Addr_Cntrct", "ADDR-CNTRCT", FieldType.STRING, 10, 
            new DbsArrayController(1, 20));
        pstl6635_Data_New_Pay_Bank_Info_Ind = pstl6635_Data_Addr_Chng_Dtls.newFieldInGroup("pstl6635_Data_New_Pay_Bank_Info_Ind", "NEW-PAY-BANK-INFO-IND", 
            FieldType.STRING, 1);
        pstl6635_Data_New_Pay_Bnk_Acct_Nbr = pstl6635_Data_Addr_Chng_Dtls.newFieldInGroup("pstl6635_Data_New_Pay_Bnk_Acct_Nbr", "NEW-PAY-BNK-ACCT-NBR", 
            FieldType.STRING, 35);
        pstl6635_Data_New_Pay_Cs_Ind = pstl6635_Data_Addr_Chng_Dtls.newFieldInGroup("pstl6635_Data_New_Pay_Cs_Ind", "NEW-PAY-CS-IND", FieldType.STRING, 
            1);
        pstl6635_Data_New_Pay_Eft_Nbr = pstl6635_Data_Addr_Chng_Dtls.newFieldInGroup("pstl6635_Data_New_Pay_Eft_Nbr", "NEW-PAY-EFT-NBR", FieldType.STRING, 
            35);
        pstl6635_Data_Bgn_Effct_Dte = pstl6635_Data_Addr_Chng_Dtls.newFieldInGroup("pstl6635_Data_Bgn_Effct_Dte", "BGN-EFFCT-DTE", FieldType.STRING, 8);
        pstl6635_Data_End_Effct_Dte = pstl6635_Data_Addr_Chng_Dtls.newFieldInGroup("pstl6635_Data_End_Effct_Dte", "END-EFFCT-DTE", FieldType.STRING, 8);
        pstl6635_Data_Tmp_Annual_Addr_Ind = pstl6635_Data_Addr_Chng_Dtls.newFieldInGroup("pstl6635_Data_Tmp_Annual_Addr_Ind", "TMP-ANNUAL-ADDR-IND", FieldType.STRING, 
            1);
        pstl6635_Data_Itd_Contract_Out = pstl6635_Data_Addr_Chng_Dtls.newFieldArrayInGroup("pstl6635_Data_Itd_Contract_Out", "ITD-CONTRACT-OUT", FieldType.STRING, 
            10, new DbsArrayController(1, 20));
        pnd_Hold_Contract = localVariables.newFieldInRecord("pnd_Hold_Contract", "#HOLD-CONTRACT", FieldType.STRING, 10);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Prev_Pin_No.setInitialValue(0);
        pnd_First_Read.setInitialValue(true);
        pnd_First_Write.setInitialValue(true);
        sort_Key_Sort_Key_Lgth.setInitialValue(1);
        pstl6635_Data_Pstl6635_Rec_Id.setInitialValue(" ");
        pstl6635_Data_Pstl6635_Rec_Lgth.setInitialValue(3720);
        pstl6635_Data_Pstl6635_Rec_Occurs.setInitialValue(1);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Nasb495k() throws Exception
    {
        super("Nasb495k");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 60 LS = 133 SG = OFF;//Natural: FORMAT ( 1 ) PS = 60 LS = 133 SG = OFF;//Natural: FORMAT ( 2 ) PS = 60 LS = 133 SG = OFF;//Natural: FORMAT ( 3 ) PS = 60 LS = 133 SG = OFF
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #WORK-LETTER-IMAGE-FILE
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Work_Letter_Image_File)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            getReports().display(0, ReportOption.NOTITLE,"PIN",                                                                                                           //Natural: DISPLAY ( 0 ) NOTITLE 'PIN' #O-PIN-NO 'Cntrct' #O-CONTRACT-NO 'Py' #O-PAYEE-CD ( EM = 99 ) 'AA' #O-ADDR-ACTVTY-CD 'Ac' #O-ACTVTY-CD 'Us' #O-USAGE 'Sc' #O-C-SECTION 'Addr' #COMPRESS-ADDRESS 'Cntrct2' #O-CONTRACT2
            		pnd_Work_Letter_Image_File_Pnd_O_Pin_No,"Cntrct",
            		pnd_Work_Letter_Image_File_Pnd_O_Contract_No,"Py",
            		pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd, new ReportEditMask ("99"),"AA",
            		pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd,"Ac",
            		pnd_Work_Letter_Image_File_Pnd_O_Actvty_Cd,"Us",
            		pnd_Work_Letter_Image_File_Pnd_O_Usage,"Sc",
            		pnd_Work_Letter_Image_File_Pnd_O_C_Section,"Addr",
            		pnd_Work_Letter_Image_File_Pnd_Compress_Address,"Cntrct2",
            		pnd_Work_Letter_Image_File_Pnd_O_Contract2);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  KCD
            //*  SWITCH CONTRACT
            if (condition(pnd_Work_Letter_Image_File_Pnd_O_Contract2.notEquals(" ")))                                                                                     //Natural: IF #O-CONTRACT2 NE ' '
            {
                pnd_Hold_Contract.setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                                                                                   //Natural: MOVE #O-CONTRACT2 TO #HOLD-CONTRACT
                pnd_Work_Letter_Image_File_Pnd_O_Contract2.setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                                                        //Natural: MOVE #O-CONTRACT-NO TO #O-CONTRACT2
                pnd_Work_Letter_Image_File_Pnd_O_Contract_No.setValue(pnd_Hold_Contract);                                                                                 //Natural: MOVE #HOLD-CONTRACT TO #O-CONTRACT-NO
                //*  PIN + MAILTO ADDRESS
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT BREAK OF #COMPRESS-ADDRESS
            if (condition(pnd_First_Read.getBoolean()))                                                                                                                   //Natural: IF #FIRST-READ
            {
                pnd_Mailto_Pin.setValue(pnd_Work_Letter_Image_File_Pnd_O_Pin_No);                                                                                         //Natural: ASSIGN #MAILTO-PIN := #O-PIN-NO
                pnd_Mailto_C_Mit_Wpid.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid);                                                                              //Natural: ASSIGN #MAILTO-C-MIT-WPID := #O-C-MIT-WPID
                pnd_Mailto_C_Mit_Log_Date_Time.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time);                                                            //Natural: ASSIGN #MAILTO-C-MIT-LOG-DATE-TIME := #O-C-MIT-LOG-DATE-TIME
                pnd_Mailto_C_Mit_Status_Cde.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Status_Cde);                                                                  //Natural: ASSIGN #MAILTO-C-MIT-STATUS-CDE := #O-C-MIT-STATUS-CDE
                pnd_Mailto_C_Mit_Unit_Cde.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde);                                                                      //Natural: ASSIGN #MAILTO-C-MIT-UNIT-CDE := #O-C-MIT-UNIT-CDE
                pnd_Mailto_M_Destination.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Destination);                                                                        //Natural: ASSIGN #MAILTO-M-DESTINATION := #O-M-DESTINATION
                pnd_Mailto_Lob.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Lob);                                                                                          //Natural: ASSIGN #MAILTO-LOB := #O-M-LOB
                pnd_Mailto_M_Name.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Name);                                                                                      //Natural: ASSIGN #MAILTO-M-NAME := #O-M-NAME
                pnd_Mailto_M_Address_1.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_1);                                                                            //Natural: ASSIGN #MAILTO-M-ADDRESS-1 := #O-M-ADDRESS-1
                pnd_Mailto_M_Address_2.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_2);                                                                            //Natural: ASSIGN #MAILTO-M-ADDRESS-2 := #O-M-ADDRESS-2
                pnd_Mailto_M_Address_3.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_3);                                                                            //Natural: ASSIGN #MAILTO-M-ADDRESS-3 := #O-M-ADDRESS-3
                pnd_Mailto_M_Address_4.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_4);                                                                            //Natural: ASSIGN #MAILTO-M-ADDRESS-4 := #O-M-ADDRESS-4
                pnd_Mailto_M_Address_5.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_5);                                                                            //Natural: ASSIGN #MAILTO-M-ADDRESS-5 := #O-M-ADDRESS-5
                pnd_Mailto_M_Postal_Data.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data);                                                                        //Natural: ASSIGN #MAILTO-M-POSTAL-DATA := #O-M-POSTAL-DATA
                pnd_Mailto_Compress_Addr.setValue(pnd_Work_Letter_Image_File_Pnd_Compress_Address);                                                                       //Natural: ASSIGN #MAILTO-COMPRESS-ADDR := #COMPRESS-ADDRESS
                pnd_Output_Time.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm);                                                                             //Natural: ASSIGN #OUTPUT-TIME := #O-C-ADDR-LAST-CH-TM
                if (condition(DbsUtil.maskMatches(pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt,"YYYYMMDD")))                                                        //Natural: IF #O-C-ADDR-LAST-CH-DT = MASK ( YYYYMMDD )
                {
                    pnd_Output_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt);                                    //Natural: MOVE EDITED #O-C-ADDR-LAST-CH-DT TO #OUTPUT-DATE ( EM = YYYYMMDD )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Date.reset();                                                                                                                              //Natural: RESET #OUTPUT-DATE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Co_Write.setValue(true);                                                                                                                              //Natural: ASSIGN #CO-WRITE := #CM-WRITE := #RS-WRITE := #FCO-WRITE := #FCM-WRITE := #FRS-WRITE := #VCO-WRITE := #VCM-WRITE := TRUE
                pnd_Cm_Write.setValue(true);
                pnd_Rs_Write.setValue(true);
                pnd_Fco_Write.setValue(true);
                pnd_Fcm_Write.setValue(true);
                pnd_Frs_Write.setValue(true);
                pnd_Vco_Write.setValue(true);
                pnd_Vcm_Write.setValue(true);
                pnd_Co_Start.setValue(1);                                                                                                                                 //Natural: ASSIGN #CO-START := #CM-START := #RS-START := #FCO-START := #FCM-START := #FRS-START := #VCO-START := #VCM-START := 1
                pnd_Cm_Start.setValue(1);
                pnd_Rs_Start.setValue(1);
                pnd_Fco_Start.setValue(1);
                pnd_Fcm_Start.setValue(1);
                pnd_Frs_Start.setValue(1);
                pnd_Vco_Start.setValue(1);
                pnd_Vcm_Start.setValue(1);
                pnd_First_Read.reset();                                                                                                                                   //Natural: RESET #FIRST-READ
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Mailto_Compress_Addr.equals(pnd_Work_Letter_Image_File_Pnd_Compress_Address)))                                                              //Natural: IF #MAILTO-COMPRESS-ADDR = #COMPRESS-ADDRESS
            {
                DbsUtil.examine(new ExamineSource(pnd_Mit_Array_Pnd_A_Mit_Log_Date_Time.getValue("*"),true), new ExamineSearch(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time,  //Natural: EXAMINE FULL VALUE #A-MIT-LOG-DATE-TIME ( * ) FOR FULL VALUE #O-C-MIT-LOG-DATE-TIME GIVING INDEX #MIT-PNTR
                    true), new ExamineGivingIndex(pnd_Mit_Pntr));
                if (condition(pnd_Mit_Pntr.equals(getZero())))                                                                                                            //Natural: IF #MIT-PNTR = 0
                {
                    pnd_Mit_Ctr.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #MIT-CTR
                    pnd_Mit_Array_Pnd_A_Mit_Wpid.getValue(pnd_Mit_Ctr).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid);                                             //Natural: ASSIGN #A-MIT-WPID ( #MIT-CTR ) := #O-C-MIT-WPID
                    pnd_Mit_Array_Pnd_A_Mit_Unit_Cde.getValue(pnd_Mit_Ctr).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde);                                     //Natural: ASSIGN #A-MIT-UNIT-CDE ( #MIT-CTR ) := #O-C-MIT-UNIT-CDE
                    pnd_Mit_Array_Pnd_A_Mit_Log_Date_Time.getValue(pnd_Mit_Ctr).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time);                           //Natural: ASSIGN #A-MIT-LOG-DATE-TIME ( #MIT-CTR ) := #O-C-MIT-LOG-DATE-TIME
                    pnd_Mit_Array_Pnd_A_Mit_Status_Cde.getValue(pnd_Mit_Ctr).setValue(8781);                                                                              //Natural: ASSIGN #A-MIT-STATUS-CDE ( #MIT-CTR ) := 8781
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  PSTA9612
                pnd_Mit_Ctr.reset();                                                                                                                                      //Natural: RESET #MIT-CTR #MIT-ARRAY ( * )
                pnd_Mit_Array.getValue("*").reset();
                pnd_Mit_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #MIT-CTR
                pnd_Mit_Array_Pnd_A_Mit_Wpid.getValue(pnd_Mit_Ctr).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid);                                                 //Natural: ASSIGN #A-MIT-WPID ( #MIT-CTR ) := #O-C-MIT-WPID
                pnd_Mit_Array_Pnd_A_Mit_Unit_Cde.getValue(pnd_Mit_Ctr).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde);                                         //Natural: ASSIGN #A-MIT-UNIT-CDE ( #MIT-CTR ) := #O-C-MIT-UNIT-CDE
                pnd_Mit_Array_Pnd_A_Mit_Log_Date_Time.getValue(pnd_Mit_Ctr).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time);                               //Natural: ASSIGN #A-MIT-LOG-DATE-TIME ( #MIT-CTR ) := #O-C-MIT-LOG-DATE-TIME
                pnd_Mit_Array_Pnd_A_Mit_Status_Cde.getValue(pnd_Mit_Ctr).setValue(8781);                                                                                  //Natural: ASSIGN #A-MIT-STATUS-CDE ( #MIT-CTR ) := 8781
                pnd_Mailto_Compress_Addr.setValue(pnd_Work_Letter_Image_File_Pnd_Compress_Address);                                                                       //Natural: ASSIGN #MAILTO-COMPRESS-ADDR := #COMPRESS-ADDRESS
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Letter_Image_File_Pnd_O_Usage.equals("RS")))                                                                                           //Natural: IF #O-USAGE = 'RS'
            {
                pnd_Address_Compare.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Letter_Image_File_Pnd_O_R_Address_1));                              //Natural: COMPRESS #O-R-ADDRESS-1 INTO #ADDRESS-COMPARE LEAVING NO
                //*  CO OR CM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Address_Compare.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Letter_Image_File_Pnd_O_C_Address_1));                              //Natural: COMPRESS #O-C-ADDRESS-1 INTO #ADDRESS-COMPARE LEAVING NO
            }                                                                                                                                                             //Natural: END-IF
            //*  CORRESPONDENCE
            short decideConditionsMet1169 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #O-C-SECTION;//Natural: VALUE '1', '3'
            if (condition((pnd_Work_Letter_Image_File_Pnd_O_C_Section.equals("1") || pnd_Work_Letter_Image_File_Pnd_O_C_Section.equals("3"))))
            {
                decideConditionsMet1169++;
                DbsUtil.examine(new ExamineSource(pnd_Co_Group_Pnd_Co_Address_Compare.getValue("*"),true), new ExamineSearch(pnd_Address_Compare, true),                  //Natural: EXAMINE FULL VALUE #CO-ADDRESS-COMPARE ( * ) FOR FULL VALUE #ADDRESS-COMPARE GIVING INDEX #CO-PNTR
                    new ExamineGivingIndex(pnd_Co_Pntr));
                if (condition(pnd_Co_Pntr.equals(getZero())))                                                                                                             //Natural: IF #CO-PNTR = 0
                {
                    pnd_Co_Cnt.setValue(0);                                                                                                                               //Natural: ASSIGN #CO-CNT := 0
                                                                                                                                                                          //Natural: PERFORM MOVE-CO
                    sub_Move_Co();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Co_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #CO-CNT
                    if (condition(pnd_Co_Cnt.greater(20)))                                                                                                                //Natural: IF #CO-CNT > 20
                    {
                        pnd_Co_Cnt.reset();                                                                                                                               //Natural: RESET #CO-CNT
                                                                                                                                                                          //Natural: PERFORM MOVE-CO
                        sub_Move_Co();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  KCD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Co_Group_Pnd_Co_Contract.getValue(pnd_Co_Idx,pnd_Co_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                              //Natural: ASSIGN #CO-CONTRACT ( #CO-IDX, #CO-CNT ) := #O-CONTRACT-NO
                        pnd_Co_Group_Pnd_Co_Itd_Contract.getValue(pnd_Co_Idx,pnd_Co_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                            //Natural: ASSIGN #CO-ITD-CONTRACT ( #CO-IDX, #CO-CNT ) := #O-CONTRACT2
                    }                                                                                                                                                     //Natural: END-IF
                    //*  CHECK-MAILING
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pnd_Work_Letter_Image_File_Pnd_O_C_Section.equals("2"))))
            {
                decideConditionsMet1169++;
                //* *    WRITE 'BEFORE EXAMINE'
                //* *    WRITE '=' #ADDRESS-COMPARE / '=' #CM-ADDRESS-COMPARE (*)
                DbsUtil.examine(new ExamineSource(pnd_Cm_Group_Pnd_Cm_Address_Compare.getValue("*"),true), new ExamineSearch(pnd_Address_Compare, true),                  //Natural: EXAMINE FULL VALUE #CM-ADDRESS-COMPARE ( * ) FOR FULL VALUE #ADDRESS-COMPARE GIVING INDEX #CM-PNTR
                    new ExamineGivingIndex(pnd_Cm_Pntr));
                //* *    WRITE '=' #CM-PNTR
                if (condition(pnd_Cm_Pntr.equals(getZero())))                                                                                                             //Natural: IF #CM-PNTR = 0
                {
                    pnd_Cm_Cnt.setValue(0);                                                                                                                               //Natural: ASSIGN #CM-CNT := 0
                                                                                                                                                                          //Natural: PERFORM MOVE-CM
                    sub_Move_Cm();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cm_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #CM-CNT
                    if (condition(pnd_Cm_Cnt.greater(20)))                                                                                                                //Natural: IF #CM-CNT > 20
                    {
                        pnd_Cm_Cnt.reset();                                                                                                                               //Natural: RESET #CM-CNT
                                                                                                                                                                          //Natural: PERFORM MOVE-CM
                        sub_Move_Cm();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //* KCD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //* *        WRITE 'KEN ' #CM-IDX  #CM-CNT
                        pnd_Cm_Group_Pnd_Cm_Contract.getValue(pnd_Cm_Idx,pnd_Cm_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                              //Natural: ASSIGN #CM-CONTRACT ( #CM-IDX, #CM-CNT ) := #O-CONTRACT-NO
                        pnd_Cm_Group_Pnd_Cm_Itd_Contract.getValue(pnd_Cm_Idx,pnd_Cm_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                            //Natural: ASSIGN #CM-ITD-CONTRACT ( #CM-IDX, #CM-CNT ) := #O-CONTRACT2
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RESIDENTIAL
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((pnd_Work_Letter_Image_File_Pnd_O_C_Section.equals("3"))))
            {
                decideConditionsMet1169++;
                pnd_Rs_Idx.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #RS-IDX
                //*  DONT MOVE IN CO
                //*  FUTURE CORRESPONDENCE
                pnd_Rs_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #RS-CNT
                pnd_Rs_Reach.setValue("Y");                                                                                                                               //Natural: ASSIGN #RS-REACH := 'Y'
                pnd_Rs_Group_Pnd_Rs_Name.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Name);                                                                               //Natural: ASSIGN #RS-NAME := #O-M-NAME
                pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue(1).setValue(pnd_Work_Letter_Image_File_Pnd_O_R_Address_1);                                                     //Natural: ASSIGN #RS-NEW-MAIL-ADDR ( 1 ) := #O-R-ADDRESS-1
                pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue(2).setValue(pnd_Work_Letter_Image_File_Pnd_O_R_Address_2);                                                     //Natural: ASSIGN #RS-NEW-MAIL-ADDR ( 2 ) := #O-R-ADDRESS-2
                pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue(3).setValue(pnd_Work_Letter_Image_File_Pnd_O_R_Address_3);                                                     //Natural: ASSIGN #RS-NEW-MAIL-ADDR ( 3 ) := #O-R-ADDRESS-3
                pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue(4).setValue(pnd_Work_Letter_Image_File_Pnd_O_R_Address_4);                                                     //Natural: ASSIGN #RS-NEW-MAIL-ADDR ( 4 ) := #O-R-ADDRESS-4
                pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue(5).setValue(pnd_Work_Letter_Image_File_Pnd_O_R_Address_5);                                                     //Natural: ASSIGN #RS-NEW-MAIL-ADDR ( 5 ) := #O-R-ADDRESS-5
                pnd_Rs_Group_Pnd_Rs_New_Mail_Zip.setValue(pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data);                                                                //Natural: ASSIGN #RS-NEW-MAIL-ZIP := #O-R-POSTAL-DATA
                pnd_Rs_Group_Pnd_Rs_Address_Compare.setValue(pnd_Address_Compare);                                                                                        //Natural: ASSIGN #RS-ADDRESS-COMPARE := #ADDRESS-COMPARE
            }                                                                                                                                                             //Natural: VALUE '4'
            else if (condition((pnd_Work_Letter_Image_File_Pnd_O_C_Section.equals("4"))))
            {
                decideConditionsMet1169++;
                DbsUtil.examine(new ExamineSource(pnd_Fco_Group_Pnd_Fco_Address_Compare.getValue("*"),true), new ExamineSearch(pnd_Address_Compare, true),                //Natural: EXAMINE FULL VALUE #FCO-ADDRESS-COMPARE ( * ) FOR FULL VALUE #ADDRESS-COMPARE GIVING INDEX #FCO-PNTR
                    new ExamineGivingIndex(pnd_Fco_Pntr));
                if (condition(pnd_Fco_Pntr.equals(getZero())))                                                                                                            //Natural: IF #FCO-PNTR = 0
                {
                    pnd_Fco_Cnt.setValue(0);                                                                                                                              //Natural: ASSIGN #FCO-CNT := 0
                                                                                                                                                                          //Natural: PERFORM MOVE-FCO
                    sub_Move_Fco();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Fco_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #FCO-CNT
                    if (condition(pnd_Fco_Cnt.greater(20)))                                                                                                               //Natural: IF #FCO-CNT > 20
                    {
                        pnd_Fco_Cnt.reset();                                                                                                                              //Natural: RESET #FCO-CNT
                                                                                                                                                                          //Natural: PERFORM MOVE-FCO
                        sub_Move_Fco();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //* KCD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Fco_Group_Pnd_Fco_Contract.getValue(pnd_Fco_Idx,pnd_Fco_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                          //Natural: ASSIGN #FCO-CONTRACT ( #FCO-IDX, #FCO-CNT ) := #O-CONTRACT-NO
                        pnd_Fco_Group_Pnd_Fco_Itd_Contract.getValue(pnd_Fco_Idx,pnd_Fco_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                        //Natural: ASSIGN #FCO-ITD-CONTRACT ( #FCO-IDX, #FCO-CNT ) := #O-CONTRACT2
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FUTURE CHECK-MAILING
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((pnd_Work_Letter_Image_File_Pnd_O_C_Section.equals("5"))))
            {
                decideConditionsMet1169++;
                DbsUtil.examine(new ExamineSource(pnd_Fcm_Group_Pnd_Fcm_Address_Compare.getValue("*"),true), new ExamineSearch(pnd_Address_Compare, true),                //Natural: EXAMINE FULL VALUE #FCM-ADDRESS-COMPARE ( * ) FOR FULL VALUE #ADDRESS-COMPARE GIVING INDEX #FCM-PNTR
                    new ExamineGivingIndex(pnd_Fcm_Pntr));
                if (condition(pnd_Fcm_Pntr.equals(getZero())))                                                                                                            //Natural: IF #FCM-PNTR = 0
                {
                    pnd_Fcm_Cnt.setValue(0);                                                                                                                              //Natural: ASSIGN #FCM-CNT := 0
                                                                                                                                                                          //Natural: PERFORM MOVE-FCM
                    sub_Move_Fcm();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Fcm_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #FCM-CNT
                    if (condition(pnd_Fcm_Cnt.greater(20)))                                                                                                               //Natural: IF #FCM-CNT > 20
                    {
                        pnd_Fcm_Cnt.reset();                                                                                                                              //Natural: RESET #FCM-CNT
                                                                                                                                                                          //Natural: PERFORM MOVE-FCM
                        sub_Move_Fcm();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //* KCD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Fcm_Group_Pnd_Fcm_Contract.getValue(pnd_Fcm_Idx,pnd_Fcm_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                          //Natural: ASSIGN #FCM-CONTRACT ( #FCM-IDX, #FCM-CNT ) := #O-CONTRACT-NO
                        pnd_Fcm_Group_Pnd_Fcm_Itd_Contract.getValue(pnd_Fcm_Idx,pnd_Fcm_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                        //Natural: ASSIGN #FCM-ITD-CONTRACT ( #FCM-IDX, #FCM-CNT ) := #O-CONTRACT2
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FUTURE RESIDENTIAL
                    //*  #O-C-PERM-ADDRESS-CH-DT
                    //*  TEMPORARY CORRESPONDENCE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '6'
            else if (condition((pnd_Work_Letter_Image_File_Pnd_O_C_Section.equals("6"))))
            {
                decideConditionsMet1169++;
                pnd_Post_Mmddyyyy_Pnd_P_Ccyy.setValue(pnd_Work_Letter_Image_File_Pnd_R_Ccyy);                                                                             //Natural: ASSIGN #P-CCYY := #R-CCYY
                pnd_Post_Mmddyyyy_Pnd_P_Mm.setValue(pnd_Work_Letter_Image_File_Pnd_R_Mm);                                                                                 //Natural: ASSIGN #P-MM := #R-MM
                pnd_Post_Mmddyyyy_Pnd_P_Dd.setValue(pnd_Work_Letter_Image_File_Pnd_R_Dd);                                                                                 //Natural: ASSIGN #P-DD := #R-DD
                pnd_Frs_Idx.setValue(1);                                                                                                                                  //Natural: ASSIGN #FRS-IDX := #FRS-CNT := 1
                pnd_Frs_Cnt.setValue(1);
                pnd_Frs_Reach.setValue("Y");                                                                                                                              //Natural: ASSIGN #FRS-REACH := 'Y'
                pnd_Frs_Group_Pnd_Frs_Name.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Name);                                                                             //Natural: ASSIGN #FRS-NAME := #O-C-NAME
                pnd_Frs_Group_Pnd_Frs_Mail_Addr.getValue(1).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_1);                                                       //Natural: ASSIGN #FRS-MAIL-ADDR ( 1 ) := #O-C-ADDRESS-1
                pnd_Frs_Group_Pnd_Frs_Mail_Addr.getValue(2).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_2);                                                       //Natural: ASSIGN #FRS-MAIL-ADDR ( 2 ) := #O-C-ADDRESS-2
                pnd_Frs_Group_Pnd_Frs_Mail_Addr.getValue(3).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_3);                                                       //Natural: ASSIGN #FRS-MAIL-ADDR ( 3 ) := #O-C-ADDRESS-3
                pnd_Frs_Group_Pnd_Frs_Mail_Addr.getValue(4).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_4);                                                       //Natural: ASSIGN #FRS-MAIL-ADDR ( 4 ) := #O-C-ADDRESS-4
                pnd_Frs_Group_Pnd_Frs_Mail_Addr.getValue(5).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_5);                                                       //Natural: ASSIGN #FRS-MAIL-ADDR ( 5 ) := #O-C-ADDRESS-5
                pnd_Frs_Group_Pnd_Frs_Mail_Zip.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data);                                                                  //Natural: ASSIGN #FRS-MAIL-ZIP := #O-C-POSTAL-DATA
                pnd_Frs_Group_Pnd_Frs_Address_Compare.setValue(pnd_Address_Compare);                                                                                      //Natural: ASSIGN #FRS-ADDRESS-COMPARE := #ADDRESS-COMPARE
                pnd_Frs_Group_Pnd_Frs_Begin_Dte.setValue(pnd_Post_Mmddyyyy);                                                                                              //Natural: ASSIGN #FRS-BEGIN-DTE := #POST-MMDDYYYY
            }                                                                                                                                                             //Natural: VALUE '7'
            else if (condition((pnd_Work_Letter_Image_File_Pnd_O_C_Section.equals("7"))))
            {
                decideConditionsMet1169++;
                DbsUtil.examine(new ExamineSource(pnd_Vco_Group_Pnd_Vco_Address_Compare.getValue("*"),true), new ExamineSearch(pnd_Address_Compare, true),                //Natural: EXAMINE FULL VALUE #VCO-ADDRESS-COMPARE ( * ) FOR FULL VALUE #ADDRESS-COMPARE GIVING INDEX #VCO-PNTR
                    new ExamineGivingIndex(pnd_Vco_Pntr));
                if (condition(pnd_Vco_Pntr.equals(getZero())))                                                                                                            //Natural: IF #VCO-PNTR = 0
                {
                    pnd_Vco_Cnt.setValue(0);                                                                                                                              //Natural: ASSIGN #VCO-CNT := 0
                                                                                                                                                                          //Natural: PERFORM MOVE-VCO
                    sub_Move_Vco();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Vco_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #VCO-CNT
                    if (condition(pnd_Vco_Cnt.greater(20)))                                                                                                               //Natural: IF #VCO-CNT > 20
                    {
                        pnd_Vco_Cnt.reset();                                                                                                                              //Natural: RESET #VCO-CNT
                                                                                                                                                                          //Natural: PERFORM MOVE-VCO
                        sub_Move_Vco();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //* KCD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Vco_Group_Pnd_Vco_Contract.getValue(pnd_Vco_Idx,pnd_Vco_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                          //Natural: ASSIGN #VCO-CONTRACT ( #VCO-IDX, #VCO-CNT ) := #O-CONTRACT-NO
                        pnd_Vco_Group_Pnd_Vco_Itd_Contract.getValue(pnd_Vco_Idx,pnd_Vco_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                        //Natural: ASSIGN #VCO-ITD-CONTRACT ( #VCO-IDX, #VCO-CNT ) := #O-CONTRACT2
                    }                                                                                                                                                     //Natural: END-IF
                    //*  TEMPORARY CHECK-MAILING
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '8'
            else if (condition((pnd_Work_Letter_Image_File_Pnd_O_C_Section.equals("8"))))
            {
                decideConditionsMet1169++;
                DbsUtil.examine(new ExamineSource(pnd_Vcm_Group_Pnd_Vcm_Address_Compare.getValue("*"),true), new ExamineSearch(pnd_Address_Compare, true),                //Natural: EXAMINE FULL VALUE #VCM-ADDRESS-COMPARE ( * ) FOR FULL VALUE #ADDRESS-COMPARE GIVING INDEX #VCM-PNTR
                    new ExamineGivingIndex(pnd_Vcm_Pntr));
                if (condition(pnd_Vcm_Pntr.equals(getZero())))                                                                                                            //Natural: IF #VCM-PNTR = 0
                {
                    pnd_Vcm_Cnt.setValue(0);                                                                                                                              //Natural: ASSIGN #VCM-CNT := 0
                                                                                                                                                                          //Natural: PERFORM MOVE-VCM
                    sub_Move_Vcm();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Vcm_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #VCM-CNT
                    if (condition(pnd_Vcm_Cnt.greater(20)))                                                                                                               //Natural: IF #VCM-CNT > 20
                    {
                        pnd_Vcm_Cnt.reset();                                                                                                                              //Natural: RESET #VCM-CNT
                                                                                                                                                                          //Natural: PERFORM MOVE-VCM
                        sub_Move_Vcm();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //* KCD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Vcm_Group_Pnd_Vcm_Contract.getValue(pnd_Vcm_Idx,pnd_Vcm_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                          //Natural: ASSIGN #VCM-CONTRACT ( #VCM-IDX, #VCM-CNT ) := #O-CONTRACT-NO
                        pnd_Vcm_Group_Pnd_Vcm_Itd_Contract.getValue(pnd_Vcm_Idx,pnd_Vcm_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                        //Natural: ASSIGN #VCM-ITD-CONTRACT ( #VCM-IDX, #VCM-CNT ) := #O-CONTRACT2
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        if (condition(pnd_Close.getBoolean()))                                                                                                                            //Natural: IF #CLOSE
        {
                                                                                                                                                                          //Natural: PERFORM POST-CLOSE
            sub_Post_Close();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL POST ERROR       :",pnd_Post_Error, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                              //Natural: WRITE ( 1 ) / 'TOTAL POST ERROR       :' #POST-ERROR
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"TOTAL LETTERS CREATED  :",pnd_Letters_Created, new ReportEditMask ("Z,ZZZ,ZZZ,Z99"));                         //Natural: WRITE ( 2 ) / 'TOTAL LETTERS CREATED  :' #LETTERS-CREATED
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,"TOTAL RECORDS IN ERROR :",pnd_Error_Work, new ReportEditMask ("Z,ZZZ,ZZZ,Z99"));                              //Natural: WRITE ( 3 ) / 'TOTAL RECORDS IN ERROR :' #ERROR-WORK
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-ADDRESS-TO-POST
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-CM
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-CO
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-FCM
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-FCO
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-VCM
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-VCO
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-CLOSE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-ERROR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-OPEN
        //* ***********************************************************************
        //* ** PSTA9612.UNIT-CDE         (#IDX) := #A-MIT-UNIT-CDE      (#IDX) ***
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-WRITE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-REPORT
        //*  THIS WILL CREATE THE NEW CORRESPONDENCE ADDRESS LINE
        //*  THIS WILL CREATE THE NEW FUTURE CORRESPONDENCE ADDRESS LINE
        //*  THIS WILL CREATE THE NEW FUTURE RESIDENTIAL ADDRESS LINE
        //*  THIS WILL CREATE THE NEW FUTURE CHECK-MAILING ADDRESS LINE
        //*  THIS WILL CREATE THE NEW TEMPORAY ADDRESS LINE
        //*  THIS WILL CREATE THE NEW TEMPORARY CHECK-MAILING ADDRESS LINE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR-REPORT
        //* ***********************************************************************
    }
    private void sub_Initialization() throws Exception                                                                                                                    //Natural: INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM MOVE-ADDRESS-TO-POST
        sub_Move_Address_To_Post();
        if (condition(Global.isEscape())) {return;}
        pnd_Mailto_Pin.setValue(pnd_Work_Letter_Image_File_Pnd_O_Pin_No);                                                                                                 //Natural: ASSIGN #MAILTO-PIN := #O-PIN-NO
        pnd_Mailto_C_Mit_Wpid.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Wpid);                                                                                      //Natural: ASSIGN #MAILTO-C-MIT-WPID := #O-C-MIT-WPID
        pnd_Mailto_C_Mit_Log_Date_Time.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Log_Date_Time);                                                                    //Natural: ASSIGN #MAILTO-C-MIT-LOG-DATE-TIME := #O-C-MIT-LOG-DATE-TIME
        pnd_Mailto_C_Mit_Status_Cde.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Status_Cde);                                                                          //Natural: ASSIGN #MAILTO-C-MIT-STATUS-CDE := #O-C-MIT-STATUS-CDE
        pnd_Mailto_C_Mit_Unit_Cde.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Mit_Unit_Cde);                                                                              //Natural: ASSIGN #MAILTO-C-MIT-UNIT-CDE := #O-C-MIT-UNIT-CDE
        pnd_Mailto_M_Destination.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Destination);                                                                                //Natural: ASSIGN #MAILTO-M-DESTINATION := #O-M-DESTINATION
        pnd_Mailto_Lob.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Lob);                                                                                                  //Natural: ASSIGN #MAILTO-LOB := #O-M-LOB
        pnd_Mailto_M_Name.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Name);                                                                                              //Natural: ASSIGN #MAILTO-M-NAME := #O-M-NAME
        pnd_Mailto_M_Address_1.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_1);                                                                                    //Natural: ASSIGN #MAILTO-M-ADDRESS-1 := #O-M-ADDRESS-1
        pnd_Mailto_M_Address_2.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_2);                                                                                    //Natural: ASSIGN #MAILTO-M-ADDRESS-2 := #O-M-ADDRESS-2
        pnd_Mailto_M_Address_3.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_3);                                                                                    //Natural: ASSIGN #MAILTO-M-ADDRESS-3 := #O-M-ADDRESS-3
        pnd_Mailto_M_Address_4.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_4);                                                                                    //Natural: ASSIGN #MAILTO-M-ADDRESS-4 := #O-M-ADDRESS-4
        pnd_Mailto_M_Address_5.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Address_5);                                                                                    //Natural: ASSIGN #MAILTO-M-ADDRESS-5 := #O-M-ADDRESS-5
        pnd_Mailto_M_Postal_Data.setValue(pnd_Work_Letter_Image_File_Pnd_O_M_Postal_Data);                                                                                //Natural: ASSIGN #MAILTO-M-POSTAL-DATA := #O-M-POSTAL-DATA
        pnd_Output_Time.setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Tm);                                                                                     //Natural: ASSIGN #OUTPUT-TIME := #O-C-ADDR-LAST-CH-TM
        pnd_First_Write.setValue(true);                                                                                                                                   //Natural: ASSIGN #FIRST-WRITE := TRUE
        if (condition(DbsUtil.maskMatches(pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt,"YYYYMMDD")))                                                                //Natural: IF #O-C-ADDR-LAST-CH-DT = MASK ( YYYYMMDD )
        {
            pnd_Output_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Last_Ch_Dt);                                            //Natural: MOVE EDITED #O-C-ADDR-LAST-CH-DT TO #OUTPUT-DATE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_Date.reset();                                                                                                                                      //Natural: RESET #OUTPUT-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pstl6635_Data_Pstl6635_Rec_Id.reset();                                                                                                                            //Natural: RESET PSTL6635-REC-ID #BANK ( * ) #CO-IDX #CO-GROUP #CO-REACH #CM-IDX #CM-GROUP #CM-REACH #RS-IDX #RS-GROUP #RS-REACH #FCO-IDX #FCO-GROUP #FCO-REACH #FCM-IDX #FCM-GROUP #FCM-REACH #FRS-IDX #FRS-GROUP #FRS-REACH #VCO-IDX #VCO-GROUP #VCO-REACH #VCM-IDX #VCM-GROUP #VCM-REACH
        pnd_Bank.getValue("*").reset();
        pnd_Co_Idx.reset();
        pnd_Co_Group.reset();
        pnd_Co_Reach.reset();
        pnd_Cm_Idx.reset();
        pnd_Cm_Group.reset();
        pnd_Cm_Reach.reset();
        pnd_Rs_Idx.reset();
        pnd_Rs_Group.reset();
        pnd_Rs_Reach.reset();
        pnd_Fco_Idx.reset();
        pnd_Fco_Group.reset();
        pnd_Fco_Reach.reset();
        pnd_Fcm_Idx.reset();
        pnd_Fcm_Group.reset();
        pnd_Fcm_Reach.reset();
        pnd_Frs_Idx.reset();
        pnd_Frs_Group.reset();
        pnd_Frs_Reach.reset();
        pnd_Vco_Idx.reset();
        pnd_Vco_Group.reset();
        pnd_Vco_Reach.reset();
        pnd_Vcm_Idx.reset();
        pnd_Vcm_Group.reset();
        pnd_Vcm_Reach.reset();
        //*  INITIALIZATION
    }
    private void sub_Move_Address_To_Post() throws Exception                                                                                                              //Natural: MOVE-ADDRESS-TO-POST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pstl6635_Data_Pstl6635_Data_Array.getValue("*").reset();                                                                                                          //Natural: RESET PSTL6635-DATA-ARRAY ( * ) #POST-CNT #POST-LOOP #LOOP
        pnd_Post_Cnt.reset();
        pnd_Post_Loop.reset();
        pnd_Loop.reset();
        pstl6635_Data_Pstl6635_Rec_Id.setValue("A");                                                                                                                      //Natural: ASSIGN PSTL6635-REC-ID := 'A'
        //*    /* START OF DELAY CODE  PRB97743
        if (condition(pnd_Prev_Pin_No.equals(pnd_Mailto_Pin)))                                                                                                            //Natural: IF #PREV-PIN-NO EQ #MAILTO-PIN
        {
            getReports().write(0, ReportOption.NOTITLE,"***** FOUND LETTER FOR SAME PIN - WAIT FOR TIME CHANGE *****");                                                   //Natural: WRITE '***** FOUND LETTER FOR SAME PIN - WAIT FOR TIME CHANGE *****'
            if (Global.isEscape()) return;
            pnd_Hold_Time.setValue(Global.getTIMX());                                                                                                                     //Natural: MOVE *TIMX TO #HOLD-TIME
            pnd_Time.setValue(Global.getTIMX());                                                                                                                          //Natural: MOVE *TIMX TO #TIME
            getReports().write(0, ReportOption.NOTITLE,"         VALUE OF #HOLD TIME = ",pnd_Hold_Time, new ReportEditMask ("SS.T"),"**");                                //Natural: WRITE '         VALUE OF #HOLD TIME = ' #HOLD-TIME ( EM = SS.T ) '**'
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"         VALUE OF #TIME      = ",pnd_Time, new ReportEditMask ("SS.T"),"**");                                     //Natural: WRITE '         VALUE OF #TIME      = ' #TIME ( EM = SS.T ) '**'
            if (Global.isEscape()) return;
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(pnd_Time.greater(pnd_Hold_Time))) {break;}                                                                                                  //Natural: UNTIL #TIME > #HOLD-TIME
                pnd_Time.setValue(Global.getTIMX());                                                                                                                      //Natural: MOVE *TIMX TO #TIME
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"         VALUE OF #HOLD TIME = ",pnd_Hold_Time, new ReportEditMask ("SS.T"),"**");                                //Natural: WRITE '         VALUE OF #HOLD TIME = ' #HOLD-TIME ( EM = SS.T ) '**'
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"         VALUE OF #TIME      = ",pnd_Time, new ReportEditMask ("SS.T"),"**");                                     //Natural: WRITE '         VALUE OF #TIME      = ' #TIME ( EM = SS.T ) '**'
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"***** FOUND LETTER FOR SAME PIN - WAITED FOR TIME CHANGE *****");                                                 //Natural: WRITE '***** FOUND LETTER FOR SAME PIN - WAITED FOR TIME CHANGE *****'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prev_Pin_No.setValue(pnd_Mailto_Pin);                                                                                                                         //Natural: MOVE #MAILTO-PIN TO #PREV-PIN-NO
        //*    /* END OF DELAY CODE    PRB97743
                                                                                                                                                                          //Natural: PERFORM POST-OPEN
        sub_Post_Open();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Co_Idx.greater(getZero()) || pnd_Cm_Idx.greater(getZero()) || pnd_Rs_Idx.greater(getZero()) || pnd_Fco_Idx.greater(getZero())                   //Natural: IF #CO-IDX > 0 OR #CM-IDX > 0 OR #RS-IDX > 0 OR #FCO-IDX > 0 OR #FCM-IDX > 0 OR #FRS-IDX > 0 OR #VCO-IDX > 0 OR #VCM-IDX > 0
            || pnd_Fcm_Idx.greater(getZero()) || pnd_Frs_Idx.greater(getZero()) || pnd_Vco_Idx.greater(getZero()) || pnd_Vcm_Idx.greater(getZero())))
        {
            pnd_Letters_Created.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #LETTERS-CREATED
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #X = 1 TO #CO-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Co_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            pnd_Co_Post.setValue(true);                                                                                                                                   //Natural: ASSIGN #CO-POST := TRUE
            FOR02:                                                                                                                                                        //Natural: FOR #Y = 1 TO 20
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(20)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pnd_Co_Group_Pnd_Co_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ") || pnd_Co_Group_Pnd_Co_New_Mail_Addr1.getValue(pnd_Counters_Pnd_X).notEquals(" "))) //Natural: IF #CO-CONTRACT ( #X, #Y ) > ' ' OR #CO-NEW-MAIL-ADDR1 ( #X ) NE ' '
                {
                    if (condition(pnd_Co_Post.getBoolean()))                                                                                                              //Natural: IF #CO-POST
                    {
                        pnd_Loop.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #LOOP
                        pnd_Post_Loop.nadd(1);                                                                                                                            //Natural: ADD 1 TO #POST-LOOP
                        pnd_Co_Post.reset();                                                                                                                              //Natural: RESET #CO-POST
                        //*  NEW MAILING ADDRESS
                        //*  KCD
                        //*  KCD
                    }                                                                                                                                                     //Natural: END-IF
                    pstl6635_Data_Addr_Chng_Type.getValue(pnd_Loop).setValue("A");                                                                                        //Natural: ASSIGN ADDR-CHNG-TYPE ( #LOOP ) := 'A'
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,1).setValue(pnd_Co_Group_Pnd_Co_New_Mail_Addr1.getValue(pnd_Counters_Pnd_X));                               //Natural: ASSIGN ADDR-LINE ( #LOOP, 1 ) := #CO-NEW-MAIL-ADDR1 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,2).setValue(pnd_Co_Group_Pnd_Co_New_Mail_Addr2.getValue(pnd_Counters_Pnd_X));                               //Natural: ASSIGN ADDR-LINE ( #LOOP, 2 ) := #CO-NEW-MAIL-ADDR2 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,3).setValue(pnd_Co_Group_Pnd_Co_New_Mail_Addr3.getValue(pnd_Counters_Pnd_X));                               //Natural: ASSIGN ADDR-LINE ( #LOOP, 3 ) := #CO-NEW-MAIL-ADDR3 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,4).setValue(pnd_Co_Group_Pnd_Co_New_Mail_Addr4.getValue(pnd_Counters_Pnd_X));                               //Natural: ASSIGN ADDR-LINE ( #LOOP, 4 ) := #CO-NEW-MAIL-ADDR4 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,5).setValue(pnd_Co_Group_Pnd_Co_New_Mail_Addr5.getValue(pnd_Counters_Pnd_X));                               //Natural: ASSIGN ADDR-LINE ( #LOOP, 5 ) := #CO-NEW-MAIL-ADDR5 ( #X )
                    pstl6635_Data_Addr_Cntrct.getValue(pnd_Loop,pnd_Counters_Pnd_Y).setValue(pnd_Co_Group_Pnd_Co_Contract.getValue(pnd_Counters_Pnd_X,                    //Natural: ASSIGN ADDR-CNTRCT ( #LOOP, #Y ) := #CO-CONTRACT ( #X, #Y )
                        pnd_Counters_Pnd_Y));
                    pstl6635_Data_Itd_Contract_Out.getValue(pnd_Loop,pnd_Counters_Pnd_Y).setValue(pnd_Co_Group_Pnd_Co_Itd_Contract.getValue(pnd_Counters_Pnd_X,           //Natural: ASSIGN ITD-CONTRACT-OUT ( #LOOP, #Y ) := #CO-ITD-CONTRACT ( #X, #Y )
                        pnd_Counters_Pnd_Y));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Counters_Pnd_X.equals(pnd_Co_Idx)))                                                                                                         //Natural: IF #X = #CO-IDX
            {
                pnd_Max_Reach.setValue(true);                                                                                                                             //Natural: ASSIGN #MAX-REACH := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pnd_Loop.equals(8)) || (pnd_Cm_Reach.equals(" ") && pnd_Rs_Reach.equals(" ") && pnd_Fco_Reach.equals(" ") && pnd_Fcm_Reach.equals(" ")        //Natural: IF ( ( #LOOP = 8 ) OR ( #CM-REACH = ' ' AND #RS-REACH = ' ' AND #FCO-REACH = ' ' AND #FCM-REACH = ' ' AND #FRS-REACH = ' ' AND #VCO-REACH = ' ' AND #VCM-REACH = ' ' AND #MAX-REACH ) )
                && pnd_Frs_Reach.equals(" ") && pnd_Vco_Reach.equals(" ") && pnd_Vcm_Reach.equals(" ") && pnd_Max_Reach.getBoolean()))))
            {
                                                                                                                                                                          //Natural: PERFORM POST-WRITE
                sub_Post_Write();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-REPORT
                sub_Process_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Loop.reset();                                                                                                                                         //Natural: RESET #LOOP PSTL6635-DATA-ARRAY ( * )
                pstl6635_Data_Pstl6635_Data_Array.getValue("*").reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Max_Reach.reset();                                                                                                                                            //Natural: RESET #MAX-REACH
        FOR03:                                                                                                                                                            //Natural: FOR #X = 1 TO #CM-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Cm_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            pnd_Cm_Post.setValue(true);                                                                                                                                   //Natural: ASSIGN #CM-POST := TRUE
            FOR04:                                                                                                                                                        //Natural: FOR #Y = 1 TO 20
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(20)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pnd_Cm_Group_Pnd_Cm_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                                 //Natural: IF #CM-CONTRACT ( #X, #Y ) > ' '
                {
                    if (condition(pnd_Cm_Post.getBoolean()))                                                                                                              //Natural: IF #CM-POST
                    {
                        pnd_Loop.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #LOOP
                        pnd_Post_Loop.nadd(1);                                                                                                                            //Natural: ADD 1 TO #POST-LOOP
                        pnd_Cm_Post.reset();                                                                                                                              //Natural: RESET #CM-POST
                        //*  NEW CHECK-MAILING ADDRESS
                    }                                                                                                                                                     //Natural: END-IF
                    pstl6635_Data_Addr_Chng_Type.getValue(pnd_Loop).setValue("B");                                                                                        //Natural: ASSIGN ADDR-CHNG-TYPE ( #LOOP ) := 'B'
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,1).setValue(pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr1.getValue(pnd_Counters_Pnd_X));                               //Natural: ASSIGN ADDR-LINE ( #LOOP, 1 ) := #CM-PAY-MAIL-ADDR1 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,2).setValue(pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr2.getValue(pnd_Counters_Pnd_X));                               //Natural: ASSIGN ADDR-LINE ( #LOOP, 2 ) := #CM-PAY-MAIL-ADDR2 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,3).setValue(pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr3.getValue(pnd_Counters_Pnd_X));                               //Natural: ASSIGN ADDR-LINE ( #LOOP, 3 ) := #CM-PAY-MAIL-ADDR3 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,4).setValue(pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr4.getValue(pnd_Counters_Pnd_X));                               //Natural: ASSIGN ADDR-LINE ( #LOOP, 4 ) := #CM-PAY-MAIL-ADDR4 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,5).setValue(pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr5.getValue(pnd_Counters_Pnd_X));                               //Natural: ASSIGN ADDR-LINE ( #LOOP, 5 ) := #CM-PAY-MAIL-ADDR5 ( #X )
                    pstl6635_Data_Addr_Cntrct.getValue(pnd_Loop,pnd_Counters_Pnd_Y).setValue(pnd_Cm_Group_Pnd_Cm_Contract.getValue(pnd_Counters_Pnd_X,                    //Natural: ASSIGN ADDR-CNTRCT ( #LOOP, #Y ) := #CM-CONTRACT ( #X, #Y )
                        pnd_Counters_Pnd_Y));
                    pstl6635_Data_Itd_Contract_Out.getValue(pnd_Loop,pnd_Counters_Pnd_Y).setValue(pnd_Cm_Group_Pnd_Cm_Itd_Contract.getValue(pnd_Counters_Pnd_X,           //Natural: ASSIGN ITD-CONTRACT-OUT ( #LOOP, #Y ) := #CM-ITD-CONTRACT ( #X, #Y )
                        pnd_Counters_Pnd_Y));
                    pstl6635_Data_New_Pay_Bank_Info_Ind.getValue(pnd_Loop).setValue(pnd_Cm_Group_Pnd_Cm_Pay_Bank_Info_Ind.getValue(pnd_Counters_Pnd_X));                  //Natural: ASSIGN NEW-PAY-BANK-INFO-IND ( #LOOP ) := #CM-PAY-BANK-INFO-IND ( #X )
                    pstl6635_Data_New_Pay_Bnk_Acct_Nbr.getValue(pnd_Loop).setValue(pnd_Bank_Pnd_Bank_Acct.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN NEW-PAY-BNK-ACCT-NBR ( #LOOP ) := #BANK-ACCT ( #X )
                    pstl6635_Data_New_Pay_Cs_Ind.getValue(pnd_Loop).setValue(pnd_Cm_Group_Pnd_Cm_Pay_Cs_Ind.getValue(pnd_Counters_Pnd_X));                                //Natural: ASSIGN NEW-PAY-CS-IND ( #LOOP ) := #CM-PAY-CS-IND ( #X )
                    pstl6635_Data_New_Pay_Eft_Nbr.getValue(pnd_Loop).setValue(pnd_Bank_Pnd_Eft_Acct.getValue(pnd_Counters_Pnd_X));                                        //Natural: ASSIGN NEW-PAY-EFT-NBR ( #LOOP ) := #EFT-ACCT ( #X )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Counters_Pnd_X.equals(pnd_Cm_Idx)))                                                                                                         //Natural: IF #X = #CM-IDX
            {
                pnd_Max_Reach.setValue(true);                                                                                                                             //Natural: ASSIGN #MAX-REACH := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pnd_Loop.equals(8)) || (pnd_Rs_Reach.equals(" ") && pnd_Fco_Reach.equals(" ") && pnd_Fcm_Reach.equals(" ") && pnd_Frs_Reach.equals(" ")       //Natural: IF ( ( #LOOP = 8 ) OR ( #RS-REACH = ' ' AND #FCO-REACH = ' ' AND #FCM-REACH = ' ' AND #FRS-REACH = ' ' AND #VCO-REACH = ' ' AND #VCM-REACH = ' ' AND #MAX-REACH ) )
                && pnd_Vco_Reach.equals(" ") && pnd_Vcm_Reach.equals(" ") && pnd_Max_Reach.getBoolean()))))
            {
                                                                                                                                                                          //Natural: PERFORM POST-WRITE
                sub_Post_Write();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-REPORT
                sub_Process_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Loop.reset();                                                                                                                                         //Natural: RESET #LOOP PSTL6635-DATA-ARRAY ( * )
                pstl6635_Data_Pstl6635_Data_Array.getValue("*").reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Max_Reach.reset();                                                                                                                                            //Natural: RESET #MAX-REACH
        if (condition(pnd_Rs_Idx.greater(getZero())))                                                                                                                     //Natural: IF #RS-IDX > 0
        {
            pnd_Loop.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #LOOP
            //*  RESIDENTIAL ADDRESS
            pnd_Post_Loop.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #POST-LOOP
            pnd_Max_Reach.setValue(true);                                                                                                                                 //Natural: ASSIGN #MAX-REACH := TRUE
            pstl6635_Data_Addr_Chng_Type.getValue(pnd_Loop).setValue("G");                                                                                                //Natural: ASSIGN ADDR-CHNG-TYPE ( #LOOP ) := 'G'
            pstl6635_Data_Addr_Line.getValue(pnd_Loop,"*").setValue(pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue("*"));                                                     //Natural: ASSIGN ADDR-LINE ( #LOOP, * ) := #RS-NEW-MAIL-ADDR ( * )
            if (condition(((pnd_Loop.equals(8)) || (pnd_Fco_Reach.equals(" ") && pnd_Fcm_Reach.equals(" ") && pnd_Frs_Reach.equals(" ") && pnd_Vco_Reach.equals(" ")      //Natural: IF ( ( #LOOP = 8 ) OR ( #FCO-REACH = ' ' AND #FCM-REACH = ' ' AND #FRS-REACH = ' ' AND #VCO-REACH = ' ' AND #VCM-REACH = ' ' AND #MAX-REACH ) )
                && pnd_Vcm_Reach.equals(" ") && pnd_Max_Reach.getBoolean()))))
            {
                                                                                                                                                                          //Natural: PERFORM POST-WRITE
                sub_Post_Write();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-REPORT
                sub_Process_Report();
                if (condition(Global.isEscape())) {return;}
                pnd_Loop.reset();                                                                                                                                         //Natural: RESET #LOOP PSTL6635-DATA-ARRAY ( * )
                pstl6635_Data_Pstl6635_Data_Array.getValue("*").reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Max_Reach.reset();                                                                                                                                            //Natural: RESET #MAX-REACH
        FOR05:                                                                                                                                                            //Natural: FOR #X = 1 TO #FCO-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Fco_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            pnd_Fco_Post.setValue(true);                                                                                                                                  //Natural: ASSIGN #FCO-POST := TRUE
            FOR06:                                                                                                                                                        //Natural: FOR #Y = 1 TO 20
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(20)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pnd_Fco_Group_Pnd_Fco_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                               //Natural: IF #FCO-CONTRACT ( #X, #Y ) > ' '
                {
                    if (condition(pnd_Fco_Post.getBoolean()))                                                                                                             //Natural: IF #FCO-POST
                    {
                        pnd_Loop.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #LOOP
                        pnd_Post_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #POST-CNT
                        pnd_Post_Loop.nadd(1);                                                                                                                            //Natural: ADD 1 TO #POST-LOOP
                        pnd_Fco_Post.reset();                                                                                                                             //Natural: RESET #FCO-POST
                        //*  FUTURE CORRESPONDENCE ADDRESS
                    }                                                                                                                                                     //Natural: END-IF
                    pstl6635_Data_Addr_Chng_Type.getValue(pnd_Loop).setValue("C");                                                                                        //Natural: ASSIGN ADDR-CHNG-TYPE ( #LOOP ) := 'C'
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,1).setValue(pnd_Fco_Group_Pnd_Fco_Mail_Addr1.getValue(pnd_Counters_Pnd_X));                                 //Natural: ASSIGN ADDR-LINE ( #LOOP, 1 ) := #FCO-MAIL-ADDR1 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,2).setValue(pnd_Fco_Group_Pnd_Fco_Mail_Addr2.getValue(pnd_Counters_Pnd_X));                                 //Natural: ASSIGN ADDR-LINE ( #LOOP, 2 ) := #FCO-MAIL-ADDR2 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,3).setValue(pnd_Fco_Group_Pnd_Fco_Mail_Addr3.getValue(pnd_Counters_Pnd_X));                                 //Natural: ASSIGN ADDR-LINE ( #LOOP, 3 ) := #FCO-MAIL-ADDR3 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,4).setValue(pnd_Fco_Group_Pnd_Fco_Mail_Addr4.getValue(pnd_Counters_Pnd_X));                                 //Natural: ASSIGN ADDR-LINE ( #LOOP, 4 ) := #FCO-MAIL-ADDR4 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,5).setValue(pnd_Fco_Group_Pnd_Fco_Mail_Addr5.getValue(pnd_Counters_Pnd_X));                                 //Natural: ASSIGN ADDR-LINE ( #LOOP, 5 ) := #FCO-MAIL-ADDR5 ( #X )
                    pstl6635_Data_Addr_Cntrct.getValue(pnd_Loop,pnd_Counters_Pnd_Y).setValue(pnd_Fco_Group_Pnd_Fco_Contract.getValue(pnd_Counters_Pnd_X,                  //Natural: ASSIGN ADDR-CNTRCT ( #LOOP, #Y ) := #FCO-CONTRACT ( #X, #Y )
                        pnd_Counters_Pnd_Y));
                    pstl6635_Data_Itd_Contract_Out.getValue(pnd_Loop,pnd_Counters_Pnd_Y).setValue(pnd_Fco_Group_Pnd_Fco_Itd_Contract.getValue(pnd_Counters_Pnd_X,         //Natural: ASSIGN ITD-CONTRACT-OUT ( #LOOP, #Y ) := #FCO-ITD-CONTRACT ( #X, #Y )
                        pnd_Counters_Pnd_Y));
                    pstl6635_Data_Bgn_Effct_Dte.getValue(pnd_Loop).setValue(pnd_Fco_Group_Pnd_Fco_Begin_Dte.getValue(pnd_Counters_Pnd_X));                                //Natural: ASSIGN BGN-EFFCT-DTE ( #LOOP ) := #FCO-BEGIN-DTE ( #X )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Counters_Pnd_X.equals(pnd_Fco_Idx)))                                                                                                        //Natural: IF #X = #FCO-IDX
            {
                pnd_Max_Reach.setValue(true);                                                                                                                             //Natural: ASSIGN #MAX-REACH := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pnd_Loop.equals(8)) || (pnd_Fcm_Reach.equals(" ") && pnd_Frs_Reach.equals(" ") && pnd_Vco_Reach.equals(" ") && pnd_Vcm_Reach.equals(" ")      //Natural: IF ( ( #LOOP = 8 ) OR ( #FCM-REACH = ' ' AND #FRS-REACH = ' ' AND #VCO-REACH = ' ' AND #VCM-REACH = ' ' AND #MAX-REACH ) )
                && pnd_Max_Reach.getBoolean()))))
            {
                                                                                                                                                                          //Natural: PERFORM POST-WRITE
                sub_Post_Write();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-REPORT
                sub_Process_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Loop.reset();                                                                                                                                         //Natural: RESET #LOOP PSTL6635-DATA-ARRAY ( * )
                pstl6635_Data_Pstl6635_Data_Array.getValue("*").reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Max_Reach.reset();                                                                                                                                            //Natural: RESET #MAX-REACH
        FOR07:                                                                                                                                                            //Natural: FOR #X = 1 TO #FCM-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Fcm_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            pnd_Fcm_Post.setValue(true);                                                                                                                                  //Natural: ASSIGN #FCM-POST := TRUE
            FOR08:                                                                                                                                                        //Natural: FOR #Y = 1 TO 20
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(20)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pnd_Fcm_Group_Pnd_Fcm_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                               //Natural: IF #FCM-CONTRACT ( #X, #Y ) > ' '
                {
                    if (condition(pnd_Fcm_Post.getBoolean()))                                                                                                             //Natural: IF #FCM-POST
                    {
                        pnd_Loop.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #LOOP
                        pnd_Post_Loop.nadd(1);                                                                                                                            //Natural: ADD 1 TO #POST-LOOP
                        pnd_Fcm_Post.reset();                                                                                                                             //Natural: RESET #FCM-POST
                        //*  FUTURE CHECK-MAILING ADDRESS
                    }                                                                                                                                                     //Natural: END-IF
                    pstl6635_Data_Addr_Chng_Type.getValue(pnd_Loop).setValue("D");                                                                                        //Natural: ASSIGN ADDR-CHNG-TYPE ( #LOOP ) := 'D'
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,1).setValue(pnd_Fcm_Group_Pnd_Fcm_Pay_Addr1.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN ADDR-LINE ( #LOOP, 1 ) := #FCM-PAY-ADDR1 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,2).setValue(pnd_Fcm_Group_Pnd_Fcm_Pay_Addr2.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN ADDR-LINE ( #LOOP, 2 ) := #FCM-PAY-ADDR2 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,3).setValue(pnd_Fcm_Group_Pnd_Fcm_Pay_Addr3.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN ADDR-LINE ( #LOOP, 3 ) := #FCM-PAY-ADDR3 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,4).setValue(pnd_Fcm_Group_Pnd_Fcm_Pay_Addr4.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN ADDR-LINE ( #LOOP, 4 ) := #FCM-PAY-ADDR4 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,5).setValue(pnd_Fcm_Group_Pnd_Fcm_Pay_Addr5.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN ADDR-LINE ( #LOOP, 5 ) := #FCM-PAY-ADDR5 ( #X )
                    pstl6635_Data_Addr_Cntrct.getValue(pnd_Loop,pnd_Counters_Pnd_Y).setValue(pnd_Fcm_Group_Pnd_Fcm_Contract.getValue(pnd_Counters_Pnd_X,                  //Natural: ASSIGN ADDR-CNTRCT ( #LOOP, #Y ) := #FCM-CONTRACT ( #X, #Y )
                        pnd_Counters_Pnd_Y));
                    pstl6635_Data_Itd_Contract_Out.getValue(pnd_Loop,pnd_Counters_Pnd_Y).setValue(pnd_Fcm_Group_Pnd_Fcm_Itd_Contract.getValue(pnd_Counters_Pnd_X,         //Natural: ASSIGN ITD-CONTRACT-OUT ( #LOOP, #Y ) := #FCM-ITD-CONTRACT ( #X, #Y )
                        pnd_Counters_Pnd_Y));
                    pstl6635_Data_Bgn_Effct_Dte.getValue(pnd_Loop).setValue(pnd_Fcm_Group_Pnd_Fcm_Begin_Dte.getValue(pnd_Counters_Pnd_X));                                //Natural: ASSIGN BGN-EFFCT-DTE ( #LOOP ) := #FCM-BEGIN-DTE ( #X )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Counters_Pnd_X.equals(pnd_Fcm_Idx)))                                                                                                        //Natural: IF #X = #FCM-IDX
            {
                pnd_Max_Reach.setValue(true);                                                                                                                             //Natural: ASSIGN #MAX-REACH := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pnd_Loop.equals(8)) || (pnd_Frs_Reach.equals(" ") && pnd_Vco_Reach.equals(" ") && pnd_Vcm_Reach.equals(" ") && pnd_Max_Reach.getBoolean())))) //Natural: IF ( ( #LOOP = 8 ) OR ( #FRS-REACH = ' ' AND #VCO-REACH = ' ' AND #VCM-REACH = ' ' AND #MAX-REACH ) )
            {
                                                                                                                                                                          //Natural: PERFORM POST-WRITE
                sub_Post_Write();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-REPORT
                sub_Process_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Loop.reset();                                                                                                                                         //Natural: RESET #LOOP PSTL6635-DATA-ARRAY ( * )
                pstl6635_Data_Pstl6635_Data_Array.getValue("*").reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Max_Reach.reset();                                                                                                                                            //Natural: RESET #MAX-REACH
        FOR09:                                                                                                                                                            //Natural: FOR #X = 1 TO #FRS-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Frs_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            pnd_Loop.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #LOOP
            pnd_Post_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #POST-CNT
            //*  FUTURE RESIDENTIAL ADDRESS
            pnd_Post_Loop.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #POST-LOOP
            pstl6635_Data_Addr_Chng_Type.getValue(pnd_Loop).setValue("H");                                                                                                //Natural: ASSIGN ADDR-CHNG-TYPE ( #LOOP ) := 'H'
            pstl6635_Data_Addr_Line.getValue(pnd_Loop,"*").setValue(pnd_Frs_Group_Pnd_Frs_Mail_Addr.getValue("*"));                                                       //Natural: ASSIGN ADDR-LINE ( #LOOP, * ) := #FRS-MAIL-ADDR ( * )
            pstl6635_Data_Bgn_Effct_Dte.getValue(pnd_Loop).setValue(pnd_Frs_Group_Pnd_Frs_Begin_Dte);                                                                     //Natural: ASSIGN BGN-EFFCT-DTE ( #LOOP ) := #FRS-BEGIN-DTE
            pnd_Max_Reach.setValue(true);                                                                                                                                 //Natural: ASSIGN #MAX-REACH := TRUE
            if (condition(((pnd_Loop.equals(8)) || (pnd_Vco_Reach.equals(" ") && pnd_Vcm_Reach.equals(" ") && pnd_Max_Reach.getBoolean()))))                              //Natural: IF ( ( #LOOP = 8 ) OR ( #VCO-REACH = ' ' AND #VCM-REACH = ' ' AND #MAX-REACH ) )
            {
                                                                                                                                                                          //Natural: PERFORM POST-WRITE
                sub_Post_Write();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-REPORT
                sub_Process_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Loop.reset();                                                                                                                                         //Natural: RESET #LOOP PSTL6635-DATA-ARRAY ( * )
                pstl6635_Data_Pstl6635_Data_Array.getValue("*").reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Max_Reach.reset();                                                                                                                                            //Natural: RESET #MAX-REACH
        FOR10:                                                                                                                                                            //Natural: FOR #X = 1 TO #VCO-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Vco_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            pnd_Vco_Post.setValue(true);                                                                                                                                  //Natural: ASSIGN #VCO-POST := TRUE
            FOR11:                                                                                                                                                        //Natural: FOR #Y = 1 TO 20
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(20)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pnd_Vco_Group_Pnd_Vco_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                               //Natural: IF #VCO-CONTRACT ( #X, #Y ) > ' '
                {
                    if (condition(pnd_Vco_Post.getBoolean()))                                                                                                             //Natural: IF #VCO-POST
                    {
                        pnd_Loop.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #LOOP
                        pnd_Post_Loop.nadd(1);                                                                                                                            //Natural: ADD 1 TO #POST-LOOP
                        pnd_Vco_Post.reset();                                                                                                                             //Natural: RESET #VCO-POST
                        //*  NEW TEMPORARY MAILING ADDR
                    }                                                                                                                                                     //Natural: END-IF
                    pstl6635_Data_Addr_Chng_Type.getValue(pnd_Loop).setValue("E");                                                                                        //Natural: ASSIGN ADDR-CHNG-TYPE ( #LOOP ) := 'E'
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,1).setValue(pnd_Vco_Group_Pnd_Vco_Mail_Addr1.getValue(pnd_Counters_Pnd_X));                                 //Natural: ASSIGN ADDR-LINE ( #LOOP, 1 ) := #VCO-MAIL-ADDR1 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,2).setValue(pnd_Vco_Group_Pnd_Vco_Mail_Addr2.getValue(pnd_Counters_Pnd_X));                                 //Natural: ASSIGN ADDR-LINE ( #LOOP, 2 ) := #VCO-MAIL-ADDR2 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,3).setValue(pnd_Vco_Group_Pnd_Vco_Mail_Addr3.getValue(pnd_Counters_Pnd_X));                                 //Natural: ASSIGN ADDR-LINE ( #LOOP, 3 ) := #VCO-MAIL-ADDR3 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,4).setValue(pnd_Vco_Group_Pnd_Vco_Mail_Addr4.getValue(pnd_Counters_Pnd_X));                                 //Natural: ASSIGN ADDR-LINE ( #LOOP, 4 ) := #VCO-MAIL-ADDR4 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,5).setValue(pnd_Vco_Group_Pnd_Vco_Mail_Addr5.getValue(pnd_Counters_Pnd_X));                                 //Natural: ASSIGN ADDR-LINE ( #LOOP, 5 ) := #VCO-MAIL-ADDR5 ( #X )
                    pstl6635_Data_Addr_Cntrct.getValue(pnd_Loop,pnd_Counters_Pnd_Y).setValue(pnd_Vco_Group_Pnd_Vco_Contract.getValue(pnd_Counters_Pnd_X,                  //Natural: ASSIGN ADDR-CNTRCT ( #LOOP, #Y ) := #VCO-CONTRACT ( #X, #Y )
                        pnd_Counters_Pnd_Y));
                    pstl6635_Data_Itd_Contract_Out.getValue(pnd_Loop,pnd_Counters_Pnd_Y).setValue(pnd_Vco_Group_Pnd_Vco_Itd_Contract.getValue(pnd_Counters_Pnd_X,         //Natural: ASSIGN ITD-CONTRACT-OUT ( #LOOP, #Y ) := #VCO-ITD-CONTRACT ( #X, #Y )
                        pnd_Counters_Pnd_Y));
                    pstl6635_Data_Bgn_Effct_Dte.getValue(pnd_Loop).setValue(pnd_Vco_Group_Pnd_Vco_Begin_Dte.getValue(pnd_Counters_Pnd_X));                                //Natural: ASSIGN BGN-EFFCT-DTE ( #LOOP ) := #VCO-BEGIN-DTE ( #X )
                    pstl6635_Data_End_Effct_Dte.getValue(pnd_Loop).setValue(pnd_Vco_Group_Pnd_Vco_End_Dte.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN END-EFFCT-DTE ( #LOOP ) := #VCO-END-DTE ( #X )
                    pstl6635_Data_Tmp_Annual_Addr_Ind.getValue(pnd_Loop).setValue(pnd_Vco_Group_Pnd_Vco_Annual_Cycle_Ind.getValue(pnd_Counters_Pnd_X));                   //Natural: ASSIGN TMP-ANNUAL-ADDR-IND ( #LOOP ) := #VCO-ANNUAL-CYCLE-IND ( #X )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Counters_Pnd_X.equals(pnd_Vco_Idx)))                                                                                                        //Natural: IF #X = #VCO-IDX
            {
                pnd_Max_Reach.setValue(true);                                                                                                                             //Natural: ASSIGN #MAX-REACH := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pnd_Loop.equals(8)) || (pnd_Vcm_Reach.equals(" ") && pnd_Max_Reach.getBoolean()))))                                                           //Natural: IF ( ( #LOOP = 8 ) OR ( #VCM-REACH = ' ' AND #MAX-REACH ) )
            {
                                                                                                                                                                          //Natural: PERFORM POST-WRITE
                sub_Post_Write();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-REPORT
                sub_Process_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Loop.reset();                                                                                                                                         //Natural: RESET #LOOP PSTL6635-DATA-ARRAY ( * )
                pstl6635_Data_Pstl6635_Data_Array.getValue("*").reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Max_Reach.reset();                                                                                                                                            //Natural: RESET #MAX-REACH
        FOR12:                                                                                                                                                            //Natural: FOR #X = 1 TO #VCM-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Vcm_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            pnd_Vcm_Post.setValue(true);                                                                                                                                  //Natural: ASSIGN #VCM-POST := TRUE
            FOR13:                                                                                                                                                        //Natural: FOR #Y = 1 TO 20
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(20)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pnd_Vcm_Group_Pnd_Vcm_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                               //Natural: IF #VCM-CONTRACT ( #X, #Y ) > ' '
                {
                    if (condition(pnd_Vcm_Post.getBoolean()))                                                                                                             //Natural: IF #VCM-POST
                    {
                        pnd_Loop.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #LOOP
                        pnd_Post_Loop.nadd(1);                                                                                                                            //Natural: ADD 1 TO #POST-LOOP
                        pnd_Vcm_Post.reset();                                                                                                                             //Natural: RESET #VCM-POST
                        //*  TEMPORARY CHECK-MAILING ADDR
                    }                                                                                                                                                     //Natural: END-IF
                    pstl6635_Data_Addr_Chng_Type.getValue(pnd_Loop).setValue("F");                                                                                        //Natural: ASSIGN ADDR-CHNG-TYPE ( #LOOP ) := 'F'
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,1).setValue(pnd_Vcm_Group_Pnd_Vcm_Pay_Addr1.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN ADDR-LINE ( #LOOP, 1 ) := #VCM-PAY-ADDR1 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,2).setValue(pnd_Vcm_Group_Pnd_Vcm_Pay_Addr2.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN ADDR-LINE ( #LOOP, 2 ) := #VCM-PAY-ADDR2 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,3).setValue(pnd_Vcm_Group_Pnd_Vcm_Pay_Addr3.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN ADDR-LINE ( #LOOP, 3 ) := #VCM-PAY-ADDR3 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,4).setValue(pnd_Vcm_Group_Pnd_Vcm_Pay_Addr4.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN ADDR-LINE ( #LOOP, 4 ) := #VCM-PAY-ADDR4 ( #X )
                    pstl6635_Data_Addr_Line.getValue(pnd_Loop,5).setValue(pnd_Vcm_Group_Pnd_Vcm_Pay_Addr5.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN ADDR-LINE ( #LOOP, 5 ) := #VCM-PAY-ADDR5 ( #X )
                    pstl6635_Data_Addr_Cntrct.getValue(pnd_Loop,pnd_Counters_Pnd_Y).setValue(pnd_Vcm_Group_Pnd_Vcm_Contract.getValue(pnd_Counters_Pnd_X,                  //Natural: ASSIGN ADDR-CNTRCT ( #LOOP, #Y ) := #VCM-CONTRACT ( #X, #Y )
                        pnd_Counters_Pnd_Y));
                    pstl6635_Data_Itd_Contract_Out.getValue(pnd_Loop,pnd_Counters_Pnd_Y).setValue(pnd_Vcm_Group_Pnd_Vcm_Itd_Contract.getValue(pnd_Counters_Pnd_X,         //Natural: ASSIGN ITD-CONTRACT-OUT ( #LOOP, #Y ) := #VCM-ITD-CONTRACT ( #X, #Y )
                        pnd_Counters_Pnd_Y));
                    pstl6635_Data_Bgn_Effct_Dte.getValue(pnd_Loop).setValue(pnd_Vcm_Group_Pnd_Vcm_Begin_Dte.getValue(pnd_Counters_Pnd_X));                                //Natural: ASSIGN BGN-EFFCT-DTE ( #LOOP ) := #VCM-BEGIN-DTE ( #X )
                    pstl6635_Data_End_Effct_Dte.getValue(pnd_Loop).setValue(pnd_Vcm_Group_Pnd_Vcm_End_Dte.getValue(pnd_Counters_Pnd_X));                                  //Natural: ASSIGN END-EFFCT-DTE ( #LOOP ) := #VCM-END-DTE ( #X )
                    pstl6635_Data_Tmp_Annual_Addr_Ind.getValue(pnd_Loop).setValue(pnd_Vcm_Group_Pnd_Vcm_Annual_Cycle_Ind.getValue(pnd_Counters_Pnd_X));                   //Natural: ASSIGN TMP-ANNUAL-ADDR-IND ( #LOOP ) := #VCM-ANNUAL-CYCLE-IND ( #X )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Counters_Pnd_X.equals(pnd_Vcm_Idx)))                                                                                                        //Natural: IF #X = #VCM-IDX
            {
                pnd_Max_Reach.setValue(true);                                                                                                                             //Natural: ASSIGN #MAX-REACH := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pnd_Loop.equals(8)) || pnd_Max_Reach.getBoolean())))                                                                                          //Natural: IF ( ( #LOOP = 8 ) OR #MAX-REACH )
            {
                                                                                                                                                                          //Natural: PERFORM POST-WRITE
                sub_Post_Write();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-REPORT
                sub_Process_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Loop.reset();                                                                                                                                         //Natural: RESET #LOOP PSTL6635-DATA-ARRAY ( * )
                pstl6635_Data_Pstl6635_Data_Array.getValue("*").reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MOVE-ADDRESS-TO-POST
    }
    private void sub_Move_Cm() throws Exception                                                                                                                           //Natural: MOVE-CM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cm_Idx.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #CM-IDX
        pnd_Cm_Cnt.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #CM-CNT
        if (condition(pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd.equals("C") || pnd_Work_Letter_Image_File_Pnd_O_C_Addr_Format_Cd.equals("D")))                    //Natural: IF #O-C-ADDR-FORMAT-CD = 'C' OR = 'D'
        {
            pnd_Cm_Group_Pnd_Cm_Pay_Bank_Info_Ind.getValue(pnd_Cm_Idx).setValue("Y");                                                                                     //Natural: ASSIGN #CM-PAY-BANK-INFO-IND ( #CM-IDX ) := 'Y'
            //*  KCD
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cm_Reach.setValue("Y");                                                                                                                                       //Natural: ASSIGN #CM-REACH := 'Y'
        pnd_Cm_Group_Pnd_Cm_Contract.getValue(pnd_Cm_Idx,pnd_Cm_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                                              //Natural: ASSIGN #CM-CONTRACT ( #CM-IDX, #CM-CNT ) := #O-CONTRACT-NO
        pnd_Cm_Group_Pnd_Cm_Itd_Contract.getValue(pnd_Cm_Idx,pnd_Cm_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                                            //Natural: ASSIGN #CM-ITD-CONTRACT ( #CM-IDX, #CM-CNT ) := #O-CONTRACT2
        pnd_Cm_Group_Pnd_Cm_Name.getValue(pnd_Cm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Name);                                                                  //Natural: ASSIGN #CM-NAME ( #CM-IDX ) := #O-C-NAME
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr1.getValue(pnd_Cm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_1);                                                   //Natural: ASSIGN #CM-PAY-MAIL-ADDR1 ( #CM-IDX ) := #O-C-ADDRESS-1
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr2.getValue(pnd_Cm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_2);                                                   //Natural: ASSIGN #CM-PAY-MAIL-ADDR2 ( #CM-IDX ) := #O-C-ADDRESS-2
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr3.getValue(pnd_Cm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_3);                                                   //Natural: ASSIGN #CM-PAY-MAIL-ADDR3 ( #CM-IDX ) := #O-C-ADDRESS-3
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr4.getValue(pnd_Cm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_4);                                                   //Natural: ASSIGN #CM-PAY-MAIL-ADDR4 ( #CM-IDX ) := #O-C-ADDRESS-4
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr5.getValue(pnd_Cm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_5);                                                   //Natural: ASSIGN #CM-PAY-MAIL-ADDR5 ( #CM-IDX ) := #O-C-ADDRESS-5
        pnd_Cm_Group_Pnd_Cm_Pay_Mail_Zip.getValue(pnd_Cm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data);                                                   //Natural: ASSIGN #CM-PAY-MAIL-ZIP ( #CM-IDX ) := #O-C-POSTAL-DATA
        pnd_Cm_Group_Pnd_Cm_Address_Compare.getValue(pnd_Cm_Idx).setValue(pnd_Address_Compare);                                                                           //Natural: ASSIGN #CM-ADDRESS-COMPARE ( #CM-IDX ) := #ADDRESS-COMPARE
        pnd_Cm_Group_Pnd_Cm_Pay_Cs_Ind.getValue(pnd_Cm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Check_Saving_Cd);                                                 //Natural: ASSIGN #CM-PAY-CS-IND ( #CM-IDX ) := #O-C-CHECK-SAVING-CD
        pnd_Cm_Group_Pnd_Cm_Pay_Bnk_Acct_Nbr.getValue(pnd_Cm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No);                                                //Natural: ASSIGN #CM-PAY-BNK-ACCT-NBR ( #CM-IDX ) := #BANK-ACCT ( #CM-IDX ) := #O-C-PH-BANK-NO
        pnd_Bank_Pnd_Bank_Acct.getValue(pnd_Cm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Ph_Bank_No);
        pnd_Cm_Group_Pnd_Cm_Pay_Eft_Nbr.getValue(pnd_Cm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No);                                                 //Natural: ASSIGN #CM-PAY-EFT-NBR ( #CM-IDX ) := #EFT-ACCT ( #CM-IDX ) := #O-C-ABA-ACCOUNT-NO
        pnd_Bank_Pnd_Eft_Acct.getValue(pnd_Cm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Aba_Account_No);
        pnd_Counters_Pnd_Z.reset();                                                                                                                                       //Natural: RESET #Z
        FOR14:                                                                                                                                                            //Natural: FOR #X = 1 TO 20
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(20)); pnd_Counters_Pnd_X.nadd(1))
        {
            FOR15:                                                                                                                                                        //Natural: FOR #Y = 35 TO 1 STEP -1
            for (pnd_Counters_Pnd_Y.setValue(35); condition(pnd_Counters_Pnd_Y.greaterOrEqual(1)); pnd_Counters_Pnd_Y.nsubtract(1))
            {
                if (condition(pnd_Bank_Pnd_Bank_Acct_A.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                                     //Natural: IF #BANK-ACCT-A ( #X, #Y ) > ' '
                {
                    pnd_Counters_Pnd_Z.nadd(1);                                                                                                                           //Natural: ADD 1 TO #Z
                    if (condition(pnd_Counters_Pnd_Z.greater(4)))                                                                                                         //Natural: IF #Z > 4
                    {
                        pnd_Bank_Pnd_Bank_Acct_A.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).setValue("X");                                                           //Natural: MOVE 'X' TO #BANK-ACCT-A ( #X, #Y )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Counters_Pnd_Z.reset();                                                                                                                                   //Natural: RESET #Z
            FOR16:                                                                                                                                                        //Natural: FOR #Y = 35 TO 1 STEP -1
            for (pnd_Counters_Pnd_Y.setValue(35); condition(pnd_Counters_Pnd_Y.greaterOrEqual(1)); pnd_Counters_Pnd_Y.nsubtract(1))
            {
                if (condition(pnd_Bank_Pnd_Eft_Acct_A.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                                      //Natural: IF #EFT-ACCT-A ( #X, #Y ) > ' '
                {
                    pnd_Counters_Pnd_Z.nadd(1);                                                                                                                           //Natural: ADD 1 TO #Z
                    if (condition(pnd_Counters_Pnd_Z.greater(4)))                                                                                                         //Natural: IF #Z > 4
                    {
                        pnd_Bank_Pnd_Eft_Acct_A.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).setValue("X");                                                            //Natural: MOVE 'X' TO #EFT-ACCT-A ( #X, #Y )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MOVE-CM
    }
    private void sub_Move_Co() throws Exception                                                                                                                           //Natural: MOVE-CO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Co_Idx.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #CO-IDX
        //*  KCD
        pnd_Co_Cnt.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #CO-CNT
        pnd_Co_Reach.setValue("Y");                                                                                                                                       //Natural: ASSIGN #CO-REACH := 'Y'
        pnd_Co_Group_Pnd_Co_Contract.getValue(pnd_Co_Idx,pnd_Co_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                                              //Natural: ASSIGN #CO-CONTRACT ( #CO-IDX, #CO-CNT ) := #O-CONTRACT-NO
        pnd_Co_Group_Pnd_Co_Itd_Contract.getValue(pnd_Co_Idx,pnd_Co_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                                            //Natural: ASSIGN #CO-ITD-CONTRACT ( #CO-IDX, #CO-CNT ) := #O-CONTRACT2
        pnd_Co_Group_Pnd_Co_Name.getValue(pnd_Co_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Name);                                                                  //Natural: ASSIGN #CO-NAME ( #CO-IDX ) := #O-C-NAME
        pnd_Co_Group_Pnd_Co_New_Mail_Addr1.getValue(pnd_Co_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_1);                                                   //Natural: ASSIGN #CO-NEW-MAIL-ADDR1 ( #CO-IDX ) := #O-C-ADDRESS-1
        pnd_Co_Group_Pnd_Co_New_Mail_Addr2.getValue(pnd_Co_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_2);                                                   //Natural: ASSIGN #CO-NEW-MAIL-ADDR2 ( #CO-IDX ) := #O-C-ADDRESS-2
        pnd_Co_Group_Pnd_Co_New_Mail_Addr3.getValue(pnd_Co_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_3);                                                   //Natural: ASSIGN #CO-NEW-MAIL-ADDR3 ( #CO-IDX ) := #O-C-ADDRESS-3
        pnd_Co_Group_Pnd_Co_New_Mail_Addr4.getValue(pnd_Co_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_4);                                                   //Natural: ASSIGN #CO-NEW-MAIL-ADDR4 ( #CO-IDX ) := #O-C-ADDRESS-4
        pnd_Co_Group_Pnd_Co_New_Mail_Addr5.getValue(pnd_Co_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_5);                                                   //Natural: ASSIGN #CO-NEW-MAIL-ADDR5 ( #CO-IDX ) := #O-C-ADDRESS-5
        pnd_Co_Group_Pnd_Co_New_Mail_Zip.getValue(pnd_Co_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data);                                                   //Natural: ASSIGN #CO-NEW-MAIL-ZIP ( #CO-IDX ) := #O-C-POSTAL-DATA
        pnd_Co_Group_Pnd_Co_Address_Compare.getValue(pnd_Co_Idx).setValue(pnd_Address_Compare);                                                                           //Natural: ASSIGN #CO-ADDRESS-COMPARE ( #CO-IDX ) := #ADDRESS-COMPARE
        if (condition(pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue(1).equals(" ")))                                                                                         //Natural: IF #RS-NEW-MAIL-ADDR ( 1 ) = ' '
        {
            pnd_Rs_Idx.setValue(1);                                                                                                                                       //Natural: ASSIGN #RS-IDX := #RS-CNT := 1
            pnd_Rs_Cnt.setValue(1);
            pnd_Rs_Reach.setValue("Y");                                                                                                                                   //Natural: ASSIGN #RS-REACH := 'Y'
            pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue(1).setValue(pnd_Work_Letter_Image_File_Pnd_O_R_Address_1);                                                         //Natural: ASSIGN #RS-NEW-MAIL-ADDR ( 1 ) := #O-R-ADDRESS-1
            pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue(2).setValue(pnd_Work_Letter_Image_File_Pnd_O_R_Address_2);                                                         //Natural: ASSIGN #RS-NEW-MAIL-ADDR ( 2 ) := #O-R-ADDRESS-2
            pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue(3).setValue(pnd_Work_Letter_Image_File_Pnd_O_R_Address_3);                                                         //Natural: ASSIGN #RS-NEW-MAIL-ADDR ( 3 ) := #O-R-ADDRESS-3
            pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue(4).setValue(pnd_Work_Letter_Image_File_Pnd_O_R_Address_4);                                                         //Natural: ASSIGN #RS-NEW-MAIL-ADDR ( 4 ) := #O-R-ADDRESS-4
            pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue(5).setValue(pnd_Work_Letter_Image_File_Pnd_O_R_Address_5);                                                         //Natural: ASSIGN #RS-NEW-MAIL-ADDR ( 5 ) := #O-R-ADDRESS-5
            pnd_Rs_Group_Pnd_Rs_New_Mail_Zip.setValue(pnd_Work_Letter_Image_File_Pnd_O_R_Postal_Data);                                                                    //Natural: ASSIGN #RS-NEW-MAIL-ZIP := #O-R-POSTAL-DATA
            //*  #RS-ADDRESS-COMPARE                  /* NO ADDRESS COMPARE MOVED HERE
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE-CO
    }
    private void sub_Move_Fcm() throws Exception                                                                                                                          //Natural: MOVE-FCM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Fcm_Idx.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #FCM-IDX
        //*  #O-C-PERM-ADDRESS-CH-DT
        //* KCD
        pnd_Fcm_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #FCM-CNT
        pnd_Post_Mmddyyyy_Pnd_P_Ccyy.setValue(pnd_Work_Letter_Image_File_Pnd_R_Ccyy);                                                                                     //Natural: ASSIGN #P-CCYY := #R-CCYY
        pnd_Post_Mmddyyyy_Pnd_P_Mm.setValue(pnd_Work_Letter_Image_File_Pnd_R_Mm);                                                                                         //Natural: ASSIGN #P-MM := #R-MM
        pnd_Post_Mmddyyyy_Pnd_P_Dd.setValue(pnd_Work_Letter_Image_File_Pnd_R_Dd);                                                                                         //Natural: ASSIGN #P-DD := #R-DD
        pnd_Fcm_Group_Pnd_Fcm_Contract.getValue(pnd_Fcm_Idx,pnd_Fcm_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                                          //Natural: ASSIGN #FCM-CONTRACT ( #FCM-IDX, #FCM-CNT ) := #O-CONTRACT-NO
        pnd_Fcm_Group_Pnd_Fcm_Itd_Contract.getValue(pnd_Fcm_Idx,pnd_Fcm_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                                        //Natural: ASSIGN #FCM-ITD-CONTRACT ( #FCM-IDX, #FCM-CNT ) := #O-CONTRACT2
        pnd_Fcm_Group_Pnd_Fcm_Address_Compare.getValue(pnd_Fcm_Idx).setValue(pnd_Address_Compare);                                                                        //Natural: ASSIGN #FCM-ADDRESS-COMPARE ( #FCM-IDX ) := #ADDRESS-COMPARE
        pnd_Fcm_Group_Pnd_Fcm_Name.getValue(pnd_Fcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Name);                                                               //Natural: ASSIGN #FCM-NAME ( #FCM-IDX ) := #O-C-NAME
        pnd_Fcm_Group_Pnd_Fcm_Pay_Addr1.getValue(pnd_Fcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_1);                                                     //Natural: ASSIGN #FCM-PAY-ADDR1 ( #FCM-IDX ) := #O-C-ADDRESS-1
        pnd_Fcm_Group_Pnd_Fcm_Pay_Addr2.getValue(pnd_Fcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_2);                                                     //Natural: ASSIGN #FCM-PAY-ADDR2 ( #FCM-IDX ) := #O-C-ADDRESS-2
        pnd_Fcm_Group_Pnd_Fcm_Pay_Addr3.getValue(pnd_Fcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_3);                                                     //Natural: ASSIGN #FCM-PAY-ADDR3 ( #FCM-IDX ) := #O-C-ADDRESS-3
        pnd_Fcm_Group_Pnd_Fcm_Pay_Addr4.getValue(pnd_Fcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_4);                                                     //Natural: ASSIGN #FCM-PAY-ADDR4 ( #FCM-IDX ) := #O-C-ADDRESS-4
        pnd_Fcm_Group_Pnd_Fcm_Pay_Addr5.getValue(pnd_Fcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_5);                                                     //Natural: ASSIGN #FCM-PAY-ADDR5 ( #FCM-IDX ) := #O-C-ADDRESS-5
        pnd_Fcm_Group_Pnd_Fcm_Pay_Zip.getValue(pnd_Fcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data);                                                     //Natural: ASSIGN #FCM-PAY-ZIP ( #FCM-IDX ) := #O-C-POSTAL-DATA
        pnd_Fcm_Group_Pnd_Fcm_Begin_Dte.getValue(pnd_Fcm_Idx).setValue(pnd_Post_Mmddyyyy);                                                                                //Natural: ASSIGN #FCM-BEGIN-DTE ( #FCM-IDX ) := #POST-MMDDYYYY
        pnd_Fcm_Reach.setValue("Y");                                                                                                                                      //Natural: ASSIGN #FCM-REACH := 'Y'
        //*  MOVE-FCM
    }
    private void sub_Move_Fco() throws Exception                                                                                                                          //Natural: MOVE-FCO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Fco_Idx.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #FCO-IDX
        //*  #O-C-PERM-ADDRESS-CH-DT
        //*  KCD
        pnd_Fco_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #FCO-CNT
        pnd_Post_Mmddyyyy_Pnd_P_Ccyy.setValue(pnd_Work_Letter_Image_File_Pnd_R_Ccyy);                                                                                     //Natural: ASSIGN #P-CCYY := #R-CCYY
        pnd_Post_Mmddyyyy_Pnd_P_Mm.setValue(pnd_Work_Letter_Image_File_Pnd_R_Mm);                                                                                         //Natural: ASSIGN #P-MM := #R-MM
        pnd_Post_Mmddyyyy_Pnd_P_Dd.setValue(pnd_Work_Letter_Image_File_Pnd_R_Dd);                                                                                         //Natural: ASSIGN #P-DD := #R-DD
        pnd_Fco_Reach.setValue("Y");                                                                                                                                      //Natural: ASSIGN #FCO-REACH := 'Y'
        pnd_Fco_Group_Pnd_Fco_Contract.getValue(pnd_Fco_Idx,pnd_Fco_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                                          //Natural: ASSIGN #FCO-CONTRACT ( #FCO-IDX, #FCO-CNT ) := #O-CONTRACT-NO
        pnd_Fco_Group_Pnd_Fco_Itd_Contract.getValue(pnd_Fco_Idx,pnd_Fco_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                                        //Natural: ASSIGN #FCO-ITD-CONTRACT ( #FCO-IDX, #FCO-CNT ) := #O-CONTRACT2
        pnd_Fco_Group_Pnd_Fco_Address_Compare.getValue(pnd_Fco_Idx).setValue(pnd_Address_Compare);                                                                        //Natural: ASSIGN #FCO-ADDRESS-COMPARE ( #FCO-IDX ) := #ADDRESS-COMPARE
        pnd_Fco_Group_Pnd_Fco_Name.getValue(pnd_Fco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Name);                                                               //Natural: ASSIGN #FCO-NAME ( #FCO-IDX ) := #O-C-NAME
        pnd_Fco_Group_Pnd_Fco_Mail_Addr1.getValue(pnd_Fco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_1);                                                    //Natural: ASSIGN #FCO-MAIL-ADDR1 ( #FCO-IDX ) := #O-C-ADDRESS-1
        pnd_Fco_Group_Pnd_Fco_Mail_Addr2.getValue(pnd_Fco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_2);                                                    //Natural: ASSIGN #FCO-MAIL-ADDR2 ( #FCO-IDX ) := #O-C-ADDRESS-2
        pnd_Fco_Group_Pnd_Fco_Mail_Addr3.getValue(pnd_Fco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_3);                                                    //Natural: ASSIGN #FCO-MAIL-ADDR3 ( #FCO-IDX ) := #O-C-ADDRESS-3
        pnd_Fco_Group_Pnd_Fco_Mail_Addr4.getValue(pnd_Fco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_4);                                                    //Natural: ASSIGN #FCO-MAIL-ADDR4 ( #FCO-IDX ) := #O-C-ADDRESS-4
        pnd_Fco_Group_Pnd_Fco_Mail_Addr5.getValue(pnd_Fco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_5);                                                    //Natural: ASSIGN #FCO-MAIL-ADDR5 ( #FCO-IDX ) := #O-C-ADDRESS-5
        pnd_Fco_Group_Pnd_Fco_Mail_Zip.getValue(pnd_Fco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data);                                                    //Natural: ASSIGN #FCO-MAIL-ZIP ( #FCO-IDX ) := #O-C-POSTAL-DATA
        pnd_Fco_Group_Pnd_Fco_Begin_Dte.getValue(pnd_Fco_Idx).setValue(pnd_Post_Mmddyyyy);                                                                                //Natural: ASSIGN #FCO-BEGIN-DTE ( #FCO-IDX ) := #POST-MMDDYYYY
        //*  MOVE-FCO
    }
    private void sub_Move_Vcm() throws Exception                                                                                                                          //Natural: MOVE-VCM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Vcm_Idx.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #VCM-IDX
        //* KCD
        pnd_Vcm_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #VCM-CNT
        pnd_Vcm_Reach.setValue("Y");                                                                                                                                      //Natural: ASSIGN #VCM-REACH := 'Y'
        pnd_Vcm_Group_Pnd_Vcm_Contract.getValue(pnd_Vcm_Idx,pnd_Vcm_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                                          //Natural: ASSIGN #VCM-CONTRACT ( #VCM-IDX, #VCM-CNT ) := #O-CONTRACT-NO
        pnd_Vcm_Group_Pnd_Vcm_Itd_Contract.getValue(pnd_Vcm_Idx,pnd_Vcm_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                                        //Natural: ASSIGN #VCM-ITD-CONTRACT ( #VCM-IDX, #VCM-CNT ) := #O-CONTRACT2
        pnd_Vcm_Group_Pnd_Vcm_Name.getValue(pnd_Vcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Name);                                                               //Natural: ASSIGN #VCM-NAME ( #VCM-IDX ) := #O-C-NAME
        pnd_Vcm_Group_Pnd_Vcm_Pay_Addr1.getValue(pnd_Vcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_1);                                                     //Natural: ASSIGN #VCM-PAY-ADDR1 ( #VCM-IDX ) := #O-C-ADDRESS-1
        pnd_Vcm_Group_Pnd_Vcm_Pay_Addr2.getValue(pnd_Vcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_2);                                                     //Natural: ASSIGN #VCM-PAY-ADDR2 ( #VCM-IDX ) := #O-C-ADDRESS-2
        pnd_Vcm_Group_Pnd_Vcm_Pay_Addr3.getValue(pnd_Vcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_3);                                                     //Natural: ASSIGN #VCM-PAY-ADDR3 ( #VCM-IDX ) := #O-C-ADDRESS-3
        pnd_Vcm_Group_Pnd_Vcm_Pay_Addr4.getValue(pnd_Vcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_4);                                                     //Natural: ASSIGN #VCM-PAY-ADDR4 ( #VCM-IDX ) := #O-C-ADDRESS-4
        pnd_Vcm_Group_Pnd_Vcm_Pay_Addr5.getValue(pnd_Vcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_5);                                                     //Natural: ASSIGN #VCM-PAY-ADDR5 ( #VCM-IDX ) := #O-C-ADDRESS-5
        pnd_Vcm_Group_Pnd_Vcm_Pay_Zip.getValue(pnd_Vcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data);                                                     //Natural: ASSIGN #VCM-PAY-ZIP ( #VCM-IDX ) := #O-C-POSTAL-DATA
        pnd_Vcm_Group_Pnd_Vcm_Address_Compare.getValue(pnd_Vcm_Idx).setValue(pnd_Address_Compare);                                                                        //Natural: ASSIGN #VCM-ADDRESS-COMPARE ( #VCM-IDX ) := #ADDRESS-COMPARE
        pnd_Vcm_Group_Pnd_Vcm_Annual_Cycle_Ind.getValue(pnd_Vcm_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind);                                   //Natural: ASSIGN #VCM-ANNUAL-CYCLE-IND ( #VCM-IDX ) := #O-C-ANNUAL-VAC-CYCLE-IND
        pnd_Post_Mmddyyyy_Pnd_P_Ccyy.setValue(pnd_Work_Letter_Image_File_Pnd_B_Ccyy);                                                                                     //Natural: ASSIGN #P-CCYY := #B-CCYY
        pnd_Post_Mmddyyyy_Pnd_P_Mm.setValue(pnd_Work_Letter_Image_File_Pnd_B_Mm);                                                                                         //Natural: ASSIGN #P-MM := #B-MM
        pnd_Post_Mmddyyyy_Pnd_P_Dd.setValue(pnd_Work_Letter_Image_File_Pnd_B_Dd);                                                                                         //Natural: ASSIGN #P-DD := #B-DD
        pnd_Vcm_Group_Pnd_Vcm_Begin_Dte.getValue(pnd_Vcm_Idx).setValue(pnd_Post_Mmddyyyy);                                                                                //Natural: ASSIGN #VCM-BEGIN-DTE ( #VCM-IDX ) := #POST-MMDDYYYY
        pnd_Post_Mmddyyyy_Pnd_P_Ccyy.setValue(pnd_Work_Letter_Image_File_Pnd_E_Ccyy);                                                                                     //Natural: ASSIGN #P-CCYY := #E-CCYY
        pnd_Post_Mmddyyyy_Pnd_P_Mm.setValue(pnd_Work_Letter_Image_File_Pnd_E_Mm);                                                                                         //Natural: ASSIGN #P-MM := #E-MM
        pnd_Post_Mmddyyyy_Pnd_P_Dd.setValue(pnd_Work_Letter_Image_File_Pnd_E_Dd);                                                                                         //Natural: ASSIGN #P-DD := #E-DD
        pnd_Vcm_Group_Pnd_Vcm_End_Dte.getValue(pnd_Vcm_Idx).setValue(pnd_Post_Mmddyyyy);                                                                                  //Natural: ASSIGN #VCM-END-DTE ( #VCM-IDX ) := #POST-MMDDYYYY
        //*  MOVE-VCM
    }
    private void sub_Move_Vco() throws Exception                                                                                                                          //Natural: MOVE-VCO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Vco_Idx.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #VCO-IDX
        //*  KCD
        //*  #O-C-PENDING-ADDR-CH-DT
        //*  #O-C-PENDING-ADDR-RS-DT
        pnd_Vco_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #VCO-CNT
        pnd_Vco_Reach.setValue("Y");                                                                                                                                      //Natural: ASSIGN #VCO-REACH := 'Y'
        pnd_Vco_Group_Pnd_Vco_Contract.getValue(pnd_Vco_Idx,pnd_Vco_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract_No);                                          //Natural: ASSIGN #VCO-CONTRACT ( #VCO-IDX, #VCO-CNT ) := #O-CONTRACT-NO
        pnd_Vco_Group_Pnd_Vco_Itd_Contract.getValue(pnd_Vco_Idx,pnd_Vco_Cnt).setValue(pnd_Work_Letter_Image_File_Pnd_O_Contract2);                                        //Natural: ASSIGN #VCO-ITD-CONTRACT ( #VCO-IDX, #VCO-CNT ) := #O-CONTRACT2
        pnd_Vco_Group_Pnd_Vco_Name.getValue(pnd_Vco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Name);                                                               //Natural: ASSIGN #VCO-NAME ( #VCO-IDX ) := #O-C-NAME
        pnd_Vco_Group_Pnd_Vco_Mail_Addr1.getValue(pnd_Vco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_1);                                                    //Natural: ASSIGN #VCO-MAIL-ADDR1 ( #VCO-IDX ) := #O-C-ADDRESS-1
        pnd_Vco_Group_Pnd_Vco_Mail_Addr2.getValue(pnd_Vco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_2);                                                    //Natural: ASSIGN #VCO-MAIL-ADDR2 ( #VCO-IDX ) := #O-C-ADDRESS-2
        pnd_Vco_Group_Pnd_Vco_Mail_Addr3.getValue(pnd_Vco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_3);                                                    //Natural: ASSIGN #VCO-MAIL-ADDR3 ( #VCO-IDX ) := #O-C-ADDRESS-3
        pnd_Vco_Group_Pnd_Vco_Mail_Addr4.getValue(pnd_Vco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_4);                                                    //Natural: ASSIGN #VCO-MAIL-ADDR4 ( #VCO-IDX ) := #O-C-ADDRESS-4
        pnd_Vco_Group_Pnd_Vco_Mail_Addr5.getValue(pnd_Vco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Address_5);                                                    //Natural: ASSIGN #VCO-MAIL-ADDR5 ( #VCO-IDX ) := #O-C-ADDRESS-5
        pnd_Vco_Group_Pnd_Vco_Mail_Zip.getValue(pnd_Vco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Postal_Data);                                                    //Natural: ASSIGN #VCO-MAIL-ZIP ( #VCO-IDX ) := #O-C-POSTAL-DATA
        pnd_Vco_Group_Pnd_Vco_Address_Compare.getValue(pnd_Vco_Idx).setValue(pnd_Address_Compare);                                                                        //Natural: ASSIGN #VCO-ADDRESS-COMPARE ( #VCO-IDX ) := #ADDRESS-COMPARE
        pnd_Vco_Group_Pnd_Vco_Annual_Cycle_Ind.getValue(pnd_Vco_Idx).setValue(pnd_Work_Letter_Image_File_Pnd_O_C_Annual_Vac_Cycle_Ind);                                   //Natural: ASSIGN #VCO-ANNUAL-CYCLE-IND ( #VCO-IDX ) := #O-C-ANNUAL-VAC-CYCLE-IND
        pnd_Post_Mmddyyyy_Pnd_P_Ccyy.setValue(pnd_Work_Letter_Image_File_Pnd_B_Ccyy);                                                                                     //Natural: ASSIGN #P-CCYY := #B-CCYY
        pnd_Post_Mmddyyyy_Pnd_P_Mm.setValue(pnd_Work_Letter_Image_File_Pnd_B_Mm);                                                                                         //Natural: ASSIGN #P-MM := #B-MM
        pnd_Post_Mmddyyyy_Pnd_P_Dd.setValue(pnd_Work_Letter_Image_File_Pnd_B_Dd);                                                                                         //Natural: ASSIGN #P-DD := #B-DD
        pnd_Vco_Group_Pnd_Vco_Begin_Dte.getValue(pnd_Vco_Idx).setValue(pnd_Post_Mmddyyyy);                                                                                //Natural: ASSIGN #VCO-BEGIN-DTE ( #VCO-IDX ) := #POST-MMDDYYYY
        pnd_Post_Mmddyyyy_Pnd_P_Ccyy.setValue(pnd_Work_Letter_Image_File_Pnd_E_Ccyy);                                                                                     //Natural: ASSIGN #P-CCYY := #E-CCYY
        pnd_Post_Mmddyyyy_Pnd_P_Mm.setValue(pnd_Work_Letter_Image_File_Pnd_E_Mm);                                                                                         //Natural: ASSIGN #P-MM := #E-MM
        pnd_Post_Mmddyyyy_Pnd_P_Dd.setValue(pnd_Work_Letter_Image_File_Pnd_E_Dd);                                                                                         //Natural: ASSIGN #P-DD := #E-DD
        pnd_Vco_Group_Pnd_Vco_End_Dte.getValue(pnd_Vco_Idx).setValue(pnd_Post_Mmddyyyy);                                                                                  //Natural: ASSIGN #VCO-END-DTE ( #VCO-IDX ) := #POST-MMDDYYYY
        //*  MOVE-VCO
    }
    private void sub_Post_Close() throws Exception                                                                                                                        //Natural: POST-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM POST-ERROR
            sub_Post_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pdaPsta9612.getPsta9612().reset();                                                                                                                                //Natural: RESET PSTA9612 PSTA9610
        pdaPsta9610.getPsta9610().reset();
        //*  POST-CLOSE
    }
    private void sub_Post_Error() throws Exception                                                                                                                        //Natural: POST-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
            DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                    //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
            if (condition(Global.isEscape())) return;
            pnd_Error_Temp1.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                          //Natural: ASSIGN #ERROR-TEMP1 := MSG-INFO-SUB.##MSG
            pnd_Error_Temp2.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().getSubstring(41,39));                                                                      //Natural: ASSIGN #ERROR-TEMP2 := SUBSTRING ( MSG-INFO-SUB.##MSG,41,39 )
            pnd_Post_Error.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #POST-ERROR
            getReports().write(1, ReportOption.NOTITLE,"****** ERROR MESSAGE ******",NEWLINE,"=",pnd_Mailto_Pin,NEWLINE,"=",pdaPsta9610.getPsta9610_Pst_Rqst_Id(),        //Natural: WRITE ( 1 ) '****** ERROR MESSAGE ******' / '=' #MAILTO-PIN / '=' PSTA9610.PST-RQST-ID / '=' MSG-INFO-SUB.##RETURN-CODE / '=' MSG-INFO-SUB.##ERROR-FIELD / '=' #ERROR-TEMP1 / '=' #ERROR-TEMP2
                NEWLINE,"=",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code(),NEWLINE,"=",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field(),NEWLINE,"=",
                pnd_Error_Temp1,NEWLINE,"=",pnd_Error_Temp2);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pdaPsta9610.getPsta9610_Pst_Backout_Ind().setValue(true);                                                                                                         //Natural: ASSIGN PSTA9610.PST-BACKOUT-IND := TRUE
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().equals(" ")))                                                                                             //Natural: IF MSG-INFO-SUB.##MSG = ' '
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  POST-ERROR
    }
    private void sub_Post_Open() throws Exception                                                                                                                         //Natural: POST-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        FOR17:                                                                                                                                                            //Natural: FOR #IDX = 1 TO 10
        for (pnd_Idx.setValue(1); condition(pnd_Idx.lessOrEqual(10)); pnd_Idx.nadd(1))
        {
            if (condition(pnd_Mit_Array_Pnd_A_Mit_Log_Date_Time.getValue(pnd_Idx).notEquals(" ")))                                                                        //Natural: IF #A-MIT-LOG-DATE-TIME ( #IDX ) NE ' '
            {
                pdaPsta9612.getPsta9612_Work_Prcss_Id().getValue(pnd_Idx).setValue(pnd_Mit_Array_Pnd_A_Mit_Wpid.getValue(pnd_Idx));                                       //Natural: ASSIGN PSTA9612.WORK-PRCSS-ID ( #IDX ) := #A-MIT-WPID ( #IDX )
                pdaPsta9612.getPsta9612_Rqst_Log_Dte_Tme().getValue(pnd_Idx).setValue(pnd_Mit_Array_Pnd_A_Mit_Log_Date_Time.getValue(pnd_Idx));                           //Natural: ASSIGN PSTA9612.RQST-LOG-DTE-TME ( #IDX ) := #A-MIT-LOG-DATE-TIME ( #IDX )
                pdaPsta9612.getPsta9612_Status_Cde().getValue(pnd_Idx).setValue(pnd_Mit_Array_Pnd_A_Mit_Status_Cde.getValue(pnd_Idx));                                    //Natural: ASSIGN PSTA9612.STATUS-CDE ( #IDX ) := #A-MIT-STATUS-CDE ( #IDX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("BATCHNAS");                                                                                                      //Natural: ASSIGN PSTA9612.SYSTM-ID-CDE := 'BATCHNAS'
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PTNMADD");                                                                                                          //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PTNMADD'
        //*  PSTA9612.PIN-NBR        := #MAILTO-PIN             /* PINE
        //*   "
        //*  PARTICIPANT
        pdaPsta9612.getPsta9612_Univ_Id().setValue(DbsUtil.compress(pnd_Mailto_Pin, " "));                                                                                //Natural: COMPRESS #MAILTO-PIN ' ' INTO PSTA9612.UNIV-ID
        pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                              //Natural: ASSIGN PSTA9612.UNIV-ID-TYP := 'P'
        pdaPsta9612.getPsta9612_Full_Nme().setValue(pnd_Mailto_M_Name);                                                                                                   //Natural: ASSIGN PSTA9612.FULL-NME := #MAILTO-M-NAME
        pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue(pnd_Mailto_M_Destination);                                                                                      //Natural: ASSIGN PSTA9612.ADDRSS-TYP-CDE := #MAILTO-M-DESTINATION
        pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(pnd_Mailto_M_Address_1);                                                                                         //Natural: ASSIGN PSTA9612.ADDRSS-LINE-1 := #MAILTO-M-ADDRESS-1
        pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(pnd_Mailto_M_Address_2);                                                                                         //Natural: ASSIGN PSTA9612.ADDRSS-LINE-2 := #MAILTO-M-ADDRESS-2
        pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(pnd_Mailto_M_Address_3);                                                                                         //Natural: ASSIGN PSTA9612.ADDRSS-LINE-3 := #MAILTO-M-ADDRESS-3
        pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(pnd_Mailto_M_Address_4);                                                                                         //Natural: ASSIGN PSTA9612.ADDRSS-LINE-4 := #MAILTO-M-ADDRESS-4
        pdaPsta9612.getPsta9612_Addrss_Line_5().setValue(pnd_Mailto_M_Address_5);                                                                                         //Natural: ASSIGN PSTA9612.ADDRSS-LINE-5 := #MAILTO-M-ADDRESS-5
        pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr().setValue(pnd_Mailto_M_Postal_Data);                                                                                      //Natural: ASSIGN PSTA9612.ADDRSS-ZP9-NBR := #MAILTO-M-POSTAL-DATA
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM POST-ERROR
            sub_Post_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  POST-OPEN
    }
    private void sub_Post_Write() throws Exception                                                                                                                        //Natural: POST-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, ReportOption.NOTITLE,"SUBROUTINE POST-WRITE");                                                                                          //Natural: WRITE 'SUBROUTINE POST-WRITE'
            if (Global.isEscape()) return;
            FOR18:                                                                                                                                                        //Natural: FOR #J = 1 TO 8
            for (pnd_Counters_Pnd_J.setValue(1); condition(pnd_Counters_Pnd_J.lessOrEqual(8)); pnd_Counters_Pnd_J.nadd(1))
            {
                if (condition(pstl6635_Data_Addr_Chng_Type.getValue(pnd_Counters_Pnd_J).notEquals(" ")))                                                                  //Natural: IF ADDR-CHNG-TYPE ( #J ) NE ' '
                {
                    getReports().write(0, ReportOption.NOTITLE,"X",pnd_Counters_Pnd_J,NEWLINE,"Typ ",pstl6635_Data_Addr_Chng_Type.getValue(pnd_Counters_Pnd_J),           //Natural: WRITE NOTITLE 'X' #J / 'Typ ' ADDR-CHNG-TYPE ( #J ) / 'Addr' ADDR-LINE ( #J,1:3 ) / 'Cntr' ADDR-CNTRCT ( #J,1:10 ) / 'Bank' NEW-PAY-BANK-INFO-IND ( #J ) / 'Acct' NEW-PAY-BNK-ACCT-NBR ( #J ) / 'Cs  ' NEW-PAY-CS-IND ( #J ) / 'Eft ' NEW-PAY-EFT-NBR ( #J ) / 'From' BGN-EFFCT-DTE ( #J ) / 'End ' END-EFFCT-DTE ( #J ) / 'Vac ' TMP-ANNUAL-ADDR-IND ( #J )
                        NEWLINE,"Addr",pstl6635_Data_Addr_Line.getValue(pnd_Counters_Pnd_J,1,":",3),NEWLINE,"Cntr",pstl6635_Data_Addr_Cntrct.getValue(pnd_Counters_Pnd_J,
                        1,":",10),NEWLINE,"Bank",pstl6635_Data_New_Pay_Bank_Info_Ind.getValue(pnd_Counters_Pnd_J),NEWLINE,"Acct",pstl6635_Data_New_Pay_Bnk_Acct_Nbr.getValue(pnd_Counters_Pnd_J),
                        NEWLINE,"Cs  ",pstl6635_Data_New_Pay_Cs_Ind.getValue(pnd_Counters_Pnd_J),NEWLINE,"Eft ",pstl6635_Data_New_Pay_Eft_Nbr.getValue(pnd_Counters_Pnd_J),
                        NEWLINE,"From",pstl6635_Data_Bgn_Effct_Dte.getValue(pnd_Counters_Pnd_J),NEWLINE,"End ",pstl6635_Data_End_Effct_Dte.getValue(pnd_Counters_Pnd_J),
                        NEWLINE,"Vac ",pstl6635_Data_Tmp_Annual_Addr_Ind.getValue(pnd_Counters_Pnd_J));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Close.setValue(true);                                                                                                                                         //Natural: ASSIGN #CLOSE := TRUE
        short decideConditionsMet1954 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #POST-LOOP;//Natural: VALUE 01:08
        if (condition(((pnd_Post_Loop.greaterOrEqual(1) && pnd_Post_Loop.lessOrEqual(8)))))
        {
            decideConditionsMet1954++;
            pstl6635_Data_Pstl6635_Rec_Id.setValue("A");                                                                                                                  //Natural: ASSIGN PSTL6635-REC-ID := 'A'
        }                                                                                                                                                                 //Natural: VALUE 09:16
        else if (condition(((pnd_Post_Loop.greaterOrEqual(9) && pnd_Post_Loop.lessOrEqual(16)))))
        {
            decideConditionsMet1954++;
            pstl6635_Data_Pstl6635_Rec_Id.setValue("B");                                                                                                                  //Natural: ASSIGN PSTL6635-REC-ID := 'B'
        }                                                                                                                                                                 //Natural: VALUE 17:24
        else if (condition(((pnd_Post_Loop.greaterOrEqual(17) && pnd_Post_Loop.lessOrEqual(24)))))
        {
            decideConditionsMet1954++;
            pstl6635_Data_Pstl6635_Rec_Id.setValue("C");                                                                                                                  //Natural: ASSIGN PSTL6635-REC-ID := 'C'
        }                                                                                                                                                                 //Natural: VALUE 25:32
        else if (condition(((pnd_Post_Loop.greaterOrEqual(25) && pnd_Post_Loop.lessOrEqual(32)))))
        {
            decideConditionsMet1954++;
            pstl6635_Data_Pstl6635_Rec_Id.setValue("D");                                                                                                                  //Natural: ASSIGN PSTL6635-REC-ID := 'D'
        }                                                                                                                                                                 //Natural: VALUE 33:40
        else if (condition(((pnd_Post_Loop.greaterOrEqual(33) && pnd_Post_Loop.lessOrEqual(40)))))
        {
            decideConditionsMet1954++;
            pstl6635_Data_Pstl6635_Rec_Id.setValue("E");                                                                                                                  //Natural: ASSIGN PSTL6635-REC-ID := 'E'
        }                                                                                                                                                                 //Natural: VALUE 41:48
        else if (condition(((pnd_Post_Loop.greaterOrEqual(41) && pnd_Post_Loop.lessOrEqual(48)))))
        {
            decideConditionsMet1954++;
            pstl6635_Data_Pstl6635_Rec_Id.setValue("F");                                                                                                                  //Natural: ASSIGN PSTL6635-REC-ID := 'F'
        }                                                                                                                                                                 //Natural: VALUE 49:56
        else if (condition(((pnd_Post_Loop.greaterOrEqual(49) && pnd_Post_Loop.lessOrEqual(56)))))
        {
            decideConditionsMet1954++;
            pstl6635_Data_Pstl6635_Rec_Id.setValue("G");                                                                                                                  //Natural: ASSIGN PSTL6635-REC-ID := 'G'
        }                                                                                                                                                                 //Natural: VALUE 57:64
        else if (condition(((pnd_Post_Loop.greaterOrEqual(57) && pnd_Post_Loop.lessOrEqual(64)))))
        {
            decideConditionsMet1954++;
            pstl6635_Data_Pstl6635_Rec_Id.setValue("H");                                                                                                                  //Natural: ASSIGN PSTL6635-REC-ID := 'H'
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY PSTL6635-DATA
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            sort_Key, pstl6635_Data);
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM POST-ERROR
            sub_Post_Error();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  POST-WRITE
    }
    private void sub_Process_Report() throws Exception                                                                                                                    //Natural: PROCESS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_First_Write.getBoolean()))                                                                                                                      //Natural: IF #FIRST-WRITE
        {
            if (condition(getReports().getAstLinesLeft(2).less(6)))                                                                                                       //Natural: NEWPAGE ( 2 ) IF LESS THAN 6 LINES LEFT
            {
                getReports().newPage(2);
                if (condition(Global.isEscape())){return;}
            }
            getReports().display(2, ReportOption.NOTITLE,"PIN",                                                                                                           //Natural: DISPLAY ( 2 ) NOTITLE 'PIN' #MAILTO-PIN 'NAME' #MAILTO-M-NAME 'CHANGE DATE' #OUTPUT-DATE ( EM = MM/DD/YYYY ) 'CHANGE TIME' #OUTPUT-TIME ( EM = 99:99:99:9 ) 81T 'RESIDENTIAL ADDRESS' #MIT-ERROR /
            		pnd_Mailto_Pin,"NAME",
            		pnd_Mailto_M_Name,"CHANGE DATE",
            		pnd_Output_Date, new ReportEditMask ("MM/DD/YYYY"),"CHANGE TIME",
            		pnd_Output_Time, new ReportEditMask ("99:99:99:9"),new TabSetting(81),"RESIDENTIAL ADDRESS",
            		pnd_Work_Letter_Image_File_Pnd_Mit_Error,NEWLINE);
            if (Global.isEscape()) return;
            pnd_First_Write.reset();                                                                                                                                      //Natural: RESET #FIRST-WRITE
            pnd_Write_Addr.getValue(1).setValue(pnd_Mailto_M_Address_1);                                                                                                  //Natural: ASSIGN #WRITE-ADDR ( 1 ) := #MAILTO-M-ADDRESS-1
            pnd_Write_Addr.getValue(2).setValue(pnd_Mailto_M_Address_2);                                                                                                  //Natural: ASSIGN #WRITE-ADDR ( 2 ) := #MAILTO-M-ADDRESS-2
            pnd_Write_Addr.getValue(3).setValue(pnd_Mailto_M_Address_3);                                                                                                  //Natural: ASSIGN #WRITE-ADDR ( 3 ) := #MAILTO-M-ADDRESS-3
            pnd_Write_Addr.getValue(4).setValue(pnd_Mailto_M_Address_4);                                                                                                  //Natural: ASSIGN #WRITE-ADDR ( 4 ) := #MAILTO-M-ADDRESS-4
            pnd_Write_Addr.getValue(5).setValue(pnd_Mailto_M_Address_5);                                                                                                  //Natural: ASSIGN #WRITE-ADDR ( 5 ) := #MAILTO-M-ADDRESS-5
            if (condition(getReports().getAstLinesLeft(2).less(3)))                                                                                                       //Natural: NEWPAGE ( 2 ) IF LESS THAN 3 LINES LEFT
            {
                getReports().newPage(2);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(9),"MAIL TO ADDRESS                   :",pnd_Write_Addr.getValue(1));                               //Natural: WRITE ( 2 ) 9T 'MAIL TO ADDRESS                   :' #WRITE-ADDR ( 1 )
            if (Global.isEscape()) return;
            FOR19:                                                                                                                                                        //Natural: FOR #J = 2 TO 5
            for (pnd_Counters_Pnd_J.setValue(2); condition(pnd_Counters_Pnd_J.lessOrEqual(5)); pnd_Counters_Pnd_J.nadd(1))
            {
                if (condition(pnd_Write_Addr.getValue(pnd_Counters_Pnd_J).greater(" ")))                                                                                  //Natural: IF #WRITE-ADDR ( #J ) > ' '
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(45),pnd_Write_Addr.getValue(pnd_Counters_Pnd_J));                                           //Natural: WRITE ( 2 ) 45T #WRITE-ADDR ( #J )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR20:                                                                                                                                                            //Natural: FOR #K = #CO-START TO #CO-IDX
        for (pnd_Counters_Pnd_K.setValue(pnd_Co_Start); condition(pnd_Counters_Pnd_K.lessOrEqual(pnd_Co_Idx)); pnd_Counters_Pnd_K.nadd(1))
        {
            if (condition(pnd_Co_Group_Pnd_Co_New_Mail_Addr1.getValue(pnd_Counters_Pnd_K).greater(" ")))                                                                  //Natural: IF #CO-NEW-MAIL-ADDR1 ( #K ) > ' '
            {
                if (condition(pnd_Co_Write.getBoolean()))                                                                                                                 //Natural: IF #CO-WRITE
                {
                    pnd_Co_Start.setValue(2);                                                                                                                             //Natural: ASSIGN #CO-START := 2
                    pnd_Co_Write.reset();                                                                                                                                 //Natural: RESET #CO-WRITE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Co_Start.nadd(1);                                                                                                                                 //Natural: ASSIGN #CO-START := #CO-START + 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Write_Addr.getValue(1).setValue(pnd_Co_Group_Pnd_Co_New_Mail_Addr1.getValue(pnd_Counters_Pnd_K));                                                     //Natural: ASSIGN #WRITE-ADDR ( 1 ) := #CO-NEW-MAIL-ADDR1 ( #K )
                pnd_Write_Addr.getValue(2).setValue(pnd_Co_Group_Pnd_Co_New_Mail_Addr2.getValue(pnd_Counters_Pnd_K));                                                     //Natural: ASSIGN #WRITE-ADDR ( 2 ) := #CO-NEW-MAIL-ADDR2 ( #K )
                pnd_Write_Addr.getValue(3).setValue(pnd_Co_Group_Pnd_Co_New_Mail_Addr3.getValue(pnd_Counters_Pnd_K));                                                     //Natural: ASSIGN #WRITE-ADDR ( 3 ) := #CO-NEW-MAIL-ADDR3 ( #K )
                pnd_Write_Addr.getValue(4).setValue(pnd_Co_Group_Pnd_Co_New_Mail_Addr4.getValue(pnd_Counters_Pnd_K));                                                     //Natural: ASSIGN #WRITE-ADDR ( 4 ) := #CO-NEW-MAIL-ADDR4 ( #K )
                pnd_Write_Addr.getValue(5).setValue(pnd_Co_Group_Pnd_Co_New_Mail_Addr5.getValue(pnd_Counters_Pnd_K));                                                     //Natural: ASSIGN #WRITE-ADDR ( 5 ) := #CO-NEW-MAIL-ADDR5 ( #K )
                pnd_Write_Addr2.getValue("*").setValue(pnd_Rs_Group_Pnd_Rs_New_Mail_Addr.getValue("*"));                                                                  //Natural: ASSIGN #WRITE-ADDR2 ( * ) := #RS-NEW-MAIL-ADDR ( * )
                if (condition(getReports().getAstLinesLeft(2).less(4)))                                                                                                   //Natural: NEWPAGE ( 2 ) IF LESS THAN 4 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(9),"NEW MAILING ADDRESS               :",pnd_Write_Addr.getValue(1),new TabSetting(81),         //Natural: WRITE ( 2 ) 9T 'NEW MAILING ADDRESS               :' #WRITE-ADDR ( 1 ) 81T #WRITE-ADDR2 ( 1 )
                    pnd_Write_Addr2.getValue(1));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR21:                                                                                                                                                    //Natural: FOR #J = 2 TO 5
                for (pnd_Counters_Pnd_J.setValue(2); condition(pnd_Counters_Pnd_J.lessOrEqual(5)); pnd_Counters_Pnd_J.nadd(1))
                {
                    if (condition(pnd_Write_Addr.getValue(pnd_Counters_Pnd_J).greater(" ") || pnd_Write_Addr2.getValue(pnd_Counters_Pnd_J).greater(" ")))                 //Natural: IF #WRITE-ADDR ( #J ) > ' ' OR #WRITE-ADDR2 ( #J ) > ' '
                    {
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(45),pnd_Write_Addr.getValue(pnd_Counters_Pnd_J),new TabSetting(81),pnd_Write_Addr2.getValue(pnd_Counters_Pnd_J)); //Natural: WRITE ( 2 ) 45T #WRITE-ADDR ( #J ) 81T #WRITE-ADDR2 ( #J )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Co_Group_Pnd_Co_Contract.getValue("*","*").greater(" ")))                                                                               //Natural: IF #CO-CONTRACT ( *, * ) > ' '
                {
                    FOR22:                                                                                                                                                //Natural: FOR #L = 1 TO 20
                    for (pnd_Counters_Pnd_L.setValue(1); condition(pnd_Counters_Pnd_L.lessOrEqual(20)); pnd_Counters_Pnd_L.nadd(1))
                    {
                        if (condition(pnd_Co_Group_Pnd_Co_Contract.getValue(pnd_Counters_Pnd_K,pnd_Counters_Pnd_L).greater(" ")))                                         //Natural: IF #CO-CONTRACT ( #K, #L ) > ' '
                        {
                            if (condition(pnd_Counters_Pnd_L.greater(10)))                                                                                                //Natural: IF #L > 10
                            {
                                pnd_Contract_Write2.setValue(DbsUtil.compress(pnd_Contract_Write2, pnd_Co_Group_Pnd_Co_Contract.getValue(pnd_Counters_Pnd_K,              //Natural: COMPRESS #CONTRACT-WRITE2 #CO-CONTRACT ( #K, #L ) INTO #CONTRACT-WRITE2
                                    pnd_Counters_Pnd_L)));
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Contract_Write1.setValue(DbsUtil.compress(pnd_Contract_Write1, pnd_Co_Group_Pnd_Co_Contract.getValue(pnd_Counters_Pnd_K,              //Natural: COMPRESS #CONTRACT-WRITE1 #CO-CONTRACT ( #K, #L ) INTO #CONTRACT-WRITE1
                                    pnd_Counters_Pnd_L)));
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(getReports().getAstLinesLeft(2).less(2)))                                                                                               //Natural: NEWPAGE ( 2 ) IF LESS THAN 2 LINES LEFT
                    {
                        getReports().newPage(2);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(9),"CONTRACTS  :",pnd_Contract_Write1);                                             //Natural: WRITE ( 2 ) / 9T 'CONTRACTS  :' #CONTRACT-WRITE1
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Contract_Write2.greater(" ")))                                                                                                      //Natural: IF #CONTRACT-WRITE2 > ' '
                    {
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(22),pnd_Contract_Write2);                                                               //Natural: WRITE ( 2 ) 22T #CONTRACT-WRITE2
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Contract_Write1.reset();                                                                                                                          //Natural: RESET #CONTRACT-WRITE1 #CONTRACT-WRITE2
                    pnd_Contract_Write2.reset();
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  THIS WILL CREATE THE NEW CHECK-MAILING ADDRESS LINE
        pnd_Counters_Pnd_K.reset();                                                                                                                                       //Natural: RESET #K #L
        pnd_Counters_Pnd_L.reset();
        FOR23:                                                                                                                                                            //Natural: FOR #K = #CM-START TO #CM-IDX
        for (pnd_Counters_Pnd_K.setValue(pnd_Cm_Start); condition(pnd_Counters_Pnd_K.lessOrEqual(pnd_Cm_Idx)); pnd_Counters_Pnd_K.nadd(1))
        {
            if (condition(pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr1.getValue(pnd_Counters_Pnd_K).greater(" ")))                                                                  //Natural: IF #CM-PAY-MAIL-ADDR1 ( #K ) > ' '
            {
                if (condition(pnd_Cm_Write.getBoolean()))                                                                                                                 //Natural: IF #CM-WRITE
                {
                    pnd_Cm_Start.setValue(2);                                                                                                                             //Natural: ASSIGN #CM-START := 2
                    pnd_Cm_Write.reset();                                                                                                                                 //Natural: RESET #CM-WRITE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cm_Start.nadd(1);                                                                                                                                 //Natural: ASSIGN #CM-START := #CM-START + 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Write_Addr.getValue(1).setValue(pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr1.getValue(pnd_Counters_Pnd_K));                                                     //Natural: ASSIGN #WRITE-ADDR ( 1 ) := #CM-PAY-MAIL-ADDR1 ( #K )
                pnd_Write_Addr.getValue(2).setValue(pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr2.getValue(pnd_Counters_Pnd_K));                                                     //Natural: ASSIGN #WRITE-ADDR ( 2 ) := #CM-PAY-MAIL-ADDR2 ( #K )
                pnd_Write_Addr.getValue(3).setValue(pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr3.getValue(pnd_Counters_Pnd_K));                                                     //Natural: ASSIGN #WRITE-ADDR ( 3 ) := #CM-PAY-MAIL-ADDR3 ( #K )
                pnd_Write_Addr.getValue(4).setValue(pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr4.getValue(pnd_Counters_Pnd_K));                                                     //Natural: ASSIGN #WRITE-ADDR ( 4 ) := #CM-PAY-MAIL-ADDR4 ( #K )
                pnd_Write_Addr.getValue(5).setValue(pnd_Cm_Group_Pnd_Cm_Pay_Mail_Addr5.getValue(pnd_Counters_Pnd_K));                                                     //Natural: ASSIGN #WRITE-ADDR ( 5 ) := #CM-PAY-MAIL-ADDR5 ( #K )
                if (condition(getReports().getAstLinesLeft(2).less(4)))                                                                                                   //Natural: NEWPAGE ( 2 ) IF LESS THAN 4 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(9),"NEW PAYMENT ADDRESS               :",pnd_Write_Addr.getValue(1));                           //Natural: WRITE ( 2 ) 9T 'NEW PAYMENT ADDRESS               :' #WRITE-ADDR ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR24:                                                                                                                                                    //Natural: FOR #J = 2 TO 5
                for (pnd_Counters_Pnd_J.setValue(2); condition(pnd_Counters_Pnd_J.lessOrEqual(5)); pnd_Counters_Pnd_J.nadd(1))
                {
                    if (condition(pnd_Write_Addr.getValue(pnd_Counters_Pnd_J).greater(" ")))                                                                              //Natural: IF #WRITE-ADDR ( #J ) > ' '
                    {
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(45),pnd_Write_Addr.getValue(pnd_Counters_Pnd_J));                                       //Natural: WRITE ( 2 ) 45T #WRITE-ADDR ( #J )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Cm_Group_Pnd_Cm_Pay_Bnk_Acct_Nbr.getValue(pnd_Counters_Pnd_K).greater(" ")))                                                            //Natural: IF #CM-PAY-BNK-ACCT-NBR ( #K ) > ' '
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(45),"ACCOUNT :",pnd_Cm_Group_Pnd_Cm_Pay_Bnk_Acct_Nbr.getValue(pnd_Counters_Pnd_K),new       //Natural: WRITE ( 2 ) 45T 'ACCOUNT :' #CM-PAY-BNK-ACCT-NBR ( #K ) 92T 'C/S :' #CM-PAY-CS-IND ( #K )
                        TabSetting(92),"C/S :",pnd_Cm_Group_Pnd_Cm_Pay_Cs_Ind.getValue(pnd_Counters_Pnd_K));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(45),"EFT/ABA :",pnd_Cm_Group_Pnd_Cm_Pay_Eft_Nbr.getValue(pnd_Counters_Pnd_K));              //Natural: WRITE ( 2 ) 45T 'EFT/ABA :' #CM-PAY-EFT-NBR ( #K )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                FOR25:                                                                                                                                                    //Natural: FOR #L 1 TO 20
                for (pnd_Counters_Pnd_L.setValue(1); condition(pnd_Counters_Pnd_L.lessOrEqual(20)); pnd_Counters_Pnd_L.nadd(1))
                {
                    if (condition(pnd_Cm_Group_Pnd_Cm_Contract.getValue(pnd_Counters_Pnd_K,pnd_Counters_Pnd_L).greater(" ")))                                             //Natural: IF #CM-CONTRACT ( #K, #L ) > ' '
                    {
                        if (condition(pnd_Counters_Pnd_L.greater(10)))                                                                                                    //Natural: IF #L > 10
                        {
                            pnd_Contract_Write2.setValue(DbsUtil.compress(pnd_Contract_Write2, pnd_Cm_Group_Pnd_Cm_Contract.getValue(pnd_Counters_Pnd_K,                  //Natural: COMPRESS #CONTRACT-WRITE2 #CM-CONTRACT ( #K, #L ) INTO #CONTRACT-WRITE2
                                pnd_Counters_Pnd_L)));
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Contract_Write1.setValue(DbsUtil.compress(pnd_Contract_Write1, pnd_Cm_Group_Pnd_Cm_Contract.getValue(pnd_Counters_Pnd_K,                  //Natural: COMPRESS #CONTRACT-WRITE1 #CM-CONTRACT ( #K, #L ) INTO #CONTRACT-WRITE1
                                pnd_Counters_Pnd_L)));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(getReports().getAstLinesLeft(2).less(2)))                                                                                                   //Natural: NEWPAGE ( 2 ) IF LESS THAN 2 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(9),"CONTRACTS  :",pnd_Contract_Write1);                                                         //Natural: WRITE ( 2 ) 9T 'CONTRACTS  :' #CONTRACT-WRITE1
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Contract_Write2.greater(" ")))                                                                                                          //Natural: IF #CONTRACT-WRITE2 > ' '
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(22),pnd_Contract_Write2);                                                                   //Natural: WRITE ( 2 ) 22T #CONTRACT-WRITE2
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Contract_Write1.reset();                                                                                                                              //Natural: RESET #CONTRACT-WRITE1 #CONTRACT-WRITE2
                pnd_Contract_Write2.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR26:                                                                                                                                                            //Natural: FOR #K = #FCO-START TO #FCO-IDX
        for (pnd_Counters_Pnd_K.setValue(pnd_Fco_Start); condition(pnd_Counters_Pnd_K.lessOrEqual(pnd_Fco_Idx)); pnd_Counters_Pnd_K.nadd(1))
        {
            if (condition(pnd_Fco_Group_Pnd_Fco_Mail_Addr1.getValue(pnd_Counters_Pnd_K).greater(" ")))                                                                    //Natural: IF #FCO-MAIL-ADDR1 ( #K ) > ' '
            {
                if (condition(pnd_Fco_Write.getBoolean()))                                                                                                                //Natural: IF #FCO-WRITE
                {
                    pnd_Fco_Start.setValue(2);                                                                                                                            //Natural: ASSIGN #FCO-START := 2
                    pnd_Fco_Write.reset();                                                                                                                                //Natural: RESET #FCO-WRITE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Fco_Start.nadd(1);                                                                                                                                //Natural: ASSIGN #FCO-START := #FCO-START + 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Write_Addr.getValue(1).setValue(pnd_Fco_Group_Pnd_Fco_Mail_Addr1.getValue(pnd_Counters_Pnd_K));                                                       //Natural: ASSIGN #WRITE-ADDR ( 1 ) := #FCO-MAIL-ADDR1 ( #K )
                pnd_Write_Addr.getValue(2).setValue(pnd_Fco_Group_Pnd_Fco_Mail_Addr2.getValue(pnd_Counters_Pnd_K));                                                       //Natural: ASSIGN #WRITE-ADDR ( 2 ) := #FCO-MAIL-ADDR2 ( #K )
                pnd_Write_Addr.getValue(3).setValue(pnd_Fco_Group_Pnd_Fco_Mail_Addr3.getValue(pnd_Counters_Pnd_K));                                                       //Natural: ASSIGN #WRITE-ADDR ( 3 ) := #FCO-MAIL-ADDR3 ( #K )
                pnd_Write_Addr.getValue(4).setValue(pnd_Fco_Group_Pnd_Fco_Mail_Addr4.getValue(pnd_Counters_Pnd_K));                                                       //Natural: ASSIGN #WRITE-ADDR ( 4 ) := #FCO-MAIL-ADDR4 ( #K )
                pnd_Write_Addr.getValue(5).setValue(pnd_Fco_Group_Pnd_Fco_Mail_Addr5.getValue(pnd_Counters_Pnd_K));                                                       //Natural: ASSIGN #WRITE-ADDR ( 5 ) := #FCO-MAIL-ADDR5 ( #K )
                if (condition(getReports().getAstLinesLeft(2).less(4)))                                                                                                   //Natural: NEWPAGE ( 2 ) IF LESS THAN 4 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(9),"NEW FUTURE MAILING ADDRESS        :",pnd_Write_Addr.getValue(1),new TabSetting(81),"START DATE:",pnd_Fco_Group_Pnd_Fco_Begin_Dte.getValue(1),  //Natural: WRITE ( 2 ) 9T 'NEW FUTURE MAILING ADDRESS        :' #WRITE-ADDR ( 1 ) 81T 'START DATE:' #FCO-BEGIN-DTE ( 1 ) ( EM = XX/XX/XXXX )
                    new ReportEditMask ("XX/XX/XXXX"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR27:                                                                                                                                                    //Natural: FOR #J = 2 TO 5
                for (pnd_Counters_Pnd_J.setValue(2); condition(pnd_Counters_Pnd_J.lessOrEqual(5)); pnd_Counters_Pnd_J.nadd(1))
                {
                    if (condition(pnd_Write_Addr.getValue(pnd_Counters_Pnd_J).greater(" ")))                                                                              //Natural: IF #WRITE-ADDR ( #J ) > ' '
                    {
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(45),pnd_Write_Addr.getValue(pnd_Counters_Pnd_J));                                       //Natural: WRITE ( 2 ) 45T #WRITE-ADDR ( #J )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR28:                                                                                                                                                    //Natural: FOR #L = 1 TO 20
                for (pnd_Counters_Pnd_L.setValue(1); condition(pnd_Counters_Pnd_L.lessOrEqual(20)); pnd_Counters_Pnd_L.nadd(1))
                {
                    if (condition(pnd_Fco_Group_Pnd_Fco_Contract.getValue(pnd_Counters_Pnd_K,pnd_Counters_Pnd_L).greater(" ")))                                           //Natural: IF #FCO-CONTRACT ( #K, #L ) > ' '
                    {
                        if (condition(pnd_Counters_Pnd_L.greater(10)))                                                                                                    //Natural: IF #L > 10
                        {
                            pnd_Contract_Write2.setValue(DbsUtil.compress(pnd_Contract_Write2, pnd_Fco_Group_Pnd_Fco_Contract.getValue(pnd_Counters_Pnd_K,                //Natural: COMPRESS #CONTRACT-WRITE2 #FCO-CONTRACT ( #K, #L ) INTO #CONTRACT-WRITE2
                                pnd_Counters_Pnd_L)));
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Contract_Write1.setValue(DbsUtil.compress(pnd_Contract_Write1, pnd_Fco_Group_Pnd_Fco_Contract.getValue(pnd_Counters_Pnd_K,                //Natural: COMPRESS #CONTRACT-WRITE1 #FCO-CONTRACT ( #K, #L ) INTO #CONTRACT-WRITE1
                                pnd_Counters_Pnd_L)));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(getReports().getAstLinesLeft(2).less(2)))                                                                                                   //Natural: NEWPAGE ( 2 ) IF LESS THAN 2 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(9),"CONTRACTS  :",pnd_Contract_Write1);                                                         //Natural: WRITE ( 2 ) 9T 'CONTRACTS  :' #CONTRACT-WRITE1
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Contract_Write2.greater(" ")))                                                                                                          //Natural: IF #CONTRACT-WRITE2 > ' '
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(22),pnd_Contract_Write2);                                                                   //Natural: WRITE ( 2 ) 22T #CONTRACT-WRITE2
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Contract_Write1.reset();                                                                                                                              //Natural: RESET #CONTRACT-WRITE1 #CONTRACT-WRITE2
                pnd_Contract_Write2.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR29:                                                                                                                                                            //Natural: FOR #K = #FRS-START TO #FRS-IDX
        for (pnd_Counters_Pnd_K.setValue(pnd_Frs_Start); condition(pnd_Counters_Pnd_K.lessOrEqual(pnd_Frs_Idx)); pnd_Counters_Pnd_K.nadd(1))
        {
            if (condition(pnd_Frs_Group_Pnd_Frs_Mail_Addr.getValue(1).greater(" ")))                                                                                      //Natural: IF #FRS-MAIL-ADDR ( 1 ) > ' '
            {
                if (condition(pnd_Frs_Write.getBoolean()))                                                                                                                //Natural: IF #FRS-WRITE
                {
                    pnd_Frs_Start.setValue(2);                                                                                                                            //Natural: ASSIGN #FRS-START := 2
                    pnd_Frs_Write.reset();                                                                                                                                //Natural: RESET #FRS-WRITE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Frs_Start.nadd(1);                                                                                                                                //Natural: ASSIGN #FRS-START := #FRS-START + 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Write_Addr.getValue("*").setValue(pnd_Frs_Group_Pnd_Frs_Mail_Addr.getValue("*"));                                                                     //Natural: ASSIGN #WRITE-ADDR ( * ) := #FRS-MAIL-ADDR ( * )
                if (condition(getReports().getAstLinesLeft(2).less(4)))                                                                                                   //Natural: NEWPAGE ( 2 ) IF LESS THAN 4 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(9),"NEW FUTURE RESIDENTIAL ADDRESS    :",pnd_Write_Addr.getValue(1),new TabSetting(81),"START DATE:",pnd_Frs_Group_Pnd_Frs_Begin_Dte,  //Natural: WRITE ( 2 ) 9T 'NEW FUTURE RESIDENTIAL ADDRESS    :' #WRITE-ADDR ( 1 ) 81T 'START DATE:' #FRS-BEGIN-DTE ( EM = XX/XX/XXXX )
                    new ReportEditMask ("XX/XX/XXXX"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR30:                                                                                                                                                    //Natural: FOR #J = 2 TO 5
                for (pnd_Counters_Pnd_J.setValue(2); condition(pnd_Counters_Pnd_J.lessOrEqual(5)); pnd_Counters_Pnd_J.nadd(1))
                {
                    if (condition(pnd_Write_Addr.getValue(pnd_Counters_Pnd_J).greater(" ")))                                                                              //Natural: IF #WRITE-ADDR ( #J ) > ' '
                    {
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(45),pnd_Write_Addr.getValue(pnd_Counters_Pnd_J));                                       //Natural: WRITE ( 2 ) 45T #WRITE-ADDR ( #J )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR31:                                                                                                                                                            //Natural: FOR #K = #FCM-START TO #FCM-IDX
        for (pnd_Counters_Pnd_K.setValue(pnd_Fcm_Start); condition(pnd_Counters_Pnd_K.lessOrEqual(pnd_Fcm_Idx)); pnd_Counters_Pnd_K.nadd(1))
        {
            if (condition(pnd_Fcm_Group_Pnd_Fcm_Pay_Addr1.getValue(pnd_Counters_Pnd_K).greater(" ")))                                                                     //Natural: IF #FCM-PAY-ADDR1 ( #K ) > ' '
            {
                if (condition(pnd_Fcm_Write.getBoolean()))                                                                                                                //Natural: IF #FCM-WRITE
                {
                    pnd_Fcm_Start.setValue(2);                                                                                                                            //Natural: ASSIGN #FCM-START := 2
                    pnd_Fcm_Write.reset();                                                                                                                                //Natural: RESET #FCM-WRITE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Fcm_Start.nadd(1);                                                                                                                                //Natural: ASSIGN #FCM-START := #FCM-START + 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Write_Addr.getValue(1).setValue(pnd_Fcm_Group_Pnd_Fcm_Pay_Addr1.getValue(pnd_Counters_Pnd_K));                                                        //Natural: ASSIGN #WRITE-ADDR ( 1 ) := #FCM-PAY-ADDR1 ( #K )
                pnd_Write_Addr.getValue(2).setValue(pnd_Fcm_Group_Pnd_Fcm_Pay_Addr2.getValue(pnd_Counters_Pnd_K));                                                        //Natural: ASSIGN #WRITE-ADDR ( 2 ) := #FCM-PAY-ADDR2 ( #K )
                pnd_Write_Addr.getValue(3).setValue(pnd_Fcm_Group_Pnd_Fcm_Pay_Addr3.getValue(pnd_Counters_Pnd_K));                                                        //Natural: ASSIGN #WRITE-ADDR ( 3 ) := #FCM-PAY-ADDR3 ( #K )
                pnd_Write_Addr.getValue(4).setValue(pnd_Fcm_Group_Pnd_Fcm_Pay_Addr4.getValue(pnd_Counters_Pnd_K));                                                        //Natural: ASSIGN #WRITE-ADDR ( 4 ) := #FCM-PAY-ADDR4 ( #K )
                pnd_Write_Addr.getValue(5).setValue(pnd_Fcm_Group_Pnd_Fcm_Pay_Addr5.getValue(pnd_Counters_Pnd_K));                                                        //Natural: ASSIGN #WRITE-ADDR ( 5 ) := #FCM-PAY-ADDR5 ( #K )
                if (condition(getReports().getAstLinesLeft(2).less(4)))                                                                                                   //Natural: NEWPAGE ( 2 ) IF LESS THAN 4 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(9),"NEW FUTURE PAYMENT ADDRESS        :",pnd_Write_Addr.getValue(1),new TabSetting(81),"START DATE:",pnd_Fcm_Group_Pnd_Fcm_Begin_Dte.getValue(1),  //Natural: WRITE ( 2 ) 9T 'NEW FUTURE PAYMENT ADDRESS        :' #WRITE-ADDR ( 1 ) 81T 'START DATE:' #FCM-BEGIN-DTE ( 1 ) ( EM = XX/XX/XXXX )
                    new ReportEditMask ("XX/XX/XXXX"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR32:                                                                                                                                                    //Natural: FOR #J = 2 TO 5
                for (pnd_Counters_Pnd_J.setValue(2); condition(pnd_Counters_Pnd_J.lessOrEqual(5)); pnd_Counters_Pnd_J.nadd(1))
                {
                    if (condition(pnd_Write_Addr.getValue(pnd_Counters_Pnd_J).greater(" ")))                                                                              //Natural: IF #WRITE-ADDR ( #J ) > ' '
                    {
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(45),pnd_Write_Addr.getValue(pnd_Counters_Pnd_J));                                       //Natural: WRITE ( 2 ) 45T #WRITE-ADDR ( #J )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR33:                                                                                                                                                    //Natural: FOR #L = 1 TO 20
                for (pnd_Counters_Pnd_L.setValue(1); condition(pnd_Counters_Pnd_L.lessOrEqual(20)); pnd_Counters_Pnd_L.nadd(1))
                {
                    if (condition(pnd_Fcm_Group_Pnd_Fcm_Contract.getValue(pnd_Counters_Pnd_K,pnd_Counters_Pnd_L).greater(" ")))                                           //Natural: IF #FCM-CONTRACT ( #K, #L ) > ' '
                    {
                        if (condition(pnd_Counters_Pnd_L.greater(10)))                                                                                                    //Natural: IF #L > 10
                        {
                            pnd_Contract_Write2.setValue(DbsUtil.compress(pnd_Contract_Write2, pnd_Fcm_Group_Pnd_Fcm_Contract.getValue(pnd_Counters_Pnd_K,                //Natural: COMPRESS #CONTRACT-WRITE2 #FCM-CONTRACT ( #K, #L ) INTO #CONTRACT-WRITE2
                                pnd_Counters_Pnd_L)));
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Contract_Write1.setValue(DbsUtil.compress(pnd_Contract_Write1, pnd_Fcm_Group_Pnd_Fcm_Contract.getValue(pnd_Counters_Pnd_K,                //Natural: COMPRESS #CONTRACT-WRITE1 #FCM-CONTRACT ( #K, #L ) INTO #CONTRACT-WRITE1
                                pnd_Counters_Pnd_L)));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(getReports().getAstLinesLeft(2).less(2)))                                                                                                   //Natural: NEWPAGE ( 2 ) IF LESS THAN 2 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(9),"CONTRACTS  :",pnd_Contract_Write1);                                                 //Natural: WRITE ( 2 ) / 9T 'CONTRACTS  :' #CONTRACT-WRITE1
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Contract_Write2.greater(" ")))                                                                                                          //Natural: IF #CONTRACT-WRITE2 > ' '
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(22),pnd_Contract_Write2);                                                                   //Natural: WRITE ( 2 ) 22T #CONTRACT-WRITE2
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Contract_Write1.reset();                                                                                                                              //Natural: RESET #CONTRACT-WRITE1 #CONTRACT-WRITE2
                pnd_Contract_Write2.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR34:                                                                                                                                                            //Natural: FOR #K = #VCO-START TO #VCO-IDX
        for (pnd_Counters_Pnd_K.setValue(pnd_Vco_Start); condition(pnd_Counters_Pnd_K.lessOrEqual(pnd_Vco_Idx)); pnd_Counters_Pnd_K.nadd(1))
        {
            if (condition(pnd_Vco_Group_Pnd_Vco_Mail_Addr1.getValue(pnd_Counters_Pnd_K).greater(" ")))                                                                    //Natural: IF #VCO-MAIL-ADDR1 ( #K ) > ' '
            {
                if (condition(pnd_Vco_Write.getBoolean()))                                                                                                                //Natural: IF #VCO-WRITE
                {
                    pnd_Vco_Start.setValue(2);                                                                                                                            //Natural: ASSIGN #VCO-START := 2
                    pnd_Vco_Write.reset();                                                                                                                                //Natural: RESET #VCO-WRITE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Vco_Start.nadd(1);                                                                                                                                //Natural: ASSIGN #VCO-START := #VCO-START + 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Write_Addr.getValue(1).setValue(pnd_Vco_Group_Pnd_Vco_Mail_Addr1.getValue(pnd_Counters_Pnd_K));                                                       //Natural: ASSIGN #WRITE-ADDR ( 1 ) := #VCO-MAIL-ADDR1 ( #K )
                pnd_Write_Addr.getValue(2).setValue(pnd_Vco_Group_Pnd_Vco_Mail_Addr2.getValue(pnd_Counters_Pnd_K));                                                       //Natural: ASSIGN #WRITE-ADDR ( 2 ) := #VCO-MAIL-ADDR2 ( #K )
                pnd_Write_Addr.getValue(3).setValue(pnd_Vco_Group_Pnd_Vco_Mail_Addr3.getValue(pnd_Counters_Pnd_K));                                                       //Natural: ASSIGN #WRITE-ADDR ( 3 ) := #VCO-MAIL-ADDR3 ( #K )
                pnd_Write_Addr.getValue(4).setValue(pnd_Vco_Group_Pnd_Vco_Mail_Addr4.getValue(pnd_Counters_Pnd_K));                                                       //Natural: ASSIGN #WRITE-ADDR ( 4 ) := #VCO-MAIL-ADDR4 ( #K )
                pnd_Write_Addr.getValue(5).setValue(pnd_Vco_Group_Pnd_Vco_Mail_Addr5.getValue(pnd_Counters_Pnd_K));                                                       //Natural: ASSIGN #WRITE-ADDR ( 5 ) := #VCO-MAIL-ADDR5 ( #K )
                if (condition(getReports().getAstLinesLeft(2).less(4)))                                                                                                   //Natural: NEWPAGE ( 2 ) IF LESS THAN 4 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(9),"NEW TEMPORARY MAILING ADDRESS     :",pnd_Write_Addr.getValue(1),new TabSetting(81),"EFF FROM:",pnd_Vco_Group_Pnd_Vco_Begin_Dte.getValue(pnd_Counters_Pnd_K),  //Natural: WRITE ( 2 ) 009T 'NEW TEMPORARY MAILING ADDRESS     :' #WRITE-ADDR ( 1 ) 081T 'EFF FROM:' #VCO-BEGIN-DTE ( #K ) ( EM = XX/XX/XXXX ) 102T 'TO:' #VCO-END-DTE ( #K ) ( EM = XX/XX/XXXX ) 117T 'ANNUAL:' #VCO-ANNUAL-CYCLE-IND ( #K )
                    new ReportEditMask ("XX/XX/XXXX"),new TabSetting(102),"TO:",pnd_Vco_Group_Pnd_Vco_End_Dte.getValue(pnd_Counters_Pnd_K), new ReportEditMask 
                    ("XX/XX/XXXX"),new TabSetting(117),"ANNUAL:",pnd_Vco_Group_Pnd_Vco_Annual_Cycle_Ind.getValue(pnd_Counters_Pnd_K));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR35:                                                                                                                                                    //Natural: FOR #J = 2 TO 5
                for (pnd_Counters_Pnd_J.setValue(2); condition(pnd_Counters_Pnd_J.lessOrEqual(5)); pnd_Counters_Pnd_J.nadd(1))
                {
                    if (condition(pnd_Write_Addr.getValue(pnd_Counters_Pnd_J).greater(" ")))                                                                              //Natural: IF #WRITE-ADDR ( #J ) > ' '
                    {
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(45),pnd_Write_Addr.getValue(pnd_Counters_Pnd_J));                                       //Natural: WRITE ( 2 ) 45T #WRITE-ADDR ( #J )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR36:                                                                                                                                                    //Natural: FOR #L = 1 TO 20
                for (pnd_Counters_Pnd_L.setValue(1); condition(pnd_Counters_Pnd_L.lessOrEqual(20)); pnd_Counters_Pnd_L.nadd(1))
                {
                    if (condition(pnd_Vco_Group_Pnd_Vco_Contract.getValue(pnd_Counters_Pnd_K,pnd_Counters_Pnd_L).greater(" ")))                                           //Natural: IF #VCO-CONTRACT ( #K, #L ) > ' '
                    {
                        if (condition(pnd_Counters_Pnd_L.greater(10)))                                                                                                    //Natural: IF #L > 10
                        {
                            pnd_Contract_Write2.setValue(DbsUtil.compress(pnd_Contract_Write2, pnd_Vco_Group_Pnd_Vco_Contract.getValue(pnd_Counters_Pnd_K,                //Natural: COMPRESS #CONTRACT-WRITE2 #VCO-CONTRACT ( #K, #L ) INTO #CONTRACT-WRITE2
                                pnd_Counters_Pnd_L)));
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Contract_Write1.setValue(DbsUtil.compress(pnd_Contract_Write1, pnd_Vco_Group_Pnd_Vco_Contract.getValue(pnd_Counters_Pnd_K,                //Natural: COMPRESS #CONTRACT-WRITE1 #VCO-CONTRACT ( #K, #L ) INTO #CONTRACT-WRITE1
                                pnd_Counters_Pnd_L)));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(getReports().getAstLinesLeft(2).less(2)))                                                                                                   //Natural: NEWPAGE ( 2 ) IF LESS THAN 2 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(9),"CONTRACTS  :",pnd_Contract_Write1);                                                 //Natural: WRITE ( 2 ) / 9T 'CONTRACTS  :' #CONTRACT-WRITE1
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Contract_Write2.greater(" ")))                                                                                                          //Natural: IF #CONTRACT-WRITE2 > ' '
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(22),pnd_Contract_Write2);                                                                   //Natural: WRITE ( 2 ) 22T #CONTRACT-WRITE2
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Contract_Write1.reset();                                                                                                                              //Natural: RESET #CONTRACT-WRITE1 #CONTRACT-WRITE2
                pnd_Contract_Write2.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR37:                                                                                                                                                            //Natural: FOR #K = #VCM-START TO #VCM-IDX
        for (pnd_Counters_Pnd_K.setValue(pnd_Vcm_Start); condition(pnd_Counters_Pnd_K.lessOrEqual(pnd_Vcm_Idx)); pnd_Counters_Pnd_K.nadd(1))
        {
            if (condition(pnd_Vcm_Group_Pnd_Vcm_Pay_Addr1.getValue(pnd_Counters_Pnd_K).greater(" ")))                                                                     //Natural: IF #VCM-PAY-ADDR1 ( #K ) > ' '
            {
                if (condition(pnd_Vcm_Write.getBoolean()))                                                                                                                //Natural: IF #VCM-WRITE
                {
                    pnd_Vcm_Start.setValue(2);                                                                                                                            //Natural: ASSIGN #VCM-START := 2
                    pnd_Vcm_Write.reset();                                                                                                                                //Natural: RESET #VCM-WRITE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Vcm_Start.nadd(1);                                                                                                                                //Natural: ASSIGN #VCM-START := #VCM-START + 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Write_Addr.getValue(1).setValue(pnd_Vcm_Group_Pnd_Vcm_Pay_Addr1.getValue(pnd_Counters_Pnd_K));                                                        //Natural: ASSIGN #WRITE-ADDR ( 1 ) := #VCM-PAY-ADDR1 ( #K )
                pnd_Write_Addr.getValue(2).setValue(pnd_Vcm_Group_Pnd_Vcm_Pay_Addr2.getValue(pnd_Counters_Pnd_K));                                                        //Natural: ASSIGN #WRITE-ADDR ( 2 ) := #VCM-PAY-ADDR2 ( #K )
                pnd_Write_Addr.getValue(3).setValue(pnd_Vcm_Group_Pnd_Vcm_Pay_Addr3.getValue(pnd_Counters_Pnd_K));                                                        //Natural: ASSIGN #WRITE-ADDR ( 3 ) := #VCM-PAY-ADDR3 ( #K )
                pnd_Write_Addr.getValue(4).setValue(pnd_Vcm_Group_Pnd_Vcm_Pay_Addr4.getValue(pnd_Counters_Pnd_K));                                                        //Natural: ASSIGN #WRITE-ADDR ( 4 ) := #VCM-PAY-ADDR4 ( #K )
                pnd_Write_Addr.getValue(5).setValue(pnd_Vcm_Group_Pnd_Vcm_Pay_Addr5.getValue(pnd_Counters_Pnd_K));                                                        //Natural: ASSIGN #WRITE-ADDR ( 5 ) := #VCM-PAY-ADDR5 ( #K )
                if (condition(getReports().getAstLinesLeft(2).less(4)))                                                                                                   //Natural: NEWPAGE ( 2 ) IF LESS THAN 4 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(9),"NEW TEMPORARY MAILING ADDRESS     :",pnd_Write_Addr.getValue(1),new TabSetting(81),"EFF FROM:",pnd_Vcm_Group_Pnd_Vcm_Begin_Dte.getValue(pnd_Counters_Pnd_K),  //Natural: WRITE ( 2 ) 009T 'NEW TEMPORARY MAILING ADDRESS     :' #WRITE-ADDR ( 1 ) 081T 'EFF FROM:' #VCM-BEGIN-DTE ( #K ) ( EM = XX/XX/XXXX ) 102T 'TO:' #VCM-END-DTE ( #K ) ( EM = XX/XX/XXXX ) 117T 'ANNUAL:' #VCM-ANNUAL-CYCLE-IND ( #K )
                    new ReportEditMask ("XX/XX/XXXX"),new TabSetting(102),"TO:",pnd_Vcm_Group_Pnd_Vcm_End_Dte.getValue(pnd_Counters_Pnd_K), new ReportEditMask 
                    ("XX/XX/XXXX"),new TabSetting(117),"ANNUAL:",pnd_Vcm_Group_Pnd_Vcm_Annual_Cycle_Ind.getValue(pnd_Counters_Pnd_K));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR38:                                                                                                                                                    //Natural: FOR #J = 1 TO 5
                for (pnd_Counters_Pnd_J.setValue(1); condition(pnd_Counters_Pnd_J.lessOrEqual(5)); pnd_Counters_Pnd_J.nadd(1))
                {
                    if (condition(pnd_Write_Addr.getValue(pnd_Counters_Pnd_J).greater(" ")))                                                                              //Natural: IF #WRITE-ADDR ( #J ) > ' '
                    {
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(45),pnd_Write_Addr.getValue(pnd_Counters_Pnd_J));                                       //Natural: WRITE ( 2 ) 45T #WRITE-ADDR ( #J )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR39:                                                                                                                                                    //Natural: FOR #L = 1 TO 20
                for (pnd_Counters_Pnd_L.setValue(1); condition(pnd_Counters_Pnd_L.lessOrEqual(20)); pnd_Counters_Pnd_L.nadd(1))
                {
                    if (condition(pnd_Vcm_Group_Pnd_Vcm_Contract.getValue(pnd_Counters_Pnd_K,pnd_Counters_Pnd_L).greater(" ")))                                           //Natural: IF #VCM-CONTRACT ( #K, #L ) > ' '
                    {
                        if (condition(pnd_Counters_Pnd_L.greater(10)))                                                                                                    //Natural: IF #L > 10
                        {
                            pnd_Contract_Write2.setValue(DbsUtil.compress(pnd_Contract_Write2, pnd_Vcm_Group_Pnd_Vcm_Contract.getValue(pnd_Counters_Pnd_K,                //Natural: COMPRESS #CONTRACT-WRITE2 #VCM-CONTRACT ( #K, #L ) INTO #CONTRACT-WRITE2
                                pnd_Counters_Pnd_L)));
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Contract_Write1.setValue(DbsUtil.compress(pnd_Contract_Write1, pnd_Vcm_Group_Pnd_Vcm_Contract.getValue(pnd_Counters_Pnd_K,                //Natural: COMPRESS #CONTRACT-WRITE1 #VCM-CONTRACT ( #K, #L ) INTO #CONTRACT-WRITE1
                                pnd_Counters_Pnd_L)));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(getReports().getAstLinesLeft(2).less(2)))                                                                                                   //Natural: NEWPAGE ( 2 ) IF LESS THAN 2 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(9),"CONTRACTS  :",pnd_Contract_Write1);                                                 //Natural: WRITE ( 2 ) / 9T 'CONTRACTS  :' #CONTRACT-WRITE1
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Contract_Write2.greater(" ")))                                                                                                          //Natural: IF #CONTRACT-WRITE2 > ' '
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(22),pnd_Contract_Write2);                                                                   //Natural: WRITE ( 2 ) 22T #CONTRACT-WRITE2
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Contract_Write1.reset();                                                                                                                              //Natural: RESET #CONTRACT-WRITE1 #CONTRACT-WRITE2
                pnd_Contract_Write2.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE);                                                                                                              //Natural: WRITE ( 2 ) /
        if (Global.isEscape()) return;
        //*  PROCESS-REPORT
    }
    private void sub_Write_Error_Report() throws Exception                                                                                                                //Natural: WRITE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        FOR40:                                                                                                                                                            //Natural: FOR #X = 1 TO #CO-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Co_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            FOR41:                                                                                                                                                        //Natural: FOR #Y = 1 TO 20
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(20)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pnd_Co_Group_Pnd_Co_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                                 //Natural: IF #CO-CONTRACT ( #X, #Y ) > ' '
                {
                    //*  PINE MOVE TAB
                    getReports().write(3, ReportOption.NOTITLE,pnd_Mailto_Pin,new TabSetting(14),pnd_Co_Group_Pnd_Co_Name.getValue(pnd_Counters_Pnd_X),new                //Natural: WRITE ( 3 ) #MAILTO-PIN 14T #CO-NAME ( #X ) 63T #CO-CONTRACT ( #X, #Y )
                        TabSetting(63),pnd_Co_Group_Pnd_Co_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Error_Work.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ERROR-WORK
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Rs_Group_Pnd_Rs_Name.notEquals(" ")))                                                                                                           //Natural: IF #RS-NAME NE ' '
        {
            //*  PINE MOVE TAB
            getReports().write(3, ReportOption.NOTITLE,pnd_Mailto_Pin,new TabSetting(14),pnd_Rs_Group_Pnd_Rs_Name);                                                       //Natural: WRITE ( 3 ) #MAILTO-PIN 14T #RS-NAME
            if (Global.isEscape()) return;
            pnd_Error_Work.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ERROR-WORK
        }                                                                                                                                                                 //Natural: END-IF
        FOR42:                                                                                                                                                            //Natural: FOR #X = 1 TO #CM-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Cm_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            FOR43:                                                                                                                                                        //Natural: FOR #Y = 1 TO 20
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(20)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pnd_Cm_Group_Pnd_Cm_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                                 //Natural: IF #CM-CONTRACT ( #X, #Y ) > ' '
                {
                    //*  PINE MOVE TAB
                    getReports().write(3, ReportOption.NOTITLE,pnd_Mailto_Pin,new TabSetting(14),pnd_Cm_Group_Pnd_Cm_Name.getValue(pnd_Counters_Pnd_X),new                //Natural: WRITE ( 3 ) #MAILTO-PIN 14T #CM-NAME ( #X ) 63T #CM-CONTRACT ( #X, #Y )
                        TabSetting(63),pnd_Cm_Group_Pnd_Cm_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Error_Work.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ERROR-WORK
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR44:                                                                                                                                                            //Natural: FOR #X = 1 TO #FCO-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Fco_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            FOR45:                                                                                                                                                        //Natural: FOR #Y = 1 TO 20
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(20)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pnd_Fco_Group_Pnd_Fco_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                               //Natural: IF #FCO-CONTRACT ( #X, #Y ) > ' '
                {
                    //*  PINE MOVE TAB
                    getReports().write(3, ReportOption.NOTITLE,pnd_Mailto_Pin,new TabSetting(14),pnd_Fco_Group_Pnd_Fco_Name.getValue(pnd_Counters_Pnd_X),new              //Natural: WRITE ( 3 ) #MAILTO-PIN 14T #FCO-NAME ( #X ) 63T #FCO-CONTRACT ( #X, #Y )
                        TabSetting(63),pnd_Fco_Group_Pnd_Fco_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Error_Work.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ERROR-WORK
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR46:                                                                                                                                                            //Natural: FOR #X = 1 TO #FCM-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Fcm_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            FOR47:                                                                                                                                                        //Natural: FOR #Y = 1 TO 20
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(20)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pnd_Fcm_Group_Pnd_Fcm_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                               //Natural: IF #FCM-CONTRACT ( #X, #Y ) > ' '
                {
                    //*  PINE MOVE TAB
                    getReports().write(3, ReportOption.NOTITLE,pnd_Mailto_Pin,new TabSetting(14),pnd_Fco_Group_Pnd_Fco_Name.getValue(pnd_Counters_Pnd_X),new              //Natural: WRITE ( 3 ) #MAILTO-PIN 14T #FCO-NAME ( #X ) 63T #FCO-CONTRACT ( #X, #Y )
                        TabSetting(63),pnd_Fco_Group_Pnd_Fco_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Error_Work.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ERROR-WORK
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Frs_Group_Pnd_Frs_Mail_Addr.getValue("*").notEquals(" ")))                                                                                      //Natural: IF #FRS-MAIL-ADDR ( * ) NE ' '
        {
            //*  PINE MOVE TAB
            getReports().write(3, ReportOption.NOTITLE,pnd_Mailto_Pin,new TabSetting(14),pnd_Frs_Group_Pnd_Frs_Name);                                                     //Natural: WRITE ( 3 ) #MAILTO-PIN 14T #FRS-NAME
            if (Global.isEscape()) return;
            pnd_Error_Work.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ERROR-WORK
        }                                                                                                                                                                 //Natural: END-IF
        FOR48:                                                                                                                                                            //Natural: FOR #X = 1 TO #VCO-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Vco_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            FOR49:                                                                                                                                                        //Natural: FOR #Y = 1 TO 20
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(20)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pnd_Vco_Group_Pnd_Vco_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                               //Natural: IF #VCO-CONTRACT ( #X, #Y ) > ' '
                {
                    //*  PINE MOVE TAB
                    getReports().write(3, ReportOption.NOTITLE,pnd_Mailto_Pin,new TabSetting(14),pnd_Vco_Group_Pnd_Vco_Name.getValue(pnd_Counters_Pnd_X),new              //Natural: WRITE ( 3 ) #MAILTO-PIN 14T #VCO-NAME ( #X ) 63T #VCO-CONTRACT ( #X, #Y )
                        TabSetting(63),pnd_Vco_Group_Pnd_Vco_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Error_Work.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ERROR-WORK
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR50:                                                                                                                                                            //Natural: FOR #X = 1 TO #VCM-IDX
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Vcm_Idx)); pnd_Counters_Pnd_X.nadd(1))
        {
            FOR51:                                                                                                                                                        //Natural: FOR #Y = 1 TO 20
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(20)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pnd_Vcm_Group_Pnd_Vcm_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y).greater(" ")))                                               //Natural: IF #VCM-CONTRACT ( #X, #Y ) > ' '
                {
                    //*  PINE MOVE TAB
                    getReports().write(3, ReportOption.NOTITLE,pnd_Mailto_Pin,new TabSetting(14),pnd_Vcm_Group_Pnd_Vcm_Name.getValue(pnd_Counters_Pnd_X),new              //Natural: WRITE ( 3 ) #MAILTO-PIN 14T #VCM-NAME ( #X ) 63T #VCM-CONTRACT ( #X, #Y )
                        TabSetting(63),pnd_Vcm_Group_Pnd_Vcm_Contract.getValue(pnd_Counters_Pnd_X,pnd_Counters_Pnd_Y));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Error_Work.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ERROR-WORK
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-ERROR-REPORT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(42),"NAME AND ADDRESS - CONFIRMATION LETTER REGISTER",new  //Natural: WRITE ( 1 ) NOTITLE 001T *PROGRAM 042T 'NAME AND ADDRESS - CONFIRMATION LETTER REGISTER' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 057T 'ERROR REPORT-POST' 120T *TIMX ( EM = HH:II:SS�AP ) /
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(57),"ERROR REPORT-POST",new TabSetting(120),Global.getTIMX(), new ReportEditMask 
                        ("HH:II:SS AP"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(42),"NAME AND ADDRESS - CONFIRMATION LETTER REGISTER",new  //Natural: WRITE ( 2 ) NOTITLE 001T *PROGRAM 042T 'NAME AND ADDRESS - CONFIRMATION LETTER REGISTER' 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 059T 'UPDATE REPORT' 120T *TIMX ( EM = HH:II:SS�AP ) /
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(59),"UPDATE REPORT",new TabSetting(120),Global.getTIMX(), new ReportEditMask ("HH:II:SS AP"),
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(42),"NAME AND ADDRESS - CONFIRMATION LETTER REGISTER",new  //Natural: WRITE ( 3 ) NOTITLE 001T *PROGRAM 042T 'NAME AND ADDRESS - CONFIRMATION LETTER REGISTER' 120T 'PAGE:' *PAGE-NUMBER ( 3 ) ( EM = ZZ,ZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 060T 'ERROR REPORT' 120T *TIMX ( EM = HH:II:SS�AP ) / 001T 'PIN' 030T 'NAME' 063T 'CONTRACT'
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(60),"ERROR REPORT",new TabSetting(120),Global.getTIMX(), new ReportEditMask ("HH:II:SS AP"),NEWLINE,new 
                        TabSetting(1),"PIN",new TabSetting(30),"NAME",new TabSetting(63),"CONTRACT");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Work_Letter_Image_File_Pnd_Compress_AddressIsBreak = pnd_Work_Letter_Image_File_Pnd_Compress_Address.isBreak(endOfData);
        if (condition(pnd_Work_Letter_Image_File_Pnd_Compress_AddressIsBreak))
        {
            if (condition(pnd_Close.getBoolean()))                                                                                                                        //Natural: IF #CLOSE
            {
                                                                                                                                                                          //Natural: PERFORM POST-CLOSE
                sub_Post_Close();
                if (condition(Global.isEscape())) {return;}
                pnd_Close.reset();                                                                                                                                        //Natural: RESET #CLOSE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Co_Cnt.reset();                                                                                                                                           //Natural: RESET #CO-CNT #CM-CNT #RS-CNT #FCO-CNT #FCM-CNT #FRS-CNT #VCO-CNT #VCM-CNT
            pnd_Cm_Cnt.reset();
            pnd_Rs_Cnt.reset();
            pnd_Fco_Cnt.reset();
            pnd_Fcm_Cnt.reset();
            pnd_Frs_Cnt.reset();
            pnd_Vco_Cnt.reset();
            pnd_Vcm_Cnt.reset();
            pnd_Co_Write.setValue(true);                                                                                                                                  //Natural: ASSIGN #CO-WRITE := #CM-WRITE := #RS-WRITE := #FCO-WRITE := #FCM-WRITE := #FRS-WRITE := #VCO-WRITE := #VCM-WRITE := TRUE
            pnd_Cm_Write.setValue(true);
            pnd_Rs_Write.setValue(true);
            pnd_Fco_Write.setValue(true);
            pnd_Fcm_Write.setValue(true);
            pnd_Frs_Write.setValue(true);
            pnd_Vco_Write.setValue(true);
            pnd_Vcm_Write.setValue(true);
            pnd_Co_Start.setValue(1);                                                                                                                                     //Natural: ASSIGN #CO-START := #CM-START := #RS-START := #FCO-START := #FCM-START := #FRS-START := #VCO-START := #VCM-START := 1
            pnd_Cm_Start.setValue(1);
            pnd_Rs_Start.setValue(1);
            pnd_Fco_Start.setValue(1);
            pnd_Fcm_Start.setValue(1);
            pnd_Frs_Start.setValue(1);
            pnd_Vco_Start.setValue(1);
            pnd_Vcm_Start.setValue(1);
                                                                                                                                                                          //Natural: PERFORM INITIALIZATION
            sub_Initialization();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 SG=OFF");
        Global.format(1, "PS=60 LS=133 SG=OFF");
        Global.format(2, "PS=60 LS=133 SG=OFF");
        Global.format(3, "PS=60 LS=133 SG=OFF");

        getReports().setDisplayColumns(0, ReportOption.NOTITLE,"PIN",
        		pnd_Work_Letter_Image_File_Pnd_O_Pin_No,"Cntrct",
        		pnd_Work_Letter_Image_File_Pnd_O_Contract_No,"Py",
        		pnd_Work_Letter_Image_File_Pnd_O_Payee_Cd, new ReportEditMask ("99"),"AA",
        		pnd_Work_Letter_Image_File_Pnd_O_Addr_Actvty_Cd,"Ac",
        		pnd_Work_Letter_Image_File_Pnd_O_Actvty_Cd,"Us",
        		pnd_Work_Letter_Image_File_Pnd_O_Usage,"Sc",
        		pnd_Work_Letter_Image_File_Pnd_O_C_Section,"Addr",
        		pnd_Work_Letter_Image_File_Pnd_Compress_Address,"Cntrct2",
        		pnd_Work_Letter_Image_File_Pnd_O_Contract2);
        getReports().setDisplayColumns(2, ReportOption.NOTITLE,"PIN",
        		pnd_Mailto_Pin,"NAME",
        		pnd_Mailto_M_Name,"CHANGE DATE",
        		pnd_Output_Date, new ReportEditMask ("MM/DD/YYYY"),"CHANGE TIME",
        		pnd_Output_Time, new ReportEditMask ("99:99:99:9"),new TabSetting(81),"RESIDENTIAL ADDRESS",
        		pnd_Work_Letter_Image_File_Pnd_Mit_Error,NEWLINE);
    }
}
