/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:59:48 PM
**        * FROM NATURAL PROGRAM : Nasb567
************************************************************
**        * FILE NAME            : Nasb567.java
**        * CLASS NAME           : Nasb567
**        * INSTANCE NAME        : Nasb567
************************************************************
***********************************************************************
** PROGRAM    : NASB567
** AUTHOR     : ROXANNE CARREON
** PURPOSE    : ENROLLMENT PROJECT. CREATE CWF REQUEST AND DOC
**              FOR PIN MERGE AND SSS CHANGE
** HISTORY    :
**
** GOPAZ  1/18/2012 CHANGE RECORD LENGTH OF INPUT FROM 80 TO 120
**                  READ COR BY SSN TO GET CORRECT PIN
** APRIL 2016 J BREMER ADD MQ OPEN/CLOSE AS PART OF COR/NAAD TO MDM
**                     REMEDIATION
** JUNE  2016 D MEADE  PIN EXPANSION - CALL MDMN191. REPLACE COR
**                     READ WITH MDMN101A CALL.
***********************************************************************
*

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nasb567 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaNasa8003 pdaNasa8003;
    private PdaMdma190 pdaMdma190;
    private PdaNasa191 pdaNasa191;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_W_Doc_Text;

    private DbsGroup pnd_W_Doc_Text__R_Field_1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Sys_Desc;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Title1;

    private DbsGroup pnd_W_Doc_Text__R_Field_2;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln3_Fill;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln3_Before;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln3_After;

    private DbsGroup pnd_W_Doc_Text__R_Field_3;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln3_Det_Title;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln3_Det_Old_Val;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln3_Det_New_Val;
    private DbsField pnd_Work_File1;

    private DbsGroup pnd_Work_File1__R_Field_4;
    private DbsField pnd_Work_File1_Pnd_Wk_Rec_Type;
    private DbsField pnd_Work_File1_Pnd_Wk_Pin;
    private DbsField pnd_Work_File1_Pnd_Wk_Contract;
    private DbsField pnd_Work_File1_Pnd_Wk_Payee_Code;
    private DbsField pnd_Work_File1_Pnd_Wk_From_Value;

    private DbsGroup pnd_Work_File1__R_Field_5;
    private DbsField pnd_Work_File1_Pnd_From_Ssn;

    private DbsGroup pnd_Work_File1__R_Field_6;
    private DbsField pnd_Work_File1_Pnd_From_Pin;

    private DbsGroup pnd_Work_File1__R_Field_7;
    private DbsField pnd_Work_File1_Pnd_From_Dflt_Enrl_Ind;
    private DbsField pnd_Work_File1_Pnd_Wk_To_Value;

    private DbsGroup pnd_Work_File1__R_Field_8;
    private DbsField pnd_Work_File1_Pnd_To_Ssn;

    private DbsGroup pnd_Work_File1__R_Field_9;
    private DbsField pnd_Work_File1_Pnd_To_Pin;

    private DbsGroup pnd_Work_File1__R_Field_10;
    private DbsField pnd_Work_File1_Pnd_To_Dflt_Enrl_Ind;
    private DbsField pnd_Work_File1_Pnd_Filler;
    private DbsField pnd_Work_File1_Pnd_Wk_Ssn;
    private DbsField pnd_Read;
    private DbsField pnd_Ssn;
    private DbsField pnd_Pin_Merge;
    private DbsField pnd_Ctr;
    private DbsField pnd_Read_B4_Restart;
    private DbsField pnd_Record_Count;

    private DbsGroup pnd_Restart_Info;
    private DbsField pnd_Restart_Info_Pnd_Restart_Txt;
    private DbsField pnd_Restart_Info_Pnd_Restart_Ctr;
    private DbsField pnd_Restart_Info_Pnd_Restart_Pin;
    private DbsField pnd_Restart_Info_Pnd_Restart_Cntrct;
    private DbsField pnd_Restart_Info_Pnd_Restart_Payee_Code;
    private DbsField pnd_Mdm_Pin;
    private DbsField pnd_Err_Msg;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaNasa8003 = new PdaNasa8003(localVariables);
        pdaMdma190 = new PdaMdma190(localVariables);
        pdaNasa191 = new PdaNasa191(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_W_Doc_Text = localVariables.newFieldInRecord("pnd_W_Doc_Text", "#W-DOC-TEXT", FieldType.STRING, 80);

        pnd_W_Doc_Text__R_Field_1 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_1", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Sys_Desc = pnd_W_Doc_Text__R_Field_1.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Sys_Desc", "#W-SYS-DESC", FieldType.STRING, 25);
        pnd_W_Doc_Text_Pnd_W_Ln1_Title1 = pnd_W_Doc_Text__R_Field_1.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Title1", "#W-LN1-TITLE1", FieldType.STRING, 
            33);

        pnd_W_Doc_Text__R_Field_2 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_2", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln3_Fill = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln3_Fill", "#W-LN3-FILL", FieldType.STRING, 15);
        pnd_W_Doc_Text_Pnd_W_Ln3_Before = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln3_Before", "#W-LN3-BEFORE", FieldType.STRING, 
            30);
        pnd_W_Doc_Text_Pnd_W_Ln3_After = pnd_W_Doc_Text__R_Field_2.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln3_After", "#W-LN3-AFTER", FieldType.STRING, 
            15);

        pnd_W_Doc_Text__R_Field_3 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_3", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln3_Det_Title = pnd_W_Doc_Text__R_Field_3.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln3_Det_Title", "#W-LN3-DET-TITLE", FieldType.STRING, 
            15);
        pnd_W_Doc_Text_Pnd_W_Ln3_Det_Old_Val = pnd_W_Doc_Text__R_Field_3.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln3_Det_Old_Val", "#W-LN3-DET-OLD-VAL", 
            FieldType.STRING, 30);
        pnd_W_Doc_Text_Pnd_W_Ln3_Det_New_Val = pnd_W_Doc_Text__R_Field_3.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln3_Det_New_Val", "#W-LN3-DET-NEW-VAL", 
            FieldType.STRING, 30);
        pnd_Work_File1 = localVariables.newFieldInRecord("pnd_Work_File1", "#WORK-FILE1", FieldType.STRING, 120);

        pnd_Work_File1__R_Field_4 = localVariables.newGroupInRecord("pnd_Work_File1__R_Field_4", "REDEFINE", pnd_Work_File1);
        pnd_Work_File1_Pnd_Wk_Rec_Type = pnd_Work_File1__R_Field_4.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Rec_Type", "#WK-REC-TYPE", FieldType.STRING, 
            3);
        pnd_Work_File1_Pnd_Wk_Pin = pnd_Work_File1__R_Field_4.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Pin", "#WK-PIN", FieldType.NUMERIC, 12);
        pnd_Work_File1_Pnd_Wk_Contract = pnd_Work_File1__R_Field_4.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Contract", "#WK-CONTRACT", FieldType.STRING, 
            10);
        pnd_Work_File1_Pnd_Wk_Payee_Code = pnd_Work_File1__R_Field_4.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Payee_Code", "#WK-PAYEE-CODE", FieldType.STRING, 
            2);
        pnd_Work_File1_Pnd_Wk_From_Value = pnd_Work_File1__R_Field_4.newFieldInGroup("pnd_Work_File1_Pnd_Wk_From_Value", "#WK-FROM-VALUE", FieldType.STRING, 
            29);

        pnd_Work_File1__R_Field_5 = pnd_Work_File1__R_Field_4.newGroupInGroup("pnd_Work_File1__R_Field_5", "REDEFINE", pnd_Work_File1_Pnd_Wk_From_Value);
        pnd_Work_File1_Pnd_From_Ssn = pnd_Work_File1__R_Field_5.newFieldInGroup("pnd_Work_File1_Pnd_From_Ssn", "#FROM-SSN", FieldType.STRING, 9);

        pnd_Work_File1__R_Field_6 = pnd_Work_File1__R_Field_4.newGroupInGroup("pnd_Work_File1__R_Field_6", "REDEFINE", pnd_Work_File1_Pnd_Wk_From_Value);
        pnd_Work_File1_Pnd_From_Pin = pnd_Work_File1__R_Field_6.newFieldInGroup("pnd_Work_File1_Pnd_From_Pin", "#FROM-PIN", FieldType.STRING, 12);

        pnd_Work_File1__R_Field_7 = pnd_Work_File1__R_Field_4.newGroupInGroup("pnd_Work_File1__R_Field_7", "REDEFINE", pnd_Work_File1_Pnd_Wk_From_Value);
        pnd_Work_File1_Pnd_From_Dflt_Enrl_Ind = pnd_Work_File1__R_Field_7.newFieldInGroup("pnd_Work_File1_Pnd_From_Dflt_Enrl_Ind", "#FROM-DFLT-ENRL-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Pnd_Wk_To_Value = pnd_Work_File1__R_Field_4.newFieldInGroup("pnd_Work_File1_Pnd_Wk_To_Value", "#WK-TO-VALUE", FieldType.STRING, 
            29);

        pnd_Work_File1__R_Field_8 = pnd_Work_File1__R_Field_4.newGroupInGroup("pnd_Work_File1__R_Field_8", "REDEFINE", pnd_Work_File1_Pnd_Wk_To_Value);
        pnd_Work_File1_Pnd_To_Ssn = pnd_Work_File1__R_Field_8.newFieldInGroup("pnd_Work_File1_Pnd_To_Ssn", "#TO-SSN", FieldType.STRING, 9);

        pnd_Work_File1__R_Field_9 = pnd_Work_File1__R_Field_4.newGroupInGroup("pnd_Work_File1__R_Field_9", "REDEFINE", pnd_Work_File1_Pnd_Wk_To_Value);
        pnd_Work_File1_Pnd_To_Pin = pnd_Work_File1__R_Field_9.newFieldInGroup("pnd_Work_File1_Pnd_To_Pin", "#TO-PIN", FieldType.STRING, 12);

        pnd_Work_File1__R_Field_10 = pnd_Work_File1__R_Field_4.newGroupInGroup("pnd_Work_File1__R_Field_10", "REDEFINE", pnd_Work_File1_Pnd_Wk_To_Value);
        pnd_Work_File1_Pnd_To_Dflt_Enrl_Ind = pnd_Work_File1__R_Field_10.newFieldInGroup("pnd_Work_File1_Pnd_To_Dflt_Enrl_Ind", "#TO-DFLT-ENRL-IND", FieldType.STRING, 
            1);
        pnd_Work_File1_Pnd_Filler = pnd_Work_File1__R_Field_4.newFieldInGroup("pnd_Work_File1_Pnd_Filler", "#FILLER", FieldType.STRING, 14);
        pnd_Work_File1_Pnd_Wk_Ssn = pnd_Work_File1__R_Field_4.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Ssn", "#WK-SSN", FieldType.NUMERIC, 9);
        pnd_Read = localVariables.newFieldInRecord("pnd_Read", "#READ", FieldType.NUMERIC, 9);
        pnd_Ssn = localVariables.newFieldInRecord("pnd_Ssn", "#SSN", FieldType.NUMERIC, 9);
        pnd_Pin_Merge = localVariables.newFieldInRecord("pnd_Pin_Merge", "#PIN-MERGE", FieldType.NUMERIC, 12);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 9);
        pnd_Read_B4_Restart = localVariables.newFieldInRecord("pnd_Read_B4_Restart", "#READ-B4-RESTART", FieldType.NUMERIC, 9);
        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.NUMERIC, 9);

        pnd_Restart_Info = localVariables.newGroupInRecord("pnd_Restart_Info", "#RESTART-INFO");
        pnd_Restart_Info_Pnd_Restart_Txt = pnd_Restart_Info.newFieldInGroup("pnd_Restart_Info_Pnd_Restart_Txt", "#RESTART-TXT", FieldType.STRING, 1);
        pnd_Restart_Info_Pnd_Restart_Ctr = pnd_Restart_Info.newFieldInGroup("pnd_Restart_Info_Pnd_Restart_Ctr", "#RESTART-CTR", FieldType.NUMERIC, 9);
        pnd_Restart_Info_Pnd_Restart_Pin = pnd_Restart_Info.newFieldInGroup("pnd_Restart_Info_Pnd_Restart_Pin", "#RESTART-PIN", FieldType.NUMERIC, 12);
        pnd_Restart_Info_Pnd_Restart_Cntrct = pnd_Restart_Info.newFieldInGroup("pnd_Restart_Info_Pnd_Restart_Cntrct", "#RESTART-CNTRCT", FieldType.STRING, 
            10);
        pnd_Restart_Info_Pnd_Restart_Payee_Code = pnd_Restart_Info.newFieldInGroup("pnd_Restart_Info_Pnd_Restart_Payee_Code", "#RESTART-PAYEE-CODE", FieldType.STRING, 
            2);
        pnd_Mdm_Pin = localVariables.newFieldInRecord("pnd_Mdm_Pin", "#MDM-PIN", FieldType.NUMERIC, 12);
        pnd_Err_Msg = localVariables.newFieldInRecord("pnd_Err_Msg", "#ERR-MSG", FieldType.STRING, 79);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Nasb567() throws Exception
    {
        super("Nasb567");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  MQ OPEN
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*                                                                                                                                                               //Natural: GET TRANSACTION DATA #RESTART-TXT #RESTART-CTR #RESTART-PIN #RESTART-CNTRCT #RESTART-PAYEE-CODE
        if (condition(pnd_Restart_Info_Pnd_Restart_Txt.notEquals(" ")))                                                                                                   //Natural: IF #RESTART-TXT NE ' '
        {
            getReports().write(0, ReportOption.NOTITLE,"---------------  RESTART ---------------");                                                                       //Natural: WRITE '---------------  RESTART ---------------'
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,new TabSetting(5),"RESTART DATA : ",NEWLINE,new TabSetting(5),"RESTART TXT         :",pnd_Restart_Info_Pnd_Restart_Txt,NEWLINE,new  //Natural: WRITE 05T 'RESTART DATA : ' / 5T 'RESTART TXT         :' #RESTART-TXT / 5T 'RESTART COUNT       :' #RESTART-CTR / 5T 'RESTART PIN         :' #RESTART-PIN / 5T 'RESTART CONTRACT #  :' #RESTART-CNTRCT / 5T 'RESTART PAYEE       :' #RESTART-PAYEE-CODE /
                TabSetting(5),"RESTART COUNT       :",pnd_Restart_Info_Pnd_Restart_Ctr,NEWLINE,new TabSetting(5),"RESTART PIN         :",pnd_Restart_Info_Pnd_Restart_Pin,NEWLINE,new 
                TabSetting(5),"RESTART CONTRACT #  :",pnd_Restart_Info_Pnd_Restart_Cntrct,NEWLINE,new TabSetting(5),"RESTART PAYEE       :",pnd_Restart_Info_Pnd_Restart_Payee_Code,
                NEWLINE);
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"------------ END OF RESTART INFO -------------");                                                                 //Natural: WRITE '------------ END OF RESTART INFO -------------'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-FILE1
        while (condition(getWorkFiles().read(1, pnd_Work_File1)))
        {
            if (condition(!(pnd_Work_File1_Pnd_Wk_Rec_Type.equals("3") || pnd_Work_File1_Pnd_Wk_Rec_Type.equals("4"))))                                                   //Natural: ACCEPT IF #WK-REC-TYPE = '3' OR = '4'
            {
                continue;
            }
            pnd_Read.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #READ
            if (condition(pnd_Restart_Info_Pnd_Restart_Txt.greater(" ")))                                                                                                 //Natural: IF #RESTART-TXT > ' '
            {
                pnd_Read_B4_Restart.nadd(1);                                                                                                                              //Natural: ADD 1 TO #READ-B4-RESTART
                if (condition(pnd_Read_B4_Restart.lessOrEqual(pnd_Restart_Info_Pnd_Restart_Ctr)))                                                                         //Natural: IF #READ-B4-RESTART LE #RESTART-CTR
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Restart_Info_Pnd_Restart_Txt.setValue(" ");                                                                                                           //Natural: ASSIGN #RESTART-TXT := ' '
                getReports().write(0, ReportOption.NOTITLE,"===================================","       First Pin to process",NEWLINE,"       --------------------",NEWLINE,new  //Natural: WRITE '===================================' '       First Pin to process' / '       --------------------' / 5T 'PIN         :' #WK-PIN / 5T 'CONTRACT #  :' #WK-CONTRACT / 5T 'PAYEE       :' #WK-PAYEE-CODE / 5T 'SSN         :' #WK-SSN
                    TabSetting(5),"PIN         :",pnd_Work_File1_Pnd_Wk_Pin,NEWLINE,new TabSetting(5),"CONTRACT #  :",pnd_Work_File1_Pnd_Wk_Contract,NEWLINE,new 
                    TabSetting(5),"PAYEE       :",pnd_Work_File1_Pnd_Wk_Payee_Code,NEWLINE,new TabSetting(5),"SSN         :",pnd_Work_File1_Pnd_Wk_Ssn);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"===================================");                                                                        //Natural: WRITE '==================================='
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  READ COR BY #WK-SSN TO GET PIN - REPLACED FOR PINE WITH MDMN101A
            //*  #KEY-SSN   := #WK-SSN
            //*  #KEY-RTYPE := 01
            //*  FIND COR WITH COR-SUPER-SSN-RCDTYPE = #COR-SUPER-SSN-RCDTYPE
            //*    IF NO RECORDS FOUND
            //*      ESCAPE BOTTOM
            //*    END-NOREC
            //*    #COR-PIN := PH-UNIQUE-ID-NBR
            pdaMdma101.getPnd_Mdma101().reset();                                                                                                                          //Natural: RESET #MDMA101
            pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn().setValue(pnd_Work_File1_Pnd_Wk_Ssn);                                                                                    //Natural: ASSIGN #I-SSN := #WK-SSN
            DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                      //Natural: CALLNAT 'MDMN101A' #MDMA101
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().notEquals("0000")))                                                                               //Natural: IF #O-RETURN-CODE NE '0000'
            {
                pnd_Err_Msg.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Text());                                                                                      //Natural: ASSIGN #ERR-MSG := #MDMA101.#O-RETURN-TEXT
                getReports().write(0, ReportOption.NOTITLE,"Failure of call to MDMN101A","RC",pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code(),NEWLINE,pnd_Err_Msg);         //Natural: WRITE NOTITLE 'Failure of call to MDMN101A' 'RC' #MDMA101.#O-RETURN-CODE / #ERR-MSG
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Mdm_Pin.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12());                                                                                          //Natural: ASSIGN #MDM-PIN := #MDMA101.#O-PIN-N12
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Wk_Rec_Type.equals("3")))                                                                                                    //Natural: IF #WK-REC-TYPE = '3'
            {
                pdaMdma190.getMdma190_Pnd_Wpid().setValue("TA MS");                                                                                                       //Natural: ASSIGN MDMA190.#WPID := 'TA MS'
                pnd_Ssn.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #SSN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaMdma190.getMdma190_Pnd_Wpid().setValue("TZ MPN");                                                                                                      //Natural: ASSIGN MDMA190.#WPID := 'TZ MPN'
                pnd_Pin_Merge.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #PIN-MERGE
            }                                                                                                                                                             //Natural: END-IF
            pdaMdma190.getMdma190_Pnd_Et_Ind().setValue("Y");                                                                                                             //Natural: ASSIGN MDMA190.#ET-IND := 'Y'
            pdaMdma190.getMdma190_Pnd_Wpid_System().setValue("MDM");                                                                                                      //Natural: ASSIGN MDMA190.#WPID-SYSTEM := 'MDM'
            pdaMdma190.getMdma190_Pnd_Gda_User_Id().setValue("MDMCWF");                                                                                                   //Natural: ASSIGN MDMA190.#GDA-USER-ID := 'MDMCWF'
            pdaMdma190.getMdma190_Pnd_Wpid_Pin().setValue(pnd_Mdm_Pin);                                                                                                   //Natural: ASSIGN MDMA190.#WPID-PIN := #MDM-PIN
            //*     GET UNIT
            pdaNasa8003.getPnd_Nasa8003().reset();                                                                                                                        //Natural: RESET #NASA8003
            pdaNasa8003.getPnd_Nasa8003_Pnd_Maintenance_Sw().setValue("Y");                                                                                               //Natural: ASSIGN #NASA8003.#MAINTENANCE-SW := 'Y'
            pdaNasa8003.getPnd_Nasa8003_Pnd_Action_Cde().setValue("U");                                                                                                   //Natural: ASSIGN #NASA8003.#ACTION-CDE := 'U'
            DbsUtil.callnat(Nasn8003.class , getCurrentProcessState(), pdaNasa8003.getPnd_Nasa8003());                                                                    //Natural: CALLNAT 'NASN8003' USING #NASA8003
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            pdaMdma190.getMdma190_Pnd_Wpid_Unit().setValue(pdaNasa8003.getPnd_Nasa8003_Pnd_Unit_Code_Return());                                                           //Natural: ASSIGN MDMA190.#WPID-UNIT := #NASA8003.#UNIT-CODE-RETURN
            pdaNasa191.getNasa191().reset();                                                                                                                              //Natural: RESET NASA191
            pdaNasa191.getNasa191_Pnd_Action().setValue("AN");                                                                                                            //Natural: ASSIGN NASA191.#ACTION := 'AN'
            pdaNasa191.getNasa191_Pnd_Doc_Class().setValue("NOT");                                                                                                        //Natural: ASSIGN NASA191.#DOC-CLASS := 'NOT'
            pdaNasa191.getNasa191_Pnd_Doc_Category().setValue("M");                                                                                                       //Natural: ASSIGN NASA191.#DOC-CATEGORY := 'M'
            pdaNasa191.getNasa191_Pnd_Doc_Direction().setValue("N");                                                                                                      //Natural: ASSIGN NASA191.#DOC-DIRECTION := 'N'
            pdaNasa191.getNasa191_Pnd_Doc_Format_Cde().setValue("T");                                                                                                     //Natural: ASSIGN NASA191.#DOC-FORMAT-CDE := 'T'
            pdaNasa191.getNasa191_Pnd_Doc_Retention_Cde().setValue("P");                                                                                                  //Natural: ASSIGN NASA191.#DOC-RETENTION-CDE := 'P'
            pdaNasa191.getNasa191_Pnd_Last_Page_Flag().setValue("Y");                                                                                                     //Natural: ASSIGN NASA191.#LAST-PAGE-FLAG := 'Y'
            pdaNasa191.getNasa191_Pnd_Batch_Nbr().setValue(1);                                                                                                            //Natural: ASSIGN NASA191.#BATCH-NBR := 1
                                                                                                                                                                          //Natural: PERFORM MOVE-TO-FREE-TEXT
            sub_Move_To_Free_Text();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            DbsUtil.callnat(Mdmn191.class , getCurrentProcessState(), pdaMdma190.getMdma190(), pdaNasa191.getNasa191());                                                  //Natural: CALLNAT 'MDMN191' USING MDMA190 NASA191
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaMdma190.getMdma190_Pnd_Error_Message().equals(" ")))                                                                                         //Natural: IF MDMA190.#ERROR-MESSAGE = ' '
            {
                pnd_Ctr.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #CTR
                if (condition(pnd_Ctr.greater(100)))                                                                                                                      //Natural: IF #CTR > 100
                {
                    pnd_Record_Count.nadd(pnd_Ctr);                                                                                                                       //Natural: ADD #CTR TO #RECORD-COUNT
                    pnd_Ctr.reset();                                                                                                                                      //Natural: RESET #CTR
                    //*  #WK-PIN
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION 'X' #RECORD-COUNT #MDM-PIN #WK-CONTRACT #WK-PAYEE-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  END-FIND
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION ' '
        //*  MQ CLOSE
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //*  -------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TO-FREE-TEXT
        getReports().write(0, ReportOption.NOTITLE,"TOTAL RECORDS READ ",pnd_Read, new ReportEditMask ("Z,ZZZ,Z99"));                                                     //Natural: WRITE 'TOTAL RECORDS READ ' #READ ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"TOTAL PIN MERGED   ",pnd_Pin_Merge, new ReportEditMask ("Z,ZZZ,Z99"));                                                //Natural: WRITE 'TOTAL PIN MERGED   ' #PIN-MERGE ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"TOTAL SSN CHANGE   ",pnd_Ssn, new ReportEditMask ("Z,ZZZ,Z99"));                                                      //Natural: WRITE 'TOTAL SSN CHANGE   ' #SSN ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
    }
    private void sub_Move_To_Free_Text() throws Exception                                                                                                                 //Natural: MOVE-TO-FREE-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------------------------------
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue("*").reset();                                                                                                       //Natural: RESET NASA191.#DOC-TEXT ( * )
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 8
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(8)); pnd_I.nadd(1))
        {
            pnd_W_Doc_Text.reset();                                                                                                                                       //Natural: RESET #W-DOC-TEXT
            //*  PINE (WAS COR)
            short decideConditionsMet420 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #I;//Natural: VALUE 1
            if (condition((pnd_I.equals(1))))
            {
                decideConditionsMet420++;
                pnd_W_Doc_Text_Pnd_W_Sys_Desc.setValue("MDM Details Change");                                                                                             //Natural: ASSIGN #W-SYS-DESC := 'MDM Details Change'
                pnd_W_Doc_Text_Pnd_W_Ln1_Title1.setValue("OMNI PLUS ENROLLMENT");                                                                                         //Natural: ASSIGN #W-LN1-TITLE1 := 'OMNI PLUS ENROLLMENT'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_I.equals(2))))
            {
                decideConditionsMet420++;
                ignore();
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_I.equals(3))))
            {
                decideConditionsMet420++;
                pnd_W_Doc_Text_Pnd_W_Ln3_Before.setValue("Before Image");                                                                                                 //Natural: ASSIGN #W-LN3-BEFORE := 'Before Image'
                pnd_W_Doc_Text_Pnd_W_Ln3_After.setValue("After Image");                                                                                                   //Natural: ASSIGN #W-LN3-AFTER := 'After Image'
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_I.equals(4))))
            {
                decideConditionsMet420++;
                pnd_W_Doc_Text_Pnd_W_Ln3_Before.setValue("===============================");                                                                              //Natural: ASSIGN #W-LN3-BEFORE := '==============================='
                pnd_W_Doc_Text_Pnd_W_Ln3_After.setValue("============================");                                                                                  //Natural: ASSIGN #W-LN3-AFTER := '============================'
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_I.equals(5))))
            {
                decideConditionsMet420++;
                ignore();
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_I.equals(6))))
            {
                decideConditionsMet420++;
                if (condition(pnd_Work_File1_Pnd_Wk_Rec_Type.equals("3")))                                                                                                //Natural: IF #WK-REC-TYPE = '3'
                {
                    pnd_W_Doc_Text_Pnd_W_Ln3_Det_Title.setValue("       SSS:");                                                                                           //Natural: ASSIGN #W-LN3-DET-TITLE := '       SSS:'
                    pnd_W_Doc_Text_Pnd_W_Ln3_Before.setValueEdited(pnd_Work_File1_Pnd_Wk_From_Value,new ReportEditMask("XXX-XX-XXXX"));                                   //Natural: MOVE EDITED #WK-FROM-VALUE ( EM = XXX-XX-XXXX ) TO #W-LN3-BEFORE
                    pnd_W_Doc_Text_Pnd_W_Ln3_After.setValueEdited(pnd_Work_File1_Pnd_Wk_To_Value,new ReportEditMask("XXX-XX-XXXX"));                                      //Natural: MOVE EDITED #WK-TO-VALUE ( EM = XXX-XX-XXXX ) TO #W-LN3-AFTER
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Doc_Text_Pnd_W_Ln3_Det_Title.setValue("       PIN:");                                                                                           //Natural: ASSIGN #W-LN3-DET-TITLE := '       PIN:'
                    pnd_W_Doc_Text_Pnd_W_Ln3_Before.setValue(pnd_Work_File1_Pnd_Wk_From_Value.getSubstring(1,7));                                                         //Natural: MOVE SUBSTR ( #WK-FROM-VALUE,1,7 ) TO #W-LN3-BEFORE
                    pnd_W_Doc_Text_Pnd_W_Ln3_After.setValue(pnd_Work_File1_Pnd_Wk_To_Value.getSubstring(1,7));                                                            //Natural: MOVE SUBSTR ( #WK-TO-VALUE,1,7 ) TO #W-LN3-AFTER
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet420 > 0))
            {
                pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(pnd_I).setValue(pnd_W_Doc_Text);                                                                            //Natural: ASSIGN NASA191.#DOC-TEXT ( #I ) := #W-DOC-TEXT
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //
}
