/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:59:52 PM
**        * FROM NATURAL PROGRAM : Nasb573
************************************************************
**        * FILE NAME            : Nasb573.java
**        * CLASS NAME           : Nasb573
**        * INSTANCE NAME        : Nasb573
************************************************************
***********************************************************************
* PROGRAM    : NASB573
* AUTHOR     : DON MEADE - ADAPTED FROM NASB572 FOR
*            : AUTOMATION OF NEW ADDRESS SETUPS FOR GROUP ANNUITY PLAN
* PURPOSE    : CREATE CWF REQUEST AND ELECTRONIC FOLDER DOCUMENT
*              WHEN NEEDED TO MANUALLY ADD ADDRESS AND BANK INFO.
*
* HISTORY    : WRITTEN MARCH, 2016
*            : DM 06/20/2016 - CHANGED MESSAGE FOR BYPASSED
*            : CASE FROM "Bypassed for Missing PIN" TO
*            : "Bypassed for Last Name/DOB Mismatch."
*            : DM 06/2017 - CALL MDMN191 CLONE OF NASN191 FOR
*            : PIN EXPANSION. USE 12-BYTE PIN THROUGHOUT.
*            : DM 08/2017 - CORRECT ASSIGNMENT OF PIN - SEE PINE2
***********************************************************************

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nasb573 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma190 pdaMdma190;
    private PdaNasa191 pdaNasa191;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_File1;

    private DbsGroup pnd_Work_File1__R_Field_1;
    private DbsField pnd_Work_File1_Pnd_Wk_Pin;

    private DbsGroup pnd_Work_File1__R_Field_2;
    private DbsField pnd_Work_File1_Pnd_Wk_Pin_7;
    private DbsField pnd_Work_File1_Pnd_Wk_Pin_Blanks;

    private DbsGroup pnd_Work_File1__R_Field_3;
    private DbsField pnd_Work_File1_Pnd_Wk_Pin_First_Blank;
    private DbsField pnd_Work_File1_Pnd_Wk_Cntrct_Nbr;
    private DbsField pnd_Work_File1_Pnd_Wk_Payee_Cde;
    private DbsField pnd_Work_File1_Pnd_Wk_Issue_Dte;
    private DbsField pnd_Work_File1_Pnd_Wk_Option_Cde;
    private DbsField pnd_Work_File1_Pnd_Wk_Last_Name;
    private DbsField pnd_Work_File1_Pnd_Wk_First_Name;
    private DbsField pnd_Work_File1_Pnd_Wk_Ssn;
    private DbsField pnd_Work_File1_Pnd_Wk_Dob;
    private DbsField pnd_Work_File1_Pnd_Wk_Sex;
    private DbsField pnd_Work_File1_Pnd_Wk_Bank_Name;
    private DbsField pnd_Work_File1_Pnd_Wk_Account_Type;
    private DbsField pnd_Work_File1_Pnd_Wk_Routing_Nbr;
    private DbsField pnd_Work_File1_Pnd_Wk_Bank_Acct_Nbr;
    private DbsField pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_1;
    private DbsField pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_2;
    private DbsField pnd_Work_File1_Pnd_Wk_Bank_Addr_City;
    private DbsField pnd_Work_File1_Pnd_Wk_Bank_Addr_State;
    private DbsField pnd_Work_File1_Pnd_Wk_Bank_Addr_Zip;
    private DbsField pnd_Work_File1_Pnd_Wk_Bank_Addr_Country;
    private DbsField pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_1;
    private DbsField pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_2;
    private DbsField pnd_Work_File1_Pnd_Wk_Corr_Addr_City;
    private DbsField pnd_Work_File1_Pnd_Wk_Corr_Addr_State;
    private DbsField pnd_Work_File1_Pnd_Wk_Corr_Addr_Zip;
    private DbsField pnd_Work_File1_Pnd_Wk_Corr_Addr_Country;
    private DbsField pnd_W_Doc_Text;

    private DbsGroup pnd_W_Doc_Text__R_Field_4;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Program;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Fill1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Title1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Fill2;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln1_Area1;

    private DbsGroup pnd_W_Doc_Text__R_Field_5;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Date;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Fill1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Title1;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Fill2;
    private DbsField pnd_W_Doc_Text_Pnd_W_Ln2_Time;
    private DbsField pnd_Ctr;
    private DbsField pnd_Read_B4_Restart;
    private DbsField pnd_Record_Count;

    private DbsGroup pnd_Restart_Info;
    private DbsField pnd_Restart_Info_Pnd_Restart_Txt;
    private DbsField pnd_Restart_Info_Pnd_Restart_Ctr;
    private DbsField pnd_Restart_Info_Pnd_Restart_Pin;
    private DbsField pnd_Date_A;
    private DbsField pnd_Read;
    private DbsField pnd_Update_Bypassed;
    private DbsField pnd_Update_Success;
    private DbsField pnd_Update_Error;
    private DbsField pnd_Line;
    private DbsField pnd_Name;
    private DbsField pnd_Mq_Response;

    private DbsGroup pnd_Mq_Response__R_Field_6;
    private DbsField pnd_Mq_Response_Pnd_Mq_Resp_Code;
    private DbsField pnd_Mq_Response_Pnd_Mq_Resp_Text;

    private DbsGroup pnd_Mq_Response__R_Field_7;
    private DbsField pnd_Mq_Response_Pnd_Mq_Resp_Text_1;
    private DbsField pnd_Mq_Response_Pnd_Mq_Resp_Text_2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma190 = new PdaMdma190(localVariables);
        pdaNasa191 = new PdaNasa191(localVariables);

        // Local Variables
        pnd_Work_File1 = localVariables.newFieldInRecord("pnd_Work_File1", "#WORK-FILE1", FieldType.STRING, 658);

        pnd_Work_File1__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_File1__R_Field_1", "REDEFINE", pnd_Work_File1);
        pnd_Work_File1_Pnd_Wk_Pin = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Pin", "#WK-PIN", FieldType.STRING, 12);

        pnd_Work_File1__R_Field_2 = pnd_Work_File1__R_Field_1.newGroupInGroup("pnd_Work_File1__R_Field_2", "REDEFINE", pnd_Work_File1_Pnd_Wk_Pin);
        pnd_Work_File1_Pnd_Wk_Pin_7 = pnd_Work_File1__R_Field_2.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Pin_7", "#WK-PIN-7", FieldType.STRING, 7);
        pnd_Work_File1_Pnd_Wk_Pin_Blanks = pnd_Work_File1__R_Field_2.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Pin_Blanks", "#WK-PIN-BLANKS", FieldType.STRING, 
            5);

        pnd_Work_File1__R_Field_3 = pnd_Work_File1__R_Field_2.newGroupInGroup("pnd_Work_File1__R_Field_3", "REDEFINE", pnd_Work_File1_Pnd_Wk_Pin_Blanks);
        pnd_Work_File1_Pnd_Wk_Pin_First_Blank = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Pin_First_Blank", "#WK-PIN-FIRST-BLANK", 
            FieldType.STRING, 1);
        pnd_Work_File1_Pnd_Wk_Cntrct_Nbr = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Cntrct_Nbr", "#WK-CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Work_File1_Pnd_Wk_Payee_Cde = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Payee_Cde", "#WK-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Work_File1_Pnd_Wk_Issue_Dte = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Issue_Dte", "#WK-ISSUE-DTE", FieldType.STRING, 
            10);
        pnd_Work_File1_Pnd_Wk_Option_Cde = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Option_Cde", "#WK-OPTION-CDE", FieldType.STRING, 
            2);
        pnd_Work_File1_Pnd_Wk_Last_Name = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Last_Name", "#WK-LAST-NAME", FieldType.STRING, 
            30);
        pnd_Work_File1_Pnd_Wk_First_Name = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_First_Name", "#WK-FIRST-NAME", FieldType.STRING, 
            30);
        pnd_Work_File1_Pnd_Wk_Ssn = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Ssn", "#WK-SSN", FieldType.NUMERIC, 9);
        pnd_Work_File1_Pnd_Wk_Dob = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Dob", "#WK-DOB", FieldType.NUMERIC, 10);
        pnd_Work_File1_Pnd_Wk_Sex = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Sex", "#WK-SEX", FieldType.STRING, 1);
        pnd_Work_File1_Pnd_Wk_Bank_Name = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Bank_Name", "#WK-BANK-NAME", FieldType.STRING, 
            30);
        pnd_Work_File1_Pnd_Wk_Account_Type = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Account_Type", "#WK-ACCOUNT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Pnd_Wk_Routing_Nbr = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Routing_Nbr", "#WK-ROUTING-NBR", FieldType.STRING, 
            9);
        pnd_Work_File1_Pnd_Wk_Bank_Acct_Nbr = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Bank_Acct_Nbr", "#WK-BANK-ACCT-NBR", FieldType.STRING, 
            30);
        pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_1 = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_1", "#WK-BANK-ADDR-LINE-1", 
            FieldType.STRING, 70);
        pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_2 = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_2", "#WK-BANK-ADDR-LINE-2", 
            FieldType.STRING, 70);
        pnd_Work_File1_Pnd_Wk_Bank_Addr_City = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Bank_Addr_City", "#WK-BANK-ADDR-CITY", 
            FieldType.STRING, 35);
        pnd_Work_File1_Pnd_Wk_Bank_Addr_State = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Bank_Addr_State", "#WK-BANK-ADDR-STATE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Pnd_Wk_Bank_Addr_Zip = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Bank_Addr_Zip", "#WK-BANK-ADDR-ZIP", FieldType.STRING, 
            10);
        pnd_Work_File1_Pnd_Wk_Bank_Addr_Country = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Bank_Addr_Country", "#WK-BANK-ADDR-COUNTRY", 
            FieldType.STRING, 50);
        pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_1 = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_1", "#WK-CORR-ADDR-LINE-1", 
            FieldType.STRING, 70);
        pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_2 = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_2", "#WK-CORR-ADDR-LINE-2", 
            FieldType.STRING, 70);
        pnd_Work_File1_Pnd_Wk_Corr_Addr_City = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Corr_Addr_City", "#WK-CORR-ADDR-CITY", 
            FieldType.STRING, 35);
        pnd_Work_File1_Pnd_Wk_Corr_Addr_State = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Corr_Addr_State", "#WK-CORR-ADDR-STATE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Pnd_Wk_Corr_Addr_Zip = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Corr_Addr_Zip", "#WK-CORR-ADDR-ZIP", FieldType.STRING, 
            10);
        pnd_Work_File1_Pnd_Wk_Corr_Addr_Country = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Wk_Corr_Addr_Country", "#WK-CORR-ADDR-COUNTRY", 
            FieldType.STRING, 50);
        pnd_W_Doc_Text = localVariables.newFieldInRecord("pnd_W_Doc_Text", "#W-DOC-TEXT", FieldType.STRING, 80);

        pnd_W_Doc_Text__R_Field_4 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_4", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln1_Program = pnd_W_Doc_Text__R_Field_4.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Program", "#W-LN1-PROGRAM", FieldType.STRING, 
            7);
        pnd_W_Doc_Text_Pnd_W_Ln1_Fill1 = pnd_W_Doc_Text__R_Field_4.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Fill1", "#W-LN1-FILL1", FieldType.STRING, 
            19);
        pnd_W_Doc_Text_Pnd_W_Ln1_Title1 = pnd_W_Doc_Text__R_Field_4.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Title1", "#W-LN1-TITLE1", FieldType.STRING, 
            23);
        pnd_W_Doc_Text_Pnd_W_Ln1_Fill2 = pnd_W_Doc_Text__R_Field_4.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Fill2", "#W-LN1-FILL2", FieldType.STRING, 
            19);
        pnd_W_Doc_Text_Pnd_W_Ln1_Area1 = pnd_W_Doc_Text__R_Field_4.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln1_Area1", "#W-LN1-AREA1", FieldType.STRING, 
            12);

        pnd_W_Doc_Text__R_Field_5 = localVariables.newGroupInRecord("pnd_W_Doc_Text__R_Field_5", "REDEFINE", pnd_W_Doc_Text);
        pnd_W_Doc_Text_Pnd_W_Ln2_Date = pnd_W_Doc_Text__R_Field_5.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Date", "#W-LN2-DATE", FieldType.STRING, 10);
        pnd_W_Doc_Text_Pnd_W_Ln2_Fill1 = pnd_W_Doc_Text__R_Field_5.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Fill1", "#W-LN2-FILL1", FieldType.STRING, 
            10);
        pnd_W_Doc_Text_Pnd_W_Ln2_Title1 = pnd_W_Doc_Text__R_Field_5.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Title1", "#W-LN2-TITLE1", FieldType.STRING, 
            40);
        pnd_W_Doc_Text_Pnd_W_Ln2_Fill2 = pnd_W_Doc_Text__R_Field_5.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Fill2", "#W-LN2-FILL2", FieldType.STRING, 
            8);
        pnd_W_Doc_Text_Pnd_W_Ln2_Time = pnd_W_Doc_Text__R_Field_5.newFieldInGroup("pnd_W_Doc_Text_Pnd_W_Ln2_Time", "#W-LN2-TIME", FieldType.STRING, 11);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 9);
        pnd_Read_B4_Restart = localVariables.newFieldInRecord("pnd_Read_B4_Restart", "#READ-B4-RESTART", FieldType.NUMERIC, 9);
        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.NUMERIC, 9);

        pnd_Restart_Info = localVariables.newGroupInRecord("pnd_Restart_Info", "#RESTART-INFO");
        pnd_Restart_Info_Pnd_Restart_Txt = pnd_Restart_Info.newFieldInGroup("pnd_Restart_Info_Pnd_Restart_Txt", "#RESTART-TXT", FieldType.STRING, 1);
        pnd_Restart_Info_Pnd_Restart_Ctr = pnd_Restart_Info.newFieldInGroup("pnd_Restart_Info_Pnd_Restart_Ctr", "#RESTART-CTR", FieldType.NUMERIC, 9);
        pnd_Restart_Info_Pnd_Restart_Pin = pnd_Restart_Info.newFieldInGroup("pnd_Restart_Info_Pnd_Restart_Pin", "#RESTART-PIN", FieldType.NUMERIC, 7);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 10);
        pnd_Read = localVariables.newFieldInRecord("pnd_Read", "#READ", FieldType.NUMERIC, 9);
        pnd_Update_Bypassed = localVariables.newFieldInRecord("pnd_Update_Bypassed", "#UPDATE-BYPASSED", FieldType.NUMERIC, 9);
        pnd_Update_Success = localVariables.newFieldInRecord("pnd_Update_Success", "#UPDATE-SUCCESS", FieldType.NUMERIC, 9);
        pnd_Update_Error = localVariables.newFieldInRecord("pnd_Update_Error", "#UPDATE-ERROR", FieldType.NUMERIC, 9);
        pnd_Line = localVariables.newFieldArrayInRecord("pnd_Line", "#LINE", FieldType.STRING, 70, new DbsArrayController(1, 4));
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 60);
        pnd_Mq_Response = localVariables.newFieldArrayInRecord("pnd_Mq_Response", "#MQ-RESPONSE", FieldType.STRING, 1, new DbsArrayController(1, 200));

        pnd_Mq_Response__R_Field_6 = localVariables.newGroupInRecord("pnd_Mq_Response__R_Field_6", "REDEFINE", pnd_Mq_Response);
        pnd_Mq_Response_Pnd_Mq_Resp_Code = pnd_Mq_Response__R_Field_6.newFieldInGroup("pnd_Mq_Response_Pnd_Mq_Resp_Code", "#MQ-RESP-CODE", FieldType.STRING, 
            4);
        pnd_Mq_Response_Pnd_Mq_Resp_Text = pnd_Mq_Response__R_Field_6.newFieldInGroup("pnd_Mq_Response_Pnd_Mq_Resp_Text", "#MQ-RESP-TEXT", FieldType.STRING, 
            100);

        pnd_Mq_Response__R_Field_7 = pnd_Mq_Response__R_Field_6.newGroupInGroup("pnd_Mq_Response__R_Field_7", "REDEFINE", pnd_Mq_Response_Pnd_Mq_Resp_Text);
        pnd_Mq_Response_Pnd_Mq_Resp_Text_1 = pnd_Mq_Response__R_Field_7.newFieldInGroup("pnd_Mq_Response_Pnd_Mq_Resp_Text_1", "#MQ-RESP-TEXT-1", FieldType.STRING, 
            80);
        pnd_Mq_Response_Pnd_Mq_Resp_Text_2 = pnd_Mq_Response__R_Field_7.newFieldInGroup("pnd_Mq_Response_Pnd_Mq_Resp_Text_2", "#MQ-RESP-TEXT-2", FieldType.STRING, 
            20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Nasb573() throws Exception
    {
        super("Nasb573");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *************************************************
        //*                                                                                                                                                               //Natural: FORMAT PS = 66 LS = 81;//Natural: FORMAT ( 1 ) PS = 30 LS = 132
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-AND-CHECK-RESTART
        sub_Initialize_And_Check_Restart();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MQ-OPEN
        sub_Mq_Open();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-FILE1
        while (condition(getWorkFiles().read(1, pnd_Work_File1)))
        {
            pnd_Read.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #READ
                                                                                                                                                                          //Natural: PERFORM DISPLAY-INPUT
            sub_Display_Input();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Restart_Info_Pnd_Restart_Txt.greater(" ")))                                                                                                 //Natural: IF #RESTART-TXT > ' '
            {
                pnd_Read_B4_Restart.nadd(1);                                                                                                                              //Natural: ADD 1 TO #READ-B4-RESTART
                if (condition(pnd_Read_B4_Restart.lessOrEqual(pnd_Restart_Info_Pnd_Restart_Ctr)))                                                                         //Natural: IF #READ-B4-RESTART LE #RESTART-CTR
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Restart_Info_Pnd_Restart_Txt.setValue(" ");                                                                                                           //Natural: ASSIGN #RESTART-TXT := ' '
                getReports().write(0, "===================================","       First Contract to process",NEWLINE,"       --------------------",NEWLINE,new          //Natural: WRITE '===================================' '       First Contract to process' / '       --------------------' / 5T 'Contract    :' #WK-CNTRCT-NBR
                    TabSetting(5),"Contract    :",pnd_Work_File1_Pnd_Wk_Cntrct_Nbr);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "===================================");                                                                                             //Natural: WRITE '==================================='
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Wk_Pin.equals("000000000000")))                                                                                              //Natural: IF #WK-PIN = '000000000000'
            {
                pnd_Update_Bypassed.nadd(1);                                                                                                                              //Natural: ADD 1 TO #UPDATE-BYPASSED
                getReports().write(0, "Bypassed for Last Name/DOB Mismatch");                                                                                             //Natural: WRITE 'Bypassed for Last Name/DOB Mismatch'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                sub_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  NEW ADDRESS
            }                                                                                                                                                             //Natural: END-IF
            pdaMdma190.getMdma190_Pnd_Wpid().setValue("TA HN");                                                                                                           //Natural: ASSIGN MDMA190.#WPID := 'TA HN'
            pdaMdma190.getMdma190_Pnd_Et_Ind().setValue("Y");                                                                                                             //Natural: ASSIGN MDMA190.#ET-IND := 'Y'
            pdaMdma190.getMdma190_Pnd_Wpid_System().setValue("MDM");                                                                                                      //Natural: ASSIGN MDMA190.#WPID-SYSTEM := 'MDM'
            pdaMdma190.getMdma190_Pnd_Gda_User_Id().setValue("MDMCWF");                                                                                                   //Natural: ASSIGN MDMA190.#GDA-USER-ID := 'MDMCWF'
            //*  PINE2
            DbsUtil.examine(new ExamineSource(pnd_Work_File1_Pnd_Wk_Pin), new ExamineSearch("H'00'"), new ExamineReplace(" "));                                           //Natural: EXAMINE #WK-PIN H'00' REPLACE WITH ' '
            //*  IF #WK-PIN-FIRST-BLANK =  H'00'
            //*     #WK-PIN-BLANKS := '     '
            //*  END-IF
            pdaMdma190.getMdma190_Pnd_Wpid_Pin().compute(new ComputeParameters(false, pdaMdma190.getMdma190_Pnd_Wpid_Pin()), pnd_Work_File1_Pnd_Wk_Pin.val());            //Natural: ASSIGN MDMA190.#WPID-PIN := VAL ( #WK-PIN )
            pdaMdma190.getMdma190_Pnd_Wpid_Status().setValue("1000");                                                                                                     //Natural: ASSIGN MDMA190.#WPID-STATUS := '1000'
            pdaMdma190.getMdma190_Pnd_Wpid_Unit().setValue("CDMSA I");                                                                                                    //Natural: ASSIGN MDMA190.#WPID-UNIT := 'CDMSA I'
            pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit().setValue("CDMSA I");                                                                                               //Natural: ASSIGN MDMA190.#WPID-NEXT-UNIT := 'CDMSA I'
            pdaNasa191.getNasa191().reset();                                                                                                                              //Natural: RESET NASA191
            pdaNasa191.getNasa191_Pnd_Action().setValue("AN");                                                                                                            //Natural: ASSIGN NASA191.#ACTION := 'AN'
            pdaNasa191.getNasa191_Pnd_Doc_Class().setValue("ADD");                                                                                                        //Natural: ASSIGN NASA191.#DOC-CLASS := 'ADD'
            pdaNasa191.getNasa191_Pnd_Doc_Category().setValue("M");                                                                                                       //Natural: ASSIGN NASA191.#DOC-CATEGORY := 'M'
            pdaNasa191.getNasa191_Pnd_Doc_Direction().setValue("N");                                                                                                      //Natural: ASSIGN NASA191.#DOC-DIRECTION := 'N'
            pdaNasa191.getNasa191_Pnd_Doc_Format_Cde().setValue("T");                                                                                                     //Natural: ASSIGN NASA191.#DOC-FORMAT-CDE := 'T'
            pdaNasa191.getNasa191_Pnd_Doc_Retention_Cde().setValue("P");                                                                                                  //Natural: ASSIGN NASA191.#DOC-RETENTION-CDE := 'P'
            pdaNasa191.getNasa191_Pnd_Last_Page_Flag().setValue("Y");                                                                                                     //Natural: ASSIGN NASA191.#LAST-PAGE-FLAG := 'Y'
            pdaNasa191.getNasa191_Pnd_Batch_Nbr().setValue(1);                                                                                                            //Natural: ASSIGN NASA191.#BATCH-NBR := 1
                                                                                                                                                                          //Natural: PERFORM FORMAT-TEXT-LINES
            sub_Format_Text_Lines();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            DbsUtil.callnat(Mdmn191.class , getCurrentProcessState(), pdaMdma190.getMdma190(), pdaNasa191.getNasa191());                                                  //Natural: CALLNAT 'MDMN191' USING MDMA190 NASA191
            if (condition(Global.isEscape())) return;
            if (condition(pdaMdma190.getMdma190_Pnd_Error_Message().equals(" ")))                                                                                         //Natural: IF MDMA190.#ERROR-MESSAGE = ' '
            {
                pnd_Ctr.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #CTR
                pnd_Update_Success.nadd(1);                                                                                                                               //Natural: ADD 1 TO #UPDATE-SUCCESS
                if (condition(pnd_Ctr.greater(100)))                                                                                                                      //Natural: IF #CTR > 100
                {
                    pnd_Record_Count.nadd(pnd_Ctr);                                                                                                                       //Natural: ADD #CTR TO #RECORD-COUNT
                    pnd_Ctr.reset();                                                                                                                                      //Natural: RESET #CTR
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION 'X' #RECORD-COUNT #WK-PIN
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Update_Error.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #UPDATE-ERROR
                getReports().write(0, "ERROR FROM NASA191 FOR PIN",pnd_Work_File1_Pnd_Wk_Pin,":",NEWLINE,pdaMdma190.getMdma190_Pnd_Error_Message());                      //Natural: WRITE 'ERROR FROM NASA191 FOR PIN' #WK-PIN ':' / MDMA190.#ERROR-MESSAGE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                sub_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM MQ-CLOSE
        sub_Mq_Close();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, "END OF JOB STATISTICS:",NEWLINE,NEWLINE,"Input Records Read:              ",pnd_Read,NEWLINE,"Restart Count:                   ",          //Natural: WRITE ( 1 ) 'END OF JOB STATISTICS:' / /'Input Records Read:              ' #READ /'Restart Count:                   ' #RESTART-CTR /'Bypassed - Last Name/DOB mismatch' #UPDATE-BYPASSED /'CWF Update Success:              ' #UPDATE-SUCCESS /'CWF Update Error:                ' #UPDATE-ERROR
            pnd_Restart_Info_Pnd_Restart_Ctr,NEWLINE,"Bypassed - Last Name/DOB mismatch",pnd_Update_Bypassed,NEWLINE,"CWF Update Success:              ",
            pnd_Update_Success,NEWLINE,"CWF Update Error:                ",pnd_Update_Error);
        if (Global.isEscape()) return;
        getReports().write(0, "END OF JOB STATISTICS:",NEWLINE,NEWLINE,"Input Records Read:              ",pnd_Read,NEWLINE,"Restart Count:                   ",          //Natural: WRITE 'END OF JOB STATISTICS:' / /'Input Records Read:              ' #READ /'Restart Count:                   ' #RESTART-CTR /'Bypassed - Last Name/DOB mismatch' #UPDATE-BYPASSED /'CWF Update Success:              ' #UPDATE-SUCCESS /'CWF Update Error:                ' #UPDATE-ERROR
            pnd_Restart_Info_Pnd_Restart_Ctr,NEWLINE,"Bypassed - Last Name/DOB mismatch",pnd_Update_Bypassed,NEWLINE,"CWF Update Success:              ",
            pnd_Update_Success,NEWLINE,"CWF Update Error:                ",pnd_Update_Error);
        if (Global.isEscape()) return;
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-AND-CHECK-RESTART
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-INPUT
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-TEXT-LINES
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR-REPORT
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MQ-OPEN
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MQ-CLOSE
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-CORR-ADDRESS
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-BANK-ADDRESS
    }
    private void sub_Initialize_And_Check_Restart() throws Exception                                                                                                      //Natural: INITIALIZE-AND-CHECK-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        pnd_Update_Success.reset();                                                                                                                                       //Natural: RESET #UPDATE-SUCCESS #UPDATE-BYPASSED #UPDATE-ERROR #READ
        pnd_Update_Bypassed.reset();
        pnd_Update_Error.reset();
        pnd_Read.reset();
        //*  ERROR REPORT HEADING
        pnd_Date_A.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                                     //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #DATE-A
        getReports().write(1, "NASB573",new ColumnSpacing(5),"Rejected New Address Setups for Group Annuity Plan",new ColumnSpacing(5),pnd_Date_A,NEWLINE,                //Natural: WRITE ( 1 ) 'NASB573' 5X 'Rejected New Address Setups for Group Annuity Plan' 5X #DATE-A //
            NEWLINE);
        if (Global.isEscape()) return;
        //*   RESTART CHECK
        //*                                                                                                                                                               //Natural: GET TRANSACTION DATA #RESTART-TXT #RESTART-CTR #RESTART-PIN
        if (condition(pnd_Restart_Info_Pnd_Restart_Txt.notEquals(" ")))                                                                                                   //Natural: IF #RESTART-TXT NE ' '
        {
            getReports().write(0, "---------------  RESTART ---------------");                                                                                            //Natural: WRITE '---------------  RESTART ---------------'
            if (Global.isEscape()) return;
            getReports().write(0, new TabSetting(5),"RESTART DATA : ",NEWLINE,new TabSetting(5),"RESTART TXT         :",pnd_Restart_Info_Pnd_Restart_Txt,NEWLINE,new      //Natural: WRITE 05T 'RESTART DATA : ' / 5T 'RESTART TXT         :' #RESTART-TXT / 5T 'RESTART COUNT       :' #RESTART-CTR / 5T 'RESTART PIN         :' #RESTART-PIN /
                TabSetting(5),"RESTART COUNT       :",pnd_Restart_Info_Pnd_Restart_Ctr,NEWLINE,new TabSetting(5),"RESTART PIN         :",pnd_Restart_Info_Pnd_Restart_Pin,
                NEWLINE);
            if (Global.isEscape()) return;
            getReports().write(0, "------------ END OF RESTART INFO -------------");                                                                                      //Natural: WRITE '------------ END OF RESTART INFO -------------'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Input() throws Exception                                                                                                                     //Natural: DISPLAY-INPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        getReports().write(0, "Input Work File record",pnd_Read, new NumericLength (6));                                                                                  //Natural: WRITE 'Input Work File record' #READ ( NL = 6 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Work_File1_Pnd_Wk_Pin,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Cntrct_Nbr,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Payee_Cde,NEWLINE,             //Natural: WRITE '=' #WK-PIN / '=' #WK-CNTRCT-NBR / '=' #WK-PAYEE-CDE / '=' #WK-ISSUE-DTE / '=' #WK-OPTION-CDE / '=' #WK-LAST-NAME / '=' #WK-FIRST-NAME / '=' #WK-SSN / '=' #WK-DOB / '=' #WK-SEX / '=' #WK-BANK-NAME / '=' #WK-ACCOUNT-TYPE / '=' #WK-ROUTING-NBR / '=' #WK-BANK-ACCT-NBR / '=' #WK-BANK-ADDR-LINE-1 / '=' #WK-BANK-ADDR-LINE-2 / '=' #WK-BANK-ADDR-CITY / '=' #WK-BANK-ADDR-STATE '=' #WK-BANK-ADDR-ZIP / '=' #WK-BANK-ADDR-COUNTRY / '=' #WK-CORR-ADDR-LINE-1 / '=' #WK-CORR-ADDR-LINE-2 / '=' #WK-CORR-ADDR-CITY / '=' #WK-CORR-ADDR-STATE / '=' #WK-CORR-ADDR-ZIP / '=' #WK-CORR-ADDR-COUNTRY /
            "=",pnd_Work_File1_Pnd_Wk_Issue_Dte,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Option_Cde,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Last_Name,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_First_Name,
            NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Ssn,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Dob,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Sex,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Bank_Name,
            NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Account_Type,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Routing_Nbr,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Bank_Acct_Nbr,
            NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_1,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_2,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Bank_Addr_City,
            NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Bank_Addr_State,"=",pnd_Work_File1_Pnd_Wk_Bank_Addr_Zip,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Bank_Addr_Country,
            NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_1,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_2,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Corr_Addr_City,
            NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Corr_Addr_State,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Corr_Addr_Zip,NEWLINE,"=",pnd_Work_File1_Pnd_Wk_Corr_Addr_Country,
            NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Format_Text_Lines() throws Exception                                                                                                                 //Natural: FORMAT-TEXT-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        pnd_W_Doc_Text.reset();                                                                                                                                           //Natural: RESET #W-DOC-TEXT NASA191.#DOC-TEXT ( * )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue("*").reset();
        pnd_W_Doc_Text_Pnd_W_Ln1_Program.setValue(Global.getPROGRAM());                                                                                                   //Natural: ASSIGN #W-LN1-PROGRAM := *PROGRAM
        pnd_W_Doc_Text_Pnd_W_Ln1_Title1.setValue("NAME AND ADDRESS SYSTEM");                                                                                              //Natural: ASSIGN #W-LN1-TITLE1 := 'NAME AND ADDRESS SYSTEM'
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(1).setValue(pnd_W_Doc_Text);                                                                                        //Natural: ASSIGN NASA191.#DOC-TEXT ( 1 ) := #W-DOC-TEXT
        pnd_W_Doc_Text.reset();                                                                                                                                           //Natural: RESET #W-DOC-TEXT
        pnd_W_Doc_Text_Pnd_W_Ln2_Title1.setValue("Group Annuities Add Request");                                                                                          //Natural: ASSIGN #W-LN2-TITLE1 := 'Group Annuities Add Request'
        pnd_W_Doc_Text_Pnd_W_Ln2_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS' 'AP"));                                                               //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS' 'AP ) TO #W-LN2-TIME
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(2).setValue(pnd_W_Doc_Text);                                                                                        //Natural: ASSIGN NASA191.#DOC-TEXT ( 2 ) := #W-DOC-TEXT
        pnd_W_Doc_Text.reset();                                                                                                                                           //Natural: RESET #W-DOC-TEXT
        pnd_Date_A.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                                     //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #DATE-A
        setValueToSubstring("on",pnd_W_Doc_Text,32,2);                                                                                                                    //Natural: MOVE 'on' TO SUBSTR ( #W-DOC-TEXT,32,2 )
        setValueToSubstring(pnd_Date_A,pnd_W_Doc_Text,35,10);                                                                                                             //Natural: MOVE #DATE-A TO SUBSTR ( #W-DOC-TEXT,35,10 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(3).setValue(pnd_W_Doc_Text);                                                                                        //Natural: MOVE #W-DOC-TEXT TO NASA191.#DOC-TEXT ( 3 )
        pnd_W_Doc_Text.setValue("The IA Payouts for Defined Benefits Accounts application requested the");                                                                //Natural: ASSIGN #W-DOC-TEXT := 'The IA Payouts for Defined Benefits Accounts application requested the'
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(5).setValue(pnd_W_Doc_Text);                                                                                        //Natural: ASSIGN NASA191.#DOC-TEXT ( 5 ) := #W-DOC-TEXT
        pnd_W_Doc_Text.setValue("addition of address and banking data on the Corporate Name and Address");                                                                //Natural: ASSIGN #W-DOC-TEXT := 'addition of address and banking data on the Corporate Name and Address'
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(6).setValue(pnd_W_Doc_Text);                                                                                        //Natural: ASSIGN NASA191.#DOC-TEXT ( 6 ) := #W-DOC-TEXT
        pnd_W_Doc_Text.setValue("System. ");                                                                                                                              //Natural: ASSIGN #W-DOC-TEXT := 'System. '
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(7).setValue(pnd_W_Doc_Text);                                                                                        //Natural: ASSIGN NASA191.#DOC-TEXT ( 7 ) := #W-DOC-TEXT
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(9).setValue(DbsUtil.compress("PIN:            ", pnd_Work_File1_Pnd_Wk_Pin));                                       //Natural: COMPRESS FULL 'PIN:            ' #WK-PIN INTO NASA191.#DOC-TEXT ( 9 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(10).setValue(DbsUtil.compress("SSN:            ", pnd_Work_File1_Pnd_Wk_Ssn));                                      //Natural: COMPRESS FULL 'SSN:            ' #WK-SSN INTO NASA191.#DOC-TEXT ( 10 )
        pnd_Name.setValue(DbsUtil.compress(pnd_Work_File1_Pnd_Wk_First_Name, pnd_Work_File1_Pnd_Wk_Last_Name));                                                           //Natural: COMPRESS #WK-FIRST-NAME #WK-LAST-NAME INTO #NAME
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(11).setValue(DbsUtil.compress("Name:           ", pnd_Name));                                                       //Natural: COMPRESS FULL 'Name:           ' #NAME INTO NASA191.#DOC-TEXT ( 11 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(12).setValue(DbsUtil.compress("DOB:            ", pnd_Work_File1_Pnd_Wk_Dob));                                      //Natural: COMPRESS FULL 'DOB:            ' #WK-DOB INTO NASA191.#DOC-TEXT ( 12 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(13).setValue(DbsUtil.compress("Sex:            ", pnd_Work_File1_Pnd_Wk_Sex));                                      //Natural: COMPRESS FULL 'Sex:            ' #WK-SEX INTO NASA191.#DOC-TEXT ( 13 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(15).setValue("Contract Info:");                                                                                     //Natural: ASSIGN NASA191.#DOC-TEXT ( 15 ) := 'Contract Info:'
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(16).setValue(DbsUtil.compress("Contract Number:", pnd_Work_File1_Pnd_Wk_Cntrct_Nbr));                               //Natural: COMPRESS FULL 'Contract Number:' #WK-CNTRCT-NBR INTO NASA191.#DOC-TEXT ( 16 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(17).setValue(DbsUtil.compress("Payee Code:     ", pnd_Work_File1_Pnd_Wk_Payee_Cde));                                //Natural: COMPRESS FULL 'Payee Code:     ' #WK-PAYEE-CDE INTO NASA191.#DOC-TEXT ( 17 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(18).setValue(DbsUtil.compress("Option Code:    ", pnd_Work_File1_Pnd_Wk_Option_Cde));                               //Natural: COMPRESS FULL 'Option Code:    ' #WK-OPTION-CDE INTO NASA191.#DOC-TEXT ( 18 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(19).setValue(DbsUtil.compress("Issue Date:     ", pnd_Work_File1_Pnd_Wk_Issue_Dte));                                //Natural: COMPRESS FULL 'Issue Date:     ' #WK-ISSUE-DTE INTO NASA191.#DOC-TEXT ( 19 )
                                                                                                                                                                          //Natural: PERFORM FORMAT-CORR-ADDRESS
        sub_Format_Corr_Address();
        if (condition(Global.isEscape())) {return;}
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(21).setValue("Participant Correspondence Address:");                                                                //Natural: ASSIGN NASA191.#DOC-TEXT ( 21 ) := 'Participant Correspondence Address:'
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(22).setValue(pnd_Line.getValue(1));                                                                                 //Natural: ASSIGN NASA191.#DOC-TEXT ( 22 ) := #LINE ( 1 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(23).setValue(pnd_Line.getValue(2));                                                                                 //Natural: ASSIGN NASA191.#DOC-TEXT ( 23 ) := #LINE ( 2 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(24).setValue(pnd_Line.getValue(3));                                                                                 //Natural: ASSIGN NASA191.#DOC-TEXT ( 24 ) := #LINE ( 3 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(25).setValue(pnd_Line.getValue(4));                                                                                 //Natural: ASSIGN NASA191.#DOC-TEXT ( 25 ) := #LINE ( 4 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(27).setValue("Bank Info:");                                                                                         //Natural: ASSIGN NASA191.#DOC-TEXT ( 27 ) := 'Bank Info:'
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(28).setValue(pnd_Work_File1_Pnd_Wk_Bank_Name);                                                                      //Natural: ASSIGN NASA191.#DOC-TEXT ( 28 ) := #WK-BANK-NAME
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(29).setValue(DbsUtil.compress("Account Number:", pnd_Work_File1_Pnd_Wk_Bank_Acct_Nbr));                             //Natural: COMPRESS 'Account Number:' #WK-BANK-ACCT-NBR INTO NASA191.#DOC-TEXT ( 29 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(30).setValue(DbsUtil.compress("Routing Number:", pnd_Work_File1_Pnd_Wk_Routing_Nbr));                               //Natural: COMPRESS 'Routing Number:' #WK-ROUTING-NBR INTO NASA191.#DOC-TEXT ( 30 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(31).setValue(DbsUtil.compress("Account Type:", pnd_Work_File1_Pnd_Wk_Account_Type));                                //Natural: COMPRESS 'Account Type:' #WK-ACCOUNT-TYPE INTO NASA191.#DOC-TEXT ( 31 )
                                                                                                                                                                          //Natural: PERFORM FORMAT-BANK-ADDRESS
        sub_Format_Bank_Address();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Work_File1_Pnd_Wk_Account_Type.equals(" ") || pnd_Work_File1_Pnd_Wk_Account_Type.equals("x")))                                                  //Natural: IF #WK-ACCOUNT-TYPE = ' ' OR = 'x'
        {
            pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(33).setValue("Check Mailing Address:");                                                                         //Natural: ASSIGN NASA191.#DOC-TEXT ( 33 ) := 'Check Mailing Address:'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(33).setValue("Bank Address:");                                                                                  //Natural: ASSIGN NASA191.#DOC-TEXT ( 33 ) := 'Bank Address:'
        }                                                                                                                                                                 //Natural: END-IF
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(34).setValue(pnd_Line.getValue(1));                                                                                 //Natural: ASSIGN NASA191.#DOC-TEXT ( 34 ) := #LINE ( 1 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(35).setValue(pnd_Line.getValue(2));                                                                                 //Natural: ASSIGN NASA191.#DOC-TEXT ( 35 ) := #LINE ( 2 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(36).setValue(pnd_Line.getValue(3));                                                                                 //Natural: ASSIGN NASA191.#DOC-TEXT ( 36 ) := #LINE ( 3 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(37).setValue(pnd_Line.getValue(4));                                                                                 //Natural: ASSIGN NASA191.#DOC-TEXT ( 37 ) := #LINE ( 4 )
        pnd_W_Doc_Text.reset();                                                                                                                                           //Natural: RESET #W-DOC-TEXT
        setValueToSubstring("The address change was requested on:",pnd_W_Doc_Text,1,36);                                                                                  //Natural: MOVE 'The address change was requested on:' TO SUBSTR ( #W-DOC-TEXT,01,36 )
        pnd_Date_A.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                                     //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #DATE-A
        setValueToSubstring(pnd_Date_A,pnd_W_Doc_Text,38,10);                                                                                                             //Natural: MOVE #DATE-A TO SUBSTR ( #W-DOC-TEXT,38,10 )
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(39).setValue(pnd_W_Doc_Text);                                                                                       //Natural: ASSIGN NASA191.#DOC-TEXT ( 39 ) := #W-DOC-TEXT
        pdaNasa191.getNasa191_Pnd_Doc_Text().getValue(40).setValue(DbsUtil.compress("Reason: GA PLANS CHECKMAIL ADDRESS ADD REQUEST"));                                   //Natural: COMPRESS 'Reason: GA PLANS CHECKMAIL ADDRESS ADD REQUEST' INTO NASA191.#DOC-TEXT ( 40 )
    }
    private void sub_Write_Error_Report() throws Exception                                                                                                                //Natural: WRITE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        if (condition(pnd_Work_File1_Pnd_Wk_Pin.equals("000000000000")))                                                                                                  //Natural: IF #WK-PIN = '000000000000'
        {
            getReports().write(1, "Bypassed for Last Name/DOB mismatch");                                                                                                 //Natural: WRITE ( 1 ) 'Bypassed for Last Name/DOB mismatch'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, "Error from NASA191:",pdaMdma190.getMdma190_Pnd_Error_Message());                                                                       //Natural: WRITE ( 1 ) 'Error from NASA191:' MDMA190.#ERROR-MESSAGE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, "PIN:           ",pnd_Work_File1_Pnd_Wk_Pin,NEWLINE,"Contract:      ",pnd_Work_File1_Pnd_Wk_Cntrct_Nbr,NEWLINE,"Payee Code:    ",           //Natural: WRITE ( 1 ) 'PIN:           ' #WK-PIN / 'Contract:      ' #WK-CNTRCT-NBR / 'Payee Code:    ' #WK-PAYEE-CDE / 'Issue Date:    ' #WK-ISSUE-DTE / 'Option Code:   ' #WK-OPTION-CDE / 'Last Name:     ' #WK-LAST-NAME / 'First Name:    ' #WK-FIRST-NAME / 'SSN:          ' #WK-SSN / 'Date of Birth:' #WK-DOB / 'Sex:           ' #WK-SEX / 'Bank Name:     ' #WK-BANK-NAME / 'Account Type:  ' #WK-ACCOUNT-TYPE / 'Routing Number:' #WK-ROUTING-NBR / 'Account Number:' #WK-BANK-ACCT-NBR / 'Bank or Check Mailing Address:' / '   ' #WK-BANK-ADDR-LINE-1 / '   ' #WK-BANK-ADDR-LINE-2 / '   ' #WK-BANK-ADDR-CITY / '   ' #WK-BANK-ADDR-STATE / '   ' #WK-BANK-ADDR-ZIP / '   ' #WK-BANK-ADDR-COUNTRY / 'Correspondence Address:' / '   ' #WK-CORR-ADDR-LINE-1 / '   ' #WK-CORR-ADDR-LINE-2 / '   ' #WK-CORR-ADDR-CITY / '   ' #WK-CORR-ADDR-STATE / '   ' #WK-CORR-ADDR-ZIP / '   ' #WK-CORR-ADDR-COUNTRY //
            pnd_Work_File1_Pnd_Wk_Payee_Cde,NEWLINE,"Issue Date:    ",pnd_Work_File1_Pnd_Wk_Issue_Dte,NEWLINE,"Option Code:   ",pnd_Work_File1_Pnd_Wk_Option_Cde,
            NEWLINE,"Last Name:     ",pnd_Work_File1_Pnd_Wk_Last_Name,NEWLINE,"First Name:    ",pnd_Work_File1_Pnd_Wk_First_Name,NEWLINE,"SSN:          ",
            pnd_Work_File1_Pnd_Wk_Ssn,NEWLINE,"Date of Birth:",pnd_Work_File1_Pnd_Wk_Dob,NEWLINE,"Sex:           ",pnd_Work_File1_Pnd_Wk_Sex,NEWLINE,"Bank Name:     ",
            pnd_Work_File1_Pnd_Wk_Bank_Name,NEWLINE,"Account Type:  ",pnd_Work_File1_Pnd_Wk_Account_Type,NEWLINE,"Routing Number:",pnd_Work_File1_Pnd_Wk_Routing_Nbr,
            NEWLINE,"Account Number:",pnd_Work_File1_Pnd_Wk_Bank_Acct_Nbr,NEWLINE,"Bank or Check Mailing Address:",NEWLINE,"   ",pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_1,
            NEWLINE,"   ",pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_2,NEWLINE,"   ",pnd_Work_File1_Pnd_Wk_Bank_Addr_City,NEWLINE,"   ",pnd_Work_File1_Pnd_Wk_Bank_Addr_State,
            NEWLINE,"   ",pnd_Work_File1_Pnd_Wk_Bank_Addr_Zip,NEWLINE,"   ",pnd_Work_File1_Pnd_Wk_Bank_Addr_Country,NEWLINE,"Correspondence Address:",NEWLINE,
            "   ",pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_1,NEWLINE,"   ",pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_2,NEWLINE,"   ",pnd_Work_File1_Pnd_Wk_Corr_Addr_City,
            NEWLINE,"   ",pnd_Work_File1_Pnd_Wk_Corr_Addr_State,NEWLINE,"   ",pnd_Work_File1_Pnd_Wk_Corr_Addr_Zip,NEWLINE,"   ",pnd_Work_File1_Pnd_Wk_Corr_Addr_Country,
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Mq_Open() throws Exception                                                                                                                           //Natural: MQ-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        pnd_Mq_Response.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue("*"));                                                    //Natural: ASSIGN #MQ-RESPONSE ( * ) := ##DATA-RESPONSE ( * )
        if (condition(pnd_Mq_Response_Pnd_Mq_Resp_Code.notEquals("0000")))                                                                                                //Natural: IF #MQ-RESP-CODE NE '0000'
        {
            getReports().write(0, "Response from MQ CONN/OPEN (MDMP0011):",pnd_Mq_Response_Pnd_Mq_Resp_Code);                                                             //Natural: WRITE 'Response from MQ CONN/OPEN (MDMP0011):' #MQ-RESP-CODE
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Mq_Response_Pnd_Mq_Resp_Text_1,NEWLINE,pnd_Mq_Response_Pnd_Mq_Resp_Text_2);                                                         //Natural: WRITE #MQ-RESP-TEXT-1 / #MQ-RESP-TEXT-2
            if (Global.isEscape()) return;
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mq_Close() throws Exception                                                                                                                          //Natural: MQ-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        pnd_Mq_Response.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue("*"));                                                    //Natural: ASSIGN #MQ-RESPONSE ( * ) := ##DATA-RESPONSE ( * )
        if (condition(pnd_Mq_Response_Pnd_Mq_Resp_Code.notEquals("0000")))                                                                                                //Natural: IF #MQ-RESP-CODE NE '0000'
        {
            getReports().write(0, "Response from MQ CLOSE (MDMP0012):",pnd_Mq_Response_Pnd_Mq_Resp_Code);                                                                 //Natural: WRITE 'Response from MQ CLOSE (MDMP0012):' #MQ-RESP-CODE
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Mq_Response_Pnd_Mq_Resp_Text_1,NEWLINE,pnd_Mq_Response_Pnd_Mq_Resp_Text_2);                                                         //Natural: WRITE #MQ-RESP-TEXT-1 / #MQ-RESP-TEXT-2
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Corr_Address() throws Exception                                                                                                               //Natural: FORMAT-CORR-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(0, "IN FORMAT-CORR-ADDRESS");                                                                                                                  //Natural: WRITE 'IN FORMAT-CORR-ADDRESS'
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_1), new ExamineSearch("H'00'"), new ExamineReplace(" "));                                  //Natural: EXAMINE #WK-CORR-ADDR-LINE-1 H'00' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_2), new ExamineSearch("H'00'"), new ExamineReplace(" "));                                  //Natural: EXAMINE #WK-CORR-ADDR-LINE-2 H'00' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Pnd_Wk_Corr_Addr_City), new ExamineSearch("H'00'"), new ExamineReplace(" "));                                    //Natural: EXAMINE #WK-CORR-ADDR-CITY H'00' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Pnd_Wk_Corr_Addr_State), new ExamineSearch("H'00'"), new ExamineReplace(" "));                                   //Natural: EXAMINE #WK-CORR-ADDR-STATE H'00' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Pnd_Wk_Corr_Addr_Zip), new ExamineSearch("H'00'"), new ExamineReplace(" "));                                     //Natural: EXAMINE #WK-CORR-ADDR-ZIP H'00' REPLACE WITH ' '
        pnd_Line.getValue(1).setValue(pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_1);                                                                                            //Natural: ASSIGN #LINE ( 1 ) := #WK-CORR-ADDR-LINE-1
        if (condition(pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_2.equals(" ")))                                                                                                //Natural: IF #WK-CORR-ADDR-LINE-2 = ' '
        {
            getReports().write(0, "LINE 2 IS HEX 00");                                                                                                                    //Natural: WRITE 'LINE 2 IS HEX 00'
            if (Global.isEscape()) return;
            pnd_Line.getValue(2).setValue(DbsUtil.compress(pnd_Work_File1_Pnd_Wk_Corr_Addr_City, pnd_Work_File1_Pnd_Wk_Corr_Addr_State, pnd_Work_File1_Pnd_Wk_Corr_Addr_Zip)); //Natural: COMPRESS #WK-CORR-ADDR-CITY #WK-CORR-ADDR-STATE #WK-CORR-ADDR-ZIP INTO #LINE ( 2 )
            pnd_Line.getValue(3).setValue(pnd_Work_File1_Pnd_Wk_Corr_Addr_Country);                                                                                       //Natural: ASSIGN #LINE ( 3 ) := #WK-CORR-ADDR-COUNTRY
            pnd_Line.getValue(4).setValue(" ");                                                                                                                           //Natural: ASSIGN #LINE ( 4 ) := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Line.getValue(2).setValue(pnd_Work_File1_Pnd_Wk_Corr_Addr_Line_2);                                                                                        //Natural: ASSIGN #LINE ( 2 ) := #WK-CORR-ADDR-LINE-2
            pnd_Line.getValue(3).setValue(DbsUtil.compress(pnd_Work_File1_Pnd_Wk_Corr_Addr_City, pnd_Work_File1_Pnd_Wk_Corr_Addr_State, pnd_Work_File1_Pnd_Wk_Corr_Addr_Zip)); //Natural: COMPRESS #WK-CORR-ADDR-CITY #WK-CORR-ADDR-STATE #WK-CORR-ADDR-ZIP INTO #LINE ( 3 )
            pnd_Line.getValue(4).setValue(pnd_Work_File1_Pnd_Wk_Corr_Addr_Country);                                                                                       //Natural: ASSIGN #LINE ( 4 ) := #WK-CORR-ADDR-COUNTRY
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Bank_Address() throws Exception                                                                                                               //Natural: FORMAT-BANK-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_1), new ExamineSearch("H'00'"), new ExamineReplace(" "));                                  //Natural: EXAMINE #WK-BANK-ADDR-LINE-1 H'00' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_2), new ExamineSearch("H'00'"), new ExamineReplace(" "));                                  //Natural: EXAMINE #WK-BANK-ADDR-LINE-2 H'00' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Pnd_Wk_Bank_Addr_City), new ExamineSearch("H'00'"), new ExamineReplace(" "));                                    //Natural: EXAMINE #WK-BANK-ADDR-CITY H'00' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Pnd_Wk_Bank_Addr_State), new ExamineSearch("H'00'"), new ExamineReplace(" "));                                   //Natural: EXAMINE #WK-BANK-ADDR-STATE H'00' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Pnd_Wk_Bank_Addr_Zip), new ExamineSearch("H'00'"), new ExamineReplace(" "));                                     //Natural: EXAMINE #WK-BANK-ADDR-ZIP H'00' REPLACE WITH ' '
        pnd_Line.getValue(1).setValue(pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_1);                                                                                            //Natural: ASSIGN #LINE ( 1 ) := #WK-BANK-ADDR-LINE-1
        if (condition(pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_2.equals(" ")))                                                                                                //Natural: IF #WK-BANK-ADDR-LINE-2 = ' '
        {
            pnd_Line.getValue(2).setValue(DbsUtil.compress(pnd_Work_File1_Pnd_Wk_Bank_Addr_City, pnd_Work_File1_Pnd_Wk_Bank_Addr_State, pnd_Work_File1_Pnd_Wk_Bank_Addr_Zip)); //Natural: COMPRESS #WK-BANK-ADDR-CITY #WK-BANK-ADDR-STATE #WK-BANK-ADDR-ZIP INTO #LINE ( 2 )
            pnd_Line.getValue(3).setValue(pnd_Work_File1_Pnd_Wk_Bank_Addr_Country);                                                                                       //Natural: ASSIGN #LINE ( 3 ) := #WK-BANK-ADDR-COUNTRY
            pnd_Line.getValue(4).setValue(" ");                                                                                                                           //Natural: ASSIGN #LINE ( 4 ) := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Line.getValue(2).setValue(pnd_Work_File1_Pnd_Wk_Bank_Addr_Line_2);                                                                                        //Natural: ASSIGN #LINE ( 2 ) := #WK-BANK-ADDR-LINE-2
            pnd_Line.getValue(3).setValue(DbsUtil.compress(pnd_Work_File1_Pnd_Wk_Bank_Addr_City, pnd_Work_File1_Pnd_Wk_Bank_Addr_State, pnd_Work_File1_Pnd_Wk_Bank_Addr_Zip)); //Natural: COMPRESS #WK-BANK-ADDR-CITY #WK-BANK-ADDR-STATE #WK-BANK-ADDR-ZIP INTO #LINE ( 3 )
            pnd_Line.getValue(4).setValue(pnd_Work_File1_Pnd_Wk_Bank_Addr_Country);                                                                                       //Natural: ASSIGN #LINE ( 4 ) := #WK-BANK-ADDR-COUNTRY
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=66 LS=81");
        Global.format(1, "PS=30 LS=132");
    }
}
