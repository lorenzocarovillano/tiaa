<?xml version="1.0" encoding="UTF-8"?>
<job id="PNI1170D" xmlns="http://xmlns.jcp.org/xml/ns/javaee"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
     http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/jobXML_1_0.xsd"
     version="1.0">
	<!-- JSR-352 XML converted for JCL PROC PNI1170D by ModernSystems -->
    <properties>
       <property name="DBID" value="003"/>
       <property name="HLQ1" value="PPM.ANN"/>
       <property name="HLQ6" value="PNA.ANN"/>
       <property name="HLQ8" value="PPDD.PNI1170D"/>
       <property name="JCLO" value="*"/>
       <property name="LIB1" value="PROD"/>
       <property name="REGN1" value="9M"/>
       <property name="NAT" value="NAT003P"/>
       <property name="NATMEM" value="PANNSEC"/>
       <property name="GDG1" value="+1"/>
       <property name="SPC1" value="5,5"/>
       <property name="SPC2" value="10,5"/>
       <property name="INCLUDE" value="PSTPINCL"/>
       <property name="CNTL1" value="UP076SEC"/>
       <property name="ORALOG" value="ORALOGON.NY.PARMLIB"/>
       <property name="EMBDY" value="PPDD.PNI1170D.ACIS.LEGAL.RLCREF.EMAIL"/>
       <property name="jcl:originalName" value="PNI1170D"/>
    </properties>
    <listeners><listener ref="procListener"/></listeners>
       <!--
    
    //*====================================================================*
    //*                         NARRATIVE                                  *
    //*====================================================================*
    //*                 DIALOGUE LEGAL CONTRACT FEED                       *
    //*====================================================================*
    //* THIS JOB READS THE DIALOGUE EXTRACT FILE FROM P1110NID AND THE     *
    //* ROUTING AND SHORT NAME RULES TO PRODUCE A FILE WHICH IS SENT TO    *
    //* ETL FOR LEGAL PACKAGES.                                            *
    //*....................................................................*
    //* MODIFICATION HISTORY                                               *
    //*....................................................................*
    //* DEVELBISS  2/19/2010 CHANGE INPUT DATA SET FROM                    *
    //*                      &HLQ6..ACIS.EXTRACT.S250.DIAG TO              *
    //*                      &HLQ6..ACIS.EXTRACT.S250                      *
    //* SCHNEIDER  7/28/2011 TRANSFER-IN CREDIT:                           *
    //*                    - SORT020.SORTOUT LRECL CHG TO 3500             *
    //* LAEVEY     7/14/2013 ADDED STEPS FRMT054 AND SORT055 TO INCLUDE    *
    //*                      BUNDLE INFORMATION AS PART OF IRA SUBSTITUTION*
    //* JENABI     8/13/2020 1.ADDED STEPS SORT021,VERY022,SORT023,SORT024,*
    //*                      PARM025 AND MAIL026 TO BYPASS THE RL CREF ONLY*
    //*                      CONTRACTS AND SEND OUT AN EMAIL.              *
    //*                      2.UPDATE THE STEP NAME FROM FRMT025 TO FRMT027*
    //*====================================================================*
    //* ALOC005: PRE-ALLOCATE OUTPUT FILES                                 *
    //*====================================================================*
    //ALOC005  EXEC PGM=IEFBR14,REGION=8M
    //ALOCLGL  DD DSN=&HLQ6..ACIS.LEGAL.DIALOG.DATA,
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
    //             DCB=(RECFM=VB,BLKSIZE=23476),
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
    --> 
    <step id="aloc005">
        <properties>
            <property name="STEP_NAME" value="ALOC005"/>
            <property name="EXEC" value="IEFBR14"/>
               <property name="REGION" value="8M"/>
            <property name="ALOCLGL" value="dsn://${HLQ6}.ACIS.LEGAL.DIALOG.DATA?DISP=(NEW,CATLG,DELETE);DCB=(RECFM=VB)"/>
        </properties>
        <batchlet ref="Dummy"/>
        <next on="*" to="very010"/>
    </step>
       <!--
    
    //*====================================================================*
    //* VERY010: CHECK FOR EMPTY FILE                                      *
    //*====================================================================*
    //VERY010  EXEC PGM=IDCAMS
    //SYSPRINT DD DUMMY
    //DDIN     DD DSN=&HLQ6..ACIS.EXTRACT.S250,DISP=SHR
    //SYSIN    DD  DSN=&LIB1..PARMLIB(NI1170D1),DISP=SHR
    --> 
    <step id="very010">
        <properties>
            <property name="STEP_NAME" value="VERY010"/>
            <property name="EXEC" value="IDCAMS"/>
            <property name="SYSPRINT" value="dummy://?DUMMY"/>
            <property name="DDIN" value="dsn://${HLQ6}.ACIS.EXTRACT.S250?DISP=SHR"/>
            <property name="SYSIN" value="dsn://${LIB1}.PARMLIB(NI1170D1)?DISP=SHR"/>
        </properties>
        <batchlet ref="IdcamsBatchlet"/>
        <next on="*" to="sort020"/>
    </step>
       <!--
    
    //*====================================================================*
    //*
    //*====================================================================*
    //* SORT020: SORT SUM TO ELIMINATE DUPLICATE CONTRACTS                 *
    //*====================================================================*
    //SORT020  EXEC PGM=SORT,COND=(0,NE)
    //SORTIN   DD DSN=&HLQ6..ACIS.EXTRACT.S250,
    //             DISP=SHR
    //SORTOUT  DD DSN=&HLQ8..LGLEXTR.DIAG.SORT,
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
    //             DCB=RECFM=FB,
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
    //SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
    //SYSABOUT DD  SYSOUT=*
    //SYSOUT   DD  SYSOUT=*
    //SYSUDUMP DD  SYSOUT=U
    //SYSIN    DD  DSN=&LIB1..PARMLIB(NI1170D2),DISP=SHR
    --> 
    <step id="sort020">
        <properties>
            <property name="STEP_NAME" value="SORT020"/>
            <property name="EXEC" value="SORT"/>
               <property name="COND" value="COND=(0,NE)"/>
            <property name="SORTIN" value="dsn://${HLQ6}.ACIS.EXTRACT.S250?DISP=SHR"/>
            <property name="SORTOUT" value="dsn://${HLQ8}.LGLEXTR.DIAG.SORT?DISP=(NEW,CATLG,DELETE);DCB=RECFM=FB"/>
            <property name="SORTWK01" value="temp://?"/>
            <property name="SYSABOUT" value="sysout://SYSABOUT?class=*"/>
            <property name="SYSOUT" value="sysout://SYSOUT?class=*"/>
            <property name="SYSUDUMP" value="sysout://SYSUDUMP?class=U"/>
            <property name="SYSIN" value="dsn://${LIB1}.PARMLIB(NI1170D2)?DISP=SHR"/>
        </properties>
        <batchlet ref="SortBatchlet"/>
        <next on="*" to="sort021"/>
    </step>
       <!--
    
    //*====================================================================*
    //* SORT021: SORT OUT RL CREF ONLY CONTRACTS HAVING INVALID ISSUE DATE *
    //*====================================================================*
    //SORT021  EXEC PGM=SORT,COND=(0,NE)                                    
    //*
    //SORTIN   DD  DSN=&HLQ8..LGLEXTR.DIAG.SORT,DISP=SHR                    
    //*
    //SORTOF01 DD DSN=&HLQ8..LGLEXTR.DIAG.SORT.FINAL,                       
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
    //             DCB=RECFM=FB,
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
    //*
    //SORTOF02 DD DSN=&HLQ8..LGLEXTR.RLCREF,                                
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
    //             DCB=RECFM=FB,
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
    //*
    //SYSOUT   DD  SYSOUT=*                                                 
    //SYSUDUMP DD  SYSOUT=U                                                 
    //SYSPRINT DD  SYSOUT=*                                                 
    //SYSIN    DD  DSN=&LIB1..PARMLIB(NI1170DA),DISP=SHR                    
    --> 
    <step id="sort021">
        <properties>
            <property name="STEP_NAME" value="SORT021"/>
            <property name="EXEC" value="SORT"/>
               <property name="COND" value="COND=(0,NE)"/>
            <property name="SORTIN" value="dsn://${HLQ8}.LGLEXTR.DIAG.SORT?DISP=SHR"/>
            <property name="SORTOF01" value="dsn://${HLQ8}.LGLEXTR.DIAG.SORT.FINAL?DISP=(NEW,CATLG,DELETE);DCB=RECFM=FB"/>
            <property name="SORTOF02" value="dsn://${HLQ8}.LGLEXTR.RLCREF?DISP=(NEW,CATLG,DELETE);DCB=RECFM=FB"/>
            <property name="SYSOUT" value="sysout://SYSOUT?class=*"/>
            <property name="SYSUDUMP" value="sysout://SYSUDUMP?class=U"/>
            <property name="SYSPRINT" value="sysout://SYSPRINT?class=*"/>
            <property name="SYSIN" value="dsn://${LIB1}.PARMLIB(NI1170DA)?DISP=SHR"/>
        </properties>
        <batchlet ref="SortBatchlet"/>
        <next on="*" to="very022"/>
    </step>
       <!--
    
    //*====================================================================*
    //* VERY022: VERIFY FOR FILE CONTENTS IN RL CREF ONLY FILE             *
    //*====================================================================*
    //VERY022  EXEC  PGM=IDCAMS,COND=(0,NE)
    //SYSPRINT DD  SYSOUT=&JCLO
    //DDIN     DD  DSN=&HLQ8..LGLEXTR.RLCREF,DISP=SHR
    //SYSIN    DD  DSN=&LIB1..PARMLIB(NI1170D1),DISP=SHR
    --> 
    <step id="very022">
        <properties>
            <property name="STEP_NAME" value="VERY022"/>
            <property name="EXEC" value="IDCAMS"/>
               <property name="COND" value="COND=(0,NE)"/>
            <property name="SYSPRINT" value="sysout://SYSPRINT?class=${JCLO}"/>
            <property name="DDIN" value="dsn://${HLQ8}.LGLEXTR.RLCREF?DISP=SHR"/>
            <property name="SYSIN" value="dsn://${LIB1}.PARMLIB(NI1170D1)?DISP=SHR"/>
        </properties>
        <batchlet ref="IdcamsBatchlet"/>
        <next on="*" to="if_0"/>
    </step>
       <!--
    
    //*
    // IF (VERY022.RC EQ 0) THEN
    //*====================================================================*
    //* SORT023: FORMAT THE RL CREF ONLY CONTRACTS                         *
    //*====================================================================*
    //SORT023  EXEC PGM=SORT,COND=(0,NE)                                    
    //*
    //SORTIN   DD  DSN=&HLQ8..LGLEXTR.RLCREF,DISP=SHR                       
    //*
    //SORTOUT  DD DSN=&HLQ8..LGLEXTR.RLCREF.CONTRCT,                        
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,               
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE),
    //             DCB=(LRECL=80,RECFM=FB,BLKSIZE=0)
    //*
    //SYSOUT   DD  SYSOUT=*                                                 
    //SYSUDUMP DD  SYSOUT=U                                                 
    //SYSPRINT DD  SYSOUT=*                                                 
    //SYSIN    DD  DSN=&LIB1..PARMLIB(NI1170DB),DISP=SHR
    //*====================================================================*
    //* SORT024: PREPARE THE EMAIL BODY FOR RL CREF ONLY CONTRACTS         *
    //*====================================================================*
    //SORT024  EXEC PGM=SORT,COND=(0,NE)                                    
    //*
    //SORTIN   DD  DSN=&HLQ1..ACIS.LEGAL.RLCREF.EMLBDY,DISP=SHR             
    //         DD  DSN=&HLQ8..LGLEXTR.RLCREF.CONTRCT,DISP=SHR               
    //*
    //SORTOUT  DD  DSN=&HLQ8..ACIS.LEGAL.RLCREF.EMAIL,                      
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,               
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE),
    //             DCB=*.SORTIN
    //*
    //SYSOUT   DD  SYSOUT=*                                                 
    //SYSUDUMP DD  SYSOUT=U                                                 
    //SYSPRINT DD  SYSOUT=*                                                 
    //SYSIN    DD  DSN=&LIB1..PARMLIB(NI1170DC),DISP=SHR                    
    //*====================================================================*
    //* PARM025: PREPARE THE EMAIL BODY FOR RL CREF ONLY CONTRACTS         *
    //*====================================================================*
    //PARM025 EXEC PGM=IRXJCL,
    //       PARM='PARMSUB EMBDY=&EMBDY',COND=(0,NE)
    //TEMPLATE DD DSN=&LIB1..PARMLIB(NI1170DD),DISP=SHR
    //SUBSTED DD DSN=&&SUBSTED,DISP=(,PASS),SPACE=(TRK,(1,2))
    //SYSTSIN DD DUMMY
    //SYSTSPRT DD SYSOUT=*
    //SYSEXEC DD DSN=PROD.REXXLIB,DISP=SHR
    //*====================================================================*
    //* MAIL026: SEND EMAIL NOTIFICATION FOR RL CREF ONLY CONTRACTS        *
    //*====================================================================*
    //MAIL026 EXEC PGM=IKJEFT01,DYNAMNBR=50,REGION=4M,COND=(0,NE)
    //SYSTSPRT DD SYSOUT=*
    //SYSUDUMP DD SYSOUT=U
    //SYSPROC DD DSN=PMVSPP.ISPF.CLISTLIB,DISP=SHR
    //SYSTSIN DD DSN=&&SUBSTED,DISP=(OLD,DELETE)
    //*
    // ENDIF
    --> 
    <decision id="if_0"  ref="bphx.batch.deciders.IfDecider">
        <properties>
            <property name="condition" value="VERY022.RC EQ 0"/>
        </properties>
        <next on="true" to="sort023"/>
        <next on="false" to="frmt027"/>
    </decision>
    <!--THEN branch for IF_0 -->
        <!--
    
    //*====================================================================*
    //* SORT023: FORMAT THE RL CREF ONLY CONTRACTS                         *
    //*====================================================================*
    //SORT023  EXEC PGM=SORT,COND=(0,NE)                                    
    //*
    //SORTIN   DD  DSN=&HLQ8..LGLEXTR.RLCREF,DISP=SHR                       
    //*
    //SORTOUT  DD DSN=&HLQ8..LGLEXTR.RLCREF.CONTRCT,                        
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,               
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE),
    //             DCB=(LRECL=80,RECFM=FB,BLKSIZE=0)
    //*
    //SYSOUT   DD  SYSOUT=*                                                 
    //SYSUDUMP DD  SYSOUT=U                                                 
    //SYSPRINT DD  SYSOUT=*                                                 
    //SYSIN    DD  DSN=&LIB1..PARMLIB(NI1170DB),DISP=SHR
    --> 
    <step id="sort023">
        <properties>
            <property name="STEP_NAME" value="SORT023"/>
            <property name="EXEC" value="SORT"/>
               <property name="COND" value="COND=(0,NE)"/>
            <property name="SORTIN" value="dsn://${HLQ8}.LGLEXTR.RLCREF?DISP=SHR"/>
            <property name="SORTOUT" value="dsn://${HLQ8}.LGLEXTR.RLCREF.CONTRCT?DISP=(NEW,CATLG,DELETE);DCB=(LRECL=80,RECFM=FB)"/>
            <property name="SYSOUT" value="sysout://SYSOUT?class=*"/>
            <property name="SYSUDUMP" value="sysout://SYSUDUMP?class=U"/>
            <property name="SYSPRINT" value="sysout://SYSPRINT?class=*"/>
            <property name="SYSIN" value="dsn://${LIB1}.PARMLIB(NI1170DB)?DISP=SHR"/>
        </properties>
        <batchlet ref="SortBatchlet"/>
        <next on="*" to="sort024"/>
    </step>
        <!--
    
    //*====================================================================*
    //* SORT024: PREPARE THE EMAIL BODY FOR RL CREF ONLY CONTRACTS         *
    //*====================================================================*
    //SORT024  EXEC PGM=SORT,COND=(0,NE)                                    
    //*
    //SORTIN   DD  DSN=&HLQ1..ACIS.LEGAL.RLCREF.EMLBDY,DISP=SHR             
    //         DD  DSN=&HLQ8..LGLEXTR.RLCREF.CONTRCT,DISP=SHR               
    //*
    //SORTOUT  DD  DSN=&HLQ8..ACIS.LEGAL.RLCREF.EMAIL,                      
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,               
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE),
    //             DCB=*.SORTIN
    //*
    //SYSOUT   DD  SYSOUT=*                                                 
    //SYSUDUMP DD  SYSOUT=U                                                 
    //SYSPRINT DD  SYSOUT=*                                                 
    //SYSIN    DD  DSN=&LIB1..PARMLIB(NI1170DC),DISP=SHR                    
    --> 
    <step id="sort024">
        <properties>
            <property name="STEP_NAME" value="SORT024"/>
            <property name="EXEC" value="SORT"/>
               <property name="COND" value="COND=(0,NE)"/>
            <property name="SORTIN" value="dsn://${HLQ1}.ACIS.LEGAL.RLCREF.EMLBDY?DISP=SHR"/>
            <property name="SORTIN_1" value="dsn://${HLQ8}.LGLEXTR.RLCREF.CONTRCT?DISP=SHR"/>
            <property name="SORTOUT" value="dsn://${HLQ8}.ACIS.LEGAL.RLCREF.EMAIL?DISP=(NEW,CATLG,DELETE);DCB=*.SORTIN"/>
            <property name="SYSOUT" value="sysout://SYSOUT?class=*"/>
            <property name="SYSUDUMP" value="sysout://SYSUDUMP?class=U"/>
            <property name="SYSPRINT" value="sysout://SYSPRINT?class=*"/>
            <property name="SYSIN" value="dsn://${LIB1}.PARMLIB(NI1170DC)?DISP=SHR"/>
        </properties>
        <batchlet ref="SortBatchlet"/>
        <next on="*" to="parm025"/>
    </step>
        <!--
    
    //*====================================================================*
    //* PARM025: PREPARE THE EMAIL BODY FOR RL CREF ONLY CONTRACTS         *
    //*====================================================================*
    //PARM025 EXEC PGM=IRXJCL,
    //       PARM='PARMSUB EMBDY=&EMBDY',COND=(0,NE)
    //TEMPLATE DD DSN=&LIB1..PARMLIB(NI1170DD),DISP=SHR
    //SUBSTED DD DSN=&&SUBSTED,DISP=(,PASS),SPACE=(TRK,(1,2))
    //SYSTSIN DD DUMMY
    //SYSTSPRT DD SYSOUT=*
    //SYSEXEC DD DSN=PROD.REXXLIB,DISP=SHR
    --> 
    <step id="parm025">
        <properties>
            <property name="STEP_NAME" value="PARM025"/>
            <property name="EXEC" value="IRXJCL"/>
               <property name="arguments" value="PARMSUB EMBDY=${EMBDY}"/>
               <property name="COND" value="COND=(0,NE)"/>
            <property name="TEMPLATE" value="dsn://${LIB1}.PARMLIB(NI1170DD)?DISP=SHR"/>
            <property name="SUBSTED" value="temp://SUBSTED?DISP=(,PASS)"/>
            <property name="SYSTSIN" value="dummy://?DUMMY"/>
            <property name="SYSTSPRT" value="sysout://SYSTSPRT?class=*"/>
            <property name="SYSEXEC" value="dsn://PROD.REXXLIB?DISP=SHR"/>
        </properties>
        <batchlet ref="StandardBatchlet"/>
        <next on="*" to="mail026"/>
    </step>
        <!--
    
    //*====================================================================*
    //* MAIL026: SEND EMAIL NOTIFICATION FOR RL CREF ONLY CONTRACTS        *
    //*====================================================================*
    //MAIL026 EXEC PGM=IKJEFT01,DYNAMNBR=50,REGION=4M,COND=(0,NE)
    //SYSTSPRT DD SYSOUT=*
    //SYSUDUMP DD SYSOUT=U
    //SYSPROC DD DSN=PMVSPP.ISPF.CLISTLIB,DISP=SHR
    //SYSTSIN DD DSN=&&SUBSTED,DISP=(OLD,DELETE)
    --> 
    <step id="mail026">
        <properties>
            <property name="STEP_NAME" value="MAIL026"/>
            <property name="EXEC" value="IKJEFT01"/>
               <property name="DYNAMNBR" value="50"/>
               <property name="REGION" value="4M"/>
               <property name="COND" value="COND=(0,NE)"/>
            <property name="SYSTSPRT" value="sysout://SYSTSPRT?class=*"/>
            <property name="SYSUDUMP" value="sysout://SYSUDUMP?class=U"/>
            <property name="SYSPROC" value="dsn://PMVSPP.ISPF.CLISTLIB?DISP=SHR"/>
            <property name="SYSTSIN" value="temp://SUBSTED?DISP=(OLD,DELETE)"/>
        </properties>
        <batchlet ref="Ikjeft1Batchlet"/>
        <next on="*" to="frmt027"/>
    </step>
    <!--ELSE branch for IF_0 -->
    <!--ENDIF for IF_0 -->
       <!--
    
    //*====================================================================*
    //* PROGRAM APPB1155                                                   *
    //*                                                                    *
    //* FRMT027: RETRIEVE AGENT INFORMATION FROM AODM                      *
    //*====================================================================*
    //FRMT027  EXEC PGM=NATB030,REGION=&REGN1,COND=(2,LT),
    //   PARM='SYS=&NAT'
    //DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
    //*....->  BEGIN ORACLE ITEMS    <......-
    //*MSYS //    INCLUDE MEMBER=&INCLUDE
    //ACISFLE  DD DSN=&LIB1..&ORALOG(&CNTL1),
    //         DISP=SHR
    //*....->  END ORACLE ITEMS    <......-
    //CMWKF01  DD DSN=&HLQ8..LGLEXTR.DIAG.SORT.FINAL,DISP=SHR
    //CMWKF02  DD DSN=&HLQ8..LGLEXTR.DIAG.SORTNEW,
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
    //             DCB=RECFM=FB,
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
    //SYSPRINT DD SYSOUT=*
    //DDPRINT  DD SYSOUT=*
    //SYSOUT   DD SYSOUT=*
    //SYSUDUMP DD SYSOUT=U
    //CMPRINT  DD SYSOUT=*
    //CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
    //         DD DSN=&LIB1..PARMLIB(NI1170D6),DISP=SHR
    --> 
    <step id="frmt027">
        <properties>
            <property name="STEP_NAME" value="FRMT027"/>
            <property name="EXEC" value="NATB030"/>
               <property name="REGION" value="${REGN1}"/>
               <property name="COND" value="COND=(2,LT)"/>
               <property name="arguments" value="SYS=${NAT}"/>
            <property name="DDCARD" value="dsn://${LIB1}.PARMLIB(DBAPP${DBID})?DISP=SHR"/>
            <property name="ACISFLE" value="dsn://${LIB1}.${ORALOG}(${CNTL1})?DISP=SHR"/>
            <property name="CMWKF01" value="dsn://${HLQ8}.LGLEXTR.DIAG.SORT.FINAL?DISP=SHR"/>
            <property name="CMWKF02" value="dsn://${HLQ8}.LGLEXTR.DIAG.SORTNEW?DISP=(NEW,CATLG,DELETE);DCB=RECFM=FB"/>
            <property name="SYSPRINT" value="sysout://SYSPRINT?class=*"/>
            <property name="DDPRINT" value="sysout://DDPRINT?class=*"/>
            <property name="SYSOUT" value="sysout://SYSOUT?class=*"/>
            <property name="SYSUDUMP" value="sysout://SYSUDUMP?class=U"/>
            <property name="CMPRINT" value="sysout://CMPRINT?class=*"/>
            <property name="CMSYNIN" value="dsn://${LIB1}.NT2LOGON.PARMLIB(${NATMEM})?DISP=SHR"/>
            <property name="CMSYNIN_1" value="dsn://${LIB1}.PARMLIB(NI1170D6)?DISP=SHR"/>
        </properties>
        <batchlet ref="NaturalProgramBatchlet"/>
        <next on="*" to="frmt030"/>
    </step>
       <!--
    
    //*====================================================================*
    //* PROGRAM PPP1006                                                    *
    //*                                                                    *
    //* ADD THE JOBNAME AND SHORT NAME TO THE EXTRACT FILE                 *
    //*====================================================================*
    //FRMT030  EXEC PGM=PPP1006,REGION=&REGN1,COND=(2,LT)
    //SYSOUT   DD SYSOUT=&JCLO
    //SYSABOUT DD SYSOUT=&JCLO,
    //            RECFM=FBM
    //SYSUDUMP DD SYSOUT=U,
    //            RECFM=FBM
    //SYSPRINT DD SYSOUT=&JCLO,
    //            RECFM=FBM
    //NROLDATA DD DSN=&HLQ8..LGLEXTR.DIAG.SORTNEW,DISP=SHR
    //CPDATA   DD DSN=&HLQ6..ACIS.DIAG.RULES,DISP=SHR
    //TEMPNROL DD DSN=&HLQ8..LEGEXTR.DIAG.PPP1006,
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
    //             DCB=RECFM=FB,
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
    //SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
    //JOBRPT   DD SYSOUT=(,NI1170D1),OUTPUT=(*.OUT1,*.OUT2),
    //            DCB=RECFM=FBM
    --> 
    <step id="frmt030">
        <properties>
            <property name="STEP_NAME" value="FRMT030"/>
            <property name="EXEC" value="PPP1006"/>
               <property name="REGION" value="${REGN1}"/>
               <property name="COND" value="COND=(2,LT)"/>
            <property name="SYSOUT" value="sysout://SYSOUT?class=${JCLO}"/>
            <property name="SYSABOUT" value="sysout://SYSABOUT?class=${JCLO};"/>
            <property name="SYSUDUMP" value="sysout://SYSUDUMP?class=U;"/>
            <property name="SYSPRINT" value="sysout://SYSPRINT?class=${JCLO};"/>
            <property name="NROLDATA" value="dsn://${HLQ8}.LGLEXTR.DIAG.SORTNEW?DISP=SHR"/>
            <property name="CPDATA" value="dsn://${HLQ6}.ACIS.DIAG.RULES?DISP=SHR"/>
            <property name="TEMPNROL" value="dsn://${HLQ8}.LEGEXTR.DIAG.PPP1006?DISP=(NEW,CATLG,DELETE);DCB=RECFM=FB"/>
            <property name="SORTWK01" value="temp://?"/>
            <property name="JOBRPT" value="sysout://JOBRPT?class=(,NI1170D1);OUTPUT=(*.OUT1,*.OUT2);DCB=RECFM=FBM"/>
        </properties>
        <batchlet ref="StandardBatchlet"/>
        <next on="*" to="frmt040"/>
    </step>
       <!--
    
    //*====================================================================*
    //* PROGRAM APPB1151                                                   *
    //*                                                                    *
    //* FRMT040: ADD THE RECORD KEY TO THE EXTRACT FILE                    *
    //*====================================================================*
    //FRMT040  EXEC PGM=NATB030,REGION=&REGN1,COND=(2,LT),
    //   PARM='SYS=&NAT'
    //DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
    //CMWKF01  DD DSN=&HLQ8..LEGEXTR.DIAG.PPP1006,DISP=SHR
    //CMWKF02  DD DSN=&HLQ8..LEGEXTR.DIAG.EXTRWHDR,
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
    //             DCB=RECFM=FB,
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
    //SYSPRINT DD SYSOUT=*
    //DDPRINT  DD SYSOUT=*
    //SYSOUT   DD SYSOUT=*
    //SYSUDUMP DD SYSOUT=U
    //CMPRINT  DD SYSOUT=*
    //CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
    //         DD DSN=&LIB1..PARMLIB(NI1170D3),DISP=SHR
    --> 
    <step id="frmt040">
        <properties>
            <property name="STEP_NAME" value="FRMT040"/>
            <property name="EXEC" value="NATB030"/>
               <property name="REGION" value="${REGN1}"/>
               <property name="COND" value="COND=(2,LT)"/>
               <property name="arguments" value="SYS=${NAT}"/>
            <property name="DDCARD" value="dsn://${LIB1}.PARMLIB(DBAPP${DBID})?DISP=SHR"/>
            <property name="CMWKF01" value="dsn://${HLQ8}.LEGEXTR.DIAG.PPP1006?DISP=SHR"/>
            <property name="CMWKF02" value="dsn://${HLQ8}.LEGEXTR.DIAG.EXTRWHDR?DISP=(NEW,CATLG,DELETE);DCB=RECFM=FB"/>
            <property name="SYSPRINT" value="sysout://SYSPRINT?class=*"/>
            <property name="DDPRINT" value="sysout://DDPRINT?class=*"/>
            <property name="SYSOUT" value="sysout://SYSOUT?class=*"/>
            <property name="SYSUDUMP" value="sysout://SYSUDUMP?class=U"/>
            <property name="CMPRINT" value="sysout://CMPRINT?class=*"/>
            <property name="CMSYNIN" value="dsn://${LIB1}.NT2LOGON.PARMLIB(${NATMEM})?DISP=SHR"/>
            <property name="CMSYNIN_1" value="dsn://${LIB1}.PARMLIB(NI1170D3)?DISP=SHR"/>
        </properties>
        <batchlet ref="NaturalProgramBatchlet"/>
        <next on="*" to="sort050"/>
    </step>
       <!--
    
    //*====================================================================*
    //* SORT050: SORT EXTRACT FILE BY THE RECORD KEY                       *
    //*====================================================================*
    //SORT050  EXEC PGM=SORT,COND=(2,LT)
    //SORTIN   DD DSN=&HLQ8..LEGEXTR.DIAG.EXTRWHDR,
    //             DISP=SHR
    //SORTOUT  DD DSN=&HLQ8..LEGEXTR.DIAG.SORTWHDR,
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
    //             DCB=RECFM=FB,
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
    //SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
    //SYSABOUT DD  SYSOUT=*
    //SYSOUT   DD  SYSOUT=*
    //SYSUDUMP DD  SYSOUT=U
    //SYSIN    DD  DSN=&LIB1..PARMLIB(NI1170D4),DISP=SHR
    --> 
    <step id="sort050">
        <properties>
            <property name="STEP_NAME" value="SORT050"/>
            <property name="EXEC" value="SORT"/>
               <property name="COND" value="COND=(2,LT)"/>
            <property name="SORTIN" value="dsn://${HLQ8}.LEGEXTR.DIAG.EXTRWHDR?DISP=SHR"/>
            <property name="SORTOUT" value="dsn://${HLQ8}.LEGEXTR.DIAG.SORTWHDR?DISP=(NEW,CATLG,DELETE);DCB=RECFM=FB"/>
            <property name="SORTWK01" value="temp://?"/>
            <property name="SYSABOUT" value="sysout://SYSABOUT?class=*"/>
            <property name="SYSOUT" value="sysout://SYSOUT?class=*"/>
            <property name="SYSUDUMP" value="sysout://SYSUDUMP?class=U"/>
            <property name="SYSIN" value="dsn://${LIB1}.PARMLIB(NI1170D4)?DISP=SHR"/>
        </properties>
        <batchlet ref="SortBatchlet"/>
        <next on="*" to="frmt054"/>
    </step>
       <!--
    
    //*====================================================================*
    //* PROGRAM:APPB1158                                                   *
    //*                                                                    *
    //* FRMT054:ADD BUNDLE INFORMATION TO THE EXTRACT FILE                 *
    //*====================================================================*
    //FRMT054  EXEC PGM=NATB030,REGION=&REGN1,COND=(2,LT),
    //   PARM='SYS=&NAT'
    //DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
    //CMWKF01  DD DSN=&HLQ8..LEGEXTR.DIAG.SORTWHDR,DISP=SHR
    //CMWKF02  DD DSN=&HLQ8..LEGEXTR.DIAG.FRMTBNDL,
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
    //             DCB=RECFM=FB,
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
    //SYSPRINT DD SYSOUT=*
    //DDPRINT  DD SYSOUT=*
    //SYSOUT   DD SYSOUT=*
    //SYSUDUMP DD SYSOUT=U
    //CMPRINT  DD SYSOUT=*
    //CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
    //         DD DSN=&LIB1..PARMLIB(NI1170D7),DISP=SHR
    --> 
    <step id="frmt054">
        <properties>
            <property name="STEP_NAME" value="FRMT054"/>
            <property name="EXEC" value="NATB030"/>
               <property name="REGION" value="${REGN1}"/>
               <property name="COND" value="COND=(2,LT)"/>
               <property name="arguments" value="SYS=${NAT}"/>
            <property name="DDCARD" value="dsn://${LIB1}.PARMLIB(DBAPP${DBID})?DISP=SHR"/>
            <property name="CMWKF01" value="dsn://${HLQ8}.LEGEXTR.DIAG.SORTWHDR?DISP=SHR"/>
            <property name="CMWKF02" value="dsn://${HLQ8}.LEGEXTR.DIAG.FRMTBNDL?DISP=(NEW,CATLG,DELETE);DCB=RECFM=FB"/>
            <property name="SYSPRINT" value="sysout://SYSPRINT?class=*"/>
            <property name="DDPRINT" value="sysout://DDPRINT?class=*"/>
            <property name="SYSOUT" value="sysout://SYSOUT?class=*"/>
            <property name="SYSUDUMP" value="sysout://SYSUDUMP?class=U"/>
            <property name="CMPRINT" value="sysout://CMPRINT?class=*"/>
            <property name="CMSYNIN" value="dsn://${LIB1}.NT2LOGON.PARMLIB(${NATMEM})?DISP=SHR"/>
            <property name="CMSYNIN_1" value="dsn://${LIB1}.PARMLIB(NI1170D7)?DISP=SHR"/>
        </properties>
        <batchlet ref="NaturalProgramBatchlet"/>
        <next on="*" to="sort055"/>
    </step>
       <!--
    
    //*====================================================================*
    //* SORT055: SORT EXTRACT FILE BY THE RECORD KEY                       *
    //*====================================================================*
    //SORT055  EXEC PGM=SORT,COND=(2,LT)
    //SORTIN   DD DSN=&HLQ8..LEGEXTR.DIAG.FRMTBNDL,
    //             DISP=SHR
    //SORTOUT  DD DSN=&HLQ8..LEGEXTR.DIAG.SORTWBND,
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
    //             DCB=RECFM=FB,
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
    //SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
    //SYSABOUT DD  SYSOUT=*
    //SYSOUT   DD  SYSOUT=*
    //SYSUDUMP DD  SYSOUT=U
    //SYSIN    DD  DSN=&LIB1..PARMLIB(NI1170D8),DISP=SHR
    --> 
    <step id="sort055">
        <properties>
            <property name="STEP_NAME" value="SORT055"/>
            <property name="EXEC" value="SORT"/>
               <property name="COND" value="COND=(2,LT)"/>
            <property name="SORTIN" value="dsn://${HLQ8}.LEGEXTR.DIAG.FRMTBNDL?DISP=SHR"/>
            <property name="SORTOUT" value="dsn://${HLQ8}.LEGEXTR.DIAG.SORTWBND?DISP=(NEW,CATLG,DELETE);DCB=RECFM=FB"/>
            <property name="SORTWK01" value="temp://?"/>
            <property name="SYSABOUT" value="sysout://SYSABOUT?class=*"/>
            <property name="SYSOUT" value="sysout://SYSOUT?class=*"/>
            <property name="SYSUDUMP" value="sysout://SYSUDUMP?class=U"/>
            <property name="SYSIN" value="dsn://${LIB1}.PARMLIB(NI1170D8)?DISP=SHR"/>
        </properties>
        <batchlet ref="SortBatchlet"/>
        <next on="*" to="frmt060"/>
    </step>
       <!--
    
    //*====================================================================*
    //* PROGRAM APPB1152                                                   *
    //*                                                                    *
    //* FRMT060: CREATE DIALOG FEED FILE                                   *
    //*====================================================================*
    //FRMT060  EXEC PGM=NATB030,REGION=&REGN1,COND=(2,LT),
    //   PARM='SYS=&NAT'
    //DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
    //CMWKF01  DD DSN=&HLQ8..LEGEXTR.DIAG.SORTWBND,DISP=SHR  /* LRECL 3776
    //CMWKF02  DD DSN=&HLQ6..ACIS.LEGAL.DIALOG.DATA,       /* LRECL 23472
    //             DISP=SHR
    //SYSPRINT DD SYSOUT=*
    //DDPRINT  DD SYSOUT=*
    //SYSOUT   DD SYSOUT=*
    //SYSUDUMP DD SYSOUT=U
    //CMPRINT  DD SYSOUT=*
    //CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
    //         DD DSN=&LIB1..PARMLIB(NI1170D5),DISP=SHR
    --> 
    <step id="frmt060">
        <properties>
            <property name="STEP_NAME" value="FRMT060"/>
            <property name="EXEC" value="NATB030"/>
               <property name="REGION" value="${REGN1}"/>
               <property name="COND" value="COND=(2,LT)"/>
               <property name="arguments" value="SYS=${NAT}"/>
            <property name="DDCARD" value="dsn://${LIB1}.PARMLIB(DBAPP${DBID})?DISP=SHR"/>
            <property name="CMWKF01" value="dsn://${HLQ8}.LEGEXTR.DIAG.SORTWBND?DISP=SHR"/>
            <property name="CMWKF02" value="dsn://${HLQ6}.ACIS.LEGAL.DIALOG.DATA?DISP=SHR"/>
            <property name="SYSPRINT" value="sysout://SYSPRINT?class=*"/>
            <property name="DDPRINT" value="sysout://DDPRINT?class=*"/>
            <property name="SYSOUT" value="sysout://SYSOUT?class=*"/>
            <property name="SYSUDUMP" value="sysout://SYSUDUMP?class=U"/>
            <property name="CMPRINT" value="sysout://CMPRINT?class=*"/>
            <property name="CMSYNIN" value="dsn://${LIB1}.NT2LOGON.PARMLIB(${NATMEM})?DISP=SHR"/>
            <property name="CMSYNIN_1" value="dsn://${LIB1}.PARMLIB(NI1170D5)?DISP=SHR"/>
        </properties>
        <batchlet ref="NaturalProgramBatchlet"/>
        <next on="*" to="copy070"/>
    </step>
       <!--
    
    //*===================================================================*
    //* COPY THE DIALOG FEED FILE TO A BACKUP GDG.
    //*===================================================================*
    //COPY070  EXEC PGM=IEBGENER,COND=(2,LT)
    //SYSPRINT DD  SYSOUT=&JCLO
    //SYSUT1   DD  DSN=&HLQ6..ACIS.LEGAL.DIALOG.DATA,DISP=SHR
    //SYSUT2   DD  DSN=&HLQ6..ACIS.LEGAL.DIALOG.BU(&GDG1),
    //             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
    //             DCB=RECFM=VB,
    //             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
    //SYSIN    DD  DUMMY
    --> 
    <step id="copy070">
        <properties>
            <property name="STEP_NAME" value="COPY070"/>
            <property name="EXEC" value="IEBGENER"/>
               <property name="COND" value="COND=(2,LT)"/>
            <property name="SYSPRINT" value="sysout://SYSPRINT?class=${JCLO}"/>
            <property name="SYSUT1" value="dsn://${HLQ6}.ACIS.LEGAL.DIALOG.DATA?DISP=SHR"/>
            <property name="SYSUT2" value="dsn://${HLQ6}.ACIS.LEGAL.DIALOG.BU(${GDG1})?DISP=(NEW,CATLG,DELETE);DCB=RECFM=VB"/>
            <property name="SYSIN" value="dummy://?DUMMY"/>
        </properties>
        <batchlet ref="IEBGENERBatchlet"/>
    </step>
</job>
