package tiaa.batch.customized.utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.bphx.ctu.af.core.config.App;
import com.google.common.base.Charsets;
import com.modernsystems.batch.files.IRecordAccessProvider;
import com.modernsystems.batch.files.IRecordReader;
import com.modernsystems.batch.files.RecordIos;
import com.modernsystems.ctu.core.InjectorProvider;

import ateras.framework.io.springBatch.SbBatchDriver;
import bphx.batch.batchlets.ReturnCode;
import bphx.batch.batchlets.StandardBatchlet;
import bphx.batch.step.DDCardInfo;
 
public class NaturalProgramBatchlet extends StandardBatchlet
{
    private static final String WORKFILE = "CMWKF[0-9]+";
    private static final String PRINTFILE = "CMPRT[0-9]+";
    private static final String CMSYNIN = "CMSYNIN";
    private static final String CMPRINT = "CMPRINT";
	private static final List<String> DISCARD = new ArrayList<>();
    private IRecordAccessProvider rap;
    
	static {
		DISCARD.add("FIN");
		DISCARD.add("PART");
		DISCARD.add("WELCOME");
	}

    public NaturalProgramBatchlet() {
        this.rap = (IRecordAccessProvider)InjectorProvider.get((Class)IRecordAccessProvider.class);
    }
    
    @Override
	public ReturnCode doProcess() throws Exception {
        return this.executeProgram(this.getProgram(), new String[] { this.getParam() });
    }
    
    private ReturnCode executeProgram(final String programName, final String[] params) {
    	String[][] Printer = new String[20][2];
    	int Printer_count = 0;
        try {
            System.out.println("Running NATURAL Batchlet with library: " + programName);
            System.out.println("Found properties: ");
            final Map<String, String> naturalProps = this.filterStepProps();
            naturalProps.keySet().forEach(p -> System.out.println("\t" + p + " -> " + naturalProps.get(p)));
            System.out.println("Using PARAMS " + String.join(", ", params));
            final Map<String, DDCardInfo> workfiles = this.getDdList("CMWKF[0-9]+");
            System.out.println("Found " + workfiles.keySet().size() + " workfile(s): " + String.join(",", workfiles.keySet()));
			List<Entry<String, DDCardInfo>> list = workfiles.entrySet().stream()
					.filter(file -> file.getValue() != null && file.getValue().getDsn() != null 
					&& file.getValue().getDsn().contains("DUMMY")).collect(Collectors.toList());
			if (list != null && !list.isEmpty()) {
				String batchRootPath = System.getProperty("user.dir");
				File dummy = new File(batchRootPath + File.separator + "jcl-root" + File.separator + "DUMMY");
				if(!dummy.exists()) {
					dummy.createNewFile();
				}else {
					List<String> lines = FileUtils.readLines(dummy, Charsets.UTF_8);
					if (lines != null && !lines.isEmpty()) {
	
						FileUtils.forceDelete(dummy);
						dummy.createNewFile();
					}
				}
			}
            final Map<String, DDCardInfo> printfiles = this.getDdList("CMPRT[0-9]+");
            System.out.println("Found " + printfiles.keySet().size() + " printfile(s): " + String.join(",", printfiles.keySet()));
            final List<String> pgmLines = this.readCmsynin().stream().filter(l -> !this.isNaturalComment(l)).collect(Collectors.toList());//(Collector<? super Object, ?, List<String>>)
            int ret = 0;
            List<String> parms = new ArrayList();
            int linesCtr = pgmLines.size();
            for (int p = 1; p < linesCtr; p++)
            {
            	if (pgmLines.get(1).equals("FIN") || pgmLines.get(1).equals("PART")) {
					break;
				} else {
					
				}
            	{
            		parms.add(pgmLines.get(1));
            		pgmLines.remove(1);
            	}
            }
            for (String pgm : pgmLines) {
            	if (pgm != null && (pgm.startsWith("EX ") || pgm.startsWith("RUN "))) {
					pgm = pgm.substring(pgm.indexOf(' ') + 1);
				}
				if (pgm != null && !DISCARD.contains(pgm)) {
                    System.out.println("Found NATURAL program call: " + pgm);
                    final String[] pgmArgs = new String[this.ddCards.size() + 1 + 2 + 1];
                    pgm =  pgm.length() > 72
                           ? pgm.substring(0, 72).trim()
                           : pgm.trim();
                    pgmArgs[0] = pgm;
                    if (parms.size() > 0)
                    {
                    	String pgrm = "";
                    	for (int p = 0; p < parms.size(); p++)
                    	{
                    		pgrm = parms.get(p);
                    		if (pgrm.startsWith("~")) {
								pgrm = pgrm.replace("~", "");
							}
                    		pgrm =  pgrm.length() > 72
                                    ? pgrm.substring(0, 72).trim()
                                    : pgrm.trim();
                    		pgmArgs[0] += " " + pgrm;
                    	}
                    }
                    int i = 1;
                    String sysoutPath="";
                    for (final DDCardInfo dci : this.ddCards.values()) {
                    	if (dci.getDdName().contains("SYSOUT") ) {
                        	String[] separated = dci.getRecordInfo().getAbsoluteFile().split("SYSOUT");
                        	sysoutPath=separated[0];
                        }
                    	else if (dci.getDdName().contains("SYSPRINT") ) {
                    	String[] separated = dci.getRecordInfo().getAbsoluteFile().split("SYSPRINT");
                    	sysoutPath=separated[0];
                    	}
                    }
                    for (final DDCardInfo dci : this.ddCards.values()) {
                        pgmArgs[i] = String.valueOf(dci.getDdName()) + "=" + dci.getRecordInfo().getAbsoluteFile();
                        Printer[Printer_count][0]=dci.getRecordInfo().getAbsoluteFile();
                        if (dci.getDdName().contains("CMPR") ) {
                        	if (!dci.getRecordInfo().getAbsoluteFile().contains("CMPR") &&
                        		!dci.getRecordInfo().getAbsoluteFile().contains("sysout") ) {
                        		String[] separated = dci.getRecordInfo().getAbsoluteFile().split("jcl-root");
                        		String name="_"+ separated[separated.length-1];
                        		name=name.substring(name.indexOf('_')+2);
                        		if (name.contains(System.getProperty("file.separator")) ) {
                        			name=name.substring(name.indexOf(System.getProperty("file.separator"))+1);
                        		}
                        		dci.getRecordInfo().setAbsoluteFile(sysoutPath+name);
                        		NaturalProgramBatchlet.log.info(dci.getDdName() + ": "+dci.getRecordInfo().getAbsoluteFile());
                        		pgmArgs[i] = String.valueOf(dci.getDdName()) + "=" + dci.getRecordInfo().getAbsoluteFile();
                                Printer[Printer_count][1]=dci.getRecordInfo().getAbsoluteFile();
                                Printer_count++;
                        	}
                        }else {
                            if (dci.getDdName().contains("CMWKF") ){
                            	if (!dci.getRecordInfo().getAbsoluteFile().contains("CMWKF") &&
                            		!dci.getRecordInfo().getAbsoluteFile().contains("sysout") &&
                            		!dci.getRecordInfo().getAbsoluteFile().contains("DUMMY") &&
                            		!dci.getRecordInfo().getAbsoluteFile().contains(".") ) {
                            		String[] separated = dci.getRecordInfo().getAbsoluteFile().split("jcl-root");
                            		String name="_"+ separated[separated.length-1];
                            		name=name.substring(name.indexOf('_')+2);
                            		if (name.contains(System.getProperty("file.separator")) ) {
                            			name=name.substring(name.indexOf(System.getProperty("file.separator"))+1);
                            		}
                            		dci.getRecordInfo().setAbsoluteFile(sysoutPath+name);
                            		NaturalProgramBatchlet.log.info(dci.getDdName() + ": "+dci.getRecordInfo().getAbsoluteFile());
                            		pgmArgs[i] = String.valueOf(dci.getDdName()) + "=" + dci.getRecordInfo().getAbsoluteFile();
                                    Printer[Printer_count][1]=dci.getRecordInfo().getAbsoluteFile();
                                    Printer_count++;
                            	}
                        	}
                        	
                        }
                        ++i;
                    }
                    
                    pgmArgs[pgmArgs.length - 3]  ="RuntimeJobName="+this.jobContext.getJobName();
                    pgmArgs[pgmArgs.length - 2]  ="RuntimeJobId="+"001";
                    pgmArgs[pgmArgs.length - 1] ="RuntimePackage="+this.getJobProps().getProperty("PACKAGE");
                    ret = SbBatchDriver.main(pgmArgs); 
                }
            }
            for (int i = 0; i < Printer_count; i++) {
            	File file = new File(Printer[i][0]);
            	boolean empty = file.exists() && file.length() == 0;
            	if (empty) {
            		File fileDest = new File(Printer[i][1]);
            		if (fileDest.exists()) {
            			file.delete(); // file jcl root deleted
            		} else {
            			file.renameTo(fileDest); // file in root moved in sysout
            		}
            	}
            }
            return new ReturnCode(ret, false);
        }
        catch (Exception e) {
        	NaturalProgramBatchlet.log.info("Can't execute program {}!"+ programName,e);
            return ReturnCode.ABEND;
        }
    }
    
    private Map<String, String> filterStepProps() {
        final Map<String, String> naturalProps = new HashMap<>();
        final Map<String, String> map = new HashMap<>();
        this.getStepProps().keySet().forEach(p -> {
            if (!this.ddCards.containsKey(p) && !"STEP_NAME".equals(p) && !"EXEC".equals(p) && !"arguments".equals(p) && !String.valueOf(p).startsWith("CMSYNIN")) {
                map.put(String.valueOf(p), (String)this.getStepProps().get(p));
            }
            return;
        });
        return naturalProps;
    }
    
    private boolean isNaturalComment(final String line) {
        return line.startsWith("*") || line.startsWith("/*");
    }
    
    protected Map<String, DDCardInfo> getDdList(final String filePattern) {
        return this.ddCards.keySet().stream().filter(ddName -> ddName.matches(filePattern)).collect(Collectors.toMap(Function.identity(), this.ddCards::get));
        
        
    }
    
    public List<String> readCmsynin() {
        try {
            Throwable t = null;
            try {
                final IRecordReader rr = this.rap.getReader(this.ddCards.get("CMSYNIN"));
                try {
                    if (rr != null) {
                        return IOUtils.readLines(RecordIos.toReader(rr, App.context().getMarshalCharset()));
                    }
                    return null;
                }
                finally {
                    if (rr != null) {
                        rr.close();
                    }
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception;
//                    t = exception;
                }
                else {
                    final Throwable exception;
//                    if (t != exception) {
//                        t.addSuppressed(exception);
//                    }
                }
            }
        }
        catch (Exception e) {
            NaturalProgramBatchlet.log.error("Can't read CMSYNIN!", e);
        }
        return null;
    }
}
