package tiaa.batch.customized.utilities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Strings;

import bphx.batch.batchlets.Ikjeft1Batchlet;
import bphx.batch.batchlets.ReturnCode;

public class Ikjeft1BatchletImpl extends Ikjeft1Batchlet {

	static final Pattern PROGRAM_PERCENT = Pattern.compile("PROGRAM\\(.+\\)");

	@Override
	public ReturnCode doProcess() throws Exception {

		// assume other external program
		return executeCustom(getProgram());
	}
	
	private ReturnCode executeProgram(String programName, String[] params) {
		try {
			int ret = getBatchSession().run(programName, params);
			String abendCode = getBatchSession().specialRegisters().getAbendCode();
			
			return new ReturnCode(ret, !Strings.isNullOrEmpty(abendCode));
		} catch (Exception e) {
			log.error("Can't execute program {}!", programName, e);
			return ReturnCode.ABEND;
		}
	}
	
	private ReturnCode executeCustom(String programName) {
		try {
			
			String arguments = getParam();
			String pgmName = null;
			if (!Strings.isNullOrEmpty(arguments)) {
				String[] splitArgs = arguments.split(" ");
				pgmName = splitArgs[0];
			} else {
				String str =  readSystsin();
				System.out.println("***********************************************");
				System.out.println("IKJFT01 SYSTIN:");
				System.out.println(str);
				System.out.println("***********************************************");
				if(str != null) {
					// XXX not all control files have the first row with exec util
					//Matcher m = EXECUTIL.matcher(str); 
					//if(!m.find()) { return null; }
					
					Matcher pgm = PROGRAM_PERCENT.matcher(str);
					if (pgm.find()) {
						String pgmLine = pgm.group(0);
						pgmName = pgmLine.substring(pgmLine.indexOf("(") + 1, pgmLine.indexOf(")"));
						// assume that after the program name are only the arguments
						int argStart = str.indexOf(pgmLine) + pgmLine.length() + 1;
						if (argStart + 1 < str.length()) {
							arguments = str.substring(argStart, str.indexOf('\n', argStart + 1));
						} else {
							arguments = "";
						}
					}
				}
			}

			if(pgmName == null) {
				log.error("Can't determine Java program name!");
				returnCode = ReturnCode.ABEND;
			}
			try {
				returnCode = executeProgram(pgmName, new String[] {arguments});
			} catch (Exception e) {
				log.error("Can't execute Java program {}!",pgmName, e);
				returnCode = ReturnCode.ABEND;
			}
			
			return returnCode;
			
		} catch (Exception e) {
			log.error("Can't execute program {}!", programName, e);
			return ReturnCode.ABEND;
		}
	}
}
