package tiaa.batch.customized.utilities;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;

import com.bphx.ctu.af.core.config.App;
import com.modernsystems.batch.files.IRecordAccessProvider;
import com.modernsystems.batch.files.IRecordReader;
import com.modernsystems.batch.files.IRecordWriter;
import com.modernsystems.batch.files.RecordIos;
import com.modernsystems.ctu.core.InjectorProvider;

import bphx.batch.batchlets.ReturnCode;
import bphx.batch.batchlets.StandardBatchlet;
import bphx.batch.step.DDCardInfo;

public class CustomizedBatchlet extends StandardBatchlet
{
	private static final List<String> DISCARD = new ArrayList<>();
	private IRecordAccessProvider rap;


	static {
		DISCARD.add("FIN");
		DISCARD.add("PART");
		DISCARD.add("WELCOME");
	}

	public CustomizedBatchlet() {
		this.rap = (IRecordAccessProvider)InjectorProvider.get((Class<?>)IRecordAccessProvider.class);
	}

	@Override
	public ReturnCode doProcess() throws Exception {
		return this.executeProgram(this.getProgram(), new String[] { this.getParam() });
	}

	private ReturnCode executeProgram(final String programName, final String[] params) {
		try {
			int ret=0;
			System.out.println("Running Customized Batchlet with pgm: " + programName);
			if (programName.contains("IDCAMS") ) {
				final List<String> Lines = this.readDD("DD1").stream().filter(l -> !this.isComment(l)).collect(Collectors.toList());//(Collector<? super Object, ?, List<String>>)
				if(Lines.size() < 2 ) {
					CustomizedBatchlet.log.error("File DD1 with less then 2 records \n");
					ret=1;
				}
				System.out.println("Exit Customized Batchlet ret:"+ret+"\n");
			}
			if (programName.contains("CUSTIN") ) {
				FilterDDname("FILEIN1");
				FilterDDname("FILEIN2");
			}

			System.out.println("Exit Customized Batchlet ret:"+ret+"\n");
			return new ReturnCode(ret, false) ;
		}
		catch (Exception e) {
			CustomizedBatchlet.log.error("Can't execute program {}!", programName, e);
			return ReturnCode.ABEND;
		}
	}
	
	private void FilterDDname(String ddname) throws Exception {
		final List<String> Lines = this.readDD(ddname).stream().collect(Collectors.toList());//(Collector<? super Object, ?, List<String>>)
		String first12="";
		FileOutputStream outputStream = new FileOutputStream(this.ddCards.get(ddname).getRecordInfo().getAbsoluteFile());
		for (String line : Lines) {
			first12=line.substring(0, 2);
			if(line.length() < 128 ) {
				for (int i = line.length(); i < 128; i++) line += " ";
			}
			if (first12.contains("1W") ||first12.contains("1A") ||first12.contains("1E") ||first12.contains("1T") ||first12.contains("1F") ) {
				if (first12.contains("1W"))line += line.substring(3, 12)+"0";
				if (first12.contains("1A")) line += "0"+"0000000000";
				if (first12.contains("1E")) line += "1"+"0000000000";
				if (first12.contains("1T")) line += "3"+"0000000000";
				if (first12.contains("1F"))  {
					line += "4"+"0000000000";
				}
				
			}else {
				line += "2"+"0000000000";
			}
            line=line + "\n";			
			outputStream.write(line.getBytes());
		}
	}
	
	private Map<String, String> filterStepProps() {
		final Map<String, String> naturalProps = new HashMap<>();
		final Map<String, String> map = new HashMap<>();
		this.getStepProps().keySet().forEach(p -> {
			if (!this.ddCards.containsKey(p) && !"STEP_NAME".equals(p) && !"EXEC".equals(p) && !"arguments".equals(p) && !String.valueOf(p).startsWith("CMSYNIN")) {
				map.put(String.valueOf(p), (String)this.getStepProps().get(p));
			}
			return;
		});
		return naturalProps;
	}

	private boolean isComment(final String line) {
		return line.startsWith("*") || line.startsWith("/*");
	}

	protected Map<String, DDCardInfo> getDdList(final String filePattern) {
		return this.ddCards.keySet().stream().filter(ddName -> ddName.matches(filePattern)).collect(Collectors.toMap(Function.identity(), this.ddCards::get));


	}

	public List<String> readDD(String ddname) {
		try {
			Throwable t = null;
			try {
				final IRecordReader rr = this.rap.getReader(this.ddCards.get(ddname));
				try {
					if (rr != null) {
						return IOUtils.readLines(RecordIos.toReader(rr, App.context().getMarshalCharset()));
					}
					return null;
				}
				finally {
					if (rr != null) {
						rr.close();
					}
				}
			}
			finally {
				if (t == null) {
					final Throwable exception;
					//                    t = exception;
				}
				else {
					final Throwable exception;
					//                    if (t != exception) {
					//                        t.addSuppressed(exception);
					//                    }
				}
			}
		}
		catch (Exception e) {
			CustomizedBatchlet.log.error("Can't read DD:"+ ddname, e);
		}
		return null;
	}
	public List<String> writeDD(String ddname) {
		try {
			Throwable t = null;
			try {
				final IRecordWriter rr = this.rap.getWriter(this.ddCards.get(ddname));
				String s="kkkk";
				try {
					if (rr != null) {
						//   return IOUtils.writeLines(RecordIos.toReader(rr, App.context().getMarshalCharset()));
					}
					return null;
				}
				finally {
					if (rr != null) {
						rr.close();
					}
				}
			}
			finally {
				if (t == null) {
					final Throwable exception;
					//                    t = exception;
				}
				else {
					final Throwable exception;
					//                    if (t != exception) {
					//                        t.addSuppressed(exception);
					//                    }
				}
			}
		}
		catch (Exception e) {
			CustomizedBatchlet.log.error("Can't read DD:"+ ddname, e);
		}
		return null;
	}

}
