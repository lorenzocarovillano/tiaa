/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:42:30 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn6705
************************************************************
**        * FILE NAME            : Cwfn6705.java
**        * CLASS NAME           : Cwfn6705
**        * INSTANCE NAME        : Cwfn6705
************************************************************
************************************************************************
* PROGRAM  : CWFN6705
* SYSTEM   : PROJCWF
* TITLE    : SSR CREATION FOR TA
* GENERATED: JAN 8, 97
* FUNCTION : THIS SUBPROGRAM CREATES SHARE SERVICE REQUEST
*            FOR TRANSFER AGENT
* HISTORY
*
* 11/03/98 : GH  DON't update the rqst data for joint Acc.
* 10/30/98 : GH  DON't update the case ind for joint Acc.
* 04/24/98 : GH  CHANGED PARM WORK-PRCSS-ID OF 'CWFN6701' TO ARRAY FIELD
*          :     FOR NEW PRODUCTS - TRUST SERVICES.
* 11/13/97 : GH  FOR INTERNET CORRESPONDENCE (PSS UPLOAD):
*          :     IF CWFA5012.RQST-ORGN-CDE = 'N' AND
*          :     CWFA5010.ORGNL-UNIT-CDE NE 'CRC' THEN READ DOC.
* 11/07/97 : GH  FOR CRC: IF CWFA5012.RQST-ORGN-CDE = 'C'( RECEIVED VIA
*          :     PHONE CALL ) AND CWFA5010.ORGNL-UNIT-CDE NE 'CRC' THEN
*          :     READ THE DOCUMENT FILE.
* 08/11/97 : GH  MOVE 'Y' TO CWFA5012.WPID-VLDTE-IND,WHILE UPDATE WPID.
* 07/22/97 : GH  WHEN AN ACCOUNT BASED SSR ADDED, CHANGE THE WPID
*          :     (ADDRESS CHANGE) BASED ON THE VALUE OF THE CONTROL
*          :     TABLE.
* 01/08/97 : GH  CREATED NEW
* 11/04/98 : AS  RESTOWED FOR INTEGRATED FIELDS IN CWFA5012 & CWFA1800
* 06/02/00 : EPM RESTOWED FOR IES FIELDS IN CWFA5012, CWFA1800
*          :     AND CWFA5010
* 11/14/00 : GH  CORRECT THE PROBLEM OF MOVING #CASE-IND-SAVE TO
*          :     EFSA9120.OLD-CASE-ID.
* 01/02/02 : GH  INCREASED #COUNT FROM N3 TO N6
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn6705 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfa1800 pdaCwfa1800;
    private PdaCwfa5010 pdaCwfa5010;
    private PdaCwfa5011 pdaCwfa5011;
    private PdaCwfa5012 pdaCwfa5012;
    private PdaCwfl5312 pdaCwfl5312;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;
    private PdaErla1000 pdaErla1000;
    private PdaFfta1300 pdaFfta1300;
    private PdaMdma111 pdaMdma111;
    private PdaMdma116 pdaMdma116;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Count;
    private DbsField pnd_Shrd_Srvce_Ind;
    private DbsField pnd_Shrd_Srvce_A_Add;
    private DbsField pnd_Shrd_Srvce_A_Upd;
    private DbsField pnd_Shrd_Srvce_D_Add;
    private DbsField pnd_Shrd_Srvce_D_Upd;
    private DbsField pnd_Shrd_Srvce_P_Add;
    private DbsField pnd_Shrd_Srvce_P_Upd;
    private DbsField pnd_Rqst_Log_Dte_Tme;

    private DataAccessProgramView vw_cwf_Doc;
    private DbsField cwf_Doc_Cabinet_Id;
    private DbsField cwf_Doc_Tiaa_Rcvd_Dte;
    private DbsField cwf_Doc_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Doc_Document_Type;
    private DbsField cwf_Doc_Document_Category;
    private DbsField cwf_Doc_Document_Sub_Category;
    private DbsField cwf_Doc_Document_Detail;
    private DbsField cwf_Doc_Document_Direction_Ind;
    private DbsField cwf_Doc_Secured_Document_Ind;
    private DbsField cwf_Doc_Document_Subtype;
    private DbsField cwf_Doc_Document_Retention_Ind;
    private DbsField cwf_Doc_Entry_System_Or_Unit;
    private DbsField cwf_Doc_Entry_Dte_Tme;
    private DbsField cwf_Doc_Text_Pointer;

    private DataAccessProgramView vw_cwf_Text;
    private DbsField cwf_Text_Cabinet_Id;
    private DbsField cwf_Text_Text_Pointer;
    private DbsField cwf_Text_Text_Sqnce_Nbr;
    private DbsField cwf_Text_Last_Text_Lines_Group_Ind;

    private DbsGroup cwf_Text_Text_Document_Line_Group;
    private DbsField cwf_Text_Text_Document_Line;

    private DbsGroup cwf_Text__R_Field_1;
    private DbsField cwf_Text_Pnd_Text_Document_Line_Acc;
    private DbsField cwf_Text_Text_Object_Key;
    private DbsField pnd_Case_Ind_New;
    private DbsField pnd_Case_Ind_Save;
    private DbsField pnd_Cor_Super_Pin_Rcdtype_Lob_Nines;

    private DbsGroup pnd_Cor_Super_Pin_Rcdtype_Lob_Nines__R_Field_2;
    private DbsField pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Ph_Unique_Id_Nbr;
    private DbsField pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Ph_Rcd_Type_Cde;
    private DbsField pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Cntrct_Lob_Cde;
    private DbsField pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Cntrct_Issue_Dte_Nines_Cmplmnt;
    private DbsField pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Cntrct_Nbr;
    private DbsField pnd_Cor_Super_Cntrct_Payee_Pin;

    private DbsGroup pnd_Cor_Super_Cntrct_Payee_Pin__R_Field_3;
    private DbsField pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Cntrct_Nbr;
    private DbsField pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Cntrct_Payee_Cde;
    private DbsField pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Ph_Unique_Id_Nbr;
    private DbsField pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Ph_Rcd_Type_Cde;
    private DbsField pnd_Crrnt_Dte_Tme;
    private DbsField pnd_Crrnt_Dte_Tme_A;

    private DbsGroup pnd_Crrnt_Dte_Tme_A__R_Field_4;
    private DbsField pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_Tme_N;

    private DbsGroup pnd_Crrnt_Dte_Tme_A__R_Field_5;
    private DbsField pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_N;
    private DbsField pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Tme_N;
    private DbsField pnd_Dest_Unit_Mfta;
    private DbsField pnd_Document_Key;

    private DbsGroup pnd_Document_Key__R_Field_6;
    private DbsField pnd_Document_Key_Pnd_Doc_Cabinet_Id;

    private DbsGroup pnd_Document_Key__R_Field_7;
    private DbsField pnd_Document_Key_Pnd_Doc_Cabinet_Type;
    private DbsField pnd_Document_Key_Pnd_Doc_Cabinet_Name;

    private DbsGroup pnd_Document_Key__R_Field_8;
    private DbsField pnd_Document_Key_Pnd_Doc_Cabinet_Name_A12;
    private DbsField pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte;
    private DbsField pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst;

    private DbsGroup pnd_Document_Key__R_Field_9;
    private DbsField pnd_Document_Key_Pnd_Doc_Case_Ind;
    private DbsField pnd_Document_Key_Pnd_Doc_Originating_Work_Prcss_Id;
    private DbsField pnd_Document_Key_Pnd_Doc_Multi_Rqst_Ind;
    private DbsField pnd_Document_Key_Pnd_Doc_Sub_Rqst_Ind;
    private DbsField pnd_Document_Key_Pnd_Doc_Entry_Dte_Tme;
    private DbsField pnd_Document_Type;

    private DbsGroup pnd_Document_Type__R_Field_10;
    private DbsField pnd_Document_Type_Pnd_Document_Category;
    private DbsField pnd_Document_Type_Pnd_Document_Sub_Category;
    private DbsField pnd_Document_Type_Pnd_Document_Detail;
    private DbsField pnd_Errors;
    private DbsField pnd_High_Multi;
    private DbsField pnd_H2_Ph_Unique_Id_Nbr_A;

    private DbsGroup pnd_H2_Ph_Unique_Id_Nbr_A__R_Field_11;
    private DbsField pnd_H2_Ph_Unique_Id_Nbr_A_Pnd_H2_Ph_Unique_Id_Nbr_N;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Mitssp1;
    private DbsField pnd_Move_Case_Ind;
    private DbsField pnd_Nines;
    private DbsField pnd_Number;
    private DbsField pnd_Old_Cabinet;

    private DbsGroup pnd_Old_Cabinet__R_Field_12;
    private DbsField pnd_Old_Cabinet_Pnd_Cabinet_Prefix;
    private DbsField pnd_Old_Cabinet_Pnd_Old_Pin_Nbr;
    private DbsField pnd_Remarks;
    private DbsField pnd_Ssr_Added;
    private DbsField pnd_Ssr_Pin_Created_Array_A;

    private DbsGroup pnd_Ssr_Pin_Created_Array_A__R_Field_13;
    private DbsField pnd_Ssr_Pin_Created_Array_A_Pnd_Ssr_Pin_Created_Array_N;
    private DbsField pnd_Ssr_Wpid;
    private DbsField pnd_Store_Msg;
    private DbsField pnd_Text_Object_Key;

    private DbsGroup pnd_Text_Object_Key__R_Field_14;
    private DbsField pnd_Text_Object_Key_Pnd_Cabinet_Id;
    private DbsField pnd_Text_Object_Key_Pnd_Text_Pointer;
    private DbsField pnd_Text_Object_Key_Pnd_Text_Sqnce_Nbr;
    private DbsField pnd_Wpid_Change;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfa1800 = new PdaCwfa1800(localVariables);
        pdaCwfa5010 = new PdaCwfa5010(localVariables);
        pdaCwfa5011 = new PdaCwfa5011(localVariables);
        pdaCwfa5012 = new PdaCwfa5012(localVariables);
        pdaCwfl5312 = new PdaCwfl5312(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaErla1000 = new PdaErla1000(localVariables);
        pdaFfta1300 = new PdaFfta1300(localVariables);
        pdaMdma111 = new PdaMdma111(localVariables);
        pdaMdma116 = new PdaMdma116(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pnd_Count = parameters.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 6);
        pnd_Count.setParameterOption(ParameterOption.ByReference);
        pnd_Shrd_Srvce_Ind = parameters.newFieldInRecord("pnd_Shrd_Srvce_Ind", "#SHRD-SRVCE-IND", FieldType.STRING, 1);
        pnd_Shrd_Srvce_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_Shrd_Srvce_A_Add = parameters.newFieldInRecord("pnd_Shrd_Srvce_A_Add", "#SHRD-SRVCE-A-ADD", FieldType.NUMERIC, 5);
        pnd_Shrd_Srvce_A_Add.setParameterOption(ParameterOption.ByReference);
        pnd_Shrd_Srvce_A_Upd = parameters.newFieldInRecord("pnd_Shrd_Srvce_A_Upd", "#SHRD-SRVCE-A-UPD", FieldType.NUMERIC, 5);
        pnd_Shrd_Srvce_A_Upd.setParameterOption(ParameterOption.ByReference);
        pnd_Shrd_Srvce_D_Add = parameters.newFieldInRecord("pnd_Shrd_Srvce_D_Add", "#SHRD-SRVCE-D-ADD", FieldType.NUMERIC, 5);
        pnd_Shrd_Srvce_D_Add.setParameterOption(ParameterOption.ByReference);
        pnd_Shrd_Srvce_D_Upd = parameters.newFieldInRecord("pnd_Shrd_Srvce_D_Upd", "#SHRD-SRVCE-D-UPD", FieldType.NUMERIC, 5);
        pnd_Shrd_Srvce_D_Upd.setParameterOption(ParameterOption.ByReference);
        pnd_Shrd_Srvce_P_Add = parameters.newFieldInRecord("pnd_Shrd_Srvce_P_Add", "#SHRD-SRVCE-P-ADD", FieldType.NUMERIC, 5);
        pnd_Shrd_Srvce_P_Add.setParameterOption(ParameterOption.ByReference);
        pnd_Shrd_Srvce_P_Upd = parameters.newFieldInRecord("pnd_Shrd_Srvce_P_Upd", "#SHRD-SRVCE-P-UPD", FieldType.NUMERIC, 5);
        pnd_Shrd_Srvce_P_Upd.setParameterOption(ParameterOption.ByReference);
        pnd_Rqst_Log_Dte_Tme = parameters.newFieldInRecord("pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Rqst_Log_Dte_Tme.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_cwf_Doc = new DataAccessProgramView(new NameInfo("vw_cwf_Doc", "CWF-DOC"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Doc_Cabinet_Id = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Doc_Cabinet_Id.setDdmHeader("CABINET/ID");
        cwf_Doc_Tiaa_Rcvd_Dte = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Doc_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Doc_Case_Workprcss_Multi_Subrqst = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Doc_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Doc_Document_Type = vw_cwf_Doc.getRecord().newGroupInGroup("CWF_DOC_DOCUMENT_TYPE", "DOCUMENT-TYPE");
        cwf_Doc_Document_Type.setDdmHeader("DOCUMENT/TYPE");
        cwf_Doc_Document_Category = cwf_Doc_Document_Type.newFieldInGroup("cwf_Doc_Document_Category", "DOCUMENT-CATEGORY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DOCUMENT_CATEGORY");
        cwf_Doc_Document_Category.setDdmHeader("DOC/CAT");
        cwf_Doc_Document_Sub_Category = cwf_Doc_Document_Type.newFieldInGroup("cwf_Doc_Document_Sub_Category", "DOCUMENT-SUB-CATEGORY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DOCUMENT_SUB_CATEGORY");
        cwf_Doc_Document_Sub_Category.setDdmHeader("DOC SUB-/CATEGORY");
        cwf_Doc_Document_Detail = cwf_Doc_Document_Type.newFieldInGroup("cwf_Doc_Document_Detail", "DOCUMENT-DETAIL", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DOCUMENT_DETAIL");
        cwf_Doc_Document_Detail.setDdmHeader("DOCUMENT/DETAIL");
        cwf_Doc_Document_Direction_Ind = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Document_Direction_Ind", "DOCUMENT-DIRECTION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOCUMENT_DIRECTION_IND");
        cwf_Doc_Document_Direction_Ind.setDdmHeader("DOC/DIR");
        cwf_Doc_Secured_Document_Ind = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Secured_Document_Ind", "SECURED-DOCUMENT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SECURED_DOCUMENT_IND");
        cwf_Doc_Secured_Document_Ind.setDdmHeader("SECRD/DOC");
        cwf_Doc_Document_Subtype = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Document_Subtype", "DOCUMENT-SUBTYPE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DOCUMENT_SUBTYPE");
        cwf_Doc_Document_Subtype.setDdmHeader("DOC/SUBTP");
        cwf_Doc_Document_Retention_Ind = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Document_Retention_Ind", "DOCUMENT-RETENTION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOCUMENT_RETENTION_IND");
        cwf_Doc_Document_Retention_Ind.setDdmHeader("DOC/RETNTN");
        cwf_Doc_Entry_System_Or_Unit = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Entry_System_Or_Unit", "ENTRY-SYSTEM-OR-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_SYSTEM_OR_UNIT");
        cwf_Doc_Entry_System_Or_Unit.setDdmHeader("CREATED BY/SYSTEM OR UNIT");
        cwf_Doc_Entry_Dte_Tme = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ENTRY_DTE_TME");
        cwf_Doc_Entry_Dte_Tme.setDdmHeader("ENTRY/DATE AND TIME");
        cwf_Doc_Text_Pointer = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Text_Pointer", "TEXT-POINTER", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "TEXT_POINTER");
        cwf_Doc_Text_Pointer.setDdmHeader("TEXT/POINTER");
        registerRecord(vw_cwf_Doc);

        vw_cwf_Text = new DataAccessProgramView(new NameInfo("vw_cwf_Text", "CWF-TEXT"), "CWF_EFM_TEXT_OBJECT", "CWF_EMF_TEXT_OBJ", DdmPeriodicGroups.getInstance().getGroups("CWF_EFM_TEXT_OBJECT"));
        cwf_Text_Cabinet_Id = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Text_Cabinet_Id.setDdmHeader("CABINET/ID");
        cwf_Text_Text_Pointer = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Text_Pointer", "TEXT-POINTER", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "TEXT_POINTER");
        cwf_Text_Text_Pointer.setDdmHeader("TEXT/POINTER");
        cwf_Text_Text_Sqnce_Nbr = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Text_Sqnce_Nbr", "TEXT-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TEXT_SQNCE_NBR");
        cwf_Text_Text_Sqnce_Nbr.setDdmHeader("SQNCE/NBR");
        cwf_Text_Last_Text_Lines_Group_Ind = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Last_Text_Lines_Group_Ind", "LAST-TEXT-LINES-GROUP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "LAST_TEXT_LINES_GROUP_IND");
        cwf_Text_Last_Text_Lines_Group_Ind.setDdmHeader("LAST/TEXT IND");

        cwf_Text_Text_Document_Line_Group = vw_cwf_Text.getRecord().newGroupArrayInGroup("cwf_Text_Text_Document_Line_Group", "TEXT-DOCUMENT-LINE-GROUP", 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_EMF_TEXT_OBJ_TEXT_DOCUMENT_LINE_GROUP");
        cwf_Text_Text_Document_Line_Group.setDdmHeader("TEXT/LINES");
        cwf_Text_Text_Document_Line = cwf_Text_Text_Document_Line_Group.newFieldInGroup("cwf_Text_Text_Document_Line", "TEXT-DOCUMENT-LINE", FieldType.STRING, 
            80, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TEXT_DOCUMENT_LINE", "CWF_EMF_TEXT_OBJ_TEXT_DOCUMENT_LINE_GROUP");
        cwf_Text_Text_Document_Line.setDdmHeader("TEXT DOCUMENT/LINE");

        cwf_Text__R_Field_1 = cwf_Text_Text_Document_Line_Group.newGroupInGroup("cwf_Text__R_Field_1", "REDEFINE", cwf_Text_Text_Document_Line);
        cwf_Text_Pnd_Text_Document_Line_Acc = cwf_Text__R_Field_1.newFieldArrayInGroup("cwf_Text_Pnd_Text_Document_Line_Acc", "#TEXT-DOCUMENT-LINE-ACC", 
            FieldType.STRING, 12, new DbsArrayController(1, 5));
        cwf_Text_Text_Object_Key = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Text_Object_Key", "TEXT-OBJECT-KEY", FieldType.BINARY, 24, RepeatingFieldStrategy.None, 
            "TEXT_OBJECT_KEY");
        cwf_Text_Text_Object_Key.setDdmHeader("TEXT OBJECT/KEY");
        cwf_Text_Text_Object_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Text);

        pnd_Case_Ind_New = localVariables.newFieldInRecord("pnd_Case_Ind_New", "#CASE-IND-NEW", FieldType.STRING, 1);
        pnd_Case_Ind_Save = localVariables.newFieldInRecord("pnd_Case_Ind_Save", "#CASE-IND-SAVE", FieldType.STRING, 1);
        pnd_Cor_Super_Pin_Rcdtype_Lob_Nines = localVariables.newFieldInRecord("pnd_Cor_Super_Pin_Rcdtype_Lob_Nines", "#COR-SUPER-PIN-RCDTYPE-LOB-NINES", 
            FieldType.STRING, 33);

        pnd_Cor_Super_Pin_Rcdtype_Lob_Nines__R_Field_2 = localVariables.newGroupInRecord("pnd_Cor_Super_Pin_Rcdtype_Lob_Nines__R_Field_2", "REDEFINE", 
            pnd_Cor_Super_Pin_Rcdtype_Lob_Nines);
        pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Ph_Unique_Id_Nbr = pnd_Cor_Super_Pin_Rcdtype_Lob_Nines__R_Field_2.newFieldInGroup("pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Ph_Unique_Id_Nbr", 
            "#H1-PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Ph_Rcd_Type_Cde = pnd_Cor_Super_Pin_Rcdtype_Lob_Nines__R_Field_2.newFieldInGroup("pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Ph_Rcd_Type_Cde", 
            "#H1-PH-RCD-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Cntrct_Lob_Cde = pnd_Cor_Super_Pin_Rcdtype_Lob_Nines__R_Field_2.newFieldInGroup("pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Cntrct_Lob_Cde", 
            "#H1-CNTRCT-LOB-CDE", FieldType.STRING, 1);
        pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Cntrct_Issue_Dte_Nines_Cmplmnt = pnd_Cor_Super_Pin_Rcdtype_Lob_Nines__R_Field_2.newFieldInGroup("pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Cntrct_Issue_Dte_Nines_Cmplmnt", 
            "#H1-CNTRCT-ISSUE-DTE-NINES-CMPLMNT", FieldType.NUMERIC, 8);
        pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Cntrct_Nbr = pnd_Cor_Super_Pin_Rcdtype_Lob_Nines__R_Field_2.newFieldInGroup("pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Cntrct_Nbr", 
            "#H1-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Cor_Super_Cntrct_Payee_Pin = localVariables.newFieldInRecord("pnd_Cor_Super_Cntrct_Payee_Pin", "#COR-SUPER-CNTRCT-PAYEE-PIN", FieldType.STRING, 
            21);

        pnd_Cor_Super_Cntrct_Payee_Pin__R_Field_3 = localVariables.newGroupInRecord("pnd_Cor_Super_Cntrct_Payee_Pin__R_Field_3", "REDEFINE", pnd_Cor_Super_Cntrct_Payee_Pin);
        pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Cntrct_Nbr = pnd_Cor_Super_Cntrct_Payee_Pin__R_Field_3.newFieldInGroup("pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Cntrct_Nbr", 
            "#H2-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Cntrct_Payee_Cde = pnd_Cor_Super_Cntrct_Payee_Pin__R_Field_3.newFieldInGroup("pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Cntrct_Payee_Cde", 
            "#H2-CNTRCT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Ph_Unique_Id_Nbr = pnd_Cor_Super_Cntrct_Payee_Pin__R_Field_3.newFieldInGroup("pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Ph_Unique_Id_Nbr", 
            "#H2-PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 7);
        pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Ph_Rcd_Type_Cde = pnd_Cor_Super_Cntrct_Payee_Pin__R_Field_3.newFieldInGroup("pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Ph_Rcd_Type_Cde", 
            "#H2-PH-RCD-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Crrnt_Dte_Tme = localVariables.newFieldInRecord("pnd_Crrnt_Dte_Tme", "#CRRNT-DTE-TME", FieldType.TIME);
        pnd_Crrnt_Dte_Tme_A = localVariables.newFieldInRecord("pnd_Crrnt_Dte_Tme_A", "#CRRNT-DTE-TME-A", FieldType.STRING, 15);

        pnd_Crrnt_Dte_Tme_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Crrnt_Dte_Tme_A__R_Field_4", "REDEFINE", pnd_Crrnt_Dte_Tme_A);
        pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_Tme_N = pnd_Crrnt_Dte_Tme_A__R_Field_4.newFieldInGroup("pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_Tme_N", "#CRRNT-DTE-TME-N", 
            FieldType.NUMERIC, 15);

        pnd_Crrnt_Dte_Tme_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Crrnt_Dte_Tme_A__R_Field_5", "REDEFINE", pnd_Crrnt_Dte_Tme_A);
        pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_N = pnd_Crrnt_Dte_Tme_A__R_Field_5.newFieldInGroup("pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_N", "#CRRNT-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Tme_N = pnd_Crrnt_Dte_Tme_A__R_Field_5.newFieldInGroup("pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Tme_N", "#CRRNT-TME-N", FieldType.NUMERIC, 
            7);
        pnd_Dest_Unit_Mfta = localVariables.newFieldInRecord("pnd_Dest_Unit_Mfta", "#DEST-UNIT-MFTA", FieldType.BOOLEAN, 1);
        pnd_Document_Key = localVariables.newFieldInRecord("pnd_Document_Key", "#DOCUMENT-KEY", FieldType.STRING, 34);

        pnd_Document_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Document_Key__R_Field_6", "REDEFINE", pnd_Document_Key);
        pnd_Document_Key_Pnd_Doc_Cabinet_Id = pnd_Document_Key__R_Field_6.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Cabinet_Id", "#DOC-CABINET-ID", FieldType.STRING, 
            14);

        pnd_Document_Key__R_Field_7 = pnd_Document_Key__R_Field_6.newGroupInGroup("pnd_Document_Key__R_Field_7", "REDEFINE", pnd_Document_Key_Pnd_Doc_Cabinet_Id);
        pnd_Document_Key_Pnd_Doc_Cabinet_Type = pnd_Document_Key__R_Field_7.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Cabinet_Type", "#DOC-CABINET-TYPE", 
            FieldType.STRING, 1);
        pnd_Document_Key_Pnd_Doc_Cabinet_Name = pnd_Document_Key__R_Field_7.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Cabinet_Name", "#DOC-CABINET-NAME", 
            FieldType.STRING, 13);

        pnd_Document_Key__R_Field_8 = pnd_Document_Key__R_Field_7.newGroupInGroup("pnd_Document_Key__R_Field_8", "REDEFINE", pnd_Document_Key_Pnd_Doc_Cabinet_Name);
        pnd_Document_Key_Pnd_Doc_Cabinet_Name_A12 = pnd_Document_Key__R_Field_8.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Cabinet_Name_A12", "#DOC-CABINET-NAME-A12", 
            FieldType.STRING, 12);
        pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte = pnd_Document_Key__R_Field_6.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte", "#DOC-TIAA-RCVD-DTE", 
            FieldType.DATE);
        pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst = pnd_Document_Key__R_Field_6.newFieldInGroup("pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst", 
            "#CASE-WORKPRCSS-MULTI-SUBRQST", FieldType.STRING, 9);

        pnd_Document_Key__R_Field_9 = pnd_Document_Key__R_Field_6.newGroupInGroup("pnd_Document_Key__R_Field_9", "REDEFINE", pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst);
        pnd_Document_Key_Pnd_Doc_Case_Ind = pnd_Document_Key__R_Field_9.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Case_Ind", "#DOC-CASE-IND", FieldType.STRING, 
            1);
        pnd_Document_Key_Pnd_Doc_Originating_Work_Prcss_Id = pnd_Document_Key__R_Field_9.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Originating_Work_Prcss_Id", 
            "#DOC-ORIGINATING-WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Document_Key_Pnd_Doc_Multi_Rqst_Ind = pnd_Document_Key__R_Field_9.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Multi_Rqst_Ind", "#DOC-MULTI-RQST-IND", 
            FieldType.STRING, 1);
        pnd_Document_Key_Pnd_Doc_Sub_Rqst_Ind = pnd_Document_Key__R_Field_9.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Sub_Rqst_Ind", "#DOC-SUB-RQST-IND", 
            FieldType.STRING, 1);
        pnd_Document_Key_Pnd_Doc_Entry_Dte_Tme = pnd_Document_Key__R_Field_6.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Entry_Dte_Tme", "#DOC-ENTRY-DTE-TME", 
            FieldType.TIME);
        pnd_Document_Type = localVariables.newFieldInRecord("pnd_Document_Type", "#DOCUMENT-TYPE", FieldType.STRING, 8);

        pnd_Document_Type__R_Field_10 = localVariables.newGroupInRecord("pnd_Document_Type__R_Field_10", "REDEFINE", pnd_Document_Type);
        pnd_Document_Type_Pnd_Document_Category = pnd_Document_Type__R_Field_10.newFieldInGroup("pnd_Document_Type_Pnd_Document_Category", "#DOCUMENT-CATEGORY", 
            FieldType.STRING, 1);
        pnd_Document_Type_Pnd_Document_Sub_Category = pnd_Document_Type__R_Field_10.newFieldInGroup("pnd_Document_Type_Pnd_Document_Sub_Category", "#DOCUMENT-SUB-CATEGORY", 
            FieldType.STRING, 3);
        pnd_Document_Type_Pnd_Document_Detail = pnd_Document_Type__R_Field_10.newFieldInGroup("pnd_Document_Type_Pnd_Document_Detail", "#DOCUMENT-DETAIL", 
            FieldType.STRING, 4);
        pnd_Errors = localVariables.newFieldArrayInRecord("pnd_Errors", "#ERRORS", FieldType.STRING, 1, new DbsArrayController(1, 10));
        pnd_High_Multi = localVariables.newFieldInRecord("pnd_High_Multi", "#HIGH-MULTI", FieldType.STRING, 1);
        pnd_H2_Ph_Unique_Id_Nbr_A = localVariables.newFieldInRecord("pnd_H2_Ph_Unique_Id_Nbr_A", "#H2-PH-UNIQUE-ID-NBR-A", FieldType.STRING, 12);

        pnd_H2_Ph_Unique_Id_Nbr_A__R_Field_11 = localVariables.newGroupInRecord("pnd_H2_Ph_Unique_Id_Nbr_A__R_Field_11", "REDEFINE", pnd_H2_Ph_Unique_Id_Nbr_A);
        pnd_H2_Ph_Unique_Id_Nbr_A_Pnd_H2_Ph_Unique_Id_Nbr_N = pnd_H2_Ph_Unique_Id_Nbr_A__R_Field_11.newFieldInGroup("pnd_H2_Ph_Unique_Id_Nbr_A_Pnd_H2_Ph_Unique_Id_Nbr_N", 
            "#H2-PH-UNIQUE-ID-NBR-N", FieldType.NUMERIC, 12);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 3);
        pnd_Mitssp1 = localVariables.newFieldInRecord("pnd_Mitssp1", "#MITSSP1", FieldType.STRING, 8);
        pnd_Move_Case_Ind = localVariables.newFieldInRecord("pnd_Move_Case_Ind", "#MOVE-CASE-IND", FieldType.BOOLEAN, 1);
        pnd_Nines = localVariables.newFieldInRecord("pnd_Nines", "#NINES", FieldType.NUMERIC, 15);
        pnd_Number = localVariables.newFieldInRecord("pnd_Number", "#NUMBER", FieldType.NUMERIC, 3);
        pnd_Old_Cabinet = localVariables.newFieldInRecord("pnd_Old_Cabinet", "#OLD-CABINET", FieldType.STRING, 13);

        pnd_Old_Cabinet__R_Field_12 = localVariables.newGroupInRecord("pnd_Old_Cabinet__R_Field_12", "REDEFINE", pnd_Old_Cabinet);
        pnd_Old_Cabinet_Pnd_Cabinet_Prefix = pnd_Old_Cabinet__R_Field_12.newFieldInGroup("pnd_Old_Cabinet_Pnd_Cabinet_Prefix", "#CABINET-PREFIX", FieldType.STRING, 
            1);
        pnd_Old_Cabinet_Pnd_Old_Pin_Nbr = pnd_Old_Cabinet__R_Field_12.newFieldInGroup("pnd_Old_Cabinet_Pnd_Old_Pin_Nbr", "#OLD-PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 8);
        pnd_Ssr_Added = localVariables.newFieldInRecord("pnd_Ssr_Added", "#SSR-ADDED", FieldType.BOOLEAN, 1);
        pnd_Ssr_Pin_Created_Array_A = localVariables.newFieldArrayInRecord("pnd_Ssr_Pin_Created_Array_A", "#SSR-PIN-CREATED-ARRAY-A", FieldType.STRING, 
            12, new DbsArrayController(1, 500));

        pnd_Ssr_Pin_Created_Array_A__R_Field_13 = localVariables.newGroupInRecord("pnd_Ssr_Pin_Created_Array_A__R_Field_13", "REDEFINE", pnd_Ssr_Pin_Created_Array_A);
        pnd_Ssr_Pin_Created_Array_A_Pnd_Ssr_Pin_Created_Array_N = pnd_Ssr_Pin_Created_Array_A__R_Field_13.newFieldArrayInGroup("pnd_Ssr_Pin_Created_Array_A_Pnd_Ssr_Pin_Created_Array_N", 
            "#SSR-PIN-CREATED-ARRAY-N", FieldType.NUMERIC, 12, new DbsArrayController(1, 500));
        pnd_Ssr_Wpid = localVariables.newFieldArrayInRecord("pnd_Ssr_Wpid", "#SSR-WPID", FieldType.STRING, 6, new DbsArrayController(1, 10));
        pnd_Store_Msg = localVariables.newFieldArrayInRecord("pnd_Store_Msg", "#STORE-MSG", FieldType.STRING, 70, new DbsArrayController(1, 2));
        pnd_Text_Object_Key = localVariables.newFieldInRecord("pnd_Text_Object_Key", "#TEXT-OBJECT-KEY", FieldType.STRING, 24);

        pnd_Text_Object_Key__R_Field_14 = localVariables.newGroupInRecord("pnd_Text_Object_Key__R_Field_14", "REDEFINE", pnd_Text_Object_Key);
        pnd_Text_Object_Key_Pnd_Cabinet_Id = pnd_Text_Object_Key__R_Field_14.newFieldInGroup("pnd_Text_Object_Key_Pnd_Cabinet_Id", "#CABINET-ID", FieldType.STRING, 
            14);
        pnd_Text_Object_Key_Pnd_Text_Pointer = pnd_Text_Object_Key__R_Field_14.newFieldInGroup("pnd_Text_Object_Key_Pnd_Text_Pointer", "#TEXT-POINTER", 
            FieldType.NUMERIC, 7);
        pnd_Text_Object_Key_Pnd_Text_Sqnce_Nbr = pnd_Text_Object_Key__R_Field_14.newFieldInGroup("pnd_Text_Object_Key_Pnd_Text_Sqnce_Nbr", "#TEXT-SQNCE-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Wpid_Change = localVariables.newFieldInRecord("pnd_Wpid_Change", "#WPID-CHANGE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Doc.reset();
        vw_cwf_Text.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Dest_Unit_Mfta.setInitialValue(false);
        pnd_Mitssp1.setInitialValue("MITSSP1");
        pnd_Move_Case_Ind.setInitialValue(false);
        pnd_Nines.setInitialValue(-2147483648);
        pnd_Ssr_Added.setInitialValue(false);
        pnd_Wpid_Change.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn6705() throws Exception
    {
        super("Cwfn6705");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFN6705", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) SG = OFF LS = 132 PS = 66;//Natural: FORMAT ( 1 ) SG = OFF LS = 132 PS = 66;//Natural: AT TOP OF PAGE ( 1 )
        //*  INITIALIZE
        pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: RESET MSG-INFO-SUB
        pdaCwfpdaun.getParm_Unit_Info_Sub_Pnd_Pnd_Unit_Cde().setValue("MFTA");                                                                                            //Natural: ASSIGN PARM-UNIT-INFO-SUB.##UNIT-CDE := 'MFTA'
        pnd_Crrnt_Dte_Tme_A.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                                                       //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO #CRRNT-DTE-TME-A
        pnd_Crrnt_Dte_Tme.setValue(Global.getTIMX());                                                                                                                     //Natural: MOVE *TIMX TO #CRRNT-DTE-TME
        //* **    GET REQUEST DATA
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                               //Natural: MOVE 'GET' TO #FUNCTION
        pdaCwfa5012.getCwfa5012_Rqst_Log_Dte_Tme().setValue(pnd_Rqst_Log_Dte_Tme);                                                                                        //Natural: MOVE #RQST-LOG-DTE-TME TO CWFA5012.RQST-LOG-DTE-TME
                                                                                                                                                                          //Natural: PERFORM CALLNAT-CWFN5011
        sub_Callnat_Cwfn5011();
        if (condition(Global.isEscape())) {return;}
        pnd_Ssr_Pin_Created_Array_A.getValue("*").reset();                                                                                                                //Natural: RESET #SSR-PIN-CREATED-ARRAY-A ( * ) #SSR-ADDED
        pnd_Ssr_Added.reset();
        //*   FOR MFTA AND SHARE SERVICE INDICATOR IS 'A'
        if (condition(pdaCwfa5012.getCwfa5012_Admin_Unit_Cde().equals("MFTA") && pnd_Shrd_Srvce_Ind.equals("A") && ! (pdaCwfa5012.getCwfa5012_Status_Cde().greaterOrEqual("4800")  //Natural: IF CWFA5012.ADMIN-UNIT-CDE = 'MFTA' AND #SHRD-SRVCE-IND = 'A' AND NOT ( CWFA5012.STATUS-CDE = '4800' THRU '4997' )
            && pdaCwfa5012.getCwfa5012_Status_Cde().lessOrEqual("4997"))))
        {
            //*  PH HAS ONLY MUTUAL FUND ACCOUNT/S; RQST APPLIES ONLY TO MF ACCTS
            //*  KEEP HISTORY
            pdaCwfa1800.getCwfa1800().setValuesByName(pdaCwfa5012.getCwfa5012());                                                                                         //Natural: MOVE BY NAME CWFA5012 TO CWFA1800
            //*  GET TA-SSR WPID
            DbsUtil.callnat(Cwfn6701.class , getCurrentProcessState(), pdaCwfa1800.getCwfa1800_Work_Prcss_Id(), pnd_Shrd_Srvce_Ind, pnd_Ssr_Wpid.getValue("*"),           //Natural: CALLNAT 'CWFN6701' CWFA1800.WORK-PRCSS-ID #SHRD-SRVCE-IND #SSR-WPID ( * ) MSG-INFO-SUB
                pdaCwfpda_M.getMsg_Info_Sub());
            if (condition(Global.isEscape())) return;
            pdaCwfa5012.getCwfa5012_Admin_Status_Cde().setValue("4800");                                                                                                  //Natural: MOVE '4800' TO CWFA5012.ADMIN-STATUS-CDE CWFA5012.STATUS-CDE
            pdaCwfa5012.getCwfa5012_Status_Cde().setValue("4800");
            pdaCwfa5012.getCwfa5012_Admin_Status_Updte_Oprtr_Cde().setValue(pnd_Mitssp1);                                                                                 //Natural: MOVE #MITSSP1 TO CWFA5012.ADMIN-STATUS-UPDTE-OPRTR-CDE CWFA5012.STATUS-UPDTE-OPRTR-CDE
            pdaCwfa5012.getCwfa5012_Status_Updte_Oprtr_Cde().setValue(pnd_Mitssp1);
            pdaCwfa5012.getCwfa5012_Admin_Status_Updte_Dte_Tme().setValue(pnd_Crrnt_Dte_Tme);                                                                             //Natural: MOVE #CRRNT-DTE-TME TO CWFA5012.ADMIN-STATUS-UPDTE-DTE-TME CWFA5012.STATUS-UPDTE-DTE-TME
            pdaCwfa5012.getCwfa5012_Status_Updte_Dte_Tme().setValue(pnd_Crrnt_Dte_Tme);
            pdaCwfa5012.getCwfa5012_Bypass_Mf_Acct().setValue("P");                                                                                                       //Natural: MOVE 'P' TO CWFA5012.BYPASS-MF-ACCT
            pdaCwfa5012.getCwfa5012_Wpid_Vldte_Ind().setValue("Y");                                                                                                       //Natural: MOVE 'Y' TO CWFA5012.WPID-VLDTE-IND
            pdaCwfa5012.getCwfa5012_Work_Prcss_Id().setValue(pnd_Ssr_Wpid.getValue(1));                                                                                   //Natural: MOVE #SSR-WPID ( 1 ) TO CWFA5012.WORK-PRCSS-ID
                                                                                                                                                                          //Natural: PERFORM UPDTE-DATE-USER
            sub_Updte_Date_User();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.callnat(Cwfn2330.class , getCurrentProcessState(), pdaCwfa5012.getCwfa5012(), pdaCwfa5012.getCwfa5012_Id(), pdaCwfa1800.getCwfa1800(),                //Natural: CALLNAT 'CWFN2330' CWFA5012 CWFA5012-ID CWFA1800 CWFA1800-ID MSG-INFO-SUB #CRRNT-DTE-TME-A
                pdaCwfa1800.getCwfa1800_Id(), pdaCwfpda_M.getMsg_Info_Sub(), pnd_Crrnt_Dte_Tme_A);
            if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM CREATE-INACTIVE-RECORD
            sub_Create_Inactive_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ")))                                                                              //Natural: IF ##ERROR-FIELD NE ' '
            {
                Global.setEscape(true); Global.setEscapeCode(EscapeType.BottomImmediate);                                                                                 //Natural: ESCAPE BOTTOM IMMEDIATE
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("UPDATE");                                                                                                        //Natural: MOVE 'UPDATE' TO #FUNCTION
                                                                                                                                                                          //Natural: PERFORM CALLNAT-CWFN5011
            sub_Callnat_Cwfn5011();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ")))                                                                              //Natural: IF ##ERROR-FIELD NE ' '
            {
                pnd_Store_Msg.getValue(1).setValue("Error at MFTA and SSR indicator is A");                                                                               //Natural: MOVE 'Error at MFTA and SSR indicator is A' TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                sub_Log_Msg();
                if (condition(Global.isEscape())) {return;}
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Cwfn5050.class , getCurrentProcessState(), pdaCwfa5012.getCwfa5012_Rqst_Log_Dte_Tme());                                                       //Natural: CALLNAT 'CWFN5050' CWFA5012.RQST-LOG-DTE-TME
            if (condition(Global.isEscape())) return;
            pnd_Dest_Unit_Mfta.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #DEST-UNIT-MFTA
            pnd_Remarks.setValue("Updated");                                                                                                                              //Natural: MOVE 'Updated' TO #REMARKS
            short decideConditionsMet2333 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SHRD-SRVCE-IND = 'A'
            if (condition(pnd_Shrd_Srvce_Ind.equals("A")))
            {
                decideConditionsMet2333++;
                pnd_Shrd_Srvce_A_Upd.nadd(1);                                                                                                                             //Natural: ADD 1 TO #SHRD-SRVCE-A-UPD
            }                                                                                                                                                             //Natural: WHEN #SHRD-SRVCE-IND = 'D'
            else if (condition(pnd_Shrd_Srvce_Ind.equals("D")))
            {
                decideConditionsMet2333++;
                pnd_Shrd_Srvce_D_Upd.nadd(1);                                                                                                                             //Natural: ADD 1 TO #SHRD-SRVCE-D-UPD
            }                                                                                                                                                             //Natural: WHEN #SHRD-SRVCE-IND = 'P'
            else if (condition(pnd_Shrd_Srvce_Ind.equals("P")))
            {
                decideConditionsMet2333++;
                pnd_Shrd_Srvce_P_Upd.nadd(1);                                                                                                                             //Natural: ADD 1 TO #SHRD-SRVCE-P-UPD
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  CREATE THE REPORT
            if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                            //Natural: IF *DEVICE EQ 'BATCH'
            {
                pnd_Count.nadd(1);                                                                                                                                        //Natural: ASSIGN #COUNT := #COUNT + 1
                getReports().write(1, new TabSetting(1),pnd_Count,new ColumnSpacing(1),pdaCwfa5012.getCwfa5012_Tiaa_Rcvd_Dte(),new ColumnSpacing(2),pdaCwfa5012.getCwfa5012_Work_Prcss_Id(),new  //Natural: WRITE ( 1 ) 1T #COUNT 1X CWFA5012.TIAA-RCVD-DTE 2X CWFA5012.WORK-PRCSS-ID 2X CWFA5012.PIN-NBR 2X CWFA5012.ORGNL-UNIT-CDE 3X CWFA5012.RQST-ORGN-CDE 3X CWFA5012.RQST-LOG-DTE-TME 3X #REMARKS
                    ColumnSpacing(2),pdaCwfa5012.getCwfa5012_Pin_Nbr(),new ColumnSpacing(2),pdaCwfa5012.getCwfa5012_Orgnl_Unit_Cde(),new ColumnSpacing(3),pdaCwfa5012.getCwfa5012_Rqst_Orgn_Cde(),new 
                    ColumnSpacing(3),pdaCwfa5012.getCwfa5012_Rqst_Log_Dte_Tme(),new ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   INITIATE THE VALUE FOR ELECTRONIC DOCUMENT FILE
        pnd_Document_Key_Pnd_Doc_Cabinet_Type.setValue("P");                                                                                                              //Natural: MOVE 'P' TO #DOC-CABINET-TYPE
        //*  MOVE CWFA5012.PIN-NBR        TO #DOC-CABINET-NAME    /* PIN-EXP
        //*  PIN-EXP
        pnd_Document_Key_Pnd_Doc_Cabinet_Name_A12.moveAll(pdaCwfa5012.getCwfa5012_Pin_Nbr());                                                                             //Natural: MOVE ALL CWFA5012.PIN-NBR TO #DOC-CABINET-NAME-A12
        pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte.setValue(pdaCwfa5012.getCwfa5012_Tiaa_Rcvd_Dte());                                                                         //Natural: MOVE CWFA5012.TIAA-RCVD-DTE TO #DOC-TIAA-RCVD-DTE
        pnd_Document_Key_Pnd_Doc_Case_Ind.setValue(pdaCwfa5012.getCwfa5012_Case_Ind());                                                                                   //Natural: MOVE CWFA5012.CASE-IND TO #DOC-CASE-IND
        pnd_Document_Key_Pnd_Doc_Originating_Work_Prcss_Id.setValue(pdaCwfa5012.getCwfa5012_Rqst_Work_Prcss_Id());                                                        //Natural: MOVE CWFA5012.RQST-WORK-PRCSS-ID TO #DOC-ORIGINATING-WORK-PRCSS-ID
        pnd_Document_Key_Pnd_Doc_Multi_Rqst_Ind.setValue(pdaCwfa5012.getCwfa5012_Multi_Rqst_Ind());                                                                       //Natural: MOVE CWFA5012.MULTI-RQST-IND TO #DOC-MULTI-RQST-IND
        pnd_Document_Key_Pnd_Doc_Sub_Rqst_Ind.setValue(pdaCwfa5012.getCwfa5012_Sub_Rqst_Sqnce_Ind());                                                                     //Natural: MOVE CWFA5012.SUB-RQST-SQNCE-IND TO #DOC-SUB-RQST-IND
        pnd_Document_Key_Pnd_Doc_Entry_Dte_Tme.reset();                                                                                                                   //Natural: RESET #DOC-ENTRY-DTE-TME
        short decideConditionsMet2366 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SHRD-SRVCE-IND = 'P' OR = 'D' OR ( #SHRD-SRVCE-IND = 'A' AND NOT #DEST-UNIT-MFTA )
        if (condition(((pnd_Shrd_Srvce_Ind.equals("P") || pnd_Shrd_Srvce_Ind.equals("D")) || (pnd_Shrd_Srvce_Ind.equals("A") && ! (pnd_Dest_Unit_Mfta.getBoolean())))))
        {
            decideConditionsMet2366++;
            pdaCwfa5010.getCwfa5010_Pin_Nbr().setValue(pdaCwfa5012.getCwfa5012_Pin_Nbr());                                                                                //Natural: MOVE CWFA5012.PIN-NBR TO CWFA5010.PIN-NBR
            //*  10/30/98 GH
            pnd_Move_Case_Ind.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #MOVE-CASE-IND
                                                                                                                                                                          //Natural: PERFORM CREATE-SSR-FOR-TA
            sub_Create_Ssr_For_Ta();
            if (condition(Global.isEscape())) {return;}
            //*    MOVE CWFA5012.PIN-NBR TO #SSR-PIN-CREATED-ARRAY-A(1) /* PIN-EXP
            //*  PIN-EXP
            pnd_Ssr_Pin_Created_Array_A_Pnd_Ssr_Pin_Created_Array_N.getValue(1).setValue(pdaCwfa5012.getCwfa5012_Pin_Nbr());                                              //Natural: MOVE CWFA5012.PIN-NBR TO #SSR-PIN-CREATED-ARRAY-N ( 1 )
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().greater(getZero())))             //Natural: IF MSG-INFO-SUB.##ERROR-FIELD GT ' ' OR MSG-INFO-SUB.##MSG-NR GT 0
            {
                pnd_Store_Msg.getValue(1).setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                            //Natural: MOVE MSG-INFO-SUB.##MSG TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                sub_Log_Msg();
                if (condition(Global.isEscape())) {return;}
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            //*    MOVE CWFA5012.PIN-NBR TO #SSR-PIN-CREATED-ARRAY-A(1) /* PIN-EXP
            //*  PIN-EXP
            pnd_Ssr_Pin_Created_Array_A_Pnd_Ssr_Pin_Created_Array_N.getValue(1).setValue(pdaCwfa5012.getCwfa5012_Pin_Nbr());                                              //Natural: MOVE CWFA5012.PIN-NBR TO #SSR-PIN-CREATED-ARRAY-N ( 1 )
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  THE FOLLOWING CODE WILL GENERATE SSR FOR JOINT OWNERS
        if (condition(pnd_Shrd_Srvce_Ind.equals("A")))                                                                                                                    //Natural: IF #SHRD-SRVCE-IND = 'A'
        {
            if (condition(((pdaCwfa5012.getCwfa5012_Rqst_Orgn_Cde().equals("C") || pdaCwfa5012.getCwfa5012_Rqst_Orgn_Cde().equals("N")) && pdaCwfa5012.getCwfa5012_Orgnl_Unit_Cde().notEquals("CRC")))) //Natural: IF CWFA5012.RQST-ORGN-CDE = 'C' OR = 'N' AND CWFA5012.ORGNL-UNIT-CDE NE 'CRC'
            {
                vw_cwf_Doc.startDatabaseRead                                                                                                                              //Natural: READ CWF-DOC WITH DOCUMENT-KEY = #DOCUMENT-KEY
                (
                "READ01",
                new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Document_Key.getBinary(), WcType.BY) },
                new Oc[] { new Oc("DOCUMENT_KEY", "ASC") }
                );
                READ01:
                while (condition(vw_cwf_Doc.readNextRow("READ01")))
                {
                    if (condition(cwf_Doc_Cabinet_Id.notEquals(pnd_Document_Key_Pnd_Doc_Cabinet_Id) || cwf_Doc_Tiaa_Rcvd_Dte.notEquals(pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte)  //Natural: IF CWF-DOC.CABINET-ID NE #DOC-CABINET-ID OR CWF-DOC.TIAA-RCVD-DTE NE #DOC-TIAA-RCVD-DTE OR CWF-DOC.CASE-WORKPRCSS-MULTI-SUBRQST NE #CASE-WORKPRCSS-MULTI-SUBRQST
                        || cwf_Doc_Case_Workprcss_Multi_Subrqst.notEquals(pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst)))
                    {
                        //* *
                        //* * THIS MEANS THAT THE CONTACT SHEET DID NOT CREATE A INOTMFA RECORD.
                        //* * WE WILL TREAT THIS AS AN ERROR. PREVIOUS UPDATES FOR THIS SSR WILL
                        //* * BE BACKED OUT.
                        //* *
                        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("DOC_TYPE");                                                                           //Natural: MOVE 'DOC_TYPE' TO MSG-INFO-SUB.##ERROR-FIELD
                        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("No INOTMFA Document Found For", pnd_Document_Key_Pnd_Doc_Cabinet_Id,         //Natural: COMPRESS 'No INOTMFA Document Found For' #DOC-CABINET-ID #DOC-TIAA-RCVD-DTE #CASE-WORKPRCSS-MULTI-SUBRQST INTO MSG-INFO-SUB.##MSG
                            pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte, pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst));
                        pnd_Store_Msg.getValue(1).setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                    //Natural: MOVE MSG-INFO-SUB.##MSG TO #STORE-MSG ( 1 )
                        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                //Natural: IF *DEVICE EQ 'BATCH'
                        {
                            getReports().write(1, pnd_Store_Msg.getValue(1));                                                                                             //Natural: WRITE ( 1 ) #STORE-MSG ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Shrd_Srvce_A_Add.nsubtract(1);                                                                                                            //Natural: ASSIGN #SHRD-SRVCE-A-ADD := #SHRD-SRVCE-A-ADD - 1
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                        sub_Log_Msg();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cwf_Doc_Document_Category.equals("I") && cwf_Doc_Document_Sub_Category.equals("NOT") && cwf_Doc_Document_Detail.equals("MFA")))         //Natural: IF DOCUMENT-CATEGORY = 'I' AND DOCUMENT-SUB-CATEGORY = 'NOT' AND DOCUMENT-DETAIL = 'MFA'
                    {
                        //* *    READ CWF TEXT FILE
                        pnd_Text_Object_Key_Pnd_Cabinet_Id.setValue(pnd_Document_Key_Pnd_Doc_Cabinet_Id);                                                                 //Natural: MOVE #DOC-CABINET-ID TO #CABINET-ID
                        pnd_Text_Object_Key_Pnd_Text_Pointer.setValue(cwf_Doc_Text_Pointer);                                                                              //Natural: MOVE CWF-DOC.TEXT-POINTER TO #TEXT-POINTER
                        //*  #SSR-PIN-CREATED-ARRAY-A(*)
                        pnd_Number.reset();                                                                                                                               //Natural: RESET #NUMBER #K
                        pnd_K.reset();
                        vw_cwf_Text.startDatabaseRead                                                                                                                     //Natural: READ ( 1 ) CWF-TEXT WITH TEXT-OBJECT-KEY = #TEXT-OBJECT-KEY
                        (
                        "READ02",
                        new Wc[] { new Wc("TEXT_OBJECT_KEY", ">=", pnd_Text_Object_Key, WcType.BY) },
                        new Oc[] { new Oc("TEXT_OBJECT_KEY", "ASC") },
                        1
                        );
                        READ02:
                        while (condition(vw_cwf_Text.readNextRow("READ02")))
                        {
                            FI:                                                                                                                                           //Natural: FOR #I 1 20
                            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
                            {
                                FJ:                                                                                                                                       //Natural: FOR #J 1 5
                                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(5)); pnd_J.nadd(1))
                                {
                                    pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Cntrct_Nbr.setValue(cwf_Text_Pnd_Text_Document_Line_Acc.getValue(pnd_I,pnd_J));                 //Natural: MOVE #TEXT-DOCUMENT-LINE-ACC ( #I,#J ) TO #H2-CNTRCT-NBR
                                    if (condition(cwf_Text_Pnd_Text_Document_Line_Acc.getValue(pnd_I,pnd_J).equals(" ")))                                                 //Natural: IF #TEXT-DOCUMENT-LINE-ACC ( #I,#J ) = ' '
                                    {
                                        if (true) break FI;                                                                                                               //Natural: ESCAPE BOTTOM ( FI. )
                                    }                                                                                                                                     //Natural: END-IF
                                    //*          DH2. HISTOGRAM COR-H2 FOR COR-SUPER-CNTRCT-PAYEE-PIN
                                    //*                   STARTING FROM #COR-SUPER-CNTRCT-PAYEE-PIN
                                    //*                 IF H2-CNTRCT-NBR NE #H2-CNTRCT-NBR
                                    //*                   ESCAPE BOTTOM IMMEDIATE
                                    //*                 END-IF
                                    //*              RESET #MDMA115 /*MDM PIN EXP
                                    //* MDM PIN EXP
                                    pdaMdma116.getPnd_Mdma116().reset();                                                                                                  //Natural: RESET #MDMA116
                                    //*              #MDMA115.#I-CONTRACT-NUMBER := #H2-CNTRCT-NBR
                                    pdaMdma116.getPnd_Mdma116_Pnd_I_Contract_Number().setValue(pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Cntrct_Nbr);                         //Natural: ASSIGN #MDMA116.#I-CONTRACT-NUMBER := #H2-CNTRCT-NBR
                                    //* MDM PIN EXP
                                    if (condition(Global.getDEVICE().equals("BATCH")))                                                                                    //Natural: IF *DEVICE = 'BATCH'
                                    {
                                        //*                CALLNAT 'MDMN115A' #MDMA115
                                        //* MDM PIN EXP
                                        DbsUtil.callnat(Mdmn116a.class , getCurrentProcessState(), pdaMdma116.getPnd_Mdma116());                                          //Natural: CALLNAT 'MDMN116A' #MDMA116
                                        if (condition(Global.isEscape())) return;
                                        //* MDM PIN EXP
                                    }                                                                                                                                     //Natural: ELSE
                                    else if (condition())
                                    {
                                        //*                CALLNAT 'MDMN115' #MDMA115
                                        //* MDM PIN EXP
                                        DbsUtil.callnat(Mdmn116.class , getCurrentProcessState(), pdaMdma116.getPnd_Mdma116());                                           //Natural: CALLNAT 'MDMN116' #MDMA116
                                        if (condition(Global.isEscape())) return;
                                        //* MDM PIN EXP
                                    }                                                                                                                                     //Natural: END-IF
                                    //*              IF #MDMA115.#O-RETURN-CODE = '0000'
                                    //* MDM PIN EXP
                                    if (condition(pdaMdma116.getPnd_Mdma116_Pnd_O_Return_Code().equals("0000")))                                                          //Natural: IF #MDMA116.#O-RETURN-CODE = '0000'
                                    {
                                        FOR01:                                                                                                                            //Natural: FOR #X 1 TO #O-OUTPUT-COUNT
                                        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pdaMdma116.getPnd_Mdma116_Pnd_O_Output_Count())); pnd_X.nadd(1))
                                        {
                                            //*               MOVE H2-PH-UNIQUE-ID-NBR TO #H2-PH-UNIQUE-ID-NBR-N
                                            //*                  MOVE #MDMA115.#O-PIN(#X) TO #H2-PH-UNIQUE-ID-NBR-N
                                            //* MDM PIN EXP
                                            pnd_H2_Ph_Unique_Id_Nbr_A_Pnd_H2_Ph_Unique_Id_Nbr_N.setValue(pdaMdma116.getPnd_Mdma116_Pnd_O_Pin_N12().getValue(pnd_X));      //Natural: MOVE #MDMA116.#O-PIN-N12 ( #X ) TO #H2-PH-UNIQUE-ID-NBR-N
                                            //* MDM PIN EXP
                                            DbsUtil.examine(new ExamineSource(pnd_Ssr_Pin_Created_Array_A.getValue("*")), new ExamineSearch(pnd_H2_Ph_Unique_Id_Nbr_A),   //Natural: EXAMINE #SSR-PIN-CREATED-ARRAY-A ( * ) FOR #H2-PH-UNIQUE-ID-NBR-A GIVING NUMBER #NUMBER
                                                new ExamineGivingNumber(pnd_Number));
                                            if (condition(pnd_Number.equals(getZero())))                                                                                  //Natural: IF #NUMBER = 0
                                            {
                                                FOR02:                                                                                                                    //Natural: FOR #K = 1 TO 500
                                                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(500)); pnd_K.nadd(1))
                                                {
                                                    if (condition(pnd_Ssr_Pin_Created_Array_A.getValue(pnd_K).equals(" ")))                                               //Natural: IF #SSR-PIN-CREATED-ARRAY-A ( #K ) = ' '
                                                    {
                                                        if (condition(true)) break;                                                                                       //Natural: ESCAPE BOTTOM
                                                    }                                                                                                                     //Natural: END-IF
                                                }                                                                                                                         //Natural: END-FOR
                                                if (condition(Global.isEscape()))
                                                {
                                                    if (condition(Global.isEscapeBottom())) break;
                                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                                    else if (condition(Global.isEscapeTop())) continue;
                                                    else if (condition(Global.isEscapeRoutine())) return;
                                                    else break;
                                                }
                                                pnd_Ssr_Pin_Created_Array_A.getValue(pnd_K).setValue(pnd_H2_Ph_Unique_Id_Nbr_A);                                          //Natural: MOVE #H2-PH-UNIQUE-ID-NBR-A TO #SSR-PIN-CREATED-ARRAY-A ( #K )
                                                //*                    MOVE H2-PH-UNIQUE-ID-NBR TO CWFA5010.PIN-NBR
                                                //*                    MOVE #MDMA115.#O-PIN(#X) TO CWFA5010.PIN-NBR
                                                //* MDM PIN EXP
                                                pdaCwfa5010.getCwfa5010_Pin_Nbr().setValue(pdaMdma116.getPnd_Mdma116_Pnd_O_Pin_N12().getValue(pnd_X));                    //Natural: MOVE #MDMA116.#O-PIN-N12 ( #X ) TO CWFA5010.PIN-NBR
                                                //* MDM PIN EXP
                                                //*  10/30/98 GH
                                                pnd_Move_Case_Ind.setValue(false);                                                                                        //Natural: MOVE FALSE TO #MOVE-CASE-IND
                                                //*  MULTIPLE TIMES
                                                                                                                                                                          //Natural: PERFORM CREATE-SSR-FOR-TA
                                                sub_Create_Ssr_For_Ta();
                                                if (condition(Global.isEscape()))
                                                {
                                                    if (condition(Global.isEscapeBottom())) break;
                                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                                    else if (condition(Global.isEscapeTop())) continue;
                                                    else if (condition(Global.isEscapeRoutine())) return;
                                                    else break;
                                                }
                                                if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().greater(getZero()))) //Natural: IF MSG-INFO-SUB.##ERROR-FIELD GT ' ' OR MSG-INFO-SUB.##MSG-NR GT 0
                                                {
                                                    pnd_Store_Msg.getValue(1).setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                        //Natural: MOVE MSG-INFO-SUB.##MSG TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                                                    sub_Log_Msg();
                                                    if (condition(Global.isEscape()))
                                                    {
                                                        if (condition(Global.isEscapeBottom())) break;
                                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                                        else if (condition(Global.isEscapeTop())) continue;
                                                        else if (condition(Global.isEscapeRoutine())) return;
                                                        else break;
                                                    }
                                                    if (condition(true)) return;                                                                                          //Natural: ESCAPE ROUTINE
                                                }                                                                                                                         //Natural: END-IF
                                            }                                                                                                                             //Natural: END-IF
                                            //*                END-HISTOGRAM
                                            //*                END-FOR   /* MDMN115 LOOP
                                            //* MDM PIN EXP
                                            //*  MDMN116 LOOP
                                        }                                                                                                                                 //Natural: END-FOR
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom("FJ"))) break;
                                            else if (condition(Global.isEscapeBottomImmediate("FJ"))) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                        //* MDM PIN EXP
                                    }                                                                                                                                     //Natural: END-IF
                                    //*  INTER FOR LOOP
                                }                                                                                                                                         //Natural: END-FOR
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("FI"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("FI"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                //*  OUTER FOR LOOP
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  END OF THE TEXT FILE READ      */
                        }                                                                                                                                                 //Natural: END-READ
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  AFTER PROCESSED ONE TEXT RECORD
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                        //*  END IF OF 'INOTMFA'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  END OF THE DOCUMENT FILE READ  */
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //*  READ COR FILE                  */
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-COR-FILE
                sub_Process_Cor_File();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wpid_Change.reset();                                                                                                                                          //Natural: RESET #WPID-CHANGE
        if (condition(pnd_Ssr_Added.getBoolean() && pnd_Shrd_Srvce_Ind.equals("A")))                                                                                      //Natural: IF #SSR-ADDED AND #SHRD-SRVCE-IND = 'A'
        {
            pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Key().setValue("S CWF-MIT-SWEEP");                                                                                     //Natural: MOVE 'S CWF-MIT-SWEEP' TO #GEN-TBL-KEY
            pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Code().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "ACCT-BASED-WPID-", pdaCwfa5012.getCwfa5012_Work_Prcss_Id())); //Natural: COMPRESS 'ACCT-BASED-WPID-' CWFA5012.WORK-PRCSS-ID TO #GEN-TBL-CODE LEAVING NO SPACE
            DbsUtil.callnat(Cwfn5312.class , getCurrentProcessState(), pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Key(), pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Desc());      //Natural: CALLNAT 'CWFN5312' #GEN-TBL-KEY #GEN-TBL-DESC
            if (condition(Global.isEscape())) return;
            if (condition(pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Desc().notEquals(" ")))                                                                                  //Natural: IF #GEN-TBL-DESC NE ' '
            {
                pnd_Wpid_Change.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO #WPID-CHANGE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* **   CHECK IF PART OF A MULTI-RQST INDICATOR IS '0'
        if (condition(pnd_Ssr_Added.getBoolean() && (pdaCwfa5012.getCwfa5012_Multi_Rqst_Ind().equals("0") || pnd_Wpid_Change.getBoolean())))                              //Natural: IF #SSR-ADDED AND ( CWFA5012.MULTI-RQST-IND EQ '0' OR #WPID-CHANGE )
        {
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                           //Natural: MOVE 'GET' TO #FUNCTION
            pdaCwfa5012.getCwfa5012_Rqst_Log_Dte_Tme().setValue(pnd_Rqst_Log_Dte_Tme);                                                                                    //Natural: MOVE #RQST-LOG-DTE-TME TO CWFA5012.RQST-LOG-DTE-TME
                                                                                                                                                                          //Natural: PERFORM CALLNAT-CWFN5011
            sub_Callnat_Cwfn5011();
            if (condition(Global.isEscape())) {return;}
            //*  KEEP HISTORY
            pdaCwfa1800.getCwfa1800().setValuesByName(pdaCwfa5012.getCwfa5012());                                                                                         //Natural: MOVE BY NAME CWFA5012 TO CWFA1800
            if (condition(pnd_Wpid_Change.getBoolean()))                                                                                                                  //Natural: IF #WPID-CHANGE
            {
                pdaCwfa5012.getCwfa5012_Work_Prcss_Id().setValue(pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Desc());                                                          //Natural: MOVE #GEN-TBL-DESC TO CWFA5012.WORK-PRCSS-ID
                pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Desc().reset();                                                                                                    //Natural: RESET #GEN-TBL-DESC
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaCwfa5012.getCwfa5012_Multi_Rqst_Ind().equals("0")))                                                                                          //Natural: IF CWFA5012.MULTI-RQST-IND EQ '0'
            {
                pdaCwfa5012.getCwfa5012_Multi_Rqst_Ind().setValue("1");                                                                                                   //Natural: MOVE '1' TO CWFA5012.MULTI-RQST-IND
                pdaCwfa5012.getCwfa5012_Admin_Status_Cde().setValue("LINK");                                                                                              //Natural: MOVE 'LINK' TO CWFA5012.ADMIN-STATUS-CDE
            }                                                                                                                                                             //Natural: END-IF
            pdaCwfa5012.getCwfa5012_Admin_Status_Updte_Oprtr_Cde().setValue(pnd_Mitssp1);                                                                                 //Natural: MOVE #MITSSP1 TO CWFA5012.ADMIN-STATUS-UPDTE-OPRTR-CDE
            pdaCwfa5012.getCwfa5012_Admin_Status_Updte_Dte_Tme().setValue(pnd_Crrnt_Dte_Tme);                                                                             //Natural: MOVE #CRRNT-DTE-TME TO CWFA5012.ADMIN-STATUS-UPDTE-DTE-TME
                                                                                                                                                                          //Natural: PERFORM UPDTE-DATE-USER
            sub_Updte_Date_User();
            if (condition(Global.isEscape())) {return;}
            //* **  COMPUTE CLOCKS
            DbsUtil.callnat(Cwfn2330.class , getCurrentProcessState(), pdaCwfa5012.getCwfa5012(), pdaCwfa5012.getCwfa5012_Id(), pdaCwfa1800.getCwfa1800(),                //Natural: CALLNAT 'CWFN2330' CWFA5012 CWFA5012-ID CWFA1800 CWFA1800-ID MSG-INFO-SUB #CRRNT-DTE-TME-A
                pdaCwfa1800.getCwfa1800_Id(), pdaCwfpda_M.getMsg_Info_Sub(), pnd_Crrnt_Dte_Tme_A);
            if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM CREATE-INACTIVE-RECORD
            sub_Create_Inactive_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ")))                                                                              //Natural: IF ##ERROR-FIELD NE ' '
            {
                Global.setEscape(true); Global.setEscapeCode(EscapeType.BottomImmediate);                                                                                 //Natural: ESCAPE BOTTOM IMMEDIATE
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("UPDATE");                                                                                                        //Natural: MOVE 'UPDATE' TO #FUNCTION
                                                                                                                                                                          //Natural: PERFORM CALLNAT-CWFN5011
            sub_Callnat_Cwfn5011();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().equals(" ")))                                                                                 //Natural: IF ##ERROR-FIELD EQ ' '
            {
                //* ** UPDATE RQST-ID FIELD OF HISTORY IF NEEDED
                DbsUtil.callnat(Cwfn5050.class , getCurrentProcessState(), pdaCwfa5012.getCwfa5012_Rqst_Log_Dte_Tme());                                                   //Natural: CALLNAT 'CWFN5050' CWFA5012.RQST-LOG-DTE-TME
                if (condition(Global.isEscape())) return;
                //*  WRITE TO ERROR LOG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Store_Msg.getValue(1).setValue("Error at MULTI-REQ-IND is 0");                                                                                        //Natural: MOVE 'Error at MULTI-REQ-IND is 0' TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                sub_Log_Msg();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-SSR-FOR-TA
        //*  READ DOCUMENT FILE TO RECEIVE THE DOCUMENT INFORMATION
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-INACTIVE-RECORD
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MULTI-RQST-IND-ROUTINE
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COR-FILE
        //*  #MDMA110.#I-PIN := #H1-PH-UNIQUE-ID-NBR
        //*    #MDMA115.#I-CONTRACT-NUMBER := #H2-CNTRCT-NBR
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDTE-DATE-USER
        //* *************************** *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALLNAT-CWFN5011
        //* *************************** *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOG-MSG
        //*  ************
        //*  ******************************************************************                                                                                           //Natural: ON ERROR
    }
    private void sub_Create_Ssr_For_Ta() throws Exception                                                                                                                 //Natural: CREATE-SSR-FOR-TA
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        if (condition(pdaCwfa5012.getCwfa5012_Pin_Nbr().equals(pdaCwfa5010.getCwfa5010_Pin_Nbr())))                                                                       //Natural: IF CWFA5012.PIN-NBR EQ CWFA5010.PIN-NBR
        {
            //*  A MULTI-RQST OF ORIGINAL PIN ADDED
            pnd_Ssr_Added.setValue(true);                                                                                                                                 //Natural: MOVE TRUE TO #SSR-ADDED
            pdaCwfa5010.getCwfa5010_Bypass_Mf_Acct().getValue(1).setValue("P");                                                                                           //Natural: MOVE 'P' TO CWFA5010.BYPASS-MF-ACCT ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfa5010.getCwfa5010_Bypass_Mf_Acct().getValue(1).setValue(" ");                                                                                           //Natural: MOVE ' ' TO CWFA5010.BYPASS-MF-ACCT ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfa5010.getCwfa5010_Nbr_Of_Rqsts().setValue(1);                                                                                                               //Natural: MOVE 1 TO CWFA5010.NBR-OF-RQSTS
        pdaCwfa5010.getCwfa5010_Rcvd_Dte().setValue(pdaCwfa5012.getCwfa5012_Tiaa_Rcvd_Dte());                                                                             //Natural: MOVE CWFA5012.TIAA-RCVD-DTE TO CWFA5010.RCVD-DTE
        pdaCwfa5010.getCwfa5010_Rqst_Entry_Oprtr_Cde().setValue(pnd_Mitssp1);                                                                                             //Natural: MOVE #MITSSP1 TO CWFA5010.RQST-ENTRY-OPRTR-CDE
        pdaCwfa5010.getCwfa5010_Rqst_Orgn_Cde().setValue(pdaCwfa5012.getCwfa5012_Rqst_Orgn_Cde());                                                                        //Natural: MOVE CWFA5012.RQST-ORGN-CDE TO CWFA5010.RQST-ORGN-CDE
        pdaCwfa5010.getCwfa5010_Orgnl_Unit_Cde().setValue("MFTA");                                                                                                        //Natural: MOVE 'MFTA' TO CWFA5010.ORGNL-UNIT-CDE
        //*  11/03/98 GH
        if (condition(pnd_Move_Case_Ind.getBoolean()))                                                                                                                    //Natural: IF #MOVE-CASE-IND
        {
            pdaCwfa5010.getCwfa5010_Rqst_Rgn_Cde().setValue(pdaCwfa5012.getCwfa5012_Rqst_Rgn_Cde());                                                                      //Natural: MOVE CWFA5012.RQST-RGN-CDE TO CWFA5010.RQST-RGN-CDE
            pdaCwfa5010.getCwfa5010_Rqst_Spcl_Dsgntn_Cde().setValue(pdaCwfa5012.getCwfa5012_Rqst_Spcl_Dsgntn_Cde());                                                      //Natural: MOVE CWFA5012.RQST-SPCL-DSGNTN-CDE TO CWFA5010.RQST-SPCL-DSGNTN-CDE
            pdaCwfa5010.getCwfa5010_Rqst_Brnch_Cde().setValue(pdaCwfa5012.getCwfa5012_Rqst_Brnch_Cde());                                                                  //Natural: MOVE CWFA5012.RQST-BRNCH-CDE TO CWFA5010.RQST-BRNCH-CDE
            pdaCwfa5010.getCwfa5010_Spcl_Srcs().getValue("*").setValue(pdaCwfa5012.getCwfa5012_Spcl_Policy_Srce_Cde().getValue("*"));                                     //Natural: MOVE CWFA5012.SPCL-POLICY-SRCE-CDE ( * ) TO CWFA5010.SPCL-SRCS ( * )
            pdaCwfa5010.getCwfa5010_Instn_Cde().setValue(pdaCwfa5012.getCwfa5012_Instn_Cde());                                                                            //Natural: MOVE CWFA5012.INSTN-CDE TO CWFA5010.INSTN-CDE
            pdaCwfa5010.getCwfa5010_Rqst_Instn_Cde().getValue(1).setValue(pdaCwfa5012.getCwfa5012_Rqst_Instn_Cde());                                                      //Natural: MOVE CWFA5012.RQST-INSTN-CDE TO CWFA5010.RQST-INSTN-CDE ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  11/03/98
            DbsUtil.callnat(Cwfn5380.class , getCurrentProcessState(), pdaCwfa5010.getCwfa5010_Pin_Nbr(), pdaCwfa5010.getCwfa5010_Rqst_Rgn_Cde(), pdaCwfa5010.getCwfa5010_Rqst_Spcl_Dsgntn_Cde(),  //Natural: CALLNAT 'CWFN5380' CWFA5010.PIN-NBR CWFA5010.RQST-RGN-CDE CWFA5010.RQST-SPCL-DSGNTN-CDE CWFA5010.RQST-BRNCH-CDE CWFA5010.INSTN-CDE CWFA5010.SPCL-SRCS ( * ) CWFA5010.MF-ACCT-IND MSG-INFO-SUB
                pdaCwfa5010.getCwfa5010_Rqst_Brnch_Cde(), pdaCwfa5010.getCwfa5010_Instn_Cde(), pdaCwfa5010.getCwfa5010_Spcl_Srcs().getValue("*"), pdaCwfa5010.getCwfa5010_Mf_Acct_Ind(), 
                pdaCwfpda_M.getMsg_Info_Sub());
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfa5010.getCwfa5010_Mail_Item_No().setValue(pdaCwfa5012.getCwfa5012_Mail_Item_Nbr().getValue(1));                                                             //Natural: MOVE CWFA5012.MAIL-ITEM-NBR ( 1 ) TO CWFA5010.MAIL-ITEM-NO
        pdaCwfa5010.getCwfa5010_Print_Prefix_Ind().setValue(pdaCwfa5012.getCwfa5012_Print_Prefix_Ind());                                                                  //Natural: MOVE CWFA5012.PRINT-PREFIX-IND TO CWFA5010.PRINT-PREFIX-IND
        pdaCwfa5010.getCwfa5010_Log_Insttn_Srce_Cde().setValue(pdaCwfa5012.getCwfa5012_Log_Insttn_Srce_Cde());                                                            //Natural: MOVE CWFA5012.LOG-INSTTN-SRCE-CDE TO CWFA5010.LOG-INSTTN-SRCE-CDE
        pdaCwfa5010.getCwfa5010_Log_Rqstr_Cde().setValue(pdaCwfa5012.getCwfa5012_Log_Rqstr_Cde());                                                                        //Natural: MOVE CWFA5012.LOG-RQSTR-CDE TO CWFA5010.LOG-RQSTR-CDE
        pdaCwfa5010.getCwfa5010_Cntct_Dte_Tme_N().setValue(pdaCwfa5012.getCwfa5012_Cntct_Dte_Tme());                                                                      //Natural: MOVE CWFA5012.CNTCT-DTE-TME TO CWFA5010.CNTCT-DTE-TME-N
        if (condition(! (DbsUtil.maskMatches(pdaCwfa5010.getCwfa5010_Cntct_Dte_Tme_A(),"NNNNNNNNNNNNNNN")) || pdaCwfa5010.getCwfa5010_Cntct_Dte_Tme_A().less("198001010000000")  //Natural: IF CWFA5010.CNTCT-DTE-TME-A NE MASK ( NNNNNNNNNNNNNNN ) OR CWFA5010.CNTCT-DTE-TME-A LT '198001010000000' OR CWFA5010.CNTCT-DTE-TME-A NE MASK ( ....01:12......... ) OR CWFA5010.CNTCT-DTE-TME-A NE MASK ( ......01:31....... ) OR CWFA5010.CNTCT-DTE-TME-A NE MASK ( ........00:24..... ) OR CWFA5010.CNTCT-DTE-TME-A NE MASK ( ..........00:59... ) OR CWFA5010.CNTCT-DTE-TME-A NE MASK ( ............000:599 )
            || ! (DbsUtil.maskMatches(pdaCwfa5010.getCwfa5010_Cntct_Dte_Tme_A(),"....01:12.........")) || ! (DbsUtil.maskMatches(pdaCwfa5010.getCwfa5010_Cntct_Dte_Tme_A(),"......01:31.......")) 
            || ! (DbsUtil.maskMatches(pdaCwfa5010.getCwfa5010_Cntct_Dte_Tme_A(),"........00:24.....")) || ! (DbsUtil.maskMatches(pdaCwfa5010.getCwfa5010_Cntct_Dte_Tme_A(),"..........00:59...")) 
            || ! (DbsUtil.maskMatches(pdaCwfa5010.getCwfa5010_Cntct_Dte_Tme_A(),"............000:599"))))
        {
            vw_cwf_Doc.startDatabaseRead                                                                                                                                  //Natural: READ ( 1 ) CWF-DOC WITH DOCUMENT-KEY = #DOCUMENT-KEY
            (
            "RDOC1",
            new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Document_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("DOCUMENT_KEY", "ASC") },
            1
            );
            RDOC1:
            while (condition(vw_cwf_Doc.readNextRow("RDOC1")))
            {
                if (condition(cwf_Doc_Cabinet_Id.notEquals(pnd_Document_Key_Pnd_Doc_Cabinet_Id) || cwf_Doc_Tiaa_Rcvd_Dte.notEquals(pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte)  //Natural: IF CWF-DOC.CABINET-ID NE #DOC-CABINET-ID OR CWF-DOC.TIAA-RCVD-DTE NE #DOC-TIAA-RCVD-DTE OR CWF-DOC.CASE-WORKPRCSS-MULTI-SUBRQST NE #CASE-WORKPRCSS-MULTI-SUBRQST
                    || cwf_Doc_Case_Workprcss_Multi_Subrqst.notEquals(pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst)))
                {
                    if (condition(vw_cwf_Doc.getAstCOUNTER().equals(1)))                                                                                                  //Natural: IF *COUNTER ( RDOC1. ) EQ 1
                    {
                        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("DOC_TYPE");                                                                           //Natural: MOVE 'DOC_TYPE' TO MSG-INFO-SUB.##ERROR-FIELD
                        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("No MF document found for", pnd_Document_Key_Pnd_Doc_Cabinet_Id,              //Natural: COMPRESS 'No MF document found for' #DOC-CABINET-ID #DOC-TIAA-RCVD-DTE #CASE-WORKPRCSS-MULTI-SUBRQST INTO MSG-INFO-SUB.##MSG
                            pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte, pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst));
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                        sub_Log_Msg();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RDOC1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RDOC1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                pdaCwfa5010.getCwfa5010_Cntct_Dte_Tme_A().setValueEdited(cwf_Doc_Entry_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                    //Natural: MOVE EDITED CWF-DOC.ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST ) TO CWFA5010.CNTCT-DTE-TME-A
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfa5010.getCwfa5010_Trade_Dte_Tme_A().setValueEdited(pdaCwfa5012.getCwfa5012_Trade_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                          //Natural: MOVE EDITED CWFA5012.TRADE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO CWFA5010.TRADE-DTE-TME-A
        //*  GET TA-SSR WPID
        DbsUtil.callnat(Cwfn6701.class , getCurrentProcessState(), pdaCwfa5012.getCwfa5012_Work_Prcss_Id(), pnd_Shrd_Srvce_Ind, pnd_Ssr_Wpid.getValue("*"),               //Natural: CALLNAT 'CWFN6701' CWFA5012.WORK-PRCSS-ID #SHRD-SRVCE-IND #SSR-WPID ( * ) MSG-INFO-SUB
            pdaCwfpda_M.getMsg_Info_Sub());
        if (condition(Global.isEscape())) return;
        pdaCwfa5010.getCwfa5010_Unit_Cde().getValue(1).setValue("MFTA");                                                                                                  //Natural: MOVE 'MFTA' TO CWFA5010.UNIT-CDE ( 1 )
        pdaCwfa5010.getCwfa5010_Wpid_Vldte_Ind().getValue(1).setValue("Y");                                                                                               //Natural: MOVE 'Y' TO CWFA5010.WPID-VLDTE-IND ( 1 )
        pdaCwfa5010.getCwfa5010_Status_Cde().getValue(1).setValue("4800");                                                                                                //Natural: MOVE '4800' TO CWFA5010.STATUS-CDE ( 1 )
        //*   START CHANGE   10/30/98 GH
        if (condition(pnd_Move_Case_Ind.getBoolean()))                                                                                                                    //Natural: IF #MOVE-CASE-IND
        {
            pdaCwfa5010.getCwfa5010_Case_Ind().getValue(1).setValue(pdaCwfa5012.getCwfa5012_Case_Ind());                                                                  //Natural: MOVE CWFA5012.CASE-IND TO CWFA5010.CASE-IND ( 1 )
            //*  11/14/00 G HONG
            pnd_Case_Ind_Save.setValue(pdaCwfa5012.getCwfa5012_Case_Ind());                                                                                               //Natural: MOVE CWFA5012.CASE-IND TO #CASE-IND-SAVE
            pdaCwfa5010.getCwfa5010_Sub_Rqst_Sqnce_Ind().getValue(1).setValue("0");                                                                                       //Natural: MOVE '0' TO CWFA5010.SUB-RQST-SQNCE-IND ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfa5012.getCwfa5012_Case_Ind().reset();                                                                                                                   //Natural: RESET CWFA5012.CASE-IND CWFA5010.CASE-IND ( 1 ) CWFA5010.SUB-RQST-SQNCE-IND ( 1 )
            pdaCwfa5010.getCwfa5010_Case_Ind().getValue(1).reset();
            pdaCwfa5010.getCwfa5010_Sub_Rqst_Sqnce_Ind().getValue(1).reset();
            //*   END CHANGE     10/30/98 GH
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfa5010.getCwfa5010_Mj_Pull_Ind().getValue(1).setValue("I");                                                                                                  //Natural: MOVE 'I' TO CWFA5010.MJ-PULL-IND ( 1 )
        pdaCwfa5010.getCwfa5010_Crprte_Status_Ind().getValue(1).setValue("0");                                                                                            //Natural: MOVE '0' TO CWFA5010.CRPRTE-STATUS-IND ( 1 )
        pdaCwfa5010.getCwfa5010_Elctrnc_Fldr_Ind().getValue(1).setValue(pdaCwfa5012.getCwfa5012_Elctrnc_Fldr_Ind());                                                      //Natural: MOVE CWFA5012.ELCTRNC-FLDR-IND TO CWFA5010.ELCTRNC-FLDR-IND ( 1 )
        pdaCwfa5010.getCwfa5010_Rescan_Ind().setValue("N");                                                                                                               //Natural: MOVE 'N' TO CWFA5010.RESCAN-IND
        pdaCwfa5010.getCwfa5010_Work_Rqst_Prty_Ind().getValue(1).setValue(pdaCwfa5012.getCwfa5012_Crrnt_Prrty_Cde());                                                     //Natural: MOVE CWFA5012.CRRNT-PRRTY-CDE TO CWFA5010.WORK-RQST-PRTY-IND ( 1 )
        pdaCwfa5010.getCwfa5010_Work_Id().getValue(1).setValue(pnd_Ssr_Wpid.getValue(1));                                                                                 //Natural: MOVE #SSR-WPID ( 1 ) TO CWFA5010.WORK-ID ( 1 )
                                                                                                                                                                          //Natural: PERFORM MULTI-RQST-IND-ROUTINE
        sub_Multi_Rqst_Ind_Routine();
        if (condition(Global.isEscape())) {return;}
        pdaCwfa5010.getCwfa5010_Effctve_Dte().getValue(1).setValue(pdaCwfa5012.getCwfa5012_Effctve_Dte());                                                                //Natural: MOVE CWFA5012.EFFCTVE-DTE TO CWFA5010.EFFCTVE-DTE ( 1 )
        pdaCwfa5010.getCwfa5010_Trnsctn_Dte().getValue(1).setValue(pdaCwfa5012.getCwfa5012_Trnsctn_Dte());                                                                //Natural: MOVE CWFA5012.TRNSCTN-DTE TO CWFA5010.TRNSCTN-DTE ( 1 )
        pdaCwfa5010.getCwfa5010_Trans_Dte().getValue(1).setValue(pdaCwfa5012.getCwfa5012_Trans_Dte());                                                                    //Natural: MOVE CWFA5012.TRANS-DTE TO CWFA5010.TRANS-DTE ( 1 )
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("EDIT");                                                                                                              //Natural: MOVE 'EDIT' TO #FUNCTION
        DbsUtil.callnat(Cwfn5010.class , getCurrentProcessState(), pdaCwfa5010.getCwfa5010(), pdaCwfa5010.getCwfa5010_Id(), pdaCwfa5011.getCwfa5011(),                    //Natural: CALLNAT 'CWFN5010' CWFA5010 CWFA5010-ID CWFA5011 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB #ERRORS ( * ) PARM-UNIT-INFO-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pnd_Errors.getValue("*"), 
            pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().greater(getZero())))                 //Natural: IF MSG-INFO-SUB.##ERROR-FIELD GT ' ' OR MSG-INFO-SUB.##MSG-NR GT 0
        {
            pnd_Store_Msg.getValue(1).setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                //Natural: MOVE MSG-INFO-SUB.##MSG TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
            sub_Log_Msg();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: MOVE 'STORE' TO #FUNCTION
        DbsUtil.callnat(Cwfn5010.class , getCurrentProcessState(), pdaCwfa5010.getCwfa5010(), pdaCwfa5010.getCwfa5010_Id(), pdaCwfa5011.getCwfa5011(),                    //Natural: CALLNAT 'CWFN5010' CWFA5010 CWFA5010-ID CWFA5011 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB #ERRORS ( * ) PARM-UNIT-INFO-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pnd_Errors.getValue("*"), 
            pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().greater(getZero())))                 //Natural: IF MSG-INFO-SUB.##ERROR-FIELD GT ' ' OR MSG-INFO-SUB.##MSG-NR GT 0
        {
            pnd_Store_Msg.getValue(1).setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                //Natural: MOVE MSG-INFO-SUB.##MSG TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
            sub_Log_Msg();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF NO CASE-IND BEFORE, THEN CWFN5010 SHOULD GENERATE A NEW ONE
        //*                                                 10/30/98  GH
        //*  10/30/98  GH
        pnd_Case_Ind_New.setValue(pdaCwfa5010.getCwfa5010_Case_Ind().getValue(1));                                                                                        //Natural: MOVE CWFA5010.CASE-IND ( 1 ) TO #CASE-IND-NEW
        //*  COPY ELECTRONIC DOCUMENTS FROM THE ORIGINAL MIT REQ. TO
        //*  THE NEWLY ADDED REQ.
        pdaEfsa9120.getEfsa9120_Action().setValue("CD");                                                                                                                  //Natural: MOVE 'CD' TO EFSA9120.ACTION
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: MOVE 'P' TO EFSA9120.CABINET-PREFIX
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(pdaCwfa5010.getCwfa5010_Pin_Nbr());                                                                                    //Natural: MOVE CWFA5010.PIN-NBR TO EFSA9120.PIN-NBR
        pdaEfsa9120.getEfsa9120_Rqst_Id().getValue(1).setValueEdited(pdaCwfa5010.getCwfa5010_Rcvd_Dte(),new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED CWFA5010.RCVD-DTE ( EM = YYYYMMDD ) TO EFSA9120.RQST-ID ( 1 )
        pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue(pdaCwfa5010.getCwfa5010_Work_Id().getValue(1));                                                               //Natural: MOVE CWFA5010.WORK-ID ( 1 ) TO EFSA9120.WPID ( 1 )
        pdaEfsa9120.getEfsa9120_Sub_Rqst_Cde().getValue(1).setValue(pdaCwfa5010.getCwfa5010_Sub_Rqst_Sqnce_Ind().getValue(1));                                            //Natural: MOVE CWFA5010.SUB-RQST-SQNCE-IND ( 1 ) TO EFSA9120.SUB-RQST-CDE ( 1 )
        pdaEfsa9120.getEfsa9120_Multi_Rqst_Ind().getValue(1).setValue(pdaCwfa5010.getCwfa5010_Multi_Rqst_Ind().getValue(1));                                              //Natural: MOVE CWFA5010.MULTI-RQST-IND ( 1 ) TO EFSA9120.MULTI-RQST-IND ( 1 )
        pnd_Old_Cabinet_Pnd_Cabinet_Prefix.setValue("P");                                                                                                                 //Natural: MOVE 'P' TO #CABINET-PREFIX
        pnd_Old_Cabinet_Pnd_Old_Pin_Nbr.setValue(pdaCwfa5012.getCwfa5012_Pin_Nbr());                                                                                      //Natural: MOVE CWFA5012.PIN-NBR TO #OLD-PIN-NBR
        pdaEfsa9120.getEfsa9120_Old_Cabinet().setValue(pnd_Old_Cabinet);                                                                                                  //Natural: MOVE #OLD-CABINET TO EFSA9120.OLD-CABINET
        pdaEfsa9120.getEfsa9120_Old_Rqst_Id().setValueEdited(pdaCwfa5012.getCwfa5012_Tiaa_Rcvd_Dte(),new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED CWFA5012.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO EFSA9120.OLD-RQST-ID
        //*   START    CHG.     10/30/98 GH
        if (condition(pnd_Move_Case_Ind.getBoolean()))                                                                                                                    //Natural: IF #MOVE-CASE-IND
        {
            pdaEfsa9120.getEfsa9120_Case_Id().getValue(1).setValue(pdaCwfa5010.getCwfa5010_Case_Ind().getValue(1));                                                       //Natural: MOVE CWFA5010.CASE-IND ( 1 ) TO EFSA9120.CASE-ID ( 1 )
            pdaEfsa9120.getEfsa9120_Old_Case_Id().setValue(pdaCwfa5012.getCwfa5012_Case_Id_Cde());                                                                        //Natural: MOVE CWFA5012.CASE-ID-CDE TO EFSA9120.OLD-CASE-ID
            //*  FOR JOINT ACCOUNTS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaEfsa9120.getEfsa9120_Case_Id().getValue(1).setValue(pnd_Case_Ind_New);                                                                                     //Natural: MOVE #CASE-IND-NEW TO EFSA9120.CASE-ID ( 1 )
            pdaEfsa9120.getEfsa9120_Old_Case_Id().setValue(pnd_Case_Ind_Save);                                                                                            //Natural: MOVE #CASE-IND-SAVE TO EFSA9120.OLD-CASE-ID
            //*   END      CHG.     10/30/98 GH
        }                                                                                                                                                                 //Natural: END-IF
        pdaEfsa9120.getEfsa9120_Old_Wpid().setValue(pdaCwfa5012.getCwfa5012_Work_Prcss_Id());                                                                             //Natural: MOVE CWFA5012.WORK-PRCSS-ID TO EFSA9120.OLD-WPID
        pdaEfsa9120.getEfsa9120_Old_Sub_Req_Cde().setValue(pdaCwfa5012.getCwfa5012_Sub_Rqst_Sqnce_Ind());                                                                 //Natural: MOVE CWFA5012.SUB-RQST-SQNCE-IND TO EFSA9120.OLD-SUB-REQ-CDE
        pdaEfsa9120.getEfsa9120_Old_Multi_Rqst_Ind().setValue(pdaCwfa5012.getCwfa5012_Multi_Rqst_Ind());                                                                  //Natural: MOVE CWFA5012.MULTI-RQST-IND TO EFSA9120.OLD-MULTI-RQST-IND
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue(pdaCwfa5012.getCwfa5012_Rqst_Log_Oprtr_Cde());                                                               //Natural: MOVE CWFA5012.RQST-LOG-OPRTR-CDE TO EFSA9120.RQST-ENTRY-OP-CDE
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue(pdaCwfa5012.getCwfa5012_Rqst_Orgn_Cde());                                                                      //Natural: MOVE CWFA5012.RQST-ORGN-CDE TO EFSA9120.RQST-ORIGIN-CDE
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(pdaCwfa5012.getCwfa5012_Orgnl_Unit_Cde());                                                                //Natural: MOVE CWFA5012.ORGNL-UNIT-CDE TO EFSA9120.RQST-ORIGIN-UNIT-CDE
        vw_cwf_Doc.startDatabaseRead                                                                                                                                      //Natural: READ CWF-DOC WITH DOCUMENT-KEY = #DOCUMENT-KEY
        (
        "RDOC",
        new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Document_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("DOCUMENT_KEY", "ASC") }
        );
        RDOC:
        while (condition(vw_cwf_Doc.readNextRow("RDOC")))
        {
            if (condition(cwf_Doc_Cabinet_Id.notEquals(pnd_Document_Key_Pnd_Doc_Cabinet_Id) || cwf_Doc_Tiaa_Rcvd_Dte.notEquals(pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte)    //Natural: IF CWF-DOC.CABINET-ID NE #DOC-CABINET-ID OR CWF-DOC.TIAA-RCVD-DTE NE #DOC-TIAA-RCVD-DTE OR CWF-DOC.CASE-WORKPRCSS-MULTI-SUBRQST NE #CASE-WORKPRCSS-MULTI-SUBRQST
                || cwf_Doc_Case_Workprcss_Multi_Subrqst.notEquals(pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst)))
            {
                if (condition(vw_cwf_Doc.getAstCOUNTER().equals(1)))                                                                                                      //Natural: IF *COUNTER ( RDOC. ) EQ 1
                {
                    //*      BACKOUT TRANSACTION
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("DOC_TYPE");                                                                               //Natural: MOVE 'DOC_TYPE' TO MSG-INFO-SUB.##ERROR-FIELD
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("No MF document found for", pnd_Document_Key_Pnd_Doc_Cabinet_Id,                  //Natural: COMPRESS 'No MF document found for' #DOC-CABINET-ID #DOC-TIAA-RCVD-DTE #CASE-WORKPRCSS-MULTI-SUBRQST INTO MSG-INFO-SUB.##MSG
                        pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte, pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst));
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                    sub_Log_Msg();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RDOC"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RDOC"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pdaEfsa9120.getEfsa9120_Doc_Category().setValue(cwf_Doc_Document_Category);                                                                                   //Natural: MOVE DOCUMENT-CATEGORY TO EFSA9120.DOC-CATEGORY EFSA9120.OLD-DOC-CATEGORY
            pdaEfsa9120.getEfsa9120_Old_Doc_Category().setValue(cwf_Doc_Document_Category);
            pdaEfsa9120.getEfsa9120_Doc_Class().setValue(cwf_Doc_Document_Sub_Category);                                                                                  //Natural: MOVE DOCUMENT-SUB-CATEGORY TO EFSA9120.DOC-CLASS
            pdaEfsa9120.getEfsa9120_Doc_Specific().setValue(cwf_Doc_Document_Detail);                                                                                     //Natural: MOVE DOCUMENT-DETAIL TO EFSA9120.DOC-SPECIFIC
            pdaEfsa9120.getEfsa9120_Doc_Direction().setValue(cwf_Doc_Document_Direction_Ind);                                                                             //Natural: MOVE DOCUMENT-DIRECTION-IND TO EFSA9120.DOC-DIRECTION EFSA9120.OLD-DOC-DIRECTION
            pdaEfsa9120.getEfsa9120_Old_Doc_Direction().setValue(cwf_Doc_Document_Direction_Ind);
            pdaEfsa9120.getEfsa9120_Doc_Security_Cde().setValue(cwf_Doc_Secured_Document_Ind);                                                                            //Natural: MOVE SECURED-DOCUMENT-IND TO EFSA9120.DOC-SECURITY-CDE EFSA9120.OLD-DOC-SECURITY-IND
            pdaEfsa9120.getEfsa9120_Old_Doc_Security_Ind().setValue(cwf_Doc_Secured_Document_Ind);
            pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue(cwf_Doc_Document_Subtype, MoveOption.LeftJustified);                                                        //Natural: MOVE LEFT DOCUMENT-SUBTYPE TO EFSA9120.DOC-FORMAT-CDE
            pdaEfsa9120.getEfsa9120_Old_Doc_Format().setValue(pdaEfsa9120.getEfsa9120_Doc_Format_Cde());                                                                  //Natural: MOVE EFSA9120.DOC-FORMAT-CDE TO EFSA9120.OLD-DOC-FORMAT
            pdaEfsa9120.getEfsa9120_Old_Doc_Wpid().setValue(pdaCwfa5012.getCwfa5012_Rqst_Work_Prcss_Id());                                                                //Natural: MOVE CWFA5012.RQST-WORK-PRCSS-ID TO EFSA9120.OLD-DOC-WPID
            pdaEfsa9120.getEfsa9120_Doc_Retention_Cde().setValue(cwf_Doc_Document_Retention_Ind);                                                                         //Natural: MOVE DOCUMENT-RETENTION-IND TO EFSA9120.DOC-RETENTION-CDE
            pdaEfsa9120.getEfsa9120_Doc_Copy_Ind().setValue("Y");                                                                                                         //Natural: MOVE 'Y' TO EFSA9120.DOC-COPY-IND
            pdaEfsa9120.getEfsa9120_System().setValue("MIT");                                                                                                             //Natural: MOVE 'MIT' TO EFSA9120.SYSTEM
            pdaEfsa9120.getEfsa9120_Entry_Dte_Tme().setValueEdited(cwf_Doc_Entry_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                          //Natural: MOVE EDITED CWF-DOC.ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST ) TO EFSA9120.ENTRY-DTE-TME
            pdaEfsa9120.getEfsa9120_Old_Doc_Create_Yymmdd().setValueEdited(cwf_Doc_Entry_Dte_Tme,new ReportEditMask("YYMMDD"));                                           //Natural: MOVE EDITED CWF-DOC.ENTRY-DTE-TME ( EM = YYMMDD ) TO EFSA9120.OLD-DOC-CREATE-YYMMDD
            if (condition(pdaEfsa9120.getEfsa9120_Entry_Dte_Tme().lessOrEqual("199912312359599")))                                                                        //Natural: IF EFSA9120.ENTRY-DTE-TME LE '199912312359599'
            {
                pdaEfsa9120.getEfsa9120_Old_Doc_Create_C_Yr().setValue("N");                                                                                              //Natural: MOVE 'N' TO EFSA9120.OLD-DOC-CREATE-C-YR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaEfsa9120.getEfsa9120_Old_Doc_Create_C_Yr().setValue("T");                                                                                              //Natural: MOVE 'T' TO EFSA9120.OLD-DOC-CREATE-C-YR
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
            if (condition(Global.isEscape())) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().greater(getZero())))             //Natural: IF MSG-INFO-SUB.##ERROR-FIELD GT ' ' OR MSG-INFO-SUB.##MSG-NR GT 0
            {
                pnd_Store_Msg.getValue(1).setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                            //Natural: MOVE MSG-INFO-SUB.##MSG TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                sub_Log_Msg();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RDOC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RDOC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF THE DOCUMENT FILE READ  */
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Remarks.setValue("Added");                                                                                                                                    //Natural: MOVE 'Added' TO #REMARKS
        short decideConditionsMet2787 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SHRD-SRVCE-IND = 'A'
        if (condition(pnd_Shrd_Srvce_Ind.equals("A")))
        {
            decideConditionsMet2787++;
            pnd_Shrd_Srvce_A_Add.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #SHRD-SRVCE-A-ADD
        }                                                                                                                                                                 //Natural: WHEN #SHRD-SRVCE-IND = 'D'
        else if (condition(pnd_Shrd_Srvce_Ind.equals("D")))
        {
            decideConditionsMet2787++;
            pnd_Shrd_Srvce_D_Add.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #SHRD-SRVCE-D-ADD
        }                                                                                                                                                                 //Natural: WHEN #SHRD-SRVCE-IND = 'P'
        else if (condition(pnd_Shrd_Srvce_Ind.equals("P")))
        {
            decideConditionsMet2787++;
            pnd_Shrd_Srvce_P_Add.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #SHRD-SRVCE-P-ADD
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CREATE THE REPORT
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE EQ 'BATCH'
        {
            pnd_Count.nadd(1);                                                                                                                                            //Natural: ASSIGN #COUNT := #COUNT + 1
            getReports().write(1, new TabSetting(1),pnd_Count,new ColumnSpacing(1),pdaCwfa5010.getCwfa5010_Rcvd_Dte(),new ColumnSpacing(2),pdaCwfa5010.getCwfa5010_Work_Id().getValue(1),new  //Natural: WRITE ( 1 ) 1T #COUNT 1X CWFA5010.RCVD-DTE 2X CWFA5010.WORK-ID ( 1 ) 2X CWFA5010.PIN-NBR 2X CWFA5010.ORGNL-UNIT-CDE 3X CWFA5010.RQST-ORGN-CDE 3X CWFA5010.RQST-LOG-DTE-TME ( 1 ) 3X #REMARKS
                ColumnSpacing(2),pdaCwfa5010.getCwfa5010_Pin_Nbr(),new ColumnSpacing(2),pdaCwfa5010.getCwfa5010_Orgnl_Unit_Cde(),new ColumnSpacing(3),pdaCwfa5010.getCwfa5010_Rqst_Orgn_Cde(),new 
                ColumnSpacing(3),pdaCwfa5010.getCwfa5010_Rqst_Log_Dte_Tme().getValue(1),new ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Inactive_Record() throws Exception                                                                                                            //Natural: CREATE-INACTIVE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        DbsUtil.callnat(Cwfn0005.class , getCurrentProcessState(), pdaCwfa1800.getCwfa1800(), pdaCwfa1800.getCwfa1800_Id(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),  //Natural: CALLNAT 'CWFN0005' CWFA1800 CWFA1800-ID CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Multi_Rqst_Ind_Routine() throws Exception                                                                                                            //Natural: MULTI-RQST-IND-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        short decideConditionsMet2814 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( ( #SHRD-SRVCE-IND = 'D' OR = 'P' ) AND CWFA5012.MULTI-RQST-IND GT '0' ) OR ( #SHRD-SRVCE-IND = 'A' AND CWFA5012.MULTI-RQST-IND GT '0' AND CWFA5012.PIN-NBR = CWFA5010.PIN-NBR )
        if (condition((((pnd_Shrd_Srvce_Ind.equals("D") || pnd_Shrd_Srvce_Ind.equals("P")) && pdaCwfa5012.getCwfa5012_Multi_Rqst_Ind().greater("0")) || 
            ((pnd_Shrd_Srvce_Ind.equals("A") && pdaCwfa5012.getCwfa5012_Multi_Rqst_Ind().greater("0")) && pdaCwfa5012.getCwfa5012_Pin_Nbr().equals(pdaCwfa5010.getCwfa5010_Pin_Nbr())))))
        {
            decideConditionsMet2814++;
            pdaCwfa5010.getCwfa5010_Multi_Rqst_Ind().getValue(1).setValue(pdaCwfa5012.getCwfa5012_Multi_Rqst_Ind());                                                      //Natural: MOVE CWFA5012.MULTI-RQST-IND TO CWFA5010.MULTI-RQST-IND ( 1 )
            DbsUtil.callnat(Cwfn5801.class , getCurrentProcessState(), pdaCwfa5010.getCwfa5010_Multi_Rqst_Ind().getValue(1));                                             //Natural: CALLNAT 'CWFN5801' CWFA5010.MULTI-RQST-IND ( 1 )
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: WHEN ( ( #SHRD-SRVCE-IND = 'D' OR = 'P' ) AND CWFA5012.MULTI-RQST-IND = '0' ) OR ( #SHRD-SRVCE-IND = 'A' AND CWFA5012.MULTI-RQST-IND = '0' AND CWFA5012.PIN-NBR = CWFA5010.PIN-NBR )
        else if (condition((((pnd_Shrd_Srvce_Ind.equals("D") || pnd_Shrd_Srvce_Ind.equals("P")) && pdaCwfa5012.getCwfa5012_Multi_Rqst_Ind().equals("0")) 
            || ((pnd_Shrd_Srvce_Ind.equals("A") && pdaCwfa5012.getCwfa5012_Multi_Rqst_Ind().equals("0")) && pdaCwfa5012.getCwfa5012_Pin_Nbr().equals(pdaCwfa5010.getCwfa5010_Pin_Nbr())))))
        {
            decideConditionsMet2814++;
            pdaCwfa5010.getCwfa5010_Multi_Rqst_Ind().getValue(1).setValue("2");                                                                                           //Natural: MOVE '2' TO CWFA5010.MULTI-RQST-IND ( 1 )
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pdaCwfa5010.getCwfa5010_Multi_Rqst_Ind().getValue(1).setValue("0");                                                                                           //Natural: MOVE '0' TO CWFA5010.MULTI-RQST-IND ( 1 )
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Process_Cor_File() throws Exception                                                                                                                  //Natural: PROCESS-COR-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Number.reset();                                                                                                                                               //Natural: RESET #NUMBER #K
        pnd_K.reset();
        pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Ph_Unique_Id_Nbr.setValue(pdaCwfa5012.getCwfa5012_Pin_Nbr());                                                          //Natural: MOVE CWFA5012.PIN-NBR TO #H1-PH-UNIQUE-ID-NBR
        pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Ph_Rcd_Type_Cde.setValue(2);                                                                                           //Natural: MOVE 2 TO #H1-PH-RCD-TYPE-CDE
        pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Cntrct_Lob_Cde.setValue("M");                                                                                          //Natural: MOVE 'M' TO #H1-CNTRCT-LOB-CDE
        //*  H1. HISTOGRAM COR-H1 FOR COR-SUPER-PIN-RCDTYPE-LOB-NINES
        //*     STARTING FROM #COR-SUPER-PIN-RCDTYPE-LOB-NINES
        //*   IF H1-PH-UNIQUE-ID-NBR NE #H1-PH-UNIQUE-ID-NBR
        //*     ESCAPE BOTTOM (H1.) IMMEDIATE
        //*   END-IF
        //*  RESET #MDMA110             /*MDM PIN EXP
        //* MDM PIN EXP
        pdaMdma111.getPnd_Mdma111().reset();                                                                                                                              //Natural: RESET #MDMA111
        pdaMdma111.getPnd_Mdma111_Pnd_I_Pin_N12().setValue(pnd_Cor_Super_Pin_Rcdtype_Lob_Nines_Pnd_H1_Ph_Unique_Id_Nbr);                                                  //Natural: ASSIGN #MDMA111.#I-PIN-N12 := #H1-PH-UNIQUE-ID-NBR
        //* MDM PIN EXP
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  CALLNAT 'MDMN110A' #MDMA110 /*MDM PIN EXP
            //* MDM PIN EXP
            DbsUtil.callnat(Mdmn111a.class , getCurrentProcessState(), pdaMdma111.getPnd_Mdma111());                                                                      //Natural: CALLNAT 'MDMN111A' #MDMA111
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  CALLNAT 'MDMN110' #MDMA110 /*MDM PIN EXP
            //* MDM PIN EXP
            DbsUtil.callnat(Mdmn111.class , getCurrentProcessState(), pdaMdma111.getPnd_Mdma111());                                                                       //Natural: CALLNAT 'MDMN111' #MDMA111
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #MDMA110.#O-RETURN-CODE = '0000'
        //* MDM PIN EXP
        if (condition(pdaMdma111.getPnd_Mdma111_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA111.#O-RETURN-CODE = '0000'
        {
            FOR03:                                                                                                                                                        //Natural: FOR #X 1 TO #O-CONTRACT-COUNT
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pdaMdma111.getPnd_Mdma111_Pnd_O_Contract_Count())); pnd_X.nadd(1))
            {
                //*    MOVE H1-CNTRCT-NBR TO #H2-CNTRCT-NBR
                //*    MOVE #MDMA110.#O-CONTRACT-NUMBER(#X) TO #H2-CNTRCT-NBR
                //* MDM PIN EXP
                pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Cntrct_Nbr.setValue(pdaMdma111.getPnd_Mdma111_Pnd_O_Contract_Number().getValue(pnd_X));                             //Natural: MOVE #MDMA111.#O-CONTRACT-NUMBER ( #X ) TO #H2-CNTRCT-NBR
                //* MDM PIN EXP
                //*     CH2.  HISTOGRAM COR-H2 FOR COR-SUPER-CNTRCT-PAYEE-PIN
                //*         STARTING FROM #COR-SUPER-CNTRCT-PAYEE-PIN
                //*       IF H2-CNTRCT-NBR NE #H2-CNTRCT-NBR
                //*         ESCAPE BOTTOM IMMEDIATE
                //*       END-IF
                //*    RESET #MDMA115           /*MDM PIN EXP
                //* MDM PIN EXP
                pdaMdma116.getPnd_Mdma116().reset();                                                                                                                      //Natural: RESET #MDMA116
                pdaMdma116.getPnd_Mdma116_Pnd_I_Contract_Number().setValue(pnd_Cor_Super_Cntrct_Payee_Pin_Pnd_H2_Cntrct_Nbr);                                             //Natural: ASSIGN #MDMA116.#I-CONTRACT-NUMBER := #H2-CNTRCT-NBR
                //* MDM PIN EXP
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    //*      CALLNAT 'MDMN115A' #MDMA115 /*MDM PIN EXP
                    //* MDM PIN EXP
                    DbsUtil.callnat(Mdmn116a.class , getCurrentProcessState(), pdaMdma116.getPnd_Mdma116());                                                              //Natural: CALLNAT 'MDMN116A' #MDMA116
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*      CALLNAT 'MDMN115' #MDMA115 /*MDM PIN EXP
                    //* MDM PIN EXP
                    DbsUtil.callnat(Mdmn116.class , getCurrentProcessState(), pdaMdma116.getPnd_Mdma116());                                                               //Natural: CALLNAT 'MDMN116' #MDMA116
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                //*    IF #MDMA115.#O-RETURN-CODE = '0000'
                //* MDM PIN EXP
                if (condition(pdaMdma116.getPnd_Mdma116_Pnd_O_Return_Code().equals("0000")))                                                                              //Natural: IF #MDMA116.#O-RETURN-CODE = '0000'
                {
                    FOR04:                                                                                                                                                //Natural: FOR #Y 1 TO #O-OUTPUT-COUNT
                    for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pdaMdma116.getPnd_Mdma116_Pnd_O_Output_Count())); pnd_Y.nadd(1))
                    {
                        //*  COMPARES THE PH-UNIQUE-NBR  /* PIN NUMBER
                        //*      MOVE H2-PH-UNIQUE-ID-NBR TO #H2-PH-UNIQUE-ID-NBR-N
                        //*        MOVE #MDMA115.#O-PIN(#Y) TO #H2-PH-UNIQUE-ID-NBR-N
                        //* MDM PIN EXP
                        pnd_H2_Ph_Unique_Id_Nbr_A_Pnd_H2_Ph_Unique_Id_Nbr_N.setValue(pdaMdma116.getPnd_Mdma116_Pnd_O_Pin_N12().getValue(pnd_Y));                          //Natural: MOVE #MDMA116.#O-PIN-N12 ( #Y ) TO #H2-PH-UNIQUE-ID-NBR-N
                        //* MDM PIN EXP
                        DbsUtil.examine(new ExamineSource(pnd_Ssr_Pin_Created_Array_A.getValue("*")), new ExamineSearch(pnd_H2_Ph_Unique_Id_Nbr_A), new                   //Natural: EXAMINE #SSR-PIN-CREATED-ARRAY-A ( * ) FOR #H2-PH-UNIQUE-ID-NBR-A GIVING NUMBER #NUMBER
                            ExamineGivingNumber(pnd_Number));
                        if (condition(pnd_Number.equals(getZero())))                                                                                                      //Natural: IF #NUMBER = 0
                        {
                            FOR05:                                                                                                                                        //Natural: FOR #K = 1 TO 500
                            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(500)); pnd_K.nadd(1))
                            {
                                if (condition(pnd_Ssr_Pin_Created_Array_A.getValue(pnd_K).equals(" ")))                                                                   //Natural: IF #SSR-PIN-CREATED-ARRAY-A ( #K ) = ' '
                                {
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Ssr_Pin_Created_Array_A.getValue(pnd_K).setValue(pnd_H2_Ph_Unique_Id_Nbr_A);                                                              //Natural: MOVE #H2-PH-UNIQUE-ID-NBR-A TO #SSR-PIN-CREATED-ARRAY-A ( #K )
                            //*         MOVE H2-PH-UNIQUE-ID-NBR TO CWFA5010.PIN-NBR
                            //*          MOVE #MDMA115.#O-PIN(#Y) TO CWFA5010.PIN-NBR
                            //* MDM PIN EXP
                            pdaCwfa5010.getCwfa5010_Pin_Nbr().setValue(pdaMdma116.getPnd_Mdma116_Pnd_O_Pin_N12().getValue(pnd_Y));                                        //Natural: MOVE #MDMA116.#O-PIN-N12 ( #Y ) TO CWFA5010.PIN-NBR
                            //* MDM PIN EXP
                            //*  10/30/98 GH
                            pnd_Move_Case_Ind.setValue(false);                                                                                                            //Natural: MOVE FALSE TO #MOVE-CASE-IND
                            //*  MULTIPLE TIMES
                                                                                                                                                                          //Natural: PERFORM CREATE-SSR-FOR-TA
                            sub_Create_Ssr_For_Ta();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().greater(getZero()))) //Natural: IF MSG-INFO-SUB.##ERROR-FIELD GT ' ' OR MSG-INFO-SUB.##MSG-NR GT 0
                            {
                                pnd_Store_Msg.getValue(1).setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                            //Natural: MOVE MSG-INFO-SUB.##MSG TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                                sub_Log_Msg();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (true) return;                                                                                                                         //Natural: ESCAPE ROUTINE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //*     END-HISTOGRAM
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  END-HISTOGRAM
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Updte_Date_User() throws Exception                                                                                                                   //Natural: UPDTE-DATE-USER
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pdaCwfa5012.getCwfa5012_Last_Updte_Dte().setValue(pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_N);                                                                           //Natural: MOVE #CRRNT-DTE-N TO CWFA5012.LAST-UPDTE-DTE
        pdaCwfa5012.getCwfa5012_Last_Chnge_Dte_Tme().setValue(pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_Tme_N);                                                                   //Natural: MOVE #CRRNT-DTE-TME-N TO CWFA5012.LAST-CHNGE-DTE-TME
        pdaCwfa5012.getCwfa5012_Last_Chnge_Oprtr_Cde().setValue(pnd_Mitssp1);                                                                                             //Natural: MOVE #MITSSP1 TO CWFA5012.LAST-CHNGE-OPRTR-CDE CWFA5012.LAST-UPDTE-OPRTR-CDE
        pdaCwfa5012.getCwfa5012_Last_Updte_Oprtr_Cde().setValue(pnd_Mitssp1);
        pdaCwfa5012.getCwfa5012_Last_Chnge_Invrt_Dte_Tme().compute(new ComputeParameters(false, pdaCwfa5012.getCwfa5012_Last_Chnge_Invrt_Dte_Tme()), pnd_Nines.subtract(pdaCwfa5012.getCwfa5012_Last_Chnge_Dte_Tme())); //Natural: COMPUTE CWFA5012.LAST-CHNGE-INVRT-DTE-TME = #NINES - CWFA5012.LAST-CHNGE-DTE-TME
        pdaCwfa5012.getCwfa5012_Last_Chnge_Unit_Cde().setValue(pdaCwfpdaun.getParm_Unit_Info_Sub_Pnd_Pnd_Unit_Cde());                                                     //Natural: MOVE PARM-UNIT-INFO-SUB.##UNIT-CDE TO CWFA5012.LAST-CHNGE-UNIT-CDE
        pdaCwfa5012.getCwfa5012_Cntct_Orgn_Type_Cde().setValue(" ");                                                                                                      //Natural: MOVE ' ' TO CWFA5012.CNTCT-ORGN-TYPE-CDE
        pdaCwfa5012.getCwfa5012_Cntct_Dte_Tme().setValue(0);                                                                                                              //Natural: MOVE 0 TO CWFA5012.CNTCT-DTE-TME
        pdaCwfa5012.getCwfa5012_Cntct_Invrt_Dte_Tme().setValue(0);                                                                                                        //Natural: MOVE 0 TO CWFA5012.CNTCT-INVRT-DTE-TME
        pdaCwfa5012.getCwfa5012_Cntct_Oprtr_Id().setValue(" ");                                                                                                           //Natural: MOVE ' ' TO CWFA5012.CNTCT-OPRTR-ID
    }
    private void sub_Callnat_Cwfn5011() throws Exception                                                                                                                  //Natural: CALLNAT-CWFN5011
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************** *
        DbsUtil.callnat(Cwfn5011.class , getCurrentProcessState(), pdaCwfa5012.getCwfa5012(), pdaCwfa5012.getCwfa5012_Id(), pdaCwfa5011.getCwfa5011(),                    //Natural: CALLNAT 'CWFN5011' CWFA5012 CWFA5012-ID CWFA5011 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        //*  CALLNAT-CWFN5011
    }
    private void sub_Log_Msg() throws Exception                                                                                                                           //Natural: LOG-MSG
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************** *
        //*  OBJECT..: ERLC1000
        //*  FUNCTION: INVOKE INFRASTRUCTURE MODULE TO STORE ERROR DETAILS TO
        //*            ERROR-HANDLER FILE.
        //*  MODIFICATION LOG:
        //*  =================
        //* *************************** *
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pdaErla1000.getErla1000_Err_Pgm_Name().setValue(Global.getPROGRAM());                                                                                             //Natural: ASSIGN ERLA1000.ERR-PGM-NAME = *PROGRAM
        pdaErla1000.getErla1000_Err_Pgm_Level().setValue(Global.getLEVEL());                                                                                              //Natural: ASSIGN ERLA1000.ERR-PGM-LEVEL = *LEVEL
        pdaErla1000.getErla1000_Err_Nbr().setValue(0);                                                                                                                    //Natural: ASSIGN ERLA1000.ERR-NBR = 0
        pdaErla1000.getErla1000_Err_Line_Nbr().setValue(2150);                                                                                                            //Natural: ASSIGN ERLA1000.ERR-LINE-NBR = 2150
        pdaErla1000.getErla1000_Err_Status_Cde().setValue("U");                                                                                                           //Natural: ASSIGN ERLA1000.ERR-STATUS-CDE = 'U'
        pdaErla1000.getErla1000_Err_Type_Cde().setValue("N");                                                                                                             //Natural: ASSIGN ERLA1000.ERR-TYPE-CDE = 'N'
        pdaErla1000.getErla1000_Err_Notes().setValue(DbsUtil.compress(pnd_Store_Msg.getValue(1)));                                                                        //Natural: COMPRESS #STORE-MSG ( 1 ) INTO ERLA1000.ERR-NOTES
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE EQ 'BATCH'
        {
            getReports().write(0, pnd_Store_Msg.getValue(1));                                                                                                             //Natural: WRITE #STORE-MSG ( 1 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Erln1000.class , getCurrentProcessState(), pdaErla1000.getErla1000());                                                                            //Natural: CALLNAT 'ERLN1000' ERLA1000
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, "        Shared Service Requests Created/Updated",NEWLINE,"  RCVDDATE  -WPID-  -----PIN----  LOG UNIT  MOC  REMARKS",           //Natural: WRITE ( 1 ) '        Shared Service Requests Created/Updated' / '  RCVDDATE  -WPID-  -----PIN----  LOG UNIT  MOC  REMARKS' / '  --------  ------  ------------  --------  ---  -------' /
                        NEWLINE,"  --------  ------  ------------  --------  ---  -------",NEWLINE);
                    //*    '    RCVDDATE  -WPID-  --PIN--  LOG UNIT  MOC  REMARKS'    /
                    //*    '    --------  ------  -------  --------  ---  -------'    /
                    //*   PIN-EXP<<
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Natural Error", Global.getERROR_NR(), "on line", Global.getERROR_LINE(),                     //Natural: COMPRESS 'Natural Error' *ERROR-NR 'on line' *ERROR-LINE 'of' *PROGRAM INTO MSG-INFO-SUB.##MSG
            "of", Global.getPROGRAM()));
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("NATURAL");                                                                                            //Natural: MOVE 'NATURAL' TO MSG-INFO-SUB.##ERROR-FIELD
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE EQ 'BATCH'
        {
            getReports().write(0, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                             //Natural: WRITE MSG-INFO-SUB.##MSG
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
        sub_Log_Msg();
        if (condition(Global.isEscape())) {return;}
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "SG=OFF LS=132 PS=66");
        Global.format(1, "SG=OFF LS=132 PS=66");
    }
}
