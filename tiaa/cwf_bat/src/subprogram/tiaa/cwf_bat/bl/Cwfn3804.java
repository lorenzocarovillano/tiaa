/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:28:24 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn3804
************************************************************
**        * FILE NAME            : Cwfn3804.java
**        * CLASS NAME           : Cwfn3804
**        * INSTANCE NAME        : Cwfn3804
************************************************************
* CWFN3804 - WRITES COMBINED GRAND TOTAL LINES
*
*
*
*
*
*
*
*
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn3804 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Combined_Totals;
    private DbsField pnd_Combined_Totals_Pnd_Cave_Total;
    private DbsField pnd_Combined_Totals_Pnd_Cave_Total_Num;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_3;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_4;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_5;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_10;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_Over;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Combined_Totals = parameters.newGroupInRecord("pnd_Combined_Totals", "#COMBINED-TOTALS");
        pnd_Combined_Totals.setParameterOption(ParameterOption.ByReference);
        pnd_Combined_Totals_Pnd_Cave_Total = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Cave_Total", "#CAVE-TOTAL", FieldType.NUMERIC, 
            10, 3, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Cave_Total_Num = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Cave_Total_Num", "#CAVE-TOTAL-NUM", 
            FieldType.NUMERIC, 10, 3, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc", "#CTOTAL-MOC", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_3 = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_3", "#CTOTAL-MOC-3", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_4 = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_4", "#CTOTAL-MOC-4", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_5 = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_5", "#CTOTAL-MOC-5", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_10 = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_10", "#CTOTAL-MOC-10", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_Over = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_Over", "#CTOTAL-MOC-OVER", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 3));
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn3804() throws Exception
    {
        super("Cwfn3804");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"COMBINED TOTALS - INTERNALLY GENERATED",NEWLINE,"TOTAL INTERNALLY GENERATED ",             //Natural: FORMAT ( 1 ) LS = 140 PS = 60;//Natural: WRITE ( 1 ) NOTITLE NOHDR / 'COMBINED TOTALS - INTERNALLY GENERATED' / 'TOTAL INTERNALLY GENERATED ' 'REQUESTS COMPLETED IN 3 BUSINESS DAYS OR LESS:' #CTOTAL-MOC-3 ( 1 ) / 'TOTAL INTERNALLY GENERATED ' 'REQUESTS COMPLETED IN 4 BUSINESS DAYS OR LESS:' #CTOTAL-MOC-4 ( 1 ) / 'TOTAL INTERNALLY GENERATED ' 'REQUESTS COMPLETED IN 5 BUSINESS DAYS OR LESS:' #CTOTAL-MOC-5 ( 1 ) / 'TOTAL INTERNALLY GENERATED ' 'REQUESTS COMPLETED IN 6 - 10 BUSINESS DAYS...:' #CTOTAL-MOC-10 ( 1 ) / 'TOTAL INTERNALLY GENERATED ' 'REQUESTS COMPLETED IN OVER 10 BUSINESS DAYS..:' #CTOTAL-MOC-OVER ( 1 ) / 'TOTAL INTERNALLY GENERATED ' 'REQUESTS COMPLETED ..........................:' #CTOTAL-MOC ( 1 ) / 'AVERAGE CORPORATE TURNAROUND' 'FOR INTERNALLY GENERATED REQUESTS...........:' #CAVE-TOTAL ( 1 ) // 'COMBINED TOTALS - EXTERNALLY GENERATED' / 'TOTAL EXTERNALLY GENERATED ' 'REQUESTS COMPLETED IN 3 BUSINESS DAYS OR LESS:' #CTOTAL-MOC-3 ( 2 ) / 'TOTAL EXTERNALLY GENERATED ' 'REQUESTS COMPLETED IN 4 BUSINESS DAYS OR LESS:' #CTOTAL-MOC-4 ( 2 ) / 'TOTAL EXTERNALLY GENERATED ' 'REQUESTS COMPLETED IN 5 BUSINESS DAYS OR LESS:' #CTOTAL-MOC-5 ( 2 ) / 'TOTAL EXTERNALLY GENERATED ' 'REQUESTS COMPLETED IN 6 - 10 BUSINESS DAYS...:' #CTOTAL-MOC-10 ( 2 ) / 'TOTAL EXTERNALLY GENERATED ' 'REQUESTS COMPLETED IN OVER 10 BUSINESS DAYS..:' #CTOTAL-MOC-OVER ( 2 ) / 'TOTAL EXTERNALLY GENERATED ' 'REQUESTS COMPLETED ..........................:' #CTOTAL-MOC ( 2 ) / 'AVERAGE CORPORATE TURNAROUND' 'FOR EXTERNALLY GENERATED REQUESTS...........:' #CAVE-TOTAL ( 2 ) // 'COMBINED TOTALS - ADJUSTMENTS' / 'TOTAL ADJUSTMENTS COMPLETED' 'IN 3 BUSINESS DAYS OR LESS...................:' #CTOTAL-MOC-3 ( 3 ) / 'TOTAL ADJUSTMENTS COMPLETED' 'IN 4 BUSINESS DAYS OR LESS...................:' #CTOTAL-MOC-4 ( 3 ) / 'TOTAL ADJUSTMENTS COMPLETED' 'IN 5 BUSINESS DAYS OR LESS...................:' #CTOTAL-MOC-5 ( 3 ) / 'TOTAL ADJUSTMENTS COMPLETED' 'IN 6 - 10 BUSINESS DAYS......................:' #CTOTAL-MOC-10 ( 3 ) / 'TOTAL ADJUSTMENTS COMPLETED' 'IN OVER 10 BUSINESS DAYS.....................:' #CTOTAL-MOC-OVER ( 3 ) / 'TOTAL ADJUSTMENTS COMPLETED' '.............................................:' #CTOTAL-MOC ( 3 ) / 'AVERAGE CORPORATE TURNAROUND' 'FOR ADJUSTMENTS.............................:' #CAVE-TOTAL ( 3 ) //
            "REQUESTS COMPLETED IN 3 BUSINESS DAYS OR LESS:",pnd_Combined_Totals_Pnd_Ctotal_Moc_3.getValue(1),NEWLINE,"TOTAL INTERNALLY GENERATED ","REQUESTS COMPLETED IN 4 BUSINESS DAYS OR LESS:",
            pnd_Combined_Totals_Pnd_Ctotal_Moc_4.getValue(1),NEWLINE,"TOTAL INTERNALLY GENERATED ","REQUESTS COMPLETED IN 5 BUSINESS DAYS OR LESS:",pnd_Combined_Totals_Pnd_Ctotal_Moc_5.getValue(1),
            NEWLINE,"TOTAL INTERNALLY GENERATED ","REQUESTS COMPLETED IN 6 - 10 BUSINESS DAYS...:",pnd_Combined_Totals_Pnd_Ctotal_Moc_10.getValue(1),NEWLINE,
            "TOTAL INTERNALLY GENERATED ","REQUESTS COMPLETED IN OVER 10 BUSINESS DAYS..:",pnd_Combined_Totals_Pnd_Ctotal_Moc_Over.getValue(1),NEWLINE,"TOTAL INTERNALLY GENERATED ",
            "REQUESTS COMPLETED ..........................:",pnd_Combined_Totals_Pnd_Ctotal_Moc.getValue(1),NEWLINE,"AVERAGE CORPORATE TURNAROUND","FOR INTERNALLY GENERATED REQUESTS...........:",
            pnd_Combined_Totals_Pnd_Cave_Total.getValue(1),NEWLINE,NEWLINE,"COMBINED TOTALS - EXTERNALLY GENERATED",NEWLINE,"TOTAL EXTERNALLY GENERATED ",
            "REQUESTS COMPLETED IN 3 BUSINESS DAYS OR LESS:",pnd_Combined_Totals_Pnd_Ctotal_Moc_3.getValue(2),NEWLINE,"TOTAL EXTERNALLY GENERATED ","REQUESTS COMPLETED IN 4 BUSINESS DAYS OR LESS:",
            pnd_Combined_Totals_Pnd_Ctotal_Moc_4.getValue(2),NEWLINE,"TOTAL EXTERNALLY GENERATED ","REQUESTS COMPLETED IN 5 BUSINESS DAYS OR LESS:",pnd_Combined_Totals_Pnd_Ctotal_Moc_5.getValue(2),
            NEWLINE,"TOTAL EXTERNALLY GENERATED ","REQUESTS COMPLETED IN 6 - 10 BUSINESS DAYS...:",pnd_Combined_Totals_Pnd_Ctotal_Moc_10.getValue(2),NEWLINE,
            "TOTAL EXTERNALLY GENERATED ","REQUESTS COMPLETED IN OVER 10 BUSINESS DAYS..:",pnd_Combined_Totals_Pnd_Ctotal_Moc_Over.getValue(2),NEWLINE,"TOTAL EXTERNALLY GENERATED ",
            "REQUESTS COMPLETED ..........................:",pnd_Combined_Totals_Pnd_Ctotal_Moc.getValue(2),NEWLINE,"AVERAGE CORPORATE TURNAROUND","FOR EXTERNALLY GENERATED REQUESTS...........:",
            pnd_Combined_Totals_Pnd_Cave_Total.getValue(2),NEWLINE,NEWLINE,"COMBINED TOTALS - ADJUSTMENTS",NEWLINE,"TOTAL ADJUSTMENTS COMPLETED","IN 3 BUSINESS DAYS OR LESS...................:",
            pnd_Combined_Totals_Pnd_Ctotal_Moc_3.getValue(3),NEWLINE,"TOTAL ADJUSTMENTS COMPLETED","IN 4 BUSINESS DAYS OR LESS...................:",pnd_Combined_Totals_Pnd_Ctotal_Moc_4.getValue(3),
            NEWLINE,"TOTAL ADJUSTMENTS COMPLETED","IN 5 BUSINESS DAYS OR LESS...................:",pnd_Combined_Totals_Pnd_Ctotal_Moc_5.getValue(3),NEWLINE,
            "TOTAL ADJUSTMENTS COMPLETED","IN 6 - 10 BUSINESS DAYS......................:",pnd_Combined_Totals_Pnd_Ctotal_Moc_10.getValue(3),NEWLINE,"TOTAL ADJUSTMENTS COMPLETED",
            "IN OVER 10 BUSINESS DAYS.....................:",pnd_Combined_Totals_Pnd_Ctotal_Moc_Over.getValue(3),NEWLINE,"TOTAL ADJUSTMENTS COMPLETED",".............................................:",
            pnd_Combined_Totals_Pnd_Ctotal_Moc.getValue(3),NEWLINE,"AVERAGE CORPORATE TURNAROUND","FOR ADJUSTMENTS.............................:",pnd_Combined_Totals_Pnd_Cave_Total.getValue(3),
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=140 PS=60");
    }
}
