/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:26:07 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn3300
************************************************************
**        * FILE NAME            : Cwfn3300.java
**        * CLASS NAME           : Cwfn3300
**        * INSTANCE NAME        : Cwfn3300
************************************************************
************************************************************************
* PROGRAM  : CWFN3300
* SYSTEM   : CRPCWF
* TITLE    : EXTRACT
* GENERATED: OCT 05,93 AT 08:41 AM
* FUNCTION :  READ BY LAST-UPDATE-DATE
*          |  EXTRACT ALL CASES THEY HAVE MET THE FOLLOWING CRITERIOR:
*          | - ADMIN-STATUS-UPDTE-DTE GE START-DATE
*          | - ADMIN-STATUS-UPDTE-DTE LE END-DATE
*          | - LAST CHANGE UNIT EQ INPUT UNIT CDES
*          | - ADMIN STATUS EQ INPUT STATUSES
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* 11/03/98 | AS  - RESTOWED TO INCLUDE INTEGRATED FIELDS IN CWFA5012
*                                                         & CWFA1800
* 06/14/00 | JF  - RESTOWED WITH NEW VERSION OF CWFA1800
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn3300 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCwfa3012 pdaCwfa3012;
    private PdaCwfa3300 pdaCwfa3300;
    private PdaCwfa1800 pdaCwfa1800;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_1;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Rgn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Brnch_Cde;
    private DbsField cwf_Master_Index_View_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Case_Id_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_2;
    private DbsField cwf_Master_Index_View_Case_Ind;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind;
    private DbsField cwf_Master_Index_View_Multi_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_3;
    private DbsField cwf_Master_Index_View_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Wpid_Vldte_Ind;
    private DbsField cwf_Master_Index_View_Unit_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_4;
    private DbsField cwf_Master_Index_View_Unit_Id_Cde;
    private DbsField cwf_Master_Index_View_Unit_Rgn_Cde;
    private DbsField cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_View_Unit_Brnch_Group_Cde;
    private DbsField cwf_Master_Index_View_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Old_Route_Cde;
    private DbsField cwf_Master_Index_View_Work_Rqst_Prty_Cde;
    private DbsField cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Assgn_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_5;
    private DbsField cwf_Master_Index_View_Empl_Racf_Id;
    private DbsField cwf_Master_Index_View_Empl_Sffx_Cde;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_6;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_N;
    private DbsField cwf_Master_Index_View_Last_Chnge_Tme_N;
    private DbsField cwf_Master_Index_View_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_View_Step_Id;
    private DbsField cwf_Master_Index_View_Rt_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Step_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Step_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Status_Cde;
    private DbsField cwf_Master_Index_View_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Updte_Dte;
    private DbsField cwf_Master_Index_View_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Actve_Ind;
    private DbsField cwf_Master_Index_View_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_View_Effctve_Dte;
    private DbsField cwf_Master_Index_View_Trans_Dte;
    private DbsGroup cwf_Master_Index_View_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Cntrct_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_7;
    private DbsField cwf_Master_Index_View_Rqst_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Case_Id_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rltnshp_Type;
    private DbsField cwf_Master_Index_View_Bsnss_Data_Ind;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_8;
    private DbsField cwf_Support_Tbl_Tbl_Prnt_Flag;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_9;
    private DbsField cwf_Support_Tbl_Tbl_Input_Data;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_10;

    private DbsGroup pnd_Tbl_Key_Data_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Key_Field;
    private DbsField pnd_Tbl_Rec_Count;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Rqst_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Last_Chnge_Invrt_Dte_Tme;
    private DbsField pnd_Work_Record_Pnd_Wpid_Actn;
    private DbsField pnd_Work_Record_Rqst_Work_Prcss_Id;

    private DbsGroup pnd_Work_Record__R_Field_11;
    private DbsField pnd_Work_Record_Rqst_Wpid_Actn;
    private DbsField pnd_Work_Record_Rqst_Tiaa_Rcvd_Dte;
    private DbsField pnd_Work_Record_Return_Doc_Rec_Dte_Tme;
    private DbsField pnd_Work_Record_Return_Rcvd_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Business_Days;
    private DbsField pnd_Work_Record_Step_Id;
    private DbsField pnd_Work_Record_Tbl_Status_Key;

    private DbsGroup pnd_Work_Record__R_Field_12;
    private DbsField pnd_Work_Record_Last_Chnge_Unit_Cde;
    private DbsField pnd_Work_Record_Admin_Status_Cde;
    private DbsField pnd_Work_Record_Admin_Status_Updte_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Calendar_Days;
    private DbsField pnd_Work_Record_Rqst_Pin_Nbr;
    private DbsField pnd_Work_Record_Cntrct_Nbr;
    private DbsField pnd_Work_Record_Admin_Status_Updte_Oprtr_Cde;

    private DbsGroup pnd_Misc_Parm;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Cde;

    private DbsGroup pnd_Misc_Parm__R_Field_13;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix;
    private DbsField pnd_Last_Chnge_Unit_Cde;

    private DbsGroup pnd_Last_Chnge_Unit_Cde__R_Field_14;
    private DbsField pnd_Last_Chnge_Unit_Cde_Pnd_Last_Chnge_Unit_Id_Cde;
    private DbsField pnd_Last_Chnge_Unit_Cde_Pnd_Last_Chnge_Suffix;
    private DbsField pnd_Status_Found;
    private DbsField pnd_Admin_Status_Updte_Dte_Tme;

    private DbsGroup pnd_Admin_Status_Updte_Dte_Tme__R_Field_15;
    private DbsField pnd_Admin_Status_Updte_Dte_Tme_Pnd_Admin_Status_Updte_Dte;
    private DbsField pnd_Admin_Status_Updte_Dte_Tme_Pnd_Admin_Status_Updte_Tme;
    private DbsField pnd_Wk_Rqst_Wpid;

    private DbsGroup pnd_Wk_Rqst_Wpid__R_Field_16;
    private DbsField pnd_Wk_Rqst_Wpid_Pnd_Wk_Rqst_Wpid_Cde;
    private DbsField pnd_Rqst_Wpid_Found_Sw;

    private DbsGroup pnd_Rqst_Wpid_Found_Sw__R_Field_17;
    private DbsField pnd_Rqst_Wpid_Found_Sw_Pnd_Rqst_Wpid_Found;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Return_Msg;
    private DbsField pnd_Input_Unit_Ctr;
    private DbsField pnd_Input_Status_Ctr;
    private DbsField pnd_Rqst_Wpid_Ctr;
    private DbsField pnd_Rep_Rqsted;
    private DbsField pnd_Data_Match;
    private DbsField pnd_Start_Date;

    private DbsGroup pnd_Start_Date__R_Field_18;
    private DbsField pnd_Start_Date_Pnd_Start_Date_N;
    private DbsField pnd_End_Date;

    private DbsGroup pnd_End_Date__R_Field_19;
    private DbsField pnd_End_Date_Pnd_End_Date_N;
    private DbsField pnd_Unit_Status_Cde;

    private DbsGroup pnd_Unit_Status_Cde__R_Field_20;
    private DbsField pnd_Unit_Status_Cde_Pnd_Unit_Cde;
    private DbsField pnd_Unit_Status_Cde_Pnd_Status_Cde;
    private DbsField pnd_Save_Unit_Status_Cde;

    private DbsGroup pnd_Save_Unit_Status_Cde__R_Field_21;
    private DbsField pnd_Save_Unit_Status_Cde_Pnd_Save_Unit_Cde;
    private DbsField pnd_Save_Unit_Status_Cde_Pnd_Save_Status_Cde;
    private DbsField pnd_Save_Rqst_Log_Dte_Tme;
    private DbsField pnd_Save_Last_Chnge_Invrt_Dte_Tme;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa1800 = new PdaCwfa1800(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaCwfa3012 = new PdaCwfa3012(parameters);
        pdaCwfa3300 = new PdaCwfa3300(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Pin_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Master_Index_View_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index_View__R_Field_1 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_1", "REDEFINE", cwf_Master_Index_View_Rqst_Log_Dte_Tme);
        cwf_Master_Index_View_Rqst_Log_Index_Dte = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Log_Index_Tme = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme", 
            "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_View_Rqst_Orgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Orgn_Cde", "RQST-ORGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_View_Rqst_Rgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Rgn_Cde", "RQST-RGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_RGN_CDE");
        cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde", 
            "RQST-SPCL-DSGNTN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_SPCL_DSGNTN_CDE");
        cwf_Master_Index_View_Rqst_Brnch_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Brnch_Cde", "RQST-BRNCH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_BRNCH_CDE");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_View_Sub_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Ind", "SUB-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SUB_RQST_IND");
        cwf_Master_Index_View_Sub_Rqst_Ind.setDdmHeader("SUB-REQUEST COUNT");
        cwf_Master_Index_View_Case_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CASE_ID_CDE");
        cwf_Master_Index_View_Case_Id_Cde.setDdmHeader("CASE/ID");

        cwf_Master_Index_View__R_Field_2 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_2", "REDEFINE", cwf_Master_Index_View_Case_Id_Cde);
        cwf_Master_Index_View_Case_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Case_Ind", "CASE-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind", "SUB-RQST-SQNCE-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Multi_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Multi_Rqst_Ind", "MULTI-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        cwf_Master_Index_View_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Master_Index_View_Orgnl_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_View_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");

        cwf_Master_Index_View__R_Field_3 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_3", "REDEFINE", cwf_Master_Index_View_Work_Prcss_Id);
        cwf_Master_Index_View_Work_Actn_Rqstd_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde", 
            "WORK-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde", 
            "WORK-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Wpid_Vldte_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Wpid_Vldte_Ind", "WPID-VLDTE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WPID_VLDTE_IND");
        cwf_Master_Index_View_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Cde", "UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_CDE");
        cwf_Master_Index_View_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Master_Index_View__R_Field_4 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_4", "REDEFINE", cwf_Master_Index_View_Unit_Cde);
        cwf_Master_Index_View_Unit_Id_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 
            5);
        cwf_Master_Index_View_Unit_Rgn_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Unit_Brnch_Group_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Unit_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Index_View_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Index_View_Old_Route_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Old_Route_Cde", "OLD-ROUTE-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "OLD_ROUTE_CDE");
        cwf_Master_Index_View_Old_Route_Cde.setDdmHeader("ACTIVITY-END/STATUS");
        cwf_Master_Index_View_Work_Rqst_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Rqst_Prty_Cde", "WORK-RQST-PRTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WORK_RQST_PRTY_CDE");
        cwf_Master_Index_View_Work_Rqst_Prty_Cde.setDdmHeader("PRIO");
        cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde", 
            "ASSGN-SPRVSR-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ASSGN_SPRVSR_OPRTR_CDE");
        cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde.setDdmHeader("SUPER/VISOR");
        cwf_Master_Index_View_Assgn_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Assgn_Dte_Tme", "ASSGN-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ASSGN_DTE_TME");
        cwf_Master_Index_View_Assgn_Dte_Tme.setDdmHeader("ASSIGNMENT/DATE-TIME");
        cwf_Master_Index_View_Empl_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_View_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");

        cwf_Master_Index_View__R_Field_5 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_5", "REDEFINE", cwf_Master_Index_View_Empl_Oprtr_Cde);
        cwf_Master_Index_View_Empl_Racf_Id = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Empl_Sffx_Cde = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Empl_Sffx_Cde", "EMPL-SFFX-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Last_Chnge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        cwf_Master_Index_View__R_Field_6 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_6", "REDEFINE", cwf_Master_Index_View_Last_Chnge_Dte_Tme);
        cwf_Master_Index_View_Last_Chnge_Dte_N = cwf_Master_Index_View__R_Field_6.newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_N", "LAST-CHNGE-DTE-N", 
            FieldType.NUMERIC, 8);
        cwf_Master_Index_View_Last_Chnge_Tme_N = cwf_Master_Index_View__R_Field_6.newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Tme_N", "LAST-CHNGE-TME-N", 
            FieldType.NUMERIC, 7);
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Oprtr_Cde", 
            "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme", 
            "LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Unit_Cde", 
            "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_View_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_View_Step_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Id", "STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "STEP_ID");
        cwf_Master_Index_View_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Index_View_Rt_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        cwf_Master_Index_View_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        cwf_Master_Index_View_Step_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Sqnce_Nbr", "STEP-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "STEP_SQNCE_NBR");
        cwf_Master_Index_View_Step_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Updte_Dte_Tme", "STEP-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STEP_UPDTE_DTE_TME");
        cwf_Master_Index_View_Step_Updte_Dte_Tme.setDdmHeader("STEP UPDATE/DATE-TIME");
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_View_Admin_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme", 
            "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_View_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Dte_Tme", 
            "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Index_View_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Oprtr_Cde", 
            "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Updte_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Dte", "LAST-UPDTE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        cwf_Master_Index_View_Last_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Index_View_Last_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Oprtr_Cde", 
            "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Actve_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");
        cwf_Master_Index_View_Crprte_Status_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_View_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme", 
            "RQST-INVRT-RCVD-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_INVRT_RCVD_DTE_TME");
        cwf_Master_Index_View_Effctve_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_View_Trans_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trans_Dte", "TRANS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        cwf_Master_Index_View_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_View_Cntrct_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_View_Cntrct_Nbr = cwf_Master_Index_View_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Cntrct_Nbr", "CNTRCT-NBR", 
            FieldType.STRING, 8, new DbsArrayController(1, 1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_View_Rqst_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Id", "RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Master_Index_View_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Master_Index_View__R_Field_7 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_7", "REDEFINE", cwf_Master_Index_View_Rqst_Id);
        cwf_Master_Index_View_Rqst_Work_Prcss_Id = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Case_Id_Cde = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Rqst_Pin_Nbr = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Rqst_Pin_Nbr", "RQST-PIN-NBR", FieldType.NUMERIC, 
            12);
        cwf_Master_Index_View_Rltnshp_Type = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rltnshp_Type", "RLTNSHP-TYPE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "RLTNSHP_TYPE");
        cwf_Master_Index_View_Bsnss_Data_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Bsnss_Data_Ind", "BSNSS-DATA-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BSNSS_DATA_IND");
        registerRecord(vw_cwf_Master_Index_View);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_8 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_8", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_Prnt_Flag = cwf_Support_Tbl__R_Field_8.newFieldInGroup("cwf_Support_Tbl_Tbl_Prnt_Flag", "TBL-PRNT-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_9 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_9", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Input_Data = cwf_Support_Tbl__R_Field_9.newFieldInGroup("cwf_Support_Tbl_Tbl_Input_Data", "TBL-INPUT-DATA", FieldType.STRING, 
            180);
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 53);

        pnd_Tbl_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_10", "REDEFINE", pnd_Tbl_Key);

        pnd_Tbl_Key_Data_Nme = pnd_Tbl_Key__R_Field_10.newGroupInGroup("pnd_Tbl_Key_Data_Nme", "DATA-NME");
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Tbl_Table_Nme = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Tbl_Key_Field = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);

        pnd_Tbl_Prime_Key = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY");
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Key_Tbl_Table_Nme = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_Tbl_Key_Field = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Tbl_Rec_Count = localVariables.newFieldInRecord("pnd_Tbl_Rec_Count", "#TBL-REC-COUNT", FieldType.NUMERIC, 4);

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Rqst_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_Record_Last_Chnge_Invrt_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Last_Chnge_Invrt_Dte_Tme", "LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15);
        pnd_Work_Record_Pnd_Wpid_Actn = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Wpid_Actn", "#WPID-ACTN", FieldType.STRING, 1);
        pnd_Work_Record_Rqst_Work_Prcss_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6);

        pnd_Work_Record__R_Field_11 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_11", "REDEFINE", pnd_Work_Record_Rqst_Work_Prcss_Id);
        pnd_Work_Record_Rqst_Wpid_Actn = pnd_Work_Record__R_Field_11.newFieldInGroup("pnd_Work_Record_Rqst_Wpid_Actn", "RQST-WPID-ACTN", FieldType.STRING, 
            1);
        pnd_Work_Record_Rqst_Tiaa_Rcvd_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", FieldType.STRING, 
            8);
        pnd_Work_Record_Return_Doc_Rec_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Return_Rcvd_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Tbl_Business_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.NUMERIC, 
            6, 1);
        pnd_Work_Record_Step_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Step_Id", "STEP-ID", FieldType.STRING, 6);
        pnd_Work_Record_Tbl_Status_Key = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Key", "TBL-STATUS-KEY", FieldType.STRING, 13);

        pnd_Work_Record__R_Field_12 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_12", "REDEFINE", pnd_Work_Record_Tbl_Status_Key);
        pnd_Work_Record_Last_Chnge_Unit_Cde = pnd_Work_Record__R_Field_12.newFieldInGroup("pnd_Work_Record_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8);
        pnd_Work_Record_Admin_Status_Cde = pnd_Work_Record__R_Field_12.newFieldInGroup("pnd_Work_Record_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record_Admin_Status_Updte_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME);
        pnd_Work_Record_Tbl_Calendar_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 
            4);
        pnd_Work_Record_Rqst_Pin_Nbr = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Rqst_Pin_Nbr", "RQST-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Work_Record_Cntrct_Nbr = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Work_Record_Admin_Status_Updte_Oprtr_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8);

        pnd_Misc_Parm = localVariables.newGroupInRecord("pnd_Misc_Parm", "#MISC-PARM");
        pnd_Misc_Parm_Pnd_Wrk_Unit_Cde = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Cde", "#WRK-UNIT-CDE", FieldType.STRING, 8);

        pnd_Misc_Parm__R_Field_13 = pnd_Misc_Parm.newGroupInGroup("pnd_Misc_Parm__R_Field_13", "REDEFINE", pnd_Misc_Parm_Pnd_Wrk_Unit_Cde);
        pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde = pnd_Misc_Parm__R_Field_13.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde", "#WRK-UNIT-ID-CDE", FieldType.STRING, 
            5);
        pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix = pnd_Misc_Parm__R_Field_13.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix", "#WRK-UNIT-SUFFIX", FieldType.STRING, 
            3);
        pnd_Last_Chnge_Unit_Cde = localVariables.newFieldInRecord("pnd_Last_Chnge_Unit_Cde", "#LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8);

        pnd_Last_Chnge_Unit_Cde__R_Field_14 = localVariables.newGroupInRecord("pnd_Last_Chnge_Unit_Cde__R_Field_14", "REDEFINE", pnd_Last_Chnge_Unit_Cde);
        pnd_Last_Chnge_Unit_Cde_Pnd_Last_Chnge_Unit_Id_Cde = pnd_Last_Chnge_Unit_Cde__R_Field_14.newFieldInGroup("pnd_Last_Chnge_Unit_Cde_Pnd_Last_Chnge_Unit_Id_Cde", 
            "#LAST-CHNGE-UNIT-ID-CDE", FieldType.STRING, 5);
        pnd_Last_Chnge_Unit_Cde_Pnd_Last_Chnge_Suffix = pnd_Last_Chnge_Unit_Cde__R_Field_14.newFieldInGroup("pnd_Last_Chnge_Unit_Cde_Pnd_Last_Chnge_Suffix", 
            "#LAST-CHNGE-SUFFIX", FieldType.STRING, 3);
        pnd_Status_Found = localVariables.newFieldInRecord("pnd_Status_Found", "#STATUS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Admin_Status_Updte_Dte_Tme = localVariables.newFieldInRecord("pnd_Admin_Status_Updte_Dte_Tme", "#ADMIN-STATUS-UPDTE-DTE-TME", FieldType.STRING, 
            15);

        pnd_Admin_Status_Updte_Dte_Tme__R_Field_15 = localVariables.newGroupInRecord("pnd_Admin_Status_Updte_Dte_Tme__R_Field_15", "REDEFINE", pnd_Admin_Status_Updte_Dte_Tme);
        pnd_Admin_Status_Updte_Dte_Tme_Pnd_Admin_Status_Updte_Dte = pnd_Admin_Status_Updte_Dte_Tme__R_Field_15.newFieldInGroup("pnd_Admin_Status_Updte_Dte_Tme_Pnd_Admin_Status_Updte_Dte", 
            "#ADMIN-STATUS-UPDTE-DTE", FieldType.NUMERIC, 8);
        pnd_Admin_Status_Updte_Dte_Tme_Pnd_Admin_Status_Updte_Tme = pnd_Admin_Status_Updte_Dte_Tme__R_Field_15.newFieldInGroup("pnd_Admin_Status_Updte_Dte_Tme_Pnd_Admin_Status_Updte_Tme", 
            "#ADMIN-STATUS-UPDTE-TME", FieldType.STRING, 7);
        pnd_Wk_Rqst_Wpid = localVariables.newFieldInRecord("pnd_Wk_Rqst_Wpid", "#WK-RQST-WPID", FieldType.STRING, 6);

        pnd_Wk_Rqst_Wpid__R_Field_16 = localVariables.newGroupInRecord("pnd_Wk_Rqst_Wpid__R_Field_16", "REDEFINE", pnd_Wk_Rqst_Wpid);
        pnd_Wk_Rqst_Wpid_Pnd_Wk_Rqst_Wpid_Cde = pnd_Wk_Rqst_Wpid__R_Field_16.newFieldArrayInGroup("pnd_Wk_Rqst_Wpid_Pnd_Wk_Rqst_Wpid_Cde", "#WK-RQST-WPID-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 6));
        pnd_Rqst_Wpid_Found_Sw = localVariables.newFieldInRecord("pnd_Rqst_Wpid_Found_Sw", "#RQST-WPID-FOUND-SW", FieldType.STRING, 6);

        pnd_Rqst_Wpid_Found_Sw__R_Field_17 = localVariables.newGroupInRecord("pnd_Rqst_Wpid_Found_Sw__R_Field_17", "REDEFINE", pnd_Rqst_Wpid_Found_Sw);
        pnd_Rqst_Wpid_Found_Sw_Pnd_Rqst_Wpid_Found = pnd_Rqst_Wpid_Found_Sw__R_Field_17.newFieldArrayInGroup("pnd_Rqst_Wpid_Found_Sw_Pnd_Rqst_Wpid_Found", 
            "#RQST-WPID-FOUND", FieldType.STRING, 1, new DbsArrayController(1, 6));
        pnd_Return_Code = localVariables.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 1);
        pnd_Return_Msg = localVariables.newFieldInRecord("pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 20);
        pnd_Input_Unit_Ctr = localVariables.newFieldInRecord("pnd_Input_Unit_Ctr", "#INPUT-UNIT-CTR", FieldType.NUMERIC, 2);
        pnd_Input_Status_Ctr = localVariables.newFieldInRecord("pnd_Input_Status_Ctr", "#INPUT-STATUS-CTR", FieldType.NUMERIC, 2);
        pnd_Rqst_Wpid_Ctr = localVariables.newFieldInRecord("pnd_Rqst_Wpid_Ctr", "#RQST-WPID-CTR", FieldType.NUMERIC, 2);
        pnd_Rep_Rqsted = localVariables.newFieldInRecord("pnd_Rep_Rqsted", "#REP-RQSTED", FieldType.BOOLEAN, 1);
        pnd_Data_Match = localVariables.newFieldInRecord("pnd_Data_Match", "#DATA-MATCH", FieldType.BOOLEAN, 1);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Start_Date__R_Field_18 = localVariables.newGroupInRecord("pnd_Start_Date__R_Field_18", "REDEFINE", pnd_Start_Date);
        pnd_Start_Date_Pnd_Start_Date_N = pnd_Start_Date__R_Field_18.newFieldInGroup("pnd_Start_Date_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_End_Date__R_Field_19 = localVariables.newGroupInRecord("pnd_End_Date__R_Field_19", "REDEFINE", pnd_End_Date);
        pnd_End_Date_Pnd_End_Date_N = pnd_End_Date__R_Field_19.newFieldInGroup("pnd_End_Date_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 8);
        pnd_Unit_Status_Cde = localVariables.newFieldInRecord("pnd_Unit_Status_Cde", "#UNIT-STATUS-CDE", FieldType.STRING, 12);

        pnd_Unit_Status_Cde__R_Field_20 = localVariables.newGroupInRecord("pnd_Unit_Status_Cde__R_Field_20", "REDEFINE", pnd_Unit_Status_Cde);
        pnd_Unit_Status_Cde_Pnd_Unit_Cde = pnd_Unit_Status_Cde__R_Field_20.newFieldInGroup("pnd_Unit_Status_Cde_Pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Unit_Status_Cde_Pnd_Status_Cde = pnd_Unit_Status_Cde__R_Field_20.newFieldInGroup("pnd_Unit_Status_Cde_Pnd_Status_Cde", "#STATUS-CDE", FieldType.STRING, 
            4);
        pnd_Save_Unit_Status_Cde = localVariables.newFieldInRecord("pnd_Save_Unit_Status_Cde", "#SAVE-UNIT-STATUS-CDE", FieldType.STRING, 12);

        pnd_Save_Unit_Status_Cde__R_Field_21 = localVariables.newGroupInRecord("pnd_Save_Unit_Status_Cde__R_Field_21", "REDEFINE", pnd_Save_Unit_Status_Cde);
        pnd_Save_Unit_Status_Cde_Pnd_Save_Unit_Cde = pnd_Save_Unit_Status_Cde__R_Field_21.newFieldInGroup("pnd_Save_Unit_Status_Cde_Pnd_Save_Unit_Cde", 
            "#SAVE-UNIT-CDE", FieldType.STRING, 8);
        pnd_Save_Unit_Status_Cde_Pnd_Save_Status_Cde = pnd_Save_Unit_Status_Cde__R_Field_21.newFieldInGroup("pnd_Save_Unit_Status_Cde_Pnd_Save_Status_Cde", 
            "#SAVE-STATUS-CDE", FieldType.STRING, 4);
        pnd_Save_Rqst_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_Save_Rqst_Log_Dte_Tme", "#SAVE-RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Save_Last_Chnge_Invrt_Dte_Tme = localVariables.newFieldInRecord("pnd_Save_Last_Chnge_Invrt_Dte_Tme", "#SAVE-LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 
            15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index_View.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind.setInitialValue("A");
        pnd_Tbl_Prime_Key_Tbl_Table_Nme.setInitialValue("CWF-REPORT12-INPUT");
        pnd_Tbl_Prime_Key_Tbl_Key_Field.setInitialValue("A");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Cwfn3300() throws Exception
    {
        super("Cwfn3300");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Save_Unit_Status_Cde_Pnd_Save_Unit_Cde.reset();                                                                                                               //Natural: RESET #SAVE-UNIT-CDE #SAVE-STATUS-CDE #SAVE-RQST-LOG-DTE-TME #SAVE-LAST-CHNGE-INVRT-DTE-TME
        pnd_Save_Unit_Status_Cde_Pnd_Save_Status_Cde.reset();
        pnd_Save_Rqst_Log_Dte_Tme.reset();
        pnd_Save_Last_Chnge_Invrt_Dte_Tme.reset();
        pnd_Start_Date.setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Start_Date());                                                                                    //Natural: MOVE #INPUT-START-DATE TO #START-DATE
        pnd_End_Date.setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_End_Date());                                                                                        //Natural: MOVE #INPUT-END-DATE TO #END-DATE
        //*  READ CWF-MASTER-INDEX
        vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                        //Natural: READ CWF-MASTER-INDEX-VIEW BY LAST-CHNGE-DTE-KEY STARTING FROM #START-DATE-N
        (
        "R1",
        new Wc[] { new Wc("LAST_CHNGE_DTE_KEY", ">=", pnd_Start_Date_Pnd_Start_Date_N, WcType.BY) },
        new Oc[] { new Oc("LAST_CHNGE_DTE_KEY", "ASC") }
        );
        R1:
        while (condition(vw_cwf_Master_Index_View.readNextRow("R1")))
        {
            if (condition(cwf_Master_Index_View_Last_Chnge_Dte_N.greater(pnd_End_Date_Pnd_End_Date_N)))                                                                   //Natural: IF LAST-CHNGE-DTE-N > #END-DATE-N
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Data_Match.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #DATA-MATCH
                                                                                                                                                                          //Natural: PERFORM MATCH-DATE-UNIT-STATUS-RACF-WPID
            sub_Match_Date_Unit_Status_Racf_Wpid();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(!(pnd_Data_Match.getBoolean())))                                                                                                                //Natural: ACCEPT IF #DATA-MATCH
            {
                continue;
            }
            getSort().writeSortInData(cwf_Master_Index_View_Rqst_Log_Index_Dte, cwf_Master_Index_View_Rqst_Log_Index_Tme, cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme, //Natural: END-ALL
                cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte, cwf_Master_Index_View_Rqst_Work_Prcss_Id, cwf_Master_Index_View_Step_Id, cwf_Master_Index_View_Last_Chnge_Unit_Cde, 
                cwf_Master_Index_View_Admin_Status_Cde, cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme, cwf_Master_Index_View_Rqst_Pin_Nbr, cwf_Master_Index_View_Cntrct_Nbr.getValue(1), 
                cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(cwf_Master_Index_View_Rqst_Log_Index_Dte, cwf_Master_Index_View_Rqst_Log_Index_Tme, cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme);           //Natural: SORT BY CWF-MASTER-INDEX-VIEW.RQST-LOG-INDEX-DTE CWF-MASTER-INDEX-VIEW.RQST-LOG-INDEX-TME CWF-MASTER-INDEX-VIEW.LAST-CHNGE-INVRT-DTE-TME USING CWF-MASTER-INDEX-VIEW.RQST-TIAA-RCVD-DTE CWF-MASTER-INDEX-VIEW.RQST-WORK-PRCSS-ID CWF-MASTER-INDEX-VIEW.STEP-ID CWF-MASTER-INDEX-VIEW.LAST-CHNGE-UNIT-CDE CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-UPDTE-DTE-TME CWF-MASTER-INDEX-VIEW.RQST-PIN-NBR CWF-MASTER-INDEX-VIEW.CNTRCT-NBR ( 1:1 ) CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-UPDTE-OPRTR-CDE
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Master_Index_View_Rqst_Log_Index_Dte, cwf_Master_Index_View_Rqst_Log_Index_Tme, cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme, 
            cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte, cwf_Master_Index_View_Rqst_Work_Prcss_Id, cwf_Master_Index_View_Step_Id, cwf_Master_Index_View_Last_Chnge_Unit_Cde, 
            cwf_Master_Index_View_Admin_Status_Cde, cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme, cwf_Master_Index_View_Rqst_Pin_Nbr, cwf_Master_Index_View_Cntrct_Nbr.getValue(1), 
            cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde)))
        {
            if (condition(cwf_Master_Index_View_Rqst_Log_Dte_Tme.notEquals(pnd_Save_Rqst_Log_Dte_Tme) && cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme.notEquals(pnd_Save_Last_Chnge_Invrt_Dte_Tme))) //Natural: IF CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME NOT = #SAVE-RQST-LOG-DTE-TME AND CWF-MASTER-INDEX-VIEW.LAST-CHNGE-INVRT-DTE-TME NOT = #SAVE-LAST-CHNGE-INVRT-DTE-TME
            {
                pnd_Save_Rqst_Log_Dte_Tme.setValue(cwf_Master_Index_View_Rqst_Log_Dte_Tme);                                                                               //Natural: MOVE CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO #SAVE-RQST-LOG-DTE-TME
                pnd_Save_Last_Chnge_Invrt_Dte_Tme.setValue(cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme);                                                               //Natural: MOVE CWF-MASTER-INDEX-VIEW.LAST-CHNGE-INVRT-DTE-TME TO #SAVE-LAST-CHNGE-INVRT-DTE-TME
                pnd_Save_Unit_Status_Cde_Pnd_Save_Unit_Cde.setValue(cwf_Master_Index_View_Last_Chnge_Unit_Cde);                                                           //Natural: MOVE CWF-MASTER-INDEX-VIEW.LAST-CHNGE-UNIT-CDE TO #SAVE-UNIT-CDE
                pnd_Save_Unit_Status_Cde_Pnd_Save_Status_Cde.setValue(cwf_Master_Index_View_Admin_Status_Cde);                                                            //Natural: MOVE CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE TO #SAVE-STATUS-CDE
                pnd_Work_Record.setValuesByName(vw_cwf_Master_Index_View);                                                                                                //Natural: MOVE BY NAME CWF-MASTER-INDEX-VIEW TO #WORK-RECORD
                pdaCwfa1800.getCwfa1800().setValuesByName(vw_cwf_Master_Index_View);                                                                                      //Natural: MOVE BY NAME CWF-MASTER-INDEX-VIEW TO CWFA1800
                pdaCwfa1800.getCwfa1800_Id_Structure().setValuesByName(pdaCwfa1800.getCwfa1800());                                                                        //Natural: MOVE BY NAME CWFA1800 TO CWFA1800-ID.STRUCTURE
                DbsUtil.callnat(Cwfn3310.class , getCurrentProcessState(), pdaCwfa1800.getCwfa1800(), pdaCwfa1800.getCwfa1800_Id(), pnd_Work_Record_Rqst_Log_Dte_Tme,     //Natural: CALLNAT 'CWFN3310' CWFA1800 CWFA1800-ID #WORK-RECORD.RQST-LOG-DTE-TME #WORK-RECORD.LAST-CHNGE-INVRT-DTE-TME #WORK-RECORD.ADMIN-STATUS-UPDTE-DTE-TME #RETURN-CODE #RETURN-MSG TBL-CALENDAR-DAYS RETURN-DOC-REC-DTE-TME RETURN-RCVD-DTE-TME TBL-BUSINESS-DAYS
                    pnd_Work_Record_Last_Chnge_Invrt_Dte_Tme, pnd_Work_Record_Admin_Status_Updte_Dte_Tme, pnd_Return_Code, pnd_Return_Msg, pnd_Work_Record_Tbl_Calendar_Days, 
                    pnd_Work_Record_Return_Doc_Rec_Dte_Tme, pnd_Work_Record_Return_Rcvd_Dte_Tme, pnd_Work_Record_Tbl_Business_Days);
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                if (condition(DbsUtil.maskMatches(cwf_Master_Index_View_Rqst_Work_Prcss_Id,"NN....")))                                                                    //Natural: IF CWF-MASTER-INDEX-VIEW.RQST-WORK-PRCSS-ID = MASK ( NN.... )
                {
                    pnd_Work_Record_Pnd_Wpid_Actn.setValue("Z");                                                                                                          //Natural: MOVE 'Z' TO #WPID-ACTN
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Wk_Rqst_Wpid.setValue(cwf_Master_Index_View_Rqst_Work_Prcss_Id);                                                                                  //Natural: MOVE CWF-MASTER-INDEX-VIEW.RQST-WORK-PRCSS-ID TO #WK-RQST-WPID
                    pnd_Work_Record_Pnd_Wpid_Actn.setValue(pnd_Wk_Rqst_Wpid_Pnd_Wk_Rqst_Wpid_Cde.getValue(1));                                                            //Natural: MOVE #WK-RQST-WPID-CDE ( 1 ) TO #WPID-ACTN
                }                                                                                                                                                         //Natural: END-IF
                getWorkFiles().write(1, false, pnd_Work_Record);                                                                                                          //Natural: WRITE WORK FILE 1 #WORK-RECORD
                pdaCwfa3300.getPnd_Parm_Data_Pnd_Extrct_Count().nadd(1);                                                                                                  //Natural: ADD 1 TO #EXTRCT-COUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        //* *************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MATCH-DATE-UNIT-STATUS-RACF-WPID
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MATCH-UNIT-STATUS-RACF-WPID
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MATCH-STATUS-RACF-WPID
        //* ****************************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MATCH-RACF-WPID
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MATCH-WPID
    }
    private void sub_Match_Date_Unit_Status_Racf_Wpid() throws Exception                                                                                                  //Natural: MATCH-DATE-UNIT-STATUS-RACF-WPID
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************************
        pnd_Admin_Status_Updte_Dte_Tme.setValueEdited(cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                            //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #ADMIN-STATUS-UPDTE-DTE-TME
        if (condition(pnd_Admin_Status_Updte_Dte_Tme_Pnd_Admin_Status_Updte_Dte.greaterOrEqual(pnd_Start_Date_Pnd_Start_Date_N) && pnd_Admin_Status_Updte_Dte_Tme_Pnd_Admin_Status_Updte_Dte.lessOrEqual(pnd_End_Date_Pnd_End_Date_N))) //Natural: IF #ADMIN-STATUS-UPDTE-DTE GE #START-DATE-N AND #ADMIN-STATUS-UPDTE-DTE LE #END-DATE-N
        {
                                                                                                                                                                          //Natural: PERFORM MATCH-UNIT-STATUS-RACF-WPID
            sub_Match_Unit_Status_Racf_Wpid();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Match_Unit_Status_Racf_Wpid() throws Exception                                                                                                       //Natural: MATCH-UNIT-STATUS-RACF-WPID
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************
        pnd_Last_Chnge_Unit_Cde.setValue(cwf_Master_Index_View_Last_Chnge_Unit_Cde);                                                                                      //Natural: MOVE CWF-MASTER-INDEX-VIEW.LAST-CHNGE-UNIT-CDE TO #LAST-CHNGE-UNIT-CDE
        FOR01:                                                                                                                                                            //Natural: FOR #INPUT-UNIT-CTR = 1 TO 11
        for (pnd_Input_Unit_Ctr.setValue(1); condition(pnd_Input_Unit_Ctr.lessOrEqual(11)); pnd_Input_Unit_Ctr.nadd(1))
        {
            if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Unit_Cde().getValue(pnd_Input_Unit_Ctr).greater(" ")))                                                  //Natural: IF #INPUT-UNIT-CDE ( #INPUT-UNIT-CTR ) GT ' '
            {
                pnd_Misc_Parm_Pnd_Wrk_Unit_Cde.setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Unit_Cde().getValue(pnd_Input_Unit_Ctr));                                 //Natural: MOVE #INPUT-UNIT-CDE ( #INPUT-UNIT-CTR ) TO #WRK-UNIT-CDE
                //*  5 BYTE GENERIC
                if (condition(pnd_Misc_Parm_Pnd_Wrk_Unit_Cde.equals(pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde)))                                                                  //Natural: IF #WRK-UNIT-CDE = #WRK-UNIT-ID-CDE
                {
                    if (condition(pnd_Last_Chnge_Unit_Cde_Pnd_Last_Chnge_Unit_Id_Cde.equals(pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde)))                                          //Natural: IF #LAST-CHNGE-UNIT-ID-CDE = #WRK-UNIT-ID-CDE
                    {
                                                                                                                                                                          //Natural: PERFORM MATCH-STATUS-RACF-WPID
                        sub_Match_Status_Racf_Wpid();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(cwf_Master_Index_View_Last_Chnge_Unit_Cde.equals(pnd_Misc_Parm_Pnd_Wrk_Unit_Cde)))                                                      //Natural: IF CWF-MASTER-INDEX-VIEW.LAST-CHNGE-UNIT-CDE = #WRK-UNIT-CDE
                    {
                                                                                                                                                                          //Natural: PERFORM MATCH-STATUS-RACF-WPID
                        sub_Match_Status_Racf_Wpid();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Match_Status_Racf_Wpid() throws Exception                                                                                                            //Natural: MATCH-STATUS-RACF-WPID
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #INPUT-STATUS-CTR = 1 TO 10
        for (pnd_Input_Status_Ctr.setValue(1); condition(pnd_Input_Status_Ctr.lessOrEqual(10)); pnd_Input_Status_Ctr.nadd(1))
        {
            if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_Cde().getValue(pnd_Input_Status_Ctr).greater(" ")))                                              //Natural: IF #INPUT-STATUS-CDE ( #INPUT-STATUS-CTR ) GT ' '
            {
                if (condition(cwf_Master_Index_View_Admin_Status_Cde.equals(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_Cde().getValue(pnd_Input_Status_Ctr))))        //Natural: IF CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE = #INPUT-STATUS-CDE ( #INPUT-STATUS-CTR )
                {
                                                                                                                                                                          //Natural: PERFORM MATCH-RACF-WPID
                    sub_Match_Racf_Wpid();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Status_Found.setValue(true);                                                                                                                      //Natural: MOVE TRUE TO #STATUS-FOUND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  STATUS RANGE
        if (condition(! (pnd_Status_Found.getBoolean())))                                                                                                                 //Natural: IF NOT #STATUS-FOUND
        {
            if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_From_1().greater(" ") && pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_To_1().greater(" ")))    //Natural: IF #INPUT-STATUS-FROM-1 GT ' ' AND #INPUT-STATUS-TO-1 GT ' '
            {
                if (condition(cwf_Master_Index_View_Admin_Status_Cde.greaterOrEqual(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_From_1()) && cwf_Master_Index_View_Admin_Status_Cde.lessOrEqual(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_To_1()))) //Natural: IF CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE GE #INPUT-STATUS-FROM-1 AND CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE LE #INPUT-STATUS-TO-1
                {
                                                                                                                                                                          //Natural: PERFORM MATCH-RACF-WPID
                    sub_Match_Racf_Wpid();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Status_Found.setValue(true);                                                                                                                      //Natural: MOVE TRUE TO #STATUS-FOUND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Status_Found.getBoolean())))                                                                                                                 //Natural: IF NOT #STATUS-FOUND
        {
            if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_From_2().greater(" ") && pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_To_2().greater(" ")))    //Natural: IF #INPUT-STATUS-FROM-2 GT ' ' AND #INPUT-STATUS-TO-2 GT ' '
            {
                if (condition(cwf_Master_Index_View_Admin_Status_Cde.greaterOrEqual(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_From_2()) && cwf_Master_Index_View_Admin_Status_Cde.lessOrEqual(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_To_2()))) //Natural: IF CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE GE #INPUT-STATUS-FROM-2 AND CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE LE #INPUT-STATUS-TO-2
                {
                                                                                                                                                                          //Natural: PERFORM MATCH-RACF-WPID
                    sub_Match_Racf_Wpid();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Status_Found.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #STATUS-FOUND
    }
    private void sub_Match_Racf_Wpid() throws Exception                                                                                                                   //Natural: MATCH-RACF-WPID
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Racf_Id().greater(" ")))                                                                                    //Natural: IF #INPUT-RACF-ID GT ' '
        {
            if (condition(cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde.equals(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Racf_Id())))                                  //Natural: IF CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-UPDTE-OPRTR-CDE = #INPUT-RACF-ID
            {
                                                                                                                                                                          //Natural: PERFORM MATCH-WPID
                sub_Match_Wpid();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM MATCH-WPID
            sub_Match_Wpid();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Match_Wpid() throws Exception                                                                                                                        //Natural: MATCH-WPID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        pnd_Rqst_Wpid_Found_Sw.reset();                                                                                                                                   //Natural: RESET #RQST-WPID-FOUND-SW
        if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Rqst_Wpid().greater(" ")))                                                                                  //Natural: IF #INPUT-RQST-WPID GT ' '
        {
            pnd_Wk_Rqst_Wpid.setValue(cwf_Master_Index_View_Rqst_Work_Prcss_Id);                                                                                          //Natural: MOVE CWF-MASTER-INDEX-VIEW.RQST-WORK-PRCSS-ID TO #WK-RQST-WPID
            FOR03:                                                                                                                                                        //Natural: FOR #RQST-WPID-CTR 1 6
            for (pnd_Rqst_Wpid_Ctr.setValue(1); condition(pnd_Rqst_Wpid_Ctr.lessOrEqual(6)); pnd_Rqst_Wpid_Ctr.nadd(1))
            {
                if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Rqst_Wpid_Cde().getValue(pnd_Rqst_Wpid_Ctr).notEquals("*")))                                        //Natural: IF #INPUT-RQST-WPID-CDE ( #RQST-WPID-CTR ) NOT = '*'
                {
                    if (condition(pnd_Wk_Rqst_Wpid_Pnd_Wk_Rqst_Wpid_Cde.getValue(pnd_Rqst_Wpid_Ctr).equals(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Rqst_Wpid_Cde().getValue(pnd_Rqst_Wpid_Ctr)))) //Natural: IF #WK-RQST-WPID-CDE ( #RQST-WPID-CTR ) = #INPUT-RQST-WPID-CDE ( #RQST-WPID-CTR )
                    {
                        pnd_Rqst_Wpid_Found_Sw_Pnd_Rqst_Wpid_Found.getValue(pnd_Rqst_Wpid_Ctr).setValue(1);                                                               //Natural: MOVE 1 TO #RQST-WPID-FOUND ( #RQST-WPID-CTR )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rqst_Wpid_Found_Sw_Pnd_Rqst_Wpid_Found.getValue(pnd_Rqst_Wpid_Ctr).setValue(1);                                                                   //Natural: MOVE 1 TO #RQST-WPID-FOUND ( #RQST-WPID-CTR )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(pnd_Rqst_Wpid_Found_Sw.equals("111111")))                                                                                                       //Natural: IF #RQST-WPID-FOUND-SW = '111111'
            {
                pnd_Data_Match.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #DATA-MATCH
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Data_Match.setValue(true);                                                                                                                                //Natural: MOVE TRUE TO #DATA-MATCH
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
