/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:25:36 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3806
************************************************************
**        * FILE NAME            : Mcsn3806.java
**        * CLASS NAME           : Mcsn3806
**        * INSTANCE NAME        : Mcsn3806
************************************************************
************************************************************************
* PROGRAM  : MCSN3806
* TITLE    : ACCESS FULFILLMENT RECORDS FROM THE CWF-ATI-FULFILLMENT
* FUNCTION : DISPLAY ATI-FULFILLMENT RECORDS ON 192 IN WPID ORDER
*          : FOR ILLU SYSTEM
* AMENDMENTS:
* 06/30/99 - INCLUDE NEW FIELD ILLU-PRJCTD-ACCUM-IND. - HAWTHORN.
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3806 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;

    private DataAccessProgramView vw_cwf_Ati_Fulfill;
    private DbsField cwf_Ati_Fulfill_Ati_Rcrd_Type;

    private DbsGroup cwf_Ati_Fulfill_Ati_Rqst_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Pin;
    private DbsField cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Stts;
    private DbsField cwf_Ati_Fulfill_Ati_Wpid;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Racf_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Unit_Cde;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Error_Txt;
    private DbsField cwf_Ati_Fulfill_Ati_Prge_Ind;
    private DbsField cwf_Ati_Fulfill_Ati_Stts_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Cmtask_Ind;
    private DbsField cwf_Ati_Fulfill_Illu_Type;
    private DbsGroup cwf_Ati_Fulfill_Illu_ContractMuGroup;
    private DbsField cwf_Ati_Fulfill_Illu_Contract;
    private DbsGroup cwf_Ati_Fulfill_Illu_Ann_Start_DteMuGroup;
    private DbsField cwf_Ati_Fulfill_Illu_Ann_Start_Dte;
    private DbsField cwf_Ati_Fulfill_Illu_Ann2_Dob;
    private DbsField cwf_Ati_Fulfill_Illu_Ann2_Rel_Cde;
    private DbsField cwf_Ati_Fulfill_Illu_Unit_Cde;
    private DbsField cwf_Ati_Fulfill_Illu_Priority_Cde;
    private DbsField cwf_Ati_Fulfill_Illu_Final_Prem_Dte;
    private DbsField cwf_Ati_Fulfill_Illu_Forms_Ind;
    private DbsField cwf_Ati_Fulfill_Illu_Mit_Dest_Unit;
    private DbsField cwf_Ati_Fulfill_Illu_Prjctd_Accum_Ind;
    private DbsField pnd_Ati_Mit_Key;

    private DbsGroup pnd_Ati_Mit_Key__R_Field_1;
    private DbsField pnd_Ati_Mit_Key_Pnd_Ati_Pin;
    private DbsField pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme;
    private DbsField pnd_Ati_Fulfill_Key;

    private DbsGroup pnd_Ati_Fulfill_Key__R_Field_2;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts;
    private DbsField pnd_Grand_Total_Open_Atis;
    private DbsField pnd_Grand_Total_Inprogress_Atis;
    private DbsField pnd_Grand_Total_Failed_Atis;
    private DbsField pnd_Grand_Total_Succesfull_Atis;
    private DbsField pnd_Interim_Acct_Name;
    private DbsField pnd_Process_Status_Literal;
    private DbsField pnd_Cmtask_Status_Literal;
    private DbsField pnd_Compressed_Text;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_Cc;
    private DbsField pnd_Prev_Wpid;
    private DbsField pnd_Wpid_Total;
    private DbsField pnd_Fld_Line;
    private DbsField pnd_Fld_Strt;
    private DbsField pnd_Fld_End;
    private DbsField pnd_Accnt_Amt_Type;
    private DbsField pnd_Field;
    private DbsField pnd_Title_Literal;
    private DbsField pnd_Input_Dte_Tme;
    private DbsField pnd_Input_Dte_D;
    private DbsField pnd_Orig_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Entry_Date;
    private DbsField pnd_Last_Page_Flag;
    private DbsField pnd_Mit_Log_Date;
    private DbsField pnd_Proj_Accum_Ind;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Input_Data = parameters.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Ati_Fulfill = new DataAccessProgramView(new NameInfo("vw_cwf_Ati_Fulfill", "CWF-ATI-FULFILL"), "CWF_ATI_FULFILL_ILLU", "CWF_KDO_FULFILL");
        cwf_Ati_Fulfill_Ati_Rcrd_Type = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Rcrd_Type", "ATI-RCRD-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_RCRD_TYPE");

        cwf_Ati_Fulfill_Ati_Rqst_Id = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ATI_RQST_ID", "ATI-RQST-ID");
        cwf_Ati_Fulfill_Ati_Pin = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Pin", "ATI-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ATI_PIN");
        cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme", "ATI-MIT-LOG-DT-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_MIT_LOG_DT_TME");
        cwf_Ati_Fulfill_Ati_Prcss_Stts = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Stts", "ATI-PRCSS-STTS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRCSS_STTS");
        cwf_Ati_Fulfill_Ati_Wpid = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Wpid", "ATI-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ATI_WPID");
        cwf_Ati_Fulfill_Ati_Entry_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Dte_Tme", "ATI-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_ENTRY_DTE_TME");
        cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt", "ATI-ENTRY-SYSTM-OR-UNT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_SYSTM_OR_UNT");
        cwf_Ati_Fulfill_Ati_Entry_Racf_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Racf_Id", "ATI-ENTRY-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ATI_ENTRY_RACF_ID");
        cwf_Ati_Fulfill_Ati_Entry_Unit_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Unit_Cde", "ATI-ENTRY-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_UNIT_CDE");
        cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id", "ATI-PRCSS-APPLCTN-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_PRCSS_APPLCTN_ID");
        cwf_Ati_Fulfill_Ati_Error_Txt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Error_Txt", "ATI-ERROR-TXT", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "ATI_ERROR_TXT");
        cwf_Ati_Fulfill_Ati_Prge_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prge_Ind", "ATI-PRGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRGE_IND");
        cwf_Ati_Fulfill_Ati_Stts_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Stts_Dte_Tme", "ATI-STTS-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_STTS_DTE_TME");
        cwf_Ati_Fulfill_Ati_Cmtask_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Cmtask_Ind", "ATI-CMTASK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_CMTASK_IND");
        cwf_Ati_Fulfill_Illu_Type = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Illu_Type", "ILLU-TYPE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ILLU_TYPE");
        cwf_Ati_Fulfill_Illu_ContractMuGroup = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ILLU_CONTRACTMuGroup", "ILLU_CONTRACTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_KDO_FULFILL_ILLU_CONTRACT");
        cwf_Ati_Fulfill_Illu_Contract = cwf_Ati_Fulfill_Illu_ContractMuGroup.newFieldArrayInGroup("cwf_Ati_Fulfill_Illu_Contract", "ILLU-CONTRACT", FieldType.STRING, 
            8, new DbsArrayController(1, 9), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ILLU_CONTRACT");
        cwf_Ati_Fulfill_Illu_Ann_Start_DteMuGroup = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ILLU_ANN_START_DTEMuGroup", "ILLU_ANN_START_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_KDO_FULFILL_ILLU_ANN_START_DTE");
        cwf_Ati_Fulfill_Illu_Ann_Start_Dte = cwf_Ati_Fulfill_Illu_Ann_Start_DteMuGroup.newFieldArrayInGroup("cwf_Ati_Fulfill_Illu_Ann_Start_Dte", "ILLU-ANN-START-DTE", 
            FieldType.NUMERIC, 8, new DbsArrayController(1, 3), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ILLU_ANN_START_DTE");
        cwf_Ati_Fulfill_Illu_Ann2_Dob = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Illu_Ann2_Dob", "ILLU-ANN2-DOB", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ILLU_ANN2_DOB");
        cwf_Ati_Fulfill_Illu_Ann2_Rel_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Illu_Ann2_Rel_Cde", "ILLU-ANN2-REL-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ILLU_ANN2_REL_CDE");
        cwf_Ati_Fulfill_Illu_Unit_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Illu_Unit_Cde", "ILLU-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ILLU_UNIT_CDE");
        cwf_Ati_Fulfill_Illu_Priority_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Illu_Priority_Cde", "ILLU-PRIORITY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ILLU_PRIORITY_CDE");
        cwf_Ati_Fulfill_Illu_Final_Prem_Dte = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Illu_Final_Prem_Dte", "ILLU-FINAL-PREM-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ILLU_FINAL_PREM_DTE");
        cwf_Ati_Fulfill_Illu_Forms_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Illu_Forms_Ind", "ILLU-FORMS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ILLU_FORMS_IND");
        cwf_Ati_Fulfill_Illu_Mit_Dest_Unit = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Illu_Mit_Dest_Unit", "ILLU-MIT-DEST-UNIT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ILLU_MIT_DEST_UNIT");
        cwf_Ati_Fulfill_Illu_Prjctd_Accum_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Illu_Prjctd_Accum_Ind", "ILLU-PRJCTD-ACCUM-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ILLU_PRJCTD_ACCUM_IND");
        registerRecord(vw_cwf_Ati_Fulfill);

        pnd_Ati_Mit_Key = localVariables.newFieldInRecord("pnd_Ati_Mit_Key", "#ATI-MIT-KEY", FieldType.BINARY, 14);

        pnd_Ati_Mit_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Ati_Mit_Key__R_Field_1", "REDEFINE", pnd_Ati_Mit_Key);
        pnd_Ati_Mit_Key_Pnd_Ati_Pin = pnd_Ati_Mit_Key__R_Field_1.newFieldInGroup("pnd_Ati_Mit_Key_Pnd_Ati_Pin", "#ATI-PIN", FieldType.NUMERIC, 7);
        pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme = pnd_Ati_Mit_Key__R_Field_1.newFieldInGroup("pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme", "#ATI-LOG-DTE-TME", FieldType.TIME);
        pnd_Ati_Fulfill_Key = localVariables.newFieldInRecord("pnd_Ati_Fulfill_Key", "#ATI-FULFILL-KEY", FieldType.STRING, 9);

        pnd_Ati_Fulfill_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Ati_Fulfill_Key__R_Field_2", "REDEFINE", pnd_Ati_Fulfill_Key);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl = pnd_Ati_Fulfill_Key__R_Field_2.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl", "#ATI-RCRD-TYPE-CNTRL", 
            FieldType.STRING, 1);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme = pnd_Ati_Fulfill_Key__R_Field_2.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme", "#ATI-ENTRY-DTE-TME", 
            FieldType.TIME);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts = pnd_Ati_Fulfill_Key__R_Field_2.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts", "#ATI-PRCSS-STTS", 
            FieldType.STRING, 1);
        pnd_Grand_Total_Open_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Open_Atis", "#GRAND-TOTAL-OPEN-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Inprogress_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Inprogress_Atis", "#GRAND-TOTAL-INPROGRESS-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Grand_Total_Failed_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Failed_Atis", "#GRAND-TOTAL-FAILED-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Succesfull_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Succesfull_Atis", "#GRAND-TOTAL-SUCCESFULL-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Interim_Acct_Name = localVariables.newFieldInRecord("pnd_Interim_Acct_Name", "#INTERIM-ACCT-NAME", FieldType.STRING, 11);
        pnd_Process_Status_Literal = localVariables.newFieldInRecord("pnd_Process_Status_Literal", "#PROCESS-STATUS-LITERAL", FieldType.STRING, 7);
        pnd_Cmtask_Status_Literal = localVariables.newFieldInRecord("pnd_Cmtask_Status_Literal", "#CMTASK-STATUS-LITERAL", FieldType.STRING, 27);
        pnd_Compressed_Text = localVariables.newFieldInRecord("pnd_Compressed_Text", "#COMPRESSED-TEXT", FieldType.STRING, 38);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 3);
        pnd_Cc = localVariables.newFieldInRecord("pnd_Cc", "#CC", FieldType.NUMERIC, 3);
        pnd_Prev_Wpid = localVariables.newFieldInRecord("pnd_Prev_Wpid", "#PREV-WPID", FieldType.STRING, 6);
        pnd_Wpid_Total = localVariables.newFieldInRecord("pnd_Wpid_Total", "#WPID-TOTAL", FieldType.NUMERIC, 8);
        pnd_Fld_Line = localVariables.newFieldInRecord("pnd_Fld_Line", "#FLD-LINE", FieldType.NUMERIC, 3);
        pnd_Fld_Strt = localVariables.newFieldInRecord("pnd_Fld_Strt", "#FLD-STRT", FieldType.NUMERIC, 3);
        pnd_Fld_End = localVariables.newFieldInRecord("pnd_Fld_End", "#FLD-END", FieldType.NUMERIC, 3);
        pnd_Accnt_Amt_Type = localVariables.newFieldInRecord("pnd_Accnt_Amt_Type", "#ACCNT-AMT-TYPE", FieldType.STRING, 1);
        pnd_Field = localVariables.newFieldInRecord("pnd_Field", "#FIELD", FieldType.STRING, 30);
        pnd_Title_Literal = localVariables.newFieldInRecord("pnd_Title_Literal", "#TITLE-LITERAL", FieldType.STRING, 54);
        pnd_Input_Dte_Tme = localVariables.newFieldInRecord("pnd_Input_Dte_Tme", "#INPUT-DTE-TME", FieldType.TIME);
        pnd_Input_Dte_D = localVariables.newFieldInRecord("pnd_Input_Dte_D", "#INPUT-DTE-D", FieldType.DATE);
        pnd_Orig_Date = localVariables.newFieldInRecord("pnd_Orig_Date", "#ORIG-DATE", FieldType.DATE);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 8);
        pnd_Entry_Date = localVariables.newFieldInRecord("pnd_Entry_Date", "#ENTRY-DATE", FieldType.DATE);
        pnd_Last_Page_Flag = localVariables.newFieldInRecord("pnd_Last_Page_Flag", "#LAST-PAGE-FLAG", FieldType.STRING, 1);
        pnd_Mit_Log_Date = localVariables.newFieldInRecord("pnd_Mit_Log_Date", "#MIT-LOG-DATE", FieldType.NUMERIC, 14);
        pnd_Proj_Accum_Ind = localVariables.newFieldInRecord("pnd_Proj_Accum_Ind", "#PROJ-ACCUM-IND", FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Ati_Fulfill.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Mcsn3806() throws Exception
    {
        super("Mcsn3806");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt5, 5);
        getReports().atEndOfPage(atEndEventRpt5, 5);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 5 ) PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 6 ) PS = 60 LS = 133 ZP = OFF
        //*  NPUT-DATE := '19970117'
        pnd_Last_Page_Flag.reset();                                                                                                                                       //Natural: RESET #LAST-PAGE-FLAG #GRAND-TOTAL-OPEN-ATIS #GRAND-TOTAL-INPROGRESS-ATIS #GRAND-TOTAL-FAILED-ATIS #GRAND-TOTAL-SUCCESFULL-ATIS
        pnd_Grand_Total_Open_Atis.reset();
        pnd_Grand_Total_Inprogress_Atis.reset();
        pnd_Grand_Total_Failed_Atis.reset();
        pnd_Grand_Total_Succesfull_Atis.reset();
        if (condition(pnd_Input_Data_Pnd_Input_Date.notEquals(" ")))                                                                                                      //Natural: IF #INPUT-DATE NE ' '
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Input_Dte_Tme.setValue(pnd_Input_Dte_D);                                                                                                                  //Natural: MOVE #INPUT-DTE-D TO #INPUT-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(5, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***");                                     //Natural: WRITE ( 5 ) /// '*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***'
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
        //* *** INITIALIZE THE ATI-CONTROL-KEY SUPER-DESCRIPTOR FIELD
        pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl.setValue("3");                                                                                                        //Natural: ASSIGN #ATI-RCRD-TYPE-CNTRL := '3'
        pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme.reset();                                                                                                                //Natural: RESET #ATI-ENTRY-DTE-TME
        //* *****************************************************************
        //* ** READ CWF-ATI-FULFILL AND PROCESS OPEN AND IN-PROGRESS REQUESTS
        //* *****************************************************************
        pnd_Title_Literal.setValue("**** OPEN/IN-PROGRESS ATI REQUESTS FOR ILLU FORMS ****");                                                                             //Natural: MOVE '**** OPEN/IN-PROGRESS ATI REQUESTS FOR ILLU FORMS ****' TO #TITLE-LITERAL
        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 5 )
        if (condition(Global.isEscape())){return;}
        vw_cwf_Ati_Fulfill.startDatabaseRead                                                                                                                              //Natural: READ CWF-ATI-FULFILL BY ATI-FULFILL-KEY STARTING FROM #ATI-FULFILL-KEY
        (
        "READ_ATI",
        new Wc[] { new Wc("ATI_FULFILL_KEY", ">=", pnd_Ati_Fulfill_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("ATI_FULFILL_KEY", "ASC") }
        );
        READ_ATI:
        while (condition(vw_cwf_Ati_Fulfill.readNextRow("READ_ATI")))
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Entry_Date.setValue(cwf_Ati_Fulfill_Ati_Stts_Dte_Tme);                                                                                                    //Natural: MOVE CWF-ATI-FULFILL.ATI-STTS-DTE-TME TO #ENTRY-DATE
            if (condition(!(pnd_Entry_Date.equals(pnd_Input_Dte_D) && cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id.equals("ILLU"))))                                              //Natural: ACCEPT IF #ENTRY-DATE = #INPUT-DTE-D AND ATI-PRCSS-APPLCTN-ID = 'ILLU'
            {
                continue;
            }
            getSort().writeSortInData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type,               //Natural: END-ALL
                cwf_Ati_Fulfill_Ati_Prcss_Stts, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, 
                cwf_Ati_Fulfill_Ati_Entry_Racf_Id, cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Illu_Type, cwf_Ati_Fulfill_Illu_Contract.getValue(1), 
                cwf_Ati_Fulfill_Illu_Contract.getValue(2), cwf_Ati_Fulfill_Illu_Contract.getValue(3), cwf_Ati_Fulfill_Illu_Contract.getValue(4), cwf_Ati_Fulfill_Illu_Contract.getValue(5), 
                cwf_Ati_Fulfill_Illu_Contract.getValue(6), cwf_Ati_Fulfill_Illu_Contract.getValue(7), cwf_Ati_Fulfill_Illu_Contract.getValue(8), cwf_Ati_Fulfill_Illu_Contract.getValue(9), 
                cwf_Ati_Fulfill_Illu_Ann_Start_Dte.getValue(1), cwf_Ati_Fulfill_Illu_Ann_Start_Dte.getValue(2), cwf_Ati_Fulfill_Illu_Ann_Start_Dte.getValue(3), 
                cwf_Ati_Fulfill_Illu_Ann2_Dob, cwf_Ati_Fulfill_Illu_Ann2_Rel_Cde, cwf_Ati_Fulfill_Illu_Unit_Cde, cwf_Ati_Fulfill_Illu_Priority_Cde, cwf_Ati_Fulfill_Illu_Final_Prem_Dte, 
                cwf_Ati_Fulfill_Illu_Forms_Ind, cwf_Ati_Fulfill_Illu_Mit_Dest_Unit, cwf_Ati_Fulfill_Illu_Prjctd_Accum_Ind);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  SORT PERFORMED TO WRITE THEM PIN/MIT-LOG-DATE-TIME/REC-TYPE ORDER
        getSort().sortData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type, cwf_Ati_Fulfill_Ati_Prcss_Stts); //Natural: SORT RECORDS BY CWF-ATI-FULFILL.ATI-WPID CWF-ATI-FULFILL.ATI-PIN CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME CWF-ATI-FULFILL.ATI-RCRD-TYPE CWF-ATI-FULFILL.ATI-PRCSS-STTS USING CWF-ATI-FULFILL.ATI-ERROR-TXT CWF-ATI-FULFILL.ATI-ENTRY-DTE-TME CWF-ATI-FULFILL.ATI-ENTRY-SYSTM-OR-UNT CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE CWF-ATI-FULFILL.ATI-STTS-DTE-TME CWF-ATI-FULFILL.ILLU-TYPE CWF-ATI-FULFILL.ILLU-CONTRACT ( * ) CWF-ATI-FULFILL.ILLU-ANN-START-DTE ( * ) CWF-ATI-FULFILL.ILLU-ANN2-DOB CWF-ATI-FULFILL.ILLU-ANN2-REL-CDE CWF-ATI-FULFILL.ILLU-UNIT-CDE CWF-ATI-FULFILL.ILLU-PRIORITY-CDE CWF-ATI-FULFILL.ILLU-FINAL-PREM-DTE CWF-ATI-FULFILL.ILLU-FORMS-IND CWF-ATI-FULFILL.ILLU-MIT-DEST-UNIT CWF-ATI-FULFILL.ILLU-PRJCTD-ACCUM-IND
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type, 
            cwf_Ati_Fulfill_Ati_Prcss_Stts, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, cwf_Ati_Fulfill_Ati_Entry_Racf_Id, 
            cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Illu_Type, cwf_Ati_Fulfill_Illu_Contract.getValue(1), 
            cwf_Ati_Fulfill_Illu_Contract.getValue(2), cwf_Ati_Fulfill_Illu_Contract.getValue(3), cwf_Ati_Fulfill_Illu_Contract.getValue(4), cwf_Ati_Fulfill_Illu_Contract.getValue(5), 
            cwf_Ati_Fulfill_Illu_Contract.getValue(6), cwf_Ati_Fulfill_Illu_Contract.getValue(7), cwf_Ati_Fulfill_Illu_Contract.getValue(8), cwf_Ati_Fulfill_Illu_Contract.getValue(9), 
            cwf_Ati_Fulfill_Illu_Ann_Start_Dte.getValue(1), cwf_Ati_Fulfill_Illu_Ann_Start_Dte.getValue(2), cwf_Ati_Fulfill_Illu_Ann_Start_Dte.getValue(3), 
            cwf_Ati_Fulfill_Illu_Ann2_Dob, cwf_Ati_Fulfill_Illu_Ann2_Rel_Cde, cwf_Ati_Fulfill_Illu_Unit_Cde, cwf_Ati_Fulfill_Illu_Priority_Cde, cwf_Ati_Fulfill_Illu_Final_Prem_Dte, 
            cwf_Ati_Fulfill_Illu_Forms_Ind, cwf_Ati_Fulfill_Illu_Mit_Dest_Unit, cwf_Ati_Fulfill_Illu_Prjctd_Accum_Ind)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF CWF-ATI-FULFILL.ATI-WPID
            pnd_Prev_Wpid.setValue(cwf_Ati_Fulfill_Ati_Wpid);                                                                                                             //Natural: ASSIGN #PREV-WPID := CWF-ATI-FULFILL.ATI-WPID
            pnd_Wpid_Total.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WPID-TOTAL
                                                                                                                                                                          //Natural: PERFORM ATI-WRITE-REC-1
            sub_Ati_Write_Rec_1();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  END PROCESSING OF OPEN AND IN-PROGRESS REQUEST
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //*  ----------------------------------------------------------------------
        //*  4T CWF-ATI-FULFILL.ATI-PIN
        //*  20T CWF-ATI-FULFILL.ATI-WPID
        //*  27T CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  48T CWF-ATI-FULFILL.ATI-STTS-DTE-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  68T #PROCESS-STATUS-LITERAL
        //*  77T CWF-ATI-FULFILL.ILLU-TYPE
        //*  81T CWF-ATI-FULFILL.ILLU-ANN2-DOB (EM=9999/99/99)
        //*  94T CWF-ATI-FULFILL.ILLU-ANN2-REL-CDE
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 5 )
        //*                                                                                                                                                               //Natural: AT END OF PAGE ( 5 )
        //* ***********************************************************************
        //*   WRITE OUT ATI GRAND TOTALS ON LAST PAGE OF REPORT
        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 5 )
        if (condition(Global.isEscape())){return;}
        pnd_Last_Page_Flag.setValue("Y");                                                                                                                                 //Natural: ASSIGN #LAST-PAGE-FLAG := 'Y'
        getReports().write(5, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,new  //Natural: WRITE ( 5 ) //////// 1T '-' ( 131 ) // 1T '-' ( 50 ) 52T '   ATI (ILLU) GRAND TOTALS   ' 82T '-' ( 51 ) // 1T '-' ( 131 ) //// 45T 'OPEN ATIS        :' #GRAND-TOTAL-OPEN-ATIS / 45T 'IN-PROGRESS ATIS :' #GRAND-TOTAL-INPROGRESS-ATIS / 45T 'SUCCESSFULL ATIS :' #GRAND-TOTAL-SUCCESFULL-ATIS / 45T 'FAILED ATIS      :' #GRAND-TOTAL-FAILED-ATIS
            TabSetting(1),"-",new RepeatItem(50),new TabSetting(52),"   ATI (ILLU) GRAND TOTALS   ",new TabSetting(82),"-",new RepeatItem(51),NEWLINE,NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"OPEN ATIS        :",pnd_Grand_Total_Open_Atis,NEWLINE,new 
            TabSetting(45),"IN-PROGRESS ATIS :",pnd_Grand_Total_Inprogress_Atis,NEWLINE,new TabSetting(45),"SUCCESSFULL ATIS :",pnd_Grand_Total_Succesfull_Atis,NEWLINE,new 
            TabSetting(45),"FAILED ATIS      :",pnd_Grand_Total_Failed_Atis);
        if (Global.isEscape()) return;
    }
    //*  START ATI-WRITE-REC-1 PROCESSING
    private void sub_Ati_Write_Rec_1() throws Exception                                                                                                                   //Natural: ATI-WRITE-REC-1
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM SET-PROCESS-STATUS
        sub_Set_Process_Status();
        if (condition(Global.isEscape())) {return;}
        if (condition(getReports().getAstLinesLeft(5).less(15)))                                                                                                          //Natural: NEWPAGE ( 5 ) IF LESS THAN 15 LINES LEFT
        {
            getReports().newPage(5);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Compressed_Text.reset();                                                                                                                                      //Natural: RESET #COMPRESSED-TEXT
        pnd_Compressed_Text.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "ENTRY RACF-ID/UNIT= ", cwf_Ati_Fulfill_Ati_Entry_Racf_Id, "/", cwf_Ati_Fulfill_Ati_Entry_Unit_Cde)); //Natural: COMPRESS 'ENTRY RACF-ID/UNIT= ' CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID '/' CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE INTO #COMPRESSED-TEXT LEAVING NO SPACE
        //*  06/30
        //*  PIN-EXP
        pnd_Proj_Accum_Ind.setValue(DbsUtil.compress("PROJ-ACCUM-IND =", cwf_Ati_Fulfill_Illu_Prjctd_Accum_Ind));                                                         //Natural: COMPRESS 'PROJ-ACCUM-IND =' CWF-ATI-FULFILL.ILLU-PRJCTD-ACCUM-IND INTO #PROJ-ACCUM-IND
        getReports().write(5, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),cwf_Ati_Fulfill_Ati_Pin,new TabSetting(15),cwf_Ati_Fulfill_Ati_Wpid,new TabSetting(22),cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme,  //Natural: WRITE ( 5 ) / 1T CWF-ATI-FULFILL.ATI-PIN 15T CWF-ATI-FULFILL.ATI-WPID 22T CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 43T CWF-ATI-FULFILL.ATI-STTS-DTE-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 63T #PROCESS-STATUS-LITERAL 72T CWF-ATI-FULFILL.ILLU-TYPE 76T CWF-ATI-FULFILL.ILLU-ANN2-DOB ( EM = 9999/99/99 ) 89T CWF-ATI-FULFILL.ILLU-ANN2-REL-CDE 96T CWF-ATI-FULFILL.ILLU-PRIORITY-CDE 102T CWF-ATI-FULFILL.ILLU-FINAL-PREM-DTE ( EM = 9999/99/99 ) 116T CWF-ATI-FULFILL.ILLU-FORMS-IND 121T CWF-ATI-FULFILL.ILLU-MIT-DEST-UNIT
            new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(43),cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new 
            TabSetting(63),pnd_Process_Status_Literal,new TabSetting(72),cwf_Ati_Fulfill_Illu_Type,new TabSetting(76),cwf_Ati_Fulfill_Illu_Ann2_Dob, new 
            ReportEditMask ("9999/99/99"),new TabSetting(89),cwf_Ati_Fulfill_Illu_Ann2_Rel_Cde,new TabSetting(96),cwf_Ati_Fulfill_Illu_Priority_Cde,new 
            TabSetting(102),cwf_Ati_Fulfill_Illu_Final_Prem_Dte, new ReportEditMask ("9999/99/99"),new TabSetting(116),cwf_Ati_Fulfill_Illu_Forms_Ind,new 
            TabSetting(121),cwf_Ati_Fulfill_Illu_Mit_Dest_Unit);
        if (Global.isEscape()) return;
        getReports().write(5, ReportOption.NOTITLE,new TabSetting(4),"START DATE/S :",cwf_Ati_Fulfill_Illu_Ann_Start_Dte.getValue(1), new ReportEditMask                  //Natural: WRITE ( 5 ) 4T 'START DATE/S :' ILLU-ANN-START-DTE ( 1 ) ( EM = 9999/99/99 ) 3X ILLU-ANN-START-DTE ( 2 ) ( EM = 9999/99/99 ) 3X ILLU-ANN-START-DTE ( 3 ) ( EM = 9999/99/99 )
            ("9999/99/99"),new ColumnSpacing(3),cwf_Ati_Fulfill_Illu_Ann_Start_Dte.getValue(2), new ReportEditMask ("9999/99/99"),new ColumnSpacing(3),cwf_Ati_Fulfill_Illu_Ann_Start_Dte.getValue(3), 
            new ReportEditMask ("9999/99/99"));
        if (Global.isEscape()) return;
        getReports().write(5, ReportOption.NOTITLE,new TabSetting(4),"CONTRACT/S :",new ColumnSpacing(2),cwf_Ati_Fulfill_Illu_Contract.getValue(1),new                    //Natural: WRITE ( 5 ) 4T 'CONTRACT/S :' 2X CWF-ATI-FULFILL.ILLU-CONTRACT ( 1 ) 2X CWF-ATI-FULFILL.ILLU-CONTRACT ( 2 ) 2X CWF-ATI-FULFILL.ILLU-CONTRACT ( 3 ) 2X CWF-ATI-FULFILL.ILLU-CONTRACT ( 4 ) 2X CWF-ATI-FULFILL.ILLU-CONTRACT ( 5 ) 2X CWF-ATI-FULFILL.ILLU-CONTRACT ( 6 ) 2X CWF-ATI-FULFILL.ILLU-CONTRACT ( 7 ) 2X CWF-ATI-FULFILL.ILLU-CONTRACT ( 8 ) 2X CWF-ATI-FULFILL.ILLU-CONTRACT ( 9 )
            ColumnSpacing(2),cwf_Ati_Fulfill_Illu_Contract.getValue(2),new ColumnSpacing(2),cwf_Ati_Fulfill_Illu_Contract.getValue(3),new ColumnSpacing(2),cwf_Ati_Fulfill_Illu_Contract.getValue(4),new 
            ColumnSpacing(2),cwf_Ati_Fulfill_Illu_Contract.getValue(5),new ColumnSpacing(2),cwf_Ati_Fulfill_Illu_Contract.getValue(6),new ColumnSpacing(2),cwf_Ati_Fulfill_Illu_Contract.getValue(7),new 
            ColumnSpacing(2),cwf_Ati_Fulfill_Illu_Contract.getValue(8),new ColumnSpacing(2),cwf_Ati_Fulfill_Illu_Contract.getValue(9));
        if (Global.isEscape()) return;
        //*  06/30
        if (condition(cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
        {
            getReports().write(5, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text,new TabSetting(55),pnd_Proj_Accum_Ind);                                     //Natural: WRITE ( 5 ) 15T #COMPRESSED-TEXT 55T #PROJ-ACCUM-IND
            if (Global.isEscape()) return;
            //*  06/30
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(5, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text,new TabSetting(55),"ERROR MESSAGE = :",new TabSetting(73),                  //Natural: WRITE ( 5 ) 15T #COMPRESSED-TEXT 55T 'ERROR MESSAGE = :' 73T CWF-ATI-FULFILL.ATI-ERROR-TXT
                cwf_Ati_Fulfill_Ati_Error_Txt);
            if (Global.isEscape()) return;
            getReports().write(5, ReportOption.NOTITLE,new TabSetting(15),pnd_Proj_Accum_Ind);                                                                            //Natural: WRITE ( 5 ) 15T #PROJ-ACCUM-IND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*   END  ATI-WRITE-REC-2 PROCESSING
    }
    private void sub_Set_Process_Status() throws Exception                                                                                                                //Natural: SET-PROCESS-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        short decideConditionsMet253 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CWF-ATI-FULFILL.ATI-PRCSS-STTS;//Natural: VALUE 'O'
        if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("O"))))
        {
            decideConditionsMet253++;
            pnd_Process_Status_Literal.setValue(" OPEN ");                                                                                                                //Natural: MOVE ' OPEN ' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Open_Atis.nadd(1);                                                                                                                            //Natural: ADD 1 TO #GRAND-TOTAL-OPEN-ATIS
            pnd_Input_Data_Pnd_W_Ati_Open.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                                     //Natural: ADD 1 TO #W-ATI-OPEN ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("I"))))
        {
            decideConditionsMet253++;
            pnd_Process_Status_Literal.setValue("INPROGS");                                                                                                               //Natural: MOVE 'INPROGS' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Inprogress_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-INPROGRESS-ATIS
            pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                              //Natural: ADD 1 TO #W-ATI-IN-PROGRESS ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE ' '
        else if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals(" "))))
        {
            decideConditionsMet253++;
            if (condition(cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))                                                                                                     //Natural: IF CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
            {
                pnd_Process_Status_Literal.setValue("SUCCESS");                                                                                                           //Natural: MOVE 'SUCCESS' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Succesfull_Atis.nadd(1);                                                                                                                  //Natural: ADD 1 TO #GRAND-TOTAL-SUCCESFULL-ATIS
                pnd_Input_Data_Pnd_W_Ati_Successful.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                           //Natural: ADD 1 TO #W-ATI-SUCCESSFUL ( #W-INX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Process_Status_Literal.setValue("FAILED");                                                                                                            //Natural: MOVE 'FAILED' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Failed_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-FAILED-ATIS
                pnd_Input_Data_Pnd_W_Ati_Manual.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                               //Natural: ADD 1 TO #W-ATI-MANUAL ( #W-INX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Process_Status_Literal.setValue("INCORCT");                                                                                                               //Natural: MOVE 'INCORCT' TO #PROCESS-STATUS-LITERAL
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(5, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(50),"AUTOMATED TRANSACTION SYSTEM",new                //Natural: WRITE ( 5 ) NOTITLE 001T *PROGRAM 050T 'AUTOMATED TRANSACTION SYSTEM' 101T *DATU 110T *TIME ( AL = 005 ) 121T 'PAGE:' 127T *PAGE-NUMBER ( 5 ) ( AD = L ) / 035T 'ATI DETAILED REPORT FOR ILLU FORMS IN WPID SEQUENCE' / 035T #TITLE-LITERAL / 001T '-' ( 131 ) / 6T 'PIN' 26T 'MASTER INDEX' 48T 'ATI STATUS' 63T 'PROCESS' 71T 'ILLU' 76T 'ANNUITY2' 88T 'RLTN' 95T 'PRTY' 102T 'FINAL' 115T 'FORM' 121T 'DESTN' / 4T 'NUMBER' 16T 'WPID' 24T 'LOG DATE & TIME' 47T 'DATE & TIME' 63T 'STATUS' 71T 'TYPE' 76T 'D-O-B' 88T 'CODE' 95T 'CODE' 102T 'PREM DATE' 115T 'IND' 121T 'CODE' / 001T '-' ( 131 ) /
                        TabSetting(101),Global.getDATU(),new TabSetting(110),Global.getTIME(), new AlphanumericLength (5),new TabSetting(121),"PAGE:",new 
                        TabSetting(127),getReports().getPageNumberDbs(5), new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(35),"ATI DETAILED REPORT FOR ILLU FORMS IN WPID SEQUENCE",NEWLINE,new 
                        TabSetting(35),pnd_Title_Literal,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(6),"PIN",new TabSetting(26),"MASTER INDEX",new 
                        TabSetting(48),"ATI STATUS",new TabSetting(63),"PROCESS",new TabSetting(71),"ILLU",new TabSetting(76),"ANNUITY2",new TabSetting(88),"RLTN",new 
                        TabSetting(95),"PRTY",new TabSetting(102),"FINAL",new TabSetting(115),"FORM",new TabSetting(121),"DESTN",NEWLINE,new TabSetting(4),"NUMBER",new 
                        TabSetting(16),"WPID",new TabSetting(24),"LOG DATE & TIME",new TabSetting(47),"DATE & TIME",new TabSetting(63),"STATUS",new TabSetting(71),"TYPE",new 
                        TabSetting(76),"D-O-B",new TabSetting(88),"CODE",new TabSetting(95),"CODE",new TabSetting(102),"PREM DATE",new TabSetting(115),"IND",new 
                        TabSetting(121),"CODE",NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE);
                    //*    31T 'MASTER INDEX'
                    //*    53T 'ATI STATUS'
                    //*    68T 'PROCESS'
                    //*    76T 'ILLU'
                    //*    81T 'ANNUITY2'
                    //*    93T 'RLTN'
                    //*   100T 'PRTY'
                    //*    107T 'FINAL'
                    //*    120T 'FORM'
                    //*    126T 'DESTN'
                    //*    21T 'WPID'
                    //*    29T 'LOG DATE & TIME'
                    //*    52T 'DATE & TIME'
                    //*    68T 'STATUS'
                    //*    76T 'TYPE'
                    //*    81T 'D-O-B'
                    //*    93T 'CODE'
                    //*   100T 'CODE'
                    //*    107T 'PREM DATE'
                    //*    120T 'IND'
                    //*    126T 'CODE'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atEndEventRpt5 = (Object sender, EventArgs e) ->
    {
        String localMethod = "MCSN3806|atEndEventRpt5";
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Last_Page_Flag.equals("Y")))                                                                                                        //Natural: IF #LAST-PAGE-FLAG = 'Y'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(5, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(1),"-",new                    //Natural: WRITE ( 5 ) / 1T '-' ( 131 ) / 1T '-' ( 131 )
                            RepeatItem(131));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-ENDPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Ati_Fulfill_Ati_WpidIsBreak = cwf_Ati_Fulfill_Ati_Wpid.isBreak(endOfData);
        if (condition(cwf_Ati_Fulfill_Ati_WpidIsBreak))
        {
            getReports().skip(5, 2);                                                                                                                                      //Natural: SKIP ( 5 ) 2
            getReports().write(5, ReportOption.NOTITLE,new TabSetting(1),"TOTAL FOR WPID :",pnd_Prev_Wpid, new ReportEditMask ("XX' 'X' 'XX' 'X"),":",                    //Natural: WRITE ( 5 ) 1T 'TOTAL FOR WPID :' #PREV-WPID ( EM = XX' 'X' 'XX' 'X ) ':' #WPID-TOTAL
                pnd_Wpid_Total);
            if (condition(Global.isEscape())) return;
            pnd_Wpid_Total.reset();                                                                                                                                       //Natural: RESET #WPID-TOTAL
            getReports().newPage(new ReportSpecification(5));                                                                                                             //Natural: NEWPAGE ( 5 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=OFF");
        Global.format(5, "PS=60 LS=133 ZP=OFF");
        Global.format(6, "PS=60 LS=133 ZP=OFF");
    }
}
