/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:29:43 AM
**        * FROM NATURAL SUBPROGRAM : Wfon4600
************************************************************
**        * FILE NAME            : Wfon4600.java
**        * CLASS NAME           : Wfon4600
**        * INSTANCE NAME        : Wfon4600
************************************************************
************************************************************************
* PROGRAM  : WFON4600
* SYSTEM   : PROJCWF
* FUNCTION : CHECKS WHETHER CONTAINER IS SUCCESSFUL OR NOT. USED BY
*          : WFOB4600
* HISTORY    BY    DESCRIPTION OF CHANGE
* MM/DD/YY  XXXXXX XXXXXXXXXXXXXXXXXXXXX
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017 <STOW ONLY>
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Wfon4600 extends BLNatBase
{
    // Data Areas
    private LdaWfol1210 ldaWfol1210;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Curr_Cont_Id;
    private DbsField pnd_Print_Rec;
    private DbsField pnd_W_Msg_Init;
    private DbsField pnd_W_Msg_Err;
    private DbsField pnd_W_Msg_Blank;
    private DbsField pnd_T_Msg_Init;
    private DbsField pnd_T_Msg_Err;
    private DbsField pnd_T_Msg_Blank;
    private DbsField pnd_K_Msg_Init;
    private DbsField pnd_K_Msg_Err;
    private DbsField pnd_K_Msg_Blank;
    private DbsField pnd_Ar_Exists;
    private DbsField pnd_Mo_Only;
    private DbsField pnd_Client_App_Code;
    private DbsField pnd_W_Exists;
    private DbsField pnd_T_Exists;
    private DbsField pnd_K_Exists;
    private DbsField pnd_Report_Clients;
    private DbsField pnd_Idx;
    private DbsField pnd_Client_Ctr;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_1;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Pnd_Client;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;

    private DbsGroup cwf_Support_Tbl__R_Field_3;
    private DbsField cwf_Support_Tbl_Fill_1;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind_2_2;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaWfol1210 = new LdaWfol1210();
        registerRecord(ldaWfol1210);
        registerRecord(ldaWfol1210.getVw_cmcv());

        // parameters
        parameters = new DbsRecord();
        pnd_Curr_Cont_Id = parameters.newFieldInRecord("pnd_Curr_Cont_Id", "#CURR-CONT-ID", FieldType.STRING, 20);
        pnd_Curr_Cont_Id.setParameterOption(ParameterOption.ByReference);
        pnd_Print_Rec = parameters.newFieldInRecord("pnd_Print_Rec", "#PRINT-REC", FieldType.STRING, 1);
        pnd_Print_Rec.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_W_Msg_Init = localVariables.newFieldInRecord("pnd_W_Msg_Init", "#W-MSG-INIT", FieldType.BOOLEAN, 1);
        pnd_W_Msg_Err = localVariables.newFieldInRecord("pnd_W_Msg_Err", "#W-MSG-ERR", FieldType.BOOLEAN, 1);
        pnd_W_Msg_Blank = localVariables.newFieldInRecord("pnd_W_Msg_Blank", "#W-MSG-BLANK", FieldType.BOOLEAN, 1);
        pnd_T_Msg_Init = localVariables.newFieldInRecord("pnd_T_Msg_Init", "#T-MSG-INIT", FieldType.BOOLEAN, 1);
        pnd_T_Msg_Err = localVariables.newFieldInRecord("pnd_T_Msg_Err", "#T-MSG-ERR", FieldType.BOOLEAN, 1);
        pnd_T_Msg_Blank = localVariables.newFieldInRecord("pnd_T_Msg_Blank", "#T-MSG-BLANK", FieldType.BOOLEAN, 1);
        pnd_K_Msg_Init = localVariables.newFieldInRecord("pnd_K_Msg_Init", "#K-MSG-INIT", FieldType.BOOLEAN, 1);
        pnd_K_Msg_Err = localVariables.newFieldInRecord("pnd_K_Msg_Err", "#K-MSG-ERR", FieldType.BOOLEAN, 1);
        pnd_K_Msg_Blank = localVariables.newFieldInRecord("pnd_K_Msg_Blank", "#K-MSG-BLANK", FieldType.BOOLEAN, 1);
        pnd_Ar_Exists = localVariables.newFieldInRecord("pnd_Ar_Exists", "#AR-EXISTS", FieldType.BOOLEAN, 1);
        pnd_Mo_Only = localVariables.newFieldInRecord("pnd_Mo_Only", "#MO-ONLY", FieldType.BOOLEAN, 1);
        pnd_Client_App_Code = localVariables.newFieldInRecord("pnd_Client_App_Code", "#CLIENT-APP-CODE", FieldType.BOOLEAN, 1);
        pnd_W_Exists = localVariables.newFieldInRecord("pnd_W_Exists", "#W-EXISTS", FieldType.BOOLEAN, 1);
        pnd_T_Exists = localVariables.newFieldInRecord("pnd_T_Exists", "#T-EXISTS", FieldType.BOOLEAN, 1);
        pnd_K_Exists = localVariables.newFieldInRecord("pnd_K_Exists", "#K-EXISTS", FieldType.BOOLEAN, 1);
        pnd_Report_Clients = localVariables.newFieldArrayInRecord("pnd_Report_Clients", "#REPORT-CLIENTS", FieldType.STRING, 8, new DbsArrayController(1, 
            100));
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.INTEGER, 2);
        pnd_Client_Ctr = localVariables.newFieldInRecord("pnd_Client_Ctr", "#CLIENT-CTR", FieldType.INTEGER, 2);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_1", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2 = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2", "#TBL-ACTVE-IND-2-2", 
            FieldType.STRING, 1);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_2 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Pnd_Client = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Client", "#CLIENT", FieldType.STRING, 8);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");

        cwf_Support_Tbl__R_Field_3 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_3", "REDEFINE", cwf_Support_Tbl_Tbl_Actve_Ind);
        cwf_Support_Tbl_Fill_1 = cwf_Support_Tbl__R_Field_3.newFieldInGroup("cwf_Support_Tbl_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Support_Tbl_Tbl_Actve_Ind_2_2 = cwf_Support_Tbl__R_Field_3.newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind_2_2", "TBL-ACTVE-IND-2-2", FieldType.STRING, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();

        ldaWfol1210.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Wfon4600() throws Exception
    {
        super("Wfon4600");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  READ THE CLIENTS WHICH SHOULD APPEAR IN REPORT WHICH SENT MO REQUESTS
        //*  ONLY
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A ");                                                                                                         //Natural: MOVE 'A ' TO #TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("WFO-CLIENTS-MO-REP");                                                                                               //Natural: MOVE 'WFO-CLIENTS-MO-REP' TO #TBL-TABLE-NME
        pnd_Client_Ctr.reset();                                                                                                                                           //Natural: RESET #CLIENT-CTR
        pnd_Report_Clients.getValue("*").reset();                                                                                                                         //Natural: RESET #REPORT-CLIENTS ( * )
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ01")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)))                                                                  //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME NE #TBL-TABLE-NME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Client_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CLIENT-CTR
            pnd_Report_Clients.getValue(pnd_Client_Ctr).setValue(cwf_Support_Tbl_Pnd_Client);                                                                             //Natural: MOVE CWF-SUPPORT-TBL.#CLIENT TO #REPORT-CLIENTS ( #CLIENT-CTR )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Client_Ctr.equals(getZero())))                                                                                                                  //Natural: IF #CLIENT-CTR = 0
        {
            getReports().write(0, "WFON4600 - WFO-CLIENTS-MO-REP table record not found");                                                                                //Natural: WRITE 'WFON4600 - WFO-CLIENTS-MO-REP table record not found'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaWfol1210.getVw_cmcv().startDatabaseRead                                                                                                                        //Natural: READ CMCV BY CONTAINER-RCRD-TYPE-KEY = #CURR-CONT-ID
        (
        "READ02",
        new Wc[] { new Wc("CONTAINER_RCRD_TYPE_KEY", ">=", pnd_Curr_Cont_Id, WcType.BY) },
        new Oc[] { new Oc("CONTAINER_RCRD_TYPE_KEY", "ASC") }
        );
        READ02:
        while (condition(ldaWfol1210.getVw_cmcv().readNextRow("READ02")))
        {
            if (condition(ldaWfol1210.getCmcv_Container_Id().notEquals(pnd_Curr_Cont_Id)))                                                                                //Natural: IF CMCV.CONTAINER-ID NE #CURR-CONT-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet287 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CMCV.RCRD-TYPE-CDE;//Natural: VALUE 'W'
            if (condition((ldaWfol1210.getCmcv_Rcrd_Type_Cde().equals("W"))))
            {
                decideConditionsMet287++;
                if (condition(ldaWfol1210.getCmcv_W_Ignore_Ind().equals(" ")))                                                                                            //Natural: IF CMCV.W-IGNORE-IND = ' '
                {
                    pnd_W_Exists.setValue(true);                                                                                                                          //Natural: ASSIGN #W-EXISTS := TRUE
                    short decideConditionsMet291 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CMCV.ERROR-MSG-TXT = 'INIT'
                    if (condition(ldaWfol1210.getCmcv_Error_Msg_Txt().equals("INIT")))
                    {
                        decideConditionsMet291++;
                        pnd_W_Msg_Init.setValue(true);                                                                                                                    //Natural: ASSIGN #W-MSG-INIT := TRUE
                        if (condition(ldaWfol1210.getCmcv_Action_Cde().getValue("*").equals("AR")))                                                                       //Natural: IF CMCV.ACTION-CDE ( * ) = 'AR'
                        {
                            pnd_Ar_Exists.setValue(true);                                                                                                                 //Natural: ASSIGN #AR-EXISTS := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Mo_Only.setValue(true);                                                                                                                   //Natural: ASSIGN #MO-ONLY := TRUE
                            pnd_Idx.reset();                                                                                                                              //Natural: RESET #IDX
                            //*  WE ONLY CHECK FIRST OCC OF CLIENT-APP-CDE SINCE
                            //*  FIRST OCC IS THE SAME AS ALL OTHER OCCURRENCES
                            DbsUtil.examine(new ExamineSource(pnd_Report_Clients.getValue("*"),true), new ExamineSearch(ldaWfol1210.getCmcv_Client_App_Cde().getValue(1), //Natural: EXAMINE FULL VALUE OF #REPORT-CLIENTS ( * ) FOR FULL VALUE OF CMCV.CLIENT-APP-CDE ( 1 ) GIVING INDEX #IDX
                                true), new ExamineGivingIndex(pnd_Idx));
                            if (condition(pnd_Idx.greater(getZero())))                                                                                                    //Natural: IF #IDX > 0
                            {
                                pnd_Client_App_Code.setValue(true);                                                                                                       //Natural: ASSIGN #CLIENT-APP-CODE := TRUE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN CMCV.ERROR-MSG-TXT NE ' '
                    else if (condition(ldaWfol1210.getCmcv_Error_Msg_Txt().notEquals(" ")))
                    {
                        decideConditionsMet291++;
                        pnd_W_Msg_Err.setValue(true);                                                                                                                     //Natural: ASSIGN #W-MSG-ERR := TRUE
                        if (condition(ldaWfol1210.getCmcv_Action_Cde().getValue("*").equals("AR")))                                                                       //Natural: IF CMCV.ACTION-CDE ( * ) = 'AR'
                        {
                            pnd_Ar_Exists.setValue(true);                                                                                                                 //Natural: ASSIGN #AR-EXISTS := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Mo_Only.setValue(true);                                                                                                                   //Natural: ASSIGN #MO-ONLY := TRUE
                            pnd_Idx.reset();                                                                                                                              //Natural: RESET #IDX
                            //*  WE ONLU CHECK FIRST OCC OF CLIENT-APP-CDE SINCE
                            //*  FIRST OCC IS THE SAME AS ALL OTHER OCCURRENCES
                            DbsUtil.examine(new ExamineSource(pnd_Report_Clients.getValue("*"),true), new ExamineSearch(ldaWfol1210.getCmcv_Client_App_Cde().getValue(1), //Natural: EXAMINE FULL VALUE OF #REPORT-CLIENTS ( * ) FOR FULL VALUE OF CMCV.CLIENT-APP-CDE ( 1 ) GIVING INDEX #IDX
                                true), new ExamineGivingIndex(pnd_Idx));
                            if (condition(pnd_Idx.greater(getZero())))                                                                                                    //Natural: IF #IDX > 0
                            {
                                pnd_Client_App_Code.setValue(true);                                                                                                       //Natural: ASSIGN #CLIENT-APP-CODE := TRUE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN CMCV.ERROR-MSG-TXT EQ ' '
                    else if (condition(ldaWfol1210.getCmcv_Error_Msg_Txt().equals(" ")))
                    {
                        decideConditionsMet291++;
                        pnd_W_Msg_Blank.setValue(true);                                                                                                                   //Natural: ASSIGN #W-MSG-BLANK := TRUE
                        if (condition(ldaWfol1210.getCmcv_Action_Cde().getValue("*").equals("AR")))                                                                       //Natural: IF CMCV.ACTION-CDE ( * ) = 'AR'
                        {
                            pnd_Ar_Exists.setValue(true);                                                                                                                 //Natural: ASSIGN #AR-EXISTS := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((ldaWfol1210.getCmcv_Rcrd_Type_Cde().equals("T"))))
            {
                decideConditionsMet287++;
                pnd_T_Exists.setValue(true);                                                                                                                              //Natural: ASSIGN #T-EXISTS := TRUE
                if (condition(ldaWfol1210.getCmcv_T_Ignore_Ind().equals(" ")))                                                                                            //Natural: IF CMCV.T-IGNORE-IND = ' '
                {
                    short decideConditionsMet332 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CMCV.ERROR-MSG-TXT = 'INIT'
                    if (condition(ldaWfol1210.getCmcv_Error_Msg_Txt().equals("INIT")))
                    {
                        decideConditionsMet332++;
                        pnd_T_Msg_Init.setValue(true);                                                                                                                    //Natural: ASSIGN #T-MSG-INIT := TRUE
                    }                                                                                                                                                     //Natural: WHEN CMCV.ERROR-MSG-TXT NE ' '
                    else if (condition(ldaWfol1210.getCmcv_Error_Msg_Txt().notEquals(" ")))
                    {
                        decideConditionsMet332++;
                        pnd_T_Msg_Err.setValue(true);                                                                                                                     //Natural: ASSIGN #T-MSG-ERR := TRUE
                    }                                                                                                                                                     //Natural: WHEN CMCV.ERROR-MSG-TXT EQ ' '
                    else if (condition(ldaWfol1210.getCmcv_Error_Msg_Txt().equals(" ")))
                    {
                        decideConditionsMet332++;
                        pnd_T_Msg_Blank.setValue(true);                                                                                                                   //Natural: ASSIGN #T-MSG-BLANK := TRUE
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'K'
            else if (condition((ldaWfol1210.getCmcv_Rcrd_Type_Cde().equals("K"))))
            {
                decideConditionsMet287++;
                pnd_K_Exists.setValue(true);                                                                                                                              //Natural: ASSIGN #K-EXISTS := TRUE
                short decideConditionsMet345 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CMCV.ERROR-MSG-TXT = 'INIT'
                if (condition(ldaWfol1210.getCmcv_Error_Msg_Txt().equals("INIT")))
                {
                    decideConditionsMet345++;
                    pnd_K_Msg_Init.setValue(true);                                                                                                                        //Natural: ASSIGN #K-MSG-INIT := TRUE
                }                                                                                                                                                         //Natural: WHEN CMCV.ERROR-MSG-TXT NE ' '
                else if (condition(ldaWfol1210.getCmcv_Error_Msg_Txt().notEquals(" ")))
                {
                    decideConditionsMet345++;
                    pnd_K_Msg_Err.setValue(true);                                                                                                                         //Natural: ASSIGN #K-MSG-ERR := TRUE
                }                                                                                                                                                         //Natural: WHEN CMCV.ERROR-MSG-TXT EQ ' '
                else if (condition(ldaWfol1210.getCmcv_Error_Msg_Txt().equals(" ")))
                {
                    decideConditionsMet345++;
                    pnd_K_Msg_Blank.setValue(true);                                                                                                                       //Natural: ASSIGN #K-MSG-BLANK := TRUE
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  AT THIS POINT WE ARE NOW READY TO DETERMINE IF WE NEED TO PRINT
        //*  THE RECORDS OF THE CONTAINER
        //*  PRINT WHEN AR EXISTS AND W REC IS EITHER INIT OR ERROR
        if (condition(pnd_Ar_Exists.getBoolean() && (pnd_W_Msg_Init.getBoolean() || pnd_W_Msg_Err.getBoolean())))                                                         //Natural: IF #AR-EXISTS AND ( #W-MSG-INIT OR #W-MSG-ERR )
        {
            pnd_Print_Rec.setValue("Y");                                                                                                                                  //Natural: ASSIGN #PRINT-REC := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        //*  PRINT WHEN W RECS HAS BEEN PROCESSED SUCCESSFULLY BUT T/K RECS
        //*  HAD ERROR OR INIT
        if (condition(((((pnd_Ar_Exists.getBoolean() && pnd_W_Msg_Blank.getBoolean()) && ! (pnd_W_Msg_Err.getBoolean())) && ! (pnd_W_Msg_Init.getBoolean()))              //Natural: IF #AR-EXISTS AND #W-MSG-BLANK AND NOT ( #W-MSG-ERR ) AND NOT ( #W-MSG-INIT ) AND ( ( #T-MSG-ERR OR #T-MSG-INIT ) OR ( #K-MSG-ERR OR #K-MSG-INIT ) )
            && ((pnd_T_Msg_Err.getBoolean() || pnd_T_Msg_Init.getBoolean()) || (pnd_K_Msg_Err.getBoolean() || pnd_K_Msg_Init.getBoolean())))))
        {
            pnd_Print_Rec.setValue("Y");                                                                                                                                  //Natural: ASSIGN #PRINT-REC := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        //*  PRINT WHEN W,T,K RECS ARE ALL INIT
        //*  IF #AR-EXISTS AND
        //*  ((#W-EXISTS AND #W-MSG-INIT AND NOT(#W-MSG-ERR) AND NOT(#W-MSG-BLANK))
        //*  OR(#T-EXISTSAND#T-MSG-INIT AND NOT(#T-MSG-ERR) AND NOT(#T-MSG-BLANK))
        //*  OR(#K-EXISTSAND#K-MSG-INIT AND NOT(#K-MSG-ERR) AND NOT(#K-MSG-BLANK))
        //*  )
        //*    #PRINT-REC := 'Y'
        //*  END-IF
        //*  COMMENTED ABOVE BECAUSE THIS IS TAKEN CARED OF BY FIRST CONDITION
        //*  PRINT WHEN K RECS ARE ERROR
        //*  IF #AR-EXISTS AND
        //*  ((#K-EXISTS AND (#K-MSG-INIT OR #K-MSG-ERR) AND NOT(#K-MSG-BLANK))
        //*  AND
        //*  (#W-EXISTS AND NOT(#W-MSG-INIT) AND NOT(#W-MSG-ERR) AND #W-MSG-BLANK)
        //*  AND
        //*  (#T-EXISTS AND NOT(#T-MSG-INIT) AND NOT(#T-MSG-ERR) AND #T-MSG-BLANK)
        //*  )
        //*     #PRINT-REC := 'Y'
        //*  END-IF
        //*  COMMENTED ABOVE BECAUSE THIS IS TAKEN CARED OF BY SECOND CONDITION
        //*  PRINT WHEN MO ONLY AND IN THE TABLE WFO-CLIENTS-MO-REP
        if (condition(! (pnd_Ar_Exists.getBoolean()) && pnd_Mo_Only.getBoolean() && pnd_Client_App_Code.getBoolean()))                                                    //Natural: IF NOT #AR-EXISTS AND #MO-ONLY AND #CLIENT-APP-CODE
        {
            pnd_Print_Rec.setValue("Y");                                                                                                                                  //Natural: ASSIGN #PRINT-REC := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
