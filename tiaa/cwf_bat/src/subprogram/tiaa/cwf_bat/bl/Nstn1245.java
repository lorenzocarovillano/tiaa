/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:50:01 AM
**        * FROM NATURAL SUBPROGRAM : Nstn1245
************************************************************
**        * FILE NAME            : Nstn1245.java
**        * CLASS NAME           : Nstn1245
**        * INSTANCE NAME        : Nstn1245
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: AGE CALCULATOR
**SAG SYSTEM: NEW-SETTLEMENTS
************************************************************************
* PROGRAM  : NSTN1245
* SYSTEM   : NEW-SETTLEMENTS
* TITLE    : NON-PREMIUM PROCESSING
* GENERATED: AUG 02,94 AT 01:46 PM
* FUNCTION : THIS WILL COMPUTE AN AGE IN YEARS, MONTHS AND DAYS GIVEN
*            A BIRTH DATE AND AN EFFECTIVE DATE.
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nstn1245 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaNsta1245 pdaNsta1245;
    private PdaNstpda_M pdaNstpda_M;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Days_Table;
    private DbsField pnd_Days_Table_Pnd_Days_In_Month;
    private DbsField pnd_Remainder;
    private DbsField pnd_Number;
    private DbsField pnd_Test_Date;
    private DbsField pnd_Leap_Year;
    private DbsField pnd_Days_Remaining;
    private DbsField pnd_Previous_Mnth;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaNsta1245 = new PdaNsta1245(parameters);
        pdaNstpda_M = new PdaNstpda_M(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Days_Table = localVariables.newGroupArrayInRecord("pnd_Days_Table", "#DAYS-TABLE", new DbsArrayController(1, 12));
        pnd_Days_Table_Pnd_Days_In_Month = pnd_Days_Table.newFieldInGroup("pnd_Days_Table_Pnd_Days_In_Month", "#DAYS-IN-MONTH", FieldType.NUMERIC, 2);
        pnd_Remainder = localVariables.newFieldInRecord("pnd_Remainder", "#REMAINDER", FieldType.NUMERIC, 3);
        pnd_Number = localVariables.newFieldInRecord("pnd_Number", "#NUMBER", FieldType.PACKED_DECIMAL, 7);
        pnd_Test_Date = localVariables.newFieldInRecord("pnd_Test_Date", "#TEST-DATE", FieldType.STRING, 8);
        pnd_Leap_Year = localVariables.newFieldInRecord("pnd_Leap_Year", "#LEAP-YEAR", FieldType.BOOLEAN, 1);
        pnd_Days_Remaining = localVariables.newFieldInRecord("pnd_Days_Remaining", "#DAYS-REMAINING", FieldType.NUMERIC, 2);
        pnd_Previous_Mnth = localVariables.newFieldInRecord("pnd_Previous_Mnth", "#PREVIOUS-MNTH", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Days_Table_Pnd_Days_In_Month.getValue(1).setInitialValue(31);
        pnd_Days_Table_Pnd_Days_In_Month.getValue(2).setInitialValue(28);
        pnd_Days_Table_Pnd_Days_In_Month.getValue(3).setInitialValue(31);
        pnd_Days_Table_Pnd_Days_In_Month.getValue(4).setInitialValue(30);
        pnd_Days_Table_Pnd_Days_In_Month.getValue(5).setInitialValue(31);
        pnd_Days_Table_Pnd_Days_In_Month.getValue(6).setInitialValue(30);
        pnd_Days_Table_Pnd_Days_In_Month.getValue(7).setInitialValue(31);
        pnd_Days_Table_Pnd_Days_In_Month.getValue(8).setInitialValue(31);
        pnd_Days_Table_Pnd_Days_In_Month.getValue(9).setInitialValue(30);
        pnd_Days_Table_Pnd_Days_In_Month.getValue(10).setInitialValue(31);
        pnd_Days_Table_Pnd_Days_In_Month.getValue(11).setInitialValue(30);
        pnd_Days_Table_Pnd_Days_In_Month.getValue(12).setInitialValue(31);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Nstn1245() throws Exception
    {
        super("Nstn1245");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("NSTN1245", onError);
        if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte().less(pdaNsta1245.getNsta1245_Pnd_Dob_Dte())))                                                                 //Natural: IF #EFF-DTE LT #DOB-DTE
        {
            pdaNstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("EFFECTIVE DATE LESS THAN DATE OF BIRTH");                                                                 //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'EFFECTIVE DATE LESS THAN DATE OF BIRTH'
            pdaNstpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE = 'E'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LEAP-YEAR
        sub_Leap_Year();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Leap_Year.getBoolean()))                                                                                                                        //Natural: IF #LEAP-YEAR
        {
            pnd_Days_Table_Pnd_Days_In_Month.getValue(2).setValue(29);                                                                                                    //Natural: ASSIGN #DAYS-IN-MONTH ( 2 ) = 29
        }                                                                                                                                                                 //Natural: END-IF
        pdaNsta1245.getNsta1245_Pnd_Years().compute(new ComputeParameters(false, pdaNsta1245.getNsta1245_Pnd_Years()), pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Yyyy().subtract(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Yyyy())); //Natural: COMPUTE #YEARS = #EFF-DTE-YYYY - #DOB-DTE-YYYY
                                                                                                                                                                          //Natural: PERFORM CALC-MONTHS
        sub_Calc_Months();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALC-DAYS
        sub_Calc_Days();
        if (condition(Global.isEscape())) {return;}
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-MONTHS
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LEAP-YEAR
        //* ***************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-DAYS
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Calc_Months() throws Exception                                                                                                                       //Natural: CALC-MONTHS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        short decideConditionsMet159 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( #EFF-DTE-MM GT #DOB-DTE-MM ) AND ( #EFF-DTE-DD GT #DOB-DTE-DD )
        if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().greater(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()) && pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().greater(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())))
        {
            decideConditionsMet159++;
            pdaNsta1245.getNsta1245_Pnd_Months().compute(new ComputeParameters(false, pdaNsta1245.getNsta1245_Pnd_Months()), pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().subtract(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm())); //Natural: COMPUTE #MONTHS = #EFF-DTE-MM - #DOB-DTE-MM
        }                                                                                                                                                                 //Natural: WHEN ( #EFF-DTE-MM GT #DOB-DTE-MM ) AND ( #EFF-DTE-DD LT #DOB-DTE-DD )
        else if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().greater(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()) && pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().less(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())))
        {
            decideConditionsMet159++;
            pdaNsta1245.getNsta1245_Pnd_Months().compute(new ComputeParameters(false, pdaNsta1245.getNsta1245_Pnd_Months()), pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().subtract(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()).subtract(1)); //Natural: COMPUTE #MONTHS = #EFF-DTE-MM - #DOB-DTE-MM - 1
        }                                                                                                                                                                 //Natural: WHEN ( #EFF-DTE-MM GT #DOB-DTE-MM ) AND ( #EFF-DTE-DD EQ #DOB-DTE-DD )
        else if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().greater(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()) && pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().equals(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())))
        {
            decideConditionsMet159++;
            pdaNsta1245.getNsta1245_Pnd_Months().compute(new ComputeParameters(false, pdaNsta1245.getNsta1245_Pnd_Months()), pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().subtract(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm())); //Natural: COMPUTE #MONTHS = #EFF-DTE-MM - #DOB-DTE-MM
        }                                                                                                                                                                 //Natural: WHEN ( #EFF-DTE-MM LT #DOB-DTE-MM ) AND ( #EFF-DTE-DD GT #DOB-DTE-DD )
        else if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().less(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()) && pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().greater(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())))
        {
            decideConditionsMet159++;
            pdaNsta1245.getNsta1245_Pnd_Months().compute(new ComputeParameters(false, pdaNsta1245.getNsta1245_Pnd_Months()), DbsField.subtract(12,pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()).add(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm())); //Natural: COMPUTE #MONTHS = 12 - #DOB-DTE-MM + #EFF-DTE-MM
            pdaNsta1245.getNsta1245_Pnd_Years().nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #YEARS
        }                                                                                                                                                                 //Natural: WHEN ( #EFF-DTE-MM LT #DOB-DTE-MM ) AND ( #EFF-DTE-DD LT #DOB-DTE-DD )
        else if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().less(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()) && pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().less(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())))
        {
            decideConditionsMet159++;
            pdaNsta1245.getNsta1245_Pnd_Months().compute(new ComputeParameters(false, pdaNsta1245.getNsta1245_Pnd_Months()), DbsField.subtract(12,pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()).add(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm()).subtract(1)); //Natural: COMPUTE #MONTHS = 12 - #DOB-DTE-MM + #EFF-DTE-MM - 1
            pdaNsta1245.getNsta1245_Pnd_Years().nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #YEARS
        }                                                                                                                                                                 //Natural: WHEN ( #EFF-DTE-MM LT #DOB-DTE-MM ) AND ( #EFF-DTE-DD EQ #DOB-DTE-DD )
        else if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().less(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()) && pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().equals(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())))
        {
            decideConditionsMet159++;
            pdaNsta1245.getNsta1245_Pnd_Months().compute(new ComputeParameters(false, pdaNsta1245.getNsta1245_Pnd_Months()), DbsField.subtract(12,pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()).add(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm())); //Natural: COMPUTE #MONTHS = 12 - #DOB-DTE-MM + #EFF-DTE-MM
            pdaNsta1245.getNsta1245_Pnd_Years().nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #YEARS
        }                                                                                                                                                                 //Natural: WHEN ( #EFF-DTE-MM EQ #DOB-DTE-MM ) AND ( #EFF-DTE-DD GT #EFF-DTE-DD )
        else if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().equals(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()) && pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().greater(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd())))
        {
            decideConditionsMet159++;
            pdaNsta1245.getNsta1245_Pnd_Months().setValue(0);                                                                                                             //Natural: ASSIGN #MONTHS = 0
        }                                                                                                                                                                 //Natural: WHEN ( #EFF-DTE-MM EQ #DOB-DTE-MM ) AND ( #EFF-DTE-DD LT #DOB-DTE-DD )
        else if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().equals(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()) && pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().less(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())))
        {
            decideConditionsMet159++;
            pdaNsta1245.getNsta1245_Pnd_Months().setValue(11);                                                                                                            //Natural: COMPUTE #MONTHS = 11
            pdaNsta1245.getNsta1245_Pnd_Years().nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #YEARS
        }                                                                                                                                                                 //Natural: WHEN ( #EFF-DTE-MM EQ #DOB-DTE-MM ) AND ( #EFF-DTE-DD EQ #DOB-DTE-DD )
        else if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().equals(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Mm()) && pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().equals(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())))
        {
            decideConditionsMet159++;
            pdaNsta1245.getNsta1245_Pnd_Months().setValue(0);                                                                                                             //Natural: COMPUTE #MONTHS = 0
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Leap_Year() throws Exception                                                                                                                         //Natural: LEAP-YEAR
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        pnd_Test_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Yyyy(), "0229"));                                      //Natural: COMPRESS #EFF-DTE-YYYY '0229' INTO #TEST-DATE LEAVING NO
        if (condition(DbsUtil.maskMatches(pnd_Test_Date,"YYYYMMDD")))                                                                                                     //Natural: IF #TEST-DATE = MASK ( YYYYMMDD )
        {
            pnd_Leap_Year.setValue(true);                                                                                                                                 //Natural: ASSIGN #LEAP-YEAR = TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Leap_Year.setValue(false);                                                                                                                                //Natural: ASSIGN #LEAP-YEAR = FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calc_Days() throws Exception                                                                                                                         //Natural: CALC-DAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************
        short decideConditionsMet198 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #EFF-DTE-DD LT #DOB-DTE-DD
        if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().less(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())))
        {
            decideConditionsMet198++;
            pnd_Previous_Mnth.compute(new ComputeParameters(false, pnd_Previous_Mnth), pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Mm().subtract(1));                             //Natural: COMPUTE #PREVIOUS-MNTH = #EFF-DTE-MM - 1
            if (condition(pnd_Previous_Mnth.equals(getZero())))                                                                                                           //Natural: IF #PREVIOUS-MNTH = 0
            {
                pnd_Previous_Mnth.setValue(12);                                                                                                                           //Natural: ASSIGN #PREVIOUS-MNTH = 12
            }                                                                                                                                                             //Natural: END-IF
            pnd_Days_Remaining.compute(new ComputeParameters(false, pnd_Days_Remaining), pnd_Days_Table_Pnd_Days_In_Month.getValue(pnd_Previous_Mnth).subtract(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())); //Natural: COMPUTE #DAYS-REMAINING = #DAYS-IN-MONTH ( #PREVIOUS-MNTH ) - #DOB-DTE-DD
            pdaNsta1245.getNsta1245_Pnd_Days().compute(new ComputeParameters(false, pdaNsta1245.getNsta1245_Pnd_Days()), pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().add(pnd_Days_Remaining)); //Natural: COMPUTE #DAYS = #EFF-DTE-DD + #DAYS-REMAINING
        }                                                                                                                                                                 //Natural: WHEN #EFF-DTE-DD GT #DOB-DTE-DD
        else if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().greater(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())))
        {
            decideConditionsMet198++;
            pdaNsta1245.getNsta1245_Pnd_Days().compute(new ComputeParameters(false, pdaNsta1245.getNsta1245_Pnd_Days()), pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().subtract(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())); //Natural: COMPUTE #DAYS = #EFF-DTE-DD - #DOB-DTE-DD
        }                                                                                                                                                                 //Natural: WHEN #EFF-DTE-DD EQ #DOB-DTE-DD
        else if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte_Dd().equals(pdaNsta1245.getNsta1245_Pnd_Dob_Dte_Dd())))
        {
            decideConditionsMet198++;
            pdaNsta1245.getNsta1245_Pnd_Days().setValue(0);                                                                                                               //Natural: ASSIGN #DAYS = 0
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaNstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("NATURAL ERROR", Global.getERROR_NR(), "AT LINE", Global.getERROR_LINE(),                     //Natural: COMPRESS 'NATURAL ERROR' *ERROR-NR 'AT LINE' *ERROR-LINE 'IN PROGRAM' *PROGRAM INTO ##MSG
            "IN PROGRAM", Global.getPROGRAM()));
        pdaNstpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                                  //Natural: ASSIGN ##RETURN-CODE = 'E'
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
}
