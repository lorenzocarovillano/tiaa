/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:25:44 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3809
************************************************************
**        * FILE NAME            : Mcsn3809.java
**        * CLASS NAME           : Mcsn3809
**        * INSTANCE NAME        : Mcsn3809
************************************************************
************************************************************************
* PROGRAM  : MCSN3809
* TITLE    : ACCESS FULFILLMENT RECORDS FROM THE CWF-ATI-FULFILLMENT
* FUNCTION : DISPLAY ATI-FULFILLMENT RECORDS ON 192 IN WPID ORDER
*          : FOR ADRS SYSTEM - SUCCESSFULL CASES ONLY
*
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------  -------- ---------------------------------------------------
* 01/24/01  GREENFI  ADDED IN PROCESSING FOR 'CONTACT' & 'DISCO' SYSTEMS
*
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3809 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;

    private DataAccessProgramView vw_cwf_Ati_Fulfill;
    private DbsField cwf_Ati_Fulfill_Ati_Rcrd_Type;

    private DbsGroup cwf_Ati_Fulfill_Ati_Rqst_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Pin;
    private DbsField cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Stts;
    private DbsField cwf_Ati_Fulfill_Ati_Wpid;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Racf_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Unit_Cde;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Error_Txt;
    private DbsField cwf_Ati_Fulfill_Ati_Prge_Ind;
    private DbsField cwf_Ati_Fulfill_Ati_Stts_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Cmtask_Ind;
    private DbsField cwf_Ati_Fulfill_Addr_Update_Ind;
    private DbsField cwf_Ati_Fulfill_Addr_Chnge_All_Ind;
    private DbsGroup cwf_Ati_Fulfill_Addr_Cntrct_Accnt_NbrMuGroup;
    private DbsField cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr;
    private DbsGroup cwf_Ati_Fulfill_Addr_Addrss_TxtMuGroup;
    private DbsField cwf_Ati_Fulfill_Addr_Addrss_Txt;
    private DbsField cwf_Ati_Fulfill_Addr_Addrss_Type_Ind;
    private DbsField cwf_Ati_Fulfill_Addr_Annual_Vctn_Ind;
    private DbsField cwf_Ati_Fulfill_Addr_Effctve_Start_Dte;
    private DbsField cwf_Ati_Fulfill_Addr_Effctve_End_Dte;
    private DbsField cwf_Ati_Fulfill_Addr_Mail_Type_Ind;
    private DbsField cwf_Ati_Fulfill_Addr_Zip_Cde;
    private DbsField cwf_Ati_Fulfill_Addr_Usage_Ind;
    private DbsField pnd_Ati_Mit_Key;

    private DbsGroup pnd_Ati_Mit_Key__R_Field_1;
    private DbsField pnd_Ati_Mit_Key_Pnd_Ati_Pin;
    private DbsField pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme;
    private DbsField pnd_Ati_Completed_Key;

    private DbsGroup pnd_Ati_Completed_Key__R_Field_2;
    private DbsField pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type_Cntrl;
    private DbsField pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme;
    private DbsField pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind;
    private DbsField pnd_Grand_Total_Open_Atis;
    private DbsField pnd_Grand_Total_Inprogress_Atis;
    private DbsField pnd_Grand_Total_Failed_Atis;
    private DbsField pnd_Grand_Total_Successful_Atis;
    private DbsField pnd_Tot_Ok_Intact_System;
    private DbsField pnd_Tot_Ok_Contact_System;
    private DbsField pnd_Tot_Ok_Disco_System;
    private DbsField pnd_Tot_Ok_Pss_System;
    private DbsField pnd_Tot_Ok_Icss_System;
    private DbsField pnd_Tot_Ok_Mcsg_System;
    private DbsField pnd_Tot_Ok_Mfta_Unit_Int;
    private DbsField pnd_Tot_Ok_Non_Mfta_Unit_Int;
    private DbsField pnd_Tot_Ok_Mfta_Unit_Con;
    private DbsField pnd_Tot_Ok_Non_Mfta_Unit_Con;
    private DbsField pnd_Tot_Ok_Mfta_Unit_Dis;
    private DbsField pnd_Tot_Ok_Non_Mfta_Unit_Dis;
    private DbsField pnd_Tot_Ok_Mfta_Unit_Pss;
    private DbsField pnd_Tot_Ok_Non_Mfta_Unit_Pss;
    private DbsField pnd_Tot_Ok_Mfta_Unit_Ics;
    private DbsField pnd_Tot_Ok_Non_Mfta_Unit_Ics;
    private DbsField pnd_Tot_Ok_Mfta_Unit_Mcs;
    private DbsField pnd_Tot_Ok_Non_Mfta_Unit_Mcs;
    private DbsField pnd_Interim_Acct_Name;
    private DbsField pnd_Process_Status_Literal;
    private DbsField pnd_Cmtask_Status_Literal;
    private DbsField pnd_Compressed_Text;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_Cc;
    private DbsField pnd_Prev_Wpid;
    private DbsField pnd_Wpid_Total;
    private DbsField pnd_Fld_Line;
    private DbsField pnd_Fld_Strt;
    private DbsField pnd_Fld_End;
    private DbsField pnd_Accnt_Amt_Type;
    private DbsField pnd_Field;
    private DbsField pnd_Title_Literal;
    private DbsField pnd_Input_Dte_Tme;
    private DbsField pnd_Input_Dte_D;
    private DbsField pnd_Orig_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Entry_Date;
    private DbsField pnd_Last_Page_Flag;
    private DbsField pnd_Mit_Log_Date;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Input_Data = parameters.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Ati_Fulfill = new DataAccessProgramView(new NameInfo("vw_cwf_Ati_Fulfill", "CWF-ATI-FULFILL"), "CWF_ATI_FULFILL_ADRS", "CWF_KDO_FULFILL");
        cwf_Ati_Fulfill_Ati_Rcrd_Type = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Rcrd_Type", "ATI-RCRD-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_RCRD_TYPE");

        cwf_Ati_Fulfill_Ati_Rqst_Id = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ATI_RQST_ID", "ATI-RQST-ID");
        cwf_Ati_Fulfill_Ati_Pin = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Pin", "ATI-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ATI_PIN");
        cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme", "ATI-MIT-LOG-DT-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_MIT_LOG_DT_TME");
        cwf_Ati_Fulfill_Ati_Prcss_Stts = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Stts", "ATI-PRCSS-STTS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRCSS_STTS");
        cwf_Ati_Fulfill_Ati_Wpid = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Wpid", "ATI-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ATI_WPID");
        cwf_Ati_Fulfill_Ati_Entry_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Dte_Tme", "ATI-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_ENTRY_DTE_TME");
        cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt", "ATI-ENTRY-SYSTM-OR-UNT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_SYSTM_OR_UNT");
        cwf_Ati_Fulfill_Ati_Entry_Racf_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Racf_Id", "ATI-ENTRY-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ATI_ENTRY_RACF_ID");
        cwf_Ati_Fulfill_Ati_Entry_Unit_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Unit_Cde", "ATI-ENTRY-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_UNIT_CDE");
        cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id", "ATI-PRCSS-APPLCTN-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_PRCSS_APPLCTN_ID");
        cwf_Ati_Fulfill_Ati_Error_Txt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Error_Txt", "ATI-ERROR-TXT", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "ATI_ERROR_TXT");
        cwf_Ati_Fulfill_Ati_Prge_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prge_Ind", "ATI-PRGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRGE_IND");
        cwf_Ati_Fulfill_Ati_Stts_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Stts_Dte_Tme", "ATI-STTS-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_STTS_DTE_TME");
        cwf_Ati_Fulfill_Ati_Cmtask_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Cmtask_Ind", "ATI-CMTASK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_CMTASK_IND");
        cwf_Ati_Fulfill_Addr_Update_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Update_Ind", "ADDR-UPDATE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADDR_UPDATE_IND");
        cwf_Ati_Fulfill_Addr_Update_Ind.setDdmHeader("ADDRESS/UPDATE IND");
        cwf_Ati_Fulfill_Addr_Chnge_All_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Chnge_All_Ind", "ADDR-CHNGE-ALL-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADDR_CHNGE_ALL_IND");
        cwf_Ati_Fulfill_Addr_Chnge_All_Ind.setDdmHeader("CHANGE ALL/CONTRACT");
        cwf_Ati_Fulfill_Addr_Cntrct_Accnt_NbrMuGroup = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ADDR_CNTRCT_ACCNT_NBRMuGroup", 
            "ADDR_CNTRCT_ACCNT_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_KDO_FULFILL_ADDR_CNTRCT_ACCNT_NBR");
        cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr = cwf_Ati_Fulfill_Addr_Cntrct_Accnt_NbrMuGroup.newFieldArrayInGroup("cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr", 
            "ADDR-CNTRCT-ACCNT-NBR", FieldType.STRING, 10, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADDR_CNTRCT_ACCNT_NBR");
        cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.setDdmHeader("CNTRCT/ACCT/NUMBER");
        cwf_Ati_Fulfill_Addr_Addrss_TxtMuGroup = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ADDR_ADDRSS_TXTMuGroup", "ADDR_ADDRSS_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_KDO_FULFILL_ADDR_ADDRSS_TXT");
        cwf_Ati_Fulfill_Addr_Addrss_Txt = cwf_Ati_Fulfill_Addr_Addrss_TxtMuGroup.newFieldArrayInGroup("cwf_Ati_Fulfill_Addr_Addrss_Txt", "ADDR-ADDRSS-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADDR_ADDRSS_TXT");
        cwf_Ati_Fulfill_Addr_Addrss_Txt.setDdmHeader("ADDRESS");
        cwf_Ati_Fulfill_Addr_Addrss_Type_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Addrss_Type_Ind", "ADDR-ADDRSS-TYPE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADDR_ADDRSS_TYPE_IND");
        cwf_Ati_Fulfill_Addr_Addrss_Type_Ind.setDdmHeader("ADDRESS/TYPE");
        cwf_Ati_Fulfill_Addr_Annual_Vctn_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Annual_Vctn_Ind", "ADDR-ANNUAL-VCTN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADDR_ANNUAL_VCTN_IND");
        cwf_Ati_Fulfill_Addr_Annual_Vctn_Ind.setDdmHeader("ANNUAL/VACATION");
        cwf_Ati_Fulfill_Addr_Effctve_Start_Dte = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Effctve_Start_Dte", "ADDR-EFFCTVE-START-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADDR_EFFCTVE_START_DTE");
        cwf_Ati_Fulfill_Addr_Effctve_Start_Dte.setDdmHeader("START/DATE");
        cwf_Ati_Fulfill_Addr_Effctve_End_Dte = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Effctve_End_Dte", "ADDR-EFFCTVE-END-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADDR_EFFCTVE_END_DTE");
        cwf_Ati_Fulfill_Addr_Effctve_End_Dte.setDdmHeader("END/DATE");
        cwf_Ati_Fulfill_Addr_Mail_Type_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Mail_Type_Ind", "ADDR-MAIL-TYPE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADDR_MAIL_TYPE_IND");
        cwf_Ati_Fulfill_Addr_Mail_Type_Ind.setDdmHeader("MAIL/TYPE");
        cwf_Ati_Fulfill_Addr_Zip_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Zip_Cde", "ADDR-ZIP-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "ADDR_ZIP_CDE");
        cwf_Ati_Fulfill_Addr_Zip_Cde.setDdmHeader("ZIP");
        cwf_Ati_Fulfill_Addr_Usage_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Usage_Ind", "ADDR-USAGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADDR_USAGE_IND");
        cwf_Ati_Fulfill_Addr_Usage_Ind.setDdmHeader("USAGE");
        registerRecord(vw_cwf_Ati_Fulfill);

        pnd_Ati_Mit_Key = localVariables.newFieldInRecord("pnd_Ati_Mit_Key", "#ATI-MIT-KEY", FieldType.BINARY, 14);

        pnd_Ati_Mit_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Ati_Mit_Key__R_Field_1", "REDEFINE", pnd_Ati_Mit_Key);
        pnd_Ati_Mit_Key_Pnd_Ati_Pin = pnd_Ati_Mit_Key__R_Field_1.newFieldInGroup("pnd_Ati_Mit_Key_Pnd_Ati_Pin", "#ATI-PIN", FieldType.NUMERIC, 7);
        pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme = pnd_Ati_Mit_Key__R_Field_1.newFieldInGroup("pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme", "#ATI-LOG-DTE-TME", FieldType.TIME);
        pnd_Ati_Completed_Key = localVariables.newFieldInRecord("pnd_Ati_Completed_Key", "#ATI-COMPLETED-KEY", FieldType.STRING, 9);

        pnd_Ati_Completed_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Ati_Completed_Key__R_Field_2", "REDEFINE", pnd_Ati_Completed_Key);
        pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type_Cntrl = pnd_Ati_Completed_Key__R_Field_2.newFieldInGroup("pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type_Cntrl", 
            "#ATI-RCRD-TYPE-CNTRL", FieldType.STRING, 1);
        pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme = pnd_Ati_Completed_Key__R_Field_2.newFieldInGroup("pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme", 
            "#ATI-ENTRY-DTE-TME", FieldType.TIME);
        pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind = pnd_Ati_Completed_Key__R_Field_2.newFieldInGroup("pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind", "#ATI-PRGE-IND", 
            FieldType.STRING, 1);
        pnd_Grand_Total_Open_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Open_Atis", "#GRAND-TOTAL-OPEN-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Inprogress_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Inprogress_Atis", "#GRAND-TOTAL-INPROGRESS-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Grand_Total_Failed_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Failed_Atis", "#GRAND-TOTAL-FAILED-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Successful_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Successful_Atis", "#GRAND-TOTAL-SUCCESSFUL-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Tot_Ok_Intact_System = localVariables.newFieldInRecord("pnd_Tot_Ok_Intact_System", "#TOT-OK-INTACT-SYSTEM", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Contact_System = localVariables.newFieldInRecord("pnd_Tot_Ok_Contact_System", "#TOT-OK-CONTACT-SYSTEM", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Disco_System = localVariables.newFieldInRecord("pnd_Tot_Ok_Disco_System", "#TOT-OK-DISCO-SYSTEM", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Pss_System = localVariables.newFieldInRecord("pnd_Tot_Ok_Pss_System", "#TOT-OK-PSS-SYSTEM", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Icss_System = localVariables.newFieldInRecord("pnd_Tot_Ok_Icss_System", "#TOT-OK-ICSS-SYSTEM", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Mcsg_System = localVariables.newFieldInRecord("pnd_Tot_Ok_Mcsg_System", "#TOT-OK-MCSG-SYSTEM", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Mfta_Unit_Int = localVariables.newFieldInRecord("pnd_Tot_Ok_Mfta_Unit_Int", "#TOT-OK-MFTA-UNIT-INT", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Non_Mfta_Unit_Int = localVariables.newFieldInRecord("pnd_Tot_Ok_Non_Mfta_Unit_Int", "#TOT-OK-NON-MFTA-UNIT-INT", FieldType.NUMERIC, 
            5);
        pnd_Tot_Ok_Mfta_Unit_Con = localVariables.newFieldInRecord("pnd_Tot_Ok_Mfta_Unit_Con", "#TOT-OK-MFTA-UNIT-CON", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Non_Mfta_Unit_Con = localVariables.newFieldInRecord("pnd_Tot_Ok_Non_Mfta_Unit_Con", "#TOT-OK-NON-MFTA-UNIT-CON", FieldType.NUMERIC, 
            5);
        pnd_Tot_Ok_Mfta_Unit_Dis = localVariables.newFieldInRecord("pnd_Tot_Ok_Mfta_Unit_Dis", "#TOT-OK-MFTA-UNIT-DIS", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Non_Mfta_Unit_Dis = localVariables.newFieldInRecord("pnd_Tot_Ok_Non_Mfta_Unit_Dis", "#TOT-OK-NON-MFTA-UNIT-DIS", FieldType.NUMERIC, 
            5);
        pnd_Tot_Ok_Mfta_Unit_Pss = localVariables.newFieldInRecord("pnd_Tot_Ok_Mfta_Unit_Pss", "#TOT-OK-MFTA-UNIT-PSS", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Non_Mfta_Unit_Pss = localVariables.newFieldInRecord("pnd_Tot_Ok_Non_Mfta_Unit_Pss", "#TOT-OK-NON-MFTA-UNIT-PSS", FieldType.NUMERIC, 
            5);
        pnd_Tot_Ok_Mfta_Unit_Ics = localVariables.newFieldInRecord("pnd_Tot_Ok_Mfta_Unit_Ics", "#TOT-OK-MFTA-UNIT-ICS", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Non_Mfta_Unit_Ics = localVariables.newFieldInRecord("pnd_Tot_Ok_Non_Mfta_Unit_Ics", "#TOT-OK-NON-MFTA-UNIT-ICS", FieldType.NUMERIC, 
            5);
        pnd_Tot_Ok_Mfta_Unit_Mcs = localVariables.newFieldInRecord("pnd_Tot_Ok_Mfta_Unit_Mcs", "#TOT-OK-MFTA-UNIT-MCS", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Non_Mfta_Unit_Mcs = localVariables.newFieldInRecord("pnd_Tot_Ok_Non_Mfta_Unit_Mcs", "#TOT-OK-NON-MFTA-UNIT-MCS", FieldType.NUMERIC, 
            5);
        pnd_Interim_Acct_Name = localVariables.newFieldInRecord("pnd_Interim_Acct_Name", "#INTERIM-ACCT-NAME", FieldType.STRING, 11);
        pnd_Process_Status_Literal = localVariables.newFieldInRecord("pnd_Process_Status_Literal", "#PROCESS-STATUS-LITERAL", FieldType.STRING, 7);
        pnd_Cmtask_Status_Literal = localVariables.newFieldInRecord("pnd_Cmtask_Status_Literal", "#CMTASK-STATUS-LITERAL", FieldType.STRING, 27);
        pnd_Compressed_Text = localVariables.newFieldInRecord("pnd_Compressed_Text", "#COMPRESSED-TEXT", FieldType.STRING, 38);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 3);
        pnd_Cc = localVariables.newFieldInRecord("pnd_Cc", "#CC", FieldType.NUMERIC, 3);
        pnd_Prev_Wpid = localVariables.newFieldInRecord("pnd_Prev_Wpid", "#PREV-WPID", FieldType.STRING, 6);
        pnd_Wpid_Total = localVariables.newFieldInRecord("pnd_Wpid_Total", "#WPID-TOTAL", FieldType.NUMERIC, 8);
        pnd_Fld_Line = localVariables.newFieldInRecord("pnd_Fld_Line", "#FLD-LINE", FieldType.NUMERIC, 3);
        pnd_Fld_Strt = localVariables.newFieldInRecord("pnd_Fld_Strt", "#FLD-STRT", FieldType.NUMERIC, 3);
        pnd_Fld_End = localVariables.newFieldInRecord("pnd_Fld_End", "#FLD-END", FieldType.NUMERIC, 3);
        pnd_Accnt_Amt_Type = localVariables.newFieldInRecord("pnd_Accnt_Amt_Type", "#ACCNT-AMT-TYPE", FieldType.STRING, 1);
        pnd_Field = localVariables.newFieldInRecord("pnd_Field", "#FIELD", FieldType.STRING, 30);
        pnd_Title_Literal = localVariables.newFieldInRecord("pnd_Title_Literal", "#TITLE-LITERAL", FieldType.STRING, 54);
        pnd_Input_Dte_Tme = localVariables.newFieldInRecord("pnd_Input_Dte_Tme", "#INPUT-DTE-TME", FieldType.TIME);
        pnd_Input_Dte_D = localVariables.newFieldInRecord("pnd_Input_Dte_D", "#INPUT-DTE-D", FieldType.DATE);
        pnd_Orig_Date = localVariables.newFieldInRecord("pnd_Orig_Date", "#ORIG-DATE", FieldType.DATE);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 8);
        pnd_Entry_Date = localVariables.newFieldInRecord("pnd_Entry_Date", "#ENTRY-DATE", FieldType.DATE);
        pnd_Last_Page_Flag = localVariables.newFieldInRecord("pnd_Last_Page_Flag", "#LAST-PAGE-FLAG", FieldType.STRING, 1);
        pnd_Mit_Log_Date = localVariables.newFieldInRecord("pnd_Mit_Log_Date", "#MIT-LOG-DATE", FieldType.NUMERIC, 14);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Ati_Fulfill.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Mcsn3809() throws Exception
    {
        super("Mcsn3809");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt6, 6);
        getReports().atEndOfPage(atEndEventRpt6, 6);
        setupReports();
        //*  ----------------------------------------------------------------------
        //*   MAIN PROGRAM
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 6 ) PS = 60 LS = 133 ZP = OFF
        if (condition(pnd_Input_Data_Pnd_Input_Date.notEquals(" ")))                                                                                                      //Natural: IF #INPUT-DATE NE ' '
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Input_Dte_Tme.setValue(pnd_Input_Dte_D);                                                                                                                  //Natural: MOVE #INPUT-DTE-D TO #INPUT-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(6, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***");                                     //Natural: WRITE ( 6 ) /// '*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***'
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
        //* *** INITIALIZE THE ATI-CONTROL-KEY SUPER-DESCRIPTOR FIELD
        pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type_Cntrl.setValue("3");                                                                                                      //Natural: ASSIGN #ATI-RCRD-TYPE-CNTRL := '3'
        pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme.reset();                                                                                                              //Natural: RESET #ATI-ENTRY-DTE-TME
        //* *****************************************************************
        //* ** READ CWF-ATI-FULFILL AND PROCESS OPEN AND IN-PROGRESS REQUESTS
        //* *****************************************************************
        getReports().newPage(new ReportSpecification(6));                                                                                                                 //Natural: NEWPAGE ( 6 )
        if (condition(Global.isEscape())){return;}
        vw_cwf_Ati_Fulfill.startDatabaseRead                                                                                                                              //Natural: READ CWF-ATI-FULFILL BY ATI-COMPLETED-KEY STARTING FROM #ATI-COMPLETED-KEY
        (
        "READ_ATI",
        new Wc[] { new Wc("ATI_COMPLETED_KEY", ">=", pnd_Ati_Completed_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("ATI_COMPLETED_KEY", "ASC") }
        );
        READ_ATI:
        while (condition(vw_cwf_Ati_Fulfill.readNextRow("READ_ATI")))
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Entry_Date.setValue(cwf_Ati_Fulfill_Ati_Stts_Dte_Tme);                                                                                                    //Natural: MOVE CWF-ATI-FULFILL.ATI-STTS-DTE-TME TO #ENTRY-DATE
            if (condition(!(pnd_Entry_Date.equals(pnd_Input_Dte_D) && cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id.equals("ADRS") && cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))) //Natural: ACCEPT IF #ENTRY-DATE = #INPUT-DTE-D AND ATI-PRCSS-APPLCTN-ID = 'ADRS' AND CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
            {
                continue;
            }
            getSort().writeSortInData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type,               //Natural: END-ALL
                cwf_Ati_Fulfill_Ati_Prcss_Stts, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, 
                cwf_Ati_Fulfill_Ati_Entry_Racf_Id, cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Addr_Update_Ind, 
                cwf_Ati_Fulfill_Addr_Chnge_All_Ind, cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(1), cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(2), 
                cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(3), cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(4), cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(5), 
                cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(6), cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(7), cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(8), 
                cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(9), cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(10), cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(1), 
                cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(2), cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(3), cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(4), cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(5), 
                cwf_Ati_Fulfill_Addr_Addrss_Type_Ind, cwf_Ati_Fulfill_Addr_Annual_Vctn_Ind, cwf_Ati_Fulfill_Addr_Effctve_Start_Dte, cwf_Ati_Fulfill_Addr_Effctve_End_Dte, 
                cwf_Ati_Fulfill_Addr_Mail_Type_Ind, cwf_Ati_Fulfill_Addr_Zip_Cde, cwf_Ati_Fulfill_Addr_Usage_Ind);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  SORT PERFORMED TO WRITE THEM PIN/MIT-LOG-DATE-TIME/REC-TYPE ORDER
        getSort().sortData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type, cwf_Ati_Fulfill_Ati_Prcss_Stts); //Natural: SORT RECORDS BY CWF-ATI-FULFILL.ATI-WPID CWF-ATI-FULFILL.ATI-PIN CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME CWF-ATI-FULFILL.ATI-RCRD-TYPE CWF-ATI-FULFILL.ATI-PRCSS-STTS USING CWF-ATI-FULFILL.ATI-ERROR-TXT CWF-ATI-FULFILL.ATI-ENTRY-DTE-TME CWF-ATI-FULFILL.ATI-ENTRY-SYSTM-OR-UNT CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE CWF-ATI-FULFILL.ATI-STTS-DTE-TME CWF-ATI-FULFILL.ADDR-UPDATE-IND CWF-ATI-FULFILL.ADDR-CHNGE-ALL-IND CWF-ATI-FULFILL.ADDR-CNTRCT-ACCNT-NBR ( 1:10 ) CWF-ATI-FULFILL.ADDR-ADDRSS-TXT ( 1:5 ) CWF-ATI-FULFILL.ADDR-ADDRSS-TYPE-IND CWF-ATI-FULFILL.ADDR-ANNUAL-VCTN-IND CWF-ATI-FULFILL.ADDR-EFFCTVE-START-DTE CWF-ATI-FULFILL.ADDR-EFFCTVE-END-DTE CWF-ATI-FULFILL.ADDR-MAIL-TYPE-IND CWF-ATI-FULFILL.ADDR-ZIP-CDE CWF-ATI-FULFILL.ADDR-USAGE-IND
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type, 
            cwf_Ati_Fulfill_Ati_Prcss_Stts, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, cwf_Ati_Fulfill_Ati_Entry_Racf_Id, 
            cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Addr_Update_Ind, cwf_Ati_Fulfill_Addr_Chnge_All_Ind, cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(1), 
            cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(2), cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(3), cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(4), 
            cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(5), cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(6), cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(7), 
            cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(8), cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(9), cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(10), 
            cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(1), cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(2), cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(3), cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(4), 
            cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(5), cwf_Ati_Fulfill_Addr_Addrss_Type_Ind, cwf_Ati_Fulfill_Addr_Annual_Vctn_Ind, cwf_Ati_Fulfill_Addr_Effctve_Start_Dte, 
            cwf_Ati_Fulfill_Addr_Effctve_End_Dte, cwf_Ati_Fulfill_Addr_Mail_Type_Ind, cwf_Ati_Fulfill_Addr_Zip_Cde, cwf_Ati_Fulfill_Addr_Usage_Ind)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF CWF-ATI-FULFILL.ATI-WPID
            pnd_Prev_Wpid.setValue(cwf_Ati_Fulfill_Ati_Wpid);                                                                                                             //Natural: ASSIGN #PREV-WPID := CWF-ATI-FULFILL.ATI-WPID
            pnd_Wpid_Total.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WPID-TOTAL
                                                                                                                                                                          //Natural: PERFORM ATI-WRITE-REC-1
            sub_Ati_Write_Rec_1();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //*  ----------------------------------------------------------------------
        //*  4T CWF-ATI-FULFILL.ATI-PIN
        //*  20T CWF-ATI-FULFILL.ATI-WPID
        //*  27T CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME (EM=MM/DD/YYYY' 'HH:II:SS)
        //*  48T CWF-ATI-FULFILL.ATI-STTS-DTE-TME   (EM=MM/DD/YYYY' 'HH:II:SS)
        //*  68T #PROCESS-STATUS-LITERAL
        //*  78T CWF-ATI-FULFILL.ADDR-UPDATE-IND
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 6 )
        //*                                                                                                                                                               //Natural: AT END OF PAGE ( 6 )
        //* ***********************************************************************
        //*   WRITE OUT ATI GRAND TOTALS ON LAST PAGE OF REPORT
        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(6));                                                                                                                 //Natural: NEWPAGE ( 6 )
        if (condition(Global.isEscape())){return;}
        pnd_Last_Page_Flag.setValue("Y");                                                                                                                                 //Natural: ASSIGN #LAST-PAGE-FLAG := 'Y'
        getReports().write(6, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,new  //Natural: WRITE ( 6 ) //////// 1T '-' ( 131 ) // 1T '-' ( 50 ) 52T 'ATI ADRS CHANGE SUCCESSFUL TOTALS' 88T '-' ( 44 ) // 1T '-' ( 131 ) ///// 45T 'SUCCESSFUL ADRS ATIS FROM INTACT :' #TOT-OK-INTACT-SYSTEM 88T 'MFTA : ' #TOT-OK-MFTA-UNIT-INT 104T 'NON-MFTA : ' #TOT-OK-NON-MFTA-UNIT-INT / 45T 'SUCCESSFUL ADRS ATIS FROM CONTACT:' #TOT-OK-CONTACT-SYSTEM 88T 'MFTA : ' #TOT-OK-MFTA-UNIT-CON 104T 'NON-MFTA : ' #TOT-OK-NON-MFTA-UNIT-CON / 45T 'SUCCESSFUL ADRS ATIS FROM DISCO  :' #TOT-OK-DISCO-SYSTEM 88T 'MFTA : ' #TOT-OK-MFTA-UNIT-DIS 104T 'NON-MFTA : ' #TOT-OK-NON-MFTA-UNIT-DIS / 45T 'SUCCESSFUL ADRS ATIS FROM PSS    :' #TOT-OK-PSS-SYSTEM 88T 'MFTA : ' #TOT-OK-MFTA-UNIT-PSS 104T 'NON-MFTA : ' #TOT-OK-NON-MFTA-UNIT-PSS / 45T 'SUCCESSFUL ADRS ATIS FROM MCSG   :' #TOT-OK-MCSG-SYSTEM 88T 'MFTA : ' #TOT-OK-MFTA-UNIT-MCS 104T 'NON-MFTA : ' #TOT-OK-NON-MFTA-UNIT-MCS / 45T 'SUCCESSFUL ADRS ATIS FROM ICSS   :' #TOT-OK-ICSS-SYSTEM 88T 'MFTA : ' #TOT-OK-MFTA-UNIT-ICS 104T 'NON-MFTA : ' #TOT-OK-NON-MFTA-UNIT-ICS / 45T '--------------------------------' // 45T 'TOTAL SUCCESSFUL ADRS ATIS     :' #GRAND-TOTAL-SUCCESSFUL-ATIS
            TabSetting(1),"-",new RepeatItem(50),new TabSetting(52),"ATI ADRS CHANGE SUCCESSFUL TOTALS",new TabSetting(88),"-",new RepeatItem(44),NEWLINE,NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"SUCCESSFUL ADRS ATIS FROM INTACT :",pnd_Tot_Ok_Intact_System,new 
            TabSetting(88),"MFTA : ",pnd_Tot_Ok_Mfta_Unit_Int,new TabSetting(104),"NON-MFTA : ",pnd_Tot_Ok_Non_Mfta_Unit_Int,NEWLINE,new TabSetting(45),"SUCCESSFUL ADRS ATIS FROM CONTACT:",pnd_Tot_Ok_Contact_System,new 
            TabSetting(88),"MFTA : ",pnd_Tot_Ok_Mfta_Unit_Con,new TabSetting(104),"NON-MFTA : ",pnd_Tot_Ok_Non_Mfta_Unit_Con,NEWLINE,new TabSetting(45),"SUCCESSFUL ADRS ATIS FROM DISCO  :",pnd_Tot_Ok_Disco_System,new 
            TabSetting(88),"MFTA : ",pnd_Tot_Ok_Mfta_Unit_Dis,new TabSetting(104),"NON-MFTA : ",pnd_Tot_Ok_Non_Mfta_Unit_Dis,NEWLINE,new TabSetting(45),"SUCCESSFUL ADRS ATIS FROM PSS    :",pnd_Tot_Ok_Pss_System,new 
            TabSetting(88),"MFTA : ",pnd_Tot_Ok_Mfta_Unit_Pss,new TabSetting(104),"NON-MFTA : ",pnd_Tot_Ok_Non_Mfta_Unit_Pss,NEWLINE,new TabSetting(45),"SUCCESSFUL ADRS ATIS FROM MCSG   :",pnd_Tot_Ok_Mcsg_System,new 
            TabSetting(88),"MFTA : ",pnd_Tot_Ok_Mfta_Unit_Mcs,new TabSetting(104),"NON-MFTA : ",pnd_Tot_Ok_Non_Mfta_Unit_Mcs,NEWLINE,new TabSetting(45),"SUCCESSFUL ADRS ATIS FROM ICSS   :",pnd_Tot_Ok_Icss_System,new 
            TabSetting(88),"MFTA : ",pnd_Tot_Ok_Mfta_Unit_Ics,new TabSetting(104),"NON-MFTA : ",pnd_Tot_Ok_Non_Mfta_Unit_Ics,NEWLINE,new TabSetting(45),"--------------------------------",NEWLINE,NEWLINE,new 
            TabSetting(45),"TOTAL SUCCESSFUL ADRS ATIS     :",pnd_Grand_Total_Successful_Atis);
        if (Global.isEscape()) return;
    }
    private void sub_Ati_Write_Rec_1() throws Exception                                                                                                                   //Natural: ATI-WRITE-REC-1
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM SET-PROCESS-STATUS
        sub_Set_Process_Status();
        if (condition(Global.isEscape())) {return;}
        if (condition(getReports().getAstLinesLeft(6).less(15)))                                                                                                          //Natural: NEWPAGE ( 6 ) IF LESS THAN 15 LINES LEFT
        {
            getReports().newPage(6);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Compressed_Text.reset();                                                                                                                                      //Natural: RESET #COMPRESSED-TEXT
        //*  PIN-EXP
        //*   PIN-EXP>>
        pnd_Compressed_Text.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "ENTRY RACF-ID/UNIT= ", cwf_Ati_Fulfill_Ati_Entry_Racf_Id, "/", cwf_Ati_Fulfill_Ati_Entry_Unit_Cde)); //Natural: COMPRESS 'ENTRY RACF-ID/UNIT= ' CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID '/' CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE INTO #COMPRESSED-TEXT LEAVING NO SPACE
        getReports().write(6, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),cwf_Ati_Fulfill_Ati_Pin,new TabSetting(15),cwf_Ati_Fulfill_Ati_Wpid,new TabSetting(22),cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme,  //Natural: WRITE ( 6 ) / 1T CWF-ATI-FULFILL.ATI-PIN 15T CWF-ATI-FULFILL.ATI-WPID 22T CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 43T CWF-ATI-FULFILL.ATI-STTS-DTE-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 63T #PROCESS-STATUS-LITERAL 73T CWF-ATI-FULFILL.ADDR-UPDATE-IND 80T CWF-ATI-FULFILL.ADDR-CHNGE-ALL-IND 86T CWF-ATI-FULFILL.ADDR-ADDRSS-TYPE-IND 92T CWF-ATI-FULFILL.ADDR-ANNUAL-VCTN-IND 98T CWF-ATI-FULFILL.ADDR-MAIL-TYPE-IND 105T CWF-ATI-FULFILL.ADDR-USAGE-IND 108T CWF-ATI-FULFILL.ADDR-ZIP-CDE 115T CWF-ATI-FULFILL.ATI-ENTRY-SYSTM-OR-UNT 124T CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE
            new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(43),cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new 
            TabSetting(63),pnd_Process_Status_Literal,new TabSetting(73),cwf_Ati_Fulfill_Addr_Update_Ind,new TabSetting(80),cwf_Ati_Fulfill_Addr_Chnge_All_Ind,new 
            TabSetting(86),cwf_Ati_Fulfill_Addr_Addrss_Type_Ind,new TabSetting(92),cwf_Ati_Fulfill_Addr_Annual_Vctn_Ind,new TabSetting(98),cwf_Ati_Fulfill_Addr_Mail_Type_Ind,new 
            TabSetting(105),cwf_Ati_Fulfill_Addr_Usage_Ind,new TabSetting(108),cwf_Ati_Fulfill_Addr_Zip_Cde,new TabSetting(115),cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt,new 
            TabSetting(124),cwf_Ati_Fulfill_Ati_Entry_Unit_Cde);
        if (Global.isEscape()) return;
        getReports().write(6, ReportOption.NOTITLE,new TabSetting(10),"START EFFECTIVE DATES :",cwf_Ati_Fulfill_Addr_Effctve_Start_Dte, new ReportEditMask                //Natural: WRITE ( 6 ) 10T 'START EFFECTIVE DATES :' CWF-ATI-FULFILL.ADDR-EFFCTVE-START-DTE ( EM = 9999/99/99 ) 8X 'END EFFECTIVE DATES :' CWF-ATI-FULFILL.ADDR-EFFCTVE-END-DTE ( EM = 9999/99/99 )
            ("9999/99/99"),new ColumnSpacing(8),"END EFFECTIVE DATES :",cwf_Ati_Fulfill_Addr_Effctve_End_Dte, new ReportEditMask ("9999/99/99"));
        if (Global.isEscape()) return;
        getReports().write(6, ReportOption.NOTITLE,new TabSetting(10),"ADDRESS :",cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(1));                                           //Natural: WRITE ( 6 ) 10T 'ADDRESS :' CWF-ATI-FULFILL.ADDR-ADDRSS-TXT ( 1 )
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #A = 2 TO 5
        for (pnd_A.setValue(2); condition(pnd_A.lessOrEqual(5)); pnd_A.nadd(1))
        {
            if (condition(cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(pnd_A).notEquals(" ")))                                                                                //Natural: IF CWF-ATI-FULFILL.ADDR-ADDRSS-TXT ( #A ) NE ' '
            {
                getReports().write(6, ReportOption.NOTITLE,new TabSetting(20),cwf_Ati_Fulfill_Addr_Addrss_Txt.getValue(pnd_A));                                           //Natural: WRITE ( 6 ) 20T CWF-ATI-FULFILL.ADDR-ADDRSS-TXT ( #A )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(6, ReportOption.NOTITLE,new TabSetting(4),"CONTRACT/S :",new ColumnSpacing(1),cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(1),new            //Natural: WRITE ( 6 ) 4T 'CONTRACT/S :' 1X CWF-ATI-FULFILL.ADDR-CNTRCT-ACCNT-NBR ( 1 ) 1X CWF-ATI-FULFILL.ADDR-CNTRCT-ACCNT-NBR ( 2 ) 1X CWF-ATI-FULFILL.ADDR-CNTRCT-ACCNT-NBR ( 3 ) 1X CWF-ATI-FULFILL.ADDR-CNTRCT-ACCNT-NBR ( 4 ) 1X CWF-ATI-FULFILL.ADDR-CNTRCT-ACCNT-NBR ( 5 ) 1X CWF-ATI-FULFILL.ADDR-CNTRCT-ACCNT-NBR ( 6 ) 1X CWF-ATI-FULFILL.ADDR-CNTRCT-ACCNT-NBR ( 7 ) 1X CWF-ATI-FULFILL.ADDR-CNTRCT-ACCNT-NBR ( 8 ) 1X CWF-ATI-FULFILL.ADDR-CNTRCT-ACCNT-NBR ( 9 ) 1X CWF-ATI-FULFILL.ADDR-CNTRCT-ACCNT-NBR ( 10 )
            ColumnSpacing(1),cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(2),new ColumnSpacing(1),cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(3),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(4),new ColumnSpacing(1),cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(5),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(6),new ColumnSpacing(1),cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(7),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(8),new ColumnSpacing(1),cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(9),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.getValue(10));
        if (Global.isEscape()) return;
        if (condition(cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
        {
            getReports().write(6, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text);                                                                           //Natural: WRITE ( 6 ) 15T #COMPRESSED-TEXT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(6, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text,new TabSetting(55),"ERROR MESSAGE = :",new TabSetting(73),                  //Natural: WRITE ( 6 ) 15T #COMPRESSED-TEXT 55T 'ERROR MESSAGE = :' 73T CWF-ATI-FULFILL.ATI-ERROR-TXT
                cwf_Ati_Fulfill_Ati_Error_Txt);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*   END  ATI-WRITE-REC-2 PROCESSING
    }
    private void sub_Set_Process_Status() throws Exception                                                                                                                //Natural: SET-PROCESS-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        short decideConditionsMet250 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CWF-ATI-FULFILL.ATI-PRCSS-STTS;//Natural: VALUE 'O'
        if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("O"))))
        {
            decideConditionsMet250++;
            pnd_Process_Status_Literal.setValue(" OPEN ");                                                                                                                //Natural: MOVE ' OPEN ' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Open_Atis.nadd(1);                                                                                                                            //Natural: ADD 1 TO #GRAND-TOTAL-OPEN-ATIS
            pnd_Input_Data_Pnd_W_Ati_Open.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                                     //Natural: ADD 1 TO #W-ATI-OPEN ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("I"))))
        {
            decideConditionsMet250++;
            pnd_Process_Status_Literal.setValue("INPROGS");                                                                                                               //Natural: MOVE 'INPROGS' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Inprogress_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-INPROGRESS-ATIS
            pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                              //Natural: ADD 1 TO #W-ATI-IN-PROGRESS ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE ' '
        else if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals(" "))))
        {
            decideConditionsMet250++;
            if (condition(cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))                                                                                                     //Natural: IF CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
            {
                pnd_Process_Status_Literal.setValue("SUCCESS");                                                                                                           //Natural: MOVE 'SUCCESS' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Successful_Atis.nadd(1);                                                                                                                  //Natural: ADD 1 TO #GRAND-TOTAL-SUCCESSFUL-ATIS
                pnd_Input_Data_Pnd_W_Ati_Successful.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                           //Natural: ADD 1 TO #W-ATI-SUCCESSFUL ( #W-INX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Process_Status_Literal.setValue("FAILED");                                                                                                            //Natural: MOVE 'FAILED' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Failed_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-FAILED-ATIS
                pnd_Input_Data_Pnd_W_Ati_Manual.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                               //Natural: ADD 1 TO #W-ATI-MANUAL ( #W-INX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Process_Status_Literal.setValue("INCORCT");                                                                                                               //Natural: MOVE 'INCORCT' TO #PROCESS-STATUS-LITERAL
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet273 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CWF-ATI-FULFILL.ATI-ENTRY-SYSTM-OR-UNT;//Natural: VALUE 'INTACT'
        if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("INTACT"))))
        {
            decideConditionsMet273++;
            pnd_Tot_Ok_Intact_System.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOT-OK-INTACT-SYSTEM
            if (condition(cwf_Ati_Fulfill_Ati_Entry_Unit_Cde.equals("MFTA")))                                                                                             //Natural: IF CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE = 'MFTA'
            {
                pnd_Tot_Ok_Mfta_Unit_Int.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-OK-MFTA-UNIT-INT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Ok_Non_Mfta_Unit_Int.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOT-OK-NON-MFTA-UNIT-INT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'CONTACT'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("CONTACT"))))
        {
            decideConditionsMet273++;
            pnd_Tot_Ok_Contact_System.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOT-OK-CONTACT-SYSTEM
            if (condition(cwf_Ati_Fulfill_Ati_Entry_Unit_Cde.equals("MFTA")))                                                                                             //Natural: IF CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE = 'MFTA'
            {
                pnd_Tot_Ok_Mfta_Unit_Con.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-OK-MFTA-UNIT-CON
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Ok_Non_Mfta_Unit_Con.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOT-OK-NON-MFTA-UNIT-CON
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'DISCO'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("DISCO"))))
        {
            decideConditionsMet273++;
            pnd_Tot_Ok_Disco_System.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOT-OK-DISCO-SYSTEM
            if (condition(cwf_Ati_Fulfill_Ati_Entry_Unit_Cde.equals("MFTA")))                                                                                             //Natural: IF CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE = 'MFTA'
            {
                pnd_Tot_Ok_Mfta_Unit_Dis.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-OK-MFTA-UNIT-DIS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Ok_Non_Mfta_Unit_Dis.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOT-OK-NON-MFTA-UNIT-DIS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'PSS'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("PSS"))))
        {
            decideConditionsMet273++;
            pnd_Tot_Ok_Pss_System.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOT-OK-PSS-SYSTEM
            if (condition(cwf_Ati_Fulfill_Ati_Entry_Unit_Cde.equals("MFTA")))                                                                                             //Natural: IF CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE = 'MFTA'
            {
                pnd_Tot_Ok_Mfta_Unit_Pss.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-OK-MFTA-UNIT-PSS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Ok_Non_Mfta_Unit_Pss.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOT-OK-NON-MFTA-UNIT-PSS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'MCSG'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("MCSG"))))
        {
            decideConditionsMet273++;
            pnd_Tot_Ok_Mcsg_System.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-OK-MCSG-SYSTEM
            if (condition(cwf_Ati_Fulfill_Ati_Entry_Unit_Cde.equals("MFTA")))                                                                                             //Natural: IF CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE = 'MFTA'
            {
                pnd_Tot_Ok_Mfta_Unit_Mcs.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-OK-MFTA-UNIT-MCS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Ok_Non_Mfta_Unit_Mcs.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOT-OK-NON-MFTA-UNIT-MCS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'ICSS'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("ICSS"))))
        {
            decideConditionsMet273++;
            pnd_Tot_Ok_Icss_System.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-OK-ICSS-SYSTEM
            if (condition(cwf_Ati_Fulfill_Ati_Entry_Unit_Cde.equals("MFTA")))                                                                                             //Natural: IF CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE = 'MFTA'
            {
                pnd_Tot_Ok_Mfta_Unit_Ics.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-OK-MFTA-UNIT-ICS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Ok_Non_Mfta_Unit_Ics.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOT-OK-NON-MFTA-UNIT-ICS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt6 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(6, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(50),"AUTOMATED TRANSACTION SYSTEM",new                //Natural: WRITE ( 6 ) NOTITLE 001T *PROGRAM 050T 'AUTOMATED TRANSACTION SYSTEM' 101T *DATU 110T *TIME ( AL = 005 ) 121T 'PAGE:' 127T *PAGE-NUMBER ( 6 ) ( AD = L ) / 035T 'ATI DETAILED REPORT FOR SUCCESSFUL ADRS RQSTS IN WPID SEQ' / 037T 'PROCESSED SUCCESSFUL ATI ADRS REQUESTS FOR :' 082T #INPUT-DTE-D ( EM = MM/DD/YY ) / 001T '-' ( 131 ) / 6T 'PIN' 26T 'MASTER INDEX' 48T 'ATI STATUS' 63T 'PROCESS' 72T 'UPDT' 79T 'CHNG' 85T 'ADRS' 91T 'ANN.' 97T 'MAIL' 103T 'USAGE' 110T 'ZIP' 115T 'ENTRY' 124T 'ENTRY' / 4T 'NUMBER' 16T 'WPID' 24T 'LOG DATE & TIME' 47T 'DATE & TIME' 63T 'STATUS' 72T 'IND' 79T 'ALL' 85T 'TYPE' 91T 'VCTN' 97T 'TYPE' 104T 'IND' 109T 'CODE' 115T 'SYSTM' 124T 'UNIT' / 001T '-' ( 131 ) /
                        TabSetting(101),Global.getDATU(),new TabSetting(110),Global.getTIME(), new AlphanumericLength (5),new TabSetting(121),"PAGE:",new 
                        TabSetting(127),getReports().getPageNumberDbs(6), new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(35),"ATI DETAILED REPORT FOR SUCCESSFUL ADRS RQSTS IN WPID SEQ",NEWLINE,new 
                        TabSetting(37),"PROCESSED SUCCESSFUL ATI ADRS REQUESTS FOR :",new TabSetting(82),pnd_Input_Dte_D, new ReportEditMask ("MM/DD/YY"),NEWLINE,new 
                        TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(6),"PIN",new TabSetting(26),"MASTER INDEX",new TabSetting(48),"ATI STATUS",new 
                        TabSetting(63),"PROCESS",new TabSetting(72),"UPDT",new TabSetting(79),"CHNG",new TabSetting(85),"ADRS",new TabSetting(91),"ANN.",new 
                        TabSetting(97),"MAIL",new TabSetting(103),"USAGE",new TabSetting(110),"ZIP",new TabSetting(115),"ENTRY",new TabSetting(124),"ENTRY",NEWLINE,new 
                        TabSetting(4),"NUMBER",new TabSetting(16),"WPID",new TabSetting(24),"LOG DATE & TIME",new TabSetting(47),"DATE & TIME",new TabSetting(63),"STATUS",new 
                        TabSetting(72),"IND",new TabSetting(79),"ALL",new TabSetting(85),"TYPE",new TabSetting(91),"VCTN",new TabSetting(97),"TYPE",new 
                        TabSetting(104),"IND",new TabSetting(109),"CODE",new TabSetting(115),"SYSTM",new TabSetting(124),"UNIT",NEWLINE,new TabSetting(1),"-",new 
                        RepeatItem(131),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atEndEventRpt6 = (Object sender, EventArgs e) ->
    {
        String localMethod = "MCSN3809|atEndEventRpt6";
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Last_Page_Flag.equals("Y")))                                                                                                        //Natural: IF #LAST-PAGE-FLAG = 'Y'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(6, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(1),"-",new                    //Natural: WRITE ( 6 ) / 1T '-' ( 131 ) / 1T '-' ( 131 )
                            RepeatItem(131));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-ENDPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Ati_Fulfill_Ati_WpidIsBreak = cwf_Ati_Fulfill_Ati_Wpid.isBreak(endOfData);
        if (condition(cwf_Ati_Fulfill_Ati_WpidIsBreak))
        {
            getReports().skip(6, 2);                                                                                                                                      //Natural: SKIP ( 6 ) 2
            getReports().write(6, ReportOption.NOTITLE,new TabSetting(1),"TOTAL FOR WPID :",pnd_Prev_Wpid, new ReportEditMask ("XX' 'X' 'XX' 'X"),":",                    //Natural: WRITE ( 6 ) 1T 'TOTAL FOR WPID :' #PREV-WPID ( EM = XX' 'X' 'XX' 'X ) ':' #WPID-TOTAL
                pnd_Wpid_Total);
            if (condition(Global.isEscape())) return;
            pnd_Wpid_Total.reset();                                                                                                                                       //Natural: RESET #WPID-TOTAL
            getReports().newPage(new ReportSpecification(6));                                                                                                             //Natural: NEWPAGE ( 6 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=OFF");
        Global.format(6, "PS=60 LS=133 ZP=OFF");
    }
}
