/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:31:04 AM
**        * FROM NATURAL SUBPROGRAM : Mihn0200
************************************************************
**        * FILE NAME            : Mihn0200.java
**        * CLASS NAME           : Mihn0200
**        * INSTANCE NAME        : Mihn0200
************************************************************
************************************************************************
* PROGRAM  : MIHN0200
* SYSTEM   :
* TITLE    : SET UP RUN CONTROL FOR THE MIT ISN DELETE PROCESS
* GENERATED: MAY 10,96 AT 15:01 PM
* FUNCTION : THIS COMMON ROUTINE CAN BE CALLED BY THE DELETE PROCESS
*          : TO DO THE INITIAL SET UP OF THE RUN CONTROL RECORD.
*
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mihn0200 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaMiha0200 pdaMiha0200;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaMihl0180 ldaMihl0180;
    private LdaMihl0181 ldaMihl0181;
    private LdaMihl0110 ldaMihl0110;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Who_Support_Tbl_Histogram;
    private DbsField cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key;
    private DbsField pnd_Cut_Off_Date_D;
    private DbsField pnd_Cut_Off_Date;

    private DbsGroup pnd_Cut_Off_Date__R_Field_1;
    private DbsField pnd_Cut_Off_Date_Pnd_Alpha;
    private DbsField pnd_Last_Run_Isn;
    private DbsField pnd_New_Run_Id;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_2;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_3;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaMihl0180 = new LdaMihl0180();
        registerRecord(ldaMihl0180);
        registerRecord(ldaMihl0180.getVw_cwf_Who_Support_Tbl());
        ldaMihl0181 = new LdaMihl0181();
        registerRecord(ldaMihl0181);
        ldaMihl0110 = new LdaMihl0110();
        registerRecord(ldaMihl0110);
        registerRecord(ldaMihl0110.getVw_cwf_Who_Support_Tbl_Last_Run_Id());

        // parameters
        parameters = new DbsRecord();
        pdaMiha0200 = new PdaMiha0200(parameters);
        pdaCdaobj = new PdaCdaobj(parameters);
        pdaCwfpda_D = new PdaCwfpda_D(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pdaCwfpda_P = new PdaCwfpda_P(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Who_Support_Tbl_Histogram = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Support_Tbl_Histogram", "CWF-WHO-SUPPORT-TBL-HISTOGRAM"), 
            "CWF_WHO_SUPPORT_TBL", "CWF_WHO_SUPPORT");
        cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key = vw_cwf_Who_Support_Tbl_Histogram.getRecord().newFieldInGroup("cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key", 
            "TBL-PRIME-KEY", FieldType.STRING, 53, RepeatingFieldStrategy.None, "TBL_PRIME_KEY");
        cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key.setDdmHeader("RECORD KEY");
        cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Who_Support_Tbl_Histogram);

        pnd_Cut_Off_Date_D = localVariables.newFieldInRecord("pnd_Cut_Off_Date_D", "#CUT-OFF-DATE-D", FieldType.DATE);
        pnd_Cut_Off_Date = localVariables.newFieldInRecord("pnd_Cut_Off_Date", "#CUT-OFF-DATE", FieldType.NUMERIC, 8);

        pnd_Cut_Off_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Cut_Off_Date__R_Field_1", "REDEFINE", pnd_Cut_Off_Date);
        pnd_Cut_Off_Date_Pnd_Alpha = pnd_Cut_Off_Date__R_Field_1.newFieldInGroup("pnd_Cut_Off_Date_Pnd_Alpha", "#ALPHA", FieldType.STRING, 8);
        pnd_Last_Run_Isn = localVariables.newFieldInRecord("pnd_Last_Run_Isn", "#LAST-RUN-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_New_Run_Id = localVariables.newFieldInRecord("pnd_New_Run_Id", "#NEW-RUN-ID", FieldType.NUMERIC, 9);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_2", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);

        pnd_Tbl_Prime_Key__R_Field_3 = pnd_Tbl_Prime_Key__R_Field_2.newGroupInGroup("pnd_Tbl_Prime_Key__R_Field_3", "REDEFINE", pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_Pnd_Run_Status = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 8);
        pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field", "#REST-OF-TBL-KEY-FIELD", 
            FieldType.STRING, 21);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Who_Support_Tbl_Histogram.reset();

        ldaMihl0180.initializeValues();
        ldaMihl0181.initializeValues();
        ldaMihl0110.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Mihn0200() throws Exception
    {
        super("Mihn0200");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("MIHN0200", onError);
        //* (TO ALLOW EASY ESCAPE FROM THE ROUTINE)
        ROUTINE:                                                                                                                                                          //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM RESET-THE-OUTPUT-PARAMETERS
            sub_Reset_The_Output_Parameters();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CHECK-PURGE-RUN-CONTROL-RECORD
            sub_Check_Purge_Run_Control_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  PERFORM SET-UP-THE-RUN-CONTROL-RECORD
            if (true) break ROUTINE;                                                                                                                                      //Natural: ESCAPE BOTTOM ( ROUTINE. )
            //*  SUBROUTINE JUST FOR ENDING THIS SUBPROGRAM IMMEDIATELY
            //*  ------------------------------------------------------
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-SUBPROGRAM
            //* (ROUTINE.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  SUBROUTINES
        //*  ===========
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-THE-OUTPUT-PARAMETERS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PURGE-RUN-CONTROL-RECORD
        //*  =============================================
        //*  CHECK TO SEE IF ACTIVE RECORD WITH EMPTY DATE EXISTS
        //*  ------------------------------------------------------
        //*  ON NATURAL RUN TIME ERROR, RETURN WITH AN APPROPRIATE ERROR MESSAGE
        //*  ===================================================================
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Subprogram() throws Exception                                                                                                                 //Natural: ESCAPE-SUBPROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( ROUTINE. )
        Global.setEscapeCode(EscapeType.Bottom, "ROUTINE");
        if (true) return;
        //* (0560)
    }
    private void sub_Reset_The_Output_Parameters() throws Exception                                                                                                       //Natural: RESET-THE-OUTPUT-PARAMETERS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===========================================
        pdaMiha0200.getMiha0200_Output().reset();                                                                                                                         //Natural: RESET MIHA0200-OUTPUT
        //* (0650)
    }
    private void sub_Check_Purge_Run_Control_Record() throws Exception                                                                                                    //Natural: CHECK-PURGE-RUN-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue(ldaMihl0181.getMihl0181_Tbl_Scrty_Level_Ind());                                                                //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND := MIHL0181.TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue(ldaMihl0181.getMihl0181_Mit_Purge_Job_Type());                                                                       //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME := MIHL0181.MIT-PURGE-JOB-TYPE
        pnd_Tbl_Prime_Key_Pnd_Run_Status.setValue(ldaMihl0181.getMihl0181_Active_Status());                                                                               //Natural: ASSIGN #TBL-PRIME-KEY.#RUN-STATUS := MIHL0181.ACTIVE-STATUS
        pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement.setValue(0);                                                                                                    //Natural: ASSIGN #TBL-PRIME-KEY.#STARTING-DATE-9S-COMPLEMENT := 0
        pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field.setValue(" ");                                                                                                        //Natural: ASSIGN #TBL-PRIME-KEY.#REST-OF-TBL-KEY-FIELD := ' '
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind.setValue(" ");                                                                                                                //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-ACTVE-IND := ' '
        ldaMihl0180.getVw_cwf_Who_Support_Tbl().startDatabaseRead                                                                                                         //Natural: READ ( 1 ) CWF-WHO-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "LAST_COMPLETED_READ",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        LAST_COMPLETED_READ:
        while (condition(ldaMihl0180.getVw_cwf_Who_Support_Tbl().readNextRow("LAST_COMPLETED_READ")))
        {
            //*  CHECK RUN-DATE FIELD
            //*  --------------------
            if (condition(ldaMihl0180.getCwf_Who_Support_Tbl_Run_Date().greater(getZero())))                                                                              //Natural: IF CWF-WHO-SUPPORT-TBL.RUN-DATE GT 0
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Purge Control record has Date of the Run"));       //Natural: COMPRESS 'Program' *PROGRAM '- Purge Control record has Date of the Run' INTO MSG-INFO-SUB.##MSG
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LAST_COMPLETED_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LAST_COMPLETED_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* (0880)
            }                                                                                                                                                             //Natural: END-IF
            pdaMiha0200.getMiha0200_Output_Run_Control_Isn().setValue(ldaMihl0180.getVw_cwf_Who_Support_Tbl().getAstISN("LAST_COMPLETED_READ"));                          //Natural: ASSIGN MIHA0200-OUTPUT.RUN-CONTROL-ISN := *ISN ( LAST-COMPLETED-READ. )
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("LAST_COMPLETED_READ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("LAST_COMPLETED_READ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* (LAST-COMPLETED-READ.)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* (0710)
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(999);                                                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR = 999
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("NATURAL ERROR", Global.getERROR_NR(), "IN", Global.getPROGRAM(), "....LINE",                 //Natural: COMPRESS 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE INTO MSG-INFO-SUB.##MSG
            Global.getERROR_LINE()));
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
        //* (1060)
    };                                                                                                                                                                    //Natural: END-ERROR
}
