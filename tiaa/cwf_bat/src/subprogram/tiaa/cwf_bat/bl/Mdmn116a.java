/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:28:36 AM
**        * FROM NATURAL SUBPROGRAM : Mdmn116a
************************************************************
**        * FILE NAME            : Mdmn116a.java
**        * CLASS NAME           : Mdmn116a
**        * INSTANCE NAME        : Mdmn116a
************************************************************
************************************************************************
* PROGRAM NAME : MDMN116A
*                PIN EXPANSION VERSION OF MDMN115A. RETURNS UP TO
*                150 OWNER PIN's/payees for an input contract.
* WRITTEN BY   : DON MEADE
* DATE WRITTEN : APRIL 2017
************************************************************************
* HISTORY OF REVISIONS
* DATE         USERID     DESCRIPTION
*
* MM/DD/YYYY
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mdmn116a extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    public DbsRecord parameters;
    private PdaMdma116 pdaMdma116;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Max_In;
    private DbsField pnd_Max_Out;
    private DbsField pnd_Data_In;

    private DbsGroup pnd_Data_In__R_Field_1;
    private DbsField pnd_Data_In_Pnd_I_Tiaa_Cref_Ind;
    private DbsField pnd_Data_In_Pnd_I_Contract_Number;
    private DbsField pnd_Data_In_Pnd_I_Payee_Code_A2;
    private DbsField pnd_Data_In_Pnd_I_Party_Inq_Lvl;
    private DbsField pnd_Data_In_Pnd_I_Address_Fltr;
    private DbsField pnd_Data_In_Pnd_I_Cntrct_Inq_Lvl;
    private DbsField pnd_Data_In_Pnd_I_Cntrct_Role_Fltr;
    private DbsField pnd_Return;

    private DbsGroup pnd_Return__R_Field_2;
    private DbsField pnd_Return_Pnd_Rtrn_Code;
    private DbsField pnd_Return_Pnd_Rtrn_Text;
    private DbsField pnd_Data_Out;

    private DbsGroup pnd_Data_Out__R_Field_3;

    private DbsGroup pnd_Data_Out_Pnd_Temp_Data;
    private DbsField pnd_Data_Out_Pnd_O_Output_Count;

    private DbsGroup pnd_Data_Out_Pnd_O_Output_Array;
    private DbsField pnd_Data_Out_Pnd_O_Pin;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Number;
    private DbsField pnd_Data_Out_Pnd_O_Payee_Code;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Status_Code;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Status_Year;
    private DbsField pnd_Data_Out_Pnd_O_Pin_A12;

    private DbsGroup pnd_Data_Out__R_Field_4;
    private DbsField pnd_Data_Out_Pnd_O_Pin_N7;
    private DbsField pnd_Data_Out_Pnd_O_Pin_Blanks;

    private DbsGroup pnd_Data_Out__R_Field_5;
    private DbsField pnd_Data_Out_Pnd_O_Pin_N12;
    private DbsField pnd_Save_Code;
    private DbsField pnd_Save_Text;
    private DbsField pnd_X;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        // parameters
        parameters = new DbsRecord();
        pdaMdma116 = new PdaMdma116(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Max_In = localVariables.newFieldInRecord("pnd_Max_In", "#MAX-IN", FieldType.NUMERIC, 5);
        pnd_Max_Out = localVariables.newFieldInRecord("pnd_Max_Out", "#MAX-OUT", FieldType.NUMERIC, 5);
        pnd_Data_In = localVariables.newFieldArrayInRecord("pnd_Data_In", "#DATA-IN", FieldType.STRING, 1, new DbsArrayController(1, 32560));

        pnd_Data_In__R_Field_1 = localVariables.newGroupInRecord("pnd_Data_In__R_Field_1", "REDEFINE", pnd_Data_In);
        pnd_Data_In_Pnd_I_Tiaa_Cref_Ind = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Tiaa_Cref_Ind", "#I-TIAA-CREF-IND", FieldType.STRING, 
            1);
        pnd_Data_In_Pnd_I_Contract_Number = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Contract_Number", "#I-CONTRACT-NUMBER", FieldType.STRING, 
            10);
        pnd_Data_In_Pnd_I_Payee_Code_A2 = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Payee_Code_A2", "#I-PAYEE-CODE-A2", FieldType.STRING, 
            2);
        pnd_Data_In_Pnd_I_Party_Inq_Lvl = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Party_Inq_Lvl", "#I-PARTY-INQ-LVL", FieldType.NUMERIC, 
            3);
        pnd_Data_In_Pnd_I_Address_Fltr = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Address_Fltr", "#I-ADDRESS-FLTR", FieldType.STRING, 
            9);
        pnd_Data_In_Pnd_I_Cntrct_Inq_Lvl = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Cntrct_Inq_Lvl", "#I-CNTRCT-INQ-LVL", FieldType.NUMERIC, 
            3);
        pnd_Data_In_Pnd_I_Cntrct_Role_Fltr = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Cntrct_Role_Fltr", "#I-CNTRCT-ROLE-FLTR", FieldType.STRING, 
            8);
        pnd_Return = localVariables.newFieldArrayInRecord("pnd_Return", "#RETURN", FieldType.STRING, 1, new DbsArrayController(1, 200));

        pnd_Return__R_Field_2 = localVariables.newGroupInRecord("pnd_Return__R_Field_2", "REDEFINE", pnd_Return);
        pnd_Return_Pnd_Rtrn_Code = pnd_Return__R_Field_2.newFieldInGroup("pnd_Return_Pnd_Rtrn_Code", "#RTRN-CODE", FieldType.STRING, 4);
        pnd_Return_Pnd_Rtrn_Text = pnd_Return__R_Field_2.newFieldInGroup("pnd_Return_Pnd_Rtrn_Text", "#RTRN-TEXT", FieldType.STRING, 80);
        pnd_Data_Out = localVariables.newFieldArrayInRecord("pnd_Data_Out", "#DATA-OUT", FieldType.STRING, 1, new DbsArrayController(1, 32360));

        pnd_Data_Out__R_Field_3 = localVariables.newGroupInRecord("pnd_Data_Out__R_Field_3", "REDEFINE", pnd_Data_Out);

        pnd_Data_Out_Pnd_Temp_Data = pnd_Data_Out__R_Field_3.newGroupInGroup("pnd_Data_Out_Pnd_Temp_Data", "#TEMP-DATA");
        pnd_Data_Out_Pnd_O_Output_Count = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Output_Count", "#O-OUTPUT-COUNT", FieldType.NUMERIC, 
            5);

        pnd_Data_Out_Pnd_O_Output_Array = pnd_Data_Out_Pnd_Temp_Data.newGroupArrayInGroup("pnd_Data_Out_Pnd_O_Output_Array", "#O-OUTPUT-ARRAY", new DbsArrayController(1, 
            150));
        pnd_Data_Out_Pnd_O_Pin = pnd_Data_Out_Pnd_O_Output_Array.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin", "#O-PIN", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Contract_Number = pnd_Data_Out_Pnd_O_Output_Array.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Number", "#O-CONTRACT-NUMBER", 
            FieldType.STRING, 10);
        pnd_Data_Out_Pnd_O_Payee_Code = pnd_Data_Out_Pnd_O_Output_Array.newFieldInGroup("pnd_Data_Out_Pnd_O_Payee_Code", "#O-PAYEE-CODE", FieldType.NUMERIC, 
            2);
        pnd_Data_Out_Pnd_O_Contract_Status_Code = pnd_Data_Out_Pnd_O_Output_Array.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Status_Code", "#O-CONTRACT-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Contract_Status_Year = pnd_Data_Out_Pnd_O_Output_Array.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Status_Year", "#O-CONTRACT-STATUS-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Data_Out_Pnd_O_Pin_A12 = pnd_Data_Out_Pnd_O_Output_Array.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_A12", "#O-PIN-A12", FieldType.STRING, 12);

        pnd_Data_Out__R_Field_4 = pnd_Data_Out_Pnd_O_Output_Array.newGroupInGroup("pnd_Data_Out__R_Field_4", "REDEFINE", pnd_Data_Out_Pnd_O_Pin_A12);
        pnd_Data_Out_Pnd_O_Pin_N7 = pnd_Data_Out__R_Field_4.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_N7", "#O-PIN-N7", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Pin_Blanks = pnd_Data_Out__R_Field_4.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_Blanks", "#O-PIN-BLANKS", FieldType.STRING, 5);

        pnd_Data_Out__R_Field_5 = pnd_Data_Out_Pnd_O_Output_Array.newGroupInGroup("pnd_Data_Out__R_Field_5", "REDEFINE", pnd_Data_Out_Pnd_O_Pin_A12);
        pnd_Data_Out_Pnd_O_Pin_N12 = pnd_Data_Out__R_Field_5.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_N12", "#O-PIN-N12", FieldType.NUMERIC, 12);
        pnd_Save_Code = localVariables.newFieldInRecord("pnd_Save_Code", "#SAVE-CODE", FieldType.STRING, 4);
        pnd_Save_Text = localVariables.newFieldInRecord("pnd_Save_Text", "#SAVE-TEXT", FieldType.STRING, 74);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 4);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Max_In.setInitialValue(36);
        pnd_Max_Out.setInitialValue(5405);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Mdmn116a() throws Exception
    {
        super("Mdmn116a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *****************************************************************
        //*  ASSIGN DEFAULTS
        short decideConditionsMet147 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #MDMA116.#I-TIAA-CREF-IND = ' '
        if (condition(pdaMdma116.getPnd_Mdma116_Pnd_I_Tiaa_Cref_Ind().equals(" ")))
        {
            decideConditionsMet147++;
            pdaMdma116.getPnd_Mdma116_Pnd_I_Tiaa_Cref_Ind().setValue("T");                                                                                                //Natural: ASSIGN #MDMA116.#I-TIAA-CREF-IND := 'T'
        }                                                                                                                                                                 //Natural: WHEN #MDMA116.#I-PARTY-INQ-LVL = 0
        if (condition(pdaMdma116.getPnd_Mdma116_Pnd_I_Party_Inq_Lvl().equals(getZero())))
        {
            decideConditionsMet147++;
            pdaMdma116.getPnd_Mdma116_Pnd_I_Party_Inq_Lvl().setValue(0);                                                                                                  //Natural: ASSIGN #MDMA116.#I-PARTY-INQ-LVL := 0
        }                                                                                                                                                                 //Natural: WHEN #MDMA116.#I-ADDRESS-FLTR = ' '
        if (condition(pdaMdma116.getPnd_Mdma116_Pnd_I_Address_Fltr().equals(" ")))
        {
            decideConditionsMet147++;
            pdaMdma116.getPnd_Mdma116_Pnd_I_Address_Fltr().setValue("ACTIVE");                                                                                            //Natural: ASSIGN #MDMA116.#I-ADDRESS-FLTR := 'ACTIVE'
        }                                                                                                                                                                 //Natural: WHEN #MDMA116.#I-CNTRCT-INQ-LVL = 0
        if (condition(pdaMdma116.getPnd_Mdma116_Pnd_I_Cntrct_Inq_Lvl().equals(getZero())))
        {
            decideConditionsMet147++;
            pdaMdma116.getPnd_Mdma116_Pnd_I_Cntrct_Inq_Lvl().setValue(3);                                                                                                 //Natural: ASSIGN #MDMA116.#I-CNTRCT-INQ-LVL := 3
        }                                                                                                                                                                 //Natural: WHEN #MDMA116.#I-CNTRCT-ROLE-FLTR = ' '
        if (condition(pdaMdma116.getPnd_Mdma116_Pnd_I_Cntrct_Role_Fltr().equals(" ")))
        {
            decideConditionsMet147++;
            pdaMdma116.getPnd_Mdma116_Pnd_I_Cntrct_Role_Fltr().setValue("ALL");                                                                                           //Natural: ASSIGN #MDMA116.#I-CNTRCT-ROLE-FLTR := 'ALL'
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet147 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM FETCH-MDMP0010
        sub_Fetch_Mdmp0010();
        if (condition(Global.isEscape())) {return;}
        //*  NO HIT ON TIAA CONTRACT
        //*  TRY CREF
        if (condition(pnd_Return_Pnd_Rtrn_Code.notEquals("0000")))                                                                                                        //Natural: IF #RTRN-CODE NE '0000'
        {
            pnd_Save_Code.setValue(pnd_Return_Pnd_Rtrn_Code);                                                                                                             //Natural: ASSIGN #SAVE-CODE := #RTRN-CODE
            pnd_Save_Text.setValue(pnd_Return_Pnd_Rtrn_Text);                                                                                                             //Natural: ASSIGN #SAVE-TEXT := #RTRN-TEXT
            pdaMdma116.getPnd_Mdma116_Pnd_I_Tiaa_Cref_Ind().setValue("C");                                                                                                //Natural: ASSIGN #MDMA116.#I-TIAA-CREF-IND := 'C'
                                                                                                                                                                          //Natural: PERFORM FETCH-MDMP0010
            sub_Fetch_Mdmp0010();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Pnd_Rtrn_Code.notEquals("0000")))                                                                                                    //Natural: IF #RTRN-CODE NE '0000'
            {
                //*  USE CODE, TEXT FROM
                //*  INITIAL TIAA ERROR
                getReports().write(0, "Failed to find TIAA Contract or CREF Certificate");                                                                                //Natural: WRITE 'Failed to find TIAA Contract or CREF Certificate'
                if (Global.isEscape()) return;
                pnd_Return_Pnd_Rtrn_Code.setValue(pnd_Save_Code);                                                                                                         //Natural: ASSIGN #RTRN-CODE := #SAVE-CODE
                pnd_Return_Pnd_Rtrn_Text.setValue(pnd_Save_Text);                                                                                                         //Natural: ASSIGN #RTRN-TEXT := #SAVE-TEXT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaMdma116.getPnd_Mdma116_Pnd_O_Return_Code().setValue(pnd_Return_Pnd_Rtrn_Code);                                                                                 //Natural: ASSIGN #O-RETURN-CODE := #RTRN-CODE
        pdaMdma116.getPnd_Mdma116_Pnd_O_Return_Text().setValue(pnd_Return_Pnd_Rtrn_Text);                                                                                 //Natural: ASSIGN #O-RETURN-TEXT := #RTRN-TEXT
        FOR01:                                                                                                                                                            //Natural: FOR #X 1 150
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(150)); pnd_X.nadd(1))
        {
            pnd_Data_Out_Pnd_O_Pin.getValue(pnd_X).setValue(0);                                                                                                           //Natural: ASSIGN #DATA-OUT.#O-PIN ( #X ) := 0
            if (condition(pnd_Data_Out_Pnd_O_Pin_Blanks.getValue(pnd_X).equals(" ") && pnd_Data_Out_Pnd_O_Pin_A12.getValue(pnd_X).notEquals(" ")))                        //Natural: IF #DATA-OUT.#O-PIN-BLANKS ( #X ) = ' ' AND #O-PIN-A12 ( #X ) NE ' '
            {
                pnd_Data_Out_Pnd_O_Pin_N12.getValue(pnd_X).setValue(pnd_Data_Out_Pnd_O_Pin_N7.getValue(pnd_X));                                                           //Natural: ASSIGN #DATA-OUT.#O-PIN-N12 ( #X ) := #DATA-OUT.#O-PIN-N7 ( #X )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaMdma116.getPnd_Mdma116_Pnd_O_Pda_Data().setValuesByName(pnd_Data_Out_Pnd_Temp_Data);                                                                           //Natural: MOVE BY NAME #TEMP-DATA TO #O-PDA-DATA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FETCH-MDMP0010
        //*  MOVE INTERNAL #DATA-IN TO GLOBAL ##DATA-IN
    }
    private void sub_Fetch_Mdmp0010() throws Exception                                                                                                                    //Natural: FETCH-MDMP0010
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        gdaMdmg0001.getPnd_Pnd_Mdmg0001().reset();                                                                                                                        //Natural: RESET ##MDMG0001
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Date_Sent().setValueEdited(Global.getDATX(),new ReportEditMask("YYYY-MM-DD"));                                        //Natural: MOVE EDITED *DATX ( EM = YYYY-MM-DD ) TO ##MSG-DATE-SENT
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Time_Sent().setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                          //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO ##MSG-TIME-SENT
        //*  MOVE TO GDG
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Guid().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "mfsync::", Global.getINIT_PROGRAM(),                 //Natural: COMPRESS 'mfsync::' *INIT-PROGRAM #MDMA116.#I-CONTRACT-NUMBER ##MSG-DATE-SENT ##MSG-TIME-SENT INTO ##MSG-GUID LEAVING NO
            pdaMdma116.getPnd_Mdma116_Pnd_I_Contract_Number(), gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Date_Sent(), gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Time_Sent()));
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Ping_Ind().setValue("N");                                                                                             //Natural: ASSIGN ##MSG-PING-IND := 'N'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Sender_Appl_Id().setValue("LEGACY");                                                                                      //Natural: ASSIGN ##SENDER-APPL-ID := 'LEGACY'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Appl_Id().setValue("MDM");                                                                                            //Natural: ASSIGN ##TGT-APPL-ID := 'MDM'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Module_Code().setValue("N");                                                                                          //Natural: ASSIGN ##TGT-MODULE-CODE := 'N'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Module_Name().setValue("MDMN116A");                                                                                   //Natural: ASSIGN ##TGT-MODULE-NAME := 'MDMN116A'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Ctr().setValue(1);                                                                                                    //Natural: ASSIGN ##PDA-CTR := 1
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length().setValue(pnd_Max_In);                                                                                        //Natural: ASSIGN ##PDA-LENGTH := #MAX-IN
        pnd_Data_In_Pnd_I_Tiaa_Cref_Ind.setValue(pdaMdma116.getPnd_Mdma116_Pnd_I_Tiaa_Cref_Ind());                                                                        //Natural: ASSIGN #DATA-IN.#I-TIAA-CREF-IND := #MDMA116.#I-TIAA-CREF-IND
        pnd_Data_In_Pnd_I_Contract_Number.setValue(pdaMdma116.getPnd_Mdma116_Pnd_I_Contract_Number());                                                                    //Natural: ASSIGN #DATA-IN.#I-CONTRACT-NUMBER := #MDMA116.#I-CONTRACT-NUMBER
        pnd_Data_In_Pnd_I_Payee_Code_A2.setValue(pdaMdma116.getPnd_Mdma116_Pnd_I_Payee_Code_A2());                                                                        //Natural: ASSIGN #DATA-IN.#I-PAYEE-CODE-A2 := #MDMA116.#I-PAYEE-CODE-A2
        pnd_Data_In_Pnd_I_Party_Inq_Lvl.setValue(pdaMdma116.getPnd_Mdma116_Pnd_I_Party_Inq_Lvl());                                                                        //Natural: ASSIGN #DATA-IN.#I-PARTY-INQ-LVL := #MDMA116.#I-PARTY-INQ-LVL
        pnd_Data_In_Pnd_I_Address_Fltr.setValue(pdaMdma116.getPnd_Mdma116_Pnd_I_Address_Fltr());                                                                          //Natural: ASSIGN #DATA-IN.#I-ADDRESS-FLTR := #MDMA116.#I-ADDRESS-FLTR
        pnd_Data_In_Pnd_I_Cntrct_Inq_Lvl.setValue(pdaMdma116.getPnd_Mdma116_Pnd_I_Cntrct_Inq_Lvl());                                                                      //Natural: ASSIGN #DATA-IN.#I-CNTRCT-INQ-LVL := #MDMA116.#I-CNTRCT-INQ-LVL
        pnd_Data_In_Pnd_I_Cntrct_Role_Fltr.setValue(pdaMdma116.getPnd_Mdma116_Pnd_I_Cntrct_Role_Fltr());                                                                  //Natural: ASSIGN #DATA-IN.#I-CNTRCT-ROLE-FLTR := #MDMA116.#I-CNTRCT-ROLE-FLTR
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_In().getValue("*").setValue(pnd_Data_In.getValue("*"));                                                              //Natural: ASSIGN ##DATA-IN ( * ) := #DATA-IN ( * )
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "=",gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Guid());                                                                                //Natural: WRITE '=' ##MSG-GUID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0010"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0010'
        if (condition(Global.isEscape())) return;
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Data Response",gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1,":",100));                                           //Natural: WRITE 'Data Response' ##DATA-RESPONSE ( 1:100 )
            if (Global.isEscape()) return;
            if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length().notEquals(pnd_Max_Out)))                                                                   //Natural: IF ##PDA-LENGTH NE #MAX-OUT
            {
                getReports().write(0, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length(),"Different PDA Length",pnd_Max_Out);                                           //Natural: WRITE ##PDA-LENGTH 'Different PDA Length' #MAX-OUT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue("*"));                                                         //Natural: ASSIGN #RETURN ( * ) := ##DATA-RESPONSE ( * )
        pnd_Data_Out.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Out().getValue("*"));                                                            //Natural: ASSIGN #DATA-OUT ( * ) := ##DATA-OUT ( * )
        //*  FETCH-MDMP0010
    }

    //
}
