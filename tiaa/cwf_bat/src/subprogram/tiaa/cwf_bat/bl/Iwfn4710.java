/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:18:03 AM
**        * FROM NATURAL SUBPROGRAM : Iwfn4710
************************************************************
**        * FILE NAME            : Iwfn4710.java
**        * CLASS NAME           : Iwfn4710
**        * INSTANCE NAME        : Iwfn4710
************************************************************
************************************************************************
* PROGRAM      : IWFN4710
* SYSTEM       : ENHANCED UNIT WORK LISTS
* DATE WRITTEN : 03/24/1999
* FUNCTION     : CWF-MASTER-INDEX INITIAL LOAD TO CWF-SCRATCH
*              : FOR A SPECIFIC UNIT-CDE
*              :
* HISTORY      : JRF 06/10/99 CHANGE CPRPTE-DUE-DTE-TME WITH
*              :              CRRNT-DUE-DTE
*              : JRF 08/26/99 ENHANCED ERROR HANDLING.
*              : JRF 09/01/99 INCLUDE NEW WORKLISTS: (WPID = 'P')
*              :              A) SUPERVISOR COMMUNICATION WORKLISTS
*              :              B) EMPLOYEE COMMUNICATION WORKLISTS
*              :              REF:JF090199
*              : JRF 09/14/99 POPULATE TEXT(1) WITH WORK LIST IND
*              :              REF:JF091499
*              : JB  09/27/99 ADJUST ERROR LOGIC. SC JB1.
*              : JRF 09/30/99 ADD DTE-TME-STAMP IN KEY8
*              : EPM 06/21/00 RESTOWED DUE TO CHANGE IN CWFA1400 FOR IES
*              :
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017 <STOW ONLY>
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iwfn4710 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCwfpda_M pdaCwfpda_M;
    private LdaIwfl4110 ldaIwfl4110;
    private PdaIwfa4700 pdaIwfa4700;
    private PdaIwfa4701 pdaIwfa4701;
    private PdaIwfa4702 pdaIwfa4702;
    private PdaIwfa4703 pdaIwfa4703;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfa1400 pdaCwfa1400;
    private PdaCwfa1401 pdaCwfa1401;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Tbl_Key_Field;
    private DbsField pnd_Parm_Unit_Cde;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Add_Ctr;
    private DbsField pnd_Start_Key;

    private DbsGroup pnd_Start_Key__R_Field_1;
    private DbsField pnd_Start_Key_Pnd_Unit_Cde;
    private DbsField pnd_Start_Key_Pnd_Work_List_Ind;
    private DbsField pnd_Start_Key_Pnd_Due_Dte;
    private DbsField pnd_Start_Key_Pnd_Wpid;
    private DbsField pnd_Ppg_Cde;
    private DbsField pnd_Wklst_Ind_Max;
    private DbsField pnd_Et_Max;
    private DbsField pnd_Et_Ctr;
    private DbsField pnd_Instn_Cde;
    private DbsField pnd_Wpid_Sort;
    private DbsField pnd_Last_Dte_Tme_Update_Hold;
    private DbsField pnd_Last_Msg_Data;
    private DbsField pnd_Individual_Err_Fl;
    private DbsField pnd_Call_4700_On_Err;
    private DbsField pnd_Err_Ctr;
    private DbsField pnd_Last_Err_Msg;
    private DbsField pnd_Last_Err_Prg;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIwfl4110 = new LdaIwfl4110();
        registerRecord(ldaIwfl4110);
        registerRecord(ldaIwfl4110.getVw_cwf_Master_Index_View());
        localVariables = new DbsRecord();
        pdaIwfa4700 = new PdaIwfa4700(localVariables);
        pdaIwfa4701 = new PdaIwfa4701(localVariables);
        pdaIwfa4702 = new PdaIwfa4702(localVariables);
        pdaIwfa4703 = new PdaIwfa4703(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfa1400 = new PdaCwfa1400(localVariables);
        pdaCwfa1401 = new PdaCwfa1401(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Tbl_Key_Field = parameters.newFieldInRecord("pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Tbl_Key_Field.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Unit_Cde = parameters.newFieldInRecord("pnd_Parm_Unit_Cde", "#PARM-UNIT-CDE", FieldType.STRING, 8);
        pnd_Parm_Unit_Cde.setParameterOption(ParameterOption.ByReference);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.NUMERIC, 7);
        pnd_Add_Ctr = localVariables.newFieldInRecord("pnd_Add_Ctr", "#ADD-CTR", FieldType.NUMERIC, 7);
        pnd_Start_Key = localVariables.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.STRING, 34);

        pnd_Start_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Start_Key__R_Field_1", "REDEFINE", pnd_Start_Key);
        pnd_Start_Key_Pnd_Unit_Cde = pnd_Start_Key__R_Field_1.newFieldInGroup("pnd_Start_Key_Pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 8);
        pnd_Start_Key_Pnd_Work_List_Ind = pnd_Start_Key__R_Field_1.newFieldInGroup("pnd_Start_Key_Pnd_Work_List_Ind", "#WORK-LIST-IND", FieldType.STRING, 
            3);
        pnd_Start_Key_Pnd_Due_Dte = pnd_Start_Key__R_Field_1.newFieldInGroup("pnd_Start_Key_Pnd_Due_Dte", "#DUE-DTE", FieldType.STRING, 17);
        pnd_Start_Key_Pnd_Wpid = pnd_Start_Key__R_Field_1.newFieldInGroup("pnd_Start_Key_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Ppg_Cde = localVariables.newFieldInRecord("pnd_Ppg_Cde", "#PPG-CDE", FieldType.STRING, 6);
        pnd_Wklst_Ind_Max = localVariables.newFieldInRecord("pnd_Wklst_Ind_Max", "#WKLST-IND-MAX", FieldType.STRING, 1);
        pnd_Et_Max = localVariables.newFieldInRecord("pnd_Et_Max", "#ET-MAX", FieldType.NUMERIC, 7);
        pnd_Et_Ctr = localVariables.newFieldInRecord("pnd_Et_Ctr", "#ET-CTR", FieldType.NUMERIC, 7);
        pnd_Instn_Cde = localVariables.newFieldInRecord("pnd_Instn_Cde", "#INSTN-CDE", FieldType.NUMERIC, 6);
        pnd_Wpid_Sort = localVariables.newFieldInRecord("pnd_Wpid_Sort", "#WPID-SORT", FieldType.STRING, 3);
        pnd_Last_Dte_Tme_Update_Hold = localVariables.newFieldInRecord("pnd_Last_Dte_Tme_Update_Hold", "#LAST-DTE-TME-UPDATE-HOLD", FieldType.STRING, 
            15);
        pnd_Last_Msg_Data = localVariables.newFieldInRecord("pnd_Last_Msg_Data", "#LAST-MSG-DATA", FieldType.STRING, 8);
        pnd_Individual_Err_Fl = localVariables.newFieldInRecord("pnd_Individual_Err_Fl", "#INDIVIDUAL-ERR-FL", FieldType.BOOLEAN, 1);
        pnd_Call_4700_On_Err = localVariables.newFieldInRecord("pnd_Call_4700_On_Err", "#CALL-4700-ON-ERR", FieldType.BOOLEAN, 1);
        pnd_Err_Ctr = localVariables.newFieldInRecord("pnd_Err_Ctr", "#ERR-CTR", FieldType.NUMERIC, 6);
        pnd_Last_Err_Msg = localVariables.newFieldInRecord("pnd_Last_Err_Msg", "#LAST-ERR-MSG", FieldType.STRING, 79);
        pnd_Last_Err_Prg = localVariables.newFieldInRecord("pnd_Last_Err_Prg", "#LAST-ERR-PRG", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIwfl4110.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Et_Max.setInitialValue(200);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iwfn4710() throws Exception
    {
        super("Iwfn4710");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IWFN4710", onError);
        //*  ======================================================================
        //*                            MAIN LOGIC
        //*  ======================================================================
                                                                                                                                                                          //Natural: PERFORM INITIALIZATIONS
        sub_Initializations();
        if (condition(Global.isEscape())) {return;}
        PROG:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(Global.getPROGRAM());                                                                     //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = *PROGRAM
            pnd_Last_Msg_Data.setValue(Global.getPROGRAM());
            ldaIwfl4110.getVw_cwf_Master_Index_View().startDatabaseRead                                                                                                   //Natural: READ CWF-MASTER-INDEX-VIEW BY UNIT-WORK-LIST-TME-KEY2 FROM #START-KEY
            (
            "READ_FILE",
            new Wc[] { new Wc("UNIT_WORK_LIST_TME_KEY2", ">=", pnd_Start_Key, WcType.BY) },
            new Oc[] { new Oc("UNIT_WORK_LIST_TME_KEY2", "ASC") }
            );
            READ_FILE:
            while (condition(ldaIwfl4110.getVw_cwf_Master_Index_View().readNextRow("READ_FILE")))
            {
                //*  RECORDS READ COUNTER
                //*  JF082699
                pnd_Read_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #READ-CTR
                pnd_Individual_Err_Fl.setValue(true);                                                                                                                     //Natural: ASSIGN #INDIVIDUAL-ERR-FL := TRUE
                if (condition(ldaIwfl4110.getCwf_Master_Index_View_Admin_Unit_Cde().notEquals(pnd_Parm_Unit_Cde) || ldaIwfl4110.getCwf_Master_Index_View_Work_List_Ind().getSubstring(1,1).compareTo(pnd_Wklst_Ind_Max.getText())  //Natural: IF CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE NE #PARM-UNIT-CDE OR SUBSTRING ( CWF-MASTER-INDEX-VIEW.WORK-LIST-IND,1,1 ) > #WKLST-IND-MAX
                    > 0))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Instn_Cde.reset();                                                                                                                                    //Natural: RESET #INSTN-CDE #WPID-SORT
                pnd_Wpid_Sort.reset();
                if (condition(ldaIwfl4110.getCwf_Master_Index_View_Instn_Cde().greater(" ")))                                                                             //Natural: IF CWF-MASTER-INDEX-VIEW.INSTN-CDE > ' '
                {
                    pnd_Ppg_Cde.setValue(ldaIwfl4110.getCwf_Master_Index_View_Instn_Cde());                                                                               //Natural: ASSIGN #PPG-CDE := CWF-MASTER-INDEX-VIEW.INSTN-CDE
                    //*  GET INSTN-CDE
                    //*  GIVEN PPG
                    DbsUtil.callnat(Iwfn4799.class , getCurrentProcessState(), pnd_Ppg_Cde, pnd_Instn_Cde);                                                               //Natural: CALLNAT 'IWFN4799' #PPG-CDE #INSTN-CDE
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                }                                                                                                                                                         //Natural: END-IF
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("CWFN1400");                                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'CWFN1400'
                pnd_Last_Msg_Data.setValue("CWFN1400");
                pdaCwfa1400.getCwfa1400().reset();                                                                                                                        //Natural: RESET CWFA1400
                pdaCwfa1400.getCwfa1400().setValuesByName(ldaIwfl4110.getVw_cwf_Master_Index_View());                                                                     //Natural: MOVE BY NAME CWF-MASTER-INDEX-VIEW TO CWFA1400
                pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                       //Natural: ASSIGN CDAOBJ.#FUNCTION = 'GET'
                DbsUtil.callnat(Cwfn1400.class , getCurrentProcessState(), pdaCwfa1400.getCwfa1400(), pdaCwfa1400.getCwfa1400_Id(), pdaCwfa1401.getCwfa1401(),            //Natural: CALLNAT 'CWFN1400' CWFA1400 CWFA1400-ID CWFA1401 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                    pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
                sub_Check_For_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FILE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FILE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  COMMUNICATION WRKLST JF090199
                //*  WPID SORT
                if (condition(pdaCwfa1400.getCwfa1400_Crprte_Prty_Ind().equals("D") || pdaCwfa1400.getCwfa1400_Crprte_Prty_Ind().equals("J") || pdaCwfa1400.getCwfa1400_Crprte_Prty_Ind().equals("N")  //Natural: IF CWFA1400.CRPRTE-PRTY-IND = 'D' OR = 'J' OR = 'N' OR = 'P'
                    || pdaCwfa1400.getCwfa1400_Crprte_Prty_Ind().equals("P")))
                {
                    pnd_Wpid_Sort.setValue(pdaCwfa1400.getCwfa1400_Crprte_Prty_Ind());                                                                                    //Natural: ASSIGN #WPID-SORT := CWFA1400.CRPRTE-PRTY-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Wpid_Sort.setValue("T");                                                                                                                          //Natural: ASSIGN #WPID-SORT := 'T'
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet1169 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTRING ( CWF-MASTER-INDEX-VIEW.WORK-LIST-IND,1,1 ) = '1'
                if (condition(ldaIwfl4110.getCwf_Master_Index_View_Work_List_Ind().getSubstring(1,1).equals("1")))
                {
                    decideConditionsMet1169++;
                    //*  UNASSIGNED
                                                                                                                                                                          //Natural: PERFORM POPULATE-UNASSIGNED-WORK-LIST
                    sub_Populate_Unassigned_Work_List();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FILE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FILE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN CWF-MASTER-INDEX-VIEW.WORK-LIST-IND = MASK ( '2'.. )
                else if (condition(DbsUtil.maskMatches(ldaIwfl4110.getCwf_Master_Index_View_Work_List_Ind(),"'2'..")))
                {
                    decideConditionsMet1169++;
                    //*  EMPL WKLIST
                                                                                                                                                                          //Natural: PERFORM POPULATE-EMPL-WORK-LIST
                    sub_Populate_Empl_Work_List();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FILE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FILE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet1169 > 0))
                {
                    pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                 //Natural: ASSIGN CDAOBJ.#FUNCTION = 'STORE'
                                                                                                                                                                          //Natural: PERFORM CALL-OBJ-MAINTENANCE
                    sub_Call_Obj_Maintenance();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FILE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FILE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  READ-FILE.
                //*  JF082699
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Individual_Err_Fl.setValue(false);                                                                                                                        //Natural: ASSIGN #INDIVIDUAL-ERR-FL := FALSE
            //*  ---------
            //*  ISSUE ET
            //*  ---------
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM UPDATE-WK-LIST-RUNS
            sub_Update_Wk_List_Runs();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break PROG;                                                                                                                                         //Natural: ESCAPE BOTTOM ( PROG. )
            //*  ----------------------------------------------------------------------
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
            //*  ----------------------------------------------------------------------
            //*  PROG.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  ======================================================================
        //*                             SUBROUTINES
        //*  ======================================================================
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATIONS
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-UNASSIGNED-WORK-LIST
        //*  ====================================
        //*  POPULATE KEY1 - INSTITUTIONAL DETAIL
        //*  ====================================
        //*  ==============
        //*  POPULATE KEY10
        //*  ==============
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-EMPL-WORK-LIST
        //*  ===============================
        //*  POPULATE KEY3 - EMPLOYEE DETAIL
        //*  ===============================
        //*  ==============
        //*  POPULATE KEY10
        //*  ==============
        //*  ===============
        //*  POPULATE OTHERS -JF091499
        //*  ===============
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-OBJ-MAINTENANCE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-WK-LIST-RUNS
        //*  ----------------------------------------------------------------------
        //*  ---------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-CWFN2100
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-KEY8
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-ERROR
        //*        MSG-INFO-SUB.##MSG         := #LAST-ERR-MSG
        //*        MSG-INFO-SUB.##MSG-DATA(3) := #LAST-ERR-PRG
        //*  -------------------------
        //*  ON NATURAL RUN TIME ERROR
        //*  -------------------------
        //*  ----------------------------------------------------------------------                                                                                       //Natural: ON ERROR
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).equals(" ")))                                                                            //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 3 ) = ' '
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(pnd_Last_Msg_Data);                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) := #LAST-MSG-DATA
        }                                                                                                                                                                 //Natural: END-IF
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
        Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
        if (true) return;
    }
    private void sub_Initializations() throws Exception                                                                                                                   //Natural: INITIALIZATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  JF082699
        //*  INITIALIZE #START-KEY
        pnd_Read_Ctr.reset();                                                                                                                                             //Natural: RESET #READ-CTR #ADD-CTR MSG-INFO-SUB #ERR-CTR #CALL-4700-ON-ERR
        pnd_Add_Ctr.reset();
        pdaCwfpda_M.getMsg_Info_Sub().reset();
        pnd_Err_Ctr.reset();
        pnd_Call_4700_On_Err.reset();
        pnd_Start_Key_Pnd_Unit_Cde.setValue(pnd_Parm_Unit_Cde);                                                                                                           //Natural: ASSIGN #START-KEY.#UNIT-CDE := #PARM-UNIT-CDE
        pnd_Start_Key_Pnd_Work_List_Ind.setValue("1");                                                                                                                    //Natural: ASSIGN #START-KEY.#WORK-LIST-IND := '1'
        pnd_Wklst_Ind_Max.setValue("2");                                                                                                                                  //Natural: ASSIGN #WKLST-IND-MAX := '2'
    }
    private void sub_Populate_Unassigned_Work_List() throws Exception                                                                                                     //Natural: POPULATE-UNASSIGNED-WORK-LIST
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pdaIwfa4700.getIwfa4700().reset();                                                                                                                                //Natural: RESET IWFA4700
        pdaIwfa4700.getIwfa4700_Pnd_Key1_Rec_Type().setValue("ID");                                                                                                       //Natural: ASSIGN IWFA4700.#KEY1-REC-TYPE := 'ID'
        pdaIwfa4700.getIwfa4700_Pnd_Key1_Admin_Unit_Cde().setValue(ldaIwfl4110.getCwf_Master_Index_View_Admin_Unit_Cde());                                                //Natural: ASSIGN IWFA4700.#KEY1-ADMIN-UNIT-CDE := CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
        pdaIwfa4700.getIwfa4700_Pnd_Key1_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                             //Natural: ASSIGN IWFA4700.#KEY1-INSTN-CDE := #INSTN-CDE
        pdaIwfa4700.getIwfa4700_Pnd_Key1_Crprte_Due_Dte().setValue(ldaIwfl4110.getCwf_Master_Index_View_Crrnt_Due_Dte());                                                 //Natural: MOVE CWF-MASTER-INDEX-VIEW.CRRNT-DUE-DTE TO IWFA4700.#KEY1-CRPRTE-DUE-DTE
        pdaIwfa4700.getIwfa4700_Pnd_Key1_Work_Prcss_Id().setValue(ldaIwfl4110.getCwf_Master_Index_View_Work_Prcss_Id());                                                  //Natural: ASSIGN IWFA4700.#KEY1-WORK-PRCSS-ID := CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID
        //*  =============================================
        //*  POPULATE KEY2 - SUPERVISOR TRANSACTION DETAIL
        //*  =============================================
        //*  REMITTANCE
        //*  APPLICATION
        //*  ADJUSTMENTS
        if (condition(pnd_Wpid_Sort.equals("D") || pnd_Wpid_Sort.equals("J") || pnd_Wpid_Sort.equals("N")))                                                               //Natural: IF #WPID-SORT = 'D' OR = 'J' OR = 'N'
        {
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Rec_Type().setValue("STD");                                                                                                  //Natural: ASSIGN IWFA4700.#KEY2-REC-TYPE := 'STD'
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Admin_Unit_Cde().setValue(ldaIwfl4110.getCwf_Master_Index_View_Admin_Unit_Cde());                                            //Natural: ASSIGN IWFA4700.#KEY2-ADMIN-UNIT-CDE := CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                         //Natural: ASSIGN IWFA4700.#KEY2-INSTN-CDE := #INSTN-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Wpid_Sort().setValue(pnd_Wpid_Sort);                                                                                         //Natural: ASSIGN IWFA4700.#KEY2-WPID-SORT := #WPID-SORT
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Crprte_Due_Dte().setValue(ldaIwfl4110.getCwf_Master_Index_View_Crrnt_Due_Dte());                                             //Natural: MOVE CWF-MASTER-INDEX-VIEW.CRRNT-DUE-DTE TO IWFA4700.#KEY2-CRPRTE-DUE-DTE
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Work_Prcss_Id().setValue(ldaIwfl4110.getCwf_Master_Index_View_Work_Prcss_Id());                                              //Natural: ASSIGN IWFA4700.#KEY2-WORK-PRCSS-ID := CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID
        }                                                                                                                                                                 //Natural: END-IF
        //*  ===============================================
        //*  POPULATE KEY4 - SUPERVISOR COMMUNICATION DETAIL - JF090199
        //*  ===============================================
        if (condition(pnd_Wpid_Sort.equals("P")))                                                                                                                         //Natural: IF #WPID-SORT = 'P'
        {
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Rec_Type().setValue("SCD");                                                                                                  //Natural: ASSIGN IWFA4700.#KEY4-REC-TYPE := 'SCD'
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Admin_Unit_Cde().setValue(ldaIwfl4110.getCwf_Master_Index_View_Admin_Unit_Cde());                                            //Natural: ASSIGN IWFA4700.#KEY4-ADMIN-UNIT-CDE := CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                         //Natural: ASSIGN IWFA4700.#KEY4-INSTN-CDE := #INSTN-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Wpid_Sort().setValue(pnd_Wpid_Sort);                                                                                         //Natural: ASSIGN IWFA4700.#KEY4-WPID-SORT := #WPID-SORT
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Crprte_Due_Dte().setValue(ldaIwfl4110.getCwf_Master_Index_View_Crrnt_Due_Dte());                                             //Natural: MOVE CWF-MASTER-INDEX-VIEW.CRRNT-DUE-DTE TO IWFA4700.#KEY4-CRPRTE-DUE-DTE
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Work_Prcss_Id().setValue(ldaIwfl4110.getCwf_Master_Index_View_Work_Prcss_Id());                                              //Natural: ASSIGN IWFA4700.#KEY4-WORK-PRCSS-ID := CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID
            //*  PARTICIPANT
        }                                                                                                                                                                 //Natural: END-IF
        pdaIwfa4700.getIwfa4700_Pnd_Key10_Rqst_Log_Dte_Tme().setValue(ldaIwfl4110.getCwf_Master_Index_View_Rqst_Log_Dte_Tme());                                           //Natural: ASSIGN IWFA4700.#KEY10-RQST-LOG-DTE-TME := CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
        pdaIwfa4700.getIwfa4700_Pnd_Key10_Cwf_Type().setValue("P");                                                                                                       //Natural: ASSIGN IWFA4700.#KEY10-CWF-TYPE := 'P'
        pdaIwfa4700.getIwfa4700_Rcrd_Add_Pgrm().setValue(Global.getPROGRAM());                                                                                            //Natural: ASSIGN IWFA4700.RCRD-ADD-PGRM := *PROGRAM
        pdaIwfa4700.getIwfa4700_Rcrd_Add_User_Id().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN IWFA4700.RCRD-ADD-USER-ID := *INIT-USER
    }
    private void sub_Populate_Empl_Work_List() throws Exception                                                                                                           //Natural: POPULATE-EMPL-WORK-LIST
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pdaIwfa4700.getIwfa4700().reset();                                                                                                                                //Natural: RESET IWFA4700
        //*  =============================================
        //*  POPULATE KEY2 - SUPERVISOR TRANSACTION DETAIL
        //*  =============================================
        //*  REMITTANCE
        //*  APPLICATION
        //*  ADJUSTMENTS
        if (condition(pnd_Wpid_Sort.equals("D") || pnd_Wpid_Sort.equals("J") || pnd_Wpid_Sort.equals("N")))                                                               //Natural: IF #WPID-SORT = 'D' OR = 'J' OR = 'N'
        {
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Rec_Type().setValue("STD");                                                                                                  //Natural: ASSIGN IWFA4700.#KEY2-REC-TYPE := 'STD'
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Admin_Unit_Cde().setValue(ldaIwfl4110.getCwf_Master_Index_View_Admin_Unit_Cde());                                            //Natural: ASSIGN IWFA4700.#KEY2-ADMIN-UNIT-CDE := CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                         //Natural: ASSIGN IWFA4700.#KEY2-INSTN-CDE := #INSTN-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Wpid_Sort().setValue(pnd_Wpid_Sort);                                                                                         //Natural: ASSIGN IWFA4700.#KEY2-WPID-SORT := #WPID-SORT
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Crprte_Due_Dte().setValue(ldaIwfl4110.getCwf_Master_Index_View_Crrnt_Due_Dte());                                             //Natural: MOVE CWF-MASTER-INDEX-VIEW.CRRNT-DUE-DTE TO IWFA4700.#KEY2-CRPRTE-DUE-DTE
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Work_Prcss_Id().setValue(ldaIwfl4110.getCwf_Master_Index_View_Work_Prcss_Id());                                              //Natural: ASSIGN IWFA4700.#KEY2-WORK-PRCSS-ID := CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID
        }                                                                                                                                                                 //Natural: END-IF
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Rec_Type().setValue("ED");                                                                                                       //Natural: ASSIGN IWFA4700.#KEY3-REC-TYPE := 'ED'
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Admin_Unit_Cde().setValue(ldaIwfl4110.getCwf_Master_Index_View_Admin_Unit_Cde());                                                //Natural: ASSIGN IWFA4700.#KEY3-ADMIN-UNIT-CDE := CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Empl_Racf_Id().setValue(ldaIwfl4110.getCwf_Master_Index_View_Empl_Racf_Id());                                                    //Natural: ASSIGN IWFA4700.#KEY3-EMPL-RACF-ID := CWF-MASTER-INDEX-VIEW.EMPL-RACF-ID
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                             //Natural: ASSIGN IWFA4700.#KEY3-INSTN-CDE := #INSTN-CDE
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Wpid_Sort().setValue(pnd_Wpid_Sort);                                                                                             //Natural: ASSIGN IWFA4700.#KEY3-WPID-SORT := #WPID-SORT
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Crprte_Due_Dte().setValue(ldaIwfl4110.getCwf_Master_Index_View_Crrnt_Due_Dte());                                                 //Natural: MOVE CWF-MASTER-INDEX-VIEW.CRRNT-DUE-DTE TO IWFA4700.#KEY3-CRPRTE-DUE-DTE
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Work_Prcss_Id().setValue(ldaIwfl4110.getCwf_Master_Index_View_Work_Prcss_Id());                                                  //Natural: ASSIGN IWFA4700.#KEY3-WORK-PRCSS-ID := CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID
        if (condition(pnd_Wpid_Sort.equals("P")))                                                                                                                         //Natural: IF #WPID-SORT = 'P'
        {
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Rec_Type().setValue("ECD");                                                                                                  //Natural: ASSIGN IWFA4700.#KEY5-REC-TYPE := 'ECD'
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Admin_Unit_Cde().setValue(ldaIwfl4110.getCwf_Master_Index_View_Admin_Unit_Cde());                                            //Natural: ASSIGN IWFA4700.#KEY5-ADMIN-UNIT-CDE := CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Empl_Racf_Id().setValue(ldaIwfl4110.getCwf_Master_Index_View_Empl_Racf_Id());                                                //Natural: ASSIGN IWFA4700.#KEY5-EMPL-RACF-ID := CWF-MASTER-INDEX-VIEW.EMPL-RACF-ID
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                         //Natural: ASSIGN IWFA4700.#KEY5-INSTN-CDE := #INSTN-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Wpid_Sort().setValue(pnd_Wpid_Sort);                                                                                         //Natural: ASSIGN IWFA4700.#KEY5-WPID-SORT := #WPID-SORT
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Crprte_Due_Dte().setValue(ldaIwfl4110.getCwf_Master_Index_View_Crrnt_Due_Dte());                                             //Natural: MOVE CWF-MASTER-INDEX-VIEW.CRRNT-DUE-DTE TO IWFA4700.#KEY5-CRPRTE-DUE-DTE
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Work_Prcss_Id().setValue(ldaIwfl4110.getCwf_Master_Index_View_Work_Prcss_Id());                                              //Natural: ASSIGN IWFA4700.#KEY5-WORK-PRCSS-ID := CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Rec_Type().setValue("SCD");                                                                                                  //Natural: ASSIGN IWFA4700.#KEY4-REC-TYPE := 'SCD'
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Admin_Unit_Cde().setValue(ldaIwfl4110.getCwf_Master_Index_View_Admin_Unit_Cde());                                            //Natural: ASSIGN IWFA4700.#KEY4-ADMIN-UNIT-CDE := CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                         //Natural: ASSIGN IWFA4700.#KEY4-INSTN-CDE := #INSTN-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Wpid_Sort().setValue(pnd_Wpid_Sort);                                                                                         //Natural: ASSIGN IWFA4700.#KEY4-WPID-SORT := #WPID-SORT
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Crprte_Due_Dte().setValue(ldaIwfl4110.getCwf_Master_Index_View_Crrnt_Due_Dte());                                             //Natural: MOVE CWF-MASTER-INDEX-VIEW.CRRNT-DUE-DTE TO IWFA4700.#KEY4-CRPRTE-DUE-DTE
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Work_Prcss_Id().setValue(ldaIwfl4110.getCwf_Master_Index_View_Work_Prcss_Id());                                              //Natural: ASSIGN IWFA4700.#KEY4-WORK-PRCSS-ID := CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID
            //*  PARTICIPANT
        }                                                                                                                                                                 //Natural: END-IF
        pdaIwfa4700.getIwfa4700_Pnd_Key10_Rqst_Log_Dte_Tme().setValue(ldaIwfl4110.getCwf_Master_Index_View_Rqst_Log_Dte_Tme());                                           //Natural: ASSIGN IWFA4700.#KEY10-RQST-LOG-DTE-TME := CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
        pdaIwfa4700.getIwfa4700_Pnd_Key10_Cwf_Type().setValue("P");                                                                                                       //Natural: ASSIGN IWFA4700.#KEY10-CWF-TYPE := 'P'
        pdaIwfa4700.getIwfa4700_Text().getValue(1).setValue(ldaIwfl4110.getCwf_Master_Index_View_Work_List_Ind());                                                        //Natural: ASSIGN IWFA4700.TEXT ( 1 ) := CWF-MASTER-INDEX-VIEW.WORK-LIST-IND
        pdaIwfa4700.getIwfa4700_Rcrd_Add_Pgrm().setValue(Global.getPROGRAM());                                                                                            //Natural: ASSIGN IWFA4700.RCRD-ADD-PGRM := *PROGRAM
        pdaIwfa4700.getIwfa4700_Rcrd_Add_User_Id().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN IWFA4700.RCRD-ADD-USER-ID := *INIT-USER
    }
    private void sub_Call_Obj_Maintenance() throws Exception                                                                                                              //Natural: CALL-OBJ-MAINTENANCE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("IWFN4700");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'IWFN4700'
        pnd_Last_Msg_Data.setValue("IWFN4700");
        DbsUtil.callnat(Iwfn4700.class , getCurrentProcessState(), pdaIwfa4700.getIwfa4700(), pdaIwfa4700.getIwfa4700_Id(), pdaIwfa4701.getIwfa4701(),                    //Natural: CALLNAT 'IWFN4700' IWFA4700 IWFA4700-ID IWFA4701 CDAOBJ MSG-INFO-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_M.getMsg_Info_Sub());
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
        sub_Check_For_Error();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE")))                                                                                                //Natural: IF CDAOBJ.#FUNCTION = 'STORE'
        {
            //*  JF082699
            if (condition(! (pnd_Call_4700_On_Err.getBoolean())))                                                                                                         //Natural: IF NOT #CALL-4700-ON-ERR
            {
                pnd_Add_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #ADD-CTR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Et_Ctr.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #ET-CTR
        }                                                                                                                                                                 //Natural: END-IF
        //*  ISSUE ET
        if (condition(pnd_Et_Ctr.greater(pnd_Et_Max)))                                                                                                                    //Natural: IF #ET-CTR > #ET-MAX
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM UPDATE-WK-LIST-RUNS
            sub_Update_Wk_List_Runs();
            if (condition(Global.isEscape())) {return;}
            pnd_Et_Ctr.reset();                                                                                                                                           //Natural: RESET #ET-CTR
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  GET RECORD
    private void sub_Update_Wk_List_Runs() throws Exception                                                                                                               //Natural: UPDATE-WK-LIST-RUNS
    {
        if (BLNatReinput.isReinput()) return;

        pdaIwfa4702.getIwfa4702_Tbl_Scrty_Level_Ind().setValue("A");                                                                                                      //Natural: ASSIGN IWFA4702.TBL-SCRTY-LEVEL-IND := 'A'
        pdaIwfa4702.getIwfa4702_Tbl_Table_Nme().setValue("ENHANCED-WK-LST-RUNS");                                                                                         //Natural: ASSIGN IWFA4702.TBL-TABLE-NME := 'ENHANCED-WK-LST-RUNS'
        pdaIwfa4702.getIwfa4702_Tbl_Key_Field().setValue(pnd_Tbl_Key_Field);                                                                                              //Natural: ASSIGN IWFA4702.TBL-KEY-FIELD := #TBL-KEY-FIELD
        pdaIwfa4702.getIwfa4702_Tbl_Actve_Ind().setValue("A ");                                                                                                           //Natural: ASSIGN IWFA4702.TBL-ACTVE-IND := 'A '
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                               //Natural: ASSIGN CDAOBJ.#FUNCTION := 'GET'
                                                                                                                                                                          //Natural: PERFORM CALL-CWFN2100
        sub_Call_Cwfn2100();
        if (condition(Global.isEscape())) {return;}
        pdaIwfa4702.getIwfa4702_Pnd_Data3_Read_Ctr().nadd(pnd_Read_Ctr);                                                                                                  //Natural: ADD #READ-CTR TO #DATA3-READ-CTR
        pdaIwfa4702.getIwfa4702_Pnd_Data3_Add_Ctr().nadd(pnd_Add_Ctr);                                                                                                    //Natural: ADD #ADD-CTR TO #DATA3-ADD-CTR
        //*  JF082699
        if (condition((pnd_Err_Ctr.add(pdaIwfa4702.getIwfa4702_Pnd_Error_Count())).greater(99)))                                                                          //Natural: IF ( #ERR-CTR + IWFA4702.#ERROR-COUNT ) > 99
        {
            pdaIwfa4702.getIwfa4702_Pnd_Error_Count().setValue(99);                                                                                                       //Natural: MOVE 99 TO IWFA4702.#ERROR-COUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIwfa4702.getIwfa4702_Pnd_Error_Count().nadd(pnd_Err_Ctr);                                                                                                  //Natural: ADD #ERR-CTR TO IWFA4702.#ERROR-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("UPDATE");                                                                                                            //Natural: ASSIGN CDAOBJ.#FUNCTION := 'UPDATE'
                                                                                                                                                                          //Natural: PERFORM CALL-CWFN2100
        sub_Call_Cwfn2100();
        if (condition(Global.isEscape())) {return;}
        //*  JF082699
        pnd_Read_Ctr.reset();                                                                                                                                             //Natural: RESET #READ-CTR #ADD-CTR #ERR-CTR
        pnd_Add_Ctr.reset();
        pnd_Err_Ctr.reset();
    }
    private void sub_Call_Cwfn2100() throws Exception                                                                                                                     //Natural: CALL-CWFN2100
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------------------------------------
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("CWFN2100");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'CWFN2100'
        pnd_Last_Msg_Data.setValue("CWFN2100");
        DbsUtil.callnat(Cwfn2100.class , getCurrentProcessState(), pdaIwfa4702.getIwfa4702(), pdaIwfa4702.getIwfa4702_Id(), pdaIwfa4703.getIwfa4703(),                    //Natural: CALLNAT 'CWFN2100' IWFA4702 IWFA4702-ID IWFA4703 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
        sub_Check_For_Error();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE") || pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE")))                                         //Natural: IF CDAOBJ.#FUNCTION = 'STORE' OR = 'UPDATE'
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  JF082699
    private void sub_Populate_Key8() throws Exception                                                                                                                     //Natural: POPULATE-KEY8
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  PARTICIPANT
        pdaIwfa4700.getIwfa4700().reset();                                                                                                                                //Natural: RESET IWFA4700
        pdaIwfa4700.getIwfa4700_Pnd_Key8_Rec_Type().setValue("ERR");                                                                                                      //Natural: ASSIGN IWFA4700.#KEY8-REC-TYPE := 'ERR'
        pdaIwfa4700.getIwfa4700_Pnd_Key8_Admin_Unit_Cde().setValue(ldaIwfl4110.getCwf_Master_Index_View_Admin_Unit_Cde());                                                //Natural: ASSIGN IWFA4700.#KEY8-ADMIN-UNIT-CDE := CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
        pdaIwfa4700.getIwfa4700_Pnd_Key8_Rqst_Log_Dte_Tme().setValue(ldaIwfl4110.getCwf_Master_Index_View_Rqst_Log_Dte_Tme());                                            //Natural: ASSIGN IWFA4700.#KEY8-RQST-LOG-DTE-TME := CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
        pdaIwfa4700.getIwfa4700_Pnd_Key8_Cwf_Type().setValue("P");                                                                                                        //Natural: ASSIGN IWFA4700.#KEY8-CWF-TYPE := 'P'
        pdaIwfa4700.getIwfa4700_Pnd_Key8_Error_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                 //Natural: ASSIGN IWFA4700.#KEY8-ERROR-MSG := MSG-INFO-SUB.##MSG
        //*  JB1
        //*  JB1
        pdaIwfa4700.getIwfa4700_Pnd_Key8_Dte_Tme_Stamp().setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                          //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO IWFA4700.#KEY8-DTE-TME-STAMP
        pdaIwfa4700.getIwfa4700_Rcrd_Add_Pgrm().setValue(Global.getPROGRAM());                                                                                            //Natural: ASSIGN IWFA4700.RCRD-ADD-PGRM := *PROGRAM
        pdaIwfa4700.getIwfa4700_Rcrd_Add_User_Id().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN IWFA4700.RCRD-ADD-USER-ID := *INIT-USER
    }
    private void sub_Check_For_Error() throws Exception                                                                                                                   //Natural: CHECK-FOR-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ")))                   //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E' OR MSG-INFO-SUB.##ERROR-FIELD > ' '
        {
            //*  START JF082699
            if (condition(pnd_Individual_Err_Fl.getBoolean() && ! (pnd_Call_4700_On_Err.getBoolean())))                                                                   //Natural: IF #INDIVIDUAL-ERR-FL AND NOT #CALL-4700-ON-ERR
            {
                pnd_Last_Err_Msg.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                     //Natural: ASSIGN #LAST-ERR-MSG := MSG-INFO-SUB.##MSG
                if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).greater(" ")))                                                                   //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 3 ) > ' '
                {
                    pnd_Last_Err_Prg.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3));                                                                //Natural: ASSIGN #LAST-ERR-PRG := MSG-INFO-SUB.##MSG-DATA ( 3 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Last_Err_Prg.setValue(pnd_Last_Msg_Data);                                                                                                         //Natural: ASSIGN #LAST-ERR-PRG := #LAST-MSG-DATA
                }                                                                                                                                                         //Natural: END-IF
                pnd_Err_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #ERR-CTR
                //*  FORCE ET
                                                                                                                                                                          //Natural: PERFORM POPULATE-KEY8
                sub_Populate_Key8();
                if (condition(Global.isEscape())) {return;}
                pnd_Call_4700_On_Err.setValue(true);                                                                                                                      //Natural: ASSIGN #CALL-4700-ON-ERR := TRUE
                pnd_Et_Ctr.setValue(pnd_Et_Max);                                                                                                                          //Natural: ASSIGN #ET-CTR := #ET-MAX
                pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                     //Natural: ASSIGN CDAOBJ.#FUNCTION = 'STORE'
                                                                                                                                                                          //Natural: PERFORM CALL-OBJ-MAINTENANCE
                sub_Call_Obj_Maintenance();
                if (condition(Global.isEscape())) {return;}
                pnd_Call_4700_On_Err.setValue(false);                                                                                                                     //Natural: ASSIGN #CALL-4700-ON-ERR := FALSE
                pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                    //Natural: RESET MSG-INFO-SUB
                //*  (READ-FILE.)
                if (condition(pnd_Individual_Err_Fl.getBoolean()))                                                                                                        //Natural: REJECT IF #INDIVIDUAL-ERR-FL
                {
                    Global.setEscape(true); Global.setEscapeCode(EscapeType.Top); return;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Call_4700_On_Err.getBoolean()))                                                                                                         //Natural: IF #CALL-4700-ON-ERR
                {
                    //*  START JB1
                    //*  END JB1
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("2 ERRS: ", pnd_Last_Err_Msg, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg()));        //Natural: COMPRESS "2 ERRS: " #LAST-ERR-MSG MSG-INFO-SUB.##MSG INTO MSG-INFO-SUB.##MSG
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(Global.getPROGRAM());                                                             //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) := *PROGRAM
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                        //Natural: RESET MSG-INFO-SUB
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(Global.getPROGRAM());                                                                     //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = *PROGRAM
            pnd_Last_Msg_Data.setValue(Global.getPROGRAM());
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).equals(" ")))                                                                            //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 3 ) = ' '
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(pnd_Last_Msg_Data);                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) := #LAST-MSG-DATA
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE = 'E'
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("NATERR", Global.getERROR_NR(), "LINE", Global.getERROR_LINE()));                             //Natural: COMPRESS 'NATERR' *ERROR-NR 'LINE' *ERROR-LINE INTO MSG-INFO-SUB.##MSG
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
}
