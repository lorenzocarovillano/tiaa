/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:12:18 AM
**        * FROM NATURAL SUBPROGRAM : Swfn4410
************************************************************
**        * FILE NAME            : Swfn4410.java
**        * CLASS NAME           : Swfn4410
**        * INSTANCE NAME        : Swfn4410
************************************************************
************************************************************************
* PROGRAM  : SWFN4410
* SYSTEM   : SURVIVOR BENEFITS
* TITLE    : DRIVER PROGRAM TO RUN POST MODULES
* CREATED  : FEB 06,98
* AUTHOR   : CHRIS SARMIENTO
* FUNCTION : CURRENTLY BEING USED TO GENERATE 30 AND 90 DAY FOLLOW-UP
*            LETTERS.
* HISTORY
* 12/11/98 SARMIEC  ADDED PACKAGE ID FOR CORRESPONDENCE FOLLOW-UP
* 03/23/00 ARI G.   ADDED IA FORMS, IA AND DA PAYMENTS, AND SPECIALIZED
*                   LETTERS LOGIC.
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Swfn4410 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaPstl6185 ldaPstl6185;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9611 pdaPsta9611;
    private PdaCdaobj pdaCdaobj;

    // Local Variables
    public DbsRecord localVariables;

    // parameters

    private DbsGroup pnd_Parms;
    private DbsField pnd_Parms_Pnd_Letter_Type;
    private DbsField pnd_Parms_Pnd_Run_Dte;
    private DbsField pnd_Parms_Pnd_Ph_Pin;
    private DbsField pnd_Parms_Pnd_Ph_Name;
    private DbsField pnd_Parms_Pnd_Srvvr_Nme1;
    private DbsField pnd_Parms_Pnd_Srvvr_Nme2;
    private DbsField pnd_Parms_Pnd_Addr_Typ_Cde;
    private DbsField pnd_Parms_Pnd_Addr_Arr;
    private DbsField pnd_Parms_Pnd_Addr_Zip;
    private DbsField pnd_Parms_Pnd_Int_Rate;
    private DbsField pnd_Parms_Pnd_Year;
    private DbsField pnd_Parms_Pnd_Empl_Unit;
    private DbsField pnd_Parms_Pnd_Empl_Id;
    private DbsField pnd_Parms_Pnd_Empl_Nme;

    private DbsGroup pnd_Parms__R_Field_1;
    private DbsField pnd_Parms_Pnd_Empl_Nme_C;
    private DbsField pnd_Parms_Pnd_Printer_Id;
    private DbsField pnd_Parms_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Msg_Txt;
    private DbsField pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaPstl6185 = new LdaPstl6185();
        registerRecord(ldaPstl6185);
        localVariables = new DbsRecord();
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9611 = new PdaPsta9611(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);

        // parameters
        parameters = new DbsRecord();

        pnd_Parms = parameters.newGroupInRecord("pnd_Parms", "#PARMS");
        pnd_Parms.setParameterOption(ParameterOption.ByReference);
        pnd_Parms_Pnd_Letter_Type = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Parms_Pnd_Run_Dte = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Run_Dte", "#RUN-DTE", FieldType.DATE);
        pnd_Parms_Pnd_Ph_Pin = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Ph_Pin", "#PH-PIN", FieldType.NUMERIC, 12);
        pnd_Parms_Pnd_Ph_Name = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Ph_Name", "#PH-NAME", FieldType.STRING, 35);
        pnd_Parms_Pnd_Srvvr_Nme1 = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Srvvr_Nme1", "#SRVVR-NME1", FieldType.STRING, 35);
        pnd_Parms_Pnd_Srvvr_Nme2 = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Srvvr_Nme2", "#SRVVR-NME2", FieldType.STRING, 35);
        pnd_Parms_Pnd_Addr_Typ_Cde = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Addr_Typ_Cde", "#ADDR-TYP-CDE", FieldType.STRING, 1);
        pnd_Parms_Pnd_Addr_Arr = pnd_Parms.newFieldArrayInGroup("pnd_Parms_Pnd_Addr_Arr", "#ADDR-ARR", FieldType.STRING, 35, new DbsArrayController(1, 
            6));
        pnd_Parms_Pnd_Addr_Zip = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Addr_Zip", "#ADDR-ZIP", FieldType.STRING, 9);
        pnd_Parms_Pnd_Int_Rate = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Int_Rate", "#INT-RATE", FieldType.STRING, 6);
        pnd_Parms_Pnd_Year = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Year", "#YEAR", FieldType.STRING, 4);
        pnd_Parms_Pnd_Empl_Unit = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Empl_Unit", "#EMPL-UNIT", FieldType.STRING, 8);
        pnd_Parms_Pnd_Empl_Id = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Empl_Id", "#EMPL-ID", FieldType.STRING, 8);
        pnd_Parms_Pnd_Empl_Nme = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Empl_Nme", "#EMPL-NME", FieldType.STRING, 30);

        pnd_Parms__R_Field_1 = pnd_Parms.newGroupInGroup("pnd_Parms__R_Field_1", "REDEFINE", pnd_Parms_Pnd_Empl_Nme);
        pnd_Parms_Pnd_Empl_Nme_C = pnd_Parms__R_Field_1.newFieldArrayInGroup("pnd_Parms_Pnd_Empl_Nme_C", "#EMPL-NME-C", FieldType.STRING, 1, new DbsArrayController(1, 
            30));
        pnd_Parms_Pnd_Printer_Id = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Printer_Id", "#PRINTER-ID", FieldType.STRING, 4);
        pnd_Parms_Pnd_Rqst_Log_Dte_Tme = pnd_Parms.newFieldInGroup("pnd_Parms_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pdaCwfpdaus = new PdaCwfpdaus(parameters);
        pdaCwfpdaun = new PdaCwfpdaun(parameters);
        pdaCwfpda_D = new PdaCwfpda_D(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pdaCwfpda_P = new PdaCwfpda_P(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Msg_Txt = localVariables.newFieldInRecord("pnd_Msg_Txt", "#MSG-TXT", FieldType.STRING, 30);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaPstl6185.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Swfn4410() throws Exception
    {
        super("Swfn4410");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *---  VALUES DIFFERENT FOR EACH LETTER
        ldaPstl6185.getPstl6185_Data_Decsed_Nme().setValue(pnd_Parms_Pnd_Ph_Name);                                                                                        //Natural: ASSIGN PSTL6185-DATA.DECSED-NME := #PH-NAME
        //*  AG
        if (condition(pnd_Parms_Pnd_Srvvr_Nme2.greater(" ")))                                                                                                             //Natural: IF #SRVVR-NME2 GT ' '
        {
            ldaPstl6185.getPstl6185_Data_Ltr_Salutation().setValue(pnd_Parms_Pnd_Srvvr_Nme2);                                                                             //Natural: ASSIGN PSTL6185-DATA.LTR-SALUTATION := #SRVVR-NME2
            ldaPstl6185.getPstl6185_Data_Minor_Nme().setValue(pnd_Parms_Pnd_Srvvr_Nme1);                                                                                  //Natural: ASSIGN PSTL6185-DATA.MINOR-NME := #SRVVR-NME1
            //*  AG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaPstl6185.getPstl6185_Data_Ltr_Salutation().setValue(pnd_Parms_Pnd_Srvvr_Nme1);                                                                             //Natural: ASSIGN PSTL6185-DATA.LTR-SALUTATION := #SRVVR-NME1
            ldaPstl6185.getPstl6185_Data_Minor_Nme().setValue(" ");                                                                                                       //Natural: ASSIGN PSTL6185-DATA.MINOR-NME := ' '
            //*   SEND LETTER TO SURV
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl6185.getPstl6185_Data_Intrst_Rte().setValue(pnd_Parms_Pnd_Int_Rate);                                                                                       //Natural: ASSIGN PSTL6185-DATA.INTRST-RTE := #INT-RATE
        ldaPstl6185.getPstl6185_Data_Effctv_Yr().setValue(pnd_Parms_Pnd_Year);                                                                                            //Natural: ASSIGN PSTL6185-DATA.EFFCTV-YR := #YEAR
        ldaPstl6185.getPstl6185_Data_Letr_Id().setValue(pnd_Parms_Pnd_Letter_Type);                                                                                       //Natural: ASSIGN PSTL6185-DATA.LETR-ID := #LETTER-TYPE
        pdaPsta9611.getPsta9611_Letter_Dte().setValue(pnd_Parms_Pnd_Run_Dte);                                                                                             //Natural: ASSIGN PSTA9611.LETTER-DTE := #RUN-DTE
        pdaPsta9611.getPsta9611_Pin_Nbr().setValue(pnd_Parms_Pnd_Ph_Pin);                                                                                                 //Natural: ASSIGN PSTA9611.PIN-NBR := #PH-PIN
        pdaPsta9611.getPsta9611_Full_Nme().setValue(pnd_Parms_Pnd_Srvvr_Nme1);                                                                                            //Natural: ASSIGN PSTA9611.FULL-NME := #SRVVR-NME1
        //* *TA9611.LAST-NME             :=
        if (condition(pnd_Parms_Pnd_Srvvr_Nme2.greater(" ") && ! (pnd_Parms_Pnd_Addr_Arr.getValue(1).contains ("C/O"))))                                                  //Natural: IF #SRVVR-NME2 GT ' ' AND #ADDR-ARR ( 1 ) NE SCAN 'C/O'
        {
            pdaPsta9611.getPsta9611_Addrss_Line_Txt().getValue(2,":",6).setValue(pnd_Parms_Pnd_Addr_Arr.getValue(1,":",5));                                               //Natural: ASSIGN PSTA9611.ADDRSS-LINE-TXT ( 2:6 ) := #ADDR-ARR ( 1:5 )
            pdaPsta9611.getPsta9611_Addrss_Line_Txt().getValue(1).setValue(DbsUtil.compress("C/O", pnd_Parms_Pnd_Srvvr_Nme2));                                            //Natural: COMPRESS 'C/O' #SRVVR-NME2 INTO PSTA9611.ADDRSS-LINE-TXT ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9611.getPsta9611_Addrss_Line_Txt().getValue(1,":",6).setValue(pnd_Parms_Pnd_Addr_Arr.getValue(1,":",6));                                               //Natural: ASSIGN PSTA9611.ADDRSS-LINE-TXT ( 1:6 ) := #ADDR-ARR ( 1:6 )
            //* ---  CONVERT EMPL NAME TO PROPER NAME CASE
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9611.getPsta9611_Empl_Sgntry_Unit_Cde().setValue(pnd_Parms_Pnd_Empl_Unit);                                                                                 //Natural: ASSIGN PSTA9611.EMPL-SGNTRY-UNIT-CDE := #EMPL-UNIT
        pdaPsta9611.getPsta9611_Empl_Sgntry_Oprtr_Cde().setValue(pnd_Parms_Pnd_Empl_Id);                                                                                  //Natural: ASSIGN PSTA9611.EMPL-SGNTRY-OPRTR-CDE := #EMPL-ID
        FOR01:                                                                                                                                                            //Natural: FOR #I EQ 1 TO 30
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(30)); pnd_I.nadd(1))
        {
            if (condition(pnd_I.equals(1) || ! (DbsUtil.maskMatches(pnd_Parms_Pnd_Empl_Nme_C.getValue(pnd_I.getDec().subtract(1)),"A"))))                                 //Natural: IF #I EQ 1 OR #EMPL-NME-C ( #I - 1 ) NE MASK ( A )
            {
                DbsUtil.examine(new ExamineSource(pnd_Parms_Pnd_Empl_Nme_C.getValue(pnd_I)), new ExamineTranslate(TranslateOption.Upper));                                //Natural: EXAMINE #EMPL-NME-C ( #I ) TRANSLATE INTO UPPER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                DbsUtil.examine(new ExamineSource(pnd_Parms_Pnd_Empl_Nme_C.getValue(pnd_I)), new ExamineTranslate(TranslateOption.Lower));                                //Natural: EXAMINE #EMPL-NME-C ( #I ) TRANSLATE INTO LOWER
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  PSTA9611.EMPL-SGNTRY-NME      := #EMPL-NME
        //* *---  VALUES CONSTANT FOR ALL THE LETTERS THAT'll be printed
        pdaPsta9611.getPsta9611_Systm_Id_Cde().setValue("MIT");                                                                                                           //Natural: ASSIGN PSTA9611.SYSTM-ID-CDE := 'MIT'
        //*  AG BEGIN
        short decideConditionsMet550 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #LETTER-TYPE;//Natural: VALUE '1', '2', '8', '9', 'A', 'B'
        if (condition((pnd_Parms_Pnd_Letter_Type.equals("1") || pnd_Parms_Pnd_Letter_Type.equals("2") || pnd_Parms_Pnd_Letter_Type.equals("8") || pnd_Parms_Pnd_Letter_Type.equals("9") 
            || pnd_Parms_Pnd_Letter_Type.equals("A") || pnd_Parms_Pnd_Letter_Type.equals("B"))))
        {
            decideConditionsMet550++;
            pdaPsta9611.getPsta9611_Pckge_Cde().setValue("PTSURFL");                                                                                                      //Natural: ASSIGN PSTA9611.PCKGE-CDE := 'PTSURFL'
            //*  VALUE '2'
            //*    PSTA9611.PCKGE-CDE        := 'PTSURBN9'
        }                                                                                                                                                                 //Natural: VALUE '3'
        else if (condition((pnd_Parms_Pnd_Letter_Type.equals("3"))))
        {
            decideConditionsMet550++;
            pdaPsta9611.getPsta9611_Pckge_Cde().setValue("PCSURMDS");                                                                                                     //Natural: ASSIGN PSTA9611.PCKGE-CDE := 'PCSURMDS'
        }                                                                                                                                                                 //Natural: VALUE '4'
        else if (condition((pnd_Parms_Pnd_Letter_Type.equals("4"))))
        {
            decideConditionsMet550++;
            pdaPsta9611.getPsta9611_Pckge_Cde().setValue("PCSURMD1");                                                                                                     //Natural: ASSIGN PSTA9611.PCKGE-CDE := 'PCSURMD1'
        }                                                                                                                                                                 //Natural: VALUE '5'
        else if (condition((pnd_Parms_Pnd_Letter_Type.equals("5"))))
        {
            decideConditionsMet550++;
            pdaPsta9611.getPsta9611_Pckge_Cde().setValue("PCSURMD2");                                                                                                     //Natural: ASSIGN PSTA9611.PCKGE-CDE := 'PCSURMD2'
        }                                                                                                                                                                 //Natural: VALUE '6'
        else if (condition((pnd_Parms_Pnd_Letter_Type.equals("6"))))
        {
            decideConditionsMet550++;
            pdaPsta9611.getPsta9611_Pckge_Cde().setValue("PTSURCM3");                                                                                                     //Natural: ASSIGN PSTA9611.PCKGE-CDE := 'PTSURCM3'
        }                                                                                                                                                                 //Natural: VALUE '7'
        else if (condition((pnd_Parms_Pnd_Letter_Type.equals("7"))))
        {
            decideConditionsMet550++;
            pdaPsta9611.getPsta9611_Pckge_Cde().setValue("PTSURCM9");                                                                                                     //Natural: ASSIGN PSTA9611.PCKGE-CDE := 'PTSURCM9'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Invalid Package ID for POST.");                                                                           //Natural: ASSIGN ##MSG := 'Invalid Package ID for POST.'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  PSTA9611.PRNTR-ID-CDE         := 'TZUG'
        pdaPsta9611.getPsta9611_Prntr_Id_Cde().setValue(pnd_Parms_Pnd_Printer_Id);                                                                                        //Natural: ASSIGN PSTA9611.PRNTR-ID-CDE := #PRINTER-ID
        pdaPsta9611.getPsta9611_Pckge_Immdte_Prnt_Ind().setValue("N");                                                                                                    //Natural: ASSIGN PSTA9611.PCKGE-IMMDTE-PRNT-IND := 'N'
        //*  PSTA9611.PCKGE-IMMDTE-PRNT-IND:= 'Y'
        pdaPsta9611.getPsta9611_Pckge_Image_Ind().setValue("Y");                                                                                                          //Natural: ASSIGN PSTA9611.PCKGE-IMAGE-IND := 'Y'
        pdaPsta9611.getPsta9611_No_Updte_To_Mit_Ind().setValue(true);                                                                                                     //Natural: ASSIGN PSTA9611.NO-UPDTE-TO-MIT-IND := TRUE
        pdaPsta9611.getPsta9611_No_Updte_To_Efm_Ind().setValue(false);                                                                                                    //Natural: ASSIGN PSTA9611.NO-UPDTE-TO-EFM-IND := FALSE
        pdaPsta9611.getPsta9611_Addrss_Typ_Cde().setValue(pnd_Parms_Pnd_Addr_Typ_Cde);                                                                                    //Natural: ASSIGN PSTA9611.ADDRSS-TYP-CDE := #ADDR-TYP-CDE
        //*  PSTA9611.ADDRSS-LINES-RULE    := TRUE
        pdaPsta9611.getPsta9611_Addrss_Zp9_Nbr().setValue(pnd_Parms_Pnd_Addr_Zip);                                                                                        //Natural: ASSIGN PSTA9611.ADDRSS-ZP9-NBR := #ADDR-ZIP
        //* *---
        pdaPsta9611.getPsta9611_Rqst_Log_Dte_Tme().getValue(1).setValue(pnd_Parms_Pnd_Rqst_Log_Dte_Tme);                                                                  //Natural: ASSIGN PSTA9611.RQST-LOG-DTE-TME ( 1 ) := #RQST-LOG-DTE-TME
        //* *---  POST OPEN
        pnd_Msg_Txt.setValue("POST OPEN");                                                                                                                                //Natural: ASSIGN #MSG-TXT := 'POST OPEN'
        DbsUtil.callnat(Pstn9610.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9611.getPsta9611(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9610' PSTA9610 PSTA9611 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().greater(" ")))                                                                                    //Natural: IF MSG-INFO-SUB.##RETURN-CODE GT ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE
            sub_Error_Routine();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *---  POST WRITE
        pnd_Msg_Txt.setValue("POST WRITE");                                                                                                                               //Natural: ASSIGN #MSG-TXT := 'POST WRITE'
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY PSTL6185-DATA
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6185.getSort_Key(), ldaPstl6185.getPstl6185_Data());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE
            sub_Error_Routine();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *---  POST CLOSE
        pnd_Msg_Txt.setValue("POST CLOSE");                                                                                                                               //Natural: ASSIGN #MSG-TXT := 'POST CLOSE'
        DbsUtil.callnat(Pstn9680.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9611.getPsta9611(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9680' PSTA9610 PSTA9611 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE <> ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE
            sub_Error_Routine();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  BACKOUT TRANSACTION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-ROUTINE
        //*  BACKOUT TRANSACTION
        //* *---  BACK OUT POST TRANSACTIONS
    }
    private void sub_Error_Routine() throws Exception                                                                                                                     //Natural: ERROR-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress(pnd_Msg_Txt, "ERROR:", pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg()));                           //Natural: COMPRESS #MSG-TXT 'ERROR:' ##MSG INTO ##MSG
        pdaPsta9610.getPsta9610_Pst_Backout_Ind().setValue(true);                                                                                                         //Natural: ASSIGN PSTA9610.PST-BACKOUT-IND := TRUE
        DbsUtil.callnat(Pstn9680.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9611.getPsta9611(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9680' PSTA9610 PSTA9611 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  BACKOUT TRANSACTION
    }

    //
}
