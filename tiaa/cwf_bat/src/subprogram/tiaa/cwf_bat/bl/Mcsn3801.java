/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:25:19 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3801
************************************************************
**        * FILE NAME            : Mcsn3801.java
**        * CLASS NAME           : Mcsn3801
**        * INSTANCE NAME        : Mcsn3801
************************************************************
************************************************************************
* PROGRAM  : MCSN3801
* TITLE    : ACCESS FULFILLMENT RECORDS FROM THE CWF-ATI-FULFILLMENT
* FUNCTION : DISPLAY ATI-FULFILLMENT RECORDS ON 192 IN WPID ORDER
* HISTORY  :
*          : MB - 05/01 - ADDED INDEX BOND CODE - I -
* 03/27/03 | GH REMOVED ACCNT(*) ADDED THE SSSS-TICKER-SYMBOL(1:20)
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3801 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;

    private DataAccessProgramView vw_cwf_Ati_Fulfill;
    private DbsField cwf_Ati_Fulfill_Ati_Rcrd_Type;

    private DbsGroup cwf_Ati_Fulfill_Ati_Rqst_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Pin;
    private DbsField cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Stts;
    private DbsField cwf_Ati_Fulfill_Ati_Wpid;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Racf_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Unit_Cde;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Error_Txt;
    private DbsField cwf_Ati_Fulfill_Ati_Prge_Ind;
    private DbsField cwf_Ati_Fulfill_Ati_Stts_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Cmtask_Ind;
    private DbsField cwf_Ati_Fulfill_Ssss_Ovrnght_Mail_Ind;
    private DbsField cwf_Ati_Fulfill_Ssss_Ctznshp_Cde;
    private DbsField cwf_Ati_Fulfill_Ssss_Rsdnce_Cde;
    private DbsField cwf_Ati_Fulfill_Ssss_Plan_Cde;
    private DbsField cwf_Ati_Fulfill_Ssss_Tiaa_Nbr;
    private DbsField cwf_Ati_Fulfill_Ssss_Rllvr_Type;
    private DbsField cwf_Ati_Fulfill_Ssss_Erly_Trm_Type;
    private DbsField cwf_Ati_Fulfill_Ssss_Wth_All_Fnds_Ind;
    private DbsField cwf_Ati_Fulfill_Ssss_Rpttve_Frqncy;
    private DbsField cwf_Ati_Fulfill_Ssss_Rpttve_Bgn_Dte;
    private DbsField cwf_Ati_Fulfill_Ssss_Rpttve_End_Dte;
    private DbsField cwf_Ati_Fulfill_Ssss_Rpttve_Nbr_Pymnts;

    private DbsGroup cwf_Ati_Fulfill_Ssss_Accnt_Data;
    private DbsField cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt;
    private DbsField cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt;
    private DbsField cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt;

    private DbsGroup cwf_Ati_Fulfill_Ssss_Accnt_Data_Oia;
    private DbsField cwf_Ati_Fulfill_Ssss_Ticker_Symbol;
    private DbsField pnd_Ati_Fulfill_Key;

    private DbsGroup pnd_Ati_Fulfill_Key__R_Field_1;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts;
    private DbsField pnd_Grand_Total_Open_Atis;
    private DbsField pnd_Grand_Total_Inprogress_Atis;
    private DbsField pnd_Grand_Total_Failed_Atis;
    private DbsField pnd_Grand_Total_Succesfull_Atis;
    private DbsField pnd_Interim_Dollar;
    private DbsField pnd_Interim_Prcnt;
    private DbsField pnd_Interim_Units;
    private DbsField pnd_Interim_Acct_Name;
    private DbsField pnd_Process_Status_Literal;
    private DbsField pnd_Cmtask_Status_Literal;
    private DbsField pnd_Compressed_Text;
    private DbsField pnd_B;
    private DbsField pnd_Cc;
    private DbsField pnd_Prev_Wpid;
    private DbsField pnd_Wpid_Total;
    private DbsField pnd_Fld_Line;
    private DbsField pnd_Fld_Strt;
    private DbsField pnd_Fld_End;
    private DbsField pnd_Accnt_Amt_Type;
    private DbsField pnd_Field;
    private DbsField pnd_Title_Literal;
    private DbsField pnd_Input_Dte_Tme;
    private DbsField pnd_Input_Dte_D;
    private DbsField pnd_Orig_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Entry_Date;
    private DbsField pnd_Last_Page_Flag;
    private DbsField pnd_Mit_Log_Date;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Input_Data = parameters.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Ati_Fulfill = new DataAccessProgramView(new NameInfo("vw_cwf_Ati_Fulfill", "CWF-ATI-FULFILL"), "CWF_ATI_FULFILL", "CWF_KDO_FULFILL", DdmPeriodicGroups.getInstance().getGroups("CWF_ATI_FULFILL"));
        cwf_Ati_Fulfill_Ati_Rcrd_Type = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Rcrd_Type", "ATI-RCRD-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_RCRD_TYPE");

        cwf_Ati_Fulfill_Ati_Rqst_Id = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ATI_RQST_ID", "ATI-RQST-ID");
        cwf_Ati_Fulfill_Ati_Pin = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Pin", "ATI-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ATI_PIN");
        cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme", "ATI-MIT-LOG-DT-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_MIT_LOG_DT_TME");
        cwf_Ati_Fulfill_Ati_Prcss_Stts = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Stts", "ATI-PRCSS-STTS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRCSS_STTS");
        cwf_Ati_Fulfill_Ati_Wpid = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Wpid", "ATI-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ATI_WPID");
        cwf_Ati_Fulfill_Ati_Entry_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Dte_Tme", "ATI-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_ENTRY_DTE_TME");
        cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt", "ATI-ENTRY-SYSTM-OR-UNT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_SYSTM_OR_UNT");
        cwf_Ati_Fulfill_Ati_Entry_Racf_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Racf_Id", "ATI-ENTRY-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ATI_ENTRY_RACF_ID");
        cwf_Ati_Fulfill_Ati_Entry_Unit_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Unit_Cde", "ATI-ENTRY-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_UNIT_CDE");
        cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id", "ATI-PRCSS-APPLCTN-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_PRCSS_APPLCTN_ID");
        cwf_Ati_Fulfill_Ati_Error_Txt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Error_Txt", "ATI-ERROR-TXT", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "ATI_ERROR_TXT");
        cwf_Ati_Fulfill_Ati_Prge_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prge_Ind", "ATI-PRGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRGE_IND");
        cwf_Ati_Fulfill_Ati_Stts_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Stts_Dte_Tme", "ATI-STTS-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_STTS_DTE_TME");
        cwf_Ati_Fulfill_Ati_Cmtask_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Cmtask_Ind", "ATI-CMTASK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_CMTASK_IND");
        cwf_Ati_Fulfill_Ssss_Ovrnght_Mail_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Ovrnght_Mail_Ind", "SSSS-OVRNGHT-MAIL-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SSSS_OVRNGHT_MAIL_IND");
        cwf_Ati_Fulfill_Ssss_Ctznshp_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Ctznshp_Cde", "SSSS-CTZNSHP-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "SSSS_CTZNSHP_CDE");
        cwf_Ati_Fulfill_Ssss_Rsdnce_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Rsdnce_Cde", "SSSS-RSDNCE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "SSSS_RSDNCE_CDE");
        cwf_Ati_Fulfill_Ssss_Plan_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Plan_Cde", "SSSS-PLAN-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "SSSS_PLAN_CDE");
        cwf_Ati_Fulfill_Ssss_Tiaa_Nbr = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Tiaa_Nbr", "SSSS-TIAA-NBR", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "SSSS_TIAA_NBR");
        cwf_Ati_Fulfill_Ssss_Rllvr_Type = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Rllvr_Type", "SSSS-RLLVR-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SSSS_RLLVR_TYPE");
        cwf_Ati_Fulfill_Ssss_Erly_Trm_Type = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Erly_Trm_Type", "SSSS-ERLY-TRM-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SSSS_ERLY_TRM_TYPE");
        cwf_Ati_Fulfill_Ssss_Wth_All_Fnds_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Wth_All_Fnds_Ind", "SSSS-WTH-ALL-FNDS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SSSS_WTH_ALL_FNDS_IND");
        cwf_Ati_Fulfill_Ssss_Rpttve_Frqncy = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Rpttve_Frqncy", "SSSS-RPTTVE-FRQNCY", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SSSS_RPTTVE_FRQNCY");
        cwf_Ati_Fulfill_Ssss_Rpttve_Bgn_Dte = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Rpttve_Bgn_Dte", "SSSS-RPTTVE-BGN-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "SSSS_RPTTVE_BGN_DTE");
        cwf_Ati_Fulfill_Ssss_Rpttve_End_Dte = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Rpttve_End_Dte", "SSSS-RPTTVE-END-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "SSSS_RPTTVE_END_DTE");
        cwf_Ati_Fulfill_Ssss_Rpttve_Nbr_Pymnts = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Rpttve_Nbr_Pymnts", "SSSS-RPTTVE-NBR-PYMNTS", 
            FieldType.PACKED_DECIMAL, 5, RepeatingFieldStrategy.None, "SSSS_RPTTVE_NBR_PYMNTS");

        cwf_Ati_Fulfill_Ssss_Accnt_Data = vw_cwf_Ati_Fulfill.getRecord().newGroupArrayInGroup("cwf_Ati_Fulfill_Ssss_Accnt_Data", "SSSS-ACCNT-DATA", new 
            DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_KDO_FULFILL_SSSS_ACCNT_DATA");
        cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt = cwf_Ati_Fulfill_Ssss_Accnt_Data.newFieldInGroup("cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt", "SSSS-WTHDRWL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "SSSS_WTHDRWL_AMT", "CWF_KDO_FULFILL_SSSS_ACCNT_DATA");
        cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt = cwf_Ati_Fulfill_Ssss_Accnt_Data.newFieldInGroup("cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt", "SSSS-WTHDRWL-PRCNT", 
            FieldType.PACKED_DECIMAL, 6, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "SSSS_WTHDRWL_PRCNT", "CWF_KDO_FULFILL_SSSS_ACCNT_DATA");
        cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt = cwf_Ati_Fulfill_Ssss_Accnt_Data.newFieldInGroup("cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt", "SSSS-WTHDRWL-UNT", FieldType.PACKED_DECIMAL, 
            11, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "SSSS_WTHDRWL_UNT", "CWF_KDO_FULFILL_SSSS_ACCNT_DATA");

        cwf_Ati_Fulfill_Ssss_Accnt_Data_Oia = vw_cwf_Ati_Fulfill.getRecord().newGroupArrayInGroup("cwf_Ati_Fulfill_Ssss_Accnt_Data_Oia", "SSSS-ACCNT-DATA-OIA", 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_KDO_FULFILL_SSSS_ACCNT_DATA_OIA");
        cwf_Ati_Fulfill_Ssss_Ticker_Symbol = cwf_Ati_Fulfill_Ssss_Accnt_Data_Oia.newFieldInGroup("cwf_Ati_Fulfill_Ssss_Ticker_Symbol", "SSSS-TICKER-SYMBOL", 
            FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "SSSS_TICKER_SYMBOL", "CWF_KDO_FULFILL_SSSS_ACCNT_DATA_OIA");
        registerRecord(vw_cwf_Ati_Fulfill);

        pnd_Ati_Fulfill_Key = localVariables.newFieldInRecord("pnd_Ati_Fulfill_Key", "#ATI-FULFILL-KEY", FieldType.STRING, 9);

        pnd_Ati_Fulfill_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Ati_Fulfill_Key__R_Field_1", "REDEFINE", pnd_Ati_Fulfill_Key);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl = pnd_Ati_Fulfill_Key__R_Field_1.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl", "#ATI-RCRD-TYPE-CNTRL", 
            FieldType.STRING, 1);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme = pnd_Ati_Fulfill_Key__R_Field_1.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme", "#ATI-ENTRY-DTE-TME", 
            FieldType.TIME);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts = pnd_Ati_Fulfill_Key__R_Field_1.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts", "#ATI-PRCSS-STTS", 
            FieldType.STRING, 1);
        pnd_Grand_Total_Open_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Open_Atis", "#GRAND-TOTAL-OPEN-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Inprogress_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Inprogress_Atis", "#GRAND-TOTAL-INPROGRESS-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Grand_Total_Failed_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Failed_Atis", "#GRAND-TOTAL-FAILED-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Succesfull_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Succesfull_Atis", "#GRAND-TOTAL-SUCCESFULL-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Interim_Dollar = localVariables.newFieldInRecord("pnd_Interim_Dollar", "#INTERIM-DOLLAR", FieldType.NUMERIC, 12, 2);
        pnd_Interim_Prcnt = localVariables.newFieldInRecord("pnd_Interim_Prcnt", "#INTERIM-PRCNT", FieldType.NUMERIC, 12, 3);
        pnd_Interim_Units = localVariables.newFieldInRecord("pnd_Interim_Units", "#INTERIM-UNITS", FieldType.NUMERIC, 12, 3);
        pnd_Interim_Acct_Name = localVariables.newFieldInRecord("pnd_Interim_Acct_Name", "#INTERIM-ACCT-NAME", FieldType.STRING, 11);
        pnd_Process_Status_Literal = localVariables.newFieldInRecord("pnd_Process_Status_Literal", "#PROCESS-STATUS-LITERAL", FieldType.STRING, 7);
        pnd_Cmtask_Status_Literal = localVariables.newFieldInRecord("pnd_Cmtask_Status_Literal", "#CMTASK-STATUS-LITERAL", FieldType.STRING, 27);
        pnd_Compressed_Text = localVariables.newFieldInRecord("pnd_Compressed_Text", "#COMPRESSED-TEXT", FieldType.STRING, 38);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 3);
        pnd_Cc = localVariables.newFieldInRecord("pnd_Cc", "#CC", FieldType.NUMERIC, 3);
        pnd_Prev_Wpid = localVariables.newFieldInRecord("pnd_Prev_Wpid", "#PREV-WPID", FieldType.STRING, 6);
        pnd_Wpid_Total = localVariables.newFieldInRecord("pnd_Wpid_Total", "#WPID-TOTAL", FieldType.NUMERIC, 8);
        pnd_Fld_Line = localVariables.newFieldInRecord("pnd_Fld_Line", "#FLD-LINE", FieldType.NUMERIC, 3);
        pnd_Fld_Strt = localVariables.newFieldInRecord("pnd_Fld_Strt", "#FLD-STRT", FieldType.NUMERIC, 3);
        pnd_Fld_End = localVariables.newFieldInRecord("pnd_Fld_End", "#FLD-END", FieldType.NUMERIC, 3);
        pnd_Accnt_Amt_Type = localVariables.newFieldInRecord("pnd_Accnt_Amt_Type", "#ACCNT-AMT-TYPE", FieldType.STRING, 1);
        pnd_Field = localVariables.newFieldInRecord("pnd_Field", "#FIELD", FieldType.STRING, 30);
        pnd_Title_Literal = localVariables.newFieldInRecord("pnd_Title_Literal", "#TITLE-LITERAL", FieldType.STRING, 54);
        pnd_Input_Dte_Tme = localVariables.newFieldInRecord("pnd_Input_Dte_Tme", "#INPUT-DTE-TME", FieldType.TIME);
        pnd_Input_Dte_D = localVariables.newFieldInRecord("pnd_Input_Dte_D", "#INPUT-DTE-D", FieldType.DATE);
        pnd_Orig_Date = localVariables.newFieldInRecord("pnd_Orig_Date", "#ORIG-DATE", FieldType.DATE);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 8);
        pnd_Entry_Date = localVariables.newFieldInRecord("pnd_Entry_Date", "#ENTRY-DATE", FieldType.DATE);
        pnd_Last_Page_Flag = localVariables.newFieldInRecord("pnd_Last_Page_Flag", "#LAST-PAGE-FLAG", FieldType.STRING, 1);
        pnd_Mit_Log_Date = localVariables.newFieldInRecord("pnd_Mit_Log_Date", "#MIT-LOG-DATE", FieldType.NUMERIC, 14);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Ati_Fulfill.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Mcsn3801() throws Exception
    {
        super("Mcsn3801");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atEndOfPage(atEndEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 1 ) PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 2 ) PS = 60 LS = 133 ZP = OFF
        pnd_Last_Page_Flag.reset();                                                                                                                                       //Natural: RESET #LAST-PAGE-FLAG #GRAND-TOTAL-OPEN-ATIS #GRAND-TOTAL-INPROGRESS-ATIS #GRAND-TOTAL-FAILED-ATIS #GRAND-TOTAL-SUCCESFULL-ATIS
        pnd_Grand_Total_Open_Atis.reset();
        pnd_Grand_Total_Inprogress_Atis.reset();
        pnd_Grand_Total_Failed_Atis.reset();
        pnd_Grand_Total_Succesfull_Atis.reset();
        if (condition(pnd_Input_Data_Pnd_Input_Date.notEquals(" ")))                                                                                                      //Natural: IF #INPUT-DATE NE ' '
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Input_Dte_Tme.setValue(pnd_Input_Dte_D);                                                                                                                  //Natural: MOVE #INPUT-DTE-D TO #INPUT-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***");                                     //Natural: WRITE ( 1 ) /// '*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***'
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
        //* *** INITIALIZE THE ATI-CONTROL-KEY SUPER-DESCRIPTOR FIELD
        pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl.setValue("1");                                                                                                        //Natural: ASSIGN #ATI-RCRD-TYPE-CNTRL := '1'
        pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme.reset();                                                                                                                //Natural: RESET #ATI-ENTRY-DTE-TME
        //* *****************************************************************
        //* ** READ CWF-ATI-FULFILL AND PROCESS OPEN AND IN-PROGRESS REQUESTS
        //* *****************************************************************
        pnd_Title_Literal.setValue("**** OPEN/IN-PROGRESS ATI REQUESTS FOR SSSS FORMS ****");                                                                             //Natural: MOVE '**** OPEN/IN-PROGRESS ATI REQUESTS FOR SSSS FORMS ****' TO #TITLE-LITERAL
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        vw_cwf_Ati_Fulfill.startDatabaseRead                                                                                                                              //Natural: READ CWF-ATI-FULFILL BY ATI-FULFILL-KEY STARTING FROM #ATI-FULFILL-KEY
        (
        "READ01",
        new Wc[] { new Wc("ATI_FULFILL_KEY", ">=", pnd_Ati_Fulfill_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("ATI_FULFILL_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Ati_Fulfill.readNextRow("READ01")))
        {
            if (condition(cwf_Ati_Fulfill_Ati_Rcrd_Type.greater("2")))                                                                                                    //Natural: IF CWF-ATI-FULFILL.ATI-RCRD-TYPE GT '2'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Entry_Date.setValue(cwf_Ati_Fulfill_Ati_Stts_Dte_Tme);                                                                                                    //Natural: MOVE CWF-ATI-FULFILL.ATI-STTS-DTE-TME TO #ENTRY-DATE
            if (condition(!(pnd_Entry_Date.equals(pnd_Input_Dte_D) && cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id.equals("SSSS"))))                                              //Natural: ACCEPT IF #ENTRY-DATE = #INPUT-DTE-D AND ATI-PRCSS-APPLCTN-ID = 'SSSS'
            {
                continue;
            }
            getSort().writeSortInData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type,               //Natural: END-ALL
                cwf_Ati_Fulfill_Ati_Prcss_Stts, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, 
                cwf_Ati_Fulfill_Ati_Entry_Racf_Id, cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Ssss_Rllvr_Type, 
                cwf_Ati_Fulfill_Ssss_Erly_Trm_Type, cwf_Ati_Fulfill_Ssss_Wth_All_Fnds_Ind, cwf_Ati_Fulfill_Ssss_Rpttve_Frqncy, cwf_Ati_Fulfill_Ssss_Rpttve_Nbr_Pymnts, 
                cwf_Ati_Fulfill_Ssss_Rpttve_Bgn_Dte, cwf_Ati_Fulfill_Ssss_Rpttve_End_Dte, cwf_Ati_Fulfill_Ssss_Ovrnght_Mail_Ind, cwf_Ati_Fulfill_Ssss_Ctznshp_Cde, 
                cwf_Ati_Fulfill_Ssss_Rsdnce_Cde, cwf_Ati_Fulfill_Ssss_Plan_Cde, cwf_Ati_Fulfill_Ssss_Tiaa_Nbr, cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(1), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(2), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(3), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(4), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(5), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(6), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(7), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(8), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(9), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(10), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(11), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(12), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(13), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(14), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(15), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(16), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(17), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(18), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(19), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(20), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(1), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(2), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(3), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(4), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(5), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(6), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(7), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(8), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(9), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(10), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(11), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(12), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(13), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(14), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(15), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(16), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(17), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(18), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(19), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(20), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(1), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(2), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(3), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(4), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(5), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(6), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(7), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(8), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(9), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(10), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(11), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(12), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(13), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(14), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(15), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(16), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(17), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(18), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(19), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(20), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(1), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(2), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(3), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(4), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(5), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(6), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(7), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(8), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(9), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(10), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(11), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(12), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(13), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(14), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(15), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(16), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(17), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(18), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(19), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(20));
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  SORT PERFORMED TO WRITE THEM PIN/MIT-LOG-DATE-TIME/REC-TYPE ORDER
        getSort().sortData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type, cwf_Ati_Fulfill_Ati_Prcss_Stts); //Natural: SORT RECORDS BY CWF-ATI-FULFILL.ATI-WPID CWF-ATI-FULFILL.ATI-PIN CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME CWF-ATI-FULFILL.ATI-RCRD-TYPE CWF-ATI-FULFILL.ATI-PRCSS-STTS USING CWF-ATI-FULFILL.ATI-ERROR-TXT CWF-ATI-FULFILL.ATI-ENTRY-DTE-TME CWF-ATI-FULFILL.ATI-ENTRY-SYSTM-OR-UNT CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE CWF-ATI-FULFILL.ATI-STTS-DTE-TME CWF-ATI-FULFILL.SSSS-RLLVR-TYPE CWF-ATI-FULFILL.SSSS-ERLY-TRM-TYPE CWF-ATI-FULFILL.SSSS-WTH-ALL-FNDS-IND CWF-ATI-FULFILL.SSSS-RPTTVE-FRQNCY CWF-ATI-FULFILL.SSSS-RPTTVE-NBR-PYMNTS CWF-ATI-FULFILL.SSSS-RPTTVE-BGN-DTE CWF-ATI-FULFILL.SSSS-RPTTVE-END-DTE CWF-ATI-FULFILL.SSSS-OVRNGHT-MAIL-IND CWF-ATI-FULFILL.SSSS-CTZNSHP-CDE CWF-ATI-FULFILL.SSSS-RSDNCE-CDE CWF-ATI-FULFILL.SSSS-PLAN-CDE CWF-ATI-FULFILL.SSSS-TIAA-NBR CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( * ) CWF-ATI-FULFILL.SSSS-WTHDRWL-AMT ( * ) CWF-ATI-FULFILL.SSSS-WTHDRWL-PRCNT ( * ) CWF-ATI-FULFILL.SSSS-WTHDRWL-UNT ( * )
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type, 
            cwf_Ati_Fulfill_Ati_Prcss_Stts, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, cwf_Ati_Fulfill_Ati_Entry_Racf_Id, 
            cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Ssss_Rllvr_Type, cwf_Ati_Fulfill_Ssss_Erly_Trm_Type, cwf_Ati_Fulfill_Ssss_Wth_All_Fnds_Ind, 
            cwf_Ati_Fulfill_Ssss_Rpttve_Frqncy, cwf_Ati_Fulfill_Ssss_Rpttve_Nbr_Pymnts, cwf_Ati_Fulfill_Ssss_Rpttve_Bgn_Dte, cwf_Ati_Fulfill_Ssss_Rpttve_End_Dte, 
            cwf_Ati_Fulfill_Ssss_Ovrnght_Mail_Ind, cwf_Ati_Fulfill_Ssss_Ctznshp_Cde, cwf_Ati_Fulfill_Ssss_Rsdnce_Cde, cwf_Ati_Fulfill_Ssss_Plan_Cde, cwf_Ati_Fulfill_Ssss_Tiaa_Nbr, 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(1), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(2), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(3), 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(4), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(5), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(6), 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(7), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(8), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(9), 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(10), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(11), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(12), 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(13), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(14), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(15), 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(16), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(17), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(18), 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(19), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(20), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(1), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(2), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(3), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(4), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(5), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(6), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(7), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(8), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(9), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(10), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(11), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(12), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(13), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(14), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(15), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(16), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(17), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(18), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(19), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(20), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(1), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(2), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(3), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(4), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(5), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(6), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(7), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(8), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(9), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(10), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(11), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(12), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(13), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(14), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(15), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(16), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(17), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(18), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(19), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(20), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(1), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(2), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(3), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(4), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(5), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(6), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(7), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(8), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(9), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(10), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(11), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(12), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(13), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(14), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(15), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(16), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(17), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(18), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(19), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(20))))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF CWF-ATI-FULFILL.ATI-WPID
            pnd_Prev_Wpid.setValue(cwf_Ati_Fulfill_Ati_Wpid);                                                                                                             //Natural: ASSIGN #PREV-WPID := CWF-ATI-FULFILL.ATI-WPID
            if (condition(cwf_Ati_Fulfill_Ati_Rcrd_Type.equals("1")))                                                                                                     //Natural: IF CWF-ATI-FULFILL.ATI-RCRD-TYPE = '1'
            {
                pnd_Wpid_Total.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WPID-TOTAL
                                                                                                                                                                          //Natural: PERFORM ATI-WRITE-REC-1
                sub_Ati_Write_Rec_1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ATI-WRITE-REC-2
                sub_Ati_Write_Rec_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  END PROCESSING OF OPEN AND IN-PROGRESS REQUEST
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //*  ----------------------------------------------------------------------
        //*  4T CWF-ATI-FULFILL.ATI-PIN
        //*  20T CWF-ATI-FULFILL.ATI-WPID
        //*  27T CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  43T CWF-ATI-FULFILL.ATI-ENTRY-DTE-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  48T CWF-ATI-FULFILL.ATI-STTS-DTE-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  68T #PROCESS-STATUS-LITERAL
        //*  78T CWF-ATI-FULFILL.SSSS-OVRNGHT-MAIL-IND
        //*  82T CWF-ATI-FULFILL.SSSS-CTZNSHP-CDE
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //* *DEFINE SET-ACCOUNT-NAME
        //*  ----------------------------------------------------------------------
        //* *DECIDE ON FIRST VALUE OF CWF-ATI-FULFILL.SSSS-ACCNT(#B)
        //*   VALUE 'T'
        //*     MOVE 'TIAA'       TO #INTERIM-ACCT-NAME
        //*   VALUE 'C'
        //*     MOVE 'STOCK'      TO #INTERIM-ACCT-NAME
        //*   VALUE 'M'
        //*     MOVE 'MMA'        TO #INTERIM-ACCT-NAME
        //*   VALUE 'W'
        //*     MOVE 'GLOBAL'     TO #INTERIM-ACCT-NAME
        //*   VALUE 'L'
        //*     MOVE 'GROWTH'     TO #INTERIM-ACCT-NAME
        //*   VALUE 'S'
        //*     MOVE 'SOCIAL'     TO #INTERIM-ACCT-NAME
        //*   VALUE 'E'
        //*     MOVE 'EQUITY'     TO #INTERIM-ACCT-NAME
        //*   VALUE 'R'
        //*     MOVE 'REAL ESTATE' TO #INTERIM-ACCT-NAME
        //*   VALUE 'B'
        //*     MOVE 'BOND'       TO #INTERIM-ACCT-NAME
        //*   VALUE 'I'
        //*     MOVE 'INDEX BOND' TO #INTERIM-ACCT-NAME
        //*   VALUE ' '
        //*     MOVE 'NO ACCOUNT' TO #INTERIM-ACCT-NAME
        //*   NONE
        //*     MOVE CWF-ATI-FULFILL.SSSS-TICKRE-SYMBOL(#B) TO #INTERIM-ACCT-NAME
        //* *END-DECIDE
        //* *END-SUBROUTINE
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT END OF PAGE ( 1 )
        //* ***********************************************************************
        //*   WRITE OUT ATI GRAND TOTALS ON LAST PAGE OF REPORT
        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Last_Page_Flag.setValue("Y");                                                                                                                                 //Natural: ASSIGN #LAST-PAGE-FLAG := 'Y'
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) //////// 1T '-' ( 131 ) // 1T '-' ( 50 ) 52T '   ATI (SSSS) GRAND TOTALS   ' 82T '-' ( 51 ) // 1T '-' ( 131 ) //// 45T 'OPEN ATIS        :' #GRAND-TOTAL-OPEN-ATIS / 45T 'IN-PROGRESS ATIS :' #GRAND-TOTAL-INPROGRESS-ATIS / 45T 'SUCCESSFULL ATIS :' #GRAND-TOTAL-SUCCESFULL-ATIS / 45T 'FAILED ATIS      :' #GRAND-TOTAL-FAILED-ATIS
            TabSetting(1),"-",new RepeatItem(50),new TabSetting(52),"   ATI (SSSS) GRAND TOTALS   ",new TabSetting(82),"-",new RepeatItem(51),NEWLINE,NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"OPEN ATIS        :",pnd_Grand_Total_Open_Atis,NEWLINE,new 
            TabSetting(45),"IN-PROGRESS ATIS :",pnd_Grand_Total_Inprogress_Atis,NEWLINE,new TabSetting(45),"SUCCESSFULL ATIS :",pnd_Grand_Total_Succesfull_Atis,NEWLINE,new 
            TabSetting(45),"FAILED ATIS      :",pnd_Grand_Total_Failed_Atis);
        if (Global.isEscape()) return;
    }
    //*  START ATI-WRITE-REC-1 PROCESSING
    private void sub_Ati_Write_Rec_1() throws Exception                                                                                                                   //Natural: ATI-WRITE-REC-1
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM SET-PROCESS-STATUS
        sub_Set_Process_Status();
        if (condition(Global.isEscape())) {return;}
        if (condition(getReports().getAstLinesLeft(1).less(15)))                                                                                                          //Natural: NEWPAGE ( 1 ) IF LESS THAN 15 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Compressed_Text.reset();                                                                                                                                      //Natural: RESET #COMPRESSED-TEXT
        //*  PIN-EXP
        //*   PIN-EXP>>
        pnd_Compressed_Text.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "ENTRY RACF-ID/UNIT= ", cwf_Ati_Fulfill_Ati_Entry_Racf_Id, "/", cwf_Ati_Fulfill_Ati_Entry_Unit_Cde)); //Natural: COMPRESS 'ENTRY RACF-ID/UNIT= ' CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID '/' CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE INTO #COMPRESSED-TEXT LEAVING NO SPACE
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),cwf_Ati_Fulfill_Ati_Pin,new TabSetting(15),cwf_Ati_Fulfill_Ati_Wpid,new TabSetting(22),cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme,  //Natural: WRITE ( 1 ) / 1T CWF-ATI-FULFILL.ATI-PIN 15T CWF-ATI-FULFILL.ATI-WPID 22T CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 43T CWF-ATI-FULFILL.ATI-STTS-DTE-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 63T #PROCESS-STATUS-LITERAL 73T CWF-ATI-FULFILL.SSSS-OVRNGHT-MAIL-IND 77T CWF-ATI-FULFILL.SSSS-CTZNSHP-CDE 82T CWF-ATI-FULFILL.SSSS-RSDNCE-CDE
            new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(43),cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new 
            TabSetting(63),pnd_Process_Status_Literal,new TabSetting(73),cwf_Ati_Fulfill_Ssss_Ovrnght_Mail_Ind,new TabSetting(77),cwf_Ati_Fulfill_Ssss_Ctznshp_Cde,new 
            TabSetting(82),cwf_Ati_Fulfill_Ssss_Rsdnce_Cde);
        if (Global.isEscape()) return;
        //*  87T CWF-ATI-FULFILL.SSSS-RSDNCE-CDE       /*  PIN-EXP<<
        //*  86T #COMPRESSED-TEXT
        if (condition(cwf_Ati_Fulfill_Ati_Prcss_Stts.equals(" ")))                                                                                                        //Natural: IF CWF-ATI-FULFILL.ATI-PRCSS-STTS = ' '
        {
            if (condition(cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))                                                                                                     //Natural: IF CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text);                                                                       //Natural: WRITE ( 1 ) 15T #COMPRESSED-TEXT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text,new TabSetting(55),"ERROR MESSAGE = :",new TabSetting(73),              //Natural: WRITE ( 1 ) 15T #COMPRESSED-TEXT 55T 'ERROR MESSAGE = :' 73T CWF-ATI-FULFILL.ATI-ERROR-TXT
                    cwf_Ati_Fulfill_Ati_Error_Txt);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text);                                                                           //Natural: WRITE ( 1 ) 15T #COMPRESSED-TEXT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*   END  ATI-WRITE-REC-1 PROCESSING
    }
    //*  START ATI-WRITE-REC-2 PROCESSING
    //*  (EM=MM/DD/YYYY)
    private void sub_Ati_Write_Rec_2() throws Exception                                                                                                                   //Natural: ATI-WRITE-REC-2
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,new TabSetting(15),"PLAN = ",new TabSetting(23),cwf_Ati_Fulfill_Ssss_Plan_Cde,new TabSetting(30),"CONTRACT NUMBER  = ",new  //Natural: WRITE ( 1 ) 15T 'PLAN = ' 23T CWF-ATI-FULFILL.SSSS-PLAN-CDE 30T 'CONTRACT NUMBER  = ' 51T CWF-ATI-FULFILL.SSSS-TIAA-NBR 88T CWF-ATI-FULFILL.SSSS-RLLVR-TYPE 93T CWF-ATI-FULFILL.SSSS-ERLY-TRM-TYPE 98T CWF-ATI-FULFILL.SSSS-WTH-ALL-FNDS-IND 101T CWF-ATI-FULFILL.SSSS-RPTTVE-FRQNCY ( EM = 999 ) 105T CWF-ATI-FULFILL.SSSS-RPTTVE-NBR-PYMNTS ( EM = 99999 ) 111T CWF-ATI-FULFILL.SSSS-RPTTVE-BGN-DTE 122T CWF-ATI-FULFILL.SSSS-RPTTVE-END-DTE
            TabSetting(51),cwf_Ati_Fulfill_Ssss_Tiaa_Nbr,new TabSetting(88),cwf_Ati_Fulfill_Ssss_Rllvr_Type,new TabSetting(93),cwf_Ati_Fulfill_Ssss_Erly_Trm_Type,new 
            TabSetting(98),cwf_Ati_Fulfill_Ssss_Wth_All_Fnds_Ind,new TabSetting(101),cwf_Ati_Fulfill_Ssss_Rpttve_Frqncy, new ReportEditMask ("999"),new 
            TabSetting(105),cwf_Ati_Fulfill_Ssss_Rpttve_Nbr_Pymnts, new ReportEditMask ("99999"),new TabSetting(111),cwf_Ati_Fulfill_Ssss_Rpttve_Bgn_Dte,new 
            TabSetting(122),cwf_Ati_Fulfill_Ssss_Rpttve_End_Dte);
        if (Global.isEscape()) return;
        F192:                                                                                                                                                             //Natural: FOR #B = 1 TO 20
        for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(20)); pnd_B.nadd(1))
        {
            if (condition(cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(pnd_B).equals(" ")))                                                                                //Natural: IF CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( #B ) = ' '
            {
                if (true) break F192;                                                                                                                                     //Natural: ESCAPE BOTTOM ( F192. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(pnd_B).notEquals(getZero())))                                                                     //Natural: IF CWF-ATI-FULFILL.SSSS-WTHDRWL-AMT ( #B ) NE 0
                {
                                                                                                                                                                          //Natural: PERFORM WRITE2-ACCT-DOLLAR
                    sub_Write2_Acct_Dollar();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F192"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F192"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(pnd_B).notEquals(getZero())))                                                               //Natural: IF CWF-ATI-FULFILL.SSSS-WTHDRWL-PRCNT ( #B ) NE 0
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE2-ACCT-PERCENT
                        sub_Write2_Acct_Percent();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("F192"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("F192"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(pnd_B).notEquals(getZero())))                                                             //Natural: IF CWF-ATI-FULFILL.SSSS-WTHDRWL-UNT ( #B ) NE 0
                        {
                                                                                                                                                                          //Natural: PERFORM WRITE2-ACCT-UNIT
                            sub_Write2_Acct_Unit();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("F192"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("F192"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*          PERFORM SET-ACCOUNT-NAME
                            pnd_Interim_Acct_Name.setValue(cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(pnd_B));                                                           //Natural: MOVE CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( #B ) TO #INTERIM-ACCT-NAME
                            getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"ACCOUNT=",new TabSetting(39),pnd_Interim_Acct_Name,new TabSetting(51),         //Natural: WRITE ( 1 ) 30T 'ACCOUNT=' 39T #INTERIM-ACCT-NAME 51T 'NO UNITS/DOLLARS/PRCNT ENTERED'
                                "NO UNITS/DOLLARS/PRCNT ENTERED");
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("F192"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("F192"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*   END  ATI-WRITE-REC-2 PROCESSING
    }
    private void sub_Set_Process_Status() throws Exception                                                                                                                //Natural: SET-PROCESS-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        short decideConditionsMet342 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CWF-ATI-FULFILL.ATI-PRCSS-STTS;//Natural: VALUE 'O'
        if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("O"))))
        {
            decideConditionsMet342++;
            pnd_Process_Status_Literal.setValue(" OPEN ");                                                                                                                //Natural: MOVE ' OPEN ' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Open_Atis.nadd(1);                                                                                                                            //Natural: ADD 1 TO #GRAND-TOTAL-OPEN-ATIS
            pnd_Input_Data_Pnd_W_Ati_Open.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                                     //Natural: ADD 1 TO #W-ATI-OPEN ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("I"))))
        {
            decideConditionsMet342++;
            pnd_Process_Status_Literal.setValue("INPROGS");                                                                                                               //Natural: MOVE 'INPROGS' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Inprogress_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-INPROGRESS-ATIS
            pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                              //Natural: ADD 1 TO #W-ATI-IN-PROGRESS ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE ' '
        else if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals(" "))))
        {
            decideConditionsMet342++;
            if (condition(cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))                                                                                                     //Natural: IF CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
            {
                pnd_Process_Status_Literal.setValue("SUCCESS");                                                                                                           //Natural: MOVE 'SUCCESS' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Succesfull_Atis.nadd(1);                                                                                                                  //Natural: ADD 1 TO #GRAND-TOTAL-SUCCESFULL-ATIS
                pnd_Input_Data_Pnd_W_Ati_Successful.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                           //Natural: ADD 1 TO #W-ATI-SUCCESSFUL ( #W-INX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Process_Status_Literal.setValue("FAILED");                                                                                                            //Natural: MOVE 'FAILED' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Failed_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-FAILED-ATIS
                pnd_Input_Data_Pnd_W_Ati_Manual.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                               //Natural: ADD 1 TO #W-ATI-MANUAL ( #W-INX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Process_Status_Literal.setValue("INCORCT");                                                                                                               //Natural: MOVE 'INCORCT' TO #PROCESS-STATUS-LITERAL
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Write2_Acct_Dollar() throws Exception                                                                                                                //Natural: WRITE2-ACCT-DOLLAR
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  PERFORM SET-ACCOUNT-NAME
        pnd_Interim_Acct_Name.setValue(cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(pnd_B));                                                                               //Natural: MOVE CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( #B ) TO #INTERIM-ACCT-NAME
        pnd_Interim_Dollar.setValue(cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(pnd_B));                                                                                    //Natural: MOVE CWF-ATI-FULFILL.SSSS-WTHDRWL-AMT ( #B ) TO #INTERIM-DOLLAR
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"ACCOUNT=",new TabSetting(39),pnd_Interim_Acct_Name,new TabSetting(51),"DOLLAR-AMT  =",new          //Natural: WRITE ( 1 ) 30T 'ACCOUNT=' 39T #INTERIM-ACCT-NAME 51T 'DOLLAR-AMT  =' 65T #INTERIM-DOLLAR
            TabSetting(65),pnd_Interim_Dollar);
        if (Global.isEscape()) return;
    }
    private void sub_Write2_Acct_Percent() throws Exception                                                                                                               //Natural: WRITE2-ACCT-PERCENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pnd_Interim_Prcnt.setValue(cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(pnd_B));                                                                                   //Natural: MOVE CWF-ATI-FULFILL.SSSS-WTHDRWL-PRCNT ( #B ) TO #INTERIM-PRCNT
        //*  PERFORM SET-ACCOUNT-NAME
        pnd_Interim_Acct_Name.setValue(cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(pnd_B));                                                                               //Natural: MOVE CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( #B ) TO #INTERIM-ACCT-NAME
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"ACCOUNT=",new TabSetting(39),pnd_Interim_Acct_Name,new TabSetting(51),"PERCENTAGE  =",new          //Natural: WRITE ( 1 ) 30T 'ACCOUNT=' 39T #INTERIM-ACCT-NAME 51T 'PERCENTAGE  =' 65T #INTERIM-PRCNT
            TabSetting(65),pnd_Interim_Prcnt);
        if (Global.isEscape()) return;
    }
    private void sub_Write2_Acct_Unit() throws Exception                                                                                                                  //Natural: WRITE2-ACCT-UNIT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pnd_Interim_Units.setValue(cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(pnd_B));                                                                                     //Natural: MOVE CWF-ATI-FULFILL.SSSS-WTHDRWL-UNT ( #B ) TO #INTERIM-UNITS
        //*  PERFORM SET-ACCOUNT-NAME
        pnd_Interim_Acct_Name.setValue(cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(pnd_B));                                                                               //Natural: MOVE CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( #B ) TO #INTERIM-ACCT-NAME
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"ACCOUNT=",new TabSetting(39),pnd_Interim_Acct_Name,new TabSetting(51),"UNITS       =",new          //Natural: WRITE ( 1 ) 30T 'ACCOUNT=' 39T #INTERIM-ACCT-NAME 51T 'UNITS       =' 65T #INTERIM-UNITS
            TabSetting(65),pnd_Interim_Units);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(50),"AUTOMATED TRANSACTION SYSTEM",new                //Natural: WRITE ( 1 ) NOTITLE 001T *PROGRAM 050T 'AUTOMATED TRANSACTION SYSTEM' 101T *DATU 110T *TIME ( AL = 005 ) 121T 'PAGE:' 127T *PAGE-NUMBER ( 1 ) ( AD = L ) / 035T 'ATI DETAILED REPORT FOR SSSS FORMS IN WPID SEQUENCE' / 035T #TITLE-LITERAL / 001T '-' ( 131 ) / 6T 'PIN' 31T 'MASTER INDEX' 53T 'ATI STATUS' 68T 'PROCESS' 76T 'MAIL' 81T 'CITZ' 86T 'RSDN' 91T 'ROLL' 96T 'ERLY' 101T 'WTHD' 106T 'RPT' 110T 'RPT #' 116T 'REPETITIVE' / 4T 'NUMBER' 21T 'WPID' 29T 'LOG DATE & TIME' 52T 'DATE & TIME' 68T 'STATUS' 76T 'CODE' 81T 'CODE' 86T 'CODE' 91T 'CODE' 96T 'TERM' 101T 'ALL' 106T 'FRQ' 111T 'PYMTS' 117T 'BGN-DATE' / 001T '-' ( 131 ) /
                        TabSetting(101),Global.getDATU(),new TabSetting(110),Global.getTIME(), new AlphanumericLength (5),new TabSetting(121),"PAGE:",new 
                        TabSetting(127),getReports().getPageNumberDbs(1), new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(35),"ATI DETAILED REPORT FOR SSSS FORMS IN WPID SEQUENCE",NEWLINE,new 
                        TabSetting(35),pnd_Title_Literal,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(6),"PIN",new TabSetting(31),"MASTER INDEX",new 
                        TabSetting(53),"ATI STATUS",new TabSetting(68),"PROCESS",new TabSetting(76),"MAIL",new TabSetting(81),"CITZ",new TabSetting(86),"RSDN",new 
                        TabSetting(91),"ROLL",new TabSetting(96),"ERLY",new TabSetting(101),"WTHD",new TabSetting(106),"RPT",new TabSetting(110),"RPT #",new 
                        TabSetting(116),"REPETITIVE",NEWLINE,new TabSetting(4),"NUMBER",new TabSetting(21),"WPID",new TabSetting(29),"LOG DATE & TIME",new 
                        TabSetting(52),"DATE & TIME",new TabSetting(68),"STATUS",new TabSetting(76),"CODE",new TabSetting(81),"CODE",new TabSetting(86),"CODE",new 
                        TabSetting(91),"CODE",new TabSetting(96),"TERM",new TabSetting(101),"ALL",new TabSetting(106),"FRQ",new TabSetting(111),"PYMTS",new 
                        TabSetting(117),"BGN-DATE",NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE);
                    //*    26T 'MASTER INDEX'
                    //*    48T 'ATI STATUS'
                    //*    63T 'PROCESS'
                    //*    71T 'MAIL'
                    //*    76T 'CITZ'
                    //*    81T 'RSDN'
                    //*    86T 'ROLL'
                    //*    91T 'ERLY'
                    //*    96T 'WTHD'
                    //*    101T 'RPT'
                    //*    105T 'RPT #'
                    //*    111T 'REPETITIVE'
                    //*    122T 'REPETITIVE'
                    //*    16T 'WPID'
                    //*    24T 'LOG DATE & TIME'
                    //*    47T 'DATE & TIME'
                    //*    63T 'STATUS'
                    //*    71T 'CODE'
                    //*    76T 'CODE'
                    //*    81T 'CODE'
                    //*    86T 'CODE'
                    //*    91T 'TERM'
                    //*    96T 'ALL'
                    //*    101T 'FRQ'
                    //*    105T 'PYMTS'
                    //*    112T 'BGN-DATE'
                    //*    123T 'END-DATE'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atEndEventRpt1 = (Object sender, EventArgs e) ->
    {
        String localMethod = "MCSN3801|atEndEventRpt1";
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Last_Page_Flag.equals("Y")))                                                                                                        //Natural: IF #LAST-PAGE-FLAG = 'Y'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(1),"-",new                    //Natural: WRITE ( 1 ) / 1T '-' ( 131 ) / 1T '-' ( 131 )
                            RepeatItem(131));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-ENDPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Ati_Fulfill_Ati_WpidIsBreak = cwf_Ati_Fulfill_Ati_Wpid.isBreak(endOfData);
        if (condition(cwf_Ati_Fulfill_Ati_WpidIsBreak))
        {
            getReports().skip(1, 2);                                                                                                                                      //Natural: SKIP ( 1 ) 2
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TOTAL FOR WPID :",pnd_Prev_Wpid, new ReportEditMask ("XX' 'X' 'XX' 'X"),":",                    //Natural: WRITE ( 1 ) 1T 'TOTAL FOR WPID :' #PREV-WPID ( EM = XX' 'X' 'XX' 'X ) ':' #WPID-TOTAL
                pnd_Wpid_Total);
            if (condition(Global.isEscape())) return;
            pnd_Wpid_Total.reset();                                                                                                                                       //Natural: RESET #WPID-TOTAL
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=OFF");
        Global.format(1, "PS=60 LS=133 ZP=OFF");
        Global.format(2, "PS=60 LS=133 ZP=OFF");
    }
}
