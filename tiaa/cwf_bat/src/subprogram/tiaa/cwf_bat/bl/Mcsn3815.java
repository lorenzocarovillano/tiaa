/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:25:56 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3815
************************************************************
**        * FILE NAME            : Mcsn3815.java
**        * CLASS NAME           : Mcsn3815
**        * INSTANCE NAME        : Mcsn3815
************************************************************
************************************************************************
* PROGRAM  : MCSN3815
* TITLE    : PRINT "Successful 'CWF to AWD PUSH' Report for mm/dd/yyyy"
*            ON REPORT 08
* FUNCTION : SELECT CWF-ATI-FULFILL RECORDS BY ATI-COMPLETED-KEY WITH
*            ATI-PRGE-IND = 'Y'
* KB 06/98 : MADE MCSN3815 INTO A SUBPROGRAM TO PASS TOTALS
*            ADDED LINES OF CODE TO ADD TO #ATI-TOTAL-TABLE
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3815 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;

    private DataAccessProgramView vw_cwf_Ati;
    private DbsField cwf_Ati_Ati_Rcrd_Type;

    private DbsGroup cwf_Ati_Ati_Rqst_Id;
    private DbsField cwf_Ati_Ati_Pin;
    private DbsField cwf_Ati_Ati_Mit_Log_Dt_Tme;
    private DbsField cwf_Ati_Ati_Prcss_Stts;
    private DbsField cwf_Ati_Ati_Wpid;
    private DbsField cwf_Ati_Ati_Entry_Dte_Tme;
    private DbsField cwf_Ati_Ati_Entry_Systm_Or_Unt;
    private DbsField cwf_Ati_Ati_Prcss_Applctn_Id;
    private DbsField cwf_Ati_Ati_Error_Txt;

    private DbsGroup cwf_Ati__R_Field_1;
    private DbsField cwf_Ati_Ati_Error_Txt_1;
    private DbsField cwf_Ati_Ati_Error_Txt_26;
    private DbsField cwf_Ati_Ati_Error_Txt_8;
    private DbsField cwf_Ati_Ati_Prge_Ind;
    private DbsField cwf_Ati_Ati_Stts_Dte_Tme;
    private DbsField cwf_Ati_Ati_Entry_Racf_Id;
    private DbsField cwf_Ati_Ati_Entry_Unit_Cde;
    private DbsField cwf_Ati_Ati_Cmtask_Ind;
    private DbsField cwf_Ati_Awd_Action_Cde;
    private DbsField cwf_Ati_Awd_Participant_Status_Ind;
    private DbsField cwf_Ati_Awd_Document_Case_Id;
    private DbsField cwf_Ati_Awd_New_Ssn;
    private DbsField cwf_Ati_Awd_New_Insrnce_Prcss_Ind;
    private DbsField cwf_Ati_Awd_New_Lob;
    private DbsField cwf_Ati_Awd_Orig_Pin;
    private DbsField cwf_Ati_Awd_Orig_Wpid;
    private DbsField cwf_Ati_Awd_Orig_Ssn;
    private DbsField cwf_Ati_Awd_Orig_Insrnce_Prcss_Ind;
    private DbsField cwf_Ati_Awd_Orig_Lob;
    private DbsField pnd_Ati_Completed_Key;

    private DbsGroup pnd_Ati_Completed_Key__R_Field_2;
    private DbsField pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type;
    private DbsField pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme;
    private DbsField pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind;
    private DbsField pnd_An;
    private DbsField pnd_Su;
    private DbsField pnd_Ad;
    private DbsField pnd_Wu;
    private DbsField pnd_Ap;
    private DbsField pnd_None;
    private DbsField pnd_Total;
    private DbsField pnd_File_Date;
    private DbsField pnd_Awd_Case_Id;
    private DbsField pnd_Input_Dte_Tme;
    private DbsField pnd_Input_Dte_D;
    private DbsField pnd_Entry_Date;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Input_Data = parameters.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Ati = new DataAccessProgramView(new NameInfo("vw_cwf_Ati", "CWF-ATI"), "CWF_ATI_FULFILL", "CWF_KDO_FULFILL");
        cwf_Ati_Ati_Rcrd_Type = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Ati_Rcrd_Type", "ATI-RCRD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ATI_RCRD_TYPE");

        cwf_Ati_Ati_Rqst_Id = vw_cwf_Ati.getRecord().newGroupInGroup("CWF_ATI_ATI_RQST_ID", "ATI-RQST-ID");
        cwf_Ati_Ati_Pin = cwf_Ati_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Ati_Pin", "ATI-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "ATI_PIN");
        cwf_Ati_Ati_Mit_Log_Dt_Tme = cwf_Ati_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Ati_Mit_Log_Dt_Tme", "ATI-MIT-LOG-DT-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ATI_MIT_LOG_DT_TME");
        cwf_Ati_Ati_Prcss_Stts = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Ati_Prcss_Stts", "ATI-PRCSS-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ATI_PRCSS_STTS");
        cwf_Ati_Ati_Wpid = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Ati_Wpid", "ATI-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, "ATI_WPID");
        cwf_Ati_Ati_Entry_Dte_Tme = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Ati_Entry_Dte_Tme", "ATI-ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ATI_ENTRY_DTE_TME");
        cwf_Ati_Ati_Entry_Systm_Or_Unt = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Ati_Entry_Systm_Or_Unt", "ATI-ENTRY-SYSTM-OR-UNT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ATI_ENTRY_SYSTM_OR_UNT");
        cwf_Ati_Ati_Prcss_Applctn_Id = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Ati_Prcss_Applctn_Id", "ATI-PRCSS-APPLCTN-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ATI_PRCSS_APPLCTN_ID");
        cwf_Ati_Ati_Error_Txt = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Ati_Error_Txt", "ATI-ERROR-TXT", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "ATI_ERROR_TXT");

        cwf_Ati__R_Field_1 = vw_cwf_Ati.getRecord().newGroupInGroup("cwf_Ati__R_Field_1", "REDEFINE", cwf_Ati_Ati_Error_Txt);
        cwf_Ati_Ati_Error_Txt_1 = cwf_Ati__R_Field_1.newFieldInGroup("cwf_Ati_Ati_Error_Txt_1", "ATI-ERROR-TXT-1", FieldType.STRING, 1);
        cwf_Ati_Ati_Error_Txt_26 = cwf_Ati__R_Field_1.newFieldInGroup("cwf_Ati_Ati_Error_Txt_26", "ATI-ERROR-TXT-26", FieldType.STRING, 26);
        cwf_Ati_Ati_Error_Txt_8 = cwf_Ati__R_Field_1.newFieldInGroup("cwf_Ati_Ati_Error_Txt_8", "ATI-ERROR-TXT-8", FieldType.STRING, 8);
        cwf_Ati_Ati_Prge_Ind = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Ati_Prge_Ind", "ATI-PRGE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ATI_PRGE_IND");
        cwf_Ati_Ati_Stts_Dte_Tme = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Ati_Stts_Dte_Tme", "ATI-STTS-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ATI_STTS_DTE_TME");
        cwf_Ati_Ati_Entry_Racf_Id = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Ati_Entry_Racf_Id", "ATI-ENTRY-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ATI_ENTRY_RACF_ID");
        cwf_Ati_Ati_Entry_Unit_Cde = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Ati_Entry_Unit_Cde", "ATI-ENTRY-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ATI_ENTRY_UNIT_CDE");
        cwf_Ati_Ati_Cmtask_Ind = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Ati_Cmtask_Ind", "ATI-CMTASK-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ATI_CMTASK_IND");
        cwf_Ati_Awd_Action_Cde = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Awd_Action_Cde", "AWD-ACTION-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AWD_ACTION_CDE");
        cwf_Ati_Awd_Action_Cde.setDdmHeader("AWD ACTION/CODE");
        cwf_Ati_Awd_Participant_Status_Ind = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Awd_Participant_Status_Ind", "AWD-PARTICIPANT-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AWD_PARTICIPANT_STATUS_IND");
        cwf_Ati_Awd_Participant_Status_Ind.setDdmHeader("AWD/STATUS");
        cwf_Ati_Awd_Document_Case_Id = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Awd_Document_Case_Id", "AWD-DOCUMENT-CASE-ID", FieldType.STRING, 
            27, RepeatingFieldStrategy.None, "AWD_DOCUMENT_CASE_ID");
        cwf_Ati_Awd_Document_Case_Id.setDdmHeader("AWD/CASE-ID");
        cwf_Ati_Awd_New_Ssn = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Awd_New_Ssn", "AWD-NEW-SSN", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "AWD_NEW_SSN");
        cwf_Ati_Awd_New_Ssn.setDdmHeader("SSN");
        cwf_Ati_Awd_New_Insrnce_Prcss_Ind = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Awd_New_Insrnce_Prcss_Ind", "AWD-NEW-INSRNCE-PRCSS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AWD_NEW_INSRNCE_PRCSS_IND");
        cwf_Ati_Awd_New_Insrnce_Prcss_Ind.setDdmHeader("CURRENT/INSURANCE-IND");
        cwf_Ati_Awd_New_Lob = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Awd_New_Lob", "AWD-NEW-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AWD_NEW_LOB");
        cwf_Ati_Awd_New_Lob.setDdmHeader("CURRENT/LOB");
        cwf_Ati_Awd_Orig_Pin = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Awd_Orig_Pin", "AWD-ORIG-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "AWD_ORIG_PIN");
        cwf_Ati_Awd_Orig_Pin.setDdmHeader("OLD PIN");
        cwf_Ati_Awd_Orig_Wpid = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Awd_Orig_Wpid", "AWD-ORIG-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AWD_ORIG_WPID");
        cwf_Ati_Awd_Orig_Wpid.setDdmHeader("OLD/WPID");
        cwf_Ati_Awd_Orig_Ssn = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Awd_Orig_Ssn", "AWD-ORIG-SSN", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "AWD_ORIG_SSN");
        cwf_Ati_Awd_Orig_Ssn.setDdmHeader("OLD/SSN");
        cwf_Ati_Awd_Orig_Insrnce_Prcss_Ind = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Awd_Orig_Insrnce_Prcss_Ind", "AWD-ORIG-INSRNCE-PRCSS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AWD_ORIG_INSRNCE_PRCSS_IND");
        cwf_Ati_Awd_Orig_Insrnce_Prcss_Ind.setDdmHeader("OLD INSURANCE/PROCESS IND");
        cwf_Ati_Awd_Orig_Lob = vw_cwf_Ati.getRecord().newFieldInGroup("cwf_Ati_Awd_Orig_Lob", "AWD-ORIG-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AWD_ORIG_LOB");
        cwf_Ati_Awd_Orig_Lob.setDdmHeader("OLD LOB");
        registerRecord(vw_cwf_Ati);

        pnd_Ati_Completed_Key = localVariables.newFieldInRecord("pnd_Ati_Completed_Key", "#ATI-COMPLETED-KEY", FieldType.STRING, 9);

        pnd_Ati_Completed_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Ati_Completed_Key__R_Field_2", "REDEFINE", pnd_Ati_Completed_Key);
        pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type = pnd_Ati_Completed_Key__R_Field_2.newFieldInGroup("pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type", "#ATI-RCRD-TYPE", 
            FieldType.STRING, 1);
        pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme = pnd_Ati_Completed_Key__R_Field_2.newFieldInGroup("pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme", 
            "#ATI-ENTRY-DTE-TME", FieldType.TIME);
        pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind = pnd_Ati_Completed_Key__R_Field_2.newFieldInGroup("pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind", "#ATI-PRGE-IND", 
            FieldType.STRING, 1);
        pnd_An = localVariables.newFieldInRecord("pnd_An", "#AN", FieldType.NUMERIC, 5);
        pnd_Su = localVariables.newFieldInRecord("pnd_Su", "#SU", FieldType.NUMERIC, 5);
        pnd_Ad = localVariables.newFieldInRecord("pnd_Ad", "#AD", FieldType.NUMERIC, 5);
        pnd_Wu = localVariables.newFieldInRecord("pnd_Wu", "#WU", FieldType.NUMERIC, 5);
        pnd_Ap = localVariables.newFieldInRecord("pnd_Ap", "#AP", FieldType.NUMERIC, 5);
        pnd_None = localVariables.newFieldInRecord("pnd_None", "#NONE", FieldType.NUMERIC, 5);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.NUMERIC, 5);
        pnd_File_Date = localVariables.newFieldInRecord("pnd_File_Date", "#FILE-DATE", FieldType.STRING, 8);
        pnd_Awd_Case_Id = localVariables.newFieldInRecord("pnd_Awd_Case_Id", "#AWD-CASE-ID", FieldType.STRING, 27);
        pnd_Input_Dte_Tme = localVariables.newFieldInRecord("pnd_Input_Dte_Tme", "#INPUT-DTE-TME", FieldType.TIME);
        pnd_Input_Dte_D = localVariables.newFieldInRecord("pnd_Input_Dte_D", "#INPUT-DTE-D", FieldType.DATE);
        pnd_Entry_Date = localVariables.newFieldInRecord("pnd_Entry_Date", "#ENTRY-DATE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Ati.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Ati_Completed_Key.setInitialValue("3       Y");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Mcsn3815() throws Exception
    {
        super("Mcsn3815");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt7, 7);
        setupReports();
        //*  =========================================================
        //*  ------------------------------------                                                                                                                         //Natural: FORMAT ( 7 ) PS = 60 LS = 133 ZP = OFF
        if (condition(pnd_Input_Data_Pnd_Input_Date.notEquals(" ")))                                                                                                      //Natural: IF #INPUT-DATE NE ' '
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Input_Dte_Tme.setValue(pnd_Input_Dte_D);                                                                                                                  //Natural: MOVE #INPUT-DTE-D TO #INPUT-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(7, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***");                                     //Natural: WRITE ( 7 ) /// '*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***'
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------------------------------
        //*  ------------------------------------                                                                                                                         //Natural: AT TOP OF PAGE ( 7 )
        pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type.setValue("3");                                                                                                            //Natural: ASSIGN #ATI-RCRD-TYPE := '3'
        pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme.reset();                                                                                                              //Natural: RESET #ATI-ENTRY-DTE-TME
        getReports().newPage(new ReportSpecification(7));                                                                                                                 //Natural: NEWPAGE ( 7 )
        if (condition(Global.isEscape())){return;}
        vw_cwf_Ati.startDatabaseRead                                                                                                                                      //Natural: READ CWF-ATI BY ATI-COMPLETED-KEY STARTING FROM #ATI-COMPLETED-KEY
        (
        "READ01",
        new Wc[] { new Wc("ATI_COMPLETED_KEY", ">=", pnd_Ati_Completed_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("ATI_COMPLETED_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Ati.readNextRow("READ01")))
        {
            //*    MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D (EM=YYYYMMDD)
            pnd_Entry_Date.setValue(cwf_Ati_Ati_Stts_Dte_Tme);                                                                                                            //Natural: MOVE ATI-STTS-DTE-TME TO #ENTRY-DATE
            if (condition(!(pnd_Entry_Date.equals(pnd_Input_Dte_D) && cwf_Ati_Ati_Prcss_Applctn_Id.equals("AWDP ") && cwf_Ati_Ati_Error_Txt_1.equals(" "))))              //Natural: ACCEPT IF #ENTRY-DATE = #INPUT-DTE-D AND ATI-PRCSS-APPLCTN-ID = 'AWDP ' AND CWF-ATI.ATI-ERROR-TXT-1 = ' '
            {
                continue;
            }
            getSort().writeSortInData(cwf_Ati_Ati_Stts_Dte_Tme, cwf_Ati_Awd_Document_Case_Id, cwf_Ati_Awd_New_Ssn, cwf_Ati_Awd_Orig_Ssn, cwf_Ati_Ati_Wpid,                //Natural: END-ALL
                cwf_Ati_Awd_Orig_Wpid, cwf_Ati_Awd_Action_Cde, cwf_Ati_Awd_Participant_Status_Ind, cwf_Ati_Ati_Mit_Log_Dt_Tme, cwf_Ati_Ati_Entry_Racf_Id, 
                cwf_Ati_Ati_Entry_Unit_Cde, cwf_Ati_Awd_New_Insrnce_Prcss_Ind, cwf_Ati_Awd_Orig_Insrnce_Prcss_Ind, cwf_Ati_Awd_New_Lob, cwf_Ati_Awd_Orig_Lob, 
                cwf_Ati_Ati_Error_Txt);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ---------------------------------------
        //*  STATUS-DTE-TME
        //*   (A22)
        //*   (N9)
        //*   (N9)
        //*   (A6)
        //*   (A6)
        //*   (A2)
        //*   (A1)
        //*   (T)
        //*   (A8)
        //*   (A8)
        //*   (A1)
        //*   (A1)
        //*   (A1)
        //*   (A1)
        //*   TEMPORARY!!!!
        getSort().sortData(cwf_Ati_Ati_Stts_Dte_Tme);                                                                                                                     //Natural: SORT RECORDS BY CWF-ATI.ATI-STTS-DTE-TME USING AWD-DOCUMENT-CASE-ID AWD-NEW-SSN AWD-ORIG-SSN ATI-WPID AWD-ORIG-WPID AWD-ACTION-CDE AWD-PARTICIPANT-STATUS-IND ATI-MIT-LOG-DT-TME ATI-ENTRY-RACF-ID ATI-ENTRY-UNIT-CDE AWD-NEW-INSRNCE-PRCSS-IND AWD-ORIG-INSRNCE-PRCSS-IND AWD-NEW-LOB AWD-ORIG-LOB ATI-ERROR-TXT
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Ati_Ati_Stts_Dte_Tme, cwf_Ati_Awd_Document_Case_Id, cwf_Ati_Awd_New_Ssn, cwf_Ati_Awd_Orig_Ssn, 
            cwf_Ati_Ati_Wpid, cwf_Ati_Awd_Orig_Wpid, cwf_Ati_Awd_Action_Cde, cwf_Ati_Awd_Participant_Status_Ind, cwf_Ati_Ati_Mit_Log_Dt_Tme, cwf_Ati_Ati_Entry_Racf_Id, 
            cwf_Ati_Ati_Entry_Unit_Cde, cwf_Ati_Awd_New_Insrnce_Prcss_Ind, cwf_Ati_Awd_Orig_Insrnce_Prcss_Ind, cwf_Ati_Awd_New_Lob, cwf_Ati_Awd_Orig_Lob, 
            cwf_Ati_Ati_Error_Txt)))
        {
            //*    ATI-STTS-DTE-TME
            //*  ----------------------------
            short decideConditionsMet159 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF AWD-ACTION-CDE;//Natural: VALUE 'AN'
            if (condition((cwf_Ati_Awd_Action_Cde.equals("AN"))))
            {
                decideConditionsMet159++;
                pnd_An.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #AN
            }                                                                                                                                                             //Natural: VALUE 'SU'
            else if (condition((cwf_Ati_Awd_Action_Cde.equals("SU"))))
            {
                decideConditionsMet159++;
                pnd_Su.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #SU
            }                                                                                                                                                             //Natural: VALUE 'AD'
            else if (condition((cwf_Ati_Awd_Action_Cde.equals("AD"))))
            {
                decideConditionsMet159++;
                pnd_Ad.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #AD
            }                                                                                                                                                             //Natural: VALUE 'WU'
            else if (condition((cwf_Ati_Awd_Action_Cde.equals("WU"))))
            {
                decideConditionsMet159++;
                pnd_Wu.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #WU
            }                                                                                                                                                             //Natural: VALUE 'AP'
            else if (condition((cwf_Ati_Awd_Action_Cde.equals("AP"))))
            {
                decideConditionsMet159++;
                pnd_Ap.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #AP
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_None.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #NONE
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(cwf_Ati_Awd_Document_Case_Id.equals(" ")))                                                                                                      //Natural: IF AWD-DOCUMENT-CASE-ID = ' '
            {
                pnd_Awd_Case_Id.reset();                                                                                                                                  //Natural: RESET #AWD-CASE-ID
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    MOVE EDITED AWD-DOCUMENT-CASE-ID (EM=XXXXXXX-XXXXXXXXXXXXXX)
                //*   PIN-EXP
                pnd_Awd_Case_Id.setValueEdited(cwf_Ati_Awd_Document_Case_Id,new ReportEditMask("XXXXXXXXXXXX-XXXXXXXXXXXXXX"));                                           //Natural: MOVE EDITED AWD-DOCUMENT-CASE-ID ( EM = XXXXXXXXXXXX-XXXXXXXXXXXXXX ) TO #AWD-CASE-ID
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Ati_Awd_Action_Cde.equals("AP")))                                                                                                           //Natural: IF AWD-ACTION-CDE = 'AP'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Input_Data_Pnd_W_Ati_Push_Successful.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                      //Natural: ADD 1 TO #W-ATI-PUSH-SUCCESSFUL ( #W-INX )
                //*  TEMPORARY!!!!!
            }                                                                                                                                                             //Natural: END-IF
            if (condition(getReports().getAstLinesLeft(7).less(10)))                                                                                                      //Natural: NEWPAGE ( 7 ) IF LESS THAN 10 LINES LEFT
            {
                getReports().newPage(7);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }
            getReports().write(7, ReportOption.NOTITLE,NEWLINE,NEWLINE,pnd_Awd_Case_Id,new TabSetting(30),cwf_Ati_Awd_New_Ssn, new ReportEditMask ("999-99-9999"),new     //Natural: WRITE ( 7 ) // #AWD-CASE-ID 30T AWD-NEW-SSN ( EM = 999-99-9999 ) 43T ATI-WPID 51T AWD-ACTION-CDE 57T AWD-PARTICIPANT-STATUS-IND 63T ATI-MIT-LOG-DT-TME ( EM = MM/DD/YY' 'HH:II:SS ) 82T ATI-STTS-DTE-TME ( EM = MM/DD/YY' 'HH:II:SS ) 101T ATI-ENTRY-RACF-ID 111T ATI-ENTRY-UNIT-CDE 130T AWD-NEW-INSRNCE-PRCSS-IND 132T AWD-NEW-LOB
                TabSetting(43),cwf_Ati_Ati_Wpid,new TabSetting(51),cwf_Ati_Awd_Action_Cde,new TabSetting(57),cwf_Ati_Awd_Participant_Status_Ind,new TabSetting(63),cwf_Ati_Ati_Mit_Log_Dt_Tme, 
                new ReportEditMask ("MM/DD/YY' 'HH:II:SS"),new TabSetting(82),cwf_Ati_Ati_Stts_Dte_Tme, new ReportEditMask ("MM/DD/YY' 'HH:II:SS"),new TabSetting(101),cwf_Ati_Ati_Entry_Racf_Id,new 
                TabSetting(111),cwf_Ati_Ati_Entry_Unit_Cde,new TabSetting(130),cwf_Ati_Awd_New_Insrnce_Prcss_Ind,new TabSetting(132),cwf_Ati_Awd_New_Lob);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  IF (AWD-NEW-SSN NOT = AWD-ORIG-SSN   AND  AWD-ORIG-SSN  NE 0)  OR
            //*    (ATI-PIN      NOT = AWD-ORIG-PIN   AND  AWD-ORIG-PIN  NE 0)  OR
            //*    (ATI-WPID     NOT = AWD-ORIG-WPID  AND  AWD-ORIG-WPID NE ' ')
            getReports().write(7, ReportOption.NOTITLE,new TabSetting(15),"  Original:",new TabSetting(30),cwf_Ati_Awd_Orig_Ssn, new ReportEditMask ("999-99-9999"),new   //Natural: WRITE ( 7 ) 15T '  Original:' 30T AWD-ORIG-SSN ( EM = 999-99-9999 ) 43T AWD-ORIG-WPID 55T ATI-ERROR-TXT 130T AWD-ORIG-INSRNCE-PRCSS-IND 132T AWD-ORIG-LOB
                TabSetting(43),cwf_Ati_Awd_Orig_Wpid,new TabSetting(55),cwf_Ati_Ati_Error_Txt,new TabSetting(130),cwf_Ati_Awd_Orig_Insrnce_Prcss_Ind,new 
                TabSetting(132),cwf_Ati_Awd_Orig_Lob);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  END-IF
            getReports().write(7, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 7 ) ' '
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        //*  ----------------------------------------------------------------------
        if (condition(getReports().getAstLinesLeft(7).less(12)))                                                                                                          //Natural: NEWPAGE ( 7 ) IF LESS THAN 12 LINES LEFT
        {
            getReports().newPage(7);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Total.compute(new ComputeParameters(false, pnd_Total), pnd_An.add(pnd_Su).add(pnd_Ad).add(pnd_Wu).add(pnd_None));                                             //Natural: COMPUTE #TOTAL = #AN + #SU + #AD + #WU + #NONE
        getReports().write(7, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(30),"Successful 'CWF to AWD Push' Download Summary",NEWLINE,new                 //Natural: WRITE ( 7 ) /// 30T 'Successful "CWF to AWD Push" Download Summary' / 30T '-' ( 051 ) / 30T 'AN - Log & Index Requests to AWD:' 75T #AN / 30T 'SU - Status Updates to AWD:' 75T #SU / 30T 'AD - Death Notification Log & Index & Push:' 75T #AD / 30T 'WU - WPID/PIN Updates to AWD:' 75T #WU / 30T 'AP - Death Notification Log&Index  NO Push:' 75T #AP / 30T 'Unknown' 75T #NONE / 30T '-' ( 051 ) / 30T 'Total Successfully Downloaded to AWD:' 75T #TOTAL
            TabSetting(30),"-",new RepeatItem(51),NEWLINE,new TabSetting(30),"AN - Log & Index Requests to AWD:",new TabSetting(75),pnd_An,NEWLINE,new TabSetting(30),"SU - Status Updates to AWD:",new 
            TabSetting(75),pnd_Su,NEWLINE,new TabSetting(30),"AD - Death Notification Log & Index & Push:",new TabSetting(75),pnd_Ad,NEWLINE,new TabSetting(30),"WU - WPID/PIN Updates to AWD:",new 
            TabSetting(75),pnd_Wu,NEWLINE,new TabSetting(30),"AP - Death Notification Log&Index  NO Push:",new TabSetting(75),pnd_Ap,NEWLINE,new TabSetting(30),"Unknown",new 
            TabSetting(75),pnd_None,NEWLINE,new TabSetting(30),"-",new RepeatItem(51),NEWLINE,new TabSetting(30),"Total Successfully Downloaded to AWD:",new 
            TabSetting(75),pnd_Total);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt7 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(7, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(42),"Corporate Workflow System",new                   //Natural: WRITE ( 7 ) NOTITLE 001T *PROGRAM 042T 'Corporate Workflow System' 101T *DATU *TIME ( AL = 005 ) '    PAGE:' *PAGE-NUMBER ( 7 ) ( AD = L ) / 030T 'Successful "CWF to AWD PUSH" Requests for' #INPUT-DTE-D ( EM = MM/DD/YY ) / 001T '-' ( 132 ) / 49T ' -- AWD --           MIT                ATI' 101T '                             I L' / 49T ' Act   Part        Logged             Download' 101T '--- Employee ----            P O' / 3T 'Document Case Id              SSN       WPID' 49T ' Code  Stat      Date & Time        Date & Time' 101T 'RACF Id   Unit               I B' / 001T '-' ( 132 )
                        TabSetting(101),Global.getDATU(),Global.getTIME(), new AlphanumericLength (5),"    PAGE:",getReports().getPageNumberDbs(7), new 
                        FieldAttributes ("AD=L"),NEWLINE,new TabSetting(30),"Successful 'CWF to AWD PUSH' Requests for",pnd_Input_Dte_D, new ReportEditMask 
                        ("MM/DD/YY"),NEWLINE,new TabSetting(1),"-",new RepeatItem(132),NEWLINE,new TabSetting(49)," -- AWD --           MIT                ATI",new 
                        TabSetting(101),"                             I L",NEWLINE,new TabSetting(49)," Act   Part        Logged             Download",new 
                        TabSetting(101),"--- Employee ----            P O",NEWLINE,new TabSetting(3),"Document Case Id              SSN       WPID",new 
                        TabSetting(49)," Code  Stat      Date & Time        Date & Time",new TabSetting(101),"RACF Id   Unit               I B",NEWLINE,new 
                        TabSetting(1),"-",new RepeatItem(132));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(7, "PS=60 LS=133 ZP=OFF");
    }
}
