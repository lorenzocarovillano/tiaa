/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:26:29 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3830
************************************************************
**        * FILE NAME            : Mcsn3830.java
**        * CLASS NAME           : Mcsn3830
**        * INSTANCE NAME        : Mcsn3830
************************************************************
************************************************************************
* PROGRAM  : MCSN3830
* TITLE    : ACCESS FULFILLMENT RECORDS FROM THE CWF-ATI-FULFILLMENT
* FUNCTION : DISPLAY ATI-FULFILLMENT RECORDS ON 192 IN WPID ORDER
*          : FOR QSUM SYSTEM "NOT PROCESSED"
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3830 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;

    private DataAccessProgramView vw_cwf_Ati_Fulfill_Qsum;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Rcrd_Type;

    private DbsGroup cwf_Ati_Fulfill_Qsum_Ati_Rqst_Id;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Pin;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Mit_Log_Dt_Tme;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Prcss_Stts;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Wpid;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Entry_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Entry_Systm_Or_Unt;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Prcss_Applctn_Id;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Error_Txt;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Prge_Ind;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Entry_Racf_Id;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Entry_Unit_Cde;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Cmtask_Ind;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Stts_Dte_Tme;
    private DbsGroup cwf_Ati_Fulfill_Qsum_Ati_Var_DataMuGroup;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Var_Data;
    private DbsField cwf_Ati_Fulfill_Qsum_Ati_Npir_Id;
    private DbsField pnd_Ati_Mit_Key;

    private DbsGroup pnd_Ati_Mit_Key__R_Field_1;
    private DbsField pnd_Ati_Mit_Key_Pnd_Ati_Pin;
    private DbsField pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme;
    private DbsField pnd_Ati_Fulfill_Key;

    private DbsGroup pnd_Ati_Fulfill_Key__R_Field_2;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts;
    private DbsField pnd_Grand_Total_Open_Atis;
    private DbsField pnd_Grand_Total_Inprogress_Atis;
    private DbsField pnd_Grand_Total_Failed_Atis;
    private DbsField pnd_Grand_Total_Successful_Atis;
    private DbsField pnd_Grand_Total_Open_Inprgrs;
    private DbsField pnd_Interim_Acct_Name;
    private DbsField pnd_Process_Status_Literal;
    private DbsField pnd_Cmtask_Status_Literal;
    private DbsField pnd_Compressed_Text;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_Cc;
    private DbsField pnd_Prev_Wpid;
    private DbsField pnd_Wpid_Total;
    private DbsField pnd_Fld_Line;
    private DbsField pnd_Fld_Strt;
    private DbsField pnd_Fld_End;
    private DbsField pnd_Accnt_Amt_Type;
    private DbsField pnd_Field;
    private DbsField pnd_Title_Literal;
    private DbsField pnd_Input_Dte_Tme;
    private DbsField pnd_Input_Dte_D;
    private DbsField pnd_Orig_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Entry_Date;
    private DbsField pnd_Last_Page_Flag;
    private DbsField pnd_Mit_Log_Date;
    private DbsField pnd_Ati_Pin_Npir_Hold;
    private DbsField pnd_Ati_Generic_Qsum_Data_Line;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Input_Data = parameters.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Ati_Fulfill_Qsum = new DataAccessProgramView(new NameInfo("vw_cwf_Ati_Fulfill_Qsum", "CWF-ATI-FULFILL-QSUM"), "CWF_ATI_FULFILL_POST", "CWF_KDO_FULFILL");
        cwf_Ati_Fulfill_Qsum_Ati_Rcrd_Type = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Rcrd_Type", "ATI-RCRD-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ATI_RCRD_TYPE");

        cwf_Ati_Fulfill_Qsum_Ati_Rqst_Id = vw_cwf_Ati_Fulfill_Qsum.getRecord().newGroupInGroup("CWF_ATI_FULFILL_QSUM_ATI_RQST_ID", "ATI-RQST-ID");
        cwf_Ati_Fulfill_Qsum_Ati_Pin = cwf_Ati_Fulfill_Qsum_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Pin", "ATI-PIN", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "ATI_PIN");
        cwf_Ati_Fulfill_Qsum_Ati_Mit_Log_Dt_Tme = cwf_Ati_Fulfill_Qsum_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Mit_Log_Dt_Tme", "ATI-MIT-LOG-DT-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ATI_MIT_LOG_DT_TME");
        cwf_Ati_Fulfill_Qsum_Ati_Prcss_Stts = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Prcss_Stts", "ATI-PRCSS-STTS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ATI_PRCSS_STTS");
        cwf_Ati_Fulfill_Qsum_Ati_Wpid = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Wpid", "ATI-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "ATI_WPID");
        cwf_Ati_Fulfill_Qsum_Ati_Entry_Dte_Tme = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Entry_Dte_Tme", "ATI-ENTRY-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ATI_ENTRY_DTE_TME");
        cwf_Ati_Fulfill_Qsum_Ati_Entry_Systm_Or_Unt = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Entry_Systm_Or_Unt", 
            "ATI-ENTRY-SYSTM-OR-UNT", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_SYSTM_OR_UNT");
        cwf_Ati_Fulfill_Qsum_Ati_Prcss_Applctn_Id = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Prcss_Applctn_Id", "ATI-PRCSS-APPLCTN-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_PRCSS_APPLCTN_ID");
        cwf_Ati_Fulfill_Qsum_Ati_Error_Txt = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Error_Txt", "ATI-ERROR-TXT", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "ATI_ERROR_TXT");
        cwf_Ati_Fulfill_Qsum_Ati_Prge_Ind = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Prge_Ind", "ATI-PRGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRGE_IND");
        cwf_Ati_Fulfill_Qsum_Ati_Entry_Racf_Id = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Entry_Racf_Id", "ATI-ENTRY-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_RACF_ID");
        cwf_Ati_Fulfill_Qsum_Ati_Entry_Unit_Cde = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Entry_Unit_Cde", "ATI-ENTRY-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_UNIT_CDE");
        cwf_Ati_Fulfill_Qsum_Ati_Cmtask_Ind = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Cmtask_Ind", "ATI-CMTASK-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ATI_CMTASK_IND");
        cwf_Ati_Fulfill_Qsum_Ati_Stts_Dte_Tme = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Stts_Dte_Tme", "ATI-STTS-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ATI_STTS_DTE_TME");
        cwf_Ati_Fulfill_Qsum_Ati_Var_DataMuGroup = vw_cwf_Ati_Fulfill_Qsum.getRecord().newGroupInGroup("CWF_ATI_FULFILL_QSUM_ATI_VAR_DATAMuGroup", "ATI_VAR_DATAMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_KDO_FULFILL_ATI_VAR_DATA");
        cwf_Ati_Fulfill_Qsum_Ati_Var_Data = cwf_Ati_Fulfill_Qsum_Ati_Var_DataMuGroup.newFieldArrayInGroup("cwf_Ati_Fulfill_Qsum_Ati_Var_Data", "ATI-VAR-DATA", 
            FieldType.STRING, 80, new DbsArrayController(1, 60), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ATI_VAR_DATA");
        cwf_Ati_Fulfill_Qsum_Ati_Npir_Id = vw_cwf_Ati_Fulfill_Qsum.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Qsum_Ati_Npir_Id", "ATI-NPIR-ID", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "ATI_NPIR_ID");
        registerRecord(vw_cwf_Ati_Fulfill_Qsum);

        pnd_Ati_Mit_Key = localVariables.newFieldInRecord("pnd_Ati_Mit_Key", "#ATI-MIT-KEY", FieldType.BINARY, 14);

        pnd_Ati_Mit_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Ati_Mit_Key__R_Field_1", "REDEFINE", pnd_Ati_Mit_Key);
        pnd_Ati_Mit_Key_Pnd_Ati_Pin = pnd_Ati_Mit_Key__R_Field_1.newFieldInGroup("pnd_Ati_Mit_Key_Pnd_Ati_Pin", "#ATI-PIN", FieldType.NUMERIC, 7);
        pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme = pnd_Ati_Mit_Key__R_Field_1.newFieldInGroup("pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme", "#ATI-LOG-DTE-TME", FieldType.TIME);
        pnd_Ati_Fulfill_Key = localVariables.newFieldInRecord("pnd_Ati_Fulfill_Key", "#ATI-FULFILL-KEY", FieldType.STRING, 9);

        pnd_Ati_Fulfill_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Ati_Fulfill_Key__R_Field_2", "REDEFINE", pnd_Ati_Fulfill_Key);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl = pnd_Ati_Fulfill_Key__R_Field_2.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl", "#ATI-RCRD-TYPE-CNTRL", 
            FieldType.STRING, 1);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme = pnd_Ati_Fulfill_Key__R_Field_2.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme", "#ATI-ENTRY-DTE-TME", 
            FieldType.TIME);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts = pnd_Ati_Fulfill_Key__R_Field_2.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts", "#ATI-PRCSS-STTS", 
            FieldType.STRING, 1);
        pnd_Grand_Total_Open_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Open_Atis", "#GRAND-TOTAL-OPEN-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Inprogress_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Inprogress_Atis", "#GRAND-TOTAL-INPROGRESS-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Grand_Total_Failed_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Failed_Atis", "#GRAND-TOTAL-FAILED-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Successful_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Successful_Atis", "#GRAND-TOTAL-SUCCESSFUL-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Grand_Total_Open_Inprgrs = localVariables.newFieldInRecord("pnd_Grand_Total_Open_Inprgrs", "#GRAND-TOTAL-OPEN-INPRGRS", FieldType.NUMERIC, 
            5);
        pnd_Interim_Acct_Name = localVariables.newFieldInRecord("pnd_Interim_Acct_Name", "#INTERIM-ACCT-NAME", FieldType.STRING, 11);
        pnd_Process_Status_Literal = localVariables.newFieldInRecord("pnd_Process_Status_Literal", "#PROCESS-STATUS-LITERAL", FieldType.STRING, 7);
        pnd_Cmtask_Status_Literal = localVariables.newFieldInRecord("pnd_Cmtask_Status_Literal", "#CMTASK-STATUS-LITERAL", FieldType.STRING, 27);
        pnd_Compressed_Text = localVariables.newFieldInRecord("pnd_Compressed_Text", "#COMPRESSED-TEXT", FieldType.STRING, 38);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 3);
        pnd_Cc = localVariables.newFieldInRecord("pnd_Cc", "#CC", FieldType.NUMERIC, 3);
        pnd_Prev_Wpid = localVariables.newFieldInRecord("pnd_Prev_Wpid", "#PREV-WPID", FieldType.STRING, 6);
        pnd_Wpid_Total = localVariables.newFieldInRecord("pnd_Wpid_Total", "#WPID-TOTAL", FieldType.NUMERIC, 8);
        pnd_Fld_Line = localVariables.newFieldInRecord("pnd_Fld_Line", "#FLD-LINE", FieldType.NUMERIC, 3);
        pnd_Fld_Strt = localVariables.newFieldInRecord("pnd_Fld_Strt", "#FLD-STRT", FieldType.NUMERIC, 3);
        pnd_Fld_End = localVariables.newFieldInRecord("pnd_Fld_End", "#FLD-END", FieldType.NUMERIC, 3);
        pnd_Accnt_Amt_Type = localVariables.newFieldInRecord("pnd_Accnt_Amt_Type", "#ACCNT-AMT-TYPE", FieldType.STRING, 1);
        pnd_Field = localVariables.newFieldInRecord("pnd_Field", "#FIELD", FieldType.STRING, 30);
        pnd_Title_Literal = localVariables.newFieldInRecord("pnd_Title_Literal", "#TITLE-LITERAL", FieldType.STRING, 54);
        pnd_Input_Dte_Tme = localVariables.newFieldInRecord("pnd_Input_Dte_Tme", "#INPUT-DTE-TME", FieldType.TIME);
        pnd_Input_Dte_D = localVariables.newFieldInRecord("pnd_Input_Dte_D", "#INPUT-DTE-D", FieldType.DATE);
        pnd_Orig_Date = localVariables.newFieldInRecord("pnd_Orig_Date", "#ORIG-DATE", FieldType.DATE);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 8);
        pnd_Entry_Date = localVariables.newFieldInRecord("pnd_Entry_Date", "#ENTRY-DATE", FieldType.DATE);
        pnd_Last_Page_Flag = localVariables.newFieldInRecord("pnd_Last_Page_Flag", "#LAST-PAGE-FLAG", FieldType.STRING, 1);
        pnd_Mit_Log_Date = localVariables.newFieldInRecord("pnd_Mit_Log_Date", "#MIT-LOG-DATE", FieldType.NUMERIC, 14);
        pnd_Ati_Pin_Npir_Hold = localVariables.newFieldInRecord("pnd_Ati_Pin_Npir_Hold", "#ATI-PIN-NPIR-HOLD", FieldType.STRING, 12);
        pnd_Ati_Generic_Qsum_Data_Line = localVariables.newFieldInRecord("pnd_Ati_Generic_Qsum_Data_Line", "#ATI-GENERIC-QSUM-DATA-LINE", FieldType.STRING, 
            132);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Ati_Fulfill_Qsum.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Mcsn3830() throws Exception
    {
        super("Mcsn3830");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt13, 13);
        getReports().atEndOfPage(atEndEventRpt13, 13);
        setupReports();
        //*  ----------------------------------------------------------------------
        //*   MAIN PROGRAM
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 13 ) PS = 60 LS = 133 ZP = OFF
        pnd_Last_Page_Flag.reset();                                                                                                                                       //Natural: RESET #LAST-PAGE-FLAG #GRAND-TOTAL-OPEN-ATIS #GRAND-TOTAL-INPROGRESS-ATIS #GRAND-TOTAL-FAILED-ATIS #GRAND-TOTAL-SUCCESSFUL-ATIS #GRAND-TOTAL-OPEN-INPRGRS
        pnd_Grand_Total_Open_Atis.reset();
        pnd_Grand_Total_Inprogress_Atis.reset();
        pnd_Grand_Total_Failed_Atis.reset();
        pnd_Grand_Total_Successful_Atis.reset();
        pnd_Grand_Total_Open_Inprgrs.reset();
        if (condition(pnd_Input_Data_Pnd_Input_Date.notEquals(" ")))                                                                                                      //Natural: IF #INPUT-DATE NE ' '
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Input_Dte_Tme.setValue(pnd_Input_Dte_D);                                                                                                                  //Natural: MOVE #INPUT-DTE-D TO #INPUT-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(13, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***");                                    //Natural: WRITE ( 13 ) /// '*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***'
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
        //* *** INITIALIZE THE ATI-CONTROL-KEY SUPER-DESCRIPTOR FIELD
        pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl.setValue("3");                                                                                                        //Natural: ASSIGN #ATI-RCRD-TYPE-CNTRL := '3'
        pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme.reset();                                                                                                                //Natural: RESET #ATI-ENTRY-DTE-TME
        //* *****************************************************************
        //* ** READ CWF-ATI-FULFILL AND PROCESS OPEN AND IN-PROGRESS REQUESTS
        //* *****************************************************************
        getReports().newPage(new ReportSpecification(13));                                                                                                                //Natural: NEWPAGE ( 13 )
        if (condition(Global.isEscape())){return;}
        vw_cwf_Ati_Fulfill_Qsum.startDatabaseRead                                                                                                                         //Natural: READ CWF-ATI-FULFILL-QSUM BY ATI-FULFILL-KEY STARTING FROM #ATI-FULFILL-KEY
        (
        "READ_ATI",
        new Wc[] { new Wc("ATI_FULFILL_KEY", ">=", pnd_Ati_Fulfill_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("ATI_FULFILL_KEY", "ASC") }
        );
        READ_ATI:
        while (condition(vw_cwf_Ati_Fulfill_Qsum.readNextRow("READ_ATI")))
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Entry_Date.setValue(cwf_Ati_Fulfill_Qsum_Ati_Stts_Dte_Tme);                                                                                               //Natural: MOVE CWF-ATI-FULFILL-QSUM.ATI-STTS-DTE-TME TO #ENTRY-DATE
            if (condition(!(pnd_Entry_Date.equals(pnd_Input_Dte_D) && cwf_Ati_Fulfill_Qsum_Ati_Prcss_Applctn_Id.equals("QSUM"))))                                         //Natural: ACCEPT IF #ENTRY-DATE = #INPUT-DTE-D AND CWF-ATI-FULFILL-QSUM.ATI-PRCSS-APPLCTN-ID = 'QSUM'
            {
                continue;
            }
            //*         AND CWF-ATI-FULFILL-QSUM.ATI-PRCSS-STTS  = 'O'
            getSort().writeSortInData(cwf_Ati_Fulfill_Qsum_Ati_Wpid, cwf_Ati_Fulfill_Qsum_Ati_Pin, cwf_Ati_Fulfill_Qsum_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Qsum_Ati_Rcrd_Type,  //Natural: END-ALL
                cwf_Ati_Fulfill_Qsum_Ati_Prcss_Stts, cwf_Ati_Fulfill_Qsum_Ati_Error_Txt, cwf_Ati_Fulfill_Qsum_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Qsum_Ati_Entry_Systm_Or_Unt, 
                cwf_Ati_Fulfill_Qsum_Ati_Entry_Racf_Id, cwf_Ati_Fulfill_Qsum_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Qsum_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Qsum_Ati_Npir_Id, 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(1), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(2), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(3), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(4), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(5), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(6), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(7), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(8), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(9), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(10), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(11), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(12), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(13), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(14), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(15), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(16), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(17), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(18), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(19), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(20), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(21), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(22), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(23), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(24), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(25), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(26), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(27), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(28), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(29), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(30), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(31), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(32), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(33), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(34), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(35), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(36), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(37), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(38), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(39), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(40), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(41), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(42), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(43), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(44), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(45), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(46), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(47), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(48), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(49), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(50), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(51), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(52), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(53), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(54), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(55), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(56), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(57), 
                cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(58), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(59), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(60));
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  SORT PERFORMED TO WRITE THEM PIN/MIT-LOG-DATE-TIME/REC-TYPE ORDER
        getSort().sortData(cwf_Ati_Fulfill_Qsum_Ati_Wpid, cwf_Ati_Fulfill_Qsum_Ati_Pin, cwf_Ati_Fulfill_Qsum_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Qsum_Ati_Rcrd_Type,      //Natural: SORT RECORDS BY CWF-ATI-FULFILL-QSUM.ATI-WPID CWF-ATI-FULFILL-QSUM.ATI-PIN CWF-ATI-FULFILL-QSUM.ATI-MIT-LOG-DT-TME CWF-ATI-FULFILL-QSUM.ATI-RCRD-TYPE CWF-ATI-FULFILL-QSUM.ATI-PRCSS-STTS USING CWF-ATI-FULFILL-QSUM.ATI-ERROR-TXT CWF-ATI-FULFILL-QSUM.ATI-ENTRY-DTE-TME CWF-ATI-FULFILL-QSUM.ATI-ENTRY-SYSTM-OR-UNT CWF-ATI-FULFILL-QSUM.ATI-ENTRY-RACF-ID CWF-ATI-FULFILL-QSUM.ATI-ENTRY-UNIT-CDE CWF-ATI-FULFILL-QSUM.ATI-STTS-DTE-TME CWF-ATI-FULFILL-QSUM.ATI-NPIR-ID CWF-ATI-FULFILL-QSUM.ATI-VAR-DATA ( 1:60 )
            cwf_Ati_Fulfill_Qsum_Ati_Prcss_Stts);
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Ati_Fulfill_Qsum_Ati_Wpid, cwf_Ati_Fulfill_Qsum_Ati_Pin, cwf_Ati_Fulfill_Qsum_Ati_Mit_Log_Dt_Tme, 
            cwf_Ati_Fulfill_Qsum_Ati_Rcrd_Type, cwf_Ati_Fulfill_Qsum_Ati_Prcss_Stts, cwf_Ati_Fulfill_Qsum_Ati_Error_Txt, cwf_Ati_Fulfill_Qsum_Ati_Entry_Dte_Tme, 
            cwf_Ati_Fulfill_Qsum_Ati_Entry_Systm_Or_Unt, cwf_Ati_Fulfill_Qsum_Ati_Entry_Racf_Id, cwf_Ati_Fulfill_Qsum_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Qsum_Ati_Stts_Dte_Tme, 
            cwf_Ati_Fulfill_Qsum_Ati_Npir_Id, cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(1), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(2), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(3), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(4), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(5), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(6), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(7), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(8), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(9), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(10), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(11), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(12), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(13), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(14), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(15), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(16), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(17), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(18), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(19), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(20), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(21), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(22), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(23), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(24), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(25), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(26), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(27), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(28), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(29), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(30), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(31), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(32), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(33), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(34), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(35), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(36), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(37), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(38), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(39), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(40), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(41), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(42), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(43), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(44), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(45), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(46), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(47), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(48), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(49), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(50), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(51), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(52), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(53), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(54), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(55), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(56), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(57), 
            cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(58), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(59), cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(60))))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF CWF-ATI-FULFILL-QSUM.ATI-WPID
            pnd_Prev_Wpid.setValue(cwf_Ati_Fulfill_Qsum_Ati_Wpid);                                                                                                        //Natural: ASSIGN #PREV-WPID := CWF-ATI-FULFILL-QSUM.ATI-WPID
            pnd_Wpid_Total.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WPID-TOTAL
                                                                                                                                                                          //Natural: PERFORM ATI-WRITE-REC-1
            sub_Ati_Write_Rec_1();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  END PROCESSING OF OPEN AND IN-PROGRESS REQUEST
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //*  ----------------------------------------------------------------------
        //*  004T #ATI-PIN-NPIR-HOLD
        //*  020T CWF-ATI-FULFILL-QSUM.ATI-WPID
        //*  029T CWF-ATI-FULFILL-QSUM.ATI-MIT-LOG-DT-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  050T CWF-ATI-FULFILL-QSUM.ATI-ENTRY-DTE-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  070T #PROCESS-STATUS-LITERAL
        //*  080T CWF-ATI-FULFILL-QSUM.ATI-ENTRY-SYSTM-OR-UNT
        //*  090T CWF-ATI-FULFILL-QSUM.ATI-ENTRY-RACF-ID
        //*  100T CWF-ATI-FULFILL-QSUM.ATI-ENTRY-UNIT-CDE
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 13 )
        //*                                                                                                                                                               //Natural: AT END OF PAGE ( 13 )
        //* ***********************************************************************
        //*   WRITE OUT ATI GRAND TOTALS ON LAST PAGE OF REPORT
        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(13));                                                                                                                //Natural: NEWPAGE ( 13 )
        if (condition(Global.isEscape())){return;}
        pnd_Last_Page_Flag.setValue("Y");                                                                                                                                 //Natural: ASSIGN #LAST-PAGE-FLAG := 'Y'
        getReports().write(13, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,new         //Natural: WRITE ( 13 ) ////// 01T '-' ( 131 ) // 01T '-' ( 43 ) 45T 'QUARTERLY SUMMARY - OPEN REQUESTS TOTALS -' 95T '-' ( 35 ) // 01T '-' ( 131 ) // 45T 'TOTAL OPEN QUARTERLY SUMMARIES          : ' #GRAND-TOTAL-OPEN-ATIS // 45T 'TOTAL IN-PROGRESS QUARTERLY SUMMARIES   : ' #GRAND-TOTAL-INPROGRESS-ATIS // 45T '----------------------------- ' // 45T 'TOTAL OPEN/IN-PRGRS QUARTERLY SUMMARIES :' #GRAND-TOTAL-OPEN-INPRGRS
            TabSetting(1),"-",new RepeatItem(43),new TabSetting(45),"QUARTERLY SUMMARY - OPEN REQUESTS TOTALS -",new TabSetting(95),"-",new RepeatItem(35),NEWLINE,NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,new TabSetting(45),"TOTAL OPEN QUARTERLY SUMMARIES          : ",pnd_Grand_Total_Open_Atis,NEWLINE,NEWLINE,new 
            TabSetting(45),"TOTAL IN-PROGRESS QUARTERLY SUMMARIES   : ",pnd_Grand_Total_Inprogress_Atis,NEWLINE,NEWLINE,new TabSetting(45),"----------------------------- ",NEWLINE,NEWLINE,new 
            TabSetting(45),"TOTAL OPEN/IN-PRGRS QUARTERLY SUMMARIES :",pnd_Grand_Total_Open_Inprgrs);
        if (Global.isEscape()) return;
    }
    //*  START ATI-WRITE-REC-1 PROCESSING
    private void sub_Ati_Write_Rec_1() throws Exception                                                                                                                   //Natural: ATI-WRITE-REC-1
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM SET-PROCESS-STATUS
        sub_Set_Process_Status();
        if (condition(Global.isEscape())) {return;}
        if (condition(getReports().getAstLinesLeft(13).less(10)))                                                                                                         //Natural: NEWPAGE ( 13 ) IF LESS THAN 10 LINES LEFT
        {
            getReports().newPage(13);
            if (condition(Global.isEscape())){return;}
        }
        if (condition(cwf_Ati_Fulfill_Qsum_Ati_Npir_Id.equals(" ")))                                                                                                      //Natural: IF CWF-ATI-FULFILL-QSUM.ATI-NPIR-ID = ' '
        {
            pnd_Ati_Pin_Npir_Hold.setValue(cwf_Ati_Fulfill_Qsum_Ati_Pin);                                                                                                 //Natural: MOVE CWF-ATI-FULFILL-QSUM.ATI-PIN TO #ATI-PIN-NPIR-HOLD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ati_Pin_Npir_Hold.setValue(cwf_Ati_Fulfill_Qsum_Ati_Npir_Id);                                                                                             //Natural: MOVE CWF-ATI-FULFILL-QSUM.ATI-NPIR-ID TO #ATI-PIN-NPIR-HOLD
            //*  PIN-EXP
            //*   PIN-EXP>>
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(13, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Ati_Pin_Npir_Hold,new TabSetting(15),cwf_Ati_Fulfill_Qsum_Ati_Wpid,new                  //Natural: WRITE ( 13 ) / 001T #ATI-PIN-NPIR-HOLD 015T CWF-ATI-FULFILL-QSUM.ATI-WPID 024T CWF-ATI-FULFILL-QSUM.ATI-MIT-LOG-DT-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 045T CWF-ATI-FULFILL-QSUM.ATI-ENTRY-DTE-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 065T #PROCESS-STATUS-LITERAL 075T CWF-ATI-FULFILL-QSUM.ATI-ENTRY-SYSTM-OR-UNT 085T CWF-ATI-FULFILL-QSUM.ATI-ENTRY-RACF-ID 095T CWF-ATI-FULFILL-QSUM.ATI-ENTRY-UNIT-CDE 105T CWF-ATI-FULFILL-QSUM.ATI-STTS-DTE-TME ( EM = MM/DD/YYYY' 'HH:II:SS )
            TabSetting(24),cwf_Ati_Fulfill_Qsum_Ati_Mit_Log_Dt_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(45),cwf_Ati_Fulfill_Qsum_Ati_Entry_Dte_Tme, 
            new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(65),pnd_Process_Status_Literal,new TabSetting(75),cwf_Ati_Fulfill_Qsum_Ati_Entry_Systm_Or_Unt,new 
            TabSetting(85),cwf_Ati_Fulfill_Qsum_Ati_Entry_Racf_Id,new TabSetting(95),cwf_Ati_Fulfill_Qsum_Ati_Entry_Unit_Cde,new TabSetting(105),cwf_Ati_Fulfill_Qsum_Ati_Stts_Dte_Tme, 
            new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"));
        if (Global.isEscape()) return;
        //*  110T CWF-ATI-FULFILL-QSUM.ATI-STTS-DTE-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*   PIN-EXP<<
        if (condition(cwf_Ati_Fulfill_Qsum_Ati_Error_Txt.notEquals(" ")))                                                                                                 //Natural: IF CWF-ATI-FULFILL-QSUM.ATI-ERROR-TXT NE ' '
        {
            getReports().write(13, ReportOption.NOTITLE,new TabSetting(16),"ERROR MESSAGE = :",new TabSetting(34),cwf_Ati_Fulfill_Qsum_Ati_Error_Txt);                    //Natural: WRITE ( 13 ) 16T 'ERROR MESSAGE = :' 34T CWF-ATI-FULFILL-QSUM.ATI-ERROR-TXT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(13, ReportOption.NOTITLE,NEWLINE,new TabSetting(15),"ATI QUARTERLY SUMMARY DATA :",new TabSetting(50),cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(1)); //Natural: WRITE ( 13 ) / 15T 'ATI QUARTERLY SUMMARY DATA :' 50T CWF-ATI-FULFILL-QSUM.ATI-VAR-DATA ( 1 )
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #A = 2 TO 60
        for (pnd_A.setValue(2); condition(pnd_A.lessOrEqual(60)); pnd_A.nadd(1))
        {
            if (condition(cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(pnd_A).notEquals(" ")))                                                                              //Natural: IF CWF-ATI-FULFILL-QSUM.ATI-VAR-DATA ( #A ) NE ' '
            {
                getReports().write(13, ReportOption.NOTITLE,new TabSetting(50),cwf_Ati_Fulfill_Qsum_Ati_Var_Data.getValue(pnd_A));                                        //Natural: WRITE ( 13 ) 50T CWF-ATI-FULFILL-QSUM.ATI-VAR-DATA ( #A )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_A.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #A
                if (condition(getReports().getAstLinesLeft(13).less(10)))                                                                                                 //Natural: NEWPAGE ( 13 ) IF LESS THAN 10 LINES LEFT
                {
                    getReports().newPage(13);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_A.nadd(60);                                                                                                                                           //Natural: ADD 60 TO #A
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*   END  ATI-WRITE-REC-2 PROCESSING
    }
    private void sub_Set_Process_Status() throws Exception                                                                                                                //Natural: SET-PROCESS-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        short decideConditionsMet231 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CWF-ATI-FULFILL-QSUM.ATI-PRCSS-STTS;//Natural: VALUE 'O'
        if (condition((cwf_Ati_Fulfill_Qsum_Ati_Prcss_Stts.equals("O"))))
        {
            decideConditionsMet231++;
            pnd_Process_Status_Literal.setValue(" OPEN ");                                                                                                                //Natural: MOVE ' OPEN ' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Open_Atis.nadd(1);                                                                                                                            //Natural: ADD 1 TO #GRAND-TOTAL-OPEN-ATIS
            pnd_Grand_Total_Open_Inprgrs.nadd(1);                                                                                                                         //Natural: ADD 1 TO #GRAND-TOTAL-OPEN-INPRGRS
            pnd_Input_Data_Pnd_W_Ati_Open.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                                     //Natural: ADD 1 TO #W-ATI-OPEN ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((cwf_Ati_Fulfill_Qsum_Ati_Prcss_Stts.equals("I"))))
        {
            decideConditionsMet231++;
            pnd_Process_Status_Literal.setValue("INPROGS");                                                                                                               //Natural: MOVE 'INPROGS' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Inprogress_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-INPROGRESS-ATIS
            pnd_Grand_Total_Open_Inprgrs.nadd(1);                                                                                                                         //Natural: ADD 1 TO #GRAND-TOTAL-OPEN-INPRGRS
            pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                              //Natural: ADD 1 TO #W-ATI-IN-PROGRESS ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE ' '
        else if (condition((cwf_Ati_Fulfill_Qsum_Ati_Prcss_Stts.equals(" "))))
        {
            decideConditionsMet231++;
            if (condition(cwf_Ati_Fulfill_Qsum_Ati_Error_Txt.equals(" ")))                                                                                                //Natural: IF CWF-ATI-FULFILL-QSUM.ATI-ERROR-TXT = ' '
            {
                pnd_Process_Status_Literal.setValue("SUCCESS");                                                                                                           //Natural: MOVE 'SUCCESS' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Successful_Atis.nadd(1);                                                                                                                  //Natural: ADD 1 TO #GRAND-TOTAL-SUCCESSFUL-ATIS
                pnd_Input_Data_Pnd_W_Ati_Successful.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                           //Natural: ADD 1 TO #W-ATI-SUCCESSFUL ( #W-INX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Process_Status_Literal.setValue("FAILED");                                                                                                            //Natural: MOVE 'FAILED' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Failed_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-FAILED-ATIS
                pnd_Input_Data_Pnd_W_Ati_Manual.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                               //Natural: ADD 1 TO #W-ATI-MANUAL ( #W-INX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Process_Status_Literal.setValue("INCORCT");                                                                                                               //Natural: MOVE 'INCORCT' TO #PROCESS-STATUS-LITERAL
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt13 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(13, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(50),"AUTOMATED TRANSACTION SYSTEM",new               //Natural: WRITE ( 13 ) NOTITLE 1T *PROGRAM 50T 'AUTOMATED TRANSACTION SYSTEM' 101T *DATU 110T *TIME ( AL = 005 ) 121T 'PAGE:' 127T *PAGE-NUMBER ( 13 ) ( AD = L ) / 039T 'ATI DETAILED REPORT FOR GENERIC QSUM LETTERS' / 036T 'OPEN/IN-PROGRESS ATI QSUM REQUESTS FOR :' 082T #INPUT-DTE-D ( EM = MM/DD/YY ) / 001T '-' ( 131 ) / 27T 'MASTER INDEX' 49T 'ATI ENTRY' 65T 'PROCESS' 75T 'ENTRY' 85T 'ENTRY' 95T 'ENTRY' 109T 'ATI STATUS' / 2T 'PIN / NPIR' 16T 'WPID' 25T 'LOG DATE & TIME' 48T 'DATE & TIME' 65T 'STATUS' 75T 'SYSTEM' 85T 'RACF-ID' 95T 'UNIT' 109T 'DATE & TIME' / 001T '-' ( 131 ) /
                        TabSetting(101),Global.getDATU(),new TabSetting(110),Global.getTIME(), new AlphanumericLength (5),new TabSetting(121),"PAGE:",new 
                        TabSetting(127),getReports().getPageNumberDbs(13), new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(39),"ATI DETAILED REPORT FOR GENERIC QSUM LETTERS",NEWLINE,new 
                        TabSetting(36),"OPEN/IN-PROGRESS ATI QSUM REQUESTS FOR :",new TabSetting(82),pnd_Input_Dte_D, new ReportEditMask ("MM/DD/YY"),NEWLINE,new 
                        TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(27),"MASTER INDEX",new TabSetting(49),"ATI ENTRY",new TabSetting(65),"PROCESS",new 
                        TabSetting(75),"ENTRY",new TabSetting(85),"ENTRY",new TabSetting(95),"ENTRY",new TabSetting(109),"ATI STATUS",NEWLINE,new TabSetting(2),"PIN / NPIR",new 
                        TabSetting(16),"WPID",new TabSetting(25),"LOG DATE & TIME",new TabSetting(48),"DATE & TIME",new TabSetting(65),"STATUS",new TabSetting(75),"SYSTEM",new 
                        TabSetting(85),"RACF-ID",new TabSetting(95),"UNIT",new TabSetting(109),"DATE & TIME",NEWLINE,new TabSetting(1),"-",new RepeatItem(131),
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atEndEventRpt13 = (Object sender, EventArgs e) ->
    {
        String localMethod = "MCSN3830|atEndEventRpt13";
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Last_Page_Flag.equals("Y")))                                                                                                        //Natural: IF #LAST-PAGE-FLAG = 'Y'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(13, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(1),"-",new                   //Natural: WRITE ( 13 ) / 1T '-' ( 131 ) / 1T '-' ( 131 )
                            RepeatItem(131));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-ENDPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Ati_Fulfill_Qsum_Ati_WpidIsBreak = cwf_Ati_Fulfill_Qsum_Ati_Wpid.isBreak(endOfData);
        if (condition(cwf_Ati_Fulfill_Qsum_Ati_WpidIsBreak))
        {
            getReports().skip(13, 2);                                                                                                                                     //Natural: SKIP ( 13 ) 2
            getReports().write(13, ReportOption.NOTITLE,new TabSetting(1),"TOTAL FOR WPID :",pnd_Prev_Wpid, new ReportEditMask ("XX' 'X' 'XX' 'X"),":",                   //Natural: WRITE ( 13 ) 1T 'TOTAL FOR WPID :' #PREV-WPID ( EM = XX' 'X' 'XX' 'X ) ':' #WPID-TOTAL
                pnd_Wpid_Total);
            if (condition(Global.isEscape())) return;
            pnd_Wpid_Total.reset();                                                                                                                                       //Natural: RESET #WPID-TOTAL
            getReports().newPage(new ReportSpecification(13));                                                                                                            //Natural: NEWPAGE ( 13 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=OFF");
        Global.format(13, "PS=60 LS=133 ZP=OFF");
    }
}
