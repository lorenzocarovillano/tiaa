/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:26:56 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3899
************************************************************
**        * FILE NAME            : Mcsn3899.java
**        * CLASS NAME           : Mcsn3899
**        * INSTANCE NAME        : Mcsn3899
************************************************************
************************************************************************
* PROGRAM  : MCSN3899
* TITLE    : SUMMARY TOTALS FOR ALL ATI'S
* FUNCTION :
* HISTORY  : KB - 06/12/99
*          : NH - 07/06/99 - ADDED CDMS PROCESSING
*          : JG - 09/22/99 - ADDED VOAD PROCESSING
*          : JG - 06/29/00 - ADDED QSUM PROCESSING
*          : GH - 06/05/01 - ADDED IPI  PROCESSING
*          :    - IMPORTANT! NEED TO INCREASE THE FOR LOOP COUNTER FOR
*          :    - THE NEXT ATI APPLICATION.
* 12/17/01 : GH - 12/17/01 - ADDED TAX  PROCESSING
* 05/14/02 : GH - ADDED PLI  PROCESSING
* 04/14/04 : GH - ADDED ADV  PROCESSING
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3899 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;
    private DbsField pnd_I;
    private DbsField pnd_W_Total_For_Line;
    private DbsField pnd_Input_Date_D;
    private DbsField pnd_W_Ati_Program_Desc;

    private DbsGroup pnd_Ati_Grand_Totals;
    private DbsField pnd_Ati_Grand_Totals_Pnd_T_Ati_Open;
    private DbsField pnd_Ati_Grand_Totals_Pnd_T_Ati_In_Progress;
    private DbsField pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Unsuccessful;
    private DbsField pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Successful;
    private DbsField pnd_Ati_Grand_Totals_Pnd_T_Ati_Manual;
    private DbsField pnd_Ati_Grand_Totals_Pnd_T_Ati_Successful;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Input_Data = parameters.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_W_Total_For_Line = localVariables.newFieldInRecord("pnd_W_Total_For_Line", "#W-TOTAL-FOR-LINE", FieldType.NUMERIC, 9);
        pnd_Input_Date_D = localVariables.newFieldInRecord("pnd_Input_Date_D", "#INPUT-DATE-D", FieldType.DATE);
        pnd_W_Ati_Program_Desc = localVariables.newFieldInRecord("pnd_W_Ati_Program_Desc", "#W-ATI-PROGRAM-DESC", FieldType.STRING, 18);

        pnd_Ati_Grand_Totals = localVariables.newGroupInRecord("pnd_Ati_Grand_Totals", "#ATI-GRAND-TOTALS");
        pnd_Ati_Grand_Totals_Pnd_T_Ati_Open = pnd_Ati_Grand_Totals.newFieldInGroup("pnd_Ati_Grand_Totals_Pnd_T_Ati_Open", "#T-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Ati_Grand_Totals_Pnd_T_Ati_In_Progress = pnd_Ati_Grand_Totals.newFieldInGroup("pnd_Ati_Grand_Totals_Pnd_T_Ati_In_Progress", "#T-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Unsuccessful = pnd_Ati_Grand_Totals.newFieldInGroup("pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Unsuccessful", "#T-ATI-PUSH-UNSUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Successful = pnd_Ati_Grand_Totals.newFieldInGroup("pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Successful", "#T-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Ati_Grand_Totals_Pnd_T_Ati_Manual = pnd_Ati_Grand_Totals.newFieldInGroup("pnd_Ati_Grand_Totals_Pnd_T_Ati_Manual", "#T-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Ati_Grand_Totals_Pnd_T_Ati_Successful = pnd_Ati_Grand_Totals.newFieldInGroup("pnd_Ati_Grand_Totals_Pnd_T_Ati_Successful", "#T-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Mcsn3899() throws Exception
    {
        super("Mcsn3899");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt15, 15);
        setupReports();
        //*  ----------------------------------------------------------------------
        //*   MAIN PROGRAM
        //*  ----------------------------------------------------------------------
        //*  ---                                                                                                                                                          //Natural: FORMAT ( 15 ) PS = 60 LS = 133 ZP = ON
        pnd_Input_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                    //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DATE-D ( EM = YYYYMMDD )
        //*  ---
        getReports().newPage(new ReportSpecification(15));                                                                                                                //Natural: NEWPAGE ( 15 )
        if (condition(Global.isEscape())){return;}
        //*  ---
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 15 )
        //* **************   I M P O R T A N T   ************************
        //* **************   I M P O R T A N T   ************************
        //*    G HONG 06/05/01
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 14
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(14)); pnd_I.nadd(1))
        {
            if (condition(pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("AWDP")))                                                                          //Natural: IF #W-ATI-PROGRAM-NAME ( #I ) = 'AWDP'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ati_Grand_Totals_Pnd_T_Ati_Open.nadd(pnd_Input_Data_Pnd_W_Ati_Open.getValue(pnd_I));                                                                  //Natural: ADD #W-ATI-OPEN ( #I ) TO #T-ATI-OPEN
                pnd_Ati_Grand_Totals_Pnd_T_Ati_In_Progress.nadd(pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_I));                                                    //Natural: ADD #W-ATI-IN-PROGRESS ( #I ) TO #T-ATI-IN-PROGRESS
                pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Unsuccessful.nadd(pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful.getValue(pnd_I));                                        //Natural: ADD #W-ATI-PUSH-UNSUCCESSFUL ( #I ) TO #T-ATI-PUSH-UNSUCCESSFUL
                pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Successful.nadd(pnd_Input_Data_Pnd_W_Ati_Push_Successful.getValue(pnd_I));                                            //Natural: ADD #W-ATI-PUSH-SUCCESSFUL ( #I ) TO #T-ATI-PUSH-SUCCESSFUL
                pnd_Ati_Grand_Totals_Pnd_T_Ati_Manual.nadd(pnd_Input_Data_Pnd_W_Ati_Manual.getValue(pnd_I));                                                              //Natural: ADD #W-ATI-MANUAL ( #I ) TO #T-ATI-MANUAL
                pnd_Ati_Grand_Totals_Pnd_T_Ati_Successful.nadd(pnd_Input_Data_Pnd_W_Ati_Successful.getValue(pnd_I));                                                      //Natural: ADD #W-ATI-SUCCESSFUL ( #I ) TO #T-ATI-SUCCESSFUL
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-LINE
                sub_Write_Detail_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  ---
        //*  ???? THIS UPPER LIMIT INCREMENTS WITH NEW ATI's
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS-SUMMARY-LINE
        sub_Write_Totals_Summary_Line();
        if (condition(Global.isEscape())) {return;}
        //*  ---
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 30
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(30)); pnd_I.nadd(1))
        {
            if (condition(pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("AWDP")))                                                                          //Natural: IF #W-ATI-PROGRAM-NAME ( #I ) = 'AWDP'
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-AWDP-DETAIL-LINE
                sub_Write_Awdp_Detail_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL-LINE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS-SUMMARY-LINE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-AWDP-DETAIL-LINE
    }
    private void sub_Write_Detail_Line() throws Exception                                                                                                                 //Natural: WRITE-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pnd_W_Total_For_Line.compute(new ComputeParameters(false, pnd_W_Total_For_Line), pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_I).add(pnd_Input_Data_Pnd_W_Ati_Manual.getValue(pnd_I)).add(pnd_Input_Data_Pnd_W_Ati_Successful.getValue(pnd_I))); //Natural: COMPUTE #W-TOTAL-FOR-LINE = #W-ATI-IN-PROGRESS ( #I ) + #W-ATI-MANUAL ( #I ) + #W-ATI-SUCCESSFUL ( #I )
        short decideConditionsMet104 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #W-ATI-PROGRAM-NAME ( #I );//Natural: VALUE 'SSSF'
        if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("SSSF"))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("SINGLE SUM FORMS  ");                                                                                                        //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'SINGLE SUM FORMS  '
        }                                                                                                                                                                 //Natural: VALUE 'FFT '
        else if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("FFT "))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("BOOKLETS & FORMS  ");                                                                                                        //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'BOOKLETS & FORMS  '
        }                                                                                                                                                                 //Natural: VALUE 'ILLU'
        else if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("ILLU"))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("ILLUSTRATIONS     ");                                                                                                        //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'ILLUSTRATIONS     '
        }                                                                                                                                                                 //Natural: VALUE 'ADRS'
        else if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("ADRS"))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("ADDRESS CHANGES   ");                                                                                                        //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'ADDRESS CHANGES   '
        }                                                                                                                                                                 //Natural: VALUE 'ABRS'
        else if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("ABRS"))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("DUP & RECALC ABR  ");                                                                                                        //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'DUP & RECALC ABR  '
        }                                                                                                                                                                 //Natural: VALUE 'IOP '
        else if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("IOP "))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("INC/OPTION PROFILE");                                                                                                        //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'INC/OPTION PROFILE'
        }                                                                                                                                                                 //Natural: VALUE 'POST'
        else if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("POST"))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("CDMS/POST LETTERS ");                                                                                                        //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'CDMS/POST LETTERS '
        }                                                                                                                                                                 //Natural: VALUE 'VOAD'
        else if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("VOAD"))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("VOA/VOD REQUESTS  ");                                                                                                        //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'VOA/VOD REQUESTS  '
        }                                                                                                                                                                 //Natural: VALUE 'QSUM'
        else if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("QSUM"))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("QUARTERLY SUMMARY ");                                                                                                        //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'QUARTERLY SUMMARY '
        }                                                                                                                                                                 //Natural: VALUE 'IPI'
        else if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("IPI"))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("IMPROVED PRO INDIV");                                                                                                        //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'IMPROVED PRO INDIV'
        }                                                                                                                                                                 //Natural: VALUE 'TAX'
        else if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("TAX"))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("TAX REPRINT FACILITY");                                                                                                      //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'TAX REPRINT FACILITY'
        }                                                                                                                                                                 //Natural: VALUE 'PLI'
        else if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("PLI"))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("PREMIUM LETTER INT");                                                                                                        //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'PREMIUM LETTER INT'
        }                                                                                                                                                                 //Natural: VALUE 'ADV'
        else if (condition((pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(pnd_I).equals("ADV"))))
        {
            decideConditionsMet104++;
            pnd_W_Ati_Program_Desc.setValue("Adv Suitability   ");                                                                                                        //Natural: ASSIGN #W-ATI-PROGRAM-DESC := 'Adv Suitability   '
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().write(15, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(1),pnd_W_Ati_Program_Desc,new TabSetting(25),pnd_Input_Data_Pnd_W_Ati_Open.getValue(pnd_I),  //Natural: WRITE ( 15 ) // 001T #W-ATI-PROGRAM-DESC 025T #W-ATI-OPEN ( #I ) ( EM = Z,ZZZ,ZZ9 ) 045T #W-ATI-IN-PROGRESS ( #I ) ( EM = Z,ZZZ,ZZ9 ) 065T #W-ATI-MANUAL ( #I ) ( EM = Z,ZZZ,ZZ9 ) 085T #W-ATI-SUCCESSFUL ( #I ) ( EM = Z,ZZZ,ZZ9 ) 105T #W-TOTAL-FOR-LINE ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(45),pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9"),new 
            TabSetting(65),pnd_Input_Data_Pnd_W_Ati_Manual.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(85),pnd_Input_Data_Pnd_W_Ati_Successful.getValue(pnd_I), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(105),pnd_W_Total_For_Line, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Write_Totals_Summary_Line() throws Exception                                                                                                         //Natural: WRITE-TOTALS-SUMMARY-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pnd_W_Total_For_Line.compute(new ComputeParameters(false, pnd_W_Total_For_Line), pnd_Ati_Grand_Totals_Pnd_T_Ati_In_Progress.add(pnd_Ati_Grand_Totals_Pnd_T_Ati_Manual).add(pnd_Ati_Grand_Totals_Pnd_T_Ati_Successful)); //Natural: COMPUTE #W-TOTAL-FOR-LINE = #T-ATI-IN-PROGRESS + #T-ATI-MANUAL + #T-ATI-SUCCESSFUL
        getReports().write(15, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(1),"=",new RepeatItem(131),NEWLINE,new TabSetting(1),"TOTAL ATI'S",new                 //Natural: WRITE ( 15 ) // 001T '=' ( 131 ) / 001T 'TOTAL ATI"S' 025T #T-ATI-OPEN ( EM = Z,ZZZ,ZZ9 ) 045T #T-ATI-IN-PROGRESS ( EM = Z,ZZZ,ZZ9 ) 065T #T-ATI-MANUAL ( EM = Z,ZZZ,ZZ9 ) 085T #T-ATI-SUCCESSFUL ( EM = Z,ZZZ,ZZ9 ) 105T #W-TOTAL-FOR-LINE ( EM = Z,ZZZ,ZZ9 ) / 001T '=' ( 131 )
            TabSetting(25),pnd_Ati_Grand_Totals_Pnd_T_Ati_Open, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(45),pnd_Ati_Grand_Totals_Pnd_T_Ati_In_Progress, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(65),pnd_Ati_Grand_Totals_Pnd_T_Ati_Manual, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(85),pnd_Ati_Grand_Totals_Pnd_T_Ati_Successful, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(105),pnd_W_Total_For_Line, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"=",new 
            RepeatItem(131));
        if (Global.isEscape()) return;
    }
    private void sub_Write_Awdp_Detail_Line() throws Exception                                                                                                            //Natural: WRITE-AWDP-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pnd_W_Total_For_Line.compute(new ComputeParameters(false, pnd_W_Total_For_Line), pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_I).add(pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful.getValue(pnd_I)).add(pnd_Input_Data_Pnd_W_Ati_Push_Successful.getValue(pnd_I))); //Natural: COMPUTE #W-TOTAL-FOR-LINE = #W-ATI-IN-PROGRESS ( #I ) + #W-ATI-PUSH-UNSUCCESSFUL ( #I ) + #W-ATI-PUSH-SUCCESSFUL ( #I )
        getReports().write(15, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"-",new RepeatItem(132),NEWLINE,new TabSetting(25),"    OPEN    ",new       //Natural: WRITE ( 15 ) /// 001T '-' ( 132 ) / 025T '    OPEN    ' 045T 'IN PROGRESS ' 065T 'UNSUCCESSFUL' 085T ' SUCCESSFUL ' 105T '   TOTAL    ' / 001T '-' ( 132 ) // 001T 'CWF TO AWD PUSH' 025T #W-ATI-OPEN ( #I ) ( EM = Z,ZZZ,ZZ9 ) 045T #W-ATI-IN-PROGRESS ( #I ) ( EM = Z,ZZZ,ZZ9 ) 065T #W-ATI-PUSH-UNSUCCESSFUL ( #I ) ( EM = Z,ZZZ,ZZ9 ) 085T #W-ATI-PUSH-SUCCESSFUL ( #I ) ( EM = Z,ZZZ,ZZ9 ) 105T #W-TOTAL-FOR-LINE ( EM = Z,ZZZ,ZZ9 ) // 001T '* NOTE * INSURANCE WORK REQUESTS ARE SENT FROM CWF TO AWD' 059T 'VIA ATI.  THEY ARE NOT CONSIDERED ATI AUTOMATED WORK.'
            TabSetting(45),"IN PROGRESS ",new TabSetting(65),"UNSUCCESSFUL",new TabSetting(85)," SUCCESSFUL ",new TabSetting(105),"   TOTAL    ",NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(132),NEWLINE,NEWLINE,new TabSetting(1),"CWF TO AWD PUSH",new TabSetting(25),pnd_Input_Data_Pnd_W_Ati_Open.getValue(pnd_I), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(45),pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9"),new 
            TabSetting(65),pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(85),pnd_Input_Data_Pnd_W_Ati_Push_Successful.getValue(pnd_I), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(105),pnd_W_Total_For_Line, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(1),"* NOTE * INSURANCE WORK REQUESTS ARE SENT FROM CWF TO AWD",new 
            TabSetting(59),"VIA ATI.  THEY ARE NOT CONSIDERED ATI AUTOMATED WORK.");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt15 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(15, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(45),"AUTOMATED TRANSACTION INITIATION SYSTEM",new    //Natural: WRITE ( 15 ) NOTITLE 001T *PROGRAM 045T 'AUTOMATED TRANSACTION INITIATION SYSTEM' 118T *DATU 127T *TIME ( AL = 005 ) / 045T 'ATI DAILY SUMMARY REPORT FOR ' #INPUT-DATE-D ( EM = MM/DD/YYYY ) 121T 'PAGE:' 127T *PAGE-NUMBER ( 15 ) ( AD = L ) // 001T '-' ( 132 ) / 001T '    ATI     ' 025T '    OPEN    ' 045T '     IN     ' 065T ' CHANGED TO ' 085T 'SUCCESSFULLY' 105T '   TOTAL    ' / 001T '  PROCESS   ' 025T '            ' 045T '  PROGRESS  ' 065T '   MANUAL   ' 085T ' AUTOMATED  ' 105T '            ' / 001T '-' ( 132 )
                        TabSetting(118),Global.getDATU(),new TabSetting(127),Global.getTIME(), new AlphanumericLength (5),NEWLINE,new TabSetting(45),"ATI DAILY SUMMARY REPORT FOR ",pnd_Input_Date_D, 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(121),"PAGE:",new TabSetting(127),getReports().getPageNumberDbs(15), new FieldAttributes 
                        ("AD=L"),NEWLINE,NEWLINE,new TabSetting(1),"-",new RepeatItem(132),NEWLINE,new TabSetting(1),"    ATI     ",new TabSetting(25),"    OPEN    ",new 
                        TabSetting(45),"     IN     ",new TabSetting(65)," CHANGED TO ",new TabSetting(85),"SUCCESSFULLY",new TabSetting(105),"   TOTAL    ",NEWLINE,new 
                        TabSetting(1),"  PROCESS   ",new TabSetting(25),"            ",new TabSetting(45),"  PROGRESS  ",new TabSetting(65),"   MANUAL   ",new 
                        TabSetting(85)," AUTOMATED  ",new TabSetting(105),"            ",NEWLINE,new TabSetting(1),"-",new RepeatItem(132));
                    //*  <--- THIS UPPER LIMIT INCREMENTS WITH NEW ATI'S
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(15, "PS=60 LS=133 ZP=ON");
    }
}
