/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:26:17 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn3320
************************************************************
**        * FILE NAME            : Cwfn3320.java
**        * CLASS NAME           : Cwfn3320
**        * INSTANCE NAME        : Cwfn3320
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 12
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFN3320
* SYSTEM   : CRPCWF
* TITLE    : REPORT 11
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF CASES WITH SPECIFIED STATUS
*          |
*          |
*          |
*          |
*          |
*          |
************************************************************************
*                    M O D I F I C A T I O N S                         *
************************************************************************
* NAME         DATE    DESCRIPTION                                     *
* ----------- -------- ------------------------------------------------*
* J. HARGRAVE 08/21/95 CHANGE TO READ CORE FILE INSTEAD OF PIF FILE    *
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn3320 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    public DbsRecord parameters;
    private PdaCwfa3012 pdaCwfa3012;
    private PdaCwfa3300 pdaCwfa3300;
    private PdaCwfa5372 pdaCwfa5372;
    private PdaCwfa3310 pdaCwfa3310;
    private PdaCdextia pdaCdextia;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Env;
    private DbsField pnd_Save_Status;
    private DbsField pnd_Rqst_Tiaa_Rcvd_Dte;
    private DbsField pnd_Unit_Status_Cde;

    private DbsGroup pnd_Unit_Status_Cde__R_Field_1;
    private DbsField pnd_Unit_Status_Cde_Pnd_Unit_Cde;
    private DbsField pnd_Unit_Status_Cde_Pnd_Status_Cde;
    private DbsField pnd_Save_Unit_Status_Cde;

    private DbsGroup pnd_Save_Unit_Status_Cde__R_Field_2;
    private DbsField pnd_Save_Unit_Status_Cde_Pnd_Save_Unit_Cde;
    private DbsField pnd_Save_Unit_Status_Cde_Pnd_Save_Status_Cde;
    private DbsField pnd_Save_Rqst_Log_Dte_Tme;
    private DbsField pnd_Save_Last_Chnge_Invrt_Dte_Tme;
    private DbsField pnd_Rec_Count;
    private DbsField pnd_Rep_Rqsted;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Page;
    private DbsField pnd_Report_No;

    private DbsRecord internalLoopRecord;
    private DbsField readWork02Rqst_Work_Prcss_IdOld;
    private DbsField readWork02Rqst_Work_Prcss_IdCount869;
    private DbsField readWork02Rqst_Work_Prcss_IdCount;
    private DbsField readWork02Pnd_Wpid_ActnOld;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaCwfa5372 = new PdaCwfa5372(localVariables);
        pdaCwfa3310 = new PdaCwfa3310(localVariables);
        pdaCdextia = new PdaCdextia(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaCwfa3012 = new PdaCwfa3012(parameters);
        pdaCwfa3300 = new PdaCwfa3300(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Save_Status = localVariables.newFieldInRecord("pnd_Save_Status", "#SAVE-STATUS", FieldType.STRING, 4);
        pnd_Rqst_Tiaa_Rcvd_Dte = localVariables.newFieldInRecord("pnd_Rqst_Tiaa_Rcvd_Dte", "#RQST-TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Unit_Status_Cde = localVariables.newFieldInRecord("pnd_Unit_Status_Cde", "#UNIT-STATUS-CDE", FieldType.STRING, 12);

        pnd_Unit_Status_Cde__R_Field_1 = localVariables.newGroupInRecord("pnd_Unit_Status_Cde__R_Field_1", "REDEFINE", pnd_Unit_Status_Cde);
        pnd_Unit_Status_Cde_Pnd_Unit_Cde = pnd_Unit_Status_Cde__R_Field_1.newFieldInGroup("pnd_Unit_Status_Cde_Pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Unit_Status_Cde_Pnd_Status_Cde = pnd_Unit_Status_Cde__R_Field_1.newFieldInGroup("pnd_Unit_Status_Cde_Pnd_Status_Cde", "#STATUS-CDE", FieldType.STRING, 
            4);
        pnd_Save_Unit_Status_Cde = localVariables.newFieldInRecord("pnd_Save_Unit_Status_Cde", "#SAVE-UNIT-STATUS-CDE", FieldType.STRING, 12);

        pnd_Save_Unit_Status_Cde__R_Field_2 = localVariables.newGroupInRecord("pnd_Save_Unit_Status_Cde__R_Field_2", "REDEFINE", pnd_Save_Unit_Status_Cde);
        pnd_Save_Unit_Status_Cde_Pnd_Save_Unit_Cde = pnd_Save_Unit_Status_Cde__R_Field_2.newFieldInGroup("pnd_Save_Unit_Status_Cde_Pnd_Save_Unit_Cde", 
            "#SAVE-UNIT-CDE", FieldType.STRING, 8);
        pnd_Save_Unit_Status_Cde_Pnd_Save_Status_Cde = pnd_Save_Unit_Status_Cde__R_Field_2.newFieldInGroup("pnd_Save_Unit_Status_Cde_Pnd_Save_Status_Cde", 
            "#SAVE-STATUS-CDE", FieldType.STRING, 4);
        pnd_Save_Rqst_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_Save_Rqst_Log_Dte_Tme", "#SAVE-RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Save_Last_Chnge_Invrt_Dte_Tme = localVariables.newFieldInRecord("pnd_Save_Last_Chnge_Invrt_Dte_Tme", "#SAVE-LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 
            15);
        pnd_Rec_Count = localVariables.newFieldInRecord("pnd_Rec_Count", "#REC-COUNT", FieldType.NUMERIC, 4);
        pnd_Rep_Rqsted = localVariables.newFieldInRecord("pnd_Rep_Rqsted", "#REP-RQSTED", FieldType.BOOLEAN, 1);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 3);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork02Rqst_Work_Prcss_IdOld = internalLoopRecord.newFieldInRecord("ReadWork02_Rqst_Work_Prcss_Id_OLD", "Rqst_Work_Prcss_Id_OLD", FieldType.STRING, 
            6);
        readWork02Rqst_Work_Prcss_IdCount869 = internalLoopRecord.newFieldInRecord("ReadWork02_Rqst_Work_Prcss_Id_COUNT_869", "Rqst_Work_Prcss_Id_COUNT_869", 
            FieldType.NUMERIC, 9);
        readWork02Rqst_Work_Prcss_IdCount = internalLoopRecord.newFieldInRecord("ReadWork02_Rqst_Work_Prcss_Id_COUNT", "Rqst_Work_Prcss_Id_COUNT", FieldType.NUMERIC, 
            9);
        readWork02Pnd_Wpid_ActnOld = internalLoopRecord.newFieldInRecord("ReadWork02_Pnd_Wpid_Actn_OLD", "Pnd_Wpid_Actn_OLD", FieldType.STRING, 1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Report_No.setInitialValue(12);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Cwfn3320() throws Exception
    {
        super("Cwfn3320");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Start_Date().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Start_Date());                                                     //Natural: MOVE #INPUT-START-DATE TO #START-DATE
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_End_Date().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_End_Date());                                                         //Natural: MOVE #INPUT-END-DATE TO #END-DATE
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Filler1().setValue("/");                                                                                                         //Natural: MOVE '/' TO #FILLER1 #FILLER2
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Filler2().setValue("/");
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Fillera().setValue("/");                                                                                                         //Natural: MOVE '/' TO #FILLERA #FILLERB
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Fillerb().setValue("/");
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Todays_Time().setValue(Global.getTIMX());                                                                                        //Natural: MOVE *TIMX TO #TODAYS-TIME
        pnd_Page.setValue(0);                                                                                                                                             //Natural: FORMAT ( 1 ) LS = 142 PS = 55;//Natural: MOVE 0 TO #PAGE
        pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue("*").setValue("________");                                                                                //Natural: MOVE '________' TO #REP-UNIT-CDE ( * )
        pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue("*").setValue("____");                                                                                  //Natural: MOVE '____' TO #REP-STATUS-CDE ( * )
        pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid().setValue("______");                                                                                                    //Natural: MOVE '______' TO #REP-WPID
        pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Empl_Name().setValue("______________________________");                                                                       //Natural: MOVE '______________________________' TO #REP-EMPL-NAME
        pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_From_1().setValue("____");                                                                                             //Natural: MOVE '____' TO #REP-STATUS-FROM-1
        pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_To_1().setValue("____");                                                                                               //Natural: MOVE '____' TO #REP-STATUS-TO-1
        pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_From_2().setValue("____");                                                                                             //Natural: MOVE '____' TO #REP-STATUS-FROM-2
        pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_To_2().setValue("____");                                                                                               //Natural: MOVE '____' TO #REP-STATUS-TO-2
        pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Break_Ind().setValue("_");                                                                                                    //Natural: MOVE '_' TO #REP-BREAK-IND
        //*  MOVE INPUT DTAT TO REPORT AREA
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 11
        for (pdaCwfa3310.getPnd_Misc_Parm_Pnd_I().setValue(1); condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_I().lessOrEqual(11)); pdaCwfa3310.getPnd_Misc_Parm_Pnd_I().nadd(1))
        {
            if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Unit_Cde().getValue(pdaCwfa3310.getPnd_Misc_Parm_Pnd_I()).greater(" ")))                                //Natural: IF #INPUT-UNIT-CDE ( #I ) > ' '
            {
                pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(pdaCwfa3310.getPnd_Misc_Parm_Pnd_I()).setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Unit_Cde().getValue(pdaCwfa3310.getPnd_Misc_Parm_Pnd_I())); //Natural: MOVE #INPUT-UNIT-CDE ( #I ) TO #REP-UNIT-CDE ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 10
        for (pdaCwfa3310.getPnd_Misc_Parm_Pnd_I().setValue(1); condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_I().lessOrEqual(10)); pdaCwfa3310.getPnd_Misc_Parm_Pnd_I().nadd(1))
        {
            if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_Cde().getValue(pdaCwfa3310.getPnd_Misc_Parm_Pnd_I()).greater(" ")))                              //Natural: IF #INPUT-STATUS-CDE ( #I ) > ' '
            {
                pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(pdaCwfa3310.getPnd_Misc_Parm_Pnd_I()).setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_Cde().getValue(pdaCwfa3310.getPnd_Misc_Parm_Pnd_I())); //Natural: MOVE #INPUT-STATUS-CDE ( #I ) TO #REP-STATUS-CDE ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_From_1().greater(" ")))                                                                              //Natural: IF #INPUT-STATUS-FROM-1 > ' '
        {
            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_From_1().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_From_1());                                        //Natural: MOVE #INPUT-STATUS-FROM-1 TO #REP-STATUS-FROM-1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_To_1().greater(" ")))                                                                                //Natural: IF #INPUT-STATUS-TO-1 > ' '
        {
            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_To_1().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_To_1());                                            //Natural: MOVE #INPUT-STATUS-TO-1 TO #REP-STATUS-TO-1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_From_2().greater(" ")))                                                                              //Natural: IF #INPUT-STATUS-FROM-2 > ' '
        {
            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_From_2().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_From_2());                                        //Natural: MOVE #INPUT-STATUS-FROM-2 TO #REP-STATUS-FROM-2
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_To_2().greater(" ")))                                                                                //Natural: IF #INPUT-STATUS-TO-2 > ' '
        {
            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_To_2().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Status_To_2());                                            //Natural: MOVE #INPUT-STATUS-TO-2 TO #REP-STATUS-TO-2
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Work_Mm().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Start_Mm());                                                          //Natural: MOVE #INPUT-START-MM TO #WORK-MM
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Work_Dd().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Start_Dd());                                                          //Natural: MOVE #INPUT-START-DD TO #WORK-DD
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Work_Yy().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Start_Yy());                                                          //Natural: MOVE #INPUT-START-YY TO #WORK-YY
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Work_Mmm().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_End_Mm());                                                           //Natural: MOVE #INPUT-END-MM TO #WORK-MMM
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Work_Ddd().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_End_Dd());                                                           //Natural: MOVE #INPUT-END-DD TO #WORK-DDD
        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Work_Yyy().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_End_Yy());                                                           //Natural: MOVE #INPUT-END-YY TO #WORK-YYY
        if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Rqst_Wpid().greater(" ")))                                                                                  //Natural: IF #INPUT-RQST-WPID > ' '
        {
            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Rqst_Wpid());                                                     //Natural: MOVE #INPUT-RQST-WPID TO #REP-WPID
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Racf_Id().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Racf_Id());                                                        //Natural: MOVE #INPUT-RACF-ID TO #REP-RACF-ID
        if (condition(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Break_Ind().greater(" ")))                                                                                  //Natural: IF #INPUT-BREAK-IND > ' '
        {
            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Break_Ind().setValue(pdaCwfa3012.getPnd_Input_Data_Pnd_Input_Break_Ind());                                                //Natural: MOVE #INPUT-BREAK-IND TO #REP-BREAK-IND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfa3300.getPnd_Parm_Data_Pnd_Extrct_Count().equals(getZero())))                                                                                 //Natural: IF #EXTRCT-COUNT = 0
        {
            getReports().eject(1, true);                                                                                                                                  //Natural: EJECT ( 1 )
            DbsUtil.callnat(Cwfn3915.class , getCurrentProcessState(), pnd_Report_No);                                                                                    //Natural: CALLNAT 'CWFN3915' #REPORT-NO
            if (condition(Global.isEscape())) return;
            getReports().eject(1, true);                                                                                                                                  //Natural: EJECT ( 1 )
                                                                                                                                                                          //Natural: PERFORM HEADER-ROUTINE
            sub_Header_Routine();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, pdaCwfa3310.getPnd_Work_Record())))
        {
            getSort().writeSortInData(pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn(), pdaCwfa3310.getPnd_Work_Record_Rqst_Work_Prcss_Id(), pdaCwfa3310.getPnd_Work_Record_Rqst_Tiaa_Rcvd_Dte(),  //Natural: END-ALL
                pdaCwfa3310.getPnd_Work_Record_Rqst_Log_Dte_Tme(), pdaCwfa3310.getPnd_Work_Record_Last_Chnge_Invrt_Dte_Tme(), pdaCwfa3310.getPnd_Work_Record_Return_Doc_Rec_Dte_Tme(), 
                pdaCwfa3310.getPnd_Work_Record_Return_Rcvd_Dte_Tme(), pdaCwfa3310.getPnd_Work_Record_Tbl_Business_Days(), pdaCwfa3310.getPnd_Work_Record_Step_Id(), 
                pdaCwfa3310.getPnd_Work_Record_Tbl_Status_Key(), pdaCwfa3310.getPnd_Work_Record_Admin_Status_Cde(), pdaCwfa3310.getPnd_Work_Record_Admin_Status_Updte_Dte_Tme(), 
                pdaCwfa3310.getPnd_Work_Record_Tbl_Calendar_Days(), pdaCwfa3310.getPnd_Work_Record_Rqst_Pin_Nbr(), pdaCwfa3310.getPnd_Work_Record_Cntrct_Nbr(), 
                pdaCwfa3310.getPnd_Work_Record_Admin_Status_Updte_Oprtr_Cde());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn(), pdaCwfa3310.getPnd_Work_Record_Rqst_Work_Prcss_Id(), pdaCwfa3310.getPnd_Work_Record_Rqst_Tiaa_Rcvd_Dte()); //Natural: SORT BY #WPID-ACTN RQST-WORK-PRCSS-ID RQST-TIAA-RCVD-DTE USING RQST-LOG-DTE-TME LAST-CHNGE-INVRT-DTE-TME RETURN-DOC-REC-DTE-TME RETURN-RCVD-DTE-TME TBL-BUSINESS-DAYS STEP-ID TBL-STATUS-KEY ADMIN-STATUS-CDE ADMIN-STATUS-UPDTE-DTE-TME TBL-CALENDAR-DAYS RQST-PIN-NBR CNTRCT-NBR ADMIN-STATUS-UPDTE-OPRTR-CDE
        SORT01:
        while (condition(getSort().readSortOutData(pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn(), pdaCwfa3310.getPnd_Work_Record_Rqst_Work_Prcss_Id(), 
            pdaCwfa3310.getPnd_Work_Record_Rqst_Tiaa_Rcvd_Dte(), pdaCwfa3310.getPnd_Work_Record_Rqst_Log_Dte_Tme(), pdaCwfa3310.getPnd_Work_Record_Last_Chnge_Invrt_Dte_Tme(), 
            pdaCwfa3310.getPnd_Work_Record_Return_Doc_Rec_Dte_Tme(), pdaCwfa3310.getPnd_Work_Record_Return_Rcvd_Dte_Tme(), pdaCwfa3310.getPnd_Work_Record_Tbl_Business_Days(), 
            pdaCwfa3310.getPnd_Work_Record_Step_Id(), pdaCwfa3310.getPnd_Work_Record_Tbl_Status_Key(), pdaCwfa3310.getPnd_Work_Record_Admin_Status_Cde(), 
            pdaCwfa3310.getPnd_Work_Record_Admin_Status_Updte_Dte_Tme(), pdaCwfa3310.getPnd_Work_Record_Tbl_Calendar_Days(), pdaCwfa3310.getPnd_Work_Record_Rqst_Pin_Nbr(), 
            pdaCwfa3310.getPnd_Work_Record_Cntrct_Nbr(), pdaCwfa3310.getPnd_Work_Record_Admin_Status_Updte_Oprtr_Cde())))
        {
            getWorkFiles().write(5, false, pdaCwfa3310.getPnd_Work_Record());                                                                                             //Natural: WRITE WORK FILE 5 #WORK-RECORD
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        boolean endOfDataReadwork02 = true;                                                                                                                               //Natural: READ WORK 5 #WORK-RECORD
        boolean firstReadwork02 = true;
        READWORK02:
        while (condition(getWorkFiles().read(5, pdaCwfa3310.getPnd_Work_Record())))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork02();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork02 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            short decideConditionsMet584 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-CALENDAR-DAYS;//Natural: VALUE 0,1
            if (condition((pdaCwfa3310.getPnd_Work_Record_Tbl_Calendar_Days().equals(0) || pdaCwfa3310.getPnd_Work_Record_Tbl_Calendar_Days().equals(1))))
            {
                decideConditionsMet584++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet587 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTN;//Natural: VALUE 'B'
                if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("B"))))
                {
                    decideConditionsMet587++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(1).nadd(1);                                                                                   //Natural: ADD 1 TO #BOOKLET-CTR ( 1 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(7).nadd(1);                                                                                   //Natural: ADD 1 TO #BOOKLET-CTR ( 7 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("F"))))
                {
                    decideConditionsMet587++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(1).nadd(1);                                                                                     //Natural: ADD 1 TO #FORMS-CTR ( 1 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(7).nadd(1);                                                                                     //Natural: ADD 1 TO #FORMS-CTR ( 7 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("I"))))
                {
                    decideConditionsMet587++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(1).nadd(1);                                                                                   //Natural: ADD 1 TO #INQUIRE-CTR ( 1 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(7).nadd(1);                                                                                   //Natural: ADD 1 TO #INQUIRE-CTR ( 7 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("R"))))
                {
                    decideConditionsMet587++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(1).nadd(1);                                                                                  //Natural: ADD 1 TO #RESEARCH-CTR ( 1 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(7).nadd(1);                                                                                  //Natural: ADD 1 TO #RESEARCH-CTR ( 7 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("T"))))
                {
                    decideConditionsMet587++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(1).nadd(1);                                                                                     //Natural: ADD 1 TO #TRANS-CTR ( 1 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(7).nadd(1);                                                                                     //Natural: ADD 1 TO #TRANS-CTR ( 7 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("X"))))
                {
                    decideConditionsMet587++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(1).nadd(1);                                                                                 //Natural: ADD 1 TO #COMPLAINT-CTR ( 1 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(7).nadd(1);                                                                                 //Natural: ADD 1 TO #COMPLAINT-CTR ( 7 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("Z"))))
                {
                    decideConditionsMet587++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(1).nadd(1);                                                                                     //Natural: ADD 1 TO #OTHER-CTR ( 1 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(7).nadd(1);                                                                                     //Natural: ADD 1 TO #OTHER-CTR ( 7 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet587 > 0))
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count().nadd(1);                                                                                                 //Natural: ADD 1 TO #SUB-COUNT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_1().nadd(1);                                                                                             //Natural: ADD 1 TO #TOTAL-COUNT-1
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pdaCwfa3310.getPnd_Work_Record_Tbl_Calendar_Days().equals(2))))
            {
                decideConditionsMet584++;
                short decideConditionsMet616 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTN;//Natural: VALUE 'B'
                if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("B"))))
                {
                    decideConditionsMet616++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(2).nadd(1);                                                                                   //Natural: ADD 1 TO #BOOKLET-CTR ( 2 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(8).nadd(1);                                                                                   //Natural: ADD 1 TO #BOOKLET-CTR ( 8 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("F"))))
                {
                    decideConditionsMet616++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(2).nadd(1);                                                                                     //Natural: ADD 1 TO #FORMS-CTR ( 2 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(8).nadd(1);                                                                                     //Natural: ADD 1 TO #FORMS-CTR ( 8 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("I"))))
                {
                    decideConditionsMet616++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(2).nadd(1);                                                                                   //Natural: ADD 1 TO #INQUIRE-CTR ( 2 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(8).nadd(1);                                                                                   //Natural: ADD 1 TO #INQUIRE-CTR ( 8 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("R"))))
                {
                    decideConditionsMet616++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(2).nadd(1);                                                                                  //Natural: ADD 1 TO #RESEARCH-CTR ( 2 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(8).nadd(1);                                                                                  //Natural: ADD 1 TO #RESEARCH-CTR ( 8 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("T"))))
                {
                    decideConditionsMet616++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(2).nadd(1);                                                                                     //Natural: ADD 1 TO #TRANS-CTR ( 2 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(8).nadd(1);                                                                                     //Natural: ADD 1 TO #TRANS-CTR ( 8 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("X"))))
                {
                    decideConditionsMet616++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(2).nadd(1);                                                                                 //Natural: ADD 1 TO #COMPLAINT-CTR ( 2 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(8).nadd(1);                                                                                 //Natural: ADD 1 TO #COMPLAINT-CTR ( 8 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("Z"))))
                {
                    decideConditionsMet616++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(2).nadd(1);                                                                                     //Natural: ADD 1 TO #OTHER-CTR ( 2 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(8).nadd(1);                                                                                     //Natural: ADD 1 TO #OTHER-CTR ( 8 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet616 > 0))
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count().nadd(1);                                                                                                 //Natural: ADD 1 TO #SUB-COUNT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_2().nadd(1);                                                                                             //Natural: ADD 1 TO #TOTAL-COUNT-2
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pdaCwfa3310.getPnd_Work_Record_Tbl_Calendar_Days().equals(3))))
            {
                decideConditionsMet584++;
                short decideConditionsMet645 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTN;//Natural: VALUE 'B'
                if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("B"))))
                {
                    decideConditionsMet645++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(3).nadd(1);                                                                                   //Natural: ADD 1 TO #BOOKLET-CTR ( 3 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(9).nadd(1);                                                                                   //Natural: ADD 1 TO #BOOKLET-CTR ( 9 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("F"))))
                {
                    decideConditionsMet645++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(3).nadd(1);                                                                                     //Natural: ADD 1 TO #FORMS-CTR ( 3 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(9).nadd(1);                                                                                     //Natural: ADD 1 TO #FORMS-CTR ( 9 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("I"))))
                {
                    decideConditionsMet645++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(3).nadd(1);                                                                                   //Natural: ADD 1 TO #INQUIRE-CTR ( 3 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(9).nadd(1);                                                                                   //Natural: ADD 1 TO #INQUIRE-CTR ( 9 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("R"))))
                {
                    decideConditionsMet645++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(3).nadd(1);                                                                                  //Natural: ADD 1 TO #RESEARCH-CTR ( 3 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(9).nadd(1);                                                                                  //Natural: ADD 1 TO #RESEARCH-CTR ( 9 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("T"))))
                {
                    decideConditionsMet645++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(3).nadd(1);                                                                                     //Natural: ADD 1 TO #TRANS-CTR ( 3 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(9).nadd(1);                                                                                     //Natural: ADD 1 TO #TRANS-CTR ( 9 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("X"))))
                {
                    decideConditionsMet645++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(3).nadd(1);                                                                                 //Natural: ADD 1 TO #COMPLAINT-CTR ( 3 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(9).nadd(1);                                                                                 //Natural: ADD 1 TO #COMPLAINT-CTR ( 9 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("Z"))))
                {
                    decideConditionsMet645++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(3).nadd(1);                                                                                     //Natural: ADD 1 TO #OTHER-CTR ( 3 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(9).nadd(1);                                                                                     //Natural: ADD 1 TO #OTHER-CTR ( 9 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet645 > 0))
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count().nadd(1);                                                                                                 //Natural: ADD 1 TO #SUB-COUNT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_3().nadd(1);                                                                                             //Natural: ADD 1 TO #TOTAL-COUNT-3
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pdaCwfa3310.getPnd_Work_Record_Tbl_Calendar_Days().equals(4))))
            {
                decideConditionsMet584++;
                short decideConditionsMet674 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTN;//Natural: VALUE 'B'
                if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("B"))))
                {
                    decideConditionsMet674++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(4).nadd(1);                                                                                   //Natural: ADD 1 TO #BOOKLET-CTR ( 4 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(10).nadd(1);                                                                                  //Natural: ADD 1 TO #BOOKLET-CTR ( 10 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("F"))))
                {
                    decideConditionsMet674++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(4).nadd(1);                                                                                     //Natural: ADD 1 TO #FORMS-CTR ( 4 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(10).nadd(1);                                                                                    //Natural: ADD 1 TO #FORMS-CTR ( 10 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("I"))))
                {
                    decideConditionsMet674++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(4).nadd(1);                                                                                   //Natural: ADD 1 TO #INQUIRE-CTR ( 4 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(10).nadd(1);                                                                                  //Natural: ADD 1 TO #INQUIRE-CTR ( 10 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("R"))))
                {
                    decideConditionsMet674++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(4).nadd(1);                                                                                  //Natural: ADD 1 TO #RESEARCH-CTR ( 4 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(10).nadd(1);                                                                                 //Natural: ADD 1 TO #RESEARCH-CTR ( 10 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("T"))))
                {
                    decideConditionsMet674++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(4).nadd(1);                                                                                     //Natural: ADD 1 TO #TRANS-CTR ( 4 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(10).nadd(1);                                                                                    //Natural: ADD 1 TO #TRANS-CTR ( 10 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("X"))))
                {
                    decideConditionsMet674++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(4).nadd(1);                                                                                 //Natural: ADD 1 TO #COMPLAINT-CTR ( 4 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(10).nadd(1);                                                                                //Natural: ADD 1 TO #COMPLAINT-CTR ( 10 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("Z"))))
                {
                    decideConditionsMet674++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(4).nadd(1);                                                                                     //Natural: ADD 1 TO #OTHER-CTR ( 4 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(10).nadd(1);                                                                                    //Natural: ADD 1 TO #OTHER-CTR ( 10 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet674 > 0))
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count().nadd(1);                                                                                                 //Natural: ADD 1 TO #SUB-COUNT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_4().nadd(1);                                                                                             //Natural: ADD 1 TO #TOTAL-COUNT-4
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pdaCwfa3310.getPnd_Work_Record_Tbl_Calendar_Days().equals(5))))
            {
                decideConditionsMet584++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet704 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTN;//Natural: VALUE 'B'
                if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("B"))))
                {
                    decideConditionsMet704++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(5).nadd(1);                                                                                   //Natural: ADD 1 TO #BOOKLET-CTR ( 5 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(11).nadd(1);                                                                                  //Natural: ADD 1 TO #BOOKLET-CTR ( 11 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("F"))))
                {
                    decideConditionsMet704++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(5).nadd(1);                                                                                     //Natural: ADD 1 TO #FORMS-CTR ( 5 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(11).nadd(1);                                                                                    //Natural: ADD 1 TO #FORMS-CTR ( 11 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("I"))))
                {
                    decideConditionsMet704++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(5).nadd(1);                                                                                   //Natural: ADD 1 TO #INQUIRE-CTR ( 5 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(11).nadd(1);                                                                                  //Natural: ADD 1 TO #INQUIRE-CTR ( 11 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("R"))))
                {
                    decideConditionsMet704++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(5).nadd(1);                                                                                  //Natural: ADD 1 TO #RESEARCH-CTR ( 5 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(11).nadd(1);                                                                                 //Natural: ADD 1 TO #RESEARCH-CTR ( 11 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("T"))))
                {
                    decideConditionsMet704++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(5).nadd(1);                                                                                     //Natural: ADD 1 TO #TRANS-CTR ( 5 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(11).nadd(1);                                                                                    //Natural: ADD 1 TO #TRANS-CTR ( 11 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("X"))))
                {
                    decideConditionsMet704++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(5).nadd(1);                                                                                 //Natural: ADD 1 TO #COMPLAINT-CTR ( 5 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(11).nadd(1);                                                                                //Natural: ADD 1 TO #COMPLAINT-CTR ( 11 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("Z"))))
                {
                    decideConditionsMet704++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(5).nadd(1);                                                                                     //Natural: ADD 1 TO #OTHER-CTR ( 5 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(11).nadd(1);                                                                                    //Natural: ADD 1 TO #OTHER-CTR ( 11 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet704 > 0))
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count().nadd(1);                                                                                                 //Natural: ADD 1 TO #SUB-COUNT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_5().nadd(1);                                                                                             //Natural: ADD 1 TO #TOTAL-COUNT-5
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 6:9999
            else if (condition(((pdaCwfa3310.getPnd_Work_Record_Tbl_Calendar_Days().greaterOrEqual(6) && pdaCwfa3310.getPnd_Work_Record_Tbl_Calendar_Days().lessOrEqual(9999)))))
            {
                decideConditionsMet584++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet734 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTN;//Natural: VALUE 'B'
                if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("B"))))
                {
                    decideConditionsMet734++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(6).nadd(1);                                                                                   //Natural: ADD 1 TO #BOOKLET-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(12).nadd(1);                                                                                  //Natural: ADD 1 TO #BOOKLET-CTR ( 12 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("F"))))
                {
                    decideConditionsMet734++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(6).nadd(1);                                                                                     //Natural: ADD 1 TO #FORMS-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(12).nadd(1);                                                                                    //Natural: ADD 1 TO #FORMS-CTR ( 12 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("I"))))
                {
                    decideConditionsMet734++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(6).nadd(1);                                                                                   //Natural: ADD 1 TO #INQUIRE-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(12).nadd(1);                                                                                  //Natural: ADD 1 TO #INQUIRE-CTR ( 12 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("R"))))
                {
                    decideConditionsMet734++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(6).nadd(1);                                                                                  //Natural: ADD 1 TO #RESEARCH-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(12).nadd(1);                                                                                 //Natural: ADD 1 TO #RESEARCH-CTR ( 12 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("T"))))
                {
                    decideConditionsMet734++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(6).nadd(1);                                                                                     //Natural: ADD 1 TO #TRANS-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(12).nadd(1);                                                                                    //Natural: ADD 1 TO #TRANS-CTR ( 12 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("X"))))
                {
                    decideConditionsMet734++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(6).nadd(1);                                                                                 //Natural: ADD 1 TO #COMPLAINT-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(12).nadd(1);                                                                                //Natural: ADD 1 TO #COMPLAINT-CTR ( 12 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().equals("Z"))))
                {
                    decideConditionsMet734++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(6).nadd(1);                                                                                     //Natural: ADD 1 TO #OTHER-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(12).nadd(1);                                                                                    //Natural: ADD 1 TO #OTHER-CTR ( 12 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet734 > 0))
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count().nadd(1);                                                                                                 //Natural: ADD 1 TO #SUB-COUNT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_Over().nadd(1);                                                                                          //Natural: ADD 1 TO #TOTAL-COUNT-OVER
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //* ***  RQST TIAA RECEIVED DATE (FIELD 1)*******                                                                                                             //Natural: AT TOP OF PAGE ( 1 )
            pnd_Rqst_Tiaa_Rcvd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pdaCwfa3310.getPnd_Work_Record_Rqst_Tiaa_Rcvd_Dte());                                    //Natural: MOVE EDITED RQST-TIAA-RCVD-DTE TO #RQST-TIAA-RCVD-DTE ( EM = YYYYMMDD )
            //* ***  DOCUMENT  RECEIVED DATE (FIELD 2)*******
            if (condition(pdaCwfa3310.getPnd_Work_Record_Return_Doc_Rec_Dte_Tme().greater(getZero())))                                                                    //Natural: IF #WORK-RECORD.RETURN-DOC-REC-DTE-TME GT 0
            {
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Return_Doc_Txt().setValueEdited(pdaCwfa3310.getPnd_Work_Record_Return_Doc_Rec_Dte_Tme(),new ReportEditMask("MM/DD/YY")); //Natural: MOVE EDITED #WORK-RECORD.RETURN-DOC-REC-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Return_Doc_Txt().setValue(" ");                                                                                          //Natural: MOVE ' ' TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: END-IF
            //* ***  RECIIVED IN UNIT (FIELD 3)****
            if (condition(pdaCwfa3310.getPnd_Work_Record_Return_Rcvd_Dte_Tme().greater(getZero())))                                                                       //Natural: IF #WORK-RECORD.RETURN-RCVD-DTE-TME GT 0
            {
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Return_Rcvd_Txt().setValueEdited(pdaCwfa3310.getPnd_Work_Record_Return_Rcvd_Dte_Tme(),new ReportEditMask("MM/DD/YY"));   //Natural: MOVE EDITED #WORK-RECORD.RETURN-RCVD-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Return_Rcvd_Txt().setValue(" ");                                                                                         //Natural: MOVE ' ' TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: END-IF
            //* ***  WPID DESCRIPTION (FIELD 5)****
            DbsUtil.callnat(Cwfn5390.class , getCurrentProcessState(), pdaCwfa3310.getPnd_Work_Record_Rqst_Work_Prcss_Id(), pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Sname()); //Natural: CALLNAT 'CWFN5390' #WORK-RECORD.RQST-WORK-PRCSS-ID #WPID-SNAME
            if (condition(Global.isEscape())) return;
            //* ***  STATUS DESCRIPTION (FIELD 7)******
            DbsUtil.callnat(Cwfn1102.class , getCurrentProcessState(), pdaCwfa3310.getPnd_Work_Record_Tbl_Status_Key(), pdaCwfa3310.getPnd_Misc_Parm_Pnd_Status_Sname()); //Natural: CALLNAT 'CWFN1102' #WORK-RECORD.TBL-STATUS-KEY #STATUS-SNAME
            if (condition(Global.isEscape())) return;
            //* ***  PARTICIPANT NAME (FIELD 11)*******
            //* ***  EMPLOYEE NAME (FIELD 14)*******
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Partic_Sname().reset();                                                                                                      //Natural: RESET #PARTIC-SNAME
            pdaCwfa5372.getCwfa5372_Pnd_Pin_Key().setValue(pdaCwfa3310.getPnd_Work_Record_Rqst_Pin_Nbr());                                                                //Natural: ASSIGN CWFA5372.#PIN-KEY := #WORK-RECORD.RQST-PIN-NBR
            DbsUtil.callnat(Cwfn5372.class , getCurrentProcessState(), pdaCwfa5372.getCwfa5372_Pnd_Pin_Key(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Name(),                    //Natural: CALLNAT 'CWFN5372' CWFA5372.#PIN-KEY CWFA5372.#PASS-NAME CWFA5372.#PASS-SSN CWFA5372.#PASS-TLC CWFA5372.#PASS-DOB CWFA5372.#PASS-FOUND
                pdaCwfa5372.getCwfa5372_Pnd_Pass_Ssn(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Tlc(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Dob(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Found());
            if (condition(Global.isEscape())) return;
            if (condition(pdaCwfa5372.getCwfa5372_Pnd_Pass_Found().getBoolean()))                                                                                         //Natural: IF #PASS-FOUND
            {
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Partic_Sname().setValue(pdaCwfa5372.getCwfa5372_Pnd_Pass_Name());                                                        //Natural: MOVE CWFA5372.#PASS-NAME TO #PARTIC-SNAME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Partic_Sname().setValue("INVALID");                                                                                      //Natural: MOVE 'INVALID' TO #PARTIC-SNAME
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaCwfa3310.getPnd_Work_Record_Admin_Status_Updte_Oprtr_Cde().greater(" ")))                                                                    //Natural: IF #WORK-RECORD.ADMIN-STATUS-UPDTE-OPRTR-CDE GT ' '
            {
                DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), pdaCwfa3310.getPnd_Work_Record_Admin_Status_Updte_Oprtr_Cde(), pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Empl_Name_Detail_Line()); //Natural: CALLNAT 'CWFN1107' #WORK-RECORD.ADMIN-STATUS-UPDTE-OPRTR-CDE #REP-EMPL-NAME-DETAIL-LINE
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            //* ***  CHECK WPID ACTION CODE (FIRST BYTE OF WPID) *******
            if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Save_Action().notEquals(pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn())))                                      //Natural: IF #SAVE-ACTION NE #WPID-ACTN
            {
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Save_Action().setValue(pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn());                                                  //Natural: MOVE #WPID-ACTN TO #SAVE-ACTION
                short decideConditionsMet847 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #SAVE-ACTION;//Natural: VALUE 'B'
                if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Save_Action().equals("B"))))
                {
                    decideConditionsMet847++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"BOOKLETS    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'BOOKLETS    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Save_Action().equals("F"))))
                {
                    decideConditionsMet847++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"FORMS       :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'FORMS       :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Save_Action().equals("I"))))
                {
                    decideConditionsMet847++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"INQUIRY     :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'INQUIRY     :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Save_Action().equals("R"))))
                {
                    decideConditionsMet847++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"RESEARCH    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'RESEARCH    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Save_Action().equals("T"))))
                {
                    decideConditionsMet847++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TRANSACTIONS:",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'TRANSACTIONS:' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Save_Action().equals("X"))))
                {
                    decideConditionsMet847++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"COMPLAINTS  :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'COMPLAINTS  :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Save_Action().equals("Z"))))
                {
                    decideConditionsMet847++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"OTHER       :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'OTHER       :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "//RECEIVED/AT TIAA",                                                                                                                 //Natural: DISPLAY ( 1 ) '//RECEIVED/AT TIAA' #RQST-TIAA-RCVD-DTE ( EM = MM/DD/YY ) '//DOC/REC"D' #RETURN-DOC-TXT '//REC"D IN/UNIT' #RETURN-RCVD-TXT 'BUSINESS/DAYS/IN/UNIT' #WORK-RECORD.TBL-BUSINESS-DAYS ( EM = Z9.9 ) '//WORK/PROCESS' #WPID-SNAME ( IS = ON ) '//WORK/STEP' #WORK-RECORD.STEP-ID ( IS = ON ) '///STATUS' #STATUS-SNAME ( AL = 15 ) '//STATUS/DATE' #WORK-RECORD.ADMIN-STATUS-UPDTE-DTE-TME ( EM = MM/DD/YY ) 'CALENDAR/DAYS/IN/TIAA' #WORK-RECORD.TBL-CALENDAR-DAYS ( EM = Z,ZZ9 ) '//PARTIC/NAME' #PARTIC-SNAME '///PIN' #WORK-RECORD.RQST-PIN-NBR '//CONTRACT/NUMBER' #WORK-RECORD.CNTRCT-NBR '///EMPLOYEE' #WORK-RECORD.ADMIN-STATUS-UPDTE-OPRTR-CDE
            		pnd_Rqst_Tiaa_Rcvd_Dte, new ReportEditMask ("MM/DD/YY"),"//DOC/REC'D",
            		pdaCwfa3310.getPnd_Misc_Parm_Pnd_Return_Doc_Txt(),"//REC'D IN/UNIT",
            		pdaCwfa3310.getPnd_Misc_Parm_Pnd_Return_Rcvd_Txt(),"BUSINESS/DAYS/IN/UNIT",
            		pdaCwfa3310.getPnd_Work_Record_Tbl_Business_Days(), new ReportEditMask ("Z9.9"),"//WORK/PROCESS",
            		pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Sname(), new IdenticalSuppress(true),"//WORK/STEP",
            		pdaCwfa3310.getPnd_Work_Record_Step_Id(), new IdenticalSuppress(true),"///STATUS",
            		pdaCwfa3310.getPnd_Misc_Parm_Pnd_Status_Sname(), new AlphanumericLength (15),"//STATUS/DATE",
            		pdaCwfa3310.getPnd_Work_Record_Admin_Status_Updte_Dte_Tme(), new ReportEditMask ("MM/DD/YY"),"CALENDAR/DAYS/IN/TIAA",
            		pdaCwfa3310.getPnd_Work_Record_Tbl_Calendar_Days(), new ReportEditMask ("Z,ZZ9"),"//PARTIC/NAME",
            		pdaCwfa3310.getPnd_Misc_Parm_Pnd_Partic_Sname(),"///PIN",
            		pdaCwfa3310.getPnd_Work_Record_Rqst_Pin_Nbr(),"//CONTRACT/NUMBER",
            		pdaCwfa3310.getPnd_Work_Record_Cntrct_Nbr(),"///EMPLOYEE",
            		pdaCwfa3310.getPnd_Work_Record_Admin_Status_Updte_Oprtr_Cde());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #WORK-RECORD.RQST-WORK-PRCSS-ID;//Natural: AT BREAK OF #WPID-ACTN;//Natural: AT END OF DATA
            readWork02Rqst_Work_Prcss_IdOld.setValue(pdaCwfa3310.getPnd_Work_Record_Rqst_Work_Prcss_Id());                                                                //Natural: END-WORK
            readWork02Pnd_Wpid_ActnOld.setValue(pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn());
        }
        READWORK02_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            readWork02Rqst_Work_Prcss_IdCount869.resetBreak();
            readWork02Rqst_Work_Prcss_IdCount.resetBreak();
            atBreakEventReadwork02(endOfDataReadwork02);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count().equals(getZero())))                                                                              //Natural: IF #TOTAL-COUNT = 0
            {
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_1().setValue(0);                                                                                            //Natural: MOVE 0 TO #PTOTAL-COUNT-1 #PTOTAL-COUNT-2 #PTOTAL-COUNT-3 #PTOTAL-COUNT-4 #PTOTAL-COUNT-5 #PTOTAL-COUNT-OVER
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_2().setValue(0);
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_3().setValue(0);
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_4().setValue(0);
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_5().setValue(0);
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_Over().setValue(0);
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_1().compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_1()),                 //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-1 = ( #TOTAL-COUNT-1 / #TOTAL-COUNT ) * 100
                    (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_1().divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count())).multiply(100));
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_2().compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_2()),                 //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-2 = ( #TOTAL-COUNT-2 / #TOTAL-COUNT ) * 100
                    (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_2().divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count())).multiply(100));
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_3().compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_3()),                 //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-3 = ( #TOTAL-COUNT-3 / #TOTAL-COUNT ) * 100
                    (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_3().divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count())).multiply(100));
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_4().compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_4()),                 //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-4 = ( #TOTAL-COUNT-4 / #TOTAL-COUNT ) * 100
                    (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_4().divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count())).multiply(100));
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_5().compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_5()),                 //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-5 = ( #TOTAL-COUNT-5 / #TOTAL-COUNT ) * 100
                    (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_5().divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count())).multiply(100));
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_Over().compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_Over()),           //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-OVER = ( #TOTAL-COUNT-OVER / #TOTAL-COUNT ) * 100
                    (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_Over().divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count())).multiply(100));
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"GRAND TOTAL OF WORK REQUESTS REACHING SPECIFIED STATUS IN 1 DAY.","..........",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_1()," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_1(),  //Natural: WRITE ( 1 ) // 'GRAND TOTAL OF WORK REQUESTS REACHING SPECIFIED STATUS IN 1 DAY.' '..........' #TOTAL-COUNT-1 ' (' #PTOTAL-COUNT-1 ( EM = ZZ9.999 ) '% )' / 'GRAND TOTAL OF WORK REQUESTS REACHING SPECIFIED STATUS IN 2 DAYS' '..........' #TOTAL-COUNT-2 ' (' #PTOTAL-COUNT-2 ( EM = ZZ9.999 ) '% )' / 'GRAND TOTAL OF WORK REQUESTS REACHING SPECIFIED STATUS IN 3 DAYS' '..........' #TOTAL-COUNT-3 ' (' #PTOTAL-COUNT-3 ( EM = ZZ9.999 ) '% )' / 'GRAND TOTAL OF WORK REQUESTS REACHING SPECIFIED STATUS IN 4 DAYS' '..........' #TOTAL-COUNT-4 ' (' #PTOTAL-COUNT-4 ( EM = ZZ9.999 ) '% )' / 'GRAND TOTAL OF WORK REQUESTS REACHING SPECIFIED STATUS IN 5 DAYS' '..........' #TOTAL-COUNT-5 ' (' #PTOTAL-COUNT-5 ( EM = ZZ9.999 ) '% )' / 'GRAND TOTAL OF WORK REQUESTS REACHING SPECIFIED STATUS IN OVER 5' ' DAYS.....' #TOTAL-COUNT-OVER ' (' #PTOTAL-COUNT-OVER ( EM = ZZ9.999 ) '% )' / 'GRAND TOTAL....................................................' '...........' #TOTAL-COUNT /
                new ReportEditMask ("ZZ9.999"),"% )",NEWLINE,"GRAND TOTAL OF WORK REQUESTS REACHING SPECIFIED STATUS IN 2 DAYS","..........",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_2()," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_2(), 
                new ReportEditMask ("ZZ9.999"),"% )",NEWLINE,"GRAND TOTAL OF WORK REQUESTS REACHING SPECIFIED STATUS IN 3 DAYS","..........",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_3()," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_3(), 
                new ReportEditMask ("ZZ9.999"),"% )",NEWLINE,"GRAND TOTAL OF WORK REQUESTS REACHING SPECIFIED STATUS IN 4 DAYS","..........",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_4()," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_4(), 
                new ReportEditMask ("ZZ9.999"),"% )",NEWLINE,"GRAND TOTAL OF WORK REQUESTS REACHING SPECIFIED STATUS IN 5 DAYS","..........",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_5()," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_5(), 
                new ReportEditMask ("ZZ9.999"),"% )",NEWLINE,"GRAND TOTAL OF WORK REQUESTS REACHING SPECIFIED STATUS IN OVER 5"," DAYS.....",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_Over()," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_Over(), 
                new ReportEditMask ("ZZ9.999"),"% )",NEWLINE,"GRAND TOTAL....................................................","...........",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count(),
                NEWLINE);
            if (condition(Global.isEscape())) return;
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Array_Ctrs().getValue("*").reset();                                                                                          //Natural: RESET #ARRAY-CTRS ( * )
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Percent_Ctrs().getValue("*").reset();                                                                                        //Natural: RESET #PERCENT-CTRS ( * )
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue("*").reset();                                                                                         //Natural: RESET #BOOKLET-CTR ( * ) #FORMS-CTR ( * ) #INQUIRE-CTR ( * ) #RESEARCH-CTR ( * ) #TRANS-CTR ( * ) #COMPLAINT-CTR ( * ) #OTHER-CTR ( * ) #SUB-COUNT #TOTAL-COUNT #TOTAL-COUNT-1 #TOTAL-COUNT-2 #TOTAL-COUNT-3 #TOTAL-COUNT-4 #TOTAL-COUNT-5 #TOTAL-COUNT-OVER #TOTAL-COUNT #PTOTAL-COUNT-1 #PTOTAL-COUNT-2 #PTOTAL-COUNT-3 #PTOTAL-COUNT-4 #PTOTAL-COUNT-5 #PTOTAL-COUNT-OVER #TOTAL-CTRS
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue("*").reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue("*").reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue("*").reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue("*").reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue("*").reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue("*").reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_1().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_2().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_3().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_4().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_5().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count_Over().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_1().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_2().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_3().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_4().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_5().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptotal_Count_Over().reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Ctrs().reset();
            getReports().eject(1, true);                                                                                                                                  //Natural: EJECT ( 1 )
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HEADER-ROUTINE
    }
    private void sub_Header_Routine() throws Exception                                                                                                                    //Natural: HEADER-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Page.nadd(1);                                                                                                                                                 //Natural: COMPUTE #PAGE = #PAGE + 1
        getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Env,new TabSetting(52),"CORPORATE WORKFLOW FACILITIES",new TabSetting(124),"PAGE",pnd_Page,    //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 52T 'CORPORATE WORKFLOW FACILITIES' 124T 'PAGE' #PAGE ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 45T 'REPORT OF CASES WITH SPECIFIED STATUS' 124T *TIMX ( EM = HH':'II' 'AP ) / 58T 'TURN AROUND REPORT'
            new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
            TabSetting(45),"REPORT OF CASES WITH SPECIFIED STATUS",new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new 
            TabSetting(58),"TURN AROUND REPORT");
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pdaCwfa3300.getPnd_Parm_Data_Pnd_Rqst_Unit(), pnd_Unit_Name);                                          //Natural: CALLNAT 'CWFN1103' #RQST-UNIT #UNIT-NAME
        if (condition(Global.isEscape())) return;
        getReports().write(1, ReportOption.NOTITLE,"REQUESTING UNIT:",pdaCwfa3300.getPnd_Parm_Data_Pnd_Rqst_Unit(),pnd_Unit_Name,"  OPTIONAL: SELECT ON THE FOLLOWING STATUSES:"); //Natural: WRITE ( 1 ) 'REQUESTING UNIT:' #RQST-UNIT #UNIT-NAME '  OPTIONAL: SELECT ON THE FOLLOWING STATUSES:'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(85),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(1),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(2), //Natural: WRITE ( 1 ) 85X #REP-STATUS-CDE ( 01 ) #REP-STATUS-CDE ( 02 ) #REP-STATUS-CDE ( 03 ) #REP-STATUS-CDE ( 04 ) #REP-STATUS-CDE ( 05 )
            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(3),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(4),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(5));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"FOR:                         ",new ColumnSpacing(56),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(6),    //Natural: WRITE ( 1 ) 'FOR:                         ' 56X #REP-STATUS-CDE ( 06 ) #REP-STATUS-CDE ( 07 ) #REP-STATUS-CDE ( 08 ) #REP-STATUS-CDE ( 09 ) #REP-STATUS-CDE ( 10 )
            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(7),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(8),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(9),
            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(10));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(6),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(1),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(2),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(3),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(4),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(5),new  //Natural: WRITE ( 1 ) 6T #REP-UNIT-CDE ( 01 ) #REP-UNIT-CDE ( 02 ) #REP-UNIT-CDE ( 03 ) #REP-UNIT-CDE ( 04 ) #REP-UNIT-CDE ( 05 ) 30X
            ColumnSpacing(30));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(6),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(6),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(7), //Natural: WRITE ( 1 ) 6T #REP-UNIT-CDE ( 06 ) #REP-UNIT-CDE ( 07 ) #REP-UNIT-CDE ( 08 ) #REP-UNIT-CDE ( 09 ) #REP-UNIT-CDE ( 10 ) #REP-UNIT-CDE ( 11 ) /
            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(8),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(9),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(10),
            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(11),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"START DATE:",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Work_Start_Date_A(),new ColumnSpacing(40),"STATUS RANGE:  FROM",        //Natural: WRITE ( 1 ) 'START DATE:' #WORK-START-DATE-A 40X 'STATUS RANGE:  FROM' #REP-STATUS-FROM-1 '  OPTIONAL ADDITIONAL RANGE:  FROM' #REP-STATUS-FROM-2
            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_From_1(),"  OPTIONAL ADDITIONAL RANGE:  FROM",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_From_2());
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"END DATE  :",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Work_End_Date_A(),new ColumnSpacing(57),"TO",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_To_1(),new  //Natural: WRITE ( 1 ) 'END DATE  :' #WORK-END-DATE-A 57X 'TO' #REP-STATUS-TO-1 33X 'TO' #REP-STATUS-TO-2 /
            ColumnSpacing(33),"TO",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_To_2(),NEWLINE);
        if (Global.isEscape()) return;
        if (condition(pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde().greater(" ")))                                                                            //Natural: IF #REP-WPID-ACTN-RQSTD-CDE GT ' '
        {
            DbsUtil.callnat(Cwfn1105.class , getCurrentProcessState(), pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid(), pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid_Name());      //Natural: CALLNAT 'CWFN1105' #REP-WPID #REP-WPID-NAME
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Racf_Id().greater(" ")))                                                                                        //Natural: IF #REP-RACF-ID GT ' '
        {
            DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Racf_Id(), pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Empl_Name());   //Natural: CALLNAT 'CWFN1107' #REP-RACF-ID #REP-EMPL-NAME
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,"FOR WPID :",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde(),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid_Lob(),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid_Mbp(),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid_Sbp(),"(OPTIONAL)",NEWLINE,new  //Natural: WRITE ( 1 ) 'FOR WPID :' #REP-WPID-ACTN-RQSTD-CDE #REP-WPID-LOB #REP-WPID-MBP #REP-WPID-SBP '(OPTIONAL)'/ 61T '(OPTIONAL)' 'EMPLOYEE  :' #REP-EMPL-NAME
            TabSetting(61),"(OPTIONAL)","EMPLOYEE  :",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Empl_Name());
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"SUPPRESS WPID SUBTOTALS :",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Break_Ind());                                          //Natural: WRITE ( 1 ) 'SUPPRESS WPID SUBTOTALS :' #REP-BREAK-IND
        if (Global.isEscape()) return;
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page.nadd(1);                                                                                                                                     //Natural: COMPUTE #PAGE = #PAGE + 1
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Env,new TabSetting(52),"CORPORATE WORKFLOW FACILITIES",new TabSetting(124),"PAGE",pnd_Page,  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 52T 'CORPORATE WORKFLOW FACILITIES' 124T 'PAGE' #PAGE ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 45T 'REPORT OF CASES WITH SPECIFIED STATUS' 124T *TIMX ( EM = HH':'II' 'AP ) / 58T 'TURN AROUND REPORT'
                        new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(45),"REPORT OF CASES WITH SPECIFIED STATUS",new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new 
                        TabSetting(58),"TURN AROUND REPORT");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    if (condition(pnd_Page.equals(1)))                                                                                                                    //Natural: IF #PAGE = 1
                    {
                        DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pdaCwfa3300.getPnd_Parm_Data_Pnd_Rqst_Unit(), pnd_Unit_Name);                          //Natural: CALLNAT 'CWFN1103' #RQST-UNIT #UNIT-NAME
                        if (condition(Global.isEscape())) return;
                        getReports().write(1, ReportOption.NOTITLE,"REQUESTING UNIT:",pdaCwfa3300.getPnd_Parm_Data_Pnd_Rqst_Unit(),pnd_Unit_Name,"  OPTIONAL: SELECT ON THE FOLLOWING STATUSES:"); //Natural: WRITE ( 1 ) 'REQUESTING UNIT:' #RQST-UNIT #UNIT-NAME '  OPTIONAL: SELECT ON THE FOLLOWING STATUSES:'
                        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(85),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(1),                    //Natural: WRITE ( 1 ) 85X #REP-STATUS-CDE ( 01 ) #REP-STATUS-CDE ( 02 ) #REP-STATUS-CDE ( 03 ) #REP-STATUS-CDE ( 04 ) #REP-STATUS-CDE ( 05 )
                            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(2),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(3),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(4),
                            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(5));
                        getReports().write(1, ReportOption.NOTITLE,"FOR:                         ",new ColumnSpacing(56),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(6), //Natural: WRITE ( 1 ) 'FOR:                         ' 56X #REP-STATUS-CDE ( 06 ) #REP-STATUS-CDE ( 07 ) #REP-STATUS-CDE ( 08 ) #REP-STATUS-CDE ( 09 ) #REP-STATUS-CDE ( 10 )
                            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(7),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(8),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(9),
                            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_Cde().getValue(10));
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(6),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(1),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(2),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(3),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(4),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(5),new  //Natural: WRITE ( 1 ) 6T #REP-UNIT-CDE ( 01 ) #REP-UNIT-CDE ( 02 ) #REP-UNIT-CDE ( 03 ) #REP-UNIT-CDE ( 04 ) #REP-UNIT-CDE ( 05 ) 30X
                            ColumnSpacing(30));
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(6),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(6),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(7), //Natural: WRITE ( 1 ) 6T #REP-UNIT-CDE ( 06 ) #REP-UNIT-CDE ( 07 ) #REP-UNIT-CDE ( 08 ) #REP-UNIT-CDE ( 09 ) #REP-UNIT-CDE ( 10 ) #REP-UNIT-CDE ( 11 ) /
                            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(8),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(9),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(10),
                            pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Unit_Cde().getValue(11),NEWLINE);
                        getReports().write(1, ReportOption.NOTITLE,"START DATE:",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Work_Start_Date_A(),new ColumnSpacing(40),              //Natural: WRITE ( 1 ) 'START DATE:' #WORK-START-DATE-A 40X 'STATUS RANGE:  FROM' #REP-STATUS-FROM-1 '  OPTIONAL ADDITIONAL RANGE:  FROM' #REP-STATUS-FROM-2
                            "STATUS RANGE:  FROM",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_From_1(),"  OPTIONAL ADDITIONAL RANGE:  FROM",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_From_2());
                        getReports().write(1, ReportOption.NOTITLE,"END DATE  :",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Work_End_Date_A(),new ColumnSpacing(57),"TO",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_To_1(),new  //Natural: WRITE ( 1 ) 'END DATE  :' #WORK-END-DATE-A 57X 'TO' #REP-STATUS-TO-1 33X 'TO' #REP-STATUS-TO-2 /
                            ColumnSpacing(33),"TO",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Status_To_2(),NEWLINE);
                        if (condition(pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde().greater(" ")))                                                            //Natural: IF #REP-WPID-ACTN-RQSTD-CDE GT ' '
                        {
                            DbsUtil.callnat(Cwfn1105.class , getCurrentProcessState(), pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid(), pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid_Name()); //Natural: CALLNAT 'CWFN1105' #REP-WPID #REP-WPID-NAME
                            if (condition(Global.isEscape())) return;
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Racf_Id().greater(" ")))                                                                        //Natural: IF #REP-RACF-ID GT ' '
                        {
                            DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Racf_Id(), pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Empl_Name()); //Natural: CALLNAT 'CWFN1107' #REP-RACF-ID #REP-EMPL-NAME
                            if (condition(Global.isEscape())) return;
                        }                                                                                                                                                 //Natural: END-IF
                        getReports().write(1, ReportOption.NOTITLE,"FOR WPID :",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde(),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid_Lob(),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid_Mbp(),pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Wpid_Sbp(),"(OPTIONAL)",NEWLINE,new  //Natural: WRITE ( 1 ) 'FOR WPID :' #REP-WPID-ACTN-RQSTD-CDE #REP-WPID-LOB #REP-WPID-MBP #REP-WPID-SBP '(OPTIONAL)'/ 61T '(OPTIONAL)' 'EMPLOYEE  :' #REP-EMPL-NAME
                            TabSetting(61),"(OPTIONAL)","EMPLOYEE  :",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Empl_Name());
                        getReports().write(1, ReportOption.NOTITLE,"SUPPRESS WPID SUBTOTALS :",pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Break_Ind());                          //Natural: WRITE ( 1 ) 'SUPPRESS WPID SUBTOTALS :' #REP-BREAK-IND
                        getReports().skip(1, 1);                                                                                                                          //Natural: SKIP ( 1 ) 1
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork02() throws Exception {atBreakEventReadwork02(false);}
    private void atBreakEventReadwork02(boolean endOfData) throws Exception
    {
        boolean pdaCwfa3310_getPnd_Work_Record_Rqst_Work_Prcss_IdIsBreak = pdaCwfa3310.getPnd_Work_Record_Rqst_Work_Prcss_Id().isBreak(endOfData);
        boolean pdaCwfa3310_getPnd_Work_Record_Pnd_Wpid_ActnIsBreak = pdaCwfa3310.getPnd_Work_Record_Pnd_Wpid_Actn().isBreak(endOfData);
        if (condition(pdaCwfa3310_getPnd_Work_Record_Rqst_Work_Prcss_IdIsBreak || pdaCwfa3310_getPnd_Work_Record_Pnd_Wpid_ActnIsBreak))
        {
            if (condition(pdaCwfa3310.getPnd_Rep_Data_Pnd_Rep_Break_Ind().notEquals("Y")))                                                                                //Natural: IF #REP-BREAK-IND NE 'Y'
            {
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid().setValue(readWork02Rqst_Work_Prcss_IdOld);                                                                        //Natural: MOVE OLD ( #WORK-RECORD.RQST-WORK-PRCSS-ID ) TO #WPID
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Count().setValue(readWork02Rqst_Work_Prcss_IdCount869);                                                             //Natural: MOVE COUNT ( #WORK-RECORD.RQST-WORK-PRCSS-ID ) TO #WPID-COUNT
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().setValue(readWork02Pnd_Wpid_ActnOld);                                                                      //Natural: MOVE OLD ( #WPID-ACTN ) TO #WPID-ACTION
                short decideConditionsMet874 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
                if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("B"))))
                {
                    decideConditionsMet874++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL BOOKLETS REACHING STATUS FOR WPID", pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid())); //Natural: COMPRESS 'TOTAL BOOKLETS REACHING STATUS FOR WPID' #WPID INTO #WPID-TEXT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr()),                //Natural: COMPUTE #TBOOKLET-CTR = #BOOKLET-CTR ( 7 ) + #BOOKLET-CTR ( 8 ) + #BOOKLET-CTR ( 9 ) + #BOOKLET-CTR ( 10 ) + #BOOKLET-CTR ( 11 ) + #BOOKLET-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(7).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(8)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(9)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(10)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(11)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(12)));
                    if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr().equals(getZero())))                                                                     //Natural: IF #TBOOKLET-CTR = 0
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(7).setValue(0);                                                                          //Natural: MOVE 0 TO #PBOOKLET-CTR ( 7 ) #PBOOKLET-CTR ( 8 ) #PBOOKLET-CTR ( 9 ) #PBOOKLET-CTR ( 10 ) #PBOOKLET-CTR ( 11 ) #PBOOKLET-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(8).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(9).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(10).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(11).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(12).setValue(0);
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(7).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(7)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 7 ) = ( #BOOKLET-CTR ( 7 ) / #TBOOKLET-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(7).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(8).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(8)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 8 ) = ( #BOOKLET-CTR ( 8 ) / #TBOOKLET-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(8).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(9).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(9)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 9 ) = ( #BOOKLET-CTR ( 9 ) / #TBOOKLET-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(9).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(10).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 10 ) = ( #BOOKLET-CTR ( 10 ) / #TBOOKLET-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(10).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(11).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 11 ) = ( #BOOKLET-CTR ( 11 ) / #TBOOKLET-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(11).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(12).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(12)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 12 ) = ( #BOOKLET-CTR ( 12 ) / #TBOOKLET-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(12).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL BOOKLETS","REACHING STATUS IN 1 DAY                           ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(7),  //Natural: WRITE ( 1 ) / 'TOTAL BOOKLETS' 'REACHING STATUS IN 1 DAY                           ' #BOOKLET-CTR ( 7 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 7 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / 'TOTAL BOOKLETS' 'REACHING STATUS IN 2 DAYS                          ' #BOOKLET-CTR ( 8 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 8 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / 'TOTAL BOOKLETS' 'REACHING STATUS IN 3 DAYS                          ' #BOOKLET-CTR ( 9 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 9 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / 'TOTAL BOOKLETS' 'REACHING STATUS IN 4 DAYS                          ' #BOOKLET-CTR ( 10 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / 'TOTAL BOOKLETS' 'REACHING STATUS IN 5 DAYS                          ' #BOOKLET-CTR ( 11 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / 'TOTAL BOOKLETS' 'REACHING STATUS IN OVER 5 DAYS                     ' #BOOKLET-CTR ( 12 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 12 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / #WPID-TEXT #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(7), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL BOOKLETS","REACHING STATUS IN 2 DAYS                          ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(8), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(8), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL BOOKLETS","REACHING STATUS IN 3 DAYS                          ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(9), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(9), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL BOOKLETS","REACHING STATUS IN 4 DAYS                          ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL BOOKLETS","REACHING STATUS IN 5 DAYS                          ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL BOOKLETS","REACHING STATUS IN OVER 5 DAYS                     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(12), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(12), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text(),pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("F"))))
                {
                    decideConditionsMet874++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL FORMS REACHING STATUS FOR WPID", pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid())); //Natural: COMPRESS 'TOTAL FORMS REACHING STATUS FOR WPID' #WPID INTO #WPID-TEXT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr()),                    //Natural: COMPUTE #TFORMS-CTR = #FORMS-CTR ( 7 ) + #FORMS-CTR ( 8 ) + #FORMS-CTR ( 9 ) + #FORMS-CTR ( 10 ) + #FORMS-CTR ( 11 ) + #FORMS-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(7).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(8)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(9)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(10)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(11)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(12)));
                    if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr().equals(getZero())))                                                                       //Natural: IF #TFORMS-CTR = 0
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(7).setValue(0);                                                                             //Natural: MOVE 0 TO #FORMS-CTR ( 7 ) #FORMS-CTR ( 8 ) #FORMS-CTR ( 9 ) #FORMS-CTR ( 10 ) #FORMS-CTR ( 11 ) #FORMS-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(8).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(9).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(10).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(11).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(12).setValue(0);
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(7).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(7)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 7 ) = ( #FORMS-CTR ( 7 ) / #TFORMS-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(7).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(8).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(8)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 8 ) = ( #FORMS-CTR ( 8 ) / #TFORMS-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(8).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(9).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(9)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 9 ) = ( #FORMS-CTR ( 9 ) / #TFORMS-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(9).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(10).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 10 ) = ( #FORMS-CTR ( 10 ) / #TFORMS-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(10).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(11).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 11 ) = ( #FORMS-CTR ( 11 ) / #TFORMS-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(11).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(12).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(12)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 12 ) = ( #FORMS-CTR ( 12 ) / #TFORMS-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(12).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL FORMS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 1 DAY             ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(7),  //Natural: WRITE ( 1 ) / 'TOTAL FORMS FOR WPID' #WPID ' REACHING STATUS IN 1 DAY             ' #FORMS-CTR ( 7 ) ( AD = OU ) ' (' #PFORMS-CTR ( 7 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / 'TOTAL FORMS FOR WPID' #WPID ' REACHING STATUS IN 2 DAYS            ' #FORMS-CTR ( 8 ) ( AD = OU ) ' (' #PFORMS-CTR ( 8 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / 'TOTAL FORMS FOR WPID' #WPID ' REACHING STATUS IN 3 DAYS            ' #FORMS-CTR ( 9 ) ( AD = OU ) ' (' #PFORMS-CTR ( 9 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / 'TOTAL FORMS FOR WPID' #WPID ' REACHING STATUS IN 4 DAYS            ' #FORMS-CTR ( 10 ) ( AD = OU ) ' (' #PFORMS-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / 'TOTAL FORMS FOR WPID' #WPID ' REACHING STATUS IN 5 DAYS            ' #FORMS-CTR ( 11 ) ( AD = OU ) ' (' #PFORMS-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / 'TOTAL FORMS FOR WPID' #WPID ' REACHING STATUS IN OVER 5 DAYS       ' #FORMS-CTR ( 12 ) ( AD = OU ) ' (' #PFORMS-CTR ( 12 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / #WPID-TEXT #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(7), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL FORMS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 2 DAYS            ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(8), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(8), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL FORMS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 3 DAYS            ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(9), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(9), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL FORMS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 4 DAYS            ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL FORMS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 5 DAYS            ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL FORMS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN OVER 5 DAYS       ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(12), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(12), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text(),pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("I"))))
                {
                    decideConditionsMet874++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL INQUIRIES REACHING STATUS FOR WPID", pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid())); //Natural: COMPRESS 'TOTAL INQUIRIES REACHING STATUS FOR WPID' #WPID INTO #WPID-TEXT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr()),                //Natural: COMPUTE #TINQUIRE-CTR = #INQUIRE-CTR ( 7 ) + #INQUIRE-CTR ( 8 ) + #INQUIRE-CTR ( 9 ) + #INQUIRE-CTR ( 10 ) + #INQUIRE-CTR ( 11 ) + #INQUIRE-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(7).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(8)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(9)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(10)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(11)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(12)));
                    if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr().equals(getZero())))                                                                     //Natural: IF #TINQUIRE-CTR = 0
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(7).setValue(0);                                                                           //Natural: MOVE 0 TO #INQUIRE-CTR ( 7 ) #INQUIRE-CTR ( 8 ) #INQUIRE-CTR ( 9 ) #INQUIRE-CTR ( 10 ) #INQUIRE-CTR ( 11 ) #INQUIRE-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(8).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(9).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(10).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(11).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(12).setValue(0);
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(7).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(7)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 7 ) = ( #INQUIRE-CTR ( 7 ) / #TINQUIRE-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(7).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(8).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(8)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 8 ) = ( #INQUIRE-CTR ( 8 ) / #TINQUIRE-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(8).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(9).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(9)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 9 ) = ( #INQUIRE-CTR ( 9 ) / #TINQUIRE-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(9).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(10).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 10 ) = ( #INQUIRE-CTR ( 10 ) / #TINQUIRE-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(10).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(11).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 11 ) = ( #INQUIRE-CTR ( 11 ) / #TINQUIRE-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(11).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(12).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(12)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 12 ) = ( #INQUIRE-CTR ( 12 ) / #TINQUIRE-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(12).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL INQUIRIES FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 1 DAY         ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(7),  //Natural: WRITE ( 1 ) / 'TOTAL INQUIRIES FOR WPID' #WPID ' REACHING STATUS IN 1 DAY         ' #INQUIRE-CTR ( 7 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 7 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / 'TOTAL INQUIRIES FOR WPID' #WPID ' REACHING STATUS IN 2 DAYS        ' #INQUIRE-CTR ( 8 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 8 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / 'TOTAL INQUIRIES FOR WPID' #WPID ' REACHING STATUS IN 3 DAYS        ' #INQUIRE-CTR ( 9 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 9 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / 'TOTAL INQUIRIES FOR WPID' #WPID ' REACHING STATUS IN 4 DAYS        ' #INQUIRE-CTR ( 10 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / 'TOTAL INQUIRIES FOR WPID' #WPID ' REACHING STATUS IN 5 DAYS        ' #INQUIRE-CTR ( 11 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / 'TOTAL INQUIRIES FOR WPID' #WPID ' REACHING STATUS IN OVER 5 DAYS   ' #INQUIRE-CTR ( 12 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 12 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / #WPID-TEXT #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(7), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL INQUIRIES FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 2 DAYS        ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(8), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(8), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL INQUIRIES FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 3 DAYS        ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(9), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(9), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL INQUIRIES FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 4 DAYS        ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL INQUIRIES FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 5 DAYS        ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL INQUIRIES FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN OVER 5 DAYS   ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(12), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(12), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text(),pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("R"))))
                {
                    decideConditionsMet874++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL RESEARCH REACHING STATUS FOR WPID", pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid())); //Natural: COMPRESS 'TOTAL RESEARCH REACHING STATUS FOR WPID' #WPID INTO #WPID-TEXT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr()),              //Natural: COMPUTE #TRESEARCH-CTR = #RESEARCH-CTR ( 7 ) + #RESEARCH-CTR ( 8 ) + #RESEARCH-CTR ( 9 ) + #RESEARCH-CTR ( 10 ) + #RESEARCH-CTR ( 11 ) + #RESEARCH-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(7).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(8)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(9)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(10)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(11)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(12)));
                    if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr().equals(getZero())))                                                                    //Natural: IF #TRESEARCH-CTR = 0
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(7).setValue(0);                                                                          //Natural: MOVE 0 TO #RESEARCH-CTR ( 7 ) #RESEARCH-CTR ( 8 ) #RESEARCH-CTR ( 9 ) #RESEARCH-CTR ( 10 ) #RESEARCH-CTR ( 11 ) #RESEARCH-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(8).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(9).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(10).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(11).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(12).setValue(0);
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(7).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(7)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 7 ) = ( #RESEARCH-CTR ( 7 ) / #TRESEARCH-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(7).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(8).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(8)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 8 ) = ( #RESEARCH-CTR ( 8 ) / #TRESEARCH-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(8).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(9).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(9)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 9 ) = ( #RESEARCH-CTR ( 9 ) / #TRESEARCH-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(9).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(10).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 10 ) = ( #RESEARCH-CTR ( 10 ) / #TRESEARCH-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(10).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(11).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 11 ) = ( #RESEARCH-CTR ( 11 ) / #TRESEARCH-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(11).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(12).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(12)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 12 ) = ( #RESEARCH-CTR ( 12 ) / #TRESEARCH-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(12).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL RESEARCH FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 1 DAY          ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(7),  //Natural: WRITE ( 1 ) / 'TOTAL RESEARCH FOR WPID' #WPID ' REACHING STATUS IN 1 DAY          ' #RESEARCH-CTR ( 7 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 7 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / 'TOTAL RESEARCH FOR WPID' #WPID ' REACHING STATUS IN 2 DAYS         ' #RESEARCH-CTR ( 8 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 8 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / 'TOTAL RESEARCH FOR WPID' #WPID ' REACHING STATUS IN 3 DAYS         ' #RESEARCH-CTR ( 9 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 9 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / 'TOTAL RESEARCH FOR WPID' #WPID ' REACHING STATUS IN 4 DAYS         ' #RESEARCH-CTR ( 10 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / 'TOTAL RESEARCH FOR WPID' #WPID ' REACHING STATUS IN 5 DAYS         ' #RESEARCH-CTR ( 11 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / 'TOTAL RESEARCH FOR WPID' #WPID ' REACHING STATUS IN OVER 5 DAYS    ' #RESEARCH-CTR ( 12 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 12 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / #WPID-TEXT #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(7), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL RESEARCH FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 2 DAYS         ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(8), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(8), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL RESEARCH FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 3 DAYS         ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(9), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(9), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL RESEARCH FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 4 DAYS         ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL RESEARCH FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 5 DAYS         ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL RESEARCH FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN OVER 5 DAYS    ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(12), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(12), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text(),pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("T"))))
                {
                    decideConditionsMet874++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL TRANSCATIONS REACHING STATUS FOR WPID", pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid())); //Natural: COMPRESS 'TOTAL TRANSCATIONS REACHING STATUS FOR WPID' #WPID INTO #WPID-TEXT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr()),                    //Natural: COMPUTE #TTRANS-CTR = #TRANS-CTR ( 7 ) + #TRANS-CTR ( 8 ) + #TRANS-CTR ( 9 ) + #TRANS-CTR ( 10 ) + #TRANS-CTR ( 11 ) + #TRANS-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(7).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(8)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(9)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(10)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(11)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(12)));
                    if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr().equals(getZero())))                                                                       //Natural: IF #TTRANS-CTR = 0
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(7).setValue(0);                                                                             //Natural: MOVE 0 TO #TRANS-CTR ( 7 ) #TRANS-CTR ( 8 ) #TRANS-CTR ( 9 ) #TRANS-CTR ( 10 ) #TRANS-CTR ( 11 ) #TRANS-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(8).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(9).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(10).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(11).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(12).setValue(0);
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(7).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(7)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 7 ) = ( #TRANS-CTR ( 7 ) / #TTRANS-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(7).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(8).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(8)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 8 ) = ( #TRANS-CTR ( 8 ) / #TTRANS-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(8).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(9).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(9)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 9 ) = ( #TRANS-CTR ( 9 ) / #TTRANS-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(9).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(10).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 10 ) = ( #TRANS-CTR ( 10 ) / #TTRANS-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(10).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(11).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 11 ) = ( #TRANS-CTR ( 11 ) / #TTRANS-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(11).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(12).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(12)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 12 ) = ( #TRANS-CTR ( 12 ) / #TTRANS-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(12).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL TRANSACTIONS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 1 DAY      ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(7),  //Natural: WRITE ( 1 ) / 'TOTAL TRANSACTIONS FOR WPID' #WPID ' REACHING STATUS IN 1 DAY      ' #TRANS-CTR ( 7 ) ( AD = OU ) ' (' #PTRANS-CTR ( 7 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / 'TOTAL TRANSACTIONS FOR WPID' #WPID ' REACHING STATUS IN 2 DAYS     ' #TRANS-CTR ( 8 ) ( AD = OU ) ' (' #PTRANS-CTR ( 8 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / 'TOTAL TRANSACTIONS FOR WPID' #WPID ' REACHING STATUS IN 3 DAYS     ' #TRANS-CTR ( 9 ) ( AD = OU ) ' (' #PTRANS-CTR ( 9 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / 'TOTAL TRANSACTIONS FOR WPID' #WPID ' REACHING STATUS IN 4 DAYS     ' #TRANS-CTR ( 10 ) ( AD = OU ) ' (' #PTRANS-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / 'TOTAL TRANSACTIONS FOR WPID' #WPID ' REACHING STATUS IN 5 DAYS     ' #TRANS-CTR ( 11 ) ( AD = OU ) ' (' #PTRANS-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / 'TOTAL TRANSACTIONS FOR WPID' #WPID ' REACHING STATUS IN OVER 5 DAYS' #TRANS-CTR ( 12 ) ( AD = OU ) ' (' #PTRANS-CTR ( 12 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / #WPID-TEXT #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(7), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL TRANSACTIONS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 2 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(8), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(8), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL TRANSACTIONS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 3 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(9), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(9), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL TRANSACTIONS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 4 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL TRANSACTIONS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 5 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL TRANSACTIONS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN OVER 5 DAYS",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(12), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(12), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text(),pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("X"))))
                {
                    decideConditionsMet874++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL COMPLAINTS REACHING STATUS FOR WPID", pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid())); //Natural: COMPRESS 'TOTAL COMPLAINTS REACHING STATUS FOR WPID' #WPID INTO #WPID-TEXT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr()),            //Natural: COMPUTE #TCOMPLAINT-CTR = #COMPLAINT-CTR ( 7 ) + #COMPLAINT-CTR ( 8 ) + #COMPLAINT-CTR ( 9 ) + #COMPLAINT-CTR ( 10 ) + #COMPLAINT-CTR ( 11 ) + #COMPLAINT-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(7).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(8)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(9)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(10)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(11)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(12)));
                    if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr().equals(getZero())))                                                                   //Natural: IF #TCOMPLAINT-CTR = 0
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(7).setValue(0);                                                                         //Natural: MOVE 0 TO #COMPLAINT-CTR ( 7 ) #COMPLAINT-CTR ( 8 ) #COMPLAINT-CTR ( 9 ) #COMPLAINT-CTR ( 10 ) #COMPLAINT-CTR ( 11 ) #COMPLAINT-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(8).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(9).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(10).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(11).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(12).setValue(0);
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(7).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(7)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 7 ) = ( #COMPLAINT-CTR ( 7 ) / #TCOMPLAINT-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(7).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(8).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(8)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 8 ) = ( #COMPLAINT-CTR ( 8 ) / #TCOMPLAINT-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(8).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(9).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(9)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 9 ) = ( #COMPLAINT-CTR ( 9 ) / #TCOMPLAINT-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(9).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(10).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 10 ) = ( #COMPLAINT-CTR ( 10 ) / #TCOMPLAINT-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(10).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(11).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 11 ) = ( #COMPLAINT-CTR ( 11 ) / #TCOMPLAINT-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(11).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(12).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(12)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 12 ) = ( #COMPLAINT-CTR ( 12 ) / #TCOMPLAINT-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(12).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL COMPLAINTS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 1 DAY        ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(7),  //Natural: WRITE ( 1 ) / 'TOTAL COMPLAINTS FOR WPID' #WPID ' REACHING STATUS IN 1 DAY        ' #COMPLAINT-CTR ( 7 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 7 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / 'TOTAL COMPLAINTS FOR WPID' #WPID ' REACHING STATUS IN 2 DAYS       ' #COMPLAINT-CTR ( 8 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 8 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / 'TOTAL COMPLAINTS FOR WPID' #WPID ' REACHING STATUS IN 3 DAYS       ' #COMPLAINT-CTR ( 9 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 9 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / 'TOTAL COMPLAINTS FOR WPID' #WPID ' REACHING STATUS IN 4 DAYS       ' #COMPLAINT-CTR ( 10 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / 'TOTAL COMPLAINTS FOR WPID' #WPID ' REACHING STATUS IN 5 DAYS       ' #COMPLAINT-CTR ( 11 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / 'TOTAL COMPLAINTS FOR WPID' #WPID ' REACHING STATUS IN OVER 5 DAYS  ' #COMPLAINT-CTR ( 12 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 12 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / #WPID-TEXT #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(7), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL COMPLAINTS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 2 DAYS       ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(8), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(8), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL COMPLAINTS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 3 DAYS       ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(9), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(9), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL COMPLAINTS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 4 DAYS       ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL COMPLAINTS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 5 DAYS       ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL COMPLAINTS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN OVER 5 DAYS  ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(12), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(12), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text(),pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("Z"))))
                {
                    decideConditionsMet874++;
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL OTHER REACHING STATUS FOR WPID", pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid())); //Natural: COMPRESS 'TOTAL OTHER REACHING STATUS FOR WPID' #WPID INTO #WPID-TEXT
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr()),                    //Natural: COMPUTE #TOTHER-CTR = #OTHER-CTR ( 7 ) + #OTHER-CTR ( 8 ) + #OTHER-CTR ( 9 ) + #OTHER-CTR ( 10 ) + #OTHER-CTR ( 11 ) + #OTHER-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(7).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(8)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(9)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(10)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(11)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(12)));
                    if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr().equals(getZero())))                                                                       //Natural: IF #TOTHER-CTR = 0
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(7).setValue(0);                                                                             //Natural: MOVE 0 TO #OTHER-CTR ( 7 ) #OTHER-CTR ( 8 ) #OTHER-CTR ( 9 ) #OTHER-CTR ( 10 ) #OTHER-CTR ( 11 ) #OTHER-CTR ( 12 )
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(8).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(9).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(10).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(11).setValue(0);
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(12).setValue(0);
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(7).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(7)),  //Natural: COMPUTE ROUNDED #POTHER-CTR ( 7 ) = ( #OTHER-CTR ( 7 ) / #TOTHER-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(7).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(8).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(8)),  //Natural: COMPUTE ROUNDED #POTHER-CTR ( 8 ) = ( #OTHER-CTR ( 8 ) / #TOTHER-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(8).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(9).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(9)),  //Natural: COMPUTE ROUNDED #POTHER-CTR ( 9 ) = ( #OTHER-CTR ( 9 ) / #TOTHER-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(9).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(10).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #POTHER-CTR ( 10 ) = ( #OTHER-CTR ( 10 ) / #TOTHER-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(10).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(11).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #POTHER-CTR ( 11 ) = ( #OTHER-CTR ( 11 ) / #TOTHER-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(11).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr())).multiply(100));
                        pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(12).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(12)),  //Natural: COMPUTE ROUNDED #POTHER-CTR ( 12 ) = ( #OTHER-CTR ( 12 ) / #TOTHER-CTR ) * 100
                            (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(12).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL OTHER FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 1 DAY             ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(7),  //Natural: WRITE ( 1 ) / 'TOTAL OTHER FOR WPID' #WPID ' REACHING STATUS IN 1 DAY             ' #OTHER-CTR ( 7 ) ( AD = OU ) ' (' #POTHER-CTR ( 7 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHER FOR WPID' #WPID ')' / 'TOTAL OTHER FOR WPID' #WPID ' REACHING STATUS IN 2 DAYS            ' #OTHER-CTR ( 8 ) ( AD = OU ) ' (' #POTHER-CTR ( 8 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHER FOR WPID' #WPID ')' / 'TOTAL OTHER FOR WPID' #WPID ' REACHING STATUS IN 3 DAYS            ' #OTHER-CTR ( 9 ) ( AD = OU ) ' (' #POTHER-CTR ( 9 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHER FOR WPID' #WPID ')' / 'TOTAL OTHER FOR WPID' #WPID ' REACHING STATUS IN 4 DAYS            ' #OTHER-CTR ( 10 ) ( AD = OU ) ' (' #POTHER-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHER FOR WPID' #WPID ')' / 'TOTAL OTHER FOR WPID' #WPID ' REACHING STATUS IN 5 DAYS            ' #OTHER-CTR ( 11 ) ( AD = OU ) ' (' #POTHER-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHER FOR WPID' #WPID ')' / 'TOTAL OTHER FOR WPID' #WPID ' REACHING STATUS IN OVER 5 DAYS       ' #OTHER-CTR ( 12 ) ( AD = OU ) ' (' #POTHER-CTR ( 12 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHER FOR WPID' #WPID ')' / #WPID-TEXT #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(7), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHER FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL OTHER FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 2 DAYS            ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(8), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(8), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHER FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL OTHER FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 3 DAYS            ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(9), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(9), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHER FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL OTHER FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 4 DAYS            ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHER FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL OTHER FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN 5 DAYS            ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHER FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,"TOTAL OTHER FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid()," REACHING STATUS IN OVER 5 DAYS       ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(12), 
                        new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(12), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHER FOR WPID",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid(),")",NEWLINE,pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Text(),pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet874 > 0))
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) ' '
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Array_Ctrs().getValue(7).reset();                                                                                        //Natural: RESET #ARRAY-CTRS ( 7 )
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Array_Ctrs().getValue(8).reset();                                                                                        //Natural: RESET #ARRAY-CTRS ( 8 )
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Array_Ctrs().getValue(9).reset();                                                                                        //Natural: RESET #ARRAY-CTRS ( 9 )
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Array_Ctrs().getValue(10).reset();                                                                                       //Natural: RESET #ARRAY-CTRS ( 10 )
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Array_Ctrs().getValue(11).reset();                                                                                       //Natural: RESET #ARRAY-CTRS ( 11 )
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Array_Ctrs().getValue(12).reset();                                                                                       //Natural: RESET #ARRAY-CTRS ( 12 )
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Percent_Ctrs().getValue(7).reset();                                                                                      //Natural: RESET #PERCENT-CTRS ( 7 )
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Percent_Ctrs().getValue(8).reset();                                                                                      //Natural: RESET #PERCENT-CTRS ( 8 )
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Percent_Ctrs().getValue(9).reset();                                                                                      //Natural: RESET #PERCENT-CTRS ( 9 )
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Percent_Ctrs().getValue(10).reset();                                                                                     //Natural: RESET #PERCENT-CTRS ( 10 )
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Percent_Ctrs().getValue(11).reset();                                                                                     //Natural: RESET #PERCENT-CTRS ( 11 )
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Percent_Ctrs().getValue(12).reset();                                                                                     //Natural: RESET #PERCENT-CTRS ( 12 )
            }                                                                                                                                                             //Natural: END-IF
            readWork02Rqst_Work_Prcss_IdCount869.setDec(new DbsDecimal(0));                                                                                               //Natural: END-BREAK
        }
        if (condition(pdaCwfa3310_getPnd_Work_Record_Pnd_Wpid_ActnIsBreak))
        {
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Total_Count().nadd(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count());                                                            //Natural: COMPUTE #TOTAL-COUNT = #TOTAL-COUNT + #SUB-COUNT
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().setValue(readWork02Pnd_Wpid_ActnOld);                                                                          //Natural: MOVE OLD ( #WPID-ACTN ) TO #WPID-ACTION
            short decideConditionsMet996 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
            if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("B"))))
            {
                decideConditionsMet996++;
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr()),                    //Natural: COMPUTE #TBOOKLET-CTR = #BOOKLET-CTR ( 1 ) + #BOOKLET-CTR ( 2 ) + #BOOKLET-CTR ( 3 ) + #BOOKLET-CTR ( 4 ) + #BOOKLET-CTR ( 5 ) + #BOOKLET-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(1).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(2)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(3)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(4)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(5)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(6)));
                if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr().equals(getZero())))                                                                         //Natural: IF #TBOOKLET-CTR = 0
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(1).setValue(0);                                                                               //Natural: MOVE 0 TO #BOOKLET-CTR ( 1 ) #BOOKLET-CTR ( 2 ) #BOOKLET-CTR ( 3 ) #BOOKLET-CTR ( 4 ) #BOOKLET-CTR ( 5 ) #BOOKLET-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(2).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(3).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(4).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(5).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(6).setValue(0);
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(1).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(1)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 1 ) = ( #BOOKLET-CTR ( 1 ) / #TBOOKLET-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(1).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(2).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(2)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 2 ) = ( #BOOKLET-CTR ( 2 ) / #TBOOKLET-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(2).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(3).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 3 ) = ( #BOOKLET-CTR ( 3 ) / #TBOOKLET-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(3).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(4).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 4 ) = ( #BOOKLET-CTR ( 4 ) / #TBOOKLET-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(4).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(5).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 5 ) = ( #BOOKLET-CTR ( 5 ) / #TBOOKLET-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(5).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(6).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(6)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 6 ) = ( #BOOKLET-CTR ( 6 ) / #TBOOKLET-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(6).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tbooklet_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL BOOKLETS REACHING STATUS IN 1 DAY      ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(1),  //Natural: WRITE ( 1 ) / 'TOTAL BOOKLETS REACHING STATUS IN 1 DAY      ' #BOOKLET-CTR ( 1 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS REACHING STATUS IN 2 DAYS     ' #BOOKLET-CTR ( 2 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS REACHING STATUS IN 3 DAYS     ' #BOOKLET-CTR ( 3 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS REACHING STATUS IN 4 DAYS     ' #BOOKLET-CTR ( 4 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS REACHING STATUS IN 5 DAYS     ' #BOOKLET-CTR ( 5 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS REACHING STATUS IN OVER 5 DAYS' #BOOKLET-CTR ( 6 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS REACHING STATUS               ' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS REACHING STATUS IN 2 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS REACHING STATUS IN 3 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS REACHING STATUS IN 4 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS REACHING STATUS IN 5 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS REACHING STATUS IN OVER 5 DAYS",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue(6), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pbooklet_Ctr().getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS REACHING STATUS               ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("F"))))
            {
                decideConditionsMet996++;
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr()), pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(1).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(2)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(3)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(4)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(5)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(6))); //Natural: COMPUTE #TFORMS-CTR = #FORMS-CTR ( 1 ) + #FORMS-CTR ( 2 ) + #FORMS-CTR ( 3 ) + #FORMS-CTR ( 4 ) + #FORMS-CTR ( 5 ) + #FORMS-CTR ( 6 )
                if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr().equals(getZero())))                                                                           //Natural: IF #TFORMS-CTR = 0
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(1).setValue(0);                                                                                 //Natural: MOVE 0 TO #FORMS-CTR ( 1 ) #FORMS-CTR ( 2 ) #FORMS-CTR ( 3 ) #FORMS-CTR ( 4 ) #FORMS-CTR ( 5 ) #FORMS-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(2).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(3).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(4).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(5).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(6).setValue(0);
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(1).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(1)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 1 ) = ( #FORMS-CTR ( 1 ) / #TFORMS-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(1).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(2).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(2)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 2 ) = ( #FORMS-CTR ( 2 ) / #TFORMS-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(2).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(3).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 3 ) = ( #FORMS-CTR ( 3 ) / #TFORMS-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(3).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(4).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 4 ) = ( #FORMS-CTR ( 4 ) / #TFORMS-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(4).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(5).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 5 ) = ( #FORMS-CTR ( 5 ) / #TFORMS-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(5).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(6).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(6)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 6 ) = ( #FORMS-CTR ( 6 ) / #TFORMS-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(6).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tforms_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL FORMS REACHING STATUS IN 1 DAY      ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(1), //Natural: WRITE ( 1 ) / 'TOTAL FORMS REACHING STATUS IN 1 DAY      ' #FORMS-CTR ( 1 ) ( AD = OU ) ' (' #PFORMS-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS REACHING STATUS IN 2 DAYS     ' #FORMS-CTR ( 2 ) ( AD = OU ) ' (' #PFORMS-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS REACHING STATUS IN 3 DAYS     ' #FORMS-CTR ( 3 ) ( AD = OU ) ' (' #PFORMS-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS REACHING STATUS IN 4 DAYS     ' #FORMS-CTR ( 4 ) ( AD = OU ) ' (' #PFORMS-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS REACHING STATUS IN 5 DAYS     ' #FORMS-CTR ( 5 ) ( AD = OU ) ' (' #PFORMS-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS REACHING STATUS IN OVER 5 DAYS' #FORMS-CTR ( 6 ) ( AD = OU ) ' (' #PFORMS-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS REACHING STATUS               ' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS REACHING STATUS IN 2 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS REACHING STATUS IN 3 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS REACHING STATUS IN 4 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS REACHING STATUS IN 5 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS REACHING STATUS IN OVER 5 DAYS",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue(6), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pforms_Ctr().getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS REACHING STATUS               ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("I"))))
            {
                decideConditionsMet996++;
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr()),                    //Natural: COMPUTE #TINQUIRE-CTR = #INQUIRE-CTR ( 1 ) + #INQUIRE-CTR ( 2 ) + #INQUIRE-CTR ( 3 ) + #INQUIRE-CTR ( 4 ) + #INQUIRE-CTR ( 5 ) + #INQUIRE-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(1).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(2)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(3)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(4)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(5)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(6)));
                if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr().equals(getZero())))                                                                         //Natural: IF #TINQUIRE-CTR = 0
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(1).setValue(0);                                                                               //Natural: MOVE 0 TO #INQUIRE-CTR ( 1 ) #INQUIRE-CTR ( 2 ) #INQUIRE-CTR ( 3 ) #INQUIRE-CTR ( 4 ) #INQUIRE-CTR ( 5 ) #INQUIRE-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(2).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(3).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(4).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(5).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(6).setValue(0);
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(1).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(1)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 1 ) = ( #INQUIRE-CTR ( 1 ) / #TINQUIRE-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(1).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(2).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(2)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 2 ) = ( #INQUIRE-CTR ( 2 ) / #TINQUIRE-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(2).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(3).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 3 ) = ( #INQUIRE-CTR ( 3 ) / #TINQUIRE-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(3).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(4).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 4 ) = ( #INQUIRE-CTR ( 4 ) / #TINQUIRE-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(4).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(5).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 5 ) = ( #INQUIRE-CTR ( 5 ) / #TINQUIRE-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(5).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(6).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(6)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 6 ) = ( #INQUIRE-CTR ( 6 ) / #TINQUIRE-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(6).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tinquire_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL INQUIRIES REACHING STATUS IN 1 DAY      ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(1),  //Natural: WRITE ( 1 ) / 'TOTAL INQUIRIES REACHING STATUS IN 1 DAY      ' #INQUIRE-CTR ( 1 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES REACHING STATUS IN 2 DAYS     ' #INQUIRE-CTR ( 2 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES REACHING STATUS IN 3 DAYS     ' #INQUIRE-CTR ( 3 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES REACHING STATUS IN 4 DAYS     ' #INQUIRE-CTR ( 4 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES REACHING STATUS IN 5 DAYS     ' #INQUIRE-CTR ( 5 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES REACHING STATUS IN OVER 5 DAYS' #INQUIRE-CTR ( 6 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES REACHING STATUS               ' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES REACHING STATUS IN 2 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES REACHING STATUS IN 3 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES REACHING STATUS IN 4 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES REACHING STATUS IN 5 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES REACHING STATUS IN OVER 5 DAYS",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue(6), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pinquire_Ctr().getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES REACHING STATUS               ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("R"))))
            {
                decideConditionsMet996++;
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr()),                  //Natural: COMPUTE #TRESEARCH-CTR = #RESEARCH-CTR ( 1 ) + #RESEARCH-CTR ( 2 ) + #RESEARCH-CTR ( 3 ) + #RESEARCH-CTR ( 4 ) + #RESEARCH-CTR ( 5 ) + #RESEARCH-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(1).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(2)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(3)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(4)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(5)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(6)));
                if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr().equals(getZero())))                                                                        //Natural: IF #TRESEARCH-CTR = 0
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(1).setValue(0);                                                                              //Natural: MOVE 0 TO #RESEARCH-CTR ( 1 ) #RESEARCH-CTR ( 2 ) #RESEARCH-CTR ( 3 ) #RESEARCH-CTR ( 4 ) #RESEARCH-CTR ( 5 ) #RESEARCH-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(2).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(3).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(4).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(5).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(6).setValue(0);
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(1).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(1)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 1 ) = ( #RESEARCH-CTR ( 1 ) / #TRESEARCH-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(1).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(2).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(2)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 2 ) = ( #RESEARCH-CTR ( 2 ) / #TRESEARCH-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(2).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(3).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 3 ) = ( #RESEARCH-CTR ( 3 ) / #TRESEARCH-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(3).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(4).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 4 ) = ( #RESEARCH-CTR ( 4 ) / #TRESEARCH-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(4).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(5).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 5 ) = ( #RESEARCH-CTR ( 5 ) / #TRESEARCH-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(5).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(6).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(6)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 6 ) = ( #RESEARCH-CTR ( 6 ) / #TRESEARCH-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(6).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tresearch_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL RESEARCH REACHING STATUS IN 1 DAY      ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(1),  //Natural: WRITE ( 1 ) / 'TOTAL RESEARCH REACHING STATUS IN 1 DAY      ' #RESEARCH-CTR ( 1 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH REACHING STATUS IN 2 DAYS     ' #RESEARCH-CTR ( 2 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH REACHING STATUS IN 3 DAYS     ' #RESEARCH-CTR ( 3 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH REACHING STATUS IN 4 DAYS     ' #RESEARCH-CTR ( 4 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH REACHING STATUS IN 5 DAYS     ' #RESEARCH-CTR ( 5 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH REACHING STATUS IN OVER 5 DAYS' #RESEARCH-CTR ( 6 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH REACHING STATUS               ' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH REACHING STATUS IN 2 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH REACHING STATUS IN 3 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH REACHING STATUS IN 4 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH REACHING STATUS IN 5 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH REACHING STATUS IN OVER 5 DAYS",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue(6), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Presearch_Ctr().getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH REACHING STATUS               ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("T"))))
            {
                decideConditionsMet996++;
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr()), pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(1).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(2)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(3)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(4)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(5)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(6))); //Natural: COMPUTE #TTRANS-CTR = #TRANS-CTR ( 1 ) + #TRANS-CTR ( 2 ) + #TRANS-CTR ( 3 ) + #TRANS-CTR ( 4 ) + #TRANS-CTR ( 5 ) + #TRANS-CTR ( 6 )
                if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr().equals(getZero())))                                                                           //Natural: IF #TTRANS-CTR = 0
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(1).setValue(0);                                                                                 //Natural: MOVE 0 TO #TRANS-CTR ( 1 ) #TRANS-CTR ( 2 ) #TRANS-CTR ( 3 ) #TRANS-CTR ( 4 ) #TRANS-CTR ( 5 ) #TRANS-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(2).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(3).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(4).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(5).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(6).setValue(0);
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(1).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(1)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 1 ) = ( #TRANS-CTR ( 1 ) / #TTRANS-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(1).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(2).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(2)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 2 ) = ( #TRANS-CTR ( 2 ) / #TTRANS-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(2).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(3).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 3 ) = ( #TRANS-CTR ( 3 ) / #TTRANS-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(3).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(4).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 4 ) = ( #TRANS-CTR ( 4 ) / #TTRANS-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(4).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(5).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 5 ) = ( #TRANS-CTR ( 5 ) / #TTRANS-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(5).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(6).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(6)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 6 ) = ( #TRANS-CTR ( 6 ) / #TTRANS-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(6).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ttrans_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL TRANSACTIONS REACHING STATUS IN 1 DAY      ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(1),  //Natural: WRITE ( 1 ) / 'TOTAL TRANSACTIONS REACHING STATUS IN 1 DAY      ' #TRANS-CTR ( 1 ) ( AD = OU ) ' (' #PTRANS-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS REACHING STATUS IN 2 DAYS     ' #TRANS-CTR ( 2 ) ( AD = OU ) ' (' #PTRANS-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS REACHING STATUS IN 3 DAYS     ' #TRANS-CTR ( 3 ) ( AD = OU ) ' (' #PTRANS-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS REACHING STATUS IN 4 DAYS     ' #TRANS-CTR ( 4 ) ( AD = OU ) ' (' #PTRANS-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS REACHING STATUS IN 5 DAYS     ' #TRANS-CTR ( 5 ) ( AD = OU ) ' (' #PTRANS-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS REACHING STATUS IN OVER 5 DAYS' #TRANS-CTR ( 6 ) ( AD = OU ) ' (' #PTRANS-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' 'TOTAL TRANSACTIONS REACHING STATUS               ' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS REACHING STATUS IN 2 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS REACHING STATUS IN 3 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS REACHING STATUS IN 4 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS REACHING STATUS IN 5 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS REACHING STATUS IN OVER 5 DAYS",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(6), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Ptrans_Ctr().getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)","TOTAL TRANSACTIONS REACHING STATUS               ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("X"))))
            {
                decideConditionsMet996++;
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr()),                //Natural: COMPUTE #TCOMPLAINT-CTR = #COMPLAINT-CTR ( 1 ) + #COMPLAINT-CTR ( 2 ) + #COMPLAINT-CTR ( 3 ) + #COMPLAINT-CTR ( 4 ) + #COMPLAINT-CTR ( 5 ) + #COMPLAINT-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(1).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(2)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(3)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(4)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(5)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(6)));
                if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr().equals(getZero())))                                                                       //Natural: IF #TCOMPLAINT-CTR = 0
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(1).setValue(0);                                                                             //Natural: MOVE 0 TO #COMPLAINT-CTR ( 1 ) #COMPLAINT-CTR ( 2 ) #COMPLAINT-CTR ( 3 ) #COMPLAINT-CTR ( 4 ) #COMPLAINT-CTR ( 5 ) #COMPLAINT-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(2).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(3).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(4).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(5).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(6).setValue(0);
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(1).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(1)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 1 ) = ( #COMPLAINT-CTR ( 1 ) / #TCOMPLAINT-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(1).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(2).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(2)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 2 ) = ( #COMPLAINT-CTR ( 2 ) / #TCOMPLAINT-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(2).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(3).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 3 ) = ( #COMPLAINT-CTR ( 3 ) / #TCOMPLAINT-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(3).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(4).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 4 ) = ( #COMPLAINT-CTR ( 4 ) / #TCOMPLAINT-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(4).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(5).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 5 ) = ( #COMPLAINT-CTR ( 5 ) / #TCOMPLAINT-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(5).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(6).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(6)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 6 ) = ( #COMPLAINT-CTR ( 6 ) / #TCOMPLAINT-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(6).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL COMPLAINTS REACHING STATUS IN 1 DAY        ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(1),  //Natural: WRITE ( 1 ) / 'TOTAL COMPLAINTS REACHING STATUS IN 1 DAY        ' #COMPLAINT-CTR ( 1 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS REACHING STATUS IN 2 DAYS       ' #COMPLAINT-CTR ( 2 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS REACHING STATUS IN 3 DAYS       ' #COMPLAINT-CTR ( 3 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS REACHING STATUS IN 4 DAY        ' #COMPLAINT-CTR ( 4 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS REACHING STATUS IN 5 DAYS       ' #COMPLAINT-CTR ( 5 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS REACHING STATUS IN OVER 5 DAYS  ' #COMPLAINT-CTR ( 6 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS REACHING STATUS                 ' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS REACHING STATUS IN 2 DAYS       ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS REACHING STATUS IN 3 DAYS       ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS REACHING STATUS IN 4 DAY        ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS REACHING STATUS IN 5 DAYS       ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS REACHING STATUS IN OVER 5 DAYS  ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue(6), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr().getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS REACHING STATUS                 ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'Z'
            else if (condition((pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Action().equals("Z"))))
            {
                decideConditionsMet996++;
                pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr().compute(new ComputeParameters(false, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr()), pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(1).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(2)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(3)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(4)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(5)).add(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(6))); //Natural: COMPUTE #TOTHER-CTR = #OTHER-CTR ( 1 ) + #OTHER-CTR ( 2 ) + #OTHER-CTR ( 3 ) + #OTHER-CTR ( 4 ) + #OTHER-CTR ( 5 ) + #OTHER-CTR ( 6 )
                if (condition(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr().equals(getZero())))                                                                           //Natural: IF #TOTHER-CTR = 0
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(1).setValue(0);                                                                                 //Natural: MOVE 0 TO #OTHER-CTR ( 1 ) #OTHER-CTR ( 2 ) #OTHER-CTR ( 3 ) #OTHER-CTR ( 4 ) #OTHER-CTR ( 5 ) #OTHER-CTR ( 6 )
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(2).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(3).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(4).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(5).setValue(0);
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(6).setValue(0);
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(1).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(1)),  //Natural: COMPUTE ROUNDED #POTHER-CTR ( 1 ) = ( #OTHER-CTR ( 1 ) / #TOTHER-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(1).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(2).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(2)),  //Natural: COMPUTE ROUNDED #POTHER-CTR ( 2 ) = ( #OTHER-CTR ( 2 ) / #TOTHER-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(2).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(3).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #POTHER-CTR ( 3 ) = ( #OTHER-CTR ( 3 ) / #TOTHER-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(3).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(4).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #POTHER-CTR ( 4 ) = ( #OTHER-CTR ( 4 ) / #TOTHER-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(4).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(5).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #POTHER-CTR ( 5 ) = ( #OTHER-CTR ( 5 ) / #TOTHER-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(5).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr())).multiply(100));
                    pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(6).compute(new ComputeParameters(true, pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(6)),  //Natural: COMPUTE ROUNDED #POTHER-CTR ( 6 ) = ( #TRANS-CTR ( 6 ) / #TOTHER-CTR ) * 100
                        (pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue(6).divide(pdaCwfa3310.getPnd_Misc_Parm_Pnd_Tother_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL OTHER REACHING STATUS IN 1 DAY      ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(1), //Natural: WRITE ( 1 ) / 'TOTAL OTHER REACHING STATUS IN 1 DAY      ' #OTHER-CTR ( 1 ) ( AD = OU ) ' (' #POTHER-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHER)' / 'TOTAL OTHER REACHING STATUS IN 2 DAYS     ' #OTHER-CTR ( 2 ) ( AD = OU ) ' (' #POTHER-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHER)' / 'TOTAL OTHER REACHING STATUS IN 3 DAYS     ' #OTHER-CTR ( 3 ) ( AD = OU ) ' (' #POTHER-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHER)' / 'TOTAL OTHER REACHING STATUS IN 4 DAYS     ' #OTHER-CTR ( 4 ) ( AD = OU ) ' (' #POTHER-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHER)' / 'TOTAL OTHER REACHING STATUS IN 5 DAYS     ' #OTHER-CTR ( 5 ) ( AD = OU ) ' (' #POTHER-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHER)' / 'TOTAL OTHER REACHING STATUS IN OVER 5 DAYS' #OTHER-CTR ( 6 ) ( AD = OU ) ' (' #POTHER-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHER)' / 'TOTAL OTHER REACHING STATUS               ' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHER)",NEWLINE,"TOTAL OTHER REACHING STATUS IN 2 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHER)",NEWLINE,"TOTAL OTHER REACHING STATUS IN 3 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHER)",NEWLINE,"TOTAL OTHER REACHING STATUS IN 4 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHER)",NEWLINE,"TOTAL OTHER REACHING STATUS IN 5 DAYS     ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHER)",NEWLINE,"TOTAL OTHER REACHING STATUS IN OVER 5 DAYS",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue(6), 
                    new FieldAttributes ("AD=OU")," (",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Pother_Ctr().getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHER)",NEWLINE,"TOTAL OTHER REACHING STATUS               ",pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Booklet_Ctr().getValue("*").reset();                                                                                         //Natural: RESET #BOOKLET-CTR ( * ) #FORMS-CTR ( * ) #INQUIRE-CTR ( * ) #RESEARCH-CTR ( * ) #TRANS-CTR ( * ) #COMPLAINT-CTR ( * ) #OTHER-CTR ( * ) #SUB-COUNT
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Forms_Ctr().getValue("*").reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Inquire_Ctr().getValue("*").reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Research_Ctr().getValue("*").reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Trans_Ctr().getValue("*").reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Complaint_Ctr().getValue("*").reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Other_Ctr().getValue("*").reset();
            pdaCwfa3310.getPnd_Misc_Parm_Pnd_Sub_Count().reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=142 PS=55");

        getReports().setDisplayColumns(1, "//RECEIVED/AT TIAA",
        		pnd_Rqst_Tiaa_Rcvd_Dte, new ReportEditMask ("MM/DD/YY"),"//DOC/REC'D",
        		pdaCwfa3310.getPnd_Misc_Parm_Pnd_Return_Doc_Txt(),"//REC'D IN/UNIT",
        		pdaCwfa3310.getPnd_Misc_Parm_Pnd_Return_Rcvd_Txt(),"BUSINESS/DAYS/IN/UNIT",
        		pdaCwfa3310.getPnd_Work_Record_Tbl_Business_Days(), new ReportEditMask ("Z9.9"),"//WORK/PROCESS",
        		pdaCwfa3310.getPnd_Misc_Parm_Pnd_Wpid_Sname(), new IdenticalSuppress(true),"//WORK/STEP",
        		pdaCwfa3310.getPnd_Work_Record_Step_Id(), new IdenticalSuppress(true),"///STATUS",
        		pdaCwfa3310.getPnd_Misc_Parm_Pnd_Status_Sname(), new AlphanumericLength (15),"//STATUS/DATE",
        		pdaCwfa3310.getPnd_Work_Record_Admin_Status_Updte_Dte_Tme(), new ReportEditMask ("MM/DD/YY"),"CALENDAR/DAYS/IN/TIAA",
        		pdaCwfa3310.getPnd_Work_Record_Tbl_Calendar_Days(), new ReportEditMask ("Z,ZZ9"),"//PARTIC/NAME",
        		pdaCwfa3310.getPnd_Misc_Parm_Pnd_Partic_Sname(),"///PIN",
        		pdaCwfa3310.getPnd_Work_Record_Rqst_Pin_Nbr(),"//CONTRACT/NUMBER",
        		pdaCwfa3310.getPnd_Work_Record_Cntrct_Nbr(),"///EMPLOYEE",
        		pdaCwfa3310.getPnd_Work_Record_Admin_Status_Updte_Oprtr_Cde());
    }
}
