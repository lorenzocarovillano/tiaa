/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:18:08 AM
**        * FROM NATURAL SUBPROGRAM : Iwfn4712
************************************************************
**        * FILE NAME            : Iwfn4712.java
**        * CLASS NAME           : Iwfn4712
**        * INSTANCE NAME        : Iwfn4712
************************************************************
************************************************************************
* PROGRAM      : IWFN4712
* SYSTEM       : ENHANCED UNIT WORK LISTS
* DATE WRITTEN : 03/25/1999
* FUNCTION     : NCW-MASTER-INDEX INITIAL LOAD TO CWF-SCRATCH
*              : FOR A SPECIFIC UNIT-CDE
*              :
* HISTORY      : JRF 06/10/99 CHANGE CRPRTE-DUE-DTE-TME WITH
*              :              CRRNT-DUE-DTE
*              : JRF 08/26/99 ENHANCED ERROR HANDLING.
*              : JRF 09/02/99 INCLUDE NEW WORKLISTS: (WPID = 'P')
*              :              A) SUPERVISOR COMMUNICATION WORKLISTS
*              :              B) EMPLOYEE COMMUNICATION WORKLISTS
*              :              REF:JF090299
*              : JRF 09/14/99 POPULATE TEXT(1) WITH WORK LIST IND
*              :              REF:JF091499
*              : JB  09/27/99 ADJUST ERROR LOGIC. SC JB1.
*              : JRF 09/30/99 ADD DTE-TME-STAMP IN KEY8
*              : EPM 06/21/00 RESTOWED DUE TO CHANGE IN CWFA1400 FOR IES
*              :     03/10/16 NPIRSUNSET
* 09/08/2017   : MDM CHANGES     /*MDM-PIN
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iwfn4712 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCwfpda_M pdaCwfpda_M;
    private LdaIwfl4112 ldaIwfl4112;
    private PdaIwfa4700 pdaIwfa4700;
    private PdaIwfa4701 pdaIwfa4701;
    private PdaIwfa4702 pdaIwfa4702;
    private PdaIwfa4703 pdaIwfa4703;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfa1400 pdaCwfa1400;
    private PdaCwfa1401 pdaCwfa1401;
    private PdaMdma2001 pdaMdma2001;
    private PdaMdma2003 pdaMdma2003;
    private PdaMdma2002 pdaMdma2002;
    private PdaNasa032 pdaNasa032;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Tbl_Key_Field;
    private DbsField pnd_Parm_Unit_Cde;

    private DbsGroup pnd_Frgn_Geo_Pass;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_1;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_2;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_3;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_4;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_5;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_6;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_7;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_8;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Return_Cde;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Mail_Cde;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Cde;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_St_Cde1;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_St_Cde2;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Add_Ctr;
    private DbsField pnd_Start_Key;

    private DbsGroup pnd_Start_Key__R_Field_1;
    private DbsField pnd_Start_Key_Pnd_Unit_Cde;
    private DbsField pnd_Start_Key_Pnd_Work_List_Ind;
    private DbsField pnd_Start_Key_Pnd_Due_Dte;
    private DbsField pnd_Start_Key_Pnd_Wpid;
    private DbsField pnd_Wklst_Ind_Max;
    private DbsField pnd_Et_Max;
    private DbsField pnd_Et_Ctr;
    private DbsField pnd_Instn_Cde;
    private DbsField pnd_Wpid_Sort;
    private DbsField pnd_Last_Dte_Tme_Update_Hold;
    private DbsField pnd_Last_Msg_Data;
    private DbsField pnd_Individual_Err_Fl;
    private DbsField pnd_Call_4700_On_Err;
    private DbsField pnd_Err_Ctr;
    private DbsField pnd_Last_Err_Msg;
    private DbsField pnd_Last_Err_Prg;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIwfl4112 = new LdaIwfl4112();
        registerRecord(ldaIwfl4112);
        registerRecord(ldaIwfl4112.getVw_ncw_Master_Index());
        localVariables = new DbsRecord();
        pdaIwfa4700 = new PdaIwfa4700(localVariables);
        pdaIwfa4701 = new PdaIwfa4701(localVariables);
        pdaIwfa4702 = new PdaIwfa4702(localVariables);
        pdaIwfa4703 = new PdaIwfa4703(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfa1400 = new PdaCwfa1400(localVariables);
        pdaCwfa1401 = new PdaCwfa1401(localVariables);
        pdaMdma2001 = new PdaMdma2001(localVariables);
        pdaMdma2003 = new PdaMdma2003(localVariables);
        pdaMdma2002 = new PdaMdma2002(localVariables);
        pdaNasa032 = new PdaNasa032(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Tbl_Key_Field = parameters.newFieldInRecord("pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Tbl_Key_Field.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Unit_Cde = parameters.newFieldInRecord("pnd_Parm_Unit_Cde", "#PARM-UNIT-CDE", FieldType.STRING, 8);
        pnd_Parm_Unit_Cde.setParameterOption(ParameterOption.ByReference);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Frgn_Geo_Pass = localVariables.newGroupInRecord("pnd_Frgn_Geo_Pass", "#FRGN-GEO-PASS");
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_1 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_1", "#GEO-ADDR-LINE-1", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_2 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_2", "#GEO-ADDR-LINE-2", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_3 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_3", "#GEO-ADDR-LINE-3", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_4 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_4", "#GEO-ADDR-LINE-4", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_5 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_5", "#GEO-ADDR-LINE-5", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_6 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_6", "#GEO-ADDR-LINE-6", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_7 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_7", "#GEO-ADDR-LINE-7", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_8 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_8", "#GEO-ADDR-LINE-8", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Return_Cde = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Return_Cde", "#RETURN-CDE", FieldType.NUMERIC, 1);
        pnd_Frgn_Geo_Pass_Pnd_Mail_Cde = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Mail_Cde", "#MAIL-CDE", FieldType.STRING, 1);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Cde = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Cde", "#GEO-CDE", FieldType.STRING, 2);
        pnd_Frgn_Geo_Pass_Pnd_St_Cde1 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_St_Cde1", "#ST-CDE1", FieldType.STRING, 2);
        pnd_Frgn_Geo_Pass_Pnd_St_Cde2 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_St_Cde2", "#ST-CDE2", FieldType.STRING, 2);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.NUMERIC, 7);
        pnd_Add_Ctr = localVariables.newFieldInRecord("pnd_Add_Ctr", "#ADD-CTR", FieldType.NUMERIC, 7);
        pnd_Start_Key = localVariables.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.STRING, 33);

        pnd_Start_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Start_Key__R_Field_1", "REDEFINE", pnd_Start_Key);
        pnd_Start_Key_Pnd_Unit_Cde = pnd_Start_Key__R_Field_1.newFieldInGroup("pnd_Start_Key_Pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 8);
        pnd_Start_Key_Pnd_Work_List_Ind = pnd_Start_Key__R_Field_1.newFieldInGroup("pnd_Start_Key_Pnd_Work_List_Ind", "#WORK-LIST-IND", FieldType.STRING, 
            3);
        pnd_Start_Key_Pnd_Due_Dte = pnd_Start_Key__R_Field_1.newFieldInGroup("pnd_Start_Key_Pnd_Due_Dte", "#DUE-DTE", FieldType.STRING, 16);
        pnd_Start_Key_Pnd_Wpid = pnd_Start_Key__R_Field_1.newFieldInGroup("pnd_Start_Key_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Wklst_Ind_Max = localVariables.newFieldInRecord("pnd_Wklst_Ind_Max", "#WKLST-IND-MAX", FieldType.STRING, 1);
        pnd_Et_Max = localVariables.newFieldInRecord("pnd_Et_Max", "#ET-MAX", FieldType.NUMERIC, 7);
        pnd_Et_Ctr = localVariables.newFieldInRecord("pnd_Et_Ctr", "#ET-CTR", FieldType.NUMERIC, 7);
        pnd_Instn_Cde = localVariables.newFieldInRecord("pnd_Instn_Cde", "#INSTN-CDE", FieldType.NUMERIC, 6);
        pnd_Wpid_Sort = localVariables.newFieldInRecord("pnd_Wpid_Sort", "#WPID-SORT", FieldType.STRING, 3);
        pnd_Last_Dte_Tme_Update_Hold = localVariables.newFieldInRecord("pnd_Last_Dte_Tme_Update_Hold", "#LAST-DTE-TME-UPDATE-HOLD", FieldType.STRING, 
            15);
        pnd_Last_Msg_Data = localVariables.newFieldInRecord("pnd_Last_Msg_Data", "#LAST-MSG-DATA", FieldType.STRING, 8);
        pnd_Individual_Err_Fl = localVariables.newFieldInRecord("pnd_Individual_Err_Fl", "#INDIVIDUAL-ERR-FL", FieldType.BOOLEAN, 1);
        pnd_Call_4700_On_Err = localVariables.newFieldInRecord("pnd_Call_4700_On_Err", "#CALL-4700-ON-ERR", FieldType.BOOLEAN, 1);
        pnd_Err_Ctr = localVariables.newFieldInRecord("pnd_Err_Ctr", "#ERR-CTR", FieldType.NUMERIC, 6);
        pnd_Last_Err_Msg = localVariables.newFieldInRecord("pnd_Last_Err_Msg", "#LAST-ERR-MSG", FieldType.STRING, 79);
        pnd_Last_Err_Prg = localVariables.newFieldInRecord("pnd_Last_Err_Prg", "#LAST-ERR-PRG", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIwfl4112.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Et_Max.setInitialValue(200);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iwfn4712() throws Exception
    {
        super("Iwfn4712");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IWFN4712", onError);
        //*  ======================================================================
        //*                            MAIN LOGIC
        //*  ======================================================================
                                                                                                                                                                          //Natural: PERFORM INITIALIZATIONS
        sub_Initializations();
        if (condition(Global.isEscape())) {return;}
        PROG:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(Global.getPROGRAM());                                                                     //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = *PROGRAM
            pnd_Last_Msg_Data.setValue(Global.getPROGRAM());
            ldaIwfl4112.getVw_ncw_Master_Index().startDatabaseRead                                                                                                        //Natural: READ NCW-MASTER-INDEX BY UNIT-WORK-LIST-KEY2 FROM #START-KEY
            (
            "READ_FILE",
            new Wc[] { new Wc("UNIT_WORK_LIST_KEY2", ">=", pnd_Start_Key, WcType.BY) },
            new Oc[] { new Oc("UNIT_WORK_LIST_KEY2", "ASC") }
            );
            READ_FILE:
            while (condition(ldaIwfl4112.getVw_ncw_Master_Index().readNextRow("READ_FILE")))
            {
                //*  RECORDS READ COUNTER
                //*  JF082699
                pnd_Read_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #READ-CTR
                pnd_Individual_Err_Fl.setValue(true);                                                                                                                     //Natural: ASSIGN #INDIVIDUAL-ERR-FL := TRUE
                if (condition(ldaIwfl4112.getNcw_Master_Index_Admin_Unit_Cde().notEquals(pnd_Parm_Unit_Cde) || ldaIwfl4112.getNcw_Master_Index_Work_List_Ind().getSubstring(1,1).compareTo(pnd_Wklst_Ind_Max.getText())  //Natural: IF NCW-MASTER-INDEX.ADMIN-UNIT-CDE NE #PARM-UNIT-CDE OR SUBSTRING ( NCW-MASTER-INDEX.WORK-LIST-IND,1,1 ) > #WKLST-IND-MAX
                    > 0))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Instn_Cde.reset();                                                                                                                                    //Natural: RESET #INSTN-CDE #WPID-SORT
                pnd_Wpid_Sort.reset();
                //*  NPIRSUNSET START 03/10/2016
                //*    ASSIGN MSG-INFO-SUB.##MSG-DATA(3) = #LAST-MSG-DATA = 'NPRN2000'
                //*    RESET NPRA2000
                //*    MOVE BY NAME NCW-MASTER-INDEX TO NPRA2000
                //*                                                 <<MDM-PIN
                //*    ASSIGN MSG-INFO-SUB.##MSG-DATA(3) = #LAST-MSG-DATA = 'MDMN2000'
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("MDMN2003");                                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'MDMN2003'
                pnd_Last_Msg_Data.setValue("MDMN2003");
                //*                                                 >>MDM-PIN
                //*    WRITE 'FETCH of MDMP0011 for MQ CONN and OPEN'
                //*    FETCH RETURN 'MDMP0011'
                //*    RESET MDMA2000                               <<MDM-PIN
                pdaMdma2003.getMdma2003().reset();                                                                                                                        //Natural: RESET MDMA2003
                //*    MOVE BY NAME NCW-MASTER-INDEX TO MDMA2000
                pdaMdma2003.getMdma2003().setValuesByName(ldaIwfl4112.getVw_ncw_Master_Index());                                                                          //Natural: MOVE BY NAME NCW-MASTER-INDEX TO MDMA2003
                //*                                                 >>MDM-PIN
                pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                       //Natural: ASSIGN CDAOBJ.#FUNCTION = 'GET'
                //*    CALLNAT 'NPRN2000'   /* GET NCW-NPIR REC
                //*      NPRA2000
                //*      NPRA2000-ID
                //*      NPRA2001
                //*      CDAOBJ
                //*      DIALOG-INFO-SUB
                //*      MSG-INFO-SUB
                //*      PASS-SUB
                //*      NPRA2002
                //*      #CSM-PARAMETER-DATA
                //*      #FRGN-GEO-PASS
                //*    CALLNAT 'MDMN2000'   /* GET NCW-NPIR REC     <<MDM-PIN
                //*  GET NCW-NPIR REC
                DbsUtil.callnat(Mdmn2003.class , getCurrentProcessState(), pdaMdma2003.getMdma2003(), pdaMdma2003.getMdma2003_Id(), pdaMdma2001.getMdma2001(),            //Natural: CALLNAT 'MDMN2003' MDMA2003 MDMA2003-ID MDMA2001 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB MDMA2002 #CSM-PARAMETER-DATA #FRGN-GEO-PASS
                    pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaMdma2002.getMdma2002(), 
                    pdaNasa032.getPnd_Csm_Parameter_Data(), pnd_Frgn_Geo_Pass);
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                //*      MDMA2000
                //*      MDMA2000-ID
                //*                                                 >>MDM-PIN
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
                sub_Check_For_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FILE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FILE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    IF NPRA2000.INSTN-NBR > 0
                //*      #INSTN-CDE := NPRA2000.INSTN-NBR
                //*    ELSE
                //*      IF NPRA2000.PPG > ' '
                //*        CALLNAT 'IWFN4799'                       /* GET INSTN-CDE
                //*          NPRA2000.PPG                          /* GIVEN PPG
                //*          #INSTN-CDE
                //*      END-IF
                //*    END-IF
                //*    IF MDMA2000.INSTN-NBR > 0                    <<MDM-PIN
                if (condition(pdaMdma2003.getMdma2003_Instn_Nbr().greater(getZero())))                                                                                    //Natural: IF MDMA2003.INSTN-NBR > 0
                {
                    //*      #INSTN-CDE := MDMA2000.INSTN-NBR
                    pnd_Instn_Cde.setValue(pdaMdma2003.getMdma2003_Instn_Nbr());                                                                                          //Natural: ASSIGN #INSTN-CDE := MDMA2003.INSTN-NBR
                    //*                                                 >>MDM-PIN
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*      IF MDMA2000.PPG > ' '                      /*MDM-PIN
                    //* MDM-PIN
                    if (condition(pdaMdma2003.getMdma2003_Ppg().greater(" ")))                                                                                            //Natural: IF MDMA2003.PPG > ' '
                    {
                        //*  GET INSTN-CDE
                        //* MDM-PIN
                        DbsUtil.callnat(Iwfn4799.class , getCurrentProcessState(), pdaMdma2003.getMdma2003_Ppg(), pnd_Instn_Cde);                                         //Natural: CALLNAT 'IWFN4799' MDMA2003.PPG #INSTN-CDE
                        if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                        //*          MDMA2000.PPG
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*    WRITE 'FETCH of MDMP0012 for MQ CLOSE'
                //*    FETCH RETURN 'MDMP0012'
                //*  NPIRSUNSET STOP  03/10/2016
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("CWFN1400");                                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'CWFN1400'
                pnd_Last_Msg_Data.setValue("CWFN1400");
                pdaCwfa1400.getCwfa1400().reset();                                                                                                                        //Natural: RESET CWFA1400
                pdaCwfa1400.getCwfa1400().setValuesByName(ldaIwfl4112.getVw_ncw_Master_Index());                                                                          //Natural: MOVE BY NAME NCW-MASTER-INDEX TO CWFA1400
                pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                       //Natural: ASSIGN CDAOBJ.#FUNCTION = 'GET'
                DbsUtil.callnat(Cwfn1400.class , getCurrentProcessState(), pdaCwfa1400.getCwfa1400(), pdaCwfa1400.getCwfa1400_Id(), pdaCwfa1401.getCwfa1401(),            //Natural: CALLNAT 'CWFN1400' CWFA1400 CWFA1400-ID CWFA1401 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                    pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
                sub_Check_For_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FILE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FILE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  COMMUNICATION WKLST - JF090299
                //*  WPID SORT
                if (condition(pdaCwfa1400.getCwfa1400_Crprte_Prty_Ind().equals("D") || pdaCwfa1400.getCwfa1400_Crprte_Prty_Ind().equals("J") || pdaCwfa1400.getCwfa1400_Crprte_Prty_Ind().equals("N")  //Natural: IF CWFA1400.CRPRTE-PRTY-IND = 'D' OR = 'J' OR = 'N' OR = 'P'
                    || pdaCwfa1400.getCwfa1400_Crprte_Prty_Ind().equals("P")))
                {
                    pnd_Wpid_Sort.setValue(pdaCwfa1400.getCwfa1400_Crprte_Prty_Ind());                                                                                    //Natural: ASSIGN #WPID-SORT := CWFA1400.CRPRTE-PRTY-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Wpid_Sort.setValue("T");                                                                                                                          //Natural: ASSIGN #WPID-SORT := 'T'
                }                                                                                                                                                         //Natural: END-IF
                //*                                                                                                                                                       //Natural: DECIDE FOR FIRST CONDITION
                short decideConditionsMet1508 = 0;                                                                                                                        //Natural: WHEN SUBSTRING ( NCW-MASTER-INDEX.WORK-LIST-IND,1,1 ) = '1'
                if (condition(ldaIwfl4112.getNcw_Master_Index_Work_List_Ind().getSubstring(1,1).equals("1")))
                {
                    decideConditionsMet1508++;
                    //*  UNASSIGNED
                                                                                                                                                                          //Natural: PERFORM POPULATE-UNASSIGNED-WORK-LIST
                    sub_Populate_Unassigned_Work_List();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FILE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FILE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN NCW-MASTER-INDEX.WORK-LIST-IND = MASK ( '2'.. )
                else if (condition(DbsUtil.maskMatches(ldaIwfl4112.getNcw_Master_Index_Work_List_Ind(),"'2'..")))
                {
                    decideConditionsMet1508++;
                    //*  EMPL WKLST
                                                                                                                                                                          //Natural: PERFORM POPULATE-EMPL-WORK-LIST
                    sub_Populate_Empl_Work_List();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FILE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FILE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet1508 > 0))
                {
                    pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                 //Natural: ASSIGN CDAOBJ.#FUNCTION = 'STORE'
                                                                                                                                                                          //Natural: PERFORM CALL-OBJ-MAINTENANCE
                    sub_Call_Obj_Maintenance();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FILE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FILE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  READ-FILE.
                //*  JF082699
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Individual_Err_Fl.setValue(false);                                                                                                                        //Natural: ASSIGN #INDIVIDUAL-ERR-FL := FALSE
            //*  --------
            //*  ISSUE ET
            //*  --------
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM UPDATE-WK-LIST-RUNS
            sub_Update_Wk_List_Runs();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break PROG;                                                                                                                                         //Natural: ESCAPE BOTTOM ( PROG. )
            //*  ----------------------------------------------------------------------
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
            //*  ----------------------------------------------------------------------
            //*  PROG.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  ======================================================================
        //*                             SUBROUTINES
        //*  ======================================================================
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATIONS
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-UNASSIGNED-WORK-LIST
        //*  ====================================
        //*  POPULATE KEY1 - INSTITUTIONAL DETAIL
        //*  ====================================
        //*  ==============
        //*  POPULATE KEY10
        //*  ==============
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-EMPL-WORK-LIST
        //*  ===============================
        //*  POPULATE KEY3 - EMPLOYEE DETAIL
        //*  ===============================
        //*  ==============
        //*  POPULATE KEY10
        //*  ==============
        //*  ===============
        //*  POPULATE OTHERS -JF091499
        //*  ===============
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-OBJ-MAINTENANCE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-WK-LIST-RUNS
        //*  ----------------------------------------------------------------------
        //*  ---------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-CWFN2100
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-KEY8
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-ERROR
        //*        MSG-INFO-SUB.##MSG         := #LAST-ERR-MSG
        //*        MSG-INFO-SUB.##MSG-DATA(3) := #LAST-ERR-PRG
        //*  -------------------------
        //*  ON NATURAL RUN TIME ERROR
        //*  -------------------------
        //*  ----------------------------------------------------------------------                                                                                       //Natural: ON ERROR
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).equals(" ")))                                                                            //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 3 ) = ' '
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(pnd_Last_Msg_Data);                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) := #LAST-MSG-DATA
        }                                                                                                                                                                 //Natural: END-IF
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
        Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
        if (true) return;
    }
    private void sub_Initializations() throws Exception                                                                                                                   //Natural: INITIALIZATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  JF082699
        //*  INITIALIZE #START-KEY
        //*          /*
        //*  UNASSIGNED AND EMPL WKLST
        pnd_Read_Ctr.reset();                                                                                                                                             //Natural: RESET #READ-CTR #ADD-CTR MSG-INFO-SUB #ERR-CTR #CALL-4700-ON-ERR
        pnd_Add_Ctr.reset();
        pdaCwfpda_M.getMsg_Info_Sub().reset();
        pnd_Err_Ctr.reset();
        pnd_Call_4700_On_Err.reset();
        pnd_Start_Key_Pnd_Unit_Cde.setValue(pnd_Parm_Unit_Cde);                                                                                                           //Natural: ASSIGN #START-KEY.#UNIT-CDE := #PARM-UNIT-CDE
        pnd_Start_Key_Pnd_Work_List_Ind.setValue("1");                                                                                                                    //Natural: ASSIGN #START-KEY.#WORK-LIST-IND := '1'
        pnd_Wklst_Ind_Max.setValue("2");                                                                                                                                  //Natural: ASSIGN #WKLST-IND-MAX := '2'
    }
    private void sub_Populate_Unassigned_Work_List() throws Exception                                                                                                     //Natural: POPULATE-UNASSIGNED-WORK-LIST
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pdaIwfa4700.getIwfa4700().reset();                                                                                                                                //Natural: RESET IWFA4700
        pdaIwfa4700.getIwfa4700_Pnd_Key1_Rec_Type().setValue("ID");                                                                                                       //Natural: ASSIGN IWFA4700.#KEY1-REC-TYPE := 'ID'
        pdaIwfa4700.getIwfa4700_Pnd_Key1_Admin_Unit_Cde().setValue(ldaIwfl4112.getNcw_Master_Index_Admin_Unit_Cde());                                                     //Natural: ASSIGN IWFA4700.#KEY1-ADMIN-UNIT-CDE := NCW-MASTER-INDEX.ADMIN-UNIT-CDE
        pdaIwfa4700.getIwfa4700_Pnd_Key1_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                             //Natural: ASSIGN IWFA4700.#KEY1-INSTN-CDE := #INSTN-CDE
        pdaIwfa4700.getIwfa4700_Pnd_Key1_Crprte_Due_Dte().setValue(ldaIwfl4112.getNcw_Master_Index_Crrnt_Due_Dte());                                                      //Natural: MOVE NCW-MASTER-INDEX.CRRNT-DUE-DTE TO IWFA4700.#KEY1-CRPRTE-DUE-DTE
        pdaIwfa4700.getIwfa4700_Pnd_Key1_Work_Prcss_Id().setValue(ldaIwfl4112.getNcw_Master_Index_Work_Prcss_Id());                                                       //Natural: ASSIGN IWFA4700.#KEY1-WORK-PRCSS-ID := NCW-MASTER-INDEX.WORK-PRCSS-ID
        //*  =============================================
        //*  POPULATE KEY2 - SUPERVISOR TRANSACTION DETAIL
        //*  =============================================
        //*  REMITTANCE
        //*  APPLICATION
        //*  ADJUSTMENTS
        if (condition(pnd_Wpid_Sort.equals("D") || pnd_Wpid_Sort.equals("J") || pnd_Wpid_Sort.equals("N")))                                                               //Natural: IF #WPID-SORT = 'D' OR = 'J' OR = 'N'
        {
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Rec_Type().setValue("STD");                                                                                                  //Natural: ASSIGN IWFA4700.#KEY2-REC-TYPE := 'STD'
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Admin_Unit_Cde().setValue(ldaIwfl4112.getNcw_Master_Index_Admin_Unit_Cde());                                                 //Natural: ASSIGN IWFA4700.#KEY2-ADMIN-UNIT-CDE := NCW-MASTER-INDEX.ADMIN-UNIT-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                         //Natural: ASSIGN IWFA4700.#KEY2-INSTN-CDE := #INSTN-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Wpid_Sort().setValue(pnd_Wpid_Sort);                                                                                         //Natural: ASSIGN IWFA4700.#KEY2-WPID-SORT := #WPID-SORT
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Crprte_Due_Dte().setValue(ldaIwfl4112.getNcw_Master_Index_Crrnt_Due_Dte());                                                  //Natural: MOVE NCW-MASTER-INDEX.CRRNT-DUE-DTE TO IWFA4700.#KEY2-CRPRTE-DUE-DTE
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Work_Prcss_Id().setValue(ldaIwfl4112.getNcw_Master_Index_Work_Prcss_Id());                                                   //Natural: ASSIGN IWFA4700.#KEY2-WORK-PRCSS-ID := NCW-MASTER-INDEX.WORK-PRCSS-ID
        }                                                                                                                                                                 //Natural: END-IF
        //*  ===============================================
        //*  POPULATE KEY4 - SUPERVISOR COMMUNICATION DETAIL - JF090299
        //*  ===============================================
        if (condition(pnd_Wpid_Sort.equals("P")))                                                                                                                         //Natural: IF #WPID-SORT = 'P'
        {
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Rec_Type().setValue("SCD");                                                                                                  //Natural: ASSIGN IWFA4700.#KEY4-REC-TYPE := 'SCD'
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Admin_Unit_Cde().setValue(ldaIwfl4112.getNcw_Master_Index_Admin_Unit_Cde());                                                 //Natural: ASSIGN IWFA4700.#KEY4-ADMIN-UNIT-CDE := NCW-MASTER-INDEX.ADMIN-UNIT-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                         //Natural: ASSIGN IWFA4700.#KEY4-INSTN-CDE := #INSTN-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Wpid_Sort().setValue(pnd_Wpid_Sort);                                                                                         //Natural: ASSIGN IWFA4700.#KEY4-WPID-SORT := #WPID-SORT
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Crprte_Due_Dte().setValue(ldaIwfl4112.getNcw_Master_Index_Crrnt_Due_Dte());                                                  //Natural: MOVE NCW-MASTER-INDEX.CRRNT-DUE-DTE TO IWFA4700.#KEY4-CRPRTE-DUE-DTE
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Work_Prcss_Id().setValue(ldaIwfl4112.getNcw_Master_Index_Work_Prcss_Id());                                                   //Natural: ASSIGN IWFA4700.#KEY4-WORK-PRCSS-ID := NCW-MASTER-INDEX.WORK-PRCSS-ID
            //*  NON-PARTICIPANT
        }                                                                                                                                                                 //Natural: END-IF
        pdaIwfa4700.getIwfa4700_Pnd_Key10_Rqst_Log_Dte_Tme().setValue(ldaIwfl4112.getNcw_Master_Index_Rqst_Log_Dte_Tme());                                                //Natural: ASSIGN IWFA4700.#KEY10-RQST-LOG-DTE-TME := NCW-MASTER-INDEX.RQST-LOG-DTE-TME
        pdaIwfa4700.getIwfa4700_Pnd_Key10_Cwf_Type().setValue("N");                                                                                                       //Natural: ASSIGN IWFA4700.#KEY10-CWF-TYPE := 'N'
        pdaIwfa4700.getIwfa4700_Rcrd_Add_Pgrm().setValue(Global.getPROGRAM());                                                                                            //Natural: ASSIGN IWFA4700.RCRD-ADD-PGRM := *PROGRAM
        pdaIwfa4700.getIwfa4700_Rcrd_Add_User_Id().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN IWFA4700.RCRD-ADD-USER-ID := *INIT-USER
    }
    private void sub_Populate_Empl_Work_List() throws Exception                                                                                                           //Natural: POPULATE-EMPL-WORK-LIST
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pdaIwfa4700.getIwfa4700().reset();                                                                                                                                //Natural: RESET IWFA4700
        //*  =============================================
        //*  POPULATE KEY2 - SUPERVISOR TRANSACTION DETAIL
        //*  =============================================
        //*  REMITTANCE
        //*  APPLICATION
        //*  ADJUSTMENTS
        if (condition(pnd_Wpid_Sort.equals("D") || pnd_Wpid_Sort.equals("J") || pnd_Wpid_Sort.equals("N")))                                                               //Natural: IF #WPID-SORT = 'D' OR = 'J' OR = 'N'
        {
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Rec_Type().setValue("STD");                                                                                                  //Natural: ASSIGN IWFA4700.#KEY2-REC-TYPE := 'STD'
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Admin_Unit_Cde().setValue(ldaIwfl4112.getNcw_Master_Index_Admin_Unit_Cde());                                                 //Natural: ASSIGN IWFA4700.#KEY2-ADMIN-UNIT-CDE := NCW-MASTER-INDEX.ADMIN-UNIT-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                         //Natural: ASSIGN IWFA4700.#KEY2-INSTN-CDE := #INSTN-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Wpid_Sort().setValue(pnd_Wpid_Sort);                                                                                         //Natural: ASSIGN IWFA4700.#KEY2-WPID-SORT := #WPID-SORT
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Crprte_Due_Dte().setValue(ldaIwfl4112.getNcw_Master_Index_Crrnt_Due_Dte());                                                  //Natural: MOVE NCW-MASTER-INDEX.CRRNT-DUE-DTE TO IWFA4700.#KEY2-CRPRTE-DUE-DTE
            pdaIwfa4700.getIwfa4700_Pnd_Key2_Work_Prcss_Id().setValue(ldaIwfl4112.getNcw_Master_Index_Work_Prcss_Id());                                                   //Natural: ASSIGN IWFA4700.#KEY2-WORK-PRCSS-ID := NCW-MASTER-INDEX.WORK-PRCSS-ID
        }                                                                                                                                                                 //Natural: END-IF
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Rec_Type().setValue("ED");                                                                                                       //Natural: ASSIGN IWFA4700.#KEY3-REC-TYPE := 'ED'
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Admin_Unit_Cde().setValue(ldaIwfl4112.getNcw_Master_Index_Admin_Unit_Cde());                                                     //Natural: ASSIGN IWFA4700.#KEY3-ADMIN-UNIT-CDE := NCW-MASTER-INDEX.ADMIN-UNIT-CDE
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Empl_Racf_Id().setValue(ldaIwfl4112.getNcw_Master_Index_Empl_Racf_Id());                                                         //Natural: ASSIGN IWFA4700.#KEY3-EMPL-RACF-ID := NCW-MASTER-INDEX.EMPL-RACF-ID
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                             //Natural: ASSIGN IWFA4700.#KEY3-INSTN-CDE := #INSTN-CDE
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Wpid_Sort().setValue(pnd_Wpid_Sort);                                                                                             //Natural: ASSIGN IWFA4700.#KEY3-WPID-SORT := #WPID-SORT
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Crprte_Due_Dte().setValue(ldaIwfl4112.getNcw_Master_Index_Crrnt_Due_Dte());                                                      //Natural: MOVE NCW-MASTER-INDEX.CRRNT-DUE-DTE TO IWFA4700.#KEY3-CRPRTE-DUE-DTE
        pdaIwfa4700.getIwfa4700_Pnd_Key3_Work_Prcss_Id().setValue(ldaIwfl4112.getNcw_Master_Index_Work_Prcss_Id());                                                       //Natural: ASSIGN IWFA4700.#KEY3-WORK-PRCSS-ID := NCW-MASTER-INDEX.WORK-PRCSS-ID
        if (condition(pnd_Wpid_Sort.equals("P")))                                                                                                                         //Natural: IF #WPID-SORT = 'P'
        {
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Rec_Type().setValue("ECD");                                                                                                  //Natural: ASSIGN IWFA4700.#KEY5-REC-TYPE := 'ECD'
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Admin_Unit_Cde().setValue(ldaIwfl4112.getNcw_Master_Index_Admin_Unit_Cde());                                                 //Natural: ASSIGN IWFA4700.#KEY5-ADMIN-UNIT-CDE := NCW-MASTER-INDEX.ADMIN-UNIT-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Empl_Racf_Id().setValue(ldaIwfl4112.getNcw_Master_Index_Empl_Racf_Id());                                                     //Natural: ASSIGN IWFA4700.#KEY5-EMPL-RACF-ID := NCW-MASTER-INDEX.EMPL-RACF-ID
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                         //Natural: ASSIGN IWFA4700.#KEY5-INSTN-CDE := #INSTN-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Wpid_Sort().setValue(pnd_Wpid_Sort);                                                                                         //Natural: ASSIGN IWFA4700.#KEY5-WPID-SORT := #WPID-SORT
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Crprte_Due_Dte().setValue(ldaIwfl4112.getNcw_Master_Index_Crrnt_Due_Dte());                                                  //Natural: MOVE NCW-MASTER-INDEX.CRRNT-DUE-DTE TO IWFA4700.#KEY5-CRPRTE-DUE-DTE
            pdaIwfa4700.getIwfa4700_Pnd_Key5_Work_Prcss_Id().setValue(ldaIwfl4112.getNcw_Master_Index_Work_Prcss_Id());                                                   //Natural: ASSIGN IWFA4700.#KEY5-WORK-PRCSS-ID := NCW-MASTER-INDEX.WORK-PRCSS-ID
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Rec_Type().setValue("SCD");                                                                                                  //Natural: ASSIGN IWFA4700.#KEY4-REC-TYPE := 'SCD'
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Admin_Unit_Cde().setValue(ldaIwfl4112.getNcw_Master_Index_Admin_Unit_Cde());                                                 //Natural: ASSIGN IWFA4700.#KEY4-ADMIN-UNIT-CDE := NCW-MASTER-INDEX.ADMIN-UNIT-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Instn_Cde().setValue(pnd_Instn_Cde);                                                                                         //Natural: ASSIGN IWFA4700.#KEY4-INSTN-CDE := #INSTN-CDE
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Wpid_Sort().setValue(pnd_Wpid_Sort);                                                                                         //Natural: ASSIGN IWFA4700.#KEY4-WPID-SORT := #WPID-SORT
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Crprte_Due_Dte().setValue(ldaIwfl4112.getNcw_Master_Index_Crrnt_Due_Dte());                                                  //Natural: MOVE NCW-MASTER-INDEX.CRRNT-DUE-DTE TO IWFA4700.#KEY4-CRPRTE-DUE-DTE
            pdaIwfa4700.getIwfa4700_Pnd_Key4_Work_Prcss_Id().setValue(ldaIwfl4112.getNcw_Master_Index_Work_Prcss_Id());                                                   //Natural: ASSIGN IWFA4700.#KEY4-WORK-PRCSS-ID := NCW-MASTER-INDEX.WORK-PRCSS-ID
            //*  NON-PARTICIPANT
        }                                                                                                                                                                 //Natural: END-IF
        pdaIwfa4700.getIwfa4700_Pnd_Key10_Rqst_Log_Dte_Tme().setValue(ldaIwfl4112.getNcw_Master_Index_Rqst_Log_Dte_Tme());                                                //Natural: ASSIGN IWFA4700.#KEY10-RQST-LOG-DTE-TME := NCW-MASTER-INDEX.RQST-LOG-DTE-TME
        pdaIwfa4700.getIwfa4700_Pnd_Key10_Cwf_Type().setValue("N");                                                                                                       //Natural: ASSIGN IWFA4700.#KEY10-CWF-TYPE := 'N'
        pdaIwfa4700.getIwfa4700_Text().getValue(1).setValue(ldaIwfl4112.getNcw_Master_Index_Work_List_Ind());                                                             //Natural: ASSIGN IWFA4700.TEXT ( 1 ) := NCW-MASTER-INDEX.WORK-LIST-IND
        pdaIwfa4700.getIwfa4700_Rcrd_Add_Pgrm().setValue(Global.getPROGRAM());                                                                                            //Natural: ASSIGN IWFA4700.RCRD-ADD-PGRM := *PROGRAM
        pdaIwfa4700.getIwfa4700_Rcrd_Add_User_Id().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN IWFA4700.RCRD-ADD-USER-ID := *INIT-USER
    }
    private void sub_Call_Obj_Maintenance() throws Exception                                                                                                              //Natural: CALL-OBJ-MAINTENANCE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("IWFN4700");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'IWFN4700'
        pnd_Last_Msg_Data.setValue("IWFN4700");
        DbsUtil.callnat(Iwfn4700.class , getCurrentProcessState(), pdaIwfa4700.getIwfa4700(), pdaIwfa4700.getIwfa4700_Id(), pdaIwfa4701.getIwfa4701(),                    //Natural: CALLNAT 'IWFN4700' IWFA4700 IWFA4700-ID IWFA4701 CDAOBJ MSG-INFO-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_M.getMsg_Info_Sub());
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
        sub_Check_For_Error();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE")))                                                                                                //Natural: IF CDAOBJ.#FUNCTION = 'STORE'
        {
            //*  JF082699
            if (condition(! (pnd_Call_4700_On_Err.getBoolean())))                                                                                                         //Natural: IF NOT #CALL-4700-ON-ERR
            {
                pnd_Add_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #ADD-CTR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Et_Ctr.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #ET-CTR
        }                                                                                                                                                                 //Natural: END-IF
        //*  ISSUE ET
        if (condition(pnd_Et_Ctr.greater(pnd_Et_Max)))                                                                                                                    //Natural: IF #ET-CTR > #ET-MAX
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM UPDATE-WK-LIST-RUNS
            sub_Update_Wk_List_Runs();
            if (condition(Global.isEscape())) {return;}
            pnd_Et_Ctr.reset();                                                                                                                                           //Natural: RESET #ET-CTR
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  GET RECORD
    private void sub_Update_Wk_List_Runs() throws Exception                                                                                                               //Natural: UPDATE-WK-LIST-RUNS
    {
        if (BLNatReinput.isReinput()) return;

        pdaIwfa4702.getIwfa4702_Tbl_Scrty_Level_Ind().setValue("A");                                                                                                      //Natural: ASSIGN IWFA4702.TBL-SCRTY-LEVEL-IND := 'A'
        pdaIwfa4702.getIwfa4702_Tbl_Table_Nme().setValue("ENHANCED-WK-LST-RUNS");                                                                                         //Natural: ASSIGN IWFA4702.TBL-TABLE-NME := 'ENHANCED-WK-LST-RUNS'
        pdaIwfa4702.getIwfa4702_Tbl_Key_Field().setValue(pnd_Tbl_Key_Field);                                                                                              //Natural: ASSIGN IWFA4702.TBL-KEY-FIELD := #TBL-KEY-FIELD
        pdaIwfa4702.getIwfa4702_Tbl_Actve_Ind().setValue("A ");                                                                                                           //Natural: ASSIGN IWFA4702.TBL-ACTVE-IND := 'A '
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                               //Natural: ASSIGN CDAOBJ.#FUNCTION := 'GET'
                                                                                                                                                                          //Natural: PERFORM CALL-CWFN2100
        sub_Call_Cwfn2100();
        if (condition(Global.isEscape())) {return;}
        pdaIwfa4702.getIwfa4702_Pnd_Data4_Read_Ctr().nadd(pnd_Read_Ctr);                                                                                                  //Natural: ADD #READ-CTR TO IWFA4702.#DATA4-READ-CTR
        pdaIwfa4702.getIwfa4702_Pnd_Data4_Add_Ctr().nadd(pnd_Add_Ctr);                                                                                                    //Natural: ADD #ADD-CTR TO IWFA4702.#DATA4-ADD-CTR
        //*  JF082699
        if (condition((pnd_Err_Ctr.add(pdaIwfa4702.getIwfa4702_Pnd_Error_Count())).greater(99)))                                                                          //Natural: IF ( #ERR-CTR + IWFA4702.#ERROR-COUNT ) > 99
        {
            pdaIwfa4702.getIwfa4702_Pnd_Error_Count().setValue(99);                                                                                                       //Natural: MOVE 99 TO IWFA4702.#ERROR-COUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIwfa4702.getIwfa4702_Pnd_Error_Count().nadd(pnd_Err_Ctr);                                                                                                  //Natural: ADD #ERR-CTR TO IWFA4702.#ERROR-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("UPDATE");                                                                                                            //Natural: ASSIGN CDAOBJ.#FUNCTION := 'UPDATE'
                                                                                                                                                                          //Natural: PERFORM CALL-CWFN2100
        sub_Call_Cwfn2100();
        if (condition(Global.isEscape())) {return;}
        //*  JF082699
        pnd_Read_Ctr.reset();                                                                                                                                             //Natural: RESET #READ-CTR #ADD-CTR #ERR-CTR
        pnd_Add_Ctr.reset();
        pnd_Err_Ctr.reset();
    }
    private void sub_Call_Cwfn2100() throws Exception                                                                                                                     //Natural: CALL-CWFN2100
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------------------------------------
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("CWFN2100");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'CWFN2100'
        pnd_Last_Msg_Data.setValue("CWFN2100");
        DbsUtil.callnat(Cwfn2100.class , getCurrentProcessState(), pdaIwfa4702.getIwfa4702(), pdaIwfa4702.getIwfa4702_Id(), pdaIwfa4703.getIwfa4703(),                    //Natural: CALLNAT 'CWFN2100' IWFA4702 IWFA4702-ID IWFA4703 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
        sub_Check_For_Error();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE") || pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE")))                                         //Natural: IF CDAOBJ.#FUNCTION = 'STORE' OR = 'UPDATE'
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  JF082699
    private void sub_Populate_Key8() throws Exception                                                                                                                     //Natural: POPULATE-KEY8
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  PARTICIPANT
        pdaIwfa4700.getIwfa4700().reset();                                                                                                                                //Natural: RESET IWFA4700
        pdaIwfa4700.getIwfa4700_Pnd_Key8_Rec_Type().setValue("ERR");                                                                                                      //Natural: ASSIGN IWFA4700.#KEY8-REC-TYPE := 'ERR'
        pdaIwfa4700.getIwfa4700_Pnd_Key8_Admin_Unit_Cde().setValue(ldaIwfl4112.getNcw_Master_Index_Admin_Unit_Cde());                                                     //Natural: ASSIGN IWFA4700.#KEY8-ADMIN-UNIT-CDE := NCW-MASTER-INDEX.ADMIN-UNIT-CDE
        pdaIwfa4700.getIwfa4700_Pnd_Key8_Rqst_Log_Dte_Tme().setValue(ldaIwfl4112.getNcw_Master_Index_Rqst_Log_Dte_Tme());                                                 //Natural: ASSIGN IWFA4700.#KEY8-RQST-LOG-DTE-TME := NCW-MASTER-INDEX.RQST-LOG-DTE-TME
        pdaIwfa4700.getIwfa4700_Pnd_Key8_Cwf_Type().setValue("N");                                                                                                        //Natural: ASSIGN IWFA4700.#KEY8-CWF-TYPE := 'N'
        pdaIwfa4700.getIwfa4700_Pnd_Key8_Error_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                 //Natural: ASSIGN IWFA4700.#KEY8-ERROR-MSG := MSG-INFO-SUB.##MSG
        //*  JB1
        //*  JB1
        pdaIwfa4700.getIwfa4700_Pnd_Key8_Dte_Tme_Stamp().setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                          //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO IWFA4700.#KEY8-DTE-TME-STAMP
        pdaIwfa4700.getIwfa4700_Rcrd_Add_Pgrm().setValue(Global.getPROGRAM());                                                                                            //Natural: ASSIGN IWFA4700.RCRD-ADD-PGRM := *PROGRAM
        pdaIwfa4700.getIwfa4700_Rcrd_Add_User_Id().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN IWFA4700.RCRD-ADD-USER-ID := *INIT-USER
    }
    private void sub_Check_For_Error() throws Exception                                                                                                                   //Natural: CHECK-FOR-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ")                     //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E' OR MSG-INFO-SUB.##ERROR-FIELD > ' ' OR MSG-INFO-SUB.##MSG-NR NE 0
            || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().notEquals(getZero())))
        {
            //*  START JF082699
            if (condition(pnd_Individual_Err_Fl.getBoolean() && ! (pnd_Call_4700_On_Err.getBoolean())))                                                                   //Natural: IF #INDIVIDUAL-ERR-FL AND NOT #CALL-4700-ON-ERR
            {
                pnd_Last_Err_Msg.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                     //Natural: ASSIGN #LAST-ERR-MSG := MSG-INFO-SUB.##MSG
                if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).greater(" ")))                                                                   //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 3 ) > ' '
                {
                    pnd_Last_Err_Prg.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3));                                                                //Natural: ASSIGN #LAST-ERR-PRG := MSG-INFO-SUB.##MSG-DATA ( 3 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Last_Err_Prg.setValue(pnd_Last_Msg_Data);                                                                                                         //Natural: ASSIGN #LAST-ERR-PRG := #LAST-MSG-DATA
                }                                                                                                                                                         //Natural: END-IF
                pnd_Err_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #ERR-CTR
                //*  FORCE ET
                                                                                                                                                                          //Natural: PERFORM POPULATE-KEY8
                sub_Populate_Key8();
                if (condition(Global.isEscape())) {return;}
                pnd_Call_4700_On_Err.setValue(true);                                                                                                                      //Natural: ASSIGN #CALL-4700-ON-ERR := TRUE
                pnd_Et_Ctr.setValue(pnd_Et_Max);                                                                                                                          //Natural: ASSIGN #ET-CTR := #ET-MAX
                pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                     //Natural: ASSIGN CDAOBJ.#FUNCTION = 'STORE'
                                                                                                                                                                          //Natural: PERFORM CALL-OBJ-MAINTENANCE
                sub_Call_Obj_Maintenance();
                if (condition(Global.isEscape())) {return;}
                pnd_Call_4700_On_Err.setValue(false);                                                                                                                     //Natural: ASSIGN #CALL-4700-ON-ERR := FALSE
                pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                    //Natural: RESET MSG-INFO-SUB
                //*  (READ-FILE.)
                if (condition(pnd_Individual_Err_Fl.getBoolean()))                                                                                                        //Natural: REJECT IF #INDIVIDUAL-ERR-FL
                {
                    Global.setEscape(true); Global.setEscapeCode(EscapeType.Top); return;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Call_4700_On_Err.getBoolean()))                                                                                                         //Natural: IF #CALL-4700-ON-ERR
                {
                    //*  START JB1
                    //*  END JB1
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("2 ERRS: ", pnd_Last_Err_Msg, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg()));        //Natural: COMPRESS "2 ERRS: " #LAST-ERR-MSG MSG-INFO-SUB.##MSG INTO MSG-INFO-SUB.##MSG
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(Global.getPROGRAM());                                                             //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) := *PROGRAM
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                        //Natural: RESET MSG-INFO-SUB
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(Global.getPROGRAM());                                                                     //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = *PROGRAM
            pnd_Last_Msg_Data.setValue(Global.getPROGRAM());
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).equals(" ")))                                                                            //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 3 ) = ' '
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(pnd_Last_Msg_Data);                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) := #LAST-MSG-DATA
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE = 'E'
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("NATERR", Global.getERROR_NR(), "LINE", Global.getERROR_LINE()));                             //Natural: COMPRESS 'NATERR' *ERROR-NR 'LINE' *ERROR-LINE INTO MSG-INFO-SUB.##MSG
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
}
