/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:54:18 AM
**        * FROM NATURAL SUBPROGRAM : Icwn3900
************************************************************
**        * FILE NAME            : Icwn3900.java
**        * CLASS NAME           : Icwn3900
**        * INSTANCE NAME        : Icwn3900
************************************************************
************************************************************************
* SUBPROGRAM : ICWN3900
* FUNCTION   : DATE ROUTINE
* AUTHOR     : JOSEPH S. PATINGO
* ----------------------------------------------------------------------
* PARAMETERS :
*
* INPUT PARAMETERS:
* =================
*
*    #START-DTE-TME(T) - REQUIRED. IF BLANK, ERROR CODE IS RETURNED
*    #END-DTE-TME  (T)   OPTIONAL. FILL IN ONLY IF CALCULATED FIELDS
*                        IN OUTPUT PARAMETER IS NEEDED
* ----------------------------------------------------------------------
* OUTPUT PARAMETERS:
* ==================
*
*   CALCULATED FIELDS           CALCULATION DONE IN DATE-CALC-ROUTINE
*   -----------------
*    #CALENDAR-DAYS      (N18)  CALENDAR DAYS BETWEEN START & END
*    #BUSINESS-DAYS      (N18)  BUSINESS DAYS BETWEEN START & END
*    #HOLIDAY-AND-WEEKEND(N18)  NON WORKING DAYS BETWEEN START & END
*
*   REFERENCE FIELDS            PROCESSING DONE IN DATE-PROCESS-ROUTINE
*   ----------------
*    #SDTE-HOLIDAY  (A20)       HOLIDAY - FILLED W/ HOLIDAY DESCRIPTION
*    #SDTE-DAYOFWEEK(A9)        RETURNS NAME OF DAY OF WEEK
*    #BSNSS-D       (D)         RETURNS NEXT BUSINESS DAY IF
*                               START DATE IS A WEEKEND OR HOLIDAY
*    #WEEK-END-D    (D)         DATE FOR WEEKEND PROCESSING
*    #MONTH-END-D   (D)         DATE FOR MONTH END PROCESSING
*    #QTR-END-D     (D)         DATE FOR QUARTER END PROCESSING
*
*   RETURN CODES AND MESSAGES
*   -------------------------
*   PDA ICWPDA-M                FILLED ONLY WHEN AN ERROR OCCURS.
*
*       ##MSG           (A79)   PASSED/RETURNED MESSAGE
*       ##RETURN-CODE   (A1)    TYPE OF MESSAGE
*                                ' ' = INFORMATIONAL
*                                'W' = WARNING; PROCESS CAN CONTINUE
*                                'E' = ERROR (FATAL)
*       ##ERROR-FIELD   (A32)   NAME OF FIELD IN ERROR.
*
************************************************************************
*
*
* NOTES      : HOLIDAYS ARE :
*
* NEW YEAR          01/01/YYYY * HOLIDAY ON ACTUAL DATE
*
* PRESIDENTS DAY    3RD MONDAY IN FEBRUARY
*
* MEMORIAL DAY      LAST MONDAY IN MAY
*
* INDEPENDENCE DAY  07/04/YYYY * IF ON SUNDAY, HOLIDAY NEXT DAY
*
* LABOR DAY         1ST MONDAY IN SEPTEMBER
*
* THANKSGIVING DAY  4TH THURSDAY IN NOVEMBER
*
* CHRISTMAS DAY     12/25/YYYY * IF SATURDAY, HOLIDAY ON PREV DAY
*                                IF SUNDAY, HOLIDAY ON NEXT DAY
*
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwn3900 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIcwpda_M pdaIcwpda_M;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Start_Dte_Tme;
    private DbsField pnd_End_Dte_Tme;
    private DbsField pnd_Calendar_Days;
    private DbsField pnd_Business_Days;
    private DbsField pnd_Holiday_And_Weekend;
    private DbsField pnd_Sdte_Holiday;
    private DbsField pnd_Sdte_Dayofweek;
    private DbsField pnd_Bsnss_D;
    private DbsField pnd_Week_End_D;
    private DbsField pnd_Month_End_D;
    private DbsField pnd_Qtr_End_D;
    private DbsField pnd_Proc_Date;
    private DbsField pnd_Start_Date_A;

    private DbsGroup pnd_Start_Date_A__R_Field_1;
    private DbsField pnd_Start_Date_A_Pnd_Start_Year;

    private DbsGroup pnd_Start_Date_A__R_Field_2;
    private DbsField pnd_Start_Date_A_Pnd_Start_Year_A;
    private DbsField pnd_Start_Date_A_Pnd_Start_Month;

    private DbsGroup pnd_Start_Date_A__R_Field_3;
    private DbsField pnd_Start_Date_A_Pnd_Start_Month_A;
    private DbsField pnd_Start_Date_A_Pnd_Start_Day;

    private DbsGroup pnd_Start_Date_A__R_Field_4;
    private DbsField pnd_Start_Date_A_Pnd_Start_Day_A;
    private DbsField pnd_No_End_Process;
    private DbsField pnd_End_Date_A;

    private DbsGroup pnd_End_Date_A__R_Field_5;
    private DbsField pnd_End_Date_A_Pnd_End_Year;

    private DbsGroup pnd_End_Date_A__R_Field_6;
    private DbsField pnd_End_Date_A_Pnd_End_Year_A;
    private DbsField pnd_End_Date_A_Pnd_End_Month;

    private DbsGroup pnd_End_Date_A__R_Field_7;
    private DbsField pnd_End_Date_A_Pnd_End_Month_A;
    private DbsField pnd_End_Date_A_Pnd_End_Day;

    private DbsGroup pnd_End_Date_A__R_Field_8;
    private DbsField pnd_End_Date_A_Pnd_End_Day_A;
    private DbsField pnd_Start_Date;
    private DbsField pnd_End_Date;
    private DbsField pnd_Year;

    private DbsGroup pnd_Year__R_Field_9;
    private DbsField pnd_Year_Pnd_Year_N;
    private DbsField pnd_Date_A;
    private DbsField pnd_Date;
    private DbsField pnd_Name_Of_Day;
    private DbsField pnd_Day_Ctr;

    private DbsGroup pnd_Holidays;
    private DbsField pnd_Holidays_Pnd_Holiday_Date;
    private DbsField pnd_Holidays_Pnd_Holiday;
    private DbsField pnd_H_Ctr;
    private DbsField pnd_Start_Ctr;
    private DbsField pnd_Between_Ctr;
    private DbsField pnd_End_Ctr;
    private DbsField pnd_Work_Date;
    private DbsField pnd_Holiday_Ctr;
    private DbsField pnd_Nbr_Of_Weekends;
    private DbsField pnd_Smonth;

    private DbsGroup pnd_Smonth__R_Field_10;
    private DbsField pnd_Smonth_Pnd_Smonth_A;
    private DbsField pnd_Base_Date;

    private DbsGroup pnd_Base_Date__R_Field_11;
    private DbsField pnd_Base_Date_Pnd_Base_Year;
    private DbsField pnd_Base_Date_Pnd_Base_Month;
    private DbsField pnd_Base_Date_Pnd_Base_Day;
    private DbsField pnd_Week_Names;
    private DbsField pnd_Month_Ends;
    private DbsField pnd_Qtr_Ends;
    private DbsField pnd_Day_No;
    private DbsField pnd_Base_Dated;
    private DbsField pnd_Feb_End;

    private DbsGroup pnd_Feb_End__R_Field_12;
    private DbsField pnd_Feb_End_Pnd_Ffeb;
    private DbsField pnd_Feb_End_Pnd_Feb_Last_Day;
    private DbsField pnd_Done;
    private DbsField pnd_Match;
    private DbsField pnd_Nameofday;
    private DbsField pnd_Qtr_End;

    private DbsGroup pnd_Qtr_End__R_Field_13;
    private DbsField pnd_Qtr_End_Pnd_Qtr_End_Yy;
    private DbsField pnd_Qtr_End_Pnd_Qtr_End_Mm;
    private DbsField pnd_Qtr_End_Pnd_Qtr_End_Dd;
    private DbsField pnd_Week_End;
    private DbsField pnd_Month_End;

    private DbsGroup pnd_Month_End__R_Field_14;
    private DbsField pnd_Month_End_Pnd_Month_End_Yy;
    private DbsField pnd_Month_End_Pnd_Month_End_Mm;
    private DbsField pnd_Month_End_Pnd_Month_End_Dd;
    private DbsField pnd_Business;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Start_Dte_Tme = parameters.newFieldInRecord("pnd_Start_Dte_Tme", "#START-DTE-TME", FieldType.TIME);
        pnd_Start_Dte_Tme.setParameterOption(ParameterOption.ByReference);
        pnd_End_Dte_Tme = parameters.newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.TIME);
        pnd_End_Dte_Tme.setParameterOption(ParameterOption.ByReference);
        pnd_Calendar_Days = parameters.newFieldInRecord("pnd_Calendar_Days", "#CALENDAR-DAYS", FieldType.NUMERIC, 18);
        pnd_Calendar_Days.setParameterOption(ParameterOption.ByReference);
        pnd_Business_Days = parameters.newFieldInRecord("pnd_Business_Days", "#BUSINESS-DAYS", FieldType.NUMERIC, 18);
        pnd_Business_Days.setParameterOption(ParameterOption.ByReference);
        pnd_Holiday_And_Weekend = parameters.newFieldInRecord("pnd_Holiday_And_Weekend", "#HOLIDAY-AND-WEEKEND", FieldType.NUMERIC, 18);
        pnd_Holiday_And_Weekend.setParameterOption(ParameterOption.ByReference);
        pnd_Sdte_Holiday = parameters.newFieldInRecord("pnd_Sdte_Holiday", "#SDTE-HOLIDAY", FieldType.STRING, 20);
        pnd_Sdte_Holiday.setParameterOption(ParameterOption.ByReference);
        pnd_Sdte_Dayofweek = parameters.newFieldInRecord("pnd_Sdte_Dayofweek", "#SDTE-DAYOFWEEK", FieldType.STRING, 9);
        pnd_Sdte_Dayofweek.setParameterOption(ParameterOption.ByReference);
        pnd_Bsnss_D = parameters.newFieldInRecord("pnd_Bsnss_D", "#BSNSS-D", FieldType.DATE);
        pnd_Bsnss_D.setParameterOption(ParameterOption.ByReference);
        pnd_Week_End_D = parameters.newFieldInRecord("pnd_Week_End_D", "#WEEK-END-D", FieldType.DATE);
        pnd_Week_End_D.setParameterOption(ParameterOption.ByReference);
        pnd_Month_End_D = parameters.newFieldInRecord("pnd_Month_End_D", "#MONTH-END-D", FieldType.DATE);
        pnd_Month_End_D.setParameterOption(ParameterOption.ByReference);
        pnd_Qtr_End_D = parameters.newFieldInRecord("pnd_Qtr_End_D", "#QTR-END-D", FieldType.DATE);
        pnd_Qtr_End_D.setParameterOption(ParameterOption.ByReference);
        pdaIcwpda_M = new PdaIcwpda_M(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Proc_Date = localVariables.newFieldInRecord("pnd_Proc_Date", "#PROC-DATE", FieldType.TIME);
        pnd_Start_Date_A = localVariables.newFieldInRecord("pnd_Start_Date_A", "#START-DATE-A", FieldType.STRING, 8);

        pnd_Start_Date_A__R_Field_1 = localVariables.newGroupInRecord("pnd_Start_Date_A__R_Field_1", "REDEFINE", pnd_Start_Date_A);
        pnd_Start_Date_A_Pnd_Start_Year = pnd_Start_Date_A__R_Field_1.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Year", "#START-YEAR", FieldType.NUMERIC, 
            4);

        pnd_Start_Date_A__R_Field_2 = pnd_Start_Date_A__R_Field_1.newGroupInGroup("pnd_Start_Date_A__R_Field_2", "REDEFINE", pnd_Start_Date_A_Pnd_Start_Year);
        pnd_Start_Date_A_Pnd_Start_Year_A = pnd_Start_Date_A__R_Field_2.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Year_A", "#START-YEAR-A", FieldType.STRING, 
            4);
        pnd_Start_Date_A_Pnd_Start_Month = pnd_Start_Date_A__R_Field_1.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Month", "#START-MONTH", FieldType.NUMERIC, 
            2);

        pnd_Start_Date_A__R_Field_3 = pnd_Start_Date_A__R_Field_1.newGroupInGroup("pnd_Start_Date_A__R_Field_3", "REDEFINE", pnd_Start_Date_A_Pnd_Start_Month);
        pnd_Start_Date_A_Pnd_Start_Month_A = pnd_Start_Date_A__R_Field_3.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Month_A", "#START-MONTH-A", FieldType.STRING, 
            2);
        pnd_Start_Date_A_Pnd_Start_Day = pnd_Start_Date_A__R_Field_1.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Day", "#START-DAY", FieldType.NUMERIC, 
            2);

        pnd_Start_Date_A__R_Field_4 = pnd_Start_Date_A__R_Field_1.newGroupInGroup("pnd_Start_Date_A__R_Field_4", "REDEFINE", pnd_Start_Date_A_Pnd_Start_Day);
        pnd_Start_Date_A_Pnd_Start_Day_A = pnd_Start_Date_A__R_Field_4.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Day_A", "#START-DAY-A", FieldType.STRING, 
            2);
        pnd_No_End_Process = localVariables.newFieldInRecord("pnd_No_End_Process", "#NO-END-PROCESS", FieldType.BOOLEAN, 1);
        pnd_End_Date_A = localVariables.newFieldInRecord("pnd_End_Date_A", "#END-DATE-A", FieldType.STRING, 8);

        pnd_End_Date_A__R_Field_5 = localVariables.newGroupInRecord("pnd_End_Date_A__R_Field_5", "REDEFINE", pnd_End_Date_A);
        pnd_End_Date_A_Pnd_End_Year = pnd_End_Date_A__R_Field_5.newFieldInGroup("pnd_End_Date_A_Pnd_End_Year", "#END-YEAR", FieldType.NUMERIC, 4);

        pnd_End_Date_A__R_Field_6 = pnd_End_Date_A__R_Field_5.newGroupInGroup("pnd_End_Date_A__R_Field_6", "REDEFINE", pnd_End_Date_A_Pnd_End_Year);
        pnd_End_Date_A_Pnd_End_Year_A = pnd_End_Date_A__R_Field_6.newFieldInGroup("pnd_End_Date_A_Pnd_End_Year_A", "#END-YEAR-A", FieldType.STRING, 4);
        pnd_End_Date_A_Pnd_End_Month = pnd_End_Date_A__R_Field_5.newFieldInGroup("pnd_End_Date_A_Pnd_End_Month", "#END-MONTH", FieldType.NUMERIC, 2);

        pnd_End_Date_A__R_Field_7 = pnd_End_Date_A__R_Field_5.newGroupInGroup("pnd_End_Date_A__R_Field_7", "REDEFINE", pnd_End_Date_A_Pnd_End_Month);
        pnd_End_Date_A_Pnd_End_Month_A = pnd_End_Date_A__R_Field_7.newFieldInGroup("pnd_End_Date_A_Pnd_End_Month_A", "#END-MONTH-A", FieldType.STRING, 
            2);
        pnd_End_Date_A_Pnd_End_Day = pnd_End_Date_A__R_Field_5.newFieldInGroup("pnd_End_Date_A_Pnd_End_Day", "#END-DAY", FieldType.NUMERIC, 2);

        pnd_End_Date_A__R_Field_8 = pnd_End_Date_A__R_Field_5.newGroupInGroup("pnd_End_Date_A__R_Field_8", "REDEFINE", pnd_End_Date_A_Pnd_End_Day);
        pnd_End_Date_A_Pnd_End_Day_A = pnd_End_Date_A__R_Field_8.newFieldInGroup("pnd_End_Date_A_Pnd_End_Day_A", "#END-DAY-A", FieldType.STRING, 2);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.DATE);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.DATE);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.STRING, 4);

        pnd_Year__R_Field_9 = localVariables.newGroupInRecord("pnd_Year__R_Field_9", "REDEFINE", pnd_Year);
        pnd_Year_Pnd_Year_N = pnd_Year__R_Field_9.newFieldInGroup("pnd_Year_Pnd_Year_N", "#YEAR-N", FieldType.NUMERIC, 4);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Name_Of_Day = localVariables.newFieldInRecord("pnd_Name_Of_Day", "#NAME-OF-DAY", FieldType.STRING, 3);
        pnd_Day_Ctr = localVariables.newFieldInRecord("pnd_Day_Ctr", "#DAY-CTR", FieldType.PACKED_DECIMAL, 2);

        pnd_Holidays = localVariables.newGroupArrayInRecord("pnd_Holidays", "#HOLIDAYS", new DbsArrayController(1, 7));
        pnd_Holidays_Pnd_Holiday_Date = pnd_Holidays.newFieldInGroup("pnd_Holidays_Pnd_Holiday_Date", "#HOLIDAY-DATE", FieldType.DATE);
        pnd_Holidays_Pnd_Holiday = pnd_Holidays.newFieldInGroup("pnd_Holidays_Pnd_Holiday", "#HOLIDAY", FieldType.STRING, 20);
        pnd_H_Ctr = localVariables.newFieldInRecord("pnd_H_Ctr", "#H-CTR", FieldType.PACKED_DECIMAL, 1);
        pnd_Start_Ctr = localVariables.newFieldInRecord("pnd_Start_Ctr", "#START-CTR", FieldType.PACKED_DECIMAL, 3);
        pnd_Between_Ctr = localVariables.newFieldInRecord("pnd_Between_Ctr", "#BETWEEN-CTR", FieldType.PACKED_DECIMAL, 3);
        pnd_End_Ctr = localVariables.newFieldInRecord("pnd_End_Ctr", "#END-CTR", FieldType.PACKED_DECIMAL, 3);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_Holiday_Ctr = localVariables.newFieldInRecord("pnd_Holiday_Ctr", "#HOLIDAY-CTR", FieldType.PACKED_DECIMAL, 18);
        pnd_Nbr_Of_Weekends = localVariables.newFieldInRecord("pnd_Nbr_Of_Weekends", "#NBR-OF-WEEKENDS", FieldType.PACKED_DECIMAL, 18);
        pnd_Smonth = localVariables.newFieldInRecord("pnd_Smonth", "#SMONTH", FieldType.NUMERIC, 2);

        pnd_Smonth__R_Field_10 = localVariables.newGroupInRecord("pnd_Smonth__R_Field_10", "REDEFINE", pnd_Smonth);
        pnd_Smonth_Pnd_Smonth_A = pnd_Smonth__R_Field_10.newFieldInGroup("pnd_Smonth_Pnd_Smonth_A", "#SMONTH-A", FieldType.STRING, 2);
        pnd_Base_Date = localVariables.newFieldInRecord("pnd_Base_Date", "#BASE-DATE", FieldType.STRING, 8);

        pnd_Base_Date__R_Field_11 = localVariables.newGroupInRecord("pnd_Base_Date__R_Field_11", "REDEFINE", pnd_Base_Date);
        pnd_Base_Date_Pnd_Base_Year = pnd_Base_Date__R_Field_11.newFieldInGroup("pnd_Base_Date_Pnd_Base_Year", "#BASE-YEAR", FieldType.STRING, 4);
        pnd_Base_Date_Pnd_Base_Month = pnd_Base_Date__R_Field_11.newFieldInGroup("pnd_Base_Date_Pnd_Base_Month", "#BASE-MONTH", FieldType.STRING, 2);
        pnd_Base_Date_Pnd_Base_Day = pnd_Base_Date__R_Field_11.newFieldInGroup("pnd_Base_Date_Pnd_Base_Day", "#BASE-DAY", FieldType.STRING, 2);
        pnd_Week_Names = localVariables.newFieldArrayInRecord("pnd_Week_Names", "#WEEK-NAMES", FieldType.STRING, 9, new DbsArrayController(1, 7));
        pnd_Month_Ends = localVariables.newFieldArrayInRecord("pnd_Month_Ends", "#MONTH-ENDS", FieldType.NUMERIC, 2, new DbsArrayController(1, 12));
        pnd_Qtr_Ends = localVariables.newFieldArrayInRecord("pnd_Qtr_Ends", "#QTR-ENDS", FieldType.NUMERIC, 2, new DbsArrayController(1, 4));
        pnd_Day_No = localVariables.newFieldInRecord("pnd_Day_No", "#DAY-NO", FieldType.NUMERIC, 1);
        pnd_Base_Dated = localVariables.newFieldInRecord("pnd_Base_Dated", "#BASE-DATED", FieldType.DATE);
        pnd_Feb_End = localVariables.newFieldInRecord("pnd_Feb_End", "#FEB-END", FieldType.STRING, 8);

        pnd_Feb_End__R_Field_12 = localVariables.newGroupInRecord("pnd_Feb_End__R_Field_12", "REDEFINE", pnd_Feb_End);
        pnd_Feb_End_Pnd_Ffeb = pnd_Feb_End__R_Field_12.newFieldInGroup("pnd_Feb_End_Pnd_Ffeb", "#FFEB", FieldType.STRING, 6);
        pnd_Feb_End_Pnd_Feb_Last_Day = pnd_Feb_End__R_Field_12.newFieldInGroup("pnd_Feb_End_Pnd_Feb_Last_Day", "#FEB-LAST-DAY", FieldType.NUMERIC, 2);
        pnd_Done = localVariables.newFieldInRecord("pnd_Done", "#DONE", FieldType.BOOLEAN, 1);
        pnd_Match = localVariables.newFieldInRecord("pnd_Match", "#MATCH", FieldType.BOOLEAN, 1);
        pnd_Nameofday = localVariables.newFieldInRecord("pnd_Nameofday", "#NAMEOFDAY", FieldType.STRING, 9);
        pnd_Qtr_End = localVariables.newFieldInRecord("pnd_Qtr_End", "#QTR-END", FieldType.STRING, 8);

        pnd_Qtr_End__R_Field_13 = localVariables.newGroupInRecord("pnd_Qtr_End__R_Field_13", "REDEFINE", pnd_Qtr_End);
        pnd_Qtr_End_Pnd_Qtr_End_Yy = pnd_Qtr_End__R_Field_13.newFieldInGroup("pnd_Qtr_End_Pnd_Qtr_End_Yy", "#QTR-END-YY", FieldType.STRING, 4);
        pnd_Qtr_End_Pnd_Qtr_End_Mm = pnd_Qtr_End__R_Field_13.newFieldInGroup("pnd_Qtr_End_Pnd_Qtr_End_Mm", "#QTR-END-MM", FieldType.STRING, 2);
        pnd_Qtr_End_Pnd_Qtr_End_Dd = pnd_Qtr_End__R_Field_13.newFieldInGroup("pnd_Qtr_End_Pnd_Qtr_End_Dd", "#QTR-END-DD", FieldType.STRING, 2);
        pnd_Week_End = localVariables.newFieldInRecord("pnd_Week_End", "#WEEK-END", FieldType.STRING, 8);
        pnd_Month_End = localVariables.newFieldInRecord("pnd_Month_End", "#MONTH-END", FieldType.STRING, 8);

        pnd_Month_End__R_Field_14 = localVariables.newGroupInRecord("pnd_Month_End__R_Field_14", "REDEFINE", pnd_Month_End);
        pnd_Month_End_Pnd_Month_End_Yy = pnd_Month_End__R_Field_14.newFieldInGroup("pnd_Month_End_Pnd_Month_End_Yy", "#MONTH-END-YY", FieldType.STRING, 
            4);
        pnd_Month_End_Pnd_Month_End_Mm = pnd_Month_End__R_Field_14.newFieldInGroup("pnd_Month_End_Pnd_Month_End_Mm", "#MONTH-END-MM", FieldType.STRING, 
            2);
        pnd_Month_End_Pnd_Month_End_Dd = pnd_Month_End__R_Field_14.newFieldInGroup("pnd_Month_End_Pnd_Month_End_Dd", "#MONTH-END-DD", FieldType.STRING, 
            2);
        pnd_Business = localVariables.newFieldInRecord("pnd_Business", "#BUSINESS", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Week_Names.getValue(1).setInitialValue("Monday   ");
        pnd_Week_Names.getValue(2).setInitialValue("Tuesday  ");
        pnd_Week_Names.getValue(3).setInitialValue("Wednesday");
        pnd_Week_Names.getValue(4).setInitialValue("Thursday ");
        pnd_Week_Names.getValue(5).setInitialValue("Friday   ");
        pnd_Week_Names.getValue(6).setInitialValue("Saturday ");
        pnd_Week_Names.getValue(7).setInitialValue("Sunday   ");
        pnd_Month_Ends.getValue(1).setInitialValue(31);
        pnd_Month_Ends.getValue(2).setInitialValue(28);
        pnd_Month_Ends.getValue(3).setInitialValue(31);
        pnd_Month_Ends.getValue(4).setInitialValue(30);
        pnd_Month_Ends.getValue(5).setInitialValue(31);
        pnd_Month_Ends.getValue(6).setInitialValue(30);
        pnd_Month_Ends.getValue(7).setInitialValue(31);
        pnd_Month_Ends.getValue(8).setInitialValue(31);
        pnd_Month_Ends.getValue(9).setInitialValue(30);
        pnd_Month_Ends.getValue(10).setInitialValue(31);
        pnd_Month_Ends.getValue(11).setInitialValue(30);
        pnd_Month_Ends.getValue(12).setInitialValue(31);
        pnd_Qtr_Ends.getValue(1).setInitialValue(3);
        pnd_Qtr_Ends.getValue(2).setInitialValue(6);
        pnd_Qtr_Ends.getValue(3).setInitialValue(9);
        pnd_Qtr_Ends.getValue(4).setInitialValue(12);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Icwn3900() throws Exception
    {
        super("Icwn3900");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  --------------------------------------------------
        //*  FIX BASE DATA
        //*  --------------------------------------------------
        pnd_Base_Date.setValueEdited(pnd_Start_Dte_Tme,new ReportEditMask("YYYYMMDD"));                                                                                   //Natural: MOVE EDITED #START-DTE-TME ( EM = YYYYMMDD ) TO #BASE-DATE
        pnd_Base_Date_Pnd_Base_Month.setValue("03");                                                                                                                      //Natural: MOVE '03' TO #BASE-MONTH
        pnd_Base_Date_Pnd_Base_Day.setValue("01");                                                                                                                        //Natural: MOVE '01' TO #BASE-DAY
        pnd_Base_Dated.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Base_Date);                                                                                      //Natural: MOVE EDITED #BASE-DATE TO #BASE-DATED ( EM = YYYYMMDD )
        pnd_Base_Dated.nsubtract(1);                                                                                                                                      //Natural: SUBTRACT 1 FROM #BASE-DATED
        pnd_Feb_End.setValueEdited(pnd_Base_Dated,new ReportEditMask("YYYYMMDD"));                                                                                        //Natural: MOVE EDITED #BASE-DATED ( EM = YYYYMMDD ) TO #FEB-END
        pnd_Month_Ends.getValue(2).setValue(pnd_Feb_End_Pnd_Feb_Last_Day);                                                                                                //Natural: MOVE #FEB-LAST-DAY TO #MONTH-ENDS ( 2 )
        //*  --------------------------------------------------
        //*  VALIDATE INPUT DATA
        //*  --------------------------------------------------
        pdaIcwpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: RESET MSG-INFO-SUB
        pnd_Start_Date.reset();                                                                                                                                           //Natural: RESET #START-DATE #END-DATE
        pnd_End_Date.reset();
        if (condition(pnd_Start_Dte_Tme.greater(getZero())))                                                                                                              //Natural: IF #START-DTE-TME GT 0
        {
            pnd_Start_Date.setValue(pnd_Start_Dte_Tme);                                                                                                                   //Natural: MOVE #START-DTE-TME TO #START-DATE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_End_Dte_Tme.greater(getZero())))                                                                                                                //Natural: IF #END-DTE-TME GT 0
        {
            pnd_End_Date.setValue(pnd_End_Dte_Tme);                                                                                                                       //Natural: MOVE #END-DTE-TME TO #END-DATE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Start_Date.lessOrEqual(getZero())))                                                                                                             //Natural: IF #START-DATE LE 0
        {
            pdaIcwpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: MOVE 'E' TO ##RETURN-CODE
            pdaIcwpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("#START-DATE");                                                                                    //Natural: MOVE '#START-DATE' TO ##ERROR-FIELD
            pdaIcwpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("NO VALUE IN START DATE FIELD");                                                                           //Natural: MOVE 'NO VALUE IN START DATE FIELD' TO ##MSG
            pnd_Holiday_And_Weekend.reset();                                                                                                                              //Natural: RESET #HOLIDAY-AND-WEEKEND
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Start_Date.greater(getZero()) && pnd_End_Date.greater(getZero())))                                                                              //Natural: IF #START-DATE GT 0 AND #END-DATE GT 0
        {
            if (condition(pnd_End_Date.less(pnd_Start_Date)))                                                                                                             //Natural: IF #END-DATE LT #START-DATE
            {
                pdaIcwpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: MOVE 'E' TO ##RETURN-CODE
                pdaIcwpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("#END-DATE");                                                                                  //Natural: MOVE '#END-DATE' TO ##ERROR-FIELD
                pdaIcwpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("END DATE GREATER THAN START-DATE");                                                                   //Natural: MOVE 'END DATE GREATER THAN START-DATE' TO ##MSG
                pnd_Holiday_And_Weekend.reset();                                                                                                                          //Natural: RESET #HOLIDAY-AND-WEEKEND
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Date_A.setValueEdited(pnd_Start_Date,new ReportEditMask("YYYYMMDD"));                                                                                         //Natural: MOVE EDITED #START-DATE ( EM = YYYYMMDD ) TO #DATE-A
        pnd_Start_Date_A.setValue(pnd_Date_A);                                                                                                                            //Natural: MOVE #DATE-A TO #START-DATE-A
        if (condition(pnd_End_Date.greater(getZero())))                                                                                                                   //Natural: IF #END-DATE GT 0
        {
                                                                                                                                                                          //Natural: PERFORM DATE-CALC-ROUTINE
            sub_Date_Calc_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DATE-PROCESS-ROUTINE
        sub_Date_Process_Routine();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  SUBROUTINES
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DATE-CALC-ROUTINE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DATE-PROCESS-ROUTINE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUSINESS-DATE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WEEK-END
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MONTH-END
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: QTR-END
        //*  ----------------------------------------------------------------------
        //*  INCLUDE CWFC3901
        //*  ----------------------------------------------------------------------
        //*  CWFC3901 : 7 HOLIDAY CALCULATION ROUTINE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEW-YEARS-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRESIDENTS-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MEMORIAL-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INDEPENDENCE-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LABOR-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: THANKSGIVING-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHRISTMAS-DAY
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-NO-OF-WEEKENDS
    }
    private void sub_Date_Calc_Routine() throws Exception                                                                                                                 //Natural: DATE-CALC-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Calendar_Days.compute(new ComputeParameters(false, pnd_Calendar_Days), (pnd_End_Date.subtract(pnd_Start_Date)));                                              //Natural: ASSIGN #CALENDAR-DAYS := ( #END-DATE - #START-DATE )
        pnd_Date_A.setValueEdited(pnd_End_Date,new ReportEditMask("YYYYMMDD"));                                                                                           //Natural: MOVE EDITED #END-DATE ( EM = YYYYMMDD ) TO #DATE-A
        pnd_End_Date_A.setValue(pnd_Date_A);                                                                                                                              //Natural: MOVE #DATE-A TO #END-DATE-A
        FOR01:                                                                                                                                                            //Natural: FOR #YEAR-N = #START-YEAR THRU #END-YEAR STEP 1
        for (pnd_Year_Pnd_Year_N.setValue(pnd_Start_Date_A_Pnd_Start_Year); condition(pnd_Year_Pnd_Year_N.lessOrEqual(pnd_End_Date_A_Pnd_End_Year)); pnd_Year_Pnd_Year_N.nadd(1))
        {
                                                                                                                                                                          //Natural: PERFORM NEW-YEARS-DAY
            sub_New_Years_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM PRESIDENTS-DAY
            sub_Presidents_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM MEMORIAL-DAY
            sub_Memorial_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM INDEPENDENCE-DAY
            sub_Independence_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM LABOR-DAY
            sub_Labor_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM THANKSGIVING-DAY
            sub_Thanksgiving_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CHRISTMAS-DAY
            sub_Christmas_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            short decideConditionsMet329 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #YEAR-N;//Natural: VALUE #START-YEAR
            if (condition((pnd_Year_Pnd_Year_N.equals(pnd_Start_Date_A_Pnd_Start_Year))))
            {
                decideConditionsMet329++;
                FOR02:                                                                                                                                                    //Natural: FOR #H-CTR = 1 TO 7
                for (pnd_H_Ctr.setValue(1); condition(pnd_H_Ctr.lessOrEqual(7)); pnd_H_Ctr.nadd(1))
                {
                    if (condition(pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).greater(getZero()) && pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).greaterOrEqual(pnd_Start_Date)  //Natural: IF #HOLIDAY-DATE ( #H-CTR ) GT 0 AND #HOLIDAY-DATE ( #H-CTR ) GE #START-DATE AND #HOLIDAY-DATE ( #H-CTR ) LE #END-DATE
                        && pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).lessOrEqual(pnd_End_Date)))
                    {
                        pnd_Holiday_Ctr.nadd(1);                                                                                                                          //Natural: ADD 1 TO #HOLIDAY-CTR
                        pnd_Start_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #START-CTR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE #END-YEAR
            else if (condition((pnd_Year_Pnd_Year_N.equals(pnd_End_Date_A_Pnd_End_Year))))
            {
                decideConditionsMet329++;
                if (condition(pnd_End_Date_A_Pnd_End_Year.greater(pnd_Start_Date_A_Pnd_Start_Year)))                                                                      //Natural: IF #END-YEAR GT #START-YEAR
                {
                    FOR03:                                                                                                                                                //Natural: FOR #H-CTR = 1 TO 7
                    for (pnd_H_Ctr.setValue(1); condition(pnd_H_Ctr.lessOrEqual(7)); pnd_H_Ctr.nadd(1))
                    {
                        if (condition(pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).greater(getZero()) && pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).lessOrEqual(pnd_End_Date))) //Natural: IF #HOLIDAY-DATE ( #H-CTR ) GT 0 AND #HOLIDAY-DATE ( #H-CTR ) LE #END-DATE
                        {
                            pnd_Holiday_Ctr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #HOLIDAY-CTR
                            pnd_End_Ctr.nadd(1);                                                                                                                          //Natural: ADD 1 TO #END-CTR
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                FOR04:                                                                                                                                                    //Natural: FOR #H-CTR = 1 TO 7
                for (pnd_H_Ctr.setValue(1); condition(pnd_H_Ctr.lessOrEqual(7)); pnd_H_Ctr.nadd(1))
                {
                    if (condition(pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).greater(getZero())))                                                                  //Natural: IF #HOLIDAY-DATE ( #H-CTR ) GT 0
                    {
                        pnd_Holiday_Ctr.nadd(1);                                                                                                                          //Natural: ADD 1 TO #HOLIDAY-CTR
                        pnd_Between_Ctr.nadd(1);                                                                                                                          //Natural: ADD 1 TO #BETWEEN-CTR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM COMPUTE-NO-OF-WEEKENDS
        sub_Compute_No_Of_Weekends();
        if (condition(Global.isEscape())) {return;}
        pnd_Holiday_And_Weekend.compute(new ComputeParameters(false, pnd_Holiday_And_Weekend), pnd_Holiday_Ctr.add(pnd_Nbr_Of_Weekends));                                 //Natural: COMPUTE #HOLIDAY-AND-WEEKEND = #HOLIDAY-CTR + #NBR-OF-WEEKENDS
        pnd_Business_Days.compute(new ComputeParameters(false, pnd_Business_Days), pnd_Calendar_Days.subtract(pnd_Holiday_And_Weekend));                                  //Natural: ASSIGN #BUSINESS-DAYS := #CALENDAR-DAYS - #HOLIDAY-AND-WEEKEND
        if (condition(pnd_Business_Days.less(getZero())))                                                                                                                 //Natural: IF #BUSINESS-DAYS LT 0
        {
            pnd_Business_Days.reset();                                                                                                                                    //Natural: RESET #BUSINESS-DAYS
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Date_Process_Routine() throws Exception                                                                                                              //Natural: DATE-PROCESS-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  RETRIEVE NAME OF DAY OF WEEK
        pnd_Sdte_Dayofweek.setValueEdited(pnd_Start_Date,new ReportEditMask("NNNNNNNNN"));                                                                                //Natural: MOVE EDITED #START-DATE ( EM = NNNNNNNNN ) TO #SDTE-DAYOFWEEK
        DbsUtil.examine(new ExamineSource(pnd_Sdte_Dayofweek), new ExamineTranslate(TranslateOption.Upper));                                                              //Natural: EXAMINE #SDTE-DAYOFWEEK TRANSLATE INTO UPPER
        //*   #SDTE-ENDING   (A1)   /* W=WEEKEND PROCESS (LAST BUS DAY OF WEEK)
        //*  DETERMINE IF A DATE IS A HOLIDAY
        pnd_Sdte_Holiday.reset();                                                                                                                                         //Natural: RESET #SDTE-HOLIDAY
        pnd_Year_Pnd_Year_N.setValue(pnd_Start_Date_A_Pnd_Start_Year);                                                                                                    //Natural: ASSIGN #YEAR-N := #START-YEAR
                                                                                                                                                                          //Natural: PERFORM NEW-YEARS-DAY
        sub_New_Years_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRESIDENTS-DAY
        sub_Presidents_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MEMORIAL-DAY
        sub_Memorial_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM INDEPENDENCE-DAY
        sub_Independence_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LABOR-DAY
        sub_Labor_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM THANKSGIVING-DAY
        sub_Thanksgiving_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CHRISTMAS-DAY
        sub_Christmas_Day();
        if (condition(Global.isEscape())) {return;}
        FOR05:                                                                                                                                                            //Natural: FOR #H-CTR = 1 TO 7
        for (pnd_H_Ctr.setValue(1); condition(pnd_H_Ctr.lessOrEqual(7)); pnd_H_Ctr.nadd(1))
        {
            if (condition(pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).greater(getZero()) && pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).equals(pnd_Start_Date))) //Natural: IF #HOLIDAY-DATE ( #H-CTR ) GT 0 AND #HOLIDAY-DATE ( #H-CTR ) = #START-DATE
            {
                pnd_Sdte_Holiday.setValue(pnd_Holidays_Pnd_Holiday.getValue(pnd_H_Ctr));                                                                                  //Natural: MOVE #HOLIDAY ( #H-CTR ) TO #SDTE-HOLIDAY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Bsnss_D.reset();                                                                                                                                              //Natural: RESET #BSNSS-D
        if (condition(pnd_Sdte_Holiday.greater(" ") || pnd_Sdte_Dayofweek.equals("SATURDAY") || pnd_Sdte_Dayofweek.equals("SUNDAY")))                                     //Natural: IF #SDTE-HOLIDAY GT ' ' OR #SDTE-DAYOFWEEK = 'SATURDAY' OR = 'SUNDAY'
        {
                                                                                                                                                                          //Natural: PERFORM BUSINESS-DATE
            sub_Business_Date();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WEEK-END
        sub_Week_End();
        if (condition(Global.isEscape())) {return;}
        pnd_Smonth_Pnd_Smonth_A.setValueEdited(pnd_Start_Date,new ReportEditMask("MM"));                                                                                  //Natural: MOVE EDITED #START-DATE ( EM = MM ) TO #SMONTH-A
                                                                                                                                                                          //Natural: PERFORM MONTH-END
        sub_Month_End();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM QTR-END
        sub_Qtr_End();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Business_Date() throws Exception                                                                                                                     //Natural: BUSINESS-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Business.reset();                                                                                                                                             //Natural: RESET #BUSINESS
        pnd_Business.setValue(pnd_Start_Date);                                                                                                                            //Natural: MOVE #START-DATE TO #BUSINESS
        pnd_Business.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #BUSINESS
        pnd_Nameofday.setValueEdited(pnd_Business,new ReportEditMask("NNNNNNNNN"));                                                                                       //Natural: MOVE EDITED #BUSINESS ( EM = NNNNNNNNN ) TO #NAMEOFDAY
        DbsUtil.examine(new ExamineSource(pnd_Week_Names.getValue("*"),true), new ExamineSearch(pnd_Nameofday), new ExamineGivingIndex(pnd_Day_No));                      //Natural: EXAMINE FULL #WEEK-NAMES ( * ) FOR #NAMEOFDAY GIVING INDEX #DAY-NO
        //*  WEEK DAY
        short decideConditionsMet409 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #DAY-NO;//Natural: VALUE 1,2,3,4,5
        if (condition((pnd_Day_No.equals(1) || pnd_Day_No.equals(2) || pnd_Day_No.equals(3) || pnd_Day_No.equals(4) || pnd_Day_No.equals(5))))
        {
            decideConditionsMet409++;
            ignore();
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((pnd_Day_No.equals(6))))
        {
            decideConditionsMet409++;
            pnd_Business.nadd(2);                                                                                                                                         //Natural: ADD 2 TO #BUSINESS
        }                                                                                                                                                                 //Natural: VALUE 7
        else if (condition((pnd_Day_No.equals(7))))
        {
            decideConditionsMet409++;
            pnd_Business.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #BUSINESS
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Done.setValue(false);                                                                                                                                         //Natural: MOVE FALSE TO #DONE #MATCH
        pnd_Match.setValue(false);
        R0:                                                                                                                                                               //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Done.getBoolean())) {break;}                                                                                                                //Natural: UNTIL #DONE
            FOR06:                                                                                                                                                        //Natural: FOR #H-CTR = 1 TO 7
            for (pnd_H_Ctr.setValue(1); condition(pnd_H_Ctr.lessOrEqual(7)); pnd_H_Ctr.nadd(1))
            {
                if (condition(pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).greater(getZero()) && pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).equals(pnd_Business))) //Natural: IF #HOLIDAY-DATE ( #H-CTR ) GT 0 AND #HOLIDAY-DATE ( #H-CTR ) = #BUSINESS
                {
                    pnd_Business.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #BUSINESS
                    pnd_Match.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #MATCH
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R0"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R0"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Match.getBoolean()))                                                                                                                        //Natural: IF #MATCH
            {
                pnd_Match.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #MATCH
                pnd_Done.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #DONE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Done.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #DONE
                if (true) break R0;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R0. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Bsnss_D.setValue(pnd_Business);                                                                                                                               //Natural: ASSIGN #BSNSS-D := #BUSINESS
    }
    private void sub_Week_End() throws Exception                                                                                                                          //Natural: WEEK-END
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Week_End_D.setValue(pnd_Start_Date);                                                                                                                          //Natural: MOVE #START-DATE TO #WEEK-END-D
        pnd_Nameofday.setValueEdited(pnd_Week_End_D,new ReportEditMask("NNNNNNNNN"));                                                                                     //Natural: MOVE EDITED #WEEK-END-D ( EM = NNNNNNNNN ) TO #NAMEOFDAY
        DbsUtil.examine(new ExamineSource(pnd_Week_Names.getValue("*"),true), new ExamineSearch(pnd_Nameofday), new ExamineGivingIndex(pnd_Day_No));                      //Natural: EXAMINE FULL #WEEK-NAMES ( * ) FOR #NAMEOFDAY GIVING INDEX #DAY-NO
        short decideConditionsMet443 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #DAY-NO;//Natural: VALUE 1
        if (condition((pnd_Day_No.equals(1))))
        {
            decideConditionsMet443++;
            pnd_Week_End_D.nadd(4);                                                                                                                                       //Natural: ADD 4 TO #WEEK-END-D
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Day_No.equals(2))))
        {
            decideConditionsMet443++;
            pnd_Week_End_D.nadd(3);                                                                                                                                       //Natural: ADD 3 TO #WEEK-END-D
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Day_No.equals(3))))
        {
            decideConditionsMet443++;
            pnd_Week_End_D.nadd(2);                                                                                                                                       //Natural: ADD 2 TO #WEEK-END-D
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Day_No.equals(4))))
        {
            decideConditionsMet443++;
            pnd_Week_End_D.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WEEK-END-D
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((pnd_Day_No.equals(5))))
        {
            decideConditionsMet443++;
            pnd_Week_End_D.nadd(0);                                                                                                                                       //Natural: ADD 0 TO #WEEK-END-D
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((pnd_Day_No.equals(6))))
        {
            decideConditionsMet443++;
            pnd_Week_End_D.nsubtract(1);                                                                                                                                  //Natural: SUBTRACT 1 FROM #WEEK-END-D
        }                                                                                                                                                                 //Natural: VALUE 7
        else if (condition((pnd_Day_No.equals(7))))
        {
            decideConditionsMet443++;
            pnd_Week_End_D.nsubtract(2);                                                                                                                                  //Natural: SUBTRACT 2 FROM #WEEK-END-D
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Done.setValue(false);                                                                                                                                         //Natural: MOVE FALSE TO #DONE #MATCH
        pnd_Match.setValue(false);
        R1:                                                                                                                                                               //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Done.getBoolean())) {break;}                                                                                                                //Natural: UNTIL #DONE
            FOR07:                                                                                                                                                        //Natural: FOR #H-CTR = 1 TO 7
            for (pnd_H_Ctr.setValue(1); condition(pnd_H_Ctr.lessOrEqual(7)); pnd_H_Ctr.nadd(1))
            {
                if (condition(pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).greater(getZero()) && pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).equals(pnd_Week_End_D))) //Natural: IF #HOLIDAY-DATE ( #H-CTR ) GT 0 AND #HOLIDAY-DATE ( #H-CTR ) = #WEEK-END-D
                {
                    pnd_Week_End_D.nsubtract(1);                                                                                                                          //Natural: SUBTRACT 1 FROM #WEEK-END-D
                    pnd_Match.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #MATCH
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Match.getBoolean()))                                                                                                                        //Natural: IF #MATCH
            {
                pnd_Match.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #MATCH
                pnd_Done.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #DONE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Done.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #DONE
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Month_End() throws Exception                                                                                                                         //Natural: MONTH-END
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Month_End.setValueEdited(pnd_Start_Date,new ReportEditMask("YYYYMMDD"));                                                                                      //Natural: MOVE EDITED #START-DATE ( EM = YYYYMMDD ) TO #MONTH-END
        pnd_Month_End_Pnd_Month_End_Dd.setValue(pnd_Month_Ends.getValue(pnd_Smonth));                                                                                     //Natural: MOVE #MONTH-ENDS ( #SMONTH ) TO #MONTH-END-DD
        pnd_Month_End_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Month_End);                                                                                     //Natural: MOVE EDITED #MONTH-END TO #MONTH-END-D ( EM = YYYYMMDD )
        //*  DETERMINE IF MONTH END IS ON A WEEKEND
        pnd_Nameofday.setValueEdited(pnd_Month_End_D,new ReportEditMask("NNNNNNNNN"));                                                                                    //Natural: MOVE EDITED #MONTH-END-D ( EM = NNNNNNNNN ) TO #NAMEOFDAY
        short decideConditionsMet489 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #NAMEOFDAY;//Natural: VALUE 'Saturday'
        if (condition((pnd_Nameofday.equals("Saturday"))))
        {
            decideConditionsMet489++;
            pnd_Month_End_D.nsubtract(1);                                                                                                                                 //Natural: SUBTRACT 1 FROM #MONTH-END-D
        }                                                                                                                                                                 //Natural: VALUE 'Sunday'
        else if (condition((pnd_Nameofday.equals("Sunday"))))
        {
            decideConditionsMet489++;
            pnd_Month_End_D.nsubtract(2);                                                                                                                                 //Natural: SUBTRACT 2 FROM #MONTH-END-D
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  DETERMINE IF MONTH END (ADJUSTED) IS ON A HOLIDAY
        pnd_Done.setValue(false);                                                                                                                                         //Natural: MOVE FALSE TO #DONE #MATCH
        pnd_Match.setValue(false);
        R2:                                                                                                                                                               //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Done.getBoolean())) {break;}                                                                                                                //Natural: UNTIL #DONE
            FOR08:                                                                                                                                                        //Natural: FOR #H-CTR = 1 TO 7
            for (pnd_H_Ctr.setValue(1); condition(pnd_H_Ctr.lessOrEqual(7)); pnd_H_Ctr.nadd(1))
            {
                if (condition(pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).greater(getZero()) && pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).equals(pnd_Month_End_D))) //Natural: IF #HOLIDAY-DATE ( #H-CTR ) GT 0 AND #HOLIDAY-DATE ( #H-CTR ) = #MONTH-END-D
                {
                    pnd_Month_End_D.nsubtract(1);                                                                                                                         //Natural: SUBTRACT 1 FROM #MONTH-END-D
                    pnd_Match.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #MATCH
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Match.getBoolean()))                                                                                                                        //Natural: IF #MATCH
            {
                pnd_Match.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #MATCH
                pnd_Done.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #DONE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R2. )
                pnd_Done.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #DONE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Qtr_End() throws Exception                                                                                                                           //Natural: QTR-END
    {
        if (BLNatReinput.isReinput()) return;

        //*  QUARTER ENDS 3/31, 6/30, 9/30, 12/31 OR PREVIOUS BUSINESS
        //*  DAY IF DATE FALLS ON A WEEKEND OR HOLIDAY
        pnd_Qtr_End.setValueEdited(pnd_Start_Date,new ReportEditMask("YYYYMMDD"));                                                                                        //Natural: MOVE EDITED #START-DATE ( EM = YYYYMMDD ) TO #QTR-END
        short decideConditionsMet525 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #QTR-END-MM;//Natural: VALUES '01', '02', '03'
        if (condition((pnd_Qtr_End_Pnd_Qtr_End_Mm.equals("01") || pnd_Qtr_End_Pnd_Qtr_End_Mm.equals("02") || pnd_Qtr_End_Pnd_Qtr_End_Mm.equals("03"))))
        {
            decideConditionsMet525++;
            pnd_Qtr_End_Pnd_Qtr_End_Mm.setValue("03");                                                                                                                    //Natural: MOVE '03' TO #QTR-END-MM
            pnd_Qtr_End_Pnd_Qtr_End_Dd.setValue(pnd_Month_Ends.getValue(3));                                                                                              //Natural: MOVE #MONTH-ENDS ( 03 ) TO #QTR-END-DD
        }                                                                                                                                                                 //Natural: VALUES '04', '05', '06'
        else if (condition((pnd_Qtr_End_Pnd_Qtr_End_Mm.equals("04") || pnd_Qtr_End_Pnd_Qtr_End_Mm.equals("05") || pnd_Qtr_End_Pnd_Qtr_End_Mm.equals("06"))))
        {
            decideConditionsMet525++;
            pnd_Qtr_End_Pnd_Qtr_End_Mm.setValue("06");                                                                                                                    //Natural: MOVE '06' TO #QTR-END-MM
            pnd_Qtr_End_Pnd_Qtr_End_Dd.setValue(pnd_Month_Ends.getValue(6));                                                                                              //Natural: MOVE #MONTH-ENDS ( 06 ) TO #QTR-END-DD
        }                                                                                                                                                                 //Natural: VALUES '07', '08', '09'
        else if (condition((pnd_Qtr_End_Pnd_Qtr_End_Mm.equals("07") || pnd_Qtr_End_Pnd_Qtr_End_Mm.equals("08") || pnd_Qtr_End_Pnd_Qtr_End_Mm.equals("09"))))
        {
            decideConditionsMet525++;
            pnd_Qtr_End_Pnd_Qtr_End_Mm.setValue("09");                                                                                                                    //Natural: MOVE '09' TO #QTR-END-MM
            pnd_Qtr_End_Pnd_Qtr_End_Dd.setValue(pnd_Month_Ends.getValue(9));                                                                                              //Natural: MOVE #MONTH-ENDS ( 09 ) TO #QTR-END-DD
        }                                                                                                                                                                 //Natural: VALUES '10', '11', '12'
        else if (condition((pnd_Qtr_End_Pnd_Qtr_End_Mm.equals("10") || pnd_Qtr_End_Pnd_Qtr_End_Mm.equals("11") || pnd_Qtr_End_Pnd_Qtr_End_Mm.equals("12"))))
        {
            decideConditionsMet525++;
            pnd_Qtr_End_Pnd_Qtr_End_Mm.setValue("12");                                                                                                                    //Natural: MOVE '12' TO #QTR-END-MM
            pnd_Qtr_End_Pnd_Qtr_End_Dd.setValue(pnd_Month_Ends.getValue(12));                                                                                             //Natural: MOVE #MONTH-ENDS ( 12 ) TO #QTR-END-DD
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet525 > 0))
        {
            pnd_Qtr_End_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Qtr_End);                                                                                     //Natural: MOVE EDITED #QTR-END TO #QTR-END-D ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Done.setValue(false);                                                                                                                                         //Natural: MOVE FALSE TO #DONE #MATCH
        pnd_Match.setValue(false);
        R3:                                                                                                                                                               //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Done.getBoolean())) {break;}                                                                                                                //Natural: UNTIL #DONE
            FOR09:                                                                                                                                                        //Natural: FOR #H-CTR = 1 TO 7
            for (pnd_H_Ctr.setValue(1); condition(pnd_H_Ctr.lessOrEqual(7)); pnd_H_Ctr.nadd(1))
            {
                if (condition(pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).greater(getZero()) && pnd_Holidays_Pnd_Holiday_Date.getValue(pnd_H_Ctr).equals(pnd_Qtr_End_D))) //Natural: IF #HOLIDAY-DATE ( #H-CTR ) GT 0 AND #HOLIDAY-DATE ( #H-CTR ) = #QTR-END-D
                {
                    pnd_Qtr_End_D.nsubtract(1);                                                                                                                           //Natural: SUBTRACT 1 FROM #QTR-END-D
                    pnd_Match.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #MATCH
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R3"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Match.getBoolean()))                                                                                                                        //Natural: IF #MATCH
            {
                pnd_Match.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #MATCH
                pnd_Done.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #DONE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Done.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #DONE
                if (true) break R3;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R3. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_New_Years_Day() throws Exception                                                                                                                     //Natural: NEW-YEARS-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  NEW YEAR 01/01/YYYY
        //*  HOLIDAY IS ON THE DAY IT FALLS ON REGARDLESS OF WHETHER IT IS A
        //*  WEEKDAY OR A WEEKEND
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0101"));                                                                           //Natural: COMPRESS #YEAR '0101' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        short decideConditionsMet572 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #NAME-OF-DAY;//Natural: VALUE 'Sun'
        if (condition((pnd_Name_Of_Day.equals("Sun"))))
        {
            decideConditionsMet572++;
            pnd_Date.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(! (pnd_Name_Of_Day.equals("Sun"))))                                                                                                                 //Natural: IF NOT ( #NAME-OF-DAY = 'Sun' )
        {
            pnd_Holidays_Pnd_Holiday_Date.getValue(1).setValue(pnd_Date);                                                                                                 //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 1 )
            pnd_Holidays_Pnd_Holiday.getValue(1).setValue("New Year");                                                                                                    //Natural: MOVE 'New Year' TO #HOLIDAY ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Presidents_Day() throws Exception                                                                                                                    //Natural: PRESIDENTS-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  PRESIDENT's Day 3rd Monday in February
        pnd_Day_Ctr.setValue(0);                                                                                                                                          //Natural: MOVE 0 TO #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0201"));                                                                           //Natural: COMPRESS #YEAR '0201' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Mon")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Mon'
            {
                pnd_Day_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DAY-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.less(3)))                                                                                                                           //Natural: IF #DAY-CTR LT 3
            {
                pnd_Date.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(3))) {break;}                                                                                                                //Natural: UNTIL #DAY-CTR = 3
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        pnd_Holidays_Pnd_Holiday_Date.getValue(2).setValue(pnd_Date);                                                                                                     //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 2 )
        pnd_Holidays_Pnd_Holiday.getValue(2).setValue("President's Day");                                                                                                 //Natural: MOVE 'President"s Day' TO #HOLIDAY ( 2 )
    }
    private void sub_Memorial_Day() throws Exception                                                                                                                      //Natural: MEMORIAL-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  MEMORIAL DAY - LAST MONDAY IN MAY
        pnd_Day_Ctr.setValue(0);                                                                                                                                          //Natural: MOVE 0 TO #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0531"));                                                                           //Natural: COMPRESS #YEAR '0531' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Mon")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Mon'
            {
                pnd_Day_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DAY-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(getZero())))                                                                                                                 //Natural: IF #DAY-CTR = 0
            {
                pnd_Date.nsubtract(1);                                                                                                                                    //Natural: SUBTRACT 1 FROM #DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(1))) {break;}                                                                                                                //Natural: UNTIL #DAY-CTR = 1
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        pnd_Holidays_Pnd_Holiday_Date.getValue(3).setValue(pnd_Date);                                                                                                     //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 3 )
        pnd_Holidays_Pnd_Holiday.getValue(3).setValue("Memorial Day");                                                                                                    //Natural: MOVE 'Memorial Day' TO #HOLIDAY ( 3 )
    }
    private void sub_Independence_Day() throws Exception                                                                                                                  //Natural: INDEPENDENCE-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  INDEPENDENCE DAY 07/04/YYYY
        //*  IF IT FALLS ON A SUNDAY, THE NEXT WORKING DAY IS A HOLIDAY
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0704"));                                                                           //Natural: COMPRESS #YEAR '0704' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        short decideConditionsMet637 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #NAME-OF-DAY;//Natural: VALUE 'Sat'
        if (condition((pnd_Name_Of_Day.equals("Sat"))))
        {
            decideConditionsMet637++;
            pnd_Date.nsubtract(1);                                                                                                                                        //Natural: SUBTRACT 1 FROM #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: VALUE 'Sun'
        else if (condition((pnd_Name_Of_Day.equals("Sun"))))
        {
            decideConditionsMet637++;
            pnd_Date.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(! (pnd_Name_Of_Day.equals("Sat") || pnd_Name_Of_Day.equals("Sun"))))                                                                                //Natural: IF NOT ( #NAME-OF-DAY = 'Sat' OR = 'Sun' )
        {
            pnd_Holidays_Pnd_Holiday_Date.getValue(4).setValue(pnd_Date);                                                                                                 //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 4 )
            pnd_Holidays_Pnd_Holiday.getValue(4).setValue("Fourth of July");                                                                                              //Natural: MOVE 'Fourth of July' TO #HOLIDAY ( 4 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Labor_Day() throws Exception                                                                                                                         //Natural: LABOR-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  LABOR DAY       1ST MONDAY IN SEPTEMBER
        pnd_Day_Ctr.setValue(0);                                                                                                                                          //Natural: MOVE 0 TO #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0901"));                                                                           //Natural: COMPRESS #YEAR '0901' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT03:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Mon")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Mon'
            {
                pnd_Day_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DAY-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(getZero())))                                                                                                                 //Natural: IF #DAY-CTR = 0
            {
                pnd_Date.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(1))) {break;}                                                                                                                //Natural: UNTIL #DAY-CTR = 1
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        pnd_Holidays_Pnd_Holiday_Date.getValue(5).setValue(pnd_Date);                                                                                                     //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 5 )
        pnd_Holidays_Pnd_Holiday.getValue(5).setValue("Labor Day");                                                                                                       //Natural: MOVE 'Labor Day' TO #HOLIDAY ( 5 )
    }
    private void sub_Thanksgiving_Day() throws Exception                                                                                                                  //Natural: THANKSGIVING-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  THANKSGIVING    4TH THURSDAY IN NOVEMBER
        pnd_Day_Ctr.setValue(0);                                                                                                                                          //Natural: MOVE 0 TO #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "1101"));                                                                           //Natural: COMPRESS #YEAR '1101' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT04:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Thu")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Thu'
            {
                pnd_Day_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DAY-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.less(4)))                                                                                                                           //Natural: IF #DAY-CTR LT 4
            {
                pnd_Date.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(4))) {break;}                                                                                                                //Natural: UNTIL #DAY-CTR = 4
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        pnd_Holidays_Pnd_Holiday_Date.getValue(6).setValue(pnd_Date);                                                                                                     //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 6 )
        pnd_Holidays_Pnd_Holiday.getValue(6).setValue("Thanksgiving");                                                                                                    //Natural: MOVE 'Thanksgiving' TO #HOLIDAY ( 6 )
    }
    private void sub_Christmas_Day() throws Exception                                                                                                                     //Natural: CHRISTMAS-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  CHRISTMAS DAY    12/25/YYYY
        //*  IF IT FALLS ON A SATURDAY,  THE PREVIOUS WORKING DAY IS A HOLIDAY
        //*  IF IT FALLS ON A SUNDAY, THE NEXT WORKING DAY IS A HOLIDAY
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "1225"));                                                                           //Natural: COMPRESS #YEAR '1225' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        short decideConditionsMet706 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #NAME-OF-DAY;//Natural: VALUE 'Sat'
        if (condition((pnd_Name_Of_Day.equals("Sat"))))
        {
            decideConditionsMet706++;
            pnd_Date.nsubtract(1);                                                                                                                                        //Natural: SUBTRACT 1 FROM #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: VALUE 'Sun'
        else if (condition((pnd_Name_Of_Day.equals("Sun"))))
        {
            decideConditionsMet706++;
            pnd_Date.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(! (pnd_Name_Of_Day.equals("Sat") || pnd_Name_Of_Day.equals("Sun"))))                                                                                //Natural: IF NOT ( #NAME-OF-DAY = 'Sat' OR = 'Sun' )
        {
            pnd_Holidays_Pnd_Holiday_Date.getValue(7).setValue(pnd_Date);                                                                                                 //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 7 )
            pnd_Holidays_Pnd_Holiday.getValue(7).setValue("Christmas Day");                                                                                               //Natural: MOVE 'Christmas Day' TO #HOLIDAY ( 7 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Compute_No_Of_Weekends() throws Exception                                                                                                            //Natural: COMPUTE-NO-OF-WEEKENDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Date.setValue(pnd_Start_Date);                                                                                                                           //Natural: MOVE #START-DATE TO #WORK-DATE
        pnd_Nbr_Of_Weekends.setValue(0);                                                                                                                                  //Natural: MOVE 0 TO #NBR-OF-WEEKENDS
        REPEAT05:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Work_Date,new ReportEditMask("NNN"));                                                                                      //Natural: MOVE EDITED #WORK-DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Sat") || pnd_Name_Of_Day.equals("Sun")))                                                                                //Natural: IF #NAME-OF-DAY = 'Sat' OR = 'Sun'
            {
                pnd_Nbr_Of_Weekends.nadd(1);                                                                                                                              //Natural: ADD 1 TO #NBR-OF-WEEKENDS
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Date.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WORK-DATE
            if (condition(pnd_Work_Date.greater(pnd_End_Date))) {break;}                                                                                                  //Natural: UNTIL #WORK-DATE GT #END-DATE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }

    //
}
