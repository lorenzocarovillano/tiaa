/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:25:19 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn3022
************************************************************
**        * FILE NAME            : Cwfn3022.java
**        * CLASS NAME           : Cwfn3022
**        * INSTANCE NAME        : Cwfn3022
************************************************************
**********************************************************************
* PROGRAM  : CWFB3022
* SYSTEM   : CRPCWF
* TITLE    : COMPARISON WHO-WORK-REQUEST VS. WHO-ACTIVITY-FILE
* FUNCTION : THIS PROGRAM COMPARES CLOSE WORK REQUEST WITH ACTIVITY.
*          : FINDS CLOSE ACTIVITY AND UPDATES WR WITH THE SAME STATUS.
*          : IT ALSO UPDATES CLOSE ACTIVITY RECORD's Download-Dte-Tme.
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017
**********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn3022 extends BLNatBase
{
    // Data Areas
    private PdaMiha0130 pdaMiha0130;
    private PdaMiha0180 pdaMiha0180;
    private PdaMiha0200 pdaMiha0200;
    private PdaMiha0030 pdaMiha0030;
    private PdaMiha0100 pdaMiha0100;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Parm_Log_Dte_Tme;
    private DbsField pnd_Date_T;

    private DataAccessProgramView vw_cwf_Master_Histo_1;
    private DbsField cwf_Master_Histo_1_Actv_Unque_Key;

    private DbsGroup cwf_Master_Histo_1__R_Field_1;
    private DbsField cwf_Master_Histo_1_Pnd_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Histo_1__R_Field_2;
    private DbsField cwf_Master_Histo_1_Pnd_Rqst_Log_Dte;
    private DbsField cwf_Master_Histo_1_Pnd_Rqst_Log_Tme;
    private DbsField cwf_Master_Histo_1_Pnd_Act_Ind;
    private DbsField pnd_Begin_Key;

    private DbsGroup pnd_Begin_Key__R_Field_3;
    private DbsField pnd_Begin_Key_Pnd_B_Crprte_Status_Ind;
    private DbsField pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte_Tme;

    private DbsGroup pnd_Begin_Key__R_Field_4;
    private DbsField pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte;
    private DbsField pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Tme;
    private DbsField pnd_Begin_Key_Pnd_B_Admin_Unit_Cde;
    private DbsField pnd_Begin_Key_Pnd_B_Actve_Ind;
    private DbsField pnd_End_Key;

    private DbsGroup pnd_End_Key__R_Field_5;
    private DbsField pnd_End_Key_Pnd_E_Crprte_Status_Ind;
    private DbsField pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte_Tme;

    private DbsGroup pnd_End_Key__R_Field_6;
    private DbsField pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte;
    private DbsField pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Tme;
    private DbsField pnd_End_Key_Pnd_E_Admin_Unit_Cde;
    private DbsField pnd_End_Key_Pnd_E_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_7;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2;
    private DbsField pnd_Tbl_Split;

    private DbsGroup pnd_Tbl_Split__R_Field_8;
    private DbsField pnd_Tbl_Split_Pnd_Tbl_Start_Dt;
    private DbsField pnd_Tbl_Split_Pnd_Tbl_Stop_Dt;
    private DbsField pnd_Conversion_Alpha;

    private DbsGroup pnd_Conversion_Alpha__R_Field_9;
    private DbsField pnd_Conversion_Alpha_Pnd_Conversion_Numeric;
    private DbsField pnd_Start_Dte_Tme;
    private DbsField pnd_End_Dte_Tme;

    private DbsGroup pnd_End_Dte_Tme__R_Field_10;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Yyyymmdd;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Hhiisst;

    private DbsGroup pnd_End_Dte_Tme__R_Field_11;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N;
    private DbsField pnd_Curr_Dte_Tme;
    private DbsField pnd_Prev_Dte_Tme;

    private DbsGroup pnd_Prev_Dte_Tme__R_Field_12;
    private DbsField pnd_Prev_Dte_Tme_Pnd_Prev_Yyyymmdd;
    private DbsField pnd_Prev_Dte_Tme_Pnd_Prev_Hhiisst;

    private DataAccessProgramView vw_cwf_Who_Work_Request;
    private DbsField cwf_Who_Work_Request_Rqst_Log_Dte_Tme;
    private DbsField cwf_Who_Work_Request_Rqst_Log_Invrt_Dte_Tme;
    private DbsField cwf_Who_Work_Request_Pin_Nbr;
    private DbsField cwf_Who_Work_Request_Rqst_Id;
    private DbsField cwf_Who_Work_Request_Tiaa_Rcvd_Dte;
    private DbsField cwf_Who_Work_Request_Work_Prcss_Id;
    private DbsField cwf_Who_Work_Request_Rlte_Rqst_Id;
    private DbsField cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme;

    private DbsGroup cwf_Who_Work_Request__R_Field_13;
    private DbsField cwf_Who_Work_Request_Pnd_Yyyymmdd;
    private DbsField cwf_Who_Work_Request_Pnd_Hhiisst;
    private DbsField cwf_Who_Work_Request_Final_Close_Out_Dte_Tme;
    private DbsField cwf_Who_Work_Request_Effctve_Dte;
    private DbsField cwf_Who_Work_Request_Status_Cde;
    private DbsField cwf_Who_Work_Request_Status_Freeze_Ind;
    private DbsField cwf_Who_Work_Request_Crprte_On_Tme_Ind;
    private DbsField cwf_Who_Work_Request_Trans_Dte;
    private DbsField cwf_Who_Work_Request_Crprte_Status_Ind;
    private DbsField cwf_Who_Work_Request_Sec_Turnaround_Tme;
    private DbsField cwf_Who_Work_Request_Sec_Updte_Dte;
    private DbsField cwf_Who_Work_Request_Rqst_Indicators;
    private DbsField cwf_Who_Work_Request_Unit_Cde;
    private DbsField cwf_Who_Work_Request_Dwnld_Updte_Dte_Tme;

    private DataAccessProgramView vw_cwf_Who_Activity_File;
    private DbsField cwf_Who_Activity_File_Rqst_Log_Dte_Tme;
    private DbsField cwf_Who_Activity_File_Strtng_Event_Dte_Tme;
    private DbsField cwf_Who_Activity_File_Endng_Event_Dte_Tme;
    private DbsField cwf_Who_Activity_File_Cmpltd_Actvty_Ind;
    private DbsField cwf_Who_Activity_File_Admin_Unit_Cde;
    private DbsField cwf_Who_Activity_File_Empl_Oprtr_Cde;
    private DbsField cwf_Who_Activity_File_Step_Id;
    private DbsField cwf_Who_Activity_File_Admin_Status_Cde;
    private DbsField cwf_Who_Activity_File_Unit_On_Tme_Ind;
    private DbsField cwf_Who_Activity_File_Crtn_Run_Id;
    private DbsField cwf_Who_Activity_File_Actvty_Elpsd_Clndr_Hours;
    private DbsField cwf_Who_Activity_File_Actvty_Elpsd_Bsnss_Hours;
    private DbsField cwf_Who_Activity_File_Invrt_Strtng_Event;
    private DbsField cwf_Who_Activity_File_Dwnld_Updte_Dte_Tme;
    private DbsField cwf_Who_Activity_File_Rt_Sqnce_Nbr;
    private DbsField cwf_Who_Activity_File_Off_Rtng_Ind;
    private DbsField cwf_Who_Activity_File_Msg_Rtrn_Cde;
    private DbsField cwf_Who_Activity_File_Msg_Nbr;

    private DataAccessProgramView vw_cwf_Master;
    private DbsField cwf_Master_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master__R_Field_14;
    private DbsField cwf_Master_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Rqst_Log_Index_Tmte;
    private DbsField cwf_Master_Pin_Nbr;
    private DbsField cwf_Master_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Work_Prcss_Id;
    private DbsField cwf_Master_Unit_Cde;
    private DbsField cwf_Master_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Empl_Oprtr_Cde;
    private DbsField cwf_Master_Last_Chnge_Dte_Tme;

    private DbsGroup cwf_Master__R_Field_15;
    private DbsField cwf_Master_Pnd_Date_C;

    private DbsGroup cwf_Master__R_Field_16;
    private DbsField cwf_Master_Pnd_Date_Ca;
    private DbsField cwf_Master_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Last_Chnge_Invrt_Dte_Tme;
    private DbsField cwf_Master_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Step_Id;
    private DbsField cwf_Master_Rt_Sqnce_Nbr;
    private DbsField cwf_Master_Admin_Unit_Cde;

    private DbsGroup cwf_Master__R_Field_17;
    private DbsField cwf_Master_Unit_Id_Cde;
    private DbsField cwf_Master_Admin_Status_Cde;
    private DbsField cwf_Master_Status_Cde;
    private DbsField cwf_Master_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Last_Updte_Dte;
    private DbsField cwf_Master_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Actve_Ind;

    private DbsGroup cwf_Master__R_Field_18;
    private DbsField cwf_Master_Actve_Ind_1;
    private DbsField cwf_Master_Actve_Ind_2;
    private DbsField cwf_Master_Crprte_Status_Ind;
    private DbsField cwf_Master_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Effctve_Dte;
    private DbsField cwf_Master_Trnsctn_Dte;
    private DbsField cwf_Master_Trans_Dte;
    private DbsField cwf_Master_Physcl_Fldr_Id_Nbr;
    private DbsField cwf_Master_Mj_Chrge_Dte_Tme;
    private DbsField cwf_Master_Mj_Pull_Ind;
    private DbsField cwf_Master_Status_Freeze_Ind;
    private DbsField cwf_Master_Work_List_Ind;
    private DbsField cwf_Master_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Empl_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Empl_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Step_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Step_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Crprte_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Final_Close_Out_Dte_Tme;
    private DbsField cwf_Master_Rqst_Id;

    private DbsGroup cwf_Master__R_Field_19;
    private DbsField cwf_Master_Rqst_Work_Prcss_Id;
    private DbsField cwf_Master_Rqst_Tiaa_Dte_Tme;
    private DbsField cwf_Master_Rlte_Rqst_Id;
    private DbsField pnd_Isn;
    private DbsField pnd_Found_Sw;
    private DbsField pnd_Disc_Msg;

    private DbsGroup pnd_Disc_Msg__R_Field_20;
    private DbsField pnd_Disc_Msg_Pnd_Disc_Msg_1;
    private DbsField pnd_Disc_Msg_Pnd_Disc_Msg_2;

    private DbsGroup pnd_Disc_Msg__R_Field_21;
    private DbsField pnd_Disc_Msg_Pnd_Disc_Msg_2_1;
    private DbsField pnd_Disc_Msg_Pnd_Disc_Msg_2_2;
    private DbsField pnd_Stop_Sweep_Time_A;

    private DbsGroup pnd_Stop_Sweep_Time_A__R_Field_22;
    private DbsField pnd_Stop_Sweep_Time_A_Pnd_Stop_Yyyymmdd;
    private DbsField pnd_Stop_Sweep_Time_A_Pnd_Stop_Hhiisst;

    private DbsGroup pnd_Stop_Sweep_Time_A__R_Field_23;
    private DbsField pnd_Stop_Sweep_Time_A_Pnd_Stop_Sweep_Time_N;
    private DbsField pnd_Stop_Sweep_Invrt_Time;
    private DbsField pnd_Save_Status;
    private DbsField pnd_Rqst_Log_Dte_Tme_Save;
    private DbsField pnd_Pin_Nbr_Save;
    private DbsField pnd_Wpid_Save;
    private DbsField pnd_Tiaa_Rcvd_Dte_Save;
    private DbsField pnd_Rqst_Id_Save;
    private DbsField pnd_Rlte_Rqst_Id_Save;
    private DbsField pnd_Last_Chnge_Dte_Tme_Save;
    private DbsField pnd_Crprte_Stat_Ind_Save;
    private DbsField pnd_Final_Close_Out_Save;
    private DbsField pnd_Crprte_Clock_Dte_Save;
    private DbsField pnd_Discrep_Sw;
    private DbsField pnd_Crprte_Status_Log_Dte_Key;

    private DbsGroup pnd_Crprte_Status_Log_Dte_Key__R_Field_24;
    private DbsField pnd_Crprte_Status_Log_Dte_Key_Pnd_Crprte_Status_Ind;
    private DbsField pnd_Crprte_Status_Log_Dte_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Log_Dte_Tme_Invrt_St_Event_Key;

    private DbsGroup pnd_Log_Dte_Tme_Invrt_St_Event_Key__R_Field_25;
    private DbsField pnd_Log_Dte_Tme_Invrt_St_Event_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Log_Dte_Tme_Invrt_St_Event_Key_Pnd_Invrt_Strtng_Event;
    private DbsField pnd_Dwnld_Updte_Log_Dte_Key;

    private DbsGroup pnd_Dwnld_Updte_Log_Dte_Key__R_Field_26;
    private DbsField pnd_Dwnld_Updte_Log_Dte_Key_Pnd_Dwnld_Updte_Dte_Tme;
    private DbsField pnd_Dwnld_Updte_Log_Dte_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Wr_Isn;
    private DbsField pnd_Ac_Isn;
    private DbsField pnd_All_Wr_Cntr;
    private DbsField pnd_Wr_Ncs_Cntr;
    private DbsField pnd_Wr_Updated;
    private DbsField pnd_Ac_Updated;
    private DbsField pnd_Restart_Cntr;
    private DbsField pnd_I;
    private DbsField pnd_Crc_Cntr;
    private DbsField pnd_Opn_Cntr;
    private DbsField pnd_Nfd_Counter;
    private DbsField pnd_Act_Frm_Wr_Cntr;
    private DbsField pnd_Act_Frm_Mit_Cntr;
    private DbsField pnd_Rldt_Value_Tbl;

    private DbsGroup pnd_Rldt_Value_Tbl__R_Field_27;
    private DbsField pnd_Rldt_Value_Tbl_Pnd_Rldt_Value;
    private DbsField pnd_Rldt_Start;
    private DbsField pnd_Calc_Field_A;

    private DbsGroup pnd_Calc_Field_A__R_Field_28;
    private DbsField pnd_Calc_Field_A_Pnd_Calc_Field_N;

    private DbsRecord setTimeRecord;
    private DbsField st;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMiha0130 = new PdaMiha0130(localVariables);
        pdaMiha0180 = new PdaMiha0180(localVariables);
        pdaMiha0200 = new PdaMiha0200(localVariables);
        pdaMiha0030 = new PdaMiha0030(localVariables);
        pdaMiha0100 = new PdaMiha0100(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Parm_Log_Dte_Tme = parameters.newFieldInRecord("pnd_Parm_Log_Dte_Tme", "#PARM-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Parm_Log_Dte_Tme.setParameterOption(ParameterOption.ByReference);
        pnd_Date_T = parameters.newFieldInRecord("pnd_Date_T", "#DATE-T", FieldType.TIME);
        pnd_Date_T.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_cwf_Master_Histo_1 = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Histo_1", "CWF-MASTER-HISTO-1"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Histo_1_Actv_Unque_Key = vw_cwf_Master_Histo_1.getRecord().newFieldInGroup("cwf_Master_Histo_1_Actv_Unque_Key", "ACTV-UNQUE-KEY", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "ACTV_UNQUE_KEY");
        cwf_Master_Histo_1_Actv_Unque_Key.setDdmHeader("UNIQUE/KEY");
        cwf_Master_Histo_1_Actv_Unque_Key.setSuperDescriptor(true);

        cwf_Master_Histo_1__R_Field_1 = vw_cwf_Master_Histo_1.getRecord().newGroupInGroup("cwf_Master_Histo_1__R_Field_1", "REDEFINE", cwf_Master_Histo_1_Actv_Unque_Key);
        cwf_Master_Histo_1_Pnd_Rqst_Log_Dte_Tme = cwf_Master_Histo_1__R_Field_1.newFieldInGroup("cwf_Master_Histo_1_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);

        cwf_Master_Histo_1__R_Field_2 = cwf_Master_Histo_1__R_Field_1.newGroupInGroup("cwf_Master_Histo_1__R_Field_2", "REDEFINE", cwf_Master_Histo_1_Pnd_Rqst_Log_Dte_Tme);
        cwf_Master_Histo_1_Pnd_Rqst_Log_Dte = cwf_Master_Histo_1__R_Field_2.newFieldInGroup("cwf_Master_Histo_1_Pnd_Rqst_Log_Dte", "#RQST-LOG-DTE", FieldType.STRING, 
            8);
        cwf_Master_Histo_1_Pnd_Rqst_Log_Tme = cwf_Master_Histo_1__R_Field_2.newFieldInGroup("cwf_Master_Histo_1_Pnd_Rqst_Log_Tme", "#RQST-LOG-TME", FieldType.STRING, 
            7);
        cwf_Master_Histo_1_Pnd_Act_Ind = cwf_Master_Histo_1__R_Field_1.newFieldInGroup("cwf_Master_Histo_1_Pnd_Act_Ind", "#ACT-IND", FieldType.STRING, 
            1);
        registerRecord(vw_cwf_Master_Histo_1);

        pnd_Begin_Key = localVariables.newFieldInRecord("pnd_Begin_Key", "#BEGIN-KEY", FieldType.STRING, 25);

        pnd_Begin_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Begin_Key__R_Field_3", "REDEFINE", pnd_Begin_Key);
        pnd_Begin_Key_Pnd_B_Crprte_Status_Ind = pnd_Begin_Key__R_Field_3.newFieldInGroup("pnd_Begin_Key_Pnd_B_Crprte_Status_Ind", "#B-CRPRTE-STATUS-IND", 
            FieldType.STRING, 1);
        pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte_Tme = pnd_Begin_Key__R_Field_3.newFieldInGroup("pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte_Tme", "#B-LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15);

        pnd_Begin_Key__R_Field_4 = pnd_Begin_Key__R_Field_3.newGroupInGroup("pnd_Begin_Key__R_Field_4", "REDEFINE", pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte_Tme);
        pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte = pnd_Begin_Key__R_Field_4.newFieldInGroup("pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte", "#B-LAST-CHNGE-INVRT-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Tme = pnd_Begin_Key__R_Field_4.newFieldInGroup("pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Tme", "#B-LAST-CHNGE-INVRT-TME", 
            FieldType.NUMERIC, 7);
        pnd_Begin_Key_Pnd_B_Admin_Unit_Cde = pnd_Begin_Key__R_Field_3.newFieldInGroup("pnd_Begin_Key_Pnd_B_Admin_Unit_Cde", "#B-ADMIN-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Begin_Key_Pnd_B_Actve_Ind = pnd_Begin_Key__R_Field_3.newFieldInGroup("pnd_Begin_Key_Pnd_B_Actve_Ind", "#B-ACTVE-IND", FieldType.STRING, 1);
        pnd_End_Key = localVariables.newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.STRING, 25);

        pnd_End_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_End_Key__R_Field_5", "REDEFINE", pnd_End_Key);
        pnd_End_Key_Pnd_E_Crprte_Status_Ind = pnd_End_Key__R_Field_5.newFieldInGroup("pnd_End_Key_Pnd_E_Crprte_Status_Ind", "#E-CRPRTE-STATUS-IND", FieldType.STRING, 
            1);
        pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte_Tme = pnd_End_Key__R_Field_5.newFieldInGroup("pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte_Tme", "#E-LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15);

        pnd_End_Key__R_Field_6 = pnd_End_Key__R_Field_5.newGroupInGroup("pnd_End_Key__R_Field_6", "REDEFINE", pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte_Tme);
        pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte = pnd_End_Key__R_Field_6.newFieldInGroup("pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte", "#E-LAST-CHNGE-INVRT-DTE", 
            FieldType.NUMERIC, 8);
        pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Tme = pnd_End_Key__R_Field_6.newFieldInGroup("pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Tme", "#E-LAST-CHNGE-INVRT-TME", 
            FieldType.NUMERIC, 7);
        pnd_End_Key_Pnd_E_Admin_Unit_Cde = pnd_End_Key__R_Field_5.newFieldInGroup("pnd_End_Key_Pnd_E_Admin_Unit_Cde", "#E-ADMIN-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_End_Key_Pnd_E_Actve_Ind = pnd_End_Key__R_Field_5.newFieldInGroup("pnd_End_Key_Pnd_E_Actve_Ind", "#E-ACTVE-IND", FieldType.STRING, 1);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_7", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2 = pnd_Tbl_Prime_Key__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2", "#TBL-ACTVE-IND-2-2", 
            FieldType.STRING, 1);
        pnd_Tbl_Split = localVariables.newFieldInRecord("pnd_Tbl_Split", "#TBL-SPLIT", FieldType.STRING, 30);

        pnd_Tbl_Split__R_Field_8 = localVariables.newGroupInRecord("pnd_Tbl_Split__R_Field_8", "REDEFINE", pnd_Tbl_Split);
        pnd_Tbl_Split_Pnd_Tbl_Start_Dt = pnd_Tbl_Split__R_Field_8.newFieldInGroup("pnd_Tbl_Split_Pnd_Tbl_Start_Dt", "#TBL-START-DT", FieldType.TIME);
        pnd_Tbl_Split_Pnd_Tbl_Stop_Dt = pnd_Tbl_Split__R_Field_8.newFieldInGroup("pnd_Tbl_Split_Pnd_Tbl_Stop_Dt", "#TBL-STOP-DT", FieldType.TIME);
        pnd_Conversion_Alpha = localVariables.newFieldInRecord("pnd_Conversion_Alpha", "#CONVERSION-ALPHA", FieldType.STRING, 15);

        pnd_Conversion_Alpha__R_Field_9 = localVariables.newGroupInRecord("pnd_Conversion_Alpha__R_Field_9", "REDEFINE", pnd_Conversion_Alpha);
        pnd_Conversion_Alpha_Pnd_Conversion_Numeric = pnd_Conversion_Alpha__R_Field_9.newFieldInGroup("pnd_Conversion_Alpha_Pnd_Conversion_Numeric", "#CONVERSION-NUMERIC", 
            FieldType.NUMERIC, 15);
        pnd_Start_Dte_Tme = localVariables.newFieldInRecord("pnd_Start_Dte_Tme", "#START-DTE-TME", FieldType.STRING, 15);
        pnd_End_Dte_Tme = localVariables.newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.STRING, 15);

        pnd_End_Dte_Tme__R_Field_10 = localVariables.newGroupInRecord("pnd_End_Dte_Tme__R_Field_10", "REDEFINE", pnd_End_Dte_Tme);
        pnd_End_Dte_Tme_Pnd_End_Yyyymmdd = pnd_End_Dte_Tme__R_Field_10.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Yyyymmdd", "#END-YYYYMMDD", FieldType.STRING, 
            8);
        pnd_End_Dte_Tme_Pnd_End_Hhiisst = pnd_End_Dte_Tme__R_Field_10.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Hhiisst", "#END-HHIISST", FieldType.STRING, 
            7);

        pnd_End_Dte_Tme__R_Field_11 = localVariables.newGroupInRecord("pnd_End_Dte_Tme__R_Field_11", "REDEFINE", pnd_End_Dte_Tme);
        pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N = pnd_End_Dte_Tme__R_Field_11.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N", "#END-DTE-TME-N", FieldType.NUMERIC, 
            15);
        pnd_Curr_Dte_Tme = localVariables.newFieldInRecord("pnd_Curr_Dte_Tme", "#CURR-DTE-TME", FieldType.STRING, 15);
        pnd_Prev_Dte_Tme = localVariables.newFieldInRecord("pnd_Prev_Dte_Tme", "#PREV-DTE-TME", FieldType.STRING, 15);

        pnd_Prev_Dte_Tme__R_Field_12 = localVariables.newGroupInRecord("pnd_Prev_Dte_Tme__R_Field_12", "REDEFINE", pnd_Prev_Dte_Tme);
        pnd_Prev_Dte_Tme_Pnd_Prev_Yyyymmdd = pnd_Prev_Dte_Tme__R_Field_12.newFieldInGroup("pnd_Prev_Dte_Tme_Pnd_Prev_Yyyymmdd", "#PREV-YYYYMMDD", FieldType.STRING, 
            8);
        pnd_Prev_Dte_Tme_Pnd_Prev_Hhiisst = pnd_Prev_Dte_Tme__R_Field_12.newFieldInGroup("pnd_Prev_Dte_Tme_Pnd_Prev_Hhiisst", "#PREV-HHIISST", FieldType.STRING, 
            7);

        vw_cwf_Who_Work_Request = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Work_Request", "CWF-WHO-WORK-REQUEST"), "CWF_WHO_WORK_REQUEST", "CWF_WORK_REQUEST");
        cwf_Who_Work_Request_Rqst_Log_Dte_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Who_Work_Request_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Who_Work_Request_Rqst_Log_Invrt_Dte_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Rqst_Log_Invrt_Dte_Tme", 
            "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        cwf_Who_Work_Request_Pin_Nbr = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Who_Work_Request_Pin_Nbr.setDdmHeader("PIN");
        cwf_Who_Work_Request_Rqst_Id = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Rqst_Id", "RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Who_Work_Request_Rqst_Id.setDdmHeader("REQUEST ID");
        cwf_Who_Work_Request_Tiaa_Rcvd_Dte = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Who_Work_Request_Work_Prcss_Id = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Who_Work_Request_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Who_Work_Request_Rlte_Rqst_Id = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Rlte_Rqst_Id", "RLTE-RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RLTE_RQST_ID");
        cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme", 
            "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");

        cwf_Who_Work_Request__R_Field_13 = vw_cwf_Who_Work_Request.getRecord().newGroupInGroup("cwf_Who_Work_Request__R_Field_13", "REDEFINE", cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme);
        cwf_Who_Work_Request_Pnd_Yyyymmdd = cwf_Who_Work_Request__R_Field_13.newFieldInGroup("cwf_Who_Work_Request_Pnd_Yyyymmdd", "#YYYYMMDD", FieldType.STRING, 
            8);
        cwf_Who_Work_Request_Pnd_Hhiisst = cwf_Who_Work_Request__R_Field_13.newFieldInGroup("cwf_Who_Work_Request_Pnd_Hhiisst", "#HHIISST", FieldType.STRING, 
            7);
        cwf_Who_Work_Request_Final_Close_Out_Dte_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Final_Close_Out_Dte_Tme", 
            "FINAL-CLOSE-OUT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_DTE_TME");
        cwf_Who_Work_Request_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        cwf_Who_Work_Request_Effctve_Dte = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Who_Work_Request_Status_Cde = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Who_Work_Request_Status_Cde.setDdmHeader("STATUS/CODE");
        cwf_Who_Work_Request_Status_Freeze_Ind = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Who_Work_Request_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Who_Work_Request_Crprte_On_Tme_Ind = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_ON_TME_IND");
        cwf_Who_Work_Request_Crprte_On_Tme_Ind.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Who_Work_Request_Trans_Dte = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Trans_Dte", "TRANS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        cwf_Who_Work_Request_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Who_Work_Request_Crprte_Status_Ind = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Who_Work_Request_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Who_Work_Request_Sec_Turnaround_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Sec_Turnaround_Tme", "SEC-TURNAROUND-TME", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "SEC_TURNAROUND_TME");
        cwf_Who_Work_Request_Sec_Turnaround_Tme.setDdmHeader("SEC/TURNAROUND");
        cwf_Who_Work_Request_Sec_Updte_Dte = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Sec_Updte_Dte", "SEC-UPDTE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "SEC_UPDTE_DTE");
        cwf_Who_Work_Request_Sec_Updte_Dte.setDdmHeader("SEC/UPDATE");
        cwf_Who_Work_Request_Rqst_Indicators = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Rqst_Indicators", "RQST-INDICATORS", 
            FieldType.STRING, 25, RepeatingFieldStrategy.None, "RQST_INDICATORS");
        cwf_Who_Work_Request_Rqst_Indicators.setDdmHeader("INDICATORS");
        cwf_Who_Work_Request_Unit_Cde = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Unit_Cde", "UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_CDE");
        cwf_Who_Work_Request_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Who_Work_Request_Dwnld_Updte_Dte_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Dwnld_Updte_Dte_Tme", "DWNLD-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "DWNLD_UPDTE_DTE_TME");
        registerRecord(vw_cwf_Who_Work_Request);

        vw_cwf_Who_Activity_File = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Activity_File", "CWF-WHO-ACTIVITY-FILE"), "CWF_WHO_ACTIVITY_FILE", 
            "CWF_WHO_ACTIVITY");
        cwf_Who_Activity_File_Rqst_Log_Dte_Tme = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Who_Activity_File_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Who_Activity_File_Strtng_Event_Dte_Tme = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Strtng_Event_Dte_Tme", 
            "STRTNG-EVENT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "STRTNG_EVENT_DTE_TME");
        cwf_Who_Activity_File_Strtng_Event_Dte_Tme.setDdmHeader("STARTING/EVENT");
        cwf_Who_Activity_File_Endng_Event_Dte_Tme = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Endng_Event_Dte_Tme", 
            "ENDNG-EVENT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ENDNG_EVENT_DTE_TME");
        cwf_Who_Activity_File_Endng_Event_Dte_Tme.setDdmHeader("ENDING/EVENT");
        cwf_Who_Activity_File_Cmpltd_Actvty_Ind = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Cmpltd_Actvty_Ind", "CMPLTD-ACTVTY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CMPLTD_ACTVTY_IND");
        cwf_Who_Activity_File_Cmpltd_Actvty_Ind.setDdmHeader("COMPLETED/ACTIVITY/INDICATOR");
        cwf_Who_Activity_File_Admin_Unit_Cde = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Who_Activity_File_Admin_Unit_Cde.setDdmHeader("UNIT");
        cwf_Who_Activity_File_Empl_Oprtr_Cde = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Who_Activity_File_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        cwf_Who_Activity_File_Step_Id = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Step_Id", "STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "STEP_ID");
        cwf_Who_Activity_File_Step_Id.setDdmHeader("STEP/ID");
        cwf_Who_Activity_File_Admin_Status_Cde = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Who_Activity_File_Admin_Status_Cde.setDdmHeader("STATUS");
        cwf_Who_Activity_File_Unit_On_Tme_Ind = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Unit_On_Tme_Ind", "UNIT-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "UNIT_ON_TME_IND");
        cwf_Who_Activity_File_Unit_On_Tme_Ind.setDdmHeader("UNIT/ON/TIME");
        cwf_Who_Activity_File_Crtn_Run_Id = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Crtn_Run_Id", "CRTN-RUN-ID", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CRTN_RUN_ID");
        cwf_Who_Activity_File_Crtn_Run_Id.setDdmHeader("CREATION/RUN ID");
        cwf_Who_Activity_File_Actvty_Elpsd_Clndr_Hours = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Actvty_Elpsd_Clndr_Hours", 
            "ACTVTY-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 6, 2, RepeatingFieldStrategy.None, "ACTVTY_ELPSD_CLNDR_HOURS");
        cwf_Who_Activity_File_Actvty_Elpsd_Clndr_Hours.setDdmHeader("ELAPSED/CALENDAR/TIME(HR)");
        cwf_Who_Activity_File_Actvty_Elpsd_Bsnss_Hours = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Actvty_Elpsd_Bsnss_Hours", 
            "ACTVTY-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 6, 2, RepeatingFieldStrategy.None, "ACTVTY_ELPSD_BSNSS_HOURS");
        cwf_Who_Activity_File_Actvty_Elpsd_Bsnss_Hours.setDdmHeader("ELAPSED/BUSINESS/TIME(HR)");
        cwf_Who_Activity_File_Invrt_Strtng_Event = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Invrt_Strtng_Event", "INVRT-STRTNG-EVENT", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "INVRT_STRTNG_EVENT");
        cwf_Who_Activity_File_Invrt_Strtng_Event.setDdmHeader("INVERTED/STARTING/EVENT");
        cwf_Who_Activity_File_Dwnld_Updte_Dte_Tme = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Dwnld_Updte_Dte_Tme", 
            "DWNLD-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "DWNLD_UPDTE_DTE_TME");
        cwf_Who_Activity_File_Dwnld_Updte_Dte_Tme.setDdmHeader("UPDATE/DATETIME");
        cwf_Who_Activity_File_Rt_Sqnce_Nbr = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        cwf_Who_Activity_File_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        cwf_Who_Activity_File_Off_Rtng_Ind = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Off_Rtng_Ind", "OFF-RTNG-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "OFF_RTNG_IND");
        cwf_Who_Activity_File_Off_Rtng_Ind.setDdmHeader("OFF-ROUTING");
        cwf_Who_Activity_File_Msg_Rtrn_Cde = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Msg_Rtrn_Cde", "MSG-RTRN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "MSG_RTRN_CDE");
        cwf_Who_Activity_File_Msg_Rtrn_Cde.setDdmHeader("MSGRET/CODE");
        cwf_Who_Activity_File_Msg_Nbr = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Msg_Nbr", "MSG-NBR", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "MSG_NBR");
        cwf_Who_Activity_File_Msg_Nbr.setDdmHeader("MSG/NUMBER");
        registerRecord(vw_cwf_Who_Activity_File);

        vw_cwf_Master = new DataAccessProgramView(new NameInfo("vw_cwf_Master", "CWF-MASTER"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Rqst_Log_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master__R_Field_14 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_14", "REDEFINE", cwf_Master_Rqst_Log_Dte_Tme);
        cwf_Master_Rqst_Log_Index_Dte = cwf_Master__R_Field_14.newFieldInGroup("cwf_Master_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 
            8);
        cwf_Master_Rqst_Log_Index_Tmte = cwf_Master__R_Field_14.newFieldInGroup("cwf_Master_Rqst_Log_Index_Tmte", "RQST-LOG-INDEX-TMTE", FieldType.STRING, 
            7);
        cwf_Master_Pin_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Rqst_Orgn_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_ORGN_CDE");
        cwf_Master_Work_Prcss_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Master_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Master_Unit_Updte_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Empl_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "EMPL_OPRTR_CDE");
        cwf_Master_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        cwf_Master_Last_Chnge_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        cwf_Master__R_Field_15 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_15", "REDEFINE", cwf_Master_Last_Chnge_Dte_Tme);
        cwf_Master_Pnd_Date_C = cwf_Master__R_Field_15.newFieldInGroup("cwf_Master_Pnd_Date_C", "#DATE-C", FieldType.NUMERIC, 8);

        cwf_Master__R_Field_16 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_16", "REDEFINE", cwf_Master_Last_Chnge_Dte_Tme);
        cwf_Master_Pnd_Date_Ca = cwf_Master__R_Field_16.newFieldInGroup("cwf_Master_Pnd_Date_Ca", "#DATE-CA", FieldType.STRING, 8);
        cwf_Master_Last_Chnge_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Last_Chnge_Invrt_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Invrt_Dte_Tme", "LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        cwf_Master_Last_Chnge_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Step_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        cwf_Master_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Rt_Sqnce_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "RT_SQNCE_NBR");
        cwf_Master_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        cwf_Master_Admin_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");

        cwf_Master__R_Field_17 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_17", "REDEFINE", cwf_Master_Admin_Unit_Cde);
        cwf_Master_Unit_Id_Cde = cwf_Master__R_Field_17.newFieldInGroup("cwf_Master_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        cwf_Master_Admin_Status_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Status_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        cwf_Master_Status_Updte_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Last_Updte_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "LAST_UPDTE_DTE");
        cwf_Master_Last_Updte_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Last_Updte_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Actve_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");

        cwf_Master__R_Field_18 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_18", "REDEFINE", cwf_Master_Actve_Ind);
        cwf_Master_Actve_Ind_1 = cwf_Master__R_Field_18.newFieldInGroup("cwf_Master_Actve_Ind_1", "ACTVE-IND-1", FieldType.STRING, 1);
        cwf_Master_Actve_Ind_2 = cwf_Master__R_Field_18.newFieldInGroup("cwf_Master_Actve_Ind_2", "ACTVE-IND-2", FieldType.STRING, 1);
        cwf_Master_Crprte_Status_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Tiaa_Rcvd_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Master_Effctve_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "EFFCTVE_DTE");
        cwf_Master_Trnsctn_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Trnsctn_Dte", "TRNSCTN-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRNSCTN_DTE");
        cwf_Master_Trnsctn_Dte.setDdmHeader("TRANSACTION/DATE");
        cwf_Master_Trans_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Trans_Dte", "TRANS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cwf_Master_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Physcl_Fldr_Id_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Mj_Chrge_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Chrge_Dte_Tme", "MJ-CHRGE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "MJ_CHRGE_DTE_TME");
        cwf_Master_Mj_Chrge_Dte_Tme.setDdmHeader("CHARGEOUT/DATE-TIME");
        cwf_Master_Mj_Pull_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MJ_PULL_IND");
        cwf_Master_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Status_Freeze_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Freeze_Ind", "STATUS-FREEZE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Work_List_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "WORK_LIST_IND");
        cwf_Master_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        cwf_Master_Unit_Clock_Start_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Clock_Start_Dte_Tme", "UNIT-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Unit_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Clock_End_Dte_Tme", "UNIT-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Empl_Clock_Start_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Empl_Clock_Start_Dte_Tme", "EMPL-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        cwf_Master_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        cwf_Master_Empl_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Empl_Clock_End_Dte_Tme", "EMPL-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        cwf_Master_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        cwf_Master_Step_Clock_Start_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Clock_Start_Dte_Tme", "STEP-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        cwf_Master_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        cwf_Master_Step_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Clock_End_Dte_Tme", "STEP-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        cwf_Master_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        cwf_Master_Crprte_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        cwf_Master_Final_Close_Out_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Final_Close_Out_Dte_Tme", "FINAL-CLOSE-OUT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_DTE_TME");
        cwf_Master_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        cwf_Master_Rqst_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Id", "RQST-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, 
            "RQST_ID");
        cwf_Master_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Master__R_Field_19 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_19", "REDEFINE", cwf_Master_Rqst_Id);
        cwf_Master_Rqst_Work_Prcss_Id = cwf_Master__R_Field_19.newFieldInGroup("cwf_Master_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        cwf_Master_Rqst_Tiaa_Dte_Tme = cwf_Master__R_Field_19.newFieldInGroup("cwf_Master_Rqst_Tiaa_Dte_Tme", "RQST-TIAA-DTE-TME", FieldType.STRING, 8);
        cwf_Master_Rlte_Rqst_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rlte_Rqst_Id", "RLTE-RQST-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, 
            "RLTE_RQST_ID");
        registerRecord(vw_cwf_Master);

        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Found_Sw = localVariables.newFieldInRecord("pnd_Found_Sw", "#FOUND-SW", FieldType.STRING, 1);
        pnd_Disc_Msg = localVariables.newFieldInRecord("pnd_Disc_Msg", "#DISC-MSG", FieldType.STRING, 63);

        pnd_Disc_Msg__R_Field_20 = localVariables.newGroupInRecord("pnd_Disc_Msg__R_Field_20", "REDEFINE", pnd_Disc_Msg);
        pnd_Disc_Msg_Pnd_Disc_Msg_1 = pnd_Disc_Msg__R_Field_20.newFieldInGroup("pnd_Disc_Msg_Pnd_Disc_Msg_1", "#DISC-MSG-1", FieldType.STRING, 34);
        pnd_Disc_Msg_Pnd_Disc_Msg_2 = pnd_Disc_Msg__R_Field_20.newFieldInGroup("pnd_Disc_Msg_Pnd_Disc_Msg_2", "#DISC-MSG-2", FieldType.STRING, 29);

        pnd_Disc_Msg__R_Field_21 = pnd_Disc_Msg__R_Field_20.newGroupInGroup("pnd_Disc_Msg__R_Field_21", "REDEFINE", pnd_Disc_Msg_Pnd_Disc_Msg_2);
        pnd_Disc_Msg_Pnd_Disc_Msg_2_1 = pnd_Disc_Msg__R_Field_21.newFieldInGroup("pnd_Disc_Msg_Pnd_Disc_Msg_2_1", "#DISC-MSG-2-1", FieldType.STRING, 15);
        pnd_Disc_Msg_Pnd_Disc_Msg_2_2 = pnd_Disc_Msg__R_Field_21.newFieldInGroup("pnd_Disc_Msg_Pnd_Disc_Msg_2_2", "#DISC-MSG-2-2", FieldType.STRING, 14);
        pnd_Stop_Sweep_Time_A = localVariables.newFieldInRecord("pnd_Stop_Sweep_Time_A", "#STOP-SWEEP-TIME-A", FieldType.STRING, 15);

        pnd_Stop_Sweep_Time_A__R_Field_22 = localVariables.newGroupInRecord("pnd_Stop_Sweep_Time_A__R_Field_22", "REDEFINE", pnd_Stop_Sweep_Time_A);
        pnd_Stop_Sweep_Time_A_Pnd_Stop_Yyyymmdd = pnd_Stop_Sweep_Time_A__R_Field_22.newFieldInGroup("pnd_Stop_Sweep_Time_A_Pnd_Stop_Yyyymmdd", "#STOP-YYYYMMDD", 
            FieldType.STRING, 8);
        pnd_Stop_Sweep_Time_A_Pnd_Stop_Hhiisst = pnd_Stop_Sweep_Time_A__R_Field_22.newFieldInGroup("pnd_Stop_Sweep_Time_A_Pnd_Stop_Hhiisst", "#STOP-HHIISST", 
            FieldType.STRING, 7);

        pnd_Stop_Sweep_Time_A__R_Field_23 = localVariables.newGroupInRecord("pnd_Stop_Sweep_Time_A__R_Field_23", "REDEFINE", pnd_Stop_Sweep_Time_A);
        pnd_Stop_Sweep_Time_A_Pnd_Stop_Sweep_Time_N = pnd_Stop_Sweep_Time_A__R_Field_23.newFieldInGroup("pnd_Stop_Sweep_Time_A_Pnd_Stop_Sweep_Time_N", 
            "#STOP-SWEEP-TIME-N", FieldType.NUMERIC, 15);
        pnd_Stop_Sweep_Invrt_Time = localVariables.newFieldInRecord("pnd_Stop_Sweep_Invrt_Time", "#STOP-SWEEP-INVRT-TIME", FieldType.NUMERIC, 15);
        pnd_Save_Status = localVariables.newFieldInRecord("pnd_Save_Status", "#SAVE-STATUS", FieldType.STRING, 4);
        pnd_Rqst_Log_Dte_Tme_Save = localVariables.newFieldInRecord("pnd_Rqst_Log_Dte_Tme_Save", "#RQST-LOG-DTE-TME-SAVE", FieldType.STRING, 15);
        pnd_Pin_Nbr_Save = localVariables.newFieldInRecord("pnd_Pin_Nbr_Save", "#PIN-NBR-SAVE", FieldType.NUMERIC, 7);
        pnd_Wpid_Save = localVariables.newFieldInRecord("pnd_Wpid_Save", "#WPID-SAVE", FieldType.STRING, 6);
        pnd_Tiaa_Rcvd_Dte_Save = localVariables.newFieldInRecord("pnd_Tiaa_Rcvd_Dte_Save", "#TIAA-RCVD-DTE-SAVE", FieldType.DATE);
        pnd_Rqst_Id_Save = localVariables.newFieldInRecord("pnd_Rqst_Id_Save", "#RQST-ID-SAVE", FieldType.STRING, 23);
        pnd_Rlte_Rqst_Id_Save = localVariables.newFieldInRecord("pnd_Rlte_Rqst_Id_Save", "#RLTE-RQST-ID-SAVE", FieldType.STRING, 23);
        pnd_Last_Chnge_Dte_Tme_Save = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_Tme_Save", "#LAST-CHNGE-DTE-TME-SAVE", FieldType.NUMERIC, 15);
        pnd_Crprte_Stat_Ind_Save = localVariables.newFieldInRecord("pnd_Crprte_Stat_Ind_Save", "#CRPRTE-STAT-IND-SAVE", FieldType.STRING, 1);
        pnd_Final_Close_Out_Save = localVariables.newFieldInRecord("pnd_Final_Close_Out_Save", "#FINAL-CLOSE-OUT-SAVE", FieldType.TIME);
        pnd_Crprte_Clock_Dte_Save = localVariables.newFieldInRecord("pnd_Crprte_Clock_Dte_Save", "#CRPRTE-CLOCK-DTE-SAVE", FieldType.STRING, 15);
        pnd_Discrep_Sw = localVariables.newFieldInRecord("pnd_Discrep_Sw", "#DISCREP-SW", FieldType.STRING, 1);
        pnd_Crprte_Status_Log_Dte_Key = localVariables.newFieldInRecord("pnd_Crprte_Status_Log_Dte_Key", "#CRPRTE-STATUS-LOG-DTE-KEY", FieldType.STRING, 
            16);

        pnd_Crprte_Status_Log_Dte_Key__R_Field_24 = localVariables.newGroupInRecord("pnd_Crprte_Status_Log_Dte_Key__R_Field_24", "REDEFINE", pnd_Crprte_Status_Log_Dte_Key);
        pnd_Crprte_Status_Log_Dte_Key_Pnd_Crprte_Status_Ind = pnd_Crprte_Status_Log_Dte_Key__R_Field_24.newFieldInGroup("pnd_Crprte_Status_Log_Dte_Key_Pnd_Crprte_Status_Ind", 
            "#CRPRTE-STATUS-IND", FieldType.STRING, 1);
        pnd_Crprte_Status_Log_Dte_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Crprte_Status_Log_Dte_Key__R_Field_24.newFieldInGroup("pnd_Crprte_Status_Log_Dte_Key_Pnd_Rqst_Log_Dte_Tme", 
            "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Log_Dte_Tme_Invrt_St_Event_Key = localVariables.newFieldInRecord("pnd_Log_Dte_Tme_Invrt_St_Event_Key", "#LOG-DTE-TME-INVRT-ST-EVENT-KEY", 
            FieldType.STRING, 30);

        pnd_Log_Dte_Tme_Invrt_St_Event_Key__R_Field_25 = localVariables.newGroupInRecord("pnd_Log_Dte_Tme_Invrt_St_Event_Key__R_Field_25", "REDEFINE", 
            pnd_Log_Dte_Tme_Invrt_St_Event_Key);
        pnd_Log_Dte_Tme_Invrt_St_Event_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Log_Dte_Tme_Invrt_St_Event_Key__R_Field_25.newFieldInGroup("pnd_Log_Dte_Tme_Invrt_St_Event_Key_Pnd_Rqst_Log_Dte_Tme", 
            "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Log_Dte_Tme_Invrt_St_Event_Key_Pnd_Invrt_Strtng_Event = pnd_Log_Dte_Tme_Invrt_St_Event_Key__R_Field_25.newFieldInGroup("pnd_Log_Dte_Tme_Invrt_St_Event_Key_Pnd_Invrt_Strtng_Event", 
            "#INVRT-STRTNG-EVENT", FieldType.NUMERIC, 15);
        pnd_Dwnld_Updte_Log_Dte_Key = localVariables.newFieldInRecord("pnd_Dwnld_Updte_Log_Dte_Key", "#DWNLD-UPDTE-LOG-DTE-KEY", FieldType.STRING, 22);

        pnd_Dwnld_Updte_Log_Dte_Key__R_Field_26 = localVariables.newGroupInRecord("pnd_Dwnld_Updte_Log_Dte_Key__R_Field_26", "REDEFINE", pnd_Dwnld_Updte_Log_Dte_Key);
        pnd_Dwnld_Updte_Log_Dte_Key_Pnd_Dwnld_Updte_Dte_Tme = pnd_Dwnld_Updte_Log_Dte_Key__R_Field_26.newFieldInGroup("pnd_Dwnld_Updte_Log_Dte_Key_Pnd_Dwnld_Updte_Dte_Tme", 
            "#DWNLD-UPDTE-DTE-TME", FieldType.TIME);
        pnd_Dwnld_Updte_Log_Dte_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Dwnld_Updte_Log_Dte_Key__R_Field_26.newFieldInGroup("pnd_Dwnld_Updte_Log_Dte_Key_Pnd_Rqst_Log_Dte_Tme", 
            "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Wr_Isn = localVariables.newFieldInRecord("pnd_Wr_Isn", "#WR-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Ac_Isn = localVariables.newFieldInRecord("pnd_Ac_Isn", "#AC-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_All_Wr_Cntr = localVariables.newFieldInRecord("pnd_All_Wr_Cntr", "#ALL-WR-CNTR", FieldType.NUMERIC, 9);
        pnd_Wr_Ncs_Cntr = localVariables.newFieldInRecord("pnd_Wr_Ncs_Cntr", "#WR-NCS-CNTR", FieldType.NUMERIC, 9);
        pnd_Wr_Updated = localVariables.newFieldInRecord("pnd_Wr_Updated", "#WR-UPDATED", FieldType.NUMERIC, 9);
        pnd_Ac_Updated = localVariables.newFieldInRecord("pnd_Ac_Updated", "#AC-UPDATED", FieldType.NUMERIC, 9);
        pnd_Restart_Cntr = localVariables.newFieldInRecord("pnd_Restart_Cntr", "#RESTART-CNTR", FieldType.NUMERIC, 9);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Crc_Cntr = localVariables.newFieldInRecord("pnd_Crc_Cntr", "#CRC-CNTR", FieldType.NUMERIC, 9);
        pnd_Opn_Cntr = localVariables.newFieldInRecord("pnd_Opn_Cntr", "#OPN-CNTR", FieldType.NUMERIC, 9);
        pnd_Nfd_Counter = localVariables.newFieldInRecord("pnd_Nfd_Counter", "#NFD-COUNTER", FieldType.NUMERIC, 9);
        pnd_Act_Frm_Wr_Cntr = localVariables.newFieldInRecord("pnd_Act_Frm_Wr_Cntr", "#ACT-FRM-WR-CNTR", FieldType.NUMERIC, 9);
        pnd_Act_Frm_Mit_Cntr = localVariables.newFieldInRecord("pnd_Act_Frm_Mit_Cntr", "#ACT-FRM-MIT-CNTR", FieldType.NUMERIC, 9);
        pnd_Rldt_Value_Tbl = localVariables.newFieldInRecord("pnd_Rldt_Value_Tbl", "#RLDT-VALUE-TBL", FieldType.STRING, 60);

        pnd_Rldt_Value_Tbl__R_Field_27 = localVariables.newGroupInRecord("pnd_Rldt_Value_Tbl__R_Field_27", "REDEFINE", pnd_Rldt_Value_Tbl);
        pnd_Rldt_Value_Tbl_Pnd_Rldt_Value = pnd_Rldt_Value_Tbl__R_Field_27.newFieldArrayInGroup("pnd_Rldt_Value_Tbl_Pnd_Rldt_Value", "#RLDT-VALUE", FieldType.STRING, 
            15, new DbsArrayController(1, 4));
        pnd_Rldt_Start = localVariables.newFieldInRecord("pnd_Rldt_Start", "#RLDT-START", FieldType.STRING, 15);
        pnd_Calc_Field_A = localVariables.newFieldInRecord("pnd_Calc_Field_A", "#CALC-FIELD-A", FieldType.STRING, 15);

        pnd_Calc_Field_A__R_Field_28 = localVariables.newGroupInRecord("pnd_Calc_Field_A__R_Field_28", "REDEFINE", pnd_Calc_Field_A);
        pnd_Calc_Field_A_Pnd_Calc_Field_N = pnd_Calc_Field_A__R_Field_28.newFieldInGroup("pnd_Calc_Field_A_Pnd_Calc_Field_N", "#CALC-FIELD-N", FieldType.NUMERIC, 
            15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Histo_1.reset();
        vw_cwf_Who_Work_Request.reset();
        vw_cwf_Who_Activity_File.reset();
        vw_cwf_Master.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn3022() throws Exception
    {
        super("Cwfn3022");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*    INITIALIZATION
        pnd_Rldt_Value_Tbl.setValue("199707071434372199710161253028199807271924383199807271917494");                                                                      //Natural: FORMAT ( 0 ) PS = 56 LS = 131;//Natural: ASSIGN #RLDT-VALUE-TBL := '199707071434372199710161253028199807271924383199807271917494'
        //*   TO ALLOW FOR EASY ESCAPE FROM PROGRAM.
        REP1:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            st = Global.getTIMN();                                                                                                                                        //Natural: SET TIME
            //* *****
                                                                                                                                                                          //Natural: PERFORM SELECT-WORK-REQUEST-CLOSE
            sub_Select_Work_Request_Close();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   WRITE (0)
            //*  / '******* Close WR and Activities fix *******'
            //*  / 'Begining period    = ' #RLDT-START
            //*  / '***************************************************'
            //*  / 'Number of Close Records read for the period      = ' #ALL-WR-CNTR
            //*  / 'Number of Close Records with Status "Not Closed" = ' #WR-NCS-CNTR
            //*  / '------- Update Statistics for Close cases -------- '
            //*  / 'Number of Actv. without Close Status        = ' #OPN-CNTR
            //*  / 'Number of CRC Units found                   = ' #CRC-CNTR
            //*  / 'Number of Actv. build from MIT              = ' #ACT-FRM-MIT-CNTR
            //*  / 'Number of Actv. build from WR               = ' #ACT-FRM-WR-CNTR
            //*  / 'MIT records Not found                       = ' #NFD-COUNTER
            //*  / 'Number of WR Updated                        = ' #WR-UPDATED
            //*  / 'Number of Activities Updated                = ' #AC-UPDATED
            //*  / '***************************************************'
            //*  / 'ELAPSED TIME TO READ RECORDS'
            //*    '(HH:MM:SS.T) :' *TIMD (ST.) (EM=99:99:99'.'9)
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            //*   (REP1.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-WORK-REQUEST-CLOSE
        //* ******************************************
        //*          CWF-WHO-WORK-REQUEST.PIN-NBR  = 1901802
        //*  WRITE /'Activity    ISN =    ' *ISN
        //*       /'Activity Admin Status' ADMIN-STATUS-CDE
        //*       /'Activity Startng Evnt' STRTNG-EVENT-DTE-TME(EM=YYYYMMDDHHIISST)
        //*  -------------- CLOSE ACTIVITY FOUND --------------------
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-MIT
        //* *************************
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-CLSE-ACT-FRM-WR
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-CLSE-ACT-FRM-MIT
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( REP1. )
        Global.setEscapeCode(EscapeType.Bottom, "REP1");
        if (true) return;
    }
    private void sub_Select_Work_Request_Close() throws Exception                                                                                                         //Natural: SELECT-WORK-REQUEST-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Who_Work_Request.startDatabaseFind                                                                                                                         //Natural: FIND ( 1 ) CWF-WHO-WORK-REQUEST WITH RQST-LOG-DTE-TME = #PARM-LOG-DTE-TME
        (
        "F1",
        new Wc[] { new Wc("RQST_LOG_DTE_TME", "=", pnd_Parm_Log_Dte_Tme, WcType.WITH) },
        1
        );
        F1:
        while (condition(vw_cwf_Who_Work_Request.readNextRow("F1", true)))
        {
            vw_cwf_Who_Work_Request.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Who_Work_Request.getAstCOUNTER().equals(0)))                                                                                             //Natural: IF NO RECORD FOUND
            {
                pnd_Nfd_Counter.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #NFD-COUNTER
                //*    WRITE (0)
                //*      / 'Close record with RLDT Not Found ==>' #PARM-LOG-DTE-TME
                if (true) break F1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F1. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_All_Wr_Cntr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #ALL-WR-CNTR
            pnd_Wr_Isn.setValue(vw_cwf_Who_Work_Request.getAstISN("F1"));                                                                                                 //Natural: ASSIGN #WR-ISN := *ISN
            //*  NOW WR IS FOUND - READ ACTIVITY BACKWARDS TO FIND CLOSE STATUS
            //*  --------------------------------------------------------------
            //*  ASSIGN KEY TO READ AN ACTIVITY FILE
            pnd_Log_Dte_Tme_Invrt_St_Event_Key.reset();                                                                                                                   //Natural: RESET #LOG-DTE-TME-INVRT-ST-EVENT-KEY
            pnd_Log_Dte_Tme_Invrt_St_Event_Key_Pnd_Rqst_Log_Dte_Tme.setValue(cwf_Who_Work_Request_Rqst_Log_Dte_Tme);                                                      //Natural: ASSIGN #LOG-DTE-TME-INVRT-ST-EVENT-KEY.#RQST-LOG-DTE-TME := CWF-WHO-WORK-REQUEST.RQST-LOG-DTE-TME
            vw_cwf_Who_Activity_File.startDatabaseRead                                                                                                                    //Natural: READ CWF-WHO-ACTIVITY-FILE BY LOG-DTE-TME-INVRT-ST-EVENT-KEY STARTING FROM #LOG-DTE-TME-INVRT-ST-EVENT-KEY
            (
            "R2",
            new Wc[] { new Wc("LOG_DTE_TME_INVRT_ST_EVENT_KEY", ">=", pnd_Log_Dte_Tme_Invrt_St_Event_Key, WcType.BY) },
            new Oc[] { new Oc("LOG_DTE_TME_INVRT_ST_EVENT_KEY", "ASC") }
            );
            R2:
            while (condition(vw_cwf_Who_Activity_File.readNextRow("R2")))
            {
                if (condition(cwf_Who_Activity_File_Rqst_Log_Dte_Tme.notEquals(cwf_Who_Work_Request_Rqst_Log_Dte_Tme)))                                                   //Natural: IF CWF-WHO-ACTIVITY-FILE.RQST-LOG-DTE-TME NE CWF-WHO-WORK-REQUEST.RQST-LOG-DTE-TME
                {
                    getReports().write(1, NEWLINE,"No Cls Act.found for WR =",cwf_Who_Work_Request_Rqst_Log_Dte_Tme,cwf_Who_Work_Request_Pin_Nbr,cwf_Who_Work_Request_Dwnld_Updte_Dte_Tme,  //Natural: WRITE ( 1 ) /'No Cls Act.found for WR =' CWF-WHO-WORK-REQUEST.RQST-LOG-DTE-TME CWF-WHO-WORK-REQUEST.PIN-NBR CWF-WHO-WORK-REQUEST.DWNLD-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST )
                        new ReportEditMask ("YYYYMMDDHHIISST"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*       ADD 1 TO #OPN-CNTR
                    //*  PIN-EXP
                    if (condition(cwf_Who_Work_Request_Unit_Cde.equals("CRC     ") || cwf_Who_Work_Request_Pin_Nbr.equals(1901802)))                                      //Natural: IF CWF-WHO-WORK-REQUEST.UNIT-CDE = 'CRC     ' OR CWF-WHO-WORK-REQUEST.PIN-NBR = 000001901802
                    {
                        pnd_Crc_Cntr.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CRC-CNTR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM FIND-MIT
                        sub_Find_Mit();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (true) break R2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R2. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cwf_Who_Activity_File_Admin_Status_Cde.less("7000")))                                                                                       //Natural: IF CWF-WHO-ACTIVITY-FILE.ADMIN-STATUS-CDE LT '7000'
                {
                    //*    OR CWF-WHO-ACTIVITY-FILE.ADMIN-STATUS-CDE GT '8999'
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Save_Status.setValue(cwf_Who_Activity_File_Admin_Status_Cde);                                                                                         //Natural: MOVE CWF-WHO-ACTIVITY-FILE.ADMIN-STATUS-CDE TO #SAVE-STATUS
                pnd_Ac_Isn.setValue(vw_cwf_Who_Activity_File.getAstISN("R2"));                                                                                            //Natural: ASSIGN #AC-ISN := *ISN
                GET_WR:                                                                                                                                                   //Natural: GET CWF-WHO-WORK-REQUEST #WR-ISN
                vw_cwf_Who_Work_Request.readByID(pnd_Wr_Isn.getLong(), "GET_WR");
                cwf_Who_Work_Request_Status_Cde.setValue(pnd_Save_Status);                                                                                                //Natural: ASSIGN CWF-WHO-WORK-REQUEST.STATUS-CDE := #SAVE-STATUS
                pnd_Wr_Updated.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WR-UPDATED
                vw_cwf_Who_Work_Request.updateDBRow("GET_WR");                                                                                                            //Natural: UPDATE ( GET-WR. )
                GET_AC:                                                                                                                                                   //Natural: GET CWF-WHO-ACTIVITY-FILE #AC-ISN
                vw_cwf_Who_Activity_File.readByID(pnd_Ac_Isn.getLong(), "GET_AC");
                cwf_Who_Activity_File_Dwnld_Updte_Dte_Tme.setValue(pnd_Date_T);                                                                                           //Natural: ASSIGN CWF-WHO-ACTIVITY-FILE.DWNLD-UPDTE-DTE-TME := #DATE-T
                pnd_Ac_Updated.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #AC-UPDATED
                vw_cwf_Who_Activity_File.updateDBRow("GET_AC");                                                                                                           //Natural: UPDATE ( GET-AC. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                if (true) break R2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R2. ) IMMEDIATE
                //*  R2.
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  F1.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Find_Mit() throws Exception                                                                                                                          //Natural: FIND-MIT
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Master.startDatabaseFind                                                                                                                                   //Natural: FIND CWF-MASTER WITH ACTV-UNQUE-KEY = CWF-WHO-WORK-REQUEST.RQST-LOG-DTE-TME
        (
        "F2",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", "=", cwf_Who_Work_Request_Rqst_Log_Dte_Tme, WcType.WITH) }
        );
        F2:
        while (condition(vw_cwf_Master.readNextRow("F2", true)))
        {
            vw_cwf_Master.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Master.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORD FOUND
            {
                pnd_Nfd_Counter.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #NFD-COUNTER
                                                                                                                                                                          //Natural: PERFORM BUILD-CLSE-ACT-FRM-WR
                sub_Build_Clse_Act_Frm_Wr();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    WRITE (0)
                //*      / 'Close MIT Record with RLDT Not Found ==>'
                //*               CWF-WHO-WORK-REQUEST.RQST-LOG-DTE-TME
                //*               CWF-WHO-WORK-REQUEST.PIN-NBR
                if (true) break F2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F2. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-NOREC
            //*  WRITE /'Went to find MIT record ='
            //*     CWF-MASTER.RQST-LOG-DTE-TME PIN-NBR *ISN
            if (condition(cwf_Master_Status_Cde.greaterOrEqual("7000") && cwf_Master_Status_Cde.less("9000")))                                                            //Natural: IF CWF-MASTER.STATUS-CDE GE '7000' AND CWF-MASTER.STATUS-CDE LT '9000'
            {
                                                                                                                                                                          //Natural: PERFORM BUILD-CLSE-ACT-FRM-MIT
                sub_Build_Clse_Act_Frm_Mit();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BUILD-CLSE-ACT-FRM-WR
                sub_Build_Clse_Act_Frm_Wr();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  F2.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Build_Clse_Act_Frm_Wr() throws Exception                                                                                                             //Natural: BUILD-CLSE-ACT-FRM-WR
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pnd_Act_Frm_Wr_Cntr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #ACT-FRM-WR-CNTR
        vw_cwf_Who_Activity_File.reset();                                                                                                                                 //Natural: RESET CWF-WHO-ACTIVITY-FILE
        cwf_Who_Activity_File_Rqst_Log_Dte_Tme.setValue(cwf_Who_Work_Request_Rqst_Log_Dte_Tme);                                                                           //Natural: MOVE CWF-WHO-WORK-REQUEST.RQST-LOG-DTE-TME TO CWF-WHO-ACTIVITY-FILE.RQST-LOG-DTE-TME
        if (condition(! (DbsUtil.maskMatches(cwf_Who_Work_Request_Pnd_Yyyymmdd,"YYYYMMDD"))))                                                                             //Natural: IF CWF-WHO-WORK-REQUEST.#YYYYMMDD NE MASK ( YYYYMMDD )
        {
            getReports().write(0, NEWLINE,"This WR has bad CRPRTE-Clock-End - RLDT = ",cwf_Who_Work_Request_Rqst_Log_Dte_Tme);                                            //Natural: WRITE /'This WR has bad CRPRTE-Clock-End - RLDT = ' CWF-WHO-WORK-REQUEST.RQST-LOG-DTE-TME
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        cwf_Who_Activity_File_Strtng_Event_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme);                   //Natural: MOVE EDITED CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME TO CWF-WHO-ACTIVITY-FILE.STRTNG-EVENT-DTE-TME ( EM = YYYYMMDDHHIISST )
        cwf_Who_Activity_File_Endng_Event_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme);                    //Natural: MOVE EDITED CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME TO CWF-WHO-ACTIVITY-FILE.ENDNG-EVENT-DTE-TME ( EM = YYYYMMDDHHIISST )
        cwf_Who_Activity_File_Cmpltd_Actvty_Ind.setValue("Y");                                                                                                            //Natural: MOVE 'Y' TO CWF-WHO-ACTIVITY-FILE.CMPLTD-ACTVTY-IND
        cwf_Who_Activity_File_Admin_Unit_Cde.setValue(cwf_Who_Work_Request_Unit_Cde);                                                                                     //Natural: MOVE CWF-WHO-WORK-REQUEST.UNIT-CDE TO CWF-WHO-ACTIVITY-FILE.ADMIN-UNIT-CDE
        cwf_Who_Activity_File_Empl_Oprtr_Cde.setValue(" ");                                                                                                               //Natural: MOVE ' ' TO CWF-WHO-ACTIVITY-FILE.EMPL-OPRTR-CDE
        cwf_Who_Activity_File_Step_Id.setValue(" ");                                                                                                                      //Natural: MOVE ' ' TO CWF-WHO-ACTIVITY-FILE.STEP-ID
        cwf_Who_Activity_File_Admin_Status_Cde.setValue("8799");                                                                                                          //Natural: MOVE '8799' TO CWF-WHO-ACTIVITY-FILE.ADMIN-STATUS-CDE
        cwf_Who_Activity_File_Unit_On_Tme_Ind.setValue(" ");                                                                                                              //Natural: MOVE ' ' TO CWF-WHO-ACTIVITY-FILE.UNIT-ON-TME-IND
        cwf_Who_Activity_File_Crtn_Run_Id.setValue(0);                                                                                                                    //Natural: MOVE 0 TO CWF-WHO-ACTIVITY-FILE.CRTN-RUN-ID
        cwf_Who_Activity_File_Actvty_Elpsd_Clndr_Hours.setValue(0);                                                                                                       //Natural: MOVE 0 TO CWF-WHO-ACTIVITY-FILE.ACTVTY-ELPSD-CLNDR-HOURS
        cwf_Who_Activity_File_Actvty_Elpsd_Bsnss_Hours.setValue(0);                                                                                                       //Natural: MOVE 0 TO CWF-WHO-ACTIVITY-FILE.ACTVTY-ELPSD-BSNSS-HOURS
        pnd_Calc_Field_A.setValue(cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme);                                                                                         //Natural: MOVE CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME TO #CALC-FIELD-A
        cwf_Who_Activity_File_Invrt_Strtng_Event.compute(new ComputeParameters(false, cwf_Who_Activity_File_Invrt_Strtng_Event), (new DbsDecimal("999999999999999").subtract(pnd_Calc_Field_A_Pnd_Calc_Field_N))); //Natural: ASSIGN CWF-WHO-ACTIVITY-FILE.INVRT-STRTNG-EVENT := ( 999999999999999 - #CALC-FIELD-N )
        cwf_Who_Activity_File_Dwnld_Updte_Dte_Tme.setValue(pnd_Date_T);                                                                                                   //Natural: MOVE #DATE-T TO CWF-WHO-ACTIVITY-FILE.DWNLD-UPDTE-DTE-TME
        cwf_Who_Activity_File_Rt_Sqnce_Nbr.setValue(0);                                                                                                                   //Natural: MOVE 0 TO CWF-WHO-ACTIVITY-FILE.RT-SQNCE-NBR
        cwf_Who_Activity_File_Off_Rtng_Ind.setValue(" ");                                                                                                                 //Natural: MOVE ' ' TO CWF-WHO-ACTIVITY-FILE.OFF-RTNG-IND
        cwf_Who_Activity_File_Msg_Rtrn_Cde.setValue(" ");                                                                                                                 //Natural: MOVE ' ' TO CWF-WHO-ACTIVITY-FILE.MSG-RTRN-CDE
        cwf_Who_Activity_File_Msg_Nbr.setValue(0);                                                                                                                        //Natural: MOVE 0 TO CWF-WHO-ACTIVITY-FILE.MSG-NBR
        //* *************
        getReports().write(1, NEWLINE,"This Activity was created from WR - RLDT = ",cwf_Who_Work_Request_Rqst_Log_Dte_Tme);                                               //Natural: WRITE ( 1 ) /'This Activity was created from WR - RLDT = ' CWF-WHO-WORK-REQUEST.RQST-LOG-DTE-TME
        if (Global.isEscape()) return;
        vw_cwf_Who_Activity_File.insertDBRow();                                                                                                                           //Natural: STORE CWF-WHO-ACTIVITY-FILE
        GET_WR_STAT1:                                                                                                                                                     //Natural: GET CWF-WHO-WORK-REQUEST #WR-ISN
        vw_cwf_Who_Work_Request.readByID(pnd_Wr_Isn.getLong(), "GET_WR_STAT1");
        cwf_Who_Work_Request_Status_Cde.setValue("8799");                                                                                                                 //Natural: ASSIGN CWF-WHO-WORK-REQUEST.STATUS-CDE := '8799'
        pnd_Wr_Updated.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #WR-UPDATED
        vw_cwf_Who_Work_Request.updateDBRow("GET_WR_STAT1");                                                                                                              //Natural: UPDATE ( GET-WR-STAT1. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* *************
    }
    private void sub_Build_Clse_Act_Frm_Mit() throws Exception                                                                                                            //Natural: BUILD-CLSE-ACT-FRM-MIT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Act_Frm_Mit_Cntr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #ACT-FRM-MIT-CNTR
        vw_cwf_Who_Activity_File.reset();                                                                                                                                 //Natural: RESET CWF-WHO-ACTIVITY-FILE
        cwf_Who_Activity_File_Rqst_Log_Dte_Tme.setValue(cwf_Master_Rqst_Log_Dte_Tme);                                                                                     //Natural: MOVE CWF-MASTER.RQST-LOG-DTE-TME TO CWF-WHO-ACTIVITY-FILE.RQST-LOG-DTE-TME
        cwf_Who_Activity_File_Strtng_Event_Dte_Tme.setValue(cwf_Master_Status_Updte_Dte_Tme);                                                                             //Natural: MOVE CWF-MASTER.STATUS-UPDTE-DTE-TME TO CWF-WHO-ACTIVITY-FILE.STRTNG-EVENT-DTE-TME
        cwf_Who_Activity_File_Endng_Event_Dte_Tme.setValue(cwf_Master_Status_Updte_Dte_Tme);                                                                              //Natural: MOVE CWF-MASTER.STATUS-UPDTE-DTE-TME TO CWF-WHO-ACTIVITY-FILE.ENDNG-EVENT-DTE-TME
        cwf_Who_Activity_File_Cmpltd_Actvty_Ind.setValue("Y");                                                                                                            //Natural: MOVE 'Y' TO CWF-WHO-ACTIVITY-FILE.CMPLTD-ACTVTY-IND
        cwf_Who_Activity_File_Admin_Unit_Cde.setValue(cwf_Master_Unit_Cde);                                                                                               //Natural: MOVE CWF-MASTER.UNIT-CDE TO CWF-WHO-ACTIVITY-FILE.ADMIN-UNIT-CDE
        cwf_Who_Activity_File_Empl_Oprtr_Cde.setValue(" ");                                                                                                               //Natural: MOVE ' ' TO CWF-WHO-ACTIVITY-FILE.EMPL-OPRTR-CDE
        cwf_Who_Activity_File_Step_Id.setValue(" ");                                                                                                                      //Natural: MOVE ' ' TO CWF-WHO-ACTIVITY-FILE.STEP-ID
        cwf_Who_Activity_File_Admin_Status_Cde.setValue(cwf_Master_Status_Cde);                                                                                           //Natural: MOVE CWF-MASTER.STATUS-CDE TO CWF-WHO-ACTIVITY-FILE.ADMIN-STATUS-CDE
        cwf_Who_Activity_File_Unit_On_Tme_Ind.setValue(" ");                                                                                                              //Natural: MOVE ' ' TO CWF-WHO-ACTIVITY-FILE.UNIT-ON-TME-IND
        cwf_Who_Activity_File_Crtn_Run_Id.setValue(0);                                                                                                                    //Natural: MOVE 0 TO CWF-WHO-ACTIVITY-FILE.CRTN-RUN-ID
        cwf_Who_Activity_File_Actvty_Elpsd_Clndr_Hours.setValue(0);                                                                                                       //Natural: MOVE 0 TO CWF-WHO-ACTIVITY-FILE.ACTVTY-ELPSD-CLNDR-HOURS
        cwf_Who_Activity_File_Actvty_Elpsd_Bsnss_Hours.setValue(0);                                                                                                       //Natural: MOVE 0 TO CWF-WHO-ACTIVITY-FILE.ACTVTY-ELPSD-BSNSS-HOURS
        pnd_Calc_Field_A.setValueEdited(cwf_Master_Status_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                           //Natural: MOVE EDITED CWF-MASTER.STATUS-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #CALC-FIELD-A
        cwf_Who_Activity_File_Invrt_Strtng_Event.compute(new ComputeParameters(false, cwf_Who_Activity_File_Invrt_Strtng_Event), (new DbsDecimal("999999999999999").subtract(pnd_Calc_Field_A_Pnd_Calc_Field_N))); //Natural: ASSIGN CWF-WHO-ACTIVITY-FILE.INVRT-STRTNG-EVENT := ( 999999999999999 - #CALC-FIELD-N )
        cwf_Who_Activity_File_Dwnld_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                             //Natural: MOVE *TIMX TO CWF-WHO-ACTIVITY-FILE.DWNLD-UPDTE-DTE-TME
        cwf_Who_Activity_File_Rt_Sqnce_Nbr.setValue(cwf_Master_Rt_Sqnce_Nbr);                                                                                             //Natural: MOVE CWF-MASTER.RT-SQNCE-NBR TO CWF-WHO-ACTIVITY-FILE.RT-SQNCE-NBR
        cwf_Who_Activity_File_Step_Id.setValue(cwf_Master_Step_Id);                                                                                                       //Natural: MOVE CWF-MASTER.STEP-ID TO CWF-WHO-ACTIVITY-FILE.STEP-ID
        cwf_Who_Activity_File_Off_Rtng_Ind.setValue(" ");                                                                                                                 //Natural: MOVE ' ' TO CWF-WHO-ACTIVITY-FILE.OFF-RTNG-IND
        cwf_Who_Activity_File_Msg_Rtrn_Cde.setValue(" ");                                                                                                                 //Natural: MOVE ' ' TO CWF-WHO-ACTIVITY-FILE.MSG-RTRN-CDE
        cwf_Who_Activity_File_Msg_Nbr.setValue(0);                                                                                                                        //Natural: MOVE 0 TO CWF-WHO-ACTIVITY-FILE.MSG-NBR
        //* *************
        getReports().write(1, NEWLINE,"This Activity was created from MIT - RLDT = ",cwf_Who_Work_Request_Rqst_Log_Dte_Tme);                                              //Natural: WRITE ( 1 ) /'This Activity was created from MIT - RLDT = ' CWF-WHO-WORK-REQUEST.RQST-LOG-DTE-TME
        if (Global.isEscape()) return;
        vw_cwf_Who_Activity_File.insertDBRow();                                                                                                                           //Natural: STORE CWF-WHO-ACTIVITY-FILE
        GET_WR_STAT2:                                                                                                                                                     //Natural: GET CWF-WHO-WORK-REQUEST #WR-ISN
        vw_cwf_Who_Work_Request.readByID(pnd_Wr_Isn.getLong(), "GET_WR_STAT2");
        cwf_Who_Work_Request_Status_Cde.setValue(cwf_Master_Status_Cde);                                                                                                  //Natural: ASSIGN CWF-WHO-WORK-REQUEST.STATUS-CDE := CWF-MASTER.STATUS-CDE
        pnd_Wr_Updated.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #WR-UPDATED
        vw_cwf_Who_Work_Request.updateDBRow("GET_WR_STAT2");                                                                                                              //Natural: UPDATE ( GET-WR-STAT2. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* *************
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=56 LS=131");
    }
}
