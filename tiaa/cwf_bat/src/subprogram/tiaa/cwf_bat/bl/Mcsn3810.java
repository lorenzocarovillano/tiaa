/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:25:47 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3810
************************************************************
**        * FILE NAME            : Mcsn3810.java
**        * CLASS NAME           : Mcsn3810
**        * INSTANCE NAME        : Mcsn3810
************************************************************
************************************************************************
* PROGRAM    : MCSN3810
* FUNCTION   : GIVEN A DATE PARAMETER - PROGRAM RETURNS THE NEXT
*            : PROCESSING DATE.  (PROCESSING DATE IS EVERY DAY EXCEPT
*            : SATURDAY, SUNDAY AND HOLIDAYS)
*            : MODULE USED BY IMAGE UPLOAD AUDIT REPORTS PROGRAMS
* WRITTEN    : 08/12/96
*            :
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3810 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Year;

    private DbsGroup pnd_Year__R_Field_1;
    private DbsField pnd_Year_Pnd_Year_N;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_2;
    private DbsField pnd_Date_A_Pnd_Date_Year;
    private DbsField pnd_Date;
    private DbsField pnd_Name_Of_Day;
    private DbsField pnd_Day_Ctr;
    private DbsField pnd_Holiday_Dates;
    private DbsField pnd_Comp_Processing_Date;
    private DbsField pnd_Next_Year;

    private DbsGroup pnd_Next_Year__R_Field_3;
    private DbsField pnd_Next_Year_Pnd_Next_Year_N;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Start_Date = parameters.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.DATE);
        pnd_Start_Date.setParameterOption(ParameterOption.ByReference);
        pnd_Next_Processing_Date = parameters.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Next_Processing_Date.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.STRING, 4);

        pnd_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_Year__R_Field_1", "REDEFINE", pnd_Year);
        pnd_Year_Pnd_Year_N = pnd_Year__R_Field_1.newFieldInGroup("pnd_Year_Pnd_Year_N", "#YEAR-N", FieldType.NUMERIC, 4);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_2", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_Year = pnd_Date_A__R_Field_2.newFieldInGroup("pnd_Date_A_Pnd_Date_Year", "#DATE-YEAR", FieldType.NUMERIC, 4);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Name_Of_Day = localVariables.newFieldInRecord("pnd_Name_Of_Day", "#NAME-OF-DAY", FieldType.STRING, 3);
        pnd_Day_Ctr = localVariables.newFieldInRecord("pnd_Day_Ctr", "#DAY-CTR", FieldType.PACKED_DECIMAL, 2);
        pnd_Holiday_Dates = localVariables.newFieldArrayInRecord("pnd_Holiday_Dates", "#HOLIDAY-DATES", FieldType.DATE, new DbsArrayController(1, 7));
        pnd_Comp_Processing_Date = localVariables.newFieldInRecord("pnd_Comp_Processing_Date", "#COMP-PROCESSING-DATE", FieldType.DATE);
        pnd_Next_Year = localVariables.newFieldInRecord("pnd_Next_Year", "#NEXT-YEAR", FieldType.STRING, 4);

        pnd_Next_Year__R_Field_3 = localVariables.newGroupInRecord("pnd_Next_Year__R_Field_3", "REDEFINE", pnd_Next_Year);
        pnd_Next_Year_Pnd_Next_Year_N = pnd_Next_Year__R_Field_3.newFieldInGroup("pnd_Next_Year_Pnd_Next_Year_N", "#NEXT-YEAR-N", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Mcsn3810() throws Exception
    {
        super("Mcsn3810");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Comp_Processing_Date.setValue(pnd_Start_Date);                                                                                                                //Natural: ASSIGN #COMP-PROCESSING-DATE := #START-DATE
        pnd_Date_A.setValueEdited(pnd_Comp_Processing_Date,new ReportEditMask("YYYYMMDD"));                                                                               //Natural: MOVE EDITED #COMP-PROCESSING-DATE ( EM = YYYYMMDD ) TO #DATE-A
        pnd_Year_Pnd_Year_N.setValue(pnd_Date_A_Pnd_Date_Year);                                                                                                           //Natural: ASSIGN #YEAR-N := #DATE-YEAR
        //*  THESE SUBROUTINES WILL RETURN HOLIDAY DATES OBSERVED AT TIAA-CREF.
                                                                                                                                                                          //Natural: PERFORM NEW-YEARS-DAY
        sub_New_Years_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRESIDENTS-DAY
        sub_Presidents_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MEMORIAL-DAY
        sub_Memorial_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM INDEPENDENCE-DAY
        sub_Independence_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LABOR-DAY
        sub_Labor_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM THANKSGIVING-DAY
        sub_Thanksgiving_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CHRISTMAS-DAY
        sub_Christmas_Day();
        if (condition(Global.isEscape())) {return;}
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Next_Processing_Date.greater(getZero()))) {break;}                                                                                          //Natural: UNTIL #NEXT-PROCESSING-DATE GT 0
            pnd_Comp_Processing_Date.nadd(1);                                                                                                                             //Natural: ADD 1 TO #COMP-PROCESSING-DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Comp_Processing_Date,new ReportEditMask("NNN"));                                                                           //Natural: MOVE EDITED #COMP-PROCESSING-DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Sun") || pnd_Comp_Processing_Date.equals(pnd_Holiday_Dates.getValue("*"))))                                             //Natural: IF #NAME-OF-DAY = 'Sun' OR #COMP-PROCESSING-DATE = #HOLIDAY-DATES ( * )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Next_Processing_Date.setValue(pnd_Comp_Processing_Date);                                                                                              //Natural: ASSIGN #NEXT-PROCESSING-DATE := #COMP-PROCESSING-DATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEW-YEARS-DAY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRESIDENTS-DAY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MEMORIAL-DAY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INDEPENDENCE-DAY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LABOR-DAY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: THANKSGIVING-DAY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHRISTMAS-DAY
        //*  ----------------------------------------------------------------------
    }
    private void sub_New_Years_Day() throws Exception                                                                                                                     //Natural: NEW-YEARS-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  NEW YEAR 01/01/YYYY
        //*  IF IT FALLS ON A SUN, THE NEXT WORKING DAY IS A HOLIDAY.
        pnd_Next_Year_Pnd_Next_Year_N.compute(new ComputeParameters(false, pnd_Next_Year_Pnd_Next_Year_N), pnd_Year_Pnd_Year_N.add(1));                                   //Natural: COMPUTE #NEXT-YEAR-N = #YEAR-N + 1
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Next_Year, "0101"));                                                                      //Natural: COMPRESS #NEXT-YEAR '0101' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        short decideConditionsMet98 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE OF #NAME-OF-DAY;//Natural: VALUE 'Sun'
        if (condition((pnd_Name_Of_Day.equals("Sun"))))
        {
            decideConditionsMet98++;
            pnd_Date.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(! (pnd_Name_Of_Day.equals("Sun"))))                                                                                                                 //Natural: IF NOT ( #NAME-OF-DAY = 'Sun' )
        {
            pnd_Holiday_Dates.getValue(1).setValue(pnd_Date);                                                                                                             //Natural: ASSIGN #HOLIDAY-DATES ( 1 ) := #DATE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Presidents_Day() throws Exception                                                                                                                    //Natural: PRESIDENTS-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  PRESIDENT's Day 3rd Monday in February
        pnd_Day_Ctr.reset();                                                                                                                                              //Natural: RESET #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0201"));                                                                           //Natural: COMPRESS #YEAR '0201' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Mon")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Mon'
            {
                pnd_Day_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DAY-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.less(3)))                                                                                                                           //Natural: IF #DAY-CTR LT 3
            {
                pnd_Date.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(3))) {break;}                                                                                                                //Natural: UNTIL #DAY-CTR = 3
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Holiday_Dates.getValue(2).setValue(pnd_Date);                                                                                                                 //Natural: ASSIGN #HOLIDAY-DATES ( 2 ) := #DATE
    }
    private void sub_Memorial_Day() throws Exception                                                                                                                      //Natural: MEMORIAL-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MEMORIAL DAY - LAST MONDAY IN MAY
        pnd_Day_Ctr.setValue(0);                                                                                                                                          //Natural: MOVE 0 TO #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0531"));                                                                           //Natural: COMPRESS #YEAR '0531' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT03:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Mon")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Mon'
            {
                pnd_Day_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DAY-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(getZero())))                                                                                                                 //Natural: IF #DAY-CTR = 0
            {
                pnd_Date.nsubtract(1);                                                                                                                                    //Natural: SUBTRACT 1 FROM #DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(1))) {break;}                                                                                                                //Natural: UNTIL #DAY-CTR = 1
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Holiday_Dates.getValue(3).setValue(pnd_Date);                                                                                                                 //Natural: ASSIGN #HOLIDAY-DATES ( 3 ) := #DATE
    }
    private void sub_Independence_Day() throws Exception                                                                                                                  //Natural: INDEPENDENCE-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INDEPENDENCE DAY 07/04/YYYY
        //*  IF IT FALLS ON A SAT, THE PREVIOUS WORKING DAY IS A HOLIDAY.
        //*  IF IT FALLS ON A SUN, THE NEXT WORKING DAY IS A HOLIDAY.
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0704"));                                                                           //Natural: COMPRESS #YEAR '0704' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        short decideConditionsMet162 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #NAME-OF-DAY;//Natural: VALUE 'Sat'
        if (condition((pnd_Name_Of_Day.equals("Sat"))))
        {
            decideConditionsMet162++;
            pnd_Date.nsubtract(1);                                                                                                                                        //Natural: SUBTRACT 1 FROM #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: VALUE 'Sun'
        else if (condition((pnd_Name_Of_Day.equals("Sun"))))
        {
            decideConditionsMet162++;
            pnd_Date.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(! (pnd_Name_Of_Day.equals("Sat") || pnd_Name_Of_Day.equals("Sun"))))                                                                                //Natural: IF NOT ( #NAME-OF-DAY = 'Sat' OR = 'Sun' )
        {
            pnd_Holiday_Dates.getValue(4).setValue(pnd_Date);                                                                                                             //Natural: ASSIGN #HOLIDAY-DATES ( 4 ) := #DATE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Labor_Day() throws Exception                                                                                                                         //Natural: LABOR-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  LABOR DAY       1ST MONDAY IN SEPTEMBER
        pnd_Day_Ctr.reset();                                                                                                                                              //Natural: RESET #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0901"));                                                                           //Natural: COMPRESS #YEAR '0901' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT04:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Mon")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Mon'
            {
                pnd_Day_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DAY-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(getZero())))                                                                                                                 //Natural: IF #DAY-CTR = 0
            {
                pnd_Date.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(1))) {break;}                                                                                                                //Natural: UNTIL #DAY-CTR = 1
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Holiday_Dates.getValue(5).setValue(pnd_Date);                                                                                                                 //Natural: ASSIGN #HOLIDAY-DATES ( 5 ) := #DATE
    }
    private void sub_Thanksgiving_Day() throws Exception                                                                                                                  //Natural: THANKSGIVING-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  THANKSGIVING IS THE 4TH THURSDAY IN NOVEMBER
        pnd_Day_Ctr.reset();                                                                                                                                              //Natural: RESET #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "1101"));                                                                           //Natural: COMPRESS #YEAR '1101' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT05:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Thu")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Thu'
            {
                pnd_Day_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DAY-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.less(4)))                                                                                                                           //Natural: IF #DAY-CTR LT 4
            {
                pnd_Date.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(4))) {break;}                                                                                                                //Natural: UNTIL #DAY-CTR = 4
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Holiday_Dates.getValue(6).setValue(pnd_Date);                                                                                                                 //Natural: ASSIGN #HOLIDAY-DATES ( 6 ) := #DATE
    }
    private void sub_Christmas_Day() throws Exception                                                                                                                     //Natural: CHRISTMAS-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CHRISTMAS DAY    12/25/YYYY
        //*  IF IT FALLS ON A SAT, THE PREVIOUS WORKING DAY IS A HOLIDAY.
        //*  IF IT FALLS ON A SUN, THE NEXT WORKING DAY IS A HOLIDAY.
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "1225"));                                                                           //Natural: COMPRESS #YEAR '1225' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        short decideConditionsMet229 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #NAME-OF-DAY;//Natural: VALUE 'Sat'
        if (condition((pnd_Name_Of_Day.equals("Sat"))))
        {
            decideConditionsMet229++;
            pnd_Date.nsubtract(1);                                                                                                                                        //Natural: SUBTRACT 1 FROM #DATE
        }                                                                                                                                                                 //Natural: VALUE 'Sun'
        else if (condition((pnd_Name_Of_Day.equals("Sun"))))
        {
            decideConditionsMet229++;
            pnd_Date.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DATE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(! (pnd_Name_Of_Day.equals("Sat") || pnd_Name_Of_Day.equals("Sun"))))                                                                                //Natural: IF NOT ( #NAME-OF-DAY = 'Sat' OR = 'Sun' )
        {
            pnd_Holiday_Dates.getValue(7).setValue(pnd_Date);                                                                                                             //Natural: ASSIGN #HOLIDAY-DATES ( 7 ) := #DATE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
