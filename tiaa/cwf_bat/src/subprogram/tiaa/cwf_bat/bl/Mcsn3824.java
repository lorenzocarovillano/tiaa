/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:26:13 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3824
************************************************************
**        * FILE NAME            : Mcsn3824.java
**        * CLASS NAME           : Mcsn3824
**        * INSTANCE NAME        : Mcsn3824
************************************************************
************************************************************************
* PROGRAM  : MCSN3824
* TITLE    : ACCESS FULFILLMENT RECORDS FROM THE CWF-ATI-FULFILLMENT
* FUNCTION : DISPLAY ATI-FULFILLMENT RECORDS ON 192 IN WPID ORDER
*          : FOR POST SYSTEM "NOT PROCESSED"
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3824 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;

    private DataAccessProgramView vw_cwf_Ati_Fulfill;
    private DbsField cwf_Ati_Fulfill_Ati_Rcrd_Type;

    private DbsGroup cwf_Ati_Fulfill_Ati_Rqst_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Pin;
    private DbsField cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Stts;
    private DbsField cwf_Ati_Fulfill_Ati_Wpid;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Racf_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Unit_Cde;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Error_Txt;
    private DbsField cwf_Ati_Fulfill_Ati_Prge_Ind;
    private DbsField cwf_Ati_Fulfill_Ati_Stts_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Cmtask_Ind;
    private DbsField pnd_Ati_Mit_Key;

    private DbsGroup pnd_Ati_Mit_Key__R_Field_1;
    private DbsField pnd_Ati_Mit_Key_Pnd_Ati_Pin;
    private DbsField pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme;
    private DbsField pnd_Ati_Fulfill_Key;

    private DbsGroup pnd_Ati_Fulfill_Key__R_Field_2;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts;
    private DbsField pnd_Grand_Total_Open_Atis;
    private DbsField pnd_Grand_Total_Inprogress_Atis;
    private DbsField pnd_Grand_Total_Failed_Atis;
    private DbsField pnd_Grand_Total_Succesfull_Atis;
    private DbsField pnd_Tot_Ok_Ic_System;
    private DbsField pnd_Tot_Ok_Pss_System;
    private DbsField pnd_Tot_Ok_Icss_System;
    private DbsField pnd_Tot_Ok_Mcsg_System;
    private DbsField pnd_Tot_Ok_Mfta_Unit;
    private DbsField pnd_Tot_Ok_Non_Mfta_Unit;
    private DbsField pnd_Interim_Acct_Name;
    private DbsField pnd_Process_Status_Literal;
    private DbsField pnd_Cmtask_Status_Literal;
    private DbsField pnd_Compressed_Text;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_Cc;
    private DbsField pnd_Prev_Wpid;
    private DbsField pnd_Wpid_Total;
    private DbsField pnd_Fld_Line;
    private DbsField pnd_Fld_Strt;
    private DbsField pnd_Fld_End;
    private DbsField pnd_Accnt_Amt_Type;
    private DbsField pnd_Field;
    private DbsField pnd_Title_Literal;
    private DbsField pnd_Input_Dte_Tme;
    private DbsField pnd_Input_Dte_D;
    private DbsField pnd_Orig_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Entry_Date;
    private DbsField pnd_Last_Page_Flag;
    private DbsField pnd_Mit_Log_Date;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Input_Data = parameters.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Ati_Fulfill = new DataAccessProgramView(new NameInfo("vw_cwf_Ati_Fulfill", "CWF-ATI-FULFILL"), "CWF_ATI_FULFILL", "CWF_KDO_FULFILL");
        cwf_Ati_Fulfill_Ati_Rcrd_Type = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Rcrd_Type", "ATI-RCRD-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_RCRD_TYPE");

        cwf_Ati_Fulfill_Ati_Rqst_Id = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ATI_RQST_ID", "ATI-RQST-ID");
        cwf_Ati_Fulfill_Ati_Pin = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Pin", "ATI-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ATI_PIN");
        cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme", "ATI-MIT-LOG-DT-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_MIT_LOG_DT_TME");
        cwf_Ati_Fulfill_Ati_Prcss_Stts = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Stts", "ATI-PRCSS-STTS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRCSS_STTS");
        cwf_Ati_Fulfill_Ati_Wpid = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Wpid", "ATI-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ATI_WPID");
        cwf_Ati_Fulfill_Ati_Entry_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Dte_Tme", "ATI-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_ENTRY_DTE_TME");
        cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt", "ATI-ENTRY-SYSTM-OR-UNT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_SYSTM_OR_UNT");
        cwf_Ati_Fulfill_Ati_Entry_Racf_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Racf_Id", "ATI-ENTRY-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ATI_ENTRY_RACF_ID");
        cwf_Ati_Fulfill_Ati_Entry_Unit_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Unit_Cde", "ATI-ENTRY-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_UNIT_CDE");
        cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id", "ATI-PRCSS-APPLCTN-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_PRCSS_APPLCTN_ID");
        cwf_Ati_Fulfill_Ati_Error_Txt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Error_Txt", "ATI-ERROR-TXT", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "ATI_ERROR_TXT");
        cwf_Ati_Fulfill_Ati_Prge_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prge_Ind", "ATI-PRGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRGE_IND");
        cwf_Ati_Fulfill_Ati_Stts_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Stts_Dte_Tme", "ATI-STTS-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_STTS_DTE_TME");
        cwf_Ati_Fulfill_Ati_Cmtask_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Cmtask_Ind", "ATI-CMTASK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_CMTASK_IND");
        registerRecord(vw_cwf_Ati_Fulfill);

        pnd_Ati_Mit_Key = localVariables.newFieldInRecord("pnd_Ati_Mit_Key", "#ATI-MIT-KEY", FieldType.BINARY, 14);

        pnd_Ati_Mit_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Ati_Mit_Key__R_Field_1", "REDEFINE", pnd_Ati_Mit_Key);
        pnd_Ati_Mit_Key_Pnd_Ati_Pin = pnd_Ati_Mit_Key__R_Field_1.newFieldInGroup("pnd_Ati_Mit_Key_Pnd_Ati_Pin", "#ATI-PIN", FieldType.NUMERIC, 7);
        pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme = pnd_Ati_Mit_Key__R_Field_1.newFieldInGroup("pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme", "#ATI-LOG-DTE-TME", FieldType.TIME);
        pnd_Ati_Fulfill_Key = localVariables.newFieldInRecord("pnd_Ati_Fulfill_Key", "#ATI-FULFILL-KEY", FieldType.STRING, 9);

        pnd_Ati_Fulfill_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Ati_Fulfill_Key__R_Field_2", "REDEFINE", pnd_Ati_Fulfill_Key);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl = pnd_Ati_Fulfill_Key__R_Field_2.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl", "#ATI-RCRD-TYPE-CNTRL", 
            FieldType.STRING, 1);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme = pnd_Ati_Fulfill_Key__R_Field_2.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme", "#ATI-ENTRY-DTE-TME", 
            FieldType.TIME);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts = pnd_Ati_Fulfill_Key__R_Field_2.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts", "#ATI-PRCSS-STTS", 
            FieldType.STRING, 1);
        pnd_Grand_Total_Open_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Open_Atis", "#GRAND-TOTAL-OPEN-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Inprogress_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Inprogress_Atis", "#GRAND-TOTAL-INPROGRESS-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Grand_Total_Failed_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Failed_Atis", "#GRAND-TOTAL-FAILED-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Succesfull_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Succesfull_Atis", "#GRAND-TOTAL-SUCCESFULL-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Tot_Ok_Ic_System = localVariables.newFieldInRecord("pnd_Tot_Ok_Ic_System", "#TOT-OK-IC-SYSTEM", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Pss_System = localVariables.newFieldInRecord("pnd_Tot_Ok_Pss_System", "#TOT-OK-PSS-SYSTEM", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Icss_System = localVariables.newFieldInRecord("pnd_Tot_Ok_Icss_System", "#TOT-OK-ICSS-SYSTEM", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Mcsg_System = localVariables.newFieldInRecord("pnd_Tot_Ok_Mcsg_System", "#TOT-OK-MCSG-SYSTEM", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Mfta_Unit = localVariables.newFieldInRecord("pnd_Tot_Ok_Mfta_Unit", "#TOT-OK-MFTA-UNIT", FieldType.NUMERIC, 5);
        pnd_Tot_Ok_Non_Mfta_Unit = localVariables.newFieldInRecord("pnd_Tot_Ok_Non_Mfta_Unit", "#TOT-OK-NON-MFTA-UNIT", FieldType.NUMERIC, 5);
        pnd_Interim_Acct_Name = localVariables.newFieldInRecord("pnd_Interim_Acct_Name", "#INTERIM-ACCT-NAME", FieldType.STRING, 11);
        pnd_Process_Status_Literal = localVariables.newFieldInRecord("pnd_Process_Status_Literal", "#PROCESS-STATUS-LITERAL", FieldType.STRING, 7);
        pnd_Cmtask_Status_Literal = localVariables.newFieldInRecord("pnd_Cmtask_Status_Literal", "#CMTASK-STATUS-LITERAL", FieldType.STRING, 27);
        pnd_Compressed_Text = localVariables.newFieldInRecord("pnd_Compressed_Text", "#COMPRESSED-TEXT", FieldType.STRING, 38);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 3);
        pnd_Cc = localVariables.newFieldInRecord("pnd_Cc", "#CC", FieldType.NUMERIC, 3);
        pnd_Prev_Wpid = localVariables.newFieldInRecord("pnd_Prev_Wpid", "#PREV-WPID", FieldType.STRING, 6);
        pnd_Wpid_Total = localVariables.newFieldInRecord("pnd_Wpid_Total", "#WPID-TOTAL", FieldType.NUMERIC, 8);
        pnd_Fld_Line = localVariables.newFieldInRecord("pnd_Fld_Line", "#FLD-LINE", FieldType.NUMERIC, 3);
        pnd_Fld_Strt = localVariables.newFieldInRecord("pnd_Fld_Strt", "#FLD-STRT", FieldType.NUMERIC, 3);
        pnd_Fld_End = localVariables.newFieldInRecord("pnd_Fld_End", "#FLD-END", FieldType.NUMERIC, 3);
        pnd_Accnt_Amt_Type = localVariables.newFieldInRecord("pnd_Accnt_Amt_Type", "#ACCNT-AMT-TYPE", FieldType.STRING, 1);
        pnd_Field = localVariables.newFieldInRecord("pnd_Field", "#FIELD", FieldType.STRING, 30);
        pnd_Title_Literal = localVariables.newFieldInRecord("pnd_Title_Literal", "#TITLE-LITERAL", FieldType.STRING, 54);
        pnd_Input_Dte_Tme = localVariables.newFieldInRecord("pnd_Input_Dte_Tme", "#INPUT-DTE-TME", FieldType.TIME);
        pnd_Input_Dte_D = localVariables.newFieldInRecord("pnd_Input_Dte_D", "#INPUT-DTE-D", FieldType.DATE);
        pnd_Orig_Date = localVariables.newFieldInRecord("pnd_Orig_Date", "#ORIG-DATE", FieldType.DATE);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 8);
        pnd_Entry_Date = localVariables.newFieldInRecord("pnd_Entry_Date", "#ENTRY-DATE", FieldType.DATE);
        pnd_Last_Page_Flag = localVariables.newFieldInRecord("pnd_Last_Page_Flag", "#LAST-PAGE-FLAG", FieldType.STRING, 1);
        pnd_Mit_Log_Date = localVariables.newFieldInRecord("pnd_Mit_Log_Date", "#MIT-LOG-DATE", FieldType.NUMERIC, 14);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Ati_Fulfill.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Mcsn3824() throws Exception
    {
        super("Mcsn3824");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt10, 10);
        getReports().atEndOfPage(atEndEventRpt10, 10);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 10 ) PS = 60 LS = 133 ZP = OFF
        pnd_Last_Page_Flag.reset();                                                                                                                                       //Natural: RESET #LAST-PAGE-FLAG #GRAND-TOTAL-OPEN-ATIS #GRAND-TOTAL-INPROGRESS-ATIS #GRAND-TOTAL-FAILED-ATIS #GRAND-TOTAL-SUCCESFULL-ATIS #TOT-OK-IC-SYSTEM #TOT-OK-PSS-SYSTEM #TOT-OK-ICSS-SYSTEM #TOT-OK-MCSG-SYSTEM #TOT-OK-MFTA-UNIT #TOT-OK-NON-MFTA-UNIT
        pnd_Grand_Total_Open_Atis.reset();
        pnd_Grand_Total_Inprogress_Atis.reset();
        pnd_Grand_Total_Failed_Atis.reset();
        pnd_Grand_Total_Succesfull_Atis.reset();
        pnd_Tot_Ok_Ic_System.reset();
        pnd_Tot_Ok_Pss_System.reset();
        pnd_Tot_Ok_Icss_System.reset();
        pnd_Tot_Ok_Mcsg_System.reset();
        pnd_Tot_Ok_Mfta_Unit.reset();
        pnd_Tot_Ok_Non_Mfta_Unit.reset();
        if (condition(pnd_Input_Data_Pnd_Input_Date.notEquals(" ")))                                                                                                      //Natural: IF #INPUT-DATE NE ' '
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Input_Dte_Tme.setValue(pnd_Input_Dte_D);                                                                                                                  //Natural: MOVE #INPUT-DTE-D TO #INPUT-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(10, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***");                                    //Natural: WRITE ( 10 ) /// '*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***'
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
        //* *** INITIALIZE THE ATI-CONTROL-KEY SUPER-DESCRIPTOR FIELD
        pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl.setValue("3");                                                                                                        //Natural: ASSIGN #ATI-RCRD-TYPE-CNTRL := '3'
        pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme.reset();                                                                                                                //Natural: RESET #ATI-ENTRY-DTE-TME
        //* *****************************************************************
        //* ** READ CWF-ATI-FULFILL AND PROCESS OPEN AND IN-PROGRESS REQUESTS
        //* *****************************************************************
        getReports().newPage(new ReportSpecification(10));                                                                                                                //Natural: NEWPAGE ( 10 )
        if (condition(Global.isEscape())){return;}
        vw_cwf_Ati_Fulfill.startDatabaseRead                                                                                                                              //Natural: READ CWF-ATI-FULFILL BY ATI-FULFILL-KEY STARTING FROM #ATI-FULFILL-KEY
        (
        "READ_ATI",
        new Wc[] { new Wc("ATI_FULFILL_KEY", ">=", pnd_Ati_Fulfill_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("ATI_FULFILL_KEY", "ASC") }
        );
        READ_ATI:
        while (condition(vw_cwf_Ati_Fulfill.readNextRow("READ_ATI")))
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Entry_Date.setValue(cwf_Ati_Fulfill_Ati_Stts_Dte_Tme);                                                                                                    //Natural: MOVE CWF-ATI-FULFILL.ATI-STTS-DTE-TME TO #ENTRY-DATE
            if (condition(!(pnd_Entry_Date.equals(pnd_Input_Dte_D) && cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id.equals("IOP") && cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("O")))) //Natural: ACCEPT IF #ENTRY-DATE = #INPUT-DTE-D AND CWF-ATI-FULFILL.ATI-PRCSS-APPLCTN-ID = 'IOP' AND CWF-ATI-FULFILL.ATI-PRCSS-STTS = 'O'
            {
                continue;
            }
            getSort().writeSortInData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type,               //Natural: END-ALL
                cwf_Ati_Fulfill_Ati_Prcss_Stts, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, 
                cwf_Ati_Fulfill_Ati_Entry_Racf_Id, cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  SORT PERFORMED TO WRITE THEM PIN/MIT-LOG-DATE-TIME/REC-TYPE ORDER
        getSort().sortData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type, cwf_Ati_Fulfill_Ati_Prcss_Stts); //Natural: SORT RECORDS BY CWF-ATI-FULFILL.ATI-WPID CWF-ATI-FULFILL.ATI-PIN CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME CWF-ATI-FULFILL.ATI-RCRD-TYPE CWF-ATI-FULFILL.ATI-PRCSS-STTS USING CWF-ATI-FULFILL.ATI-ERROR-TXT CWF-ATI-FULFILL.ATI-ENTRY-DTE-TME CWF-ATI-FULFILL.ATI-ENTRY-SYSTM-OR-UNT CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE CWF-ATI-FULFILL.ATI-STTS-DTE-TME
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type, 
            cwf_Ati_Fulfill_Ati_Prcss_Stts, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, cwf_Ati_Fulfill_Ati_Entry_Racf_Id, 
            cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF CWF-ATI-FULFILL.ATI-WPID
            pnd_Prev_Wpid.setValue(cwf_Ati_Fulfill_Ati_Wpid);                                                                                                             //Natural: ASSIGN #PREV-WPID := CWF-ATI-FULFILL.ATI-WPID
            pnd_Wpid_Total.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WPID-TOTAL
                                                                                                                                                                          //Natural: PERFORM ATI-WRITE-REC-1
            sub_Ati_Write_Rec_1();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  END PROCESSING OF OPEN AND IN-PROGRESS REQUEST
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //*  ----------------------------------------------------------------------
        //*  4T CWF-ATI-FULFILL.ATI-PIN
        //*  20T CWF-ATI-FULFILL.ATI-WPID
        //*  27T CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  48T CWF-ATI-FULFILL.ATI-STTS-DTE-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 10 )
        //*                                                                                                                                                               //Natural: AT END OF PAGE ( 10 )
        //* ***********************************************************************
        //*   WRITE OUT ATI GRAND TOTALS ON LAST PAGE OF REPORT
        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(10));                                                                                                                //Natural: NEWPAGE ( 10 )
        if (condition(Global.isEscape())){return;}
        pnd_Last_Page_Flag.setValue("Y");                                                                                                                                 //Natural: ASSIGN #LAST-PAGE-FLAG := 'Y'
        getReports().write(10, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,new  //Natural: WRITE ( 10 ) //////// 1T '-' ( 131 ) // 1T '-' ( 50 ) 52T 'INCOME OPTION PROFILE REQUEST OPEN TOTALS' 95T '-' ( 35 ) // 1T '-' ( 131 ) //// // 45T 'OPEN IOPS ATIS FROM PSS  :' #TOT-OK-PSS-SYSTEM / 45T '------------------------------' // 45T 'TOTAL OPEN IOPS ATIS     :' #GRAND-TOTAL-OPEN-ATIS
            TabSetting(1),"-",new RepeatItem(50),new TabSetting(52),"INCOME OPTION PROFILE REQUEST OPEN TOTALS",new TabSetting(95),"-",new RepeatItem(35),NEWLINE,NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"OPEN IOPS ATIS FROM PSS  :",pnd_Tot_Ok_Pss_System,NEWLINE,new 
            TabSetting(45),"------------------------------",NEWLINE,NEWLINE,new TabSetting(45),"TOTAL OPEN IOPS ATIS     :",pnd_Grand_Total_Open_Atis);
        if (Global.isEscape()) return;
        //*  / 45T  'OPEN IOPS ATIS FROM IC   :' #TOT-OK-IC-SYSTEM
        //*  // 45T 'OPEN IOPS ATIS FROM MCSG :' #TOT-OK-MCSG-SYSTEM
        //*  // 45T 'OPEN IOPS ATIS FROM ICSS :' #TOT-OK-ICSS-SYSTEM
        //*  85T 'MFTA : ' #TOT-OK-MFTA-UNIT
        //*  101T 'NON-MFTA : ' #TOT-OK-NON-MFTA-UNIT
    }
    //*  START ATI-WRITE-REC-1 PROCESSING
    private void sub_Ati_Write_Rec_1() throws Exception                                                                                                                   //Natural: ATI-WRITE-REC-1
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM SET-PROCESS-STATUS
        sub_Set_Process_Status();
        if (condition(Global.isEscape())) {return;}
        if (condition(getReports().getAstLinesLeft(10).less(15)))                                                                                                         //Natural: NEWPAGE ( 10 ) IF LESS THAN 15 LINES LEFT
        {
            getReports().newPage(10);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Compressed_Text.reset();                                                                                                                                      //Natural: RESET #COMPRESSED-TEXT
        //*  PIN-EXP
        //*   PIN-EXP>>
        pnd_Compressed_Text.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "ENTRY RACF-ID/UNIT= ", cwf_Ati_Fulfill_Ati_Entry_Racf_Id, "/", cwf_Ati_Fulfill_Ati_Entry_Unit_Cde)); //Natural: COMPRESS 'ENTRY RACF-ID/UNIT= ' CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID '/' CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE INTO #COMPRESSED-TEXT LEAVING NO SPACE
        getReports().write(10, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),cwf_Ati_Fulfill_Ati_Pin,new TabSetting(15),cwf_Ati_Fulfill_Ati_Wpid,new                     //Natural: WRITE ( 10 ) / 1T CWF-ATI-FULFILL.ATI-PIN 15T CWF-ATI-FULFILL.ATI-WPID 22T CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 43T CWF-ATI-FULFILL.ATI-STTS-DTE-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 63T #PROCESS-STATUS-LITERAL
            TabSetting(22),cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(43),cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, 
            new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(63),pnd_Process_Status_Literal);
        if (Global.isEscape()) return;
        //*  68T #PROCESS-STATUS-LITERAL               /*  PIN-EXP<<
        if (condition(cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
        {
            getReports().write(10, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text);                                                                          //Natural: WRITE ( 10 ) 15T #COMPRESSED-TEXT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(10, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text,new TabSetting(55),"ERROR MESSAGE = :",new TabSetting(73),                 //Natural: WRITE ( 10 ) 15T #COMPRESSED-TEXT 55T 'ERROR MESSAGE = :' 73T CWF-ATI-FULFILL.ATI-ERROR-TXT
                cwf_Ati_Fulfill_Ati_Error_Txt);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*   END  ATI-WRITE-REC-2 PROCESSING
    }
    private void sub_Set_Process_Status() throws Exception                                                                                                                //Natural: SET-PROCESS-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        short decideConditionsMet211 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CWF-ATI-FULFILL.ATI-PRCSS-STTS;//Natural: VALUE 'O'
        if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("O"))))
        {
            decideConditionsMet211++;
            pnd_Process_Status_Literal.setValue(" OPEN ");                                                                                                                //Natural: MOVE ' OPEN ' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Open_Atis.nadd(1);                                                                                                                            //Natural: ADD 1 TO #GRAND-TOTAL-OPEN-ATIS
            pnd_Input_Data_Pnd_W_Ati_Open.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                                     //Natural: ADD 1 TO #W-ATI-OPEN ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("I"))))
        {
            decideConditionsMet211++;
            pnd_Process_Status_Literal.setValue("INPROGS");                                                                                                               //Natural: MOVE 'INPROGS' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Inprogress_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-INPROGRESS-ATIS
            pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                              //Natural: ADD 1 TO #W-ATI-IN-PROGRESS ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE ' '
        else if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals(" "))))
        {
            decideConditionsMet211++;
            if (condition(cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))                                                                                                     //Natural: IF CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
            {
                pnd_Process_Status_Literal.setValue("SUCCESS");                                                                                                           //Natural: MOVE 'SUCCESS' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Succesfull_Atis.nadd(1);                                                                                                                  //Natural: ADD 1 TO #GRAND-TOTAL-SUCCESFULL-ATIS
                pnd_Input_Data_Pnd_W_Ati_Successful.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                           //Natural: ADD 1 TO #W-ATI-SUCCESSFUL ( #W-INX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Process_Status_Literal.setValue("FAILED");                                                                                                            //Natural: MOVE 'FAILED' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Failed_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-FAILED-ATIS
                pnd_Input_Data_Pnd_W_Ati_Manual.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                               //Natural: ADD 1 TO #W-ATI-MANUAL ( #W-INX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Process_Status_Literal.setValue("INCORCT");                                                                                                               //Natural: MOVE 'INCORCT' TO #PROCESS-STATUS-LITERAL
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet233 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CWF-ATI-FULFILL.ATI-ENTRY-SYSTM-OR-UNT;//Natural: VALUE 'IC'
        if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("IC"))))
        {
            decideConditionsMet233++;
            pnd_Tot_Ok_Ic_System.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOT-OK-IC-SYSTEM
        }                                                                                                                                                                 //Natural: VALUE 'PSS'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("PSS"))))
        {
            decideConditionsMet233++;
            pnd_Tot_Ok_Pss_System.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOT-OK-PSS-SYSTEM
        }                                                                                                                                                                 //Natural: VALUE 'MCSG'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("MCSG"))))
        {
            decideConditionsMet233++;
            pnd_Tot_Ok_Mcsg_System.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-OK-MCSG-SYSTEM
        }                                                                                                                                                                 //Natural: VALUE 'ICSS'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("ICSS"))))
        {
            decideConditionsMet233++;
            pnd_Tot_Ok_Icss_System.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-OK-ICSS-SYSTEM
            if (condition(cwf_Ati_Fulfill_Ati_Entry_Unit_Cde.equals("MFTA")))                                                                                             //Natural: IF CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE = 'MFTA'
            {
                pnd_Tot_Ok_Mfta_Unit.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOT-OK-MFTA-UNIT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Ok_Non_Mfta_Unit.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-OK-NON-MFTA-UNIT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt10 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(10, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(50),"AUTOMATED TRANSACTION SYSTEM",new               //Natural: WRITE ( 10 ) NOTITLE 1T *PROGRAM 50T 'AUTOMATED TRANSACTION SYSTEM' 101T *DATU 110T *TIME ( AL = 005 ) 121T 'PAGE:' 127T *PAGE-NUMBER ( 10 ) ( AD = L ) / 039T 'ATI DETAILED REPORT FOR INCOME OPTION PROFILE' / 034T 'OPEN ATI INCOME OPTION PROFILE REQUESTS FOR :' 080T #INPUT-DTE-D ( EM = MM/DD/YY ) / 001T '-' ( 131 ) / 6T 'PIN' 26T 'MASTER INDEX' 48T 'ATI STATUS' 63T 'PROCESS' / 4T 'NUMBER' 16T 'WPID' 24T 'LOG DATE & TIME' 47T 'DATE & TIME' 63T 'STATUS' / 001T '-' ( 131 ) /
                        TabSetting(101),Global.getDATU(),new TabSetting(110),Global.getTIME(), new AlphanumericLength (5),new TabSetting(121),"PAGE:",new 
                        TabSetting(127),getReports().getPageNumberDbs(10), new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(39),"ATI DETAILED REPORT FOR INCOME OPTION PROFILE",NEWLINE,new 
                        TabSetting(34),"OPEN ATI INCOME OPTION PROFILE REQUESTS FOR :",new TabSetting(80),pnd_Input_Dte_D, new ReportEditMask ("MM/DD/YY"),NEWLINE,new 
                        TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(6),"PIN",new TabSetting(26),"MASTER INDEX",new TabSetting(48),"ATI STATUS",new 
                        TabSetting(63),"PROCESS",NEWLINE,new TabSetting(4),"NUMBER",new TabSetting(16),"WPID",new TabSetting(24),"LOG DATE & TIME",new TabSetting(47),"DATE & TIME",new 
                        TabSetting(63),"STATUS",NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atEndEventRpt10 = (Object sender, EventArgs e) ->
    {
        String localMethod = "MCSN3824|atEndEventRpt10";
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Last_Page_Flag.equals("Y")))                                                                                                        //Natural: IF #LAST-PAGE-FLAG = 'Y'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(10, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(1),"-",new                   //Natural: WRITE ( 10 ) / 1T '-' ( 131 ) / 1T '-' ( 131 )
                            RepeatItem(131));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-ENDPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Ati_Fulfill_Ati_WpidIsBreak = cwf_Ati_Fulfill_Ati_Wpid.isBreak(endOfData);
        if (condition(cwf_Ati_Fulfill_Ati_WpidIsBreak))
        {
            getReports().skip(10, 2);                                                                                                                                     //Natural: SKIP ( 10 ) 2
            getReports().write(10, ReportOption.NOTITLE,new TabSetting(1),"TOTAL FOR WPID :",pnd_Prev_Wpid, new ReportEditMask ("XX' 'X' 'XX' 'X"),":",                   //Natural: WRITE ( 10 ) 1T 'TOTAL FOR WPID :' #PREV-WPID ( EM = XX' 'X' 'XX' 'X ) ':' #WPID-TOTAL
                pnd_Wpid_Total);
            if (condition(Global.isEscape())) return;
            pnd_Wpid_Total.reset();                                                                                                                                       //Natural: RESET #WPID-TOTAL
            getReports().newPage(new ReportSpecification(10));                                                                                                            //Natural: NEWPAGE ( 10 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=OFF");
        Global.format(10, "PS=60 LS=133 ZP=OFF");
    }
}
