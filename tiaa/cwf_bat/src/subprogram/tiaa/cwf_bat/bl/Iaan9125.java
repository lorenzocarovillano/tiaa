/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:41:10 AM
**        * FROM NATURAL SUBPROGRAM : Iaan9125
************************************************************
**        * FILE NAME            : Iaan9125.java
**        * CLASS NAME           : Iaan9125
**        * INSTANCE NAME        : Iaan9125
************************************************************
************************************************************************
* PROGRAM: IAAN9125
* PURPOSE: INTERFACE MODULE CALLED BY CWF TO AUTO-TERMINATE SINGLE
*          LIFE CONTRACTS WITH NO GUARANTEED PERIOD OR WITH GUARANTEED
*          PERIOD THAT HAS EXPIRED AND GENERATE NO FURTHER PAYMENT
*          LETTER. THIS WILL SIMULATE INITIAL DEATH PROCESSING IN IAIQ.
*
* RETURN CODES: 01 - AT LEAST ONE PARAMETER VALUE IS ZERO
*               02 - AT LEAST ONE CONTRACT IS A GROUP CONTRACT
*               03 - AT LEAST ONE CONTRACT HAS JOINT OR NON-SINGLE
*                    LIFE OPTION
*               04 - AT LEAST ONE CONTRACT IS NOT YET EXPIRED
*                    (FOR SINGLE LIFE WITH GUARANTEED PERIOD)
*               05 - DEATH REVERSAL WAS PERFORMED ON THE CONTRACT -
*                    ASSUMED DEATH REPORT WAS A MISTAKE
*               06 - NO CONTRACT ELIGIBLE FOR PROCESSING
*               07 - ERROR WHEN LOGGING TAIDIN
*               08 - ERROR WHEN CREATING NO FURTHER PAYMENT LETTER'
*               09 - NO PIN BASE ADDRESS. LETTER WILL NOT BE PRODUCED.'
*               10 - PIN NOT ON COR. LETTER WILL NOT BE PRODUCED.'
*               11 - AT LEAST ONE CONTRACT IS PENDED.
*               12 - INVALID NUMERIC VALUE FOR PIN.
*               13 - INVALID NUMERIC VALUE FOR SSN.
*               14 - INVALID DOD FORMAT.
*               15 - NOT EXACT MATCH - CANNOT TERMINATE. /* JT0813
*               99 - NO FURTHER PAYMENT SENT - MIXED OPTIONS'
*               00 - NO FURTHER PAYMENT SENT - ALL CONTRACTS TERMINATED
*
* HISTORY:
*
* DATE          PROGRAMMER  DESCRIPTION
* ----------    ----------  -------------
* 06/01/2011    JUN TINIO   ORIGINAL CODE
* 09/12/2011    SET THE LIMIT OF CONTRACTS THAT WILL DISPLAY ON THE
*               NFP LETTER TO 10.
* 04/06/12 J BREMER   RATE BASIS EXPANSION  - RESTOW FOR IAAL9125
* 08/06/12 J TINIO    NY REG 200 PROJECT - ONLY AUTO-TERMINATE IF
*               EXACT MATCH. SC JT0813 FOR CHANGES
* 08/05/15 J TINIO    COR/NAAD SUNSET. SC JT0815 FOR CHANGES.
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* 04/2017  O SOTTO    ADDITIONAL PIN EXPANSION CHANGES MARKED 082017.
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan9125 extends BLNatBase
{
    // Data Areas
    private LdaIaal9125 ldaIaal9125;
    private PdaCwfa8010 pdaCwfa8010;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCdaobj pdaCdaobj;
    private LdaPstl6235 ldaPstl6235;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaEfsa9020 pdaEfsa9020;
    private PdaEfsa902r pdaEfsa902r;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Pin;

    private DbsGroup pnd_Pin__R_Field_1;
    private DbsField pnd_Pin_Pnd_Pin_A;
    private DbsField pnd_Ssn_A;

    private DbsGroup pnd_Ssn_A__R_Field_2;
    private DbsField pnd_Ssn_A_Pnd_Ssn;
    private DbsField pnd_Dod;

    private DbsGroup pnd_Dod__R_Field_3;
    private DbsField pnd_Dod_Pnd_Dod_Ym;

    private DbsGroup pnd_Dod__R_Field_4;
    private DbsField pnd_Dod_Pnd_Dod_A;
    private DbsField pnd_Rc;
    private DbsField pnd_Msg;
    private DbsField pnd_Match_Ind;
    private DbsField pnd_Cpr_Key;

    private DbsGroup pnd_Cpr_Key__R_Field_5;
    private DbsField pnd_Cpr_Key_Pnd_Cpr_Pin;
    private DbsField pnd_Cpr_Key_Pnd_Cpr_Cntrct;
    private DbsField pnd_Cpr_Key_Pnd_Cpr_Payee;
    private DbsField pnd_Trans_Cntrct_Key;

    private DbsGroup pnd_Trans_Cntrct_Key__R_Field_6;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Invrse_Dte;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Cde;
    private DbsField pnd_Sl;
    private DbsField pnd_Fp_Pay_Dte;

    private DbsGroup pnd_Fp_Pay_Dte__R_Field_7;
    private DbsField pnd_Fp_Pay_Dte_Pnd_Fp_Pay_Dte_N;
    private DbsField pnd_W_Cntrct_Payee;

    private DbsGroup pnd_W_Cntrct_Payee__R_Field_8;
    private DbsField pnd_W_Cntrct_Payee_Pnd_W_Cntrct;
    private DbsField pnd_W_Cntrct_Payee_Pnd_W_Payee;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key;

    private DbsGroup pnd_Tiaa_Cntrct_Fund_Key__R_Field_9;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key__Filler1;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fnd;
    private DbsField pnd_W_Addr;

    private DbsGroup pnd_W_Addr__R_Field_10;
    private DbsField pnd_W_Addr_Pnd_W_A1;
    private DbsField pnd_New_Addr;

    private DbsGroup pnd_New_Addr__R_Field_11;
    private DbsField pnd_New_Addr_Pnd_N_A1;
    private DbsField pnd_Addr_Lines;

    private DbsGroup pnd_Addr_Lines__R_Field_12;
    private DbsField pnd_Addr_Lines_Pnd_Addr1;
    private DbsField pnd_Addr_Lines_Pnd_Addr2;
    private DbsField pnd_Addr_Lines_Pnd_Addr3;
    private DbsField pnd_Addr_Lines_Pnd_Addr4;
    private DbsField pnd_Addr_Lines_Pnd_Addr5;
    private DbsField pnd_City;
    private DbsField pnd_State;
    private DbsField pnd_Zip;
    private DbsField pnd_W_Names;
    private DbsField pnd_Zip_Found;
    private DbsField pnd_Naad;
    private DbsField pnd_Fnme;

    private DbsGroup pnd_Fnme__R_Field_13;
    private DbsField pnd_Fnme_Pnd_F_Init;
    private DbsField pnd_Fnme_Pnd_F_Rest;
    private DbsField pnd_Lnme;

    private DbsGroup pnd_Lnme__R_Field_14;
    private DbsField pnd_Lnme_Pnd_L_Init;
    private DbsField pnd_Lnme_Pnd_L_Rest;
    private DbsField pnd_Mnme;

    private DbsGroup pnd_Mnme__R_Field_15;
    private DbsField pnd_Mnme_Pnd_M_Init;
    private DbsField pnd_Mnme_Pnd_M_Rest;
    private DbsField pnd_Error;
    private DbsField pnd_Rvrsl;
    private DbsField pnd_Name;
    private DbsField pnd_Name2;
    private DbsField pnd_T_Dte;

    private DbsGroup pnd_T_Dte__R_Field_16;
    private DbsField pnd_T_Dte_Pnd_T_Dte_A;
    private DbsField pnd_W_Dte;
    private DbsField pnd_W_Yyyymm;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Invrse_Trans_Dte;
    private DbsField pnd_Cntrct_Isn;
    private DbsField pnd_Cpr_Isn;
    private DbsField pnd_Cntrct_Payee;
    private DbsField pnd_Len;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_S;
    private DbsField pnd_T;
    private DbsField pnd_Space;
    private DbsField pnd_Max;
    private DbsField pnd_Txt;

    private DbsGroup pnd_Txt__R_Field_17;
    private DbsField pnd_Txt__Filler2;
    private DbsField pnd_Txt_Pnd_Cntrct;
    private DbsField pnd_Tiaa;
    private DbsField pnd_Cref;

    private DataAccessProgramView vw_cwf_Mit_Sub;
    private DbsField cwf_Mit_Sub_Pin_Nbr;
    private DbsField cwf_Mit_Sub_Rqst_Log_Dte_Tme;
    private DbsField cwf_Mit_Sub_Work_Prcss_Id;
    private DbsField cwf_Mit_Sub_Tiaa_Rcvd_Dte;
    private DbsField cwf_Mit_Sub_Rqst_Invrt_Rcvd_Dte_Tme;
    private DbsField cwf_Mit_Sub_Case_Id_Cde;
    private DbsField pnd_Pin_History_Key;

    private DbsGroup pnd_Pin_History_Key__R_Field_18;
    private DbsField pnd_Pin_History_Key_Pnd_Pin_Nbrx;

    private DbsGroup pnd_Pin_History_Key__R_Field_19;
    private DbsField pnd_Pin_History_Key_Pnd_Pin_N;
    private DbsField pnd_Pin_History_Key_Pnd_Crprte_Status_Ind;
    private DbsField pnd_Pin_History_Key_Pnd_Rqst_Invrt_Rcvd_Dte_Tme;
    private DbsField pnd_Pin_History_Key_Pnd_Work_Prcss_Id;
    private DbsField pnd_Pin_History_Key_Pnd_Actve_Ind_2_2;
    private DbsField pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Msgx;
    private DbsField pls_Cntrl_Check_Dte;
    private DbsField pls_Cntrl_Todays_Dte;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal9125 = new LdaIaal9125();
        registerRecord(ldaIaal9125);
        registerRecord(ldaIaal9125.getVw_iaa_Cntrl_Rcrd());
        registerRecord(ldaIaal9125.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal9125.getVw_iaa_Cntrct());
        registerRecord(ldaIaal9125.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal9125.getVw_cpr());
        registerRecord(ldaIaal9125.getVw_cpr_Trans());
        registerRecord(ldaIaal9125.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal9125.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal9125.getVw_iaa_Dc_Sttlmnt_Req());
        registerRecord(ldaIaal9125.getVw_iaa_Dc_Cntrct());
        localVariables = new DbsRecord();
        pdaCwfa8010 = new PdaCwfa8010(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        ldaPstl6235 = new LdaPstl6235();
        registerRecord(ldaPstl6235);
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaEfsa9020 = new PdaEfsa9020(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Pin = parameters.newFieldInRecord("pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Pin.setParameterOption(ParameterOption.ByReference);

        pnd_Pin__R_Field_1 = parameters.newGroupInRecord("pnd_Pin__R_Field_1", "REDEFINE", pnd_Pin);
        pnd_Pin_Pnd_Pin_A = pnd_Pin__R_Field_1.newFieldInGroup("pnd_Pin_Pnd_Pin_A", "#PIN-A", FieldType.STRING, 12);
        pnd_Ssn_A = parameters.newFieldInRecord("pnd_Ssn_A", "#SSN-A", FieldType.STRING, 9);
        pnd_Ssn_A.setParameterOption(ParameterOption.ByReference);

        pnd_Ssn_A__R_Field_2 = parameters.newGroupInRecord("pnd_Ssn_A__R_Field_2", "REDEFINE", pnd_Ssn_A);
        pnd_Ssn_A_Pnd_Ssn = pnd_Ssn_A__R_Field_2.newFieldInGroup("pnd_Ssn_A_Pnd_Ssn", "#SSN", FieldType.NUMERIC, 9);
        pnd_Dod = parameters.newFieldInRecord("pnd_Dod", "#DOD", FieldType.NUMERIC, 8);
        pnd_Dod.setParameterOption(ParameterOption.ByReference);

        pnd_Dod__R_Field_3 = parameters.newGroupInRecord("pnd_Dod__R_Field_3", "REDEFINE", pnd_Dod);
        pnd_Dod_Pnd_Dod_Ym = pnd_Dod__R_Field_3.newFieldInGroup("pnd_Dod_Pnd_Dod_Ym", "#DOD-YM", FieldType.STRING, 6);

        pnd_Dod__R_Field_4 = parameters.newGroupInRecord("pnd_Dod__R_Field_4", "REDEFINE", pnd_Dod);
        pnd_Dod_Pnd_Dod_A = pnd_Dod__R_Field_4.newFieldInGroup("pnd_Dod_Pnd_Dod_A", "#DOD-A", FieldType.STRING, 8);
        pnd_Rc = parameters.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 4);
        pnd_Rc.setParameterOption(ParameterOption.ByReference);
        pnd_Msg = parameters.newFieldInRecord("pnd_Msg", "#MSG", FieldType.STRING, 50);
        pnd_Msg.setParameterOption(ParameterOption.ByReference);
        pnd_Match_Ind = parameters.newFieldInRecord("pnd_Match_Ind", "#MATCH-IND", FieldType.STRING, 3);
        pnd_Match_Ind.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Cpr_Key = localVariables.newFieldInRecord("pnd_Cpr_Key", "#CPR-KEY", FieldType.STRING, 24);

        pnd_Cpr_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cpr_Key__R_Field_5", "REDEFINE", pnd_Cpr_Key);
        pnd_Cpr_Key_Pnd_Cpr_Pin = pnd_Cpr_Key__R_Field_5.newFieldInGroup("pnd_Cpr_Key_Pnd_Cpr_Pin", "#CPR-PIN", FieldType.NUMERIC, 12);
        pnd_Cpr_Key_Pnd_Cpr_Cntrct = pnd_Cpr_Key__R_Field_5.newFieldInGroup("pnd_Cpr_Key_Pnd_Cpr_Cntrct", "#CPR-CNTRCT", FieldType.STRING, 10);
        pnd_Cpr_Key_Pnd_Cpr_Payee = pnd_Cpr_Key__R_Field_5.newFieldInGroup("pnd_Cpr_Key_Pnd_Cpr_Payee", "#CPR-PAYEE", FieldType.NUMERIC, 2);
        pnd_Trans_Cntrct_Key = localVariables.newFieldInRecord("pnd_Trans_Cntrct_Key", "#TRANS-CNTRCT-KEY", FieldType.STRING, 27);

        pnd_Trans_Cntrct_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Trans_Cntrct_Key__R_Field_6", "REDEFINE", pnd_Trans_Cntrct_Key);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Cntrct_Key__R_Field_6.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde = pnd_Trans_Cntrct_Key__R_Field_6.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Cntrct_Key_Pnd_Invrse_Dte = pnd_Trans_Cntrct_Key__R_Field_6.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Invrse_Dte", "#INVRSE-DTE", FieldType.NUMERIC, 
            12);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Cde = pnd_Trans_Cntrct_Key__R_Field_6.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 
            3);
        pnd_Sl = localVariables.newFieldArrayInRecord("pnd_Sl", "#SL", FieldType.NUMERIC, 2, new DbsArrayController(1, 5));
        pnd_Fp_Pay_Dte = localVariables.newFieldInRecord("pnd_Fp_Pay_Dte", "#FP-PAY-DTE", FieldType.STRING, 6);

        pnd_Fp_Pay_Dte__R_Field_7 = localVariables.newGroupInRecord("pnd_Fp_Pay_Dte__R_Field_7", "REDEFINE", pnd_Fp_Pay_Dte);
        pnd_Fp_Pay_Dte_Pnd_Fp_Pay_Dte_N = pnd_Fp_Pay_Dte__R_Field_7.newFieldInGroup("pnd_Fp_Pay_Dte_Pnd_Fp_Pay_Dte_N", "#FP-PAY-DTE-N", FieldType.NUMERIC, 
            6);
        pnd_W_Cntrct_Payee = localVariables.newFieldInRecord("pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_W_Cntrct_Payee__R_Field_8 = localVariables.newGroupInRecord("pnd_W_Cntrct_Payee__R_Field_8", "REDEFINE", pnd_W_Cntrct_Payee);
        pnd_W_Cntrct_Payee_Pnd_W_Cntrct = pnd_W_Cntrct_Payee__R_Field_8.newFieldInGroup("pnd_W_Cntrct_Payee_Pnd_W_Cntrct", "#W-CNTRCT", FieldType.STRING, 
            10);
        pnd_W_Cntrct_Payee_Pnd_W_Payee = pnd_W_Cntrct_Payee__R_Field_8.newFieldInGroup("pnd_W_Cntrct_Payee_Pnd_W_Payee", "#W-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Tiaa_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Tiaa_Cntrct_Fund_Key", "#TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Tiaa_Cntrct_Fund_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Tiaa_Cntrct_Fund_Key__R_Field_9", "REDEFINE", pnd_Tiaa_Cntrct_Fund_Key);
        pnd_Tiaa_Cntrct_Fund_Key__Filler1 = pnd_Tiaa_Cntrct_Fund_Key__R_Field_9.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key__Filler1", "_FILLER1", FieldType.STRING, 
            12);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fnd = pnd_Tiaa_Cntrct_Fund_Key__R_Field_9.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fnd", "#FND", FieldType.STRING, 
            3);
        pnd_W_Addr = localVariables.newFieldInRecord("pnd_W_Addr", "#W-ADDR", FieldType.STRING, 25);

        pnd_W_Addr__R_Field_10 = localVariables.newGroupInRecord("pnd_W_Addr__R_Field_10", "REDEFINE", pnd_W_Addr);
        pnd_W_Addr_Pnd_W_A1 = pnd_W_Addr__R_Field_10.newFieldArrayInGroup("pnd_W_Addr_Pnd_W_A1", "#W-A1", FieldType.STRING, 1, new DbsArrayController(1, 
            25));
        pnd_New_Addr = localVariables.newFieldInRecord("pnd_New_Addr", "#NEW-ADDR", FieldType.STRING, 25);

        pnd_New_Addr__R_Field_11 = localVariables.newGroupInRecord("pnd_New_Addr__R_Field_11", "REDEFINE", pnd_New_Addr);
        pnd_New_Addr_Pnd_N_A1 = pnd_New_Addr__R_Field_11.newFieldArrayInGroup("pnd_New_Addr_Pnd_N_A1", "#N-A1", FieldType.STRING, 1, new DbsArrayController(1, 
            25));
        pnd_Addr_Lines = localVariables.newFieldArrayInRecord("pnd_Addr_Lines", "#ADDR-LINES", FieldType.STRING, 25, new DbsArrayController(1, 5));

        pnd_Addr_Lines__R_Field_12 = localVariables.newGroupInRecord("pnd_Addr_Lines__R_Field_12", "REDEFINE", pnd_Addr_Lines);
        pnd_Addr_Lines_Pnd_Addr1 = pnd_Addr_Lines__R_Field_12.newFieldInGroup("pnd_Addr_Lines_Pnd_Addr1", "#ADDR1", FieldType.STRING, 25);
        pnd_Addr_Lines_Pnd_Addr2 = pnd_Addr_Lines__R_Field_12.newFieldInGroup("pnd_Addr_Lines_Pnd_Addr2", "#ADDR2", FieldType.STRING, 25);
        pnd_Addr_Lines_Pnd_Addr3 = pnd_Addr_Lines__R_Field_12.newFieldInGroup("pnd_Addr_Lines_Pnd_Addr3", "#ADDR3", FieldType.STRING, 25);
        pnd_Addr_Lines_Pnd_Addr4 = pnd_Addr_Lines__R_Field_12.newFieldInGroup("pnd_Addr_Lines_Pnd_Addr4", "#ADDR4", FieldType.STRING, 25);
        pnd_Addr_Lines_Pnd_Addr5 = pnd_Addr_Lines__R_Field_12.newFieldInGroup("pnd_Addr_Lines_Pnd_Addr5", "#ADDR5", FieldType.STRING, 25);
        pnd_City = localVariables.newFieldInRecord("pnd_City", "#CITY", FieldType.STRING, 25);
        pnd_State = localVariables.newFieldInRecord("pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Zip = localVariables.newFieldInRecord("pnd_Zip", "#ZIP", FieldType.STRING, 5);
        pnd_W_Names = localVariables.newFieldArrayInRecord("pnd_W_Names", "#W-NAMES", FieldType.STRING, 25, new DbsArrayController(1, 19));
        pnd_Zip_Found = localVariables.newFieldInRecord("pnd_Zip_Found", "#ZIP-FOUND", FieldType.BOOLEAN, 1);
        pnd_Naad = localVariables.newFieldInRecord("pnd_Naad", "#NAAD", FieldType.BOOLEAN, 1);
        pnd_Fnme = localVariables.newFieldInRecord("pnd_Fnme", "#FNME", FieldType.STRING, 30);

        pnd_Fnme__R_Field_13 = localVariables.newGroupInRecord("pnd_Fnme__R_Field_13", "REDEFINE", pnd_Fnme);
        pnd_Fnme_Pnd_F_Init = pnd_Fnme__R_Field_13.newFieldInGroup("pnd_Fnme_Pnd_F_Init", "#F-INIT", FieldType.STRING, 1);
        pnd_Fnme_Pnd_F_Rest = pnd_Fnme__R_Field_13.newFieldInGroup("pnd_Fnme_Pnd_F_Rest", "#F-REST", FieldType.STRING, 29);
        pnd_Lnme = localVariables.newFieldInRecord("pnd_Lnme", "#LNME", FieldType.STRING, 30);

        pnd_Lnme__R_Field_14 = localVariables.newGroupInRecord("pnd_Lnme__R_Field_14", "REDEFINE", pnd_Lnme);
        pnd_Lnme_Pnd_L_Init = pnd_Lnme__R_Field_14.newFieldInGroup("pnd_Lnme_Pnd_L_Init", "#L-INIT", FieldType.STRING, 1);
        pnd_Lnme_Pnd_L_Rest = pnd_Lnme__R_Field_14.newFieldInGroup("pnd_Lnme_Pnd_L_Rest", "#L-REST", FieldType.STRING, 29);
        pnd_Mnme = localVariables.newFieldInRecord("pnd_Mnme", "#MNME", FieldType.STRING, 30);

        pnd_Mnme__R_Field_15 = localVariables.newGroupInRecord("pnd_Mnme__R_Field_15", "REDEFINE", pnd_Mnme);
        pnd_Mnme_Pnd_M_Init = pnd_Mnme__R_Field_15.newFieldInGroup("pnd_Mnme_Pnd_M_Init", "#M-INIT", FieldType.STRING, 1);
        pnd_Mnme_Pnd_M_Rest = pnd_Mnme__R_Field_15.newFieldInGroup("pnd_Mnme_Pnd_M_Rest", "#M-REST", FieldType.STRING, 29);
        pnd_Error = localVariables.newFieldInRecord("pnd_Error", "#ERROR", FieldType.BOOLEAN, 1);
        pnd_Rvrsl = localVariables.newFieldInRecord("pnd_Rvrsl", "#RVRSL", FieldType.BOOLEAN, 1);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 35);
        pnd_Name2 = localVariables.newFieldInRecord("pnd_Name2", "#NAME2", FieldType.STRING, 35);
        pnd_T_Dte = localVariables.newFieldInRecord("pnd_T_Dte", "#T-DTE", FieldType.NUMERIC, 8);

        pnd_T_Dte__R_Field_16 = localVariables.newGroupInRecord("pnd_T_Dte__R_Field_16", "REDEFINE", pnd_T_Dte);
        pnd_T_Dte_Pnd_T_Dte_A = pnd_T_Dte__R_Field_16.newFieldInGroup("pnd_T_Dte_Pnd_T_Dte_A", "#T-DTE-A", FieldType.STRING, 8);
        pnd_W_Dte = localVariables.newFieldInRecord("pnd_W_Dte", "#W-DTE", FieldType.STRING, 8);
        pnd_W_Yyyymm = localVariables.newFieldInRecord("pnd_W_Yyyymm", "#W-YYYYMM", FieldType.STRING, 6);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Invrse_Trans_Dte = localVariables.newFieldInRecord("pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Cntrct_Isn = localVariables.newFieldInRecord("pnd_Cntrct_Isn", "#CNTRCT-ISN", FieldType.PACKED_DECIMAL, 14);
        pnd_Cpr_Isn = localVariables.newFieldInRecord("pnd_Cpr_Isn", "#CPR-ISN", FieldType.PACKED_DECIMAL, 14);
        pnd_Cntrct_Payee = localVariables.newFieldArrayInRecord("pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12, new DbsArrayController(1, 100));
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_S = localVariables.newFieldInRecord("pnd_S", "#S", FieldType.INTEGER, 2);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.INTEGER, 2);
        pnd_Space = localVariables.newFieldInRecord("pnd_Space", "#SPACE", FieldType.INTEGER, 2);
        pnd_Max = localVariables.newFieldInRecord("pnd_Max", "#MAX", FieldType.INTEGER, 2);
        pnd_Txt = localVariables.newFieldInRecord("pnd_Txt", "#TXT", FieldType.STRING, 30);

        pnd_Txt__R_Field_17 = localVariables.newGroupInRecord("pnd_Txt__R_Field_17", "REDEFINE", pnd_Txt);
        pnd_Txt__Filler2 = pnd_Txt__R_Field_17.newFieldInGroup("pnd_Txt__Filler2", "_FILLER2", FieldType.STRING, 21);
        pnd_Txt_Pnd_Cntrct = pnd_Txt__R_Field_17.newFieldInGroup("pnd_Txt_Pnd_Cntrct", "#CNTRCT", FieldType.STRING, 9);
        pnd_Tiaa = localVariables.newFieldInRecord("pnd_Tiaa", "#TIAA", FieldType.INTEGER, 2);
        pnd_Cref = localVariables.newFieldInRecord("pnd_Cref", "#CREF", FieldType.INTEGER, 2);

        vw_cwf_Mit_Sub = new DataAccessProgramView(new NameInfo("vw_cwf_Mit_Sub", "CWF-MIT-SUB"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Mit_Sub_Pin_Nbr = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Mit_Sub_Pin_Nbr.setDdmHeader("PIN");
        cwf_Mit_Sub_Rqst_Log_Dte_Tme = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Mit_Sub_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Mit_Sub_Work_Prcss_Id = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Mit_Sub_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Mit_Sub_Tiaa_Rcvd_Dte = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Mit_Sub_Rqst_Invrt_Rcvd_Dte_Tme = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Rqst_Invrt_Rcvd_Dte_Tme", "RQST-INVRT-RCVD-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_INVRT_RCVD_DTE_TME");
        cwf_Mit_Sub_Case_Id_Cde = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CASE_ID_CDE");
        cwf_Mit_Sub_Case_Id_Cde.setDdmHeader("CASE/ID");
        registerRecord(vw_cwf_Mit_Sub);

        pnd_Pin_History_Key = localVariables.newFieldInRecord("pnd_Pin_History_Key", "#PIN-HISTORY-KEY", FieldType.STRING, 35);

        pnd_Pin_History_Key__R_Field_18 = localVariables.newGroupInRecord("pnd_Pin_History_Key__R_Field_18", "REDEFINE", pnd_Pin_History_Key);
        pnd_Pin_History_Key_Pnd_Pin_Nbrx = pnd_Pin_History_Key__R_Field_18.newFieldInGroup("pnd_Pin_History_Key_Pnd_Pin_Nbrx", "#PIN-NBRX", FieldType.STRING, 
            12);

        pnd_Pin_History_Key__R_Field_19 = pnd_Pin_History_Key__R_Field_18.newGroupInGroup("pnd_Pin_History_Key__R_Field_19", "REDEFINE", pnd_Pin_History_Key_Pnd_Pin_Nbrx);
        pnd_Pin_History_Key_Pnd_Pin_N = pnd_Pin_History_Key__R_Field_19.newFieldInGroup("pnd_Pin_History_Key_Pnd_Pin_N", "#PIN-N", FieldType.NUMERIC, 
            12);
        pnd_Pin_History_Key_Pnd_Crprte_Status_Ind = pnd_Pin_History_Key__R_Field_18.newFieldInGroup("pnd_Pin_History_Key_Pnd_Crprte_Status_Ind", "#CRPRTE-STATUS-IND", 
            FieldType.STRING, 1);
        pnd_Pin_History_Key_Pnd_Rqst_Invrt_Rcvd_Dte_Tme = pnd_Pin_History_Key__R_Field_18.newFieldInGroup("pnd_Pin_History_Key_Pnd_Rqst_Invrt_Rcvd_Dte_Tme", 
            "#RQST-INVRT-RCVD-DTE-TME", FieldType.STRING, 15);
        pnd_Pin_History_Key_Pnd_Work_Prcss_Id = pnd_Pin_History_Key__R_Field_18.newFieldInGroup("pnd_Pin_History_Key_Pnd_Work_Prcss_Id", "#WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        pnd_Pin_History_Key_Pnd_Actve_Ind_2_2 = pnd_Pin_History_Key__R_Field_18.newFieldInGroup("pnd_Pin_History_Key_Pnd_Actve_Ind_2_2", "#ACTVE-IND-2-2", 
            FieldType.STRING, 1);
        pnd_Rqst_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Msgx = localVariables.newFieldInRecord("pnd_Msgx", "#MSGX", FieldType.STRING, 30);
        pls_Cntrl_Check_Dte = WsIndependent.getInstance().newFieldInRecord("pls_Cntrl_Check_Dte", "+CNTRL-CHECK-DTE", FieldType.NUMERIC, 8);
        pls_Cntrl_Todays_Dte = WsIndependent.getInstance().newFieldInRecord("pls_Cntrl_Todays_Dte", "+CNTRL-TODAYS-DTE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Mit_Sub.reset();

        ldaIaal9125.initializeValues();
        ldaPstl6235.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Sl.getValue(1).setInitialValue(1);
        pnd_Sl.getValue(2).setInitialValue(5);
        pnd_Sl.getValue(3).setInitialValue(6);
        pnd_Sl.getValue(4).setInitialValue(9);
        pnd_Sl.getValue(5).setInitialValue(32);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan9125() throws Exception
    {
        super("Iaan9125");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  JT0813 START
        if (condition(pnd_Match_Ind.equals("BFL") || pnd_Match_Ind.equals("BLF")))                                                                                        //Natural: IF #MATCH-IND = 'BFL' OR = 'BLF'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rc.setValue(15);                                                                                                                                          //Natural: ASSIGN #RC := 15
            pnd_Msg.setValue("Not exact match - cannot terminate.");                                                                                                      //Natural: ASSIGN #MSG := 'Not exact match - cannot terminate.'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
            //*  JT0813 END
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #PIN-A IS (N7)
        //*   PIN EXP   06/2017
        if (condition(DbsUtil.is(pnd_Pin_Pnd_Pin_A.getText(),"N12")))                                                                                                     //Natural: IF #PIN-A IS ( N12 )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rc.setValue(12);                                                                                                                                          //Natural: ASSIGN #RC := 12
            pnd_Msg.setValue("Invalid numeric value for PIN");                                                                                                            //Natural: ASSIGN #MSG := 'Invalid numeric value for PIN'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.is(pnd_Ssn_A.getText(),"N9")))                                                                                                              //Natural: IF #SSN-A IS ( N9 )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rc.setValue(13);                                                                                                                                          //Natural: ASSIGN #RC := 13
            pnd_Msg.setValue("Invalid numeric value for SSN");                                                                                                            //Natural: ASSIGN #MSG := 'Invalid numeric value for SSN'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(pnd_Dod_Pnd_Dod_A,"YYYYMMDD")))                                                                                                 //Natural: IF #DOD-A = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rc.setValue(14);                                                                                                                                          //Natural: ASSIGN #RC := 14
            pnd_Msg.setValue("Invalid DOD format");                                                                                                                       //Natural: ASSIGN #MSG := 'Invalid DOD format'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Rc.reset();                                                                                                                                                   //Natural: RESET #RC #INVRSE-DTE #TRANS-CDE #MSG
        pnd_Trans_Cntrct_Key_Pnd_Invrse_Dte.reset();
        pnd_Trans_Cntrct_Key_Pnd_Trans_Cde.reset();
        pnd_Msg.reset();
        if (condition(pnd_Pin.equals(getZero()) || pnd_Ssn_A_Pnd_Ssn.equals(getZero()) || pnd_Dod.equals(getZero())))                                                     //Natural: IF #PIN = 0 OR #SSN = 0 OR #DOD = 0
        {
            pnd_Rc.setValue(1);                                                                                                                                           //Natural: ASSIGN #RC := 01
            pnd_Msg.setValue("At least one required parameter value is zero");                                                                                            //Natural: ASSIGN #MSG := 'At least one required parameter value is zero'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Fp_Pay_Dte.setValue(pnd_Dod_Pnd_Dod_Ym);                                                                                                                      //Natural: ASSIGN #FP-PAY-DTE := #DOD-YM
        pnd_Cpr_Key_Pnd_Cpr_Pin.setValue(pnd_Pin);                                                                                                                        //Natural: ASSIGN #CPR-PIN := #PIN
        ldaIaal9125.getVw_cpr().startDatabaseRead                                                                                                                         //Natural: READ CPR BY PIN-CNTRCT-PAYEE-KEY STARTING FROM #CPR-KEY
        (
        "READ01",
        new Wc[] { new Wc("PIN_CNTRCT_PAYEE_KEY", ">=", pnd_Cpr_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ01:
        while (condition(ldaIaal9125.getVw_cpr().readNextRow("READ01")))
        {
            if (condition(ldaIaal9125.getCpr_Cpr_Id_Nbr().notEquals(pnd_Pin)))                                                                                            //Natural: IF CPR-ID-NBR NE #PIN
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaIaal9125.getCpr_Cntrct_Actvty_Cde().notEquals(9) && ldaIaal9125.getCpr_Prtcpnt_Tax_Id_Nbr().equals(pnd_Ssn_A_Pnd_Ssn))))                   //Natural: ACCEPT IF CNTRCT-ACTVTY-CDE NE 9 AND PRTCPNT-TAX-ID-NBR = #SSN
            {
                continue;
            }
            //*    AND CNTRCT-PEND-CDE= '0'
            ldaIaal9125.getVw_iaa_Cntrct().startDatabaseFind                                                                                                              //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = CNTRCT-PART-PPCN-NBR
            (
            "FIND01",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", ldaIaal9125.getCpr_Cntrct_Part_Ppcn_Nbr(), WcType.WITH) }
            );
            FIND01:
            while (condition(ldaIaal9125.getVw_iaa_Cntrct().readNextRow("FIND01")))
            {
                ldaIaal9125.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ALREADY DECEASED
            if (condition(ldaIaal9125.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greater(getZero()) && ldaIaal9125.getCpr_Cntrct_Part_Payee_Cde().equals(1)))              //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE GT 0 AND CPR.CNTRCT-PART-PAYEE-CDE = 01
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  GROUP
            if (condition(ldaIaal9125.getIaa_Cntrct_Cntrct_Orgn_Cde().equals(4) || ldaIaal9125.getIaa_Cntrct_Cntrct_Orgn_Cde().equals(6)))                                //Natural: IF IAA-CNTRCT.CNTRCT-ORGN-CDE = 04 OR = 06
            {
                pnd_Rc.setValue(2);                                                                                                                                       //Natural: ASSIGN #RC := 02
                pnd_Msg.setValue("At least one contract is a group contract");                                                                                            //Natural: ASSIGN #MSG := 'At least one contract is a group contract'
                //*    ESCAPE ROUTINE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal9125.getIaa_Cntrct_Cntrct_Optn_Cde().equals(pnd_Sl.getValue("*"))))                                                                      //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = #SL ( * )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rc.setValue(3);                                                                                                                                       //Natural: ASSIGN #RC := 03
                pnd_Msg.setValue("At least one contract has joint or non-life option");                                                                                   //Natural: ASSIGN #MSG := 'At least one contract has joint or non-life option'
                //*    ESCAPE ROUTINE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1769 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE IAA-CNTRCT.CNTRCT-OPTN-CDE;//Natural: VALUE 01
            if (condition((ldaIaal9125.getIaa_Cntrct_Cntrct_Optn_Cde().equals(1))))
            {
                decideConditionsMet1769++;
                ignore();
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                //*  NOT EXPIRED
                if (condition(pnd_Fp_Pay_Dte_Pnd_Fp_Pay_Dte_N.lessOrEqual(ldaIaal9125.getCpr_Cntrct_Final_Per_Pay_Dte())))                                                //Natural: IF #FP-PAY-DTE-N LE CPR.CNTRCT-FINAL-PER-PAY-DTE
                {
                    pnd_Rc.setValue(4);                                                                                                                                   //Natural: ASSIGN #RC := 04
                    pnd_Msg.setValue("At least one contract is not yet expired");                                                                                         //Natural: ASSIGN #MSG := 'At least one contract is not yet expired'
                    //*        ESCAPE ROUTINE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(ldaIaal9125.getCpr_Cntrct_Pend_Cde().notEquals("0")))                                                                                           //Natural: IF CNTRCT-PEND-CDE NE '0'
            {
                pnd_Rc.setValue(11);                                                                                                                                      //Natural: ASSIGN #RC := 11
                pnd_Msg.setValue("At least one contract is pended");                                                                                                      //Natural: ASSIGN #MSG := 'At least one contract is pended'
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Cntrct_Payee_Pnd_W_Cntrct.setValue(ldaIaal9125.getCpr_Cntrct_Part_Ppcn_Nbr());                                                                          //Natural: ASSIGN #W-CNTRCT := CNTRCT-PART-PPCN-NBR
            pnd_W_Cntrct_Payee_Pnd_W_Payee.setValue(ldaIaal9125.getCpr_Cntrct_Part_Payee_Cde());                                                                          //Natural: ASSIGN #W-PAYEE := CNTRCT-PART-PAYEE-CDE
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-REVERSAL
            sub_Check_For_Reversal();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ID REVERSAL WAS PERFORMED - DEATH REPORTED IN ERROR
            if (condition(pnd_Rvrsl.getBoolean()))                                                                                                                        //Natural: IF #RVRSL
            {
                pnd_Rc.setValue(5);                                                                                                                                       //Natural: ASSIGN #RC := 05
                pnd_Msg.setValue("Death reversal was performed on the contract");                                                                                         //Natural: ASSIGN #MSG := 'Death reversal was performed on the contract'
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Max.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #MAX
            pnd_Cntrct_Payee.getValue(pnd_Max).setValue(pnd_W_Cntrct_Payee);                                                                                              //Natural: ASSIGN #CNTRCT-PAYEE ( #MAX ) := #W-CNTRCT-PAYEE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Max.equals(getZero())))                                                                                                                         //Natural: IF #MAX = 0
        {
            if (condition(pnd_Rc.greater(getZero())))                                                                                                                     //Natural: IF #RC GT 0
            {
                ignore();
                //*  NO ELIGIBLE CONTRACTS FOR PROCESSING
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rc.setValue(6);                                                                                                                                       //Natural: ASSIGN #RC := 06
                pnd_Msg.setValue("No contract eligible for processing");                                                                                                  //Natural: ASSIGN #MSG := 'No contract eligible for processing'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pls_Cntrl_Check_Dte.equals(getZero())))                                                                                                             //Natural: IF +CNTRL-CHECK-DTE = 0
        {
            ldaIaal9125.getVw_iaa_Cntrl_Rcrd().startDatabaseRead                                                                                                          //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'DC'
            (
            "READ02",
            new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
            new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
            1
            );
            READ02:
            while (condition(ldaIaal9125.getVw_iaa_Cntrl_Rcrd().readNextRow("READ02")))
            {
                pls_Cntrl_Check_Dte.setValue(ldaIaal9125.getIaa_Cntrl_Rcrd_Cntrl_Check_Dte());                                                                            //Natural: ASSIGN +CNTRL-CHECK-DTE := CNTRL-CHECK-DTE
                pls_Cntrl_Todays_Dte.setValue(ldaIaal9125.getIaa_Cntrl_Rcrd_Cntrl_Todays_Dte());                                                                          //Natural: ASSIGN +CNTRL-TODAYS-DTE := CNTRL-TODAYS-DTE
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Trans_Dte.setValue(Global.getTIMX());                                                                                                                         //Natural: ASSIGN #TRANS-DTE := *TIMX
        pnd_Invrse_Trans_Dte.compute(new ComputeParameters(false, pnd_Invrse_Trans_Dte), new DbsDecimal("1000000000000").subtract(pnd_Trans_Dte));                        //Natural: COMPUTE #INVRSE-TRANS-DTE = 1000000000000 - #TRANS-DTE
        pnd_T_Dte_Pnd_T_Dte_A.setValueEdited(pls_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED +CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #T-DTE-A
                                                                                                                                                                          //Natural: PERFORM GET-NAME
        sub_Get_Name();
        if (condition(Global.isEscape())) {return;}
        //*  PIN NOT ON COR
        if (condition(pnd_Rc.equals(10)))                                                                                                                                 //Natural: IF #RC = 10
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-BASE-ADDRESS
        sub_Get_Base_Address();
        if (condition(Global.isEscape())) {return;}
        //*  MISSING PIN BASE ADDRESS
        if (condition(pnd_Naad.equals(false)))                                                                                                                            //Natural: IF #NAAD = FALSE
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-DC-NOTIFY-RECORD
        sub_Create_Dc_Notify_Record();
        if (condition(Global.isEscape())) {return;}
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO #MAX
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max)); pnd_I.nadd(1))
        {
            if (condition(pnd_Cntrct_Payee.getValue(pnd_I).notEquals(" ")))                                                                                               //Natural: IF #CNTRCT-PAYEE ( #I ) NE ' '
            {
                pnd_W_Cntrct_Payee.setValue(pnd_Cntrct_Payee.getValue(pnd_I));                                                                                            //Natural: ASSIGN #W-CNTRCT-PAYEE := #CNTRCT-PAYEE ( #I )
                                                                                                                                                                          //Natural: PERFORM CREATE-DC-CNTRCT
                sub_Create_Dc_Cntrct();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CREATE-TRANS-RCRD
                sub_Create_Trans_Rcrd();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM CREATE-NO-FURTHER-PAYMENT-LETTER
        sub_Create_No_Further_Payment_Letter();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Error.getBoolean()))                                                                                                                            //Natural: IF #ERROR
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, "=",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(),NEWLINE,"=",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field(),NEWLINE);                   //Natural: WRITE '=' MSG-INFO-SUB.##MSG / '=' ##ERROR-FIELD /
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Rc.greater(getZero())))                                                                                                                     //Natural: IF #RC GT 0
            {
                pnd_Rc.setValue(99);                                                                                                                                      //Natural: ASSIGN #RC := 99
                pnd_Msg.setValue("NFP Letter sent - other contracts still active");                                                                                       //Natural: ASSIGN #MSG := 'NFP Letter sent - other contracts still active'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Msg.setValue("NFP Letter sent - all contracts terminated");                                                                                           //Natural: ASSIGN #MSG := 'NFP Letter sent - all contracts terminated'
            }                                                                                                                                                             //Natural: END-IF
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TRANS-RCRD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-DC-NOTIFY-RECORD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME
        //* ***********************************************************************
        //*  JT0815 - START
        //*  READ(1) COR-XREF-PH BY COR-SUPER-PIN-RCDTYPE STARTING FROM #PIN-KEY
        //*    IF COR-XREF-PH.PH-UNIQUE-ID-NBR NE #PIN-NBR OR
        //*        COR-XREF-PH.PH-RCD-TYPE-CDE NE 01
        //*      ESCAPE BOTTOM
        //*    END-IF
        //*    #FNME := PH-FIRST-NME
        //*    PH-FIRST-NME := #FNME
        //*    #LNME := PH-LAST-NME
        //*    PH-LAST-NME := #LNME
        //*    #MNME := PH-MDDLE-NME
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-DC-CNTRCT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-REVERSAL
        //* ***********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-NO-FURTHER-PAYMENT-LETTER
        //* **********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-LETTER
        //* ***********************************************************************
        //* *PSTA9611.PIN-NBR := #PIN
        //* *PSTA9611.ADDRSS-LINE-1 := #O-BASE-ADDRESS-LINE-1
        //* *PSTA9611.ADDRSS-LINE-2 := #O-BASE-ADDRESS-LINE-2
        //* *PSTA9611.ADDRSS-LINE-3 := #O-BASE-ADDRESS-LINE-3
        //* *PSTA9611.ADDRSS-LINE-4 := #O-BASE-ADDRESS-LINE-4
        //* *PSTA9611.ADDRSS-LINE-5 := ADDRSS-LNE-5
        //* *PSTA9611.ADDRSS-LINE-6 := ADDRSS-LNE-6
        //* *PSTL6235-DATA.BENEFACTOR-NAME-SALU := #NAME
        //* *FOR #I = 1 TO #MAX
        //*  ADD 1 TO #J
        //*  PSTL6235-DATA.CONTRACT-DATA-A(#J) := #TXT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-OPEN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-WRITE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-CLOSE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BASE-ADDRESS
        //*  #ADDR1 := ADDRSS-LNE-1
        //*  #ADDR2 := ADDRSS-LNE-2
        //*  #ADDR3 := ADDRSS-LNE-3
        //*  #ADDR4 := ADDRSS-LNE-4
        //*  #ADDR5 := ADDRSS-LNE-5
        //*  #ZIP := ADDRSS-ZIP
        //* *PSTA9611.ADDRSS-TYP-CDE := ADDRSS-TYPE-CDE
        //* *PSTA9611.ADDRSS-PSTL-DTA := ADDRSS-POSTAL-DATA
        //*    PERFORM GET-CITY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CITY
        //*  JT0815 - END
    }
    private void sub_Create_Trans_Rcrd() throws Exception                                                                                                                 //Natural: CREATE-TRANS-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaal9125.getVw_iaa_Trans_Rcrd().reset();                                                                                                                       //Natural: RESET IAA-TRANS-RCRD
        ldaIaal9125.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr().setValue(pnd_W_Cntrct_Payee_Pnd_W_Cntrct);                                                                         //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #W-CNTRCT
        ldaIaal9125.getIaa_Trans_Rcrd_Trans_Payee_Cde().setValue(pnd_W_Cntrct_Payee_Pnd_W_Payee);                                                                         //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := #W-PAYEE
        ldaIaal9125.getIaa_Trans_Rcrd_Trans_Sub_Cde().setValue("001");                                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-SUB-CDE := '001'
        ldaIaal9125.getIaa_Trans_Rcrd_Trans_Cde().setValue(66);                                                                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 66
        ldaIaal9125.getIaa_Trans_Rcrd_Trans_Actvty_Cde().setValue("A");                                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
        pnd_W_Dte.setValueEdited(pls_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                                     //Natural: MOVE EDITED +CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #W-DTE
        pnd_W_Yyyymm.setValue(pnd_W_Dte.getSubstring(1,6));                                                                                                               //Natural: ASSIGN #W-YYYYMM := SUBSTR ( #W-DTE,1,6 )
        ldaIaal9125.getIaa_Trans_Rcrd_Trans_Check_Dte().compute(new ComputeParameters(false, ldaIaal9125.getIaa_Trans_Rcrd_Trans_Check_Dte()), pnd_W_Dte.val());          //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := VAL ( #W-DTE )
        ldaIaal9125.getIaa_Trans_Rcrd_Trans_Todays_Dte().setValue(pnd_T_Dte);                                                                                             //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-TODAYS-DTE := #T-DTE
        ldaIaal9125.getIaa_Trans_Rcrd_Trans_User_Area().setValue("BATCH");                                                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := 'BATCH'
        ldaIaal9125.getIaa_Trans_Rcrd_Trans_User_Id().setValue("BERWYN");                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := 'BERWYN'
        ldaIaal9125.getIaa_Trans_Rcrd_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-DTE := #TRANS-DTE
        ldaIaal9125.getIaa_Trans_Rcrd_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                            //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := #TRANS-DTE
        ldaIaal9125.getIaa_Trans_Rcrd_Invrse_Trans_Dte().setValue(pnd_Invrse_Trans_Dte);                                                                                  //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := #INVRSE-TRANS-DTE
        ldaIaal9125.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                                 //Natural: STORE IAA-TRANS-RCRD
        G1:                                                                                                                                                               //Natural: GET IAA-CNTRCT #CNTRCT-ISN
        ldaIaal9125.getVw_iaa_Cntrct().readByID(pnd_Cntrct_Isn.getLong(), "G1");
        ldaIaal9125.getVw_iaa_Cntrct_Trans().setValuesByName(ldaIaal9125.getVw_iaa_Trans_Rcrd());                                                                         //Natural: MOVE BY NAME IAA-TRANS-RCRD TO IAA-CNTRCT-TRANS
        ldaIaal9125.getVw_iaa_Cntrct_Trans().setValuesByName(ldaIaal9125.getVw_iaa_Cntrct());                                                                             //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
        ldaIaal9125.getIaa_Cntrct_Trans_Bfre_Imge_Id().setValue("1");                                                                                                     //Natural: ASSIGN IAA-CNTRCT-TRANS.BFRE-IMGE-ID := '1'
        ldaIaal9125.getIaa_Cntrct_Trans_Aftr_Imge_Id().setValue(" ");                                                                                                     //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID := ' '
        //*  BEFORE IMAGE
        //*  CONTRACT MASTER UPDATE OF DOD
        ldaIaal9125.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                               //Natural: STORE IAA-CNTRCT-TRANS
        ldaIaal9125.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().compute(new ComputeParameters(false, ldaIaal9125.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte()),                //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE := VAL ( #DOD-YM )
            pnd_Dod_Pnd_Dod_Ym.val());
        ldaIaal9125.getIaa_Cntrct_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                                //Natural: ASSIGN IAA-CNTRCT.LST-TRANS-DTE := #TRANS-DTE
        ldaIaal9125.getVw_iaa_Cntrct().updateDBRow("G1");                                                                                                                 //Natural: UPDATE ( G1. )
        ldaIaal9125.getVw_iaa_Cntrct_Trans().setValuesByName(ldaIaal9125.getVw_iaa_Cntrct());                                                                             //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
        ldaIaal9125.getIaa_Cntrct_Trans_Bfre_Imge_Id().setValue(" ");                                                                                                     //Natural: ASSIGN IAA-CNTRCT-TRANS.BFRE-IMGE-ID := ' '
        ldaIaal9125.getIaa_Cntrct_Trans_Aftr_Imge_Id().setValue("2");                                                                                                     //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID := '2'
        //*  AFTER IMAGE
        ldaIaal9125.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                               //Natural: STORE IAA-CNTRCT-TRANS
        G2:                                                                                                                                                               //Natural: GET CPR #CPR-ISN
        ldaIaal9125.getVw_cpr().readByID(pnd_Cpr_Isn.getLong(), "G2");
        ldaIaal9125.getVw_cpr_Trans().setValuesByName(ldaIaal9125.getVw_iaa_Trans_Rcrd());                                                                                //Natural: MOVE BY NAME IAA-TRANS-RCRD TO CPR-TRANS
        ldaIaal9125.getVw_cpr_Trans().setValuesByName(ldaIaal9125.getVw_cpr());                                                                                           //Natural: MOVE BY NAME CPR TO CPR-TRANS
        ldaIaal9125.getCpr_Trans_Bfre_Imge_Id().setValue("1");                                                                                                            //Natural: ASSIGN CPR-TRANS.BFRE-IMGE-ID := '1'
        ldaIaal9125.getCpr_Trans_Aftr_Imge_Id().setValue(" ");                                                                                                            //Natural: ASSIGN CPR-TRANS.AFTR-IMGE-ID := ' '
        //*  BEFORE IMAGE
        //*  TERMINATE CPR RECORD
        ldaIaal9125.getVw_cpr_Trans().insertDBRow();                                                                                                                      //Natural: STORE CPR-TRANS
        ldaIaal9125.getCpr_Cntrct_Actvty_Cde().setValue(9);                                                                                                               //Natural: ASSIGN CPR.CNTRCT-ACTVTY-CDE := 9
        ldaIaal9125.getCpr_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                                       //Natural: ASSIGN CPR.LST-TRANS-DTE := #TRANS-DTE
        ldaIaal9125.getCpr_Cntrct_Lst_Chnge_Dte().compute(new ComputeParameters(false, ldaIaal9125.getCpr_Cntrct_Lst_Chnge_Dte()), pnd_W_Yyyymm.val());                   //Natural: ASSIGN CPR.CNTRCT-LST-CHNGE-DTE := VAL ( #W-YYYYMM )
        ldaIaal9125.getVw_cpr().updateDBRow("G2");                                                                                                                        //Natural: UPDATE ( G2. )
        ldaIaal9125.getVw_cpr_Trans().setValuesByName(ldaIaal9125.getVw_cpr());                                                                                           //Natural: MOVE BY NAME CPR TO CPR-TRANS
        ldaIaal9125.getCpr_Trans_Bfre_Imge_Id().setValue(" ");                                                                                                            //Natural: ASSIGN CPR-TRANS.BFRE-IMGE-ID := ' '
        ldaIaal9125.getCpr_Trans_Aftr_Imge_Id().setValue("2");                                                                                                            //Natural: ASSIGN CPR-TRANS.AFTR-IMGE-ID := '2'
        //*  AFTER IMAGE
        ldaIaal9125.getVw_cpr_Trans().insertDBRow();                                                                                                                      //Natural: STORE CPR-TRANS
        ldaIaal9125.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #W-CNTRCT-PAYEE
        (
        "R3",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_W_Cntrct_Payee, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R3:
        while (condition(ldaIaal9125.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R3")))
        {
            if (condition(ldaIaal9125.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().notEquals(pnd_W_Cntrct_Payee_Pnd_W_Cntrct) || ldaIaal9125.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().notEquals(pnd_W_Cntrct_Payee_Pnd_W_Payee))) //Natural: IF TIAA-CNTRCT-PPCN-NBR NE #W-CNTRCT OR TIAA-CNTRCT-PAYEE-CDE NE #W-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal9125.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaIaal9125.getVw_iaa_Trans_Rcrd());                                                                  //Natural: MOVE BY NAME IAA-TRANS-RCRD TO IAA-TIAA-FUND-TRANS
            ldaIaal9125.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaIaal9125.getVw_iaa_Tiaa_Fund_Rcrd());                                                              //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
            ldaIaal9125.getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id().setValue("1");                                                                                              //Natural: ASSIGN IAA-TIAA-FUND-TRANS.BFRE-IMGE-ID := '1'
            ldaIaal9125.getIaa_Tiaa_Fund_Trans_Aftr_Imge_Id().setValue(" ");                                                                                              //Natural: ASSIGN IAA-TIAA-FUND-TRANS.AFTR-IMGE-ID := ' '
            //*  BEFORE IMAGE
            ldaIaal9125.getVw_iaa_Tiaa_Fund_Trans().insertDBRow();                                                                                                        //Natural: STORE IAA-TIAA-FUND-TRANS
            G4:                                                                                                                                                           //Natural: GET IAA-TIAA-FUND-RCRD *ISN ( R3. )
            ldaIaal9125.getVw_iaa_Tiaa_Fund_Rcrd().readByID(ldaIaal9125.getVw_iaa_Tiaa_Fund_Rcrd().getAstISN("R3"), "G4");
            ldaIaal9125.getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                    //Natural: ASSIGN IAA-TIAA-FUND-RCRD.LST-TRANS-DTE := #TRANS-DTE
            ldaIaal9125.getVw_iaa_Tiaa_Fund_Rcrd().updateDBRow("G4");                                                                                                     //Natural: UPDATE ( G4. )
            ldaIaal9125.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaIaal9125.getVw_iaa_Tiaa_Fund_Rcrd());                                                              //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
            ldaIaal9125.getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id().setValue(" ");                                                                                              //Natural: ASSIGN IAA-TIAA-FUND-TRANS.BFRE-IMGE-ID := ' '
            ldaIaal9125.getIaa_Tiaa_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                              //Natural: ASSIGN IAA-TIAA-FUND-TRANS.AFTR-IMGE-ID := '2'
            //*  AFTER IMAGE
            ldaIaal9125.getVw_iaa_Tiaa_Fund_Trans().insertDBRow();                                                                                                        //Natural: STORE IAA-TIAA-FUND-TRANS
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Create_Dc_Notify_Record() throws Exception                                                                                                           //Natural: CREATE-DC-NOTIFY-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Id_Nbr().setValue(pnd_Pin);                                                                                             //Natural: ASSIGN STTLMNT-ID-NBR := #PIN
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Tax_Id_Nbr().setValue(pnd_Ssn_A_Pnd_Ssn);                                                                               //Natural: ASSIGN STTLMNT-TAX-ID-NBR := #SSN
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Req_Seq_Nbr().setValue(1);                                                                                              //Natural: ASSIGN STTLMNT-REQ-SEQ-NBR := 1
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Process_Type().setValue("ID1A");                                                                                        //Natural: ASSIGN STTLMNT-PROCESS-TYPE := 'ID1A'
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Timestamp().setValue(pnd_Trans_Dte);                                                                                    //Natural: ASSIGN STTLMNT-TIMESTAMP := #TRANS-DTE
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Status_Cde().setValue(99);                                                                                              //Natural: ASSIGN STTLMNT-STATUS-CDE := 99
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Status_Timestamp().setValue(pnd_Trans_Dte);                                                                             //Natural: ASSIGN STTLMNT-STATUS-TIMESTAMP := #TRANS-DTE
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Cwf_Wpid().setValue("TAIDIN");                                                                                          //Natural: ASSIGN STTLMNT-CWF-WPID := 'TAIDIN'
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Name().setValue(pnd_Name);                                                                                     //Natural: ASSIGN STTLMNT-DECEDENT-NAME := #NAME
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Dod_Dte().setValue(pnd_Dod);                                                                                            //Natural: ASSIGN STTLMNT-DOD-DTE := #DOD
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Name().setValue(DbsUtil.compress("Estate of", pnd_Name));                                                      //Natural: COMPRESS 'Estate of' #NAME INTO STTLMNT-NOTIFIER-NAME
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr1().setValue(pnd_Addr_Lines_Pnd_Addr1);                                                                    //Natural: ASSIGN STTLMNT-NOTIFIER-ADDR1 := #ADDR1
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr2().setValue(pnd_Addr_Lines_Pnd_Addr2);                                                                    //Natural: ASSIGN STTLMNT-NOTIFIER-ADDR2 := #ADDR2
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr3().setValue(pnd_Addr_Lines_Pnd_Addr3);                                                                    //Natural: ASSIGN STTLMNT-NOTIFIER-ADDR3 := #ADDR3
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr4().setValue(pnd_Addr_Lines_Pnd_Addr4);                                                                    //Natural: ASSIGN STTLMNT-NOTIFIER-ADDR4 := #ADDR4
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_City().setValue(pnd_City);                                                                                     //Natural: ASSIGN STTLMNT-NOTIFIER-CITY := #CITY
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_State().setValue(pnd_State);                                                                                   //Natural: ASSIGN STTLMNT-NOTIFIER-STATE := #STATE
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Zip().setValue(pnd_Zip);                                                                                       //Natural: ASSIGN STTLMNT-NOTIFIER-ZIP := #ZIP
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notify_Date().setValue(pnd_T_Dte);                                                                                      //Natural: ASSIGN STTLMNT-NOTIFY-DATE := #T-DTE
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Rltnshp().setValue("Estate");                                                                                  //Natural: ASSIGN STTLMNT-NOTIFIER-RLTNSHP := 'Estate'
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Oper_Id().setValue("BERWYN");                                                                                                   //Natural: ASSIGN OPER-ID := 'BERWYN'
        ldaIaal9125.getVw_iaa_Dc_Sttlmnt_Req().insertDBRow();                                                                                                             //Natural: STORE IAA-DC-STTLMNT-REQ
    }
    //*  082017
    private void sub_Get_Name() throws Exception                                                                                                                          //Natural: GET-NAME
    {
        if (BLNatReinput.isReinput()) return;

        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Pin);                                                                                                      //Natural: ASSIGN #I-PIN-N12 := #PIN
        //* *#PIN-NBR := #PIN
        //* *#REC-TYPE := 01
        //* *IF #PIN-NBR NE 9999999
        //*  082017
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().notEquals(new DbsDecimal("999999999999"))))                                                               //Natural: IF #I-PIN-N12 NE 999999999999
        {
            //* *CALLNAT 'MDMN100A' #MDMA100                    /* 082017
            //*  082017
            DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                      //Natural: CALLNAT 'MDMN101A' #MDMA101
            if (condition(Global.isEscape())) return;
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().notEquals("0000")))                                                                               //Natural: IF #O-RETURN-CODE NE '0000'
            {
                pnd_Rc.setValue(10);                                                                                                                                      //Natural: ASSIGN #RC := 10
                pnd_Msg.setValue("PIN not on COR");                                                                                                                       //Natural: ASSIGN #MSG := 'PIN not on COR'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fnme.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name());                                                                                              //Natural: ASSIGN #FNME := #O-FIRST-NAME
            DbsUtil.examine(new ExamineSource(pnd_Fnme_Pnd_F_Rest), new ExamineTranslate(TranslateOption.Lower));                                                         //Natural: EXAMINE #F-REST TRANSLATE INTO LOWER CASE
            pnd_Lnme.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name());                                                                                               //Natural: ASSIGN #LNME := #O-LAST-NAME
            DbsUtil.examine(new ExamineSource(pnd_Lnme_Pnd_L_Rest), new ExamineTranslate(TranslateOption.Lower));                                                         //Natural: EXAMINE #L-REST TRANSLATE INTO LOWER CASE
            pnd_Mnme.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name());                                                                                             //Natural: ASSIGN #MNME := #O-MIDDLE-NAME
            DbsUtil.examine(new ExamineSource(pnd_Mnme_Pnd_M_Rest), new ExamineTranslate(TranslateOption.Lower));                                                         //Natural: EXAMINE #M-REST TRANSLATE INTO LOWER CASE
            //*    PH-MDDLE-NME := #MNME
            //*    COMPRESS PH-FIRST-NME PH-MDDLE-NME PH-LAST-NME INTO #NAME
            pnd_Name.setValue(DbsUtil.compress(pnd_Fnme, pnd_Mnme, pnd_Lnme));                                                                                            //Natural: COMPRESS #FNME #MNME #LNME INTO #NAME
            DbsUtil.examine(new ExamineSource(pnd_Name), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                                       //Natural: EXAMINE #NAME FOR ' ' GIVING LENGTH #LEN
            if (condition(pnd_Len.greater(25)))                                                                                                                           //Natural: IF #LEN GT 25
            {
                //*      COMPRESS PH-FIRST-NME PH-MDDLE-INIT PH-LAST-NME INTO #NAME2
                pnd_Name2.setValue(DbsUtil.compress(pnd_Fnme, pnd_Mnme_Pnd_M_Init, pnd_Lnme));                                                                            //Natural: COMPRESS #FNME #M-INIT #LNME INTO #NAME2
                DbsUtil.examine(new ExamineSource(pnd_Name2), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                                  //Natural: EXAMINE #NAME2 FOR ' ' GIVING LENGTH #LEN
                if (condition(pnd_Len.greater(25)))                                                                                                                       //Natural: IF #LEN GT 25
                {
                    //*        COMPRESS PH-FIRST-NME PH-LAST-NME INTO #NAME2
                    pnd_Name2.setValue(DbsUtil.compress(pnd_Fnme, pnd_Lnme));                                                                                             //Natural: COMPRESS #FNME #LNME INTO #NAME2
                    DbsUtil.examine(new ExamineSource(pnd_Name2), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                              //Natural: EXAMINE #NAME2 FOR ' ' GIVING LENGTH #LEN
                    if (condition(pnd_Len.greater(25)))                                                                                                                   //Natural: IF #LEN GT 25
                    {
                        //*          COMPRESS PH-FIRST-INIT PH-MDDLE-INIT PH-LAST-NME INTO #NAME2
                        pnd_Name2.setValue(DbsUtil.compress(pnd_Fnme_Pnd_F_Init, pnd_Mnme_Pnd_M_Init, pnd_Lnme));                                                         //Natural: COMPRESS #F-INIT #M-INIT #LNME INTO #NAME2
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Name2.setValue(pnd_Name);                                                                                                                             //Natural: ASSIGN #NAME2 := #NAME
            }                                                                                                                                                             //Natural: END-IF
            pnd_Name.setValue(pnd_Name2);                                                                                                                                 //Natural: ASSIGN #NAME := #NAME2
            //*  END-READ
            //*  JT0815 END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Name.equals(" ")))                                                                                                                              //Natural: IF #NAME = ' '
        {
            pnd_Rc.setValue(10);                                                                                                                                          //Natural: ASSIGN #RC := 10
            pnd_Msg.setValue("PIN not on COR");                                                                                                                           //Natural: ASSIGN #MSG := 'PIN not on COR'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Dc_Cntrct() throws Exception                                                                                                                  //Natural: CREATE-DC-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_I.equals(1)))                                                                                                                                   //Natural: IF #I = 1
        {
            ldaIaal9125.getIaa_Dc_Cntrct_Cntrct_Id_Nbr().setValue(pnd_Pin);                                                                                               //Natural: ASSIGN CNTRCT-ID-NBR := #PIN
            ldaIaal9125.getIaa_Dc_Cntrct_Cntrct_Tax_Id_Nbr().setValue(pnd_Ssn_A_Pnd_Ssn);                                                                                 //Natural: ASSIGN CNTRCT-TAX-ID-NBR := #SSN
            ldaIaal9125.getIaa_Dc_Cntrct_Cntrct_Process_Type().setValue("ID1A");                                                                                          //Natural: ASSIGN CNTRCT-PROCESS-TYPE := 'ID1A'
            ldaIaal9125.getIaa_Dc_Cntrct_Cntrct_Req_Seq_Nbr().setValue(1);                                                                                                //Natural: ASSIGN CNTRCT-REQ-SEQ-NBR := 1
            ldaIaal9125.getIaa_Dc_Cntrct_Cntrct_Timestamp().setValue(pnd_Trans_Dte);                                                                                      //Natural: ASSIGN CNTRCT-TIMESTAMP := #TRANS-DTE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal9125.getVw_iaa_Cntrct().startDatabaseRead                                                                                                                  //Natural: READ ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #W-CNTRCT
        (
        "R1",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", pnd_W_Cntrct_Payee_Pnd_W_Cntrct, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") },
        1
        );
        R1:
        while (condition(ldaIaal9125.getVw_iaa_Cntrct().readNextRow("R1")))
        {
            pnd_Cntrct_Isn.setValue(ldaIaal9125.getVw_iaa_Cntrct().getAstISN("R1"));                                                                                      //Natural: ASSIGN #CNTRCT-ISN := *ISN ( R1. )
            ldaIaal9125.getVw_iaa_Dc_Cntrct().setValuesByName(ldaIaal9125.getVw_iaa_Cntrct());                                                                            //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-DC-CNTRCT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal9125.getVw_cpr().startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY STARTING FROM #W-CNTRCT-PAYEE
        (
        "R2",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_W_Cntrct_Payee, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        R2:
        while (condition(ldaIaal9125.getVw_cpr().readNextRow("R2")))
        {
            pnd_Cpr_Isn.setValue(ldaIaal9125.getVw_cpr().getAstISN("R2"));                                                                                                //Natural: ASSIGN #CPR-ISN := *ISN ( R2. )
            ldaIaal9125.getVw_iaa_Dc_Cntrct().setValuesByName(ldaIaal9125.getVw_cpr());                                                                                   //Natural: MOVE BY NAME CPR TO IAA-DC-CNTRCT
            ldaIaal9125.getIaa_Dc_Cntrct_Cntrct_Payee_Cde().setValue(ldaIaal9125.getCpr_Cntrct_Part_Payee_Cde());                                                         //Natural: ASSIGN CNTRCT-PAYEE-CDE := CNTRCT-PART-PAYEE-CDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal9125.getIaa_Dc_Cntrct_Cntrct_Status_Cde().setValue(99);                                                                                                    //Natural: ASSIGN CNTRCT-STATUS-CDE := 99
        ldaIaal9125.getIaa_Dc_Cntrct_Cntrct_Status_Timestamp().setValue(pnd_Trans_Dte);                                                                                   //Natural: ASSIGN CNTRCT-STATUS-TIMESTAMP := #TRANS-DTE
        ldaIaal9125.getIaa_Dc_Cntrct_Cntrct_Annt_Type().setValue("1ST");                                                                                                  //Natural: ASSIGN CNTRCT-ANNT-TYPE := '1ST'
        ldaIaal9125.getIaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_Cde().getValue(1).setValue("D032");                                                                              //Natural: ASSIGN CNTRCT-STTLMNT-INFO-CDE ( 1 ) := 'D032'
        ldaIaal9125.getIaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_Cde().getValue(2).setValue("D001");                                                                              //Natural: ASSIGN CNTRCT-STTLMNT-INFO-CDE ( 2 ) := 'D001'
        ldaIaal9125.getVw_iaa_Dc_Cntrct().insertDBRow();                                                                                                                  //Natural: STORE IAA-DC-CNTRCT
    }
    private void sub_Check_For_Reversal() throws Exception                                                                                                                //Natural: CHECK-FOR-REVERSAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr.setValue(pnd_W_Cntrct_Payee_Pnd_W_Cntrct);                                                                                //Natural: ASSIGN #TRANS-PPCN-NBR := #W-CNTRCT
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde.setValue(pnd_W_Cntrct_Payee_Pnd_W_Payee);                                                                                //Natural: ASSIGN #TRANS-PAYEE-CDE := #W-PAYEE
        ldaIaal9125.getVw_iaa_Trans_Rcrd().startDatabaseRead                                                                                                              //Natural: READ IAA-TRANS-RCRD BY TRANS-CNTRCT-KEY STARTING FROM #TRANS-CNTRCT-KEY
        (
        "READ03",
        new Wc[] { new Wc("TRANS_CNTRCT_KEY", ">=", pnd_Trans_Cntrct_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CNTRCT_KEY", "ASC") }
        );
        READ03:
        while (condition(ldaIaal9125.getVw_iaa_Trans_Rcrd().readNextRow("READ03")))
        {
            if (condition(ldaIaal9125.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr().notEquals(pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr) || ldaIaal9125.getIaa_Trans_Rcrd_Trans_Payee_Cde().notEquals(pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde))) //Natural: IF TRANS-PPCN-NBR NE #TRANS-PPCN-NBR OR TRANS-PAYEE-CDE NE #TRANS-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!((ldaIaal9125.getIaa_Trans_Rcrd_Trans_Cde().equals(66) && ldaIaal9125.getIaa_Trans_Rcrd_Trans_Actvty_Cde().equals("R")) ||                     //Natural: ACCEPT IF ( TRANS-CDE = 66 AND TRANS-ACTVTY-CDE = 'R' ) OR TRANS-CDE = 37
                ldaIaal9125.getIaa_Trans_Rcrd_Trans_Cde().equals(37))))
            {
                continue;
            }
            pnd_Rvrsl.setValue(true);                                                                                                                                     //Natural: ASSIGN #RVRSL := TRUE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Create_No_Further_Payment_Letter() throws Exception                                                                                                  //Natural: CREATE-NO-FURTHER-PAYMENT-LETTER
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa9020.getEfsa9020_Action().setValue("AR");                                                                                                                  //Natural: ASSIGN EFSA9020.ACTION := 'AR'
        pdaEfsa9020.getEfsa9020_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: ASSIGN EFSA9020.CABINET-PREFIX := 'P'
        pdaEfsa9020.getEfsa9020_Pin_Nbr().setValue(pnd_Pin);                                                                                                              //Natural: ASSIGN EFSA9020.PIN-NBR := #PIN
        pdaEfsa9020.getEfsa9020_Tiaa_Rcvd_Dte().getValue(1).setValue(Global.getDATN());                                                                                   //Natural: ASSIGN EFSA9020.TIAA-RCVD-DTE ( 1 ) := *DATN
        pdaEfsa9020.getEfsa9020_Empl_Oprtr_Cde().getValue(1).setValue(" ");                                                                                               //Natural: ASSIGN EFSA9020.EMPL-OPRTR-CDE ( 1 ) := ' '
        pdaEfsa9020.getEfsa9020_Wpid().getValue(1).setValue("TAIDIN");                                                                                                    //Natural: ASSIGN EFSA9020.WPID ( 1 ) := 'TAIDIN'
        pdaEfsa9020.getEfsa9020_Unit_Cde().getValue(1).setValue("BPSSFWC");                                                                                               //Natural: ASSIGN EFSA9020.UNIT-CDE ( 1 ) := 'BPSSFWC'
        pdaEfsa9020.getEfsa9020_Status_Cde().getValue(1).setValue("C003");                                                                                                //Natural: ASSIGN EFSA9020.STATUS-CDE ( 1 ) := 'C003'
        pdaEfsa9020.getEfsa9020_Corp_Status().getValue(1).setValue(0);                                                                                                    //Natural: ASSIGN EFSA9020.CORP-STATUS ( 1 ) := 0
        pdaEfsa9020.getEfsa9020_Rqst_Origin_Cde().setValue("M");                                                                                                          //Natural: ASSIGN EFSA9020.RQST-ORIGIN-CDE := 'M'
        pdaEfsa9020.getEfsa9020_Rqst_Entry_Op_Cde().setValue("BATCHCWF");                                                                                                 //Natural: ASSIGN EFSA9020.RQST-ENTRY-OP-CDE := 'BATCHCWF'
        pdaEfsa9020.getEfsa9020_Rqst_Origin_Unit_Cde().setValue("BPSSFWC");                                                                                               //Natural: ASSIGN EFSA9020.RQST-ORIGIN-UNIT-CDE := 'BPSSFWC'
        pdaEfsa9020.getEfsa9020_System().setValue("MIT");                                                                                                                 //Natural: ASSIGN EFSA9020.SYSTEM := 'MIT'
        DbsUtil.callnat(Efsn9020.class , getCurrentProcessState(), pdaEfsa9020.getEfsa9020(), pdaEfsa9020.getEfsa9020_Id(), pdaEfsa902r.getEfsa902r(),                    //Natural: CALLNAT 'EFSN9020' EFSA9020 EFSA9020-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        pnd_Pin_History_Key_Pnd_Pin_N.setValue(pdaEfsa9020.getEfsa9020_Pin_Nbr());                                                                                        //Natural: ASSIGN #PIN-N := EFSA9020.PIN-NBR
        pnd_Pin_History_Key_Pnd_Crprte_Status_Ind.setValue("0");                                                                                                          //Natural: ASSIGN #CRPRTE-STATUS-IND := '0'
        pnd_Pin_History_Key_Pnd_Work_Prcss_Id.setValue("TAIDIN");                                                                                                         //Natural: ASSIGN #WORK-PRCSS-ID := 'TAIDIN'
        vw_cwf_Mit_Sub.startDatabaseRead                                                                                                                                  //Natural: READ ( 1 ) CWF-MIT-SUB BY PIN-HISTORY-KEY = #PIN-HISTORY-KEY
        (
        "READ04",
        new Wc[] { new Wc("PIN_HISTORY_KEY", ">=", pnd_Pin_History_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_HISTORY_KEY", "ASC") },
        1
        );
        READ04:
        while (condition(vw_cwf_Mit_Sub.readNextRow("READ04")))
        {
            if (condition(cwf_Mit_Sub_Work_Prcss_Id.equals(pnd_Pin_History_Key_Pnd_Work_Prcss_Id)))                                                                       //Natural: IF CWF-MIT-SUB.WORK-PRCSS-ID = #WORK-PRCSS-ID
            {
                pnd_Rqst_Log_Dte_Tme.setValue(cwf_Mit_Sub_Rqst_Log_Dte_Tme);                                                                                              //Natural: MOVE CWF-MIT-SUB.RQST-LOG-DTE-TME TO #RQST-LOG-DTE-TME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ") ||                     //Natural: IF MSG-INFO-SUB.##MSG NE ' ' OR ##ERROR-FIELD NE ' ' OR ##MSG-NR NE 0
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().notEquals(getZero())))
        {
            pnd_Rc.setValue(7);                                                                                                                                           //Natural: ASSIGN #RC := 07
            pnd_Msg.setValue("Error in CWF when logging TAIDIN");                                                                                                         //Natural: ASSIGN #MSG := 'Error in CWF when logging TAIDIN'
            pnd_Msg.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                                  //Natural: ASSIGN #MSG := MSG-INFO-SUB.##MSG
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-LETTER
            sub_Print_Letter();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Error.getBoolean()))                                                                                                                        //Natural: IF #ERROR
            {
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Letter() throws Exception                                                                                                                      //Natural: PRINT-LETTER
    {
        if (BLNatReinput.isReinput()) return;

        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PTIANBEN");                                                                                                         //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PTIANBEN'
        pdaPsta9612.getPsta9612_Univ_Id().setValue(pnd_Pin);                                                                                                              //Natural: ASSIGN PSTA9612.UNIV-ID := #PIN
        pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                              //Natural: ASSIGN PSTA9612.UNIV-ID-TYP := 'P'
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("IAIQ");                                                                                                          //Natural: ASSIGN PSTA9612.SYSTM-ID-CDE := 'IAIQ'
        pdaPsta9612.getPsta9612_Prntr_Id_Cde().setValue(" ");                                                                                                             //Natural: ASSIGN PSTA9612.PRNTR-ID-CDE := ' '
        pdaPsta9612.getPsta9612_Dont_Use_Finalist().setValue(false);                                                                                                      //Natural: ASSIGN PSTA9612.DONT-USE-FINALIST := FALSE
        pdaPsta9612.getPsta9612_Addrss_Lines_Rule().setValue(false);                                                                                                      //Natural: ASSIGN PSTA9612.ADDRSS-LINES-RULE := FALSE
        pdaPsta9612.getPsta9612_Next_Bsnss_Day_Ind().setValue(true);                                                                                                      //Natural: ASSIGN PSTA9612.NEXT-BSNSS-DAY-IND := TRUE
        pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(false);                                                                                                    //Natural: ASSIGN PSTA9612.NO-UPDTE-TO-MIT-IND := FALSE
        pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(false);                                                                                                    //Natural: ASSIGN PSTA9612.NO-UPDTE-TO-EFM-IND := FALSE
        pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().setValue(" ");                                                                                                          //Natural: ASSIGN PSTA9612.PCKGE-DLVRY-TYP := ' '
        pdaPsta9612.getPsta9612_Last_Nme().setValue(" ");                                                                                                                 //Natural: ASSIGN PSTA9612.LAST-NME := ' '
        pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue(" ");                                                                                                           //Natural: ASSIGN PSTA9612.ADDRSS-TYP-CDE := ' '
        pdaPsta9612.getPsta9612_Lttr_Slttn_Txt().setValue(" ");                                                                                                           //Natural: ASSIGN PSTA9612.LTTR-SLTTN-TXT := ' '
        pdaPsta9612.getPsta9612_Full_Nme().setValue(ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Name());                                                           //Natural: ASSIGN PSTA9612.FULL-NME := STTLMNT-NOTIFIER-NAME
        pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1());                                                          //Natural: ASSIGN PSTA9612.ADDRSS-LINE-1 := #O-BASE-ADDRESS-LINE-1
        pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2());                                                          //Natural: ASSIGN PSTA9612.ADDRSS-LINE-2 := #O-BASE-ADDRESS-LINE-2
        pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3());                                                          //Natural: ASSIGN PSTA9612.ADDRSS-LINE-3 := #O-BASE-ADDRESS-LINE-3
        pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_4());                                                          //Natural: ASSIGN PSTA9612.ADDRSS-LINE-4 := #O-BASE-ADDRESS-LINE-4
        pdaPsta9612.getPsta9612_Print_Fclty_Cde().setValue("F");                                                                                                          //Natural: ASSIGN PSTA9612.PRINT-FCLTY-CDE := 'F'
        pdaPsta9612.getPsta9612_Form_Lbrry_Id().setValue(" ");                                                                                                            //Natural: ASSIGN PSTA9612.FORM-LBRRY-ID := ' '
        pdaPsta9612.getPsta9612_Fnshng_Cde().setValue(" ");                                                                                                               //Natural: ASSIGN PSTA9612.FNSHNG-CDE := ' '
        pdaPsta9612.getPsta9612_Rqst_Log_Dte_Tme().getValue(1).setValue(pnd_Rqst_Log_Dte_Tme);                                                                            //Natural: ASSIGN PSTA9612.RQST-LOG-DTE-TME ( 1 ) := #RQST-LOG-DTE-TME
        pdaPsta9612.getPsta9612_Work_Prcss_Id().getValue(1).setValue("TAIDIN");                                                                                           //Natural: ASSIGN PSTA9612.WORK-PRCSS-ID ( 1 ) := 'TAIDIN'
        pnd_W_Cntrct_Payee.setValue(pnd_Cntrct_Payee.getValue(1));                                                                                                        //Natural: ASSIGN #W-CNTRCT-PAYEE := #CNTRCT-PAYEE ( 1 )
        pdaPsta9612.getPsta9612_Mit_Contract_Nbr().getValue(1,1).setValue(pnd_W_Cntrct_Payee_Pnd_W_Cntrct);                                                               //Natural: ASSIGN PSTA9612.MIT-CONTRACT-NBR ( 1,1 ) := #W-CNTRCT
        pdaPsta9612.getPsta9612_Wpid_Vldte_Ind().getValue(1).setValue("Y");                                                                                               //Natural: ASSIGN PSTA9612.WPID-VLDTE-IND ( 1 ) := 'Y'
        pdaPsta9612.getPsta9612_Status_Cde().getValue(1).setValue("8020");                                                                                                //Natural: ASSIGN PSTA9612.STATUS-CDE ( 1 ) := '8020'
        DbsUtil.callnat(Pstn9525.class , getCurrentProcessState(), pdaPsta9612.getPsta9612_Letter_Dte(), pdaCwfpda_M.getMsg_Info_Sub());                                  //Natural: CALLNAT 'PSTN9525' PSTA9612.LETTER-DTE MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        ldaPstl6235.getPstl6235_Data_Benefactor_Name_Addr().setValue(ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Name());                                          //Natural: ASSIGN PSTL6235-DATA.BENEFACTOR-NAME-ADDR := STTLMNT-NOTIFIER-NAME
        ldaPstl6235.getPstl6235_Data_Benefactor_Name_Salu().setValue(ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Name());                                          //Natural: ASSIGN PSTL6235-DATA.BENEFACTOR-NAME-SALU := STTLMNT-NOTIFIER-NAME
        ldaPstl6235.getPstl6235_Data_Deceased_Name1().setValue(ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Name());                                                //Natural: ASSIGN PSTL6235-DATA.DECEASED-NAME1 := STTLMNT-DECEDENT-NAME
        //*  ONLY PROCESS THE FIRST 10 CONTRACTS - MAX POST
        ldaPstl6235.getPstl6235_Data_Deceased_Name1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaPstl6235.getPstl6235_Data_Deceased_Name1(),             //Natural: COMPRESS PSTL6235-DATA.DECEASED-NAME1 ', deceased' INTO PSTL6235-DATA.DECEASED-NAME1 LEAVING NO SPACE
            ", deceased"));
        ldaPstl6235.getPstl6235_Data_Deceased_Name_Text1().setValue(ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Name());                                           //Natural: ASSIGN PSTL6235-DATA.DECEASED-NAME-TEXT1 := STTLMNT-DECEDENT-NAME
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            //*  CAN HANDLE
            if (condition(pnd_Cntrct_Payee.getValue(pnd_I).notEquals(" ")))                                                                                               //Natural: IF #CNTRCT-PAYEE ( #I ) NE ' '
            {
                pnd_W_Cntrct_Payee.setValue(pnd_Cntrct_Payee.getValue(pnd_I));                                                                                            //Natural: ASSIGN #W-CNTRCT-PAYEE := #CNTRCT-PAYEE ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition((DbsUtil.maskMatches(pnd_W_Cntrct_Payee_Pnd_W_Cntrct,"'Z'") || (((DbsUtil.maskMatches(pnd_W_Cntrct_Payee_Pnd_W_Cntrct,"N") &&                   //Natural: IF #W-CNTRCT = MASK ( 'Z' ) OR ( #W-CNTRCT = MASK ( N ) AND #W-CNTRCT NE MASK ( '6L7' ) AND #W-CNTRCT NE MASK ( '6M7' ) AND #W-CNTRCT NE MASK ( '6N7' ) )
                ! (DbsUtil.maskMatches(pnd_W_Cntrct_Payee_Pnd_W_Cntrct,"'6L7'"))) && ! (DbsUtil.maskMatches(pnd_W_Cntrct_Payee_Pnd_W_Cntrct,"'6M7'"))) && 
                ! (DbsUtil.maskMatches(pnd_W_Cntrct_Payee_Pnd_W_Cntrct,"'6N7'"))))))
            {
                pnd_Txt.setValue("CREF Certificate No.");                                                                                                                 //Natural: ASSIGN #TXT := 'CREF Certificate No.'
                pnd_Cref.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CREF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Txt.setValue("TIAA Contract No.");                                                                                                                    //Natural: ASSIGN #TXT := 'TIAA Contract No.'
                pnd_Tiaa.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TIAA
            }                                                                                                                                                             //Natural: END-IF
            pnd_Txt_Pnd_Cntrct.setValueEdited(pnd_W_Cntrct_Payee_Pnd_W_Cntrct,new ReportEditMask("XXXXXXX'-'X"));                                                         //Natural: MOVE EDITED #W-CNTRCT ( EM = XXXXXXX'-'X ) TO #CNTRCT
            ldaPstl6235.getPstl6235_Data_Contract_Data_A().getValue(pnd_I).setValue(pnd_Txt);                                                                             //Natural: ASSIGN PSTL6235-DATA.CONTRACT-DATA-A ( #I ) := #TXT
            //*  ????
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaPsta9612.getPsta9612_Empl_Sgntry_Unit_Cde().setValue("BPSSFWC");                                                                                               //Natural: ASSIGN PSTA9612.EMPL-SGNTRY-UNIT-CDE := 'BPSSFWC'
        pdaPsta9612.getPsta9612_Empl_Sgntry_Oprtr_Cde().setValue(" ");                                                                                                    //Natural: ASSIGN PSTA9612.EMPL-SGNTRY-OPRTR-CDE := ' '
        pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().setValue("Beneficiary Relationship Team");                                                                              //Natural: ASSIGN PSTA9612.EMPL-SGNTRY-NME := 'Beneficiary Relationship Team'
        if (condition(pnd_Max.greater(1)))                                                                                                                                //Natural: IF #MAX GT 1
        {
            ldaPstl6235.getPstl6235_Data_Have_Has_Text().setValue("have");                                                                                                //Natural: ASSIGN PSTL6235-DATA.HAVE-HAS-TEXT := 'have'
            short decideConditionsMet2275 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TIAA = 0
            if (condition(pnd_Tiaa.equals(getZero())))
            {
                decideConditionsMet2275++;
                ldaPstl6235.getPstl6235_Data_Contract_Certificate_Text().setValue("certificates");                                                                        //Natural: ASSIGN PSTL6235-DATA.CONTRACT-CERTIFICATE-TEXT := 'certificates'
            }                                                                                                                                                             //Natural: WHEN #CREF = 0
            else if (condition(pnd_Cref.equals(getZero())))
            {
                decideConditionsMet2275++;
                ldaPstl6235.getPstl6235_Data_Contract_Certificate_Text().setValue("contracts");                                                                           //Natural: ASSIGN PSTL6235-DATA.CONTRACT-CERTIFICATE-TEXT := 'contracts'
            }                                                                                                                                                             //Natural: WHEN #TIAA = 1 AND #CREF = 1
            else if (condition(pnd_Tiaa.equals(1) && pnd_Cref.equals(1)))
            {
                decideConditionsMet2275++;
                ldaPstl6235.getPstl6235_Data_Contract_Certificate_Text().setValue("contract and certificate");                                                            //Natural: ASSIGN PSTL6235-DATA.CONTRACT-CERTIFICATE-TEXT := 'contract and certificate'
            }                                                                                                                                                             //Natural: WHEN #TIAA GT 1 AND #CREF GT 1
            else if (condition(pnd_Tiaa.greater(1) && pnd_Cref.greater(1)))
            {
                decideConditionsMet2275++;
                ldaPstl6235.getPstl6235_Data_Contract_Certificate_Text().setValue("contracts and certificates");                                                          //Natural: ASSIGN PSTL6235-DATA.CONTRACT-CERTIFICATE-TEXT := 'contracts and certificates'
            }                                                                                                                                                             //Natural: WHEN #TIAA = 1
            else if (condition(pnd_Tiaa.equals(1)))
            {
                decideConditionsMet2275++;
                ldaPstl6235.getPstl6235_Data_Contract_Certificate_Text().setValue("contract and certificates");                                                           //Natural: ASSIGN PSTL6235-DATA.CONTRACT-CERTIFICATE-TEXT := 'contract and certificates'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ldaPstl6235.getPstl6235_Data_Contract_Certificate_Text().setValue("contracts and certificate");                                                           //Natural: ASSIGN PSTL6235-DATA.CONTRACT-CERTIFICATE-TEXT := 'contracts and certificate'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaPstl6235.getPstl6235_Data_Have_Has_Text().setValue("has");                                                                                                 //Natural: ASSIGN PSTL6235-DATA.HAVE-HAS-TEXT := 'has'
            if (condition(pnd_Cref.greater(getZero())))                                                                                                                   //Natural: IF #CREF GT 0
            {
                ldaPstl6235.getPstl6235_Data_Contract_Certificate_Text().setValue("certificate");                                                                         //Natural: ASSIGN PSTL6235-DATA.CONTRACT-CERTIFICATE-TEXT := 'certificate'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl6235.getPstl6235_Data_Contract_Certificate_Text().setValue("contract");                                                                            //Natural: ASSIGN PSTL6235-DATA.CONTRACT-CERTIFICATE-TEXT := 'contract'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl6235.getPstl6235_Data_Deceased_Name_Text1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaPstl6235.getPstl6235_Data_Deceased_Name_Text1(),   //Natural: COMPRESS PSTL6235-DATA.DECEASED-NAME-TEXT1 ',' INTO PSTL6235-DATA.DECEASED-NAME-TEXT1 LEAVING NO SPACE
            ","));
                                                                                                                                                                          //Natural: PERFORM POST-OPEN
        sub_Post_Open();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Error.getBoolean()))                                                                                                                            //Natural: IF #ERROR
        {
            pnd_Rc.setValue(8);                                                                                                                                           //Natural: ASSIGN #RC := 08
            pnd_Msg.setValue("POST error when creating No Further Payment letter");                                                                                       //Natural: ASSIGN #MSG := 'POST error when creating No Further Payment letter'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM POST-WRITE
            sub_Post_Write();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Error.getBoolean()))                                                                                                                            //Natural: IF #ERROR
        {
            pnd_Rc.setValue(8);                                                                                                                                           //Natural: ASSIGN #RC := 08
            pnd_Msg.setValue("POST error when creating No Further Payment letter");                                                                                       //Natural: ASSIGN #MSG := 'POST error when creating No Further Payment letter'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM POST-CLOSE
            sub_Post_Close();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Post_Open() throws Exception                                                                                                                         //Natural: POST-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CLOSE MQ - PSTN9610 DOES IT'S OWN OPEN
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //*                        /* JT0815
        //* *CALLNAT 'PSTN9610'
        //* *  PSTA9610
        //* *  PSTA9611
        //* *  MSG-INFO-SUB
        //* *  PARM-EMPLOYEE-INFO-SUB
        //* *  PARM-UNIT-INFO-SUB
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        //*  082017 END
        //*  OPEN MQ TO FACILITATE MDM CALL
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*                        /* JT0815
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
            pnd_Error.setValue(true);                                                                                                                                     //Natural: ASSIGN #ERROR := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Post_Write() throws Exception                                                                                                                        //Natural: POST-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CLOSE MQ - PSTN9500 DOES IT'S OWN OPEN
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //*                        /* JT0815
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY PSTL6235-DATA
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6235.getSort_Key(), ldaPstl6235.getPstl6235_Data());
        if (condition(Global.isEscape())) return;
        //*  OPEN MQ TO FACILITATE MDM CALL
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*                        /* JT0815
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
            pnd_Error.setValue(true);                                                                                                                                     //Natural: ASSIGN #ERROR := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Post_Close() throws Exception                                                                                                                        //Natural: POST-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CLOSE MQ - PSTN9680 DOES IT'S OWN OPEN
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //*                        /* JT0815
        //*  082017 START
        //* *CALLNAT 'PSTN9680'
        //* *  PSTA9610
        //* *  PSTA9611
        //* *  MSG-INFO-SUB
        //* *  PARM-EMPLOYEE-INFO-SUB
        //* *  PARM-UNIT-INFO-SUB
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        //*  082017 END
        //*  CLOSE MQ - PSTN9500 DOES IT'S OWN OPEN
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*                        /* JT0815
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
            pnd_Error.setValue(true);                                                                                                                                     //Natural: ASSIGN #ERROR := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Base_Address() throws Exception                                                                                                                  //Natural: GET-BASE-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  JT0815 - START
        //* *#PH-UNQUE-ID-NMBR := #PIN
        //* *#PH-BSE-ADDRSS-IND := 'B'
        pnd_Addr_Lines.getValue("*").reset();                                                                                                                             //Natural: RESET #ADDR-LINES ( * ) #CITY #STATE #ZIP #NAAD
        pnd_City.reset();
        pnd_State.reset();
        pnd_Zip.reset();
        pnd_Naad.reset();
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1().notEquals(" ") || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2().notEquals(" ")))      //Natural: IF #O-BASE-ADDRESS-LINE-1 NE ' ' OR #O-BASE-ADDRESS-LINE-2 NE ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rc.setValue(9);                                                                                                                                           //Natural: ASSIGN #RC := 09
            pnd_Msg.setValue("No PIN base address.");                                                                                                                     //Natural: ASSIGN #MSG := 'No PIN base address.'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *READ(1) NAAD BY PIN-BASE-KEY STARTING FROM #PIN-BASE-KEY
        //*   IF PH-UNQUE-ID-NMBR NE #PH-UNQUE-ID-NMBR OR
        //*       PH-BSE-ADDRSS-IND NE #PH-BSE-ADDRSS-IND OR
        //*       ADDRSS-STATUS-CDE NE ' '
        //*     ESCAPE BOTTOM
        //*   END-IF
        pnd_W_Names.getValue("*").reset();                                                                                                                                //Natural: RESET #W-NAMES ( * )
        ldaIaal9125.getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Name().setValue(DbsUtil.compress("Estate of", pnd_Name));                                                      //Natural: COMPRESS 'Estate of' #NAME INTO STTLMNT-NOTIFIER-NAME
        pnd_Addr_Lines_Pnd_Addr1.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1());                                                                         //Natural: ASSIGN #ADDR1 := #O-BASE-ADDRESS-LINE-1
        pnd_Addr_Lines_Pnd_Addr2.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2());                                                                         //Natural: ASSIGN #ADDR2 := #O-BASE-ADDRESS-LINE-2
        pnd_Addr_Lines_Pnd_Addr3.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3());                                                                         //Natural: ASSIGN #ADDR3 := #O-BASE-ADDRESS-LINE-3
        pnd_Addr_Lines_Pnd_Addr4.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_4());                                                                         //Natural: ASSIGN #ADDR4 := #O-BASE-ADDRESS-LINE-4
        pnd_Zip.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Zip_Code());                                                                                        //Natural: ASSIGN #ZIP := #O-BASE-ADDRESS-ZIP-CODE
        pnd_Naad.setValue(true);                                                                                                                                          //Natural: ASSIGN #NAAD := TRUE
        pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Type_Code());                                                      //Natural: ASSIGN PSTA9612.ADDRSS-TYP-CDE := #O-BASE-ADDRESS-TYPE-CODE
        pdaPsta9612.getPsta9612_Addrss_Pstl_Dta().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Postal_Data());                                                   //Natural: ASSIGN PSTA9612.ADDRSS-PSTL-DTA := #O-BASE-ADDRESS-POSTAL-DATA
        //*  IF ADDRSS-TYPE-CDE = 'U'
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Type_Code().equals("U")))                                                                              //Natural: IF #O-BASE-ADDRESS-TYPE-CODE = 'U'
        {
            //*    IF ADDRSS-GEOGRAPHIC-CDE NE ' '
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().notEquals(" ")))                                                                 //Natural: IF #O-BASE-ADDRESS-GEOGRAPHIC-CODE NE ' '
            {
                //*      CALLNAT 'NAZN031' MSG-INFO-SUB #STATE ADDRSS-GEOGRAPHIC-CDE
                DbsUtil.callnat(Nazn031.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pnd_State, pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code()); //Natural: CALLNAT 'NAZN031' MSG-INFO-SUB #STATE #O-BASE-ADDRESS-GEOGRAPHIC-CODE
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_City.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_City());                                                                                       //Natural: ASSIGN #CITY := #O-BASE-ADDRESS-CITY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_City.reset();                                                                                                                                             //Natural: RESET #CITY #STATE
            pnd_State.reset();
        }                                                                                                                                                                 //Natural: END-IF
        //* *END-READ
        //* *IF #NAAD = FALSE
        //*   #RC := 09
        //*   #MSG := 'No PIN base address.'
        //* *END-IF
    }
    private void sub_Get_City() throws Exception                                                                                                                          //Natural: GET-CITY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_W_Names.getValue("*").reset();                                                                                                                                //Natural: RESET #W-NAMES ( * ) #ZIP-FOUND #CITY #L
        pnd_Zip_Found.reset();
        pnd_City.reset();
        pnd_L.reset();
        //* *FOR #I 5 TO 1 STEP -1
        //*   IF #ADDR-LINES(#I) NE ' '
        //*     IF #L = 0
        //*       #L := #I
        //*     END-IF
        //*     #W-ADDR := #ADDR-LINES(#I)
        //*     RESET #NEW-ADDR #T
        //*     FOR #S = 1 TO 25
        //*       IF #W-A1(#S) NE ' '
        //*         IF #SPACE GT 0
        //*           RESET #SPACE
        //*        END-IF
        //*        ADD 1 TO #T
        //*        #N-A1(#T) := #W-A1(#S)
        //*      ELSE
        //*        ADD 1 TO #SPACE
        //*        IF #SPACE = 1
        //*          ADD 1 TO #T
        //*        END-IF
        //*      END-IF
        //*    END-FOR
        //*    SEPARATE #NEW-ADDR INTO #W-NAMES(*) WITH DELIMITER ' '
        //*    IF #W-NAMES(1) = ADDRSS-ZIP
        //*      IF #W-NAMES(2:19) NE ' '
        //*        IGNORE
        //*      ELSE
        //*        RESET #ADDR-LINES(#I)
        //*        #ZIP-FOUND := TRUE
        //*        ESCAPE TOP
        //*      END-IF
        //*    END-IF
        //*    FOR #J 19 TO 1 STEP -1
        //*      IF #W-NAMES(#J) = ' '
        //*        ESCAPE TOP
        //*      END-IF
        //*      IF #W-NAMES(#J) = ADDRSS-ZIP
        //*        IF #J - 2 = 0
        //*          #K := #J -1
        //*        ELSE
        //*          #K := #J - 2
        //*        END-IF
        //*        COMPRESS #W-NAMES(1:#K) INTO #CITY
        //*        #K := #J - 1
        //*        IF #K = 0
        //*          #K := 1
        //*        END-IF
        //*        IF ADDRSS-GEOGRAPHIC-CDE = ' '
        //*          #STATE := #W-NAMES(#K)
        //*        END-IF
        //*        RESET #ADDR-LINES(#I)
        //*        ESCAPE ROUTINE
        //*      ELSE
        //*        IF #ZIP-FOUND
        //*          #K := #J - 1
        //*          IF #K = 0
        //*            RESET #CITY
        //*          ELSE
        //*            COMPRESS #W-NAMES(1:#K) INTO #CITY
        //*          END-IF
        //*          IF ADDRSS-GEOGRAPHIC-CDE = ' '
        //*            #STATE := #W-NAMES(#J)
        //*          END-IF
        //*          RESET #ADDR-LINES(#I)
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*      END-IF
        //*    END-FOR
        //*  END-IF
        //* *END-FOR
        //* *IF #ZIP-FOUND = FALSE
        //*   #W-ADDR := #ADDR-LINES(#L)
        //*   RESET #NEW-ADDR #T
        //*   FOR #S = 1 TO 25
        //*     IF #W-A1(#S) NE ' '
        //*       IF #SPACE GT 0
        //*         RESET #SPACE
        //*       END-IF
        //*       ADD 1 TO #T
        //*       #N-A1(#T) := #W-A1(#S)
        //*     ELSE
        //*       ADD 1 TO #SPACE
        //*       IF #SPACE = 1
        //*         ADD 1 TO #T
        //*       END-IF
        //*     END-IF
        //*   END-FOR
        //*   SEPARATE #NEW-ADDR INTO #W-NAMES(*) WITH DELIMITER ' '
        //*   FOR #J 19 TO 1 STEP -1
        //*     IF #W-NAMES(#J) = ' '
        //*       ESCAPE TOP
        //*    END-IF
        //*    #K := #J - 1
        //*    IF #K = 0
        //*      RESET #CITY
        //*    ELSE
        //*      COMPRESS #W-NAMES(1:#K) INTO #CITY
        //*    END-IF
        //*    IF ADDRSS-GEOGRAPHIC-CDE = ' '
        //*      #STATE := #W-NAMES(#J)
        //*    END-IF
        //*    RESET #ADDR-LINES(#L)
        //*    ESCAPE BOTTOM
        //*  END-FOR
        //* *END-IF
    }

    //
}
