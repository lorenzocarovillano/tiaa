/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:31:21 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn4815
************************************************************
**        * FILE NAME            : Cwfn4815.java
**        * CLASS NAME           : Cwfn4815
**        * INSTANCE NAME        : Cwfn4815
************************************************************
************************************************************************
* PROGRAM  : CWFN4815
* AUTHOR   : LEON EPSTEIN
* SYSTEM   : CORPORATE WORK FLOW REPORTING
* TITLE    : SET UP RUN CONTROL FOR THE MIT EXTRACT PROCESS (REPORTING)
* GENERATED: FEB 15,99 AT 15:01 PM
* FUNCTION : THIS SUB-PROGRAM CALLED BY THE EXTRACT PROCESS TO DO THE
*          : INITIAL SET UP OF THE RUN CONTROL RECORD.
*
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn4815 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCwfa4815 pdaCwfa4815;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaCwfl4815 ldaCwfl4815;
    private LdaCwfl4816 ldaCwfl4816;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl_Histogram;
    private DbsField cwf_Support_Tbl_Histogram_Tbl_Prime_Key;
    private DbsField pnd_Last_Run_Isn;
    private DbsField pnd_New_Run_Id;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_1;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_2;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key_From;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_3;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_4;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key_To;

    private DbsGroup pnd_Tbl_Prime_Key_To__R_Field_5;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_To__R_Field_6;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Work_Rqst_Retaining_Days;
    private DbsField pnd_Date_D;
    private DbsField pnd_Date_Out_D;
    private DbsField pnd_Date_Out_N;
    private DbsField pnd_Time_Out_T;
    private DbsField pnd_Time_Out_A_15;

    private DbsGroup pnd_Time_Out_A_15__R_Field_7;
    private DbsField pnd_Time_Out_A_15_Pnd_Time_Out_N_15;
    private DbsField pnd_Time_Out_A;

    private DbsGroup pnd_Time_Out_A__R_Field_8;
    private DbsField pnd_Time_Out_A_Pnd_Time_Out_N;
    private DbsField pnd_Time_Out_Dte_Tme;

    private DbsGroup pnd_Time_Out_Dte_Tme__R_Field_9;
    private DbsField pnd_Time_Out_Dte_Tme_Pnd_Time_Yyyymmdd;
    private DbsField pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Hours;
    private DbsField pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Minutes;
    private DbsField pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Seconds;
    private DbsField pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Tenths;
    private DbsField pnd_Starting_Date_Save;
    private DbsField pnd_Ending_Date_Save;
    private DbsField pnd_Starting_Time_A;

    private DbsGroup pnd_Starting_Time_A__R_Field_10;
    private DbsField pnd_Starting_Time_A_Pnd_Starting_Time_N;

    private DbsGroup pnd_Starting_Time_A__R_Field_11;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Yyyy;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Mm;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Dd;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Hours;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Minutes;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Seconds;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Tenths;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCwfl4815 = new LdaCwfl4815();
        registerRecord(ldaCwfl4815);
        registerRecord(ldaCwfl4815.getVw_cwf_Support_Tbl());
        ldaCwfl4816 = new LdaCwfl4816();
        registerRecord(ldaCwfl4816);
        registerRecord(ldaCwfl4816.getVw_cwf_Support_Tbl_Last_Run_Id());

        // parameters
        parameters = new DbsRecord();
        pdaCwfa4815 = new PdaCwfa4815(parameters);
        pdaCdaobj = new PdaCdaobj(parameters);
        pdaCwfpda_D = new PdaCwfpda_D(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pdaCwfpda_P = new PdaCwfpda_P(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Support_Tbl_Histogram = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl_Histogram", "CWF-SUPPORT-TBL-HISTOGRAM"), "CWF_SUPPORT_TBL", 
            "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Histogram_Tbl_Prime_Key = vw_cwf_Support_Tbl_Histogram.getRecord().newFieldInGroup("cwf_Support_Tbl_Histogram_Tbl_Prime_Key", 
            "TBL-PRIME-KEY", FieldType.STRING, 53, RepeatingFieldStrategy.None, "TBL_PRIME_KEY");
        cwf_Support_Tbl_Histogram_Tbl_Prime_Key.setDdmHeader("RECORD KEY");
        cwf_Support_Tbl_Histogram_Tbl_Prime_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Support_Tbl_Histogram);

        pnd_Last_Run_Isn = localVariables.newFieldInRecord("pnd_Last_Run_Isn", "#LAST-RUN-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_New_Run_Id = localVariables.newFieldInRecord("pnd_New_Run_Id", "#NEW-RUN-ID", FieldType.NUMERIC, 9);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_1", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);

        pnd_Tbl_Prime_Key__R_Field_2 = pnd_Tbl_Prime_Key__R_Field_1.newGroupInGroup("pnd_Tbl_Prime_Key__R_Field_2", "REDEFINE", pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_Pnd_Run_Status = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 8);
        pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field", "#REST-OF-TBL-KEY-FIELD", 
            FieldType.STRING, 21);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_From = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_From", "#TBL-PRIME-KEY-FROM", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_From__R_Field_3 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_From__R_Field_3", "REDEFINE", pnd_Tbl_Prime_Key_From);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_From__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_From__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_From__R_Field_4 = pnd_Tbl_Prime_Key_From__R_Field_3.newGroupInGroup("pnd_Tbl_Prime_Key_From__R_Field_4", "REDEFINE", pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Run_Status", "#RUN-STATUS", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 15);
        pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 14);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_From__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_To = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_To", "#TBL-PRIME-KEY-TO", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_To__R_Field_5 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_To__R_Field_5", "REDEFINE", pnd_Tbl_Prime_Key_To);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_To__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_To__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_To__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_To__R_Field_6 = pnd_Tbl_Prime_Key_To__R_Field_5.newGroupInGroup("pnd_Tbl_Prime_Key_To__R_Field_6", "REDEFINE", pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 15);
        pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 14);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_To__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Work_Rqst_Retaining_Days = localVariables.newFieldInRecord("pnd_Work_Rqst_Retaining_Days", "#WORK-RQST-RETAINING-DAYS", FieldType.NUMERIC, 
            3);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Date_Out_D = localVariables.newFieldInRecord("pnd_Date_Out_D", "#DATE-OUT-D", FieldType.DATE);
        pnd_Date_Out_N = localVariables.newFieldInRecord("pnd_Date_Out_N", "#DATE-OUT-N", FieldType.NUMERIC, 8);
        pnd_Time_Out_T = localVariables.newFieldInRecord("pnd_Time_Out_T", "#TIME-OUT-T", FieldType.TIME);
        pnd_Time_Out_A_15 = localVariables.newFieldInRecord("pnd_Time_Out_A_15", "#TIME-OUT-A-15", FieldType.STRING, 15);

        pnd_Time_Out_A_15__R_Field_7 = localVariables.newGroupInRecord("pnd_Time_Out_A_15__R_Field_7", "REDEFINE", pnd_Time_Out_A_15);
        pnd_Time_Out_A_15_Pnd_Time_Out_N_15 = pnd_Time_Out_A_15__R_Field_7.newFieldInGroup("pnd_Time_Out_A_15_Pnd_Time_Out_N_15", "#TIME-OUT-N-15", FieldType.NUMERIC, 
            15);
        pnd_Time_Out_A = localVariables.newFieldInRecord("pnd_Time_Out_A", "#TIME-OUT-A", FieldType.STRING, 8);

        pnd_Time_Out_A__R_Field_8 = localVariables.newGroupInRecord("pnd_Time_Out_A__R_Field_8", "REDEFINE", pnd_Time_Out_A);
        pnd_Time_Out_A_Pnd_Time_Out_N = pnd_Time_Out_A__R_Field_8.newFieldInGroup("pnd_Time_Out_A_Pnd_Time_Out_N", "#TIME-OUT-N", FieldType.NUMERIC, 8);
        pnd_Time_Out_Dte_Tme = localVariables.newFieldInRecord("pnd_Time_Out_Dte_Tme", "#TIME-OUT-DTE-TME", FieldType.NUMERIC, 15);

        pnd_Time_Out_Dte_Tme__R_Field_9 = localVariables.newGroupInRecord("pnd_Time_Out_Dte_Tme__R_Field_9", "REDEFINE", pnd_Time_Out_Dte_Tme);
        pnd_Time_Out_Dte_Tme_Pnd_Time_Yyyymmdd = pnd_Time_Out_Dte_Tme__R_Field_9.newFieldInGroup("pnd_Time_Out_Dte_Tme_Pnd_Time_Yyyymmdd", "#TIME-YYYYMMDD", 
            FieldType.NUMERIC, 8);
        pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Hours = pnd_Time_Out_Dte_Tme__R_Field_9.newFieldInGroup("pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Hours", "#TIME-OUT-N-HOURS", 
            FieldType.NUMERIC, 2);
        pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Minutes = pnd_Time_Out_Dte_Tme__R_Field_9.newFieldInGroup("pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Minutes", "#TIME-OUT-N-MINUTES", 
            FieldType.NUMERIC, 2);
        pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Seconds = pnd_Time_Out_Dte_Tme__R_Field_9.newFieldInGroup("pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Seconds", "#TIME-OUT-N-SECONDS", 
            FieldType.NUMERIC, 2);
        pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Tenths = pnd_Time_Out_Dte_Tme__R_Field_9.newFieldInGroup("pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Tenths", "#TIME-OUT-N-TENTHS", 
            FieldType.NUMERIC, 1);
        pnd_Starting_Date_Save = localVariables.newFieldInRecord("pnd_Starting_Date_Save", "#STARTING-DATE-SAVE", FieldType.TIME);
        pnd_Ending_Date_Save = localVariables.newFieldInRecord("pnd_Ending_Date_Save", "#ENDING-DATE-SAVE", FieldType.TIME);
        pnd_Starting_Time_A = localVariables.newFieldInRecord("pnd_Starting_Time_A", "#STARTING-TIME-A", FieldType.STRING, 15);

        pnd_Starting_Time_A__R_Field_10 = localVariables.newGroupInRecord("pnd_Starting_Time_A__R_Field_10", "REDEFINE", pnd_Starting_Time_A);
        pnd_Starting_Time_A_Pnd_Starting_Time_N = pnd_Starting_Time_A__R_Field_10.newFieldInGroup("pnd_Starting_Time_A_Pnd_Starting_Time_N", "#STARTING-TIME-N", 
            FieldType.NUMERIC, 15);

        pnd_Starting_Time_A__R_Field_11 = pnd_Starting_Time_A__R_Field_10.newGroupInGroup("pnd_Starting_Time_A__R_Field_11", "REDEFINE", pnd_Starting_Time_A_Pnd_Starting_Time_N);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Yyyy = pnd_Starting_Time_A__R_Field_11.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Yyyy", "#STRT-TIME-N-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Mm = pnd_Starting_Time_A__R_Field_11.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Mm", "#STRT-TIME-N-MM", 
            FieldType.NUMERIC, 2);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Dd = pnd_Starting_Time_A__R_Field_11.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Dd", "#STRT-TIME-N-DD", 
            FieldType.NUMERIC, 2);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Hours = pnd_Starting_Time_A__R_Field_11.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Hours", "#STRT-TIME-N-HOURS", 
            FieldType.NUMERIC, 2);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Minutes = pnd_Starting_Time_A__R_Field_11.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Minutes", "#STRT-TIME-N-MINUTES", 
            FieldType.NUMERIC, 2);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Seconds = pnd_Starting_Time_A__R_Field_11.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Seconds", "#STRT-TIME-N-SECONDS", 
            FieldType.NUMERIC, 2);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Tenths = pnd_Starting_Time_A__R_Field_11.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Tenths", "#STRT-TIME-N-TENTHS", 
            FieldType.NUMERIC, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl_Histogram.reset();

        ldaCwfl4815.initializeValues();
        ldaCwfl4816.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn4815() throws Exception
    {
        super("Cwfn4815");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFN4815", onError);
        //* (TO ALLOW EASY ESCAPE FROM THE ROUTINE)
        ROUTINE:                                                                                                                                                          //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM RESET-THE-OUTPUT-PARAMETERS
            sub_Reset_The_Output_Parameters();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM SET-UP-THE-RUN-CONTROL-RECORD
            sub_Set_Up_The_Run_Control_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break ROUTINE;                                                                                                                                      //Natural: ESCAPE BOTTOM ( ROUTINE. )
            //*  SUBROUTINE JUST FOR ENDING THIS SUBPROGRAM IMMEDIATELY
            //*  ------------------------------------------------------
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-SUBPROGRAM
            //* (ROUTINE.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  SUBROUTINES
        //*  ===========
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-THE-OUTPUT-PARAMETERS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-UP-THE-RUN-CONTROL-RECORD
        //*  =============================================
        //*  FIND CWF-EXTR-RPT-LAST-RUN RECORD TO RETAIN LAST-RUN-ID (ISN)
        //*  ----------------------------------------------------------------------
        //* ************************************************
        //*  CHECK TO SEE IF AN ACTIVE RECORD ALREADY EXISTS
        //* ************************************************
        //*  -------- DEFINING FROM KEY --------------------
        //*  -------- DEFINING TO KEY --------------------
        //* ************************************************
        //*  CHECK TO SEE IF A PENDING RECORD ALREADY EXISTS
        //* ************************************************
        //*  IF A PENDING RECORD IS FOUND, UPDATE THE RECORD TO ACTIVE,
        //*  POPULATE THE OUTPUT PARAMETERS AND RETURN TO THE CALLING PROGRAM
        //*  ----------------------------------------------------------------
        //*  LOOK UP THE MOST RECENT PERIOD ARCHIVED TO CREATE THE NEW RECORD
        //*  ----------------------------------------------------------------
        //*  USE THIS RECORD TO CREATE THE NEW ONE
        //*  -------------------------------------
        //*  ----- CALCULATE STARTING TIME FOR WINDOW ----------------
        //*  ------------- CREATING CONTROL RECORD ---------------------
        //*  ON NATURAL RUN TIME ERROR, RETURN WITH AN APPROPRIATE ERROR MESSAGE
        //*  ===================================================================
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Subprogram() throws Exception                                                                                                                 //Natural: ESCAPE-SUBPROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( ROUTINE. )
        Global.setEscapeCode(EscapeType.Bottom, "ROUTINE");
        if (true) return;
        //* (1060)
    }
    private void sub_Reset_The_Output_Parameters() throws Exception                                                                                                       //Natural: RESET-THE-OUTPUT-PARAMETERS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===========================================
        pdaCwfa4815.getCwfa4815_Output().reset();                                                                                                                         //Natural: RESET CWFA4815-OUTPUT
        //* (1150)
    }
    private void sub_Set_Up_The_Run_Control_Record() throws Exception                                                                                                     //Natural: SET-UP-THE-RUN-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("CWF-EXTR-RPT-LST-RUN");                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME := 'CWF-EXTR-RPT-LST-RUN'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("CWF-EXTR-LST-KEY");                                                                                                 //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-KEY-FIELD := 'CWF-EXTR-LST-KEY'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind.setValue(" A");                                                                                                               //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-ACTVE-IND := ' A'
        ldaCwfl4816.getVw_cwf_Support_Tbl_Last_Run_Id().startDatabaseFind                                                                                                 //Natural: FIND ( 1 ) CWF-SUPPORT-TBL-LAST-RUN-ID WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "LAST_RUN_ID_FIND",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        LAST_RUN_ID_FIND:
        while (condition(ldaCwfl4816.getVw_cwf_Support_Tbl_Last_Run_Id().readNextRow("LAST_RUN_ID_FIND")))
        {
            ldaCwfl4816.getVw_cwf_Support_Tbl_Last_Run_Id().setIfNotFoundControlFlag(false);
            pnd_Last_Run_Isn.setValue(ldaCwfl4816.getVw_cwf_Support_Tbl_Last_Run_Id().getAstISN("LAST_RUN_ID_FIND"));                                                     //Natural: ASSIGN #LAST-RUN-ISN := *ISN ( LAST-RUN-ID-FIND. )
            //* (LAST-RUN-ID-FIND.)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(ldaCwfl4816.getVw_cwf_Support_Tbl_Last_Run_Id().getAstCOUNTER().equals(getZero())))                                                                 //Natural: IF *COUNTER ( LAST-RUN-ID-FIND. ) = 0
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(26);                                                                                                    //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 26
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Last-Run-Id control record not found"));               //Natural: COMPRESS 'Program' *PROGRAM '- Last-Run-Id control record not found' INTO MSG-INFO-SUB.##MSG
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape())) {return;}
            //* (1350)
            //*  ACTIVE STATUS
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                     //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme.setValue("CWF-EXTR-RPT-NXT-RUN");                                                                                        //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-TABLE-NME := 'CWF-EXTR-RPT-NXT-RUN'
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status.setValue("A ");                                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#RUN-STATUS := 'A '
        pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement.setValue(0);                                                                                               //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#STARTING-DATE-9S-COMPLEMENT := 0
        pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field.setValue(" ");                                                                                                   //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#REST-OF-TBL-KEY-FIELD := ' '
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind.setValue(" ");                                                                                                           //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-ACTVE-IND := ' '
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                       //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme.setValue("CWF-EXTR-RPT-NXT-RUN");                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-TABLE-NME := 'CWF-EXTR-RPT-NXT-RUN'
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status.setValue("A ");                                                                                                               //Natural: ASSIGN #TBL-PRIME-KEY-TO.#RUN-STATUS := 'A '
        pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement.setValue(999999999999999L);                                                                                  //Natural: ASSIGN #TBL-PRIME-KEY-TO.#STARTING-DATE-9S-COMPLEMENT := 999999999999999
        pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field.setValue("99999999999999");                                                                                        //Natural: ASSIGN #TBL-PRIME-KEY-TO.#REST-OF-TBL-KEY-FIELD := '99999999999999'
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind.setValue("9");                                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-ACTVE-IND := '9'
        vw_cwf_Support_Tbl_Histogram.createHistogram                                                                                                                      //Natural: HISTOGRAM CWF-SUPPORT-TBL-HISTOGRAM FOR TBL-PRIME-KEY FROM #TBL-PRIME-KEY-FROM THRU #TBL-PRIME-KEY-TO
        (
        "ACTIVE_HISTOGRAM",
        "TBL_PRIME_KEY",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key_From, "And", WcType.WITH) ,
        new Wc("TBL_PRIME_KEY", "<=", pnd_Tbl_Prime_Key_To, WcType.WITH) }
        );
        ACTIVE_HISTOGRAM:
        while (condition(vw_cwf_Support_Tbl_Histogram.readNextRow("ACTIVE_HISTOGRAM")))
        {
            if (condition(vw_cwf_Support_Tbl_Histogram.getAstNUMBER().greater(1)))                                                                                        //Natural: IF *NUMBER ( ACTIVE-HISTOGRAM. ) GT 1
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(23);                                                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 23
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- More than one active control records",             //Natural: COMPRESS 'Program' *PROGRAM '- More than one active control records' 'exist' INTO MSG-INFO-SUB.##MSG
                    "exist"));
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* (1650)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(vw_cwf_Support_Tbl_Histogram.getAstNUMBER().equals(1)))                                                                                         //Natural: IF *NUMBER ( ACTIVE-HISTOGRAM. ) EQ 1
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(24);                                                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 24
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Active control record already Exist",              //Natural: COMPRESS 'Program' *PROGRAM '- Active control record already Exist' 'exist' INTO MSG-INFO-SUB.##MSG
                    "exist"));
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* (1650)
            }                                                                                                                                                             //Natural: END-IF
            //* (ACTIVE-HISTOGRAM.)
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status.setValue("P");                                                                                                              //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#RUN-STATUS := 'P'
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status.setValue("P");                                                                                                                //Natural: ASSIGN #TBL-PRIME-KEY-TO.#RUN-STATUS := 'P'
        vw_cwf_Support_Tbl_Histogram.createHistogram                                                                                                                      //Natural: HISTOGRAM CWF-SUPPORT-TBL-HISTOGRAM FOR TBL-PRIME-KEY FROM #TBL-PRIME-KEY-FROM THRU #TBL-PRIME-KEY-TO
        (
        "PENDING_HISTOGRAM",
        "TBL_PRIME_KEY",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key_From, "And", WcType.WITH) ,
        new Wc("TBL_PRIME_KEY", "<=", pnd_Tbl_Prime_Key_To, WcType.WITH) }
        );
        PENDING_HISTOGRAM:
        while (condition(vw_cwf_Support_Tbl_Histogram.readNextRow("PENDING_HISTOGRAM")))
        {
            pnd_Tbl_Prime_Key.setValue(cwf_Support_Tbl_Histogram_Tbl_Prime_Key);                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY := CWF-SUPPORT-TBL-HISTOGRAM.TBL-PRIME-KEY
            ldaCwfl4815.getVw_cwf_Support_Tbl().startDatabaseFind                                                                                                         //Natural: FIND ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
            (
            "PENDING_FIND",
            new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
            1
            );
            PENDING_FIND:
            while (condition(ldaCwfl4815.getVw_cwf_Support_Tbl().readNextRow("PENDING_FIND")))
            {
                ldaCwfl4815.getVw_cwf_Support_Tbl().setIfNotFoundControlFlag(false);
                ldaCwfl4815.getCwf_Support_Tbl_Run_Status().setValue("A");                                                                                                //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-STATUS := 'A'
                ldaCwfl4815.getCwf_Support_Tbl_Tbl_Updte_Dte().setValue(Global.getDATX());                                                                                //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-UPDTE-DTE := *DATX
                ldaCwfl4815.getVw_cwf_Support_Tbl().updateDBRow("PENDING_FIND");                                                                                          //Natural: UPDATE ( PENDING-FIND. )
                pdaCwfa4815.getCwfa4815_Output_Run_Control_Isn().setValue(ldaCwfl4815.getVw_cwf_Support_Tbl().getAstISN("PENDING_FIND"));                                 //Natural: ASSIGN CWFA4815-OUTPUT.RUN-CONTROL-ISN := *ISN ( PENDING-FIND. )
                pdaCwfa4815.getCwfa4815_Output_Control_Rec_Status().setValue("P");                                                                                        //Natural: ASSIGN CWFA4815-OUTPUT.CONTROL-REC-STATUS := 'P'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PENDING_FIND"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PENDING_FIND"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* (PENDING-FIND.)
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PENDING_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PENDING_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* (PENDING-HISTOGRAM.)
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //*  ---------------- CREATE A NEW CONTROL RECORD ------------------------
        //*  THERE ARE NEITHER ACTIVE NOR PENDING CONTROL RECORDS IF THIS POINT IN
        //*  THE PROCESS IS REACHED.  CHECK THE CONTROL RECORD FOR THE LAST RUN ID
        //*  AND, IF THE FLAG THERE TO CONTROL AUTOMATIC RUN CONTROL RECORD
        //*  CREATION ALLOWS, CREATE A NEW ACTIVE RECORD.
        //*  ---------------------------------------------------------------------
        //*  FIRST ALLOCATE A NEW RUN ID
        //*  ---------------------------
        LAST_RUN_ID_GET:                                                                                                                                                  //Natural: GET CWF-SUPPORT-TBL-LAST-RUN-ID #LAST-RUN-ISN
        ldaCwfl4816.getVw_cwf_Support_Tbl_Last_Run_Id().readByID(pnd_Last_Run_Isn.getLong(), "LAST_RUN_ID_GET");
        if (condition(ldaCwfl4816.getCwf_Support_Tbl_Last_Run_Id_Enable_Auto_Run_Control_Creation().equals(false) || ldaCwfl4816.getCwf_Support_Tbl_Last_Run_Id_Last_Run_Id().greater(999999998))) //Natural: IF CWF-SUPPORT-TBL-LAST-RUN-ID.ENABLE-AUTO-RUN-CONTROL-CREATION = FALSE OR CWF-SUPPORT-TBL-LAST-RUN-ID.LAST-RUN-ID GT 999999998
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(25);                                                                                                    //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 25
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Automatic run control creation denied L.C"));          //Natural: COMPRESS 'Program' *PROGRAM '- Automatic run control creation denied L.C' INTO MSG-INFO-SUB.##MSG
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape())) {return;}
            //* (2210)
        }                                                                                                                                                                 //Natural: END-IF
        ldaCwfl4816.getCwf_Support_Tbl_Last_Run_Id_Last_Run_Id().nadd(1);                                                                                                 //Natural: ADD 1 TO CWF-SUPPORT-TBL-LAST-RUN-ID.LAST-RUN-ID
        ldaCwfl4816.getVw_cwf_Support_Tbl_Last_Run_Id().updateDBRow("LAST_RUN_ID_GET");                                                                                   //Natural: UPDATE ( LAST-RUN-ID-GET. )
        pnd_New_Run_Id.setValue(ldaCwfl4816.getCwf_Support_Tbl_Last_Run_Id_Last_Run_Id());                                                                                //Natural: ASSIGN #NEW-RUN-ID := CWF-SUPPORT-TBL-LAST-RUN-ID.LAST-RUN-ID
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("CWF-EXTR-RPT-NXT-RUN");                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME := 'CWF-EXTR-RPT-NXT-RUN'
        pnd_Tbl_Prime_Key_Pnd_Run_Status.setValue("C");                                                                                                                   //Natural: ASSIGN #TBL-PRIME-KEY.#RUN-STATUS := 'C'
        pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement.reset();                                                                                                        //Natural: RESET #TBL-PRIME-KEY.#STARTING-DATE-9S-COMPLEMENT #TBL-PRIME-KEY.#REST-OF-TBL-KEY-FIELD #TBL-PRIME-KEY.#TBL-ACTVE-IND
        pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field.reset();
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind.reset();
        ldaCwfl4815.getVw_cwf_Support_Tbl().startDatabaseRead                                                                                                             //Natural: READ ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "LAST_COMPLETED_READ",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        LAST_COMPLETED_READ:
        while (condition(ldaCwfl4815.getVw_cwf_Support_Tbl().readNextRow("LAST_COMPLETED_READ")))
        {
            if (condition(ldaCwfl4815.getCwf_Support_Tbl_Tbl_Scrty_Level_Ind().notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind) || ldaCwfl4815.getCwf_Support_Tbl_Tbl_Table_Nme().notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)  //Natural: IF CWF-SUPPORT-TBL.TBL-SCRTY-LEVEL-IND NE #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND OR CWF-SUPPORT-TBL.TBL-TABLE-NME NE #TBL-PRIME-KEY.#TBL-TABLE-NME OR CWF-SUPPORT-TBL.RUN-STATUS NE #TBL-PRIME-KEY.#RUN-STATUS
                || ldaCwfl4815.getCwf_Support_Tbl_Run_Status().notEquals(pnd_Tbl_Prime_Key_Pnd_Run_Status)))
            {
                ldaCwfl4815.getVw_cwf_Support_Tbl().getAstCOUNTER().nsubtract(1);                                                                                         //Natural: SUBTRACT 1 FROM *COUNTER ( LAST-COMPLETED-READ. )
                if (true) break LAST_COMPLETED_READ;                                                                                                                      //Natural: ESCAPE BOTTOM ( LAST-COMPLETED-READ. )
                //* (2500)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCwfl4815.getVw_cwf_Support_Tbl().getAstCOUNTER().equals(getZero())))                                                                         //Natural: IF *COUNTER ( LAST-COMPLETED-READ. ) = 0
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(25);                                                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 25
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Automatic run control creation denied N.Complt")); //Natural: COMPRESS 'Program' *PROGRAM '- Automatic run control creation denied N.Complt' INTO MSG-INFO-SUB.##MSG
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LAST_COMPLETED_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LAST_COMPLETED_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* (2610)
            }                                                                                                                                                             //Natural: END-IF
            ldaCwfl4815.getCwf_Support_Tbl_Starting_Date().compute(new ComputeParameters(false, ldaCwfl4815.getCwf_Support_Tbl_Starting_Date()), ldaCwfl4815.getCwf_Support_Tbl_Ending_Date().add(1)); //Natural: ASSIGN CWF-SUPPORT-TBL.STARTING-DATE := CWF-SUPPORT-TBL.ENDING-DATE + 1
            pnd_Starting_Date_Save.setValue(ldaCwfl4815.getCwf_Support_Tbl_Starting_Date());                                                                              //Natural: ASSIGN #STARTING-DATE-SAVE := CWF-SUPPORT-TBL.STARTING-DATE
            pnd_Starting_Time_A.setValueEdited(pnd_Starting_Date_Save,new ReportEditMask("YYYYMMDDHHIISST"));                                                             //Natural: MOVE EDITED #STARTING-DATE-SAVE ( EM = YYYYMMDDHHIISST ) TO #STARTING-TIME-A
            //*  ----- CALCULATE ENDING TIME FOR WINDOW ----------------
            pnd_Time_Out_A_Pnd_Time_Out_N.setValue(Global.getDATN());                                                                                                     //Natural: MOVE *DATN TO #TIME-OUT-N
            pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Time_Out_A);                                                                                     //Natural: MOVE EDITED #TIME-OUT-A TO #DATE-D ( EM = YYYYMMDD )
            pnd_Date_Out_D.compute(new ComputeParameters(false, pnd_Date_Out_D), (pnd_Date_D.subtract(1)));                                                               //Natural: COMPUTE #DATE-OUT-D = ( #DATE-D - 1 )
            pnd_Time_Out_A.setValueEdited(pnd_Date_Out_D,new ReportEditMask("YYYYMMDD"));                                                                                 //Natural: MOVE EDITED #DATE-OUT-D ( EM = YYYYMMDD ) TO #TIME-OUT-A
            pnd_Time_Out_Dte_Tme_Pnd_Time_Yyyymmdd.setValue(pnd_Time_Out_A_Pnd_Time_Out_N);                                                                               //Natural: MOVE #TIME-OUT-N TO #TIME-YYYYMMDD
            pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Hours.setValue(23);                                                                                                       //Natural: ASSIGN #TIME-OUT-N-HOURS := 23
            pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Minutes.setValue(59);                                                                                                     //Natural: ASSIGN #TIME-OUT-N-MINUTES := 59
            pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Seconds.setValue(59);                                                                                                     //Natural: ASSIGN #TIME-OUT-N-SECONDS := 59
            pnd_Time_Out_Dte_Tme_Pnd_Time_Out_N_Tenths.setValue(9);                                                                                                       //Natural: ASSIGN #TIME-OUT-N-TENTHS := 9
            pnd_Time_Out_A_15.setValue(pnd_Time_Out_Dte_Tme);                                                                                                             //Natural: MOVE #TIME-OUT-DTE-TME TO #TIME-OUT-A-15
            ldaCwfl4815.getCwf_Support_Tbl_Ending_Date().setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Time_Out_A_15);                                         //Natural: MOVE EDITED #TIME-OUT-A-15 TO CWF-SUPPORT-TBL.ENDING-DATE ( EM = YYYYMMDDHHIISST )
            pnd_Ending_Date_Save.setValue(ldaCwfl4815.getCwf_Support_Tbl_Ending_Date());                                                                                  //Natural: ASSIGN #ENDING-DATE-SAVE := CWF-SUPPORT-TBL.ENDING-DATE
            //* ***
            if (condition(pnd_Ending_Date_Save.less(pnd_Starting_Date_Save)))                                                                                             //Natural: IF #ENDING-DATE-SAVE LT #STARTING-DATE-SAVE
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(21);                                                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 21
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Ending-Date less than Starting-Date"));            //Natural: COMPRESS 'Program' *PROGRAM '- Ending-Date less than Starting-Date' INTO MSG-INFO-SUB.##MSG
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LAST_COMPLETED_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LAST_COMPLETED_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* (2970)
            }                                                                                                                                                             //Natural: END-IF
            ldaCwfl4815.getCwf_Support_Tbl_Run_Status().setValue("A");                                                                                                    //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-STATUS := 'A'
            ldaCwfl4815.getCwf_Support_Tbl_Starting_Date().setValue(pnd_Starting_Date_Save);                                                                              //Natural: ASSIGN CWF-SUPPORT-TBL.STARTING-DATE := #STARTING-DATE-SAVE
            ldaCwfl4815.getCwf_Support_Tbl_Ending_Date().setValue(pnd_Ending_Date_Save);                                                                                  //Natural: ASSIGN CWF-SUPPORT-TBL.ENDING-DATE := #ENDING-DATE-SAVE
            pnd_Time_Out_A_15.setValueEdited(ldaCwfl4815.getCwf_Support_Tbl_Starting_Date(),new ReportEditMask("YYYYMMDDHHIISST"));                                       //Natural: MOVE EDITED CWF-SUPPORT-TBL.STARTING-DATE ( EM = YYYYMMDDHHIISST ) TO #TIME-OUT-A-15
            ldaCwfl4815.getCwf_Support_Tbl_Starting_Date_9s_Complement().compute(new ComputeParameters(false, ldaCwfl4815.getCwf_Support_Tbl_Starting_Date_9s_Complement()),  //Natural: ASSIGN CWF-SUPPORT-TBL.STARTING-DATE-9S-COMPLEMENT := 999999999999999 - #TIME-OUT-N-15
                new DbsDecimal("999999999999999").subtract(pnd_Time_Out_A_15_Pnd_Time_Out_N_15));
            ldaCwfl4815.getCwf_Support_Tbl_Run_Id().setValue(pnd_New_Run_Id);                                                                                             //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-ID := #NEW-RUN-ID
            ldaCwfl4815.getCwf_Support_Tbl_Tbl_Entry_Dte_Tme().setValue(Global.getTIMX());                                                                                //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-ENTRY-DTE-TME := *TIMX
            ldaCwfl4815.getCwf_Support_Tbl_Tbl_Entry_Oprtr_Cde().setValue("CWFB4810");                                                                                    //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-ENTRY-OPRTR-CDE := 'CWFB4810'
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Mit_Wr_Selected().reset();                                                                                           //Natural: RESET CWF-SUPPORT-TBL.NUMBER-OF-MIT-WR-SELECTED CWF-SUPPORT-TBL.NUMBER-OF-RECS-PROCESSED CWF-SUPPORT-TBL.NUMBER-OF-WR-ADDED CWF-SUPPORT-TBL.NUMBER-OF-WR-UPDTD CWF-SUPPORT-TBL.NUMBER-OF-UNIT-ACT-ADDED CWF-SUPPORT-TBL.NUMBER-OF-UNIT-ACT-UPDTD CWF-SUPPORT-TBL.NUMBER-OF-EMPL-ACT-ADDED CWF-SUPPORT-TBL.NUMBER-OF-EMPL-ACT-UPDTD CWF-SUPPORT-TBL.NUMBER-OF-STEP-ACT-ADDED CWF-SUPPORT-TBL.NUMBER-OF-STEP-ACT-UPDTD CWF-SUPPORT-TBL.NUMBER-OF-STAT-ACT-ADDED CWF-SUPPORT-TBL.NUMBER-OF-STAT-ACT-UPDTD CWF-SUPPORT-TBL.NUMBER-OF-INTR-ACT-ADDED CWF-SUPPORT-TBL.NUMBER-OF-INTR-ACT-UPDTD CWF-SUPPORT-TBL.NUMBER-OF-EXTR-ACT-ADDED CWF-SUPPORT-TBL.NUMBER-OF-EXTR-ACT-UPDTD CWF-SUPPORT-TBL.NUMBER-OF-ENRT-ACT-ADDED CWF-SUPPORT-TBL.NUMBER-OF-ENRT-ACT-UPDTD CWF-SUPPORT-TBL.TBL-UPDTE-DTE-TME CWF-SUPPORT-TBL.TBL-UPDTE-DTE CWF-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE CWF-SUPPORT-TBL.TBL-DLTE-DTE-TME CWF-SUPPORT-TBL.TBL-DLTE-OPRTR-CDE
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Recs_Processed().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Wr_Added().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Wr_Updtd().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Unit_Act_Added().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Unit_Act_Updtd().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Empl_Act_Added().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Empl_Act_Updtd().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Step_Act_Added().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Step_Act_Updtd().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Stat_Act_Added().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Stat_Act_Updtd().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Intr_Act_Added().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Intr_Act_Updtd().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Extr_Act_Added().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Extr_Act_Updtd().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Enrt_Act_Added().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Enrt_Act_Updtd().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Tbl_Updte_Dte_Tme().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Tbl_Updte_Dte().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Tbl_Updte_Oprtr_Cde().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Tbl_Dlte_Dte_Tme().reset();
            ldaCwfl4815.getCwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde().reset();
            CREATED_CONTROL_RECORD:                                                                                                                                       //Natural: STORE CWF-SUPPORT-TBL
            ldaCwfl4815.getVw_cwf_Support_Tbl().insertDBRow("CREATED_CONTROL_RECORD");
            pdaCwfa4815.getCwfa4815_Output_Run_Control_Isn().setValue(ldaCwfl4815.getVw_cwf_Support_Tbl().getAstISN("LAST_COMPLETED_READ"));                              //Natural: ASSIGN CWFA4815-OUTPUT.RUN-CONTROL-ISN := *ISN ( CREATED-CONTROL-RECORD. )
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("LAST_COMPLETED_READ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("LAST_COMPLETED_READ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  LAST-COMPLETED-READ
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* (1210)
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(999);                                                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR = 999
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("NATURAL ERROR", Global.getERROR_NR(), "IN", Global.getPROGRAM(), "....LINE",                 //Natural: COMPRESS 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE INTO MSG-INFO-SUB.##MSG
            Global.getERROR_LINE()));
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
        //* (3550)
    };                                                                                                                                                                    //Natural: END-ERROR
}
