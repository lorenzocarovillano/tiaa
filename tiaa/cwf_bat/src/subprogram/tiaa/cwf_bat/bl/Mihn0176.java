/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:30:58 AM
**        * FROM NATURAL SUBPROGRAM : Mihn0176
************************************************************
**        * FILE NAME            : Mihn0176.java
**        * CLASS NAME           : Mihn0176
**        * INSTANCE NAME        : Mihn0176
************************************************************
************************************************************************
* PROGRAM  : MIHN0176
* SYSTEM   :
* TITLE    : READ RUN CONTROL FOR THE MIT ARCHIVE PROCESSES
* GENERATED: MAR 17,98 AT 01:45 PM
* FUNCTION : THIS SUB-PROGRAM CREATED SPECIFICALLY TO READ COMPLETED
*          : CONTROL RECORD TO PICKUP LAST DATE & TIME FIELD.
*
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mihn0176 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaMiha0100 pdaMiha0100;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaMihl0171 ldaMihl0171;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Who_Support_Tbl_Histogram;
    private DbsField cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key;

    private DataAccessProgramView vw_cwf_Who_Support_Tbl;
    private DbsField cwf_Who_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Who_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Who_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Who_Support_Tbl__R_Field_1;
    private DbsField cwf_Who_Support_Tbl_Pnd_Run_Status;
    private DbsField cwf_Who_Support_Tbl_Pnd_Starting_Time_9s_Complement;
    private DbsField cwf_Who_Support_Tbl_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField cwf_Who_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Who_Support_Tbl__R_Field_2;
    private DbsField cwf_Who_Support_Tbl_Pnd_Starting_Time;
    private DbsField cwf_Who_Support_Tbl_Pnd_Ending_Time;
    private DbsField cwf_Who_Support_Tbl_Tbl_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_3;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_4;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Starting_Time_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key_From;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_5;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_6;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Starting_Time_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key_To;

    private DbsGroup pnd_Tbl_Prime_Key_To__R_Field_7;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_To__R_Field_8;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Starting_Time_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaMihl0171 = new LdaMihl0171();
        registerRecord(ldaMihl0171);

        // parameters
        parameters = new DbsRecord();
        pdaMiha0100 = new PdaMiha0100(parameters);
        pdaCdaobj = new PdaCdaobj(parameters);
        pdaCwfpda_D = new PdaCwfpda_D(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pdaCwfpda_P = new PdaCwfpda_P(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Who_Support_Tbl_Histogram = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Support_Tbl_Histogram", "CWF-WHO-SUPPORT-TBL-HISTOGRAM"), 
            "CWF_WHO_SUPPORT_TBL", "CWF_WHO_SUPPORT");
        cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key = vw_cwf_Who_Support_Tbl_Histogram.getRecord().newFieldInGroup("cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key", 
            "TBL-PRIME-KEY", FieldType.STRING, 53, RepeatingFieldStrategy.None, "TBL_PRIME_KEY");
        cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key.setDdmHeader("RECORD KEY");
        cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Who_Support_Tbl_Histogram);

        vw_cwf_Who_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Support_Tbl", "CWF-WHO-SUPPORT-TBL"), "CWF_WHO_SUPPORT_TBL", "CWF_WHO_SUPPORT");
        cwf_Who_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Who_Support_Tbl.getRecord().newFieldInGroup("cwf_Who_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Who_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Who_Support_Tbl_Tbl_Table_Nme = vw_cwf_Who_Support_Tbl.getRecord().newFieldInGroup("cwf_Who_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Who_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Who_Support_Tbl_Tbl_Key_Field = vw_cwf_Who_Support_Tbl.getRecord().newFieldInGroup("cwf_Who_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Who_Support_Tbl__R_Field_1 = vw_cwf_Who_Support_Tbl.getRecord().newGroupInGroup("cwf_Who_Support_Tbl__R_Field_1", "REDEFINE", cwf_Who_Support_Tbl_Tbl_Key_Field);
        cwf_Who_Support_Tbl_Pnd_Run_Status = cwf_Who_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Who_Support_Tbl_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        cwf_Who_Support_Tbl_Pnd_Starting_Time_9s_Complement = cwf_Who_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Who_Support_Tbl_Pnd_Starting_Time_9s_Complement", 
            "#STARTING-TIME-9S-COMPLEMENT", FieldType.NUMERIC, 15);
        cwf_Who_Support_Tbl_Pnd_Rest_Of_Tbl_Key_Field = cwf_Who_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Who_Support_Tbl_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 14);
        cwf_Who_Support_Tbl_Tbl_Data_Field = vw_cwf_Who_Support_Tbl.getRecord().newFieldInGroup("cwf_Who_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", 
            FieldType.STRING, 253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Who_Support_Tbl__R_Field_2 = vw_cwf_Who_Support_Tbl.getRecord().newGroupInGroup("cwf_Who_Support_Tbl__R_Field_2", "REDEFINE", cwf_Who_Support_Tbl_Tbl_Data_Field);
        cwf_Who_Support_Tbl_Pnd_Starting_Time = cwf_Who_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Who_Support_Tbl_Pnd_Starting_Time", "#STARTING-TIME", 
            FieldType.TIME);
        cwf_Who_Support_Tbl_Pnd_Ending_Time = cwf_Who_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Who_Support_Tbl_Pnd_Ending_Time", "#ENDING-TIME", FieldType.TIME);
        cwf_Who_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Who_Support_Tbl.getRecord().newFieldInGroup("cwf_Who_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        registerRecord(vw_cwf_Who_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_3", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);

        pnd_Tbl_Prime_Key__R_Field_4 = pnd_Tbl_Prime_Key__R_Field_3.newGroupInGroup("pnd_Tbl_Prime_Key__R_Field_4", "REDEFINE", pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_Pnd_Run_Status = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_Pnd_Starting_Time_9s_Complement = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Starting_Time_9s_Complement", 
            "#STARTING-TIME-9S-COMPLEMENT", FieldType.NUMERIC, 15);
        pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field", "#REST-OF-TBL-KEY-FIELD", 
            FieldType.STRING, 14);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_From = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_From", "#TBL-PRIME-KEY-FROM", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_From__R_Field_5 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_From__R_Field_5", "REDEFINE", pnd_Tbl_Prime_Key_From);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_From__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_From__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_From__R_Field_6 = pnd_Tbl_Prime_Key_From__R_Field_5.newGroupInGroup("pnd_Tbl_Prime_Key_From__R_Field_6", "REDEFINE", pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status = pnd_Tbl_Prime_Key_From__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Run_Status", "#RUN-STATUS", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_From_Pnd_Starting_Time_9s_Complement = pnd_Tbl_Prime_Key_From__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Starting_Time_9s_Complement", 
            "#STARTING-TIME-9S-COMPLEMENT", FieldType.NUMERIC, 15);
        pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 14);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_From__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_To = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_To", "#TBL-PRIME-KEY-TO", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_To__R_Field_7 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_To__R_Field_7", "REDEFINE", pnd_Tbl_Prime_Key_To);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_To__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_To__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_To__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_To__R_Field_8 = pnd_Tbl_Prime_Key_To__R_Field_7.newGroupInGroup("pnd_Tbl_Prime_Key_To__R_Field_8", "REDEFINE", pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status = pnd_Tbl_Prime_Key_To__R_Field_8.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_To_Pnd_Starting_Time_9s_Complement = pnd_Tbl_Prime_Key_To__R_Field_8.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Starting_Time_9s_Complement", 
            "#STARTING-TIME-9S-COMPLEMENT", FieldType.NUMERIC, 15);
        pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_To__R_Field_8.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 14);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_To__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Who_Support_Tbl_Histogram.reset();
        vw_cwf_Who_Support_Tbl.reset();

        ldaMihl0171.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Mihn0176() throws Exception
    {
        super("Mihn0176");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("MIHN0176", onError);
        //* (TO ALLOW EASY ESCAPE FROM THE ROUTINE)
        ROUTINE:                                                                                                                                                          //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL
            sub_Read_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break ROUTINE;                                                                                                                                      //Natural: ESCAPE BOTTOM ( ROUTINE. )
            //*  SUBROUTINE JUST FOR ENDING THIS SUBPROGRAM IMMEDIATELY
            //*  ------------------------------------------------------
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-SUBPROGRAM
            //* (ROUTINE.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  SUBROUTINES
        //*  ===========
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL
        //*  ---------------------------------------------------------------------
        //*  LOOK UP THE MOST RECENT PERIOD ARCHIVED TO CREATE THE NEW RECORD
        //*  ----------------------------------------------------------------
        //*  ON NATURAL RUN TIME ERROR, RETURN WITH AN APPROPRIATE ERROR MESSAGE
        //*  ===================================================================
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Subprogram() throws Exception                                                                                                                 //Natural: ESCAPE-SUBPROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( ROUTINE. )
        Global.setEscapeCode(EscapeType.Bottom, "ROUTINE");
        if (true) return;
        //* (0800)
    }
    private void sub_Read_Control() throws Exception                                                                                                                      //Natural: READ-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue(ldaMihl0171.getMihl0171_Tbl_Scrty_Level_Ind());                                                                //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND := MIHL0171.TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue(ldaMihl0171.getMihl0171_Mit_Archive_Job_Type());                                                                     //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME := MIHL0171.MIT-ARCHIVE-JOB-TYPE
        pnd_Tbl_Prime_Key_Pnd_Run_Status.setValue(ldaMihl0171.getMihl0171_Completed_Status());                                                                            //Natural: ASSIGN #TBL-PRIME-KEY.#RUN-STATUS := MIHL0171.COMPLETED-STATUS
        pnd_Tbl_Prime_Key_Pnd_Starting_Time_9s_Complement.reset();                                                                                                        //Natural: RESET #TBL-PRIME-KEY.#STARTING-TIME-9S-COMPLEMENT #TBL-PRIME-KEY.#REST-OF-TBL-KEY-FIELD #TBL-PRIME-KEY.#TBL-ACTVE-IND
        pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field.reset();
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind.reset();
        vw_cwf_Who_Support_Tbl.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CWF-WHO-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "R1",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        R1:
        while (condition(vw_cwf_Who_Support_Tbl.readNextRow("R1")))
        {
            if (condition(cwf_Who_Support_Tbl_Tbl_Scrty_Level_Ind.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind) || cwf_Who_Support_Tbl_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)  //Natural: IF CWF-WHO-SUPPORT-TBL.TBL-SCRTY-LEVEL-IND NE #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND OR CWF-WHO-SUPPORT-TBL.TBL-TABLE-NME NE #TBL-PRIME-KEY.#TBL-TABLE-NME OR CWF-WHO-SUPPORT-TBL.#RUN-STATUS NE #TBL-PRIME-KEY.#RUN-STATUS
                || cwf_Who_Support_Tbl_Pnd_Run_Status.notEquals(pnd_Tbl_Prime_Key_Pnd_Run_Status)))
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(21);                                                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 21
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Security, Table name or Run status Error"));       //Natural: COMPRESS 'Program' *PROGRAM '- Security, Table name or Run status Error' INTO MSG-INFO-SUB.##MSG
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                //* (1030)
            }                                                                                                                                                             //Natural: END-IF
            pdaMiha0100.getMiha0100_Starting_Time().setValue(cwf_Who_Support_Tbl_Pnd_Starting_Time);                                                                      //Natural: ASSIGN MIHA0100.STARTING-TIME := CWF-WHO-SUPPORT-TBL.#STARTING-TIME
            pdaMiha0100.getMiha0100_Ending_Time().setValue(cwf_Who_Support_Tbl_Pnd_Ending_Time);                                                                          //Natural: ASSIGN MIHA0100.ENDING-TIME := CWF-WHO-SUPPORT-TBL.#ENDING-TIME
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  -------------------------------------
        //*  R1.
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(999);                                                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR = 999
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("NATURAL ERROR", Global.getERROR_NR(), "IN", Global.getPROGRAM(), "....LINE",                 //Natural: COMPRESS 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE INTO MSG-INFO-SUB.##MSG
            Global.getERROR_LINE()));
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
        //* (1260)
    };                                                                                                                                                                    //Natural: END-ERROR
}
