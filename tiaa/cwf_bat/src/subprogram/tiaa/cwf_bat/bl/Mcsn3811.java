/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:25:48 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3811
************************************************************
**        * FILE NAME            : Mcsn3811.java
**        * CLASS NAME           : Mcsn3811
**        * INSTANCE NAME        : Mcsn3811
************************************************************
************************************************************************
** RRATIADD - DAILY ATI ERROR SUMMARY
** ATTEMPTS TO JOIN FINALIST ERRORS AND CONTRACT NOT FOUND
*
*
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------  -------- ---------------------------------------------------
* 01/24/01  GREENFI  ADDED IN PROCESSING FOR 'CONTACT' & 'DISCO' SYSTEMS
*
* 02/23/2017 - JHANWAR - PIN EXPANSION - AUG 2017 <STOW ONLY>
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3811 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;

    private DataAccessProgramView vw_cwf_Ati_Fulfill;
    private DbsField cwf_Ati_Fulfill_Ati_Rcrd_Type;

    private DbsGroup cwf_Ati_Fulfill_Ati_Rqst_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Pin;
    private DbsField cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Stts;
    private DbsField cwf_Ati_Fulfill_Ati_Wpid;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Error_Txt;
    private DbsField cwf_Ati_Fulfill_Ati_Prge_Ind;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Racf_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Unit_Cde;
    private DbsField cwf_Ati_Fulfill_Ati_Stts_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Cmtask_Ind;
    private DbsField cwf_Ati_Fulfill_Addr_Update_Ind;
    private DbsField cwf_Ati_Fulfill_Addr_Chnge_All_Ind;
    private DbsGroup cwf_Ati_Fulfill_Addr_Cntrct_Accnt_NbrMuGroup;
    private DbsField cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr;
    private DbsGroup cwf_Ati_Fulfill_Addr_Addrss_TxtMuGroup;
    private DbsField cwf_Ati_Fulfill_Addr_Addrss_Txt;
    private DbsField cwf_Ati_Fulfill_Addr_Addrss_Type_Ind;
    private DbsField cwf_Ati_Fulfill_Addr_Annual_Vctn_Ind;
    private DbsField cwf_Ati_Fulfill_Addr_Effctve_Start_Dte;
    private DbsField cwf_Ati_Fulfill_Addr_Effctve_End_Dte;
    private DbsField cwf_Ati_Fulfill_Addr_Mail_Type_Ind;
    private DbsField cwf_Ati_Fulfill_Addr_Zip_Cde;
    private DbsField cwf_Ati_Fulfill_Addr_Usage_Ind;
    private DbsField pnd_Input_From;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;
    private DbsField pnd_Key_Pnd_Type1;
    private DbsField pnd_Key_Pnd_Entry_Dte_Tme1;
    private DbsField pnd_Key_Pnd_Prge;
    private DbsField pnd_Count_Bad;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Errors;
    private DbsField pnd_Title_Literal;
    private DbsField pnd_Error_Txt;
    private DbsField pnd_Error_Count;
    private DbsField pnd_Sum_Of_Error;
    private DbsField pnd_Pct_Of_Error;
    private DbsField pnd_Total_Error_Txt;
    private DbsField pnd_Total_Error_Count;
    private DbsField pnd_Input_Dte_D;
    private DbsField pnd_Stts_Date_D;
    private DbsField pnd_Entry_Date_D;
    private DbsField pnd_A;
    private DbsField pnd_Prev_Txt;
    private DbsField pnd_Stts_Date;
    private DbsField pnd_Stts_Date_Title;
    private DbsField pnd_Dte_Tme_A;

    private DbsGroup pnd_Dte_Tme_A__R_Field_2;
    private DbsField pnd_Dte_Tme_A_Pnd_Dte_Tme_Yyyymmdd;

    private DbsRecord internalLoopRecord;
    private DbsField for01Ati_Error_TxtOld;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Input_Data = parameters.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Ati_Fulfill = new DataAccessProgramView(new NameInfo("vw_cwf_Ati_Fulfill", "CWF-ATI-FULFILL"), "CWF_ATI_FULFILL_ADRS", "CWF_KDO_FULFILL");
        cwf_Ati_Fulfill_Ati_Rcrd_Type = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Rcrd_Type", "ATI-RCRD-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_RCRD_TYPE");

        cwf_Ati_Fulfill_Ati_Rqst_Id = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ATI_RQST_ID", "ATI-RQST-ID");
        cwf_Ati_Fulfill_Ati_Pin = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Pin", "ATI-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ATI_PIN");
        cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme", "ATI-MIT-LOG-DT-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_MIT_LOG_DT_TME");
        cwf_Ati_Fulfill_Ati_Prcss_Stts = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Stts", "ATI-PRCSS-STTS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRCSS_STTS");
        cwf_Ati_Fulfill_Ati_Wpid = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Wpid", "ATI-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ATI_WPID");
        cwf_Ati_Fulfill_Ati_Entry_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Dte_Tme", "ATI-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_ENTRY_DTE_TME");
        cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt", "ATI-ENTRY-SYSTM-OR-UNT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_SYSTM_OR_UNT");
        cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id", "ATI-PRCSS-APPLCTN-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_PRCSS_APPLCTN_ID");
        cwf_Ati_Fulfill_Ati_Error_Txt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Error_Txt", "ATI-ERROR-TXT", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "ATI_ERROR_TXT");
        cwf_Ati_Fulfill_Ati_Prge_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prge_Ind", "ATI-PRGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRGE_IND");
        cwf_Ati_Fulfill_Ati_Entry_Racf_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Racf_Id", "ATI-ENTRY-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ATI_ENTRY_RACF_ID");
        cwf_Ati_Fulfill_Ati_Entry_Unit_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Unit_Cde", "ATI-ENTRY-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_UNIT_CDE");
        cwf_Ati_Fulfill_Ati_Stts_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Stts_Dte_Tme", "ATI-STTS-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_STTS_DTE_TME");
        cwf_Ati_Fulfill_Ati_Cmtask_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Cmtask_Ind", "ATI-CMTASK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_CMTASK_IND");
        cwf_Ati_Fulfill_Addr_Update_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Update_Ind", "ADDR-UPDATE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADDR_UPDATE_IND");
        cwf_Ati_Fulfill_Addr_Update_Ind.setDdmHeader("ADDRESS/UPDATE IND");
        cwf_Ati_Fulfill_Addr_Chnge_All_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Chnge_All_Ind", "ADDR-CHNGE-ALL-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADDR_CHNGE_ALL_IND");
        cwf_Ati_Fulfill_Addr_Chnge_All_Ind.setDdmHeader("CHANGE ALL/CONTRACT");
        cwf_Ati_Fulfill_Addr_Cntrct_Accnt_NbrMuGroup = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ADDR_CNTRCT_ACCNT_NBRMuGroup", 
            "ADDR_CNTRCT_ACCNT_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_KDO_FULFILL_ADDR_CNTRCT_ACCNT_NBR");
        cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr = cwf_Ati_Fulfill_Addr_Cntrct_Accnt_NbrMuGroup.newFieldArrayInGroup("cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr", 
            "ADDR-CNTRCT-ACCNT-NBR", FieldType.STRING, 10, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADDR_CNTRCT_ACCNT_NBR");
        cwf_Ati_Fulfill_Addr_Cntrct_Accnt_Nbr.setDdmHeader("CNTRCT/ACCT/NUMBER");
        cwf_Ati_Fulfill_Addr_Addrss_TxtMuGroup = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ADDR_ADDRSS_TXTMuGroup", "ADDR_ADDRSS_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_KDO_FULFILL_ADDR_ADDRSS_TXT");
        cwf_Ati_Fulfill_Addr_Addrss_Txt = cwf_Ati_Fulfill_Addr_Addrss_TxtMuGroup.newFieldArrayInGroup("cwf_Ati_Fulfill_Addr_Addrss_Txt", "ADDR-ADDRSS-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADDR_ADDRSS_TXT");
        cwf_Ati_Fulfill_Addr_Addrss_Txt.setDdmHeader("ADDRESS");
        cwf_Ati_Fulfill_Addr_Addrss_Type_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Addrss_Type_Ind", "ADDR-ADDRSS-TYPE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADDR_ADDRSS_TYPE_IND");
        cwf_Ati_Fulfill_Addr_Addrss_Type_Ind.setDdmHeader("ADDRESS/TYPE");
        cwf_Ati_Fulfill_Addr_Annual_Vctn_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Annual_Vctn_Ind", "ADDR-ANNUAL-VCTN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADDR_ANNUAL_VCTN_IND");
        cwf_Ati_Fulfill_Addr_Annual_Vctn_Ind.setDdmHeader("ANNUAL/VACATION");
        cwf_Ati_Fulfill_Addr_Effctve_Start_Dte = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Effctve_Start_Dte", "ADDR-EFFCTVE-START-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADDR_EFFCTVE_START_DTE");
        cwf_Ati_Fulfill_Addr_Effctve_Start_Dte.setDdmHeader("START/DATE");
        cwf_Ati_Fulfill_Addr_Effctve_End_Dte = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Effctve_End_Dte", "ADDR-EFFCTVE-END-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADDR_EFFCTVE_END_DTE");
        cwf_Ati_Fulfill_Addr_Effctve_End_Dte.setDdmHeader("END/DATE");
        cwf_Ati_Fulfill_Addr_Mail_Type_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Mail_Type_Ind", "ADDR-MAIL-TYPE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADDR_MAIL_TYPE_IND");
        cwf_Ati_Fulfill_Addr_Mail_Type_Ind.setDdmHeader("MAIL/TYPE");
        cwf_Ati_Fulfill_Addr_Zip_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Zip_Cde", "ADDR-ZIP-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "ADDR_ZIP_CDE");
        cwf_Ati_Fulfill_Addr_Zip_Cde.setDdmHeader("ZIP");
        cwf_Ati_Fulfill_Addr_Usage_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Addr_Usage_Ind", "ADDR-USAGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADDR_USAGE_IND");
        cwf_Ati_Fulfill_Addr_Usage_Ind.setDdmHeader("USAGE");
        registerRecord(vw_cwf_Ati_Fulfill);

        pnd_Input_From = localVariables.newFieldInRecord("pnd_Input_From", "#INPUT-FROM", FieldType.STRING, 15);
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 9);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);
        pnd_Key_Pnd_Type1 = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Type1", "#TYPE1", FieldType.STRING, 1);
        pnd_Key_Pnd_Entry_Dte_Tme1 = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Entry_Dte_Tme1", "#ENTRY-DTE-TME1", FieldType.TIME);
        pnd_Key_Pnd_Prge = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Prge", "#PRGE", FieldType.STRING, 1);
        pnd_Count_Bad = localVariables.newFieldInRecord("pnd_Count_Bad", "#COUNT-BAD", FieldType.NUMERIC, 9);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 9);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 9);
        pnd_Errors = localVariables.newFieldInRecord("pnd_Errors", "#ERRORS", FieldType.PACKED_DECIMAL, 9);
        pnd_Title_Literal = localVariables.newFieldInRecord("pnd_Title_Literal", "#TITLE-LITERAL", FieldType.STRING, 45);
        pnd_Error_Txt = localVariables.newFieldArrayInRecord("pnd_Error_Txt", "#ERROR-TXT", FieldType.STRING, 35, new DbsArrayController(1, 200));
        pnd_Error_Count = localVariables.newFieldArrayInRecord("pnd_Error_Count", "#ERROR-COUNT", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            200));
        pnd_Sum_Of_Error = localVariables.newFieldInRecord("pnd_Sum_Of_Error", "#SUM-OF-ERROR", FieldType.PACKED_DECIMAL, 9);
        pnd_Pct_Of_Error = localVariables.newFieldInRecord("pnd_Pct_Of_Error", "#PCT-OF-ERROR", FieldType.PACKED_DECIMAL, 7, 4);
        pnd_Total_Error_Txt = localVariables.newFieldArrayInRecord("pnd_Total_Error_Txt", "#TOTAL-ERROR-TXT", FieldType.STRING, 35, new DbsArrayController(1, 
            200));
        pnd_Total_Error_Count = localVariables.newFieldArrayInRecord("pnd_Total_Error_Count", "#TOTAL-ERROR-COUNT", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            200));
        pnd_Input_Dte_D = localVariables.newFieldInRecord("pnd_Input_Dte_D", "#INPUT-DTE-D", FieldType.DATE);
        pnd_Stts_Date_D = localVariables.newFieldInRecord("pnd_Stts_Date_D", "#STTS-DATE-D", FieldType.DATE);
        pnd_Entry_Date_D = localVariables.newFieldInRecord("pnd_Entry_Date_D", "#ENTRY-DATE-D", FieldType.DATE);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_Prev_Txt = localVariables.newFieldInRecord("pnd_Prev_Txt", "#PREV-TXT", FieldType.STRING, 35);
        pnd_Stts_Date = localVariables.newFieldInRecord("pnd_Stts_Date", "#STTS-DATE", FieldType.STRING, 8);
        pnd_Stts_Date_Title = localVariables.newFieldInRecord("pnd_Stts_Date_Title", "#STTS-DATE-TITLE", FieldType.STRING, 8);
        pnd_Dte_Tme_A = localVariables.newFieldInRecord("pnd_Dte_Tme_A", "#DTE-TME-A", FieldType.STRING, 15);

        pnd_Dte_Tme_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Dte_Tme_A__R_Field_2", "REDEFINE", pnd_Dte_Tme_A);
        pnd_Dte_Tme_A_Pnd_Dte_Tme_Yyyymmdd = pnd_Dte_Tme_A__R_Field_2.newFieldInGroup("pnd_Dte_Tme_A_Pnd_Dte_Tme_Yyyymmdd", "#DTE-TME-YYYYMMDD", FieldType.STRING, 
            8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        for01Ati_Error_TxtOld = internalLoopRecord.newFieldInRecord("For01_Ati_Error_Txt_OLD", "Ati_Error_Txt_OLD", FieldType.STRING, 35);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Ati_Fulfill.reset();
        internalLoopRecord.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Mcsn3811() throws Exception
    {
        super("Mcsn3811");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*  ----------------------------------------------------------------------
        //*   MAIN PROGRAM
        //*  ----------------------------------------------------------------------
        if (condition(! (DbsUtil.maskMatches(pnd_Input_Data_Pnd_Input_Date,"YYYYMMDD"))))                                                                                 //Natural: IF #INPUT-DATE NE MASK ( YYYYMMDD )
        {
            getReports().write(6, "DATE MUST BE IN FORMAT YYYYMMDD");                                                                                                     //Natural: WRITE ( 6 ) 'DATE MUST BE IN FORMAT YYYYMMDD'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().newPage(new ReportSpecification(6));                                                                                                                 //Natural: FORMAT ( 6 ) PS = 55;//Natural: NEWPAGE ( 6 )
        if (condition(Global.isEscape())){return;}
        pnd_Key_Pnd_Type1.setValue("3");                                                                                                                                  //Natural: MOVE '3' TO #TYPE1
        pnd_Key_Pnd_Entry_Dte_Tme1.setValueEdited(new ReportEditMask("HHIISST"),"0000000");                                                                               //Natural: MOVE EDITED '0000000' TO #ENTRY-DTE-TME1 ( EM = HHIISST )
        //*  MOVE EDITED #INPUT-DATE TO #ENTRY-DTE-TME1(EM=YYYYMMDD)
        vw_cwf_Ati_Fulfill.startDatabaseRead                                                                                                                              //Natural: READ CWF-ATI-FULFILL WITH ATI-COMPLETED-KEY = #KEY
        (
        "READ01",
        new Wc[] { new Wc("ATI_COMPLETED_KEY", ">=", pnd_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("ATI_COMPLETED_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Ati_Fulfill.readNextRow("READ01")))
        {
            pnd_Stts_Date.setValueEdited(cwf_Ati_Fulfill_Ati_Stts_Dte_Tme,new ReportEditMask("YYYYMMDD"));                                                                //Natural: MOVE EDITED ATI-STTS-DTE-TME ( EM = YYYYMMDD ) TO #STTS-DATE
            pnd_Stts_Date_Title.setValueEdited(cwf_Ati_Fulfill_Ati_Stts_Dte_Tme,new ReportEditMask("MMDDYYYY"));                                                          //Natural: MOVE EDITED ATI-STTS-DTE-TME ( EM = MMDDYYYY ) TO #STTS-DATE-TITLE
            //*  SET UP DATE FIELDS IN (D) FORMAT FOR ACCEPT CHECK
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Stts_Date_D.setValue(cwf_Ati_Fulfill_Ati_Stts_Dte_Tme);                                                                                                   //Natural: MOVE ATI-STTS-DTE-TME TO #STTS-DATE-D
            pnd_Entry_Date_D.setValue(cwf_Ati_Fulfill_Ati_Entry_Dte_Tme);                                                                                                 //Natural: MOVE ATI-ENTRY-DTE-TME TO #ENTRY-DATE-D
            if (condition(cwf_Ati_Fulfill_Ati_Rcrd_Type.notEquals("3")))                                                                                                  //Natural: IF ATI-RCRD-TYPE NE '3'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id.equals("ADRS") && cwf_Ati_Fulfill_Ati_Error_Txt.notEquals(" ") && pnd_Stts_Date_D.equals(pnd_Input_Dte_D)))) //Natural: ACCEPT IF ATI-PRCSS-APPLCTN-ID = 'ADRS' AND ATI-ERROR-TXT NE ' ' AND #STTS-DATE-D = #INPUT-DTE-D
            {
                continue;
            }
            //*  THE FOLLOWING CODE WILL DIVIDE ICSS CONTACT SHEETS FROM
            //*  MUTUAL FUND ATIS RECEIVED FROM THE VENDOR
            if (condition(cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("ICSS") && cwf_Ati_Fulfill_Ati_Entry_Unit_Cde.equals("MFTA")))                                    //Natural: IF CWF-ATI-FULFILL.ATI-ENTRY-SYSTM-OR-UNT = 'ICSS' AND CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE = 'MFTA'
            {
                cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.setValue("ICST");                                                                                                  //Natural: MOVE 'ICST' TO CWF-ATI-FULFILL.ATI-ENTRY-SYSTM-OR-UNT
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme,  //Natural: END-ALL
                cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Racf_Id, cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Wpid, 
                pnd_Stts_Date_Title);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme);                                    //Natural: SORT RECORDS BY CWF-ATI-FULFILL.ATI-ENTRY-SYSTM-OR-UNT CWF-ATI-FULFILL.ATI-ERROR-TXT CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME USING CWF-ATI-FULFILL.ATI-ENTRY-DTE-TME CWF-ATI-FULFILL.ATI-STTS-DTE-TME CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE CWF-ATI-FULFILL.ATI-PIN CWF-ATI-FULFILL.ATI-WPID #STTS-DATE-TITLE
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, 
            cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Racf_Id, cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, 
            cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Wpid, pnd_Stts_Date_Title)))
        {
            CheckAtStartofData148();

            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK ATI-ERROR-TXT
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*                                                                                                                                                           //Natural: AT BREAK ATI-ENTRY-SYSTM-OR-UNT
            pnd_Count_Bad.nadd(1);                                                                                                                                        //Natural: AT END OF DATA;//Natural: ADD 1 TO #COUNT-BAD
            pnd_Errors.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #ERRORS
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
            getReports().newPage(new ReportSpecification(6));                                                                                                             //Natural: NEWPAGE ( 6 )
            if (condition(Global.isEscape())){return;}
            pnd_Title_Literal.setValue("              TOTAL FOR ALL SYSTEMS          ");                                                                                  //Natural: MOVE '              TOTAL FOR ALL SYSTEMS          ' TO #TITLE-LITERAL
            pnd_Sum_Of_Error.nadd(pnd_Total_Error_Count.getValue("*"));                                                                                                   //Natural: ADD #TOTAL-ERROR-COUNT ( * ) TO #SUM-OF-ERROR
            FOR04:                                                                                                                                                        //Natural: FOR #I EQ 1 TO 200
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(200)); pnd_I.nadd(1))
            {
                if (condition(pnd_Total_Error_Txt.getValue(pnd_I).equals(" ")))                                                                                           //Natural: IF #TOTAL-ERROR-TXT ( #I ) EQ ' '
                {
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Pct_Of_Error.compute(new ComputeParameters(true, pnd_Pct_Of_Error), (pnd_Total_Error_Count.getValue(pnd_I).divide(pnd_Sum_Of_Error)).multiply(100));  //Natural: COMPUTE ROUNDED #PCT-OF-ERROR = ( #TOTAL-ERROR-COUNT ( #I ) / #SUM-OF-ERROR ) * 100
                getReports().write(6, pnd_Total_Error_Txt.getValue(pnd_I),new TabSetting(40),pnd_Total_Error_Count.getValue(pnd_I), new ReportEditMask                    //Natural: WRITE ( 6 ) #TOTAL-ERROR-TXT ( #I ) 40T #TOTAL-ERROR-COUNT ( #I ) ( EM = ZZZZZZZZ9 ) 53T #PCT-OF-ERROR ( EM = ZZ9.9 ) /
                    ("ZZZZZZZZ9"),new TabSetting(53),pnd_Pct_Of_Error, new ReportEditMask ("ZZ9.9"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            getReports().write(6, NEWLINE,NEWLINE,"THE TOTAL NUMBER OF ERRORS FOR THE DAY IS",pnd_Sum_Of_Error);                                                          //Natural: WRITE ( 6 ) // 'THE TOTAL NUMBER OF ERRORS FOR THE DAY IS' #SUM-OF-ERROR
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 6 ) TITLE LEFT *PROGRAM 10X 'NAME AND ADDRESS ANALYSIS REPORT SUMMARY' 5X 'PAGE:' *PAGE-NUMBER ( 6 ) / *DATX 14X 'FOR ATIS PROCESSED ON ' #STTS-DATE-TITLE ( EM = XX/XX/XXXX ) // 15T #TITLE-LITERAL // '        ERROR TEXT LITERAL          ' 43T 'NUMBER' 54T 'PERCENT' / '====================================' 43T '======' 54T '=======' //
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TITLE-LITERAL
    }
    private void sub_Title_Literal() throws Exception                                                                                                                     //Natural: TITLE-LITERAL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        short decideConditionsMet207 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE ATI-ENTRY-SYSTM-OR-UNT;//Natural: VALUE 'ICSS'
        if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("ICSS"))))
        {
            decideConditionsMet207++;
            pnd_Title_Literal.setValue("INSURANCE SERVICES CONTACT SHEET SYSTEM(ICSS)");                                                                                  //Natural: ASSIGN #TITLE-LITERAL := 'INSURANCE SERVICES CONTACT SHEET SYSTEM(ICSS)'
        }                                                                                                                                                                 //Natural: VALUE 'ICST'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("ICST"))))
        {
            decideConditionsMet207++;
            pnd_Title_Literal.setValue(" MUTUAL FUNDS VENDOR SYSTEM (ICSS UNIT MFTA) ");                                                                                  //Natural: ASSIGN #TITLE-LITERAL := ' MUTUAL FUNDS VENDOR SYSTEM (ICSS UNIT MFTA) '
        }                                                                                                                                                                 //Natural: VALUE 'MCSG'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("MCSG"))))
        {
            decideConditionsMet207++;
            pnd_Title_Literal.setValue("    MAINFRAME CONTACT SHEET SYSTEM(MCSG)     ");                                                                                  //Natural: ASSIGN #TITLE-LITERAL := '    MAINFRAME CONTACT SHEET SYSTEM(MCSG)     '
        }                                                                                                                                                                 //Natural: VALUE 'PSS'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("PSS"))))
        {
            decideConditionsMet207++;
            pnd_Title_Literal.setValue("   PARTICIPANT SERVICES CONTACT SYSTEM  (PSS)");                                                                                  //Natural: ASSIGN #TITLE-LITERAL := '   PARTICIPANT SERVICES CONTACT SYSTEM  (PSS)'
        }                                                                                                                                                                 //Natural: VALUE 'INTACT'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("INTACT"))))
        {
            decideConditionsMet207++;
            pnd_Title_Literal.setValue("               INTERACT SYSTEM               ");                                                                                  //Natural: ASSIGN #TITLE-LITERAL := '               INTERACT SYSTEM               '
        }                                                                                                                                                                 //Natural: VALUE 'CONTACT'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("CONTACT"))))
        {
            decideConditionsMet207++;
            pnd_Title_Literal.setValue("                CONTACT SYSTEM               ");                                                                                  //Natural: ASSIGN #TITLE-LITERAL := '                CONTACT SYSTEM               '
        }                                                                                                                                                                 //Natural: VALUE 'DISCO'
        else if (condition((cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.equals("DISCO"))))
        {
            decideConditionsMet207++;
            pnd_Title_Literal.setValue("         DISCONNECTED CONTACT SYSTEM         ");                                                                                  //Natural: ASSIGN #TITLE-LITERAL := '         DISCONNECTED CONTACT SYSTEM         '
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Title_Literal.setValue("               UNKNOWN ATI SOURCE            ");                                                                                  //Natural: ASSIGN #TITLE-LITERAL := '               UNKNOWN ATI SOURCE            '
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Ati_Fulfill_Ati_Error_TxtIsBreak = cwf_Ati_Fulfill_Ati_Error_Txt.isBreak(endOfData);
        boolean cwf_Ati_Fulfill_Ati_Entry_Systm_Or_UntIsBreak = cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt.isBreak(endOfData);
        if (condition(cwf_Ati_Fulfill_Ati_Error_TxtIsBreak || cwf_Ati_Fulfill_Ati_Entry_Systm_Or_UntIsBreak))
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I EQ 1 TO 200
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(200)); pnd_I.nadd(1))
            {
                if (condition(pnd_Error_Txt.getValue(pnd_I).equals(" ")))                                                                                                 //Natural: IF #ERROR-TXT ( #I ) EQ ' '
                {
                    pnd_Error_Txt.getValue(pnd_I).setValue(for01Ati_Error_TxtOld);                                                                                        //Natural: MOVE OLD ( ATI-ERROR-TXT ) TO #ERROR-TXT ( #I )
                    pnd_Error_Count.getValue(pnd_I).setValue(pnd_Errors);                                                                                                 //Natural: MOVE #ERRORS TO #ERROR-COUNT ( #I )
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            pnd_Errors.reset();                                                                                                                                           //Natural: RESET #ERRORS
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(cwf_Ati_Fulfill_Ati_Entry_Systm_Or_UntIsBreak))
        {
            getReports().newPage(new ReportSpecification(6));                                                                                                             //Natural: NEWPAGE ( 6 )
            if (condition(Global.isEscape())){return;}
            pnd_Sum_Of_Error.nadd(pnd_Error_Count.getValue("*"));                                                                                                         //Natural: ADD #ERROR-COUNT ( * ) TO #SUM-OF-ERROR
            FOR02:                                                                                                                                                        //Natural: FOR #I EQ 1 TO 200
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(200)); pnd_I.nadd(1))
            {
                if (condition(pnd_Error_Txt.getValue(pnd_I).equals(" ")))                                                                                                 //Natural: IF #ERROR-TXT ( #I ) EQ ' '
                {
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Pct_Of_Error.compute(new ComputeParameters(true, pnd_Pct_Of_Error), (pnd_Error_Count.getValue(pnd_I).divide(pnd_Sum_Of_Error)).multiply(100));        //Natural: COMPUTE ROUNDED #PCT-OF-ERROR = ( #ERROR-COUNT ( #I ) / #SUM-OF-ERROR ) * 100
                getReports().write(6, pnd_Error_Txt.getValue(pnd_I),new TabSetting(40),pnd_Error_Count.getValue(pnd_I), new ReportEditMask ("ZZZZZZZZ9"),new              //Natural: WRITE ( 6 ) #ERROR-TXT ( #I ) 40T #ERROR-COUNT ( #I ) ( EM = ZZZZZZZZ9 ) 53T #PCT-OF-ERROR ( EM = ZZ9.9 ) /
                    TabSetting(53),pnd_Pct_Of_Error, new ReportEditMask ("ZZ9.9"),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR03:                                                                                                                                                    //Natural: FOR #J EQ 1 TO 50
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(50)); pnd_J.nadd(1))
                {
                    if (condition(pnd_Error_Txt.getValue(pnd_I).equals(pnd_Total_Error_Txt.getValue(pnd_J))))                                                             //Natural: IF #ERROR-TXT ( #I ) EQ #TOTAL-ERROR-TXT ( #J )
                    {
                        pnd_Total_Error_Count.getValue(pnd_J).nadd(pnd_Error_Count.getValue(pnd_I));                                                                      //Natural: ADD #ERROR-COUNT ( #I ) TO #TOTAL-ERROR-COUNT ( #J )
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Total_Error_Txt.getValue(pnd_J).equals(" ")))                                                                                       //Natural: IF #TOTAL-ERROR-TXT ( #J ) EQ ' '
                    {
                        pnd_Total_Error_Txt.getValue(pnd_J).setValue(pnd_Error_Txt.getValue(pnd_I));                                                                      //Natural: MOVE #ERROR-TXT ( #I ) TO #TOTAL-ERROR-TXT ( #J )
                        pnd_Total_Error_Count.getValue(pnd_J).setValue(pnd_Error_Count.getValue(pnd_I));                                                                  //Natural: MOVE #ERROR-COUNT ( #I ) TO #TOTAL-ERROR-COUNT ( #J )
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            getReports().write(6, NEWLINE,NEWLINE,"THE TOTAL NUMBER OF ERRORS FOR THIS SYSTEM =",pnd_Sum_Of_Error);                                                       //Natural: WRITE ( 6 ) // 'THE TOTAL NUMBER OF ERRORS FOR THIS SYSTEM =' #SUM-OF-ERROR
            if (condition(Global.isEscape())) return;
            pnd_Error_Txt.getValue("*").reset();                                                                                                                          //Natural: RESET #ERROR-TXT ( * ) #ERROR-COUNT ( * ) #SUM-OF-ERROR
            pnd_Error_Count.getValue("*").reset();
            pnd_Sum_Of_Error.reset();
                                                                                                                                                                          //Natural: PERFORM TITLE-LITERAL
            sub_Title_Literal();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(6, "PS=55");

        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(10),"NAME AND ADDRESS ANALYSIS REPORT SUMMARY",new 
            ColumnSpacing(5),"PAGE:",getReports().getPageNumberDbs(6),NEWLINE,Global.getDATX(),new ColumnSpacing(14),"FOR ATIS PROCESSED ON ",pnd_Stts_Date_Title, 
            new ReportEditMask ("XX/XX/XXXX"),NEWLINE,NEWLINE,new TabSetting(15),pnd_Title_Literal,NEWLINE,NEWLINE,"        ERROR TEXT LITERAL          ",new 
            TabSetting(43),"NUMBER",new TabSetting(54),"PERCENT",NEWLINE,"====================================",new TabSetting(43),"======",new TabSetting(54),
            "=======",NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData148() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM TITLE-LITERAL
            sub_Title_Literal();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
