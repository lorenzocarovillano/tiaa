/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:29:40 AM
**        * FROM NATURAL SUBPROGRAM : Wfon4500
************************************************************
**        * FILE NAME            : Wfon4500.java
**        * CLASS NAME           : Wfon4500
**        * INSTANCE NAME        : Wfon4500
************************************************************
************************************************************************
* PROGRAM  : WFON4500
* SYSTEM   : PROJCWF
* TITLE    : CHECK SUCCESS/INIT/ERROR CONTAINER
* FUNCTION : CHECKS WHETHER CONTAINER IS SUCCESSFUL OR NOT. USED BY
*          : WFOB4500
* HISTORY    BY    DESCRIPTION OF CHANGE
* MM/DD/YY  XXXXXX XXXXXXXXXXXXXXXXXXXXX
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Wfon4500 extends BLNatBase
{
    // Data Areas
    private LdaWfol4502 ldaWfol4502;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Curr_Cont_Id;
    private DbsField pnd_Init_Error_Cont;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaWfol4502 = new LdaWfol4502();
        registerRecord(ldaWfol4502);
        registerRecord(ldaWfol4502.getVw_cmcv3());

        // parameters
        parameters = new DbsRecord();
        pnd_Curr_Cont_Id = parameters.newFieldInRecord("pnd_Curr_Cont_Id", "#CURR-CONT-ID", FieldType.STRING, 20);
        pnd_Curr_Cont_Id.setParameterOption(ParameterOption.ByReference);
        pnd_Init_Error_Cont = parameters.newFieldInRecord("pnd_Init_Error_Cont", "#INIT-ERROR-CONT", FieldType.STRING, 1);
        pnd_Init_Error_Cont.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaWfol4502.initializeValues();

        parameters.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Wfon4500() throws Exception
    {
        super("Wfon4500");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  CALLNAT 'WFON4500' USING #CURR-CONT-ID
        //*                           #INIT-ERROR-CONT
        ldaWfol4502.getVw_cmcv3().startDatabaseRead                                                                                                                       //Natural: READ CMCV3 BY CONTAINER-RCRD-TYPE-KEY = #CURR-CONT-ID
        (
        "READ01",
        new Wc[] { new Wc("CONTAINER_RCRD_TYPE_KEY", ">=", pnd_Curr_Cont_Id, WcType.BY) },
        new Oc[] { new Oc("CONTAINER_RCRD_TYPE_KEY", "ASC") }
        );
        READ01:
        while (condition(ldaWfol4502.getVw_cmcv3().readNextRow("READ01")))
        {
            if (condition(ldaWfol4502.getCmcv3_Container_Id().notEquals(pnd_Curr_Cont_Id)))                                                                               //Natural: IF CMCV3.CONTAINER-ID NE #CURR-CONT-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaWfol4502.getCmcv3_Error_Msg_Txt().notEquals(" ")))                                                                                           //Natural: IF CMCV3.ERROR-MSG-TXT NE ' '
            {
                pnd_Init_Error_Cont.setValue("Y");                                                                                                                        //Natural: MOVE 'Y' TO #INIT-ERROR-CONT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
