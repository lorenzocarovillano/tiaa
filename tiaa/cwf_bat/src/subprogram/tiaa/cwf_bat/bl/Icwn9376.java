/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:06:52 AM
**        * FROM NATURAL SUBPROGRAM : Icwn9376
************************************************************
**        * FILE NAME            : Icwn9376.java
**        * CLASS NAME           : Icwn9376
**        * INSTANCE NAME        : Icwn9376
************************************************************
************************************************************************
* PROGRAM  : ICWN9376
* SYSTEM   : PROJICW
* FUNCTION : COMPUTES THE ELAPSED DAYS BETWEEN 2
*          |   DATES & TIME
*          | USED FOR CALENDAR CORP TURNAROUND CALCULATION
*          |
*          |
*          |  CHECK TO-DAYS GT 999
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwn9376 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCwfpda_M pdaCwfpda_M;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Tiaa_Rcvd_Dte_Tme;
    private DbsField pnd_Instn_Close_Dte_Tme;
    private DbsField pnd_Elapsed_Days;
    private DbsField pnd_Tme_T;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pnd_Tiaa_Rcvd_Dte_Tme = parameters.newFieldInRecord("pnd_Tiaa_Rcvd_Dte_Tme", "#TIAA-RCVD-DTE-TME", FieldType.TIME);
        pnd_Tiaa_Rcvd_Dte_Tme.setParameterOption(ParameterOption.ByReference);
        pnd_Instn_Close_Dte_Tme = parameters.newFieldInRecord("pnd_Instn_Close_Dte_Tme", "#INSTN-CLOSE-DTE-TME", FieldType.TIME);
        pnd_Instn_Close_Dte_Tme.setParameterOption(ParameterOption.ByReference);
        pnd_Elapsed_Days = parameters.newFieldInRecord("pnd_Elapsed_Days", "#ELAPSED-DAYS", FieldType.NUMERIC, 9, 2);
        pnd_Elapsed_Days.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Tme_T = localVariables.newFieldInRecord("pnd_Tme_T", "#TME-T", FieldType.PACKED_DECIMAL, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Icwn9376() throws Exception
    {
        super("Icwn9376");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        if (condition(pnd_Instn_Close_Dte_Tme.lessOrEqual(pnd_Tiaa_Rcvd_Dte_Tme)))                                                                                        //Natural: IF #INSTN-CLOSE-DTE-TME LE #TIAA-RCVD-DTE-TME
        {
            pnd_Elapsed_Days.setValue(0);                                                                                                                                 //Natural: MOVE 0 TO #ELAPSED-DAYS
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Elapsed_Days.reset();                                                                                                                                         //Natural: RESET #ELAPSED-DAYS
        pnd_Tme_T.compute(new ComputeParameters(false, pnd_Tme_T), pnd_Instn_Close_Dte_Tme.subtract(pnd_Tiaa_Rcvd_Dte_Tme));                                              //Natural: COMPUTE #TME-T = #INSTN-CLOSE-DTE-TME - #TIAA-RCVD-DTE-TME
        pnd_Elapsed_Days.compute(new ComputeParameters(false, pnd_Elapsed_Days), pnd_Tme_T.divide(864000));                                                               //Natural: COMPUTE #ELAPSED-DAYS = #TME-T / 864000
    }

    //
}
