/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:53:37 AM
**        * FROM NATURAL SUBPROGRAM : Icwn2613
************************************************************
**        * FILE NAME            : Icwn2613.java
**        * CLASS NAME           : Icwn2613
**        * INSTANCE NAME        : Icwn2613
************************************************************
* SUBPROGRAM ICWN2612 - IRIS UPDATE INTERFACE

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwn2613 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    public DbsRecord parameters;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaIcwa2613 pdaIcwa2613;
    private PdaIcwa2611 pdaIcwa2611;
    private PdaCdaobj pdaCdaobj;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Parm_Date;
    private DbsField pnd_Parm_Records;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaIcwa2613 = new PdaIcwa2613(localVariables);
        pdaIcwa2611 = new PdaIcwa2611(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Parm_Date = parameters.newFieldInRecord("pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);
        pnd_Parm_Date.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Records = parameters.newFieldInRecord("pnd_Parm_Records", "#PARM-RECORDS", FieldType.NUMERIC, 5);
        pnd_Parm_Records.setParameterOption(ParameterOption.ByReference);
        pdaCwfpda_D = new PdaCwfpda_D(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pdaCwfpda_P = new PdaCwfpda_P(parameters);
        pdaCwfpdaun = new PdaCwfpdaun(parameters);
        pdaCwfpdaus = new PdaCwfpdaus(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Icwn2613() throws Exception
    {
        super("Icwn2613");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  COPY GDA VARIABLES TO PDA's
        gdaCwfg000.getDialog_Info().setValuesByName(pdaCwfpda_D.getDialog_Info_Sub());                                                                                    //Natural: MOVE BY NAME DIALOG-INFO-SUB TO DIALOG-INFO
        gdaCwfg000.getMsg_Info().setValuesByName(pdaCwfpda_M.getMsg_Info_Sub());                                                                                          //Natural: MOVE BY NAME MSG-INFO-SUB TO MSG-INFO
        gdaCwfg000.getPass().setValuesByName(pdaCwfpda_P.getPass_Sub());                                                                                                  //Natural: MOVE BY NAME PASS-SUB TO PASS
        gdaCwfg000.getParm_Employee_Info().setValuesByName(pdaCwfpdaus.getParm_Employee_Info_Sub());                                                                      //Natural: MOVE BY NAME PARM-EMPLOYEE-INFO-SUB TO PARM-EMPLOYEE-INFO
        gdaCwfg000.getParm_Unit_Info().setValuesByName(pdaCwfpdaun.getParm_Unit_Info_Sub());                                                                              //Natural: MOVE BY NAME PARM-UNIT-INFO-SUB TO PARM-UNIT-INFO
        pdaIcwa2613.getIcwa2613_Tbl_Scrty_Level_Ind().setValue("A");                                                                                                      //Natural: MOVE 'A' TO ICWA2613.TBL-SCRTY-LEVEL-IND
        pdaIcwa2613.getIcwa2613_Tbl_Actve_Ind().setValue("A");                                                                                                            //Natural: MOVE 'A' TO ICWA2613.TBL-ACTVE-IND
        pdaIcwa2613.getIcwa2613_Tbl_Table_Nme().setValue("ICW-IRIS-HEADER-REC");                                                                                          //Natural: MOVE 'ICW-IRIS-HEADER-REC' TO ICWA2613.TBL-TABLE-NME
        pdaIcwa2613.getIcwa2613_File_Date().setValue(pnd_Parm_Date);                                                                                                      //Natural: MOVE #PARM-DATE TO ICWA2613.FILE-DATE
        pdaIcwa2613.getIcwa2613_Records_Loaded().setValue(pnd_Parm_Records);                                                                                              //Natural: MOVE #PARM-RECORDS TO ICWA2613.RECORDS-LOADED
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                               //Natural: MOVE 'GET' TO CDAOBJ.#FUNCTION
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
        sub_Call_Object();
        if (condition(Global.isEscape())) {return;}
        //*  RECORD FOUND
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals(" ")))                                                                                     //Natural: IF MSG-INFO-SUB.##RETURN-CODE = ' '
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("RECORD ALREADY EXISTS");                                                                                  //Natural: MOVE 'RECORD ALREADY EXISTS' TO MSG-INFO-SUB.##MSG
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-OBJECT
    }
    private void sub_Call_Object() throws Exception                                                                                                                       //Natural: CALL-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        DbsUtil.callnat(Icwn2617.class , getCurrentProcessState(), pdaIcwa2613.getIcwa2613(), pdaIcwa2613.getIcwa2613_Id(), pdaIcwa2611.getIcwa2611(),                    //Natural: CALLNAT 'ICWN2617' ICWA2613 ICWA2613-ID ICWA2611 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
    }

    //
}
