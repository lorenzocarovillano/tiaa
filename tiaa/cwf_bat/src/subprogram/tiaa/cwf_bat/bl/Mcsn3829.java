/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:26:25 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3829
************************************************************
**        * FILE NAME            : Mcsn3829.java
**        * CLASS NAME           : Mcsn3829
**        * INSTANCE NAME        : Mcsn3829
************************************************************
************************************************************************
* PROGRAM  : MCSN3829
* TITLE    : ACCESS FULFILLMENT RECORDS FROM THE CWF-ATI-FULFILLMENT
* FUNCTION : DISPLAY ATI-FULFILLMENT RECORDS ON 192 IN WPID ORDER
*          : FOR VOAD SYSTEM "NOT PROCESSED"
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3829 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;

    private DataAccessProgramView vw_cwf_Ati_Fulfill_Voa;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Rcrd_Type;

    private DbsGroup cwf_Ati_Fulfill_Voa_Ati_Rqst_Id;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Pin;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Mit_Log_Dt_Tme;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Prcss_Stts;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Wpid;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Entry_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Entry_Systm_Or_Unt;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Prcss_Applctn_Id;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Error_Txt;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Prge_Ind;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Entry_Racf_Id;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Entry_Unit_Cde;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Cmtask_Ind;
    private DbsField cwf_Ati_Fulfill_Voa_Ati_Stts_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Orig_Ssn;
    private DbsGroup cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_NbrMuGroup;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_All_Cntrct_Ind;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Adrs_On_File_Ind;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Location_Ind;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Ph_Comm_Mthd;

    private DbsGroup cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Nmbr;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Area_Cde;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Prefix;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Suffix;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Ph_Email_Adrs;
    private DbsGroup cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_AdrsMuGroup;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs_Zip_1_5;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs_Zip_6_9;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Ph_Cntry_Cde;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Comm_Mthd;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Name;

    private DbsGroup cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Nmbr;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Area_Cde;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Prefix;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Suffix;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Email_Adrs;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Attn;
    private DbsGroup cwf_Ati_Fulfill_Voa_Voa_Third_Prty_AdrsMuGroup;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs_Zip_1_5;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs_Zip_6_9;
    private DbsField cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Cntry_Cde;
    private DbsField pnd_Ati_Completed_Key;

    private DbsGroup pnd_Ati_Completed_Key__R_Field_1;
    private DbsField pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type_Cntrl;
    private DbsField pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme;
    private DbsField pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind;
    private DbsField pnd_Grand_Total_Open_Atis;
    private DbsField pnd_Grand_Total_Inprogress_Atis;
    private DbsField pnd_Grand_Total_Failed_Atis;
    private DbsField pnd_Grand_Total_Successful_Atis;
    private DbsField pnd_Grand_Total_Open_Inprgrs;
    private DbsField pnd_Grand_Total_Processed;
    private DbsField pnd_Prev_Wpid;
    private DbsField pnd_Wpid_Total;
    private DbsField pnd_Input_Dte_Tme;
    private DbsField pnd_Input_Dte_D;
    private DbsField pnd_Entry_Date;
    private DbsField pnd_Ati_Pin_Hold;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Process_Status_Literal;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Input_Data = parameters.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Ati_Fulfill_Voa = new DataAccessProgramView(new NameInfo("vw_cwf_Ati_Fulfill_Voa", "CWF-ATI-FULFILL-VOA"), "CWF_ATI_FULFILL_VOA", "CWF_KDO_FULFILL");
        cwf_Ati_Fulfill_Voa_Ati_Rcrd_Type = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Rcrd_Type", "ATI-RCRD-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_RCRD_TYPE");

        cwf_Ati_Fulfill_Voa_Ati_Rqst_Id = vw_cwf_Ati_Fulfill_Voa.getRecord().newGroupInGroup("CWF_ATI_FULFILL_VOA_ATI_RQST_ID", "ATI-RQST-ID");
        cwf_Ati_Fulfill_Voa_Ati_Pin = cwf_Ati_Fulfill_Voa_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Pin", "ATI-PIN", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "ATI_PIN");
        cwf_Ati_Fulfill_Voa_Ati_Mit_Log_Dt_Tme = cwf_Ati_Fulfill_Voa_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Mit_Log_Dt_Tme", "ATI-MIT-LOG-DT-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ATI_MIT_LOG_DT_TME");
        cwf_Ati_Fulfill_Voa_Ati_Prcss_Stts = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Prcss_Stts", "ATI-PRCSS-STTS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ATI_PRCSS_STTS");
        cwf_Ati_Fulfill_Voa_Ati_Wpid = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Wpid", "ATI-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "ATI_WPID");
        cwf_Ati_Fulfill_Voa_Ati_Entry_Dte_Tme = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Entry_Dte_Tme", "ATI-ENTRY-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ATI_ENTRY_DTE_TME");
        cwf_Ati_Fulfill_Voa_Ati_Entry_Systm_Or_Unt = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Entry_Systm_Or_Unt", 
            "ATI-ENTRY-SYSTM-OR-UNT", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_SYSTM_OR_UNT");
        cwf_Ati_Fulfill_Voa_Ati_Prcss_Applctn_Id = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Prcss_Applctn_Id", "ATI-PRCSS-APPLCTN-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_PRCSS_APPLCTN_ID");
        cwf_Ati_Fulfill_Voa_Ati_Error_Txt = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Error_Txt", "ATI-ERROR-TXT", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "ATI_ERROR_TXT");
        cwf_Ati_Fulfill_Voa_Ati_Prge_Ind = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Prge_Ind", "ATI-PRGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRGE_IND");
        cwf_Ati_Fulfill_Voa_Ati_Entry_Racf_Id = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Entry_Racf_Id", "ATI-ENTRY-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_RACF_ID");
        cwf_Ati_Fulfill_Voa_Ati_Entry_Unit_Cde = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Entry_Unit_Cde", "ATI-ENTRY-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_UNIT_CDE");
        cwf_Ati_Fulfill_Voa_Ati_Cmtask_Ind = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Cmtask_Ind", "ATI-CMTASK-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ATI_CMTASK_IND");
        cwf_Ati_Fulfill_Voa_Ati_Stts_Dte_Tme = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Ati_Stts_Dte_Tme", "ATI-STTS-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ATI_STTS_DTE_TME");
        cwf_Ati_Fulfill_Voa_Voa_Orig_Ssn = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Orig_Ssn", "VOA-ORIG-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "VOA_ORIG_SSN");
        cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_NbrMuGroup = vw_cwf_Ati_Fulfill_Voa.getRecord().newGroupInGroup("CWF_ATI_FULFILL_VOA_VOA_CNTRCT_ACCNT_NBRMuGroup", 
            "VOA_CNTRCT_ACCNT_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_KDO_FULFILL_VOA_CNTRCT_ACCNT_NBR");
        cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr = cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_NbrMuGroup.newFieldArrayInGroup("cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr", 
            "VOA-CNTRCT-ACCNT-NBR", FieldType.STRING, 10, new DbsArrayController(1, 40), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "VOA_CNTRCT_ACCNT_NBR");
        cwf_Ati_Fulfill_Voa_Voa_All_Cntrct_Ind = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_All_Cntrct_Ind", "VOA-ALL-CNTRCT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "VOA_ALL_CNTRCT_IND");
        cwf_Ati_Fulfill_Voa_Voa_Adrs_On_File_Ind = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Adrs_On_File_Ind", "VOA-ADRS-ON-FILE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "VOA_ADRS_ON_FILE_IND");
        cwf_Ati_Fulfill_Voa_Voa_Location_Ind = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Location_Ind", "VOA-LOCATION-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "VOA_LOCATION_IND");
        cwf_Ati_Fulfill_Voa_Voa_Ph_Comm_Mthd = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Ph_Comm_Mthd", "VOA-PH-COMM-MTHD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "VOA_PH_COMM_MTHD");

        cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Nmbr = vw_cwf_Ati_Fulfill_Voa.getRecord().newGroupInGroup("CWF_ATI_FULFILL_VOA_VOA_PH_FAX_NMBR", "VOA-PH-FAX-NMBR");
        cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Area_Cde = cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Nmbr.newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Area_Cde", "VOA-PH-FAX-AREA-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "VOA_PH_FAX_AREA_CDE");
        cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Prefix = cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Nmbr.newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Prefix", "VOA-PH-FAX-PREFIX", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "VOA_PH_FAX_PREFIX");
        cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Suffix = cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Nmbr.newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Suffix", "VOA-PH-FAX-SUFFIX", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "VOA_PH_FAX_SUFFIX");
        cwf_Ati_Fulfill_Voa_Voa_Ph_Email_Adrs = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Ph_Email_Adrs", "VOA-PH-EMAIL-ADRS", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "VOA_PH_EMAIL_ADRS");
        cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_AdrsMuGroup = vw_cwf_Ati_Fulfill_Voa.getRecord().newGroupInGroup("CWF_ATI_FULFILL_VOA_VOA_PH_ALT_ADRSMuGroup", 
            "VOA_PH_ALT_ADRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_KDO_FULFILL_VOA_PH_ALT_ADRS");
        cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs = cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_AdrsMuGroup.newFieldArrayInGroup("cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs", "VOA-PH-ALT-ADRS", 
            FieldType.STRING, 35, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "VOA_PH_ALT_ADRS");
        cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs_Zip_1_5 = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs_Zip_1_5", 
            "VOA-PH-ALT-ADRS-ZIP-1-5", FieldType.STRING, 5, RepeatingFieldStrategy.None, "VOA_PH_ALT_ADRS_ZIP_1_5");
        cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs_Zip_6_9 = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs_Zip_6_9", 
            "VOA-PH-ALT-ADRS-ZIP-6-9", FieldType.STRING, 4, RepeatingFieldStrategy.None, "VOA_PH_ALT_ADRS_ZIP_6_9");
        cwf_Ati_Fulfill_Voa_Voa_Ph_Cntry_Cde = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Ph_Cntry_Cde", "VOA-PH-CNTRY-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "VOA_PH_CNTRY_CDE");
        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Comm_Mthd = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Comm_Mthd", 
            "VOA-THIRD-PRTY-COMM-MTHD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "VOA_THIRD_PRTY_COMM_MTHD");
        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Name = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Name", "VOA-THIRD-PRTY-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "VOA_THIRD_PRTY_NAME");

        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Nmbr = vw_cwf_Ati_Fulfill_Voa.getRecord().newGroupInGroup("CWF_ATI_FULFILL_VOA_VOA_THIRD_PRTY_FAX_NMBR", 
            "VOA-THIRD-PRTY-FAX-NMBR");
        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Area_Cde = cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Nmbr.newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Area_Cde", 
            "VOA-THIRD-PRTY-FAX-AREA-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "VOA_THIRD_PRTY_FAX_AREA_CDE");
        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Prefix = cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Nmbr.newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Prefix", 
            "VOA-THIRD-PRTY-FAX-PREFIX", FieldType.STRING, 3, RepeatingFieldStrategy.None, "VOA_THIRD_PRTY_FAX_PREFIX");
        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Suffix = cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Nmbr.newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Suffix", 
            "VOA-THIRD-PRTY-FAX-SUFFIX", FieldType.STRING, 4, RepeatingFieldStrategy.None, "VOA_THIRD_PRTY_FAX_SUFFIX");
        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Email_Adrs = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Email_Adrs", 
            "VOA-THIRD-PRTY-EMAIL-ADRS", FieldType.STRING, 50, RepeatingFieldStrategy.None, "VOA_THIRD_PRTY_EMAIL_ADRS");
        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Attn = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Attn", "VOA-THIRD-PRTY-ATTN", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "VOA_THIRD_PRTY_ATTN");
        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_AdrsMuGroup = vw_cwf_Ati_Fulfill_Voa.getRecord().newGroupInGroup("CWF_ATI_FULFILL_VOA_VOA_THIRD_PRTY_ADRSMuGroup", 
            "VOA_THIRD_PRTY_ADRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_KDO_FULFILL_VOA_THIRD_PRTY_ADRS");
        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs = cwf_Ati_Fulfill_Voa_Voa_Third_Prty_AdrsMuGroup.newFieldArrayInGroup("cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs", 
            "VOA-THIRD-PRTY-ADRS", FieldType.STRING, 35, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "VOA_THIRD_PRTY_ADRS");
        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs_Zip_1_5 = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs_Zip_1_5", 
            "VOA-THIRD-PRTY-ADRS-ZIP-1-5", FieldType.STRING, 5, RepeatingFieldStrategy.None, "VOA_THIRD_PRTY_ADRS_ZIP_1_5");
        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs_Zip_6_9 = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs_Zip_6_9", 
            "VOA-THIRD-PRTY-ADRS-ZIP-6-9", FieldType.STRING, 4, RepeatingFieldStrategy.None, "VOA_THIRD_PRTY_ADRS_ZIP_6_9");
        cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Cntry_Cde = vw_cwf_Ati_Fulfill_Voa.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Cntry_Cde", 
            "VOA-THIRD-PRTY-CNTRY-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "VOA_THIRD_PRTY_CNTRY_CDE");
        registerRecord(vw_cwf_Ati_Fulfill_Voa);

        pnd_Ati_Completed_Key = localVariables.newFieldInRecord("pnd_Ati_Completed_Key", "#ATI-COMPLETED-KEY", FieldType.STRING, 9);

        pnd_Ati_Completed_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Ati_Completed_Key__R_Field_1", "REDEFINE", pnd_Ati_Completed_Key);
        pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type_Cntrl = pnd_Ati_Completed_Key__R_Field_1.newFieldInGroup("pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type_Cntrl", 
            "#ATI-RCRD-TYPE-CNTRL", FieldType.STRING, 1);
        pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme = pnd_Ati_Completed_Key__R_Field_1.newFieldInGroup("pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme", 
            "#ATI-ENTRY-DTE-TME", FieldType.TIME);
        pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind = pnd_Ati_Completed_Key__R_Field_1.newFieldInGroup("pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind", "#ATI-PRGE-IND", 
            FieldType.STRING, 1);
        pnd_Grand_Total_Open_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Open_Atis", "#GRAND-TOTAL-OPEN-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Inprogress_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Inprogress_Atis", "#GRAND-TOTAL-INPROGRESS-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Grand_Total_Failed_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Failed_Atis", "#GRAND-TOTAL-FAILED-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Successful_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Successful_Atis", "#GRAND-TOTAL-SUCCESSFUL-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Grand_Total_Open_Inprgrs = localVariables.newFieldInRecord("pnd_Grand_Total_Open_Inprgrs", "#GRAND-TOTAL-OPEN-INPRGRS", FieldType.NUMERIC, 
            5);
        pnd_Grand_Total_Processed = localVariables.newFieldInRecord("pnd_Grand_Total_Processed", "#GRAND-TOTAL-PROCESSED", FieldType.NUMERIC, 5);
        pnd_Prev_Wpid = localVariables.newFieldInRecord("pnd_Prev_Wpid", "#PREV-WPID", FieldType.STRING, 6);
        pnd_Wpid_Total = localVariables.newFieldInRecord("pnd_Wpid_Total", "#WPID-TOTAL", FieldType.NUMERIC, 8);
        pnd_Input_Dte_Tme = localVariables.newFieldInRecord("pnd_Input_Dte_Tme", "#INPUT-DTE-TME", FieldType.TIME);
        pnd_Input_Dte_D = localVariables.newFieldInRecord("pnd_Input_Dte_D", "#INPUT-DTE-D", FieldType.DATE);
        pnd_Entry_Date = localVariables.newFieldInRecord("pnd_Entry_Date", "#ENTRY-DATE", FieldType.DATE);
        pnd_Ati_Pin_Hold = localVariables.newFieldInRecord("pnd_Ati_Pin_Hold", "#ATI-PIN-HOLD", FieldType.STRING, 12);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Process_Status_Literal = localVariables.newFieldInRecord("pnd_Process_Status_Literal", "#PROCESS-STATUS-LITERAL", FieldType.STRING, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Ati_Fulfill_Voa.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Mcsn3829() throws Exception
    {
        super("Mcsn3829");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("MCSN3829", onError);
        getReports().atTopOfPage(atTopEventRpt12, 12);
        setupReports();
        //*  ----------------------------------------------------------------------
        //*   MAIN PROGRAM
        //*  ----------------------------------------------------------------------
        //*  ---                                                                                                                                                          //Natural: FORMAT ( 12 ) PS = 63 LS = 133 ZP = OFF
        pnd_Grand_Total_Open_Atis.reset();                                                                                                                                //Natural: RESET #GRAND-TOTAL-OPEN-ATIS #GRAND-TOTAL-INPROGRESS-ATIS #GRAND-TOTAL-FAILED-ATIS #GRAND-TOTAL-SUCCESSFUL-ATIS #GRAND-TOTAL-OPEN-INPRGRS #GRAND-TOTAL-PROCESSED
        pnd_Grand_Total_Inprogress_Atis.reset();
        pnd_Grand_Total_Failed_Atis.reset();
        pnd_Grand_Total_Successful_Atis.reset();
        pnd_Grand_Total_Open_Inprgrs.reset();
        pnd_Grand_Total_Processed.reset();
        if (condition(pnd_Input_Data_Pnd_Input_Date.notEquals(" ")))                                                                                                      //Natural: IF #INPUT-DATE NE ' '
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Input_Dte_Tme.setValue(pnd_Input_Dte_D);                                                                                                                  //Natural: MOVE #INPUT-DTE-D TO #INPUT-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(12, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***");                                    //Natural: WRITE ( 12 ) /// '*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***'
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
        //* *** INITIALIZE THE ATI-CONTROL-KEY SUPER-DESCRIPTOR FIELD
        pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type_Cntrl.setValue("3");                                                                                                      //Natural: ASSIGN #ATI-RCRD-TYPE-CNTRL := '3'
        pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme.reset();                                                                                                              //Natural: RESET #ATI-ENTRY-DTE-TME
        //* *****************************************************************
        //* ** READ CWF-ATI-FULFILL AND PROCESS OPEN AND IN-PROGRESS REQUESTS
        //* *****************************************************************
        getReports().newPage(new ReportSpecification(12));                                                                                                                //Natural: NEWPAGE ( 12 )
        if (condition(Global.isEscape())){return;}
        vw_cwf_Ati_Fulfill_Voa.startDatabaseRead                                                                                                                          //Natural: READ CWF-ATI-FULFILL-VOA BY ATI-COMPLETED-KEY STARTING FROM #ATI-COMPLETED-KEY
        (
        "READ_ATI",
        new Wc[] { new Wc("ATI_COMPLETED_KEY", ">=", pnd_Ati_Completed_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("ATI_COMPLETED_KEY", "ASC") }
        );
        READ_ATI:
        while (condition(vw_cwf_Ati_Fulfill_Voa.readNextRow("READ_ATI")))
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Entry_Date.setValue(cwf_Ati_Fulfill_Voa_Ati_Stts_Dte_Tme);                                                                                                //Natural: MOVE CWF-ATI-FULFILL-VOA.ATI-STTS-DTE-TME TO #ENTRY-DATE
            if (condition(!(pnd_Entry_Date.equals(pnd_Input_Dte_D) && cwf_Ati_Fulfill_Voa_Ati_Prcss_Applctn_Id.equals("VOAD"))))                                          //Natural: ACCEPT IF #ENTRY-DATE = #INPUT-DTE-D AND CWF-ATI-FULFILL-VOA.ATI-PRCSS-APPLCTN-ID = 'VOAD'
            {
                continue;
            }
            getSort().writeSortInData(cwf_Ati_Fulfill_Voa_Ati_Wpid, cwf_Ati_Fulfill_Voa_Ati_Pin, cwf_Ati_Fulfill_Voa_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Voa_Ati_Rcrd_Type,  //Natural: END-ALL
                cwf_Ati_Fulfill_Voa_Ati_Prcss_Stts, cwf_Ati_Fulfill_Voa_Ati_Error_Txt, cwf_Ati_Fulfill_Voa_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Voa_Ati_Entry_Systm_Or_Unt, 
                cwf_Ati_Fulfill_Voa_Ati_Entry_Racf_Id, cwf_Ati_Fulfill_Voa_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Voa_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Voa_Voa_Orig_Ssn, 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(1), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(2), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(3), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(4), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(5), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(6), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(7), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(8), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(9), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(10), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(11), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(12), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(13), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(14), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(15), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(16), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(17), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(18), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(19), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(20), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(21), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(22), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(23), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(24), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(25), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(26), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(27), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(28), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(29), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(30), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(31), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(32), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(33), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(34), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(35), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(36), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(37), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(38), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(39), 
                cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(40), cwf_Ati_Fulfill_Voa_Voa_All_Cntrct_Ind, cwf_Ati_Fulfill_Voa_Voa_Adrs_On_File_Ind, 
                cwf_Ati_Fulfill_Voa_Voa_Location_Ind, cwf_Ati_Fulfill_Voa_Voa_Ph_Comm_Mthd, cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Area_Cde, cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Prefix, 
                cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Suffix, cwf_Ati_Fulfill_Voa_Voa_Ph_Email_Adrs, cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(1), cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(2), 
                cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(3), cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(4), cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(5), 
                cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs_Zip_1_5, cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs_Zip_6_9, cwf_Ati_Fulfill_Voa_Voa_Ph_Cntry_Cde, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Comm_Mthd, 
                cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Name, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Area_Cde, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Prefix, 
                cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Suffix, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Email_Adrs, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Attn, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(1), 
                cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(2), cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(3), cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(4), 
                cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(5), cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs_Zip_1_5, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs_Zip_6_9, 
                cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Cntry_Cde);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  SORT PERFORMED TO WRITE THEM PIN/MIT-LOG-DATE-TIME/REC-TYPE ORDER
        getSort().sortData(cwf_Ati_Fulfill_Voa_Ati_Wpid, cwf_Ati_Fulfill_Voa_Ati_Pin, cwf_Ati_Fulfill_Voa_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Voa_Ati_Rcrd_Type,          //Natural: SORT RECORDS BY CWF-ATI-FULFILL-VOA.ATI-WPID CWF-ATI-FULFILL-VOA.ATI-PIN CWF-ATI-FULFILL-VOA.ATI-MIT-LOG-DT-TME CWF-ATI-FULFILL-VOA.ATI-RCRD-TYPE CWF-ATI-FULFILL-VOA.ATI-PRCSS-STTS USING CWF-ATI-FULFILL-VOA.ATI-ERROR-TXT CWF-ATI-FULFILL-VOA.ATI-ENTRY-DTE-TME CWF-ATI-FULFILL-VOA.ATI-ENTRY-SYSTM-OR-UNT CWF-ATI-FULFILL-VOA.ATI-ENTRY-RACF-ID CWF-ATI-FULFILL-VOA.ATI-ENTRY-UNIT-CDE CWF-ATI-FULFILL-VOA.ATI-STTS-DTE-TME CWF-ATI-FULFILL-VOA.VOA-ORIG-SSN CWF-ATI-FULFILL-VOA.VOA-CNTRCT-ACCNT-NBR ( 1:40 ) CWF-ATI-FULFILL-VOA.VOA-ALL-CNTRCT-IND CWF-ATI-FULFILL-VOA.VOA-ADRS-ON-FILE-IND CWF-ATI-FULFILL-VOA.VOA-LOCATION-IND CWF-ATI-FULFILL-VOA.VOA-PH-COMM-MTHD CWF-ATI-FULFILL-VOA.VOA-PH-FAX-AREA-CDE CWF-ATI-FULFILL-VOA.VOA-PH-FAX-PREFIX CWF-ATI-FULFILL-VOA.VOA-PH-FAX-SUFFIX CWF-ATI-FULFILL-VOA.VOA-PH-EMAIL-ADRS CWF-ATI-FULFILL-VOA.VOA-PH-ALT-ADRS ( 1:5 ) CWF-ATI-FULFILL-VOA.VOA-PH-ALT-ADRS-ZIP-1-5 CWF-ATI-FULFILL-VOA.VOA-PH-ALT-ADRS-ZIP-6-9 CWF-ATI-FULFILL-VOA.VOA-PH-CNTRY-CDE CWF-ATI-FULFILL-VOA.VOA-THIRD-PRTY-COMM-MTHD CWF-ATI-FULFILL-VOA.VOA-THIRD-PRTY-NAME CWF-ATI-FULFILL-VOA.VOA-THIRD-PRTY-FAX-AREA-CDE CWF-ATI-FULFILL-VOA.VOA-THIRD-PRTY-FAX-PREFIX CWF-ATI-FULFILL-VOA.VOA-THIRD-PRTY-FAX-SUFFIX CWF-ATI-FULFILL-VOA.VOA-THIRD-PRTY-EMAIL-ADRS CWF-ATI-FULFILL-VOA.VOA-THIRD-PRTY-ATTN CWF-ATI-FULFILL-VOA.VOA-THIRD-PRTY-ADRS ( 1:5 ) CWF-ATI-FULFILL-VOA.VOA-THIRD-PRTY-ADRS-ZIP-1-5 CWF-ATI-FULFILL-VOA.VOA-THIRD-PRTY-ADRS-ZIP-6-9 CWF-ATI-FULFILL-VOA.VOA-THIRD-PRTY-CNTRY-CDE
            cwf_Ati_Fulfill_Voa_Ati_Prcss_Stts);
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Ati_Fulfill_Voa_Ati_Wpid, cwf_Ati_Fulfill_Voa_Ati_Pin, cwf_Ati_Fulfill_Voa_Ati_Mit_Log_Dt_Tme, 
            cwf_Ati_Fulfill_Voa_Ati_Rcrd_Type, cwf_Ati_Fulfill_Voa_Ati_Prcss_Stts, cwf_Ati_Fulfill_Voa_Ati_Error_Txt, cwf_Ati_Fulfill_Voa_Ati_Entry_Dte_Tme, 
            cwf_Ati_Fulfill_Voa_Ati_Entry_Systm_Or_Unt, cwf_Ati_Fulfill_Voa_Ati_Entry_Racf_Id, cwf_Ati_Fulfill_Voa_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Voa_Ati_Stts_Dte_Tme, 
            cwf_Ati_Fulfill_Voa_Voa_Orig_Ssn, cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(1), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(2), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(3), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(4), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(5), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(6), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(7), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(8), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(9), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(10), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(11), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(12), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(13), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(14), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(15), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(16), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(17), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(18), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(19), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(20), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(21), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(22), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(23), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(24), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(25), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(26), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(27), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(28), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(29), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(30), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(31), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(32), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(33), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(34), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(35), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(36), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(37), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(38), 
            cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(39), cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(40), cwf_Ati_Fulfill_Voa_Voa_All_Cntrct_Ind, 
            cwf_Ati_Fulfill_Voa_Voa_Adrs_On_File_Ind, cwf_Ati_Fulfill_Voa_Voa_Location_Ind, cwf_Ati_Fulfill_Voa_Voa_Ph_Comm_Mthd, cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Area_Cde, 
            cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Prefix, cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Suffix, cwf_Ati_Fulfill_Voa_Voa_Ph_Email_Adrs, cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(1), 
            cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(2), cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(3), cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(4), 
            cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(5), cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs_Zip_1_5, cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs_Zip_6_9, cwf_Ati_Fulfill_Voa_Voa_Ph_Cntry_Cde, 
            cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Comm_Mthd, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Name, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Area_Cde, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Prefix, 
            cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Suffix, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Email_Adrs, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Attn, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(1), 
            cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(2), cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(3), cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(4), 
            cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(5), cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs_Zip_1_5, cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs_Zip_6_9, 
            cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Cntry_Cde)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  ---
            //*  ---                                                                                                                                                      //Natural: AT BREAK OF CWF-ATI-FULFILL-VOA.ATI-WPID
            pnd_Prev_Wpid.setValue(cwf_Ati_Fulfill_Voa_Ati_Wpid);                                                                                                         //Natural: ASSIGN #PREV-WPID := CWF-ATI-FULFILL-VOA.ATI-WPID
            pnd_Wpid_Total.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WPID-TOTAL
                                                                                                                                                                          //Natural: PERFORM ATI-WRITE-REC-1
            sub_Ati_Write_Rec_1();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //*  ----------------------------------------------------------------------
        //*  004T #ATI-PIN-HOLD
        //*  020T CWF-ATI-FULFILL-VOA.ATI-WPID
        //*  029T CWF-ATI-FULFILL-VOA.ATI-MIT-LOG-DT-TME (EM=MM/DD/YYYY' 'HH:II:SS)
        //*  050T CWF-ATI-FULFILL-VOA.ATI-ENTRY-DTE-TME  (EM=MM/DD/YYYY' 'HH:II:SS)
        //*  070T #PROCESS-STATUS-LITERAL
        //*  080T CWF-ATI-FULFILL-VOA.ATI-ENTRY-SYSTM-OR-UNT
        //*  090T CWF-ATI-FULFILL-VOA.ATI-ENTRY-RACF-ID
        //*  100T CWF-ATI-FULFILL-VOA.ATI-ENTRY-UNIT-CDE
        //*  110T CWF-ATI-FULFILL-VOA.ATI-STTS-DTE-TME   (EM=MM/DD/YYYY' 'HH:II:SS)
        //*  130T CWF-ATI-FULFILL-VOA.VOA-ADRS-ON-FILE-IND
        //*  ----------------------------------------------------------------------
        //*  ---------------------------
        //*   AT TOP OF PAGE DEFINITION
        //*  ---------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 12 )
        //*  ---------------------------------------------------
        //*   WRITE OUT ATI GRAND TOTALS ON LAST PAGE OF REPORT
        //*  ---------------------------------------------------
        getReports().newPage(new ReportSpecification(12));                                                                                                                //Natural: NEWPAGE ( 12 )
        if (condition(Global.isEscape())){return;}
        getReports().write(12, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,new         //Natural: WRITE ( 12 ) ////// 01T '-' ( 131 ) // 01T '-' ( 38 ) 45T 'VOA/VOD REQUESTS - PROCESSED REQUESTS TOTALS -' 95T '-' ( 35 ) // 01T '-' ( 131 ) // 45T 'TOTAL SUCCESSFUL VOA/VOD REQUESTS : ' #GRAND-TOTAL-SUCCESSFUL-ATIS // 45T 'TOTAL FAILED VOA/VOD REQUESTS     : ' #GRAND-TOTAL-FAILED-ATIS // 45T '----------------------------- ' // 45T 'TOTAL PROCESSED VOA/VOD REQUESTS  : ' #GRAND-TOTAL-PROCESSED
            TabSetting(1),"-",new RepeatItem(38),new TabSetting(45),"VOA/VOD REQUESTS - PROCESSED REQUESTS TOTALS -",new TabSetting(95),"-",new RepeatItem(35),NEWLINE,NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,new TabSetting(45),"TOTAL SUCCESSFUL VOA/VOD REQUESTS : ",pnd_Grand_Total_Successful_Atis,NEWLINE,NEWLINE,new 
            TabSetting(45),"TOTAL FAILED VOA/VOD REQUESTS     : ",pnd_Grand_Total_Failed_Atis,NEWLINE,NEWLINE,new TabSetting(45),"----------------------------- ",NEWLINE,NEWLINE,new 
            TabSetting(45),"TOTAL PROCESSED VOA/VOD REQUESTS  : ",pnd_Grand_Total_Processed);
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    //*  START ATI-WRITE-REC-1 PROCESSING
    private void sub_Ati_Write_Rec_1() throws Exception                                                                                                                   //Natural: ATI-WRITE-REC-1
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM SET-PROCESS-STATUS
        sub_Set_Process_Status();
        if (condition(Global.isEscape())) {return;}
        if (condition(getReports().getAstLinesLeft(12).less(17)))                                                                                                         //Natural: NEWPAGE ( 12 ) IF LESS THAN 17 LINES LEFT
        {
            getReports().newPage(12);
            if (condition(Global.isEscape())){return;}
        }
        //*  PIN-EXP
        //*   PIN-EXP>>
        pnd_Ati_Pin_Hold.setValue(cwf_Ati_Fulfill_Voa_Ati_Pin);                                                                                                           //Natural: MOVE CWF-ATI-FULFILL-VOA.ATI-PIN TO #ATI-PIN-HOLD
        getReports().write(12, ReportOption.NOTITLE,new TabSetting(1),pnd_Ati_Pin_Hold,new TabSetting(15),cwf_Ati_Fulfill_Voa_Ati_Wpid,new TabSetting(24),cwf_Ati_Fulfill_Voa_Ati_Mit_Log_Dt_Tme,  //Natural: WRITE ( 12 ) 001T #ATI-PIN-HOLD 015T CWF-ATI-FULFILL-VOA.ATI-WPID 024T CWF-ATI-FULFILL-VOA.ATI-MIT-LOG-DT-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 045T CWF-ATI-FULFILL-VOA.ATI-ENTRY-DTE-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 065T #PROCESS-STATUS-LITERAL 075T CWF-ATI-FULFILL-VOA.ATI-ENTRY-SYSTM-OR-UNT 085T CWF-ATI-FULFILL-VOA.ATI-ENTRY-RACF-ID 095T CWF-ATI-FULFILL-VOA.ATI-ENTRY-UNIT-CDE 105T CWF-ATI-FULFILL-VOA.ATI-STTS-DTE-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 126T CWF-ATI-FULFILL-VOA.VOA-ADRS-ON-FILE-IND 130T CWF-ATI-FULFILL-VOA.VOA-LOCATION-IND
            new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(45),cwf_Ati_Fulfill_Voa_Ati_Entry_Dte_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new 
            TabSetting(65),pnd_Process_Status_Literal,new TabSetting(75),cwf_Ati_Fulfill_Voa_Ati_Entry_Systm_Or_Unt,new TabSetting(85),cwf_Ati_Fulfill_Voa_Ati_Entry_Racf_Id,new 
            TabSetting(95),cwf_Ati_Fulfill_Voa_Ati_Entry_Unit_Cde,new TabSetting(105),cwf_Ati_Fulfill_Voa_Ati_Stts_Dte_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new 
            TabSetting(126),cwf_Ati_Fulfill_Voa_Voa_Adrs_On_File_Ind,new TabSetting(130),cwf_Ati_Fulfill_Voa_Voa_Location_Ind);
        if (Global.isEscape()) return;
        //*  132T CWF-ATI-FULFILL-VOA.VOA-LOCATION-IND
        if (condition(cwf_Ati_Fulfill_Voa_Ati_Error_Txt.notEquals(" ")))                                                                                                  //Natural: IF CWF-ATI-FULFILL-VOA.ATI-ERROR-TXT NE ' '
        {
            getReports().write(12, ReportOption.NOTITLE,new TabSetting(16),"ERROR MESSAGE = :",new TabSetting(34),cwf_Ati_Fulfill_Voa_Ati_Error_Txt);                     //Natural: WRITE ( 12 ) 16T 'ERROR MESSAGE = :' 34T CWF-ATI-FULFILL-VOA.ATI-ERROR-TXT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(12, ReportOption.NOTITLE,NEWLINE,new TabSetting(15),"POLICY HOLDER                      ",new ColumnSpacing(1),"THIRD PARTY                        ",new  //Natural: WRITE ( 12 ) / 15T 'POLICY HOLDER                      ' 01X 'THIRD PARTY                        ' 01X 'CONTRACT ACCOUNT NUMBERS                     ' / 15T '-----------------------------------' 01X '-----------------------------------' 01X '---------------------------------------------' / 01T 'SSN/3rd NME:' 15T VOA-ORIG-SSN ( EM = 999-99-9999 ) 51T VOA-THIRD-PRTY-NAME 88T VOA-CNTRCT-ACCNT-NBR ( 01 ) 01X VOA-CNTRCT-ACCNT-NBR ( 11 ) 01X VOA-CNTRCT-ACCNT-NBR ( 21 ) 01X VOA-CNTRCT-ACCNT-NBR ( 31 ) / 01T 'COMM MTHD  :' 15T VOA-PH-COMM-MTHD 51T VOA-THIRD-PRTY-COMM-MTHD 88T VOA-CNTRCT-ACCNT-NBR ( 02 ) 01X VOA-CNTRCT-ACCNT-NBR ( 12 ) 01X VOA-CNTRCT-ACCNT-NBR ( 22 ) 01X VOA-CNTRCT-ACCNT-NBR ( 32 ) / 01T 'FAX NBR    :' 15T VOA-PH-FAX-AREA-CDE 01X VOA-PH-FAX-PREFIX 01X VOA-PH-FAX-SUFFIX 51T VOA-THIRD-PRTY-FAX-AREA-CDE 01X VOA-THIRD-PRTY-FAX-PREFIX 01X VOA-THIRD-PRTY-FAX-SUFFIX 88T VOA-CNTRCT-ACCNT-NBR ( 03 ) 01X VOA-CNTRCT-ACCNT-NBR ( 12 ) 01X VOA-CNTRCT-ACCNT-NBR ( 23 ) 01X VOA-CNTRCT-ACCNT-NBR ( 33 ) / 01T 'EMAIL ADDR :' 15T VOA-PH-EMAIL-ADRS ( AL = 35 ) 51T VOA-THIRD-PRTY-EMAIL-ADRS ( AL = 35 ) 88T VOA-CNTRCT-ACCNT-NBR ( 04 ) 01X VOA-CNTRCT-ACCNT-NBR ( 12 ) 01X VOA-CNTRCT-ACCNT-NBR ( 24 ) 01X VOA-CNTRCT-ACCNT-NBR ( 34 ) / 01T 'ADDRESS 1  :' 15T VOA-PH-ALT-ADRS ( 1 ) 51T VOA-THIRD-PRTY-ADRS ( 1 ) 88T VOA-CNTRCT-ACCNT-NBR ( 05 ) 01X VOA-CNTRCT-ACCNT-NBR ( 12 ) 01X VOA-CNTRCT-ACCNT-NBR ( 25 ) 01X VOA-CNTRCT-ACCNT-NBR ( 35 ) / 01T 'ADDRESS 2  :' 15T VOA-PH-ALT-ADRS ( 2 ) 51T VOA-THIRD-PRTY-ADRS ( 2 ) 88T VOA-CNTRCT-ACCNT-NBR ( 06 ) 01X VOA-CNTRCT-ACCNT-NBR ( 16 ) 01X VOA-CNTRCT-ACCNT-NBR ( 26 ) 01X VOA-CNTRCT-ACCNT-NBR ( 36 ) / 01T 'ADDRESS 3  :' 15T VOA-PH-ALT-ADRS ( 3 ) 51T VOA-THIRD-PRTY-ADRS ( 3 ) 88T VOA-CNTRCT-ACCNT-NBR ( 07 ) 01X VOA-CNTRCT-ACCNT-NBR ( 17 ) 01X VOA-CNTRCT-ACCNT-NBR ( 27 ) 01X VOA-CNTRCT-ACCNT-NBR ( 37 ) / 01T 'ADDRESS 4  :' 15T VOA-PH-ALT-ADRS ( 4 ) 51T VOA-THIRD-PRTY-ADRS ( 4 ) 88T VOA-CNTRCT-ACCNT-NBR ( 08 ) 01X VOA-CNTRCT-ACCNT-NBR ( 18 ) 01X VOA-CNTRCT-ACCNT-NBR ( 28 ) 01X VOA-CNTRCT-ACCNT-NBR ( 38 ) / 01T 'ADDRESS 5  :' 15T VOA-PH-ALT-ADRS ( 5 ) 51T VOA-THIRD-PRTY-ADRS ( 5 ) 88T VOA-CNTRCT-ACCNT-NBR ( 09 ) 01X VOA-CNTRCT-ACCNT-NBR ( 19 ) 01X VOA-CNTRCT-ACCNT-NBR ( 29 ) 01X VOA-CNTRCT-ACCNT-NBR ( 39 ) / 01T 'ZIP CODE   :' 15T VOA-PH-ALT-ADRS-ZIP-1-5 01X VOA-PH-ALT-ADRS-ZIP-6-9 51T VOA-THIRD-PRTY-ADRS-ZIP-1-5 01X VOA-THIRD-PRTY-ADRS-ZIP-6-9 88T VOA-CNTRCT-ACCNT-NBR ( 10 ) 01X VOA-CNTRCT-ACCNT-NBR ( 20 ) 01X VOA-CNTRCT-ACCNT-NBR ( 30 ) 01X VOA-CNTRCT-ACCNT-NBR ( 40 ) / 01T 'ATTN TO    :' 51T VOA-THIRD-PRTY-ATTN / 01T 'CNTRY CODE :' 15T VOA-PH-CNTRY-CDE 51T VOA-THIRD-PRTY-CNTRY-CDE //
            ColumnSpacing(1),"CONTRACT ACCOUNT NUMBERS                     ",NEWLINE,new TabSetting(15),"-----------------------------------",new ColumnSpacing(1),"-----------------------------------",new 
            ColumnSpacing(1),"---------------------------------------------",NEWLINE,new TabSetting(1),"SSN/3rd NME:",new TabSetting(15),cwf_Ati_Fulfill_Voa_Voa_Orig_Ssn, 
            new ReportEditMask ("999-99-9999"),new TabSetting(51),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Name,new TabSetting(88),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(1),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(11),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(21),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(31),NEWLINE,new TabSetting(1),"COMM MTHD  :",new TabSetting(15),cwf_Ati_Fulfill_Voa_Voa_Ph_Comm_Mthd,new 
            TabSetting(51),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Comm_Mthd,new TabSetting(88),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(2),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(12),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(22),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(32),NEWLINE,new 
            TabSetting(1),"FAX NBR    :",new TabSetting(15),cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Area_Cde,new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Prefix,new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Ph_Fax_Suffix,new TabSetting(51),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Area_Cde,new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Prefix,new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Fax_Suffix,new TabSetting(88),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(3),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(12),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(23),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(33),NEWLINE,new 
            TabSetting(1),"EMAIL ADDR :",new TabSetting(15),cwf_Ati_Fulfill_Voa_Voa_Ph_Email_Adrs, new AlphanumericLength (35),new TabSetting(51),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Email_Adrs, 
            new AlphanumericLength (35),new TabSetting(88),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(4),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(12),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(24),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(34),NEWLINE,new 
            TabSetting(1),"ADDRESS 1  :",new TabSetting(15),cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(1),new TabSetting(51),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(1),new 
            TabSetting(88),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(5),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(12),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(25),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(35),NEWLINE,new 
            TabSetting(1),"ADDRESS 2  :",new TabSetting(15),cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(2),new TabSetting(51),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(2),new 
            TabSetting(88),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(6),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(16),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(26),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(36),NEWLINE,new 
            TabSetting(1),"ADDRESS 3  :",new TabSetting(15),cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(3),new TabSetting(51),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(3),new 
            TabSetting(88),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(7),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(17),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(27),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(37),NEWLINE,new 
            TabSetting(1),"ADDRESS 4  :",new TabSetting(15),cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(4),new TabSetting(51),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(4),new 
            TabSetting(88),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(8),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(18),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(28),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(38),NEWLINE,new 
            TabSetting(1),"ADDRESS 5  :",new TabSetting(15),cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs.getValue(5),new TabSetting(51),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs.getValue(5),new 
            TabSetting(88),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(9),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(19),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(29),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(39),NEWLINE,new 
            TabSetting(1),"ZIP CODE   :",new TabSetting(15),cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs_Zip_1_5,new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Ph_Alt_Adrs_Zip_6_9,new 
            TabSetting(51),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs_Zip_1_5,new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Adrs_Zip_6_9,new TabSetting(88),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(10),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(20),new ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(30),new 
            ColumnSpacing(1),cwf_Ati_Fulfill_Voa_Voa_Cntrct_Accnt_Nbr.getValue(40),NEWLINE,new TabSetting(1),"ATTN TO    :",new TabSetting(51),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Attn,NEWLINE,new 
            TabSetting(1),"CNTRY CODE :",new TabSetting(15),cwf_Ati_Fulfill_Voa_Voa_Ph_Cntry_Cde,new TabSetting(51),cwf_Ati_Fulfill_Voa_Voa_Third_Prty_Cntry_Cde,
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  NEWPAGE (12) IF LESS THAN 16 LINES LEFT
    }
    private void sub_Set_Process_Status() throws Exception                                                                                                                //Natural: SET-PROCESS-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        short decideConditionsMet226 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CWF-ATI-FULFILL-VOA.ATI-PRCSS-STTS;//Natural: VALUE 'O'
        if (condition((cwf_Ati_Fulfill_Voa_Ati_Prcss_Stts.equals("O"))))
        {
            decideConditionsMet226++;
            pnd_Process_Status_Literal.setValue(" OPEN ");                                                                                                                //Natural: MOVE ' OPEN ' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Open_Atis.nadd(1);                                                                                                                            //Natural: ADD 1 TO #GRAND-TOTAL-OPEN-ATIS
            pnd_Grand_Total_Open_Inprgrs.nadd(1);                                                                                                                         //Natural: ADD 1 TO #GRAND-TOTAL-OPEN-INPRGRS
            pnd_Input_Data_Pnd_W_Ati_Open.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                                     //Natural: ADD 1 TO #W-ATI-OPEN ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((cwf_Ati_Fulfill_Voa_Ati_Prcss_Stts.equals("I"))))
        {
            decideConditionsMet226++;
            pnd_Process_Status_Literal.setValue("INPROGS");                                                                                                               //Natural: MOVE 'INPROGS' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Inprogress_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-INPROGRESS-ATIS
            pnd_Grand_Total_Open_Inprgrs.nadd(1);                                                                                                                         //Natural: ADD 1 TO #GRAND-TOTAL-OPEN-INPRGRS
            pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                              //Natural: ADD 1 TO #W-ATI-IN-PROGRESS ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE ' '
        else if (condition((cwf_Ati_Fulfill_Voa_Ati_Prcss_Stts.equals(" "))))
        {
            decideConditionsMet226++;
            if (condition(cwf_Ati_Fulfill_Voa_Ati_Error_Txt.equals(" ")))                                                                                                 //Natural: IF CWF-ATI-FULFILL-VOA.ATI-ERROR-TXT = ' '
            {
                pnd_Process_Status_Literal.setValue("SUCCESS");                                                                                                           //Natural: MOVE 'SUCCESS' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Successful_Atis.nadd(1);                                                                                                                  //Natural: ADD 1 TO #GRAND-TOTAL-SUCCESSFUL-ATIS
                pnd_Input_Data_Pnd_W_Ati_Successful.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                           //Natural: ADD 1 TO #W-ATI-SUCCESSFUL ( #W-INX )
                pnd_Grand_Total_Processed.nadd(1);                                                                                                                        //Natural: ADD 1 TO #GRAND-TOTAL-PROCESSED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Process_Status_Literal.setValue("FAILED");                                                                                                            //Natural: MOVE 'FAILED' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Failed_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-FAILED-ATIS
                pnd_Input_Data_Pnd_W_Ati_Manual.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                               //Natural: ADD 1 TO #W-ATI-MANUAL ( #W-INX )
                pnd_Grand_Total_Processed.nadd(1);                                                                                                                        //Natural: ADD 1 TO #GRAND-TOTAL-PROCESSED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Process_Status_Literal.setValue("INCORCT");                                                                                                               //Natural: MOVE 'INCORCT' TO #PROCESS-STATUS-LITERAL
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt12 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(12, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(50),"AUTOMATED TRANSACTION SYSTEM",new               //Natural: WRITE ( 12 ) NOTITLE 001T *PROGRAM 050T 'AUTOMATED TRANSACTION SYSTEM' 101T *DATU 110T *TIME ( AL = 005 ) 121T 'PAGE:' 127T *PAGE-NUMBER ( 12 ) ( AD = L ) / 043T 'ATI DETAILED REPORT FOR VOA/VOD REQUESTS' / 039T 'SUCCESSFUL/FAILED VOA/VOD REQUESTS FOR :' 081T #INPUT-DTE-D ( EM = MM/DD/YY ) // 001T '-' ( 131 ) / 027T 'MASTER INDEX' 049T 'ATI ENTRY' 065T 'PROCESS' 075T 'ENTRY' 085T 'ENTRY' 095T 'ENTRY' 109T 'ATI STATUS' 125T 'ADR' 129T 'LOC' / 002T 'PIN / NPIR' 016T 'WPID' 025T 'LOG DATE & TIME' 048T 'DATE & TIME' 065T 'STATUS' 075T 'SYSTEM' 085T 'RACF-ID' 095T 'UNIT' 109T 'DATE & TIME' 125T 'FIL' 129T 'IND' / 001T '-' ( 131 ) /
                        TabSetting(101),Global.getDATU(),new TabSetting(110),Global.getTIME(), new AlphanumericLength (5),new TabSetting(121),"PAGE:",new 
                        TabSetting(127),getReports().getPageNumberDbs(12), new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(43),"ATI DETAILED REPORT FOR VOA/VOD REQUESTS",NEWLINE,new 
                        TabSetting(39),"SUCCESSFUL/FAILED VOA/VOD REQUESTS FOR :",new TabSetting(81),pnd_Input_Dte_D, new ReportEditMask ("MM/DD/YY"),NEWLINE,NEWLINE,new 
                        TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(27),"MASTER INDEX",new TabSetting(49),"ATI ENTRY",new TabSetting(65),"PROCESS",new 
                        TabSetting(75),"ENTRY",new TabSetting(85),"ENTRY",new TabSetting(95),"ENTRY",new TabSetting(109),"ATI STATUS",new TabSetting(125),"ADR",new 
                        TabSetting(129),"LOC",NEWLINE,new TabSetting(2),"PIN / NPIR",new TabSetting(16),"WPID",new TabSetting(25),"LOG DATE & TIME",new 
                        TabSetting(48),"DATE & TIME",new TabSetting(65),"STATUS",new TabSetting(75),"SYSTEM",new TabSetting(85),"RACF-ID",new TabSetting(95),"UNIT",new 
                        TabSetting(109),"DATE & TIME",new TabSetting(125),"FIL",new TabSetting(129),"IND",NEWLINE,new TabSetting(1),"-",new RepeatItem(131),
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "*** ERROR ***",Global.getERROR_NR(),"LINE:",Global.getERROR_LINE());                                                                       //Natural: WRITE '*** ERROR ***' *ERROR-NR 'LINE:' *ERROR-LINE
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Ati_Fulfill_Voa_Ati_WpidIsBreak = cwf_Ati_Fulfill_Voa_Ati_Wpid.isBreak(endOfData);
        if (condition(cwf_Ati_Fulfill_Voa_Ati_WpidIsBreak))
        {
            getReports().skip(12, 2);                                                                                                                                     //Natural: SKIP ( 12 ) 2
            getReports().write(12, ReportOption.NOTITLE,new TabSetting(1),"TOTAL FOR WPID :",pnd_Prev_Wpid, new ReportEditMask ("XX X XX X"),":",pnd_Wpid_Total,          //Natural: WRITE ( 12 ) 1T 'TOTAL FOR WPID :' #PREV-WPID ( EM = XX�X�XX�X ) ':' #WPID-TOTAL ( AD = L )
                new FieldAttributes ("AD=L"));
            if (condition(Global.isEscape())) return;
            pnd_Wpid_Total.reset();                                                                                                                                       //Natural: RESET #WPID-TOTAL
            getReports().newPage(new ReportSpecification(12));                                                                                                            //Natural: NEWPAGE ( 12 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(12, "PS=63 LS=133 ZP=OFF");
    }
}
