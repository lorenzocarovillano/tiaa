/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:25:23 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3802
************************************************************
**        * FILE NAME            : Mcsn3802.java
**        * CLASS NAME           : Mcsn3802
**        * INSTANCE NAME        : Mcsn3802
************************************************************
************************************************************************
* PROGRAM  : MCSN3802
* TITLE    : ACCESS FULFILLMENT RECORDS FROM THE CWF-ATI-FULFILLMENT
* FUNCTION : DISPLAY ATI-FULFILLMENT RECORDS ON 192 IN WPID ORDER
*
* REVISIONS:
*
* 04/16/96 � BT  ADD WPID SUMMARY PAGE FOR SSSS FORMS FOR
*          �     SUCCESSFUL / FAILED ATI REQUESTS.
* 01/22/98 � GH  ADDED 9 NEW WPID's for unit BPSSS1, BPSSS2, BPSSSW:
*          �        FAKCW, FAKCK, FAKTD,
*          �        FASCW, FASCK, FASTD,
*          �        FABCW, FABCK, FABTD,
* 03/27/03 | GH REMOVED ACCNT(*) ADDED THE SSSS-TICKER-SYMBOL(1:20)
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3802 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;

    private DataAccessProgramView vw_cwf_Ati_Fulfill;
    private DbsField cwf_Ati_Fulfill_Ati_Rcrd_Type;

    private DbsGroup cwf_Ati_Fulfill_Ati_Rqst_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Pin;
    private DbsField cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Stts;
    private DbsField cwf_Ati_Fulfill_Ati_Wpid;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Racf_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Unit_Cde;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Error_Txt;
    private DbsField cwf_Ati_Fulfill_Ati_Prge_Ind;
    private DbsField cwf_Ati_Fulfill_Ati_Stts_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Cmtask_Ind;
    private DbsField cwf_Ati_Fulfill_Ssss_Ovrnght_Mail_Ind;
    private DbsField cwf_Ati_Fulfill_Ssss_Ctznshp_Cde;
    private DbsField cwf_Ati_Fulfill_Ssss_Rsdnce_Cde;
    private DbsField cwf_Ati_Fulfill_Ssss_Plan_Cde;
    private DbsField cwf_Ati_Fulfill_Ssss_Tiaa_Nbr;
    private DbsField cwf_Ati_Fulfill_Ssss_Rllvr_Type;
    private DbsField cwf_Ati_Fulfill_Ssss_Erly_Trm_Type;
    private DbsField cwf_Ati_Fulfill_Ssss_Wth_All_Fnds_Ind;
    private DbsField cwf_Ati_Fulfill_Ssss_Rpttve_Frqncy;
    private DbsField cwf_Ati_Fulfill_Ssss_Rpttve_Bgn_Dte;
    private DbsField cwf_Ati_Fulfill_Ssss_Rpttve_End_Dte;
    private DbsField cwf_Ati_Fulfill_Ssss_Rpttve_Nbr_Pymnts;

    private DbsGroup cwf_Ati_Fulfill_Ssss_Accnt_Data;
    private DbsField cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt;
    private DbsField cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt;
    private DbsField cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt;

    private DbsGroup cwf_Ati_Fulfill_Ssss_Accnt_Data_Oia;
    private DbsField cwf_Ati_Fulfill_Ssss_Ticker_Symbol;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_1;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_View_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_2;
    private DbsField cwf_Master_Index_View_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;
    private DbsField pnd_Ati_Completed_Key;

    private DbsGroup pnd_Ati_Completed_Key__R_Field_3;
    private DbsField pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type_Cmpl;
    private DbsField pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme;
    private DbsField pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind;
    private DbsField pnd_Grand_Total_Open_Atis;
    private DbsField pnd_Grand_Total_Inprogress_Atis;
    private DbsField pnd_Grand_Total_Failed_Atis;
    private DbsField pnd_Grand_Total_Succesfull_Atis;
    private DbsField pnd_Interim_Dollar;
    private DbsField pnd_Interim_Prcnt;
    private DbsField pnd_Interim_Units;
    private DbsField pnd_Interim_Acct_Name;
    private DbsField pnd_Process_Status_Literal;
    private DbsField pnd_Cmtask_Status_Literal;
    private DbsField pnd_Compressed_Text;
    private DbsField pnd_B;
    private DbsField pnd_Cc;
    private DbsField pnd_Prev_Wpid;
    private DbsField pnd_Wpid_Total;
    private DbsField pnd_Fld_Line;
    private DbsField pnd_Fld_Strt;
    private DbsField pnd_Fld_End;
    private DbsField pnd_Accnt_Amt_Type;
    private DbsField pnd_Field;
    private DbsField pnd_Title_Literal;
    private DbsField pnd_Input_Dte_Tme;
    private DbsField pnd_Input_Dte_D;
    private DbsField pnd_Orig_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Entry_Date;
    private DbsField pnd_Last_Page_Flag;
    private DbsField pnd_Mit_Log_Date;

    private DbsGroup pnd_Wpid_Summary_Data;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faccw_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faccw_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Factd_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Factd_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Facck_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Facck_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagcw_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagcw_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagtd_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagtd_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagck_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fagck_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakcw_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakcw_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fascw_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fascw_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabcw_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabcw_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakck_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fakck_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fasck_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fasck_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabck_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabck_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faktd_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Faktd_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fastd_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fastd_Fail_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps1;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps2;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bpsw;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Misc;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabtd_Ok_Grand;
    private DbsField pnd_Wpid_Summary_Data_Pnd_Fabtd_Fail_Grand;
    private DbsField pnd_Wpid_Sub_Total;
    private DbsField pnd_Wpid_Ati_Ok_Grand_Total;
    private DbsField pnd_Wpid_Ati_Fail_Grand_Total;
    private DbsField pnd_Wpid_Grand_Total;
    private DbsField pnd_Rqst_Routing_Key;

    private DbsGroup pnd_Rqst_Routing_Key__R_Field_4;
    private DbsField pnd_Rqst_Routing_Key_Pnd_Rqst_Mit_Log_Dt_Tme;
    private DbsField pnd_Rqst_Routing_Key_Pnd_Rqst_Mit_Last_Chnge_Dte_Tme;
    private DbsField pnd_Mit_Unit_Cde;

    private DbsGroup pnd_Mit_Unit_Cde__R_Field_5;
    private DbsField pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6;

    private DbsGroup pnd_Mit_Unit_Cde__R_Field_6;
    private DbsField pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Id_Cde;
    private DbsField pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Rgn_Cde;
    private DbsField pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Spcl_Dsgntn_Cde;
    private DbsField pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Brnch_Group_Cde;
    private DbsField pnd_Summary_Date;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Input_Data = parameters.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Ati_Fulfill = new DataAccessProgramView(new NameInfo("vw_cwf_Ati_Fulfill", "CWF-ATI-FULFILL"), "CWF_ATI_FULFILL", "CWF_KDO_FULFILL", DdmPeriodicGroups.getInstance().getGroups("CWF_ATI_FULFILL"));
        cwf_Ati_Fulfill_Ati_Rcrd_Type = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Rcrd_Type", "ATI-RCRD-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_RCRD_TYPE");

        cwf_Ati_Fulfill_Ati_Rqst_Id = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ATI_RQST_ID", "ATI-RQST-ID");
        cwf_Ati_Fulfill_Ati_Pin = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Pin", "ATI-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ATI_PIN");
        cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme", "ATI-MIT-LOG-DT-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_MIT_LOG_DT_TME");
        cwf_Ati_Fulfill_Ati_Prcss_Stts = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Stts", "ATI-PRCSS-STTS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRCSS_STTS");
        cwf_Ati_Fulfill_Ati_Wpid = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Wpid", "ATI-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ATI_WPID");
        cwf_Ati_Fulfill_Ati_Entry_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Dte_Tme", "ATI-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_ENTRY_DTE_TME");
        cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt", "ATI-ENTRY-SYSTM-OR-UNT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_SYSTM_OR_UNT");
        cwf_Ati_Fulfill_Ati_Entry_Racf_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Racf_Id", "ATI-ENTRY-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ATI_ENTRY_RACF_ID");
        cwf_Ati_Fulfill_Ati_Entry_Unit_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Unit_Cde", "ATI-ENTRY-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_UNIT_CDE");
        cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id", "ATI-PRCSS-APPLCTN-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_PRCSS_APPLCTN_ID");
        cwf_Ati_Fulfill_Ati_Error_Txt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Error_Txt", "ATI-ERROR-TXT", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "ATI_ERROR_TXT");
        cwf_Ati_Fulfill_Ati_Prge_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prge_Ind", "ATI-PRGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRGE_IND");
        cwf_Ati_Fulfill_Ati_Stts_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Stts_Dte_Tme", "ATI-STTS-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_STTS_DTE_TME");
        cwf_Ati_Fulfill_Ati_Cmtask_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Cmtask_Ind", "ATI-CMTASK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_CMTASK_IND");
        cwf_Ati_Fulfill_Ssss_Ovrnght_Mail_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Ovrnght_Mail_Ind", "SSSS-OVRNGHT-MAIL-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SSSS_OVRNGHT_MAIL_IND");
        cwf_Ati_Fulfill_Ssss_Ctznshp_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Ctznshp_Cde", "SSSS-CTZNSHP-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "SSSS_CTZNSHP_CDE");
        cwf_Ati_Fulfill_Ssss_Rsdnce_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Rsdnce_Cde", "SSSS-RSDNCE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "SSSS_RSDNCE_CDE");
        cwf_Ati_Fulfill_Ssss_Plan_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Plan_Cde", "SSSS-PLAN-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "SSSS_PLAN_CDE");
        cwf_Ati_Fulfill_Ssss_Tiaa_Nbr = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Tiaa_Nbr", "SSSS-TIAA-NBR", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "SSSS_TIAA_NBR");
        cwf_Ati_Fulfill_Ssss_Rllvr_Type = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Rllvr_Type", "SSSS-RLLVR-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SSSS_RLLVR_TYPE");
        cwf_Ati_Fulfill_Ssss_Erly_Trm_Type = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Erly_Trm_Type", "SSSS-ERLY-TRM-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SSSS_ERLY_TRM_TYPE");
        cwf_Ati_Fulfill_Ssss_Wth_All_Fnds_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Wth_All_Fnds_Ind", "SSSS-WTH-ALL-FNDS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SSSS_WTH_ALL_FNDS_IND");
        cwf_Ati_Fulfill_Ssss_Rpttve_Frqncy = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Rpttve_Frqncy", "SSSS-RPTTVE-FRQNCY", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SSSS_RPTTVE_FRQNCY");
        cwf_Ati_Fulfill_Ssss_Rpttve_Bgn_Dte = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Rpttve_Bgn_Dte", "SSSS-RPTTVE-BGN-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "SSSS_RPTTVE_BGN_DTE");
        cwf_Ati_Fulfill_Ssss_Rpttve_End_Dte = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Rpttve_End_Dte", "SSSS-RPTTVE-END-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "SSSS_RPTTVE_END_DTE");
        cwf_Ati_Fulfill_Ssss_Rpttve_Nbr_Pymnts = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ssss_Rpttve_Nbr_Pymnts", "SSSS-RPTTVE-NBR-PYMNTS", 
            FieldType.PACKED_DECIMAL, 5, RepeatingFieldStrategy.None, "SSSS_RPTTVE_NBR_PYMNTS");

        cwf_Ati_Fulfill_Ssss_Accnt_Data = vw_cwf_Ati_Fulfill.getRecord().newGroupArrayInGroup("cwf_Ati_Fulfill_Ssss_Accnt_Data", "SSSS-ACCNT-DATA", new 
            DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_KDO_FULFILL_SSSS_ACCNT_DATA");
        cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt = cwf_Ati_Fulfill_Ssss_Accnt_Data.newFieldInGroup("cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt", "SSSS-WTHDRWL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "SSSS_WTHDRWL_AMT", "CWF_KDO_FULFILL_SSSS_ACCNT_DATA");
        cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt = cwf_Ati_Fulfill_Ssss_Accnt_Data.newFieldInGroup("cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt", "SSSS-WTHDRWL-PRCNT", 
            FieldType.PACKED_DECIMAL, 6, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "SSSS_WTHDRWL_PRCNT", "CWF_KDO_FULFILL_SSSS_ACCNT_DATA");
        cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt = cwf_Ati_Fulfill_Ssss_Accnt_Data.newFieldInGroup("cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt", "SSSS-WTHDRWL-UNT", FieldType.PACKED_DECIMAL, 
            11, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "SSSS_WTHDRWL_UNT", "CWF_KDO_FULFILL_SSSS_ACCNT_DATA");

        cwf_Ati_Fulfill_Ssss_Accnt_Data_Oia = vw_cwf_Ati_Fulfill.getRecord().newGroupArrayInGroup("cwf_Ati_Fulfill_Ssss_Accnt_Data_Oia", "SSSS-ACCNT-DATA-OIA", 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_KDO_FULFILL_SSSS_ACCNT_DATA_OIA");
        cwf_Ati_Fulfill_Ssss_Ticker_Symbol = cwf_Ati_Fulfill_Ssss_Accnt_Data_Oia.newFieldInGroup("cwf_Ati_Fulfill_Ssss_Ticker_Symbol", "SSSS-TICKER-SYMBOL", 
            FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "SSSS_TICKER_SYMBOL", "CWF_KDO_FULFILL_SSSS_ACCNT_DATA_OIA");
        registerRecord(vw_cwf_Ati_Fulfill);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Pin_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Master_Index_View_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index_View__R_Field_1 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_1", "REDEFINE", cwf_Master_Index_View_Rqst_Log_Dte_Tme);
        cwf_Master_Index_View_Rqst_Log_Index_Dte = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Log_Index_Tme = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_View_Rqst_Orgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Orgn_Cde", "RQST-ORGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_View_Orgnl_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_View_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");

        cwf_Master_Index_View__R_Field_2 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_2", "REDEFINE", cwf_Master_Index_View_Work_Prcss_Id);
        cwf_Master_Index_View_Work_Actn_Rqstd_Cde = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde", 
            "WORK-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde", 
            "WORK-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        registerRecord(vw_cwf_Master_Index_View);

        pnd_Ati_Completed_Key = localVariables.newFieldInRecord("pnd_Ati_Completed_Key", "#ATI-COMPLETED-KEY", FieldType.STRING, 9);

        pnd_Ati_Completed_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Ati_Completed_Key__R_Field_3", "REDEFINE", pnd_Ati_Completed_Key);
        pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type_Cmpl = pnd_Ati_Completed_Key__R_Field_3.newFieldInGroup("pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type_Cmpl", 
            "#ATI-RCRD-TYPE-CMPL", FieldType.STRING, 1);
        pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme = pnd_Ati_Completed_Key__R_Field_3.newFieldInGroup("pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme", 
            "#ATI-ENTRY-DTE-TME", FieldType.TIME);
        pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind = pnd_Ati_Completed_Key__R_Field_3.newFieldInGroup("pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind", "#ATI-PRGE-IND", 
            FieldType.STRING, 1);
        pnd_Grand_Total_Open_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Open_Atis", "#GRAND-TOTAL-OPEN-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Inprogress_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Inprogress_Atis", "#GRAND-TOTAL-INPROGRESS-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Grand_Total_Failed_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Failed_Atis", "#GRAND-TOTAL-FAILED-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Succesfull_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Succesfull_Atis", "#GRAND-TOTAL-SUCCESFULL-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Interim_Dollar = localVariables.newFieldInRecord("pnd_Interim_Dollar", "#INTERIM-DOLLAR", FieldType.NUMERIC, 12, 2);
        pnd_Interim_Prcnt = localVariables.newFieldInRecord("pnd_Interim_Prcnt", "#INTERIM-PRCNT", FieldType.NUMERIC, 12, 3);
        pnd_Interim_Units = localVariables.newFieldInRecord("pnd_Interim_Units", "#INTERIM-UNITS", FieldType.NUMERIC, 12, 3);
        pnd_Interim_Acct_Name = localVariables.newFieldInRecord("pnd_Interim_Acct_Name", "#INTERIM-ACCT-NAME", FieldType.STRING, 11);
        pnd_Process_Status_Literal = localVariables.newFieldInRecord("pnd_Process_Status_Literal", "#PROCESS-STATUS-LITERAL", FieldType.STRING, 7);
        pnd_Cmtask_Status_Literal = localVariables.newFieldInRecord("pnd_Cmtask_Status_Literal", "#CMTASK-STATUS-LITERAL", FieldType.STRING, 27);
        pnd_Compressed_Text = localVariables.newFieldInRecord("pnd_Compressed_Text", "#COMPRESSED-TEXT", FieldType.STRING, 38);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 3);
        pnd_Cc = localVariables.newFieldInRecord("pnd_Cc", "#CC", FieldType.NUMERIC, 3);
        pnd_Prev_Wpid = localVariables.newFieldInRecord("pnd_Prev_Wpid", "#PREV-WPID", FieldType.STRING, 6);
        pnd_Wpid_Total = localVariables.newFieldInRecord("pnd_Wpid_Total", "#WPID-TOTAL", FieldType.NUMERIC, 8);
        pnd_Fld_Line = localVariables.newFieldInRecord("pnd_Fld_Line", "#FLD-LINE", FieldType.NUMERIC, 3);
        pnd_Fld_Strt = localVariables.newFieldInRecord("pnd_Fld_Strt", "#FLD-STRT", FieldType.NUMERIC, 3);
        pnd_Fld_End = localVariables.newFieldInRecord("pnd_Fld_End", "#FLD-END", FieldType.NUMERIC, 3);
        pnd_Accnt_Amt_Type = localVariables.newFieldInRecord("pnd_Accnt_Amt_Type", "#ACCNT-AMT-TYPE", FieldType.STRING, 1);
        pnd_Field = localVariables.newFieldInRecord("pnd_Field", "#FIELD", FieldType.STRING, 30);
        pnd_Title_Literal = localVariables.newFieldInRecord("pnd_Title_Literal", "#TITLE-LITERAL", FieldType.STRING, 54);
        pnd_Input_Dte_Tme = localVariables.newFieldInRecord("pnd_Input_Dte_Tme", "#INPUT-DTE-TME", FieldType.TIME);
        pnd_Input_Dte_D = localVariables.newFieldInRecord("pnd_Input_Dte_D", "#INPUT-DTE-D", FieldType.DATE);
        pnd_Orig_Date = localVariables.newFieldInRecord("pnd_Orig_Date", "#ORIG-DATE", FieldType.DATE);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 8);
        pnd_Entry_Date = localVariables.newFieldInRecord("pnd_Entry_Date", "#ENTRY-DATE", FieldType.DATE);
        pnd_Last_Page_Flag = localVariables.newFieldInRecord("pnd_Last_Page_Flag", "#LAST-PAGE-FLAG", FieldType.STRING, 1);
        pnd_Mit_Log_Date = localVariables.newFieldInRecord("pnd_Mit_Log_Date", "#MIT-LOG-DATE", FieldType.NUMERIC, 14);

        pnd_Wpid_Summary_Data = localVariables.newGroupInRecord("pnd_Wpid_Summary_Data", "#WPID-SUMMARY-DATA");
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps1", "#FACCW-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps2", "#FACCW-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bpsw", "#FACCW-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Misc", "#FACCW-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps1", "#FACCW-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps2", "#FACCW-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bpsw", "#FACCW-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Misc", "#FACCW-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faccw_Ok_Grand", "#FACCW-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faccw_Fail_Grand", "#FACCW-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps1", "#FACTD-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps2", "#FACTD-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bpsw", "#FACTD-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Misc", "#FACTD-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps1", "#FACTD-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps2", "#FACTD-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bpsw", "#FACTD-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Misc", "#FACTD-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Factd_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Factd_Ok_Grand", "#FACTD-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Factd_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Factd_Fail_Grand", "#FACTD-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps1", "#FACCK-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps2", "#FACCK-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bpsw", "#FACCK-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Misc", "#FACCK-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps1", "#FACCK-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps2", "#FACCK-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bpsw", "#FACCK-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Misc", "#FACCK-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Facck_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Facck_Ok_Grand", "#FACCK-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Facck_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Facck_Fail_Grand", "#FACCK-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps1", "#FAGCW-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps2", "#FAGCW-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bpsw", "#FAGCW-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Misc", "#FAGCW-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps1", "#FAGCW-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps2", "#FAGCW-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bpsw", "#FAGCW-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Misc", "#FAGCW-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagcw_Ok_Grand", "#FAGCW-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagcw_Fail_Grand", "#FAGCW-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps1", "#FAGTD-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps2", "#FAGTD-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bpsw", "#FAGTD-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Misc", "#FAGTD-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps1", "#FAGTD-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps2", "#FAGTD-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bpsw", "#FAGTD-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Misc", "#FAGTD-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagtd_Ok_Grand", "#FAGTD-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagtd_Fail_Grand", "#FAGTD-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps1", "#FAGCK-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps2", "#FAGCK-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bpsw", "#FAGCK-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Misc", "#FAGCK-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps1", "#FAGCK-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps2", "#FAGCK-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bpsw", "#FAGCK-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Misc", "#FAGCK-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagck_Ok_Grand", "#FAGCK-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fagck_Fail_Grand", "#FAGCK-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps1", "#FAKCW-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps2", "#FAKCW-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bpsw", "#FAKCW-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Misc", "#FAKCW-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps1", "#FAKCW-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps2", "#FAKCW-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bpsw", "#FAKCW-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Misc", "#FAKCW-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakcw_Ok_Grand", "#FAKCW-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakcw_Fail_Grand", "#FAKCW-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps1", "#FASCW-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps2", "#FASCW-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bpsw", "#FASCW-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Misc", "#FASCW-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps1", "#FASCW-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps2", "#FASCW-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bpsw", "#FASCW-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Misc", "#FASCW-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fascw_Ok_Grand", "#FASCW-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fascw_Fail_Grand", "#FASCW-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps1", "#FABCW-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps2", "#FABCW-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bpsw", "#FABCW-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Misc", "#FABCW-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps1", "#FABCW-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps2", "#FABCW-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bpsw", "#FABCW-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Misc", "#FABCW-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabcw_Ok_Grand", "#FABCW-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabcw_Fail_Grand", "#FABCW-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps1", "#FAKCK-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps2", "#FAKCK-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bpsw", "#FAKCK-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Misc", "#FAKCK-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps1", "#FAKCK-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps2", "#FAKCK-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bpsw", "#FAKCK-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Misc", "#FAKCK-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakck_Ok_Grand", "#FAKCK-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fakck_Fail_Grand", "#FAKCK-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps1", "#FASCK-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps2", "#FASCK-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bpsw", "#FASCK-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Misc", "#FASCK-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps1", "#FASCK-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps2", "#FASCK-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bpsw", "#FASCK-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Misc", "#FASCK-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fasck_Ok_Grand", "#FASCK-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fasck_Fail_Grand", "#FASCK-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps1", "#FABCK-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps2", "#FABCK-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bpsw", "#FABCK-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Misc", "#FABCK-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps1", "#FABCK-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps2", "#FABCK-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bpsw", "#FABCK-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Misc", "#FABCK-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabck_Ok_Grand", "#FABCK-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabck_Fail_Grand", "#FABCK-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps1", "#FAKTD-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps2", "#FAKTD-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bpsw", "#FAKTD-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Misc", "#FAKTD-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps1", "#FAKTD-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps2", "#FAKTD-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bpsw", "#FAKTD-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Misc", "#FAKTD-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faktd_Ok_Grand", "#FAKTD-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Faktd_Fail_Grand", "#FAKTD-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps1", "#FASTD-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps2", "#FASTD-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bpsw", "#FASTD-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Misc", "#FASTD-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps1", "#FASTD-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps2", "#FASTD-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bpsw", "#FASTD-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Misc", "#FASTD-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fastd_Ok_Grand", "#FASTD-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fastd_Fail_Grand", "#FASTD-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps1", "#FABTD-SUM-OK-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps2", "#FABTD-SUM-OK-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bpsw", "#FABTD-SUM-OK-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Misc", "#FABTD-SUM-OK-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps1 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps1", "#FABTD-SUM-FAIL-BPS1", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps2 = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps2", "#FABTD-SUM-FAIL-BPS2", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bpsw = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bpsw", "#FABTD-SUM-FAIL-BPSW", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Misc = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Misc", "#FABTD-SUM-FAIL-MISC", 
            FieldType.NUMERIC, 5);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Ok_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabtd_Ok_Grand", "#FABTD-OK-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Fail_Grand = pnd_Wpid_Summary_Data.newFieldInGroup("pnd_Wpid_Summary_Data_Pnd_Fabtd_Fail_Grand", "#FABTD-FAIL-GRAND", 
            FieldType.NUMERIC, 6);
        pnd_Wpid_Sub_Total = localVariables.newFieldInRecord("pnd_Wpid_Sub_Total", "#WPID-SUB-TOTAL", FieldType.NUMERIC, 6);
        pnd_Wpid_Ati_Ok_Grand_Total = localVariables.newFieldInRecord("pnd_Wpid_Ati_Ok_Grand_Total", "#WPID-ATI-OK-GRAND-TOTAL", FieldType.NUMERIC, 7);
        pnd_Wpid_Ati_Fail_Grand_Total = localVariables.newFieldInRecord("pnd_Wpid_Ati_Fail_Grand_Total", "#WPID-ATI-FAIL-GRAND-TOTAL", FieldType.NUMERIC, 
            7);
        pnd_Wpid_Grand_Total = localVariables.newFieldInRecord("pnd_Wpid_Grand_Total", "#WPID-GRAND-TOTAL", FieldType.NUMERIC, 8);
        pnd_Rqst_Routing_Key = localVariables.newFieldInRecord("pnd_Rqst_Routing_Key", "#RQST-ROUTING-KEY", FieldType.STRING, 30);

        pnd_Rqst_Routing_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Rqst_Routing_Key__R_Field_4", "REDEFINE", pnd_Rqst_Routing_Key);
        pnd_Rqst_Routing_Key_Pnd_Rqst_Mit_Log_Dt_Tme = pnd_Rqst_Routing_Key__R_Field_4.newFieldInGroup("pnd_Rqst_Routing_Key_Pnd_Rqst_Mit_Log_Dt_Tme", 
            "#RQST-MIT-LOG-DT-TME", FieldType.STRING, 15);
        pnd_Rqst_Routing_Key_Pnd_Rqst_Mit_Last_Chnge_Dte_Tme = pnd_Rqst_Routing_Key__R_Field_4.newFieldInGroup("pnd_Rqst_Routing_Key_Pnd_Rqst_Mit_Last_Chnge_Dte_Tme", 
            "#RQST-MIT-LAST-CHNGE-DTE-TME", FieldType.STRING, 15);
        pnd_Mit_Unit_Cde = localVariables.newFieldInRecord("pnd_Mit_Unit_Cde", "#MIT-UNIT-CDE", FieldType.STRING, 8);

        pnd_Mit_Unit_Cde__R_Field_5 = localVariables.newGroupInRecord("pnd_Mit_Unit_Cde__R_Field_5", "REDEFINE", pnd_Mit_Unit_Cde);
        pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6 = pnd_Mit_Unit_Cde__R_Field_5.newFieldInGroup("pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6", "#MIT-UNIT-CDE-1-6", 
            FieldType.STRING, 6);

        pnd_Mit_Unit_Cde__R_Field_6 = pnd_Mit_Unit_Cde__R_Field_5.newGroupInGroup("pnd_Mit_Unit_Cde__R_Field_6", "REDEFINE", pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6);
        pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Id_Cde = pnd_Mit_Unit_Cde__R_Field_6.newFieldInGroup("pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Id_Cde", "#MIT-UNIT-ID-CDE", 
            FieldType.STRING, 5);
        pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Rgn_Cde = pnd_Mit_Unit_Cde__R_Field_6.newFieldInGroup("pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Rgn_Cde", "#MIT-UNIT-RGN-CDE", 
            FieldType.STRING, 1);
        pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Spcl_Dsgntn_Cde = pnd_Mit_Unit_Cde__R_Field_5.newFieldInGroup("pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Spcl_Dsgntn_Cde", "#MIT-UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Brnch_Group_Cde = pnd_Mit_Unit_Cde__R_Field_5.newFieldInGroup("pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Brnch_Group_Cde", "#MIT-UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        pnd_Summary_Date = localVariables.newFieldInRecord("pnd_Summary_Date", "#SUMMARY-DATE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Ati_Fulfill.reset();
        vw_cwf_Master_Index_View.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faccw_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Factd_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Factd_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Facck_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Facck_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fagck_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fascw_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fakck_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fasck_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabck_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Faktd_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fastd_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps1.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps2.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bpsw.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Misc.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Ok_Grand.setInitialValue(0);
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Fail_Grand.setInitialValue(0);
        pnd_Wpid_Sub_Total.setInitialValue(0);
        pnd_Wpid_Ati_Ok_Grand_Total.setInitialValue(0);
        pnd_Wpid_Ati_Fail_Grand_Total.setInitialValue(0);
        pnd_Wpid_Grand_Total.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Mcsn3802() throws Exception
    {
        super("Mcsn3802");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atEndOfPage(atEndEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 1 ) PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 2 ) PS = 60 LS = 133 ZP = OFF
        pnd_Last_Page_Flag.reset();                                                                                                                                       //Natural: RESET #LAST-PAGE-FLAG #GRAND-TOTAL-OPEN-ATIS #GRAND-TOTAL-INPROGRESS-ATIS #GRAND-TOTAL-FAILED-ATIS #GRAND-TOTAL-SUCCESFULL-ATIS
        pnd_Grand_Total_Open_Atis.reset();
        pnd_Grand_Total_Inprogress_Atis.reset();
        pnd_Grand_Total_Failed_Atis.reset();
        pnd_Grand_Total_Succesfull_Atis.reset();
        //* *** INITIALIZE THE ATI-COMPLETED-KEY SUPER-DESCRIPTOR FIELD
        pnd_Ati_Completed_Key_Pnd_Ati_Entry_Dte_Tme.reset();                                                                                                              //Natural: RESET #ATI-ENTRY-DTE-TME #ATI-PRGE-IND
        pnd_Ati_Completed_Key_Pnd_Ati_Prge_Ind.reset();
        pnd_Ati_Completed_Key_Pnd_Ati_Rcrd_Type_Cmpl.setValue("1");                                                                                                       //Natural: ASSIGN #ATI-RCRD-TYPE-CMPL := '1'
        //* ******************************************************************
        //* ** READ CWF-ATI-FULFILL AND PROCESS SUCCESFULL AND FAILED REQUESTS
        //* ******************************************************************
        pnd_Title_Literal.setValue("*** SUCCESSFULL/FAILED ATI REQUESTS FOR SSSS FORMS ***");                                                                             //Natural: MOVE '*** SUCCESSFULL/FAILED ATI REQUESTS FOR SSSS FORMS ***' TO #TITLE-LITERAL
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        vw_cwf_Ati_Fulfill.startDatabaseRead                                                                                                                              //Natural: READ CWF-ATI-FULFILL BY ATI-COMPLETED-KEY STARTING FROM #ATI-COMPLETED-KEY
        (
        "READ_COMPLETE",
        new Wc[] { new Wc("ATI_COMPLETED_KEY", ">=", pnd_Ati_Completed_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("ATI_COMPLETED_KEY", "ASC") }
        );
        READ_COMPLETE:
        while (condition(vw_cwf_Ati_Fulfill.readNextRow("READ_COMPLETE")))
        {
            if (condition(cwf_Ati_Fulfill_Ati_Rcrd_Type.greater("2")))                                                                                                    //Natural: IF CWF-ATI-FULFILL.ATI-RCRD-TYPE GT '2'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Entry_Date.setValue(cwf_Ati_Fulfill_Ati_Stts_Dte_Tme);                                                                                                    //Natural: MOVE CWF-ATI-FULFILL.ATI-STTS-DTE-TME TO #ENTRY-DATE
            if (condition(!(pnd_Entry_Date.equals(pnd_Input_Dte_D) && cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id.equals("SSSS"))))                                              //Natural: ACCEPT IF #ENTRY-DATE = #INPUT-DTE-D AND ATI-PRCSS-APPLCTN-ID = 'SSSS'
            {
                continue;
            }
            getSort().writeSortInData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type,               //Natural: END-ALL
                cwf_Ati_Fulfill_Ati_Prcss_Stts, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, 
                cwf_Ati_Fulfill_Ati_Entry_Racf_Id, cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Ssss_Rllvr_Type, 
                cwf_Ati_Fulfill_Ssss_Erly_Trm_Type, cwf_Ati_Fulfill_Ssss_Wth_All_Fnds_Ind, cwf_Ati_Fulfill_Ssss_Rpttve_Frqncy, cwf_Ati_Fulfill_Ssss_Rpttve_Nbr_Pymnts, 
                cwf_Ati_Fulfill_Ssss_Rpttve_Bgn_Dte, cwf_Ati_Fulfill_Ssss_Rpttve_End_Dte, cwf_Ati_Fulfill_Ssss_Ovrnght_Mail_Ind, cwf_Ati_Fulfill_Ssss_Ctznshp_Cde, 
                cwf_Ati_Fulfill_Ssss_Rsdnce_Cde, cwf_Ati_Fulfill_Ssss_Plan_Cde, cwf_Ati_Fulfill_Ssss_Tiaa_Nbr, cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(1), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(2), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(3), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(4), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(5), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(6), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(7), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(8), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(9), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(10), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(11), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(12), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(13), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(14), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(15), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(16), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(17), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(18), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(19), 
                cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(20), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(1), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(2), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(3), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(4), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(5), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(6), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(7), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(8), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(9), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(10), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(11), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(12), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(13), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(14), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(15), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(16), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(17), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(18), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(19), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(20), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(1), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(2), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(3), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(4), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(5), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(6), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(7), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(8), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(9), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(10), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(11), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(12), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(13), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(14), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(15), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(16), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(17), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(18), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(19), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(20), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(1), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(2), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(3), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(4), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(5), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(6), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(7), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(8), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(9), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(10), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(11), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(12), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(13), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(14), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(15), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(16), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(17), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(18), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(19), 
                cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(20));
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  SORT PERFORMED TO WRITE IN WPID/PIN/MIT-LOG-DATE-TIME/REC-TYPE ORDER
        getSort().sortData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type, cwf_Ati_Fulfill_Ati_Prcss_Stts); //Natural: SORT RECORDS BY CWF-ATI-FULFILL.ATI-WPID CWF-ATI-FULFILL.ATI-PIN CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME CWF-ATI-FULFILL.ATI-RCRD-TYPE CWF-ATI-FULFILL.ATI-PRCSS-STTS USING CWF-ATI-FULFILL.ATI-ERROR-TXT CWF-ATI-FULFILL.ATI-ENTRY-DTE-TME CWF-ATI-FULFILL.ATI-ENTRY-SYSTM-OR-UNT CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE CWF-ATI-FULFILL.ATI-STTS-DTE-TME CWF-ATI-FULFILL.SSSS-RLLVR-TYPE CWF-ATI-FULFILL.SSSS-ERLY-TRM-TYPE CWF-ATI-FULFILL.SSSS-WTH-ALL-FNDS-IND CWF-ATI-FULFILL.SSSS-RPTTVE-FRQNCY CWF-ATI-FULFILL.SSSS-RPTTVE-NBR-PYMNTS CWF-ATI-FULFILL.SSSS-RPTTVE-BGN-DTE CWF-ATI-FULFILL.SSSS-RPTTVE-END-DTE CWF-ATI-FULFILL.SSSS-OVRNGHT-MAIL-IND CWF-ATI-FULFILL.SSSS-CTZNSHP-CDE CWF-ATI-FULFILL.SSSS-RSDNCE-CDE CWF-ATI-FULFILL.SSSS-PLAN-CDE CWF-ATI-FULFILL.SSSS-TIAA-NBR CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( * ) CWF-ATI-FULFILL.SSSS-WTHDRWL-AMT ( * ) CWF-ATI-FULFILL.SSSS-WTHDRWL-PRCNT ( * ) CWF-ATI-FULFILL.SSSS-WTHDRWL-UNT ( * )
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type, 
            cwf_Ati_Fulfill_Ati_Prcss_Stts, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, cwf_Ati_Fulfill_Ati_Entry_Racf_Id, 
            cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Ssss_Rllvr_Type, cwf_Ati_Fulfill_Ssss_Erly_Trm_Type, cwf_Ati_Fulfill_Ssss_Wth_All_Fnds_Ind, 
            cwf_Ati_Fulfill_Ssss_Rpttve_Frqncy, cwf_Ati_Fulfill_Ssss_Rpttve_Nbr_Pymnts, cwf_Ati_Fulfill_Ssss_Rpttve_Bgn_Dte, cwf_Ati_Fulfill_Ssss_Rpttve_End_Dte, 
            cwf_Ati_Fulfill_Ssss_Ovrnght_Mail_Ind, cwf_Ati_Fulfill_Ssss_Ctznshp_Cde, cwf_Ati_Fulfill_Ssss_Rsdnce_Cde, cwf_Ati_Fulfill_Ssss_Plan_Cde, cwf_Ati_Fulfill_Ssss_Tiaa_Nbr, 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(1), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(2), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(3), 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(4), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(5), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(6), 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(7), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(8), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(9), 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(10), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(11), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(12), 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(13), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(14), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(15), 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(16), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(17), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(18), 
            cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(19), cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(20), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(1), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(2), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(3), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(4), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(5), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(6), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(7), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(8), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(9), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(10), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(11), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(12), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(13), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(14), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(15), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(16), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(17), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(18), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(19), cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(20), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(1), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(2), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(3), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(4), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(5), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(6), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(7), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(8), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(9), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(10), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(11), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(12), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(13), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(14), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(15), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(16), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(17), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(18), cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(19), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(20), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(1), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(2), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(3), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(4), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(5), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(6), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(7), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(8), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(9), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(10), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(11), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(12), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(13), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(14), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(15), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(16), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(17), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(18), cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(19), 
            cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(20))))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF CWF-ATI-FULFILL.ATI-WPID
            pnd_Prev_Wpid.setValue(cwf_Ati_Fulfill_Ati_Wpid);                                                                                                             //Natural: ASSIGN #PREV-WPID := CWF-ATI-FULFILL.ATI-WPID
            if (condition(cwf_Ati_Fulfill_Ati_Rcrd_Type.equals("1")))                                                                                                     //Natural: IF CWF-ATI-FULFILL.ATI-RCRD-TYPE = '1'
            {
                pnd_Wpid_Total.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WPID-TOTAL
                //*  REV. WPID SMRY
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-MIT-UNIT-CDE
                sub_Retrieve_Mit_Unit_Cde();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ATI-WRITE-REC-1
                sub_Ati_Write_Rec_1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ATI-WRITE-REC-2
                sub_Ati_Write_Rec_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* *****************************************
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //*  4T CWF-ATI-FULFILL.ATI-PIN
        //*  20T CWF-ATI-FULFILL.ATI-WPID
        //*  27T CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  43T CWF-ATI-FULFILL.ATI-ENTRY-DTE-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  48T CWF-ATI-FULFILL.ATI-STTS-DTE-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  68T #PROCESS-STATUS-LITERAL
        //*  78T CWF-ATI-FULFILL.SSSS-OVRNGHT-MAIL-IND
        //*  82T CWF-ATI-FULFILL.SSSS-CTZNSHP-CDE
        //* *DEFINE SET-ACCOUNT-NAME
        //* *DECIDE ON FIRST VALUE OF CWF-ATI-FULFILL.SSSS-ACCNT(#B)
        //*   VALUE 'T'
        //*     MOVE 'TIAA'       TO #INTERIM-ACCT-NAME
        //*   VALUE 'C'
        //*     MOVE 'STOCK'      TO #INTERIM-ACCT-NAME
        //*   VALUE 'M'
        //*     MOVE 'MMA'        TO #INTERIM-ACCT-NAME
        //*   VALUE 'W'
        //*     MOVE 'GLOBAL'     TO #INTERIM-ACCT-NAME
        //*   VALUE 'L'
        //*     MOVE 'GROWTH'     TO #INTERIM-ACCT-NAME
        //*   VALUE 'S'
        //*     MOVE 'SOCIAL'     TO #INTERIM-ACCT-NAME
        //*   VALUE 'E'
        //*     MOVE 'EQUITY'     TO #INTERIM-ACCT-NAME
        //*   VALUE 'R'
        //*     MOVE 'REAL ESTATE' TO #INTERIM-ACCT-NAME
        //*   VALUE 'B'
        //*     MOVE 'BOND'       TO #INTERIM-ACCT-NAME
        //*   VALUE 'I'
        //*    MOVE 'INDEX BOND'  TO #INTERIM-ACCT-NAME
        //*   VALUE ' '
        //*    MOVE 'NO ACCOUNT'  TO #INTERIM-ACCT-NAME
        //*   NONE
        //*    MOVE CWF-ATI-FULFILL.SSSS-ACCNT(#B) TO #INTERIM-ACCT-NAME
        //* *END-DECIDE
        //* *END-SUBROUTINE
        //* *************************************************************
        //*              MOVE ERROR MESSAGE TO CONSOLE
        //* *************************************************************
        //* ***********************************************************************
        //*   AT TOP OF PAGE ROUTINE FOR REPORT (1)
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //* ***********************************************************************
        //*   AT END OF PAGE ROUTINE FOR REPORT (1)
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: AT END OF PAGE ( 1 )
        //* ***********************************************************************
        //*   AT TOP OF PAGE ROUTINE FOR REPORT (2)
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //* ***********************************************************************
        //*   WRITE OUT ATI GRAND TOTALS ON LAST PAGE OF REPORT (REPORT 1)
        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Last_Page_Flag.setValue("Y");                                                                                                                                 //Natural: ASSIGN #LAST-PAGE-FLAG := 'Y'
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"_",new RepeatItem(131),NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) //////// 1T '_' ( 131 ) // 1T '_' ( 50 ) 52T '   ATI (SSSS) GRAND TOTALS   ' 82T '_' ( 51 ) // 1T '_' ( 131 ) //// 45T 'OPEN ATIS        :' #GRAND-TOTAL-OPEN-ATIS / 45T 'IN-PROGRESS ATIS :' #GRAND-TOTAL-INPROGRESS-ATIS / 45T 'SUCCESSFULL ATIS :' #GRAND-TOTAL-SUCCESFULL-ATIS / 45T 'FAILED ATIS      :' #GRAND-TOTAL-FAILED-ATIS
            TabSetting(1),"_",new RepeatItem(50),new TabSetting(52),"   ATI (SSSS) GRAND TOTALS   ",new TabSetting(82),"_",new RepeatItem(51),NEWLINE,NEWLINE,new 
            TabSetting(1),"_",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"OPEN ATIS        :",pnd_Grand_Total_Open_Atis,NEWLINE,new 
            TabSetting(45),"IN-PROGRESS ATIS :",pnd_Grand_Total_Inprogress_Atis,NEWLINE,new TabSetting(45),"SUCCESSFULL ATIS :",pnd_Grand_Total_Succesfull_Atis,NEWLINE,new 
            TabSetting(45),"FAILED ATIS      :",pnd_Grand_Total_Failed_Atis);
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*   WRITE OUT ATI SUMMARY REPORT (REPORT 2)  /* *  REV. WPID SMRY ******
        //* ***********************************************************************
        //*  BPSSS1 SUMMARY REPORT
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),"UNIT = ",new TabSetting(10),"BPSSS1 ",NEWLINE,new TabSetting(10),"------",NEWLINE,NEWLINE,NEWLINE,new  //Natural: WRITE ( 2 ) / 2T 'UNIT = ' 10T 'BPSSS1 ' / 10T '------' /// 10T 'WPID' 77T 'ATI' 96T 'SSSS' 115T 'TOTAL' / 10T '____' 77T '___' 96T '____' 115T '_____' /
            TabSetting(10),"WPID",new TabSetting(77),"ATI",new TabSetting(96),"SSSS",new TabSetting(115),"TOTAL",NEWLINE,new TabSetting(10),"____",new TabSetting(77),"___",new 
            TabSetting(96),"____",new TabSetting(115),"_____",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACCW-SUM-OK-BPS1 + #FACCW-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACCW -  REQUEST FOR CREF CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FACCW -  REQUEST FOR CREF CASH WITHDRAWAL FORM' 74T #FACCW-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FACCW-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACTD-SUM-OK-BPS1 + #FACTD-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACTD -  REQUEST FOR CREF DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FACTD -  REQUEST FOR CREF DIRECT TRANSFER FORM' 74T #FACTD-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FACTD-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACCK-SUM-OK-BPS1 + #FACCK-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACCK -  REQUEST FOR CREF SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FACCK -  REQUEST FOR CREF SWAT CASH WITHDRAWAL FORM' 74T #FACCK-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FACCK-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGCW-SUM-OK-BPS1 + #FAGCW-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGCW -  REQUEST FOR GRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FAGCW -  REQUEST FOR GRA WITHDRAWAL FORM' 74T #FAGCW-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FAGCW-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGTD-SUM-OK-BPS1 + #FAGTD-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGTD -  REQUEST FOR GRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FAGTD -  REQUEST FOR GRA DIRECT TRANSFER FORM' 74T #FAGTD-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FAGTD-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGCK-SUM-OK-BPS1 + #FAGCK-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGCK -  REQUEST FOR GRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FAGCK -  REQUEST FOR GRA SWAT CASH WITHDRAWAL FORM' 74T #FAGCK-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FAGCK-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKCW-SUM-OK-BPS1 + #FAKCW-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKCW -  REQUEST FOR IRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FAKCW -  REQUEST FOR IRA WITHDRAWAL FORM' 74T #FAKCW-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FAKCW-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASCW-SUM-OK-BPS1 + #FASCW-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASCW -  REQUEST FOR SRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FASCW -  REQUEST FOR SRA WITHDRAWAL FORM' 74T #FASCW-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FASCW-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABCW-SUM-OK-BPS1 + #FABCW-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABCW -  REQUEST FOR GSRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FABCW -  REQUEST FOR GSRA WITHDRAWAL FORM' 74T #FABCW-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FABCW-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKCK-SUM-OK-BPS1 + #FAKCK-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKCK -  REQUEST FOR IRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FAKCK -  REQUEST FOR IRA SWAT CASH WITHDRAWAL FORM' 74T #FAKCK-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FAKCK-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASCK-SUM-OK-BPS1 + #FASCK-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASCK -  REQUEST FOR SRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FASCK -  REQUEST FOR SRA SWAT CASH WITHDRAWAL FORM' 74T #FASCK-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FASCK-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABCK-SUM-OK-BPS1 + #FABCK-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABCK -  REQUEST FOR GSRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FABCK -  REQUEST FOR GSRA SWAT CASH WITHDRAWAL FORM' 74T #FABCK-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FABCK-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKTD-SUM-OK-BPS1 + #FAKTD-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKTD -  REQUEST FOR IRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FAKTD -  REQUEST FOR IRA DIRECT TRANSFER FORM' 74T #FAKTD-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FAKTD-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASTD-SUM-OK-BPS1 + #FASTD-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASTD -  REQUEST FOR SRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FASTD -  REQUEST FOR SRA DIRECT TRANSFER FORM' 74T #FASTD-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FASTD-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABTD-SUM-OK-BPS1 + #FABTD-SUM-FAIL-BPS1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABTD -  REQUEST FOR GSRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps1,  //Natural: WRITE ( 2 ) / 10T 'FABTD -  REQUEST FOR GSRA DIRECT TRANSFER FORM' 74T #FABTD-SUM-OK-BPS1 ( EM = ZZ,ZZ9 ) 94T #FABTD-SUM-FAIL-BPS1 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps1, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(70),"__________",new TabSetting(90),"__________",new TabSetting(110),                   //Natural: WRITE ( 2 ) // 70T '__________' 90T '__________' 110T '__________' /
            "__________",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wpid_Ati_Ok_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Ati_Ok_Grand_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps1)); //Natural: COMPUTE #WPID-ATI-OK-GRAND-TOTAL = #FACCW-SUM-OK-BPS1 + #FACTD-SUM-OK-BPS1 + #FACCK-SUM-OK-BPS1 + #FAGCW-SUM-OK-BPS1 + #FAGTD-SUM-OK-BPS1 + #FAGCK-SUM-OK-BPS1 + #FAKCW-SUM-OK-BPS1 + #FASCW-SUM-OK-BPS1 + #FABCW-SUM-OK-BPS1 + #FAKCK-SUM-OK-BPS1 + #FASCK-SUM-OK-BPS1 + #FABCK-SUM-OK-BPS1 + #FAKTD-SUM-OK-BPS1 + #FASTD-SUM-OK-BPS1 + #FABTD-SUM-OK-BPS1
        pnd_Wpid_Ati_Fail_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Ati_Fail_Grand_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps1).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps1)); //Natural: COMPUTE #WPID-ATI-FAIL-GRAND-TOTAL = #FACCW-SUM-FAIL-BPS1 + #FACTD-SUM-FAIL-BPS1 + #FACCK-SUM-FAIL-BPS1 + #FAGCW-SUM-FAIL-BPS1 + #FAGTD-SUM-FAIL-BPS1 + #FAGCK-SUM-FAIL-BPS1 + #FAKCW-SUM-FAIL-BPS1 + #FASCW-SUM-FAIL-BPS1 + #FABCW-SUM-FAIL-BPS1 + #FAKCK-SUM-FAIL-BPS1 + #FASCK-SUM-FAIL-BPS1 + #FABCK-SUM-FAIL-BPS1 + #FAKTD-SUM-FAIL-BPS1 + #FASTD-SUM-FAIL-BPS1 + #FABTD-SUM-FAIL-BPS1
        pnd_Wpid_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Grand_Total), pnd_Wpid_Ati_Ok_Grand_Total.add(pnd_Wpid_Ati_Fail_Grand_Total));                 //Natural: COMPUTE #WPID-GRAND-TOTAL = #WPID-ATI-OK-GRAND-TOTAL + #WPID-ATI-FAIL-GRAND-TOTAL
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(10),"TOTALS  ",new TabSetting(71),pnd_Wpid_Ati_Ok_Grand_Total, new ReportEditMask       //Natural: WRITE ( 2 ) // 10T 'TOTALS  ' 71T #WPID-ATI-OK-GRAND-TOTAL ( EM = Z,ZZZ,ZZ9 ) 91T #WPID-ATI-FAIL-GRAND-TOTAL ( EM = Z,ZZZ,ZZ9 ) 111T #WPID-GRAND-TOTAL
            ("Z,ZZZ,ZZ9"),new TabSetting(91),pnd_Wpid_Ati_Fail_Grand_Total, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(111),pnd_Wpid_Grand_Total);
        if (Global.isEscape()) return;
        //*  BPSSS2 SUMMARY REPORT
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),"UNIT = ",new TabSetting(10),"BPSSS2 ",NEWLINE,new TabSetting(10),"------",NEWLINE,NEWLINE,NEWLINE,new  //Natural: WRITE ( 2 ) / 2T 'UNIT = ' 10T 'BPSSS2 ' / 10T '------' /// 10T 'WPID' 77T 'ATI' 96T 'SSSS' 115T 'TOTAL' / 10T '____' 77T '___' 96T '____' 115T '_____' /
            TabSetting(10),"WPID",new TabSetting(77),"ATI",new TabSetting(96),"SSSS",new TabSetting(115),"TOTAL",NEWLINE,new TabSetting(10),"____",new TabSetting(77),"___",new 
            TabSetting(96),"____",new TabSetting(115),"_____",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACCW-SUM-OK-BPS2 + #FACCW-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACCW -  REQUEST FOR CREF CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FACCW -  REQUEST FOR CREF CASH WITHDRAWAL FORM' 74T #FACCW-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FACCW-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACTD-SUM-OK-BPS2 + #FACTD-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACTD -  REQUEST FOR CREF DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FACTD -  REQUEST FOR CREF DIRECT TRANSFER FORM' 74T #FACTD-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FACTD-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACCK-SUM-OK-BPS2 + #FACCK-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACCK -  REQUEST FOR CREF SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FACCK -  REQUEST FOR CREF SWAT CASH WITHDRAWAL FORM' 74T #FACCK-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FACCK-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGCW-SUM-OK-BPS2 + #FAGCW-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGCW -  REQUEST FOR GRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FAGCW -  REQUEST FOR GRA WITHDRAWAL FORM' 74T #FAGCW-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FAGCW-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGTD-SUM-OK-BPS2 + #FAGTD-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGTD -  REQUEST FOR GRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FAGTD -  REQUEST FOR GRA DIRECT TRANSFER FORM' 74T #FAGTD-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FAGTD-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGCK-SUM-OK-BPS2 + #FAGCK-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGCK -  REQUEST FOR GRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FAGCK -  REQUEST FOR GRA SWAT CASH WITHDRAWAL FORM' 74T #FAGCK-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FAGCK-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKCW-SUM-OK-BPS2 + #FAKCW-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKCW -  REQUEST FOR IRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FAKCW -  REQUEST FOR IRA WITHDRAWAL FORM' 74T #FAKCW-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FAKCW-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASCW-SUM-OK-BPS2 + #FASCW-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASCW -  REQUEST FOR SRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FASCW -  REQUEST FOR SRA WITHDRAWAL FORM' 74T #FASCW-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FASCW-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABCW-SUM-OK-BPS2 + #FABCW-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABCW -  REQUEST FOR GSRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FABCW -  REQUEST FOR GSRA WITHDRAWAL FORM' 74T #FABCW-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FABCW-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKCK-SUM-OK-BPS2 + #FAKCK-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKCK -  REQUEST FOR IRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FAKCK -  REQUEST FOR IRA SWAT CASH WITHDRAWAL FORM' 74T #FAKCK-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FAKCK-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASCK-SUM-OK-BPS2 + #FASCK-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASCK -  REQUEST FOR SRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FASCK -  REQUEST FOR SRA SWAT CASH WITHDRAWAL FORM' 74T #FASCK-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FASCK-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABCK-SUM-OK-BPS2 + #FABCK-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABCK -  REQUEST FOR GSRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FABCK -  REQUEST FOR GSRA SWAT CASH WITHDRAWAL FORM' 74T #FABCK-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FABCK-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKTD-SUM-OK-BPS2 + #FAKTD-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKTD -  REQUEST FOR IRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FAKTD -  REQUEST FOR IRA DIRECT TRANSFER FORM' 74T #FAKTD-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FAKTD-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASTD-SUM-OK-BPS2 + #FASTD-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASTD -  REQUEST FOR SRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FASTD -  REQUEST FOR SRA DIRECT TRANSFER FORM' 74T #FASTD-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FASTD-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABTD-SUM-OK-BPS2 + #FABTD-SUM-FAIL-BPS2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABTD -  REQUEST FOR GSRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps2,  //Natural: WRITE ( 2 ) / 10T 'FABTD -  REQUEST FOR GSRA DIRECT TRANSFER FORM' 74T #FABTD-SUM-OK-BPS2 ( EM = ZZ,ZZ9 ) 94T #FABTD-SUM-FAIL-BPS2 ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps2, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(70),"__________",new TabSetting(90),"__________",new TabSetting(110),                   //Natural: WRITE ( 2 ) // 70T '__________' 90T '__________' 110T '__________' /
            "__________",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wpid_Ati_Ok_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Ati_Ok_Grand_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps2)); //Natural: COMPUTE #WPID-ATI-OK-GRAND-TOTAL = #FACCW-SUM-OK-BPS2 + #FACTD-SUM-OK-BPS2 + #FACCK-SUM-OK-BPS2 + #FAGCW-SUM-OK-BPS2 + #FAGTD-SUM-OK-BPS2 + #FAGCK-SUM-OK-BPS2 + #FAKCW-SUM-OK-BPS2 + #FASCW-SUM-OK-BPS2 + #FABCW-SUM-OK-BPS2 + #FAKCK-SUM-OK-BPS2 + #FASCK-SUM-OK-BPS2 + #FABCK-SUM-OK-BPS2 + #FAKTD-SUM-OK-BPS2 + #FASTD-SUM-OK-BPS2 + #FABTD-SUM-OK-BPS2
        pnd_Wpid_Ati_Fail_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Ati_Fail_Grand_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps2.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps2)); //Natural: COMPUTE #WPID-ATI-FAIL-GRAND-TOTAL = #FACCW-SUM-FAIL-BPS2 + #FACTD-SUM-FAIL-BPS2 + #FACCK-SUM-FAIL-BPS2 + #FAGCW-SUM-FAIL-BPS2 + #FAGTD-SUM-FAIL-BPS2 + #FAGCK-SUM-FAIL-BPS2 + #FAKCW-SUM-FAIL-BPS2 + #FASCW-SUM-FAIL-BPS2 + #FABCW-SUM-FAIL-BPS2 + #FAKCK-SUM-FAIL-BPS2 + #FASCK-SUM-FAIL-BPS2 + #FABCK-SUM-FAIL-BPS2 + #FAKTD-SUM-FAIL-BPS2 + #FASTD-SUM-FAIL-BPS2 + #FABTD-SUM-FAIL-BPS2
        pnd_Wpid_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Grand_Total), pnd_Wpid_Ati_Ok_Grand_Total.add(pnd_Wpid_Ati_Fail_Grand_Total));                 //Natural: COMPUTE #WPID-GRAND-TOTAL = #WPID-ATI-OK-GRAND-TOTAL + #WPID-ATI-FAIL-GRAND-TOTAL
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(10),"TOTALS  ",new TabSetting(71),pnd_Wpid_Ati_Ok_Grand_Total, new ReportEditMask       //Natural: WRITE ( 2 ) // 10T 'TOTALS  ' 71T #WPID-ATI-OK-GRAND-TOTAL ( EM = Z,ZZZ,ZZ9 ) 91T #WPID-ATI-FAIL-GRAND-TOTAL ( EM = Z,ZZZ,ZZ9 ) 111T #WPID-GRAND-TOTAL
            ("Z,ZZZ,ZZ9"),new TabSetting(91),pnd_Wpid_Ati_Fail_Grand_Total, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(111),pnd_Wpid_Grand_Total);
        if (Global.isEscape()) return;
        //*  BPSSSW SUMMARY REPORT
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),"UNIT = ",new TabSetting(10),"BPSSSW ",NEWLINE,new TabSetting(10),"------",NEWLINE,NEWLINE,NEWLINE,new  //Natural: WRITE ( 2 ) / 2T 'UNIT = ' 10T 'BPSSSW ' / 10T '------' /// 10T 'WPID' 77T 'ATI' 96T 'SSSS' 115T 'TOTAL' / 10T '____' 77T '___' 96T '____' 115T '_____' /
            TabSetting(10),"WPID",new TabSetting(77),"ATI",new TabSetting(96),"SSSS",new TabSetting(115),"TOTAL",NEWLINE,new TabSetting(10),"____",new TabSetting(77),"___",new 
            TabSetting(96),"____",new TabSetting(115),"_____",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACCW-SUM-OK-BPSW + #FACCW-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACCW -  REQUEST FOR CREF CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FACCW -  REQUEST FOR CREF CASH WITHDRAWAL FORM' 74T #FACCW-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FACCW-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACTD-SUM-OK-BPSW + #FACTD-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACTD -  REQUEST FOR CREF DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FACTD -  REQUEST FOR CREF DIRECT TRANSFER FORM' 74T #FACTD-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FACTD-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACCK-SUM-OK-BPSW + #FACCK-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACCK -  REQUEST FOR CREF SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FACCK -  REQUEST FOR CREF SWAT CASH WITHDRAWAL FORM' 74T #FACCK-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FACCK-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGCW-SUM-OK-BPSW + #FAGCW-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGCW -  REQUEST FOR GRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FAGCW -  REQUEST FOR GRA WITHDRAWAL FORM' 74T #FAGCW-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FAGCW-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGTD-SUM-OK-BPSW + #FAGTD-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGTD -  REQUEST FOR GRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FAGTD -  REQUEST FOR GRA DIRECT TRANSFER FORM' 74T #FAGTD-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FAGTD-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGCK-SUM-OK-BPSW + #FAGCK-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGCK -  REQUEST FOR GRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FAGCK -  REQUEST FOR GRA SWAT CASH WITHDRAWAL FORM' 74T #FAGCK-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FAGCK-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKCW-SUM-OK-BPSW + #FAKCW-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKCW -  REQUEST FOR IRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FAKCW -  REQUEST FOR IRA WITHDRAWAL FORM' 74T #FAKCW-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FAKCW-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASCW-SUM-OK-BPSW + #FASCW-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASCW -  REQUEST FOR SRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FASCW -  REQUEST FOR SRA WITHDRAWAL FORM' 74T #FASCW-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FASCW-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABCW-SUM-OK-BPSW + #FABCW-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABCW -  REQUEST FOR GSRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FABCW -  REQUEST FOR GSRA WITHDRAWAL FORM' 74T #FABCW-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FABCW-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKCK-SUM-OK-BPSW + #FAKCK-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKCK -  REQUEST FOR IRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FAKCK -  REQUEST FOR IRA SWAT CASH WITHDRAWAL FORM' 74T #FAKCK-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FAKCK-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASCK-SUM-OK-BPSW + #FASCK-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASCK -  REQUEST FOR SRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FASCK -  REQUEST FOR SRA SWAT CASH WITHDRAWAL FORM' 74T #FASCK-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FASCK-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABCK-SUM-OK-BPSW + #FABCK-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABCK -  REQUEST FOR GSRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FABCK -  REQUEST FOR GSRA SWAT CASH WITHDRAWAL FORM' 74T #FABCK-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FABCK-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKTD-SUM-OK-BPSW + #FAKTD-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKTD -  REQUEST FOR IRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FAKTD -  REQUEST FOR IRA DIRECT TRANSFER FORM' 74T #FAKTD-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FAKTD-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASTD-SUM-OK-BPSW + #FASTD-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASTD -  REQUEST FOR SRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FASTD -  REQUEST FOR SRA DIRECT TRANSFER FORM' 74T #FASTD-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FASTD-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABTD-SUM-OK-BPSW + #FABTD-SUM-FAIL-BPSW
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABTD -  REQUEST FOR GSRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bpsw,  //Natural: WRITE ( 2 ) / 10T 'FABTD -  REQUEST FOR GSRA DIRECT TRANSFER FORM' 74T #FABTD-SUM-OK-BPSW ( EM = ZZ,ZZ9 ) 94T #FABTD-SUM-FAIL-BPSW ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bpsw, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(70),"__________",new TabSetting(90),"__________",new TabSetting(110),                   //Natural: WRITE ( 2 ) // 70T '__________' 90T '__________' 110T '__________' /
            "__________",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wpid_Ati_Ok_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Ati_Ok_Grand_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bpsw)); //Natural: COMPUTE #WPID-ATI-OK-GRAND-TOTAL = #FACCW-SUM-OK-BPSW + #FACTD-SUM-OK-BPSW + #FACCK-SUM-OK-BPSW + #FAGCW-SUM-OK-BPSW + #FAGTD-SUM-OK-BPSW + #FAGCK-SUM-OK-BPSW + #FAKCW-SUM-OK-BPSW + #FASCW-SUM-OK-BPSW + #FABCW-SUM-OK-BPSW + #FAKCK-SUM-OK-BPSW + #FASCK-SUM-OK-BPSW + #FABCK-SUM-OK-BPSW + #FAKTD-SUM-OK-BPSW + #FASTD-SUM-OK-BPSW + #FABTD-SUM-OK-BPSW
        pnd_Wpid_Ati_Fail_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Ati_Fail_Grand_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bpsw.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bpsw)); //Natural: COMPUTE #WPID-ATI-FAIL-GRAND-TOTAL = #FACCW-SUM-FAIL-BPSW + #FACTD-SUM-FAIL-BPSW + #FACCK-SUM-FAIL-BPSW + #FAGCW-SUM-FAIL-BPSW + #FAGTD-SUM-FAIL-BPSW + #FAGCK-SUM-FAIL-BPSW + #FAKCW-SUM-FAIL-BPSW + #FASCW-SUM-FAIL-BPSW + #FABCW-SUM-FAIL-BPSW + #FAKCK-SUM-FAIL-BPSW + #FASCK-SUM-FAIL-BPSW + #FABCK-SUM-FAIL-BPSW + #FAKTD-SUM-FAIL-BPSW + #FASTD-SUM-FAIL-BPSW + #FABTD-SUM-FAIL-BPSW
        pnd_Wpid_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Grand_Total), pnd_Wpid_Ati_Ok_Grand_Total.add(pnd_Wpid_Ati_Fail_Grand_Total));                 //Natural: COMPUTE #WPID-GRAND-TOTAL = #WPID-ATI-OK-GRAND-TOTAL + #WPID-ATI-FAIL-GRAND-TOTAL
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(10),"TOTALS",new TabSetting(71),pnd_Wpid_Ati_Ok_Grand_Total, new ReportEditMask         //Natural: WRITE ( 2 ) // 10T 'TOTALS' 71T #WPID-ATI-OK-GRAND-TOTAL ( EM = Z,ZZZ,ZZ9 ) 91T #WPID-ATI-FAIL-GRAND-TOTAL ( EM = Z,ZZZ,ZZ9 ) 111T #WPID-GRAND-TOTAL
            ("Z,ZZZ,ZZ9"),new TabSetting(91),pnd_Wpid_Ati_Fail_Grand_Total, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(111),pnd_Wpid_Grand_Total);
        if (Global.isEscape()) return;
        //*  MISC UNITS SUMMARY REPORT (NE BPSSS1,BPSSS2, OR BPSSSW)
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        //*  * THESE COMPUTES ARE DONE FIRST TO DETERMINE IF THIS *
        //*  * MISC. SUMMARY REPORT EVEN NEEDS TO BE GENERATED.   *
        //*  *                                                    *
        //*  * IF #WPID-GRAND-TOTAL GREATER EQUAL 0               *
        //*  * DO NOT GENERATE MISC. SUMMARY REPORT.              *
        //*  *                                                    *
        pnd_Wpid_Ati_Ok_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Ati_Ok_Grand_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Misc)); //Natural: COMPUTE #WPID-ATI-OK-GRAND-TOTAL = #FACCW-SUM-OK-MISC + #FACTD-SUM-OK-MISC + #FACCK-SUM-OK-MISC + #FAGCW-SUM-OK-MISC + #FAGTD-SUM-OK-MISC + #FAGCK-SUM-OK-MISC + #FAKCW-SUM-OK-MISC + #FASCW-SUM-OK-MISC + #FABCW-SUM-OK-MISC + #FAKCK-SUM-OK-MISC + #FASCK-SUM-OK-MISC + #FABCK-SUM-OK-MISC + #FAKTD-SUM-OK-MISC + #FASTD-SUM-OK-MISC + #FABTD-SUM-OK-MISC
        pnd_Wpid_Ati_Fail_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Ati_Fail_Grand_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Misc.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Misc).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-ATI-FAIL-GRAND-TOTAL = #FACCW-SUM-FAIL-MISC + #FACTD-SUM-FAIL-MISC + #FACCK-SUM-FAIL-MISC + #FAGCW-SUM-FAIL-MISC + #FAGTD-SUM-FAIL-MISC + #FAGCK-SUM-FAIL-MISC + #FAKCW-SUM-FAIL-MISC + #FASCW-SUM-FAIL-MISC + #FABCW-SUM-FAIL-MISC + #FAKCK-SUM-FAIL-MISC + #FASCK-SUM-FAIL-MISC + #FABCK-SUM-FAIL-MISC + #FAKTD-SUM-FAIL-MISC + #FASTD-SUM-FAIL-MISC + #FABTD-SUM-FAIL-MISC
        pnd_Wpid_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Grand_Total), pnd_Wpid_Ati_Ok_Grand_Total.add(pnd_Wpid_Ati_Fail_Grand_Total));                 //Natural: COMPUTE #WPID-GRAND-TOTAL = #WPID-ATI-OK-GRAND-TOTAL + #WPID-ATI-FAIL-GRAND-TOTAL
        //*  NO MISC.UNIT'S SUMMARY REPORT TO PRINT.
        if (condition(pnd_Wpid_Grand_Total.equals(getZero())))                                                                                                            //Natural: IF #WPID-GRAND-TOTAL = 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"ADDITIONAL UNITS (OTHER THAN BPSSS1 OR BPSSS2 OR BPSSSW)",NEWLINE,new                  //Natural: WRITE ( 2 ) / 10T 'ADDITIONAL UNITS (OTHER THAN BPSSS1 OR BPSSS2 OR BPSSSW)' / 10T '--------------------------------------------------------' /// 10T 'WPID' 77T 'ATI' 96T 'SSSS' 115T 'TOTAL' / 10T '____' 77T '___' 96T '____' 115T '_____' /
                TabSetting(10),"--------------------------------------------------------",NEWLINE,NEWLINE,NEWLINE,new TabSetting(10),"WPID",new TabSetting(77),"ATI",new 
                TabSetting(96),"SSSS",new TabSetting(115),"TOTAL",NEWLINE,new TabSetting(10),"____",new TabSetting(77),"___",new TabSetting(96),"____",new 
                TabSetting(115),"_____",NEWLINE);
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACCW-SUM-OK-MISC + #FACCW-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACCW -  REQUEST FOR CREF CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FACCW -  REQUEST FOR CREF CASH WITHDRAWAL FORM' 74T #FACCW-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FACCW-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACTD-SUM-OK-MISC + #FACTD-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACTD -  REQUEST FOR CREF DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FACTD -  REQUEST FOR CREF DIRECT TRANSFER FORM' 74T #FACTD-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FACTD-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACCK-SUM-OK-MISC + #FACCK-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACCK -  REQUEST FOR CREF SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FACCK -  REQUEST FOR CREF SWAT CASH WITHDRAWAL FORM' 74T #FACCK-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FACCK-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGCW-SUM-OK-MISC + #FAGCW-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGCW -  REQUEST FOR GRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FAGCW -  REQUEST FOR GRA WITHDRAWAL FORM' 74T #FAGCW-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FAGCW-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGTD-SUM-OK-MISC + #FAGTD-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGTD -  REQUEST FOR GRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FAGTD -  REQUEST FOR GRA DIRECT TRANSFER FORM' 74T #FAGTD-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FAGTD-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGCK-SUM-OK-MISC + #FAGCK-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGCK -  REQUEST FOR GRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FAGCK -  REQUEST FOR GRA SWAT CASH WITHDRAWAL FORM' 74T #FAGCK-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FAGCK-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKCW-SUM-OK-MISC + #FAKCW-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKCW -  REQUEST FOR IRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FAKCW -  REQUEST FOR IRA WITHDRAWAL FORM' 74T #FAKCW-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FAKCW-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASCW-SUM-OK-MISC + #FASCW-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASCW -  REQUEST FOR SRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FASCW -  REQUEST FOR SRA WITHDRAWAL FORM' 74T #FASCW-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FASCW-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABCW-SUM-OK-MISC + #FABCW-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABCW -  REQUEST FOR GSRA WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FABCW -  REQUEST FOR GSRA WITHDRAWAL FORM' 74T #FABCW-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FABCW-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKCK-SUM-OK-MISC + #FAKCK-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKCK -  REQUEST FOR IRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FAKCK -  REQUEST FOR IRA SWAT CASH WITHDRAWAL FORM' 74T #FAKCK-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FAKCK-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASCK-SUM-OK-MISC + #FASCK-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASCK -  REQUEST FOR SRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FASCK -  REQUEST FOR SRA SWAT CASH WITHDRAWAL FORM' 74T #FASCK-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FASCK-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABCK-SUM-OK-MISC + #FABCK-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABCK -  REQUEST FOR GSRA SWAT CASH WITHDRAWAL FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FABCK -  REQUEST FOR GSRA SWAT CASH WITHDRAWAL FORM' 74T #FABCK-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FABCK-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKTD-SUM-OK-MISC + #FAKTD-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKTD -  REQUEST FOR IRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FAKTD -  REQUEST FOR IRA DIRECT TRANSFER FORM' 74T #FAKTD-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FAKTD-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASTD-SUM-OK-MISC + #FASTD-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASTD -  REQUEST FOR SRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FASTD -  REQUEST FOR SRA DIRECT TRANSFER FORM' 74T #FASTD-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FASTD-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Misc.add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Misc)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABTD-SUM-OK-MISC + #FABTD-SUM-FAIL-MISC
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABTD -  REQUEST FOR GSRA DIRECT TRANSFER FORM",new TabSetting(74),pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Misc,  //Natural: WRITE ( 2 ) / 10T 'FABTD -  REQUEST FOR GSRA DIRECT TRANSFER FORM' 74T #FABTD-SUM-OK-MISC ( EM = ZZ,ZZ9 ) 94T #FABTD-SUM-FAIL-MISC ( EM = ZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(94),pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Misc, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(70),"__________",new TabSetting(90),"__________",new TabSetting(110),               //Natural: WRITE ( 2 ) // 70T '__________' 90T '__________' 110T '__________' /
                "__________",NEWLINE);
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(10),"TOTALS",new TabSetting(71),pnd_Wpid_Ati_Ok_Grand_Total, new                    //Natural: WRITE ( 2 ) // 10T 'TOTALS' 71T #WPID-ATI-OK-GRAND-TOTAL ( EM = Z,ZZZ,ZZ9 ) 91T #WPID-ATI-FAIL-GRAND-TOTAL ( EM = Z,ZZZ,ZZ9 ) 111T #WPID-GRAND-TOTAL
                ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(91),pnd_Wpid_Ati_Fail_Grand_Total, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(111),pnd_Wpid_Grand_Total);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  GRAND GRAND TOTAL REPORT *
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"GRAND TOTAL (ALL UNITS) SUMMARY REPORT FOR SSSS FORMS :",NEWLINE,new TabSetting(10),"-------------------------------------------------------",NEWLINE,NEWLINE,NEWLINE,new  //Natural: WRITE ( 2 ) / 10T 'GRAND TOTAL (ALL UNITS) SUMMARY REPORT FOR SSSS FORMS :' / 10T '-------------------------------------------------------' /// 10T 'WPID' 77T 'ATI' 96T 'SSSS' 115T 'TOTAL' / 10T '____' 77T '___' 96T '____' 115T '_____' /
            TabSetting(10),"WPID",new TabSetting(77),"ATI",new TabSetting(96),"SSSS",new TabSetting(115),"TOTAL",NEWLINE,new TabSetting(10),"____",new TabSetting(77),"___",new 
            TabSetting(96),"____",new TabSetting(115),"_____",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Faccw_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Faccw_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Misc)); //Natural: COMPUTE #FACCW-OK-GRAND = #FACCW-SUM-OK-BPS1 + #FACCW-SUM-OK-BPS2 + #FACCW-SUM-OK-BPSW + #FACCW-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Faccw_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Faccw_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Misc)); //Natural: COMPUTE #FACCW-FAIL-GRAND = #FACCW-SUM-FAIL-BPS1 + #FACCW-SUM-FAIL-BPS2 + #FACCW-SUM-FAIL-BPSW + #FACCW-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Faccw_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACCW-OK-GRAND + #FACCW-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACCW -  REQUEST FOR CREF CASH WITHDRAWAL FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Faccw_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FACCW -  REQUEST FOR CREF CASH WITHDRAWAL FORM' 73T #FACCW-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FACCW-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Faccw_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Factd_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Factd_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Misc)); //Natural: COMPUTE #FACTD-OK-GRAND = #FACTD-SUM-OK-BPS1 + #FACTD-SUM-OK-BPS2 + #FACTD-SUM-OK-BPSW + #FACTD-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Factd_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Factd_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Misc)); //Natural: COMPUTE #FACTD-FAIL-GRAND = #FACTD-SUM-FAIL-BPS1 + #FACTD-SUM-FAIL-BPS2 + #FACTD-SUM-FAIL-BPSW + #FACTD-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Factd_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Factd_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACTD-OK-GRAND + #FACTD-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACTD -  REQUEST FOR CREF DIRECT TRANSFER FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Factd_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FACTD -  REQUEST FOR CREF DIRECT TRANSFER FORM' 73T #FACTD-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FACTD-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Factd_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Facck_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Facck_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Misc)); //Natural: COMPUTE #FACCK-OK-GRAND = #FACCK-SUM-OK-BPS1 + #FACCK-SUM-OK-BPS2 + #FACCK-SUM-OK-BPSW + #FACCK-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Facck_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Facck_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Misc)); //Natural: COMPUTE #FACCK-FAIL-GRAND = #FACCK-SUM-FAIL-BPS1 + #FACCK-SUM-FAIL-BPS2 + #FACCK-SUM-FAIL-BPSW + #FACCK-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Facck_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Facck_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FACCK-OK-GRAND + #FACCK-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FACCK -  REQUEST FOR CREF SWAT CASH WITHDRAWAL FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Facck_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FACCK -  REQUEST FOR CREF SWAT CASH WITHDRAWAL FORM' 73T #FACCK-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FACCK-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Facck_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fagcw_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Misc)); //Natural: COMPUTE #FAGCW-OK-GRAND = #FAGCW-SUM-OK-BPS1 + #FAGCW-SUM-OK-BPS2 + #FAGCW-SUM-OK-BPSW + #FAGCW-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Fagcw_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fagcw_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Misc)); //Natural: COMPUTE #FAGCW-FAIL-GRAND = #FAGCW-SUM-FAIL-BPS1 + #FAGCW-SUM-FAIL-BPS2 + #FAGCW-SUM-FAIL-BPSW + #FAGCW-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagcw_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGCW-OK-GRAND + #FAGCW-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGCW -  REQUEST FOR GRA WITHDRAWAL FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Fagcw_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FAGCW -  REQUEST FOR GRA WITHDRAWAL FORM' 73T #FAGCW-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FAGCW-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Fagcw_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fagtd_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Misc)); //Natural: COMPUTE #FAGTD-OK-GRAND = #FAGTD-SUM-OK-BPS1 + #FAGTD-SUM-OK-BPS2 + #FAGTD-SUM-OK-BPSW + #FAGTD-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Fagtd_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fagtd_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Misc)); //Natural: COMPUTE #FAGTD-FAIL-GRAND = #FAGTD-SUM-FAIL-BPS1 + #FAGTD-SUM-FAIL-BPS2 + #FAGTD-SUM-FAIL-BPSW + #FAGTD-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagtd_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGTD-OK-GRAND + #FAGTD-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGTD -  REQUEST FOR GRA DIRECT TRANSFER FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Fagtd_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FAGTD -  REQUEST FOR GRA DIRECT TRANSFER FORM' 73T #FAGTD-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FAGTD-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Fagtd_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Fagck_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fagck_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Misc)); //Natural: COMPUTE #FAGCK-OK-GRAND = #FAGCK-SUM-OK-BPS1 + #FAGCK-SUM-OK-BPS2 + #FAGCK-SUM-OK-BPSW + #FAGCK-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Fagck_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fagck_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Misc)); //Natural: COMPUTE #FAGCK-FAIL-GRAND = #FAGCK-SUM-FAIL-BPS1 + #FAGCK-SUM-FAIL-BPS2 + #FAGCK-SUM-FAIL-BPSW + #FAGCK-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fagck_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Fagck_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAGCK-OK-GRAND + #FAGCK-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAGCK -  REQUEST FOR GRA SWAT CASH WITHDRAWAL FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Fagck_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FAGCK -  REQUEST FOR GRA SWAT CASH WITHDRAWAL FORM' 73T #FAGCK-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FAGCK-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Fagck_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fakcw_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Misc)); //Natural: COMPUTE #FAKCW-OK-GRAND = #FAKCW-SUM-OK-BPS1 + #FAKCW-SUM-OK-BPS2 + #FAKCW-SUM-OK-BPSW + #FAKCW-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Fakcw_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fakcw_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Misc)); //Natural: COMPUTE #FAKCW-FAIL-GRAND = #FAKCW-SUM-FAIL-BPS1 + #FAKCW-SUM-FAIL-BPS2 + #FAKCW-SUM-FAIL-BPSW + #FAKCW-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fakcw_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKCW-OK-GRAND + #FAKCW-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKCW -  REQUEST FOR IRA WITHDRAWAL FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Fakcw_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FAKCW -  REQUEST FOR IRA WITHDRAWAL FORM' 73T #FAKCW-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FAKCW-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Fakcw_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Fascw_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fascw_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Misc)); //Natural: COMPUTE #FASCW-OK-GRAND = #FASCW-SUM-OK-BPS1 + #FASCW-SUM-OK-BPS2 + #FASCW-SUM-OK-BPSW + #FASCW-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Fascw_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fascw_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Misc)); //Natural: COMPUTE #FASCW-FAIL-GRAND = #FASCW-SUM-FAIL-BPS1 + #FASCW-SUM-FAIL-BPS2 + #FASCW-SUM-FAIL-BPSW + #FASCW-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fascw_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Fascw_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASCW-OK-GRAND + #FASCW-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASCW -  REQUEST FOR SRA WITHDRAWAL FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Fascw_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FASCW -  REQUEST FOR SRA WITHDRAWAL FORM' 73T #FASCW-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FASCW-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Fascw_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fabcw_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Misc)); //Natural: COMPUTE #FABCW-OK-GRAND = #FABCW-SUM-OK-BPS1 + #FABCW-SUM-OK-BPS2 + #FABCW-SUM-OK-BPSW + #FABCW-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Fabcw_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fabcw_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Misc)); //Natural: COMPUTE #FABCW-FAIL-GRAND = #FABCW-SUM-FAIL-BPS1 + #FABCW-SUM-FAIL-BPS2 + #FABCW-SUM-FAIL-BPSW + #FABCW-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabcw_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABCW-OK-GRAND + #FABCW-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABCW -  REQUEST FOR GSRA WITHDRAWAL FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Fabcw_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FABCW -  REQUEST FOR GSRA WITHDRAWAL FORM' 73T #FABCW-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FABCW-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Fabcw_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Fakck_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fakck_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Misc)); //Natural: COMPUTE #FAKCK-OK-GRAND = #FAKCK-SUM-OK-BPS1 + #FAKCK-SUM-OK-BPS2 + #FAKCK-SUM-OK-BPSW + #FAKCK-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Fakck_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fakck_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Misc)); //Natural: COMPUTE #FAKCK-FAIL-GRAND = #FAKCK-SUM-FAIL-BPS1 + #FAKCK-SUM-FAIL-BPS2 + #FAKCK-SUM-FAIL-BPSW + #FAKCK-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fakck_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Fakck_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKCK-OK-GRAND + #FAKCK-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKCK -  REQUEST FOR IRA SWAT CASH WITHDRAWAL FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Fakck_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FAKCK -  REQUEST FOR IRA SWAT CASH WITHDRAWAL FORM' 73T #FAKCK-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FAKCK-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Fakck_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Fasck_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fasck_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Misc)); //Natural: COMPUTE #FASCK-OK-GRAND = #FASCK-SUM-OK-BPS1 + #FASCK-SUM-OK-BPS2 + #FASCK-SUM-OK-BPSW + #FASCK-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Fasck_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fasck_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Misc)); //Natural: COMPUTE #FASCK-FAIL-GRAND = #FASCK-SUM-FAIL-BPS1 + #FASCK-SUM-FAIL-BPS2 + #FASCK-SUM-FAIL-BPSW + #FASCK-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fasck_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Fasck_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASCK-OK-GRAND + #FASCK-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASCK -  REQUEST FOR SRA SWAT CASH WITHDRAWAL FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Fasck_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FASCK -  REQUEST FOR SRA SWAT CASH WITHDRAWAL FORM' 73T #FASCK-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FASCK-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Fasck_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Fabck_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fabck_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Misc)); //Natural: COMPUTE #FABCK-OK-GRAND = #FABCK-SUM-OK-BPS1 + #FABCK-SUM-OK-BPS2 + #FABCK-SUM-OK-BPSW + #FABCK-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Fabck_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fabck_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Misc)); //Natural: COMPUTE #FABCK-FAIL-GRAND = #FABCK-SUM-FAIL-BPS1 + #FABCK-SUM-FAIL-BPS2 + #FABCK-SUM-FAIL-BPSW + #FABCK-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabck_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Fabck_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABCK-OK-GRAND + #FABCK-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABCK -  REQUEST FOR GSRA SWAT CASH WITHDRAWAL FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Fabck_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FABCK -  REQUEST FOR GSRA SWAT CASH WITHDRAWAL FORM' 73T #FABCK-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FABCK-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Fabck_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Faktd_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Faktd_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Misc)); //Natural: COMPUTE #FAKTD-OK-GRAND = #FAKTD-SUM-OK-BPS1 + #FAKTD-SUM-OK-BPS2 + #FAKTD-SUM-OK-BPSW + #FAKTD-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Faktd_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Faktd_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Misc)); //Natural: COMPUTE #FAKTD-FAIL-GRAND = #FAKTD-SUM-FAIL-BPS1 + #FAKTD-SUM-FAIL-BPS2 + #FAKTD-SUM-FAIL-BPSW + #FAKTD-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Faktd_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Faktd_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FAKTD-OK-GRAND + #FAKTD-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FAKTD -  REQUEST FOR IRA DIRECT TRANSFER FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Faktd_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FAKTD -  REQUEST FOR IRA DIRECT TRANSFER FORM' 73T #FAKTD-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FAKTD-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Faktd_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Fastd_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fastd_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Misc)); //Natural: COMPUTE #FASTD-OK-GRAND = #FASTD-SUM-OK-BPS1 + #FASTD-SUM-OK-BPS2 + #FASTD-SUM-OK-BPSW + #FASTD-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Fastd_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fastd_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Misc)); //Natural: COMPUTE #FASTD-FAIL-GRAND = #FASTD-SUM-FAIL-BPS1 + #FASTD-SUM-FAIL-BPS2 + #FASTD-SUM-FAIL-BPSW + #FASTD-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fastd_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Fastd_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FASTD-OK-GRAND + #FASTD-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FASTD -  REQUEST FOR SRA DIRECT TRANSFER FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Fastd_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FASTD -  REQUEST FOR SRA DIRECT TRANSFER FORM' 73T #FASTD-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FASTD-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Fastd_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Ok_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fabtd_Ok_Grand), pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Misc)); //Natural: COMPUTE #FABTD-OK-GRAND = #FABTD-SUM-OK-BPS1 + #FABTD-SUM-OK-BPS2 + #FABTD-SUM-OK-BPSW + #FABTD-SUM-OK-MISC
        pnd_Wpid_Summary_Data_Pnd_Fabtd_Fail_Grand.compute(new ComputeParameters(false, pnd_Wpid_Summary_Data_Pnd_Fabtd_Fail_Grand), pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps1.add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps2).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bpsw).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Misc)); //Natural: COMPUTE #FABTD-FAIL-GRAND = #FABTD-SUM-FAIL-BPS1 + #FABTD-SUM-FAIL-BPS2 + #FABTD-SUM-FAIL-BPSW + #FABTD-SUM-FAIL-MISC
        pnd_Wpid_Sub_Total.compute(new ComputeParameters(false, pnd_Wpid_Sub_Total), pnd_Wpid_Summary_Data_Pnd_Fabtd_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Fail_Grand)); //Natural: COMPUTE #WPID-SUB-TOTAL = #FABTD-OK-GRAND + #FABTD-FAIL-GRAND
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"FABTD -  REQUEST FOR GSRA DIRECT TRANSFER FORM",new TabSetting(73),pnd_Wpid_Summary_Data_Pnd_Fabtd_Ok_Grand,  //Natural: WRITE ( 2 ) / 10T 'FABTD -  REQUEST FOR GSRA DIRECT TRANSFER FORM' 73T #FABTD-OK-GRAND ( EM = ZZZ,ZZ9 ) 93T #FABTD-FAIL-GRAND ( EM = ZZZ,ZZ9 ) 113T #WPID-SUB-TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(93),pnd_Wpid_Summary_Data_Pnd_Fabtd_Fail_Grand, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(113),pnd_Wpid_Sub_Total, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(70),"__________",new TabSetting(90),"__________",new TabSetting(110),                   //Natural: WRITE ( 2 ) // 70T '__________' 90T '__________' 110T '__________' /
            "__________",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wpid_Ati_Ok_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Ati_Ok_Grand_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Ok_Grand.add(pnd_Wpid_Summary_Data_Pnd_Factd_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Facck_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Ok_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Ok_Grand)); //Natural: COMPUTE #WPID-ATI-OK-GRAND-TOTAL = #FACCW-OK-GRAND + #FACTD-OK-GRAND + #FACCK-OK-GRAND + #FAGCW-OK-GRAND + #FAGTD-OK-GRAND + #FAGCK-OK-GRAND + #FAKCW-OK-GRAND + #FASCW-OK-GRAND + #FABCW-OK-GRAND + #FAKCK-OK-GRAND + #FASCK-OK-GRAND + #FABCK-OK-GRAND + #FAKTD-OK-GRAND + #FASTD-OK-GRAND + #FABTD-OK-GRAND
        pnd_Wpid_Ati_Fail_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Ati_Fail_Grand_Total), pnd_Wpid_Summary_Data_Pnd_Faccw_Fail_Grand.add(pnd_Wpid_Summary_Data_Pnd_Factd_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Facck_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fagcw_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fagtd_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fagck_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fakcw_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fascw_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fabcw_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fakck_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fasck_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fabck_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Faktd_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fastd_Fail_Grand).add(pnd_Wpid_Summary_Data_Pnd_Fabtd_Fail_Grand)); //Natural: COMPUTE #WPID-ATI-FAIL-GRAND-TOTAL = #FACCW-FAIL-GRAND + #FACTD-FAIL-GRAND + #FACCK-FAIL-GRAND + #FAGCW-FAIL-GRAND + #FAGTD-FAIL-GRAND + #FAGCK-FAIL-GRAND + #FAKCW-FAIL-GRAND + #FASCW-FAIL-GRAND + #FABCW-FAIL-GRAND + #FAKCK-FAIL-GRAND + #FASCK-FAIL-GRAND + #FABCK-FAIL-GRAND + #FAKTD-FAIL-GRAND + #FASTD-FAIL-GRAND + #FABTD-FAIL-GRAND
        pnd_Wpid_Grand_Total.compute(new ComputeParameters(false, pnd_Wpid_Grand_Total), pnd_Wpid_Ati_Ok_Grand_Total.add(pnd_Wpid_Ati_Fail_Grand_Total));                 //Natural: COMPUTE #WPID-GRAND-TOTAL = #WPID-ATI-OK-GRAND-TOTAL + #WPID-ATI-FAIL-GRAND-TOTAL
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"GRAND TOTALS :",new TabSetting(71),pnd_Wpid_Ati_Ok_Grand_Total, new ReportEditMask         //Natural: WRITE ( 2 ) / 10T 'GRAND TOTALS :' 71T #WPID-ATI-OK-GRAND-TOTAL ( EM = Z,ZZZ,ZZ9 ) 91T #WPID-ATI-FAIL-GRAND-TOTAL ( EM = Z,ZZZ,ZZ9 ) 111T #WPID-GRAND-TOTAL
            ("Z,ZZZ,ZZ9"),new TabSetting(91),pnd_Wpid_Ati_Fail_Grand_Total, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(111),pnd_Wpid_Grand_Total);
        if (Global.isEscape()) return;
        //*  *** END OF PROGRAM ***
    }
    //*  START ATI-WRITE-REC-1 PROCESSING
    private void sub_Ati_Write_Rec_1() throws Exception                                                                                                                   //Natural: ATI-WRITE-REC-1
    {
        if (BLNatReinput.isReinput()) return;

                                                                                                                                                                          //Natural: PERFORM SET-PROCESS-STATUS
        sub_Set_Process_Status();
        if (condition(Global.isEscape())) {return;}
        if (condition(getReports().getAstLinesLeft(1).less(15)))                                                                                                          //Natural: NEWPAGE ( 1 ) IF LESS THAN 15 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Compressed_Text.reset();                                                                                                                                      //Natural: RESET #COMPRESSED-TEXT
        //*  PIN-EXP
        //*   PIN-EXP>>
        pnd_Compressed_Text.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "ENTRY RACF-ID/UNIT= ", cwf_Ati_Fulfill_Ati_Entry_Racf_Id, "/", cwf_Ati_Fulfill_Ati_Entry_Unit_Cde)); //Natural: COMPRESS 'ENTRY RACF-ID/UNIT= ' CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID '/' CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE INTO #COMPRESSED-TEXT LEAVING NO SPACE
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),cwf_Ati_Fulfill_Ati_Pin,new TabSetting(15),cwf_Ati_Fulfill_Ati_Wpid,new TabSetting(22),cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme,  //Natural: WRITE ( 1 ) / 1T CWF-ATI-FULFILL.ATI-PIN 15T CWF-ATI-FULFILL.ATI-WPID 22T CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 43T CWF-ATI-FULFILL.ATI-STTS-DTE-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 63T #PROCESS-STATUS-LITERAL 73T CWF-ATI-FULFILL.SSSS-OVRNGHT-MAIL-IND 77T CWF-ATI-FULFILL.SSSS-CTZNSHP-CDE 82T CWF-ATI-FULFILL.SSSS-RSDNCE-CDE
            new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(43),cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new 
            TabSetting(63),pnd_Process_Status_Literal,new TabSetting(73),cwf_Ati_Fulfill_Ssss_Ovrnght_Mail_Ind,new TabSetting(77),cwf_Ati_Fulfill_Ssss_Ctznshp_Cde,new 
            TabSetting(82),cwf_Ati_Fulfill_Ssss_Rsdnce_Cde);
        if (Global.isEscape()) return;
        //*  87T CWF-ATI-FULFILL.SSSS-RSDNCE-CDE       /*  PIN-EXP<<
        //*  86T #COMPRESSED-TEXT
        if (condition(cwf_Ati_Fulfill_Ati_Prcss_Stts.equals(" ")))                                                                                                        //Natural: IF CWF-ATI-FULFILL.ATI-PRCSS-STTS = ' '
        {
            if (condition(cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))                                                                                                     //Natural: IF CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
            {
                //*  REV. WPID SMRY
                                                                                                                                                                          //Natural: PERFORM SUCCESSFUL-WPID-STATS
                sub_Successful_Wpid_Stats();
                if (condition(Global.isEscape())) {return;}
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text);                                                                       //Natural: WRITE ( 1 ) 15T #COMPRESSED-TEXT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  REV. WPID SMRY
                                                                                                                                                                          //Natural: PERFORM FAILED-WPID-STATS
                sub_Failed_Wpid_Stats();
                if (condition(Global.isEscape())) {return;}
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text,new TabSetting(55),"ERROR MESSAGE = :",new TabSetting(73),              //Natural: WRITE ( 1 ) 15T #COMPRESSED-TEXT 55T 'ERROR MESSAGE = :' 73T CWF-ATI-FULFILL.ATI-ERROR-TXT
                    cwf_Ati_Fulfill_Ati_Error_Txt);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text);                                                                           //Natural: WRITE ( 1 ) 15T #COMPRESSED-TEXT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*   END  ATI-WRITE-REC-1 PROCESSING
    }
    //*  START ATI-WRITE-REC-2 PROCESSING
    //*  (EM=MM/DD/YYYY)
    private void sub_Ati_Write_Rec_2() throws Exception                                                                                                                   //Natural: ATI-WRITE-REC-2
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,new TabSetting(15),"PLAN = ",new TabSetting(23),cwf_Ati_Fulfill_Ssss_Plan_Cde,new TabSetting(30),"CONTRACT NUMBER  = ",new  //Natural: WRITE ( 1 ) 15T 'PLAN = ' 23T CWF-ATI-FULFILL.SSSS-PLAN-CDE 30T 'CONTRACT NUMBER  = ' 51T CWF-ATI-FULFILL.SSSS-TIAA-NBR 88T CWF-ATI-FULFILL.SSSS-RLLVR-TYPE 93T CWF-ATI-FULFILL.SSSS-ERLY-TRM-TYPE 98T CWF-ATI-FULFILL.SSSS-WTH-ALL-FNDS-IND 101T CWF-ATI-FULFILL.SSSS-RPTTVE-FRQNCY ( EM = 999 ) 105T CWF-ATI-FULFILL.SSSS-RPTTVE-NBR-PYMNTS ( EM = 99999 ) 111T CWF-ATI-FULFILL.SSSS-RPTTVE-BGN-DTE 122T CWF-ATI-FULFILL.SSSS-RPTTVE-END-DTE
            TabSetting(51),cwf_Ati_Fulfill_Ssss_Tiaa_Nbr,new TabSetting(88),cwf_Ati_Fulfill_Ssss_Rllvr_Type,new TabSetting(93),cwf_Ati_Fulfill_Ssss_Erly_Trm_Type,new 
            TabSetting(98),cwf_Ati_Fulfill_Ssss_Wth_All_Fnds_Ind,new TabSetting(101),cwf_Ati_Fulfill_Ssss_Rpttve_Frqncy, new ReportEditMask ("999"),new 
            TabSetting(105),cwf_Ati_Fulfill_Ssss_Rpttve_Nbr_Pymnts, new ReportEditMask ("99999"),new TabSetting(111),cwf_Ati_Fulfill_Ssss_Rpttve_Bgn_Dte,new 
            TabSetting(122),cwf_Ati_Fulfill_Ssss_Rpttve_End_Dte);
        if (Global.isEscape()) return;
        F192:                                                                                                                                                             //Natural: FOR #B = 1 TO 20
        for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(20)); pnd_B.nadd(1))
        {
            if (condition(cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(pnd_B).equals(" ")))                                                                                //Natural: IF CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( #B ) = ' '
            {
                if (true) break F192;                                                                                                                                     //Natural: ESCAPE BOTTOM ( F192. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(pnd_B).notEquals(getZero())))                                                                     //Natural: IF CWF-ATI-FULFILL.SSSS-WTHDRWL-AMT ( #B ) NE 0
                {
                                                                                                                                                                          //Natural: PERFORM WRITE2-ACCT-DOLLAR
                    sub_Write2_Acct_Dollar();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F192"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F192"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(pnd_B).notEquals(getZero())))                                                               //Natural: IF CWF-ATI-FULFILL.SSSS-WTHDRWL-PRCNT ( #B ) NE 0
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE2-ACCT-PERCENT
                        sub_Write2_Acct_Percent();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("F192"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("F192"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(pnd_B).notEquals(getZero())))                                                             //Natural: IF CWF-ATI-FULFILL.SSSS-WTHDRWL-UNT ( #B ) NE 0
                        {
                                                                                                                                                                          //Natural: PERFORM WRITE2-ACCT-UNIT
                            sub_Write2_Acct_Unit();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("F192"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("F192"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*          PERFORM SET-ACCOUNT-NAME
                            pnd_Interim_Acct_Name.setValue(cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(pnd_B));                                                           //Natural: MOVE CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( #B ) TO #INTERIM-ACCT-NAME
                            getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"ACCOUNT=",new TabSetting(39),pnd_Interim_Acct_Name,new TabSetting(51),         //Natural: WRITE ( 1 ) 30T 'ACCOUNT=' 39T #INTERIM-ACCT-NAME 51T 'NO UNITS/DOLLARS/PRCNT ENTERED'
                                "NO UNITS/DOLLARS/PRCNT ENTERED");
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("F192"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("F192"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*   END  ATI-WRITE-REC-2 PROCESSING
    }
    //*  **************************************
    private void sub_Set_Process_Status() throws Exception                                                                                                                //Natural: SET-PROCESS-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet913 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CWF-ATI-FULFILL.ATI-PRCSS-STTS;//Natural: VALUE 'O'
        if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("O"))))
        {
            decideConditionsMet913++;
            pnd_Process_Status_Literal.setValue(" OPEN ");                                                                                                                //Natural: MOVE ' OPEN ' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Open_Atis.nadd(1);                                                                                                                            //Natural: ADD 1 TO #GRAND-TOTAL-OPEN-ATIS
            pnd_Input_Data_Pnd_W_Ati_Open.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                                     //Natural: ADD 1 TO #W-ATI-OPEN ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("I"))))
        {
            decideConditionsMet913++;
            pnd_Process_Status_Literal.setValue("INPROGS");                                                                                                               //Natural: MOVE 'INPROGS' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Inprogress_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-INPROGRESS-ATIS
            pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                              //Natural: ADD 1 TO #W-ATI-IN-PROGRESS ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE ' '
        else if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals(" "))))
        {
            decideConditionsMet913++;
            if (condition(cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))                                                                                                     //Natural: IF CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
            {
                pnd_Process_Status_Literal.setValue("SUCCESS");                                                                                                           //Natural: MOVE 'SUCCESS' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Succesfull_Atis.nadd(1);                                                                                                                  //Natural: ADD 1 TO #GRAND-TOTAL-SUCCESFULL-ATIS
                pnd_Input_Data_Pnd_W_Ati_Successful.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                           //Natural: ADD 1 TO #W-ATI-SUCCESSFUL ( #W-INX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Process_Status_Literal.setValue("FAILED");                                                                                                            //Natural: MOVE 'FAILED' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Failed_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-FAILED-ATIS
                pnd_Input_Data_Pnd_W_Ati_Manual.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                               //Natural: ADD 1 TO #W-ATI-MANUAL ( #W-INX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Process_Status_Literal.setValue("INCORCT");                                                                                                               //Natural: MOVE 'INCORCT' TO #PROCESS-STATUS-LITERAL
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  *************************************************
    }
    //*  *********************************
    private void sub_Write2_Acct_Dollar() throws Exception                                                                                                                //Natural: WRITE2-ACCT-DOLLAR
    {
        if (BLNatReinput.isReinput()) return;

        //*  PERFORM SET-ACCOUNT-NAME
        pnd_Interim_Acct_Name.setValue(cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(pnd_B));                                                                               //Natural: MOVE CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( #B ) TO #INTERIM-ACCT-NAME
        pnd_Interim_Dollar.setValue(cwf_Ati_Fulfill_Ssss_Wthdrwl_Amt.getValue(pnd_B));                                                                                    //Natural: MOVE CWF-ATI-FULFILL.SSSS-WTHDRWL-AMT ( #B ) TO #INTERIM-DOLLAR
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"ACCOUNT=",new TabSetting(39),pnd_Interim_Acct_Name,new TabSetting(51),"DOLLAR-AMT  =",new          //Natural: WRITE ( 1 ) 30T 'ACCOUNT=' 39T #INTERIM-ACCT-NAME 51T 'DOLLAR-AMT  =' 65T #INTERIM-DOLLAR
            TabSetting(65),pnd_Interim_Dollar);
        if (Global.isEscape()) return;
        //*  ********************************************
    }
    //*  *********************************
    private void sub_Write2_Acct_Percent() throws Exception                                                                                                               //Natural: WRITE2-ACCT-PERCENT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Interim_Prcnt.setValue(cwf_Ati_Fulfill_Ssss_Wthdrwl_Prcnt.getValue(pnd_B));                                                                                   //Natural: MOVE CWF-ATI-FULFILL.SSSS-WTHDRWL-PRCNT ( #B ) TO #INTERIM-PRCNT
        //*  PERFORM SET-ACCOUNT-NAME
        pnd_Interim_Acct_Name.setValue(cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(pnd_B));                                                                               //Natural: MOVE CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( #B ) TO #INTERIM-ACCT-NAME
        pnd_Interim_Acct_Name.setValue(cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(pnd_B));                                                                               //Natural: MOVE CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( #B ) TO #INTERIM-ACCT-NAME
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"ACCOUNT=",new TabSetting(39),pnd_Interim_Acct_Name,new TabSetting(51),"PERCENTAGE  =",new          //Natural: WRITE ( 1 ) 30T 'ACCOUNT=' 39T #INTERIM-ACCT-NAME 51T 'PERCENTAGE  =' 65T #INTERIM-PRCNT
            TabSetting(65),pnd_Interim_Prcnt);
        if (Global.isEscape()) return;
        //*  *********************************************
    }
    //*  **********************************
    private void sub_Write2_Acct_Unit() throws Exception                                                                                                                  //Natural: WRITE2-ACCT-UNIT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Interim_Units.setValue(cwf_Ati_Fulfill_Ssss_Wthdrwl_Unt.getValue(pnd_B));                                                                                     //Natural: MOVE CWF-ATI-FULFILL.SSSS-WTHDRWL-UNT ( #B ) TO #INTERIM-UNITS
        //*  PERFORM SET-ACCOUNT-NAME
        pnd_Interim_Acct_Name.setValue(cwf_Ati_Fulfill_Ssss_Ticker_Symbol.getValue(pnd_B));                                                                               //Natural: MOVE CWF-ATI-FULFILL.SSSS-TICKER-SYMBOL ( #B ) TO #INTERIM-ACCT-NAME
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"ACCOUNT=",new TabSetting(39),pnd_Interim_Acct_Name,new TabSetting(51),"UNITS       =",new          //Natural: WRITE ( 1 ) 30T 'ACCOUNT=' 39T #INTERIM-ACCT-NAME 51T 'UNITS       =' 65T #INTERIM-UNITS
            TabSetting(65),pnd_Interim_Units);
        if (Global.isEscape()) return;
        //*  *******************************************
    }
    //*  *  REV. WPID SMRY **********
    private void sub_Retrieve_Mit_Unit_Cde() throws Exception                                                                                                             //Natural: RETRIEVE-MIT-UNIT-CDE
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************************************
        pnd_Rqst_Routing_Key_Pnd_Rqst_Mit_Last_Chnge_Dte_Tme.reset();                                                                                                     //Natural: RESET #RQST-MIT-LAST-CHNGE-DTE-TME #MIT-UNIT-CDE
        pnd_Mit_Unit_Cde.reset();
        pnd_Rqst_Routing_Key_Pnd_Rqst_Mit_Log_Dt_Tme.setValueEdited(cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                            //Natural: MOVE EDITED CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME ( EM = YYYYMMDDHHIISST ) TO #RQST-MIT-LOG-DT-TME
        //*  READ MIT TO RETRIEVE THE ORIGINAL MIT RECORD LOGGED & INDEXED BY PSS.
        //*  CAPTURE THE MIT UNIT CODE.
        vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) CWF-MASTER-INDEX-VIEW BY RQST-ROUTING-KEY STARTING FROM #RQST-ROUTING-KEY
        (
        "R_MIT",
        new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Rqst_Routing_Key, WcType.BY) },
        new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
        1
        );
        R_MIT:
        while (condition(vw_cwf_Master_Index_View.readNextRow("R_MIT")))
        {
            if (condition(cwf_Master_Index_View_Rqst_Log_Dte_Tme.notEquals(pnd_Rqst_Routing_Key)))                                                                        //Natural: IF CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME NE #RQST-ROUTING-KEY
            {
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(2),"NO RECORD FOUND ON MIT (FOR UNIT INFO) FOR PIN :",cwf_Ati_Fulfill_Ati_Pin);                 //Natural: WRITE ( 2 ) 2T 'NO RECORD FOUND ON MIT (FOR UNIT INFO) FOR PIN :' CWF-ATI-FULFILL.ATI-PIN
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_MIT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_MIT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) break R_MIT;                                                                                                                                    //Natural: ESCAPE BOTTOM ( R-MIT. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Mit_Unit_Cde.setValue(cwf_Master_Index_View_Admin_Unit_Cde);                                                                                              //Natural: MOVE ADMIN-UNIT-CDE TO #MIT-UNIT-CDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *************************************************************
    }
    //*  *  REV. WPID SMRY **********
    private void sub_Successful_Wpid_Stats() throws Exception                                                                                                             //Natural: SUCCESSFUL-WPID-STATS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FACCW ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FACCW '
        {
            short decideConditionsMet993 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet993++;
                pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FACCW-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet993++;
                pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FACCW-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet993++;
                pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FACCW-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FACCW-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FACTD ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FACTD '
        {
            short decideConditionsMet1006 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1006++;
                pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FACTD-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1006++;
                pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FACTD-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1006++;
                pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FACTD-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FACTD-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FACCK ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FACCK '
        {
            short decideConditionsMet1019 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1019++;
                pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FACCK-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1019++;
                pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FACCK-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1019++;
                pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FACCK-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FACCK-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FAGCW ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FAGCW '
        {
            short decideConditionsMet1032 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1032++;
                pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAGCW-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1032++;
                pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAGCW-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1032++;
                pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAGCW-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAGCW-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FAGTD ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FAGTD '
        {
            short decideConditionsMet1045 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1045++;
                pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAGTD-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1045++;
                pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAGTD-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1045++;
                pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAGTD-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAGTD-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FAGCK ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FAGCK '
        {
            short decideConditionsMet1058 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1058++;
                pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAGCK-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1058++;
                pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAGCK-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1058++;
                pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAGCK-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAGCK-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FAKCW ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FAKCW '
        {
            short decideConditionsMet1071 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1071++;
                pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAKCW-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1071++;
                pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAKCW-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1071++;
                pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAKCW-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAKCW-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FASCW ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FASCW '
        {
            short decideConditionsMet1084 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1084++;
                pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FASCW-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1084++;
                pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FASCW-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1084++;
                pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FASCW-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FASCW-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FABCW ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FABCW '
        {
            short decideConditionsMet1097 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1097++;
                pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FABCW-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1097++;
                pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FABCW-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1097++;
                pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FABCW-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FABCW-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FAKCK ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FAKCK '
        {
            short decideConditionsMet1110 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1110++;
                pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAKCK-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1110++;
                pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAKCK-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1110++;
                pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAKCK-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAKCK-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FASCK ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FASCK '
        {
            short decideConditionsMet1123 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1123++;
                pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FASCK-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1123++;
                pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FASCK-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1123++;
                pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FASCK-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FASCK-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FABCK ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FABCK '
        {
            short decideConditionsMet1136 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1136++;
                pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FABCK-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1136++;
                pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FABCK-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1136++;
                pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FABCK-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FABCK-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FAKTD ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FAKTD '
        {
            short decideConditionsMet1149 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1149++;
                pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAKTD-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1149++;
                pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAKTD-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1149++;
                pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAKTD-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FAKTD-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FASTD ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FASTD '
        {
            short decideConditionsMet1162 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1162++;
                pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FASTD-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1162++;
                pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FASTD-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1162++;
                pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FASTD-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FASTD-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FABTD ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FABTD '
        {
            short decideConditionsMet1175 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1175++;
                pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps1.nadd(1);                                                                                                      //Natural: ADD 1 TO #FABTD-SUM-OK-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1175++;
                pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bps2.nadd(1);                                                                                                      //Natural: ADD 1 TO #FABTD-SUM-OK-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1175++;
                pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Bpsw.nadd(1);                                                                                                      //Natural: ADD 1 TO #FABTD-SUM-OK-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Ok_Misc.nadd(1);                                                                                                      //Natural: ADD 1 TO #FABTD-SUM-OK-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  *******************************************
    }
    //*  *  REV. WPID SMRY **********
    private void sub_Failed_Wpid_Stats() throws Exception                                                                                                                 //Natural: FAILED-WPID-STATS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FACCW ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FACCW '
        {
            short decideConditionsMet1192 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1192++;
                pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FACCW-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1192++;
                pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FACCW-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1192++;
                pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FACCW-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Faccw_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FACCW-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FACTD ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FACTD '
        {
            short decideConditionsMet1205 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1205++;
                pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FACTD-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1205++;
                pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FACTD-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1205++;
                pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FACTD-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Factd_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FACTD-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FACCK ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FACCK '
        {
            short decideConditionsMet1218 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1218++;
                pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FACCK-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1218++;
                pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FACCK-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1218++;
                pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FACCK-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Facck_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FACCK-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FAGCW ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FAGCW '
        {
            short decideConditionsMet1231 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1231++;
                pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAGCW-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1231++;
                pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAGCW-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1231++;
                pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAGCW-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fagcw_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAGCW-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FAGTD ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FAGTD '
        {
            short decideConditionsMet1244 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1244++;
                pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAGTD-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1244++;
                pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAGTD-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1244++;
                pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAGTD-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fagtd_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAGTD-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FAGCK ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FAGCK '
        {
            short decideConditionsMet1257 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1257++;
                pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAGCK-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1257++;
                pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAGCK-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1257++;
                pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAGCK-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fagck_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAGCK-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FAKCW ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FAKCW '
        {
            short decideConditionsMet1270 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1270++;
                pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAKCW-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1270++;
                pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAKCW-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1270++;
                pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAKCW-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fakcw_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAKCW-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FASCW ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FASCW '
        {
            short decideConditionsMet1283 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1283++;
                pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FASCW-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1283++;
                pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FASCW-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1283++;
                pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FASCW-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fascw_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FASCW-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FABCW ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FABCW '
        {
            short decideConditionsMet1296 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1296++;
                pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FABCW-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1296++;
                pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FABCW-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1296++;
                pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FABCW-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fabcw_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FABCW-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FAKCK ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FAKCK '
        {
            short decideConditionsMet1309 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1309++;
                pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAKCK-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1309++;
                pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAKCK-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1309++;
                pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAKCK-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fakck_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAKCK-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FASCK ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FASCK '
        {
            short decideConditionsMet1322 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1322++;
                pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FASCK-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1322++;
                pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FASCK-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1322++;
                pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FASCK-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fasck_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FASCK-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FABCK ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FABCK '
        {
            short decideConditionsMet1335 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1335++;
                pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FABCK-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1335++;
                pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FABCK-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1335++;
                pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FABCK-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fabck_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FABCK-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FAKTD ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FAKTD '
        {
            short decideConditionsMet1348 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1348++;
                pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAKTD-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1348++;
                pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAKTD-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1348++;
                pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAKTD-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Faktd_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FAKTD-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FASTD ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FASTD '
        {
            short decideConditionsMet1361 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1361++;
                pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FASTD-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1361++;
                pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FASTD-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1361++;
                pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FASTD-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fastd_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FASTD-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Ati_Wpid.equals("FABTD ")))                                                                                                         //Natural: IF CWF-ATI-FULFILL.ATI-WPID = 'FABTD '
        {
            short decideConditionsMet1374 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS1'
            if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS1")))
            {
                decideConditionsMet1374++;
                pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps1.nadd(1);                                                                                                    //Natural: ADD 1 TO #FABTD-SUM-FAIL-BPS1
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSS2'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSS2")))
            {
                decideConditionsMet1374++;
                pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bps2.nadd(1);                                                                                                    //Natural: ADD 1 TO #FABTD-SUM-FAIL-BPS2
            }                                                                                                                                                             //Natural: WHEN #MIT-UNIT-CDE-1-6 = 'BPSSSW'
            else if (condition(pnd_Mit_Unit_Cde_Pnd_Mit_Unit_Cde_1_6.equals("BPSSSW")))
            {
                decideConditionsMet1374++;
                pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Bpsw.nadd(1);                                                                                                    //Natural: ADD 1 TO #FABTD-SUM-FAIL-BPSW
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Wpid_Summary_Data_Pnd_Fabtd_Sum_Fail_Misc.nadd(1);                                                                                                    //Natural: ADD 1 TO #FABTD-SUM-FAIL-MISC
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  *******************************************
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(50),"AUTOMATED TRANSACTION SYSTEM",new                //Natural: WRITE ( 1 ) NOTITLE 001T *PROGRAM 050T 'AUTOMATED TRANSACTION SYSTEM' 101T *DATU 110T *TIME ( AL = 005 ) 121T 'PAGE:' 127T *PAGE-NUMBER ( 1 ) ( AD = L ) / 035T 'ATI DETAILED REPORT FOR SSSS FORMS IN WPID SEQUENCE' / 035T #TITLE-LITERAL / 001T '_' ( 131 ) / 6T 'PIN' 26T 'MASTER INDEX' 48T 'ATI STATUS' 63T 'PROCESS' 71T 'MAIL' 76T 'CITZ' 81T 'RSDN' 86T 'ROLL' 91T 'ERLY' 96T 'WTHD' 101T 'RPT' 105T 'RPT #' 111T 'REPETITIVE' 122T 'REPETITIVE' / 4T 'NUMBER' 16T 'WPID' 24T 'LOG DATE & TIME' 47T 'DATE & TIME' 63T 'STATUS' 71T 'CODE' 76T 'CODE' 81T 'CODE' 86T 'CODE' 91T 'TERM' 96T 'ALL' 101T 'FRQ' 105T 'PYMTS' 112T 'BGN-DATE' 123T 'END-DATE' / 001T '_' ( 131 ) /
                        TabSetting(101),Global.getDATU(),new TabSetting(110),Global.getTIME(), new AlphanumericLength (5),new TabSetting(121),"PAGE:",new 
                        TabSetting(127),getReports().getPageNumberDbs(1), new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(35),"ATI DETAILED REPORT FOR SSSS FORMS IN WPID SEQUENCE",NEWLINE,new 
                        TabSetting(35),pnd_Title_Literal,NEWLINE,new TabSetting(1),"_",new RepeatItem(131),NEWLINE,new TabSetting(6),"PIN",new TabSetting(26),"MASTER INDEX",new 
                        TabSetting(48),"ATI STATUS",new TabSetting(63),"PROCESS",new TabSetting(71),"MAIL",new TabSetting(76),"CITZ",new TabSetting(81),"RSDN",new 
                        TabSetting(86),"ROLL",new TabSetting(91),"ERLY",new TabSetting(96),"WTHD",new TabSetting(101),"RPT",new TabSetting(105),"RPT #",new 
                        TabSetting(111),"REPETITIVE",new TabSetting(122),"REPETITIVE",NEWLINE,new TabSetting(4),"NUMBER",new TabSetting(16),"WPID",new TabSetting(24),"LOG DATE & TIME",new 
                        TabSetting(47),"DATE & TIME",new TabSetting(63),"STATUS",new TabSetting(71),"CODE",new TabSetting(76),"CODE",new TabSetting(81),"CODE",new 
                        TabSetting(86),"CODE",new TabSetting(91),"TERM",new TabSetting(96),"ALL",new TabSetting(101),"FRQ",new TabSetting(105),"PYMTS",new 
                        TabSetting(112),"BGN-DATE",new TabSetting(123),"END-DATE",NEWLINE,new TabSetting(1),"_",new RepeatItem(131),NEWLINE);
                    //*    31T 'MASTER INDEX'
                    //*    53T 'ATI STATUS'
                    //*    68T 'PROCESS'
                    //*    76T 'MAIL'
                    //*    81T 'CITZ'
                    //*    86T 'RSDN'
                    //*    91T 'ROLL'
                    //*    96T 'ERLY'
                    //*   101T 'WTHD'
                    //*    106T 'RPT'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atEndEventRpt1 = (Object sender, EventArgs e) ->
    {
        String localMethod = "MCSN3802|atEndEventRpt1";
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Last_Page_Flag.equals("Y")))                                                                                                        //Natural: IF #LAST-PAGE-FLAG = 'Y'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"_",new RepeatItem(131),NEWLINE,new TabSetting(1),"_",new                    //Natural: WRITE ( 1 ) / 1T '_' ( 131 ) / 1T '_' ( 131 )
                            RepeatItem(131));
                    }                                                                                                                                                     //Natural: END-IF
                    //* *****************************************
                }                                                                                                                                                         //Natural: END-ENDPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(50),"AUTOMATED TRANSACTION SYSTEM",new                //Natural: WRITE ( 2 ) NOTITLE 001T *PROGRAM 050T 'AUTOMATED TRANSACTION SYSTEM' 101T *DATU 110T *TIME ( AL = 005 ) 121T 'PAGE:' 127T *PAGE-NUMBER ( 2 ) ( AD = L ) // 039T 'ATI SYSTEM SUMMARY FOR SINGLE SUM SETTLEMENT FORMS' // 051T 'ATI SUMMARY FOR :' #INPUT-DTE-D ( EM = MM/DD/YY ) // 001T '_' ( 131 ) /
                        TabSetting(101),Global.getDATU(),new TabSetting(110),Global.getTIME(), new AlphanumericLength (5),new TabSetting(121),"PAGE:",new 
                        TabSetting(127),getReports().getPageNumberDbs(2), new FieldAttributes ("AD=L"),NEWLINE,NEWLINE,new TabSetting(39),"ATI SYSTEM SUMMARY FOR SINGLE SUM SETTLEMENT FORMS",NEWLINE,NEWLINE,new 
                        TabSetting(51),"ATI SUMMARY FOR :",pnd_Input_Dte_D, new ReportEditMask ("MM/DD/YY"),NEWLINE,NEWLINE,new TabSetting(1),"_",new RepeatItem(131),
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Ati_Fulfill_Ati_WpidIsBreak = cwf_Ati_Fulfill_Ati_Wpid.isBreak(endOfData);
        if (condition(cwf_Ati_Fulfill_Ati_WpidIsBreak))
        {
            getReports().skip(1, 2);                                                                                                                                      //Natural: SKIP ( 1 ) 2
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TOTAL FOR WPID :",pnd_Prev_Wpid, new ReportEditMask ("XX' 'X' 'XX' 'X"),":",                    //Natural: WRITE ( 1 ) 1T 'TOTAL FOR WPID :' #PREV-WPID ( EM = XX' 'X' 'XX' 'X ) ':' #WPID-TOTAL
                pnd_Wpid_Total);
            if (condition(Global.isEscape())) return;
            pnd_Wpid_Total.reset();                                                                                                                                       //Natural: RESET #WPID-TOTAL
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=OFF");
        Global.format(1, "PS=60 LS=133 ZP=OFF");
        Global.format(2, "PS=60 LS=133 ZP=OFF");
    }
}
