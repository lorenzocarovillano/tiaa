/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:26:01 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3818
************************************************************
**        * FILE NAME            : Mcsn3818.java
**        * CLASS NAME           : Mcsn3818
**        * INSTANCE NAME        : Mcsn3818
************************************************************
************************************************************************
* PROGRAM  : MCSN3818
* TITLE    : ACCESS AWD TO CWF AUDIT RECORDS FROM THE  CWF-MCSS-CALLS
* FUNCTION : DISPLAY CWF-MCSS-CALLS RECORDS ON 188
*          : FOR THE "AWD TO CWF" STATUS/WPID/SSN UPDATE INTERFACE
* KB 11/12/97  THE REPORT WAS MODIFIED TO PRINTE SUCCESSFUL SEPARATE
*              FROM FAILED
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3818 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;

    private DataAccessProgramView vw_cwf_Mcss_Calls;
    private DbsField cwf_Mcss_Calls_Rcrd_Stamp_Tme;
    private DbsField cwf_Mcss_Calls_Rcrd_Upld_Tme;
    private DbsField cwf_Mcss_Calls_Rcrd_Type_Cde;
    private DbsField cwf_Mcss_Calls_Rcrd_Contn_Ind;
    private DbsField cwf_Mcss_Calls_Systm_Ind;
    private DbsField cwf_Mcss_Calls_Pin_Nbr;
    private DbsField cwf_Mcss_Calls_Contact_Id_Cde;
    private DbsField cwf_Mcss_Calls_Call_Stamp_Tme;
    private DbsField cwf_Mcss_Calls_Btch_Nbr;
    private DbsField cwf_Mcss_Calls_Lines_Nbr;
    private DbsField cwf_Mcss_Calls_Wpids_Nbr;
    private DbsField cwf_Mcss_Calls_Lines_Total_Nbr;
    private DbsField cwf_Mcss_Calls_Wpids_Total_Nbr;
    private DbsField cwf_Mcss_Calls_Cntct_Total_Nbr;
    private DbsField cwf_Mcss_Calls_Cntct_Error_Nbr;
    private DbsField cwf_Mcss_Calls_Rqst_Entry_Op_Cde;
    private DbsField cwf_Mcss_Calls_Ph_Ssn_Nbr;
    private DbsField cwf_Mcss_Calls_Spcl_Hndlng_Txt;

    private DbsGroup cwf_Mcss_Calls__R_Field_1;
    private DbsField cwf_Mcss_Calls_Pnd_Fillera;
    private DbsField cwf_Mcss_Calls_Pnd_Calls_Rtrn_Cd_As400;
    private DbsField cwf_Mcss_Calls_Pnd_Fillerb;
    private DbsField cwf_Mcss_Calls_Error_Msg_Txt;

    private DbsGroup cwf_Mcss_Calls_Request_Array_Grp;
    private DbsField cwf_Mcss_Calls_Action_Cde;
    private DbsField cwf_Mcss_Calls_Wpid_Cde;
    private DbsField pnd_Batch_Key;

    private DbsGroup pnd_Batch_Key__R_Field_2;
    private DbsField pnd_Batch_Key_Pnd_Systm_Ind;
    private DbsField pnd_Batch_Key_Pnd_Rcrd_Upld_Tme;
    private DbsField pnd_Batch_Key_Pnd_Rcrd_Type_Cde;
    private DbsField pnd_Batch_Key_Pnd_Btch_Nbr;
    private DbsField pnd_Batch_Key_Pnd_Contact_Id_Cde;
    private DbsField pnd_Spcl_Handling_Txt_Brkdwn;

    private DbsGroup pnd_Spcl_Handling_Txt_Brkdwn__R_Field_3;
    private DbsField pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Awd_Update_Dt_Tme;
    private DbsField pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Filler1;
    private DbsField pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Mit_Log_Dt_Tme;
    private DbsField pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Filler2;
    private DbsField pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Mit_Status;
    private DbsField pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Filler3;
    private DbsField pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Rtrn_Cd_As400;
    private DbsField pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Filler4;
    private DbsField pnd_Processing_Literal;
    private DbsField pnd_Updt_Rqst_Literal;

    private DbsGroup pnd_Updt_Rqst_Literal__R_Field_4;
    private DbsField pnd_Updt_Rqst_Literal_Pnd_Status_Literal;
    private DbsField pnd_Updt_Rqst_Literal_Pnd_Fillerfslash__1;
    private DbsField pnd_Updt_Rqst_Literal_Pnd_Wpid_Literal;
    private DbsField pnd_Updt_Rqst_Literal_Pnd_Fillerfslash__2;
    private DbsField pnd_Updt_Rqst_Literal_Pnd_Ssn_Literal;
    private DbsField pnd_Input_Dte_Tme;
    private DbsField pnd_Input_Dte_D;
    private DbsField pnd_Orig_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Entry_Date;
    private DbsField pnd_Last_Page_Flag;
    private DbsField pnd_Mit_Log_Date;
    private DbsField pnd_Tot_Ok_Awd_Updates;
    private DbsField pnd_Tot_Ng_Awd_Updates;
    private DbsField pnd_Grand_Total_Awd_Updates;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Input_Data = parameters.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Mcss_Calls = new DataAccessProgramView(new NameInfo("vw_cwf_Mcss_Calls", "CWF-MCSS-CALLS"), "CWF_MCSS_CALLS", "CWF_MCSS_CALLS", DdmPeriodicGroups.getInstance().getGroups("CWF_MCSS_CALLS"));
        cwf_Mcss_Calls_Rcrd_Stamp_Tme = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rcrd_Stamp_Tme", "RCRD-STAMP-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_STAMP_TME");
        cwf_Mcss_Calls_Rcrd_Stamp_Tme.setDdmHeader("RECORD/TIME/STAMP");
        cwf_Mcss_Calls_Rcrd_Upld_Tme = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_UPLD_TME");
        cwf_Mcss_Calls_Rcrd_Upld_Tme.setDdmHeader("RECORD/UPLOAD/TIME");
        cwf_Mcss_Calls_Rcrd_Type_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        cwf_Mcss_Calls_Rcrd_Type_Cde.setDdmHeader("RECORD/TYPE");
        cwf_Mcss_Calls_Rcrd_Contn_Ind = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rcrd_Contn_Ind", "RCRD-CONTN-IND", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "RCRD_CONTN_IND");
        cwf_Mcss_Calls_Rcrd_Contn_Ind.setDdmHeader("RECORD/CONT./NUMBER");
        cwf_Mcss_Calls_Systm_Ind = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTM_IND");
        cwf_Mcss_Calls_Systm_Ind.setDdmHeader("SYSTEM/OF/ORIGIN");
        cwf_Mcss_Calls_Pin_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Mcss_Calls_Pin_Nbr.setDdmHeader("PIN");
        cwf_Mcss_Calls_Contact_Id_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CONTACT_ID_CDE");
        cwf_Mcss_Calls_Contact_Id_Cde.setDdmHeader("CONTACT/ID");
        cwf_Mcss_Calls_Call_Stamp_Tme = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Call_Stamp_Tme", "CALL-STAMP-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CALL_STAMP_TME");
        cwf_Mcss_Calls_Call_Stamp_Tme.setDdmHeader("CALL/DATE &/TIME");
        cwf_Mcss_Calls_Btch_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Btch_Nbr", "BTCH-NBR", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "BTCH_NBR");
        cwf_Mcss_Calls_Btch_Nbr.setDdmHeader("BATCH/NUM.");
        cwf_Mcss_Calls_Lines_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Lines_Nbr", "LINES-NBR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "LINES_NBR");
        cwf_Mcss_Calls_Lines_Nbr.setDdmHeader("TEXT/LINES/SENT");
        cwf_Mcss_Calls_Wpids_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Wpids_Nbr", "WPIDS-NBR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "WPIDS_NBR");
        cwf_Mcss_Calls_Wpids_Nbr.setDdmHeader("WPIDS/SENT");
        cwf_Mcss_Calls_Lines_Total_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Lines_Total_Nbr", "LINES-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LINES_TOTAL_NBR");
        cwf_Mcss_Calls_Lines_Total_Nbr.setDdmHeader("TOTAL/LINES");
        cwf_Mcss_Calls_Wpids_Total_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Wpids_Total_Nbr", "WPIDS-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "WPIDS_TOTAL_NBR");
        cwf_Mcss_Calls_Wpids_Total_Nbr.setDdmHeader("TOTAL/WPIDS");
        cwf_Mcss_Calls_Cntct_Total_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Cntct_Total_Nbr", "CNTCT-TOTAL-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTCT_TOTAL_NBR");
        cwf_Mcss_Calls_Cntct_Total_Nbr.setDdmHeader("TOTAL/CONTACT/SHEETS");
        cwf_Mcss_Calls_Cntct_Error_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Cntct_Error_Nbr", "CNTCT-ERROR-NBR", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "CNTCT_ERROR_NBR");
        cwf_Mcss_Calls_Cntct_Error_Nbr.setDdmHeader("TOTAL/ERRORS");
        cwf_Mcss_Calls_Rqst_Entry_Op_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rqst_Entry_Op_Cde", "RQST-ENTRY-OP-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_ENTRY_OP_CDE");
        cwf_Mcss_Calls_Rqst_Entry_Op_Cde.setDdmHeader("ORIGIN/RACF/ID");
        cwf_Mcss_Calls_Ph_Ssn_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Ph_Ssn_Nbr", "PH-SSN-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PH_SSN_NBR");
        cwf_Mcss_Calls_Spcl_Hndlng_Txt = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        cwf_Mcss_Calls_Spcl_Hndlng_Txt.setDdmHeader("SPECIAL/HANDLING");

        cwf_Mcss_Calls__R_Field_1 = vw_cwf_Mcss_Calls.getRecord().newGroupInGroup("cwf_Mcss_Calls__R_Field_1", "REDEFINE", cwf_Mcss_Calls_Spcl_Hndlng_Txt);
        cwf_Mcss_Calls_Pnd_Fillera = cwf_Mcss_Calls__R_Field_1.newFieldInGroup("cwf_Mcss_Calls_Pnd_Fillera", "#FILLERA", FieldType.STRING, 34);
        cwf_Mcss_Calls_Pnd_Calls_Rtrn_Cd_As400 = cwf_Mcss_Calls__R_Field_1.newFieldInGroup("cwf_Mcss_Calls_Pnd_Calls_Rtrn_Cd_As400", "#CALLS-RTRN-CD-AS400", 
            FieldType.STRING, 1);
        cwf_Mcss_Calls_Pnd_Fillerb = cwf_Mcss_Calls__R_Field_1.newFieldInGroup("cwf_Mcss_Calls_Pnd_Fillerb", "#FILLERB", FieldType.STRING, 10);
        cwf_Mcss_Calls_Error_Msg_Txt = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Error_Msg_Txt", "ERROR-MSG-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ERROR_MSG_TXT");
        cwf_Mcss_Calls_Error_Msg_Txt.setDdmHeader("ERROR/MSG.");

        cwf_Mcss_Calls_Request_Array_Grp = vw_cwf_Mcss_Calls.getRecord().newGroupArrayInGroup("cwf_Mcss_Calls_Request_Array_Grp", "REQUEST-ARRAY-GRP", 
            new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Action_Cde = cwf_Mcss_Calls_Request_Array_Grp.newFieldInGroup("cwf_Mcss_Calls_Action_Cde", "ACTION-CDE", FieldType.STRING, 2, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACTION_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Wpid_Cde = cwf_Mcss_Calls_Request_Array_Grp.newFieldInGroup("cwf_Mcss_Calls_Wpid_Cde", "WPID-CDE", FieldType.STRING, 6, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "WPID_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        registerRecord(vw_cwf_Mcss_Calls);

        pnd_Batch_Key = localVariables.newFieldInRecord("pnd_Batch_Key", "#BATCH-KEY", FieldType.STRING, 41);

        pnd_Batch_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Batch_Key__R_Field_2", "REDEFINE", pnd_Batch_Key);
        pnd_Batch_Key_Pnd_Systm_Ind = pnd_Batch_Key__R_Field_2.newFieldInGroup("pnd_Batch_Key_Pnd_Systm_Ind", "#SYSTM-IND", FieldType.STRING, 15);
        pnd_Batch_Key_Pnd_Rcrd_Upld_Tme = pnd_Batch_Key__R_Field_2.newFieldInGroup("pnd_Batch_Key_Pnd_Rcrd_Upld_Tme", "#RCRD-UPLD-TME", FieldType.TIME);
        pnd_Batch_Key_Pnd_Rcrd_Type_Cde = pnd_Batch_Key__R_Field_2.newFieldInGroup("pnd_Batch_Key_Pnd_Rcrd_Type_Cde", "#RCRD-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Batch_Key_Pnd_Btch_Nbr = pnd_Batch_Key__R_Field_2.newFieldInGroup("pnd_Batch_Key_Pnd_Btch_Nbr", "#BTCH-NBR", FieldType.NUMERIC, 8);
        pnd_Batch_Key_Pnd_Contact_Id_Cde = pnd_Batch_Key__R_Field_2.newFieldInGroup("pnd_Batch_Key_Pnd_Contact_Id_Cde", "#CONTACT-ID-CDE", FieldType.STRING, 
            10);
        pnd_Spcl_Handling_Txt_Brkdwn = localVariables.newFieldInRecord("pnd_Spcl_Handling_Txt_Brkdwn", "#SPCL-HANDLING-TXT-BRKDWN", FieldType.STRING, 
            45);

        pnd_Spcl_Handling_Txt_Brkdwn__R_Field_3 = localVariables.newGroupInRecord("pnd_Spcl_Handling_Txt_Brkdwn__R_Field_3", "REDEFINE", pnd_Spcl_Handling_Txt_Brkdwn);
        pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Awd_Update_Dt_Tme = pnd_Spcl_Handling_Txt_Brkdwn__R_Field_3.newFieldInGroup("pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Awd_Update_Dt_Tme", 
            "#AWD-UPDATE-DT-TME", FieldType.NUMERIC, 15);
        pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Filler1 = pnd_Spcl_Handling_Txt_Brkdwn__R_Field_3.newFieldInGroup("pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Filler1", 
            "#FILLER1", FieldType.STRING, 1);
        pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Mit_Log_Dt_Tme = pnd_Spcl_Handling_Txt_Brkdwn__R_Field_3.newFieldInGroup("pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Mit_Log_Dt_Tme", 
            "#MIT-LOG-DT-TME", FieldType.NUMERIC, 15);
        pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Filler2 = pnd_Spcl_Handling_Txt_Brkdwn__R_Field_3.newFieldInGroup("pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Filler2", 
            "#FILLER2", FieldType.STRING, 1);
        pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Mit_Status = pnd_Spcl_Handling_Txt_Brkdwn__R_Field_3.newFieldInGroup("pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Mit_Status", 
            "#MIT-STATUS", FieldType.STRING, 1);
        pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Filler3 = pnd_Spcl_Handling_Txt_Brkdwn__R_Field_3.newFieldInGroup("pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Filler3", 
            "#FILLER3", FieldType.STRING, 1);
        pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Rtrn_Cd_As400 = pnd_Spcl_Handling_Txt_Brkdwn__R_Field_3.newFieldInGroup("pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Rtrn_Cd_As400", 
            "#RTRN-CD-AS400", FieldType.STRING, 1);
        pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Filler4 = pnd_Spcl_Handling_Txt_Brkdwn__R_Field_3.newFieldInGroup("pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Filler4", 
            "#FILLER4", FieldType.STRING, 10);
        pnd_Processing_Literal = localVariables.newFieldInRecord("pnd_Processing_Literal", "#PROCESSING-LITERAL", FieldType.STRING, 10);
        pnd_Updt_Rqst_Literal = localVariables.newFieldInRecord("pnd_Updt_Rqst_Literal", "#UPDT-RQST-LITERAL", FieldType.STRING, 15);

        pnd_Updt_Rqst_Literal__R_Field_4 = localVariables.newGroupInRecord("pnd_Updt_Rqst_Literal__R_Field_4", "REDEFINE", pnd_Updt_Rqst_Literal);
        pnd_Updt_Rqst_Literal_Pnd_Status_Literal = pnd_Updt_Rqst_Literal__R_Field_4.newFieldInGroup("pnd_Updt_Rqst_Literal_Pnd_Status_Literal", "#STATUS-LITERAL", 
            FieldType.STRING, 6);
        pnd_Updt_Rqst_Literal_Pnd_Fillerfslash__1 = pnd_Updt_Rqst_Literal__R_Field_4.newFieldInGroup("pnd_Updt_Rqst_Literal_Pnd_Fillerfslash__1", "#FILLER/-1", 
            FieldType.STRING, 1);
        pnd_Updt_Rqst_Literal_Pnd_Wpid_Literal = pnd_Updt_Rqst_Literal__R_Field_4.newFieldInGroup("pnd_Updt_Rqst_Literal_Pnd_Wpid_Literal", "#WPID-LITERAL", 
            FieldType.STRING, 4);
        pnd_Updt_Rqst_Literal_Pnd_Fillerfslash__2 = pnd_Updt_Rqst_Literal__R_Field_4.newFieldInGroup("pnd_Updt_Rqst_Literal_Pnd_Fillerfslash__2", "#FILLER/-2", 
            FieldType.STRING, 1);
        pnd_Updt_Rqst_Literal_Pnd_Ssn_Literal = pnd_Updt_Rqst_Literal__R_Field_4.newFieldInGroup("pnd_Updt_Rqst_Literal_Pnd_Ssn_Literal", "#SSN-LITERAL", 
            FieldType.STRING, 3);
        pnd_Input_Dte_Tme = localVariables.newFieldInRecord("pnd_Input_Dte_Tme", "#INPUT-DTE-TME", FieldType.TIME);
        pnd_Input_Dte_D = localVariables.newFieldInRecord("pnd_Input_Dte_D", "#INPUT-DTE-D", FieldType.DATE);
        pnd_Orig_Date = localVariables.newFieldInRecord("pnd_Orig_Date", "#ORIG-DATE", FieldType.DATE);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 8);
        pnd_Entry_Date = localVariables.newFieldInRecord("pnd_Entry_Date", "#ENTRY-DATE", FieldType.DATE);
        pnd_Last_Page_Flag = localVariables.newFieldInRecord("pnd_Last_Page_Flag", "#LAST-PAGE-FLAG", FieldType.STRING, 1);
        pnd_Mit_Log_Date = localVariables.newFieldInRecord("pnd_Mit_Log_Date", "#MIT-LOG-DATE", FieldType.NUMERIC, 14);
        pnd_Tot_Ok_Awd_Updates = localVariables.newFieldInRecord("pnd_Tot_Ok_Awd_Updates", "#TOT-OK-AWD-UPDATES", FieldType.NUMERIC, 5);
        pnd_Tot_Ng_Awd_Updates = localVariables.newFieldInRecord("pnd_Tot_Ng_Awd_Updates", "#TOT-NG-AWD-UPDATES", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Awd_Updates = localVariables.newFieldInRecord("pnd_Grand_Total_Awd_Updates", "#GRAND-TOTAL-AWD-UPDATES", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Mcss_Calls.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Batch_Key.setInitialValue("BATCHAWD");
        pnd_Tot_Ok_Awd_Updates.setInitialValue(0);
        pnd_Tot_Ng_Awd_Updates.setInitialValue(0);
        pnd_Grand_Total_Awd_Updates.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Mcsn3818() throws Exception
    {
        super("Mcsn3818");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt8, 8);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 8 ) PS = 60 LS = 133 ZP = OFF
        pnd_Tot_Ok_Awd_Updates.reset();                                                                                                                                   //Natural: RESET #TOT-OK-AWD-UPDATES #TOT-NG-AWD-UPDATES #GRAND-TOTAL-AWD-UPDATES
        pnd_Tot_Ng_Awd_Updates.reset();
        pnd_Grand_Total_Awd_Updates.reset();
        if (condition(pnd_Input_Data_Pnd_Input_Date.notEquals(" ")))                                                                                                      //Natural: IF #INPUT-DATE NE ' '
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Input_Dte_Tme.setValue(pnd_Input_Dte_D);                                                                                                                  //Natural: MOVE #INPUT-DTE-D TO #INPUT-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(8, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***");                                     //Natural: WRITE ( 8 ) /// '*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***'
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
            //*  PIN-EXP <<
            //*  PIN-EXP <<
        }                                                                                                                                                                 //Natural: END-IF
        //*  -----------------------------------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 8 )
        //*  -----------------------------------------------------------------
        //*  SET BATCH-KEY FOR CWF-MCSS-CALLS READ *
        pnd_Batch_Key_Pnd_Rcrd_Upld_Tme.reset();                                                                                                                          //Natural: RESET #BATCH-KEY.#RCRD-UPLD-TME #BATCH-KEY.#RCRD-TYPE-CDE #BATCH-KEY.#BTCH-NBR #BATCH-KEY.#CONTACT-ID-CDE
        pnd_Batch_Key_Pnd_Rcrd_Type_Cde.reset();
        pnd_Batch_Key_Pnd_Btch_Nbr.reset();
        pnd_Batch_Key_Pnd_Contact_Id_Cde.reset();
        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        //*  READ CWF-MCSS-CALLS ---  KEY = BATCH-KEY *
        vw_cwf_Mcss_Calls.startDatabaseRead                                                                                                                               //Natural: READ CWF-MCSS-CALLS BY BATCH-KEY STARTING FROM #BATCH-KEY
        (
        "READ01",
        new Wc[] { new Wc("BATCH_KEY", ">=", pnd_Batch_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("BATCH_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Mcss_Calls.readNextRow("READ01")))
        {
            if (condition(cwf_Mcss_Calls_Systm_Ind.notEquals("BATCHAWD")))                                                                                                //Natural: IF CWF-MCSS-CALLS.SYSTM-IND NOT = 'BATCHAWD'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Entry_Date.setValue(cwf_Mcss_Calls_Rcrd_Upld_Tme);                                                                                                        //Natural: MOVE CWF-MCSS-CALLS.RCRD-UPLD-TME TO #ENTRY-DATE
            if (condition(!(pnd_Entry_Date.equals(pnd_Input_Dte_D) && cwf_Mcss_Calls_Rcrd_Type_Cde.equals("A"))))                                                         //Natural: ACCEPT IF #ENTRY-DATE = #INPUT-DTE-D AND CWF-MCSS-CALLS.RCRD-TYPE-CDE = 'A'
            {
                continue;
            }
            getSort().writeSortInData(cwf_Mcss_Calls_Pnd_Calls_Rtrn_Cd_As400, cwf_Mcss_Calls_Pin_Nbr, cwf_Mcss_Calls_Rcrd_Upld_Tme, cwf_Mcss_Calls_Systm_Ind,             //Natural: END-ALL
                cwf_Mcss_Calls_Error_Msg_Txt, cwf_Mcss_Calls_Ph_Ssn_Nbr, cwf_Mcss_Calls_Spcl_Hndlng_Txt, cwf_Mcss_Calls_Action_Cde.getValue(1), cwf_Mcss_Calls_Wpid_Cde.getValue(1));
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  SORT RECORDS BY PIN AND UPLOAD DATE/TIME *
        getSort().sortData(cwf_Mcss_Calls_Pnd_Calls_Rtrn_Cd_As400, cwf_Mcss_Calls_Pin_Nbr, cwf_Mcss_Calls_Rcrd_Upld_Tme);                                                 //Natural: SORT RECORDS BY CWF-MCSS-CALLS.#CALLS-RTRN-CD-AS400 CWF-MCSS-CALLS.PIN-NBR CWF-MCSS-CALLS.RCRD-UPLD-TME USING CWF-MCSS-CALLS.SYSTM-IND CWF-MCSS-CALLS.ERROR-MSG-TXT CWF-MCSS-CALLS.PH-SSN-NBR CWF-MCSS-CALLS.SPCL-HNDLNG-TXT CWF-MCSS-CALLS.ACTION-CDE ( 1 ) CWF-MCSS-CALLS.WPID-CDE ( 1 )
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Mcss_Calls_Pnd_Calls_Rtrn_Cd_As400, cwf_Mcss_Calls_Pin_Nbr, cwf_Mcss_Calls_Rcrd_Upld_Tme, cwf_Mcss_Calls_Systm_Ind, 
            cwf_Mcss_Calls_Error_Msg_Txt, cwf_Mcss_Calls_Ph_Ssn_Nbr, cwf_Mcss_Calls_Spcl_Hndlng_Txt, cwf_Mcss_Calls_Action_Cde.getValue(1), cwf_Mcss_Calls_Wpid_Cde.getValue(1))))
        {
            pnd_Spcl_Handling_Txt_Brkdwn.setValue(cwf_Mcss_Calls_Spcl_Hndlng_Txt);                                                                                        //Natural: MOVE CWF-MCSS-CALLS.SPCL-HNDLNG-TXT TO #SPCL-HANDLING-TXT-BRKDWN
            pnd_Processing_Literal.reset();                                                                                                                               //Natural: RESET #PROCESSING-LITERAL
            if (condition(pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Rtrn_Cd_As400.equals("0")))                                                                                    //Natural: IF #RTRN-CD-AS400 = '0'
            {
                pnd_Processing_Literal.setValue("SUCCESSFUL");                                                                                                            //Natural: MOVE 'SUCCESSFUL' TO #PROCESSING-LITERAL
                pnd_Tot_Ok_Awd_Updates.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOT-OK-AWD-UPDATES
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Rtrn_Cd_As400.equals("1")))                                                                                //Natural: IF #RTRN-CD-AS400 = '1'
                {
                    pnd_Processing_Literal.setValue("FAILED");                                                                                                            //Natural: MOVE 'FAILED' TO #PROCESSING-LITERAL
                    if (condition(pnd_Tot_Ng_Awd_Updates.equals(getZero())))                                                                                              //Natural: IF #TOT-NG-AWD-UPDATES = 0
                    {
                                                                                                                                                                          //Natural: PERFORM SUCCESSFUL-TOTALS
                        sub_Successful_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().newPage(new ReportSpecification(8));                                                                                                 //Natural: NEWPAGE ( 8 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Tot_Ng_Awd_Updates.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOT-NG-AWD-UPDATES
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Processing_Literal.setValue("ERRORAS400");                                                                                                        //Natural: MOVE 'ERRORAS400' TO #PROCESSING-LITERAL
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Grand_Total_Awd_Updates.nadd(1);                                                                                                                          //Natural: ADD 1 TO #GRAND-TOTAL-AWD-UPDATES
            pnd_Updt_Rqst_Literal.reset();                                                                                                                                //Natural: RESET #UPDT-RQST-LITERAL
            //*  #FILLER/-1:= '/'
            //*  #FILLER/-2:= '/'
            if (condition(pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Mit_Status.equals("C")))                                                                                       //Natural: IF #MIT-STATUS = 'C'
            {
                pnd_Updt_Rqst_Literal_Pnd_Status_Literal.setValue("STATUS");                                                                                              //Natural: MOVE 'STATUS' TO #STATUS-LITERAL
                pnd_Updt_Rqst_Literal_Pnd_Fillerfslash__1.setValue("/");                                                                                                  //Natural: MOVE '/' TO #FILLER/-1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Updt_Rqst_Literal_Pnd_Status_Literal.setValue(" ");                                                                                                   //Natural: MOVE ' ' TO #STATUS-LITERAL
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Mcss_Calls_Wpid_Cde.getValue(1).notEquals(" ")))                                                                                            //Natural: IF CWF-MCSS-CALLS.WPID-CDE ( 1 ) NE ' '
            {
                pnd_Updt_Rqst_Literal_Pnd_Wpid_Literal.setValue("WPID");                                                                                                  //Natural: MOVE 'WPID' TO #WPID-LITERAL
                pnd_Updt_Rqst_Literal_Pnd_Fillerfslash__2.setValue("/");                                                                                                  //Natural: MOVE '/' TO #FILLER/-2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Updt_Rqst_Literal_Pnd_Wpid_Literal.setValue(" ");                                                                                                     //Natural: MOVE ' ' TO #WPID-LITERAL
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Mcss_Calls_Ph_Ssn_Nbr.notEquals(getZero())))                                                                                                //Natural: IF CWF-MCSS-CALLS.PH-SSN-NBR NE 0
            {
                pnd_Updt_Rqst_Literal_Pnd_Ssn_Literal.setValue("SSN");                                                                                                    //Natural: MOVE 'SSN' TO #SSN-LITERAL
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Updt_Rqst_Literal_Pnd_Ssn_Literal.setValue(" ");                                                                                                      //Natural: MOVE ' ' TO #SSN-LITERAL
                //*   PIN-EXP<<
            }                                                                                                                                                             //Natural: END-IF
            if (condition(getReports().getAstLinesLeft(8).less(10)))                                                                                                      //Natural: NEWPAGE ( 8 ) IF LESS THAN 10 LINES LEFT
            {
                getReports().newPage(8);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }
            getReports().write(8, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(2),cwf_Mcss_Calls_Pin_Nbr,new TabSetting(18),pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Mit_Log_Dt_Tme,new  //Natural: WRITE ( 8 ) /// 2T CWF-MCSS-CALLS.PIN-NBR 18T #MIT-LOG-DT-TME 39T #PROCESSING-LITERAL 54T #UPDT-RQST-LITERAL 71T #MIT-STATUS 77T CWF-MCSS-CALLS.WPID-CDE ( 1 ) 86T CWF-MCSS-CALLS.PH-SSN-NBR ( EM = 999-99-9999 ) 99T #AWD-UPDATE-DT-TME 118T CWF-MCSS-CALLS.RCRD-UPLD-TME ( EM = YYYYMMDDHHIISST )
                TabSetting(39),pnd_Processing_Literal,new TabSetting(54),pnd_Updt_Rqst_Literal,new TabSetting(71),pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Mit_Status,new 
                TabSetting(77),cwf_Mcss_Calls_Wpid_Cde.getValue(1),new TabSetting(86),cwf_Mcss_Calls_Ph_Ssn_Nbr, new ReportEditMask ("999-99-9999"),new 
                TabSetting(99),pnd_Spcl_Handling_Txt_Brkdwn_Pnd_Awd_Update_Dt_Tme,new TabSetting(118),cwf_Mcss_Calls_Rcrd_Upld_Tme, new ReportEditMask ("YYYYMMDDHHIISST"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  13T #MIT-LOG-DT-TME
            //*  34T #PROCESSING-LITERAL
            //*  49T #UPDT-RQST-LITERAL
            getReports().write(8, ReportOption.NOTITLE,NEWLINE,new TabSetting(14),"CWF PROCESSING MESSAGE :",new TabSetting(40),cwf_Mcss_Calls_Error_Msg_Txt);            //Natural: WRITE ( 8 ) / 14T 'CWF PROCESSING MESSAGE :' 40T CWF-MCSS-CALLS.ERROR-MSG-TXT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        if (condition(pnd_Tot_Ng_Awd_Updates.equals(getZero())))                                                                                                          //Natural: IF #TOT-NG-AWD-UPDATES = 0
        {
                                                                                                                                                                          //Natural: PERFORM SUCCESSFUL-TOTALS
            sub_Successful_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Processing_Literal.setValue("FAILED");                                                                                                                        //Natural: MOVE 'FAILED' TO #PROCESSING-LITERAL
                                                                                                                                                                          //Natural: PERFORM FAILED-TOTALS
        sub_Failed_Totals();
        if (condition(Global.isEscape())) {return;}
        pnd_Processing_Literal.setValue("      ");                                                                                                                        //Natural: MOVE '      ' TO #PROCESSING-LITERAL
        //*  ----------------------------------------------------------------
        //* ***********************************************************************
        //*   WRITE OUT 'AWD TO CWF'  UPDATES GRAND TOTALS ON LAST PAGE OF REPORT
        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        pnd_Last_Page_Flag.setValue("Y");                                                                                                                                 //Natural: ASSIGN #LAST-PAGE-FLAG := 'Y'
        getReports().write(8, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,new  //Natural: WRITE ( 8 ) //////// 1T '-' ( 131 ) // 1T '-' ( 50 ) 52T 'AWD TO CWF UPDATE GRAND TOTALS' 83T '-' ( 49 ) // 1T '-' ( 131 ) //// / 45T 'SUCCESSFUL "AWD TO CWF" UPDATES  :' #TOT-OK-AWD-UPDATES // 45T 'FAILED     "AWD TO CWF" UPDATES  :' #TOT-NG-AWD-UPDATES / 45T '------------------------------' // 45T 'TOTAL      "AWD TO CWF" UPDATES  :' #GRAND-TOTAL-AWD-UPDATES
            TabSetting(1),"-",new RepeatItem(50),new TabSetting(52),"AWD TO CWF UPDATE GRAND TOTALS",new TabSetting(83),"-",new RepeatItem(49),NEWLINE,NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"SUCCESSFUL 'AWD TO CWF' UPDATES  :",pnd_Tot_Ok_Awd_Updates,NEWLINE,NEWLINE,new 
            TabSetting(45),"FAILED     'AWD TO CWF' UPDATES  :",pnd_Tot_Ng_Awd_Updates,NEWLINE,new TabSetting(45),"------------------------------",NEWLINE,NEWLINE,new 
            TabSetting(45),"TOTAL      'AWD TO CWF' UPDATES  :",pnd_Grand_Total_Awd_Updates);
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUCCESSFUL-TOTALS
        //* ***********************************************************************
        //*   WRITE OUT 'AWD TO CWF'  UPDATES SUCCESSFUL TOTALS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FAILED-TOTALS
        //* ***********************************************************************
        //*   WRITE OUT 'AWD TO CWF'  UPDATES SUCCESSFUL TOTALS
        //* ***********************************************************************
    }
    private void sub_Successful_Totals() throws Exception                                                                                                                 //Natural: SUCCESSFUL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        pnd_Last_Page_Flag.setValue("Y");                                                                                                                                 //Natural: ASSIGN #LAST-PAGE-FLAG := 'Y'
        getReports().write(8, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,new  //Natural: WRITE ( 8 ) //////// 1T '-' ( 131 ) // 1T '-' ( 50 ) 52T 'AWD TO CWF SUCCESSFUL TOTALS  ' 83T '-' ( 49 ) // 1T '-' ( 131 ) //// // 45T 'SUCCESSFUL "AWD TO CWF" UPDATES  :' #TOT-OK-AWD-UPDATES
            TabSetting(1),"-",new RepeatItem(50),new TabSetting(52),"AWD TO CWF SUCCESSFUL TOTALS  ",new TabSetting(83),"-",new RepeatItem(49),NEWLINE,NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"SUCCESSFUL 'AWD TO CWF' UPDATES  :",
            pnd_Tot_Ok_Awd_Updates);
        if (Global.isEscape()) return;
    }
    private void sub_Failed_Totals() throws Exception                                                                                                                     //Natural: FAILED-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        pnd_Last_Page_Flag.setValue("Y");                                                                                                                                 //Natural: ASSIGN #LAST-PAGE-FLAG := 'Y'
        getReports().write(8, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,new  //Natural: WRITE ( 8 ) //////// 1T '-' ( 131 ) // 1T '-' ( 50 ) 52T 'AWD TO CWF Failed Totals      ' 83T '-' ( 49 ) // 1T '-' ( 131 ) //// // 45T 'FAILED     "AWD TO CWF" UPDATES  :' #TOT-NG-AWD-UPDATES
            TabSetting(1),"-",new RepeatItem(50),new TabSetting(52),"AWD TO CWF Failed Totals      ",new TabSetting(83),"-",new RepeatItem(49),NEWLINE,NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"FAILED     'AWD TO CWF' UPDATES  :",
            pnd_Tot_Ng_Awd_Updates);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt8 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(8, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(42),"CORPORATE WORKFLOW SYSTEM",new                   //Natural: WRITE ( 8 ) NOTITLE 001T *PROGRAM 042T 'CORPORATE WORKFLOW SYSTEM' 101T *DATU *TIME ( AL = 005 ) '    PAGE:' *PAGE-NUMBER ( 8 ) ( AD = L ) / 025T ' "AWD TO CWF INTERFACE"  -- STATUS / WPID / SSN UPDATES --' / 033T '               FOR ' #INPUT-DTE-D ( EM = MM/DD/YY ) / 033T '              ' #PROCESSING-LITERAL / 001T '-' ( 132 ) // 054T 'TYPE OF UPDATE' 074T 'CORP' 109T 'AWD' 125T 'MIT' / 005T 'PIN' 019T 'MIT LOG DT/TME' 041T 'STATUS' 054T 'REQUESTED BY AWD' 074T 'STATUS' 083T 'WPID' 094T 'SSN' 105T 'UPDATE DT/TME' 120T 'UPDATE DT/TME' / 001T '-' ( 132 )
                        TabSetting(101),Global.getDATU(),Global.getTIME(), new AlphanumericLength (5),"    PAGE:",getReports().getPageNumberDbs(8), new 
                        FieldAttributes ("AD=L"),NEWLINE,new TabSetting(25)," 'AWD TO CWF INTERFACE'  -- STATUS / WPID / SSN UPDATES --",NEWLINE,new TabSetting(33),"               FOR ",pnd_Input_Dte_D, 
                        new ReportEditMask ("MM/DD/YY"),NEWLINE,new TabSetting(33),"              ",pnd_Processing_Literal,NEWLINE,new TabSetting(1),"-",new 
                        RepeatItem(132),NEWLINE,NEWLINE,new TabSetting(54),"TYPE OF UPDATE",new TabSetting(74),"CORP",new TabSetting(109),"AWD",new TabSetting(125),"MIT",NEWLINE,new 
                        TabSetting(5),"PIN",new TabSetting(19),"MIT LOG DT/TME",new TabSetting(41),"STATUS",new TabSetting(54),"REQUESTED BY AWD",new TabSetting(74),"STATUS",new 
                        TabSetting(83),"WPID",new TabSetting(94),"SSN",new TabSetting(105),"UPDATE DT/TME",new TabSetting(120),"UPDATE DT/TME",NEWLINE,new 
                        TabSetting(1),"-",new RepeatItem(132));
                    //*  // 050T 'TYPE OF UPDATE'
                    //*    070T 'CORP'
                    //*    105T 'AWD'
                    //*    123T 'MIT'
                    //*    014T 'MIT LOG DT/TME'
                    //*    036T 'STATUS'
                    //*    049T 'REQUESTED BY AWD'
                    //*    069T 'STATUS'
                    //*    078T 'WPID'
                    //*    089T 'SSN'
                    //*    100T 'UPDATE DT/TME'
                    //*    118T 'UPDATE DT/TME'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(8, "PS=60 LS=133 ZP=OFF");
    }
}
