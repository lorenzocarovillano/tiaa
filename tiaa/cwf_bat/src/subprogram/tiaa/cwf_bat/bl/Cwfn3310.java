/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:26:13 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn3310
************************************************************
**        * FILE NAME            : Cwfn3310.java
**        * CLASS NAME           : Cwfn3310
**        * INSTANCE NAME        : Cwfn3310
************************************************************
************************************************************************
* PROGRAM  : CWFN3310
* SYSTEM   : CRPCWF
* TITLE    : PROCESS SUBPROGRAM
* GENERATED: OCT 05,93 AT 08:41 AM
* FUNCTION : THIS MODULE COMPUTES THE
*          | CALENDAR AND BUSINESS DAYS, AND
*          | EXTRACTS THE DOC RECEIVED DATE
*          | AND THE DATE RECEIVED IN UNIT
*          | BASED ON THE VALUES OF THE
*          | STATUS CODES, AND COMPUTES
*          | THE PEND DAYS (INTERNAL 900 - 999, 4000 - 4449)
*          | THIS PROGRAM IS CALLED BY CWFB3012 - REPORT12
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* 11/03/98 | AS  - RESTOWED TO INCLUDE INTEGRATED FIELDS IN CWFA5012
*                                                         & CWFA1800
* 06/14/00 | JF  - RESTOWED WITH NEW VERSION OF CWFA1800
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn3310 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCwfa1800 pdaCwfa1800;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Last_Chnge_Invrt_Dte_Tme;
    private DbsField pnd_Todays_Time;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Return_Msg;
    private DbsField pnd_Return_Clndr_Days;
    private DbsField pnd_Return_Doc_Rec_Dte_Tme;
    private DbsField pnd_Return_Rcvd_Dte_Tme;
    private DbsField pnd_Return_Bsnss_Days;
    private DbsField pnd_Start_History_Value;

    private DbsGroup pnd_Start_History_Value__R_Field_1;
    private DbsField pnd_Start_History_Value_Pnd_Start_Rqst_Log_Dte_Tme;
    private DbsField pnd_Start_History_Value_Pnd_Start_Last_Chnge_Invrt_Dte_Tme;
    private DbsField pnd_Return_Pend_Days;
    private DbsField pnd_Return_Ext_Pend_Days;
    private DbsField pnd_Return_Clndr_Daysx;
    private DbsField pnd_Return_Bsnss_Daysx;
    private DbsField pnd_Return_Pend_Daysx;
    private DbsField pnd_Return_Ext_Pend_Daysx;

    private DataAccessProgramView vw_history_View;
    private DbsField history_View_Rqst_Log_Dte_Tme;
    private DbsField history_View_Last_Chnge_Dte_Tme;
    private DbsField history_View_Last_Chnge_Invrt_Dte_Tme;
    private DbsField history_View_Admin_Status_Updte_Dte_Tme;
    private DbsField history_View_Last_Chnge_Unit_Cde;
    private DbsField history_View_Admin_Status_Cde;
    private DbsField history_View_Last_Updte_Dte;
    private DbsField history_View_Last_Updte_Dte_Tme;
    private DbsField history_View_Actve_Ind;
    private DbsField history_View_Rqst_Id;

    private DbsGroup history_View__R_Field_2;
    private DbsField history_View_Rqst_Work_Prcss_Id;
    private DbsField history_View_Rqst_Tiaa_Rcvd_Dte;
    private DbsField history_View_Rqst_Case_Id_Cde;
    private DbsField history_View_Rqst_Pin_Nbr;
    private DbsField pnd_Start_Date;
    private DbsField pnd_End_Date;
    private DbsField pnd_Start_Date_A;
    private DbsField pnd_End_Date_A;
    private DbsField pnd_No_Work_Days;
    private DbsField pnd_End_Time;
    private DbsField pnd_Rcvd_Dte_Tme;
    private DbsField pnd_Rcvd_Dte_Tmex;
    private DbsField pnd_Work_Date_D;
    private DbsField pnd_Work_Date_Time_A;

    private DbsGroup pnd_Work_Date_Time_A__R_Field_3;
    private DbsField pnd_Work_Date_Time_A_Pnd_Work_Date_Time_N;
    private DbsField pnd_Work_Date_Time;
    private DbsField pnd_Work_Date_A;
    private DbsField pnd_Frdate;
    private DbsField pnd_Todate;
    private DbsField pnd_Todays_Date;
    private DbsField pnd_Rqst_Tiaa_Rcvd_Dte;
    private DbsField pnd_Save_Unit_Cde;
    private DbsField pnd_Save_Admin_Status_Updte_Dte_Tme;
    private DbsField pnd_Save_Last_Chnge_Dte_Tme;

    private DbsGroup pnd_Save_Last_Chnge_Dte_Tme__R_Field_4;
    private DbsField pnd_Save_Last_Chnge_Dte_Tme_Pnd_Save_Last_Chnge_Dte;
    private DbsField pnd_Last_Chnge_Dte_Tme;

    private DbsGroup pnd_Last_Chnge_Dte_Tme__R_Field_5;
    private DbsField pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte_Hhmm;
    private DbsField pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Iit;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaCwfa1800 = new PdaCwfa1800(parameters);
        pnd_Rqst_Log_Dte_Tme = parameters.newFieldInRecord("pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Rqst_Log_Dte_Tme.setParameterOption(ParameterOption.ByReference);
        pnd_Last_Chnge_Invrt_Dte_Tme = parameters.newFieldInRecord("pnd_Last_Chnge_Invrt_Dte_Tme", "#LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Last_Chnge_Invrt_Dte_Tme.setParameterOption(ParameterOption.ByReference);
        pnd_Todays_Time = parameters.newFieldInRecord("pnd_Todays_Time", "#TODAYS-TIME", FieldType.TIME);
        pnd_Todays_Time.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Code = parameters.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 1);
        pnd_Return_Code.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Msg = parameters.newFieldInRecord("pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 20);
        pnd_Return_Msg.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Clndr_Days = parameters.newFieldInRecord("pnd_Return_Clndr_Days", "#RETURN-CLNDR-DAYS", FieldType.NUMERIC, 4);
        pnd_Return_Clndr_Days.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Doc_Rec_Dte_Tme = parameters.newFieldInRecord("pnd_Return_Doc_Rec_Dte_Tme", "#RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        pnd_Return_Doc_Rec_Dte_Tme.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Rcvd_Dte_Tme = parameters.newFieldInRecord("pnd_Return_Rcvd_Dte_Tme", "#RETURN-RCVD-DTE-TME", FieldType.TIME);
        pnd_Return_Rcvd_Dte_Tme.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Bsnss_Days = parameters.newFieldInRecord("pnd_Return_Bsnss_Days", "#RETURN-BSNSS-DAYS", FieldType.NUMERIC, 6, 1);
        pnd_Return_Bsnss_Days.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Start_History_Value = localVariables.newFieldInRecord("pnd_Start_History_Value", "#START-HISTORY-VALUE", FieldType.STRING, 30);

        pnd_Start_History_Value__R_Field_1 = localVariables.newGroupInRecord("pnd_Start_History_Value__R_Field_1", "REDEFINE", pnd_Start_History_Value);
        pnd_Start_History_Value_Pnd_Start_Rqst_Log_Dte_Tme = pnd_Start_History_Value__R_Field_1.newFieldInGroup("pnd_Start_History_Value_Pnd_Start_Rqst_Log_Dte_Tme", 
            "#START-RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Start_History_Value_Pnd_Start_Last_Chnge_Invrt_Dte_Tme = pnd_Start_History_Value__R_Field_1.newFieldInGroup("pnd_Start_History_Value_Pnd_Start_Last_Chnge_Invrt_Dte_Tme", 
            "#START-LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Return_Pend_Days = localVariables.newFieldInRecord("pnd_Return_Pend_Days", "#RETURN-PEND-DAYS", FieldType.NUMERIC, 5, 1);
        pnd_Return_Ext_Pend_Days = localVariables.newFieldInRecord("pnd_Return_Ext_Pend_Days", "#RETURN-EXT-PEND-DAYS", FieldType.NUMERIC, 5, 1);
        pnd_Return_Clndr_Daysx = localVariables.newFieldInRecord("pnd_Return_Clndr_Daysx", "#RETURN-CLNDR-DAYSX", FieldType.NUMERIC, 18);
        pnd_Return_Bsnss_Daysx = localVariables.newFieldInRecord("pnd_Return_Bsnss_Daysx", "#RETURN-BSNSS-DAYSX", FieldType.NUMERIC, 19, 1);
        pnd_Return_Pend_Daysx = localVariables.newFieldInRecord("pnd_Return_Pend_Daysx", "#RETURN-PEND-DAYSX", FieldType.NUMERIC, 19, 1);
        pnd_Return_Ext_Pend_Daysx = localVariables.newFieldInRecord("pnd_Return_Ext_Pend_Daysx", "#RETURN-EXT-PEND-DAYSX", FieldType.NUMERIC, 19, 1);

        vw_history_View = new DataAccessProgramView(new NameInfo("vw_history_View", "HISTORY-VIEW"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        history_View_Rqst_Log_Dte_Tme = vw_history_View.getRecord().newFieldInGroup("history_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        history_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        history_View_Last_Chnge_Dte_Tme = vw_history_View.getRecord().newFieldInGroup("history_View_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        history_View_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        history_View_Last_Chnge_Invrt_Dte_Tme = vw_history_View.getRecord().newFieldInGroup("history_View_Last_Chnge_Invrt_Dte_Tme", "LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        history_View_Admin_Status_Updte_Dte_Tme = vw_history_View.getRecord().newFieldInGroup("history_View_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        history_View_Last_Chnge_Unit_Cde = vw_history_View.getRecord().newFieldInGroup("history_View_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        history_View_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        history_View_Admin_Status_Cde = vw_history_View.getRecord().newFieldInGroup("history_View_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        history_View_Last_Updte_Dte = vw_history_View.getRecord().newFieldInGroup("history_View_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        history_View_Last_Updte_Dte_Tme = vw_history_View.getRecord().newFieldInGroup("history_View_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        history_View_Actve_Ind = vw_history_View.getRecord().newFieldInGroup("history_View_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        history_View_Rqst_Id = vw_history_View.getRecord().newFieldInGroup("history_View_Rqst_Id", "RQST-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, 
            "RQST_ID");
        history_View_Rqst_Id.setDdmHeader("REQUEST ID");

        history_View__R_Field_2 = vw_history_View.getRecord().newGroupInGroup("history_View__R_Field_2", "REDEFINE", history_View_Rqst_Id);
        history_View_Rqst_Work_Prcss_Id = history_View__R_Field_2.newFieldInGroup("history_View_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        history_View_Rqst_Tiaa_Rcvd_Dte = history_View__R_Field_2.newFieldInGroup("history_View_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", FieldType.STRING, 
            8);
        history_View_Rqst_Case_Id_Cde = history_View__R_Field_2.newFieldInGroup("history_View_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", FieldType.STRING, 
            2);
        history_View_Rqst_Pin_Nbr = history_View__R_Field_2.newFieldInGroup("history_View_Rqst_Pin_Nbr", "RQST-PIN-NBR", FieldType.NUMERIC, 12);
        registerRecord(vw_history_View);

        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.DATE);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.DATE);
        pnd_Start_Date_A = localVariables.newFieldInRecord("pnd_Start_Date_A", "#START-DATE-A", FieldType.STRING, 8);
        pnd_End_Date_A = localVariables.newFieldInRecord("pnd_End_Date_A", "#END-DATE-A", FieldType.STRING, 8);
        pnd_No_Work_Days = localVariables.newFieldInRecord("pnd_No_Work_Days", "#NO-WORK-DAYS", FieldType.NUMERIC, 18);
        pnd_End_Time = localVariables.newFieldInRecord("pnd_End_Time", "#END-TIME", FieldType.TIME);
        pnd_Rcvd_Dte_Tme = localVariables.newFieldInRecord("pnd_Rcvd_Dte_Tme", "#RCVD-DTE-TME", FieldType.TIME);
        pnd_Rcvd_Dte_Tmex = localVariables.newFieldInRecord("pnd_Rcvd_Dte_Tmex", "#RCVD-DTE-TMEX", FieldType.TIME);
        pnd_Work_Date_D = localVariables.newFieldInRecord("pnd_Work_Date_D", "#WORK-DATE-D", FieldType.DATE);
        pnd_Work_Date_Time_A = localVariables.newFieldInRecord("pnd_Work_Date_Time_A", "#WORK-DATE-TIME-A", FieldType.STRING, 15);

        pnd_Work_Date_Time_A__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_Date_Time_A__R_Field_3", "REDEFINE", pnd_Work_Date_Time_A);
        pnd_Work_Date_Time_A_Pnd_Work_Date_Time_N = pnd_Work_Date_Time_A__R_Field_3.newFieldInGroup("pnd_Work_Date_Time_A_Pnd_Work_Date_Time_N", "#WORK-DATE-TIME-N", 
            FieldType.NUMERIC, 15);
        pnd_Work_Date_Time = localVariables.newFieldInRecord("pnd_Work_Date_Time", "#WORK-DATE-TIME", FieldType.TIME);
        pnd_Work_Date_A = localVariables.newFieldInRecord("pnd_Work_Date_A", "#WORK-DATE-A", FieldType.STRING, 8);
        pnd_Frdate = localVariables.newFieldInRecord("pnd_Frdate", "#FRDATE", FieldType.DATE);
        pnd_Todate = localVariables.newFieldInRecord("pnd_Todate", "#TODATE", FieldType.DATE);
        pnd_Todays_Date = localVariables.newFieldInRecord("pnd_Todays_Date", "#TODAYS-DATE", FieldType.DATE);
        pnd_Rqst_Tiaa_Rcvd_Dte = localVariables.newFieldInRecord("pnd_Rqst_Tiaa_Rcvd_Dte", "#RQST-TIAA-RCVD-DTE", FieldType.NUMERIC, 8);
        pnd_Save_Unit_Cde = localVariables.newFieldInRecord("pnd_Save_Unit_Cde", "#SAVE-UNIT-CDE", FieldType.STRING, 8);
        pnd_Save_Admin_Status_Updte_Dte_Tme = localVariables.newFieldInRecord("pnd_Save_Admin_Status_Updte_Dte_Tme", "#SAVE-ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME);
        pnd_Save_Last_Chnge_Dte_Tme = localVariables.newFieldInRecord("pnd_Save_Last_Chnge_Dte_Tme", "#SAVE-LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);

        pnd_Save_Last_Chnge_Dte_Tme__R_Field_4 = localVariables.newGroupInRecord("pnd_Save_Last_Chnge_Dte_Tme__R_Field_4", "REDEFINE", pnd_Save_Last_Chnge_Dte_Tme);
        pnd_Save_Last_Chnge_Dte_Tme_Pnd_Save_Last_Chnge_Dte = pnd_Save_Last_Chnge_Dte_Tme__R_Field_4.newFieldInGroup("pnd_Save_Last_Chnge_Dte_Tme_Pnd_Save_Last_Chnge_Dte", 
            "#SAVE-LAST-CHNGE-DTE", FieldType.NUMERIC, 8);
        pnd_Last_Chnge_Dte_Tme = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_Tme", "#LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);

        pnd_Last_Chnge_Dte_Tme__R_Field_5 = localVariables.newGroupInRecord("pnd_Last_Chnge_Dte_Tme__R_Field_5", "REDEFINE", pnd_Last_Chnge_Dte_Tme);
        pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte_Hhmm = pnd_Last_Chnge_Dte_Tme__R_Field_5.newFieldInGroup("pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte_Hhmm", 
            "#LAST-CHNGE-DTE-HHMM", FieldType.NUMERIC, 12);
        pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Iit = pnd_Last_Chnge_Dte_Tme__R_Field_5.newFieldInGroup("pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Iit", "#LAST-CHNGE-IIT", 
            FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_history_View.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn3310() throws Exception
    {
        super("Cwfn3310");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFN3310", onError);
                                                                                                                                                                          //Natural: PERFORM GET-RECEIVED-DOC-PEND-DATE
        sub_Get_Received_Doc_Pend_Date();
        if (condition(Global.isEscape())) {return;}
        //*  IF NO RECEIVED DATE
        if (condition(pnd_Return_Rcvd_Dte_Tme.equals(getZero())))                                                                                                         //Natural: IF #RETURN-RCVD-DTE-TME = 0
        {
            pnd_Return_Rcvd_Dte_Tme.setValue(pnd_Save_Admin_Status_Updte_Dte_Tme);                                                                                        //Natural: MOVE #SAVE-ADMIN-STATUS-UPDTE-DTE-TME TO #RETURN-RCVD-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-CALENDAR-DAYS
        sub_Get_Calendar_Days();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-BUSINESS-DAYS
        sub_Get_Business_Days();
        if (condition(Global.isEscape())) {return;}
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RECEIVED-DOC-PEND-DATE
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CALENDAR-DAYS
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BUSINESS-DAYS
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Get_Received_Doc_Pend_Date() throws Exception                                                                                                        //Natural: GET-RECEIVED-DOC-PEND-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Return_Rcvd_Dte_Tme.reset();                                                                                                                                  //Natural: RESET #RETURN-RCVD-DTE-TME #RETURN-DOC-REC-DTE-TME #RETURN-PEND-DAYSX #RETURN-EXT-PEND-DAYSX #RETURN-CLNDR-DAYS #RETURN-BSNSS-DAYS #WORK-DATE-TIME-A
        pnd_Return_Doc_Rec_Dte_Tme.reset();
        pnd_Return_Pend_Daysx.reset();
        pnd_Return_Ext_Pend_Daysx.reset();
        pnd_Return_Clndr_Days.reset();
        pnd_Return_Bsnss_Days.reset();
        pnd_Work_Date_Time_A.reset();
        pnd_Work_Date_A.setValueEdited(pnd_Todays_Time,new ReportEditMask("YYYYMMDD"));                                                                                   //Natural: MOVE EDITED #TODAYS-TIME ( EM = YYYYMMDD ) TO #WORK-DATE-A
        pnd_Todays_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_Date_A);                                                                                   //Natural: MOVE EDITED #WORK-DATE-A TO #TODAYS-DATE ( EM = YYYYMMDD )
        pnd_Start_History_Value_Pnd_Start_Rqst_Log_Dte_Tme.setValue(pnd_Rqst_Log_Dte_Tme);                                                                                //Natural: MOVE #RQST-LOG-DTE-TME TO #START-RQST-LOG-DTE-TME
        pnd_Start_History_Value_Pnd_Start_Last_Chnge_Invrt_Dte_Tme.setValue(pnd_Last_Chnge_Invrt_Dte_Tme);                                                                //Natural: MOVE #LAST-CHNGE-INVRT-DTE-TME TO #START-LAST-CHNGE-INVRT-DTE-TME
        vw_history_View.startDatabaseRead                                                                                                                                 //Natural: READ HISTORY-VIEW BY RQST-HISTORY-KEY FROM #START-HISTORY-VALUE
        (
        "PND_PND_L1180",
        new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", pnd_Start_History_Value, WcType.BY) },
        new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
        );
        PND_PND_L1180:
        while (condition(vw_history_View.readNextRow("PND_PND_L1180")))
        {
            if (condition(history_View_Rqst_Log_Dte_Tme.notEquals(pnd_Rqst_Log_Dte_Tme)))                                                                                 //Natural: IF HISTORY-VIEW.RQST-LOG-DTE-TME NE #RQST-LOG-DTE-TME
            {
                if (true) break PND_PND_L1180;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L1180. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(history_View_Last_Chnge_Unit_Cde.notEquals(pdaCwfa1800.getCwfa1800_Last_Chnge_Unit_Cde())))                                                     //Natural: IF HISTORY-VIEW.LAST-CHNGE-UNIT-CDE NE CWFA1800.LAST-CHNGE-UNIT-CDE
            {
                if (true) break PND_PND_L1180;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L1180. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Last_Chnge_Dte_Tme.setValue(history_View_Last_Chnge_Dte_Tme);                                                                                             //Natural: MOVE HISTORY-VIEW.LAST-CHNGE-DTE-TME TO #LAST-CHNGE-DTE-TME
            //*  60 SECOND
            if (condition(pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Iit.equals(600)))                                                                                         //Natural: IF #LAST-CHNGE-IIT = 600
            {
                history_View_Last_Chnge_Dte_Tme.nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM HISTORY-VIEW.LAST-CHNGE-DTE-TME
            }                                                                                                                                                             //Natural: END-IF
            pnd_Save_Admin_Status_Updte_Dte_Tme.setValue(history_View_Admin_Status_Updte_Dte_Tme);                                                                        //Natural: MOVE HISTORY-VIEW.ADMIN-STATUS-UPDTE-DTE-TME TO #SAVE-ADMIN-STATUS-UPDTE-DTE-TME
            //*  RECEIVED IN UNIT DATE ROUTINE
            short decideConditionsMet623 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF HISTORY-VIEW.ADMIN-STATUS-CDE;//Natural: VALUE '1000':'1999'
            if (condition((history_View_Admin_Status_Cde.equals("1000' : '1999"))))
            {
                decideConditionsMet623++;
                pnd_Work_Date_Time_A.setValue(history_View_Last_Chnge_Dte_Tme);                                                                                           //Natural: MOVE HISTORY-VIEW.LAST-CHNGE-DTE-TME TO #WORK-DATE-TIME-A
                pnd_Return_Rcvd_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Work_Date_Time_A);                                                       //Natural: MOVE EDITED #WORK-DATE-TIME-A TO #RETURN-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST )
            }                                                                                                                                                             //Natural: VALUE '2100'
            else if (condition((history_View_Admin_Status_Cde.equals("2100"))))
            {
                decideConditionsMet623++;
                pnd_Work_Date_Time_A.setValue(history_View_Last_Chnge_Dte_Tme);                                                                                           //Natural: MOVE HISTORY-VIEW.LAST-CHNGE-DTE-TME TO #WORK-DATE-TIME-A
                pnd_Return_Rcvd_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Work_Date_Time_A);                                                       //Natural: MOVE EDITED #WORK-DATE-TIME-A TO #RETURN-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST )
            }                                                                                                                                                             //Natural: VALUE '5000':'5999'
            else if (condition((history_View_Admin_Status_Cde.equals("5000' : '5999"))))
            {
                decideConditionsMet623++;
                pnd_Work_Date_Time_A.setValue(history_View_Last_Chnge_Dte_Tme);                                                                                           //Natural: MOVE HISTORY-VIEW.LAST-CHNGE-DTE-TME TO #WORK-DATE-TIME-A
                pnd_Return_Rcvd_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Work_Date_Time_A);                                                       //Natural: MOVE EDITED #WORK-DATE-TIME-A TO #RETURN-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  RETURN DOCUMENT DATE ROUTINE
            short decideConditionsMet639 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF HISTORY-VIEW.ADMIN-STATUS-CDE;//Natural: VALUE '0200'
            if (condition((history_View_Admin_Status_Cde.equals("0200"))))
            {
                decideConditionsMet639++;
                pnd_Work_Date_Time_A.setValue(history_View_Last_Chnge_Dte_Tme);                                                                                           //Natural: MOVE HISTORY-VIEW.LAST-CHNGE-DTE-TME TO #WORK-DATE-TIME-A
                pnd_Return_Doc_Rec_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Work_Date_Time_A);                                                    //Natural: MOVE EDITED #WORK-DATE-TIME-A TO #RETURN-DOC-REC-DTE-TME ( EM = YYYYMMDDHHIISST )
            }                                                                                                                                                             //Natural: VALUE '5500':'5999'
            else if (condition((history_View_Admin_Status_Cde.equals("5500' : '5999"))))
            {
                decideConditionsMet639++;
                pnd_Work_Date_Time_A.setValue(history_View_Last_Chnge_Dte_Tme);                                                                                           //Natural: MOVE HISTORY-VIEW.LAST-CHNGE-DTE-TME TO #WORK-DATE-TIME-A
                pnd_Return_Doc_Rec_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Work_Date_Time_A);                                                    //Natural: MOVE EDITED #WORK-DATE-TIME-A TO #RETURN-DOC-REC-DTE-TME ( EM = YYYYMMDDHHIISST )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  PEND DAYS ROUTINE
            if (condition(pnd_Return_Pend_Daysx.equals(getZero())))                                                                                                       //Natural: IF #RETURN-PEND-DAYSX = 0
            {
                short decideConditionsMet653 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF HISTORY-VIEW.ADMIN-STATUS-CDE;//Natural: VALUE '0900':'0999'
                if (condition((history_View_Admin_Status_Cde.equals("0900' : '0999"))))
                {
                    decideConditionsMet653++;
                    pnd_Work_Date_Time_A.setValue(history_View_Last_Chnge_Dte_Tme);                                                                                       //Natural: MOVE HISTORY-VIEW.LAST-CHNGE-DTE-TME TO #WORK-DATE-TIME-A
                    pnd_Rcvd_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Work_Date_Time_A);                                                          //Natural: MOVE EDITED #WORK-DATE-TIME-A TO #RCVD-DTE-TME ( EM = YYYYMMDDHHIISST )
                    pnd_Return_Pend_Daysx.compute(new ComputeParameters(false, pnd_Return_Pend_Daysx), (pnd_Todays_Time.subtract(pnd_Rcvd_Dte_Tme)));                     //Natural: COMPUTE #RETURN-PEND-DAYSX = ( #TODAYS-TIME - #RCVD-DTE-TME )
                    pnd_Return_Pend_Days.compute(new ComputeParameters(false, pnd_Return_Pend_Days), pnd_Return_Pend_Daysx.divide(900000));                               //Natural: COMPUTE #RETURN-PEND-DAYS = #RETURN-PEND-DAYSX / 900000
                }                                                                                                                                                         //Natural: VALUE '4000':'4449'
                else if (condition((history_View_Admin_Status_Cde.equals("4000' : '4449"))))
                {
                    decideConditionsMet653++;
                    pnd_Work_Date_Time_A.setValue(history_View_Last_Chnge_Dte_Tme);                                                                                       //Natural: MOVE HISTORY-VIEW.LAST-CHNGE-DTE-TME TO #WORK-DATE-TIME-A
                    pnd_Rcvd_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Work_Date_Time_A);                                                          //Natural: MOVE EDITED #WORK-DATE-TIME-A TO #RCVD-DTE-TME ( EM = YYYYMMDDHHIISST )
                    pnd_Return_Pend_Daysx.compute(new ComputeParameters(false, pnd_Return_Pend_Daysx), (pnd_Todays_Time.subtract(pnd_Rcvd_Dte_Tme)));                     //Natural: COMPUTE #RETURN-PEND-DAYSX = ( #TODAYS-TIME - #RCVD-DTE-TME )
                    pnd_Return_Pend_Days.compute(new ComputeParameters(false, pnd_Return_Pend_Days), pnd_Return_Pend_Daysx.divide(900000));                               //Natural: COMPUTE #RETURN-PEND-DAYS = #RETURN-PEND-DAYSX / 900000
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Return_Ext_Pend_Daysx.equals(getZero())))                                                                                                   //Natural: IF #RETURN-EXT-PEND-DAYSX = 0
            {
                if (condition(history_View_Admin_Status_Cde.greaterOrEqual("4500") && history_View_Admin_Status_Cde.lessOrEqual("4997")))                                 //Natural: IF HISTORY-VIEW.ADMIN-STATUS-CDE GE '4500' AND HISTORY-VIEW.ADMIN-STATUS-CDE LE '4997'
                {
                    pnd_Work_Date_Time_A.setValue(history_View_Last_Chnge_Dte_Tme);                                                                                       //Natural: MOVE HISTORY-VIEW.LAST-CHNGE-DTE-TME TO #WORK-DATE-TIME-A
                    pnd_Rcvd_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Work_Date_Time_A);                                                          //Natural: MOVE EDITED #WORK-DATE-TIME-A TO #RCVD-DTE-TME ( EM = YYYYMMDDHHIISST )
                    pnd_Return_Ext_Pend_Daysx.compute(new ComputeParameters(false, pnd_Return_Ext_Pend_Daysx), (pnd_Todays_Time.subtract(pnd_Rcvd_Dte_Tme)));             //Natural: COMPUTE #RETURN-EXT-PEND-DAYSX = ( #TODAYS-TIME - #RCVD-DTE-TME )
                    pnd_Return_Ext_Pend_Days.compute(new ComputeParameters(false, pnd_Return_Ext_Pend_Days), pnd_Return_Ext_Pend_Daysx.divide(900000));                   //Natural: COMPUTE #RETURN-EXT-PEND-DAYS = #RETURN-EXT-PEND-DAYSX / 900000
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Calendar_Days() throws Exception                                                                                                                 //Natural: GET-CALENDAR-DAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Return_Doc_Rec_Dte_Tme.greater(getZero())))                                                                                                     //Natural: IF #RETURN-DOC-REC-DTE-TME GT 0
        {
            pnd_Work_Date_A.setValueEdited(pnd_Return_Doc_Rec_Dte_Tme,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED #RETURN-DOC-REC-DTE-TME ( EM = YYYYMMDD ) TO #WORK-DATE-A
            pnd_Work_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_Date_A);                                                                               //Natural: MOVE EDITED #WORK-DATE-A TO #WORK-DATE-D ( EM = YYYYMMDD )
            pnd_Return_Clndr_Days.compute(new ComputeParameters(false, pnd_Return_Clndr_Days), pnd_Todays_Date.subtract(pnd_Work_Date_D));                                //Natural: COMPUTE #RETURN-CLNDR-DAYS = #TODAYS-DATE - #WORK-DATE-D
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pdaCwfa1800.getCwfa1800_Rqst_Tiaa_Rcvd_Dte());                                                  //Natural: MOVE EDITED CWFA1800.RQST-TIAA-RCVD-DTE TO #WORK-DATE-D ( EM = YYYYMMDD )
            pnd_Return_Clndr_Days.compute(new ComputeParameters(false, pnd_Return_Clndr_Days), pnd_Todays_Date.subtract(pnd_Work_Date_D));                                //Natural: COMPUTE #RETURN-CLNDR-DAYS = #TODAYS-DATE - #WORK-DATE-D
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Business_Days() throws Exception                                                                                                                 //Natural: GET-BUSINESS-DAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Return_Doc_Rec_Dte_Tme.greater(getZero())))                                                                                                     //Natural: IF #RETURN-DOC-REC-DTE-TME GT 0
        {
            pnd_End_Time.setValue(pnd_Return_Doc_Rec_Dte_Tme);                                                                                                            //Natural: MOVE #RETURN-DOC-REC-DTE-TME TO #END-TIME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_End_Time.setValue(pnd_Return_Rcvd_Dte_Tme);                                                                                                               //Natural: MOVE #RETURN-RCVD-DTE-TME TO #END-TIME
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_End_Time.greater(getZero())))                                                                                                                   //Natural: IF #END-TIME GT 0
        {
            pnd_Return_Bsnss_Daysx.compute(new ComputeParameters(false, pnd_Return_Bsnss_Daysx), (pnd_Todays_Time.subtract(pnd_End_Time)));                               //Natural: COMPUTE #RETURN-BSNSS-DAYSX = ( #TODAYS-TIME - #END-TIME )
            pnd_Return_Bsnss_Days.compute(new ComputeParameters(false, pnd_Return_Bsnss_Days), (pnd_Return_Bsnss_Daysx.divide(900000)));                                  //Natural: COMPUTE #RETURN-BSNSS-DAYS = ( #RETURN-BSNSS-DAYSX / 900000 )
            if (condition(pnd_Return_Bsnss_Days.greater(getZero())))                                                                                                      //Natural: IF #RETURN-BSNSS-DAYS GT 0
            {
                pnd_Start_Date_A.setValueEdited(pnd_End_Time,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED #END-TIME ( EM = YYYYMMDD ) TO #START-DATE-A
                pnd_End_Date_A.setValueEdited(pnd_Todays_Time,new ReportEditMask("YYYYMMDD"));                                                                            //Natural: MOVE EDITED #TODAYS-TIME ( EM = YYYYMMDD ) TO #END-DATE-A
                pnd_Start_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Start_Date_A);                                                                           //Natural: MOVE EDITED #START-DATE-A TO #START-DATE ( EM = YYYYMMDD )
                pnd_End_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_End_Date_A);                                                                               //Natural: MOVE EDITED #END-DATE-A TO #END-DATE ( EM = YYYYMMDD )
                DbsUtil.callnat(Cwfn3901.class , getCurrentProcessState(), pnd_Start_Date, pnd_End_Date, pnd_No_Work_Days);                                               //Natural: CALLNAT 'CWFN3901' #START-DATE #END-DATE #NO-WORK-DAYS
                if (condition(Global.isEscape())) return;
                pnd_Return_Bsnss_Days.compute(new ComputeParameters(false, pnd_Return_Bsnss_Days), pnd_Return_Bsnss_Days.subtract(pnd_Return_Pend_Days).subtract(pnd_No_Work_Days)); //Natural: COMPUTE #RETURN-BSNSS-DAYS = #RETURN-BSNSS-DAYS - #RETURN-PEND-DAYS - #NO-WORK-DAYS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(1, "PROGRAM       :",Global.getPROGRAM(),NEWLINE,"ERROR NO      :",Global.getERROR_NR(), new FieldAttributes ("AD=ODR"),NEWLINE,"LINE NUMBER   :",Global.getERROR_LINE(),  //Natural: WRITE ( 1 ) 'PROGRAM       :' *PROGRAM / 'ERROR NO      :' *ERROR-NR ( AD = ODR ) / 'LINE NUMBER   :' *ERROR-LINE ( AD = ODR ) / 'CHNGE DTE     :' HISTORY-VIEW.LAST-CHNGE-DTE-TME / 'PIN-NBR       :' HISTORY-VIEW.RQST-PIN-NBR / 'UNIT          :' HISTORY-VIEW.LAST-CHNGE-UNIT-CDE / 'STATUS        :' HISTORY-VIEW.ADMIN-STATUS-CDE
            new FieldAttributes ("AD=ODR"),NEWLINE,"CHNGE DTE     :",history_View_Last_Chnge_Dte_Tme,NEWLINE,"PIN-NBR       :",history_View_Rqst_Pin_Nbr,
            NEWLINE,"UNIT          :",history_View_Last_Chnge_Unit_Cde,NEWLINE,"STATUS        :",history_View_Admin_Status_Cde);
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
    };                                                                                                                                                                    //Natural: END-ERROR
}
