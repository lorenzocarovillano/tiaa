/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:29:07 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn4033
************************************************************
**        * FILE NAME            : Cwfn4033.java
**        * CLASS NAME           : Cwfn4033
**        * INSTANCE NAME        : Cwfn4033
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: DIVISION TABLE
**SAG SYSTEM: CRPCWF
**SAG REPORT-HEADING(1): CWF DIVISION CODES EXTRACT REPORT
**SAG PRINT-FILE(1): 1
**SAG HEADING-LINE: 10000000
**SAG DESCS(1): THIS PROGRAM READS THE CWF-DIV-TBL TABLE AND WRITES A
**SAG DESCS(2): WORK FILE TO BE DOWNLOADED TO THE SQL SERVER.
**SAG PRIMARY-FILE: CWF-DIV-TBL
**SAG PRIMARY-KEY: TBL-PRIME-KEY
************************************************************************
* PROGRAM  : CWFB4033
* SYSTEM   : CRPCWF
* TITLE    : DIVISION TABLE
* GENERATED: DEC 18,95 AT 11:11 AM
* FUNCTION : THIS PROGRAM READS THE CWF-DIV-TBL TABLE AND WRITES A
*            WORK FILE TO BE DOWNLOADED TO THE SQL SERVER.
*
*
* HISTORY
**SAG DEFINE EXIT CHANGE-HISTORY
* CHANGED ON DEC 18,95 BY HARGRAV FOR RELEASE ____
* >
* >
* >

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn4033 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Unit_Cde;
    private DbsField pnd_Div;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Div_Tbl;
    private DbsField cwf_Div_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Div_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Div_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Div_Tbl__R_Field_1;
    private DbsField cwf_Div_Tbl_Division;
    private DbsField cwf_Div_Tbl_Div_Fill;
    private DbsField cwf_Div_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Div_Tbl__R_Field_2;
    private DbsField cwf_Div_Tbl_Div_Name;
    private DbsField cwf_Div_Tbl_Mang_Lname;
    private DbsField cwf_Div_Tbl_Mang_Fname;
    private DbsField cwf_Div_Tbl_Mang_Title;
    private DbsField cwf_Div_Tbl_Sup_Lname;
    private DbsField cwf_Div_Tbl_Sup_Fname;
    private DbsField cwf_Div_Tbl_Tbl_Fill;

    private DbsGroup cwf_Div_Tbl__R_Field_3;
    private DbsField cwf_Div_Tbl_Unit_Cde;
    private DbsField cwf_Div_Tbl_Unit_Fill;
    private DbsField cwf_Div_Tbl_Tbl_Actve_Ind;

    private DbsGroup cwf_Div_Tbl__R_Field_4;
    private DbsField cwf_Div_Tbl_Fill_Acb770;
    private DbsField cwf_Div_Tbl_Tbl_Actve_Ind_2_2;
    private DbsField cwf_Div_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Div_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Div_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Div_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Div_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Div_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Div_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Div_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Div_Tbl_Tbl_Table_Access_Level;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_5;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // parameters
        parameters = new DbsRecord();
        pnd_Unit_Cde = parameters.newFieldInRecord("pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 8);
        pnd_Unit_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Div = parameters.newFieldInRecord("pnd_Div", "#DIV", FieldType.STRING, 6);
        pnd_Div.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Div_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Div_Tbl", "CWF-DIV-TBL"), "CWF_DIV_TBL", "CWF_DCMNT_TABLE");
        cwf_Div_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Div_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Div_Tbl_Tbl_Table_Nme = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TBL_TABLE_NME");
        cwf_Div_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Div_Tbl_Tbl_Key_Field = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "TBL_KEY_FIELD");

        cwf_Div_Tbl__R_Field_1 = vw_cwf_Div_Tbl.getRecord().newGroupInGroup("cwf_Div_Tbl__R_Field_1", "REDEFINE", cwf_Div_Tbl_Tbl_Key_Field);
        cwf_Div_Tbl_Division = cwf_Div_Tbl__R_Field_1.newFieldInGroup("cwf_Div_Tbl_Division", "DIVISION", FieldType.STRING, 6);
        cwf_Div_Tbl_Div_Fill = cwf_Div_Tbl__R_Field_1.newFieldInGroup("cwf_Div_Tbl_Div_Fill", "DIV-FILL", FieldType.STRING, 24);
        cwf_Div_Tbl_Tbl_Data_Field = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, 
            RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Div_Tbl__R_Field_2 = vw_cwf_Div_Tbl.getRecord().newGroupInGroup("cwf_Div_Tbl__R_Field_2", "REDEFINE", cwf_Div_Tbl_Tbl_Data_Field);
        cwf_Div_Tbl_Div_Name = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Div_Name", "DIV-NAME", FieldType.STRING, 30);
        cwf_Div_Tbl_Mang_Lname = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Mang_Lname", "MANG-LNAME", FieldType.STRING, 20);
        cwf_Div_Tbl_Mang_Fname = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Mang_Fname", "MANG-FNAME", FieldType.STRING, 20);
        cwf_Div_Tbl_Mang_Title = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Mang_Title", "MANG-TITLE", FieldType.STRING, 30);
        cwf_Div_Tbl_Sup_Lname = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Sup_Lname", "SUP-LNAME", FieldType.STRING, 20);
        cwf_Div_Tbl_Sup_Fname = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Sup_Fname", "SUP-FNAME", FieldType.STRING, 20);
        cwf_Div_Tbl_Tbl_Fill = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Tbl_Fill", "TBL-FILL", FieldType.STRING, 113);

        cwf_Div_Tbl__R_Field_3 = vw_cwf_Div_Tbl.getRecord().newGroupInGroup("cwf_Div_Tbl__R_Field_3", "REDEFINE", cwf_Div_Tbl_Tbl_Data_Field);
        cwf_Div_Tbl_Unit_Cde = cwf_Div_Tbl__R_Field_3.newFieldArrayInGroup("cwf_Div_Tbl_Unit_Cde", "UNIT-CDE", FieldType.STRING, 7, new DbsArrayController(1, 
            36));
        cwf_Div_Tbl_Unit_Fill = cwf_Div_Tbl__R_Field_3.newFieldInGroup("cwf_Div_Tbl_Unit_Fill", "UNIT-FILL", FieldType.STRING, 1);
        cwf_Div_Tbl_Tbl_Actve_Ind = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TBL_ACTVE_IND");

        cwf_Div_Tbl__R_Field_4 = vw_cwf_Div_Tbl.getRecord().newGroupInGroup("cwf_Div_Tbl__R_Field_4", "REDEFINE", cwf_Div_Tbl_Tbl_Actve_Ind);
        cwf_Div_Tbl_Fill_Acb770 = cwf_Div_Tbl__R_Field_4.newFieldInGroup("cwf_Div_Tbl_Fill_Acb770", "FILL-ACB770", FieldType.STRING, 1);
        cwf_Div_Tbl_Tbl_Actve_Ind_2_2 = cwf_Div_Tbl__R_Field_4.newFieldInGroup("cwf_Div_Tbl_Tbl_Actve_Ind_2_2", "TBL-ACTVE-IND-2-2", FieldType.STRING, 
            1);
        cwf_Div_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Div_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Div_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Div_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Div_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Div_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Div_Tbl_Tbl_Updte_Dte = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Div_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Div_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Div_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Div_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Div_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Div_Tbl_Tbl_Table_Rectype = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Div_Tbl_Tbl_Table_Access_Level = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Div_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");
        registerRecord(vw_cwf_Div_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 22);

        pnd_Tbl_Prime_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_5", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Div_Tbl.reset();

        ldaCdbatxa.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Tbl_Prime_Key.setInitialValue("A DIVISION-TABLE");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn4033() throws Exception
    {
        super("Cwfn4033");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        vw_cwf_Div_Tbl.startDatabaseRead                                                                                                                                  //Natural: READ CWF-DIV-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ_PRIME",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Div_Tbl.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            if (condition(cwf_Div_Tbl_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)))                                                                      //Natural: IF CWF-DIV-TBL.TBL-TABLE-NME NE #TBL-PRIME-KEY.#TBL-TABLE-NME
            {
                if (true) break READ_PRIME;                                                                                                                               //Natural: ESCAPE BOTTOM ( READ-PRIME. )
            }                                                                                                                                                             //Natural: END-IF
            //* ***********************************************************************
            //*   AS LONG AS DIVISION TABLE HAS UNIT ON IT WE NEED TO WRITE A RECORD  *
            //*   FOR EACH DIVISION/UNIT COMBINATION.  ONCE WE PUT DIVISION ON THE    *
            //*   UNIT TABLE THE FOLLOWING DECIDE STATEMENT CAN BE REMOVED. AT THAT   *
            //*   POINT WE WILL WRITE ONE RECORD FOR EACH DIVISION RECORD READ.       *
            //*                                                                       *
            //*                                                                       *
            //* ***********************************************************************
            if (condition(pnd_Unit_Cde.equals(cwf_Div_Tbl_Unit_Cde.getValue("*"))))                                                                                       //Natural: IF #UNIT-CDE EQ CWF-DIV-TBL.UNIT-CDE ( * )
            {
                pnd_Div.setValue(cwf_Div_Tbl_Division);                                                                                                                   //Natural: MOVE CWF-DIV-TBL.DIVISION TO #DIV
                if (true) break READ_PRIME;                                                                                                                               //Natural: ESCAPE BOTTOM ( READ-PRIME. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //* *SAG END-EXIT
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* ******************************
        //* *SAG END-EXIT
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
