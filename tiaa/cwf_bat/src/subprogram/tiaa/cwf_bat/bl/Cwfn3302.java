/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:26:11 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn3302
************************************************************
**        * FILE NAME            : Cwfn3302.java
**        * CLASS NAME           : Cwfn3302
**        * INSTANCE NAME        : Cwfn3302
************************************************************
************************************************************************
* PROGRAM  : CWFN3302
* SYSTEM   : CRPCWF
* TITLE    : REPORT 1
* FUNCTION : REPORT OF WORK REQUEST LOGGED AND INDEXED
*          : SORT BY SITE-ID, LOG COUNT, WPID.
*          :
* MM DD YY : BY  DESCRIPTION OF CHANGES
* -------- + --  -------------------------------------------------------
* 05/17/02 : JRF IMPLEMENT
*
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn3302 extends BLNatBase
{
    // Data Areas
    private PdaCwfa2100 pdaCwfa2100;
    private PdaCwfa2101 pdaCwfa2101;
    private PdaCdaobjr pdaCdaobjr;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpda_M pdaCwfpda_M;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Run_Type;
    private DbsField pnd_Totl_Cnt;
    private DbsField pnd_Total_Unclear;
    private DbsField pnd_Counters;

    private DbsGroup pnd_Rep_Parm;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Racf_Id;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Unit_Cde;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date;

    private DbsGroup pnd_Rep_Parm__R_Field_1;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date_N;

    private DbsGroup pnd_Rep_Parm__R_Field_2;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date_Fill;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date_Dd;
    private DbsField pnd_Rep_Parm_Pnd_End_Date;

    private DbsGroup pnd_Rep_Parm__R_Field_3;
    private DbsField pnd_Rep_Parm_Pnd_End_Date_N;
    private DbsField pnd_Rep_Parm_Pnd_Work_Start_Date_A;

    private DbsGroup pnd_Rep_Parm__R_Field_4;
    private DbsField pnd_Rep_Parm_Pnd_Work_Mm;
    private DbsField pnd_Rep_Parm_Pnd_Filler1;
    private DbsField pnd_Rep_Parm_Pnd_Work_Dd;
    private DbsField pnd_Rep_Parm_Pnd_Filler2;
    private DbsField pnd_Rep_Parm_Pnd_Work_Yy;
    private DbsField pnd_Rep_Parm_Pnd_Work_End_Date_A;

    private DbsGroup pnd_Rep_Parm__R_Field_5;
    private DbsField pnd_Rep_Parm_Pnd_Work_Mmm;
    private DbsField pnd_Rep_Parm_Pnd_Fillera;
    private DbsField pnd_Rep_Parm_Pnd_Work_Ddd;
    private DbsField pnd_Rep_Parm_Pnd_Fillerb;
    private DbsField pnd_Rep_Parm_Pnd_Work_Yyy;

    private DbsGroup pnd_Local_Data;
    private DbsField pnd_Local_Data_Pnd_Site_Id;
    private DbsField pnd_Local_Data_Pnd_Total_New;
    private DbsField pnd_Local_Data_Pnd_Total_Logged;
    private DbsField pnd_Local_Data_Pnd_Total_Retn_Doc;
    private DbsField pnd_Local_Data_Pnd_Wpid_Count;
    private DbsField pnd_Page;
    private DbsField pnd_Page2;
    private DbsField pnd_Page3;
    private DbsField pnd_Yyyymmdd;

    private DbsGroup pnd_Yyyymmdd__R_Field_6;
    private DbsField pnd_Yyyymmdd_Pnd_Century;
    private DbsField pnd_Yyyymmdd_Pnd_Yy;
    private DbsField pnd_Yyyymmdd_Pnd_Mm;
    private DbsField pnd_Yyyymmdd_Pnd_Dd;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Work_Comp_Date;
    private DbsField pnd_Day_Of_Week;
    private DbsField pnd_Days_To_Subtract;

    private DbsGroup pnd_Days_To_Subtract__R_Field_7;
    private DbsField pnd_Days_To_Subtract_Pnd_Days_To_Subtract_A;
    private DbsField pnd_Crc_Unit;
    private DbsField pnd_Override_Sw;

    private DbsGroup pnd_H1;
    private DbsField pnd_H1_Pnd_H1_Pgm;
    private DbsField pnd_H1_Pnd_H1_Env;
    private DbsField pnd_H1_Pnd_H1_Fill_1;
    private DbsField pnd_H1_Pnd_H1_Title;
    private DbsField pnd_H1_Pnd_H1_Fill_2;
    private DbsField pnd_H1_Pnd_H1_Page;

    private DbsGroup pnd_H1__R_Field_8;
    private DbsField pnd_H1_Pnd_Hdr_1;

    private DbsGroup pnd_H2;
    private DbsField pnd_H2_Pnd_H2_Date;
    private DbsField pnd_H2_Pnd_H2_Fill_1;
    private DbsField pnd_H2_Pnd_H2_Fill_2;
    private DbsField pnd_H2_Pnd_H2_Title;
    private DbsField pnd_H2_Pnd_H2_Fill_3;
    private DbsField pnd_H2_Pnd_H2_Time;

    private DbsGroup pnd_H2__R_Field_9;
    private DbsField pnd_H2_Pnd_Hdr_2;

    private DbsGroup pnd_H5;
    private DbsField pnd_H5_Pnd_H5_Title_1;
    private DbsField pnd_H5_Pnd_H5_Start_Date;
    private DbsField pnd_H5_Pnd_Hdr_Fill_1;
    private DbsField pnd_H5_Pnd_H5_Title_2;
    private DbsField pnd_H5_Pnd_H5_End_Date;

    private DbsGroup pnd_H5__R_Field_10;
    private DbsField pnd_H5_Pnd_Hdr_5;

    private DbsGroup pnd_H8;
    private DbsField pnd_H8_Pnd_H8_Fill_1;
    private DbsField pnd_H8_Pnd_H8_Fill_2;
    private DbsField pnd_H8_Pnd_H8_Fill_3;
    private DbsField pnd_H8_Pnd_H8_Fill_4;

    private DbsGroup pnd_H8__R_Field_11;
    private DbsField pnd_H8_Pnd_Hdr_8;

    private DbsGroup pnd_H9;
    private DbsField pnd_H9_Pnd_H9_Fill_1;
    private DbsField pnd_H9_Pnd_H9_Fill_2;
    private DbsField pnd_H9_Pnd_H9_Site_1;
    private DbsField pnd_H9_Pnd_H9_Fill_3;
    private DbsField pnd_H9_Pnd_H9_Site_2;
    private DbsField pnd_H9_Pnd_H9_Fill_4;
    private DbsField pnd_H9_Pnd_H9_Site_3;
    private DbsField pnd_H9_Pnd_H9_Fill_5;

    private DbsGroup pnd_H9__R_Field_12;
    private DbsField pnd_H9_Pnd_Hdr_9;

    private DbsGroup pnd_H10;
    private DbsField pnd_H10_Pnd_H10_Fill_1;
    private DbsField pnd_H10_Pnd_H10_Fill_2;
    private DbsField pnd_H10_Pnd_H10_Fill_3;
    private DbsField pnd_H10_Pnd_H10_Fill_4;
    private DbsField pnd_H10_Pnd_H10_Fill_5;

    private DbsGroup pnd_H10__R_Field_13;
    private DbsField pnd_H10_Pnd_Hdr_10;
    private DbsField pnd_Hdr_11;

    private DbsGroup pnd_H12;
    private DbsField pnd_H12_Pnd_H12_Fill_1;
    private DbsField pnd_H12_Pnd_H12_Fill_2;
    private DbsField pnd_H12_Pnd_H12_Fill_3;

    private DbsGroup pnd_H12__R_Field_14;
    private DbsField pnd_H12_Pnd_Hdr_12;

    private DbsGroup pnd_H13;
    private DbsField pnd_H13_Pnd_H13_Fill_1;
    private DbsField pnd_H13_Pnd_H13_Fill_2;
    private DbsField pnd_H13_Pnd_H13_Fill_3;

    private DbsGroup pnd_H13__R_Field_15;
    private DbsField pnd_H13_Pnd_Hdr_13;

    private DbsGroup pnd_H14;
    private DbsField pnd_H14_Pnd_H14_Fill_1;
    private DbsField pnd_H14_Pnd_H14_Fill_2;
    private DbsField pnd_H14_Pnd_H14_Fill_3;

    private DbsGroup pnd_H14__R_Field_16;
    private DbsField pnd_H14_Pnd_Hdr_14;

    private DbsGroup pnd_D1;
    private DbsField pnd_D1_Pnd_Dtl1_Wpid_Code;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_1;
    private DbsField pnd_D1_Pnd_Dtl1_Wpid_Desc;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_2;
    private DbsField pnd_D1_Pnd_Dtl1_Owner_Cde;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_5;
    private DbsField pnd_D1_Pnd_Dtl1_Wpid_New;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_6;
    private DbsField pnd_D1_Pnd_Dtl1_Dtot_New;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_7;
    private DbsField pnd_D1_Pnd_Dtl1_Retrn_Doc;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_8;
    private DbsField pnd_D1_Pnd_Dtl1_Dtot_Retn;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_9;
    private DbsField pnd_D1_Pnd_Dtl1_Wpid_Cnt;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_10;
    private DbsField pnd_D1_Pnd_Dtl1_Dtot_Cnt;

    private DbsGroup pnd_D1__R_Field_17;
    private DbsField pnd_D1_Pnd_Dtl_1;

    private DbsGroup pnd_T1;
    private DbsField pnd_T1_Pnd_Tot1_Fill_1;
    private DbsField pnd_T1_Pnd_Tot1_Fill_2;
    private DbsField pnd_T1_Pnd_Tot1_Total_New;
    private DbsField pnd_T1_Pnd_Tot1_Fill_3;
    private DbsField pnd_T1_Pnd_Tot1_Stot_New;
    private DbsField pnd_T1_Pnd_Tot1_Fill_4;
    private DbsField pnd_T1_Pnd_Tot1_Retrn_Doc;
    private DbsField pnd_T1_Pnd_Tot1_Fill_5;
    private DbsField pnd_T1_Pnd_Tot1_Stot_Retn;
    private DbsField pnd_T1_Pnd_Tot1_Fill_6;
    private DbsField pnd_T1_Pnd_Tot1_Logged;
    private DbsField pnd_T1_Pnd_Tot1_Fill_7;
    private DbsField pnd_T1_Pnd_Tot1_Stot_Log;

    private DbsGroup pnd_T1__R_Field_18;
    private DbsField pnd_T1_Pnd_Tot_1;

    private DbsGroup pnd_T2;
    private DbsField pnd_T2_Pnd_Tot2_Fill_1;
    private DbsField pnd_T2_Pnd_Tot2_Fill_2;
    private DbsField pnd_T2_Pnd_Tot2_Unclear;

    private DbsGroup pnd_T2__R_Field_19;
    private DbsField pnd_T2_Pnd_Tot_2;

    private DbsGroup pnd_T3;
    private DbsField pnd_T3_Pnd_Tot3_Fill_1;

    private DbsGroup pnd_T3_Pnd_Tot3_Array;
    private DbsField pnd_T3_Pnd_Tot3_Ctrs;
    private DbsField pnd_T3_Pnd_Tot3_Fill_2;
    private DbsField pnd_T3_Pnd_Tot3_Message;

    private DbsGroup pnd_T3__R_Field_20;
    private DbsField pnd_T3_Pnd_Tot_3;

    private DbsGroup pnd_Work_Record_Sort;
    private DbsField pnd_Work_Record_Sort_Site_Id;
    private DbsField pnd_Work_Record_Sort_Wpid;
    private DbsField pnd_Work_Record_Sort_New_Rqst_Site;
    private DbsField pnd_Work_Record_Sort_New_Rqst_All;
    private DbsField pnd_Work_Record_Sort_Ret_Doc_Site;
    private DbsField pnd_Work_Record_Sort_Ret_Doc_All;
    private DbsField pnd_Work_Record_Sort_Tot_Log_Site;
    private DbsField pnd_Work_Record_Sort_Tot_Log_All;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa2100 = new PdaCwfa2100(localVariables);
        pdaCwfa2101 = new PdaCwfa2101(localVariables);
        pdaCdaobjr = new PdaCdaobjr(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Run_Type = parameters.newFieldInRecord("pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 7);
        pnd_Run_Type.setParameterOption(ParameterOption.ByReference);
        pnd_Totl_Cnt = parameters.newFieldArrayInRecord("pnd_Totl_Cnt", "#TOTL-CNT", FieldType.PACKED_DECIMAL, 6, new DbsArrayController(1, 3, 1, 2));
        pnd_Totl_Cnt.setParameterOption(ParameterOption.ByReference);
        pnd_Total_Unclear = parameters.newFieldArrayInRecord("pnd_Total_Unclear", "#TOTAL-UNCLEAR", FieldType.PACKED_DECIMAL, 6, new DbsArrayController(1, 
            3));
        pnd_Total_Unclear.setParameterOption(ParameterOption.ByReference);
        pnd_Counters = parameters.newFieldArrayInRecord("pnd_Counters", "#COUNTERS", FieldType.PACKED_DECIMAL, 6, new DbsArrayController(1, 3, 1, 2, 1, 
            8));
        pnd_Counters.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Rep_Parm = localVariables.newGroupInRecord("pnd_Rep_Parm", "#REP-PARM");
        pnd_Rep_Parm_Pnd_Rep_Racf_Id = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Racf_Id", "#REP-RACF-ID", FieldType.STRING, 8);
        pnd_Rep_Parm_Pnd_Rep_Unit_Cde = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);
        pnd_Rep_Parm_Pnd_Start_Date = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_1 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_1", "REDEFINE", pnd_Rep_Parm_Pnd_Start_Date);
        pnd_Rep_Parm_Pnd_Start_Date_N = pnd_Rep_Parm__R_Field_1.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 8);

        pnd_Rep_Parm__R_Field_2 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_2", "REDEFINE", pnd_Rep_Parm_Pnd_Start_Date);
        pnd_Rep_Parm_Pnd_Start_Date_Fill = pnd_Rep_Parm__R_Field_2.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date_Fill", "#START-DATE-FILL", FieldType.STRING, 
            6);
        pnd_Rep_Parm_Pnd_Start_Date_Dd = pnd_Rep_Parm__R_Field_2.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date_Dd", "#START-DATE-DD", FieldType.STRING, 
            2);
        pnd_Rep_Parm_Pnd_End_Date = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_3 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_3", "REDEFINE", pnd_Rep_Parm_Pnd_End_Date);
        pnd_Rep_Parm_Pnd_End_Date_N = pnd_Rep_Parm__R_Field_3.newFieldInGroup("pnd_Rep_Parm_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 8);
        pnd_Rep_Parm_Pnd_Work_Start_Date_A = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Start_Date_A", "#WORK-START-DATE-A", FieldType.STRING, 
            8);

        pnd_Rep_Parm__R_Field_4 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_4", "REDEFINE", pnd_Rep_Parm_Pnd_Work_Start_Date_A);
        pnd_Rep_Parm_Pnd_Work_Mm = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Mm", "#WORK-MM", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Filler1 = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Dd = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Filler2 = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Yy = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Yy", "#WORK-YY", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Work_End_Date_A = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_End_Date_A", "#WORK-END-DATE-A", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_5 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_5", "REDEFINE", pnd_Rep_Parm_Pnd_Work_End_Date_A);
        pnd_Rep_Parm_Pnd_Work_Mmm = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Mmm", "#WORK-MMM", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Fillera = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Fillera", "#FILLERA", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Ddd = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Ddd", "#WORK-DDD", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Fillerb = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Fillerb", "#FILLERB", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Yyy = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Yyy", "#WORK-YYY", FieldType.STRING, 2);

        pnd_Local_Data = localVariables.newGroupInRecord("pnd_Local_Data", "#LOCAL-DATA");
        pnd_Local_Data_Pnd_Site_Id = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Site_Id", "#SITE-ID", FieldType.STRING, 2);
        pnd_Local_Data_Pnd_Total_New = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_New", "#TOTAL-NEW", FieldType.NUMERIC, 6);
        pnd_Local_Data_Pnd_Total_Logged = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_Logged", "#TOTAL-LOGGED", FieldType.NUMERIC, 6);
        pnd_Local_Data_Pnd_Total_Retn_Doc = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_Retn_Doc", "#TOTAL-RETN-DOC", FieldType.NUMERIC, 
            6);
        pnd_Local_Data_Pnd_Wpid_Count = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Count", "#WPID-COUNT", FieldType.NUMERIC, 6);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 5);
        pnd_Page2 = localVariables.newFieldInRecord("pnd_Page2", "#PAGE2", FieldType.NUMERIC, 5);
        pnd_Page3 = localVariables.newFieldInRecord("pnd_Page3", "#PAGE3", FieldType.NUMERIC, 5);
        pnd_Yyyymmdd = localVariables.newFieldInRecord("pnd_Yyyymmdd", "#YYYYMMDD", FieldType.STRING, 8);

        pnd_Yyyymmdd__R_Field_6 = localVariables.newGroupInRecord("pnd_Yyyymmdd__R_Field_6", "REDEFINE", pnd_Yyyymmdd);
        pnd_Yyyymmdd_Pnd_Century = pnd_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Yyyymmdd_Pnd_Century", "#CENTURY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Yy = pnd_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Yyyymmdd_Pnd_Yy", "#YY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Mm = pnd_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Yyyymmdd_Pnd_Mm", "#MM", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Dd = pnd_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Yyyymmdd_Pnd_Dd", "#DD", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Work_Comp_Date = localVariables.newFieldInRecord("pnd_Work_Comp_Date", "#WORK-COMP-DATE", FieldType.DATE);
        pnd_Day_Of_Week = localVariables.newFieldInRecord("pnd_Day_Of_Week", "#DAY-OF-WEEK", FieldType.STRING, 3);
        pnd_Days_To_Subtract = localVariables.newFieldInRecord("pnd_Days_To_Subtract", "#DAYS-TO-SUBTRACT", FieldType.NUMERIC, 2);

        pnd_Days_To_Subtract__R_Field_7 = localVariables.newGroupInRecord("pnd_Days_To_Subtract__R_Field_7", "REDEFINE", pnd_Days_To_Subtract);
        pnd_Days_To_Subtract_Pnd_Days_To_Subtract_A = pnd_Days_To_Subtract__R_Field_7.newFieldInGroup("pnd_Days_To_Subtract_Pnd_Days_To_Subtract_A", "#DAYS-TO-SUBTRACT-A", 
            FieldType.STRING, 2);
        pnd_Crc_Unit = localVariables.newFieldInRecord("pnd_Crc_Unit", "#CRC-UNIT", FieldType.STRING, 8);
        pnd_Override_Sw = localVariables.newFieldInRecord("pnd_Override_Sw", "#OVERRIDE-SW", FieldType.BOOLEAN, 1);

        pnd_H1 = localVariables.newGroupInRecord("pnd_H1", "#H1");
        pnd_H1_Pnd_H1_Pgm = pnd_H1.newFieldInGroup("pnd_H1_Pnd_H1_Pgm", "#H1-PGM", FieldType.STRING, 9);
        pnd_H1_Pnd_H1_Env = pnd_H1.newFieldInGroup("pnd_H1_Pnd_H1_Env", "#H1-ENV", FieldType.STRING, 10);
        pnd_H1_Pnd_H1_Fill_1 = pnd_H1.newFieldInGroup("pnd_H1_Pnd_H1_Fill_1", "#H1-FILL-1", FieldType.STRING, 12);
        pnd_H1_Pnd_H1_Title = pnd_H1.newFieldInGroup("pnd_H1_Pnd_H1_Title", "#H1-TITLE", FieldType.STRING, 29);
        pnd_H1_Pnd_H1_Fill_2 = pnd_H1.newFieldInGroup("pnd_H1_Pnd_H1_Fill_2", "#H1-FILL-2", FieldType.STRING, 15);
        pnd_H1_Pnd_H1_Page = pnd_H1.newFieldInGroup("pnd_H1_Pnd_H1_Page", "#H1-PAGE", FieldType.STRING, 10);

        pnd_H1__R_Field_8 = localVariables.newGroupInRecord("pnd_H1__R_Field_8", "REDEFINE", pnd_H1);
        pnd_H1_Pnd_Hdr_1 = pnd_H1__R_Field_8.newFieldInGroup("pnd_H1_Pnd_Hdr_1", "#HDR-1", FieldType.STRING, 85);

        pnd_H2 = localVariables.newGroupInRecord("pnd_H2", "#H2");
        pnd_H2_Pnd_H2_Date = pnd_H2.newFieldInGroup("pnd_H2_Pnd_H2_Date", "#H2-DATE", FieldType.STRING, 12);
        pnd_H2_Pnd_H2_Fill_1 = pnd_H2.newFieldInGroup("pnd_H2_Pnd_H2_Fill_1", "#H2-FILL-1", FieldType.STRING, 12);
        pnd_H2_Pnd_H2_Fill_2 = pnd_H2.newFieldInGroup("pnd_H2_Pnd_H2_Fill_2", "#H2-FILL-2", FieldType.STRING, 11);
        pnd_H2_Pnd_H2_Title = pnd_H2.newFieldInGroup("pnd_H2_Pnd_H2_Title", "#H2-TITLE", FieldType.STRING, 32);
        pnd_H2_Pnd_H2_Fill_3 = pnd_H2.newFieldInGroup("pnd_H2_Pnd_H2_Fill_3", "#H2-FILL-3", FieldType.STRING, 10);
        pnd_H2_Pnd_H2_Time = pnd_H2.newFieldInGroup("pnd_H2_Pnd_H2_Time", "#H2-TIME", FieldType.STRING, 8);

        pnd_H2__R_Field_9 = localVariables.newGroupInRecord("pnd_H2__R_Field_9", "REDEFINE", pnd_H2);
        pnd_H2_Pnd_Hdr_2 = pnd_H2__R_Field_9.newFieldInGroup("pnd_H2_Pnd_Hdr_2", "#HDR-2", FieldType.STRING, 85);

        pnd_H5 = localVariables.newGroupInRecord("pnd_H5", "#H5");
        pnd_H5_Pnd_H5_Title_1 = pnd_H5.newFieldInGroup("pnd_H5_Pnd_H5_Title_1", "#H5-TITLE-1", FieldType.STRING, 12);
        pnd_H5_Pnd_H5_Start_Date = pnd_H5.newFieldInGroup("pnd_H5_Pnd_H5_Start_Date", "#H5-START-DATE", FieldType.STRING, 8);
        pnd_H5_Pnd_Hdr_Fill_1 = pnd_H5.newFieldInGroup("pnd_H5_Pnd_Hdr_Fill_1", "#HDR-FILL-1", FieldType.STRING, 3);
        pnd_H5_Pnd_H5_Title_2 = pnd_H5.newFieldInGroup("pnd_H5_Pnd_H5_Title_2", "#H5-TITLE-2", FieldType.STRING, 12);
        pnd_H5_Pnd_H5_End_Date = pnd_H5.newFieldInGroup("pnd_H5_Pnd_H5_End_Date", "#H5-END-DATE", FieldType.STRING, 8);

        pnd_H5__R_Field_10 = localVariables.newGroupInRecord("pnd_H5__R_Field_10", "REDEFINE", pnd_H5);
        pnd_H5_Pnd_Hdr_5 = pnd_H5__R_Field_10.newFieldInGroup("pnd_H5_Pnd_Hdr_5", "#HDR-5", FieldType.STRING, 43);

        pnd_H8 = localVariables.newGroupInRecord("pnd_H8", "#H8");
        pnd_H8_Pnd_H8_Fill_1 = pnd_H8.newFieldInGroup("pnd_H8_Pnd_H8_Fill_1", "#H8-FILL-1", FieldType.STRING, 53);
        pnd_H8_Pnd_H8_Fill_2 = pnd_H8.newFieldInGroup("pnd_H8_Pnd_H8_Fill_2", "#H8-FILL-2", FieldType.STRING, 27);
        pnd_H8_Pnd_H8_Fill_3 = pnd_H8.newFieldInGroup("pnd_H8_Pnd_H8_Fill_3", "#H8-FILL-3", FieldType.STRING, 25);
        pnd_H8_Pnd_H8_Fill_4 = pnd_H8.newFieldInGroup("pnd_H8_Pnd_H8_Fill_4", "#H8-FILL-4", FieldType.STRING, 15);

        pnd_H8__R_Field_11 = localVariables.newGroupInRecord("pnd_H8__R_Field_11", "REDEFINE", pnd_H8);
        pnd_H8_Pnd_Hdr_8 = pnd_H8__R_Field_11.newFieldInGroup("pnd_H8_Pnd_Hdr_8", "#HDR-8", FieldType.STRING, 120);

        pnd_H9 = localVariables.newGroupInRecord("pnd_H9", "#H9");
        pnd_H9_Pnd_H9_Fill_1 = pnd_H9.newFieldInGroup("pnd_H9_Pnd_H9_Fill_1", "#H9-FILL-1", FieldType.STRING, 53);
        pnd_H9_Pnd_H9_Fill_2 = pnd_H9.newFieldInGroup("pnd_H9_Pnd_H9_Fill_2", "#H9-FILL-2", FieldType.STRING, 10);
        pnd_H9_Pnd_H9_Site_1 = pnd_H9.newFieldInGroup("pnd_H9_Pnd_H9_Site_1", "#H9-SITE-1", FieldType.STRING, 10);
        pnd_H9_Pnd_H9_Fill_3 = pnd_H9.newFieldInGroup("pnd_H9_Pnd_H9_Fill_3", "#H9-FILL-3", FieldType.STRING, 10);
        pnd_H9_Pnd_H9_Site_2 = pnd_H9.newFieldInGroup("pnd_H9_Pnd_H9_Site_2", "#H9-SITE-2", FieldType.STRING, 10);
        pnd_H9_Pnd_H9_Fill_4 = pnd_H9.newFieldInGroup("pnd_H9_Pnd_H9_Fill_4", "#H9-FILL-4", FieldType.STRING, 10);
        pnd_H9_Pnd_H9_Site_3 = pnd_H9.newFieldInGroup("pnd_H9_Pnd_H9_Site_3", "#H9-SITE-3", FieldType.STRING, 10);
        pnd_H9_Pnd_H9_Fill_5 = pnd_H9.newFieldInGroup("pnd_H9_Pnd_H9_Fill_5", "#H9-FILL-5", FieldType.STRING, 8);

        pnd_H9__R_Field_12 = localVariables.newGroupInRecord("pnd_H9__R_Field_12", "REDEFINE", pnd_H9);
        pnd_H9_Pnd_Hdr_9 = pnd_H9__R_Field_12.newFieldInGroup("pnd_H9_Pnd_Hdr_9", "#HDR-9", FieldType.STRING, 120);

        pnd_H10 = localVariables.newGroupInRecord("pnd_H10", "#H10");
        pnd_H10_Pnd_H10_Fill_1 = pnd_H10.newFieldInGroup("pnd_H10_Pnd_H10_Fill_1", "#H10-FILL-1", FieldType.STRING, 7);
        pnd_H10_Pnd_H10_Fill_2 = pnd_H10.newFieldInGroup("pnd_H10_Pnd_H10_Fill_2", "#H10-FILL-2", FieldType.STRING, 45);
        pnd_H10_Pnd_H10_Fill_3 = pnd_H10.newFieldInGroup("pnd_H10_Pnd_H10_Fill_3", "#H10-FILL-3", FieldType.STRING, 20);
        pnd_H10_Pnd_H10_Fill_4 = pnd_H10.newFieldInGroup("pnd_H10_Pnd_H10_Fill_4", "#H10-FILL-4", FieldType.STRING, 21);
        pnd_H10_Pnd_H10_Fill_5 = pnd_H10.newFieldInGroup("pnd_H10_Pnd_H10_Fill_5", "#H10-FILL-5", FieldType.STRING, 29);

        pnd_H10__R_Field_13 = localVariables.newGroupInRecord("pnd_H10__R_Field_13", "REDEFINE", pnd_H10);
        pnd_H10_Pnd_Hdr_10 = pnd_H10__R_Field_13.newFieldInGroup("pnd_H10_Pnd_Hdr_10", "#HDR-10", FieldType.STRING, 122);
        pnd_Hdr_11 = localVariables.newFieldInRecord("pnd_Hdr_11", "#HDR-11", FieldType.STRING, 130);

        pnd_H12 = localVariables.newGroupInRecord("pnd_H12", "#H12");
        pnd_H12_Pnd_H12_Fill_1 = pnd_H12.newFieldInGroup("pnd_H12_Pnd_H12_Fill_1", "#H12-FILL-1", FieldType.STRING, 15);
        pnd_H12_Pnd_H12_Fill_2 = pnd_H12.newFieldInGroup("pnd_H12_Pnd_H12_Fill_2", "#H12-FILL-2", FieldType.STRING, 46);
        pnd_H12_Pnd_H12_Fill_3 = pnd_H12.newFieldInGroup("pnd_H12_Pnd_H12_Fill_3", "#H12-FILL-3", FieldType.STRING, 15);

        pnd_H12__R_Field_14 = localVariables.newGroupInRecord("pnd_H12__R_Field_14", "REDEFINE", pnd_H12);
        pnd_H12_Pnd_Hdr_12 = pnd_H12__R_Field_14.newFieldInGroup("pnd_H12_Pnd_Hdr_12", "#HDR-12", FieldType.STRING, 76);

        pnd_H13 = localVariables.newGroupInRecord("pnd_H13", "#H13");
        pnd_H13_Pnd_H13_Fill_1 = pnd_H13.newFieldInGroup("pnd_H13_Pnd_H13_Fill_1", "#H13-FILL-1", FieldType.STRING, 31);
        pnd_H13_Pnd_H13_Fill_2 = pnd_H13.newFieldInGroup("pnd_H13_Pnd_H13_Fill_2", "#H13-FILL-2", FieldType.STRING, 30);
        pnd_H13_Pnd_H13_Fill_3 = pnd_H13.newFieldInGroup("pnd_H13_Pnd_H13_Fill_3", "#H13-FILL-3", FieldType.STRING, 18);

        pnd_H13__R_Field_15 = localVariables.newGroupInRecord("pnd_H13__R_Field_15", "REDEFINE", pnd_H13);
        pnd_H13_Pnd_Hdr_13 = pnd_H13__R_Field_15.newFieldInGroup("pnd_H13_Pnd_Hdr_13", "#HDR-13", FieldType.STRING, 79);

        pnd_H14 = localVariables.newGroupInRecord("pnd_H14", "#H14");
        pnd_H14_Pnd_H14_Fill_1 = pnd_H14.newFieldInGroup("pnd_H14_Pnd_H14_Fill_1", "#H14-FILL-1", FieldType.STRING, 31);
        pnd_H14_Pnd_H14_Fill_2 = pnd_H14.newFieldInGroup("pnd_H14_Pnd_H14_Fill_2", "#H14-FILL-2", FieldType.STRING, 30);
        pnd_H14_Pnd_H14_Fill_3 = pnd_H14.newFieldInGroup("pnd_H14_Pnd_H14_Fill_3", "#H14-FILL-3", FieldType.STRING, 18);

        pnd_H14__R_Field_16 = localVariables.newGroupInRecord("pnd_H14__R_Field_16", "REDEFINE", pnd_H14);
        pnd_H14_Pnd_Hdr_14 = pnd_H14__R_Field_16.newFieldInGroup("pnd_H14_Pnd_Hdr_14", "#HDR-14", FieldType.STRING, 79);

        pnd_D1 = localVariables.newGroupInRecord("pnd_D1", "#D1");
        pnd_D1_Pnd_Dtl1_Wpid_Code = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Wpid_Code", "#DTL1-WPID-CODE", FieldType.STRING, 6);
        pnd_D1_Pnd_Dtl1_Fill_1 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_1", "#DTL1-FILL-1", FieldType.STRING, 1);
        pnd_D1_Pnd_Dtl1_Wpid_Desc = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Wpid_Desc", "#DTL1-WPID-DESC", FieldType.STRING, 45);
        pnd_D1_Pnd_Dtl1_Fill_2 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_2", "#DTL1-FILL-2", FieldType.STRING, 1);
        pnd_D1_Pnd_Dtl1_Owner_Cde = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Owner_Cde", "#DTL1-OWNER-CDE", FieldType.STRING, 8);
        pnd_D1_Pnd_Dtl1_Fill_5 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_5", "#DTL1-FILL-5", FieldType.STRING, 3);
        pnd_D1_Pnd_Dtl1_Wpid_New = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Wpid_New", "#DTL1-WPID-NEW", FieldType.STRING, 7);
        pnd_D1_Pnd_Dtl1_Fill_6 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_6", "#DTL1-FILL-6", FieldType.STRING, 3);
        pnd_D1_Pnd_Dtl1_Dtot_New = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Dtot_New", "#DTL1-DTOT-NEW", FieldType.STRING, 7);
        pnd_D1_Pnd_Dtl1_Fill_7 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_7", "#DTL1-FILL-7", FieldType.STRING, 3);
        pnd_D1_Pnd_Dtl1_Retrn_Doc = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Retrn_Doc", "#DTL1-RETRN-DOC", FieldType.STRING, 7);
        pnd_D1_Pnd_Dtl1_Fill_8 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_8", "#DTL1-FILL-8", FieldType.STRING, 3);
        pnd_D1_Pnd_Dtl1_Dtot_Retn = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Dtot_Retn", "#DTL1-DTOT-RETN", FieldType.STRING, 7);
        pnd_D1_Pnd_Dtl1_Fill_9 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_9", "#DTL1-FILL-9", FieldType.STRING, 3);
        pnd_D1_Pnd_Dtl1_Wpid_Cnt = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Wpid_Cnt", "#DTL1-WPID-CNT", FieldType.STRING, 7);
        pnd_D1_Pnd_Dtl1_Fill_10 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_10", "#DTL1-FILL-10", FieldType.STRING, 3);
        pnd_D1_Pnd_Dtl1_Dtot_Cnt = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Dtot_Cnt", "#DTL1-DTOT-CNT", FieldType.STRING, 7);

        pnd_D1__R_Field_17 = localVariables.newGroupInRecord("pnd_D1__R_Field_17", "REDEFINE", pnd_D1);
        pnd_D1_Pnd_Dtl_1 = pnd_D1__R_Field_17.newFieldInGroup("pnd_D1_Pnd_Dtl_1", "#DTL-1", FieldType.STRING, 121);

        pnd_T1 = localVariables.newGroupInRecord("pnd_T1", "#T1");
        pnd_T1_Pnd_Tot1_Fill_1 = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Fill_1", "#TOT1-FILL-1", FieldType.STRING, 47);
        pnd_T1_Pnd_Tot1_Fill_2 = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Fill_2", "#TOT1-FILL-2", FieldType.STRING, 17);
        pnd_T1_Pnd_Tot1_Total_New = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Total_New", "#TOT1-TOTAL-NEW", FieldType.STRING, 7);
        pnd_T1_Pnd_Tot1_Fill_3 = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Fill_3", "#TOT1-FILL-3", FieldType.STRING, 3);
        pnd_T1_Pnd_Tot1_Stot_New = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Stot_New", "#TOT1-STOT-NEW", FieldType.STRING, 7);
        pnd_T1_Pnd_Tot1_Fill_4 = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Fill_4", "#TOT1-FILL-4", FieldType.STRING, 3);
        pnd_T1_Pnd_Tot1_Retrn_Doc = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Retrn_Doc", "#TOT1-RETRN-DOC", FieldType.STRING, 7);
        pnd_T1_Pnd_Tot1_Fill_5 = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Fill_5", "#TOT1-FILL-5", FieldType.STRING, 3);
        pnd_T1_Pnd_Tot1_Stot_Retn = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Stot_Retn", "#TOT1-STOT-RETN", FieldType.STRING, 7);
        pnd_T1_Pnd_Tot1_Fill_6 = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Fill_6", "#TOT1-FILL-6", FieldType.STRING, 3);
        pnd_T1_Pnd_Tot1_Logged = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Logged", "#TOT1-LOGGED", FieldType.STRING, 7);
        pnd_T1_Pnd_Tot1_Fill_7 = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Fill_7", "#TOT1-FILL-7", FieldType.STRING, 3);
        pnd_T1_Pnd_Tot1_Stot_Log = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Stot_Log", "#TOT1-STOT-LOG", FieldType.STRING, 7);

        pnd_T1__R_Field_18 = localVariables.newGroupInRecord("pnd_T1__R_Field_18", "REDEFINE", pnd_T1);
        pnd_T1_Pnd_Tot_1 = pnd_T1__R_Field_18.newFieldInGroup("pnd_T1_Pnd_Tot_1", "#TOT-1", FieldType.STRING, 121);

        pnd_T2 = localVariables.newGroupInRecord("pnd_T2", "#T2");
        pnd_T2_Pnd_Tot2_Fill_1 = pnd_T2.newFieldInGroup("pnd_T2_Pnd_Tot2_Fill_1", "#TOT2-FILL-1", FieldType.STRING, 35);
        pnd_T2_Pnd_Tot2_Fill_2 = pnd_T2.newFieldInGroup("pnd_T2_Pnd_Tot2_Fill_2", "#TOT2-FILL-2", FieldType.STRING, 32);
        pnd_T2_Pnd_Tot2_Unclear = pnd_T2.newFieldInGroup("pnd_T2_Pnd_Tot2_Unclear", "#TOT2-UNCLEAR", FieldType.STRING, 7);

        pnd_T2__R_Field_19 = localVariables.newGroupInRecord("pnd_T2__R_Field_19", "REDEFINE", pnd_T2);
        pnd_T2_Pnd_Tot_2 = pnd_T2__R_Field_19.newFieldInGroup("pnd_T2_Pnd_Tot_2", "#TOT-2", FieldType.STRING, 74);

        pnd_T3 = localVariables.newGroupInRecord("pnd_T3", "#T3");
        pnd_T3_Pnd_Tot3_Fill_1 = pnd_T3.newFieldInGroup("pnd_T3_Pnd_Tot3_Fill_1", "#TOT3-FILL-1", FieldType.STRING, 3);

        pnd_T3_Pnd_Tot3_Array = pnd_T3.newGroupArrayInGroup("pnd_T3_Pnd_Tot3_Array", "#TOT3-ARRAY", new DbsArrayController(1, 8));
        pnd_T3_Pnd_Tot3_Ctrs = pnd_T3_Pnd_Tot3_Array.newFieldInGroup("pnd_T3_Pnd_Tot3_Ctrs", "#TOT3-CTRS", FieldType.STRING, 6);
        pnd_T3_Pnd_Tot3_Fill_2 = pnd_T3_Pnd_Tot3_Array.newFieldInGroup("pnd_T3_Pnd_Tot3_Fill_2", "#TOT3-FILL-2", FieldType.STRING, 4);
        pnd_T3_Pnd_Tot3_Message = pnd_T3.newFieldInGroup("pnd_T3_Pnd_Tot3_Message", "#TOT3-MESSAGE", FieldType.STRING, 22);

        pnd_T3__R_Field_20 = localVariables.newGroupInRecord("pnd_T3__R_Field_20", "REDEFINE", pnd_T3);
        pnd_T3_Pnd_Tot_3 = pnd_T3__R_Field_20.newFieldInGroup("pnd_T3_Pnd_Tot_3", "#TOT-3", FieldType.STRING, 105);

        pnd_Work_Record_Sort = localVariables.newGroupInRecord("pnd_Work_Record_Sort", "#WORK-RECORD-SORT");
        pnd_Work_Record_Sort_Site_Id = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_Site_Id", "SITE-ID", FieldType.STRING, 2);
        pnd_Work_Record_Sort_Wpid = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_Wpid", "WPID", FieldType.STRING, 6);
        pnd_Work_Record_Sort_New_Rqst_Site = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_New_Rqst_Site", "NEW-RQST-SITE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Work_Record_Sort_New_Rqst_All = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_New_Rqst_All", "NEW-RQST-ALL", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Work_Record_Sort_Ret_Doc_Site = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_Ret_Doc_Site", "RET-DOC-SITE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Work_Record_Sort_Ret_Doc_All = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_Ret_Doc_All", "RET-DOC-ALL", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Work_Record_Sort_Tot_Log_Site = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_Tot_Log_Site", "TOT-LOG-SITE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Work_Record_Sort_Tot_Log_All = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_Tot_Log_All", "TOT-LOG-ALL", FieldType.PACKED_DECIMAL, 
            6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Crc_Unit.setInitialValue("CRC");
        pnd_H1_Pnd_H1_Title.setInitialValue("CORPORATE WORKFLOW FACILITIES");
        pnd_H2_Pnd_H2_Fill_2.setInitialValue("SUMMARY OF");
        pnd_H2_Pnd_H2_Title.setInitialValue("WORK REQUESTS LOGGED AND INDEXED");
        pnd_H5_Pnd_H5_Title_1.setInitialValue("START DATE:");
        pnd_H5_Pnd_H5_Title_2.setInitialValue("END DATE:");
        pnd_H8_Pnd_H8_Fill_2.setInitialValue("OWNER        NEW REQUESTS  ");
        pnd_H8_Pnd_H8_Fill_3.setInitialValue("     RETURNING DOCS");
        pnd_H8_Pnd_H8_Fill_4.setInitialValue("LOGGED REQUESTS");
        pnd_H9_Pnd_H9_Fill_1.setInitialValue("WPID   DESCRIPTION");
        pnd_H9_Pnd_H9_Fill_2.setInitialValue("UNIT");
        pnd_H9_Pnd_H9_Fill_3.setInitialValue("    ALL");
        pnd_H9_Pnd_H9_Fill_4.setInitialValue("    ALL");
        pnd_H9_Pnd_H9_Fill_5.setInitialValue("    ALL");
        pnd_H10_Pnd_H10_Fill_1.setInitialValue("------ ");
        pnd_H10_Pnd_H10_Fill_2.setInitialValue("---------------------------------------------");
        pnd_H10_Pnd_H10_Fill_3.setInitialValue(" --------- ---------");
        pnd_H10_Pnd_H10_Fill_4.setInitialValue(" --------- --------- ");
        pnd_H10_Pnd_H10_Fill_5.setInitialValue("--------- --------- ---------");
        pnd_Hdr_11.setInitialValue("**********************************************************************************************************************************");
        pnd_H12_Pnd_H12_Fill_1.setInitialValue("BOOKLETS  FORMS");
        pnd_H12_Pnd_H12_Fill_3.setInitialValue("MISC NEW");
        pnd_H13_Pnd_H13_Fill_1.setInitialValue("REQUESTED REQUESTED INQUIRIES");
        pnd_H13_Pnd_H13_Fill_2.setInitialValue("RESEARCH TRANSACTS CMPLAINTS");
        pnd_H13_Pnd_H13_Fill_3.setInitialValue("REQUESTS    OTHERS");
        pnd_H14_Pnd_H14_Fill_1.setInitialValue("--------- --------- ---------");
        pnd_H14_Pnd_H14_Fill_2.setInitialValue("-------- --------- --------- ");
        pnd_H14_Pnd_H14_Fill_3.setInitialValue("--------  --------");
        pnd_T1_Pnd_Tot1_Fill_2.setInitialValue("TOTAL -------->");
        pnd_T2_Pnd_Tot2_Fill_1.setInitialValue("NUMBER OF WORK REQUESTS LOGGED AND");
        pnd_T2_Pnd_Tot2_Fill_2.setInitialValue("INDEXED WITH 'U' IN ANY ELEMENT:");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Cwfn3302() throws Exception
    {
        super("Cwfn3302");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_H1_Pnd_H1_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", Global.getLIBRARY_ID(), ")"));                                                //Natural: COMPRESS '(' *LIBRARY-ID ')' INTO #H1-ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        //*  --------------
        //*  REPORT SECTION
        //*  --------------
        //*  -----                                                                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 58;//Natural: FORMAT ( 2 ) LS = 132 PS = 58;//Natural: FORMAT ( 3 ) LS = 132 PS = 58
        pnd_H1_Pnd_H1_Pgm.setValue(Global.getPROGRAM());                                                                                                                  //Natural: MOVE *PROGRAM TO #H1-PGM
        pnd_H2_Pnd_H2_Date.setValueEdited(Global.getDATX(),new ReportEditMask("LLL' 'DD', 'YYYY"));                                                                       //Natural: MOVE EDITED *DATX ( EM = LLL' 'DD', 'YYYY ) TO #H2-DATE
        pnd_H2_Pnd_H2_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HH':'II' 'AP"));                                                                           //Natural: MOVE EDITED *TIMX ( EM = HH':'II' 'AP ) TO #H2-TIME
        pnd_Rep_Parm_Pnd_Filler1.setValue("/");                                                                                                                           //Natural: MOVE '/' TO #FILLER1 #FILLER2
        pnd_Rep_Parm_Pnd_Filler2.setValue("/");
        pnd_Rep_Parm_Pnd_Fillera.setValue("/");                                                                                                                           //Natural: MOVE '/' TO #FILLERA #FILLERB
        pnd_Rep_Parm_Pnd_Fillerb.setValue("/");
        //*  ------------------------------------------
        //*  GET OVERRIDE PARAMETERS FROM SUPPORT TABLE -- JF030102
        //*  ------------------------------------------
        pdaCwfa2100.getCwfa2100().reset();                                                                                                                                //Natural: RESET CWFA2100 #OVERRIDE-SW
        pnd_Override_Sw.reset();
        if (condition(pnd_Run_Type.equals("MONTHLY")))                                                                                                                    //Natural: IF #RUN-TYPE = 'MONTHLY'
        {
            pdaCwfa2100.getCwfa2100_Tbl_Key_Field().setValue("MONTHLY-OVRDE");                                                                                            //Natural: ASSIGN CWFA2100.TBL-KEY-FIELD := 'MONTHLY-OVRDE'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfa2100.getCwfa2100_Tbl_Key_Field().setValue("WEEKLY-OVRDE");                                                                                             //Natural: ASSIGN CWFA2100.TBL-KEY-FIELD := 'WEEKLY-OVRDE'
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfa2100.getCwfa2100_Tbl_Scrty_Level_Ind().setValue("A");                                                                                                      //Natural: ASSIGN CWFA2100.TBL-SCRTY-LEVEL-IND := 'A'
        pdaCwfa2100.getCwfa2100_Tbl_Actve_Ind().setValue("A");                                                                                                            //Natural: ASSIGN CWFA2100.TBL-ACTVE-IND := 'A'
        pdaCwfa2100.getCwfa2100_Tbl_Table_Nme().setValue("CWF-REPORT-CWFB3401");                                                                                          //Natural: ASSIGN CWFA2100.TBL-TABLE-NME := 'CWF-REPORT-CWFB3401'
        pdaCdaobjr.getCdaobj_Pnd_Function().setValue("GET");                                                                                                              //Natural: ASSIGN CDAOBJ.#FUNCTION := 'GET'
                                                                                                                                                                          //Natural: PERFORM CALL-CWFN2100
        sub_Call_Cwfn2100();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals(" ") && pdaCwfa2100.getCwfa2100_Tbl_Data3().getSubstring(1,1).equals("Y")))                //Natural: IF MSG-INFO-SUB.##RETURN-CODE = ' ' AND SUBSTRING ( CWFA2100.TBL-DATA3,1,1 ) = 'Y'
        {
            pnd_Rep_Parm_Pnd_Start_Date.setValue(pdaCwfa2100.getCwfa2100_Tbl_Data1().getSubstring(1,8));                                                                  //Natural: MOVE SUBSTRING ( CWFA2100.TBL-DATA1,1,8 ) TO #START-DATE
            pnd_Rep_Parm_Pnd_End_Date.setValue(pdaCwfa2100.getCwfa2100_Tbl_Data2().getSubstring(1,8));                                                                    //Natural: MOVE SUBSTRING ( CWFA2100.TBL-DATA2,1,8 ) TO #END-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  -----
        //* -- OVERRIDE REPORT DATE
        if (condition(DbsUtil.maskMatches(pnd_Rep_Parm_Pnd_Start_Date,"YYYYMMDD") && DbsUtil.maskMatches(pnd_Rep_Parm_Pnd_End_Date,"YYYYMMDD")))                          //Natural: IF #START-DATE = MASK ( YYYYMMDD ) AND #END-DATE = MASK ( YYYYMMDD )
        {
            pnd_Override_Sw.setValue(true);                                                                                                                               //Natural: ASSIGN #OVERRIDE-SW := TRUE
            //* -- COMPUTE FOR REPORT DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Override_Sw.setValue(false);                                                                                                                              //Natural: ASSIGN #OVERRIDE-SW := FALSE
            pnd_Work_Comp_Date.setValue(Global.getDATX());                                                                                                                //Natural: MOVE *DATX TO #WORK-COMP-DATE
            pnd_Day_Of_Week.setValueEdited(Global.getDATX(),new ReportEditMask("NNN"));                                                                                   //Natural: MOVE EDITED *DATX ( EM = NNN ) TO #DAY-OF-WEEK
            //*  REPORT GENERATED FROM LAST SUNDAY TO LATEST SATURDAY (7 DAYS)
            if (condition(pnd_Run_Type.notEquals("MONTHLY")))                                                                                                             //Natural: IF #RUN-TYPE NE 'MONTHLY'
            {
                short decideConditionsMet456 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #DAY-OF-WEEK;//Natural: VALUE 'Sat'
                if (condition((pnd_Day_Of_Week.equals("Sat"))))
                {
                    decideConditionsMet456++;
                    pnd_Days_To_Subtract.setValue(6);                                                                                                                     //Natural: MOVE 6 TO #DAYS-TO-SUBTRACT
                }                                                                                                                                                         //Natural: VALUE 'Sun'
                else if (condition((pnd_Day_Of_Week.equals("Sun"))))
                {
                    decideConditionsMet456++;
                    pnd_Days_To_Subtract.setValue(7);                                                                                                                     //Natural: MOVE 7 TO #DAYS-TO-SUBTRACT
                }                                                                                                                                                         //Natural: VALUE 'Mon'
                else if (condition((pnd_Day_Of_Week.equals("Mon"))))
                {
                    decideConditionsMet456++;
                    pnd_Days_To_Subtract.setValue(8);                                                                                                                     //Natural: MOVE 8 TO #DAYS-TO-SUBTRACT
                }                                                                                                                                                         //Natural: VALUE 'Tue'
                else if (condition((pnd_Day_Of_Week.equals("Tue"))))
                {
                    decideConditionsMet456++;
                    pnd_Days_To_Subtract.setValue(9);                                                                                                                     //Natural: MOVE 9 TO #DAYS-TO-SUBTRACT
                }                                                                                                                                                         //Natural: VALUE 'Wed'
                else if (condition((pnd_Day_Of_Week.equals("Wed"))))
                {
                    decideConditionsMet456++;
                    pnd_Days_To_Subtract.setValue(10);                                                                                                                    //Natural: MOVE 10 TO #DAYS-TO-SUBTRACT
                }                                                                                                                                                         //Natural: VALUE 'Thu'
                else if (condition((pnd_Day_Of_Week.equals("Thu"))))
                {
                    decideConditionsMet456++;
                    pnd_Days_To_Subtract.setValue(11);                                                                                                                    //Natural: MOVE 11 TO #DAYS-TO-SUBTRACT
                }                                                                                                                                                         //Natural: VALUE 'Fri'
                else if (condition((pnd_Day_Of_Week.equals("Fri"))))
                {
                    decideConditionsMet456++;
                    pnd_Days_To_Subtract.setValue(12);                                                                                                                    //Natural: MOVE 12 TO #DAYS-TO-SUBTRACT
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Work_Comp_Date.nsubtract(pnd_Days_To_Subtract);                                                                                                       //Natural: SUBTRACT #DAYS-TO-SUBTRACT FROM #WORK-COMP-DATE
                pnd_Rep_Parm_Pnd_Start_Date.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = YYYYMMDD ) TO #START-DATE
                pnd_Work_Comp_Date.nadd(6);                                                                                                                               //Natural: ADD 6 TO #WORK-COMP-DATE
                pnd_Rep_Parm_Pnd_End_Date.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = YYYYMMDD ) TO #END-DATE
                //*  THIS IS A MONTHLY RUN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *
                //* *  WE MUST CALCULATE THE FIRST AND LAST DAYS OF LAST MONTH. IF YOU
                //* *  SUBTRACT THE VALUE OF TODAYS DAY, YOU ALWAYS GET THE LAST DAY
                //* *  OF LAST MONTH. EXAMPLE: TODAY IS JAN 3. 3 DAYS AGO IS DEC 31.
                //* *
                pnd_Days_To_Subtract_Pnd_Days_To_Subtract_A.setValueEdited(Global.getDATX(),new ReportEditMask("DD"));                                                    //Natural: MOVE EDITED *DATX ( EM = DD ) TO #DAYS-TO-SUBTRACT-A
                pnd_Work_Comp_Date.nsubtract(pnd_Days_To_Subtract);                                                                                                       //Natural: SUBTRACT #DAYS-TO-SUBTRACT FROM #WORK-COMP-DATE
                pnd_Rep_Parm_Pnd_End_Date.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = YYYYMMDD ) TO #END-DATE
                pnd_Rep_Parm_Pnd_Start_Date.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                          //Natural: MOVE #END-DATE TO #START-DATE
                pnd_Rep_Parm_Pnd_Start_Date_Dd.setValue("01");                                                                                                            //Natural: MOVE '01' TO #START-DATE-DD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Comp_Date.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                                                //Natural: MOVE #END-DATE TO #COMP-DATE
        pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_Start_Date);                                                                                                               //Natural: MOVE #START-DATE TO #YYYYMMDD
        pnd_Rep_Parm_Pnd_Work_Mm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                                           //Natural: MOVE #MM TO #WORK-MM
        pnd_Rep_Parm_Pnd_Work_Dd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                                           //Natural: MOVE #DD TO #WORK-DD
        pnd_Rep_Parm_Pnd_Work_Yy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                                           //Natural: MOVE #YY TO #WORK-YY
        pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                                                 //Natural: MOVE #END-DATE TO #YYYYMMDD
        pnd_Rep_Parm_Pnd_Work_Mmm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                                          //Natural: MOVE #MM TO #WORK-MMM
        pnd_Rep_Parm_Pnd_Work_Ddd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                                          //Natural: MOVE #DD TO #WORK-DDD
        pnd_Rep_Parm_Pnd_Work_Yyy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                                          //Natural: MOVE #YY TO #WORK-YYY
        pnd_H5_Pnd_H5_Start_Date.setValue(pnd_Rep_Parm_Pnd_Work_Start_Date_A);                                                                                            //Natural: MOVE #WORK-START-DATE-A TO #H5-START-DATE
        pnd_H5_Pnd_H5_End_Date.setValue(pnd_Rep_Parm_Pnd_Work_End_Date_A);                                                                                                //Natural: MOVE #WORK-END-DATE-A TO #H5-END-DATE
        pnd_Rep_Parm_Pnd_Rep_Unit_Cde.setValue(pnd_Crc_Unit);                                                                                                             //Natural: MOVE #CRC-UNIT TO #REP-UNIT-CDE
        DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Rep_Parm_Pnd_Rep_Unit_Cde, pnd_Unit_Name);                                                         //Natural: CALLNAT 'CWFN1103' #REP-UNIT-CDE #UNIT-NAME
        if (condition(Global.isEscape())) return;
        pnd_Local_Data_Pnd_Total_New.nadd(pnd_Totl_Cnt.getValue("*",1));                                                                                                  //Natural: ADD #TOTL-CNT ( *,1 ) TO #TOTAL-NEW
        pnd_Local_Data_Pnd_Total_Retn_Doc.nadd(pnd_Totl_Cnt.getValue("*",2));                                                                                             //Natural: ADD #TOTL-CNT ( *,2 ) TO #TOTAL-RETN-DOC
        pnd_Local_Data_Pnd_Total_Logged.nadd(pnd_Totl_Cnt.getValue("*","*"));                                                                                             //Natural: ADD #TOTL-CNT ( *,* ) TO #TOTAL-LOGGED
        READWORK01:                                                                                                                                                       //Natural: READ WORK 2 #WORK-RECORD-SORT
        while (condition(getWorkFiles().read(2, pnd_Work_Record_Sort)))
        {
            getSort().writeSortInData(pnd_Work_Record_Sort_Site_Id, pnd_Work_Record_Sort_Tot_Log_Site, pnd_Work_Record_Sort_Wpid, pnd_Work_Record_Sort_New_Rqst_Site,     //Natural: END-ALL
                pnd_Work_Record_Sort_New_Rqst_All, pnd_Work_Record_Sort_Ret_Doc_Site, pnd_Work_Record_Sort_Ret_Doc_All, pnd_Work_Record_Sort_Tot_Log_All);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  CHARLOTE
        getSort().sortData(pnd_Work_Record_Sort_Site_Id, pnd_Work_Record_Sort_Tot_Log_Site, "DESCENDING", pnd_Work_Record_Sort_Wpid);                                     //Natural: SORT BY SITE-ID ASCENDING TOT-LOG-SITE DESCENDING WPID ASCENDING USING NEW-RQST-SITE NEW-RQST-ALL RET-DOC-SITE RET-DOC-ALL TOT-LOG-ALL
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Work_Record_Sort_Site_Id, pnd_Work_Record_Sort_Tot_Log_Site, pnd_Work_Record_Sort_Wpid, pnd_Work_Record_Sort_New_Rqst_Site, 
            pnd_Work_Record_Sort_New_Rqst_All, pnd_Work_Record_Sort_Ret_Doc_Site, pnd_Work_Record_Sort_Ret_Doc_All, pnd_Work_Record_Sort_Tot_Log_All)))
        {
            CheckAtStartofData561();

            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 );//Natural: AT TOP OF PAGE ( 3 )
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*  AT BREAK OF WPID
            //*    MOVE OLD(WPID) TO #DTL1-WPID-CODE
            pnd_D1_Pnd_Dtl1_Wpid_Code.setValue(pnd_Work_Record_Sort_Wpid);                                                                                                //Natural: MOVE WPID TO #DTL1-WPID-CODE
            //*  GET WPID DESCRIPTION AND OWNER UNIT CODE
            DbsUtil.callnat(Cwfn1125.class , getCurrentProcessState(), pnd_D1_Pnd_Dtl1_Wpid_Code, pnd_D1_Pnd_Dtl1_Wpid_Desc, pnd_D1_Pnd_Dtl1_Owner_Cde);                  //Natural: CALLNAT 'CWFN1125' #DTL1-WPID-CODE #DTL1-WPID-DESC #DTL1-OWNER-CDE
            if (condition(Global.isEscape())) return;
            pnd_D1_Pnd_Dtl1_Wpid_New.setValueEdited(pnd_Work_Record_Sort_New_Rqst_Site,new ReportEditMask("ZZZ,ZZ9"));                                                    //Natural: MOVE EDITED NEW-RQST-SITE ( EM = ZZZ,ZZ9 ) TO #DTL1-WPID-NEW
            pnd_D1_Pnd_Dtl1_Dtot_New.setValueEdited(pnd_Work_Record_Sort_New_Rqst_All,new ReportEditMask("ZZZ,ZZ9"));                                                     //Natural: MOVE EDITED NEW-RQST-ALL ( EM = ZZZ,ZZ9 ) TO #DTL1-DTOT-NEW
            pnd_D1_Pnd_Dtl1_Retrn_Doc.setValueEdited(pnd_Work_Record_Sort_Ret_Doc_Site,new ReportEditMask("ZZZ,ZZ9"));                                                    //Natural: MOVE EDITED RET-DOC-SITE ( EM = ZZZ,ZZ9 ) TO #DTL1-RETRN-DOC
            pnd_D1_Pnd_Dtl1_Dtot_Retn.setValueEdited(pnd_Work_Record_Sort_Ret_Doc_All,new ReportEditMask("ZZZ,ZZ9"));                                                     //Natural: MOVE EDITED RET-DOC-ALL ( EM = ZZZ,ZZ9 ) TO #DTL1-DTOT-RETN
            pnd_D1_Pnd_Dtl1_Wpid_Cnt.setValueEdited(pnd_Work_Record_Sort_Tot_Log_Site,new ReportEditMask("ZZZ,ZZ9"));                                                     //Natural: MOVE EDITED TOT-LOG-SITE ( EM = ZZZ,ZZ9 ) TO #DTL1-WPID-CNT
            pnd_D1_Pnd_Dtl1_Dtot_Cnt.setValueEdited(pnd_Work_Record_Sort_Tot_Log_All,new ReportEditMask("ZZZ,ZZ9"));                                                      //Natural: MOVE EDITED TOT-LOG-ALL ( EM = ZZZ,ZZ9 ) TO #DTL1-DTOT-CNT
            short decideConditionsMet576 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #SITE-ID;//Natural: VALUE 'C'
            if (condition((pnd_Local_Data_Pnd_Site_Id.equals("C"))))
            {
                decideConditionsMet576++;
                getReports().write(1, ReportOption.NOTITLE,pnd_D1_Pnd_Dtl_1);                                                                                             //Natural: WRITE ( 1 ) #DTL-1
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pnd_Local_Data_Pnd_Site_Id.equals("D"))))
            {
                decideConditionsMet576++;
                getReports().write(2, ReportOption.NOTITLE,pnd_D1_Pnd_Dtl_1);                                                                                             //Natural: WRITE ( 2 ) #DTL-1
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pnd_Local_Data_Pnd_Site_Id.equals("N"))))
            {
                decideConditionsMet576++;
                getReports().write(3, ReportOption.NOTITLE,pnd_D1_Pnd_Dtl_1);                                                                                             //Natural: WRITE ( 3 ) #DTL-1
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  END-BREAK
            //*                                                                                                                                                           //Natural: AT BREAK OF SITE-ID
            //*                                                                                                                                                           //Natural: AT END OF DATA
            //*  READ-2.
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-CWFN2100
    }
    //*  JF030102
    private void sub_Call_Cwfn2100() throws Exception                                                                                                                     //Natural: CALL-CWFN2100
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        DbsUtil.callnat(Cwfn2100.class , getCurrentProcessState(), pdaCwfa2100.getCwfa2100(), pdaCwfa2100.getCwfa2100_Id(), pdaCwfa2101.getCwfa2101(),                    //Natural: CALLNAT 'CWFN2100' CWFA2100 CWFA2100-ID CWFA2101 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobjr.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page.nadd(1);                                                                                                                                     //Natural: COMPUTE #PAGE = #PAGE + 1
                    pnd_H1_Pnd_H1_Page.setValue(DbsUtil.compress("PAGE", pnd_Page));                                                                                      //Natural: COMPRESS 'PAGE' #PAGE INTO #H1-PAGE
                    //*  #SITE-ID
                    pnd_H1_Pnd_H1_Page.setValue(pnd_H1_Pnd_H1_Page, MoveOption.RightJustified);                                                                           //Natural: MOVE RIGHT #H1-PAGE TO #H1-PAGE
                    getReports().write(1, ReportOption.NOTITLE,pnd_H1_Pnd_Hdr_1,NEWLINE,pnd_H2_Pnd_Hdr_2,NEWLINE);                                                        //Natural: WRITE ( 1 ) NOTITLE #HDR-1 / #HDR-2 /
                    getReports().write(1, ReportOption.NOTITLE,"UNIT      :",pnd_Rep_Parm_Pnd_Rep_Unit_Cde,pnd_Unit_Name);                                                //Natural: WRITE ( 1 ) 'UNIT      :' #REP-UNIT-CDE #UNIT-NAME
                    getReports().write(1, ReportOption.NOTITLE,"SITE ID   :","C");                                                                                        //Natural: WRITE ( 1 ) 'SITE ID   :' 'C'
                    getReports().write(1, ReportOption.NOTITLE,pnd_H5_Pnd_Hdr_5);                                                                                         //Natural: WRITE ( 1 ) #HDR-5
                    getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(130),NEWLINE);                                                                          //Natural: WRITE ( 1 ) '=' ( 130 ) /
                    pnd_H9_Pnd_H9_Site_1.setValue("CHARLOTTE");                                                                                                           //Natural: MOVE 'CHARLOTTE' TO #H9-SITE-1 #H9-SITE-2 #H9-SITE-3
                    pnd_H9_Pnd_H9_Site_2.setValue("CHARLOTTE");
                    pnd_H9_Pnd_H9_Site_3.setValue("CHARLOTTE");
                    getReports().write(1, ReportOption.NOTITLE,pnd_H8_Pnd_Hdr_8,NEWLINE,pnd_H9_Pnd_Hdr_9,NEWLINE,pnd_H10_Pnd_Hdr_10);                                     //Natural: WRITE ( 1 ) #HDR-8 / #HDR-9 / #HDR-10
                    //*  DENVER
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page2.nadd(1);                                                                                                                                    //Natural: COMPUTE #PAGE2 = #PAGE2 + 1
                    pnd_H1_Pnd_H1_Page.setValue(DbsUtil.compress("PAGE", pnd_Page2));                                                                                     //Natural: COMPRESS 'PAGE' #PAGE2 INTO #H1-PAGE
                    //*  #SITE-ID
                    pnd_H1_Pnd_H1_Page.setValue(pnd_H1_Pnd_H1_Page, MoveOption.RightJustified);                                                                           //Natural: MOVE RIGHT #H1-PAGE TO #H1-PAGE
                    getReports().write(2, ReportOption.NOTITLE,pnd_H1_Pnd_Hdr_1,NEWLINE,pnd_H2_Pnd_Hdr_2,NEWLINE);                                                        //Natural: WRITE ( 2 ) NOTITLE #HDR-1 / #HDR-2 /
                    getReports().write(2, ReportOption.NOTITLE,"UNIT      :",pnd_Rep_Parm_Pnd_Rep_Unit_Cde,pnd_Unit_Name);                                                //Natural: WRITE ( 2 ) 'UNIT      :' #REP-UNIT-CDE #UNIT-NAME
                    getReports().write(2, ReportOption.NOTITLE,"SITE ID   :","D");                                                                                        //Natural: WRITE ( 2 ) 'SITE ID   :' 'D'
                    getReports().write(2, ReportOption.NOTITLE,pnd_H5_Pnd_Hdr_5);                                                                                         //Natural: WRITE ( 2 ) #HDR-5
                    getReports().write(2, ReportOption.NOTITLE,"=",new RepeatItem(130),NEWLINE);                                                                          //Natural: WRITE ( 2 ) '=' ( 130 ) /
                    pnd_H9_Pnd_H9_Site_1.setValue("   DENVER");                                                                                                           //Natural: MOVE '   DENVER' TO #H9-SITE-1 #H9-SITE-2 #H9-SITE-3
                    pnd_H9_Pnd_H9_Site_2.setValue("   DENVER");
                    pnd_H9_Pnd_H9_Site_3.setValue("   DENVER");
                    getReports().write(2, ReportOption.NOTITLE,pnd_H8_Pnd_Hdr_8,NEWLINE,pnd_H9_Pnd_Hdr_9,NEWLINE,pnd_H10_Pnd_Hdr_10);                                     //Natural: WRITE ( 2 ) #HDR-8 / #HDR-9 / #HDR-10
                    //*  NEW YORK
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page3.nadd(1);                                                                                                                                    //Natural: COMPUTE #PAGE3 = #PAGE3 + 1
                    pnd_H1_Pnd_H1_Page.setValue(DbsUtil.compress("PAGE", pnd_Page3));                                                                                     //Natural: COMPRESS 'PAGE' #PAGE3 INTO #H1-PAGE
                    //*  #SITE-ID
                    pnd_H1_Pnd_H1_Page.setValue(pnd_H1_Pnd_H1_Page, MoveOption.RightJustified);                                                                           //Natural: MOVE RIGHT #H1-PAGE TO #H1-PAGE
                    getReports().write(3, ReportOption.NOTITLE,pnd_H1_Pnd_Hdr_1,NEWLINE,pnd_H2_Pnd_Hdr_2,NEWLINE);                                                        //Natural: WRITE ( 3 ) NOTITLE #HDR-1 / #HDR-2 /
                    getReports().write(3, ReportOption.NOTITLE,"UNIT      :",pnd_Rep_Parm_Pnd_Rep_Unit_Cde,pnd_Unit_Name);                                                //Natural: WRITE ( 3 ) 'UNIT      :' #REP-UNIT-CDE #UNIT-NAME
                    getReports().write(3, ReportOption.NOTITLE,"SITE ID   :","N");                                                                                        //Natural: WRITE ( 3 ) 'SITE ID   :' 'N'
                    getReports().write(3, ReportOption.NOTITLE,pnd_H5_Pnd_Hdr_5);                                                                                         //Natural: WRITE ( 3 ) #HDR-5
                    getReports().write(3, ReportOption.NOTITLE,"=",new RepeatItem(130),NEWLINE);                                                                          //Natural: WRITE ( 3 ) '=' ( 130 ) /
                    pnd_H9_Pnd_H9_Site_1.setValue(" NEW YORK");                                                                                                           //Natural: MOVE ' NEW YORK' TO #H9-SITE-1 #H9-SITE-2 #H9-SITE-3
                    pnd_H9_Pnd_H9_Site_2.setValue(" NEW YORK");
                    pnd_H9_Pnd_H9_Site_3.setValue(" NEW YORK");
                    getReports().write(3, ReportOption.NOTITLE,pnd_H8_Pnd_Hdr_8,NEWLINE,pnd_H9_Pnd_Hdr_9,NEWLINE,pnd_H10_Pnd_Hdr_10);                                     //Natural: WRITE ( 3 ) #HDR-8 / #HDR-9 / #HDR-10
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Work_Record_Sort_Site_IdIsBreak = pnd_Work_Record_Sort_Site_Id.isBreak(endOfData);
        if (condition(pnd_Work_Record_Sort_Site_IdIsBreak))
        {
            pnd_Local_Data_Pnd_Wpid_Count.reset();                                                                                                                        //Natural: RESET #WPID-COUNT
            short decideConditionsMet590 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #SITE-ID;//Natural: VALUE 'C'
            if (condition((pnd_Local_Data_Pnd_Site_Id.equals("C"))))
            {
                decideConditionsMet590++;
                pnd_T1_Pnd_Tot1_Total_New.setValueEdited(pnd_Totl_Cnt.getValue(1,1),new ReportEditMask("ZZZ,ZZ9"));                                                       //Natural: MOVE EDITED #TOTL-CNT ( 1,1 ) ( EM = ZZZ,ZZ9 ) TO #TOT1-TOTAL-NEW
                pnd_T1_Pnd_Tot1_Retrn_Doc.setValueEdited(pnd_Totl_Cnt.getValue(1,2),new ReportEditMask("ZZZ,ZZ9"));                                                       //Natural: MOVE EDITED #TOTL-CNT ( 1,2 ) ( EM = ZZZ,ZZ9 ) TO #TOT1-RETRN-DOC
                pnd_Local_Data_Pnd_Wpid_Count.nadd(pnd_Totl_Cnt.getValue(1,1,":",2));                                                                                     //Natural: ADD #TOTL-CNT ( 1,1:2 ) TO #WPID-COUNT
                pnd_T1_Pnd_Tot1_Logged.setValueEdited(pnd_Local_Data_Pnd_Wpid_Count,new ReportEditMask("ZZZ,ZZ9"));                                                       //Natural: MOVE EDITED #WPID-COUNT ( EM = ZZZ,ZZ9 ) TO #TOT1-LOGGED
                pnd_T1_Pnd_Tot1_Stot_New.setValueEdited(pnd_Local_Data_Pnd_Total_New,new ReportEditMask("ZZZ,ZZ9"));                                                      //Natural: MOVE EDITED #TOTAL-NEW ( EM = ZZZ,ZZ9 ) TO #TOT1-STOT-NEW
                pnd_T1_Pnd_Tot1_Stot_Retn.setValueEdited(pnd_Local_Data_Pnd_Total_Retn_Doc,new ReportEditMask("ZZZ,ZZ9"));                                                //Natural: MOVE EDITED #TOTAL-RETN-DOC ( EM = ZZZ,ZZ9 ) TO #TOT1-STOT-RETN
                pnd_T1_Pnd_Tot1_Stot_Log.setValueEdited(pnd_Local_Data_Pnd_Total_Logged,new ReportEditMask("ZZZ,ZZ9"));                                                   //Natural: MOVE EDITED #TOTAL-LOGGED ( EM = ZZZ,ZZ9 ) TO #TOT1-STOT-LOG
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_T1_Pnd_Tot_1,NEWLINE);                                                                             //Natural: WRITE ( 1 ) / #TOT-1 /
                if (condition(Global.isEscape())) return;
                pnd_T2_Pnd_Tot2_Unclear.setValueEdited(pnd_Total_Unclear.getValue(1),new ReportEditMask("ZZZ,ZZ9"));                                                      //Natural: MOVE EDITED #TOTAL-UNCLEAR ( 1 ) ( EM = ZZZ,ZZ9 ) TO #TOT2-UNCLEAR
                getReports().write(1, ReportOption.NOTITLE,pnd_T2_Pnd_Tot_2);                                                                                             //Natural: WRITE ( 1 ) #TOT-2
                if (condition(Global.isEscape())) return;
                if (condition(getReports().getAstLineCount(1).greater(53)))                                                                                               //Natural: IF *LINE-COUNT ( 1 ) GT 53
                {
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape())){return;}
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,pnd_Hdr_11,NEWLINE,NEWLINE,pnd_H12_Pnd_Hdr_12,NEWLINE,pnd_H13_Pnd_Hdr_13,NEWLINE,              //Natural: WRITE ( 1 ) NOTITLE // #HDR-11 // #HDR-12 / #HDR-13 / #HDR-14
                    pnd_H14_Pnd_Hdr_14);
                if (condition(Global.isEscape())) return;
                pnd_T3_Pnd_Tot3_Ctrs.getValue("*").setValueEdited(pnd_Counters.getValue(1,1,"*"),new ReportEditMask("ZZZZZ9"));                                           //Natural: MOVE EDITED #COUNTERS ( 1,1,* ) ( EM = ZZZZZ9 ) TO #TOT3-CTRS ( * )
                pnd_T3_Pnd_Tot3_Message.setValue("(LOGGED)");                                                                                                             //Natural: MOVE '(LOGGED)' TO #TOT3-MESSAGE
                getReports().write(1, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 1 ) #TOT-3
                if (condition(Global.isEscape())) return;
                pnd_T3_Pnd_Tot3_Ctrs.getValue("*").setValueEdited(pnd_Counters.getValue(1,2,"*"),new ReportEditMask("ZZZZZ9"));                                           //Natural: MOVE EDITED #COUNTERS ( 1,2,* ) ( EM = ZZZZZ9 ) TO #TOT3-CTRS ( * )
                pnd_T3_Pnd_Tot3_Message.setValue("(RETURNING DOCUMENTS)");                                                                                                //Natural: MOVE '(RETURNING DOCUMENTS)' TO #TOT3-MESSAGE
                getReports().write(1, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 1 ) #TOT-3
                if (condition(Global.isEscape())) return;
                pnd_Local_Data_Pnd_Site_Id.setValue(pnd_Work_Record_Sort_Site_Id);                                                                                        //Natural: MOVE SITE-ID TO #SITE-ID
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pnd_Local_Data_Pnd_Site_Id.equals("D"))))
            {
                decideConditionsMet590++;
                pnd_T1_Pnd_Tot1_Total_New.setValueEdited(pnd_Totl_Cnt.getValue(2,1),new ReportEditMask("ZZZ,ZZ9"));                                                       //Natural: MOVE EDITED #TOTL-CNT ( 2,1 ) ( EM = ZZZ,ZZ9 ) TO #TOT1-TOTAL-NEW
                pnd_T1_Pnd_Tot1_Retrn_Doc.setValueEdited(pnd_Totl_Cnt.getValue(2,2),new ReportEditMask("ZZZ,ZZ9"));                                                       //Natural: MOVE EDITED #TOTL-CNT ( 2,2 ) ( EM = ZZZ,ZZ9 ) TO #TOT1-RETRN-DOC
                pnd_Local_Data_Pnd_Wpid_Count.nadd(pnd_Totl_Cnt.getValue(2,1,":",2));                                                                                     //Natural: ADD #TOTL-CNT ( 2,1:2 ) TO #WPID-COUNT
                pnd_T1_Pnd_Tot1_Logged.setValueEdited(pnd_Local_Data_Pnd_Wpid_Count,new ReportEditMask("ZZZ,ZZ9"));                                                       //Natural: MOVE EDITED #WPID-COUNT ( EM = ZZZ,ZZ9 ) TO #TOT1-LOGGED
                pnd_T1_Pnd_Tot1_Stot_New.setValueEdited(pnd_Local_Data_Pnd_Total_New,new ReportEditMask("ZZZ,ZZ9"));                                                      //Natural: MOVE EDITED #TOTAL-NEW ( EM = ZZZ,ZZ9 ) TO #TOT1-STOT-NEW
                pnd_T1_Pnd_Tot1_Stot_Retn.setValueEdited(pnd_Local_Data_Pnd_Total_Retn_Doc,new ReportEditMask("ZZZ,ZZ9"));                                                //Natural: MOVE EDITED #TOTAL-RETN-DOC ( EM = ZZZ,ZZ9 ) TO #TOT1-STOT-RETN
                pnd_T1_Pnd_Tot1_Stot_Log.setValueEdited(pnd_Local_Data_Pnd_Total_Logged,new ReportEditMask("ZZZ,ZZ9"));                                                   //Natural: MOVE EDITED #TOTAL-LOGGED ( EM = ZZZ,ZZ9 ) TO #TOT1-STOT-LOG
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,pnd_T1_Pnd_Tot_1,NEWLINE);                                                                             //Natural: WRITE ( 2 ) / #TOT-1 /
                if (condition(Global.isEscape())) return;
                pnd_T2_Pnd_Tot2_Unclear.setValueEdited(pnd_Total_Unclear.getValue(2),new ReportEditMask("ZZZ,ZZ9"));                                                      //Natural: MOVE EDITED #TOTAL-UNCLEAR ( 2 ) ( EM = ZZZ,ZZ9 ) TO #TOT2-UNCLEAR
                getReports().write(2, ReportOption.NOTITLE,pnd_T2_Pnd_Tot_2);                                                                                             //Natural: WRITE ( 2 ) #TOT-2
                if (condition(Global.isEscape())) return;
                if (condition(getReports().getAstLineCount(2).greater(53)))                                                                                               //Natural: IF *LINE-COUNT ( 2 ) GT 53
                {
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape())){return;}
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,pnd_Hdr_11,NEWLINE,NEWLINE,pnd_H12_Pnd_Hdr_12,NEWLINE,pnd_H13_Pnd_Hdr_13,NEWLINE,              //Natural: WRITE ( 2 ) NOTITLE // #HDR-11 // #HDR-12 / #HDR-13 / #HDR-14
                    pnd_H14_Pnd_Hdr_14);
                if (condition(Global.isEscape())) return;
                pnd_T3_Pnd_Tot3_Ctrs.getValue("*").setValueEdited(pnd_Counters.getValue(2,1,"*"),new ReportEditMask("ZZZZZ9"));                                           //Natural: MOVE EDITED #COUNTERS ( 2,1,* ) ( EM = ZZZZZ9 ) TO #TOT3-CTRS ( * )
                pnd_T3_Pnd_Tot3_Message.setValue("(LOGGED)");                                                                                                             //Natural: MOVE '(LOGGED)' TO #TOT3-MESSAGE
                getReports().write(2, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 2 ) #TOT-3
                if (condition(Global.isEscape())) return;
                pnd_T3_Pnd_Tot3_Ctrs.getValue("*").setValueEdited(pnd_Counters.getValue(2,2,"*"),new ReportEditMask("ZZZZZ9"));                                           //Natural: MOVE EDITED #COUNTERS ( 2,2,* ) ( EM = ZZZZZ9 ) TO #TOT3-CTRS ( * )
                pnd_T3_Pnd_Tot3_Message.setValue("(RETURNING DOCUMENTS)");                                                                                                //Natural: MOVE '(RETURNING DOCUMENTS)' TO #TOT3-MESSAGE
                getReports().write(2, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 2 ) #TOT-3
                if (condition(Global.isEscape())) return;
                pnd_Local_Data_Pnd_Site_Id.setValue(pnd_Work_Record_Sort_Site_Id);                                                                                        //Natural: MOVE SITE-ID TO #SITE-ID
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pnd_Local_Data_Pnd_Site_Id.equals("N"))))
            {
                decideConditionsMet590++;
                pnd_T1_Pnd_Tot1_Total_New.setValueEdited(pnd_Totl_Cnt.getValue(3,1),new ReportEditMask("ZZZ,ZZ9"));                                                       //Natural: MOVE EDITED #TOTL-CNT ( 3,1 ) ( EM = ZZZ,ZZ9 ) TO #TOT1-TOTAL-NEW
                pnd_T1_Pnd_Tot1_Retrn_Doc.setValueEdited(pnd_Totl_Cnt.getValue(3,2),new ReportEditMask("ZZZ,ZZ9"));                                                       //Natural: MOVE EDITED #TOTL-CNT ( 3,2 ) ( EM = ZZZ,ZZ9 ) TO #TOT1-RETRN-DOC
                pnd_Local_Data_Pnd_Wpid_Count.nadd(pnd_Totl_Cnt.getValue(3,1,":",2));                                                                                     //Natural: ADD #TOTL-CNT ( 3,1:2 ) TO #WPID-COUNT
                pnd_T1_Pnd_Tot1_Logged.setValueEdited(pnd_Local_Data_Pnd_Wpid_Count,new ReportEditMask("ZZZ,ZZ9"));                                                       //Natural: MOVE EDITED #WPID-COUNT ( EM = ZZZ,ZZ9 ) TO #TOT1-LOGGED
                pnd_T1_Pnd_Tot1_Stot_New.setValueEdited(pnd_Local_Data_Pnd_Total_New,new ReportEditMask("ZZZ,ZZ9"));                                                      //Natural: MOVE EDITED #TOTAL-NEW ( EM = ZZZ,ZZ9 ) TO #TOT1-STOT-NEW
                pnd_T1_Pnd_Tot1_Stot_Retn.setValueEdited(pnd_Local_Data_Pnd_Total_Retn_Doc,new ReportEditMask("ZZZ,ZZ9"));                                                //Natural: MOVE EDITED #TOTAL-RETN-DOC ( EM = ZZZ,ZZ9 ) TO #TOT1-STOT-RETN
                pnd_T1_Pnd_Tot1_Stot_Log.setValueEdited(pnd_Local_Data_Pnd_Total_Logged,new ReportEditMask("ZZZ,ZZ9"));                                                   //Natural: MOVE EDITED #TOTAL-LOGGED ( EM = ZZZ,ZZ9 ) TO #TOT1-STOT-LOG
                getReports().write(3, ReportOption.NOTITLE,NEWLINE,pnd_T1_Pnd_Tot_1,NEWLINE);                                                                             //Natural: WRITE ( 3 ) / #TOT-1 /
                if (condition(Global.isEscape())) return;
                pnd_T2_Pnd_Tot2_Unclear.setValueEdited(pnd_Total_Unclear.getValue(3),new ReportEditMask("ZZZ,ZZ9"));                                                      //Natural: MOVE EDITED #TOTAL-UNCLEAR ( 3 ) ( EM = ZZZ,ZZ9 ) TO #TOT2-UNCLEAR
                getReports().write(3, ReportOption.NOTITLE,pnd_T2_Pnd_Tot_2);                                                                                             //Natural: WRITE ( 3 ) #TOT-2
                if (condition(Global.isEscape())) return;
                if (condition(getReports().getAstLineCount(3).greater(53)))                                                                                               //Natural: IF *LINE-COUNT ( 3 ) GT 53
                {
                    getReports().newPage(new ReportSpecification(3));                                                                                                     //Natural: NEWPAGE ( 3 )
                    if (condition(Global.isEscape())){return;}
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,pnd_Hdr_11,NEWLINE,NEWLINE,pnd_H12_Pnd_Hdr_12,NEWLINE,pnd_H13_Pnd_Hdr_13,NEWLINE,              //Natural: WRITE ( 3 ) NOTITLE // #HDR-11 // #HDR-12 / #HDR-13 / #HDR-14
                    pnd_H14_Pnd_Hdr_14);
                if (condition(Global.isEscape())) return;
                pnd_T3_Pnd_Tot3_Ctrs.getValue("*").setValueEdited(pnd_Counters.getValue(3,1,"*"),new ReportEditMask("ZZZZZ9"));                                           //Natural: MOVE EDITED #COUNTERS ( 3,1,* ) ( EM = ZZZZZ9 ) TO #TOT3-CTRS ( * )
                pnd_T3_Pnd_Tot3_Message.setValue("(LOGGED)");                                                                                                             //Natural: MOVE '(LOGGED)' TO #TOT3-MESSAGE
                getReports().write(3, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 3 ) #TOT-3
                if (condition(Global.isEscape())) return;
                pnd_T3_Pnd_Tot3_Ctrs.getValue("*").setValueEdited(pnd_Counters.getValue(3,2,"*"),new ReportEditMask("ZZZZZ9"));                                           //Natural: MOVE EDITED #COUNTERS ( 3,2,* ) ( EM = ZZZZZ9 ) TO #TOT3-CTRS ( * )
                pnd_T3_Pnd_Tot3_Message.setValue("(RETURNING DOCUMENTS)");                                                                                                //Natural: MOVE '(RETURNING DOCUMENTS)' TO #TOT3-MESSAGE
                getReports().write(3, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 3 ) #TOT-3
                if (condition(Global.isEscape())) return;
                pnd_Local_Data_Pnd_Site_Id.setValue(pnd_Work_Record_Sort_Site_Id);                                                                                        //Natural: MOVE SITE-ID TO #SITE-ID
                getReports().newPage(new ReportSpecification(3));                                                                                                         //Natural: NEWPAGE ( 3 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=58");
        Global.format(2, "LS=132 PS=58");
        Global.format(3, "LS=132 PS=58");
    }
    private void CheckAtStartofData561() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
            pnd_Local_Data_Pnd_Site_Id.setValue(pnd_Work_Record_Sort_Site_Id);                                                                                            //Natural: MOVE SITE-ID TO #SITE-ID
        }                                                                                                                                                                 //Natural: END-START
    }
}
