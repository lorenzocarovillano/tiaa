/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:17:13 AM
**        * FROM NATURAL SUBPROGRAM : Efsn6130
************************************************************
**        * FILE NAME            : Efsn6130.java
**        * CLASS NAME           : Efsn6130
**        * INSTANCE NAME        : Efsn6130
************************************************************
***********************************************************************
* SUB-PGM  : EFSN6130
* DATE     : 01/22/2003
* TITLE    : ADD ORPHAN IMAGES TO APPROPRIATE DOCUMENT FILE(S).
* FUNCTION : THIS PROGRAM RUNS IN BATCH WITH TWO ACTIONS CODES AD AND
*            AN. THIS PROCESSES CWF-IMAGE-XREF FILE WITHOUT DOCUMENT
*            IMAGES AND FINDS THE MASTER INDEX MISCELLENEOUS WPID
*            DEPENDING ON XREF CABINET ID OR PIN NBR KEY. AD ACTION
*            CODE IS EXECUTED IF Z WPID IS FOUND OTHERWISE IT's AN.
*
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
*  MM DD YY  02/06/03 DEFINE, FORMAT AND NEWPAGE COMMENTED (L.E.)___
*  MM DD YY  02/06/03 WRITE-STATISTICAL-COUNT RTN DELETED  (L.E.)___
*  MM DD YY  03/19/03 ERROR MESSAGE MOVED DIRECTLY FROM MODULE (L.E.)
*  MM DD YY  04/11/03 OPERATOR CODE CHANGE FROM "DEZENDO" TO "BATCHZAP"
*  05 22 07  RCC      CHANGE COMPARE OF MIN FROM A14 TO A11
* 02/23/2017 - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsn6130 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaEfsa6110 pdaEfsa6110;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaIefa9000 pdaIefa9000;
    private PdaNefa9000 pdaNefa9000;
    private PdaPsta9657 pdaPsta9657;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ncw_Master_Index;
    private DbsField ncw_Master_Index_Np_Pin;
    private DbsField ncw_Master_Index_Rqst_Log_Dte_Tme;
    private DbsField ncw_Master_Index_Tiaa_Rcvd_Dte_Tme;
    private DbsField ncw_Master_Index_Elctrn_Fld_Ind;
    private DbsField ncw_Master_Index_Work_Prcss_Id;
    private DbsField ncw_Master_Index_Admin_Unit_Cde;
    private DbsField ncw_Master_Index_Rqst_Id;

    private DbsGroup ncw_Master_Index__R_Field_1;
    private DbsField ncw_Master_Index_Rqst_Work_Prcss_Id;
    private DbsField ncw_Master_Index_Rqst_Tiaa_Rcvd_Dte;
    private DbsField ncw_Master_Index_Rqst_Case_Id_Cde;

    private DbsGroup ncw_Master_Index__R_Field_2;
    private DbsField ncw_Master_Index_Pnd_Case_Id_Cde;
    private DbsField ncw_Master_Index_Pnd_Subrqst_Id_Cde;
    private DbsField ncw_Master_Index_Rqst_Np_Pin;

    private DataAccessProgramView vw_icw_Master_Index;
    private DbsField icw_Master_Index_Rqst_Log_Dte_Tme;
    private DbsField icw_Master_Index_Ppg_Cde;
    private DbsField icw_Master_Index_Actve_Ind;
    private DbsField icw_Master_Index_Iis_Instn_Link_Cde;
    private DbsField icw_Master_Index_Iis_Hrchy_Level_Cde;
    private DbsField icw_Master_Index_Elctrn_Fld_Ind;
    private DbsField icw_Master_Index_Work_Prcss_Id;
    private DbsField icw_Master_Index_Tiaa_Rcvd_Dte_Tme;
    private DbsField icw_Master_Index_Admin_Unit_Cde;
    private DbsField icw_Master_Index_Rqst_Id;

    private DbsGroup icw_Master_Index__R_Field_3;
    private DbsField icw_Master_Index_Rqst_Work_Prcss_Id;
    private DbsField icw_Master_Index_Rqst_Tiaa_Rcvd_Dte;
    private DbsField icw_Master_Index_Rqst_Case_Id_Cde;

    private DbsGroup icw_Master_Index__R_Field_4;
    private DbsField icw_Master_Index_Pnd_Case_Id_Cde;
    private DbsField icw_Master_Index_Pnd_Subrqst_Id_Cde;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_5;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Rgn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Brnch_Cde;
    private DbsField cwf_Master_Index_View_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Case_Id_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_6;
    private DbsField cwf_Master_Index_View_Case_Ind;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind;
    private DbsField cwf_Master_Index_View_Multi_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_7;
    private DbsField cwf_Master_Index_View_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Unit_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_8;
    private DbsField cwf_Master_Index_View_Unit_Id_Cde;
    private DbsField cwf_Master_Index_View_Unit_Rgn_Cde;
    private DbsField cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_View_Unit_Brnch_Group_Cde;
    private DbsField cwf_Master_Index_View_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Work_Rqst_Prty_Cde;
    private DbsField cwf_Master_Index_View_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_9;
    private DbsField cwf_Master_Index_View_Empl_Racf_Id;
    private DbsField cwf_Master_Index_View_Empl_Sffx_Cde;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Cde;
    private DbsField cwf_Master_Index_View_Status_Cde;
    private DbsField cwf_Master_Index_View_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Updte_Dte;
    private DbsField cwf_Master_Index_View_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Cntct_Orgn_Type_Cde;
    private DbsField cwf_Master_Index_View_Cntct_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Oprtr_Id;
    private DbsField cwf_Master_Index_View_Actve_Ind;
    private DbsField cwf_Master_Index_View_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_View_Cnflct_Ind;
    private DbsField cwf_Master_Index_View_Spcl_Hndlng_Txt;
    private DbsField cwf_Master_Index_View_Instn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Instn_Cde;
    private DbsField cwf_Master_Index_View_Check_Ind;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_View_Effctve_Dte;
    private DbsField cwf_Master_Index_View_Trnsctn_Dte;
    private DbsField cwf_Master_Index_View_Trans_Dte;
    private DbsField cwf_Master_Index_View_Rescan_Ind;
    private DbsField cwf_Master_Index_View_Dup_Ind;
    private DbsField cwf_Master_Index_View_Cmplnt_Ind;
    private DbsGroup cwf_Master_Index_View_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Cntrct_Nbr;
    private DbsGroup cwf_Master_Index_View_Mail_Item_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Mail_Item_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_10;
    private DbsField cwf_Master_Index_View_Rqst_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Case_Id_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Elctrnc_Fldr_Ind;
    private DbsField cwf_Master_Index_View_Log_Rqstr_Cde;
    private DbsField cwf_Master_Index_View_Crprte_Due_Dte_Tme;
    private DbsField cwf_Master_Index_View_Owner_Unit_Cde;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_View_Instn_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Log_Sqnce_Nbr;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Tbl_Table_Access_Level;

    private DataAccessProgramView vw_cwf_Image_Xref;
    private DbsField cwf_Image_Xref_Mail_Item_Nbr;
    private DbsField cwf_Image_Xref_Image_Document_Address_Cde;
    private DbsField cwf_Image_Xref_Ph_Unique_Id_Nbr;
    private DbsField cwf_Image_Xref_Image_Quality_Ind;
    private DbsField cwf_Image_Xref_Upld_Date_Time;
    private DbsField cwf_Image_Xref_Number_Of_Pages;
    private DbsField cwf_Image_Xref_Source_Id;
    private DbsField cwf_Image_Xref_Capture_Id;
    private DbsField cwf_Image_Xref_Batch_Id;
    private DbsField cwf_Image_Xref_Image_Delete_Ind;
    private DbsField cwf_Image_Xref_Audit_System;
    private DbsField cwf_Image_Xref_Audit_Dte_Tme;
    private DbsField cwf_Image_Xref_Audit_Empl_Racf_Id;
    private DbsField cwf_Image_Xref_Cabinet_Id;

    private DbsGroup cwf_Image_Xref__R_Field_11;
    private DbsField cwf_Image_Xref_Pnd_Np_Pin;

    private DbsGroup cwf_Image_Xref__R_Field_12;
    private DbsField cwf_Image_Xref_Pnd_Ppg_Code1;

    private DbsGroup cwf_Image_Xref__R_Field_13;
    private DbsField cwf_Image_Xref_Pnd_Inst_Code;
    private DbsField cwf_Image_Xref_Pnd_Ppg_Code2;

    private DbsGroup cwf_Image_Xref__R_Field_14;
    private DbsField cwf_Image_Xref_Pnd_Ppg_Cd4;
    private DbsField cwf_Image_Xref_Archive_Dte_Tme;
    private DbsField cwf_Image_Xref_Identify_Dte_Tme;
    private DbsField cwf_Image_Xref_Source_Id_Min_Key;

    private DataAccessProgramView vw_cwf_Efm_Document;
    private DbsField cwf_Efm_Document_Cabinet_Id;

    private DbsGroup cwf_Efm_Document__R_Field_15;
    private DbsField cwf_Efm_Document_Pin_Prefix;
    private DbsField cwf_Efm_Document_Pin_Nbr;

    private DbsGroup cwf_Efm_Document_Folder_Id;
    private DbsField cwf_Efm_Document_Tiaa_Rcvd_Dte;
    private DbsField cwf_Efm_Document_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Efm_Document__R_Field_16;
    private DbsField cwf_Efm_Document_Case_Ind;
    private DbsField cwf_Efm_Document_Originating_Work_Prcss_Id;
    private DbsField cwf_Efm_Document_Multi_Rqst_Ind;
    private DbsField cwf_Efm_Document_Sub_Rqst_Ind;
    private DbsField cwf_Efm_Document_Entry_Dte_Tme;
    private DbsField cwf_Efm_Document_Image_Min;
    private DbsField cwf_Efm_Document_Document_Retention_Ind;
    private DbsField cwf_Efm_Document_Image_Source_Id;
    private DbsField cwf_Efm_Document_Entry_System_Or_Unit;

    private DataAccessProgramView vw_ncw_Efm_Document;
    private DbsField ncw_Efm_Document_Np_Pin;
    private DbsField ncw_Efm_Document_Rqst_Log_Dte_Tme;
    private DbsField ncw_Efm_Document_Crte_Dte_Tme;
    private DbsField ncw_Efm_Document_Crte_Sqnce_Nbr;
    private DbsField ncw_Efm_Document_Crte_Empl_Racf_Id;
    private DbsField ncw_Efm_Document_Crte_Unit;
    private DbsField ncw_Efm_Document_Crte_System;
    private DbsField ncw_Efm_Document_Crte_Jobname;

    private DbsGroup ncw_Efm_Document_Dcmnt_Type;
    private DbsField ncw_Efm_Document_Dcmnt_Ctgry;
    private DbsField ncw_Efm_Document_Dcmnt_Sub_Ctgry;
    private DbsField ncw_Efm_Document_Dcmnt_Dtl;
    private DbsField ncw_Efm_Document_Dcmnt_Dscrptn;
    private DbsField ncw_Efm_Document_Dcmnt_Drctn;
    private DbsField ncw_Efm_Document_Dcmnt_Scrty_Ind;
    private DbsField ncw_Efm_Document_Dcmnt_Rtntn_Ind;
    private DbsField ncw_Efm_Document_Dcmt_Frmt;
    private DbsField ncw_Efm_Document_Dcmnt_Anntn_Ind;
    private DbsField ncw_Efm_Document_Dcmnt_Copy_Ind;
    private DbsField ncw_Efm_Document_Invrt_Crte_Dte_Tme;

    private DbsGroup ncw_Efm_Document_Audit_Data;
    private DbsField ncw_Efm_Document_Audit_Action;
    private DbsField ncw_Efm_Document_Audit_Empl_Racf_Id;
    private DbsField ncw_Efm_Document_Audit_System;
    private DbsField ncw_Efm_Document_Audit_Unit;
    private DbsField ncw_Efm_Document_Audit_Jobname;
    private DbsField ncw_Efm_Document_Audit_Dte_Tme;
    private DbsField ncw_Efm_Document_Text_Dte_Tme;
    private DbsField ncw_Efm_Document_Image_Source_Id;
    private DbsField ncw_Efm_Document_Image_Min;
    private DbsField ncw_Efm_Document_Image_End_Page;
    private DbsField ncw_Efm_Document_Image_Ornttn;
    private DbsField ncw_Efm_Document_Kdo_Crte_Dte_Tme;
    private DbsField ncw_Efm_Document_Orgnl_Np_Pin;
    private DbsField ncw_Efm_Document_Prvte_Dcmnt_Ind;

    private DataAccessProgramView vw_cwf_Arch_Document;
    private DbsField cwf_Arch_Document_Image_Source_Id;
    private DbsField cwf_Arch_Document_Image_Min;
    private DbsField cwf_Arch_Document_Document_Retention_Ind;

    private DataAccessProgramView vw_icw_Efm_Document;
    private DbsField icw_Efm_Document_Cabinet_Id;

    private DbsGroup icw_Efm_Document__R_Field_17;
    private DbsField icw_Efm_Document_Iis_Hrchy_Level_Cde;
    private DbsField icw_Efm_Document_Ppg_Cde;
    private DbsField icw_Efm_Document_Rqst_Log_Dte_Tme;
    private DbsField icw_Efm_Document_Crte_Dte_Tme;
    private DbsField icw_Efm_Document_Crte_Sqnce_Nbr;
    private DbsField icw_Efm_Document_Crte_Empl_Racf_Id;
    private DbsField icw_Efm_Document_Crte_Unit;
    private DbsField icw_Efm_Document_Crte_System;
    private DbsField icw_Efm_Document_Crte_Jobname;
    private DbsField icw_Efm_Document_Dcmnt_Rtntn;
    private DbsField icw_Efm_Document_Image_Source_Id;
    private DbsField icw_Efm_Document_Image_Min;
    private DbsField icw_Efm_Document_Image_End_Page;
    private DbsField icw_Efm_Document_Image_Ornttn;
    private DbsField icw_Efm_Document_Orgnl_Cabinet;

    private DataAccessProgramView vw_cwf_Mit_History;
    private DbsField cwf_Mit_History_Admin_Unit_Cde;

    private DataAccessProgramView vw_icw_Mit_History;
    private DbsField icw_Mit_History_Admin_Unit_Cde;

    private DataAccessProgramView vw_ncw_Mit_History;
    private DbsField ncw_Mit_History_Admin_Unit_Cde;

    private DataAccessProgramView vw_pst_Outgoing_Mail_Item;
    private DbsField pst_Outgoing_Mail_Item_Image_Srce_Id_Id;
    private DbsField pst_Outgoing_Mail_Item_Pckge_Cde;

    private DataAccessProgramView vw_cwf_Image_Xref_H1;
    private DbsField cwf_Image_Xref_H1_Source_Id_Min_Key;

    private DbsGroup cwf_Image_Xref_H1__R_Field_18;
    private DbsField cwf_Image_Xref_H1_Xref_Source;
    private DbsField cwf_Image_Xref_H1_Xref_Min;

    private DataAccessProgramView vw_cwf_Efm_Document_H1;
    private DbsField cwf_Efm_Document_H1_Image_Pointer_Key;

    private DbsGroup cwf_Efm_Document_H1__R_Field_19;
    private DbsField cwf_Efm_Document_H1_Doc_Source;
    private DbsField cwf_Efm_Document_H1_Doc_Min;

    private DataAccessProgramView vw_icw_Efm_Document_H1;
    private DbsField icw_Efm_Document_H1_Image_Pointer_Key;

    private DbsGroup icw_Efm_Document_H1__R_Field_20;
    private DbsField icw_Efm_Document_H1_Doc_Source;
    private DbsField icw_Efm_Document_H1_Doc_Min;

    private DataAccessProgramView vw_ncw_Efm_Document_H1;
    private DbsField ncw_Efm_Document_H1_Image_Pointer_Key;

    private DbsGroup ncw_Efm_Document_H1__R_Field_21;
    private DbsField ncw_Efm_Document_H1_Doc_Source;
    private DbsField ncw_Efm_Document_H1_Doc_Min;
    private DbsField pnd_Work_Record;

    private DbsGroup pnd_Work_Record__R_Field_22;
    private DbsField pnd_Work_Record_Pnd_Source_Id;
    private DbsField pnd_Work_Record_Pnd_Mail_Item_No;
    private DbsField pnd_Work_Record_Pnd_Record_Type;
    private DbsField pnd_Curr_Unit;
    private DbsField pnd_Init_Unit;
    private DbsField pnd_Create_Unit;
    private DbsField pnd_Ppg_Code2_N;
    private DbsField pnd_Start_Min;

    private DbsGroup pnd_Start_Min__R_Field_23;
    private DbsField pnd_Start_Min_Pnd_Start_Srce_Id;
    private DbsField pnd_Start_Min_Pnd_Start_Min_Nbr;
    private DbsField pnd_End_Min;
    private DbsField pnd_Doc_Exist;
    private DbsField pnd_Input_Err;
    private DbsField pnd_Cwf_Misc_Fldr_Exist;
    private DbsField pnd_Ncw_Misc_Fldr_Exist;
    private DbsField pnd_Icwp1_Misc_Fldr_Exist;
    private DbsField pnd_Icwi_Misc_Fldr_Exist;
    private DbsField pnd_Upld_Entry_Dte_Full;

    private DbsGroup pnd_Upld_Entry_Dte_Full__R_Field_24;
    private DbsField pnd_Upld_Entry_Dte_Full_Pnd_Upld_Entry_Dte;
    private DbsField pnd_Upld_Entry_Dte_Full_Pnd_Upld_Entry_Tme;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_25;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Inst_Code_N;
    private DbsField pnd_Dcmt_Type;

    private DbsGroup pnd_Dcmt_Type__R_Field_26;
    private DbsField pnd_Dcmt_Type_Pnd_Dcmt_Ctgry;
    private DbsField pnd_Dcmt_Type_Pnd_Dcmt_Sub_Ctgry;
    private DbsField pnd_Dcmt_Type_Pnd_Dcmt_Dtl;
    private DbsField pnd_Iis_Hrchy_Level_Cde;
    private DbsField pnd_Rqst_Log_Dte_Tme_Icw;
    private DbsField pnd_Iis_Instn_Link_Cde_Icw;
    private DbsField pnd_Rqst_Log_Dte_Tme_Cwf;
    private DbsField pnd_Rqst_Log_Dte_Tme_Ncw;
    private DbsField pnd_Multi_Rqst_Ind;
    private DbsField pnd_Pin_Sw;
    private DbsField pnd_Inst_Sw;
    private DbsField pnd_Ppg1_Sw;
    private DbsField pnd_Ppg2_Sw;
    private DbsField pnd_Npin_Sw;
    private DbsField pnd_Suppress_Sw;
    private DbsField pnd_Cab_5_12;
    private DbsField pnd_Cab_7_7;
    private DbsField pnd_Cab_5_6;
    private DbsField pnd_Length;
    private DbsField pnd_Curr_Dte_Tme;

    private DbsGroup pnd_Curr_Dte_Tme__R_Field_27;
    private DbsField pnd_Curr_Dte_Tme_Pnd_Curr_Dte;
    private DbsField pnd_Curr_Dte_Tme_Pnd_Curr_Tme;
    private DbsField pnd_Xref_With_Doc;
    private DbsField pnd_Xref_No_Doc;
    private DbsField pnd_Pin_Not_Zero;
    private DbsField pnd_Cab_Not_Blank;
    private DbsField pnd_Pin_With_Z;
    private DbsField pnd_Pin_With_No_Z;
    private DbsField pnd_Inst_In_Cab;
    private DbsField pnd_Inst_With_Z;
    private DbsField pnd_Inst_With_No_Z;
    private DbsField pnd_Ppg1_In_Cab;
    private DbsField pnd_Ppg1_With_Z;
    private DbsField pnd_Ppg1_With_No_Z;
    private DbsField pnd_Ppg2_In_Cab;
    private DbsField pnd_Ppg2_With_Z;
    private DbsField pnd_Ppg2_With_No_Z;
    private DbsField pnd_Npin_In_Cab;
    private DbsField pnd_Npin_With_Z;
    private DbsField pnd_Npin_With_No_Z;
    private DbsField pnd_Cab_Id_Nocond;
    private DbsField pnd_Cab_Id_Cirs;
    private DbsField pnd_Suppress_Cnt;
    private DbsField pnd_Cab_N_Pin_Blank;
    private DbsField pnd_Total_Passed_Cut_Off;
    private DbsField pnd_Total_Msg_Doctype;
    private DbsField pnd_Total_Msg_Cwf;
    private DbsField pnd_Total_Msg_Icw;
    private DbsField pnd_Total_Msg_Ncw;
    private DbsField pnd_Total_Msg;
    private DbsField pnd_Total_Success;
    private DbsField pnd_Total_Success_Cnt;
    private DbsField pnd_Cwf_Success;
    private DbsField pnd_Icw_Success;
    private DbsField pnd_Ncw_Success;
    private DbsField pnd_Doctype_Success;
    private DbsField pnd_Image_Del_Cnt;
    private DbsField pnd_Upld_Dte_Today_Cnt;
    private DbsField pnd_Archve_Dte_Cnt;
    private DbsField pnd_Program;
    private DbsField pnd_Min_Save;
    private DbsField pnd_Rqst_Routing_Key;

    private DbsGroup pnd_Rqst_Routing_Key__R_Field_28;
    private DbsField pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Icw_Rqst_Routing_Key;

    private DbsGroup pnd_Icw_Rqst_Routing_Key__R_Field_29;
    private DbsField pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Rqst_Log_Dte_Tme;
    private DbsField pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Strt_Evnt_Dte_Tme;
    private DbsField pnd_Rqst_Id_Key;

    private DbsGroup pnd_Rqst_Id_Key__R_Field_30;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Id;

    private DbsGroup pnd_Rqst_Id_Key__R_Field_31;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Work_Prcss_Id;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Tiaa_Rcvd_Dte;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Case_Id;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Sub_Rqst_Cde;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Pin_Nbr;
    private DbsField pnd_Rqst_Id_Key_Pnd_Multi_Rqst_Id;
    private DbsField pnd_Rqst_Id_Key_Pnd_Actve_Ind;
    private DbsField pnd_Image_Source_Id;
    private DbsField pnd_Cabinet_Id;
    private DbsField pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Case_Workprcss_Multi_Subrqst;

    private DbsGroup pnd_Case_Workprcss_Multi_Subrqst__R_Field_32;
    private DbsField pnd_Case_Workprcss_Multi_Subrqst_Pnd_Case_Id;
    private DbsField pnd_Case_Workprcss_Multi_Subrqst_Pnd_Work_Prcss_Id;
    private DbsField pnd_Case_Workprcss_Multi_Subrqst_Pnd_Multi_Ind;
    private DbsField pnd_Case_Workprcss_Multi_Subrqst_Pnd_Subrqst_Ind;
    private DbsField pnd_Pin_Ppg;
    private DbsField pnd_Entry_Dte_Tme;
    private DbsField pnd_Identify_Dte_Tme;
    private DbsField pnd_Tiaa_Rcvd_Dte_Tme_Cwf;
    private DbsField pnd_Tiaa_Rcvd_Dte_Tme_Icw;
    private DbsField pnd_Tiaa_Rcvd_Dte_Tme_Ncw;
    private DbsField pnd_Action_Cde;
    private DbsField pnd_Wrk_Msg;

    private DbsGroup pnd_Wrk_Msg__R_Field_33;
    private DbsField pnd_Wrk_Msg_Pnd_Wrk_Msg_1_50;
    private DbsField pnd_Batch_Nbr;
    private DbsField pnd_Date_Today;
    private DbsField pnd_Arch_Dte;
    private DbsField pnd_Parm_Key_Value;

    private DbsGroup pnd_Parm_Key_Value__R_Field_34;
    private DbsField pnd_Parm_Key_Value_Pnd_Parm_Key_Source;
    private DbsField pnd_Parm_Key_Value_Pnd_Parm_Key_Min;

    private DbsGroup pnd_Parm_Key_Value__R_Field_35;
    private DbsField pnd_Parm_Key_Value_Pnd_Parm_Key_Min_N;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaIefa9000 = new PdaIefa9000(localVariables);
        pdaNefa9000 = new PdaNefa9000(localVariables);
        pdaPsta9657 = new PdaPsta9657(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaEfsa6110 = new PdaEfsa6110(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_ncw_Master_Index = new DataAccessProgramView(new NameInfo("vw_ncw_Master_Index", "NCW-MASTER-INDEX"), "NCW_MASTER_INDEX", "NCW_MASTER_INDEX");
        ncw_Master_Index_Np_Pin = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Master_Index_Rqst_Log_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        ncw_Master_Index_Tiaa_Rcvd_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        ncw_Master_Index_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        ncw_Master_Index_Elctrn_Fld_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Elctrn_Fld_Ind", "ELCTRN-FLD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ELCTRN_FLD_IND");
        ncw_Master_Index_Elctrn_Fld_Ind.setDdmHeader("ELECTRNC/FLDR IND");
        ncw_Master_Index_Work_Prcss_Id = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        ncw_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        ncw_Master_Index_Admin_Unit_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        ncw_Master_Index_Rqst_Id = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rqst_Id", "RQST-ID", FieldType.STRING, 23, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ncw_Master_Index_Rqst_Id.setDdmHeader("REQUEST ID");

        ncw_Master_Index__R_Field_1 = vw_ncw_Master_Index.getRecord().newGroupInGroup("ncw_Master_Index__R_Field_1", "REDEFINE", ncw_Master_Index_Rqst_Id);
        ncw_Master_Index_Rqst_Work_Prcss_Id = ncw_Master_Index__R_Field_1.newFieldInGroup("ncw_Master_Index_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        ncw_Master_Index_Rqst_Tiaa_Rcvd_Dte = ncw_Master_Index__R_Field_1.newFieldInGroup("ncw_Master_Index_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        ncw_Master_Index_Rqst_Case_Id_Cde = ncw_Master_Index__R_Field_1.newFieldInGroup("ncw_Master_Index_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", FieldType.STRING, 
            2);

        ncw_Master_Index__R_Field_2 = ncw_Master_Index__R_Field_1.newGroupInGroup("ncw_Master_Index__R_Field_2", "REDEFINE", ncw_Master_Index_Rqst_Case_Id_Cde);
        ncw_Master_Index_Pnd_Case_Id_Cde = ncw_Master_Index__R_Field_2.newFieldInGroup("ncw_Master_Index_Pnd_Case_Id_Cde", "#CASE-ID-CDE", FieldType.STRING, 
            1);
        ncw_Master_Index_Pnd_Subrqst_Id_Cde = ncw_Master_Index__R_Field_2.newFieldInGroup("ncw_Master_Index_Pnd_Subrqst_Id_Cde", "#SUBRQST-ID-CDE", FieldType.STRING, 
            1);
        ncw_Master_Index_Rqst_Np_Pin = ncw_Master_Index__R_Field_1.newFieldInGroup("ncw_Master_Index_Rqst_Np_Pin", "RQST-NP-PIN", FieldType.STRING, 7);
        registerRecord(vw_ncw_Master_Index);

        vw_icw_Master_Index = new DataAccessProgramView(new NameInfo("vw_icw_Master_Index", "ICW-MASTER-INDEX"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        icw_Master_Index_Rqst_Log_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        icw_Master_Index_Ppg_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PPG_CDE");
        icw_Master_Index_Actve_Ind = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        icw_Master_Index_Iis_Instn_Link_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Iis_Instn_Link_Cde", "IIS-INSTN-LINK-CDE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "IIS_INSTN_LINK_CDE");
        icw_Master_Index_Iis_Hrchy_Level_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Iis_Hrchy_Level_Cde", "IIS-HRCHY-LEVEL-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IIS_HRCHY_LEVEL_CDE");
        icw_Master_Index_Elctrn_Fld_Ind = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Elctrn_Fld_Ind", "ELCTRN-FLD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ELCTRN_FLD_IND");
        icw_Master_Index_Elctrn_Fld_Ind.setDdmHeader("ELECTRNC/FLDR IND");
        icw_Master_Index_Work_Prcss_Id = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        icw_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        icw_Master_Index_Tiaa_Rcvd_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        icw_Master_Index_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        icw_Master_Index_Admin_Unit_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        icw_Master_Index_Rqst_Id = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Id", "RQST-ID", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "RQST_ID");
        icw_Master_Index_Rqst_Id.setDdmHeader("REQUEST ID");

        icw_Master_Index__R_Field_3 = vw_icw_Master_Index.getRecord().newGroupInGroup("icw_Master_Index__R_Field_3", "REDEFINE", icw_Master_Index_Rqst_Id);
        icw_Master_Index_Rqst_Work_Prcss_Id = icw_Master_Index__R_Field_3.newFieldInGroup("icw_Master_Index_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        icw_Master_Index_Rqst_Tiaa_Rcvd_Dte = icw_Master_Index__R_Field_3.newFieldInGroup("icw_Master_Index_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        icw_Master_Index_Rqst_Case_Id_Cde = icw_Master_Index__R_Field_3.newFieldInGroup("icw_Master_Index_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", FieldType.STRING, 
            2);

        icw_Master_Index__R_Field_4 = icw_Master_Index__R_Field_3.newGroupInGroup("icw_Master_Index__R_Field_4", "REDEFINE", icw_Master_Index_Rqst_Case_Id_Cde);
        icw_Master_Index_Pnd_Case_Id_Cde = icw_Master_Index__R_Field_4.newFieldInGroup("icw_Master_Index_Pnd_Case_Id_Cde", "#CASE-ID-CDE", FieldType.STRING, 
            1);
        icw_Master_Index_Pnd_Subrqst_Id_Cde = icw_Master_Index__R_Field_4.newFieldInGroup("icw_Master_Index_Pnd_Subrqst_Id_Cde", "#SUBRQST-ID-CDE", FieldType.STRING, 
            1);
        registerRecord(vw_icw_Master_Index);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Pin_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Master_Index_View_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index_View__R_Field_5 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_5", "REDEFINE", cwf_Master_Index_View_Rqst_Log_Dte_Tme);
        cwf_Master_Index_View_Rqst_Log_Index_Dte = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Log_Index_Tme = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme", 
            "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_View_Rqst_Orgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Orgn_Cde", "RQST-ORGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_View_Rqst_Rgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Rgn_Cde", "RQST-RGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_RGN_CDE");
        cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde", 
            "RQST-SPCL-DSGNTN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_SPCL_DSGNTN_CDE");
        cwf_Master_Index_View_Rqst_Brnch_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Brnch_Cde", "RQST-BRNCH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_BRNCH_CDE");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_View_Sub_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Ind", "SUB-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SUB_RQST_IND");
        cwf_Master_Index_View_Sub_Rqst_Ind.setDdmHeader("SUB-REQUEST COUNT");
        cwf_Master_Index_View_Case_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CASE_ID_CDE");
        cwf_Master_Index_View_Case_Id_Cde.setDdmHeader("CASE/ID");

        cwf_Master_Index_View__R_Field_6 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_6", "REDEFINE", cwf_Master_Index_View_Case_Id_Cde);
        cwf_Master_Index_View_Case_Ind = cwf_Master_Index_View__R_Field_6.newFieldInGroup("cwf_Master_Index_View_Case_Ind", "CASE-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind = cwf_Master_Index_View__R_Field_6.newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind", "SUB-RQST-SQNCE-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Multi_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Multi_Rqst_Ind", "MULTI-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        cwf_Master_Index_View_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Master_Index_View_Orgnl_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_View_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");

        cwf_Master_Index_View__R_Field_7 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_7", "REDEFINE", cwf_Master_Index_View_Work_Prcss_Id);
        cwf_Master_Index_View_Work_Actn_Rqstd_Cde = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde", 
            "WORK-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde", 
            "WORK-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Cde", "UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_CDE");
        cwf_Master_Index_View_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Master_Index_View__R_Field_8 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_8", "REDEFINE", cwf_Master_Index_View_Unit_Cde);
        cwf_Master_Index_View_Unit_Id_Cde = cwf_Master_Index_View__R_Field_8.newFieldInGroup("cwf_Master_Index_View_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 
            5);
        cwf_Master_Index_View_Unit_Rgn_Cde = cwf_Master_Index_View__R_Field_8.newFieldInGroup("cwf_Master_Index_View_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde = cwf_Master_Index_View__R_Field_8.newFieldInGroup("cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Unit_Brnch_Group_Cde = cwf_Master_Index_View__R_Field_8.newFieldInGroup("cwf_Master_Index_View_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Unit_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Index_View_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Index_View_Work_Rqst_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Rqst_Prty_Cde", "WORK-RQST-PRTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WORK_RQST_PRTY_CDE");
        cwf_Master_Index_View_Work_Rqst_Prty_Cde.setDdmHeader("PRIO");
        cwf_Master_Index_View_Empl_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_View_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");

        cwf_Master_Index_View__R_Field_9 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_9", "REDEFINE", cwf_Master_Index_View_Empl_Oprtr_Cde);
        cwf_Master_Index_View_Empl_Racf_Id = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Empl_Sffx_Cde = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Empl_Sffx_Cde", "EMPL-SFFX-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_View_Admin_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_View_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_View_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Dte_Tme", 
            "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Index_View_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Oprtr_Cde", 
            "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Updte_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Dte", "LAST-UPDTE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        cwf_Master_Index_View_Last_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Index_View_Last_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Oprtr_Cde", 
            "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Cntct_Orgn_Type_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Orgn_Type_Cde", 
            "CNTCT-ORGN-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTCT_ORGN_TYPE_CDE");
        cwf_Master_Index_View_Cntct_Orgn_Type_Cde.setDdmHeader("CONTACT/CODE");
        cwf_Master_Index_View_Cntct_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Dte_Tme", "CNTCT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "CNTCT_DTE_TME");
        cwf_Master_Index_View_Cntct_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Invrt_Dte_Tme", 
            "CNTCT-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "CNTCT_INVRT_DTE_TME");
        cwf_Master_Index_View_Cntct_Oprtr_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Oprtr_Id", "CNTCT-OPRTR-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTCT_OPRTR_ID");
        cwf_Master_Index_View_Actve_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");
        cwf_Master_Index_View_Crprte_Status_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_View_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_View_Cnflct_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cnflct_Ind", "CNFLCT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNFLCT_IND");
        cwf_Master_Index_View_Cnflct_Ind.setDdmHeader("CONFLICT/REQUEST");
        cwf_Master_Index_View_Spcl_Hndlng_Txt = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", 
            FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        cwf_Master_Index_View_Spcl_Hndlng_Txt.setDdmHeader("MESSAGES");
        cwf_Master_Index_View_Instn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Instn_Cde", "INSTN-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "INSTN_CDE");
        cwf_Master_Index_View_Rqst_Instn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Instn_Cde", "RQST-INSTN-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "RQST_INSTN_CDE");
        cwf_Master_Index_View_Check_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Check_Ind", "CHECK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CHECK_IND");
        cwf_Master_Index_View_Check_Ind.setDdmHeader("CHK");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme", 
            "RQST-INVRT-RCVD-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_INVRT_RCVD_DTE_TME");
        cwf_Master_Index_View_Effctve_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_View_Trnsctn_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trnsctn_Dte", "TRNSCTN-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRNSCTN_DTE");
        cwf_Master_Index_View_Trnsctn_Dte.setDdmHeader("TRANSACTION/DATE");
        cwf_Master_Index_View_Trans_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trans_Dte", "TRANS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        cwf_Master_Index_View_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_View_Rescan_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RESCAN_IND");
        cwf_Master_Index_View_Rescan_Ind.setDdmHeader("RE-SCAN/INDICATOR");
        cwf_Master_Index_View_Dup_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Dup_Ind", "DUP-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DUP_IND");
        cwf_Master_Index_View_Dup_Ind.setDdmHeader("DUP/IND");
        cwf_Master_Index_View_Cmplnt_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CMPLNT_IND");
        cwf_Master_Index_View_Cntrct_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_View_Cntrct_Nbr = cwf_Master_Index_View_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Cntrct_Nbr", "CNTRCT-NBR", 
            FieldType.STRING, 8, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_View_Mail_Item_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_MAIL_ITEM_NBRMuGroup", 
            "MAIL_ITEM_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_MAIL_ITEM_NBR");
        cwf_Master_Index_View_Mail_Item_Nbr = cwf_Master_Index_View_Mail_Item_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Mail_Item_Nbr", "MAIL-ITEM-NBR", 
            FieldType.STRING, 11, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "MAIL_ITEM_NBR");
        cwf_Master_Index_View_Mail_Item_Nbr.setDdmHeader("MIN");
        cwf_Master_Index_View_Rqst_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Id", "RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Master_Index_View_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Master_Index_View__R_Field_10 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_10", "REDEFINE", cwf_Master_Index_View_Rqst_Id);
        cwf_Master_Index_View_Rqst_Work_Prcss_Id = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Case_Id_Cde = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Rqst_Pin_Nbr = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rqst_Pin_Nbr", "RQST-PIN-NBR", FieldType.NUMERIC, 
            12);
        cwf_Master_Index_View_Elctrnc_Fldr_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_Index_View_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        cwf_Master_Index_View_Log_Rqstr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Log_Rqstr_Cde", "LOG-RQSTR-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "LOG_RQSTR_CDE");
        cwf_Master_Index_View_Log_Rqstr_Cde.setDdmHeader("REQUESTOR/TYPE");
        cwf_Master_Index_View_Crprte_Due_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CRPRTE_DUE_DTE_TME");
        cwf_Master_Index_View_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Master_Index_View_Owner_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Owner_Unit_Cde", "OWNER-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "OWNER_UNIT_CDE");
        cwf_Master_Index_View_Owner_Unit_Cde.setDdmHeader("OWNERUNIT");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        cwf_Master_Index_View_Instn_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Instn_Rqst_Log_Dte_Tme", 
            "INSTN-RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "INSTN_RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Instn_Rqst_Log_Dte_Tme.setDdmHeader("GUARDIAN INST RQST LOG DTE/TME");
        cwf_Master_Index_View_Log_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Log_Sqnce_Nbr", "LOG-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "LOG_SQNCE_NBR");
        cwf_Master_Index_View_Log_Sqnce_Nbr.setDdmHeader("LOG SEQUENCE NUMBER");
        registerRecord(vw_cwf_Master_Index_View);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");
        registerRecord(vw_cwf_Support_Tbl);

        vw_cwf_Image_Xref = new DataAccessProgramView(new NameInfo("vw_cwf_Image_Xref", "CWF-IMAGE-XREF"), "CWF_IMAGE_XREF", "CWF_IMAGE_XREF");
        cwf_Image_Xref_Mail_Item_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Mail_Item_Nbr", "MAIL-ITEM-NBR", FieldType.NUMERIC, 
            11, RepeatingFieldStrategy.None, "MAIL_ITEM_NBR");
        cwf_Image_Xref_Mail_Item_Nbr.setDdmHeader("MAIL ITEM/NUMBER");
        cwf_Image_Xref_Image_Document_Address_Cde = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Document_Address_Cde", "IMAGE-DOCUMENT-ADDRESS-CDE", 
            FieldType.STRING, 22, RepeatingFieldStrategy.None, "IMAGE_DOCUMENT_ADDRESS_CDE");
        cwf_Image_Xref_Image_Document_Address_Cde.setDdmHeader("IMNET DOC/ADDRESS");
        cwf_Image_Xref_Ph_Unique_Id_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PH_UNIQUE_ID_NBR");
        cwf_Image_Xref_Ph_Unique_Id_Nbr.setDdmHeader("PIN");
        cwf_Image_Xref_Image_Quality_Ind = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Quality_Ind", "IMAGE-QUALITY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_QUALITY_IND");
        cwf_Image_Xref_Image_Quality_Ind.setDdmHeader("POOR/QLTY");
        cwf_Image_Xref_Upld_Date_Time = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Upld_Date_Time", "UPLD-DATE-TIME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPLD_DATE_TIME");
        cwf_Image_Xref_Upld_Date_Time.setDdmHeader("UPLOAD/DATE TIME");
        cwf_Image_Xref_Number_Of_Pages = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Number_Of_Pages", "NUMBER-OF-PAGES", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "NUMBER_OF_PAGES");
        cwf_Image_Xref_Number_Of_Pages.setDdmHeader("NBR OF/PAGES");
        cwf_Image_Xref_Source_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Source_Id", "SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SOURCE_ID");
        cwf_Image_Xref_Capture_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Capture_Id", "CAPTURE-ID", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CAPTURE_ID");
        cwf_Image_Xref_Batch_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Batch_Id", "BATCH-ID", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "BATCH_ID");
        cwf_Image_Xref_Image_Delete_Ind = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Delete_Ind", "IMAGE-DELETE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_DELETE_IND");
        cwf_Image_Xref_Audit_System = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Audit_System", "AUDIT-SYSTEM", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "AUDIT_SYSTEM");
        cwf_Image_Xref_Audit_Dte_Tme = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "AUDIT_DTE_TME");
        cwf_Image_Xref_Audit_Empl_Racf_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_EMPL_RACF_ID");
        cwf_Image_Xref_Cabinet_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Cabinet_Id", "CABINET-ID", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CABINET_ID");

        cwf_Image_Xref__R_Field_11 = vw_cwf_Image_Xref.getRecord().newGroupInGroup("cwf_Image_Xref__R_Field_11", "REDEFINE", cwf_Image_Xref_Cabinet_Id);
        cwf_Image_Xref_Pnd_Np_Pin = cwf_Image_Xref__R_Field_11.newFieldInGroup("cwf_Image_Xref_Pnd_Np_Pin", "#NP-PIN", FieldType.STRING, 7);

        cwf_Image_Xref__R_Field_12 = vw_cwf_Image_Xref.getRecord().newGroupInGroup("cwf_Image_Xref__R_Field_12", "REDEFINE", cwf_Image_Xref_Cabinet_Id);
        cwf_Image_Xref_Pnd_Ppg_Code1 = cwf_Image_Xref__R_Field_12.newFieldInGroup("cwf_Image_Xref_Pnd_Ppg_Code1", "#PPG-CODE1", FieldType.STRING, 4);

        cwf_Image_Xref__R_Field_13 = vw_cwf_Image_Xref.getRecord().newGroupInGroup("cwf_Image_Xref__R_Field_13", "REDEFINE", cwf_Image_Xref_Cabinet_Id);
        cwf_Image_Xref_Pnd_Inst_Code = cwf_Image_Xref__R_Field_13.newFieldInGroup("cwf_Image_Xref_Pnd_Inst_Code", "#INST-CODE", FieldType.STRING, 6);
        cwf_Image_Xref_Pnd_Ppg_Code2 = cwf_Image_Xref__R_Field_13.newFieldInGroup("cwf_Image_Xref_Pnd_Ppg_Code2", "#PPG-CODE2", FieldType.STRING, 6);

        cwf_Image_Xref__R_Field_14 = cwf_Image_Xref__R_Field_13.newGroupInGroup("cwf_Image_Xref__R_Field_14", "REDEFINE", cwf_Image_Xref_Pnd_Ppg_Code2);
        cwf_Image_Xref_Pnd_Ppg_Cd4 = cwf_Image_Xref__R_Field_14.newFieldInGroup("cwf_Image_Xref_Pnd_Ppg_Cd4", "#PPG-CD4", FieldType.STRING, 4);
        cwf_Image_Xref_Archive_Dte_Tme = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Archive_Dte_Tme", "ARCHIVE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ARCHIVE_DTE_TME");
        cwf_Image_Xref_Identify_Dte_Tme = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Identify_Dte_Tme", "IDENTIFY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "IDENTIFY_DTE_TME");
        cwf_Image_Xref_Source_Id_Min_Key = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Source_Id_Min_Key", "SOURCE-ID-MIN-KEY", FieldType.STRING, 
            17, RepeatingFieldStrategy.None, "SOURCE_ID_MIN_KEY");
        cwf_Image_Xref_Source_Id_Min_Key.setDdmHeader("SOURCE-ID/MIN");
        cwf_Image_Xref_Source_Id_Min_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Image_Xref);

        vw_cwf_Efm_Document = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Document", "CWF-EFM-DOCUMENT"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Efm_Document_Cabinet_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Document_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Document__R_Field_15 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_15", "REDEFINE", cwf_Efm_Document_Cabinet_Id);
        cwf_Efm_Document_Pin_Prefix = cwf_Efm_Document__R_Field_15.newFieldInGroup("cwf_Efm_Document_Pin_Prefix", "PIN-PREFIX", FieldType.STRING, 1);
        cwf_Efm_Document_Pin_Nbr = cwf_Efm_Document__R_Field_15.newFieldInGroup("cwf_Efm_Document_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);

        cwf_Efm_Document_Folder_Id = vw_cwf_Efm_Document.getRecord().newGroupInGroup("CWF_EFM_DOCUMENT_FOLDER_ID", "FOLDER-ID");
        cwf_Efm_Document_Folder_Id.setDdmHeader("FOLDER/ID");
        cwf_Efm_Document_Tiaa_Rcvd_Dte = cwf_Efm_Document_Folder_Id.newFieldInGroup("cwf_Efm_Document_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Efm_Document_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Efm_Document_Case_Workprcss_Multi_Subrqst = cwf_Efm_Document_Folder_Id.newFieldInGroup("cwf_Efm_Document_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Efm_Document_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Efm_Document__R_Field_16 = cwf_Efm_Document_Folder_Id.newGroupInGroup("cwf_Efm_Document__R_Field_16", "REDEFINE", cwf_Efm_Document_Case_Workprcss_Multi_Subrqst);
        cwf_Efm_Document_Case_Ind = cwf_Efm_Document__R_Field_16.newFieldInGroup("cwf_Efm_Document_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        cwf_Efm_Document_Originating_Work_Prcss_Id = cwf_Efm_Document__R_Field_16.newFieldInGroup("cwf_Efm_Document_Originating_Work_Prcss_Id", "ORIGINATING-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Efm_Document_Multi_Rqst_Ind = cwf_Efm_Document__R_Field_16.newFieldInGroup("cwf_Efm_Document_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 
            1);
        cwf_Efm_Document_Sub_Rqst_Ind = cwf_Efm_Document__R_Field_16.newFieldInGroup("cwf_Efm_Document_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 
            1);
        cwf_Efm_Document_Entry_Dte_Tme = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Efm_Document_Entry_Dte_Tme.setDdmHeader("ENTRY/DATE AND TIME");
        cwf_Efm_Document_Image_Min = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        cwf_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");
        cwf_Efm_Document_Document_Retention_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Document_Retention_Ind", "DOCUMENT-RETENTION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_RETENTION_IND");
        cwf_Efm_Document_Document_Retention_Ind.setDdmHeader("DOC/RETNTN");
        cwf_Efm_Document_Image_Source_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        cwf_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        cwf_Efm_Document_Entry_System_Or_Unit = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_System_Or_Unit", "ENTRY-SYSTEM-OR-UNIT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_SYSTEM_OR_UNIT");
        cwf_Efm_Document_Entry_System_Or_Unit.setDdmHeader("CREATED BY/SYSTEM OR UNIT");
        registerRecord(vw_cwf_Efm_Document);

        vw_ncw_Efm_Document = new DataAccessProgramView(new NameInfo("vw_ncw_Efm_Document", "NCW-EFM-DOCUMENT"), "NCW_EFM_DOCUMENT", "NCW_EFM_DOCUMENT");
        ncw_Efm_Document_Np_Pin = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Efm_Document_Rqst_Log_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        ncw_Efm_Document_Rqst_Log_Dte_Tme.setDdmHeader("MIT RQST/DATE AND TIME");
        ncw_Efm_Document_Crte_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Dte_Tme", "CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRTE_DTE_TME");
        ncw_Efm_Document_Crte_Dte_Tme.setDdmHeader("CREATE/DATE AND TIME");
        ncw_Efm_Document_Crte_Sqnce_Nbr = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Sqnce_Nbr", "CRTE-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CRTE_SQNCE_NBR");
        ncw_Efm_Document_Crte_Empl_Racf_Id = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Empl_Racf_Id", "CRTE-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CRTE_EMPL_RACF_ID");
        ncw_Efm_Document_Crte_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        ncw_Efm_Document_Crte_Unit = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Unit", "CRTE-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CRTE_UNIT");
        ncw_Efm_Document_Crte_Unit.setDdmHeader("CREATED BY/UNIT");
        ncw_Efm_Document_Crte_System = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_System", "CRTE-SYSTEM", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_SYSTEM");
        ncw_Efm_Document_Crte_System.setDdmHeader("CREATED BY/SYSTEM");
        ncw_Efm_Document_Crte_Jobname = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Jobname", "CRTE-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_JOBNAME");
        ncw_Efm_Document_Crte_Jobname.setDdmHeader("CREATED BY/JOBNAME");

        ncw_Efm_Document_Dcmnt_Type = vw_ncw_Efm_Document.getRecord().newGroupInGroup("NCW_EFM_DOCUMENT_DCMNT_TYPE", "DCMNT-TYPE");
        ncw_Efm_Document_Dcmnt_Type.setDdmHeader("DOCUMENT/TYPE");
        ncw_Efm_Document_Dcmnt_Ctgry = ncw_Efm_Document_Dcmnt_Type.newFieldInGroup("ncw_Efm_Document_Dcmnt_Ctgry", "DCMNT-CTGRY", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "DCMNT_CTGRY");
        ncw_Efm_Document_Dcmnt_Ctgry.setDdmHeader("DOC/CAT");
        ncw_Efm_Document_Dcmnt_Sub_Ctgry = ncw_Efm_Document_Dcmnt_Type.newFieldInGroup("ncw_Efm_Document_Dcmnt_Sub_Ctgry", "DCMNT-SUB-CTGRY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DCMNT_SUB_CTGRY");
        ncw_Efm_Document_Dcmnt_Sub_Ctgry.setDdmHeader("DOC SUB-/CATEGORY");
        ncw_Efm_Document_Dcmnt_Dtl = ncw_Efm_Document_Dcmnt_Type.newFieldInGroup("ncw_Efm_Document_Dcmnt_Dtl", "DCMNT-DTL", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DCMNT_DTL");
        ncw_Efm_Document_Dcmnt_Dtl.setDdmHeader("DOCUMENT/DETAIL");
        ncw_Efm_Document_Dcmnt_Dscrptn = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Dscrptn", "DCMNT-DSCRPTN", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "DCMNT_DSCRPTN");
        ncw_Efm_Document_Dcmnt_Drctn = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Drctn", "DCMNT-DRCTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_DRCTN");
        ncw_Efm_Document_Dcmnt_Drctn.setDdmHeader("DOC/DIR");
        ncw_Efm_Document_Dcmnt_Scrty_Ind = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Scrty_Ind", "DCMNT-SCRTY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_SCRTY_IND");
        ncw_Efm_Document_Dcmnt_Scrty_Ind.setDdmHeader("SECRD/DOC");
        ncw_Efm_Document_Dcmnt_Rtntn_Ind = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Rtntn_Ind", "DCMNT-RTNTN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_RTNTN_IND");
        ncw_Efm_Document_Dcmnt_Rtntn_Ind.setDdmHeader("DOC/RETNTN");
        ncw_Efm_Document_Dcmt_Frmt = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmt_Frmt", "DCMT-FRMT", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DCMT_FRMT");
        ncw_Efm_Document_Dcmnt_Anntn_Ind = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Anntn_Ind", "DCMNT-ANNTN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_ANNTN_IND");
        ncw_Efm_Document_Dcmnt_Anntn_Ind.setDdmHeader("ANOTATION/IND");
        ncw_Efm_Document_Dcmnt_Copy_Ind = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Copy_Ind", "DCMNT-COPY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_COPY_IND");
        ncw_Efm_Document_Dcmnt_Copy_Ind.setDdmHeader("COPY/IND");
        ncw_Efm_Document_Invrt_Crte_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Invrt_Crte_Dte_Tme", "INVRT-CRTE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "INVRT_CRTE_DTE_TME");
        ncw_Efm_Document_Invrt_Crte_Dte_Tme.setDdmHeader("INVERTED CREATE/DATE AND TIME");

        ncw_Efm_Document_Audit_Data = vw_ncw_Efm_Document.getRecord().newGroupInGroup("NCW_EFM_DOCUMENT_AUDIT_DATA", "AUDIT-DATA");
        ncw_Efm_Document_Audit_Action = ncw_Efm_Document_Audit_Data.newFieldInGroup("ncw_Efm_Document_Audit_Action", "AUDIT-ACTION", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AUDIT_ACTION");
        ncw_Efm_Document_Audit_Action.setDdmHeader("ACTION");
        ncw_Efm_Document_Audit_Empl_Racf_Id = ncw_Efm_Document_Audit_Data.newFieldInGroup("ncw_Efm_Document_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AUDIT_EMPL_RACF_ID");
        ncw_Efm_Document_Audit_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF ID");
        ncw_Efm_Document_Audit_System = ncw_Efm_Document_Audit_Data.newFieldInGroup("ncw_Efm_Document_Audit_System", "AUDIT-SYSTEM", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_SYSTEM");
        ncw_Efm_Document_Audit_System.setDdmHeader("AUDIT/SYSTEM");
        ncw_Efm_Document_Audit_Unit = ncw_Efm_Document_Audit_Data.newFieldInGroup("ncw_Efm_Document_Audit_Unit", "AUDIT-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AUDIT_UNIT");
        ncw_Efm_Document_Audit_Unit.setDdmHeader("AUDIT/UNIT");
        ncw_Efm_Document_Audit_Jobname = ncw_Efm_Document_Audit_Data.newFieldInGroup("ncw_Efm_Document_Audit_Jobname", "AUDIT-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_JOBNAME");
        ncw_Efm_Document_Audit_Jobname.setDdmHeader("JOBNAME");
        ncw_Efm_Document_Audit_Dte_Tme = ncw_Efm_Document_Audit_Data.newFieldInGroup("ncw_Efm_Document_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "AUDIT_DTE_TME");
        ncw_Efm_Document_Audit_Dte_Tme.setDdmHeader("DATE/& TIME");
        ncw_Efm_Document_Text_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Text_Dte_Tme", "TEXT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TEXT_DTE_TME");
        ncw_Efm_Document_Text_Dte_Tme.setDdmHeader("TEXT/DATE AND TIME");
        ncw_Efm_Document_Image_Source_Id = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        ncw_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        ncw_Efm_Document_Image_Min = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        ncw_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");
        ncw_Efm_Document_Image_End_Page = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_End_Page", "IMAGE-END-PAGE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "IMAGE_END_PAGE");
        ncw_Efm_Document_Image_End_Page.setDdmHeader("IMAGE/END PAGE");
        ncw_Efm_Document_Image_Ornttn = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_Ornttn", "IMAGE-ORNTTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_ORNTTN");
        ncw_Efm_Document_Image_Ornttn.setDdmHeader("DOC/ORIENT");
        ncw_Efm_Document_Kdo_Crte_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Kdo_Crte_Dte_Tme", "KDO-CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "KDO_CRTE_DTE_TME");
        ncw_Efm_Document_Kdo_Crte_Dte_Tme.setDdmHeader("K D O/DATE AND TIME");
        ncw_Efm_Document_Orgnl_Np_Pin = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Orgnl_Np_Pin", "ORGNL-NP-PIN", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "ORGNL_NP_PIN");
        ncw_Efm_Document_Prvte_Dcmnt_Ind = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Prvte_Dcmnt_Ind", "PRVTE-DCMNT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRVTE_DCMNT_IND");
        registerRecord(vw_ncw_Efm_Document);

        vw_cwf_Arch_Document = new DataAccessProgramView(new NameInfo("vw_cwf_Arch_Document", "CWF-ARCH-DOCUMENT"), "CWF_ARCH_DOCUMENT", "EFM_ARCH_DOCMNT");
        cwf_Arch_Document_Image_Source_Id = vw_cwf_Arch_Document.getRecord().newFieldInGroup("cwf_Arch_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        cwf_Arch_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        cwf_Arch_Document_Image_Min = vw_cwf_Arch_Document.getRecord().newFieldInGroup("cwf_Arch_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        cwf_Arch_Document_Image_Min.setDdmHeader("IMAGE/POINTER");
        cwf_Arch_Document_Document_Retention_Ind = vw_cwf_Arch_Document.getRecord().newFieldInGroup("cwf_Arch_Document_Document_Retention_Ind", "DOCUMENT-RETENTION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_RETENTION_IND");
        cwf_Arch_Document_Document_Retention_Ind.setDdmHeader("DOC/RETNTN");
        registerRecord(vw_cwf_Arch_Document);

        vw_icw_Efm_Document = new DataAccessProgramView(new NameInfo("vw_icw_Efm_Document", "ICW-EFM-DOCUMENT"), "ICW_EFM_DOCUMENT", "ICW_EFM_DOCUMENT");
        icw_Efm_Document_Cabinet_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Cabinet_Id", "CABINET-ID", FieldType.STRING, 12, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        icw_Efm_Document_Cabinet_Id.setDdmHeader("CABINET/ID");

        icw_Efm_Document__R_Field_17 = vw_icw_Efm_Document.getRecord().newGroupInGroup("icw_Efm_Document__R_Field_17", "REDEFINE", icw_Efm_Document_Cabinet_Id);
        icw_Efm_Document_Iis_Hrchy_Level_Cde = icw_Efm_Document__R_Field_17.newFieldInGroup("icw_Efm_Document_Iis_Hrchy_Level_Cde", "IIS-HRCHY-LEVEL-CDE", 
            FieldType.NUMERIC, 6);
        icw_Efm_Document_Ppg_Cde = icw_Efm_Document__R_Field_17.newFieldInGroup("icw_Efm_Document_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6);
        icw_Efm_Document_Rqst_Log_Dte_Tme = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        icw_Efm_Document_Rqst_Log_Dte_Tme.setDdmHeader("MIT RQST/DATE AND TIME");
        icw_Efm_Document_Crte_Dte_Tme = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Dte_Tme", "CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRTE_DTE_TME");
        icw_Efm_Document_Crte_Dte_Tme.setDdmHeader("CREATE/DATE AND TIME");
        icw_Efm_Document_Crte_Sqnce_Nbr = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Sqnce_Nbr", "CRTE-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CRTE_SQNCE_NBR");
        icw_Efm_Document_Crte_Empl_Racf_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Empl_Racf_Id", "CRTE-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CRTE_EMPL_RACF_ID");
        icw_Efm_Document_Crte_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        icw_Efm_Document_Crte_Unit = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Unit", "CRTE-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CRTE_UNIT");
        icw_Efm_Document_Crte_Unit.setDdmHeader("CREATED BY/UNIT");
        icw_Efm_Document_Crte_System = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_System", "CRTE-SYSTEM", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_SYSTEM");
        icw_Efm_Document_Crte_System.setDdmHeader("CREATED BY/SYSTEM");
        icw_Efm_Document_Crte_Jobname = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Jobname", "CRTE-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_JOBNAME");
        icw_Efm_Document_Crte_Jobname.setDdmHeader("CREATED BY/JOBNAME");
        icw_Efm_Document_Dcmnt_Rtntn = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Dcmnt_Rtntn", "DCMNT-RTNTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_RTNTN");
        icw_Efm_Document_Dcmnt_Rtntn.setDdmHeader("DOC/RETNTN");
        icw_Efm_Document_Image_Source_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        icw_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        icw_Efm_Document_Image_Min = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        icw_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");
        icw_Efm_Document_Image_End_Page = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_End_Page", "IMAGE-END-PAGE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "IMAGE_END_PAGE");
        icw_Efm_Document_Image_End_Page.setDdmHeader("IMAGE/END PAGE");
        icw_Efm_Document_Image_Ornttn = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Ornttn", "IMAGE-ORNTTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_ORNTTN");
        icw_Efm_Document_Image_Ornttn.setDdmHeader("DOC/ORIENT");
        icw_Efm_Document_Orgnl_Cabinet = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Orgnl_Cabinet", "ORGNL-CABINET", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "ORGNL_CABINET");
        registerRecord(vw_icw_Efm_Document);

        vw_cwf_Mit_History = new DataAccessProgramView(new NameInfo("vw_cwf_Mit_History", "CWF-MIT-HISTORY"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Mit_History_Admin_Unit_Cde = vw_cwf_Mit_History.getRecord().newFieldInGroup("cwf_Mit_History_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        registerRecord(vw_cwf_Mit_History);

        vw_icw_Mit_History = new DataAccessProgramView(new NameInfo("vw_icw_Mit_History", "ICW-MIT-HISTORY"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        icw_Mit_History_Admin_Unit_Cde = vw_icw_Mit_History.getRecord().newFieldInGroup("icw_Mit_History_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        registerRecord(vw_icw_Mit_History);

        vw_ncw_Mit_History = new DataAccessProgramView(new NameInfo("vw_ncw_Mit_History", "NCW-MIT-HISTORY"), "NCW_MASTER_INDEX", "NCW_MASTER_INDEX");
        ncw_Mit_History_Admin_Unit_Cde = vw_ncw_Mit_History.getRecord().newFieldInGroup("ncw_Mit_History_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        registerRecord(vw_ncw_Mit_History);

        vw_pst_Outgoing_Mail_Item = new DataAccessProgramView(new NameInfo("vw_pst_Outgoing_Mail_Item", "PST-OUTGOING-MAIL-ITEM"), "PST_OUTGOING_MAIL_ITEM", 
            "PST_REQUEST_DATA");
        pst_Outgoing_Mail_Item_Image_Srce_Id_Id = vw_pst_Outgoing_Mail_Item.getRecord().newFieldInGroup("pst_Outgoing_Mail_Item_Image_Srce_Id_Id", "IMAGE-SRCE-ID-ID", 
            FieldType.STRING, 17, RepeatingFieldStrategy.None, "IMAGE_SRCE_ID_ID");
        pst_Outgoing_Mail_Item_Image_Srce_Id_Id.setSuperDescriptor(true);
        pst_Outgoing_Mail_Item_Pckge_Cde = vw_pst_Outgoing_Mail_Item.getRecord().newFieldInGroup("pst_Outgoing_Mail_Item_Pckge_Cde", "PCKGE-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "PCKGE_CDE");
        pst_Outgoing_Mail_Item_Pckge_Cde.setDdmHeader("PACKAGE/ID");
        registerRecord(vw_pst_Outgoing_Mail_Item);

        vw_cwf_Image_Xref_H1 = new DataAccessProgramView(new NameInfo("vw_cwf_Image_Xref_H1", "CWF-IMAGE-XREF-H1"), "CWF_IMAGE_XREF", "CWF_IMAGE_XREF");
        cwf_Image_Xref_H1_Source_Id_Min_Key = vw_cwf_Image_Xref_H1.getRecord().newFieldInGroup("cwf_Image_Xref_H1_Source_Id_Min_Key", "SOURCE-ID-MIN-KEY", 
            FieldType.STRING, 17, RepeatingFieldStrategy.None, "SOURCE_ID_MIN_KEY");
        cwf_Image_Xref_H1_Source_Id_Min_Key.setDdmHeader("SOURCE-ID/MIN");
        cwf_Image_Xref_H1_Source_Id_Min_Key.setSuperDescriptor(true);

        cwf_Image_Xref_H1__R_Field_18 = vw_cwf_Image_Xref_H1.getRecord().newGroupInGroup("cwf_Image_Xref_H1__R_Field_18", "REDEFINE", cwf_Image_Xref_H1_Source_Id_Min_Key);
        cwf_Image_Xref_H1_Xref_Source = cwf_Image_Xref_H1__R_Field_18.newFieldInGroup("cwf_Image_Xref_H1_Xref_Source", "XREF-SOURCE", FieldType.STRING, 
            6);
        cwf_Image_Xref_H1_Xref_Min = cwf_Image_Xref_H1__R_Field_18.newFieldInGroup("cwf_Image_Xref_H1_Xref_Min", "XREF-MIN", FieldType.NUMERIC, 11);
        registerRecord(vw_cwf_Image_Xref_H1);

        vw_cwf_Efm_Document_H1 = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Document_H1", "CWF-EFM-DOCUMENT-H1"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Efm_Document_H1_Image_Pointer_Key = vw_cwf_Efm_Document_H1.getRecord().newFieldInGroup("cwf_Efm_Document_H1_Image_Pointer_Key", "IMAGE-POINTER-KEY", 
            FieldType.STRING, 17, RepeatingFieldStrategy.None, "IMAGE_POINTER_KEY");
        cwf_Efm_Document_H1_Image_Pointer_Key.setDdmHeader("IMAGE/POINTER KEY");
        cwf_Efm_Document_H1_Image_Pointer_Key.setSuperDescriptor(true);

        cwf_Efm_Document_H1__R_Field_19 = vw_cwf_Efm_Document_H1.getRecord().newGroupInGroup("cwf_Efm_Document_H1__R_Field_19", "REDEFINE", cwf_Efm_Document_H1_Image_Pointer_Key);
        cwf_Efm_Document_H1_Doc_Source = cwf_Efm_Document_H1__R_Field_19.newFieldInGroup("cwf_Efm_Document_H1_Doc_Source", "DOC-SOURCE", FieldType.STRING, 
            6);
        cwf_Efm_Document_H1_Doc_Min = cwf_Efm_Document_H1__R_Field_19.newFieldInGroup("cwf_Efm_Document_H1_Doc_Min", "DOC-MIN", FieldType.NUMERIC, 11);
        registerRecord(vw_cwf_Efm_Document_H1);

        vw_icw_Efm_Document_H1 = new DataAccessProgramView(new NameInfo("vw_icw_Efm_Document_H1", "ICW-EFM-DOCUMENT-H1"), "ICW_EFM_DOCUMENT", "ICW_EFM_DOCUMENT");
        icw_Efm_Document_H1_Image_Pointer_Key = vw_icw_Efm_Document_H1.getRecord().newFieldInGroup("icw_Efm_Document_H1_Image_Pointer_Key", "IMAGE-POINTER-KEY", 
            FieldType.STRING, 17, RepeatingFieldStrategy.None, "IMAGE_POINTER_KEY");
        icw_Efm_Document_H1_Image_Pointer_Key.setDdmHeader("IMAGE/POINTER KEY");
        icw_Efm_Document_H1_Image_Pointer_Key.setSuperDescriptor(true);

        icw_Efm_Document_H1__R_Field_20 = vw_icw_Efm_Document_H1.getRecord().newGroupInGroup("icw_Efm_Document_H1__R_Field_20", "REDEFINE", icw_Efm_Document_H1_Image_Pointer_Key);
        icw_Efm_Document_H1_Doc_Source = icw_Efm_Document_H1__R_Field_20.newFieldInGroup("icw_Efm_Document_H1_Doc_Source", "DOC-SOURCE", FieldType.STRING, 
            6);
        icw_Efm_Document_H1_Doc_Min = icw_Efm_Document_H1__R_Field_20.newFieldInGroup("icw_Efm_Document_H1_Doc_Min", "DOC-MIN", FieldType.NUMERIC, 11);
        registerRecord(vw_icw_Efm_Document_H1);

        vw_ncw_Efm_Document_H1 = new DataAccessProgramView(new NameInfo("vw_ncw_Efm_Document_H1", "NCW-EFM-DOCUMENT-H1"), "NCW_EFM_DOCUMENT", "NCW_EFM_DOCUMENT");
        ncw_Efm_Document_H1_Image_Pointer_Key = vw_ncw_Efm_Document_H1.getRecord().newFieldInGroup("ncw_Efm_Document_H1_Image_Pointer_Key", "IMAGE-POINTER-KEY", 
            FieldType.STRING, 17, RepeatingFieldStrategy.None, "IMAGE_POINTER_KEY");
        ncw_Efm_Document_H1_Image_Pointer_Key.setDdmHeader("IMAGE/POINTER KEY");
        ncw_Efm_Document_H1_Image_Pointer_Key.setSuperDescriptor(true);

        ncw_Efm_Document_H1__R_Field_21 = vw_ncw_Efm_Document_H1.getRecord().newGroupInGroup("ncw_Efm_Document_H1__R_Field_21", "REDEFINE", ncw_Efm_Document_H1_Image_Pointer_Key);
        ncw_Efm_Document_H1_Doc_Source = ncw_Efm_Document_H1__R_Field_21.newFieldInGroup("ncw_Efm_Document_H1_Doc_Source", "DOC-SOURCE", FieldType.STRING, 
            6);
        ncw_Efm_Document_H1_Doc_Min = ncw_Efm_Document_H1__R_Field_21.newFieldInGroup("ncw_Efm_Document_H1_Doc_Min", "DOC-MIN", FieldType.NUMERIC, 11);
        registerRecord(vw_ncw_Efm_Document_H1);

        pnd_Work_Record = localVariables.newFieldInRecord("pnd_Work_Record", "#WORK-RECORD", FieldType.STRING, 18);

        pnd_Work_Record__R_Field_22 = localVariables.newGroupInRecord("pnd_Work_Record__R_Field_22", "REDEFINE", pnd_Work_Record);
        pnd_Work_Record_Pnd_Source_Id = pnd_Work_Record__R_Field_22.newFieldInGroup("pnd_Work_Record_Pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 6);
        pnd_Work_Record_Pnd_Mail_Item_No = pnd_Work_Record__R_Field_22.newFieldInGroup("pnd_Work_Record_Pnd_Mail_Item_No", "#MAIL-ITEM-NO", FieldType.NUMERIC, 
            11);
        pnd_Work_Record_Pnd_Record_Type = pnd_Work_Record__R_Field_22.newFieldInGroup("pnd_Work_Record_Pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Curr_Unit = localVariables.newFieldInRecord("pnd_Curr_Unit", "#CURR-UNIT", FieldType.STRING, 8);
        pnd_Init_Unit = localVariables.newFieldInRecord("pnd_Init_Unit", "#INIT-UNIT", FieldType.STRING, 8);
        pnd_Create_Unit = localVariables.newFieldInRecord("pnd_Create_Unit", "#CREATE-UNIT", FieldType.STRING, 8);
        pnd_Ppg_Code2_N = localVariables.newFieldInRecord("pnd_Ppg_Code2_N", "#PPG-CODE2-N", FieldType.NUMERIC, 6);
        pnd_Start_Min = localVariables.newFieldInRecord("pnd_Start_Min", "#START-MIN", FieldType.STRING, 17);

        pnd_Start_Min__R_Field_23 = localVariables.newGroupInRecord("pnd_Start_Min__R_Field_23", "REDEFINE", pnd_Start_Min);
        pnd_Start_Min_Pnd_Start_Srce_Id = pnd_Start_Min__R_Field_23.newFieldInGroup("pnd_Start_Min_Pnd_Start_Srce_Id", "#START-SRCE-ID", FieldType.STRING, 
            6);
        pnd_Start_Min_Pnd_Start_Min_Nbr = pnd_Start_Min__R_Field_23.newFieldInGroup("pnd_Start_Min_Pnd_Start_Min_Nbr", "#START-MIN-NBR", FieldType.STRING, 
            11);
        pnd_End_Min = localVariables.newFieldInRecord("pnd_End_Min", "#END-MIN", FieldType.STRING, 17);
        pnd_Doc_Exist = localVariables.newFieldInRecord("pnd_Doc_Exist", "#DOC-EXIST", FieldType.BOOLEAN, 1);
        pnd_Input_Err = localVariables.newFieldInRecord("pnd_Input_Err", "#INPUT-ERR", FieldType.BOOLEAN, 1);
        pnd_Cwf_Misc_Fldr_Exist = localVariables.newFieldInRecord("pnd_Cwf_Misc_Fldr_Exist", "#CWF-MISC-FLDR-EXIST", FieldType.BOOLEAN, 1);
        pnd_Ncw_Misc_Fldr_Exist = localVariables.newFieldInRecord("pnd_Ncw_Misc_Fldr_Exist", "#NCW-MISC-FLDR-EXIST", FieldType.BOOLEAN, 1);
        pnd_Icwp1_Misc_Fldr_Exist = localVariables.newFieldInRecord("pnd_Icwp1_Misc_Fldr_Exist", "#ICWP1-MISC-FLDR-EXIST", FieldType.BOOLEAN, 1);
        pnd_Icwi_Misc_Fldr_Exist = localVariables.newFieldInRecord("pnd_Icwi_Misc_Fldr_Exist", "#ICWI-MISC-FLDR-EXIST", FieldType.BOOLEAN, 1);
        pnd_Upld_Entry_Dte_Full = localVariables.newFieldInRecord("pnd_Upld_Entry_Dte_Full", "#UPLD-ENTRY-DTE-FULL", FieldType.STRING, 15);

        pnd_Upld_Entry_Dte_Full__R_Field_24 = localVariables.newGroupInRecord("pnd_Upld_Entry_Dte_Full__R_Field_24", "REDEFINE", pnd_Upld_Entry_Dte_Full);
        pnd_Upld_Entry_Dte_Full_Pnd_Upld_Entry_Dte = pnd_Upld_Entry_Dte_Full__R_Field_24.newFieldInGroup("pnd_Upld_Entry_Dte_Full_Pnd_Upld_Entry_Dte", 
            "#UPLD-ENTRY-DTE", FieldType.STRING, 8);
        pnd_Upld_Entry_Dte_Full_Pnd_Upld_Entry_Tme = pnd_Upld_Entry_Dte_Full__R_Field_24.newFieldInGroup("pnd_Upld_Entry_Dte_Full_Pnd_Upld_Entry_Tme", 
            "#UPLD-ENTRY-TME", FieldType.STRING, 7);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_25 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_25", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_25.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_25.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_25.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_25.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Inst_Code_N = localVariables.newFieldInRecord("pnd_Inst_Code_N", "#INST-CODE-N", FieldType.NUMERIC, 6);
        pnd_Dcmt_Type = localVariables.newFieldInRecord("pnd_Dcmt_Type", "#DCMT-TYPE", FieldType.STRING, 8);

        pnd_Dcmt_Type__R_Field_26 = localVariables.newGroupInRecord("pnd_Dcmt_Type__R_Field_26", "REDEFINE", pnd_Dcmt_Type);
        pnd_Dcmt_Type_Pnd_Dcmt_Ctgry = pnd_Dcmt_Type__R_Field_26.newFieldInGroup("pnd_Dcmt_Type_Pnd_Dcmt_Ctgry", "#DCMT-CTGRY", FieldType.STRING, 1);
        pnd_Dcmt_Type_Pnd_Dcmt_Sub_Ctgry = pnd_Dcmt_Type__R_Field_26.newFieldInGroup("pnd_Dcmt_Type_Pnd_Dcmt_Sub_Ctgry", "#DCMT-SUB-CTGRY", FieldType.STRING, 
            3);
        pnd_Dcmt_Type_Pnd_Dcmt_Dtl = pnd_Dcmt_Type__R_Field_26.newFieldInGroup("pnd_Dcmt_Type_Pnd_Dcmt_Dtl", "#DCMT-DTL", FieldType.STRING, 4);
        pnd_Iis_Hrchy_Level_Cde = localVariables.newFieldInRecord("pnd_Iis_Hrchy_Level_Cde", "#IIS-HRCHY-LEVEL-CDE", FieldType.STRING, 1);
        pnd_Rqst_Log_Dte_Tme_Icw = localVariables.newFieldInRecord("pnd_Rqst_Log_Dte_Tme_Icw", "#RQST-LOG-DTE-TME-ICW", FieldType.STRING, 15);
        pnd_Iis_Instn_Link_Cde_Icw = localVariables.newFieldInRecord("pnd_Iis_Instn_Link_Cde_Icw", "#IIS-INSTN-LINK-CDE-ICW", FieldType.NUMERIC, 6);
        pnd_Rqst_Log_Dte_Tme_Cwf = localVariables.newFieldInRecord("pnd_Rqst_Log_Dte_Tme_Cwf", "#RQST-LOG-DTE-TME-CWF", FieldType.STRING, 15);
        pnd_Rqst_Log_Dte_Tme_Ncw = localVariables.newFieldInRecord("pnd_Rqst_Log_Dte_Tme_Ncw", "#RQST-LOG-DTE-TME-NCW", FieldType.STRING, 15);
        pnd_Multi_Rqst_Ind = localVariables.newFieldInRecord("pnd_Multi_Rqst_Ind", "#MULTI-RQST-IND", FieldType.STRING, 1);
        pnd_Pin_Sw = localVariables.newFieldInRecord("pnd_Pin_Sw", "#PIN-SW", FieldType.BOOLEAN, 1);
        pnd_Inst_Sw = localVariables.newFieldInRecord("pnd_Inst_Sw", "#INST-SW", FieldType.BOOLEAN, 1);
        pnd_Ppg1_Sw = localVariables.newFieldInRecord("pnd_Ppg1_Sw", "#PPG1-SW", FieldType.BOOLEAN, 1);
        pnd_Ppg2_Sw = localVariables.newFieldInRecord("pnd_Ppg2_Sw", "#PPG2-SW", FieldType.BOOLEAN, 1);
        pnd_Npin_Sw = localVariables.newFieldInRecord("pnd_Npin_Sw", "#NPIN-SW", FieldType.BOOLEAN, 1);
        pnd_Suppress_Sw = localVariables.newFieldInRecord("pnd_Suppress_Sw", "#SUPPRESS-SW", FieldType.BOOLEAN, 1);
        pnd_Cab_5_12 = localVariables.newFieldInRecord("pnd_Cab_5_12", "#CAB-5-12", FieldType.STRING, 8);
        pnd_Cab_7_7 = localVariables.newFieldInRecord("pnd_Cab_7_7", "#CAB-7-7", FieldType.STRING, 1);
        pnd_Cab_5_6 = localVariables.newFieldInRecord("pnd_Cab_5_6", "#CAB-5-6", FieldType.STRING, 2);
        pnd_Length = localVariables.newFieldInRecord("pnd_Length", "#LENGTH", FieldType.NUMERIC, 2);
        pnd_Curr_Dte_Tme = localVariables.newFieldInRecord("pnd_Curr_Dte_Tme", "#CURR-DTE-TME", FieldType.STRING, 15);

        pnd_Curr_Dte_Tme__R_Field_27 = localVariables.newGroupInRecord("pnd_Curr_Dte_Tme__R_Field_27", "REDEFINE", pnd_Curr_Dte_Tme);
        pnd_Curr_Dte_Tme_Pnd_Curr_Dte = pnd_Curr_Dte_Tme__R_Field_27.newFieldInGroup("pnd_Curr_Dte_Tme_Pnd_Curr_Dte", "#CURR-DTE", FieldType.NUMERIC, 
            8);
        pnd_Curr_Dte_Tme_Pnd_Curr_Tme = pnd_Curr_Dte_Tme__R_Field_27.newFieldInGroup("pnd_Curr_Dte_Tme_Pnd_Curr_Tme", "#CURR-TME", FieldType.NUMERIC, 
            7);
        pnd_Xref_With_Doc = localVariables.newFieldInRecord("pnd_Xref_With_Doc", "#XREF-WITH-DOC", FieldType.PACKED_DECIMAL, 7);
        pnd_Xref_No_Doc = localVariables.newFieldInRecord("pnd_Xref_No_Doc", "#XREF-NO-DOC", FieldType.PACKED_DECIMAL, 7);
        pnd_Pin_Not_Zero = localVariables.newFieldInRecord("pnd_Pin_Not_Zero", "#PIN-NOT-ZERO", FieldType.PACKED_DECIMAL, 7);
        pnd_Cab_Not_Blank = localVariables.newFieldInRecord("pnd_Cab_Not_Blank", "#CAB-NOT-BLANK", FieldType.PACKED_DECIMAL, 7);
        pnd_Pin_With_Z = localVariables.newFieldInRecord("pnd_Pin_With_Z", "#PIN-WITH-Z", FieldType.PACKED_DECIMAL, 7);
        pnd_Pin_With_No_Z = localVariables.newFieldInRecord("pnd_Pin_With_No_Z", "#PIN-WITH-NO-Z", FieldType.PACKED_DECIMAL, 7);
        pnd_Inst_In_Cab = localVariables.newFieldInRecord("pnd_Inst_In_Cab", "#INST-IN-CAB", FieldType.PACKED_DECIMAL, 7);
        pnd_Inst_With_Z = localVariables.newFieldInRecord("pnd_Inst_With_Z", "#INST-WITH-Z", FieldType.PACKED_DECIMAL, 7);
        pnd_Inst_With_No_Z = localVariables.newFieldInRecord("pnd_Inst_With_No_Z", "#INST-WITH-NO-Z", FieldType.PACKED_DECIMAL, 7);
        pnd_Ppg1_In_Cab = localVariables.newFieldInRecord("pnd_Ppg1_In_Cab", "#PPG1-IN-CAB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ppg1_With_Z = localVariables.newFieldInRecord("pnd_Ppg1_With_Z", "#PPG1-WITH-Z", FieldType.PACKED_DECIMAL, 7);
        pnd_Ppg1_With_No_Z = localVariables.newFieldInRecord("pnd_Ppg1_With_No_Z", "#PPG1-WITH-NO-Z", FieldType.PACKED_DECIMAL, 7);
        pnd_Ppg2_In_Cab = localVariables.newFieldInRecord("pnd_Ppg2_In_Cab", "#PPG2-IN-CAB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ppg2_With_Z = localVariables.newFieldInRecord("pnd_Ppg2_With_Z", "#PPG2-WITH-Z", FieldType.PACKED_DECIMAL, 7);
        pnd_Ppg2_With_No_Z = localVariables.newFieldInRecord("pnd_Ppg2_With_No_Z", "#PPG2-WITH-NO-Z", FieldType.PACKED_DECIMAL, 7);
        pnd_Npin_In_Cab = localVariables.newFieldInRecord("pnd_Npin_In_Cab", "#NPIN-IN-CAB", FieldType.PACKED_DECIMAL, 7);
        pnd_Npin_With_Z = localVariables.newFieldInRecord("pnd_Npin_With_Z", "#NPIN-WITH-Z", FieldType.PACKED_DECIMAL, 7);
        pnd_Npin_With_No_Z = localVariables.newFieldInRecord("pnd_Npin_With_No_Z", "#NPIN-WITH-NO-Z", FieldType.PACKED_DECIMAL, 7);
        pnd_Cab_Id_Nocond = localVariables.newFieldInRecord("pnd_Cab_Id_Nocond", "#CAB-ID-NOCOND", FieldType.PACKED_DECIMAL, 7);
        pnd_Cab_Id_Cirs = localVariables.newFieldInRecord("pnd_Cab_Id_Cirs", "#CAB-ID-CIRS", FieldType.PACKED_DECIMAL, 7);
        pnd_Suppress_Cnt = localVariables.newFieldInRecord("pnd_Suppress_Cnt", "#SUPPRESS-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cab_N_Pin_Blank = localVariables.newFieldInRecord("pnd_Cab_N_Pin_Blank", "#CAB-N-PIN-BLANK", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Passed_Cut_Off = localVariables.newFieldInRecord("pnd_Total_Passed_Cut_Off", "#TOTAL-PASSED-CUT-OFF", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Msg_Doctype = localVariables.newFieldInRecord("pnd_Total_Msg_Doctype", "#TOTAL-MSG-DOCTYPE", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Msg_Cwf = localVariables.newFieldInRecord("pnd_Total_Msg_Cwf", "#TOTAL-MSG-CWF", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Msg_Icw = localVariables.newFieldInRecord("pnd_Total_Msg_Icw", "#TOTAL-MSG-ICW", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Msg_Ncw = localVariables.newFieldInRecord("pnd_Total_Msg_Ncw", "#TOTAL-MSG-NCW", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Msg = localVariables.newFieldInRecord("pnd_Total_Msg", "#TOTAL-MSG", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Success = localVariables.newFieldInRecord("pnd_Total_Success", "#TOTAL-SUCCESS", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Success_Cnt = localVariables.newFieldInRecord("pnd_Total_Success_Cnt", "#TOTAL-SUCCESS-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cwf_Success = localVariables.newFieldInRecord("pnd_Cwf_Success", "#CWF-SUCCESS", FieldType.PACKED_DECIMAL, 7);
        pnd_Icw_Success = localVariables.newFieldInRecord("pnd_Icw_Success", "#ICW-SUCCESS", FieldType.PACKED_DECIMAL, 7);
        pnd_Ncw_Success = localVariables.newFieldInRecord("pnd_Ncw_Success", "#NCW-SUCCESS", FieldType.PACKED_DECIMAL, 7);
        pnd_Doctype_Success = localVariables.newFieldInRecord("pnd_Doctype_Success", "#DOCTYPE-SUCCESS", FieldType.PACKED_DECIMAL, 7);
        pnd_Image_Del_Cnt = localVariables.newFieldInRecord("pnd_Image_Del_Cnt", "#IMAGE-DEL-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Upld_Dte_Today_Cnt = localVariables.newFieldInRecord("pnd_Upld_Dte_Today_Cnt", "#UPLD-DTE-TODAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Archve_Dte_Cnt = localVariables.newFieldInRecord("pnd_Archve_Dte_Cnt", "#ARCHVE-DTE-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Min_Save = localVariables.newFieldInRecord("pnd_Min_Save", "#MIN-SAVE", FieldType.NUMERIC, 11);
        pnd_Rqst_Routing_Key = localVariables.newFieldInRecord("pnd_Rqst_Routing_Key", "#RQST-ROUTING-KEY", FieldType.STRING, 30);

        pnd_Rqst_Routing_Key__R_Field_28 = localVariables.newGroupInRecord("pnd_Rqst_Routing_Key__R_Field_28", "REDEFINE", pnd_Rqst_Routing_Key);
        pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Rqst_Routing_Key__R_Field_28.newFieldInGroup("pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Icw_Rqst_Routing_Key = localVariables.newFieldInRecord("pnd_Icw_Rqst_Routing_Key", "#ICW-RQST-ROUTING-KEY", FieldType.STRING, 22);

        pnd_Icw_Rqst_Routing_Key__R_Field_29 = localVariables.newGroupInRecord("pnd_Icw_Rqst_Routing_Key__R_Field_29", "REDEFINE", pnd_Icw_Rqst_Routing_Key);
        pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Rqst_Log_Dte_Tme = pnd_Icw_Rqst_Routing_Key__R_Field_29.newFieldInGroup("pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Rqst_Log_Dte_Tme", 
            "#ICW-RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Strt_Evnt_Dte_Tme = pnd_Icw_Rqst_Routing_Key__R_Field_29.newFieldInGroup("pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Strt_Evnt_Dte_Tme", 
            "#ICW-STRT-EVNT-DTE-TME", FieldType.TIME);
        pnd_Rqst_Id_Key = localVariables.newFieldInRecord("pnd_Rqst_Id_Key", "#RQST-ID-KEY", FieldType.STRING, 30);

        pnd_Rqst_Id_Key__R_Field_30 = localVariables.newGroupInRecord("pnd_Rqst_Id_Key__R_Field_30", "REDEFINE", pnd_Rqst_Id_Key);
        pnd_Rqst_Id_Key_Pnd_Rqst_Id = pnd_Rqst_Id_Key__R_Field_30.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 28);

        pnd_Rqst_Id_Key__R_Field_31 = pnd_Rqst_Id_Key__R_Field_30.newGroupInGroup("pnd_Rqst_Id_Key__R_Field_31", "REDEFINE", pnd_Rqst_Id_Key_Pnd_Rqst_Id);
        pnd_Rqst_Id_Key_Pnd_Rqst_Work_Prcss_Id = pnd_Rqst_Id_Key__R_Field_31.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Work_Prcss_Id", "#RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        pnd_Rqst_Id_Key_Pnd_Rqst_Tiaa_Rcvd_Dte = pnd_Rqst_Id_Key__R_Field_31.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Tiaa_Rcvd_Dte", "#RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        pnd_Rqst_Id_Key_Pnd_Rqst_Case_Id = pnd_Rqst_Id_Key__R_Field_31.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Case_Id", "#RQST-CASE-ID", FieldType.STRING, 
            1);
        pnd_Rqst_Id_Key_Pnd_Rqst_Sub_Rqst_Cde = pnd_Rqst_Id_Key__R_Field_31.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Sub_Rqst_Cde", "#RQST-SUB-RQST-CDE", 
            FieldType.STRING, 1);
        pnd_Rqst_Id_Key_Pnd_Rqst_Pin_Nbr = pnd_Rqst_Id_Key__R_Field_31.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Pin_Nbr", "#RQST-PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Rqst_Id_Key_Pnd_Multi_Rqst_Id = pnd_Rqst_Id_Key__R_Field_30.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Multi_Rqst_Id", "#MULTI-RQST-ID", FieldType.STRING, 
            1);
        pnd_Rqst_Id_Key_Pnd_Actve_Ind = pnd_Rqst_Id_Key__R_Field_30.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 1);
        pnd_Image_Source_Id = localVariables.newFieldInRecord("pnd_Image_Source_Id", "#IMAGE-SOURCE-ID", FieldType.STRING, 6);
        pnd_Cabinet_Id = localVariables.newFieldInRecord("pnd_Cabinet_Id", "#CABINET-ID", FieldType.STRING, 12);
        pnd_Tiaa_Rcvd_Dte = localVariables.newFieldInRecord("pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Case_Workprcss_Multi_Subrqst = localVariables.newFieldInRecord("pnd_Case_Workprcss_Multi_Subrqst", "#CASE-WORKPRCSS-MULTI-SUBRQST", FieldType.STRING, 
            9);

        pnd_Case_Workprcss_Multi_Subrqst__R_Field_32 = localVariables.newGroupInRecord("pnd_Case_Workprcss_Multi_Subrqst__R_Field_32", "REDEFINE", pnd_Case_Workprcss_Multi_Subrqst);
        pnd_Case_Workprcss_Multi_Subrqst_Pnd_Case_Id = pnd_Case_Workprcss_Multi_Subrqst__R_Field_32.newFieldInGroup("pnd_Case_Workprcss_Multi_Subrqst_Pnd_Case_Id", 
            "#CASE-ID", FieldType.STRING, 1);
        pnd_Case_Workprcss_Multi_Subrqst_Pnd_Work_Prcss_Id = pnd_Case_Workprcss_Multi_Subrqst__R_Field_32.newFieldInGroup("pnd_Case_Workprcss_Multi_Subrqst_Pnd_Work_Prcss_Id", 
            "#WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Case_Workprcss_Multi_Subrqst_Pnd_Multi_Ind = pnd_Case_Workprcss_Multi_Subrqst__R_Field_32.newFieldInGroup("pnd_Case_Workprcss_Multi_Subrqst_Pnd_Multi_Ind", 
            "#MULTI-IND", FieldType.STRING, 1);
        pnd_Case_Workprcss_Multi_Subrqst_Pnd_Subrqst_Ind = pnd_Case_Workprcss_Multi_Subrqst__R_Field_32.newFieldInGroup("pnd_Case_Workprcss_Multi_Subrqst_Pnd_Subrqst_Ind", 
            "#SUBRQST-IND", FieldType.STRING, 1);
        pnd_Pin_Ppg = localVariables.newFieldInRecord("pnd_Pin_Ppg", "#PIN-PPG", FieldType.STRING, 12);
        pnd_Entry_Dte_Tme = localVariables.newFieldInRecord("pnd_Entry_Dte_Tme", "#ENTRY-DTE-TME", FieldType.TIME);
        pnd_Identify_Dte_Tme = localVariables.newFieldInRecord("pnd_Identify_Dte_Tme", "#IDENTIFY-DTE-TME", FieldType.STRING, 19);
        pnd_Tiaa_Rcvd_Dte_Tme_Cwf = localVariables.newFieldInRecord("pnd_Tiaa_Rcvd_Dte_Tme_Cwf", "#TIAA-RCVD-DTE-TME-CWF", FieldType.STRING, 8);
        pnd_Tiaa_Rcvd_Dte_Tme_Icw = localVariables.newFieldInRecord("pnd_Tiaa_Rcvd_Dte_Tme_Icw", "#TIAA-RCVD-DTE-TME-ICW", FieldType.STRING, 8);
        pnd_Tiaa_Rcvd_Dte_Tme_Ncw = localVariables.newFieldInRecord("pnd_Tiaa_Rcvd_Dte_Tme_Ncw", "#TIAA-RCVD-DTE-TME-NCW", FieldType.STRING, 8);
        pnd_Action_Cde = localVariables.newFieldInRecord("pnd_Action_Cde", "#ACTION-CDE", FieldType.STRING, 2);
        pnd_Wrk_Msg = localVariables.newFieldInRecord("pnd_Wrk_Msg", "#WRK-MSG", FieldType.STRING, 79);

        pnd_Wrk_Msg__R_Field_33 = localVariables.newGroupInRecord("pnd_Wrk_Msg__R_Field_33", "REDEFINE", pnd_Wrk_Msg);
        pnd_Wrk_Msg_Pnd_Wrk_Msg_1_50 = pnd_Wrk_Msg__R_Field_33.newFieldInGroup("pnd_Wrk_Msg_Pnd_Wrk_Msg_1_50", "#WRK-MSG-1-50", FieldType.STRING, 50);
        pnd_Batch_Nbr = localVariables.newFieldInRecord("pnd_Batch_Nbr", "#BATCH-NBR", FieldType.NUMERIC, 10, 2);
        pnd_Date_Today = localVariables.newFieldInRecord("pnd_Date_Today", "#DATE-TODAY", FieldType.STRING, 8);
        pnd_Arch_Dte = localVariables.newFieldInRecord("pnd_Arch_Dte", "#ARCH-DTE", FieldType.STRING, 8);
        pnd_Parm_Key_Value = localVariables.newFieldInRecord("pnd_Parm_Key_Value", "#PARM-KEY-VALUE", FieldType.STRING, 17);

        pnd_Parm_Key_Value__R_Field_34 = localVariables.newGroupInRecord("pnd_Parm_Key_Value__R_Field_34", "REDEFINE", pnd_Parm_Key_Value);
        pnd_Parm_Key_Value_Pnd_Parm_Key_Source = pnd_Parm_Key_Value__R_Field_34.newFieldInGroup("pnd_Parm_Key_Value_Pnd_Parm_Key_Source", "#PARM-KEY-SOURCE", 
            FieldType.STRING, 6);
        pnd_Parm_Key_Value_Pnd_Parm_Key_Min = pnd_Parm_Key_Value__R_Field_34.newFieldInGroup("pnd_Parm_Key_Value_Pnd_Parm_Key_Min", "#PARM-KEY-MIN", FieldType.STRING, 
            11);

        pnd_Parm_Key_Value__R_Field_35 = pnd_Parm_Key_Value__R_Field_34.newGroupInGroup("pnd_Parm_Key_Value__R_Field_35", "REDEFINE", pnd_Parm_Key_Value_Pnd_Parm_Key_Min);
        pnd_Parm_Key_Value_Pnd_Parm_Key_Min_N = pnd_Parm_Key_Value__R_Field_35.newFieldInGroup("pnd_Parm_Key_Value_Pnd_Parm_Key_Min_N", "#PARM-KEY-MIN-N", 
            FieldType.NUMERIC, 11);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ncw_Master_Index.reset();
        vw_icw_Master_Index.reset();
        vw_cwf_Master_Index_View.reset();
        vw_cwf_Support_Tbl.reset();
        vw_cwf_Image_Xref.reset();
        vw_cwf_Efm_Document.reset();
        vw_ncw_Efm_Document.reset();
        vw_cwf_Arch_Document.reset();
        vw_icw_Efm_Document.reset();
        vw_cwf_Mit_History.reset();
        vw_icw_Mit_History.reset();
        vw_ncw_Mit_History.reset();
        vw_pst_Outgoing_Mail_Item.reset();
        vw_cwf_Image_Xref_H1.reset();
        vw_cwf_Efm_Document_H1.reset();
        vw_icw_Efm_Document_H1.reset();
        vw_ncw_Efm_Document_H1.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Efsn6130() throws Exception
    {
        super("Efsn6130");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  DEFINE PRINTER (DAT=1)
        //*  DEFINE PRINTER (MSG=2)
        //*  FORMAT        LS=133  PS=60 ZP=OFF
        //*  FORMAT  (DAT) LS=133  PS=60 ZP=OFF
        //*  FORMAT  (MSG) LS=133  PS=60 ZP=OFF
        //*  NEWPAGE
        //*  NEWPAGE (DAT)
        //*  NEWPAGE (MSG)
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM := *PROGRAM
        //*  ---------------- SET UP KEY --------------------------------
        pnd_Parm_Key_Value_Pnd_Parm_Key_Source.setValue(pdaEfsa6110.getEfsa6110_Parm_Source_Id());                                                                        //Natural: ASSIGN #PARM-KEY-VALUE.#PARM-KEY-SOURCE := EFSA6110.PARM-SOURCE-ID
        pnd_Parm_Key_Value_Pnd_Parm_Key_Min_N.setValue(pdaEfsa6110.getEfsa6110_Parm_Min());                                                                               //Natural: ASSIGN #PARM-KEY-VALUE.#PARM-KEY-MIN-N := EFSA6110.PARM-MIN
        vw_cwf_Image_Xref.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) CWF-IMAGE-XREF BY SOURCE-ID-MIN-KEY = #PARM-KEY-VALUE
        (
        "RD1",
        new Wc[] { new Wc("SOURCE_ID_MIN_KEY", ">=", pnd_Parm_Key_Value, WcType.BY) },
        new Oc[] { new Oc("SOURCE_ID_MIN_KEY", "ASC") },
        1
        );
        RD1:
        while (condition(vw_cwf_Image_Xref.readNextRow("RD1")))
        {
            if (condition(cwf_Image_Xref_Image_Delete_Ind.equals("Y")))                                                                                                   //Natural: IF CWF-IMAGE-XREF.IMAGE-DELETE-IND = 'Y'
            {
                //*    ADD 1 TO #IMAGE-DEL-CNT
                pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(2);                                                                                                   //Natural: MOVE 2 TO EFSA6110.PARM-RETURN-CODE
                pdaEfsa6110.getEfsa6110_Parm_Err_Msg().setValue("Image Delete Ind Equal 'Y' ");                                                                           //Natural: MOVE 'Image Delete Ind Equal "Y" ' TO EFSA6110.PARM-ERR-MSG
                if (true) break RD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Upld_Entry_Dte_Full.setValueEdited(cwf_Image_Xref_Upld_Date_Time,new ReportEditMask("YYYYMMDDHHIISST"));                                                  //Natural: MOVE EDITED CWF-IMAGE-XREF.UPLD-DATE-TIME ( EM = YYYYMMDDHHIISST ) TO #UPLD-ENTRY-DTE-FULL
            pnd_Date_Today.setValue(Global.getDATN());                                                                                                                    //Natural: ASSIGN #DATE-TODAY := *DATN
            if (condition(pnd_Upld_Entry_Dte_Full_Pnd_Upld_Entry_Dte.equals(pnd_Date_Today)))                                                                             //Natural: IF #UPLD-ENTRY-DTE = #DATE-TODAY
            {
                pnd_Upld_Dte_Today_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #UPLD-DTE-TODAY-CNT
                pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(2);                                                                                                   //Natural: MOVE 2 TO EFSA6110.PARM-RETURN-CODE
                pdaEfsa6110.getEfsa6110_Parm_Err_Msg().setValue("UPLOAD DATE EQUAL TODAY'S DATE");                                                                        //Natural: MOVE 'UPLOAD DATE EQUAL TODAY"S DATE' TO EFSA6110.PARM-ERR-MSG
                if (true) break RD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Arch_Dte.setValueEdited(cwf_Image_Xref_Archive_Dte_Tme,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED CWF-IMAGE-XREF.ARCHIVE-DTE-TME ( EM = YYYYMMDD ) TO #ARCH-DTE
            //*  11/10/00
            if (condition(DbsUtil.maskMatches(pnd_Arch_Dte,"YYYYMMDD") && DbsUtil.maskMatches(pnd_Arch_Dte,"19-20YYMMDD")))                                               //Natural: IF #ARCH-DTE = MASK ( YYYYMMDD ) AND #ARCH-DTE = MASK ( 19-20YYMMDD )
            {
                //*    ADD 1 TO #ARCHVE-DTE-CNT
                pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(2);                                                                                                   //Natural: MOVE 2 TO EFSA6110.PARM-RETURN-CODE
                pdaEfsa6110.getEfsa6110_Parm_Err_Msg().setValue("Image record already Archived");                                                                         //Natural: MOVE 'Image record already Archived' TO EFSA6110.PARM-ERR-MSG
                if (true) break RD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Doc_Exist.reset();                                                                                                                                        //Natural: RESET #DOC-EXIST
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-DOC-EXISTS
            sub_Check_If_Doc_Exists();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Doc_Exist.getBoolean()))                                                                                                                    //Natural: IF #DOC-EXIST
            {
                pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(2);                                                                                                   //Natural: MOVE 2 TO EFSA6110.PARM-RETURN-CODE
                pdaEfsa6110.getEfsa6110_Parm_Err_Msg().setValue("Document Record already Exist ");                                                                        //Natural: MOVE 'Document Record already Exist ' TO EFSA6110.PARM-ERR-MSG
                //*      ADD 1 TO #XREF-WITH-DOC
                if (true) break RD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. ) IMMEDIATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Xref_No_Doc.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #XREF-NO-DOC
                pnd_Curr_Dte_Tme_Pnd_Curr_Dte.setValue(Global.getDATN());                                                                                                 //Natural: ASSIGN #CURR-DTE := *DATN
                pnd_Curr_Dte_Tme_Pnd_Curr_Tme.setValue(Global.getTIMN());                                                                                                 //Natural: ASSIGN #CURR-TME := *TIMN
                pnd_Cwf_Misc_Fldr_Exist.reset();                                                                                                                          //Natural: RESET #CWF-MISC-FLDR-EXIST #NCW-MISC-FLDR-EXIST #ICWP1-MISC-FLDR-EXIST #ICWI-MISC-FLDR-EXIST #RQST-LOG-DTE-TME-CWF #RQST-LOG-DTE-TME-ICW #RQST-LOG-DTE-TME-NCW #TIAA-RCVD-DTE-TME-CWF #TIAA-RCVD-DTE-TME-ICW #TIAA-RCVD-DTE-TME-NCW #IIS-INSTN-LINK-CDE-ICW #IIS-HRCHY-LEVEL-CDE #MULTI-RQST-IND EFSA9120 IEFA9000 NEFA9000 PSTA9657 #PIN-SW #INST-SW #PPG1-SW #PPG2-SW #NPIN-SW ##MSG
                pnd_Ncw_Misc_Fldr_Exist.reset();
                pnd_Icwp1_Misc_Fldr_Exist.reset();
                pnd_Icwi_Misc_Fldr_Exist.reset();
                pnd_Rqst_Log_Dte_Tme_Cwf.reset();
                pnd_Rqst_Log_Dte_Tme_Icw.reset();
                pnd_Rqst_Log_Dte_Tme_Ncw.reset();
                pnd_Tiaa_Rcvd_Dte_Tme_Cwf.reset();
                pnd_Tiaa_Rcvd_Dte_Tme_Icw.reset();
                pnd_Tiaa_Rcvd_Dte_Tme_Ncw.reset();
                pnd_Iis_Instn_Link_Cde_Icw.reset();
                pnd_Iis_Hrchy_Level_Cde.reset();
                pnd_Multi_Rqst_Ind.reset();
                pdaEfsa9120.getEfsa9120().reset();
                pdaIefa9000.getIefa9000().reset();
                pdaNefa9000.getNefa9000().reset();
                pdaPsta9657.getPsta9657().reset();
                pnd_Pin_Sw.reset();
                pnd_Inst_Sw.reset();
                pnd_Ppg1_Sw.reset();
                pnd_Ppg2_Sw.reset();
                pnd_Npin_Sw.reset();
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
                pdaPsta9657.getPsta9657_Pnd_Srce_Id().setValue(pdaEfsa6110.getEfsa6110_Parm_Source_Id());                                                                 //Natural: ASSIGN PSTA9657.#SRCE-ID := EFSA6110.PARM-SOURCE-ID
                pdaPsta9657.getPsta9657_Pnd_Min_A().setValue(pdaEfsa6110.getEfsa6110_Parm_Min());                                                                         //Natural: ASSIGN PSTA9657.#MIN-A := EFSA6110.PARM-MIN
                //*  PST-OUTGOING-MAIL-ITEM
                DbsUtil.callnat(Pstn9657.class , getCurrentProcessState(), pdaPsta9657.getPsta9657(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),            //Natural: CALLNAT 'PSTN9657' PSTA9657 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                    pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ") && pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ")))              //Natural: IF ##MSG NE ' ' AND ##ERROR-FIELD NE ' '
                {
                    pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(2);                                                                                               //Natural: MOVE 2 TO EFSA6110.PARM-RETURN-CODE
                    pdaEfsa6110.getEfsa6110_Parm_Err_Msg().setValue("Outgoing Mail Item Not Exist");                                                                      //Natural: MOVE 'Outgoing Mail Item Not Exist' TO EFSA6110.PARM-ERR-MSG
                    if (true) break RD1;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RD1. ) IMMEDIATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Doctype_Success.nadd(1);                                                                                                                          //Natural: ADD 1 TO #DOCTYPE-SUCCESS
                }                                                                                                                                                         //Natural: END-IF
                pnd_Batch_Nbr.setValue(cwf_Image_Xref_Batch_Id);                                                                                                          //Natural: ASSIGN #BATCH-NBR := CWF-IMAGE-XREF.BATCH-ID
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-MISC-FLDR-EXISTS
                sub_Check_If_Misc_Fldr_Exists();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ADD A DOCUMENT
                //*  ADD A DOCUMENT & MIT
                //*  SUPPRESS CASES
                short decideConditionsMet1244 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CWF-MISC-FLDR-EXIST OR #NCW-MISC-FLDR-EXIST OR #ICWP1-MISC-FLDR-EXIST OR #ICWI-MISC-FLDR-EXIST
                if (condition(pnd_Cwf_Misc_Fldr_Exist.getBoolean() || pnd_Ncw_Misc_Fldr_Exist.getBoolean() || pnd_Icwp1_Misc_Fldr_Exist.getBoolean() || 
                    pnd_Icwi_Misc_Fldr_Exist.getBoolean()))
                {
                    decideConditionsMet1244++;
                    pdaEfsa9120.getEfsa9120_Action().setValue("AD");                                                                                                      //Natural: ASSIGN EFSA9120.ACTION := IEFA9000.ACTION := NEFA9000.ACTION := 'AD'
                    pdaIefa9000.getIefa9000_Action().setValue("AD");
                    pdaNefa9000.getNefa9000_Action().setValue("AD");
                }                                                                                                                                                         //Natural: WHEN #PIN-SW OR #INST-SW OR #PPG1-SW OR #PPG2-SW OR #NPIN-SW
                else if (condition(pnd_Pin_Sw.getBoolean() || pnd_Inst_Sw.getBoolean() || pnd_Ppg1_Sw.getBoolean() || pnd_Ppg2_Sw.getBoolean() || pnd_Npin_Sw.getBoolean()))
                {
                    decideConditionsMet1244++;
                    pdaEfsa9120.getEfsa9120_Action().setValue("AN");                                                                                                      //Natural: ASSIGN EFSA9120.ACTION := IEFA9000.ACTION := NEFA9000.ACTION := 'AN'
                    pdaIefa9000.getIefa9000_Action().setValue("AN");
                    pdaNefa9000.getNefa9000_Action().setValue("AN");
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM DETN-CWF-TO-ADD-DOCUMENT-OR-MIT
                sub_Detn_Cwf_To_Add_Document_Or_Mit();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaEfsa6110.getEfsa6110_Parm_Err_Msg().greater(" ")))                                                                                       //Natural: IF EFSA6110.PARM-ERR-MSG GT ' '
                {
                    if (true) break RD1;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RD1. ) IMMEDIATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(1);                                                                                               //Natural: MOVE 1 TO EFSA6110.PARM-RETURN-CODE
                    if (true) break RD1;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RD1. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  RD1.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ========================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-DOC-EXISTS
        //*  OR CWF-EFM-DOCUMENT.IMAGE-MIN       NE
        //*  OR ICW-EFM-DOCUMENT.IMAGE-MIN       NE
        //*  OR NCW-EFM-DOCUMENT.IMAGE-MIN       NE
        //*  ==============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-MISC-FLDR-EXISTS
        //*          OR (#PPG-CODE1 IS (N4) AND #CAB-5-12 = ' ')
        //*  ====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETN-CWF-TO-ADD-DOCUMENT-OR-MIT
        //*  ------
        //*  ---------
        //*  =======================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CWF-DOCUMENT
        //*  ==================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ICW-DOCUMENT
        //*  #MULTI-IND     := ICW-MASTER-INDEX.MULTI-RQST-IND
        //*  DETERMINE INITIAL DESTINATION UNIT
        //*  ==================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NCW-DOCUMENT
        //*  #MULTI-IND     := NCW-MASTER-INDEX.MULTI-RQST-IND
        //*  DETERMINE INITIAL DESTINATION UNIT
    }
    private void sub_Check_If_Doc_Exists() throws Exception                                                                                                               //Natural: CHECK-IF-DOC-EXISTS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ========================
        //*  FIND MINS ON CWF-EFM-DOCUMENT FILE USING IMAGE-POINTER-KEY.
        vw_cwf_Efm_Document.startDatabaseFind                                                                                                                             //Natural: FIND CWF-EFM-DOCUMENT IMAGE-POINTER-KEY = #PARM-KEY-VALUE
        (
        "FND1",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Parm_Key_Value, WcType.WITH) }
        );
        FND1:
        while (condition(vw_cwf_Efm_Document.readNextRow("FND1")))
        {
            vw_cwf_Efm_Document.setIfNotFoundControlFlag(false);
            if (condition(cwf_Efm_Document_Image_Source_Id.notEquals(pnd_Parm_Key_Value_Pnd_Parm_Key_Source) || !cwf_Efm_Document_Image_Min.getSubstring(1,               //Natural: IF CWF-EFM-DOCUMENT.IMAGE-SOURCE-ID NE #PARM-KEY-VALUE.#PARM-KEY-SOURCE OR SUBSTR ( CWF-EFM-DOCUMENT.IMAGE-MIN,1,11 ) NE #PARM-KEY-VALUE.#PARM-KEY-MIN
                11).equals(pnd_Parm_Key_Value_Pnd_Parm_Key_Min)))
            {
                if (true) break FND1;                                                                                                                                     //Natural: ESCAPE BOTTOM ( FND1. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Efm_Document_Document_Retention_Ind.equals("D")))                                                                                           //Natural: IF CWF-EFM-DOCUMENT.DOCUMENT-RETENTION-IND = 'D'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Doc_Exist.setValue(true);                                                                                                                                 //Natural: ASSIGN #DOC-EXIST := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  FND1.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  FIND MINS ON ICW-EFM-DOCUMENT FILE USING IMAGE-POINTER-KEY.
        vw_icw_Efm_Document.startDatabaseFind                                                                                                                             //Natural: FIND ICW-EFM-DOCUMENT IMAGE-POINTER-KEY = #PARM-KEY-VALUE
        (
        "FND2",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Parm_Key_Value, WcType.WITH) }
        );
        FND2:
        while (condition(vw_icw_Efm_Document.readNextRow("FND2")))
        {
            vw_icw_Efm_Document.setIfNotFoundControlFlag(false);
            if (condition(icw_Efm_Document_Image_Source_Id.notEquals(pnd_Parm_Key_Value_Pnd_Parm_Key_Source) || !icw_Efm_Document_Image_Min.getSubstring(1,               //Natural: IF ICW-EFM-DOCUMENT.IMAGE-SOURCE-ID NE #PARM-KEY-VALUE.#PARM-KEY-SOURCE OR SUBSTR ( ICW-EFM-DOCUMENT.IMAGE-MIN,1,11 ) NE #PARM-KEY-VALUE.#PARM-KEY-MIN
                11).equals(pnd_Parm_Key_Value_Pnd_Parm_Key_Min)))
            {
                if (true) break FND2;                                                                                                                                     //Natural: ESCAPE BOTTOM ( FND2. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(icw_Efm_Document_Dcmnt_Rtntn.equals("D")))                                                                                                      //Natural: IF ICW-EFM-DOCUMENT.DCMNT-RTNTN = 'D'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Doc_Exist.setValue(true);                                                                                                                                 //Natural: ASSIGN #DOC-EXIST := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  FIND MINS ON NCW-EFM-DOCUMENT FILE USING IMAGE-POINTER-KEY.
        vw_ncw_Efm_Document.startDatabaseFind                                                                                                                             //Natural: FIND NCW-EFM-DOCUMENT IMAGE-POINTER-KEY = #PARM-KEY-VALUE
        (
        "FND3",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Parm_Key_Value, WcType.WITH) }
        );
        FND3:
        while (condition(vw_ncw_Efm_Document.readNextRow("FND3")))
        {
            vw_ncw_Efm_Document.setIfNotFoundControlFlag(false);
            if (condition(ncw_Efm_Document_Image_Source_Id.notEquals(pnd_Parm_Key_Value_Pnd_Parm_Key_Source) || !ncw_Efm_Document_Image_Min.getSubstring(1,               //Natural: IF NCW-EFM-DOCUMENT.IMAGE-SOURCE-ID NE #PARM-KEY-VALUE.#PARM-KEY-SOURCE OR SUBSTR ( NCW-EFM-DOCUMENT.IMAGE-MIN,1,11 ) NE #PARM-KEY-VALUE.#PARM-KEY-MIN
                11).equals(pnd_Parm_Key_Value_Pnd_Parm_Key_Min)))
            {
                if (true) break FND3;                                                                                                                                     //Natural: ESCAPE BOTTOM ( FND3. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ncw_Efm_Document_Dcmnt_Rtntn_Ind.equals("D")))                                                                                                  //Natural: IF NCW-EFM-DOCUMENT.DCMNT-RTNTN-IND = 'D'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Doc_Exist.setValue(true);                                                                                                                                 //Natural: ASSIGN #DOC-EXIST := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  FND3.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  FIND MINS ON ARCH-EFM-DOCUMENT FILE USING IMAGE-POINTER-KEY.
        vw_cwf_Arch_Document.startDatabaseFind                                                                                                                            //Natural: FIND CWF-ARCH-DOCUMENT IMAGE-POINTER-KEY = #PARM-KEY-VALUE
        (
        "FND4",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Parm_Key_Value, WcType.WITH) }
        );
        FND4:
        while (condition(vw_cwf_Arch_Document.readNextRow("FND4")))
        {
            vw_cwf_Arch_Document.setIfNotFoundControlFlag(false);
            if (condition(cwf_Arch_Document_Image_Source_Id.notEquals(pnd_Parm_Key_Value_Pnd_Parm_Key_Source) || cwf_Arch_Document_Image_Min.notEquals(pnd_Parm_Key_Value_Pnd_Parm_Key_Min))) //Natural: IF CWF-ARCH-DOCUMENT.IMAGE-SOURCE-ID NE #PARM-KEY-VALUE.#PARM-KEY-SOURCE OR CWF-ARCH-DOCUMENT.IMAGE-MIN NE #PARM-KEY-VALUE.#PARM-KEY-MIN
            {
                if (true) break FND4;                                                                                                                                     //Natural: ESCAPE BOTTOM ( FND4. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Arch_Document_Document_Retention_Ind.equals("D")))                                                                                          //Natural: IF CWF-ARCH-DOCUMENT.DOCUMENT-RETENTION-IND = 'D'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Doc_Exist.setValue(true);                                                                                                                                 //Natural: ASSIGN #DOC-EXIST := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //* FND4.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Check_If_Misc_Fldr_Exists() throws Exception                                                                                                         //Natural: CHECK-IF-MISC-FLDR-EXISTS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ==============================
        short decideConditionsMet1384 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-IMAGE-XREF.PH-UNIQUE-ID-NBR GT 0
        if (condition(cwf_Image_Xref_Ph_Unique_Id_Nbr.greater(getZero())))
        {
            decideConditionsMet1384++;
            pnd_Pin_Not_Zero.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #PIN-NOT-ZERO
            pnd_Pin_Sw.setValue(true);                                                                                                                                    //Natural: ASSIGN #PIN-SW := TRUE
            pdaEfsa6110.getEfsa6110_Parm_System().setValue("CWF");                                                                                                        //Natural: MOVE 'CWF' TO EFSA6110.PARM-SYSTEM
            vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                    //Natural: READ CWF-MASTER-INDEX-VIEW BY PIN-HISTORY-KEY = CWF-IMAGE-XREF.PH-UNIQUE-ID-NBR
            (
            "READ01",
            new Wc[] { new Wc("PIN_HISTORY_KEY", ">=", cwf_Image_Xref_Ph_Unique_Id_Nbr, WcType.BY) },
            new Oc[] { new Oc("PIN_HISTORY_KEY", "ASC") }
            );
            READ01:
            while (condition(vw_cwf_Master_Index_View.readNextRow("READ01")))
            {
                if (condition(cwf_Master_Index_View_Pin_Nbr.notEquals(cwf_Image_Xref_Ph_Unique_Id_Nbr)))                                                                  //Natural: IF CWF-MASTER-INDEX-VIEW.PIN-NBR NE CWF-IMAGE-XREF.PH-UNIQUE-ID-NBR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cwf_Master_Index_View_Rqst_Work_Prcss_Id.equals("Z")))                                                                                      //Natural: IF CWF-MASTER-INDEX-VIEW.RQST-WORK-PRCSS-ID = 'Z'
                {
                    pnd_Pin_With_Z.nadd(1);                                                                                                                               //Natural: ADD 1 TO #PIN-WITH-Z
                    pnd_Cwf_Misc_Fldr_Exist.setValue(true);                                                                                                               //Natural: ASSIGN #CWF-MISC-FLDR-EXIST := TRUE
                    pnd_Rqst_Log_Dte_Tme_Cwf.setValue(cwf_Master_Index_View_Rqst_Log_Dte_Tme);                                                                            //Natural: ASSIGN #RQST-LOG-DTE-TME-CWF := CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
                    pnd_Multi_Rqst_Ind.setValue(cwf_Master_Index_View_Multi_Rqst_Ind);                                                                                    //Natural: ASSIGN #MULTI-RQST-IND := CWF-MASTER-INDEX-VIEW.MULTI-RQST-IND
                    pnd_Tiaa_Rcvd_Dte_Tme_Cwf.setValue(cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte);                                                                         //Natural: ASSIGN #TIAA-RCVD-DTE-TME-CWF := CWF-MASTER-INDEX-VIEW.RQST-TIAA-RCVD-DTE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN CWF-IMAGE-XREF.CABINET-ID GT ' ' AND #START-SRCE-ID NE 'CIRS'
        else if (condition(cwf_Image_Xref_Cabinet_Id.greater(" ") && pnd_Start_Min_Pnd_Start_Srce_Id.notEquals("CIRS")))
        {
            decideConditionsMet1384++;
            pnd_Cab_5_12.setValue(cwf_Image_Xref_Cabinet_Id.getSubstring(5,8));                                                                                           //Natural: ASSIGN #CAB-5-12 := SUBSTR ( CWF-IMAGE-XREF.CABINET-ID,5,8 )
            pnd_Cab_7_7.setValue(cwf_Image_Xref_Cabinet_Id.getSubstring(7,1));                                                                                            //Natural: ASSIGN #CAB-7-7 := SUBSTR ( CWF-IMAGE-XREF.CABINET-ID,7,1 )
            pnd_Cab_5_6.setValue(cwf_Image_Xref_Cabinet_Id.getSubstring(5,3));                                                                                            //Natural: ASSIGN #CAB-5-6 := SUBSTR ( CWF-IMAGE-XREF.CABINET-ID,5,3 )
            pnd_Cab_Not_Blank.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CAB-NOT-BLANK
            short decideConditionsMet1407 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #INST-CODE IS ( N6 )
            if (condition(DbsUtil.is(cwf_Image_Xref_Pnd_Inst_Code.getText(),"N6")))
            {
                decideConditionsMet1407++;
                pnd_Inst_Sw.setValue(true);                                                                                                                               //Natural: ASSIGN #INST-SW := TRUE
                pdaEfsa6110.getEfsa6110_Parm_System().setValue("ICW");                                                                                                    //Natural: MOVE 'ICW' TO EFSA6110.PARM-SYSTEM
                pnd_Inst_Code_N.reset();                                                                                                                                  //Natural: RESET #INST-CODE-N
                pnd_Inst_Code_N.compute(new ComputeParameters(false, pnd_Inst_Code_N), cwf_Image_Xref_Pnd_Inst_Code.val());                                               //Natural: ASSIGN #INST-CODE-N := VAL ( CWF-IMAGE-XREF.#INST-CODE )
                vw_icw_Master_Index.startDatabaseRead                                                                                                                     //Natural: READ ICW-MASTER-INDEX BY INSTTN-ACTVTY-KEY = #INST-CODE
                (
                "READ02",
                new Wc[] { new Wc("INSTTN_ACTVTY_KEY", ">=", cwf_Image_Xref_Pnd_Inst_Code, WcType.BY) },
                new Oc[] { new Oc("INSTTN_ACTVTY_KEY", "ASC") }
                );
                READ02:
                while (condition(vw_icw_Master_Index.readNextRow("READ02")))
                {
                    if (condition(icw_Master_Index_Iis_Instn_Link_Cde.notEquals(pnd_Inst_Code_N)))                                                                        //Natural: IF ICW-MASTER-INDEX.IIS-INSTN-LINK-CDE NE #INST-CODE-N
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(icw_Master_Index_Rqst_Work_Prcss_Id.equals("Z")))                                                                                       //Natural: IF ICW-MASTER-INDEX.RQST-WORK-PRCSS-ID = 'Z'
                    {
                        pnd_Icwi_Misc_Fldr_Exist.setValue(true);                                                                                                          //Natural: ASSIGN #ICWI-MISC-FLDR-EXIST := TRUE
                        pnd_Iis_Hrchy_Level_Cde.setValue(icw_Master_Index_Iis_Hrchy_Level_Cde);                                                                           //Natural: ASSIGN #IIS-HRCHY-LEVEL-CDE := ICW-MASTER-INDEX.IIS-HRCHY-LEVEL-CDE
                        pnd_Rqst_Log_Dte_Tme_Icw.setValue(icw_Master_Index_Rqst_Log_Dte_Tme);                                                                             //Natural: ASSIGN #RQST-LOG-DTE-TME-ICW := ICW-MASTER-INDEX.RQST-LOG-DTE-TME
                        pnd_Tiaa_Rcvd_Dte_Tme_Icw.setValue(icw_Master_Index_Rqst_Tiaa_Rcvd_Dte);                                                                          //Natural: ASSIGN #TIAA-RCVD-DTE-TME-ICW := ICW-MASTER-INDEX.RQST-TIAA-RCVD-DTE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                DbsUtil.examine(new ExamineSource(cwf_Image_Xref_Pnd_Ppg_Code2), new ExamineSearch(" "), new ExamineGivingLength(pnd_Length));                            //Natural: EXAMINE #PPG-CODE2 FOR ' ' GIVING LENGTH #LENGTH
                if (condition(pnd_Length.greaterOrEqual(1) && pnd_Length.lessOrEqual(3)))                                                                                 //Natural: IF #LENGTH EQ 1 THRU 3
                {
                    cwf_Image_Xref_Pnd_Ppg_Cd4.setValue(cwf_Image_Xref_Pnd_Ppg_Cd4, MoveOption.RightJustified);                                                           //Natural: MOVE RIGHT #PPG-CD4 TO #PPG-CD4
                    DbsUtil.examine(new ExamineSource(cwf_Image_Xref_Pnd_Ppg_Cd4), new ExamineSearch(" "), new ExamineReplace("0"));                                      //Natural: EXAMINE #PPG-CD4 FOR ' ' REPLACE WITH '0'
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet1431 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PPG-CODE2 IS ( N6 ) OR #PPG-CODE2 = ' '
                if (condition(DbsUtil.is(cwf_Image_Xref_Pnd_Ppg_Code2.getText(),"N6") || cwf_Image_Xref_Pnd_Ppg_Code2.equals(" ")))
                {
                    decideConditionsMet1431++;
                    pnd_Ppg2_Sw.setValue(false);                                                                                                                          //Natural: ASSIGN #PPG2-SW := FALSE
                    pnd_Inst_In_Cab.nadd(1);                                                                                                                              //Natural: ADD 1 TO #INST-IN-CAB
                    if (condition(pnd_Icwi_Misc_Fldr_Exist.getBoolean()))                                                                                                 //Natural: IF #ICWI-MISC-FLDR-EXIST
                    {
                        pnd_Inst_With_Z.nadd(1);                                                                                                                          //Natural: ADD 1 TO #INST-WITH-Z
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Ppg2_Sw.setValue(true);                                                                                                                           //Natural: ASSIGN #PPG2-SW := TRUE
                    pnd_Ppg2_In_Cab.nadd(1);                                                                                                                              //Natural: ADD 1 TO #PPG2-IN-CAB
                    if (condition(pnd_Icwi_Misc_Fldr_Exist.getBoolean()))                                                                                                 //Natural: IF #ICWI-MISC-FLDR-EXIST
                    {
                        pnd_Ppg2_With_Z.nadd(1);                                                                                                                          //Natural: ADD 1 TO #PPG2-WITH-Z
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: WHEN NOT #PPG-CODE1 IS ( N4 ) AND #CAB-5-12 = ' '
            else if (condition(! ((DbsUtil.is(cwf_Image_Xref_Pnd_Ppg_Code1.getText(),"N4")) && pnd_Cab_5_12.equals(" "))))
            {
                decideConditionsMet1407++;
                pnd_Ppg1_Sw.setValue(true);                                                                                                                               //Natural: ASSIGN #PPG1-SW := TRUE
                pnd_Ppg1_In_Cab.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #PPG1-IN-CAB
                pdaEfsa6110.getEfsa6110_Parm_System().setValue("ICW");                                                                                                    //Natural: MOVE 'ICW' TO EFSA6110.PARM-SYSTEM
                vw_icw_Master_Index.startDatabaseRead                                                                                                                     //Natural: READ ICW-MASTER-INDEX BY PPG-HISTORY-KEY = #PPG-CODE1
                (
                "READ03",
                new Wc[] { new Wc("PPG_HISTORY_KEY", ">=", cwf_Image_Xref_Pnd_Ppg_Code1, WcType.BY) },
                new Oc[] { new Oc("PPG_HISTORY_KEY", "ASC") }
                );
                READ03:
                while (condition(vw_icw_Master_Index.readNextRow("READ03")))
                {
                    if (condition(icw_Master_Index_Ppg_Cde.notEquals(cwf_Image_Xref_Pnd_Ppg_Code1)))                                                                      //Natural: IF ICW-MASTER-INDEX.PPG-CDE NE CWF-IMAGE-XREF.#PPG-CODE1
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(icw_Master_Index_Rqst_Work_Prcss_Id.equals("Z")))                                                                                       //Natural: IF ICW-MASTER-INDEX.RQST-WORK-PRCSS-ID = 'Z'
                    {
                        pnd_Ppg1_With_Z.nadd(1);                                                                                                                          //Natural: ADD 1 TO #PPG1-WITH-Z
                        pnd_Icwp1_Misc_Fldr_Exist.setValue(true);                                                                                                         //Natural: ASSIGN #ICWP1-MISC-FLDR-EXIST := TRUE
                        pnd_Iis_Hrchy_Level_Cde.setValue(icw_Master_Index_Iis_Hrchy_Level_Cde);                                                                           //Natural: ASSIGN #IIS-HRCHY-LEVEL-CDE := ICW-MASTER-INDEX.IIS-HRCHY-LEVEL-CDE
                        pnd_Rqst_Log_Dte_Tme_Icw.setValue(icw_Master_Index_Rqst_Log_Dte_Tme);                                                                             //Natural: ASSIGN #RQST-LOG-DTE-TME-ICW := ICW-MASTER-INDEX.RQST-LOG-DTE-TME
                        pnd_Tiaa_Rcvd_Dte_Tme_Icw.setValue(icw_Master_Index_Rqst_Tiaa_Rcvd_Dte);                                                                          //Natural: ASSIGN #TIAA-RCVD-DTE-TME-ICW := ICW-MASTER-INDEX.RQST-TIAA-RCVD-DTE
                        pnd_Iis_Instn_Link_Cde_Icw.setValue(icw_Master_Index_Iis_Instn_Link_Cde);                                                                         //Natural: ASSIGN #IIS-INSTN-LINK-CDE-ICW := ICW-MASTER-INDEX.IIS-INSTN-LINK-CDE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: WHEN NOT #NP-PIN IS ( N7 ) AND #CAB-7-7 NE ' '
            else if (condition(! ((DbsUtil.is(cwf_Image_Xref_Pnd_Np_Pin.getText(),"N7")) && pnd_Cab_7_7.notEquals(" "))))
            {
                decideConditionsMet1407++;
                pnd_Npin_Sw.setValue(true);                                                                                                                               //Natural: ASSIGN #NPIN-SW := TRUE
                pdaEfsa6110.getEfsa6110_Parm_System().setValue("NCW");                                                                                                    //Natural: MOVE 'NCW' TO EFSA6110.PARM-SYSTEM
                pnd_Npin_In_Cab.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #NPIN-IN-CAB
                vw_ncw_Master_Index.startDatabaseRead                                                                                                                     //Natural: READ NCW-MASTER-INDEX BY NP-HISTORY-KEY = #NP-PIN
                (
                "READ04",
                new Wc[] { new Wc("NP_HISTORY_KEY", ">=", cwf_Image_Xref_Pnd_Np_Pin, WcType.BY) },
                new Oc[] { new Oc("NP_HISTORY_KEY", "ASC") }
                );
                READ04:
                while (condition(vw_ncw_Master_Index.readNextRow("READ04")))
                {
                    if (condition(ncw_Master_Index_Np_Pin.notEquals(cwf_Image_Xref_Pnd_Np_Pin)))                                                                          //Natural: IF NCW-MASTER-INDEX.NP-PIN NE CWF-IMAGE-XREF.#NP-PIN
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ncw_Master_Index_Rqst_Work_Prcss_Id.equals("Z")))                                                                                       //Natural: IF NCW-MASTER-INDEX.RQST-WORK-PRCSS-ID = 'Z'
                    {
                        pnd_Npin_With_Z.nadd(1);                                                                                                                          //Natural: ADD 1 TO #NPIN-WITH-Z
                        pnd_Ncw_Misc_Fldr_Exist.setValue(true);                                                                                                           //Natural: ASSIGN #NCW-MISC-FLDR-EXIST := TRUE
                        pnd_Rqst_Log_Dte_Tme_Ncw.setValue(ncw_Master_Index_Rqst_Log_Dte_Tme);                                                                             //Natural: ASSIGN #RQST-LOG-DTE-TME-NCW := NCW-MASTER-INDEX.RQST-LOG-DTE-TME
                        pnd_Tiaa_Rcvd_Dte_Tme_Ncw.setValue(ncw_Master_Index_Rqst_Tiaa_Rcvd_Dte);                                                                          //Natural: ASSIGN #TIAA-RCVD-DTE-TME-NCW := NCW-MASTER-INDEX.RQST-TIAA-RCVD-DTE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  SUPPRESS CNT
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Cab_Id_Nocond.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CAB-ID-NOCOND
                ignore();
                pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(2);                                                                                                   //Natural: MOVE 2 TO EFSA6110.PARM-RETURN-CODE
                pdaEfsa6110.getEfsa6110_Parm_Err_Msg().setValue("No Condition Satisfied (Non CIRS) ");                                                                    //Natural: MOVE 'No Condition Satisfied (Non CIRS) ' TO EFSA6110.PARM-ERR-MSG
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN CWF-IMAGE-XREF.CABINET-ID GT ' ' AND #START-SRCE-ID EQ 'CIRS'
        else if (condition(cwf_Image_Xref_Cabinet_Id.greater(" ") && pnd_Start_Min_Pnd_Start_Srce_Id.equals("CIRS")))
        {
            decideConditionsMet1384++;
            //*  SUPPRESS CNT
            pnd_Cab_Id_Cirs.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CAB-ID-CIRS
            pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(2);                                                                                                       //Natural: MOVE 2 TO EFSA6110.PARM-RETURN-CODE
            //*  SUPPRESS CNT
            pdaEfsa6110.getEfsa6110_Parm_Err_Msg().setValue("CIRS Cabinet-ID w/Value & PIN Zero");                                                                        //Natural: MOVE 'CIRS Cabinet-ID w/Value & PIN Zero' TO EFSA6110.PARM-ERR-MSG
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Cab_N_Pin_Blank.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CAB-N-PIN-BLANK
            ignore();
            pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(2);                                                                                                       //Natural: MOVE 2 TO EFSA6110.PARM-RETURN-CODE
            pdaEfsa6110.getEfsa6110_Parm_Err_Msg().setValue("Cabinet-ID and PIN w/out Values   ");                                                                        //Natural: MOVE 'Cabinet-ID and PIN w/out Values   ' TO EFSA6110.PARM-ERR-MSG
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Detn_Cwf_To_Add_Document_Or_Mit() throws Exception                                                                                                   //Natural: DETN-CWF-TO-ADD-DOCUMENT-OR-MIT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ====================================
        short decideConditionsMet1502 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PIN-SW
        if (condition(pnd_Pin_Sw.getBoolean()))
        {
            decideConditionsMet1502++;
            pdaEfsa9120.getEfsa9120_System().setValue("EFM");                                                                                                             //Natural: ASSIGN EFSA9120.SYSTEM := 'EFM'
            if (condition(pdaEfsa9120.getEfsa9120_Action().equals("AD")))                                                                                                 //Natural: IF EFSA9120.ACTION = 'AD'
            {
                pdaEfsa9120.getEfsa9120_Rqst_Log_Dte_Tme().getValue(1).setValue(pnd_Rqst_Log_Dte_Tme_Cwf);                                                                //Natural: ASSIGN EFSA9120.RQST-LOG-DTE-TME ( 1 ) := #RQST-LOG-DTE-TME-CWF
                pdaEfsa9120.getEfsa9120_Multi_Rqst_Ind().getValue(1).setValue(pnd_Multi_Rqst_Ind);                                                                        //Natural: ASSIGN EFSA9120.MULTI-RQST-IND ( 1 ) := #MULTI-RQST-IND
                //*  #UPLD-ENTRY-DTE
                //*  TABLE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaEfsa9120.getEfsa9120_Tiaa_Rcvd_Dte().getValue(1).setValue(pnd_Curr_Dte_Tme_Pnd_Curr_Dte);                                                              //Natural: ASSIGN EFSA9120.TIAA-RCVD-DTE ( 1 ) := #CURR-DTE
                pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue("Z");                                                                                                 //Natural: ASSIGN EFSA9120.WPID ( 1 ) := 'Z'
                pdaEfsa9120.getEfsa9120_Case_Id().getValue(1).setValue(" ");                                                                                              //Natural: ASSIGN EFSA9120.CASE-ID ( 1 ) := ' '
                pdaEfsa9120.getEfsa9120_Sub_Rqst_Cde().getValue(1).setValue(" ");                                                                                         //Natural: ASSIGN EFSA9120.SUB-RQST-CDE ( 1 ) := ' '
                pdaEfsa9120.getEfsa9120_Multi_Rqst_Ind().getValue(1).setValue("0");                                                                                       //Natural: ASSIGN EFSA9120.MULTI-RQST-IND ( 1 ) := '0'
                //*  TABLE
                //*  TABLE
                //*  NO TRANS SLIP
                //*  INTRNL RQST ('M,S')
            }                                                                                                                                                             //Natural: END-IF
            pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                       //Natural: ASSIGN EFSA9120.CABINET-PREFIX := 'P'
            pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(cwf_Image_Xref_Ph_Unique_Id_Nbr);                                                                                  //Natural: ASSIGN EFSA9120.PIN-NBR := CWF-IMAGE-XREF.PH-UNIQUE-ID-NBR
            pdaEfsa9120.getEfsa9120_Unit_Cde().getValue(1).setValue("CWF");                                                                                               //Natural: ASSIGN EFSA9120.UNIT-CDE ( 1 ) := 'CWF'
            pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).setValue("9990");                                                                                            //Natural: ASSIGN EFSA9120.STATUS-CDE ( 1 ) := '9990'
            pdaEfsa9120.getEfsa9120_Corp_Status().getValue(1).setValue("9");                                                                                              //Natural: ASSIGN EFSA9120.CORP-STATUS ( 1 ) := '9'
            pdaEfsa9120.getEfsa9120_Mj_Pull_Ind().getValue(1).setValue("I");                                                                                              //Natural: ASSIGN EFSA9120.MJ-PULL-IND ( 1 ) := 'I'
            pdaEfsa9120.getEfsa9120_Wpid_Validate_Ind().getValue(1).setValue("Y");                                                                                        //Natural: ASSIGN EFSA9120.WPID-VALIDATE-IND ( 1 ) := 'Y'
            pdaEfsa9120.getEfsa9120_Batch_Nbr().setValue(pnd_Batch_Nbr);                                                                                                  //Natural: ASSIGN EFSA9120.BATCH-NBR := #BATCH-NBR
            pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue("BATCHZAP");                                                                                             //Natural: ASSIGN EFSA9120.RQST-ENTRY-OP-CDE := 'BATCHZAP'
            pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("I");                                                                                                      //Natural: ASSIGN EFSA9120.RQST-ORIGIN-CDE := 'I'
            pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue("CWF");                                                                                               //Natural: ASSIGN EFSA9120.RQST-ORIGIN-UNIT-CDE := 'CWF'
            if (condition(pdaPsta9657.getPsta9657_Pnd_Doc_Typ().notEquals(" ")))                                                                                          //Natural: IF #DOC-TYP NE ' '
            {
                pdaEfsa9120.getEfsa9120_Doc_Category().setValue(pdaPsta9657.getPsta9657_Pnd_Category());                                                                  //Natural: ASSIGN EFSA9120.DOC-CATEGORY := #CATEGORY
                pdaEfsa9120.getEfsa9120_Doc_Class().setValue(pdaPsta9657.getPsta9657_Pnd_Class());                                                                        //Natural: ASSIGN EFSA9120.DOC-CLASS := #CLASS
                pdaEfsa9120.getEfsa9120_Doc_Specific().setValue(pdaPsta9657.getPsta9657_Pnd_Specific());                                                                  //Natural: ASSIGN EFSA9120.DOC-SPECIFIC := #SPECIFIC
                pnd_Dcmt_Type.setValue(pdaPsta9657.getPsta9657_Pnd_Doc_Typ());                                                                                            //Natural: ASSIGN #DCMT-TYPE := #DOC-TYP
                //*  ?
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaEfsa9120.getEfsa9120_Doc_Category().setValue("Z");                                                                                                     //Natural: ASSIGN EFSA9120.DOC-CATEGORY := 'Z'
                pdaEfsa9120.getEfsa9120_Doc_Class().setValue("IMG");                                                                                                      //Natural: ASSIGN EFSA9120.DOC-CLASS := 'IMG'
                pdaEfsa9120.getEfsa9120_Doc_Specific().setValue("UNK");                                                                                                   //Natural: ASSIGN EFSA9120.DOC-SPECIFIC := 'UNK'
                pnd_Dcmt_Type.setValue("ZIMGUNK");                                                                                                                        //Natural: ASSIGN #DCMT-TYPE := 'ZIMGUNK'
            }                                                                                                                                                             //Natural: END-IF
            //*  IMAGE
            DbsUtil.callnat(Cwfn5432.class , getCurrentProcessState(), pnd_Dcmt_Type, pdaEfsa9120.getEfsa9120_Wpid().getValue(1), pdaEfsa9120.getEfsa9120_Doc_Direction(),  //Natural: CALLNAT 'CWFN5432' #DCMT-TYPE EFSA9120.WPID ( 1 ) EFSA9120.DOC-DIRECTION EFSA9120.DOC-RETENTION-CDE EFSA9120.DOC-SECURITY-CDE
                pdaEfsa9120.getEfsa9120_Doc_Retention_Cde(), pdaEfsa9120.getEfsa9120_Doc_Security_Cde());
            if (condition(Global.isEscape())) return;
            pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue("I");                                                                                                       //Natural: ASSIGN EFSA9120.DOC-FORMAT-CDE := 'I'
            pdaEfsa9120.getEfsa9120_Mail_Item_Nbr().setValue(cwf_Image_Xref_Mail_Item_Nbr);                                                                               //Natural: ASSIGN EFSA9120.MAIL-ITEM-NBR := CWF-IMAGE-XREF.MAIL-ITEM-NBR
            pdaEfsa9120.getEfsa9120_Page_Nbr().setValue(1);                                                                                                               //Natural: ASSIGN EFSA9120.PAGE-NBR := 1
            pdaEfsa9120.getEfsa9120_Image_Source_Id().setValue(cwf_Image_Xref_Source_Id);                                                                                 //Natural: ASSIGN EFSA9120.IMAGE-SOURCE-ID := CWF-IMAGE-XREF.SOURCE-ID
            pdaEfsa9120.getEfsa9120_Dgtze_Doc_End_Page().setValue(cwf_Image_Xref_Number_Of_Pages);                                                                        //Natural: ASSIGN EFSA9120.DGTZE-DOC-END-PAGE := CWF-IMAGE-XREF.NUMBER-OF-PAGES
            DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
            if (condition(Global.isEscape())) return;
            pdaEfsa6110.getEfsa6110_Parm_System().setValue("CWF");                                                                                                        //Natural: MOVE 'CWF' TO EFSA6110.PARM-SYSTEM
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().equals(" ")))                                                                                         //Natural: IF ##MSG EQ ' '
            {
                pnd_Action_Cde.setValue(pdaEfsa9120.getEfsa9120_Action());                                                                                                //Natural: ASSIGN #ACTION-CDE := EFSA9120.ACTION
                pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(1);                                                                                                   //Natural: MOVE 1 TO EFSA6110.PARM-RETURN-CODE
                //*      MOVE 'CWF' TO EFSA6110.PARM-SYSTEM
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  SUPPRESS
                pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(3);                                                                                                   //Natural: MOVE 3 TO EFSA6110.PARM-RETURN-CODE
                pdaEfsa6110.getEfsa6110_Parm_Err_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                               //Natural: MOVE ##MSG TO EFSA6110.PARM-ERR-MSG
                //*      COMPRESS  'EFSN9120 - ' ##MSG INTO EFSA6110.PARM-ERR-MSG
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #INST-SW OR #PPG1-SW OR #PPG2-SW
        else if (condition(pnd_Inst_Sw.getBoolean() || pnd_Ppg1_Sw.getBoolean() || pnd_Ppg2_Sw.getBoolean()))
        {
            decideConditionsMet1502++;
            pdaIefa9000.getIefa9000_System().setValue("IEFM");                                                                                                            //Natural: ASSIGN IEFA9000.SYSTEM := 'IEFM'
            if (condition(pnd_Inst_Sw.getBoolean() || pnd_Ppg2_Sw.getBoolean()))                                                                                          //Natural: IF #INST-SW OR #PPG2-SW
            {
                pdaIefa9000.getIefa9000_Iis_Hrchy_Key_Cde().getValue(1).compute(new ComputeParameters(false, pdaIefa9000.getIefa9000_Iis_Hrchy_Key_Cde().getValue(1)),    //Natural: ASSIGN IEFA9000.IIS-HRCHY-KEY-CDE ( 1 ) := VAL ( #INST-CODE )
                    cwf_Image_Xref_Pnd_Inst_Code.val());
                pdaIefa9000.getIefa9000_Ppg_Cde().getValue(1).setValue(cwf_Image_Xref_Pnd_Ppg_Code2);                                                                     //Natural: ASSIGN IEFA9000.PPG-CDE ( 1 ) := #PPG-CODE2
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ppg1_Sw.getBoolean()))                                                                                                                      //Natural: IF #PPG1-SW
            {
                pdaIefa9000.getIefa9000_Iis_Hrchy_Key_Cde().getValue(1).setValue(pnd_Iis_Instn_Link_Cde_Icw);                                                             //Natural: ASSIGN IEFA9000.IIS-HRCHY-KEY-CDE ( 1 ) := #IIS-INSTN-LINK-CDE-ICW
                pdaIefa9000.getIefa9000_Ppg_Cde().getValue(1).setValue(cwf_Image_Xref_Pnd_Ppg_Code1);                                                                     //Natural: ASSIGN IEFA9000.PPG-CDE ( 1 ) := #PPG-CODE1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaIefa9000.getIefa9000_Action().equals("AD")))                                                                                                 //Natural: IF IEFA9000.ACTION = 'AD'
            {
                pdaIefa9000.getIefa9000_Rqst_Log_Dte_Tme().getValue(1).setValue(pnd_Rqst_Log_Dte_Tme_Icw);                                                                //Natural: ASSIGN IEFA9000.RQST-LOG-DTE-TME ( 1 ) := #RQST-LOG-DTE-TME-ICW
                pdaIefa9000.getIefa9000_Iis_Hrchy_Level_Cde().getValue(1).setValue(pnd_Iis_Hrchy_Level_Cde);                                                              //Natural: ASSIGN IEFA9000.IIS-HRCHY-LEVEL-CDE ( 1 ) := #IIS-HRCHY-LEVEL-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaIefa9000.getIefa9000_Rqst_Log_Dte_Tme().getValue(1).setValue(pnd_Curr_Dte_Tme);                                                                        //Natural: ASSIGN IEFA9000.RQST-LOG-DTE-TME ( 1 ) := IEFA9000.TIAA-RCVD-DTE-TME ( 1 ) := IEFA9000.CNTCT-DTE-TME := #CURR-DTE-TME
                pdaIefa9000.getIefa9000_Tiaa_Rcvd_Dte_Tme().getValue(1).setValue(pnd_Curr_Dte_Tme);
                pdaIefa9000.getIefa9000_Cntct_Dte_Tme().setValue(pnd_Curr_Dte_Tme);
                short decideConditionsMet1582 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PPG1-SW OR #PPG2-SW
                if (condition(pnd_Ppg1_Sw.getBoolean() || pnd_Ppg2_Sw.getBoolean()))
                {
                    decideConditionsMet1582++;
                    pdaIefa9000.getIefa9000_Iis_Hrchy_Level_Cde().getValue(1).setValue("G");                                                                              //Natural: ASSIGN IEFA9000.IIS-HRCHY-LEVEL-CDE ( 1 ) := 'G'
                }                                                                                                                                                         //Natural: WHEN #INST-SW
                else if (condition(pnd_Inst_Sw.getBoolean()))
                {
                    decideConditionsMet1582++;
                    pdaIefa9000.getIefa9000_Iis_Hrchy_Level_Cde().getValue(1).setValue("I");                                                                              //Natural: ASSIGN IEFA9000.IIS-HRCHY-LEVEL-CDE ( 1 ) := 'I'
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            pdaIefa9000.getIefa9000_Work_Prcss_Id().getValue(1).setValue("Z");                                                                                            //Natural: ASSIGN IEFA9000.WORK-PRCSS-ID ( 1 ) := 'Z'
            pdaIefa9000.getIefa9000_Crprte_Status_Ind().getValue(1).setValue("9");                                                                                        //Natural: ASSIGN IEFA9000.CRPRTE-STATUS-IND ( 1 ) := '9'
            pdaIefa9000.getIefa9000_Unit_Cde().getValue(1).setValue("CWF");                                                                                               //Natural: ASSIGN IEFA9000.UNIT-CDE ( 1 ) := 'CWF'
            pdaIefa9000.getIefa9000_Oprtr_Cde().setValue("BATCHZAP");                                                                                                     //Natural: ASSIGN IEFA9000.OPRTR-CDE := 'BATCHZAP'
            pdaIefa9000.getIefa9000_Rqst_Orgn_Cde().setValue("I");                                                                                                        //Natural: ASSIGN IEFA9000.RQST-ORGN-CDE := 'I'
            pdaIefa9000.getIefa9000_Oprtr_Unit_Cde().setValue("CWF");                                                                                                     //Natural: ASSIGN IEFA9000.OPRTR-UNIT-CDE := 'CWF'
            if (condition(pdaPsta9657.getPsta9657_Pnd_Doc_Typ().notEquals(" ")))                                                                                          //Natural: IF #DOC-TYP NE ' '
            {
                pdaIefa9000.getIefa9000_Dcmnt_Ctgry().setValue(pdaPsta9657.getPsta9657_Pnd_Category());                                                                   //Natural: ASSIGN IEFA9000.DCMNT-CTGRY := #CATEGORY
                pdaIefa9000.getIefa9000_Dcmnt_Sub_Ctgry().setValue(pdaPsta9657.getPsta9657_Pnd_Class());                                                                  //Natural: ASSIGN IEFA9000.DCMNT-SUB-CTGRY := #CLASS
                pdaIefa9000.getIefa9000_Dcmnt_Dtl().setValue(pdaPsta9657.getPsta9657_Pnd_Specific());                                                                     //Natural: ASSIGN IEFA9000.DCMNT-DTL := #SPECIFIC
                pnd_Dcmt_Type.setValue(pdaPsta9657.getPsta9657_Pnd_Doc_Typ());                                                                                            //Natural: ASSIGN #DCMT-TYPE := #DOC-TYP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaIefa9000.getIefa9000_Dcmnt_Ctgry().setValue("Z");                                                                                                      //Natural: ASSIGN IEFA9000.DCMNT-CTGRY := 'Z'
                pdaIefa9000.getIefa9000_Dcmnt_Sub_Ctgry().setValue("IMG");                                                                                                //Natural: ASSIGN IEFA9000.DCMNT-SUB-CTGRY := 'IMG'
                pdaIefa9000.getIefa9000_Dcmnt_Dtl().setValue("UNK");                                                                                                      //Natural: ASSIGN IEFA9000.DCMNT-DTL := 'UNK'
                pnd_Dcmt_Type.setValue("ZIMGUNK");                                                                                                                        //Natural: ASSIGN #DCMT-TYPE := 'ZIMGUNK'
                //*  IMAGE
            }                                                                                                                                                             //Natural: END-IF
            pdaIefa9000.getIefa9000_Dcmnt_Frmt().setValue("I");                                                                                                           //Natural: ASSIGN IEFA9000.DCMNT-FRMT := 'I'
            pdaIefa9000.getIefa9000_Dcmnt_Anttn_Ind().setValue(" ");                                                                                                      //Natural: ASSIGN IEFA9000.DCMNT-ANTTN-IND := ' '
            pdaIefa9000.getIefa9000_Dcmnt_Copy_Ind().setValue(" ");                                                                                                       //Natural: ASSIGN IEFA9000.DCMNT-COPY-IND := ' '
            pdaIefa9000.getIefa9000_Dcmnt_Drctn().reset();                                                                                                                //Natural: RESET IEFA9000.DCMNT-DRCTN IEFA9000.DCMNT-RTNTN IEFA9000.DCMNT-SCRTY-IND
            pdaIefa9000.getIefa9000_Dcmnt_Rtntn().reset();
            pdaIefa9000.getIefa9000_Dcmnt_Scrty_Ind().reset();
            //*  GET DOCUMENT ATTRIBUTES
            DbsUtil.callnat(Cwfn5432.class , getCurrentProcessState(), pnd_Dcmt_Type, pdaIefa9000.getIefa9000_Work_Prcss_Id().getValue(1), pdaIefa9000.getIefa9000_Dcmnt_Drctn(),  //Natural: CALLNAT 'CWFN5432' #DCMT-TYPE IEFA9000.WORK-PRCSS-ID ( 1 ) IEFA9000.DCMNT-DRCTN IEFA9000.DCMNT-RTNTN IEFA9000.DCMNT-SCRTY-IND
                pdaIefa9000.getIefa9000_Dcmnt_Rtntn(), pdaIefa9000.getIefa9000_Dcmnt_Scrty_Ind());
            if (condition(Global.isEscape())) return;
            //*  INCOMING
            if (condition(pdaIefa9000.getIefa9000_Dcmnt_Drctn().equals(" ")))                                                                                             //Natural: IF IEFA9000.DCMNT-DRCTN EQ ' '
            {
                pdaIefa9000.getIefa9000_Dcmnt_Drctn().setValue("I");                                                                                                      //Natural: ASSIGN IEFA9000.DCMNT-DRCTN := 'I'
                //*  LANDSCAPE
            }                                                                                                                                                             //Natural: END-IF
            pdaIefa9000.getIefa9000_Image_Source_Id().setValue(cwf_Image_Xref_Source_Id);                                                                                 //Natural: ASSIGN IEFA9000.IMAGE-SOURCE-ID := CWF-IMAGE-XREF.SOURCE-ID
            pdaIefa9000.getIefa9000_Mail_Item_Nbr().setValue(cwf_Image_Xref_Mail_Item_Nbr);                                                                               //Natural: ASSIGN IEFA9000.MAIL-ITEM-NBR := CWF-IMAGE-XREF.MAIL-ITEM-NBR
            pdaIefa9000.getIefa9000_Image_Ornttn().setValue("L");                                                                                                         //Natural: ASSIGN IEFA9000.IMAGE-ORNTTN := 'L'
            pdaIefa9000.getIefa9000_Start_Page_Nbr().setValue(1);                                                                                                         //Natural: ASSIGN IEFA9000.START-PAGE-NBR := 1
            pdaIefa9000.getIefa9000_Image_End_Page().setValue(cwf_Image_Xref_Number_Of_Pages);                                                                            //Natural: ASSIGN IEFA9000.IMAGE-END-PAGE := CWF-IMAGE-XREF.NUMBER-OF-PAGES
            DbsUtil.callnat(Iefn9000.class , getCurrentProcessState(), pdaIefa9000.getIefa9000(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),                //Natural: CALLNAT 'IEFN9000' IEFA9000 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
            if (condition(Global.isEscape())) return;
            pdaEfsa6110.getEfsa6110_Parm_System().setValue("ICW");                                                                                                        //Natural: MOVE 'ICW' TO EFSA6110.PARM-SYSTEM
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().equals(" ")))                                                                                         //Natural: IF ##MSG EQ ' '
            {
                pnd_Action_Cde.setValue(pdaIefa9000.getIefa9000_Action());                                                                                                //Natural: ASSIGN #ACTION-CDE := IEFA9000.ACTION
                pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(1);                                                                                                   //Natural: MOVE 1 TO EFSA6110.PARM-RETURN-CODE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(3);                                                                                                   //Natural: MOVE 3 TO EFSA6110.PARM-RETURN-CODE
                pdaEfsa6110.getEfsa6110_Parm_Err_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                               //Natural: MOVE ##MSG TO EFSA6110.PARM-ERR-MSG
                //*      COMPRESS  'IEFN9000 - ' ##MSG INTO EFSA6110.PARM-ERR-MSG
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #NPIN-SW
        else if (condition(pnd_Npin_Sw.getBoolean()))
        {
            decideConditionsMet1502++;
            pdaNefa9000.getNefa9000_System().setValue("BATCHCWF");                                                                                                        //Natural: ASSIGN NEFA9000.SYSTEM := 'BATCHCWF'
            pdaNefa9000.getNefa9000_Np_Pin().setValue(cwf_Image_Xref_Cabinet_Id.getSubstring(1,7));                                                                       //Natural: ASSIGN NEFA9000.NP-PIN := SUBSTR ( CWF-IMAGE-XREF.CABINET-ID,1,7 )
            if (condition(pdaNefa9000.getNefa9000_Action().equals("AD")))                                                                                                 //Natural: IF NEFA9000.ACTION = 'AD'
            {
                pdaNefa9000.getNefa9000_Rqst_Log_Dte_Tme().getValue(1).setValue(pnd_Rqst_Log_Dte_Tme_Ncw);                                                                //Natural: ASSIGN NEFA9000.RQST-LOG-DTE-TME ( 1 ) := #RQST-LOG-DTE-TME-NCW
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaNefa9000.getNefa9000_Rqst_Log_Dte_Tme().getValue(1).setValue(pnd_Curr_Dte_Tme);                                                                        //Natural: ASSIGN NEFA9000.RQST-LOG-DTE-TME ( 1 ) := NEFA9000.TIAA-RCVD-DTE-TME ( 1 ) := NEFA9000.CNTCT-DTE-TME := #CURR-DTE-TME
                pdaNefa9000.getNefa9000_Tiaa_Rcvd_Dte_Tme().getValue(1).setValue(pnd_Curr_Dte_Tme);
                pdaNefa9000.getNefa9000_Cntct_Dte_Tme().setValue(pnd_Curr_Dte_Tme);
            }                                                                                                                                                             //Natural: END-IF
            pdaNefa9000.getNefa9000_Work_Prcss_Id().getValue(1).setValue("Z");                                                                                            //Natural: ASSIGN NEFA9000.WORK-PRCSS-ID ( 1 ) := 'Z'
            pdaNefa9000.getNefa9000_Crprte_Status_Ind().getValue(1).setValue("9");                                                                                        //Natural: ASSIGN NEFA9000.CRPRTE-STATUS-IND ( 1 ) := '9'
            pdaNefa9000.getNefa9000_Status_Cde().getValue(1).setValue("9990");                                                                                            //Natural: ASSIGN NEFA9000.STATUS-CDE ( 1 ) := '9990'
            pdaNefa9000.getNefa9000_Unit_Cde().getValue(1).setValue("CWF");                                                                                               //Natural: ASSIGN NEFA9000.UNIT-CDE ( 1 ) := 'CWF'
            pdaNefa9000.getNefa9000_Oprtr_Cde().setValue("BATCHZAP");                                                                                                     //Natural: ASSIGN NEFA9000.OPRTR-CDE := 'BATCHZAP'
            pdaNefa9000.getNefa9000_Oprtr_Unit_Cde().setValue("CWF");                                                                                                     //Natural: ASSIGN NEFA9000.OPRTR-UNIT-CDE := 'CWF'
            if (condition(pdaPsta9657.getPsta9657_Pnd_Doc_Typ().notEquals(" ")))                                                                                          //Natural: IF #DOC-TYP NE ' '
            {
                pdaNefa9000.getNefa9000_Dcmnt_Ctgry().setValue(pdaPsta9657.getPsta9657_Pnd_Category());                                                                   //Natural: ASSIGN NEFA9000.DCMNT-CTGRY := #CATEGORY
                pdaNefa9000.getNefa9000_Dcmnt_Sub_Ctgry().setValue(pdaPsta9657.getPsta9657_Pnd_Class());                                                                  //Natural: ASSIGN NEFA9000.DCMNT-SUB-CTGRY := #CLASS
                pdaNefa9000.getNefa9000_Dcmnt_Dtl().setValue(pdaPsta9657.getPsta9657_Pnd_Specific());                                                                     //Natural: ASSIGN NEFA9000.DCMNT-DTL := #SPECIFIC
                pnd_Dcmt_Type.setValue(pdaPsta9657.getPsta9657_Pnd_Doc_Typ());                                                                                            //Natural: ASSIGN #DCMT-TYPE := #DOC-TYP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaNefa9000.getNefa9000_Dcmnt_Ctgry().setValue("Z");                                                                                                      //Natural: ASSIGN NEFA9000.DCMNT-CTGRY := 'Z'
                pdaNefa9000.getNefa9000_Dcmnt_Sub_Ctgry().setValue("IMG");                                                                                                //Natural: ASSIGN NEFA9000.DCMNT-SUB-CTGRY := 'IMG'
                pdaNefa9000.getNefa9000_Dcmnt_Dtl().setValue("UNK");                                                                                                      //Natural: ASSIGN NEFA9000.DCMNT-DTL := 'UNK'
                pnd_Dcmt_Type.setValue("ZIMGUNK");                                                                                                                        //Natural: ASSIGN #DCMT-TYPE := 'ZIMGUNK'
                //*  IMAGE
            }                                                                                                                                                             //Natural: END-IF
            pdaNefa9000.getNefa9000_Rqst_Orgn_Cde().setValue("I");                                                                                                        //Natural: ASSIGN NEFA9000.RQST-ORGN-CDE := 'I'
            pdaNefa9000.getNefa9000_Dcmnt_Frmt().setValue("I");                                                                                                           //Natural: ASSIGN NEFA9000.DCMNT-FRMT := 'I'
            pdaNefa9000.getNefa9000_Dcmnt_Anttn_Ind().setValue(" ");                                                                                                      //Natural: ASSIGN NEFA9000.DCMNT-ANTTN-IND := ' '
            pdaNefa9000.getNefa9000_Dcmnt_Copy_Ind().setValue(" ");                                                                                                       //Natural: ASSIGN NEFA9000.DCMNT-COPY-IND := ' '
            pdaNefa9000.getNefa9000_Dcmnt_Drctn().reset();                                                                                                                //Natural: RESET NEFA9000.DCMNT-DRCTN NEFA9000.DCMNT-RTNTN NEFA9000.DCMNT-SCRTY-IND
            pdaNefa9000.getNefa9000_Dcmnt_Rtntn().reset();
            pdaNefa9000.getNefa9000_Dcmnt_Scrty_Ind().reset();
            //*  GET DOCUMENT ATTRIBUTES
            DbsUtil.callnat(Cwfn5432.class , getCurrentProcessState(), pnd_Dcmt_Type, pdaNefa9000.getNefa9000_Work_Prcss_Id().getValue(1), pdaNefa9000.getNefa9000_Dcmnt_Drctn(),  //Natural: CALLNAT 'CWFN5432' #DCMT-TYPE NEFA9000.WORK-PRCSS-ID ( 1 ) NEFA9000.DCMNT-DRCTN NEFA9000.DCMNT-RTNTN NEFA9000.DCMNT-SCRTY-IND
                pdaNefa9000.getNefa9000_Dcmnt_Rtntn(), pdaNefa9000.getNefa9000_Dcmnt_Scrty_Ind());
            if (condition(Global.isEscape())) return;
            //*  INCOMING
            if (condition(pdaNefa9000.getNefa9000_Dcmnt_Drctn().equals(" ")))                                                                                             //Natural: IF NEFA9000.DCMNT-DRCTN EQ ' '
            {
                pdaNefa9000.getNefa9000_Dcmnt_Drctn().setValue("I");                                                                                                      //Natural: ASSIGN NEFA9000.DCMNT-DRCTN := 'I'
            }                                                                                                                                                             //Natural: END-IF
            //*  PENDING
            if (condition(pdaNefa9000.getNefa9000_Dcmnt_Rtntn().equals(" ")))                                                                                             //Natural: IF NEFA9000.DCMNT-RTNTN EQ ' '
            {
                pdaNefa9000.getNefa9000_Dcmnt_Rtntn().setValue("P");                                                                                                      //Natural: ASSIGN NEFA9000.DCMNT-RTNTN := 'P'
                //*  LANDSCAPE
            }                                                                                                                                                             //Natural: END-IF
            pdaNefa9000.getNefa9000_Image_Source_Id().setValue(cwf_Image_Xref_Source_Id);                                                                                 //Natural: ASSIGN NEFA9000.IMAGE-SOURCE-ID := CWF-IMAGE-XREF.SOURCE-ID
            pdaNefa9000.getNefa9000_Mail_Item_Nbr().setValue(cwf_Image_Xref_Mail_Item_Nbr);                                                                               //Natural: ASSIGN NEFA9000.MAIL-ITEM-NBR := CWF-IMAGE-XREF.MAIL-ITEM-NBR
            pdaNefa9000.getNefa9000_Image_Ornttn().setValue("L");                                                                                                         //Natural: ASSIGN NEFA9000.IMAGE-ORNTTN := 'L'
            pdaNefa9000.getNefa9000_Start_Page_Nbr().setValue(1);                                                                                                         //Natural: ASSIGN NEFA9000.START-PAGE-NBR := 1
            pdaNefa9000.getNefa9000_Image_End_Page().setValue(cwf_Image_Xref_Number_Of_Pages);                                                                            //Natural: ASSIGN NEFA9000.IMAGE-END-PAGE := CWF-IMAGE-XREF.NUMBER-OF-PAGES
            DbsUtil.callnat(Nefn9000.class , getCurrentProcessState(), pdaNefa9000.getNefa9000(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),                //Natural: CALLNAT 'NEFN9000' NEFA9000 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
            if (condition(Global.isEscape())) return;
            pdaEfsa6110.getEfsa6110_Parm_System().setValue("NCW");                                                                                                        //Natural: MOVE 'NCW' TO EFSA6110.PARM-SYSTEM
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().equals(" ")))                                                                                         //Natural: IF ##MSG EQ ' '
            {
                pnd_Action_Cde.setValue(pdaNefa9000.getNefa9000_Action());                                                                                                //Natural: ASSIGN #ACTION-CDE := NEFA9000.ACTION
                pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(1);                                                                                                   //Natural: MOVE 1 TO EFSA6110.PARM-RETURN-CODE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaEfsa6110.getEfsa6110_Parm_Return_Code().setValue(3);                                                                                                   //Natural: MOVE 3 TO EFSA6110.PARM-RETURN-CODE
                pdaEfsa6110.getEfsa6110_Parm_Err_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                               //Natural: MOVE ##MSG TO EFSA6110.PARM-ERR-MSG
                //*      COMPRESS  'NEFN9000 - ' ##MSG INTO EFSA6110.PARM-ERR-MSG
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Write_Cwf_Document() throws Exception                                                                                                                //Natural: WRITE-CWF-DOCUMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  =======================
        vw_cwf_Efm_Document.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) CWF-EFM-DOCUMENT WITH IMAGE-POINTER-KEY = #PARM-KEY-VALUE
        (
        "FIND01",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Parm_Key_Value, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_cwf_Efm_Document.readNextRow("FIND01", true)))
        {
            vw_cwf_Efm_Document.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Efm_Document.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  DELETED
        if (condition(cwf_Efm_Document_Document_Retention_Ind.equals("D")))                                                                                               //Natural: IF CWF-EFM-DOCUMENT.DOCUMENT-RETENTION-IND = 'D'
        {
            //*  DO NOT REPORT ON DELETED MINS.
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Min_Save.setValue(pnd_Parm_Key_Value_Pnd_Parm_Key_Min_N);                                                                                                     //Natural: ASSIGN #MIN-SAVE := #PARM-KEY-MIN-N
        pnd_Image_Source_Id.setValue(cwf_Efm_Document_Image_Source_Id);                                                                                                   //Natural: ASSIGN #IMAGE-SOURCE-ID := CWF-EFM-DOCUMENT.IMAGE-SOURCE-ID
        pnd_Cabinet_Id.setValue(cwf_Efm_Document_Cabinet_Id);                                                                                                             //Natural: ASSIGN #CABINET-ID := CWF-EFM-DOCUMENT.CABINET-ID
        pnd_Tiaa_Rcvd_Dte.setValue(cwf_Efm_Document_Tiaa_Rcvd_Dte);                                                                                                       //Natural: ASSIGN #TIAA-RCVD-DTE := CWF-EFM-DOCUMENT.TIAA-RCVD-DTE
        pnd_Case_Workprcss_Multi_Subrqst.setValue(cwf_Efm_Document_Case_Workprcss_Multi_Subrqst);                                                                         //Natural: ASSIGN #CASE-WORKPRCSS-MULTI-SUBRQST := CWF-EFM-DOCUMENT.CASE-WORKPRCSS-MULTI-SUBRQST
        pnd_Entry_Dte_Tme.setValue(cwf_Efm_Document_Entry_Dte_Tme);                                                                                                       //Natural: ASSIGN #ENTRY-DTE-TME := CWF-EFM-DOCUMENT.ENTRY-DTE-TME
        pnd_Rqst_Id_Key_Pnd_Rqst_Work_Prcss_Id.setValue(cwf_Efm_Document_Originating_Work_Prcss_Id);                                                                      //Natural: ASSIGN #RQST-WORK-PRCSS-ID := CWF-EFM-DOCUMENT.ORIGINATING-WORK-PRCSS-ID
        pnd_Rqst_Id_Key_Pnd_Rqst_Tiaa_Rcvd_Dte.setValueEdited(cwf_Efm_Document_Tiaa_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED CWF-EFM-DOCUMENT.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO #RQST-TIAA-RCVD-DTE
        pnd_Rqst_Id_Key_Pnd_Rqst_Case_Id.setValue(cwf_Efm_Document_Case_Ind);                                                                                             //Natural: ASSIGN #RQST-CASE-ID := CWF-EFM-DOCUMENT.CASE-IND
        pnd_Rqst_Id_Key_Pnd_Rqst_Sub_Rqst_Cde.setValue(cwf_Efm_Document_Sub_Rqst_Ind);                                                                                    //Natural: ASSIGN #RQST-SUB-RQST-CDE := CWF-EFM-DOCUMENT.SUB-RQST-IND
        pnd_Rqst_Id_Key_Pnd_Rqst_Pin_Nbr.setValue(cwf_Efm_Document_Pin_Nbr);                                                                                              //Natural: ASSIGN #RQST-PIN-NBR := CWF-EFM-DOCUMENT.PIN-NBR
        pnd_Rqst_Id_Key_Pnd_Multi_Rqst_Id.setValue(cwf_Efm_Document_Multi_Rqst_Ind);                                                                                      //Natural: ASSIGN #MULTI-RQST-ID := CWF-EFM-DOCUMENT.MULTI-RQST-IND
        pnd_Create_Unit.setValue(cwf_Efm_Document_Entry_System_Or_Unit);                                                                                                  //Natural: ASSIGN #CREATE-UNIT := CWF-EFM-DOCUMENT.ENTRY-SYSTEM-OR-UNIT
        vw_cwf_Master_Index_View.startDatabaseFind                                                                                                                        //Natural: FIND ( 1 ) CWF-MASTER-INDEX-VIEW WITH RQST-ID-KEY = #RQST-ID-KEY
        (
        "FIND02",
        new Wc[] { new Wc("RQST_ID_KEY", "=", pnd_Rqst_Id_Key, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(vw_cwf_Master_Index_View.readNextRow("FIND02", true)))
        {
            vw_cwf_Master_Index_View.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Master_Index_View.getAstCOUNTER().equals(0)))                                                                                            //Natural: IF NO RECORD FOUND
            {
                vw_cwf_Master_Index_View.reset();                                                                                                                         //Natural: RESET CWF-MASTER-INDEX-VIEW
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Curr_Unit.setValue(cwf_Master_Index_View_Admin_Unit_Cde);                                                                                                 //Natural: ASSIGN #CURR-UNIT := CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(cwf_Master_Index_View_Rqst_Log_Dte_Tme.greater(" ")))                                                                                               //Natural: IF CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME GT ' '
        {
            //*  DETERMINE INITIAL DESTINATION UNIT
            pnd_Rqst_Routing_Key.reset();                                                                                                                                 //Natural: RESET #RQST-ROUTING-KEY
            pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme.setValue(cwf_Master_Index_View_Rqst_Log_Dte_Tme);                                                                   //Natural: ASSIGN #RQST-ROUTING-KEY.#RQST-LOG-DTE-TME := CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
            vw_cwf_Mit_History.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CWF-MIT-HISTORY BY RQST-ROUTING-KEY = #RQST-ROUTING-KEY
            (
            "READ05",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Rqst_Routing_Key, WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
            1
            );
            READ05:
            while (condition(vw_cwf_Mit_History.readNextRow("READ05")))
            {
                pnd_Init_Unit.setValue(cwf_Mit_History_Admin_Unit_Cde);                                                                                                   //Natural: ASSIGN #INIT-UNIT := CWF-MIT-HISTORY.ADMIN-UNIT-CDE
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE(DAT) NOTITLE NOHDR USING FORM 'EFSF8708'
    }
    private void sub_Write_Icw_Document() throws Exception                                                                                                                //Natural: WRITE-ICW-DOCUMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ==================================
        vw_icw_Efm_Document.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) ICW-EFM-DOCUMENT WITH IMAGE-POINTER-KEY = #PARM-KEY-VALUE
        (
        "FIND03",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Parm_Key_Value, WcType.WITH) },
        1
        );
        FIND03:
        while (condition(vw_icw_Efm_Document.readNextRow("FIND03", true)))
        {
            vw_icw_Efm_Document.setIfNotFoundControlFlag(false);
            if (condition(vw_icw_Efm_Document.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  DELETED
        if (condition(icw_Efm_Document_Dcmnt_Rtntn.equals("D")))                                                                                                          //Natural: IF ICW-EFM-DOCUMENT.DCMNT-RTNTN = 'D'
        {
            //*  DO NOT REPORT ON DELETED MINS.
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  INST & PPG-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Min_Save.setValue(pnd_Parm_Key_Value_Pnd_Parm_Key_Min_N);                                                                                                     //Natural: ASSIGN #MIN-SAVE := #PARM-KEY-MIN-N
        pnd_Image_Source_Id.setValue(icw_Efm_Document_Image_Source_Id);                                                                                                   //Natural: ASSIGN #IMAGE-SOURCE-ID := ICW-EFM-DOCUMENT.IMAGE-SOURCE-ID
        pnd_Cabinet_Id.setValue(icw_Efm_Document_Cabinet_Id);                                                                                                             //Natural: ASSIGN #CABINET-ID := ICW-EFM-DOCUMENT.CABINET-ID
        pnd_Entry_Dte_Tme.setValue(icw_Efm_Document_Crte_Dte_Tme);                                                                                                        //Natural: ASSIGN #ENTRY-DTE-TME := ICW-EFM-DOCUMENT.CRTE-DTE-TME
        pnd_Create_Unit.setValue(icw_Efm_Document_Crte_Unit);                                                                                                             //Natural: ASSIGN #CREATE-UNIT := ICW-EFM-DOCUMENT.CRTE-UNIT
        vw_icw_Master_Index.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) ICW-MASTER-INDEX WITH ACTV-UNQUE-KEY = ICW-EFM-DOCUMENT.RQST-LOG-DTE-TME
        (
        "FIND04",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", "=", icw_Efm_Document_Rqst_Log_Dte_Tme, WcType.WITH) },
        1
        );
        FIND04:
        while (condition(vw_icw_Master_Index.readNextRow("FIND04")))
        {
            vw_icw_Master_Index.setIfNotFoundControlFlag(false);
            pnd_Tiaa_Rcvd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),icw_Master_Index_Rqst_Tiaa_Rcvd_Dte);                                                         //Natural: MOVE EDITED ICW-MASTER-INDEX.RQST-TIAA-RCVD-DTE TO #TIAA-RCVD-DTE ( EM = YYYYMMDD )
            pnd_Case_Workprcss_Multi_Subrqst_Pnd_Case_Id.setValue(icw_Master_Index_Pnd_Case_Id_Cde);                                                                      //Natural: ASSIGN #CASE-ID := ICW-MASTER-INDEX.#CASE-ID-CDE
            pnd_Case_Workprcss_Multi_Subrqst_Pnd_Work_Prcss_Id.setValue(icw_Master_Index_Rqst_Work_Prcss_Id);                                                             //Natural: ASSIGN #WORK-PRCSS-ID := ICW-MASTER-INDEX.RQST-WORK-PRCSS-ID
            pnd_Case_Workprcss_Multi_Subrqst_Pnd_Subrqst_Ind.setValue(icw_Master_Index_Pnd_Subrqst_Id_Cde);                                                               //Natural: ASSIGN #SUBRQST-IND := ICW-MASTER-INDEX.#SUBRQST-ID-CDE
            pnd_Curr_Unit.setValue(icw_Master_Index_Admin_Unit_Cde);                                                                                                      //Natural: ASSIGN #CURR-UNIT := ICW-MASTER-INDEX.ADMIN-UNIT-CDE
            pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Rqst_Log_Dte_Tme.setValue(icw_Master_Index_Rqst_Log_Dte_Tme);                                                                //Natural: ASSIGN #ICW-RQST-LOG-DTE-TME := ICW-MASTER-INDEX.RQST-LOG-DTE-TME
            pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Strt_Evnt_Dte_Tme.reset();                                                                                                   //Natural: RESET #ICW-STRT-EVNT-DTE-TME
            vw_icw_Mit_History.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) ICW-MIT-HISTORY BY RQST-ROUTING-KEY = #ICW-RQST-ROUTING-KEY
            (
            "READ06",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Icw_Rqst_Routing_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
            1
            );
            READ06:
            while (condition(vw_icw_Mit_History.readNextRow("READ06")))
            {
                pnd_Init_Unit.setValue(icw_Mit_History_Admin_Unit_Cde);                                                                                                   //Natural: ASSIGN #INIT-UNIT := ICW-MIT-HISTORY.ADMIN-UNIT-CDE
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  WRITE(DAT) NOTITLE NOHDR USING FORM 'EFSF8708'
    }
    private void sub_Write_Ncw_Document() throws Exception                                                                                                                //Natural: WRITE-NCW-DOCUMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ==================================
        vw_ncw_Efm_Document.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) NCW-EFM-DOCUMENT WITH IMAGE-POINTER-KEY = #PARM-KEY-VALUE
        (
        "FIND05",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Parm_Key_Value, WcType.WITH) },
        1
        );
        FIND05:
        while (condition(vw_ncw_Efm_Document.readNextRow("FIND05", true)))
        {
            vw_ncw_Efm_Document.setIfNotFoundControlFlag(false);
            if (condition(vw_ncw_Efm_Document.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  DELETED
        if (condition(ncw_Efm_Document_Dcmnt_Rtntn_Ind.equals("D")))                                                                                                      //Natural: IF NCW-EFM-DOCUMENT.DCMNT-RTNTN-IND = 'D'
        {
            //*  DO NOT REPORT ON DELETED MINS.
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Min_Save.setValue(pnd_Parm_Key_Value_Pnd_Parm_Key_Min_N);                                                                                                     //Natural: ASSIGN #MIN-SAVE := #PARM-KEY-MIN-N
        pnd_Image_Source_Id.setValue(ncw_Efm_Document_Image_Source_Id);                                                                                                   //Natural: ASSIGN #IMAGE-SOURCE-ID := NCW-EFM-DOCUMENT.IMAGE-SOURCE-ID
        pnd_Cabinet_Id.setValue(ncw_Efm_Document_Np_Pin);                                                                                                                 //Natural: ASSIGN #CABINET-ID := NCW-EFM-DOCUMENT.NP-PIN
        pnd_Entry_Dte_Tme.setValue(ncw_Efm_Document_Crte_Dte_Tme);                                                                                                        //Natural: ASSIGN #ENTRY-DTE-TME := NCW-EFM-DOCUMENT.CRTE-DTE-TME
        pnd_Create_Unit.setValue(ncw_Efm_Document_Crte_Unit);                                                                                                             //Natural: ASSIGN #CREATE-UNIT := NCW-EFM-DOCUMENT.CRTE-UNIT
        vw_ncw_Master_Index.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) NCW-MASTER-INDEX WITH ACTV-UNQUE-KEY = NCW-EFM-DOCUMENT.RQST-LOG-DTE-TME
        (
        "FIND06",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", "=", ncw_Efm_Document_Rqst_Log_Dte_Tme, WcType.WITH) },
        1
        );
        FIND06:
        while (condition(vw_ncw_Master_Index.readNextRow("FIND06")))
        {
            vw_ncw_Master_Index.setIfNotFoundControlFlag(false);
            pnd_Tiaa_Rcvd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ncw_Master_Index_Rqst_Tiaa_Rcvd_Dte);                                                         //Natural: MOVE EDITED NCW-MASTER-INDEX.RQST-TIAA-RCVD-DTE TO #TIAA-RCVD-DTE ( EM = YYYYMMDD )
            pnd_Case_Workprcss_Multi_Subrqst_Pnd_Case_Id.setValue(ncw_Master_Index_Pnd_Case_Id_Cde);                                                                      //Natural: ASSIGN #CASE-ID := NCW-MASTER-INDEX.#CASE-ID-CDE
            pnd_Case_Workprcss_Multi_Subrqst_Pnd_Work_Prcss_Id.setValue(ncw_Master_Index_Rqst_Work_Prcss_Id);                                                             //Natural: ASSIGN #WORK-PRCSS-ID := NCW-MASTER-INDEX.RQST-WORK-PRCSS-ID
            pnd_Case_Workprcss_Multi_Subrqst_Pnd_Subrqst_Ind.setValue(ncw_Master_Index_Pnd_Subrqst_Id_Cde);                                                               //Natural: ASSIGN #SUBRQST-IND := NCW-MASTER-INDEX.#SUBRQST-ID-CDE
            pnd_Curr_Unit.setValue(ncw_Master_Index_Admin_Unit_Cde);                                                                                                      //Natural: ASSIGN #CURR-UNIT := NCW-MASTER-INDEX.ADMIN-UNIT-CDE
            pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Rqst_Log_Dte_Tme.setValue(ncw_Master_Index_Rqst_Log_Dte_Tme);                                                                //Natural: ASSIGN #ICW-RQST-LOG-DTE-TME := NCW-MASTER-INDEX.RQST-LOG-DTE-TME
            pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Strt_Evnt_Dte_Tme.reset();                                                                                                   //Natural: RESET #ICW-STRT-EVNT-DTE-TME
            vw_ncw_Mit_History.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) NCW-MIT-HISTORY BY RQST-ROUTING-KEY = #ICW-RQST-ROUTING-KEY
            (
            "READ07",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Icw_Rqst_Routing_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
            1
            );
            READ07:
            while (condition(vw_ncw_Mit_History.readNextRow("READ07")))
            {
                pnd_Init_Unit.setValue(ncw_Mit_History_Admin_Unit_Cde);                                                                                                   //Natural: ASSIGN #INIT-UNIT := NCW-MIT-HISTORY.ADMIN-UNIT-CDE
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  WRITE(DAT) NOTITLE NOHDR USING FORM 'EFSF8708'
    }

    //
}
