/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:28:25 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn3900
************************************************************
**        * FILE NAME            : Cwfn3900.java
**        * CLASS NAME           : Cwfn3900
**        * INSTANCE NAME        : Cwfn3900
************************************************************
************************************************************************
* SUBPROGRAM : CWFN3900
* FUNCTION   : COMPUTE BUSINESS DAYS GIVEN A START AND END
* AUTHOR     : JOSEPH S. PATINGO
*            : RCG INFORMATION TECHNOLOGY INC.
* NOTES      : HOLIDAYS ARE :
*            : NEW YEAR 01/01                                       ##
*            : MARTIN LUTHER KING DAY  3RD MONDAY IN JAN
*            : PRESIDENTS DAY          3RD MONDAY IN FEB
*            : MEMORIAL DAY            LAST MONDAY IN MAY
*            : INDEPENDENCE DAY        07/04                        ***
*            : LABOR DAY               1ST MONDAY IN SEPTEMBER
*            : THANKSGIVING DAY        4TH THURSDAY IN NOVEMBER
*            : CHRISTMAS HOLIDAY       12/25
*            :
*            : *** IF IT FALLS ON A SATURDAY, PREVIOUS FRIDAY NO WORK
*            :     IF IT FALLS ON A SUNDAY, NEXT MONDAY NO WORK
*            :  ## IF IT FALLS ON A WEEKEND, CHECK HOLIDAY TABLE IF DATE
*            :     STARTS FROM 1993 FORWARD, ELSE HOLIDAY STAYS ON THAT
*            :     DATE
* ---------- -----------------------------------------------------------
* 12/29/97 LZ  - ADD MARTIN LUTHER KING, JR. DAY AS A HOLIDAY
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn3900 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Start_Date;
    private DbsField pnd_End_Date;
    private DbsField pnd_Business_Days;
    private DbsField pnd_Business_Daysx;
    private DbsField pnd_Holiday_And_Weekend;
    private DbsField pnd_Start_Date_A;

    private DbsGroup pnd_Start_Date_A__R_Field_1;
    private DbsField pnd_Start_Date_A_Pnd_Start_Year;

    private DbsGroup pnd_Start_Date_A__R_Field_2;
    private DbsField pnd_Start_Date_A_Pnd_Start_Year_A;
    private DbsField pnd_Start_Date_A_Pnd_Start_Month;

    private DbsGroup pnd_Start_Date_A__R_Field_3;
    private DbsField pnd_Start_Date_A_Pnd_Start_Month_A;
    private DbsField pnd_Start_Date_A_Pnd_Start_Day;

    private DbsGroup pnd_Start_Date_A__R_Field_4;
    private DbsField pnd_Start_Date_A_Pnd_Start_Day_A;
    private DbsField pnd_End_Date_A;

    private DbsGroup pnd_End_Date_A__R_Field_5;
    private DbsField pnd_End_Date_A_Pnd_End_Year;

    private DbsGroup pnd_End_Date_A__R_Field_6;
    private DbsField pnd_End_Date_A_Pnd_End_Year_A;
    private DbsField pnd_End_Date_A_Pnd_End_Month;

    private DbsGroup pnd_End_Date_A__R_Field_7;
    private DbsField pnd_End_Date_A_Pnd_End_Month_A;
    private DbsField pnd_End_Date_A_Pnd_End_Day;

    private DbsGroup pnd_End_Date_A__R_Field_8;
    private DbsField pnd_End_Date_A_Pnd_End_Day_A;
    private DbsField pnd_Year;

    private DbsGroup pnd_Year__R_Field_9;
    private DbsField pnd_Year_Pnd_Year_N;
    private DbsField pnd_Date_A;
    private DbsField pnd_Date;
    private DbsField pnd_Name_Of_Day;
    private DbsField pnd_Day_Ctr;
    private DbsField pnd_Holiday_Date;
    private DbsField pnd_H_Ctr;
    private DbsField pnd_Start_Ctr;
    private DbsField pnd_Between_Ctr;
    private DbsField pnd_End_Ctr;
    private DbsField pnd_Work_Date;
    private DbsField pnd_Holiday_Ctr;
    private DbsField pnd_Nbr_Of_Weekends;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Start_Date = parameters.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.TIME);
        pnd_Start_Date.setParameterOption(ParameterOption.ByReference);
        pnd_End_Date = parameters.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.TIME);
        pnd_End_Date.setParameterOption(ParameterOption.ByReference);
        pnd_Business_Days = parameters.newFieldInRecord("pnd_Business_Days", "#BUSINESS-DAYS", FieldType.NUMERIC, 20, 2);
        pnd_Business_Days.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Business_Daysx = localVariables.newFieldInRecord("pnd_Business_Daysx", "#BUSINESS-DAYSX", FieldType.NUMERIC, 20, 2);
        pnd_Holiday_And_Weekend = localVariables.newFieldInRecord("pnd_Holiday_And_Weekend", "#HOLIDAY-AND-WEEKEND", FieldType.NUMERIC, 18);
        pnd_Start_Date_A = localVariables.newFieldInRecord("pnd_Start_Date_A", "#START-DATE-A", FieldType.STRING, 8);

        pnd_Start_Date_A__R_Field_1 = localVariables.newGroupInRecord("pnd_Start_Date_A__R_Field_1", "REDEFINE", pnd_Start_Date_A);
        pnd_Start_Date_A_Pnd_Start_Year = pnd_Start_Date_A__R_Field_1.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Year", "#START-YEAR", FieldType.NUMERIC, 
            4);

        pnd_Start_Date_A__R_Field_2 = pnd_Start_Date_A__R_Field_1.newGroupInGroup("pnd_Start_Date_A__R_Field_2", "REDEFINE", pnd_Start_Date_A_Pnd_Start_Year);
        pnd_Start_Date_A_Pnd_Start_Year_A = pnd_Start_Date_A__R_Field_2.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Year_A", "#START-YEAR-A", FieldType.STRING, 
            4);
        pnd_Start_Date_A_Pnd_Start_Month = pnd_Start_Date_A__R_Field_1.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Month", "#START-MONTH", FieldType.NUMERIC, 
            2);

        pnd_Start_Date_A__R_Field_3 = pnd_Start_Date_A__R_Field_1.newGroupInGroup("pnd_Start_Date_A__R_Field_3", "REDEFINE", pnd_Start_Date_A_Pnd_Start_Month);
        pnd_Start_Date_A_Pnd_Start_Month_A = pnd_Start_Date_A__R_Field_3.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Month_A", "#START-MONTH-A", FieldType.STRING, 
            2);
        pnd_Start_Date_A_Pnd_Start_Day = pnd_Start_Date_A__R_Field_1.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Day", "#START-DAY", FieldType.NUMERIC, 
            2);

        pnd_Start_Date_A__R_Field_4 = pnd_Start_Date_A__R_Field_1.newGroupInGroup("pnd_Start_Date_A__R_Field_4", "REDEFINE", pnd_Start_Date_A_Pnd_Start_Day);
        pnd_Start_Date_A_Pnd_Start_Day_A = pnd_Start_Date_A__R_Field_4.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Day_A", "#START-DAY-A", FieldType.STRING, 
            2);
        pnd_End_Date_A = localVariables.newFieldInRecord("pnd_End_Date_A", "#END-DATE-A", FieldType.STRING, 8);

        pnd_End_Date_A__R_Field_5 = localVariables.newGroupInRecord("pnd_End_Date_A__R_Field_5", "REDEFINE", pnd_End_Date_A);
        pnd_End_Date_A_Pnd_End_Year = pnd_End_Date_A__R_Field_5.newFieldInGroup("pnd_End_Date_A_Pnd_End_Year", "#END-YEAR", FieldType.NUMERIC, 4);

        pnd_End_Date_A__R_Field_6 = pnd_End_Date_A__R_Field_5.newGroupInGroup("pnd_End_Date_A__R_Field_6", "REDEFINE", pnd_End_Date_A_Pnd_End_Year);
        pnd_End_Date_A_Pnd_End_Year_A = pnd_End_Date_A__R_Field_6.newFieldInGroup("pnd_End_Date_A_Pnd_End_Year_A", "#END-YEAR-A", FieldType.STRING, 4);
        pnd_End_Date_A_Pnd_End_Month = pnd_End_Date_A__R_Field_5.newFieldInGroup("pnd_End_Date_A_Pnd_End_Month", "#END-MONTH", FieldType.NUMERIC, 2);

        pnd_End_Date_A__R_Field_7 = pnd_End_Date_A__R_Field_5.newGroupInGroup("pnd_End_Date_A__R_Field_7", "REDEFINE", pnd_End_Date_A_Pnd_End_Month);
        pnd_End_Date_A_Pnd_End_Month_A = pnd_End_Date_A__R_Field_7.newFieldInGroup("pnd_End_Date_A_Pnd_End_Month_A", "#END-MONTH-A", FieldType.STRING, 
            2);
        pnd_End_Date_A_Pnd_End_Day = pnd_End_Date_A__R_Field_5.newFieldInGroup("pnd_End_Date_A_Pnd_End_Day", "#END-DAY", FieldType.NUMERIC, 2);

        pnd_End_Date_A__R_Field_8 = pnd_End_Date_A__R_Field_5.newGroupInGroup("pnd_End_Date_A__R_Field_8", "REDEFINE", pnd_End_Date_A_Pnd_End_Day);
        pnd_End_Date_A_Pnd_End_Day_A = pnd_End_Date_A__R_Field_8.newFieldInGroup("pnd_End_Date_A_Pnd_End_Day_A", "#END-DAY-A", FieldType.STRING, 2);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.STRING, 4);

        pnd_Year__R_Field_9 = localVariables.newGroupInRecord("pnd_Year__R_Field_9", "REDEFINE", pnd_Year);
        pnd_Year_Pnd_Year_N = pnd_Year__R_Field_9.newFieldInGroup("pnd_Year_Pnd_Year_N", "#YEAR-N", FieldType.NUMERIC, 4);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Name_Of_Day = localVariables.newFieldInRecord("pnd_Name_Of_Day", "#NAME-OF-DAY", FieldType.STRING, 3);
        pnd_Day_Ctr = localVariables.newFieldInRecord("pnd_Day_Ctr", "#DAY-CTR", FieldType.PACKED_DECIMAL, 2);
        pnd_Holiday_Date = localVariables.newFieldArrayInRecord("pnd_Holiday_Date", "#HOLIDAY-DATE", FieldType.DATE, new DbsArrayController(1, 8));
        pnd_H_Ctr = localVariables.newFieldInRecord("pnd_H_Ctr", "#H-CTR", FieldType.PACKED_DECIMAL, 1);
        pnd_Start_Ctr = localVariables.newFieldInRecord("pnd_Start_Ctr", "#START-CTR", FieldType.PACKED_DECIMAL, 3);
        pnd_Between_Ctr = localVariables.newFieldInRecord("pnd_Between_Ctr", "#BETWEEN-CTR", FieldType.PACKED_DECIMAL, 3);
        pnd_End_Ctr = localVariables.newFieldInRecord("pnd_End_Ctr", "#END-CTR", FieldType.PACKED_DECIMAL, 3);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_Holiday_Ctr = localVariables.newFieldInRecord("pnd_Holiday_Ctr", "#HOLIDAY-CTR", FieldType.PACKED_DECIMAL, 18);
        pnd_Nbr_Of_Weekends = localVariables.newFieldInRecord("pnd_Nbr_Of_Weekends", "#NBR-OF-WEEKENDS", FieldType.PACKED_DECIMAL, 18);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn3900() throws Exception
    {
        super("Cwfn3900");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Date_A.setValueEdited(pnd_Start_Date,new ReportEditMask("YYYYMMDD"));                                                                                         //Natural: MOVE EDITED #START-DATE ( EM = YYYYMMDD ) TO #DATE-A
        pnd_Start_Date_A.setValue(pnd_Date_A);                                                                                                                            //Natural: MOVE #DATE-A TO #START-DATE-A
        pnd_Date_A.setValueEdited(pnd_End_Date,new ReportEditMask("YYYYMMDD"));                                                                                           //Natural: MOVE EDITED #END-DATE ( EM = YYYYMMDD ) TO #DATE-A
        pnd_End_Date_A.setValue(pnd_Date_A);                                                                                                                              //Natural: MOVE #DATE-A TO #END-DATE-A
        FOR01:                                                                                                                                                            //Natural: FOR #YEAR-N = #START-YEAR THRU #END-YEAR STEP 1
        for (pnd_Year_Pnd_Year_N.setValue(pnd_Start_Date_A_Pnd_Start_Year); condition(pnd_Year_Pnd_Year_N.lessOrEqual(pnd_End_Date_A_Pnd_End_Year)); pnd_Year_Pnd_Year_N.nadd(1))
        {
                                                                                                                                                                          //Natural: PERFORM NEW-YEARS-DAY
            sub_New_Years_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  LZ
                                                                                                                                                                          //Natural: PERFORM MARTIN-LUTHER-KING-DAY
            sub_Martin_Luther_King_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM PRESIDENTS-DAY
            sub_Presidents_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM MEMORIAL-DAY
            sub_Memorial_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM INDEPENDENCE-DAY
            sub_Independence_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM LABOR-DAY
            sub_Labor_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM THANKSGIVING-DAY
            sub_Thanksgiving_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CHRISTMAS-DAY
            sub_Christmas_Day();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  LZ
            short decideConditionsMet92 = 0;                                                                                                                              //Natural: DECIDE ON FIRST VALUE OF #YEAR-N;//Natural: VALUE #START-YEAR
            if (condition((pnd_Year_Pnd_Year_N.equals(pnd_Start_Date_A_Pnd_Start_Year))))
            {
                decideConditionsMet92++;
                //* *    FOR #H-CTR = 1 TO 7
                FOR02:                                                                                                                                                    //Natural: FOR #H-CTR = 1 TO 8
                for (pnd_H_Ctr.setValue(1); condition(pnd_H_Ctr.lessOrEqual(8)); pnd_H_Ctr.nadd(1))
                {
                    if (condition(pnd_Holiday_Date.getValue(pnd_H_Ctr).greater(getZero()) && pnd_Holiday_Date.getValue(pnd_H_Ctr).greaterOrEqual(pnd_Start_Date)          //Natural: IF #HOLIDAY-DATE ( #H-CTR ) GT 0 AND #HOLIDAY-DATE ( #H-CTR ) GE #START-DATE AND #HOLIDAY-DATE ( #H-CTR ) LE #END-DATE
                        && pnd_Holiday_Date.getValue(pnd_H_Ctr).lessOrEqual(pnd_End_Date)))
                    {
                        pnd_Holiday_Ctr.nadd(1);                                                                                                                          //Natural: ADD 1 TO #HOLIDAY-CTR
                        pnd_Start_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #START-CTR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE #END-YEAR
            else if (condition((pnd_Year_Pnd_Year_N.equals(pnd_End_Date_A_Pnd_End_Year))))
            {
                decideConditionsMet92++;
                //*  LZ
                if (condition(pnd_End_Date_A_Pnd_End_Year.greater(pnd_Start_Date_A_Pnd_Start_Year)))                                                                      //Natural: IF #END-YEAR GT #START-YEAR
                {
                    //* *      FOR #H-CTR = 1 TO 7
                    FOR03:                                                                                                                                                //Natural: FOR #H-CTR = 1 TO 8
                    for (pnd_H_Ctr.setValue(1); condition(pnd_H_Ctr.lessOrEqual(8)); pnd_H_Ctr.nadd(1))
                    {
                        if (condition(pnd_Holiday_Date.getValue(pnd_H_Ctr).greater(getZero()) && pnd_Holiday_Date.getValue(pnd_H_Ctr).lessOrEqual(pnd_End_Date)))         //Natural: IF #HOLIDAY-DATE ( #H-CTR ) GT 0 AND #HOLIDAY-DATE ( #H-CTR ) LE #END-DATE
                        {
                            pnd_Holiday_Ctr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #HOLIDAY-CTR
                            pnd_End_Ctr.nadd(1);                                                                                                                          //Natural: ADD 1 TO #END-CTR
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  LZ
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                //* *    FOR #H-CTR = 1 TO 7
                FOR04:                                                                                                                                                    //Natural: FOR #H-CTR = 1 TO 8
                for (pnd_H_Ctr.setValue(1); condition(pnd_H_Ctr.lessOrEqual(8)); pnd_H_Ctr.nadd(1))
                {
                    if (condition(pnd_Holiday_Date.getValue(pnd_H_Ctr).greater(getZero())))                                                                               //Natural: IF #HOLIDAY-DATE ( #H-CTR ) GT 0
                    {
                        pnd_Holiday_Ctr.nadd(1);                                                                                                                          //Natural: ADD 1 TO #HOLIDAY-CTR
                        pnd_Between_Ctr.nadd(1);                                                                                                                          //Natural: ADD 1 TO #BETWEEN-CTR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  RESET #HOLIDAY-DATE(*)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM COMPUTE-NO-OF-WEEKENDS
        sub_Compute_No_Of_Weekends();
        if (condition(Global.isEscape())) {return;}
        pnd_Holiday_And_Weekend.compute(new ComputeParameters(false, pnd_Holiday_And_Weekend), pnd_Holiday_Ctr.add(pnd_Nbr_Of_Weekends));                                 //Natural: COMPUTE #HOLIDAY-AND-WEEKEND = #HOLIDAY-CTR + #NBR-OF-WEEKENDS
        pnd_Business_Daysx.compute(new ComputeParameters(false, pnd_Business_Daysx), pnd_End_Date.subtract(pnd_Start_Date));                                              //Natural: COMPUTE #BUSINESS-DAYSX = #END-DATE - #START-DATE
        pnd_Business_Days.compute(new ComputeParameters(false, pnd_Business_Days), (pnd_Business_Daysx.divide(864000)).subtract(pnd_Holiday_And_Weekend));                //Natural: COMPUTE #BUSINESS-DAYS = ( #BUSINESS-DAYSX / 864000 ) - #HOLIDAY-AND-WEEKEND
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  CWFC3901 : 8 HOLIDAY CALCULATION ROUTINE
        //*           : USED IN CWFN3900 CWFN3901 CWFN3905 CWFN3999
        //*                     CWFN1846
        //*  ---------- -----------------------------------------------------------
        //*  12/29/97 LZ  - ADD MARTIN LUTHER KING, JR. DAY AS A HOLIDAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEW-YEARS-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MARTIN-LUTHER-KING-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRESIDENTS-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MEMORIAL-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INDEPENDENCE-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LABOR-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: THANKSGIVING-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHRISTMAS-DAY
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-NO-OF-WEEKENDS
    }
    private void sub_New_Years_Day() throws Exception                                                                                                                     //Natural: NEW-YEARS-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  NEW YEAR 01/01/YYYY
        //*  HOLIDAY IS ON THE DAY IT FALLS ON REGARDLESS OF WHETHER IT IS A
        //*  WEEKDAY OR A WEEKEND
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0101"));                                                                           //Natural: COMPRESS #YEAR '0101' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        short decideConditionsMet170 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #NAME-OF-DAY;//Natural: VALUE 'Sun'
        if (condition((pnd_Name_Of_Day.equals("Sun"))))
        {
            decideConditionsMet170++;
            pnd_Date.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(! (pnd_Name_Of_Day.equals("Sun"))))                                                                                                                 //Natural: IF NOT ( #NAME-OF-DAY = 'Sun' )
        {
            pnd_Holiday_Date.getValue(1).setValue(pnd_Date);                                                                                                              //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  LZ
    private void sub_Martin_Luther_King_Day() throws Exception                                                                                                            //Natural: MARTIN-LUTHER-KING-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  MARTIN LUTHER KING, JR. DAY 3RD MONDAY IN JANUARY
        if (condition(pnd_Year.less("1998")))                                                                                                                             //Natural: IF #YEAR < '1998'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Day_Ctr.setValue(0);                                                                                                                                          //Natural: MOVE 0 TO #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0101"));                                                                           //Natural: COMPRESS #YEAR '0101' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Mon")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Mon'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Date.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DATE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Date.nadd(14);                                                                                                                                                //Natural: ADD 14 TO #DATE
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        pnd_Holiday_Date.getValue(2).setValue(pnd_Date);                                                                                                                  //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 2 )
    }
    private void sub_Presidents_Day() throws Exception                                                                                                                    //Natural: PRESIDENTS-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  PRESIDENT's Day 3rd Monday in February
        pnd_Day_Ctr.setValue(0);                                                                                                                                          //Natural: MOVE 0 TO #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0201"));                                                                           //Natural: COMPRESS #YEAR '0201' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Mon")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Mon'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Date.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DATE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Date.nadd(14);                                                                                                                                                //Natural: ADD 14 TO #DATE
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        //* *MOVE #DATE TO #HOLIDAY-DATE(2)
        //*  LZ
        pnd_Holiday_Date.getValue(3).setValue(pnd_Date);                                                                                                                  //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 3 )
    }
    private void sub_Memorial_Day() throws Exception                                                                                                                      //Natural: MEMORIAL-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  MEMORIAL DAY - LAST MONDAY IN MAY
        pnd_Day_Ctr.setValue(0);                                                                                                                                          //Natural: MOVE 0 TO #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0531"));                                                                           //Natural: COMPRESS #YEAR '0531' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT03:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Mon")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Mon'
            {
                pnd_Day_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DAY-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(getZero())))                                                                                                                 //Natural: IF #DAY-CTR = 0
            {
                pnd_Date.nsubtract(1);                                                                                                                                    //Natural: SUBTRACT 1 FROM #DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(1))) {break;}                                                                                                                //Natural: UNTIL #DAY-CTR = 1
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        //* *MOVE #DATE TO #HOLIDAY-DATE(3)
        //*  LZ
        pnd_Holiday_Date.getValue(4).setValue(pnd_Date);                                                                                                                  //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 4 )
    }
    private void sub_Independence_Day() throws Exception                                                                                                                  //Natural: INDEPENDENCE-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  INDEPENDENCE DAY 07/04/YYYY
        //*  IF IT FALLS ON A SUNDAY, THE NEXT WORKING DAY IS A HOLIDAY
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0704"));                                                                           //Natural: COMPRESS #YEAR '0704' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        short decideConditionsMet257 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #NAME-OF-DAY;//Natural: VALUE 'Sat'
        if (condition((pnd_Name_Of_Day.equals("Sat"))))
        {
            decideConditionsMet257++;
            pnd_Date.nsubtract(1);                                                                                                                                        //Natural: SUBTRACT 1 FROM #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: VALUE 'Sun'
        else if (condition((pnd_Name_Of_Day.equals("Sun"))))
        {
            decideConditionsMet257++;
            pnd_Date.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(! (pnd_Name_Of_Day.equals("Sat") || pnd_Name_Of_Day.equals("Sun"))))                                                                                //Natural: IF NOT ( #NAME-OF-DAY = 'Sat' OR = 'Sun' )
        {
            //* *  MOVE #DATE TO #HOLIDAY-DATE(4)
            //*  LZ
            pnd_Holiday_Date.getValue(5).setValue(pnd_Date);                                                                                                              //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 5 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Labor_Day() throws Exception                                                                                                                         //Natural: LABOR-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  LABOR DAY       1ST MONDAY IN SEPTEMBER
        pnd_Day_Ctr.setValue(0);                                                                                                                                          //Natural: MOVE 0 TO #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "0901"));                                                                           //Natural: COMPRESS #YEAR '0901' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT04:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Mon")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Mon'
            {
                pnd_Day_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DAY-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(getZero())))                                                                                                                 //Natural: IF #DAY-CTR = 0
            {
                pnd_Date.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Ctr.equals(1))) {break;}                                                                                                                //Natural: UNTIL #DAY-CTR = 1
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        //* *MOVE #DATE TO #HOLIDAY-DATE(5)
        //*  LZ
        pnd_Holiday_Date.getValue(6).setValue(pnd_Date);                                                                                                                  //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 6 )
    }
    private void sub_Thanksgiving_Day() throws Exception                                                                                                                  //Natural: THANKSGIVING-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  THANKSGIVING    4TH THURSDAY IN NOVEMBER
        pnd_Day_Ctr.setValue(0);                                                                                                                                          //Natural: MOVE 0 TO #DAY-CTR
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "1101"));                                                                           //Natural: COMPRESS #YEAR '1101' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        REPEAT05:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Thu")))                                                                                                                 //Natural: IF #NAME-OF-DAY = 'Thu'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Date.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DATE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Date.nadd(21);                                                                                                                                                //Natural: ADD 21 TO #DATE
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        //* *MOVE #DATE TO #HOLIDAY-DATE(6)
        //*  LZ
        pnd_Holiday_Date.getValue(7).setValue(pnd_Date);                                                                                                                  //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 7 )
    }
    private void sub_Christmas_Day() throws Exception                                                                                                                     //Natural: CHRISTMAS-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  CHRISTMAS DAY    12/25/YYYY
        //*  IF IT FALLS ON A SATURDAY,  THE PREVIOUS WORKING DAY IS A HOLIDAY
        //*  IF IT FALLS ON A SUNDAY, THE NEXT WORKING DAY IS A HOLIDAY
        pnd_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Year, "1225"));                                                                           //Natural: COMPRESS #YEAR '1225' INTO #DATE-A LEAVING NO SPACE
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A);                                                                                               //Natural: MOVE EDITED #DATE-A TO #DATE ( EM = YYYYMMDD )
        pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                               //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        short decideConditionsMet327 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #NAME-OF-DAY;//Natural: VALUE 'Sat'
        if (condition((pnd_Name_Of_Day.equals("Sat"))))
        {
            decideConditionsMet327++;
            pnd_Date.nsubtract(1);                                                                                                                                        //Natural: SUBTRACT 1 FROM #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: VALUE 'Sun'
        else if (condition((pnd_Name_Of_Day.equals("Sun"))))
        {
            decideConditionsMet327++;
            pnd_Date.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DATE
            pnd_Name_Of_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                           //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #NAME-OF-DAY
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(! (pnd_Name_Of_Day.equals("Sat") || pnd_Name_Of_Day.equals("Sun"))))                                                                                //Natural: IF NOT ( #NAME-OF-DAY = 'Sat' OR = 'Sun' )
        {
            //* *MOVE #DATE TO #HOLIDAY-DATE(7)
            //*  LZ
            pnd_Holiday_Date.getValue(8).setValue(pnd_Date);                                                                                                              //Natural: MOVE #DATE TO #HOLIDAY-DATE ( 8 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Compute_No_Of_Weekends() throws Exception                                                                                                            //Natural: COMPUTE-NO-OF-WEEKENDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Date.setValue(pnd_Start_Date);                                                                                                                           //Natural: MOVE #START-DATE TO #WORK-DATE
        pnd_Nbr_Of_Weekends.setValue(0);                                                                                                                                  //Natural: MOVE 0 TO #NBR-OF-WEEKENDS
        REPEAT06:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Name_Of_Day.setValueEdited(pnd_Work_Date,new ReportEditMask("NNN"));                                                                                      //Natural: MOVE EDITED #WORK-DATE ( EM = NNN ) TO #NAME-OF-DAY
            if (condition(pnd_Name_Of_Day.equals("Sat") || pnd_Name_Of_Day.equals("Sun")))                                                                                //Natural: IF #NAME-OF-DAY = 'Sat' OR = 'Sun'
            {
                pnd_Nbr_Of_Weekends.nadd(1);                                                                                                                              //Natural: ADD 1 TO #NBR-OF-WEEKENDS
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Date.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WORK-DATE
            if (condition(pnd_Work_Date.greater(pnd_End_Date))) {break;}                                                                                                  //Natural: UNTIL #WORK-DATE GT #END-DATE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }

    //
}
