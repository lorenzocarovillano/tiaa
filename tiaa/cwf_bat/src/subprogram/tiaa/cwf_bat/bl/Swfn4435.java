/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:12:20 AM
**        * FROM NATURAL SUBPROGRAM : Swfn4435
************************************************************
**        * FILE NAME            : Swfn4435.java
**        * CLASS NAME           : Swfn4435
**        * INSTANCE NAME        : Swfn4435
************************************************************
************************************************************************
* PROGRAM  : SWFN4435
* SYSTEM   : SURVIVOR BENEFITS
* TITLE    : CHECK TWO (2) LINES OF SURVIVOR NAME
* CREATED  :
* AUTHOR   : CHRIS SARMIENTO
* FUNCTION : DETERMINES WHICH OF THE TWO LINES OF SRVVR NAME IS THE
*            ACTUAL SURVIVOR NAME, OR NAME OF ESTATE OR ORGANIZATION
*            OR THE EXECUTOR
*            RETURNS #NAME1 WITH
*               NAME OF ESTATE, ORGANIZATION, OR INDIVIDUAL
*            RETURNS #NAME2 WITH
*               NAME OF EXECUTOR, TRUSTEE, ETC.
*
* HISTORY
*
*
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Swfn4435 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Name1;
    private DbsField pnd_Name2;
    private DbsField pnd_Update_Dte;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Tbl_Reps;

    private DbsGroup cwf_Support_Tbl__R_Field_2;

    private DbsGroup cwf_Support_Tbl_Tbl_Reps_Redef;
    private DbsField cwf_Support_Tbl_Rep_Actv_Ind;
    private DbsField cwf_Support_Tbl_Rep_Id;
    private DbsField cwf_Support_Tbl__Filler1;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_3;
    private DbsField pnd_Tbl_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Key_Field;
    private DbsField pnd_Long_Name;
    private DbsField pnd_Addr_Name;
    private DbsField pnd_Max_Words;
    private DbsField pnd_Key_Words;
    private DbsField pnd_Str_Arr;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Found1;
    private DbsField pnd_Found2;
    private DbsField pnd_T_Name2;
    private DbsField pnd_T_Name1;
    private DbsField pnd_Date_Const;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Name1 = parameters.newFieldInRecord("pnd_Name1", "#NAME1", FieldType.STRING, 35);
        pnd_Name1.setParameterOption(ParameterOption.ByReference);
        pnd_Name2 = parameters.newFieldInRecord("pnd_Name2", "#NAME2", FieldType.STRING, 35);
        pnd_Name2.setParameterOption(ParameterOption.ByReference);
        pnd_Update_Dte = parameters.newFieldInRecord("pnd_Update_Dte", "#UPDATE-DTE", FieldType.TIME);
        pnd_Update_Dte.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Reps = cwf_Support_Tbl__R_Field_1.newFieldArrayInGroup("cwf_Support_Tbl_Tbl_Reps", "TBL-REPS", FieldType.STRING, 25, new DbsArrayController(1, 
            10));

        cwf_Support_Tbl__R_Field_2 = cwf_Support_Tbl__R_Field_1.newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Tbl_Reps);

        cwf_Support_Tbl_Tbl_Reps_Redef = cwf_Support_Tbl__R_Field_2.newGroupArrayInGroup("cwf_Support_Tbl_Tbl_Reps_Redef", "TBL-REPS-REDEF", new DbsArrayController(1, 
            10));
        cwf_Support_Tbl_Rep_Actv_Ind = cwf_Support_Tbl_Tbl_Reps_Redef.newFieldInGroup("cwf_Support_Tbl_Rep_Actv_Ind", "REP-ACTV-IND", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Rep_Id = cwf_Support_Tbl_Tbl_Reps_Redef.newFieldInGroup("cwf_Support_Tbl_Rep_Id", "REP-ID", FieldType.STRING, 8);
        cwf_Support_Tbl__Filler1 = cwf_Support_Tbl_Tbl_Reps_Redef.newFieldInGroup("cwf_Support_Tbl__Filler1", "_FILLER1", FieldType.STRING, 16);
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 53);

        pnd_Tbl_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_3", "REDEFINE", pnd_Tbl_Key);
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Tbl_Table_Nme = pnd_Tbl_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Tbl_Key_Field = pnd_Tbl_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Long_Name = localVariables.newFieldInRecord("pnd_Long_Name", "#LONG-NAME", FieldType.STRING, 35);
        pnd_Addr_Name = localVariables.newFieldInRecord("pnd_Addr_Name", "#ADDR-NAME", FieldType.STRING, 70);
        pnd_Max_Words = localVariables.newFieldInRecord("pnd_Max_Words", "#MAX-WORDS", FieldType.INTEGER, 2);
        pnd_Key_Words = localVariables.newFieldArrayInRecord("pnd_Key_Words", "#KEY-WORDS", FieldType.STRING, 20, new DbsArrayController(1, 50));
        pnd_Str_Arr = localVariables.newFieldArrayInRecord("pnd_Str_Arr", "#STR-ARR", FieldType.STRING, 35, new DbsArrayController(1, 50));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 1);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 1);
        pnd_Found1 = localVariables.newFieldInRecord("pnd_Found1", "#FOUND1", FieldType.BOOLEAN, 1);
        pnd_Found2 = localVariables.newFieldInRecord("pnd_Found2", "#FOUND2", FieldType.BOOLEAN, 1);
        pnd_T_Name2 = localVariables.newFieldInRecord("pnd_T_Name2", "#T-NAME2", FieldType.STRING, 35);
        pnd_T_Name1 = localVariables.newFieldInRecord("pnd_T_Name1", "#T-NAME1", FieldType.STRING, 35);
        pnd_Date_Const = localVariables.newFieldInRecord("pnd_Date_Const", "#DATE-CONST", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Swfn4435() throws Exception
    {
        super("Swfn4435");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Date_Const.setValueEdited(new ReportEditMask("YYYYMMDD"),"19990410");                                                                                         //Natural: MOVE EDITED '19990410' TO #DATE-CONST ( EM = YYYYMMDD )
        //*   ANY NAME ENTERED AFTER THIS DATE
        if (condition(pnd_Update_Dte.greater(pnd_Date_Const)))                                                                                                            //Natural: IF #UPDATE-DTE GT #DATE-CONST
        {
            //*   IS ASSUMED TO BE IN THE RIGHT PLACE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *---  GET TABLE VALUES
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind.setValue("S");                                                                                                                    //Natural: ASSIGN #TBL-KEY.TBL-SCRTY-LEVEL-IND := 'S'
        pnd_Tbl_Key_Tbl_Table_Nme.setValue("CWF-SB-LETTER-CNTRL");                                                                                                        //Natural: ASSIGN #TBL-KEY.TBL-TABLE-NME := 'CWF-SB-LETTER-CNTRL'
        pnd_Tbl_Key_Tbl_Key_Field.setValue("BYPASS NAMES");                                                                                                               //Natural: ASSIGN #TBL-KEY.TBL-KEY-FIELD := 'BYPASS NAMES'
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY EQ #TBL-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ01")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.greater(pnd_Tbl_Key_Tbl_Table_Nme) || ! (DbsUtil.maskMatches(cwf_Support_Tbl_Tbl_Key_Field,"'BYPASS NAMES'"))))   //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME GT #TBL-KEY.TBL-TABLE-NME OR CWF-SUPPORT-TBL.TBL-KEY-FIELD NE MASK ( 'BYPASS NAMES' )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            cwf_Support_Tbl_Tbl_Data_Field.separate(SeparateOption.WithAnyDelimiters, "\\", pnd_Key_Words.getValue("*"));                                                 //Natural: SEPARATE CWF-SUPPORT-TBL.TBL-DATA-FIELD INTO #KEY-WORDS ( * ) WITH DELIMITERS "\" GIVING NUMBER #MAX-WORDS
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *---  CHECK NAME LINE 1
        pnd_Name1.separate(SeparateOption.Null, pnd_Str_Arr.getValue("*"));                                                                                               //Natural: SEPARATE #NAME1 INTO #STR-ARR ( * ) NUMBER #J
        if (condition(pnd_J.greater(getZero())))                                                                                                                          //Natural: IF #J GT 0
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I EQ 1 TO #MAX-WORDS
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Words)); pnd_I.nadd(1))
            {
                if (condition(pnd_Key_Words.getValue(pnd_I).greater(" ") && pnd_Key_Words.getValue(pnd_I).equals(pnd_Str_Arr.getValue(1,":",pnd_J))))                     //Natural: IF #KEY-WORDS ( #I ) GT ' ' AND #KEY-WORDS ( #I ) EQ #STR-ARR ( 1:#J )
                {
                    pnd_Found1.setValue(true);                                                                                                                            //Natural: ASSIGN #FOUND1 := TRUE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *---  CHECK NAME LINE 2
        if (condition(pnd_Name2.greater(" ")))                                                                                                                            //Natural: IF #NAME2 GT ' '
        {
            pnd_Name2.separate(SeparateOption.Null, pnd_Str_Arr.getValue("*"));                                                                                           //Natural: SEPARATE #NAME2 INTO #STR-ARR ( * ) NUMBER #J
            if (condition(pnd_J.greater(getZero())))                                                                                                                      //Natural: IF #J GT 0
            {
                FOR02:                                                                                                                                                    //Natural: FOR #I EQ 1 TO #MAX-WORDS
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Words)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Key_Words.getValue(pnd_I).greater(" ") && pnd_Key_Words.getValue(pnd_I).equals(pnd_Str_Arr.getValue(1,":",pnd_J))))                 //Natural: IF #KEY-WORDS ( #I ) GT ' ' AND #KEY-WORDS ( #I ) EQ #STR-ARR ( 1:#J )
                    {
                        pnd_Found2.setValue(true);                                                                                                                        //Natural: ASSIGN #FOUND2 := TRUE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ---  ESTATE/ORG?
        if (condition(pnd_Found1.getBoolean()))                                                                                                                           //Natural: IF #FOUND1
        {
            //*   IF NO NAME ON SECOND LINE
            //*   THEN ERROR
            if (condition(pnd_Name2.equals(" ")))                                                                                                                         //Natural: IF #NAME2 EQ ' '
            {
                pnd_T_Name1.setValue(" ");                                                                                                                                //Natural: ASSIGN #T-NAME1 := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_T_Name1.setValue(pnd_Name1);                                                                                                                          //Natural: ASSIGN #T-NAME1 := #NAME1
                pnd_T_Name2.setValue(pnd_Name2);                                                                                                                          //Natural: ASSIGN #T-NAME2 := #NAME2
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*   IF ONLY ONE NAME AND IT's valid
            if (condition(pnd_Name2.equals(" ")))                                                                                                                         //Natural: IF #NAME2 EQ ' '
            {
                pnd_T_Name1.setValue(pnd_Name1);                                                                                                                          //Natural: ASSIGN #T-NAME1 := #NAME1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*   CHECK IF NAME's are in the wrong sequence
                if (condition(pnd_Found2.getBoolean()))                                                                                                                   //Natural: IF #FOUND2
                {
                    pnd_T_Name1.setValue(pnd_Name2);                                                                                                                      //Natural: ASSIGN #T-NAME1 := #NAME2
                    pnd_T_Name2.setValue(pnd_Name1);                                                                                                                      //Natural: ASSIGN #T-NAME2 := #NAME1
                    //*   OR NAMES WERE ENTERED CORRECTLY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_T_Name1.setValue(pnd_Name1);                                                                                                                      //Natural: ASSIGN #T-NAME1 := #NAME1
                    pnd_T_Name2.setValue(pnd_Name2);                                                                                                                      //Natural: ASSIGN #T-NAME2 := #NAME2
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_T_Name2.contains ("C/O") ||pnd_T_Name2.contains ("ATTN:")))                                                                                     //Natural: IF #T-NAME2 EQ SCAN 'C/O' OR EQ SCAN 'ATTN:'
        {
            DbsUtil.examine(new ExamineSource(pnd_T_Name2), new ExamineSearch("C/O"), new ExamineDelete());                                                               //Natural: EXAMINE #T-NAME2 FOR 'C/O' DELETE
            DbsUtil.examine(new ExamineSource(pnd_T_Name2), new ExamineSearch("ATTN:"), new ExamineDelete());                                                             //Natural: EXAMINE #T-NAME2 FOR 'ATTN:' DELETE
            pnd_T_Name2.setValue(pnd_T_Name2, MoveOption.LeftJustified);                                                                                                  //Natural: MOVE LEFT #T-NAME2 TO #T-NAME2
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Name1.setValue(pnd_T_Name1);                                                                                                                                  //Natural: ASSIGN #NAME1 := #T-NAME1
        pnd_Name2.setValue(pnd_T_Name2);                                                                                                                                  //Natural: ASSIGN #NAME2 := #T-NAME2
    }

    //
}
