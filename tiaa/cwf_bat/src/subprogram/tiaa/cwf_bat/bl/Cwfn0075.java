/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:12:06 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn0075
************************************************************
**        * FILE NAME            : Cwfn0075.java
**        * CLASS NAME           : Cwfn0075
**        * INSTANCE NAME        : Cwfn0075
************************************************************
************************************************************************
* PROGRAM  : CWFN0075
* SYSTEM   : PROJCWF
* TITLE    : CONTRACT SELECTION FOR PIN OF MUTUAL FUNDS
* FUNCTION : PASSES BACK THE COUNT OF MUTUAL FUND PARTICIPANTS
*          |
*          |
*          |
*          |
*          |
* MOD DATE | BY    DESCRIPTION OF CHANGES
*          |
* 04/24/98 | GH  CHANGE #MUTUAL-FUND TO #LOB-FOUND(*), AND
*          |     CHANGE #CNTRCT-NBR  TO #CNTRCT-NBR(*)
*          |     1: MUTUAL FUNDS (M)
*          |     2: TRUST SERVICES (A)
* 07/22/97 | GH  ADDED ADDITIONAL PARAMETER - #TIAA-RCVD-DTE(D)
* 12/26/96 | GH  CREATED NEW / MODIFIED FROM CWFN0070
* 12/23/15 | JB  COR/NAAD RETIREMENT
* 02/23/2017 - JHANWAR - PIN EXPANSION - AUG 2017
* 06/19/2017 - MDM PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn0075 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaMdma111 pdaMdma111;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Pin_Nbr;
    private DbsField pnd_Lob_Found;
    private DbsField pnd_Cntrct_Nbr;
    private DbsField pnd_Tiaa_Rcvd_Dte;

    private DbsGroup pnd_Misc;
    private DbsField pnd_Misc_Pnd_X;
    private DbsField pnd_Misc_Pnd_Tiaa_Rcvd_Dte_A;

    private DbsGroup pnd_Misc__R_Field_1;
    private DbsField pnd_Misc_Pnd_Tiaa_Rcvd_Dte_N;
    private DbsField pnd_Misc_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMdma111 = new PdaMdma111(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pnd_Pin_Nbr = parameters.newFieldInRecord("pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Pin_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Lob_Found = parameters.newFieldArrayInRecord("pnd_Lob_Found", "#LOB-FOUND", FieldType.BOOLEAN, 1, new DbsArrayController(1, 10));
        pnd_Lob_Found.setParameterOption(ParameterOption.ByReference);
        pnd_Cntrct_Nbr = parameters.newFieldArrayInRecord("pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10, new DbsArrayController(1, 10));
        pnd_Cntrct_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Tiaa_Rcvd_Dte = parameters.newFieldInRecord("pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Tiaa_Rcvd_Dte.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Misc = localVariables.newGroupInRecord("pnd_Misc", "#MISC");
        pnd_Misc_Pnd_X = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_Misc_Pnd_Tiaa_Rcvd_Dte_A = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Tiaa_Rcvd_Dte_A", "#TIAA-RCVD-DTE-A", FieldType.STRING, 8);

        pnd_Misc__R_Field_1 = pnd_Misc.newGroupInGroup("pnd_Misc__R_Field_1", "REDEFINE", pnd_Misc_Pnd_Tiaa_Rcvd_Dte_A);
        pnd_Misc_Pnd_Tiaa_Rcvd_Dte_N = pnd_Misc__R_Field_1.newFieldInGroup("pnd_Misc_Pnd_Tiaa_Rcvd_Dte_N", "#TIAA-RCVD-DTE-N", FieldType.NUMERIC, 8);
        pnd_Misc_I = pnd_Misc.newFieldInGroup("pnd_Misc_I", "I", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn0075() throws Exception
    {
        super("Cwfn0075");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFN0075", onError);
        //*  #COR-KEY
        //* MDM PIN EXP
        pnd_Lob_Found.getValue("*").reset();                                                                                                                              //Natural: RESET #LOB-FOUND ( * ) #CNTRCT-NBR ( * )
        pnd_Cntrct_Nbr.getValue("*").reset();
        //*  MOVE #PIN-NBR TO #CK-PIN
        //*  MOVE  2       TO #CK-TYP
        //*  FOR I = 1 TO 10
        //*   DECIDE ON FIRST VALUE OF I
        //*     VALUE 1   MOVE 'M' TO #CK-LOB
        //*     VALUE 2   MOVE 'A' TO #CK-LOB
        //*     NONE      MOVE ' ' TO #CK-LOB
        //*       ESCAPE BOTTOM
        //*   END-DECIDE
        //*   READ COR BY COR-SUPER-PIN-RCDTYPE-LOB-NINES FROM #COR-KEY
        //*     IF COR.CNTRCT-LOB-CDE   NE #CK-LOB  OR
        //*        COR.PH-UNIQUE-ID-NBR NE #CK-PIN  OR
        //*        COR.PH-RCD-TYPE-CDE  NE #CK-TYP
        //*       ESCAPE BOTTOM IMMEDIATE
        //*     END-IF
        //*  #MDMA110.#I-PIN-A7 := #PIN-NBR
        pdaMdma111.getPnd_Mdma111_Pnd_I_Pin_A12().setValue(pnd_Pin_Nbr);                                                                                                  //Natural: ASSIGN #MDMA111.#I-PIN-A12 := #PIN-NBR
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  CALLNAT 'MDMN110A' #MDMA110     /*MDM PIN EXP
            //* MDM PIN EXP
            DbsUtil.callnat(Mdmn111a.class , getCurrentProcessState(), pdaMdma111.getPnd_Mdma111());                                                                      //Natural: CALLNAT 'MDMN111A' #MDMA111
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  CALLNAT 'MDMN110' #MDMA110      /*MDM PIN EXP
            //* MDM PIN EXP
            DbsUtil.callnat(Mdmn111.class , getCurrentProcessState(), pdaMdma111.getPnd_Mdma111());                                                                       //Natural: CALLNAT 'MDMN111' #MDMA111
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #MDMA110.#O-RETURN-CODE =  '0000'
        //* MDM PIN EXP
        if (condition(pdaMdma111.getPnd_Mdma111_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA111.#O-RETURN-CODE = '0000'
        {
            FOR01:                                                                                                                                                        //Natural: FOR #X 1 TO 200
            for (pnd_Misc_Pnd_X.setValue(1); condition(pnd_Misc_Pnd_X.lessOrEqual(200)); pnd_Misc_Pnd_X.nadd(1))
            {
                //*    IF #MDMA110.#O-CONTRACT-NUMBER(#X) = ' '
                //* MDM PIN EXP
                if (condition(pdaMdma111.getPnd_Mdma111_Pnd_O_Contract_Number().getValue(pnd_Misc_Pnd_X).equals(" ")))                                                    //Natural: IF #MDMA111.#O-CONTRACT-NUMBER ( #X ) = ' '
                {
                    //* MDM PIN EXP
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Misc_Pnd_Tiaa_Rcvd_Dte_A.setValueEdited(pnd_Tiaa_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED #TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO #TIAA-RCVD-DTE-A
                //*    IF CNTRCT-ISSUE-DTE < #TIAA-RCVD-DTE-N
                //*    IF #MDMA110.#O-CONTRACT-ISSUE-DATE(#X) < #TIAA-RCVD-DTE-N
                //* MDM PIN EXP
                if (condition(pdaMdma111.getPnd_Mdma111_Pnd_O_Contract_Issue_Date().getValue(pnd_Misc_Pnd_X).less(pnd_Misc_Pnd_Tiaa_Rcvd_Dte_N)))                         //Natural: IF #MDMA111.#O-CONTRACT-ISSUE-DATE ( #X ) < #TIAA-RCVD-DTE-N
                {
                    //* MDM PIN EXP
                    if (condition(pdaMdma111.getPnd_Mdma111_Pnd_O_Line_Of_Business().getValue(pnd_Misc_Pnd_X).equals("M")))                                               //Natural: IF #O-LINE-OF-BUSINESS ( #X ) = 'M'
                    {
                        pnd_Lob_Found.getValue(1).setValue(true);                                                                                                         //Natural: MOVE TRUE TO #LOB-FOUND ( 1 )
                        //*         IF #CK-LOB = 'M' MOVE TRUE TO #LOB-FOUND(1)
                        //*          MOVE CNTRCT-NBR TO #CNTRCT-NBR(1)
                        pnd_Cntrct_Nbr.getValue(1).setValue(pdaMdma111.getPnd_Mdma111_Pnd_O_Contract_Number().getValue(pnd_Misc_Pnd_X));                                  //Natural: MOVE #O-CONTRACT-NUMBER ( #X ) TO #CNTRCT-NBR ( 1 )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaMdma111.getPnd_Mdma111_Pnd_O_Line_Of_Business().getValue(pnd_Misc_Pnd_X).equals("A")))                                               //Natural: IF #O-LINE-OF-BUSINESS ( #X ) = 'A'
                    {
                        pnd_Lob_Found.getValue(2).setValue(true);                                                                                                         //Natural: MOVE TRUE TO #LOB-FOUND ( 2 )
                        //*         IF #CK-LOB = 'A' MOVE TRUE TO #LOB-FOUND(2)
                        //*         MOVE CNTRCT-NBR TO #CNTRCT-NBR(2)
                        pnd_Cntrct_Nbr.getValue(2).setValue(pdaMdma111.getPnd_Mdma111_Pnd_O_Contract_Number().getValue(pnd_Misc_Pnd_X));                                  //Natural: MOVE #O-CONTRACT-NUMBER ( #X ) TO #CNTRCT-NBR ( 2 )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*   END-READ
        //*  END-FOR
        //* ***********************************************************************
        //*  PROGRAM  : CWFC0040
        //*  SYSTEM   : PROJCWF
        //*  FUNCTION : THIS COPYCODE RETURNS AN ERROR MESSAGE AND AN 'E'
        //*           | ON THE ##RETURN-CODE WHEN A NATURAL ERROR OCCURS
        //*           |
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
        //* *********** CWFC0040
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Natural Error", Global.getERROR_NR(), "on line", Global.getERROR_LINE(),                     //Natural: COMPRESS 'Natural Error' *ERROR-NR 'on line' *ERROR-LINE 'of' *PROGRAM INTO MSG-INFO-SUB.##MSG
            "of", Global.getPROGRAM()));
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(Global.getERROR_NR());                                                                                      //Natural: MOVE *ERROR-NR TO MSG-INFO-SUB.##MSG-NR
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                                  //Natural: MOVE 'E' TO MSG-INFO-SUB.##RETURN-CODE
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue(Global.getPROGRAM());                                                                                  //Natural: MOVE *PROGRAM TO MSG-INFO-SUB.##ERROR-FIELD
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
}
