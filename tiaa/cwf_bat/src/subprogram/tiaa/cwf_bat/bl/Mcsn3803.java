/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:25:29 AM
**        * FROM NATURAL SUBPROGRAM : Mcsn3803
************************************************************
**        * FILE NAME            : Mcsn3803.java
**        * CLASS NAME           : Mcsn3803
**        * INSTANCE NAME        : Mcsn3803
************************************************************
************************************************************************
* PROGRAM  : MCSN3803
* TITLE    : ACCESS FULFILLMENT RECORDS FROM THE CWF-ATI-FULFILLMENT
* FUNCTION : DISPLAY ATI-FULFILLMENT RECORDS ON 192 IN WPID ORDER
*          : FOR FFT SYSTEM
* HISTORY  :
* 02/14/03 : DIL - ADDED INSTITUTIONAL ATIS AND DISPLAY OF NPIN-NUMBER
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsn3803 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;

    private DataAccessProgramView vw_cwf_Ati_Fulfill;
    private DbsField cwf_Ati_Fulfill_Ati_Rcrd_Type;

    private DbsGroup cwf_Ati_Fulfill_Ati_Rqst_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Pin;
    private DbsField cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Stts;
    private DbsField cwf_Ati_Fulfill_Ati_Wpid;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Racf_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Unit_Cde;
    private DbsField cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Error_Txt;
    private DbsField cwf_Ati_Fulfill_Ati_Prge_Ind;
    private DbsField cwf_Ati_Fulfill_Ati_Stts_Dte_Tme;
    private DbsField cwf_Ati_Fulfill_Ati_Cmtask_Ind;
    private DbsField cwf_Ati_Fulfill_Fft_Ordr_Id;
    private DbsField cwf_Ati_Fulfill_Fft_Last_Name;
    private DbsField cwf_Ati_Fulfill_Fft_Non_Particpant_Ind;
    private DbsField cwf_Ati_Fulfill_Fft_Rqst_Orgn_Cde;
    private DbsField cwf_Ati_Fulfill_Fft_Rush_Ind;
    private DbsField cwf_Ati_Fulfill_Fft_Spcl_Hndlng_Ind;
    private DbsField cwf_Ati_Fulfill_Fft_Spcl_Rmrks;
    private DbsField cwf_Ati_Fulfill_Fft_Addrss_Chnge_Ind;
    private DbsField cwf_Ati_Fulfill_Fft_Addrss_Typ_Cde;

    private DbsGroup cwf_Ati_Fulfill_Fft_Order_Items;
    private DbsField cwf_Ati_Fulfill_Fft_Unique_Itm_Id;
    private DbsField cwf_Ati_Fulfill_Fft_Qty_Requested;
    private DbsField cwf_Ati_Fulfill_Fft_Ovrflw_Ind;
    private DbsField cwf_Ati_Fulfill_Fft_Shipping_Method;
    private DbsField cwf_Ati_Fulfill_Ati_Wfo_Ind;
    private DbsField cwf_Ati_Fulfill_Ati_Inst_Id;
    private DbsField cwf_Ati_Fulfill_Ati_Npir_Id;

    private DataAccessProgramView vw_cwf_Ati_Get;
    private DbsField cwf_Ati_Get_Ati_Rcrd_Type;

    private DbsGroup cwf_Ati_Get_Ati_Rqst_Id;
    private DbsField cwf_Ati_Get_Ati_Pin;
    private DbsField cwf_Ati_Get_Ati_Mit_Log_Dt_Tme;

    private DbsGroup cwf_Ati_Get_Fft_Order_Items;
    private DbsField cwf_Ati_Get_Fft_Unique_Itm_Id;
    private DbsField cwf_Ati_Get_Fft_Qty_Requested;
    private DbsField pnd_Ati_Mit_Key;

    private DbsGroup pnd_Ati_Mit_Key__R_Field_1;
    private DbsField pnd_Ati_Mit_Key_Pnd_Ati_Pin;
    private DbsField pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme;
    private DbsField pnd_Ati_Fulfill_Key;

    private DbsGroup pnd_Ati_Fulfill_Key__R_Field_2;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme;
    private DbsField pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts;
    private DbsField pnd_Grand_Total_Open_Atis;
    private DbsField pnd_Grand_Total_Inprogress_Atis;
    private DbsField pnd_Grand_Total_Failed_Atis;
    private DbsField pnd_Grand_Total_Succesfull_Atis;
    private DbsField pnd_Interim_Dollar;
    private DbsField pnd_Interim_Prcnt;
    private DbsField pnd_Interim_Units;
    private DbsField pnd_Interim_Acct_Name;
    private DbsField pnd_Process_Status_Literal;
    private DbsField pnd_Cmtask_Status_Literal;
    private DbsField pnd_Compressed_Text;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_Cc;
    private DbsField pnd_Prev_Wpid;
    private DbsField pnd_Wpid_Total;
    private DbsField pnd_Fld_Line;
    private DbsField pnd_Fld_Strt;
    private DbsField pnd_Fld_End;
    private DbsField pnd_Accnt_Amt_Type;
    private DbsField pnd_Field;
    private DbsField pnd_Title_Literal;
    private DbsField pnd_Input_Dte_Tme;
    private DbsField pnd_Input_Dte_D;
    private DbsField pnd_Orig_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Entry_Date;
    private DbsField pnd_Last_Page_Flag;
    private DbsField pnd_Mit_Log_Date;
    private DbsField pnd_Id_Number;
    private DbsField pnd_Npart_Inst_Flg;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Input_Data = parameters.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Ati_Fulfill = new DataAccessProgramView(new NameInfo("vw_cwf_Ati_Fulfill", "CWF-ATI-FULFILL"), "CWF_ATI_FULFILL", "CWF_KDO_FULFILL", DdmPeriodicGroups.getInstance().getGroups("CWF_ATI_FULFILL"));
        cwf_Ati_Fulfill_Ati_Rcrd_Type = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Rcrd_Type", "ATI-RCRD-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_RCRD_TYPE");

        cwf_Ati_Fulfill_Ati_Rqst_Id = vw_cwf_Ati_Fulfill.getRecord().newGroupInGroup("CWF_ATI_FULFILL_ATI_RQST_ID", "ATI-RQST-ID");
        cwf_Ati_Fulfill_Ati_Pin = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Pin", "ATI-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ATI_PIN");
        cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme = cwf_Ati_Fulfill_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme", "ATI-MIT-LOG-DT-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_MIT_LOG_DT_TME");
        cwf_Ati_Fulfill_Ati_Prcss_Stts = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Stts", "ATI-PRCSS-STTS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRCSS_STTS");
        cwf_Ati_Fulfill_Ati_Wpid = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Wpid", "ATI-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ATI_WPID");
        cwf_Ati_Fulfill_Ati_Entry_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Dte_Tme", "ATI-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_ENTRY_DTE_TME");
        cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt", "ATI-ENTRY-SYSTM-OR-UNT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_SYSTM_OR_UNT");
        cwf_Ati_Fulfill_Ati_Entry_Racf_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Racf_Id", "ATI-ENTRY-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ATI_ENTRY_RACF_ID");
        cwf_Ati_Fulfill_Ati_Entry_Unit_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Unit_Cde", "ATI-ENTRY-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_ENTRY_UNIT_CDE");
        cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id", "ATI-PRCSS-APPLCTN-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATI_PRCSS_APPLCTN_ID");
        cwf_Ati_Fulfill_Ati_Error_Txt = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Error_Txt", "ATI-ERROR-TXT", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "ATI_ERROR_TXT");
        cwf_Ati_Fulfill_Ati_Prge_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Prge_Ind", "ATI-PRGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_PRGE_IND");
        cwf_Ati_Fulfill_Ati_Stts_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Stts_Dte_Tme", "ATI-STTS-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_STTS_DTE_TME");
        cwf_Ati_Fulfill_Ati_Cmtask_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Cmtask_Ind", "ATI-CMTASK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ATI_CMTASK_IND");
        cwf_Ati_Fulfill_Fft_Ordr_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Fft_Ordr_Id", "FFT-ORDR-ID", FieldType.STRING, 11, 
            RepeatingFieldStrategy.None, "FFT_ORDR_ID");
        cwf_Ati_Fulfill_Fft_Ordr_Id.setDdmHeader("ORDER/ID");
        cwf_Ati_Fulfill_Fft_Last_Name = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Fft_Last_Name", "FFT-LAST-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "FFT_LAST_NAME");
        cwf_Ati_Fulfill_Fft_Non_Particpant_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Fft_Non_Particpant_Ind", "FFT-NON-PARTICPANT-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "FFT_NON_PARTICPANT_IND");
        cwf_Ati_Fulfill_Fft_Rqst_Orgn_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Fft_Rqst_Orgn_Cde", "FFT-RQST-ORGN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "FFT_RQST_ORGN_CDE");
        cwf_Ati_Fulfill_Fft_Rqst_Orgn_Cde.setDdmHeader("REQUEST/ORIGIN/CODE");
        cwf_Ati_Fulfill_Fft_Rush_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Fft_Rush_Ind", "FFT-RUSH-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "FFT_RUSH_IND");
        cwf_Ati_Fulfill_Fft_Rush_Ind.setDdmHeader("RUSH/IND");
        cwf_Ati_Fulfill_Fft_Spcl_Hndlng_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Fft_Spcl_Hndlng_Ind", "FFT-SPCL-HNDLNG-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "FFT_SPCL_HNDLNG_IND");
        cwf_Ati_Fulfill_Fft_Spcl_Hndlng_Ind.setDdmHeader("SPECIAL/HANDLE/IND");
        cwf_Ati_Fulfill_Fft_Spcl_Rmrks = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Fft_Spcl_Rmrks", "FFT-SPCL-RMRKS", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "FFT_SPCL_RMRKS");
        cwf_Ati_Fulfill_Fft_Spcl_Rmrks.setDdmHeader("SPECIAL/REMARKS");
        cwf_Ati_Fulfill_Fft_Addrss_Chnge_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Fft_Addrss_Chnge_Ind", "FFT-ADDRSS-CHNGE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "FFT_ADDRSS_CHNGE_IND");
        cwf_Ati_Fulfill_Fft_Addrss_Chnge_Ind.setDdmHeader("ADDRESS/CHANGE/IND");
        cwf_Ati_Fulfill_Fft_Addrss_Typ_Cde = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Fft_Addrss_Typ_Cde", "FFT-ADDRSS-TYP-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "FFT_ADDRSS_TYP_CDE");
        cwf_Ati_Fulfill_Fft_Addrss_Typ_Cde.setDdmHeader("ADDRESS/TYPE/CODE");

        cwf_Ati_Fulfill_Fft_Order_Items = vw_cwf_Ati_Fulfill.getRecord().newGroupArrayInGroup("cwf_Ati_Fulfill_Fft_Order_Items", "FFT-ORDER-ITEMS", new 
            DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_KDO_FULFILL_FFT_ORDER_ITEMS");
        cwf_Ati_Fulfill_Fft_Unique_Itm_Id = cwf_Ati_Fulfill_Fft_Order_Items.newFieldInGroup("cwf_Ati_Fulfill_Fft_Unique_Itm_Id", "FFT-UNIQUE-ITM-ID", 
            FieldType.STRING, 12, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "FFT_UNIQUE_ITM_ID", "CWF_KDO_FULFILL_FFT_ORDER_ITEMS");
        cwf_Ati_Fulfill_Fft_Qty_Requested = cwf_Ati_Fulfill_Fft_Order_Items.newFieldInGroup("cwf_Ati_Fulfill_Fft_Qty_Requested", "FFT-QTY-REQUESTED", 
            FieldType.PACKED_DECIMAL, 5, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "FFT_QTY_REQUESTED", "CWF_KDO_FULFILL_FFT_ORDER_ITEMS");
        cwf_Ati_Fulfill_Fft_Ovrflw_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Fft_Ovrflw_Ind", "FFT-OVRFLW-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "FFT_OVRFLW_IND");
        cwf_Ati_Fulfill_Fft_Shipping_Method = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Fft_Shipping_Method", "FFT-SHIPPING-METHOD", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "FFT_SHIPPING_METHOD");
        cwf_Ati_Fulfill_Ati_Wfo_Ind = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Wfo_Ind", "ATI-WFO-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "ATI_WFO_IND");
        cwf_Ati_Fulfill_Ati_Inst_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Inst_Id", "ATI-INST-ID", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "ATI_INST_ID");
        cwf_Ati_Fulfill_Ati_Npir_Id = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Npir_Id", "ATI-NPIR-ID", FieldType.STRING, 7, 
            RepeatingFieldStrategy.None, "ATI_NPIR_ID");
        registerRecord(vw_cwf_Ati_Fulfill);

        vw_cwf_Ati_Get = new DataAccessProgramView(new NameInfo("vw_cwf_Ati_Get", "CWF-ATI-GET"), "CWF_ATI_FULFILL_FFT", "CWF_KDO_FULFILL", DdmPeriodicGroups.getInstance().getGroups("CWF_ATI_FULFILL_FFT"));
        cwf_Ati_Get_Ati_Rcrd_Type = vw_cwf_Ati_Get.getRecord().newFieldInGroup("cwf_Ati_Get_Ati_Rcrd_Type", "ATI-RCRD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ATI_RCRD_TYPE");

        cwf_Ati_Get_Ati_Rqst_Id = vw_cwf_Ati_Get.getRecord().newGroupInGroup("CWF_ATI_GET_ATI_RQST_ID", "ATI-RQST-ID");
        cwf_Ati_Get_Ati_Pin = cwf_Ati_Get_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Get_Ati_Pin", "ATI-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ATI_PIN");
        cwf_Ati_Get_Ati_Mit_Log_Dt_Tme = cwf_Ati_Get_Ati_Rqst_Id.newFieldInGroup("cwf_Ati_Get_Ati_Mit_Log_Dt_Tme", "ATI-MIT-LOG-DT-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_MIT_LOG_DT_TME");

        cwf_Ati_Get_Fft_Order_Items = vw_cwf_Ati_Get.getRecord().newGroupArrayInGroup("cwf_Ati_Get_Fft_Order_Items", "FFT-ORDER-ITEMS", new DbsArrayController(1, 
            60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_KDO_FULFILL_FFT_ORDER_ITEMS");
        cwf_Ati_Get_Fft_Unique_Itm_Id = cwf_Ati_Get_Fft_Order_Items.newFieldInGroup("cwf_Ati_Get_Fft_Unique_Itm_Id", "FFT-UNIQUE-ITM-ID", FieldType.STRING, 
            12, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "FFT_UNIQUE_ITM_ID", "CWF_KDO_FULFILL_FFT_ORDER_ITEMS");
        cwf_Ati_Get_Fft_Qty_Requested = cwf_Ati_Get_Fft_Order_Items.newFieldInGroup("cwf_Ati_Get_Fft_Qty_Requested", "FFT-QTY-REQUESTED", FieldType.PACKED_DECIMAL, 
            5, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "FFT_QTY_REQUESTED", "CWF_KDO_FULFILL_FFT_ORDER_ITEMS");
        registerRecord(vw_cwf_Ati_Get);

        pnd_Ati_Mit_Key = localVariables.newFieldInRecord("pnd_Ati_Mit_Key", "#ATI-MIT-KEY", FieldType.BINARY, 19);

        pnd_Ati_Mit_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Ati_Mit_Key__R_Field_1", "REDEFINE", pnd_Ati_Mit_Key);
        pnd_Ati_Mit_Key_Pnd_Ati_Pin = pnd_Ati_Mit_Key__R_Field_1.newFieldInGroup("pnd_Ati_Mit_Key_Pnd_Ati_Pin", "#ATI-PIN", FieldType.NUMERIC, 12);
        pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme = pnd_Ati_Mit_Key__R_Field_1.newFieldInGroup("pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme", "#ATI-LOG-DTE-TME", FieldType.TIME);
        pnd_Ati_Fulfill_Key = localVariables.newFieldInRecord("pnd_Ati_Fulfill_Key", "#ATI-FULFILL-KEY", FieldType.STRING, 9);

        pnd_Ati_Fulfill_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Ati_Fulfill_Key__R_Field_2", "REDEFINE", pnd_Ati_Fulfill_Key);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl = pnd_Ati_Fulfill_Key__R_Field_2.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl", "#ATI-RCRD-TYPE-CNTRL", 
            FieldType.STRING, 1);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme = pnd_Ati_Fulfill_Key__R_Field_2.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme", "#ATI-ENTRY-DTE-TME", 
            FieldType.TIME);
        pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts = pnd_Ati_Fulfill_Key__R_Field_2.newFieldInGroup("pnd_Ati_Fulfill_Key_Pnd_Ati_Prcss_Stts", "#ATI-PRCSS-STTS", 
            FieldType.STRING, 1);
        pnd_Grand_Total_Open_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Open_Atis", "#GRAND-TOTAL-OPEN-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Inprogress_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Inprogress_Atis", "#GRAND-TOTAL-INPROGRESS-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Grand_Total_Failed_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Failed_Atis", "#GRAND-TOTAL-FAILED-ATIS", FieldType.NUMERIC, 5);
        pnd_Grand_Total_Succesfull_Atis = localVariables.newFieldInRecord("pnd_Grand_Total_Succesfull_Atis", "#GRAND-TOTAL-SUCCESFULL-ATIS", FieldType.NUMERIC, 
            5);
        pnd_Interim_Dollar = localVariables.newFieldInRecord("pnd_Interim_Dollar", "#INTERIM-DOLLAR", FieldType.NUMERIC, 12, 2);
        pnd_Interim_Prcnt = localVariables.newFieldInRecord("pnd_Interim_Prcnt", "#INTERIM-PRCNT", FieldType.NUMERIC, 12, 3);
        pnd_Interim_Units = localVariables.newFieldInRecord("pnd_Interim_Units", "#INTERIM-UNITS", FieldType.NUMERIC, 12, 3);
        pnd_Interim_Acct_Name = localVariables.newFieldInRecord("pnd_Interim_Acct_Name", "#INTERIM-ACCT-NAME", FieldType.STRING, 11);
        pnd_Process_Status_Literal = localVariables.newFieldInRecord("pnd_Process_Status_Literal", "#PROCESS-STATUS-LITERAL", FieldType.STRING, 7);
        pnd_Cmtask_Status_Literal = localVariables.newFieldInRecord("pnd_Cmtask_Status_Literal", "#CMTASK-STATUS-LITERAL", FieldType.STRING, 27);
        pnd_Compressed_Text = localVariables.newFieldInRecord("pnd_Compressed_Text", "#COMPRESSED-TEXT", FieldType.STRING, 38);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 3);
        pnd_Cc = localVariables.newFieldInRecord("pnd_Cc", "#CC", FieldType.NUMERIC, 3);
        pnd_Prev_Wpid = localVariables.newFieldInRecord("pnd_Prev_Wpid", "#PREV-WPID", FieldType.STRING, 6);
        pnd_Wpid_Total = localVariables.newFieldInRecord("pnd_Wpid_Total", "#WPID-TOTAL", FieldType.NUMERIC, 8);
        pnd_Fld_Line = localVariables.newFieldInRecord("pnd_Fld_Line", "#FLD-LINE", FieldType.NUMERIC, 3);
        pnd_Fld_Strt = localVariables.newFieldInRecord("pnd_Fld_Strt", "#FLD-STRT", FieldType.NUMERIC, 3);
        pnd_Fld_End = localVariables.newFieldInRecord("pnd_Fld_End", "#FLD-END", FieldType.NUMERIC, 3);
        pnd_Accnt_Amt_Type = localVariables.newFieldInRecord("pnd_Accnt_Amt_Type", "#ACCNT-AMT-TYPE", FieldType.STRING, 1);
        pnd_Field = localVariables.newFieldInRecord("pnd_Field", "#FIELD", FieldType.STRING, 30);
        pnd_Title_Literal = localVariables.newFieldInRecord("pnd_Title_Literal", "#TITLE-LITERAL", FieldType.STRING, 54);
        pnd_Input_Dte_Tme = localVariables.newFieldInRecord("pnd_Input_Dte_Tme", "#INPUT-DTE-TME", FieldType.TIME);
        pnd_Input_Dte_D = localVariables.newFieldInRecord("pnd_Input_Dte_D", "#INPUT-DTE-D", FieldType.DATE);
        pnd_Orig_Date = localVariables.newFieldInRecord("pnd_Orig_Date", "#ORIG-DATE", FieldType.DATE);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 8);
        pnd_Entry_Date = localVariables.newFieldInRecord("pnd_Entry_Date", "#ENTRY-DATE", FieldType.DATE);
        pnd_Last_Page_Flag = localVariables.newFieldInRecord("pnd_Last_Page_Flag", "#LAST-PAGE-FLAG", FieldType.STRING, 1);
        pnd_Mit_Log_Date = localVariables.newFieldInRecord("pnd_Mit_Log_Date", "#MIT-LOG-DATE", FieldType.NUMERIC, 14);
        pnd_Id_Number = localVariables.newFieldInRecord("pnd_Id_Number", "#ID-NUMBER", FieldType.STRING, 12);
        pnd_Npart_Inst_Flg = localVariables.newFieldInRecord("pnd_Npart_Inst_Flg", "#NPART-INST-FLG", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Ati_Fulfill.reset();
        vw_cwf_Ati_Get.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Mcsn3803() throws Exception
    {
        super("Mcsn3803");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atEndOfPage(atEndEventRpt3, 3);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 1 ) PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 2 ) PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 3 ) PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 4 ) PS = 60 LS = 133 ZP = OFF
        pnd_Last_Page_Flag.reset();                                                                                                                                       //Natural: RESET #LAST-PAGE-FLAG #GRAND-TOTAL-OPEN-ATIS #GRAND-TOTAL-INPROGRESS-ATIS #GRAND-TOTAL-FAILED-ATIS #GRAND-TOTAL-SUCCESFULL-ATIS
        pnd_Grand_Total_Open_Atis.reset();
        pnd_Grand_Total_Inprogress_Atis.reset();
        pnd_Grand_Total_Failed_Atis.reset();
        pnd_Grand_Total_Succesfull_Atis.reset();
        if (condition(pnd_Input_Data_Pnd_Input_Date.notEquals(" ")))                                                                                                      //Natural: IF #INPUT-DATE NE ' '
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Input_Dte_Tme.setValue(pnd_Input_Dte_D);                                                                                                                  //Natural: MOVE #INPUT-DTE-D TO #INPUT-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***");                                     //Natural: WRITE ( 3 ) /// '*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***'
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
        //* *** INITIALIZE THE ATI-CONTROL-KEY SUPER-DESCRIPTOR FIELD
        pnd_Ati_Fulfill_Key_Pnd_Ati_Rcrd_Type_Cntrl.setValue("3");                                                                                                        //Natural: ASSIGN #ATI-RCRD-TYPE-CNTRL := '3'
        pnd_Ati_Fulfill_Key_Pnd_Ati_Entry_Dte_Tme.reset();                                                                                                                //Natural: RESET #ATI-ENTRY-DTE-TME
        //* *****************************************************************
        //* ** READ CWF-ATI-FULFILL AND PROCESS OPEN AND IN-PROGRESS REQUESTS
        //* *****************************************************************
        pnd_Title_Literal.setValue("**** OPEN/IN-PROGRESS ATI REQUESTS FOR FFT FORMS ****");                                                                              //Natural: MOVE '**** OPEN/IN-PROGRESS ATI REQUESTS FOR FFT FORMS ****' TO #TITLE-LITERAL
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        vw_cwf_Ati_Fulfill.startDatabaseRead                                                                                                                              //Natural: READ CWF-ATI-FULFILL BY ATI-FULFILL-KEY STARTING FROM #ATI-FULFILL-KEY
        (
        "READ_ATI",
        new Wc[] { new Wc("ATI_FULFILL_KEY", ">=", pnd_Ati_Fulfill_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("ATI_FULFILL_KEY", "ASC") }
        );
        READ_ATI:
        while (condition(vw_cwf_Ati_Fulfill.readNextRow("READ_ATI")))
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            pnd_Entry_Date.setValue(cwf_Ati_Fulfill_Ati_Stts_Dte_Tme);                                                                                                    //Natural: MOVE CWF-ATI-FULFILL.ATI-STTS-DTE-TME TO #ENTRY-DATE
            if (condition(!(pnd_Entry_Date.equals(pnd_Input_Dte_D) && cwf_Ati_Fulfill_Ati_Prcss_Applctn_Id.equals("FFT"))))                                               //Natural: ACCEPT IF #ENTRY-DATE = #INPUT-DTE-D AND ATI-PRCSS-APPLCTN-ID = 'FFT'
            {
                continue;
            }
            getSort().writeSortInData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type,               //Natural: END-ALL
                cwf_Ati_Fulfill_Ati_Prcss_Stts, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, 
                cwf_Ati_Fulfill_Ati_Entry_Racf_Id, cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Fft_Non_Particpant_Ind, 
                cwf_Ati_Fulfill_Fft_Rqst_Orgn_Cde, cwf_Ati_Fulfill_Fft_Rush_Ind, cwf_Ati_Fulfill_Fft_Spcl_Hndlng_Ind, cwf_Ati_Fulfill_Fft_Spcl_Rmrks, cwf_Ati_Fulfill_Fft_Addrss_Chnge_Ind, 
                cwf_Ati_Fulfill_Fft_Addrss_Typ_Cde, cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(1), cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(2), cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(3), 
                cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(4), cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(5), cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(6), 
                cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(7), cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(8), cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(9), 
                cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(10), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(1), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(2), 
                cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(3), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(4), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(5), 
                cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(6), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(7), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(8), 
                cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(9), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(10), cwf_Ati_Fulfill_Fft_Ovrflw_Ind, cwf_Ati_Fulfill_Fft_Shipping_Method, 
                cwf_Ati_Fulfill_Ati_Npir_Id, cwf_Ati_Fulfill_Ati_Wfo_Ind, cwf_Ati_Fulfill_Ati_Inst_Id);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  SORT PERFORMED TO WRITE THEM PIN/MIT-LOG-DATE-TIME/REC-TYPE ORDER
        //*  DIL - 02/14/03
        //*  DIL - END
        getSort().sortData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type, cwf_Ati_Fulfill_Ati_Prcss_Stts); //Natural: SORT RECORDS BY CWF-ATI-FULFILL.ATI-WPID CWF-ATI-FULFILL.ATI-PIN CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME CWF-ATI-FULFILL.ATI-RCRD-TYPE CWF-ATI-FULFILL.ATI-PRCSS-STTS USING CWF-ATI-FULFILL.ATI-ERROR-TXT CWF-ATI-FULFILL.ATI-ENTRY-DTE-TME CWF-ATI-FULFILL.ATI-ENTRY-SYSTM-OR-UNT CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE CWF-ATI-FULFILL.ATI-STTS-DTE-TME CWF-ATI-FULFILL.FFT-NON-PARTICPANT-IND CWF-ATI-FULFILL.FFT-RQST-ORGN-CDE CWF-ATI-FULFILL.FFT-RUSH-IND CWF-ATI-FULFILL.FFT-SPCL-HNDLNG-IND CWF-ATI-FULFILL.FFT-SPCL-RMRKS CWF-ATI-FULFILL.FFT-ADDRSS-CHNGE-IND CWF-ATI-FULFILL.FFT-ADDRSS-TYP-CDE CWF-ATI-FULFILL.FFT-UNIQUE-ITM-ID ( * ) CWF-ATI-FULFILL.FFT-QTY-REQUESTED ( * ) CWF-ATI-FULFILL.FFT-OVRFLW-IND CWF-ATI-FULFILL.FFT-SHIPPING-METHOD CWF-ATI-FULFILL.ATI-NPIR-ID CWF-ATI-FULFILL.ATI-WFO-IND CWF-ATI-FULFILL.ATI-INST-ID
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Ati_Fulfill_Ati_Wpid, cwf_Ati_Fulfill_Ati_Pin, cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme, cwf_Ati_Fulfill_Ati_Rcrd_Type, 
            cwf_Ati_Fulfill_Ati_Prcss_Stts, cwf_Ati_Fulfill_Ati_Error_Txt, cwf_Ati_Fulfill_Ati_Entry_Dte_Tme, cwf_Ati_Fulfill_Ati_Entry_Systm_Or_Unt, cwf_Ati_Fulfill_Ati_Entry_Racf_Id, 
            cwf_Ati_Fulfill_Ati_Entry_Unit_Cde, cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, cwf_Ati_Fulfill_Fft_Non_Particpant_Ind, cwf_Ati_Fulfill_Fft_Rqst_Orgn_Cde, 
            cwf_Ati_Fulfill_Fft_Rush_Ind, cwf_Ati_Fulfill_Fft_Spcl_Hndlng_Ind, cwf_Ati_Fulfill_Fft_Spcl_Rmrks, cwf_Ati_Fulfill_Fft_Addrss_Chnge_Ind, cwf_Ati_Fulfill_Fft_Addrss_Typ_Cde, 
            cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(1), cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(2), cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(3), 
            cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(4), cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(5), cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(6), 
            cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(7), cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(8), cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(9), 
            cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(10), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(1), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(2), 
            cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(3), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(4), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(5), 
            cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(6), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(7), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(8), 
            cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(9), cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(10), cwf_Ati_Fulfill_Fft_Ovrflw_Ind, cwf_Ati_Fulfill_Fft_Shipping_Method, 
            cwf_Ati_Fulfill_Ati_Npir_Id, cwf_Ati_Fulfill_Ati_Wfo_Ind, cwf_Ati_Fulfill_Ati_Inst_Id)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF CWF-ATI-FULFILL.ATI-WPID
            pnd_Prev_Wpid.setValue(cwf_Ati_Fulfill_Ati_Wpid);                                                                                                             //Natural: ASSIGN #PREV-WPID := CWF-ATI-FULFILL.ATI-WPID
            pnd_Wpid_Total.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WPID-TOTAL
                                                                                                                                                                          //Natural: PERFORM ATI-WRITE-REC-1
            sub_Ati_Write_Rec_1();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  END PROCESSING OF OPEN AND IN-PROGRESS REQUEST
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //*  ----------------------------------------------------------------------
        //*  4T CWF-ATI-FULFILL.ATI-PIN
        //*  4T #ID-NUMBER
        //*  20T CWF-ATI-FULFILL.ATI-WPID
        //*  27T CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  43T CWF-ATI-FULFILL.ATI-ENTRY-DTE-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  48T CWF-ATI-FULFILL.ATI-STTS-DTE-TME(EM=MM/DD/YYYY' 'HH:II:SS)
        //*  68T #PROCESS-STATUS-LITERAL
        //*  77T FFT-SPCL-HNDLNG-IND
        //*  82T FFT-ADDRSS-CHNGE-IND
        //*  87T FFT-RUSH-IND
        //*  91T FFT-SHIPPING-METHOD
        //*  97T FFT-OVRFLW-IND
        //*  102T FFT-RQST-ORGN-CDE
        //*  106T FFT-ORDR-ID
        //*  120T FFT-NON-PARTICPANT-IND
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        //*                                                                                                                                                               //Natural: AT END OF PAGE ( 3 )
        //* ***********************************************************************
        //*   WRITE OUT ATI GRAND TOTALS ON LAST PAGE OF REPORT
        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        pnd_Last_Page_Flag.setValue("Y");                                                                                                                                 //Natural: ASSIGN #LAST-PAGE-FLAG := 'Y'
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,new  //Natural: WRITE ( 3 ) //////// 1T '-' ( 131 ) // 1T '-' ( 50 ) 52T '   ATI (FFT) GRAND TOTALS   ' 82T '-' ( 51 ) // 1T '-' ( 131 ) //// 45T 'OPEN ATIS        :' #GRAND-TOTAL-OPEN-ATIS / 45T 'IN-PROGRESS ATIS :' #GRAND-TOTAL-INPROGRESS-ATIS / 45T 'SUCCESSFULL ATIS :' #GRAND-TOTAL-SUCCESFULL-ATIS / 45T 'FAILED ATIS      :' #GRAND-TOTAL-FAILED-ATIS
            TabSetting(1),"-",new RepeatItem(50),new TabSetting(52),"   ATI (FFT) GRAND TOTALS   ",new TabSetting(82),"-",new RepeatItem(51),NEWLINE,NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"OPEN ATIS        :",pnd_Grand_Total_Open_Atis,NEWLINE,new 
            TabSetting(45),"IN-PROGRESS ATIS :",pnd_Grand_Total_Inprogress_Atis,NEWLINE,new TabSetting(45),"SUCCESSFULL ATIS :",pnd_Grand_Total_Succesfull_Atis,NEWLINE,new 
            TabSetting(45),"FAILED ATIS      :",pnd_Grand_Total_Failed_Atis);
        if (Global.isEscape()) return;
    }
    //*  START ATI-WRITE-REC-1 PROCESSING
    private void sub_Ati_Write_Rec_1() throws Exception                                                                                                                   //Natural: ATI-WRITE-REC-1
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM SET-PROCESS-STATUS
        sub_Set_Process_Status();
        if (condition(Global.isEscape())) {return;}
        if (condition(getReports().getAstLinesLeft(3).less(15)))                                                                                                          //Natural: NEWPAGE ( 3 ) IF LESS THAN 15 LINES LEFT
        {
            getReports().newPage(3);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Compressed_Text.reset();                                                                                                                                      //Natural: RESET #COMPRESSED-TEXT
        pnd_Compressed_Text.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "ENTRY RACF-ID/UNIT= ", cwf_Ati_Fulfill_Ati_Entry_Racf_Id, "/", cwf_Ati_Fulfill_Ati_Entry_Unit_Cde)); //Natural: COMPRESS 'ENTRY RACF-ID/UNIT= ' CWF-ATI-FULFILL.ATI-ENTRY-RACF-ID '/' CWF-ATI-FULFILL.ATI-ENTRY-UNIT-CDE INTO #COMPRESSED-TEXT LEAVING NO SPACE
        //* * --------------
        //* * DIL - 02/14/03
        //* * --------------
        //*  INSTITUTION
        short decideConditionsMet277 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-ATI-FULFILL.ATI-WFO-IND = 'I'
        if (condition(cwf_Ati_Fulfill_Ati_Wfo_Ind.equals("I")))
        {
            decideConditionsMet277++;
            pnd_Npart_Inst_Flg.setValue("I");                                                                                                                             //Natural: ASSIGN #NPART-INST-FLG = 'I'
            //*  NON-PART
            pnd_Id_Number.setValue(cwf_Ati_Fulfill_Ati_Inst_Id);                                                                                                          //Natural: ASSIGN #ID-NUMBER = CWF-ATI-FULFILL.ATI-INST-ID
        }                                                                                                                                                                 //Natural: WHEN CWF-ATI-FULFILL.ATI-WFO-IND = 'N'
        else if (condition(cwf_Ati_Fulfill_Ati_Wfo_Ind.equals("N")))
        {
            decideConditionsMet277++;
            pnd_Npart_Inst_Flg.setValue("N");                                                                                                                             //Natural: ASSIGN #NPART-INST-FLG = 'N'
            //*  ASSUME PARTICIPANT
            pnd_Id_Number.setValue(cwf_Ati_Fulfill_Ati_Npir_Id);                                                                                                          //Natural: ASSIGN #ID-NUMBER = CWF-ATI-FULFILL.ATI-NPIR-ID
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Npart_Inst_Flg.reset();                                                                                                                                   //Natural: RESET #NPART-INST-FLG
            pnd_Id_Number.setValue(cwf_Ati_Fulfill_Ati_Pin);                                                                                                              //Natural: ASSIGN #ID-NUMBER = CWF-ATI-FULFILL.ATI-PIN
            //*  PIN-EXP
            //*   PIN-EXP>>
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Id_Number,new TabSetting(15),cwf_Ati_Fulfill_Ati_Wpid,new TabSetting(22),cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme,  //Natural: WRITE ( 3 ) / 1T #ID-NUMBER 15T CWF-ATI-FULFILL.ATI-WPID 22T CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 43T CWF-ATI-FULFILL.ATI-STTS-DTE-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 63T #PROCESS-STATUS-LITERAL 72T FFT-SPCL-HNDLNG-IND 77T FFT-ADDRSS-CHNGE-IND 82T FFT-RUSH-IND 86T FFT-SHIPPING-METHOD 92T FFT-OVRFLW-IND 97T FFT-RQST-ORGN-CDE 101T FFT-ORDR-ID 118T #NPART-INST-FLG 126T FFT-ADDRSS-TYP-CDE
            new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(43),cwf_Ati_Fulfill_Ati_Stts_Dte_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new 
            TabSetting(63),pnd_Process_Status_Literal,new TabSetting(72),cwf_Ati_Fulfill_Fft_Spcl_Hndlng_Ind,new TabSetting(77),cwf_Ati_Fulfill_Fft_Addrss_Chnge_Ind,new 
            TabSetting(82),cwf_Ati_Fulfill_Fft_Rush_Ind,new TabSetting(86),cwf_Ati_Fulfill_Fft_Shipping_Method,new TabSetting(92),cwf_Ati_Fulfill_Fft_Ovrflw_Ind,new 
            TabSetting(97),cwf_Ati_Fulfill_Fft_Rqst_Orgn_Cde,new TabSetting(101),cwf_Ati_Fulfill_Fft_Ordr_Id,new TabSetting(118),pnd_Npart_Inst_Flg,new 
            TabSetting(126),cwf_Ati_Fulfill_Fft_Addrss_Typ_Cde);
        if (Global.isEscape()) return;
        //*   WRITE (3)
        //*   'SPCL RMKS :' FFT-SPCL-RMRKS
        //*    'ERROR MSG :' ATI-ERROR-TXT
        if (condition(cwf_Ati_Fulfill_Ati_Prcss_Stts.equals(" ")))                                                                                                        //Natural: IF CWF-ATI-FULFILL.ATI-PRCSS-STTS = ' '
        {
            if (condition(cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))                                                                                                     //Natural: IF CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
            {
                getReports().write(3, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text);                                                                       //Natural: WRITE ( 3 ) 15T #COMPRESSED-TEXT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(3, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text,new TabSetting(55),"ERROR MESSAGE = :",new TabSetting(73),              //Natural: WRITE ( 3 ) 15T #COMPRESSED-TEXT 55T 'ERROR MESSAGE = :' 73T CWF-ATI-FULFILL.ATI-ERROR-TXT
                    cwf_Ati_Fulfill_Ati_Error_Txt);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(3, ReportOption.NOTITLE,new TabSetting(15),pnd_Compressed_Text);                                                                           //Natural: WRITE ( 3 ) 15T #COMPRESSED-TEXT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(10).equals(" ")))                                                                                        //Natural: IF CWF-ATI-FULFILL.FFT-UNIQUE-ITM-ID ( 10 ) = ' '
        {
            getReports().write(3, ReportOption.NOTITLE,new ColumnSpacing(5),cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(1),cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(1),new  //Natural: WRITE ( 3 ) 5X CWF-ATI-FULFILL.FFT-QTY-REQUESTED ( 1 ) CWF-ATI-FULFILL.FFT-UNIQUE-ITM-ID ( 1 ) 5X CWF-ATI-FULFILL.FFT-QTY-REQUESTED ( 2 ) CWF-ATI-FULFILL.FFT-UNIQUE-ITM-ID ( 2 ) 5X CWF-ATI-FULFILL.FFT-QTY-REQUESTED ( 3 ) CWF-ATI-FULFILL.FFT-UNIQUE-ITM-ID ( 3 ) 5X CWF-ATI-FULFILL.FFT-QTY-REQUESTED ( 4 ) CWF-ATI-FULFILL.FFT-UNIQUE-ITM-ID ( 4 ) 5X CWF-ATI-FULFILL.FFT-QTY-REQUESTED ( 5 ) CWF-ATI-FULFILL.FFT-UNIQUE-ITM-ID ( 5 )
                ColumnSpacing(5),cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(2),cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(2),new ColumnSpacing(5),cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(3),cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(3),new 
                ColumnSpacing(5),cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(4),cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(4),new ColumnSpacing(5),cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(5),
                cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(5));
            if (Global.isEscape()) return;
            getReports().write(3, ReportOption.NOTITLE,new ColumnSpacing(5),cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(6),cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(6),new  //Natural: WRITE ( 3 ) 5X CWF-ATI-FULFILL.FFT-QTY-REQUESTED ( 6 ) CWF-ATI-FULFILL.FFT-UNIQUE-ITM-ID ( 6 ) 5X CWF-ATI-FULFILL.FFT-QTY-REQUESTED ( 7 ) CWF-ATI-FULFILL.FFT-UNIQUE-ITM-ID ( 7 ) 5X CWF-ATI-FULFILL.FFT-QTY-REQUESTED ( 8 ) CWF-ATI-FULFILL.FFT-UNIQUE-ITM-ID ( 8 ) 5X CWF-ATI-FULFILL.FFT-QTY-REQUESTED ( 9 ) CWF-ATI-FULFILL.FFT-UNIQUE-ITM-ID ( 9 ) 5X CWF-ATI-FULFILL.FFT-QTY-REQUESTED ( 10 ) CWF-ATI-FULFILL.FFT-UNIQUE-ITM-ID ( 10 )
                ColumnSpacing(5),cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(7),cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(7),new ColumnSpacing(5),cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(8),cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(8),new 
                ColumnSpacing(5),cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(9),cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(9),new ColumnSpacing(5),cwf_Ati_Fulfill_Fft_Qty_Requested.getValue(10),
                cwf_Ati_Fulfill_Fft_Unique_Itm_Id.getValue(10));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ati_Mit_Key_Pnd_Ati_Pin.setValue(cwf_Ati_Fulfill_Ati_Pin);                                                                                                //Natural: ASSIGN #ATI-PIN := CWF-ATI-FULFILL.ATI-PIN
            pnd_Ati_Mit_Key_Pnd_Ati_Log_Dte_Tme.setValue(cwf_Ati_Fulfill_Ati_Mit_Log_Dt_Tme);                                                                             //Natural: ASSIGN #ATI-LOG-DTE-TME := CWF-ATI-FULFILL.ATI-MIT-LOG-DT-TME
            vw_cwf_Ati_Get.startDatabaseFind                                                                                                                              //Natural: FIND CWF-ATI-GET WITH ATI-MIT-KEY = #ATI-MIT-KEY
            (
            "FIND01",
            new Wc[] { new Wc("ATI_MIT_KEY", "=", pnd_Ati_Mit_Key.getBinary(), WcType.WITH) }
            );
            FIND01:
            while (condition(vw_cwf_Ati_Get.readNextRow("FIND01")))
            {
                vw_cwf_Ati_Get.setIfNotFoundControlFlag(false);
                pnd_B.setValue(1);                                                                                                                                        //Natural: ASSIGN #B := 1
                FOR01:                                                                                                                                                    //Natural: FOR #A = 1 TO 12
                for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(12)); pnd_A.nadd(1))
                {
                    if (condition(cwf_Ati_Get_Fft_Qty_Requested.getValue(pnd_B).notEquals(getZero())))                                                                    //Natural: IF CWF-ATI-GET.FFT-QTY-REQUESTED ( #B ) NE 0
                    {
                        getReports().write(3, ReportOption.NOTITLE,new ColumnSpacing(5),cwf_Ati_Get_Fft_Qty_Requested.getValue(pnd_B),cwf_Ati_Get_Fft_Unique_Itm_Id.getValue(pnd_B),new  //Natural: WRITE ( 3 ) 5X CWF-ATI-GET.FFT-QTY-REQUESTED ( #B ) CWF-ATI-GET.FFT-UNIQUE-ITM-ID ( #B ) 5X CWF-ATI-GET.FFT-QTY-REQUESTED ( #B+1 ) CWF-ATI-GET.FFT-UNIQUE-ITM-ID ( #B+1 ) 5X CWF-ATI-GET.FFT-QTY-REQUESTED ( #B+2 ) CWF-ATI-GET.FFT-UNIQUE-ITM-ID ( #B+2 ) 5X CWF-ATI-GET.FFT-QTY-REQUESTED ( #B+3 ) CWF-ATI-GET.FFT-UNIQUE-ITM-ID ( #B+3 ) 5X CWF-ATI-GET.FFT-QTY-REQUESTED ( #B+4 ) CWF-ATI-GET.FFT-UNIQUE-ITM-ID ( #B+4 )
                            ColumnSpacing(5),cwf_Ati_Get_Fft_Qty_Requested.getValue(pnd_B.getDec().add(1)),cwf_Ati_Get_Fft_Unique_Itm_Id.getValue(pnd_B.getDec().add(1)),new 
                            ColumnSpacing(5),cwf_Ati_Get_Fft_Qty_Requested.getValue(pnd_B.getDec().add(2)),cwf_Ati_Get_Fft_Unique_Itm_Id.getValue(pnd_B.getDec().add(2)),new 
                            ColumnSpacing(5),cwf_Ati_Get_Fft_Qty_Requested.getValue(pnd_B.getDec().add(3)),cwf_Ati_Get_Fft_Unique_Itm_Id.getValue(pnd_B.getDec().add(3)),new 
                            ColumnSpacing(5),cwf_Ati_Get_Fft_Qty_Requested.getValue(pnd_B.getDec().add(4)),cwf_Ati_Get_Fft_Unique_Itm_Id.getValue(pnd_B.getDec().add(4)));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_B.nadd(5);                                                                                                                                        //Natural: ADD 5 TO #B
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*   END  ATI-WRITE-REC-2 PROCESSING
    }
    private void sub_Set_Process_Status() throws Exception                                                                                                                //Natural: SET-PROCESS-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        short decideConditionsMet328 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CWF-ATI-FULFILL.ATI-PRCSS-STTS;//Natural: VALUE 'O'
        if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("O"))))
        {
            decideConditionsMet328++;
            pnd_Process_Status_Literal.setValue(" OPEN ");                                                                                                                //Natural: MOVE ' OPEN ' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Open_Atis.nadd(1);                                                                                                                            //Natural: ADD 1 TO #GRAND-TOTAL-OPEN-ATIS
            pnd_Input_Data_Pnd_W_Ati_Open.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                                     //Natural: ADD 1 TO #W-ATI-OPEN ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals("I"))))
        {
            decideConditionsMet328++;
            pnd_Process_Status_Literal.setValue("INPROGS");                                                                                                               //Natural: MOVE 'INPROGS' TO #PROCESS-STATUS-LITERAL
            pnd_Grand_Total_Inprogress_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-INPROGRESS-ATIS
            pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                              //Natural: ADD 1 TO #W-ATI-IN-PROGRESS ( #W-INX )
        }                                                                                                                                                                 //Natural: VALUE ' '
        else if (condition((cwf_Ati_Fulfill_Ati_Prcss_Stts.equals(" "))))
        {
            decideConditionsMet328++;
            if (condition(cwf_Ati_Fulfill_Ati_Error_Txt.equals(" ")))                                                                                                     //Natural: IF CWF-ATI-FULFILL.ATI-ERROR-TXT = ' '
            {
                pnd_Process_Status_Literal.setValue("SUCCESS");                                                                                                           //Natural: MOVE 'SUCCESS' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Succesfull_Atis.nadd(1);                                                                                                                  //Natural: ADD 1 TO #GRAND-TOTAL-SUCCESFULL-ATIS
                pnd_Input_Data_Pnd_W_Ati_Successful.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                           //Natural: ADD 1 TO #W-ATI-SUCCESSFUL ( #W-INX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Process_Status_Literal.setValue("FAILED");                                                                                                            //Natural: MOVE 'FAILED' TO #PROCESS-STATUS-LITERAL
                pnd_Grand_Total_Failed_Atis.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL-FAILED-ATIS
                pnd_Input_Data_Pnd_W_Ati_Manual.getValue(pnd_Input_Data_Pnd_W_Inx).nadd(1);                                                                               //Natural: ADD 1 TO #W-ATI-MANUAL ( #W-INX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Process_Status_Literal.setValue("INCORCT");                                                                                                               //Natural: MOVE 'INCORCT' TO #PROCESS-STATUS-LITERAL
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(50),"AUTOMATED TRANSACTION SYSTEM",new                //Natural: WRITE ( 3 ) NOTITLE 001T *PROGRAM 050T 'AUTOMATED TRANSACTION SYSTEM' 101T *DATU 110T *TIME ( AL = 005 ) 121T 'PAGE:' 127T *PAGE-NUMBER ( 3 ) ( AD = L ) / 035T 'ATI DETAILED REPORT FOR FFT FORMS IN WPID SEQUENCE' / 035T #TITLE-LITERAL / 001T '-' ( 131 ) / 6T 'PIN' 26T 'MASTER INDEX' 48T 'ATI STATUS' 63T 'PROCESS' 71T 'SPCL' 76T 'ADDR' 81T 'RUSH' 86T 'SHIP' 91T 'OVER' 96T 'RQST' 101T 'ORDER' 117T 'N=NPIN' 125T 'ADDR' / 4T 'NUMBER' 16T 'WPID' 24T 'LOG DATE & TIME' 47T 'DATE & TIME' 63T 'STATUS' 71T 'HNDL' 76T 'CHNG' 81T 'IND ' 86T 'MTHD' 91T 'FLOW' 96T 'ORGN' 101T 'NUMBER' 117T 'I=INST' 125T 'TYPE' / / 001T '-' ( 131 ) /
                        TabSetting(101),Global.getDATU(),new TabSetting(110),Global.getTIME(), new AlphanumericLength (5),new TabSetting(121),"PAGE:",new 
                        TabSetting(127),getReports().getPageNumberDbs(3), new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(35),"ATI DETAILED REPORT FOR FFT FORMS IN WPID SEQUENCE",NEWLINE,new 
                        TabSetting(35),pnd_Title_Literal,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(6),"PIN",new TabSetting(26),"MASTER INDEX",new 
                        TabSetting(48),"ATI STATUS",new TabSetting(63),"PROCESS",new TabSetting(71),"SPCL",new TabSetting(76),"ADDR",new TabSetting(81),"RUSH",new 
                        TabSetting(86),"SHIP",new TabSetting(91),"OVER",new TabSetting(96),"RQST",new TabSetting(101),"ORDER",new TabSetting(117),"N=NPIN",new 
                        TabSetting(125),"ADDR",NEWLINE,new TabSetting(4),"NUMBER",new TabSetting(16),"WPID",new TabSetting(24),"LOG DATE & TIME",new TabSetting(47),"DATE & TIME",new 
                        TabSetting(63),"STATUS",new TabSetting(71),"HNDL",new TabSetting(76),"CHNG",new TabSetting(81),"IND ",new TabSetting(86),"MTHD",new 
                        TabSetting(91),"FLOW",new TabSetting(96),"ORGN",new TabSetting(101),"NUMBER",new TabSetting(117),"I=INST",new TabSetting(125),"TYPE",NEWLINE,NEWLINE,new 
                        TabSetting(1),"-",new RepeatItem(131),NEWLINE);
                    //*    31T 'MASTER INDEX'
                    //*    53T 'ATI STATUS'
                    //*    68T 'PROCESS'
                    //*    76T 'SPCL'
                    //*    81T 'ADDR'
                    //*    86T 'RUSH'
                    //*    91T 'SHIP'
                    //*    96T 'OVER'
                    //*   101T 'RQST'
                    //*    106T 'ORDER'
                    //*    119T 'NON'
                    //*    21T 'WPID'
                    //*    29T 'LOG DATE & TIME'
                    //*    52T 'DATE & TIME'
                    //*    68T 'STATUS'
                    //*    76T 'HNDL'
                    //*    81T 'CHNG'
                    //*    86T 'IND '
                    //*    91T 'MTHD'
                    //*    96T 'FLOW'
                    //*   101T 'ORGN'
                    //*    106T 'NUMBER'
                    //*    119T 'PART'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atEndEventRpt3 = (Object sender, EventArgs e) ->
    {
        String localMethod = "MCSN3803|atEndEventRpt3";
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Last_Page_Flag.equals("Y")))                                                                                                        //Natural: IF #LAST-PAGE-FLAG = 'Y'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(3, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(1),"-",new                    //Natural: WRITE ( 3 ) / 1T '-' ( 131 ) / 1T '-' ( 131 )
                            RepeatItem(131));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-ENDPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Ati_Fulfill_Ati_WpidIsBreak = cwf_Ati_Fulfill_Ati_Wpid.isBreak(endOfData);
        if (condition(cwf_Ati_Fulfill_Ati_WpidIsBreak))
        {
            getReports().skip(3, 2);                                                                                                                                      //Natural: SKIP ( 3 ) 2
            getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"TOTAL FOR WPID :",pnd_Prev_Wpid, new ReportEditMask ("XX' 'X' 'XX' 'X"),":",                    //Natural: WRITE ( 3 ) 1T 'TOTAL FOR WPID :' #PREV-WPID ( EM = XX' 'X' 'XX' 'X ) ':' #WPID-TOTAL
                pnd_Wpid_Total);
            if (condition(Global.isEscape())) return;
            pnd_Wpid_Total.reset();                                                                                                                                       //Natural: RESET #WPID-TOTAL
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=OFF");
        Global.format(1, "PS=60 LS=133 ZP=OFF");
        Global.format(2, "PS=60 LS=133 ZP=OFF");
        Global.format(3, "PS=60 LS=133 ZP=OFF");
        Global.format(4, "PS=60 LS=133 ZP=OFF");
    }
}
