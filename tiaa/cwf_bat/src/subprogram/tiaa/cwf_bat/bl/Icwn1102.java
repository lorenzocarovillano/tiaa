/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:48:00 AM
**        * FROM NATURAL SUBPROGRAM : Icwn1102
************************************************************
**        * FILE NAME            : Icwn1102.java
**        * CLASS NAME           : Icwn1102
**        * INSTANCE NAME        : Icwn1102
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: FIND STATUS DESC
**SAG SYSTEM: CRPICW
************************************************************************
* PROGRAM  : ICWN1102
* SYSTEM   : CRPICW
* TITLE    : FIND STATUS DESC
* GENERATED: JUN 09,93 AT 01:56 PM
* FUNCTION : THIS PROGRAM RETRIEVES THE STATUS NAME USING
*          | THE PASSED STATUS-KEY
*          |
*          |
*          |
*          |
*          |
* MOD DATE
* ---------+
* 09/27/96 JHH - COPIED FROM CWF
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwn1102 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Parm_Status_Key;

    private DbsGroup pnd_Parm_Status_Key__R_Field_1;
    private DbsField pnd_Parm_Status_Key_Pnd_Parm_Status_Unit;
    private DbsField pnd_Parm_Status_Key_Pnd_Parm_Status_Code;
    private DbsField pnd_Parm_Status_Name;

    private DataAccessProgramView vw_cwf_Org_Status_Tbl;
    private DbsField cwf_Org_Status_Tbl_Status_Cde;
    private DbsField cwf_Org_Status_Tbl_Status_Unit_Cde;
    private DbsField cwf_Org_Status_Tbl_Status_Dscrptn_Txt;
    private DbsField cwf_Org_Status_Tbl_Dlte_Dte_Tme;

    private DataAccessProgramView vw_route_Cde_Tbl;
    private DbsField route_Cde_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField route_Cde_Tbl_Tbl_Table_Nme;
    private DbsField route_Cde_Tbl_Tbl_Key_Field;
    private DbsField route_Cde_Tbl_Tbl_Data_Field;

    private DbsGroup route_Cde_Tbl__R_Field_2;
    private DbsField route_Cde_Tbl_Status_Desc;

    private DbsGroup pnd_Gen_Var;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Key;

    private DbsGroup pnd_Gen_Var__R_Field_3;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Authrty;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Name;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Code;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Desc;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Parm_Status_Key = parameters.newFieldInRecord("pnd_Parm_Status_Key", "#PARM-STATUS-KEY", FieldType.STRING, 13);
        pnd_Parm_Status_Key.setParameterOption(ParameterOption.ByReference);

        pnd_Parm_Status_Key__R_Field_1 = parameters.newGroupInRecord("pnd_Parm_Status_Key__R_Field_1", "REDEFINE", pnd_Parm_Status_Key);
        pnd_Parm_Status_Key_Pnd_Parm_Status_Unit = pnd_Parm_Status_Key__R_Field_1.newFieldInGroup("pnd_Parm_Status_Key_Pnd_Parm_Status_Unit", "#PARM-STATUS-UNIT", 
            FieldType.STRING, 8);
        pnd_Parm_Status_Key_Pnd_Parm_Status_Code = pnd_Parm_Status_Key__R_Field_1.newFieldInGroup("pnd_Parm_Status_Key_Pnd_Parm_Status_Code", "#PARM-STATUS-CODE", 
            FieldType.STRING, 4);
        pnd_Parm_Status_Name = parameters.newFieldInRecord("pnd_Parm_Status_Name", "#PARM-STATUS-NAME", FieldType.STRING, 25);
        pnd_Parm_Status_Name.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Org_Status_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Status_Tbl", "CWF-ORG-STATUS-TBL"), "CWF_ORG_STATUS_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Status_Tbl_Status_Cde = vw_cwf_Org_Status_Tbl.getRecord().newFieldInGroup("cwf_Org_Status_Tbl_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Org_Status_Tbl_Status_Cde.setDdmHeader("STATUS/CODE");
        cwf_Org_Status_Tbl_Status_Unit_Cde = vw_cwf_Org_Status_Tbl.getRecord().newFieldInGroup("cwf_Org_Status_Tbl_Status_Unit_Cde", "STATUS-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UNIT_CDE");
        cwf_Org_Status_Tbl_Status_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Status_Tbl_Status_Dscrptn_Txt = vw_cwf_Org_Status_Tbl.getRecord().newFieldInGroup("cwf_Org_Status_Tbl_Status_Dscrptn_Txt", "STATUS-DSCRPTN-TXT", 
            FieldType.STRING, 25, RepeatingFieldStrategy.None, "STATUS_DSCRPTN_TXT");
        cwf_Org_Status_Tbl_Status_Dscrptn_Txt.setDdmHeader("STEP/DESCRIPTION");
        cwf_Org_Status_Tbl_Dlte_Dte_Tme = vw_cwf_Org_Status_Tbl.getRecord().newFieldInGroup("cwf_Org_Status_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Org_Status_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        registerRecord(vw_cwf_Org_Status_Tbl);

        vw_route_Cde_Tbl = new DataAccessProgramView(new NameInfo("vw_route_Cde_Tbl", "ROUTE-CDE-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        route_Cde_Tbl_Tbl_Scrty_Level_Ind = vw_route_Cde_Tbl.getRecord().newFieldInGroup("route_Cde_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        route_Cde_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        route_Cde_Tbl_Tbl_Table_Nme = vw_route_Cde_Tbl.getRecord().newFieldInGroup("route_Cde_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, 
            RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        route_Cde_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        route_Cde_Tbl_Tbl_Key_Field = vw_route_Cde_Tbl.getRecord().newFieldInGroup("route_Cde_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        route_Cde_Tbl_Tbl_Data_Field = vw_route_Cde_Tbl.getRecord().newFieldInGroup("route_Cde_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        route_Cde_Tbl__R_Field_2 = vw_route_Cde_Tbl.getRecord().newGroupInGroup("route_Cde_Tbl__R_Field_2", "REDEFINE", route_Cde_Tbl_Tbl_Data_Field);
        route_Cde_Tbl_Status_Desc = route_Cde_Tbl__R_Field_2.newFieldInGroup("route_Cde_Tbl_Status_Desc", "STATUS-DESC", FieldType.STRING, 25);
        registerRecord(vw_route_Cde_Tbl);

        pnd_Gen_Var = localVariables.newGroupInRecord("pnd_Gen_Var", "#GEN-VAR");
        pnd_Gen_Var_Pnd_Gen_Tbl_Key = pnd_Gen_Var.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Key", "#GEN-TBL-KEY", FieldType.STRING, 53);

        pnd_Gen_Var__R_Field_3 = pnd_Gen_Var.newGroupInGroup("pnd_Gen_Var__R_Field_3", "REDEFINE", pnd_Gen_Var_Pnd_Gen_Tbl_Key);
        pnd_Gen_Var_Pnd_Gen_Tbl_Authrty = pnd_Gen_Var__R_Field_3.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Authrty", "#GEN-TBL-AUTHRTY", FieldType.STRING, 
            2);
        pnd_Gen_Var_Pnd_Gen_Tbl_Name = pnd_Gen_Var__R_Field_3.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Name", "#GEN-TBL-NAME", FieldType.STRING, 20);
        pnd_Gen_Var_Pnd_Gen_Tbl_Code = pnd_Gen_Var__R_Field_3.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Code", "#GEN-TBL-CODE", FieldType.STRING, 30);
        pnd_Gen_Var_Pnd_Gen_Tbl_Desc = pnd_Gen_Var.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Desc", "#GEN-TBL-DESC", FieldType.STRING, 30);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Org_Status_Tbl.reset();
        vw_route_Cde_Tbl.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Icwn1102() throws Exception
    {
        super("Icwn1102");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*  ============================================
        //*  DEFINE FORMATS
        //*                                                                                                                                                               //Natural: FORMAT KD = ON
        vw_cwf_Org_Status_Tbl.startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) CWF-ORG-STATUS-TBL BY STATUS-KEY FROM #PARM-STATUS-KEY
        (
        "FIND",
        new Wc[] { new Wc("STATUS_KEY", ">=", pnd_Parm_Status_Key, WcType.BY) },
        new Oc[] { new Oc("STATUS_KEY", "ASC") },
        1
        );
        FIND:
        while (condition(vw_cwf_Org_Status_Tbl.readNextRow("FIND")))
        {
            if (condition(cwf_Org_Status_Tbl_Status_Cde.greater(pnd_Parm_Status_Key_Pnd_Parm_Status_Code) || cwf_Org_Status_Tbl_Status_Unit_Cde.greater(pnd_Parm_Status_Key_Pnd_Parm_Status_Unit))) //Natural: IF CWF-ORG-STATUS-TBL.STATUS-CDE GT #PARM-STATUS-CODE OR CWF-ORG-STATUS-TBL.STATUS-UNIT-CDE GT #PARM-STATUS-UNIT
            {
                pnd_Parm_Status_Name.setValue(" ");                                                                                                                       //Natural: MOVE ' ' TO #PARM-STATUS-NAME
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Org_Status_Tbl_Dlte_Dte_Tme.greater(getZero())))                                                                                            //Natural: IF CWF-ORG-STATUS-TBL.DLTE-DTE-TME GT 0
            {
                pnd_Parm_Status_Name.setValue(DbsUtil.compress("*", cwf_Org_Status_Tbl_Status_Dscrptn_Txt));                                                              //Natural: COMPRESS '*' CWF-ORG-STATUS-TBL.STATUS-DSCRPTN-TXT INTO #PARM-STATUS-NAME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Parm_Status_Name.setValue(cwf_Org_Status_Tbl_Status_Dscrptn_Txt);                                                                                     //Natural: MOVE CWF-ORG-STATUS-TBL.STATUS-DSCRPTN-TXT TO #PARM-STATUS-NAME
            }                                                                                                                                                             //Natural: END-IF
            //*  FIND-STATUS.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ----------------------------------
        if (condition(pnd_Parm_Status_Name.equals(" ")))                                                                                                                  //Natural: IF #PARM-STATUS-NAME EQ ' '
        {
            pnd_Gen_Var_Pnd_Gen_Tbl_Authrty.setValue("S");                                                                                                                //Natural: MOVE 'S' TO #GEN-TBL-AUTHRTY
            pnd_Gen_Var_Pnd_Gen_Tbl_Name.setValue("C-STATUS-TABLE");                                                                                                      //Natural: MOVE 'C-STATUS-TABLE' TO #GEN-TBL-NAME
            pnd_Gen_Var_Pnd_Gen_Tbl_Code.setValue(pnd_Parm_Status_Key_Pnd_Parm_Status_Code);                                                                              //Natural: MOVE #PARM-STATUS-CODE TO #GEN-TBL-CODE
            vw_route_Cde_Tbl.startDatabaseRead                                                                                                                            //Natural: READ ( 1 ) ROUTE-CDE-TBL BY TBL-PRIME-KEY FROM #GEN-TBL-KEY
            (
            "READ01",
            new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Gen_Var_Pnd_Gen_Tbl_Key, WcType.BY) },
            new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
            1
            );
            READ01:
            while (condition(vw_route_Cde_Tbl.readNextRow("READ01")))
            {
                if (condition(route_Cde_Tbl_Tbl_Scrty_Level_Ind.greater(pnd_Gen_Var_Pnd_Gen_Tbl_Authrty) || route_Cde_Tbl_Tbl_Table_Nme.greater(pnd_Gen_Var_Pnd_Gen_Tbl_Name)  //Natural: IF ROUTE-CDE-TBL.TBL-SCRTY-LEVEL-IND GT #GEN-TBL-AUTHRTY OR ROUTE-CDE-TBL.TBL-TABLE-NME GT #GEN-TBL-NAME OR ROUTE-CDE-TBL.TBL-KEY-FIELD GT #GEN-TBL-CODE
                    || route_Cde_Tbl_Tbl_Key_Field.greater(pnd_Gen_Var_Pnd_Gen_Tbl_Code)))
                {
                    pnd_Parm_Status_Name.setValue("** Status not defined **");                                                                                            //Natural: MOVE '** Status not defined **' TO #PARM-STATUS-NAME
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Parm_Status_Name.setValue(route_Cde_Tbl_Status_Desc);                                                                                                 //Natural: MOVE ROUTE-CDE-TBL.STATUS-DESC TO #PARM-STATUS-NAME
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  ----------------------------------
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "KD=ON");
    }
}
