/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:28:23 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn3803
************************************************************
**        * FILE NAME            : Cwfn3803.java
**        * CLASS NAME           : Cwfn3803
**        * INSTANCE NAME        : Cwfn3803
************************************************************
* CWFN3803

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn3803 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Gtot_Registers;
    private DbsField pnd_Gtot_Registers_Pnd_Ave_Total;
    private DbsField pnd_Gtot_Registers_Pnd_Ave_Total_Num;
    private DbsField pnd_Gtot_Registers_Pnd_Total_Moc;
    private DbsField pnd_Gtot_Registers_Pnd_Total_Moc_3;
    private DbsField pnd_Gtot_Registers_Pnd_Total_Moc_4;
    private DbsField pnd_Gtot_Registers_Pnd_Total_Moc_5;
    private DbsField pnd_Gtot_Registers_Pnd_Total_Moc_10;
    private DbsField pnd_Gtot_Registers_Pnd_Total_Moc_Over;

    private DbsGroup pnd_Combined_Totals;
    private DbsField pnd_Combined_Totals_Pnd_Cave_Total;
    private DbsField pnd_Combined_Totals_Pnd_Cave_Total_Num;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_3;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_4;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_5;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_10;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_Over;
    private DbsField pnd_Old_Moc;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Gtot_Registers = parameters.newGroupInRecord("pnd_Gtot_Registers", "#GTOT-REGISTERS");
        pnd_Gtot_Registers.setParameterOption(ParameterOption.ByReference);
        pnd_Gtot_Registers_Pnd_Ave_Total = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Ave_Total", "#AVE-TOTAL", FieldType.NUMERIC, 10, 
            3);
        pnd_Gtot_Registers_Pnd_Ave_Total_Num = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Ave_Total_Num", "#AVE-TOTAL-NUM", FieldType.NUMERIC, 
            10, 3);
        pnd_Gtot_Registers_Pnd_Total_Moc = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Total_Moc", "#TOTAL-MOC", FieldType.NUMERIC, 7);
        pnd_Gtot_Registers_Pnd_Total_Moc_3 = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Total_Moc_3", "#TOTAL-MOC-3", FieldType.NUMERIC, 
            7);
        pnd_Gtot_Registers_Pnd_Total_Moc_4 = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Total_Moc_4", "#TOTAL-MOC-4", FieldType.NUMERIC, 
            7);
        pnd_Gtot_Registers_Pnd_Total_Moc_5 = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Total_Moc_5", "#TOTAL-MOC-5", FieldType.NUMERIC, 
            7);
        pnd_Gtot_Registers_Pnd_Total_Moc_10 = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Total_Moc_10", "#TOTAL-MOC-10", FieldType.NUMERIC, 
            7);
        pnd_Gtot_Registers_Pnd_Total_Moc_Over = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Total_Moc_Over", "#TOTAL-MOC-OVER", FieldType.NUMERIC, 
            7);

        pnd_Combined_Totals = parameters.newGroupInRecord("pnd_Combined_Totals", "#COMBINED-TOTALS");
        pnd_Combined_Totals.setParameterOption(ParameterOption.ByReference);
        pnd_Combined_Totals_Pnd_Cave_Total = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Cave_Total", "#CAVE-TOTAL", FieldType.NUMERIC, 
            10, 3, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Cave_Total_Num = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Cave_Total_Num", "#CAVE-TOTAL-NUM", 
            FieldType.NUMERIC, 10, 3, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc", "#CTOTAL-MOC", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_3 = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_3", "#CTOTAL-MOC-3", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_4 = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_4", "#CTOTAL-MOC-4", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_5 = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_5", "#CTOTAL-MOC-5", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_10 = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_10", "#CTOTAL-MOC-10", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_Over = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_Over", "#CTOTAL-MOC-OVER", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 3));
        pnd_Old_Moc = parameters.newFieldInRecord("pnd_Old_Moc", "#OLD-MOC", FieldType.STRING, 1);
        pnd_Old_Moc.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn3803() throws Exception
    {
        super("Cwfn3803");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        short decideConditionsMet25 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE OF #OLD-MOC;//Natural: VALUE '1'
        if (condition((pnd_Old_Moc.equals("1"))))
        {
            decideConditionsMet25++;
            pnd_Combined_Totals_Pnd_Ctotal_Moc.getValue(1).nadd(pnd_Gtot_Registers_Pnd_Total_Moc);                                                                        //Natural: ASSIGN #CTOTAL-MOC ( 1 ) := #CTOTAL-MOC ( 1 ) + #TOTAL-MOC
            pnd_Combined_Totals_Pnd_Ctotal_Moc_3.getValue(1).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_3);                                                                    //Natural: ASSIGN #CTOTAL-MOC-3 ( 1 ) := #CTOTAL-MOC-3 ( 1 ) + #TOTAL-MOC-3
            pnd_Combined_Totals_Pnd_Ctotal_Moc_4.getValue(1).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_4);                                                                    //Natural: ASSIGN #CTOTAL-MOC-4 ( 1 ) := #CTOTAL-MOC-4 ( 1 ) + #TOTAL-MOC-4
            pnd_Combined_Totals_Pnd_Ctotal_Moc_5.getValue(1).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_5);                                                                    //Natural: ASSIGN #CTOTAL-MOC-5 ( 1 ) := #CTOTAL-MOC-5 ( 1 ) + #TOTAL-MOC-5
            pnd_Combined_Totals_Pnd_Ctotal_Moc_10.getValue(1).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_10);                                                                  //Natural: ASSIGN #CTOTAL-MOC-10 ( 1 ) := #CTOTAL-MOC-10 ( 1 ) + #TOTAL-MOC-10
            pnd_Combined_Totals_Pnd_Ctotal_Moc_Over.getValue(1).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_Over);                                                              //Natural: ASSIGN #CTOTAL-MOC-OVER ( 1 ) := #CTOTAL-MOC-OVER ( 1 ) + #TOTAL-MOC-OVER
            pnd_Combined_Totals_Pnd_Cave_Total_Num.getValue(1).nadd(pnd_Gtot_Registers_Pnd_Ave_Total_Num);                                                                //Natural: ASSIGN #CAVE-TOTAL-NUM ( 1 ) := #CAVE-TOTAL-NUM ( 1 ) + #AVE-TOTAL-NUM
            pnd_Combined_Totals_Pnd_Cave_Total.getValue(1).compute(new ComputeParameters(false, pnd_Combined_Totals_Pnd_Cave_Total.getValue(1)), pnd_Combined_Totals_Pnd_Cave_Total_Num.getValue(1).divide(pnd_Combined_Totals_Pnd_Ctotal_Moc.getValue(1))); //Natural: COMPUTE #CAVE-TOTAL ( 1 ) = #CAVE-TOTAL-NUM ( 1 ) / #CTOTAL-MOC ( 1 )
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((pnd_Old_Moc.equals("2"))))
        {
            decideConditionsMet25++;
            pnd_Combined_Totals_Pnd_Ctotal_Moc.getValue(2).nadd(pnd_Gtot_Registers_Pnd_Total_Moc);                                                                        //Natural: ASSIGN #CTOTAL-MOC ( 2 ) := #CTOTAL-MOC ( 2 ) + #TOTAL-MOC
            pnd_Combined_Totals_Pnd_Ctotal_Moc_3.getValue(2).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_3);                                                                    //Natural: ASSIGN #CTOTAL-MOC-3 ( 2 ) := #CTOTAL-MOC-3 ( 2 ) + #TOTAL-MOC-3
            pnd_Combined_Totals_Pnd_Ctotal_Moc_4.getValue(2).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_4);                                                                    //Natural: ASSIGN #CTOTAL-MOC-4 ( 2 ) := #CTOTAL-MOC-4 ( 2 ) + #TOTAL-MOC-4
            pnd_Combined_Totals_Pnd_Ctotal_Moc_5.getValue(2).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_5);                                                                    //Natural: ASSIGN #CTOTAL-MOC-5 ( 2 ) := #CTOTAL-MOC-5 ( 2 ) + #TOTAL-MOC-5
            pnd_Combined_Totals_Pnd_Ctotal_Moc_10.getValue(2).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_10);                                                                  //Natural: ASSIGN #CTOTAL-MOC-10 ( 2 ) := #CTOTAL-MOC-10 ( 2 ) + #TOTAL-MOC-10
            pnd_Combined_Totals_Pnd_Ctotal_Moc_Over.getValue(2).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_Over);                                                              //Natural: ASSIGN #CTOTAL-MOC-OVER ( 2 ) := #CTOTAL-MOC-OVER ( 2 ) + #TOTAL-MOC-OVER
            pnd_Combined_Totals_Pnd_Cave_Total_Num.getValue(2).nadd(pnd_Gtot_Registers_Pnd_Ave_Total_Num);                                                                //Natural: ASSIGN #CAVE-TOTAL-NUM ( 2 ) := #CAVE-TOTAL-NUM ( 2 ) + #AVE-TOTAL-NUM
            pnd_Combined_Totals_Pnd_Cave_Total.getValue(2).compute(new ComputeParameters(false, pnd_Combined_Totals_Pnd_Cave_Total.getValue(2)), pnd_Combined_Totals_Pnd_Cave_Total_Num.getValue(2).divide(pnd_Combined_Totals_Pnd_Ctotal_Moc.getValue(2))); //Natural: COMPUTE #CAVE-TOTAL ( 2 ) = #CAVE-TOTAL-NUM ( 2 ) / #CTOTAL-MOC ( 2 )
        }                                                                                                                                                                 //Natural: VALUE '3'
        else if (condition((pnd_Old_Moc.equals("3"))))
        {
            decideConditionsMet25++;
            pnd_Combined_Totals_Pnd_Ctotal_Moc.getValue(3).nadd(pnd_Gtot_Registers_Pnd_Total_Moc);                                                                        //Natural: ASSIGN #CTOTAL-MOC ( 3 ) := #CTOTAL-MOC ( 3 ) + #TOTAL-MOC
            pnd_Combined_Totals_Pnd_Ctotal_Moc_3.getValue(3).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_3);                                                                    //Natural: ASSIGN #CTOTAL-MOC-3 ( 3 ) := #CTOTAL-MOC-3 ( 3 ) + #TOTAL-MOC-3
            pnd_Combined_Totals_Pnd_Ctotal_Moc_4.getValue(3).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_4);                                                                    //Natural: ASSIGN #CTOTAL-MOC-4 ( 3 ) := #CTOTAL-MOC-4 ( 3 ) + #TOTAL-MOC-4
            pnd_Combined_Totals_Pnd_Ctotal_Moc_5.getValue(3).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_5);                                                                    //Natural: ASSIGN #CTOTAL-MOC-5 ( 3 ) := #CTOTAL-MOC-5 ( 3 ) + #TOTAL-MOC-5
            pnd_Combined_Totals_Pnd_Ctotal_Moc_10.getValue(3).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_10);                                                                  //Natural: ASSIGN #CTOTAL-MOC-10 ( 3 ) := #CTOTAL-MOC-10 ( 3 ) + #TOTAL-MOC-10
            pnd_Combined_Totals_Pnd_Ctotal_Moc_Over.getValue(3).nadd(pnd_Gtot_Registers_Pnd_Total_Moc_Over);                                                              //Natural: ASSIGN #CTOTAL-MOC-OVER ( 3 ) := #CTOTAL-MOC-OVER ( 3 ) + #TOTAL-MOC-OVER
            pnd_Combined_Totals_Pnd_Cave_Total_Num.getValue(3).nadd(pnd_Gtot_Registers_Pnd_Ave_Total_Num);                                                                //Natural: ASSIGN #CAVE-TOTAL-NUM ( 3 ) := #CAVE-TOTAL-NUM ( 3 ) + #AVE-TOTAL-NUM
            pnd_Combined_Totals_Pnd_Cave_Total.getValue(3).compute(new ComputeParameters(false, pnd_Combined_Totals_Pnd_Cave_Total.getValue(3)), pnd_Combined_Totals_Pnd_Cave_Total_Num.getValue(3).divide(pnd_Combined_Totals_Pnd_Ctotal_Moc.getValue(3))); //Natural: COMPUTE #CAVE-TOTAL ( 3 ) = #CAVE-TOTAL-NUM ( 3 ) / #CTOTAL-MOC ( 3 )
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //
}
