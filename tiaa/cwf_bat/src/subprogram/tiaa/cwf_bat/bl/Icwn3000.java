/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:53:51 AM
**        * FROM NATURAL SUBPROGRAM : Icwn3000
************************************************************
**        * FILE NAME            : Icwn3000.java
**        * CLASS NAME           : Icwn3000
**        * INSTANCE NAME        : Icwn3000
************************************************************
************************************************************************
* PROGRAM  : ICWN3000
* SYSTEM   :
* TITLE    : SET UP RUN CONTROL RECORD FOR THE IMIT EXTRACTION PROCESS
* GENERATED: JUN 25,97 AT 15:01 PM
* FUNCTION : THIS MODULE CAN BE CALLED BY THE EXTRACTION PROCESS
*          : TO DO THE INITIAL SET UP OF THE RUN CONTROL RECORD.
*
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwn3000 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIcwa3000 pdaIcwa3000;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaIcwl3000 ldaIcwl3000;
    private LdaIcwl3001 ldaIcwl3001;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl_Histogram;
    private DbsField cwf_Support_Tbl_Histogram_Tbl_Prime_Key;
    private DbsField pnd_Cut_Off_Date_D;
    private DbsField pnd_Cut_Off_Date;

    private DbsGroup pnd_Cut_Off_Date__R_Field_1;
    private DbsField pnd_Cut_Off_Date_Pnd_Alpha;
    private DbsField pnd_Last_Run_Isn;
    private DbsField pnd_New_Run_Id;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_2;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_3;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key_From;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_4;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_5;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key_To;

    private DbsGroup pnd_Tbl_Prime_Key_To__R_Field_6;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_To__R_Field_7;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Work_Rqst_Retaining_Days;
    private DbsField pnd_Str_Calc_Date_A;

    private DbsGroup pnd_Str_Calc_Date_A__R_Field_8;
    private DbsField pnd_Str_Calc_Date_A_Pnd_Str_Calc_Date_N;
    private DbsField pnd_End_Calc_Date;

    private DbsGroup pnd_End_Calc_Date__R_Field_9;
    private DbsField pnd_End_Calc_Date_Pnd_End_Calc_Dte;
    private DbsField pnd_End_Calc_Date_Pnd_End_Calc_Tme;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIcwl3000 = new LdaIcwl3000();
        registerRecord(ldaIcwl3000);
        registerRecord(ldaIcwl3000.getVw_cwf_Support_Tbl());
        ldaIcwl3001 = new LdaIcwl3001();
        registerRecord(ldaIcwl3001);
        registerRecord(ldaIcwl3001.getVw_cwf_Support_Tbl_Last_Run_Id());

        // parameters
        parameters = new DbsRecord();
        pdaIcwa3000 = new PdaIcwa3000(parameters);
        pdaCdaobj = new PdaCdaobj(parameters);
        pdaCwfpda_D = new PdaCwfpda_D(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pdaCwfpda_P = new PdaCwfpda_P(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Support_Tbl_Histogram = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl_Histogram", "CWF-SUPPORT-TBL-HISTOGRAM"), "CWF_SUPPORT_TBL", 
            "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Histogram_Tbl_Prime_Key = vw_cwf_Support_Tbl_Histogram.getRecord().newFieldInGroup("cwf_Support_Tbl_Histogram_Tbl_Prime_Key", 
            "TBL-PRIME-KEY", FieldType.STRING, 53, RepeatingFieldStrategy.None, "TBL_PRIME_KEY");
        cwf_Support_Tbl_Histogram_Tbl_Prime_Key.setDdmHeader("RECORD KEY");
        cwf_Support_Tbl_Histogram_Tbl_Prime_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Support_Tbl_Histogram);

        pnd_Cut_Off_Date_D = localVariables.newFieldInRecord("pnd_Cut_Off_Date_D", "#CUT-OFF-DATE-D", FieldType.DATE);
        pnd_Cut_Off_Date = localVariables.newFieldInRecord("pnd_Cut_Off_Date", "#CUT-OFF-DATE", FieldType.NUMERIC, 8);

        pnd_Cut_Off_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Cut_Off_Date__R_Field_1", "REDEFINE", pnd_Cut_Off_Date);
        pnd_Cut_Off_Date_Pnd_Alpha = pnd_Cut_Off_Date__R_Field_1.newFieldInGroup("pnd_Cut_Off_Date_Pnd_Alpha", "#ALPHA", FieldType.STRING, 8);
        pnd_Last_Run_Isn = localVariables.newFieldInRecord("pnd_Last_Run_Isn", "#LAST-RUN-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_New_Run_Id = localVariables.newFieldInRecord("pnd_New_Run_Id", "#NEW-RUN-ID", FieldType.NUMERIC, 9);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_2", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);

        pnd_Tbl_Prime_Key__R_Field_3 = pnd_Tbl_Prime_Key__R_Field_2.newGroupInGroup("pnd_Tbl_Prime_Key__R_Field_3", "REDEFINE", pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_Pnd_Run_Status = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 8);
        pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field", "#REST-OF-TBL-KEY-FIELD", 
            FieldType.STRING, 21);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_From = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_From", "#TBL-PRIME-KEY-FROM", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_From__R_Field_4 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_From__R_Field_4", "REDEFINE", pnd_Tbl_Prime_Key_From);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_From__R_Field_5 = pnd_Tbl_Prime_Key_From__R_Field_4.newGroupInGroup("pnd_Tbl_Prime_Key_From__R_Field_5", "REDEFINE", pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status = pnd_Tbl_Prime_Key_From__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Run_Status", "#RUN-STATUS", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key_From__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 8);
        pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 21);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_To = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_To", "#TBL-PRIME-KEY-TO", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_To__R_Field_6 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_To__R_Field_6", "REDEFINE", pnd_Tbl_Prime_Key_To);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_To__R_Field_7 = pnd_Tbl_Prime_Key_To__R_Field_6.newGroupInGroup("pnd_Tbl_Prime_Key_To__R_Field_7", "REDEFINE", pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status = pnd_Tbl_Prime_Key_To__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key_To__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 8);
        pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_To__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 21);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Work_Rqst_Retaining_Days = localVariables.newFieldInRecord("pnd_Work_Rqst_Retaining_Days", "#WORK-RQST-RETAINING-DAYS", FieldType.NUMERIC, 
            3);
        pnd_Str_Calc_Date_A = localVariables.newFieldInRecord("pnd_Str_Calc_Date_A", "#STR-CALC-DATE-A", FieldType.STRING, 15);

        pnd_Str_Calc_Date_A__R_Field_8 = localVariables.newGroupInRecord("pnd_Str_Calc_Date_A__R_Field_8", "REDEFINE", pnd_Str_Calc_Date_A);
        pnd_Str_Calc_Date_A_Pnd_Str_Calc_Date_N = pnd_Str_Calc_Date_A__R_Field_8.newFieldInGroup("pnd_Str_Calc_Date_A_Pnd_Str_Calc_Date_N", "#STR-CALC-DATE-N", 
            FieldType.NUMERIC, 15);
        pnd_End_Calc_Date = localVariables.newFieldInRecord("pnd_End_Calc_Date", "#END-CALC-DATE", FieldType.STRING, 15);

        pnd_End_Calc_Date__R_Field_9 = localVariables.newGroupInRecord("pnd_End_Calc_Date__R_Field_9", "REDEFINE", pnd_End_Calc_Date);
        pnd_End_Calc_Date_Pnd_End_Calc_Dte = pnd_End_Calc_Date__R_Field_9.newFieldInGroup("pnd_End_Calc_Date_Pnd_End_Calc_Dte", "#END-CALC-DTE", FieldType.NUMERIC, 
            8);
        pnd_End_Calc_Date_Pnd_End_Calc_Tme = pnd_End_Calc_Date__R_Field_9.newFieldInGroup("pnd_End_Calc_Date_Pnd_End_Calc_Tme", "#END-CALC-TME", FieldType.NUMERIC, 
            7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl_Histogram.reset();

        ldaIcwl3000.initializeValues();
        ldaIcwl3001.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Icwn3000() throws Exception
    {
        super("Icwn3000");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("ICWN3000", onError);
        //* (TO ALLOW EASY ESCAPE FROM THE ROUTINE)
        ROUTINE:                                                                                                                                                          //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM RESET-THE-OUTPUT-PARAMETERS
            sub_Reset_The_Output_Parameters();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM SET-UP-THE-RUN-CONTROL-RECORD
            sub_Set_Up_The_Run_Control_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break ROUTINE;                                                                                                                                      //Natural: ESCAPE BOTTOM ( ROUTINE. )
            //*  SUBROUTINE JUST FOR ENDING THIS SUBPROGRAM IMMEDIATELY
            //*  ------------------------------------------------------
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-SUBPROGRAM
            //* (ROUTINE.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  SUBROUTINES
        //*  ===========
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-THE-OUTPUT-PARAMETERS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-UP-THE-RUN-CONTROL-RECORD
        //*  =============================================
        //*  FIND EXTR-LST-RUN-CNTRL RECORD TO RETAIN LAST-RUN-ID (ISN)
        //*  ----------------------------------------------------------------------
        //*  IF AN ACTIVE RECORD IS FOUND, DO NOT POPULATE ANY RESTART PARAMETERS
        //*  RESET NUMBER OF WORK REQUESTS AND NUMBER OF EVENTS PURGED AND
        //*  RETURN TO THE CALLING PROGRAM.
        //*  ---------------------------------------------------------------------
        //*  CHECK TO SEE IF A PENDING RECORD ALREADY EXISTS
        //*  -----------------------------------------------
        //*  IF A PENDING RECORD IS FOUND, UPDATE THE RECORD TO ACTIVE,
        //*  POPULATE THE OUTPUT PARAMETERS AND RETURN TO THE CALLING PROGRAM
        //*  ----------------------------------------------------------------
        //*  USE THIS RECORD TO CREATE THE NEW ONE
        //*  -------------------------------------
        //*  ENDING-DATE(LAST-COMPLETED-READ.) := *DATN - 1
        //*    999999999999999 - STARTING-DATE(LAST-COMPLETED-READ.)
        //*  ON NATURAL RUN TIME ERROR, RETURN WITH AN APPROPRIATE ERROR MESSAGE
        //*  ===================================================================
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Subprogram() throws Exception                                                                                                                 //Natural: ESCAPE-SUBPROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( ROUTINE. )
        Global.setEscapeCode(EscapeType.Bottom, "ROUTINE");
        if (true) return;
        //* (0850)
    }
    private void sub_Reset_The_Output_Parameters() throws Exception                                                                                                       //Natural: RESET-THE-OUTPUT-PARAMETERS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===========================================
        pdaIcwa3000.getIcwa3000_Output().reset();                                                                                                                         //Natural: RESET ICWA3000-OUTPUT
        //* (0940)
    }
    private void sub_Set_Up_The_Run_Control_Record() throws Exception                                                                                                     //Natural: SET-UP-THE-RUN-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("ICW-EXTR-RPT-LST-RUN");                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME := 'ICW-EXTR-RPT-LST-RUN'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("ICW-EXTR-LST-KEY");                                                                                                 //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-KEY-FIELD := 'ICW-EXTR-LST-KEY'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind.setValue("A");                                                                                                                //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-ACTVE-IND := 'A'
        ldaIcwl3001.getVw_cwf_Support_Tbl_Last_Run_Id().startDatabaseFind                                                                                                 //Natural: FIND ( 1 ) CWF-SUPPORT-TBL-LAST-RUN-ID WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "LAST_RUN_ID_FIND",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        LAST_RUN_ID_FIND:
        while (condition(ldaIcwl3001.getVw_cwf_Support_Tbl_Last_Run_Id().readNextRow("LAST_RUN_ID_FIND")))
        {
            ldaIcwl3001.getVw_cwf_Support_Tbl_Last_Run_Id().setIfNotFoundControlFlag(false);
            pnd_Last_Run_Isn.setValue(ldaIcwl3001.getVw_cwf_Support_Tbl_Last_Run_Id().getAstISN("LAST_RUN_ID_FIND"));                                                     //Natural: ASSIGN #LAST-RUN-ISN := *ISN ( LAST-RUN-ID-FIND. )
            //* (LAST-RUN-ID-FIND.)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(ldaIcwl3001.getVw_cwf_Support_Tbl_Last_Run_Id().getAstCOUNTER().equals(getZero())))                                                                 //Natural: IF *COUNTER ( LAST-RUN-ID-FIND. ) = 0
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(26);                                                                                                    //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 26
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Last-Run-Id control record not found"));               //Natural: COMPRESS 'Program' *PROGRAM '- Last-Run-Id control record not found' INTO MSG-INFO-SUB.##MSG
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape())) {return;}
            //* (1150)
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK TO SEE IF AN ACTIVE RECORD ALREADY EXISTS
        //*  -----------------------------------------------
        //*  ACTIVE STATUS
        pnd_Tbl_Prime_Key_From.reset();                                                                                                                                   //Natural: RESET #TBL-PRIME-KEY-FROM
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                     //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme.setValue("ICW-EXTR-RPT-NXT-RUN");                                                                                        //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-TABLE-NME := 'ICW-EXTR-RPT-NXT-RUN'
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status.setValue("A");                                                                                                              //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#RUN-STATUS := 'A'
        pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement.setValue(0);                                                                                               //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#STARTING-DATE-9S-COMPLEMENT := 0
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind.setValue(" ");                                                                                                           //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-ACTVE-IND := ' '
        //*  ACTIVE STATUS
        pnd_Tbl_Prime_Key_To.reset();                                                                                                                                     //Natural: RESET #TBL-PRIME-KEY-TO
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                       //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme.setValue("ICW-EXTR-RPT-NXT-RUN");                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-TABLE-NME := 'ICW-EXTR-RPT-NXT-RUN'
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status.setValue("A");                                                                                                                //Natural: ASSIGN #TBL-PRIME-KEY-TO.#RUN-STATUS := 'A'
        pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement.setValue(99999999);                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY-TO.#STARTING-DATE-9S-COMPLEMENT := 99999999
        pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field.setValue("999999999999999999999");                                                                                 //Natural: ASSIGN #TBL-PRIME-KEY-TO.#REST-OF-TBL-KEY-FIELD := '999999999999999999999'
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind.setValue("9");                                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-ACTVE-IND := '9'
        vw_cwf_Support_Tbl_Histogram.createHistogram                                                                                                                      //Natural: HISTOGRAM CWF-SUPPORT-TBL-HISTOGRAM FOR TBL-PRIME-KEY FROM #TBL-PRIME-KEY-FROM THRU #TBL-PRIME-KEY-TO
        (
        "ACTIVE_HISTOGRAM",
        "TBL_PRIME_KEY",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key_From, "And", WcType.WITH) ,
        new Wc("TBL_PRIME_KEY", "<=", pnd_Tbl_Prime_Key_To, WcType.WITH) }
        );
        ACTIVE_HISTOGRAM:
        while (condition(vw_cwf_Support_Tbl_Histogram.readNextRow("ACTIVE_HISTOGRAM")))
        {
            if (condition(vw_cwf_Support_Tbl_Histogram.getAstNUMBER().greater(1)))                                                                                        //Natural: IF *NUMBER ( ACTIVE-HISTOGRAM. ) GT 1
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(23);                                                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 23
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- More than one active control records",             //Natural: COMPRESS 'Program' *PROGRAM '- More than one active control records' 'exist' INTO MSG-INFO-SUB.##MSG
                    "exist"));
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* (1450)
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tbl_Prime_Key.setValue(cwf_Support_Tbl_Histogram_Tbl_Prime_Key);                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY := CWF-SUPPORT-TBL-HISTOGRAM.TBL-PRIME-KEY
            ldaIcwl3000.getVw_cwf_Support_Tbl().startDatabaseFind                                                                                                         //Natural: FIND ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
            (
            "ACTIVE_FIND",
            new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
            1
            );
            ACTIVE_FIND:
            while (condition(ldaIcwl3000.getVw_cwf_Support_Tbl().readNextRow("ACTIVE_FIND")))
            {
                ldaIcwl3000.getVw_cwf_Support_Tbl().setIfNotFoundControlFlag(false);
                pdaIcwa3000.getIcwa3000_Output_Run_Control_Isn().setValue(ldaIcwl3000.getVw_cwf_Support_Tbl().getAstISN("ACTIVE_FIND"));                                  //Natural: ASSIGN ICWA3000-OUTPUT.RUN-CONTROL-ISN := *ISN ( ACTIVE-FIND. )
                ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Upd_Wr().setValue(0);                                                                                          //Natural: ASSIGN NBR-OF-RECS-UPD-WR := 0
                ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Add_Wr().setValue(0);                                                                                          //Natural: ASSIGN NBR-OF-RECS-ADD-WR := 0
                ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Opn_Ac().setValue(0);                                                                                          //Natural: ASSIGN NBR-OF-RECS-OPN-AC := 0
                ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Cls_Ac().setValue(0);                                                                                          //Natural: ASSIGN NBR-OF-RECS-CLS-AC := 0
                ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Spl_Pr().setValue(0);                                                                                          //Natural: ASSIGN NBR-OF-RECS-SPL-PR := 0
                ldaIcwl3000.getVw_cwf_Support_Tbl().updateDBRow("ACTIVE_FIND");                                                                                           //Natural: UPDATE ( ACTIVE-FIND. )
                //* (ACTIVE-FIND.)
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* (ACTIVE-HISTOGRAM.)
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        if (condition(vw_cwf_Support_Tbl_Histogram.getAstCOUNTER().greater(1)))                                                                                           //Natural: IF *COUNTER ( ACTIVE-HISTOGRAM. ) GT 1
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(23);                                                                                                    //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 23
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- More than one active control records",                 //Natural: COMPRESS 'Program' *PROGRAM '- More than one active control records' 'exist' INTO MSG-INFO-SUB.##MSG
                "exist"));
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape())) {return;}
            //* (1750)
            //*  PENDING STATUS
            //*  PENDING STATUS
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status.setValue("P");                                                                                                              //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#RUN-STATUS := 'P'
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status.setValue("P");                                                                                                                //Natural: ASSIGN #TBL-PRIME-KEY-TO.#RUN-STATUS := 'P'
        //*  (THE REST OF THE KEY SET UP VARIABLES ARE ALREADY SET AS A RESULT OF
        //*  THE ACTIVE STATUS CHECK ABOVE)
        vw_cwf_Support_Tbl_Histogram.createHistogram                                                                                                                      //Natural: HISTOGRAM CWF-SUPPORT-TBL-HISTOGRAM FOR TBL-PRIME-KEY FROM #TBL-PRIME-KEY-FROM THRU #TBL-PRIME-KEY-TO
        (
        "PENDING_HISTOGRAM",
        "TBL_PRIME_KEY",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key_From, "And", WcType.WITH) ,
        new Wc("TBL_PRIME_KEY", "<=", pnd_Tbl_Prime_Key_To, WcType.WITH) }
        );
        PENDING_HISTOGRAM:
        while (condition(vw_cwf_Support_Tbl_Histogram.readNextRow("PENDING_HISTOGRAM")))
        {
            pnd_Tbl_Prime_Key.setValue(cwf_Support_Tbl_Histogram_Tbl_Prime_Key);                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY := CWF-SUPPORT-TBL-HISTOGRAM.TBL-PRIME-KEY
            ldaIcwl3000.getVw_cwf_Support_Tbl().startDatabaseFind                                                                                                         //Natural: FIND ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
            (
            "PENDING_FIND",
            new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
            1
            );
            PENDING_FIND:
            while (condition(ldaIcwl3000.getVw_cwf_Support_Tbl().readNextRow("PENDING_FIND")))
            {
                ldaIcwl3000.getVw_cwf_Support_Tbl().setIfNotFoundControlFlag(false);
                ldaIcwl3000.getCwf_Support_Tbl_Run_Status().setValue("A");                                                                                                //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-STATUS := 'A'
                ldaIcwl3000.getCwf_Support_Tbl_Tbl_Updte_Dte().setValue(Global.getDATX());                                                                                //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-UPDTE-DTE := *DATX
                ldaIcwl3000.getVw_cwf_Support_Tbl().updateDBRow("PENDING_FIND");                                                                                          //Natural: UPDATE ( PENDING-FIND. )
                pdaIcwa3000.getIcwa3000_Output_Run_Control_Isn().setValue(ldaIcwl3000.getVw_cwf_Support_Tbl().getAstISN("PENDING_FIND"));                                 //Natural: ASSIGN ICWA3000-OUTPUT.RUN-CONTROL-ISN := *ISN ( PENDING-FIND. )
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PENDING_FIND"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PENDING_FIND"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* (PENDING-FIND.)
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PENDING_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PENDING_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* (PENDING-HISTOGRAM.)
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //*  THERE ARE NEITHER ACTIVE NOR PENDING CONTROL RECORDS IF THIS POINT IN
        //*  THE PROCESS IS REACHED.  CHECK THE CONTROL RECORD FOR THE LAST RUN ID
        //*  AND, IF THE FLAG THERE TO CONTROL AUTOMATIC RUN CONTROL RECORD
        //*  CREATION ALLOWS, CREATE A NEW ACTIVE RECORD.
        //*  ---------------------------------------------------------------------
        //*  FIRST ALLOCATE A NEW RUN ID
        //*  ---------------------------
        LAST_RUN_ID_GET:                                                                                                                                                  //Natural: GET CWF-SUPPORT-TBL-LAST-RUN-ID #LAST-RUN-ISN
        ldaIcwl3001.getVw_cwf_Support_Tbl_Last_Run_Id().readByID(pnd_Last_Run_Isn.getLong(), "LAST_RUN_ID_GET");
        if (condition(ldaIcwl3001.getCwf_Support_Tbl_Last_Run_Id_Enable_Auto_Run_Control_Creation().equals(false) || ldaIcwl3001.getCwf_Support_Tbl_Last_Run_Id_Last_Run_Id().greater(999999998))) //Natural: IF CWF-SUPPORT-TBL-LAST-RUN-ID.ENABLE-AUTO-RUN-CONTROL-CREATION = FALSE OR CWF-SUPPORT-TBL-LAST-RUN-ID.LAST-RUN-ID GT 999999998
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(25);                                                                                                    //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 25
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Automatic run control creation denied"));              //Natural: COMPRESS 'Program' *PROGRAM '- Automatic run control creation denied' INTO MSG-INFO-SUB.##MSG
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape())) {return;}
            //* (2250)
        }                                                                                                                                                                 //Natural: END-IF
        ldaIcwl3001.getCwf_Support_Tbl_Last_Run_Id_Last_Run_Id().nadd(1);                                                                                                 //Natural: ADD 1 TO CWF-SUPPORT-TBL-LAST-RUN-ID.LAST-RUN-ID
        ldaIcwl3001.getVw_cwf_Support_Tbl_Last_Run_Id().updateDBRow("LAST_RUN_ID_GET");                                                                                   //Natural: UPDATE ( LAST-RUN-ID-GET. )
        pnd_New_Run_Id.setValue(ldaIcwl3001.getCwf_Support_Tbl_Last_Run_Id_Last_Run_Id());                                                                                //Natural: ASSIGN #NEW-RUN-ID := CWF-SUPPORT-TBL-LAST-RUN-ID.LAST-RUN-ID
        //*  LOOK UP THE MOST RECENT PERIOD ARCHIVED TO CREATE THE NEW RECORD
        //*  ----------------------------------------------------------------
        //*  COMPLETED STATUS
        pnd_Tbl_Prime_Key.reset();                                                                                                                                        //Natural: RESET #TBL-PRIME-KEY
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("ICW-EXTR-RPT-NXT-RUN");                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME := 'ICW-EXTR-RPT-NXT-RUN'
        pnd_Tbl_Prime_Key_Pnd_Run_Status.setValue("C");                                                                                                                   //Natural: ASSIGN #TBL-PRIME-KEY.#RUN-STATUS := 'C'
        ldaIcwl3000.getVw_cwf_Support_Tbl().startDatabaseRead                                                                                                             //Natural: READ ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "LAST_COMPLETED_READ",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        LAST_COMPLETED_READ:
        while (condition(ldaIcwl3000.getVw_cwf_Support_Tbl().readNextRow("LAST_COMPLETED_READ")))
        {
            if (condition(ldaIcwl3000.getCwf_Support_Tbl_Tbl_Scrty_Level_Ind().notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind) || ldaIcwl3000.getCwf_Support_Tbl_Tbl_Table_Nme().notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)  //Natural: IF CWF-SUPPORT-TBL.TBL-SCRTY-LEVEL-IND NE #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND OR CWF-SUPPORT-TBL.TBL-TABLE-NME NE #TBL-PRIME-KEY.#TBL-TABLE-NME OR CWF-SUPPORT-TBL.RUN-STATUS NE #TBL-PRIME-KEY.#RUN-STATUS
                || ldaIcwl3000.getCwf_Support_Tbl_Run_Status().notEquals(pnd_Tbl_Prime_Key_Pnd_Run_Status)))
            {
                ldaIcwl3000.getVw_cwf_Support_Tbl().getAstCOUNTER().nsubtract(1);                                                                                         //Natural: SUBTRACT 1 FROM *COUNTER ( LAST-COMPLETED-READ. )
                if (true) break LAST_COMPLETED_READ;                                                                                                                      //Natural: ESCAPE BOTTOM ( LAST-COMPLETED-READ. )
                //* (2510)
            }                                                                                                                                                             //Natural: END-IF
            ldaIcwl3000.getCwf_Support_Tbl_Run_Status().setValue("A");                                                                                                    //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-STATUS := 'A'
            ldaIcwl3000.getCwf_Support_Tbl_Starting_Date().compute(new ComputeParameters(false, ldaIcwl3000.getCwf_Support_Tbl_Starting_Date()), ldaIcwl3000.getCwf_Support_Tbl_Ending_Date().add(1)); //Natural: ASSIGN CWF-SUPPORT-TBL.STARTING-DATE := CWF-SUPPORT-TBL.ENDING-DATE + 1
            pnd_End_Calc_Date_Pnd_End_Calc_Dte.setValue(Global.getDATN());                                                                                                //Natural: ASSIGN #END-CALC-DTE := *DATN
            pnd_End_Calc_Date_Pnd_End_Calc_Tme.setValue(Global.getTIMN());                                                                                                //Natural: ASSIGN #END-CALC-TME := *TIMN
            ldaIcwl3000.getCwf_Support_Tbl_Ending_Date().setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_End_Calc_Date);                                         //Natural: MOVE EDITED #END-CALC-DATE TO CWF-SUPPORT-TBL.ENDING-DATE ( EM = YYYYMMDDHHIISST )
            //* ***
            if (condition(ldaIcwl3000.getCwf_Support_Tbl_Ending_Date().less(ldaIcwl3000.getCwf_Support_Tbl_Starting_Date())))                                             //Natural: IF CWF-SUPPORT-TBL.ENDING-DATE LT CWF-SUPPORT-TBL.STARTING-DATE
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(21);                                                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 21
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Ending-Date less than Starting-Date"));            //Natural: COMPRESS 'Program' *PROGRAM '- Ending-Date less than Starting-Date' INTO MSG-INFO-SUB.##MSG
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LAST_COMPLETED_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LAST_COMPLETED_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* (2710)
            }                                                                                                                                                             //Natural: END-IF
            //* ***
            //*  ENDING-DATE(LAST-COMPLETED-READ.) := #CUT-OFF-DATE
            pnd_Str_Calc_Date_A.setValueEdited(ldaIcwl3000.getCwf_Support_Tbl_Starting_Date(),new ReportEditMask("YYYYMMDDHHIISST"));                                     //Natural: MOVE EDITED CWF-SUPPORT-TBL.STARTING-DATE ( EM = YYYYMMDDHHIISST ) TO #STR-CALC-DATE-A
            ldaIcwl3000.getCwf_Support_Tbl_Starting_Date_9s_Complement().compute(new ComputeParameters(false, ldaIcwl3000.getCwf_Support_Tbl_Starting_Date_9s_Complement()),  //Natural: ASSIGN CWF-SUPPORT-TBL.STARTING-DATE-9S-COMPLEMENT := 999999999999999 - #STR-CALC-DATE-N
                new DbsDecimal("999999999999999").subtract(pnd_Str_Calc_Date_A_Pnd_Str_Calc_Date_N));
            ldaIcwl3000.getCwf_Support_Tbl_Run_Id().setValue(pnd_New_Run_Id);                                                                                             //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-ID := #NEW-RUN-ID
            ldaIcwl3000.getCwf_Support_Tbl_Tbl_Entry_Dte_Tme().setValue(Global.getTIMX());                                                                                //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-ENTRY-DTE-TME := *TIMX
            ldaIcwl3000.getCwf_Support_Tbl_Tbl_Entry_Oprtr_Cde().setValue("ICWB3000");                                                                                    //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-ENTRY-OPRTR-CDE := 'ICWB3000'
            ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Upd_Wr().reset();                                                                                                  //Natural: RESET CWF-SUPPORT-TBL.NBR-OF-RECS-UPD-WR CWF-SUPPORT-TBL.NBR-OF-RECS-ADD-WR CWF-SUPPORT-TBL.NBR-OF-RECS-OPN-AC CWF-SUPPORT-TBL.NBR-OF-RECS-CLS-AC CWF-SUPPORT-TBL.NBR-OF-RECS-SPL-PR CWF-SUPPORT-TBL.RUN-DATE CWF-SUPPORT-TBL.TBL-UPDTE-DTE-TME CWF-SUPPORT-TBL.TBL-UPDTE-DTE CWF-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE CWF-SUPPORT-TBL.TBL-DLTE-DTE-TME CWF-SUPPORT-TBL.TBL-DLTE-OPRTR-CDE
            ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Add_Wr().reset();
            ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Opn_Ac().reset();
            ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Cls_Ac().reset();
            ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Spl_Pr().reset();
            ldaIcwl3000.getCwf_Support_Tbl_Run_Date().reset();
            ldaIcwl3000.getCwf_Support_Tbl_Tbl_Updte_Dte_Tme().reset();
            ldaIcwl3000.getCwf_Support_Tbl_Tbl_Updte_Dte().reset();
            ldaIcwl3000.getCwf_Support_Tbl_Tbl_Updte_Oprtr_Cde().reset();
            ldaIcwl3000.getCwf_Support_Tbl_Tbl_Dlte_Dte_Tme().reset();
            ldaIcwl3000.getCwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde().reset();
            CREATED_CONTROL_RECORD:                                                                                                                                       //Natural: STORE CWF-SUPPORT-TBL
            ldaIcwl3000.getVw_cwf_Support_Tbl().insertDBRow("CREATED_CONTROL_RECORD");
            pdaIcwa3000.getIcwa3000_Output_Run_Control_Isn().setValue(ldaIcwl3000.getVw_cwf_Support_Tbl().getAstISN("LAST_COMPLETED_READ"));                              //Natural: ASSIGN ICWA3000-OUTPUT.RUN-CONTROL-ISN := *ISN ( CREATED-CONTROL-RECORD. )
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("LAST_COMPLETED_READ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("LAST_COMPLETED_READ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* (LAST-COMPLETED-READ.)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIcwl3000.getVw_cwf_Support_Tbl().getAstCOUNTER().equals(getZero())))                                                                             //Natural: IF *COUNTER ( LAST-COMPLETED-READ. ) = 0
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(25);                                                                                                    //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 25
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Automatic run control creation denied"));              //Natural: COMPRESS 'Program' *PROGRAM '- Automatic run control creation denied' INTO MSG-INFO-SUB.##MSG
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape())) {return;}
            //* (3110)
        }                                                                                                                                                                 //Natural: END-IF
        //* (1000)
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(999);                                                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR = 999
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("NATURAL ERROR", Global.getERROR_NR(), "IN", Global.getPROGRAM(), "....LINE",                 //Natural: COMPRESS 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE INTO MSG-INFO-SUB.##MSG
            Global.getERROR_LINE()));
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
        //* (3240)
    };                                                                                                                                                                    //Natural: END-ERROR
}
