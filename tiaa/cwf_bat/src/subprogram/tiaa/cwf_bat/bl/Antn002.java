/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:58:47 PM
**        * FROM NATURAL SUBPROGRAM : Antn002
************************************************************
**        * FILE NAME            : Antn002.java
**        * CLASS NAME           : Antn002
**        * INSTANCE NAME        : Antn002
************************************************************
************************************************************************
*
* NAME : ANTN002
*
* DESC : THIS PROGRAM RETURNS THE CURRENT P&I RATES.
*
* DATE : JAN. 1998
*
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Antn002 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAnta002 pdaAnta002;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_sys012v;
    private DbsField sys012v_Dpi_Key;

    private DbsGroup sys012v__R_Field_1;
    private DbsField sys012v_Dpi_Id;
    private DbsField sys012v_Dpi_End_Dat;

    private DbsGroup sys012v_Dpi_Rec;
    private DbsField sys012v_Dpi_Beg_Dat;
    private DbsField sys012v_Dpi_Tiaa_Rate;

    private DbsGroup sys012v__R_Field_2;
    private DbsField sys012v_Dpi_Tiaa_Int_Rate;
    private DbsField sys012v_Dpi_Cref_Rate;

    private DbsGroup sys012v__R_Field_3;
    private DbsField sys012v_Dpi_Cref_Int_Rate;
    private DbsField sys012v_Dpi_Insr_Rate;

    private DbsGroup sys012v__R_Field_4;
    private DbsField sys012v_Dpi_Insr_Int_Rate;
    private DbsField sys012v_Dpi_Res;

    private DbsGroup sys012v__R_Field_5;

    private DbsGroup sys012v_Special_St_Rec;

    private DbsGroup sys012v_Without_Rule_Rec;
    private DbsField sys012v_Valid_Ind_Wo_Rule;
    private DbsField sys012v_Number_Of_Days_After;
    private DbsField sys012v_Dci_Grace_Period;
    private DbsField sys012v_Int_Rate_Wo_Ru;

    private DbsGroup sys012v__R_Field_6;
    private DbsField sys012v_Int_Rate_Wo_Rule;

    private DbsGroup sys012v_With_Rule_Rec;
    private DbsField sys012v_Valid_Ind_W_Rule;
    private DbsField sys012v_Number_Of_Days_Before;
    private DbsField sys012v_Int_Rate_W_Ru;

    private DbsGroup sys012v__R_Field_7;
    private DbsField sys012v_Int_Rate_W_Rule;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaAnta002 = new PdaAnta002(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_sys012v = new DataAccessProgramView(new NameInfo("vw_sys012v", "SYS012V"), "ACT_DPI_FACTOR_FILE", "ACT_DPI_FACTOR_FILE");
        sys012v_Dpi_Key = vw_sys012v.getRecord().newFieldInGroup("sys012v_Dpi_Key", "DPI-KEY", FieldType.STRING, 10, RepeatingFieldStrategy.None, "DPI_KEY");

        sys012v__R_Field_1 = vw_sys012v.getRecord().newGroupInGroup("sys012v__R_Field_1", "REDEFINE", sys012v_Dpi_Key);
        sys012v_Dpi_Id = sys012v__R_Field_1.newFieldInGroup("sys012v_Dpi_Id", "DPI-ID", FieldType.STRING, 2);
        sys012v_Dpi_End_Dat = sys012v__R_Field_1.newFieldInGroup("sys012v_Dpi_End_Dat", "DPI-END-DAT", FieldType.NUMERIC, 8);

        sys012v_Dpi_Rec = vw_sys012v.getRecord().newGroupInGroup("SYS012V_DPI_REC", "DPI-REC");
        sys012v_Dpi_Beg_Dat = sys012v_Dpi_Rec.newFieldInGroup("sys012v_Dpi_Beg_Dat", "DPI-BEG-DAT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "DPI_BEG_DAT");
        sys012v_Dpi_Tiaa_Rate = sys012v_Dpi_Rec.newFieldInGroup("sys012v_Dpi_Tiaa_Rate", "DPI-TIAA-RATE", FieldType.BINARY, 8, RepeatingFieldStrategy.None, 
            "DPI_TIAA_RATE");

        sys012v__R_Field_2 = sys012v_Dpi_Rec.newGroupInGroup("sys012v__R_Field_2", "REDEFINE", sys012v_Dpi_Tiaa_Rate);
        sys012v_Dpi_Tiaa_Int_Rate = sys012v__R_Field_2.newFieldInGroup("sys012v_Dpi_Tiaa_Int_Rate", "DPI-TIAA-INT-RATE", FieldType.FLOAT, 8);
        sys012v_Dpi_Cref_Rate = sys012v_Dpi_Rec.newFieldInGroup("sys012v_Dpi_Cref_Rate", "DPI-CREF-RATE", FieldType.BINARY, 8, RepeatingFieldStrategy.None, 
            "DPI_CREF_RATE");

        sys012v__R_Field_3 = sys012v_Dpi_Rec.newGroupInGroup("sys012v__R_Field_3", "REDEFINE", sys012v_Dpi_Cref_Rate);
        sys012v_Dpi_Cref_Int_Rate = sys012v__R_Field_3.newFieldInGroup("sys012v_Dpi_Cref_Int_Rate", "DPI-CREF-INT-RATE", FieldType.FLOAT, 8);
        sys012v_Dpi_Insr_Rate = sys012v_Dpi_Rec.newFieldInGroup("sys012v_Dpi_Insr_Rate", "DPI-INSR-RATE", FieldType.BINARY, 8, RepeatingFieldStrategy.None, 
            "DPI_INSR_RATE");

        sys012v__R_Field_4 = sys012v_Dpi_Rec.newGroupInGroup("sys012v__R_Field_4", "REDEFINE", sys012v_Dpi_Insr_Rate);
        sys012v_Dpi_Insr_Int_Rate = sys012v__R_Field_4.newFieldInGroup("sys012v_Dpi_Insr_Int_Rate", "DPI-INSR-INT-RATE", FieldType.FLOAT, 8);
        sys012v_Dpi_Res = sys012v_Dpi_Rec.newFieldInGroup("sys012v_Dpi_Res", "DPI-RES", FieldType.STRING, 40, RepeatingFieldStrategy.None, "DPI_RES");

        sys012v__R_Field_5 = vw_sys012v.getRecord().newGroupInGroup("sys012v__R_Field_5", "REDEFINE", sys012v_Dpi_Rec);

        sys012v_Special_St_Rec = sys012v__R_Field_5.newGroupArrayInGroup("sys012v_Special_St_Rec", "SPECIAL-ST-REC", new DbsArrayController(1, 3));

        sys012v_Without_Rule_Rec = sys012v_Special_St_Rec.newGroupInGroup("sys012v_Without_Rule_Rec", "WITHOUT-RULE-REC");
        sys012v_Valid_Ind_Wo_Rule = sys012v_Without_Rule_Rec.newFieldInGroup("sys012v_Valid_Ind_Wo_Rule", "VALID-IND-WO-RULE", FieldType.STRING, 1);
        sys012v_Number_Of_Days_After = sys012v_Without_Rule_Rec.newFieldInGroup("sys012v_Number_Of_Days_After", "NUMBER-OF-DAYS-AFTER", FieldType.NUMERIC, 
            2);
        sys012v_Dci_Grace_Period = sys012v_Without_Rule_Rec.newFieldInGroup("sys012v_Dci_Grace_Period", "DCI-GRACE-PERIOD", FieldType.NUMERIC, 2);
        sys012v_Int_Rate_Wo_Ru = sys012v_Without_Rule_Rec.newFieldInGroup("sys012v_Int_Rate_Wo_Ru", "INT-RATE-WO-RU", FieldType.BINARY, 8);

        sys012v__R_Field_6 = sys012v_Without_Rule_Rec.newGroupInGroup("sys012v__R_Field_6", "REDEFINE", sys012v_Int_Rate_Wo_Ru);
        sys012v_Int_Rate_Wo_Rule = sys012v__R_Field_6.newFieldInGroup("sys012v_Int_Rate_Wo_Rule", "INT-RATE-WO-RULE", FieldType.FLOAT, 8);

        sys012v_With_Rule_Rec = sys012v_Special_St_Rec.newGroupInGroup("sys012v_With_Rule_Rec", "WITH-RULE-REC");
        sys012v_Valid_Ind_W_Rule = sys012v_With_Rule_Rec.newFieldInGroup("sys012v_Valid_Ind_W_Rule", "VALID-IND-W-RULE", FieldType.STRING, 1);
        sys012v_Number_Of_Days_Before = sys012v_With_Rule_Rec.newFieldInGroup("sys012v_Number_Of_Days_Before", "NUMBER-OF-DAYS-BEFORE", FieldType.NUMERIC, 
            2);
        sys012v_Int_Rate_W_Ru = sys012v_With_Rule_Rec.newFieldInGroup("sys012v_Int_Rate_W_Ru", "INT-RATE-W-RU", FieldType.BINARY, 8);

        sys012v__R_Field_7 = sys012v_With_Rule_Rec.newGroupInGroup("sys012v__R_Field_7", "REDEFINE", sys012v_Int_Rate_W_Ru);
        sys012v_Int_Rate_W_Rule = sys012v__R_Field_7.newFieldInGroup("sys012v_Int_Rate_W_Rule", "INT-RATE-W-RULE", FieldType.FLOAT, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_sys012v.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Antn002() throws Exception
    {
        super("Antn002");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Code().reset();                                                                                                       //Natural: RESET #LS-RET-CODE #LS-RET-MSG
        pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Msg().reset();
        pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Tiaa_Rate().reset();                                                                                               //Natural: RESET #LS-P&I-TIAA-RATE #LS-P&I-CREF-RATE #LS-P&I-INSR-RATE
        pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Cref_Rate().reset();
        pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Insr_Rate().reset();
        if (condition(pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Curr_Bus_Date().equals(getZero())))                                                                         //Natural: IF #LS-CURR-BUS-DATE = 0
        {
            pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Code().setValue(1);                                                                                               //Natural: ASSIGN #LS-RET-CODE := 0001
            pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Msg().setValue(" CURR BUSINESS DATE IS ZEROS");                                                                   //Natural: ASSIGN #LS-RET-MSG := ' CURR BUSINESS DATE IS ZEROS'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        vw_sys012v.startDatabaseRead                                                                                                                                      //Natural: READ SYS012V BY DPI-KEY
        (
        "READ01",
        new Oc[] { new Oc("DPI_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_sys012v.readNextRow("READ01")))
        {
            //*        WRITE DPI-BEG-DAT DPI-END-DAT DPI-TIAA-INT-RATE
            //*              DPI-CREF-INT-RATE DPI-INSR-RATE
            //*        IF DPI-ID = '00'
            if (condition(pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Curr_Bus_Date().greaterOrEqual(sys012v_Dpi_Beg_Dat) && pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Curr_Bus_Date().lessOrEqual(sys012v_Dpi_End_Dat))) //Natural: IF #LS-CURR-BUS-DATE NOT < DPI-BEG-DAT AND #LS-CURR-BUS-DATE NOT > DPI-END-DAT
            {
                pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Tiaa_Rate().compute(new ComputeParameters(true, pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Tiaa_Rate()),  //Natural: ASSIGN ROUNDED #LS-P&I-TIAA-RATE = DPI-TIAA-INT-RATE
                    sys012v_Dpi_Tiaa_Int_Rate);
                pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Cref_Rate().compute(new ComputeParameters(true, pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Cref_Rate()),  //Natural: ASSIGN ROUNDED #LS-P&I-CREF-RATE = DPI-CREF-INT-RATE
                    sys012v_Dpi_Cref_Int_Rate);
                pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Insr_Rate().compute(new ComputeParameters(true, pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Insr_Rate()),  //Natural: ASSIGN ROUNDED #LS-P&I-INSR-RATE = DPI-INSR-INT-RATE
                    sys012v_Dpi_Insr_Int_Rate);
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*        END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Tiaa_Rate().equals(getZero()) || pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Cref_Rate().equals(getZero())  //Natural: IF #LS-P&I-TIAA-RATE = 0 OR #LS-P&I-CREF-RATE = 0 OR #LS-P&I-INSR-RATE = 0
            || pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Insr_Rate().equals(getZero())))
        {
            pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Code().setValue(2);                                                                                               //Natural: ASSIGN #LS-RET-CODE := 0002
            pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Msg().setValue(" PROBLEM IN READING DPI FACTOR FILE");                                                            //Natural: ASSIGN #LS-RET-MSG := ' PROBLEM IN READING DPI FACTOR FILE'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE IMMEDIATE
    }

    //
}
