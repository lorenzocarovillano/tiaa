/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:13:08 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn1125
************************************************************
**        * FILE NAME            : Cwfn1125.java
**        * CLASS NAME           : Cwfn1125
**        * INSTANCE NAME        : Cwfn1125
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: FIND WPID DESC
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFN1105
* SYSTEM   : CRPCWF
* TITLE    : FIND WPID DESC
* GENERATED: JUN 09,93 AT 01:56 PM
* FUNCTION : THIS PROGRAM RETRIEVES THE WPID NAME USING
*          | THE PASSED WPID.
*          |
*          | THIS IS A CLONE OF CWFN1105.
*          |
*          |
*          |
* MOD DATE   MOD BY   DESCRIPTION OF CHANGES
* NOV 30 01  GY       ADD OWNER-UNIT-CDE TO PASSED PARAMETERS.
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn1125 extends BLNatBase
{
    // Data Areas
    private PdaCwfl5312 pdaCwfl5312;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Wpid;
    private DbsField pnd_Wpid_Desc;
    private DbsField pnd_Owner_Unit_Cde;

    private DataAccessProgramView vw_cwf_Wpid;
    private DbsField cwf_Wpid_Work_Prcss_Id;
    private DbsField cwf_Wpid_Work_Prcss_Long_Nme;
    private DbsField cwf_Wpid_Work_Prcss_Short_Nme;
    private DbsField cwf_Wpid_Dlte_Oprtr_Cde;
    private DbsField cwf_Wpid_Owner_Unit_Cde;

    private DataAccessProgramView vw_route_Cde_Tbl;
    private DbsField route_Cde_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField route_Cde_Tbl_Tbl_Table_Nme;
    private DbsField route_Cde_Tbl_Tbl_Key_Field;
    private DbsField route_Cde_Tbl_Tbl_Data_Field;

    private DbsGroup route_Cde_Tbl__R_Field_1;
    private DbsField route_Cde_Tbl_Wpid;
    private DbsField route_Cde_Tbl_Wpid_Desc;
    private DbsField route_Cde_Tbl_Unit_Code;
    private DbsField route_Cde_Tbl_Status_Code;
    private DbsField route_Cde_Tbl_Status_Desc;
    private DbsField route_Cde_Tbl_Work_Step;
    private DbsField route_Cde_Tbl_Step_Name;
    private DbsField route_Cde_Tbl_Route_Desc;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfl5312 = new PdaCwfl5312(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Wpid = parameters.newFieldInRecord("pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Wpid.setParameterOption(ParameterOption.ByReference);
        pnd_Wpid_Desc = parameters.newFieldInRecord("pnd_Wpid_Desc", "#WPID-DESC", FieldType.STRING, 45);
        pnd_Wpid_Desc.setParameterOption(ParameterOption.ByReference);
        pnd_Owner_Unit_Cde = parameters.newFieldInRecord("pnd_Owner_Unit_Cde", "#OWNER-UNIT-CDE", FieldType.STRING, 8);
        pnd_Owner_Unit_Cde.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_cwf_Wpid = new DataAccessProgramView(new NameInfo("vw_cwf_Wpid", "CWF-WPID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wpid_Work_Prcss_Id = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Wpid_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wpid_Work_Prcss_Long_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        cwf_Wpid_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        cwf_Wpid_Work_Prcss_Short_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wpid_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        cwf_Wpid_Dlte_Oprtr_Cde = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DLTE_OPRTR_CDE");
        cwf_Wpid_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        cwf_Wpid_Owner_Unit_Cde = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Owner_Unit_Cde", "OWNER-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "OWNER_UNIT_CDE");
        cwf_Wpid_Owner_Unit_Cde.setDdmHeader("OWNER/UNIT");
        registerRecord(vw_cwf_Wpid);

        vw_route_Cde_Tbl = new DataAccessProgramView(new NameInfo("vw_route_Cde_Tbl", "ROUTE-CDE-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        route_Cde_Tbl_Tbl_Scrty_Level_Ind = vw_route_Cde_Tbl.getRecord().newFieldInGroup("route_Cde_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        route_Cde_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        route_Cde_Tbl_Tbl_Table_Nme = vw_route_Cde_Tbl.getRecord().newFieldInGroup("route_Cde_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, 
            RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        route_Cde_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        route_Cde_Tbl_Tbl_Key_Field = vw_route_Cde_Tbl.getRecord().newFieldInGroup("route_Cde_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        route_Cde_Tbl_Tbl_Data_Field = vw_route_Cde_Tbl.getRecord().newFieldInGroup("route_Cde_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        route_Cde_Tbl__R_Field_1 = vw_route_Cde_Tbl.getRecord().newGroupInGroup("route_Cde_Tbl__R_Field_1", "REDEFINE", route_Cde_Tbl_Tbl_Data_Field);
        route_Cde_Tbl_Wpid = route_Cde_Tbl__R_Field_1.newFieldInGroup("route_Cde_Tbl_Wpid", "WPID", FieldType.STRING, 6);
        route_Cde_Tbl_Wpid_Desc = route_Cde_Tbl__R_Field_1.newFieldInGroup("route_Cde_Tbl_Wpid_Desc", "WPID-DESC", FieldType.STRING, 15);
        route_Cde_Tbl_Unit_Code = route_Cde_Tbl__R_Field_1.newFieldInGroup("route_Cde_Tbl_Unit_Code", "UNIT-CODE", FieldType.STRING, 8);
        route_Cde_Tbl_Status_Code = route_Cde_Tbl__R_Field_1.newFieldInGroup("route_Cde_Tbl_Status_Code", "STATUS-CODE", FieldType.STRING, 4);
        route_Cde_Tbl_Status_Desc = route_Cde_Tbl__R_Field_1.newFieldInGroup("route_Cde_Tbl_Status_Desc", "STATUS-DESC", FieldType.STRING, 25);
        route_Cde_Tbl_Work_Step = route_Cde_Tbl__R_Field_1.newFieldInGroup("route_Cde_Tbl_Work_Step", "WORK-STEP", FieldType.STRING, 6);
        route_Cde_Tbl_Step_Name = route_Cde_Tbl__R_Field_1.newFieldInGroup("route_Cde_Tbl_Step_Name", "STEP-NAME", FieldType.STRING, 15);
        route_Cde_Tbl_Route_Desc = route_Cde_Tbl__R_Field_1.newFieldInGroup("route_Cde_Tbl_Route_Desc", "ROUTE-DESC", FieldType.STRING, 41);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Wpid.reset();
        vw_route_Cde_Tbl.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn1125() throws Exception
    {
        super("Cwfn1125");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*  DEFINE FORMATS
        //* ******************** START OF MAIN PROGRAM LOGIC **********************                                                                                       //Natural: FORMAT KD = ON
        pnd_Wpid_Desc.reset();                                                                                                                                            //Natural: RESET #WPID-DESC
        if (condition(DbsUtil.maskMatches(pnd_Wpid,"NN....")))                                                                                                            //Natural: IF #WPID EQ MASK ( NN.... )
        {
            pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Authrty().setValue("S");                                                                                               //Natural: MOVE 'S' TO #GEN-TBL-AUTHRTY
            pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Name().setValue("ROUTE-CODE-TABLE");                                                                                   //Natural: MOVE 'ROUTE-CODE-TABLE' TO #GEN-TBL-NAME
            pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Code().setValue(pnd_Wpid);                                                                                             //Natural: MOVE #WPID TO #GEN-TBL-CODE
            pnd_Wpid_Desc.setValue(" ");                                                                                                                                  //Natural: MOVE ' ' TO #WPID-DESC
            vw_route_Cde_Tbl.startDatabaseFind                                                                                                                            //Natural: FIND ( 1 ) ROUTE-CDE-TBL WITH TBL-PRIME-KEY EQ #GEN-TBL-KEY
            (
            "FIND01",
            new Wc[] { new Wc("TBL_PRIME_KEY", "=", pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Key(), WcType.WITH) },
            1
            );
            FIND01:
            while (condition(vw_route_Cde_Tbl.readNextRow("FIND01")))
            {
                vw_route_Cde_Tbl.setIfNotFoundControlFlag(false);
                pnd_Wpid_Desc.setValue(route_Cde_Tbl_Wpid_Desc);                                                                                                          //Natural: MOVE ROUTE-CDE-TBL.WPID-DESC TO #WPID-DESC
                pnd_Owner_Unit_Cde.setValue("????????");                                                                                                                  //Natural: MOVE '????????' TO #OWNER-UNIT-CDE
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            vw_cwf_Wpid.startDatabaseRead                                                                                                                                 //Natural: READ ( 1 ) CWF-WPID BY WPID-UNIQ-KEY FROM #WPID
            (
            "READ",
            new Wc[] { new Wc("WPID_UNIQ_KEY", ">=", pnd_Wpid, WcType.BY) },
            new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") },
            1
            );
            READ:
            while (condition(vw_cwf_Wpid.readNextRow("READ")))
            {
                pnd_Owner_Unit_Cde.setValue(cwf_Wpid_Owner_Unit_Cde);                                                                                                     //Natural: MOVE OWNER-UNIT-CDE TO #OWNER-UNIT-CDE
                if (condition(cwf_Wpid_Work_Prcss_Id.greater(pnd_Wpid)))                                                                                                  //Natural: IF WORK-PRCSS-ID GT #WPID
                {
                    pnd_Wpid_Desc.setValue(DbsUtil.compress("* Test file * ", cwf_Wpid_Work_Prcss_Id, "GT", pnd_Wpid));                                                   //Natural: COMPRESS '* Test file * ' CWF-WPID.WORK-PRCSS-ID 'GT' #WPID INTO #WPID-DESC
                    if (true) break READ;                                                                                                                                 //Natural: ESCAPE BOTTOM ( READ. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cwf_Wpid_Dlte_Oprtr_Cde.greater(" ")))                                                                                                      //Natural: IF CWF-WPID.DLTE-OPRTR-CDE GT ' '
                {
                    pnd_Wpid_Desc.setValue(DbsUtil.compress("*", cwf_Wpid_Work_Prcss_Long_Nme));                                                                          //Natural: COMPRESS '*' CWF-WPID.WORK-PRCSS-LONG-NME INTO #WPID-DESC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Wpid_Desc.setValue(cwf_Wpid_Work_Prcss_Long_Nme);                                                                                                 //Natural: MOVE WORK-PRCSS-LONG-NME TO #WPID-DESC
                }                                                                                                                                                         //Natural: END-IF
                //*  FIND.
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "KD=ON");
    }
}
