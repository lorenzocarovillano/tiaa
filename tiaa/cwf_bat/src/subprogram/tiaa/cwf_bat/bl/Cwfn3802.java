/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:28:22 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn3802
************************************************************
**        * FILE NAME            : Cwfn3802.java
**        * CLASS NAME           : Cwfn3802
**        * INSTANCE NAME        : Cwfn3802
************************************************************
* CWFN3802
*
* 06/09/2017 - PIN EXPANSION - AUG 2017

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn3802 extends BLNatBase
{
    // Data Areas
    private PdaCwfa5372 pdaCwfa5372;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField cwf_Master_Index_Pin_Nbr;
    private DbsField cwf_Master_Index_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_Physcl_Fldr_Id_Nbr;
    private DbsField pnd_Partic_Sname;

    private DbsGroup pnd_Partic_Sname__R_Field_1;
    private DbsField pnd_Partic_Sname_Pnd_Fldr_Prefix;
    private DbsField pnd_Partic_Sname_Pnd_Physcl_Fldr_Id_Nbr;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa5372 = new PdaCwfa5372(localVariables);

        // parameters
        parameters = new DbsRecord();
        cwf_Master_Index_Pin_Nbr = parameters.newFieldInRecord("cwf_Master_Index_Pin_Nbr", "CWF-MASTER-INDEX-PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Master_Index_Pin_Nbr.setParameterOption(ParameterOption.ByReference);
        cwf_Master_Index_Rqst_Orgn_Cde = parameters.newFieldInRecord("cwf_Master_Index_Rqst_Orgn_Cde", "CWF-MASTER-INDEX-RQST-ORGN-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_Rqst_Orgn_Cde.setParameterOption(ParameterOption.ByReference);
        cwf_Master_Index_Mj_Pull_Ind = parameters.newFieldInRecord("cwf_Master_Index_Mj_Pull_Ind", "CWF-MASTER-INDEX-MJ-PULL-IND", FieldType.STRING, 1);
        cwf_Master_Index_Mj_Pull_Ind.setParameterOption(ParameterOption.ByReference);
        cwf_Master_Index_Physcl_Fldr_Id_Nbr = parameters.newFieldInRecord("cwf_Master_Index_Physcl_Fldr_Id_Nbr", "CWF-MASTER-INDEX-PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6);
        cwf_Master_Index_Physcl_Fldr_Id_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Partic_Sname = parameters.newFieldInRecord("pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);
        pnd_Partic_Sname.setParameterOption(ParameterOption.ByReference);

        pnd_Partic_Sname__R_Field_1 = parameters.newGroupInRecord("pnd_Partic_Sname__R_Field_1", "REDEFINE", pnd_Partic_Sname);
        pnd_Partic_Sname_Pnd_Fldr_Prefix = pnd_Partic_Sname__R_Field_1.newFieldInGroup("pnd_Partic_Sname_Pnd_Fldr_Prefix", "#FLDR-PREFIX", FieldType.STRING, 
            1);
        pnd_Partic_Sname_Pnd_Physcl_Fldr_Id_Nbr = pnd_Partic_Sname__R_Field_1.newFieldInGroup("pnd_Partic_Sname_Pnd_Physcl_Fldr_Id_Nbr", "#PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn3802() throws Exception
    {
        super("Cwfn3802");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Partic_Sname.reset();                                                                                                                                         //Natural: RESET #PARTIC-SNAME
        pdaCwfa5372.getCwfa5372_Pnd_Pin_Key().setValue(cwf_Master_Index_Pin_Nbr);                                                                                         //Natural: ASSIGN CWFA5372.#PIN-KEY := CWF-MASTER-INDEX-PIN-NBR
        DbsUtil.callnat(Cwfn5372.class , getCurrentProcessState(), pdaCwfa5372.getCwfa5372_Pnd_Pin_Key(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Name(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Ssn(),  //Natural: CALLNAT 'CWFN5372' CWFA5372.#PIN-KEY CWFA5372.#PASS-NAME CWFA5372.#PASS-SSN CWFA5372.#PASS-TLC CWFA5372.#PASS-DOB CWFA5372.#PASS-FOUND
            pdaCwfa5372.getCwfa5372_Pnd_Pass_Tlc(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Dob(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Found());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfa5372.getCwfa5372_Pnd_Pass_Found().getBoolean()))                                                                                             //Natural: IF #PASS-FOUND
        {
            pnd_Partic_Sname.setValue(pdaCwfa5372.getCwfa5372_Pnd_Pass_Name());                                                                                           //Natural: MOVE CWFA5372.#PASS-NAME TO #PARTIC-SNAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(cwf_Master_Index_Rqst_Orgn_Cde.equals("J")))                                                                                                    //Natural: IF CWF-MASTER-INDEX-RQST-ORGN-CDE = 'J'
            {
                pnd_Partic_Sname_Pnd_Fldr_Prefix.setValue("R");                                                                                                           //Natural: MOVE 'R' TO #FLDR-PREFIX
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cwf_Master_Index_Mj_Pull_Ind.equals("Y") || cwf_Master_Index_Mj_Pull_Ind.equals("R")))                                                      //Natural: IF CWF-MASTER-INDEX-MJ-PULL-IND = 'Y' OR = 'R'
                {
                    pnd_Partic_Sname_Pnd_Fldr_Prefix.setValue("M");                                                                                                       //Natural: MOVE 'M' TO #FLDR-PREFIX
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Partic_Sname_Pnd_Fldr_Prefix.setValue("F");                                                                                                       //Natural: MOVE 'F' TO #FLDR-PREFIX
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Partic_Sname_Pnd_Physcl_Fldr_Id_Nbr.setValue(cwf_Master_Index_Physcl_Fldr_Id_Nbr);                                                                        //Natural: MOVE CWF-MASTER-INDEX-PHYSCL-FLDR-ID-NBR TO #PHYSCL-FLDR-ID-NBR
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
