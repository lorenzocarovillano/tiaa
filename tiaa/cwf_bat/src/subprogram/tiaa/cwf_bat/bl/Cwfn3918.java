/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:28:45 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn3918
************************************************************
**        * FILE NAME            : Cwfn3918.java
**        * CLASS NAME           : Cwfn3918
**        * INSTANCE NAME        : Cwfn3918
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT SEPARATOR - NO REPORT PAGE
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFN3910
* SYSTEM   : CRPCWF
* TITLE    : SEPARATOR - START
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : PRINTS NO REPORT PAGE IF THERE ARE NO
*          | REQUESTS SATISFYING THE SELECTION CRITERIA OF THE
*          | REPORT.
*          |
*          |
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn3918 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Parm_Report_No;
    private DbsField pnd_Env;
    private DbsField pnd_Record;
    private DbsField pnd_Report_Names;
    private DbsField pnd_Report_Name;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Parm_Report_No = parameters.newFieldInRecord("pnd_Parm_Report_No", "#PARM-REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Parm_Report_No.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        pnd_Report_Names = localVariables.newFieldArrayInRecord("pnd_Report_Names", "#REPORT-NAMES", FieldType.STRING, 45, new DbsArrayController(1, 12));
        pnd_Report_Name = localVariables.newFieldInRecord("pnd_Report_Name", "#REPORT-NAME", FieldType.STRING, 45);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Report_Names.getValue(1).setInitialValue("WORK REQUESTS LOGGED AND INDEXED             ");
        pnd_Report_Names.getValue(2).setInitialValue("REPORT OF UNKNOWN CASES                      ");
        pnd_Report_Names.getValue(3).setInitialValue("REPORT OF CASES ROUTED ERRONEOUSLY           ");
        pnd_Report_Names.getValue(4).setInitialValue("REQUESTS CURRENTLY ACTIVE IN UNIT            ");
        pnd_Report_Names.getValue(5).setInitialValue("WORK ACTIVITY FOR A PERIOD                   ");
        pnd_Report_Names.getValue(6).setInitialValue("REPORT OF OPEN CASES                         ");
        pnd_Report_Names.getValue(7).setInitialValue("EMPLOYEE CASE REPORT                         ");
        pnd_Report_Names.getValue(8).setInitialValue("REPORT OF CURRENTLY PENDED INTERNAL CASES    ");
        pnd_Report_Names.getValue(9).setInitialValue("REPORT OF CURRENTLY PENDED EXTERNAL CASES    ");
        pnd_Report_Names.getValue(10).setInitialValue("FOLLOW-UP OF EXTERNALLY PENDED CASES         ");
        pnd_Report_Names.getValue(11).setInitialValue("PARTICIPANT-CLOSED CASES                     ");
        pnd_Report_Names.getValue(12).setInitialValue("REPORT OF CASES WITH SPECIFIED STATUS        ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn3918() throws Exception
    {
        super("Cwfn3918");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        pnd_Report_Name.setValue(pnd_Report_Names.getValue(pnd_Parm_Report_No));                                                                                          //Natural: FORMAT ( 1 ) PS = 58 LS = 140;//Natural: MOVE #REPORT-NAMES ( #PARM-REPORT-NO ) TO #REPORT-NAME
        //*  MOVE '�E�&L1O2A5.6C72P3E66F�&A2L�(9905X�&L0L' TO #RECORD
        getReports().write(1, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3918.class));                                                          //Natural: WRITE ( 1 ) NOTITLE NOHDR USING MAP 'CWFF3918'
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=140");
    }
}
