/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:47:12 PM
**        * FROM NATURAL SUBPROGRAM : Cwfx1400
************************************************************
**        * FILE NAME            : Cwfx1400.java
**        * CLASS NAME           : Cwfx1400
**        * INSTANCE NAME        : Cwfx1400
************************************************************
************************************************************************
* PROGRAM  : CWFN1400
* SYSTEM   : CRPCWF
* TITLE    : OBJECT SUBPROGRAM - WPID
* GENERATED: JUN 11,93 AT 12:33 PM
* FUNCTION : THIS SUBPROGRAM IS USED TO PERFORM OBJECT MAINTENANCE
*            FOR...
*
*
* HISTORY
* 10/04/95 | OB  ADDED BACK-END SCAN INDICATOR
* 04/22/96 | BE  ADDED FIELDS FOR DIGITIZING.
* 12/23/96 | GH  ADDED FIELD, SHRD-SRVCE-IND, FOR MUTUAL FUNDS.
* 06/24/97 | JHH ADDED INSRNCE-PRCSS-IND TO CWFA1400 & CWF-WP-WORK-PRCSS
* 08/25/97 | GH  ADDED FIELD FUNCTION-CDE FOR ATS.
* 03/30/98 | GH  ADDED FIELDS WPID-SUPPRESS-IND AND WPID-INTRNET-NME
*          |     FOR INTER/ACT.
* 07/06/98 | JB  ADDED FIELD TEAM-IND FOR ENHANCED SERVICES PROJECT.
*          |     SC ON JB TO REVIEW.
* 03/23/00 | EPM ADDED FIELD WPID-INDS FOR IES
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfx1400 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    public DbsRecord parameters;
    private PdaCwfa1400 pdaCwfa1400;
    private PdaCwfa1401 pdaCwfa1401;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfl1400 pdaCwfl1400;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Max;
    private DbsField pnd_Max_Pnd_Cwf_Wp_Routing;

    private DbsGroup pnd_Min;
    private DbsField pnd_Min_Pnd_Cwf_Wp_Routing;

    private DbsGroup pnd_Call;

    private DbsGroup pnd_Call_Cwf_Wp_Routing_Rel;
    private DbsField pnd_Call_Pnd_Cwf_Wp_Routing;

    private DbsGroup pnd_From;
    private DbsField pnd_From_Cwf_Wp_Routing_Rel;

    private DbsGroup pnd_Indx;
    private DbsField pnd_Indx_Pnd_Cwf_Wp_Routing;

    private DbsGroup pnd_Save;

    private DbsGroup pnd_Save_Cwf_Wp_Routing_Rel;
    private DbsField pnd_Save_Pnd_Cwf_Wp_Routing;

    private DbsGroup pnd_Thru;
    private DbsField pnd_Thru_Cwf_Wp_Routing_Rel;
    private DbsField pnd_Cwf_Wp_Routing_Rel;

    private DbsGroup pnd_Cwf_Wp_Routing_Rel__R_Field_1;

    private DbsGroup pnd_Cwf_Wp_Routing_Rel_Mbn_Structure;
    private DbsField pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Actn_Rqstd_Cde;
    private DbsField pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Lob_Cmpny_Prdct_Cde;
    private DbsField pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Mjr_Bsnss_Prcss_Cde;
    private DbsField pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Spcfc_Bsnss_Prcss_Cde;
    private DbsField pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Nbr;
    private DbsField pnd_Cwf_Wp_Routing_Rel_Actve_Ind_2_2;

    private DbsGroup pnd_Cwf_Wp_Routing_Rel__R_Field_2;
    private DbsField pnd_Cwf_Wp_Routing_Rel_Related_Key;
    private DbsField pnd_Cwf_Wp_Routing_Rel_Min_Max;
    private DbsField pnd_L2;
    private DbsField pnd_Current_Field;
    private DbsField pnd_Db_Call;
    private DbsField pnd_D1;
    private DbsField pnd_D2;
    private DbsField pnd_Object;
    private DbsField pnd_Old_Rec;
    private DbsField pnd_Save_Rec;

    private DataAccessProgramView vw_next_View;
    private DbsField next_View_Wpid_Uniq_Key;

    private DataAccessProgramView vw_cwf_Wp_Work_Prcss_Id;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Id;

    private DbsGroup cwf_Wp_Work_Prcss_Id__R_Field_3;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme;
    private DbsField cwf_Wp_Work_Prcss_Id_Cntrct_Slctn_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Mj_Pull_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Prty_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Check_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Crc_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp;

    private DbsGroup cwf_Wp_Work_Prcss_Id__R_Field_4;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Days_Nbr;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Hours_Nbr;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Mins_Nbr;
    private DbsField cwf_Wp_Work_Prcss_Id_Actve_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme;
    private DbsField cwf_Wp_Work_Prcss_Id_Entry_Oprtr_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Updte_Dte;
    private DbsField cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme;
    private DbsField cwf_Wp_Work_Prcss_Id_Updte_Oprtr_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme;
    private DbsField cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Sec_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Effctve_Dte_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Trnsctn_Dte_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Check_Mail_Dte_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Back_End_Scan_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Dgtze_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Dgtze_Region_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Dgtze_Branch_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Dgtze_Special_Needs_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Shrd_Srvce_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Da_Sra_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Wpid_Usage_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Insrnce_Prcss_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Function_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Wpid_Suppress_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Wpid_Intrnet_Nme;
    private DbsField cwf_Wp_Work_Prcss_Id_Team_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Wpid_Inds;

    private DataAccessProgramView vw_cwf_Wp_Routing;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Prcss_Id;

    private DbsGroup cwf_Wp_Routing__R_Field_5;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Actn_Rqstd_Cde;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Nbr;
    private DbsField cwf_Wp_Routing_Unit_Cde;

    private DbsGroup cwf_Wp_Routing__R_Field_6;
    private DbsField cwf_Wp_Routing_Unit_Id_Cde;
    private DbsField cwf_Wp_Routing_Unit_Rgn_Cde;
    private DbsField cwf_Wp_Routing_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Wp_Routing_Unit_Brnch_Group_Cde;
    private DbsField cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp;

    private DbsGroup cwf_Wp_Routing__R_Field_7;
    private DbsField cwf_Wp_Routing_Crprte_Srvce_Stndrd_Days_Nbr;
    private DbsField cwf_Wp_Routing_Crprte_Srvce_Stndrd_Hours_Nbr;
    private DbsField cwf_Wp_Routing_Crprte_Srvce_Stndrd_Mins_Nbr;
    private DbsField cwf_Wp_Routing_Actve_Ind;

    private DbsGroup cwf_Wp_Routing__R_Field_8;
    private DbsField cwf_Wp_Routing_Fill_1;
    private DbsField cwf_Wp_Routing_Actve_Ind_2_2;
    private DbsField cwf_Wp_Routing_Entry_Dte_Tme;
    private DbsField cwf_Wp_Routing_Entry_Oprtr_Cde;
    private DbsField cwf_Wp_Routing_Updte_Dte;
    private DbsField cwf_Wp_Routing_Updte_Dte_Tme;
    private DbsField cwf_Wp_Routing_Updte_Oprtr_Cde;
    private DbsField cwf_Wp_Routing_Dlte_Dte_Tme;
    private DbsField cwf_Wp_Routing_Dlte_Oprtr_Cde;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaCwfl1400 = new PdaCwfl1400(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaCwfa1400 = new PdaCwfa1400(parameters);
        pdaCwfa1401 = new PdaCwfa1401(parameters);
        pdaCdaobj = new PdaCdaobj(parameters);
        pdaCwfpda_D = new PdaCwfpda_D(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pdaCwfpda_P = new PdaCwfpda_P(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Max = localVariables.newGroupInRecord("pnd_Max", "#MAX");
        pnd_Max_Pnd_Cwf_Wp_Routing = pnd_Max.newFieldInGroup("pnd_Max_Pnd_Cwf_Wp_Routing", "#CWF-WP-ROUTING", FieldType.PACKED_DECIMAL, 5);

        pnd_Min = localVariables.newGroupInRecord("pnd_Min", "#MIN");
        pnd_Min_Pnd_Cwf_Wp_Routing = pnd_Min.newFieldInGroup("pnd_Min_Pnd_Cwf_Wp_Routing", "#CWF-WP-ROUTING", FieldType.PACKED_DECIMAL, 5);

        pnd_Call = localVariables.newGroupInRecord("pnd_Call", "#CALL");

        pnd_Call_Cwf_Wp_Routing_Rel = pnd_Call.newGroupArrayInGroup("pnd_Call_Cwf_Wp_Routing_Rel", "CWF-WP-ROUTING-REL", new DbsArrayController(1, 20));
        pnd_Call_Pnd_Cwf_Wp_Routing = pnd_Call_Cwf_Wp_Routing_Rel.newFieldInGroup("pnd_Call_Pnd_Cwf_Wp_Routing", "#CWF-WP-ROUTING", FieldType.STRING, 
            1);

        pnd_From = localVariables.newGroupInRecord("pnd_From", "#FROM");
        pnd_From_Cwf_Wp_Routing_Rel = pnd_From.newFieldInGroup("pnd_From_Cwf_Wp_Routing_Rel", "CWF-WP-ROUTING-REL", FieldType.STRING, 10);

        pnd_Indx = localVariables.newGroupInRecord("pnd_Indx", "#INDX");
        pnd_Indx_Pnd_Cwf_Wp_Routing = pnd_Indx.newFieldInGroup("pnd_Indx_Pnd_Cwf_Wp_Routing", "#CWF-WP-ROUTING", FieldType.PACKED_DECIMAL, 5);

        pnd_Save = localVariables.newGroupInRecord("pnd_Save", "#SAVE");

        pnd_Save_Cwf_Wp_Routing_Rel = pnd_Save.newGroupArrayInGroup("pnd_Save_Cwf_Wp_Routing_Rel", "CWF-WP-ROUTING-REL", new DbsArrayController(1, 20));
        pnd_Save_Pnd_Cwf_Wp_Routing = pnd_Save_Cwf_Wp_Routing_Rel.newFieldInGroup("pnd_Save_Pnd_Cwf_Wp_Routing", "#CWF-WP-ROUTING", FieldType.BOOLEAN, 
            1);

        pnd_Thru = localVariables.newGroupInRecord("pnd_Thru", "#THRU");
        pnd_Thru_Cwf_Wp_Routing_Rel = pnd_Thru.newFieldInGroup("pnd_Thru_Cwf_Wp_Routing_Rel", "CWF-WP-ROUTING-REL", FieldType.STRING, 10);
        pnd_Cwf_Wp_Routing_Rel = localVariables.newFieldInRecord("pnd_Cwf_Wp_Routing_Rel", "#CWF-WP-ROUTING-REL", FieldType.STRING, 10);

        pnd_Cwf_Wp_Routing_Rel__R_Field_1 = localVariables.newGroupInRecord("pnd_Cwf_Wp_Routing_Rel__R_Field_1", "REDEFINE", pnd_Cwf_Wp_Routing_Rel);

        pnd_Cwf_Wp_Routing_Rel_Mbn_Structure = pnd_Cwf_Wp_Routing_Rel__R_Field_1.newGroupInGroup("pnd_Cwf_Wp_Routing_Rel_Mbn_Structure", "MBN-STRUCTURE");
        pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Actn_Rqstd_Cde = pnd_Cwf_Wp_Routing_Rel_Mbn_Structure.newFieldInGroup("pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Actn_Rqstd_Cde", 
            "RT-SQNCE-ACTN-RQSTD-CDE", FieldType.STRING, 1);
        pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Lob_Cmpny_Prdct_Cde = pnd_Cwf_Wp_Routing_Rel_Mbn_Structure.newFieldInGroup("pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Lob_Cmpny_Prdct_Cde", 
            "RT-SQNCE-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Mjr_Bsnss_Prcss_Cde = pnd_Cwf_Wp_Routing_Rel_Mbn_Structure.newFieldInGroup("pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Mjr_Bsnss_Prcss_Cde", 
            "RT-SQNCE-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Spcfc_Bsnss_Prcss_Cde = pnd_Cwf_Wp_Routing_Rel_Mbn_Structure.newFieldInGroup("pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Spcfc_Bsnss_Prcss_Cde", 
            "RT-SQNCE-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Nbr = pnd_Cwf_Wp_Routing_Rel_Mbn_Structure.newFieldInGroup("pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Cwf_Wp_Routing_Rel_Actve_Ind_2_2 = pnd_Cwf_Wp_Routing_Rel_Mbn_Structure.newFieldInGroup("pnd_Cwf_Wp_Routing_Rel_Actve_Ind_2_2", "ACTVE-IND-2-2", 
            FieldType.STRING, 1);

        pnd_Cwf_Wp_Routing_Rel__R_Field_2 = localVariables.newGroupInRecord("pnd_Cwf_Wp_Routing_Rel__R_Field_2", "REDEFINE", pnd_Cwf_Wp_Routing_Rel);
        pnd_Cwf_Wp_Routing_Rel_Related_Key = pnd_Cwf_Wp_Routing_Rel__R_Field_2.newFieldInGroup("pnd_Cwf_Wp_Routing_Rel_Related_Key", "RELATED-KEY", FieldType.STRING, 
            7);
        pnd_Cwf_Wp_Routing_Rel_Min_Max = pnd_Cwf_Wp_Routing_Rel__R_Field_2.newFieldInGroup("pnd_Cwf_Wp_Routing_Rel_Min_Max", "MIN-MAX", FieldType.STRING, 
            3);
        pnd_L2 = localVariables.newFieldInRecord("pnd_L2", "#L2", FieldType.PACKED_DECIMAL, 5);
        pnd_Current_Field = localVariables.newFieldInRecord("pnd_Current_Field", "#CURRENT-FIELD", FieldType.STRING, 32);
        pnd_Db_Call = localVariables.newFieldInRecord("pnd_Db_Call", "#DB-CALL", FieldType.STRING, 1);
        pnd_D1 = localVariables.newFieldInRecord("pnd_D1", "#D1", FieldType.PACKED_DECIMAL, 3);
        pnd_D2 = localVariables.newFieldInRecord("pnd_D2", "#D2", FieldType.PACKED_DECIMAL, 3);
        pnd_Object = localVariables.newFieldInRecord("pnd_Object", "#OBJECT", FieldType.STRING, 20);
        pnd_Old_Rec = localVariables.newFieldInRecord("pnd_Old_Rec", "#OLD-REC", FieldType.BOOLEAN, 1);
        pnd_Save_Rec = localVariables.newFieldInRecord("pnd_Save_Rec", "#SAVE-REC", FieldType.BOOLEAN, 1);

        vw_next_View = new DataAccessProgramView(new NameInfo("vw_next_View", "NEXT-VIEW"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        next_View_Wpid_Uniq_Key = vw_next_View.getRecord().newFieldInGroup("next_View_Wpid_Uniq_Key", "WPID-UNIQ-KEY", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "WPID_UNIQ_KEY");
        next_View_Wpid_Uniq_Key.setDdmHeader("WORK PROCESS ID");
        next_View_Wpid_Uniq_Key.setSuperDescriptor(true);
        registerRecord(vw_next_View);

        vw_cwf_Wp_Work_Prcss_Id = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Work_Prcss_Id", "CWF-WP-WORK-PRCSS-ID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Id = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");

        cwf_Wp_Work_Prcss_Id__R_Field_3 = vw_cwf_Wp_Work_Prcss_Id.getRecord().newGroupInGroup("cwf_Wp_Work_Prcss_Id__R_Field_3", "REDEFINE", cwf_Wp_Work_Prcss_Id_Work_Prcss_Id);
        cwf_Wp_Work_Prcss_Id_Work_Actn_Rqstd_Cde = cwf_Wp_Work_Prcss_Id__R_Field_3.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Wp_Work_Prcss_Id_Work_Lob_Cmpny_Prdct_Cde = cwf_Wp_Work_Prcss_Id__R_Field_3.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Lob_Cmpny_Prdct_Cde", 
            "WORK-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        cwf_Wp_Work_Prcss_Id_Work_Mjr_Bsnss_Prcss_Cde = cwf_Wp_Work_Prcss_Id__R_Field_3.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Mjr_Bsnss_Prcss_Cde", 
            "WORK-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        cwf_Wp_Work_Prcss_Id_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Wp_Work_Prcss_Id__R_Field_3.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", 
            FieldType.STRING, 45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        cwf_Wp_Work_Prcss_Id_Cntrct_Slctn_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Cntrct_Slctn_Ind", "CNTRCT-SLCTN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SLCTN_IND");
        cwf_Wp_Work_Prcss_Id_Cntrct_Slctn_Ind.setDdmHeader("CONTRACT/SELECTION");
        cwf_Wp_Work_Prcss_Id_Mj_Pull_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Wp_Work_Prcss_Id_Mj_Pull_Ind.setDdmHeader("MJ/IND");
        cwf_Wp_Work_Prcss_Id_Crprte_Prty_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Prty_Ind", "CRPRTE-PRTY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_PRTY_IND");
        cwf_Wp_Work_Prcss_Id_Crprte_Prty_Ind.setDdmHeader("CORP/PRTY");
        cwf_Wp_Work_Prcss_Id_Check_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Check_Ind", "CHECK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CHECK_IND");
        cwf_Wp_Work_Prcss_Id_Check_Ind.setDdmHeader("CHECK/IND");
        cwf_Wp_Work_Prcss_Id_Crc_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crc_Ind", "CRC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRC_IND");
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp", 
            "CRPRTE-SRVCE-TIME-STNDRD-GRP", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "CRPRTE_SRVCE_TIME_STNDRD_GRP");
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp.setDdmHeader("CORPORATE SERVICE/TIME STANDARD");

        cwf_Wp_Work_Prcss_Id__R_Field_4 = vw_cwf_Wp_Work_Prcss_Id.getRecord().newGroupInGroup("cwf_Wp_Work_Prcss_Id__R_Field_4", "REDEFINE", cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp);
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Days_Nbr = cwf_Wp_Work_Prcss_Id__R_Field_4.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Days_Nbr", 
            "CRPRTE-SRVCE-STNDRD-DAYS-NBR", FieldType.NUMERIC, 3);
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Hours_Nbr = cwf_Wp_Work_Prcss_Id__R_Field_4.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Hours_Nbr", 
            "CRPRTE-SRVCE-STNDRD-HOURS-NBR", FieldType.NUMERIC, 2);
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Mins_Nbr = cwf_Wp_Work_Prcss_Id__R_Field_4.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Mins_Nbr", 
            "CRPRTE-SRVCE-STNDRD-MINS-NBR", FieldType.NUMERIC, 2);
        cwf_Wp_Work_Prcss_Id_Actve_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");
        cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme", "ENTRY-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        cwf_Wp_Work_Prcss_Id_Entry_Oprtr_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");
        cwf_Wp_Work_Prcss_Id_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Wp_Work_Prcss_Id_Updte_Dte = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Updte_Dte", "UPDTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "UPDTE_DTE");
        cwf_Wp_Work_Prcss_Id_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme", "UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UPDTE_DTE_TME");
        cwf_Wp_Work_Prcss_Id_Updte_Oprtr_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        cwf_Wp_Work_Prcss_Id_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPER");
        cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme.setDdmHeader("DELETE/DT-TM");
        cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde", "OWNER-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "OWNER_UNIT_CDE");
        cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde.setDdmHeader("OWNER/UNIT");
        cwf_Wp_Work_Prcss_Id_Sec_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Sec_Ind", "SEC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SEC_IND");
        cwf_Wp_Work_Prcss_Id_Sec_Ind.setDdmHeader("SEC/IND");
        cwf_Wp_Work_Prcss_Id_Effctve_Dte_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Effctve_Dte_Ind", "EFFCTVE-DTE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "EFFCTVE_DTE_IND");
        cwf_Wp_Work_Prcss_Id_Effctve_Dte_Ind.setDdmHeader("EFFCTVE/DTE-IND");
        cwf_Wp_Work_Prcss_Id_Trnsctn_Dte_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Trnsctn_Dte_Ind", "TRNSCTN-DTE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TRNSCTN_DTE_IND");
        cwf_Wp_Work_Prcss_Id_Trnsctn_Dte_Ind.setDdmHeader("TRNSCTN/DTE-IND");
        cwf_Wp_Work_Prcss_Id_Check_Mail_Dte_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Check_Mail_Dte_Ind", "CHECK-MAIL-DTE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CHECK_MAIL_DTE_IND");
        cwf_Wp_Work_Prcss_Id_Check_Mail_Dte_Ind.setDdmHeader("CHK-MAIL/DTE-IND");
        cwf_Wp_Work_Prcss_Id_Back_End_Scan_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Back_End_Scan_Ind", "BACK-END-SCAN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BACK_END_SCAN_IND");
        cwf_Wp_Work_Prcss_Id_Back_End_Scan_Ind.setDdmHeader("BACK-END/SCAN-IND");
        cwf_Wp_Work_Prcss_Id_Dgtze_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dgtze_Ind", "DGTZE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DGTZE_IND");
        cwf_Wp_Work_Prcss_Id_Dgtze_Ind.setDdmHeader("DIGITIZE/INDICATOR");
        cwf_Wp_Work_Prcss_Id_Dgtze_Region_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dgtze_Region_Ind", "DGTZE-REGION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DGTZE_REGION_IND");
        cwf_Wp_Work_Prcss_Id_Dgtze_Region_Ind.setDdmHeader("DIGITIZE REGION/INDICATOR");
        cwf_Wp_Work_Prcss_Id_Dgtze_Branch_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dgtze_Branch_Ind", "DGTZE-BRANCH-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DGTZE_BRANCH_IND");
        cwf_Wp_Work_Prcss_Id_Dgtze_Branch_Ind.setDdmHeader("DIGITIZE BRANCH/INDICATOR");
        cwf_Wp_Work_Prcss_Id_Dgtze_Special_Needs_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dgtze_Special_Needs_Ind", 
            "DGTZE-SPECIAL-NEEDS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "DGTZE_SPECIAL_NEEDS_IND");
        cwf_Wp_Work_Prcss_Id_Dgtze_Special_Needs_Ind.setDdmHeader("DIGITIZE SPECIAL/NEEDS INDICATOR");
        cwf_Wp_Work_Prcss_Id_Shrd_Srvce_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Shrd_Srvce_Ind", "SHRD-SRVCE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SHRD_SRVCE_IND");
        cwf_Wp_Work_Prcss_Id_Shrd_Srvce_Ind.setDdmHeader("SHARED/SERVICE");
        cwf_Wp_Work_Prcss_Id_Da_Sra_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Da_Sra_Ind", "DA-SRA-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DA_SRA_IND");
        cwf_Wp_Work_Prcss_Id_Da_Sra_Ind.setDdmHeader("DA/SRA/IND");
        cwf_Wp_Work_Prcss_Id_Wpid_Usage_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Wpid_Usage_Ind", "WPID-USAGE-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "WPID_USAGE_IND");
        cwf_Wp_Work_Prcss_Id_Wpid_Usage_Ind.setDdmHeader("WPID/USAGE");
        cwf_Wp_Work_Prcss_Id_Insrnce_Prcss_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Insrnce_Prcss_Ind", "INSRNCE-PRCSS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "INSRNCE_PRCSS_IND");
        cwf_Wp_Work_Prcss_Id_Insrnce_Prcss_Ind.setDdmHeader("INSURANCE/PROCESS");
        cwf_Wp_Work_Prcss_Id_Function_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Function_Cde", "FUNCTION-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "FUNCTION_CDE");
        cwf_Wp_Work_Prcss_Id_Function_Cde.setDdmHeader("FUNCTION/CODE");
        cwf_Wp_Work_Prcss_Id_Wpid_Suppress_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Wpid_Suppress_Ind", "WPID-SUPPRESS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WPID_SUPPRESS_IND");
        cwf_Wp_Work_Prcss_Id_Wpid_Suppress_Ind.setDdmHeader("SUPPRESS/IND");
        cwf_Wp_Work_Prcss_Id_Wpid_Intrnet_Nme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Wpid_Intrnet_Nme", "WPID-INTRNET-NME", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "WPID_INTRNET_NME");
        cwf_Wp_Work_Prcss_Id_Wpid_Intrnet_Nme.setDdmHeader("INTER/INTRANET WPID NAME");
        cwf_Wp_Work_Prcss_Id_Team_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Team_Ind", "TEAM-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TEAM_IND");
        cwf_Wp_Work_Prcss_Id_Wpid_Inds = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Wpid_Inds", "WPID-INDS", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "WPID_INDS");
        registerRecord(vw_cwf_Wp_Work_Prcss_Id);

        vw_cwf_Wp_Routing = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Routing", "CWF-WP-ROUTING"), "CWF_WP_ROUTING", "CWF_PROFILE");
        cwf_Wp_Routing_Rt_Sqnce_Prcss_Id = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Prcss_Id", "RT-SQNCE-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "RT_SQNCE_PRCSS_ID");

        cwf_Wp_Routing__R_Field_5 = vw_cwf_Wp_Routing.getRecord().newGroupInGroup("cwf_Wp_Routing__R_Field_5", "REDEFINE", cwf_Wp_Routing_Rt_Sqnce_Prcss_Id);
        cwf_Wp_Routing_Rt_Sqnce_Actn_Rqstd_Cde = cwf_Wp_Routing__R_Field_5.newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Actn_Rqstd_Cde", "RT-SQNCE-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Wp_Routing_Rt_Sqnce_Lob_Cmpny_Prdct_Cde = cwf_Wp_Routing__R_Field_5.newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Lob_Cmpny_Prdct_Cde", "RT-SQNCE-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        cwf_Wp_Routing_Rt_Sqnce_Mjr_Bsnss_Prcss_Cde = cwf_Wp_Routing__R_Field_5.newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Mjr_Bsnss_Prcss_Cde", "RT-SQNCE-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        cwf_Wp_Routing_Rt_Sqnce_Spcfc_Bsnss_Prcss_Cde = cwf_Wp_Routing__R_Field_5.newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Spcfc_Bsnss_Prcss_Cde", "RT-SQNCE-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        cwf_Wp_Routing_Rt_Sqnce_Nbr = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        cwf_Wp_Routing_Rt_Sqnce_Nbr.setDdmHeader("ROUTING/SEQUENCE");
        cwf_Wp_Routing_Unit_Cde = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Wp_Routing_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Wp_Routing__R_Field_6 = vw_cwf_Wp_Routing.getRecord().newGroupInGroup("cwf_Wp_Routing__R_Field_6", "REDEFINE", cwf_Wp_Routing_Unit_Cde);
        cwf_Wp_Routing_Unit_Id_Cde = cwf_Wp_Routing__R_Field_6.newFieldInGroup("cwf_Wp_Routing_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        cwf_Wp_Routing_Unit_Rgn_Cde = cwf_Wp_Routing__R_Field_6.newFieldInGroup("cwf_Wp_Routing_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 1);
        cwf_Wp_Routing_Unit_Spcl_Dsgntn_Cde = cwf_Wp_Routing__R_Field_6.newFieldInGroup("cwf_Wp_Routing_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Wp_Routing_Unit_Brnch_Group_Cde = cwf_Wp_Routing__R_Field_6.newFieldInGroup("cwf_Wp_Routing_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp", "CRPRTE-SRVCE-TIME-STNDRD-GRP", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "CRPRTE_SRVCE_TIME_STNDRD_GRP");
        cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp.setDdmHeader("CORPORATE SERVICE/TIME STANDARD");

        cwf_Wp_Routing__R_Field_7 = vw_cwf_Wp_Routing.getRecord().newGroupInGroup("cwf_Wp_Routing__R_Field_7", "REDEFINE", cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp);
        cwf_Wp_Routing_Crprte_Srvce_Stndrd_Days_Nbr = cwf_Wp_Routing__R_Field_7.newFieldInGroup("cwf_Wp_Routing_Crprte_Srvce_Stndrd_Days_Nbr", "CRPRTE-SRVCE-STNDRD-DAYS-NBR", 
            FieldType.NUMERIC, 3);
        cwf_Wp_Routing_Crprte_Srvce_Stndrd_Hours_Nbr = cwf_Wp_Routing__R_Field_7.newFieldInGroup("cwf_Wp_Routing_Crprte_Srvce_Stndrd_Hours_Nbr", "CRPRTE-SRVCE-STNDRD-HOURS-NBR", 
            FieldType.NUMERIC, 2);
        cwf_Wp_Routing_Crprte_Srvce_Stndrd_Mins_Nbr = cwf_Wp_Routing__R_Field_7.newFieldInGroup("cwf_Wp_Routing_Crprte_Srvce_Stndrd_Mins_Nbr", "CRPRTE-SRVCE-STNDRD-MINS-NBR", 
            FieldType.NUMERIC, 2);
        cwf_Wp_Routing_Actve_Ind = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");

        cwf_Wp_Routing__R_Field_8 = vw_cwf_Wp_Routing.getRecord().newGroupInGroup("cwf_Wp_Routing__R_Field_8", "REDEFINE", cwf_Wp_Routing_Actve_Ind);
        cwf_Wp_Routing_Fill_1 = cwf_Wp_Routing__R_Field_8.newFieldInGroup("cwf_Wp_Routing_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Wp_Routing_Actve_Ind_2_2 = cwf_Wp_Routing__R_Field_8.newFieldInGroup("cwf_Wp_Routing_Actve_Ind_2_2", "ACTVE-IND-2-2", FieldType.STRING, 1);
        cwf_Wp_Routing_Entry_Dte_Tme = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Wp_Routing_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        cwf_Wp_Routing_Entry_Oprtr_Cde = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");
        cwf_Wp_Routing_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Wp_Routing_Updte_Dte = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Updte_Dte", "UPDTE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "UPDTE_DTE");
        cwf_Wp_Routing_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Wp_Routing_Updte_Dte_Tme = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPDTE_DTE_TME");
        cwf_Wp_Routing_Updte_Oprtr_Cde = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        cwf_Wp_Routing_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPER");
        cwf_Wp_Routing_Dlte_Dte_Tme = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "DLTE_DTE_TME");
        cwf_Wp_Routing_Dlte_Dte_Tme.setDdmHeader("DELETE/DT-TM");
        cwf_Wp_Routing_Dlte_Oprtr_Cde = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Wp_Routing_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_next_View.reset();
        vw_cwf_Wp_Work_Prcss_Id.reset();
        vw_cwf_Wp_Routing.reset();

        localVariables.reset();
        pnd_Max_Pnd_Cwf_Wp_Routing.setInitialValue(20);
        pnd_Min_Pnd_Cwf_Wp_Routing.setInitialValue(1);
        pnd_Object.setInitialValue("Work Process ID");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfx1400() throws Exception
    {
        super("Cwfx1400");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  INITIALIZE OUTPUT FIELDS.
        //*  CJS
        pdaCdaobj.getCdaobj_Outputs().reset();                                                                                                                            //Natural: RESET CDAOBJ.OUTPUTS MSG-INFO-SUB
        pdaCwfpda_M.getMsg_Info_Sub().reset();
        //*  BUILD UP PRIME KEY OF THE OBJECT.
        pdaCwfa1400.getCwfa1400_Id_Structure().setValuesByName(pdaCwfa1400.getCwfa1400());                                                                                //Natural: MOVE BY NAME CWFA1400 TO CWFA1400-ID.STRUCTURE
        //*  SET UP REPEAT TO ALLOW ESCAPE FROM WITHIN A SUBROUTINE.
        PROG:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  DECIDE WHAT TO DO BASED ON FUNCTION SUPPLIED.
            //*  READ THE OBJECT
            short decideConditionsMet690 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE CDAOBJ.#FUNCTION;//Natural: VALUE 'GET'
            if (condition((pdaCdaobj.getCdaobj_Pnd_Function().equals("GET"))))
            {
                decideConditionsMet690++;
                                                                                                                                                                          //Natural: PERFORM GET-OBJECT
                sub_Get_Object();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SET-OBJECT-ID
                sub_Set_Object_Id();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  READ THE NEXT HIGHER OBJECT
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:displayed successfully");                                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:displayed successfully'
            }                                                                                                                                                             //Natural: VALUE 'NEXT'
            else if (condition((pdaCdaobj.getCdaobj_Pnd_Function().equals("NEXT"))))
            {
                decideConditionsMet690++;
                vw_next_View.createHistogram                                                                                                                              //Natural: HISTOGRAM ( 2 ) NEXT-VIEW FOR WPID-UNIQ-KEY STARTING FROM CWFA1400-ID
                (
                "HIST01",
                "WPID_UNIQ_KEY",
                new Wc[] { new Wc("WPID_UNIQ_KEY", ">=", pdaCwfa1400.getCwfa1400_Id(), WcType.WITH) },
                2
                );
                HIST01:
                while (condition(vw_next_View.readNextRow("HIST01")))
                {
                    if (condition(next_View_Wpid_Uniq_Key.notEquals(pdaCwfa1400.getCwfa1400_Id())))                                                                       //Natural: IF NEXT-VIEW.WPID-UNIQ-KEY NE CWFA1400-ID THEN
                    {
                        pdaCwfa1400.getCwfa1400_Id().setValue(next_View_Wpid_Uniq_Key);                                                                                   //Natural: ASSIGN CWFA1400-ID = NEXT-VIEW.WPID-UNIQ-KEY
                        pdaCwfa1400.getCwfa1400().setValuesByName(pdaCwfa1400.getCwfa1400_Id_Structure());                                                                //Natural: MOVE BY NAME CWFA1400-ID.STRUCTURE TO CWFA1400
                                                                                                                                                                          //Natural: PERFORM GET-OBJECT
                        sub_Get_Object();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(pnd_Object);                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = #OBJECT
                        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Next:1:displayed");                                                                           //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'Next:1:displayed'
                        if (true) break PROG;                                                                                                                             //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-HISTOGRAM
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  NO RECORDS BEYOND CURRENT RECORD.
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("End of data reached");                                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'End of data reached'
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("WPID-UNIQ-KEY");                                                                              //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'WPID-UNIQ-KEY'
                //*  WARNING.
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("W");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE = 'W'
                //*  MODIFY OR PURGE CURRENT OBJECT
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE IMMEDIATE
            }                                                                                                                                                             //Natural: VALUE 'UPDATE','DELETE'
            else if (condition((pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") || pdaCdaobj.getCdaobj_Pnd_Function().equals("DELETE"))))
            {
                decideConditionsMet690++;
                //*  MAKE SURE THE RECORD WAS HELD PRIOR TO UPDATING.
                if (condition(pdaCwfa1400.getCwfa1400_Id().notEquals(pdaCwfa1401.getCwfa1401_Held_Id())))                                                                 //Natural: IF CWFA1400-ID NE CWFA1401.HELD-ID THEN
                {
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Attempted to update/delete:1:that was not in held status");                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'Attempted to update/delete:1:that was not in held status'
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("#FUNCTION");                                                                              //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = '#FUNCTION'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                    sub_Process_Error();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM HOLD-OBJECT
                sub_Hold_Object();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SET-OBJECT-ID
                sub_Set_Object_Id();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE")))                                                                                       //Natural: IF CDAOBJ.#FUNCTION = 'UPDATE'
                {
                                                                                                                                                                          //Natural: PERFORM CLEAR
                    sub_Clear();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(! (pdaCdaobj.getCdaobj_Pnd_Clear_After_Update().getBoolean())))                                                                         //Natural: IF NOT CDAOBJ.#CLEAR-AFTER-UPDATE THEN
                    {
                        //*  GET NEW COPY OF OBJECT IF CLEAR NOT REQ.
                                                                                                                                                                          //Natural: PERFORM GET-OBJECT
                        sub_Get_Object();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PROG"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:modified successfully");                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:modified successfully'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaCdaobj.getCdaobj_Pnd_Clear_After_Update().getBoolean()))                                                                             //Natural: IF CDAOBJ.#CLEAR-AFTER-UPDATE THEN
                    {
                        //*  CLEAR SCREEN AFTER PURGE IF REQUESTED.
                                                                                                                                                                          //Natural: PERFORM CLEAR
                        sub_Clear();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PROG"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:purged successfully");                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:purged successfully'
                    //*  EXISTENCE CHECK
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'EXISTS'
            else if (condition((pdaCdaobj.getCdaobj_Pnd_Function().equals("EXISTS"))))
            {
                decideConditionsMet690++;
                                                                                                                                                                          //Natural: PERFORM CHECK-EXISTENCE
                sub_Check_Existence();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ADD NEW OBJECT
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE IMMEDIATE
            }                                                                                                                                                             //Natural: VALUE 'STORE'
            else if (condition((pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE"))))
            {
                decideConditionsMet690++;
                                                                                                                                                                          //Natural: PERFORM CREATE-OBJECT
                sub_Create_Object();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SET-OBJECT-ID
                sub_Set_Object_Id();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CLEAR
                sub_Clear();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(! (pdaCdaobj.getCdaobj_Pnd_Clear_After_Update().getBoolean())))                                                                             //Natural: IF NOT CDAOBJ.#CLEAR-AFTER-UPDATE THEN
                {
                    //*  GET NEW COPY OF OBJECT IF CLEAR NOT REQ.
                                                                                                                                                                          //Natural: PERFORM GET-OBJECT
                    sub_Get_Object();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:added successfully");                                                                              //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:added successfully'
            }                                                                                                                                                             //Natural: VALUE 'INITIALIZE'
            else if (condition((pdaCdaobj.getCdaobj_Pnd_Function().equals("INITIALIZE"))))
            {
                decideConditionsMet690++;
                                                                                                                                                                          //Natural: PERFORM CLEAR
                sub_Clear();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet690 > 0))
            {
                //*  ALL DONE.
                //*  UNKNOWN ACTION CODE
                if (true) break PROG;                                                                                                                                     //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(pnd_Object);                                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = #OBJECT
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2).setValue(pdaCdaobj.getCdaobj_Pnd_Function());                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 2 ) = CDAOBJ.#FUNCTION
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Invalid:1:function specified::2:");                                                                   //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'Invalid:1:function specified::2:'
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("#FUNCTION");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = '#FUNCTION'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            //* ***********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: G-CWF-WP-ROUTING
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HOLD-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-AND-UPDATE-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EDIT-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C-CWF-WP-ROUTING
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: E-CWF-WP-ROUTING
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-EXISTENCE
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CLEAR
        //* *SAG DEFINE EXIT UPDATE-EDITS
        //*  SPECIFY SUBROUTINES OF THE FORM V0-ENTITY-NAME (OR V1- OR V2-) USED
        //*  TO VALIDATE OR MANIPULATE DATA BEFORE AN UPDATE/STORE OPERATION WHICH
        //*  MOVE DATA FROM THE OBJECT TO THE ENTITY-NAME RECORD. DATA MANIPULATION
        //*  MUST BE MADE AGAINST THE RECORD. IF ANY REFERENCE TO THE OBJECT IS
        //*  REQUIRED BECAUSE THE RELEVANT FIELDS HAVE DIFFERENT NAMES ON THE
        //*  OBJECT AND THE FILE, APPROPRIATE INDICES (#L2),(#L2,#L3),(#L2,#L3,#L4)
        //*  SHOULD BE USED. THE V2- ROUTINE IS PERFORMED FOR AN ENTITY AFTER ALL
        //*  OF ITS CHILDREN's processing while the V0-/V1- are invoked before
        //*  ITS CHILDREN's, and before/after its PREDICT rules respectively.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: V0-CWF-WP-WORK-PRCSS-ID
        //*  WHEN CWF-WP-WORK-PRCSS-ID.CNTRCT-SLCTN-IND EQ ' '
        //*    ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
        //*    ASSIGN MSG-INFO-SUB.##MSG-DATA(1) = 'Contract Selection'
        //*    ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'CNTRCT-SLCTN-IND'
        //*  WHEN CWF-WP-WORK-PRCSS-ID.MJ-PULL-IND EQ ' '
        //*    ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
        //*    ASSIGN MSG-INFO-SUB.##MSG-DATA(1) = 'MJ Ind'
        //*    ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'MJ-PULL-IND'
        //*  WHEN CWF-WP-WORK-PRCSS-ID.CRPRTE-PRTY-IND EQ ' '
        //*    ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
        //*    ASSIGN MSG-INFO-SUB.##MSG-DATA(1) = 'Corp Prty'
        //*    ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'CRPRTE-PRTY-IND'
        //*  WHEN CWF-WP-WORK-PRCSS-ID.CHECK-IND EQ ' '
        //*    ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
        //*    ASSIGN MSG-INFO-SUB.##MSG-DATA(1) = 'Check Ind'
        //*    ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'CHECK-IND'
        //*  WHEN CWF-WP-WORK-PRCSS-ID.CRPRTE-SRVCE-TIME-STNDRD-GRP EQ 0
        //*    ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
        //*    ASSIGN MSG-INFO-SUB.##MSG-DATA(1) = 'Corporate Service Time Standard'
        //*    ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'CRPRTE-SRVCE-TIME-STNDRD-GRP'
        //*  WHEN CWF-WP-WORK-PRCSS-ID.CRPRTE-SRVCE-STNDRD-DAYS-NBR EQ 0
        //*    ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
        //*    ASSIGN MSG-INFO-SUB.##MSG-DATA(1) = 'Day(s)'
        //*    ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'CRPRTE-SRVCE-STNDRD-DAYS-NBR'
        //*  WHEN CWF-WP-WORK-PRCSS-ID.CRPRTE-SRVCE-STNDRD-HOURS-NBR EQ 0
        //*    ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
        //*    ASSIGN MSG-INFO-SUB.##MSG-DATA(1) = 'Hrs'
        //*    ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'CRPRTE-SRVCE-STNDRD-HOURS-NBR'
        //*  WHEN CWF-WP-WORK-PRCSS-ID.CRPRTE-SRVCE-STNDRD-MINS-NBR EQ 0
        //*    ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
        //*    ASSIGN MSG-INFO-SUB.##MSG-DATA(1) = 'Minutes'
        //*    ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'CRPRTE-SRVCE-STNDRD-MINS-NBR'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: V1-CWF-WP-WORK-PRCSS-ID
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: V2-CWF-WP-WORK-PRCSS-ID
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: V0-CWF-WP-ROUTING
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: V1-CWF-WP-ROUTING
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT AFTER-GET-EDITS
        //*  SPECIFY SUBROUTINES OF THE FORM A-ENTITY-NAME USED TO DO ANY REQUIRED
        //*  DATA CONVERSION OR COMPUTATION AFTER A GET OPERATION WHICH RETURNS
        //*  DATA FROM THE ENTITY-NAME RECORD TO THE OBJECT PDA. DATA MANIPULATION
        //*  SHOULD BE MADE AGAINST THE RECORD. IF ANY MANIPULATION DIRECTLY TO THE
        //*  OBJECT IS REQUIRED BECAUSE THE RELEVANT FIELDS HAVE DIFFERENT NAMES ON
        //*  THE OBJECT AND THE FILE, APPROPRIATE INDICES (#L2), (#L2,#L3) OR
        //*  (#L2,#L3,#L4) SHOULD BE USED.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A-CWF-WP-WORK-PRCSS-ID
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A-CWF-WP-ROUTING
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT BUILD-ENTITY-KEYS
        //*  USE THIS EXIT TO BUILD THE ENTITY KEYS WHEN THEY ARE COMPRISED OF
        //*  NON-OBJECT FIELDS, OR ARE UN-NORMALIZED. WHERE MORE THAN 1 RECORD IS
        //*  EXPECTED, BUILD THE KEY PREFIX AS ABOVE, THEN FILL THE REMAINING
        //*  FIELDS WITH H'00' TO CREATE #FROM.KEYFIELD, AND WITH H'FF' TO CREATE
        //*  #THRU.KEYFIELD.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BD-CWF-WP-ROUTING
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT ASSIGN-SUB-ENTITY-ID
        //*  USE THIS EXIT TO OVERRIDE THE ASSIGNMENT OF RELATED-KEY = PDA-ID IN
        //*  CHECK AND UPDATE ENTITY SUBROUTINE.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AS-CWF-WP-ROUTING
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT SUB-ENTITY-LINE-NUMBERING
        //*  USE THIS EXIT TO OVERRIDE THE LINE NUMBER ELEMENT BEING INCREMENTED IN
        //*  THE CHECK AND UPDATE ENTITY SUBROUTINE.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LN-CWF-WP-ROUTING
        //* *SAG END-EXIT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DB-CALL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-OBJECT-ID
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  SET DEFAULT FOR :1:.
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                            //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 1 ) = ' ' THEN
        {
                                                                                                                                                                          //Natural: PERFORM SET-OBJECT-ID
            sub_Set_Object_Id();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET DEFAULT ERROR FIELD.
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().equals(" ")))                                                                                     //Natural: IF MSG-INFO-SUB.##ERROR-FIELD = ' ' THEN
        {
            if (condition(pnd_Current_Field.notEquals(" ")))                                                                                                              //Natural: IF #CURRENT-FIELD NE ' '
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue(pnd_Current_Field);                                                                            //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = #CURRENT-FIELD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("WPID-UNIQ-KEY");                                                                              //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'WPID-UNIQ-KEY'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ERROR.
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE = 'E'
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
        Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
        if (true) return;
        //*  PROCESS-ERROR
    }
    private void sub_Create_Object() throws Exception                                                                                                                     //Natural: CREATE-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  DON't allow the key to be null.
        //*  COMPARE WITH NULL
        if (condition(pdaCwfa1400.getCwfa1400_Id().equals(next_View_Wpid_Uniq_Key)))                                                                                      //Natural: IF CWFA1400-ID = NEXT-VIEW.WPID-UNIQ-KEY
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(pnd_Object);                                                                              //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = #OBJECT
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:identifier is required");                                                                              //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:identifier is required'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF OBJECT ALREADY EXIST, RETURN WITH ERROR.
                                                                                                                                                                          //Natural: PERFORM CHECK-EXISTENCE
        sub_Check_Existence();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaCdaobj.getCdaobj_Pnd_Exists().getBoolean()))                                                                                                     //Natural: IF CDAOBJ.#EXISTS THEN
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:already exists");                                                                                      //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:already exists'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  PRE-EDIT OBJECT HEADER
                                                                                                                                                                          //Natural: PERFORM EDIT-OBJECT
        sub_Edit_Object();
        if (condition(Global.isEscape())) {return;}
        cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                    //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.UPDTE-DTE-TME = *TIMX
        //*  CHECK AND UPDATE CHILDREN
                                                                                                                                                                          //Natural: PERFORM CHECK-AND-UPDATE-OBJECT
        sub_Check_And_Update_Object();
        if (condition(Global.isEscape())) {return;}
        vw_cwf_Wp_Work_Prcss_Id.insertDBRow();                                                                                                                            //Natural: STORE CWF-WP-WORK-PRCSS-ID
        //*  SINCE OBJECT ID DOES NOT HAVE UNIQUE KEY ON DATABASE, MAKE SURE
        //*  THAT TWO USERS DID NOT STORE OBJECT AT THE SAME TIME.
                                                                                                                                                                          //Natural: PERFORM CHECK-EXISTENCE
        sub_Check_Existence();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-OBJECT
    }
    private void sub_Get_Object() throws Exception                                                                                                                        //Natural: GET-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  GET THE PRIMARY OBJECT RECORD.
        vw_cwf_Wp_Work_Prcss_Id.startDatabaseFind                                                                                                                         //Natural: FIND CWF-WP-WORK-PRCSS-ID WITH WPID-UNIQ-KEY = CWFA1400-ID
        (
        "FIND01",
        new Wc[] { new Wc("WPID_UNIQ_KEY", "=", pdaCwfa1400.getCwfa1400_Id(), WcType.WITH) }
        );
        FIND01:
        while (condition(vw_cwf_Wp_Work_Prcss_Id.readNextRow("FIND01", true)))
        {
            vw_cwf_Wp_Work_Prcss_Id.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Wp_Work_Prcss_Id.getAstCOUNTER().equals(0)))                                                                                             //Natural: IF NO RECORDS FOUND
            {
                //*  RETURN THE FACT THAT THE OBJECT DOES NOT EXIST.
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:does not exist");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:does not exist'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
                                                                                                                                                                          //Natural: PERFORM CLEAR
            sub_Clear();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaCdaobj.getCdaobj_Pnd_Exists().setValue(true);                                                                                                              //Natural: ASSIGN CDAOBJ.#EXISTS = TRUE
            //*  "Hold" THE RECORD BY CAPTURING THE VALUE OF THE UPDATE FLAG.
            pdaCwfa1401.getCwfa1401_Intervening_Upd_Fld().setValue(cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme);                                                                   //Natural: ASSIGN CWFA1401.INTERVENING-UPD-FLD = CWF-WP-WORK-PRCSS-ID.UPDTE-DTE-TME
                                                                                                                                                                          //Natural: PERFORM A-CWF-WP-WORK-PRCSS-ID
            sub_A_Cwf_Wp_Work_Prcss_Id();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  SHIFT PRIMARY ENTITY INFORMATION TO OBJECT
            pdaCwfa1400.getCwfa1400().setValuesByName(vw_cwf_Wp_Work_Prcss_Id);                                                                                           //Natural: MOVE BY NAME CWF-WP-WORK-PRCSS-ID TO CWFA1400
                                                                                                                                                                          //Natural: PERFORM G-CWF-WP-ROUTING
            sub_G_Cwf_Wp_Routing();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaCwfa1401.getCwfa1401_Held_Id().setValue(pdaCwfa1400.getCwfa1400_Id());                                                                                     //Natural: ASSIGN CWFA1401.HELD-ID = CWFA1400-ID
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT AFTER-GET
        pdaCwfa1400.getCwfa1400_Entry_Dte_Tmex().setValue(cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme);                                                                            //Natural: MOVE CWF-WP-WORK-PRCSS-ID.ENTRY-DTE-TME TO CWFA1400.ENTRY-DTE-TMEX
        pdaCwfa1400.getCwfa1400_Entry_Oprtr_Cdex().setValue(cwf_Wp_Work_Prcss_Id_Entry_Oprtr_Cde);                                                                        //Natural: MOVE CWF-WP-WORK-PRCSS-ID.ENTRY-OPRTR-CDE TO CWFA1400.ENTRY-OPRTR-CDEX
        pdaCwfa1400.getCwfa1400_Updte_Dtex().setValue(cwf_Wp_Work_Prcss_Id_Updte_Dte);                                                                                    //Natural: MOVE CWF-WP-WORK-PRCSS-ID.UPDTE-DTE TO CWFA1400.UPDTE-DTEX
        pdaCwfa1400.getCwfa1400_Updte_Dte_Tmex().setValue(cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme);                                                                            //Natural: MOVE CWF-WP-WORK-PRCSS-ID.UPDTE-DTE-TME TO CWFA1400.UPDTE-DTE-TMEX
        pdaCwfa1400.getCwfa1400_Updte_Oprtr_Cdex().setValue(cwf_Wp_Work_Prcss_Id_Updte_Oprtr_Cde);                                                                        //Natural: MOVE CWF-WP-WORK-PRCSS-ID.UPDTE-OPRTR-CDE TO CWFA1400.UPDTE-OPRTR-CDEX
        pdaCwfa1400.getCwfa1400_Dlte_Dte_Tmex().setValue(cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme);                                                                              //Natural: MOVE CWF-WP-WORK-PRCSS-ID.DLTE-DTE-TME TO CWFA1400.DLTE-DTE-TMEX
        pdaCwfa1400.getCwfa1400_Dlte_Oprtr_Cdex().setValue(cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde);                                                                          //Natural: MOVE CWF-WP-WORK-PRCSS-ID.DLTE-OPRTR-CDE TO CWFA1400.DLTE-OPRTR-CDEX
        //*  INSERT CODE TO BE PROCESSED AFTER SUCCESSFULLY RETRIEVING ALL OBJECT
        //*  COMPONENTS FROM THE DATA BASE.
        //* *SAG END-EXIT
        //*  GET-OBJECT
    }
    private void sub_G_Cwf_Wp_Routing() throws Exception                                                                                                                  //Natural: G-CWF-WP-ROUTING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  GET CWF-WP-ROUTING RELATED TO CWF-WP-WORK-PRCSS-ID
        //*  FIRST SET UP MIN/MAX VALUES FOR ROUTING-SQNCE-KEY
        pnd_Cwf_Wp_Routing_Rel_Related_Key.setValue(pdaCwfa1400.getCwfa1400_Id());                                                                                        //Natural: ASSIGN #CWF-WP-ROUTING-REL.RELATED-KEY = CWFA1400-ID
        pnd_Cwf_Wp_Routing_Rel_Min_Max.moveAll("H'00'");                                                                                                                  //Natural: MOVE ALL H'00' TO #CWF-WP-ROUTING-REL.MIN-MAX
        pnd_From_Cwf_Wp_Routing_Rel.setValue(pnd_Cwf_Wp_Routing_Rel);                                                                                                     //Natural: ASSIGN #FROM.CWF-WP-ROUTING-REL = #CWF-WP-ROUTING-REL
        pnd_Cwf_Wp_Routing_Rel_Min_Max.moveAll("H'FF'");                                                                                                                  //Natural: MOVE ALL H'FF' TO #CWF-WP-ROUTING-REL.MIN-MAX
        pnd_Thru_Cwf_Wp_Routing_Rel.setValue(pnd_Cwf_Wp_Routing_Rel);                                                                                                     //Natural: ASSIGN #THRU.CWF-WP-ROUTING-REL = #CWF-WP-ROUTING-REL
        pnd_L2.reset();                                                                                                                                                   //Natural: RESET #L2
                                                                                                                                                                          //Natural: PERFORM BD-CWF-WP-ROUTING
        sub_Bd_Cwf_Wp_Routing();
        if (condition(Global.isEscape())) {return;}
        vw_cwf_Wp_Routing.startDatabaseRead                                                                                                                               //Natural: READ CWF-WP-ROUTING BY ROUTING-SQNCE-KEY FROM #FROM.CWF-WP-ROUTING-REL
        (
        "L_CWF_WP_ROUTING",
        new Wc[] { new Wc("ROUTING_SQNCE_KEY", ">=", pnd_From_Cwf_Wp_Routing_Rel, WcType.BY) },
        new Oc[] { new Oc("ROUTING_SQNCE_KEY", "ASC") }
        );
        L_CWF_WP_ROUTING:
        while (condition(vw_cwf_Wp_Routing.readNextRow("L_CWF_WP_ROUTING")))
        {
            pnd_Cwf_Wp_Routing_Rel_Mbn_Structure.setValuesByName(vw_cwf_Wp_Routing);                                                                                      //Natural: MOVE BY NAME CWF-WP-ROUTING TO #CWF-WP-ROUTING-REL.MBN-STRUCTURE
            if (condition(pnd_Cwf_Wp_Routing_Rel.greater(pnd_Thru_Cwf_Wp_Routing_Rel)))                                                                                   //Natural: IF #CWF-WP-ROUTING-REL GT #THRU.CWF-WP-ROUTING-REL
            {
                if (true) break L_CWF_WP_ROUTING;                                                                                                                         //Natural: ESCAPE BOTTOM ( L-CWF-WP-ROUTING. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_L2.setValue(vw_cwf_Wp_Routing.getAstCOUNTER());                                                                                                           //Natural: ASSIGN #L2 = *COUNTER ( L-CWF-WP-ROUTING. )
            if (condition(pnd_L2.greater(pnd_Max_Pnd_Cwf_Wp_Routing)))                                                                                                    //Natural: IF #L2 GT #MAX.#CWF-WP-ROUTING
            {
                //*  THE MAXIMUM NUMBER OF SCREEN LINES IS EXCEEDED.
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2).setValue(DbsUtil.compress(pnd_Max_Pnd_Cwf_Wp_Routing, "ROUTING-SQNCE-KEY "));                  //Natural: COMPRESS #MAX.#CWF-WP-ROUTING 'ROUTING-SQNCE-KEY ' TO MSG-INFO-SUB.##MSG-DATA ( 2 )
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:contains more than:2:");                                                                           //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:contains more than:2:'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("L_CWF_WP_ROUTING"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("L_CWF_WP_ROUTING"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pdaCwfa1401.getCwfa1401_Pnd_Cwf_Wp_Routing().getValue(pnd_L2).setValue(vw_cwf_Wp_Routing.getAstISN("L_CWF_WP_ROUTING"));                                      //Natural: ASSIGN CWFA1401.#CWF-WP-ROUTING ( #L2 ) = *ISN ( L-CWF-WP-ROUTING. )
                                                                                                                                                                          //Natural: PERFORM A-CWF-WP-ROUTING
            sub_A_Cwf_Wp_Routing();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("L_CWF_WP_ROUTING"))) break;
                else if (condition(Global.isEscapeBottomImmediate("L_CWF_WP_ROUTING"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaCwfa1400.getCwfa1400_Cwf_Wp_Routing_Rel().getValue(pnd_L2).setValuesByName(vw_cwf_Wp_Routing);                                                             //Natural: MOVE BY NAME CWF-WP-ROUTING TO CWFA1400.CWF-WP-ROUTING-REL ( #L2 )
            pdaCwfa1400.getCwfa1400_Rt_Srvce_Time_Stndrd_Grp().getValue(pnd_L2).setValue(cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp);                                    //Natural: MOVE CWF-WP-ROUTING.CRPRTE-SRVCE-TIME-STNDRD-GRP TO CWFA1400.RT-SRVCE-TIME-STNDRD-GRP ( #L2 )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pdaCwfa1400.getCwfa1400_Cpnd_Cwf_Wp_Routing_Rel().setValue(pnd_L2);                                                                                               //Natural: ASSIGN C#CWF-WP-ROUTING-REL = #L2
        //*  G-CWF-WP-ROUTING
    }
    private void sub_Hold_Object() throws Exception                                                                                                                       //Natural: HOLD-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  GET PRIMARY ENTITY AND PROCESS IT.
        vw_cwf_Wp_Work_Prcss_Id.startDatabaseFind                                                                                                                         //Natural: FIND CWF-WP-WORK-PRCSS-ID WITH WPID-UNIQ-KEY = CWFA1400-ID
        (
        "HOLD_PRIME",
        new Wc[] { new Wc("WPID_UNIQ_KEY", "=", pdaCwfa1400.getCwfa1400_Id(), WcType.WITH) }
        );
        HOLD_PRIME:
        while (condition(vw_cwf_Wp_Work_Prcss_Id.readNextRow("HOLD_PRIME", true)))
        {
            vw_cwf_Wp_Work_Prcss_Id.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Wp_Work_Prcss_Id.getAstCOUNTER().equals(0)))                                                                                             //Natural: IF NO RECORDS FOUND
            {
                //*  TRYING TO PROCESS AN OBJECT THAT DOESN't exist.
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Intervening purge, please try again");                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'Intervening purge, please try again'
                pdaCwfa1401.getCwfa1401().reset();                                                                                                                        //Natural: RESET CWFA1401
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("HOLD_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("HOLD_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(pdaCwfa1401.getCwfa1401_Intervening_Upd_Fld().notEquals(cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme)))                                                   //Natural: IF CWFA1401.INTERVENING-UPD-FLD NE CWF-WP-WORK-PRCSS-ID.UPDTE-DTE-TME THEN
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Intervening update, please try again");                                                               //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'Intervening update, please try again'
                pdaCwfa1401.getCwfa1401().reset();                                                                                                                        //Natural: RESET CWFA1401
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("HOLD_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("HOLD_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pdaCdaobj.getCdaobj_Pnd_Exists().setValue(true);                                                                                                              //Natural: ASSIGN CDAOBJ.#EXISTS = TRUE
            //*  PRE-EDIT OBJECT HEADER
                                                                                                                                                                          //Natural: PERFORM EDIT-OBJECT
            sub_Edit_Object();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("HOLD_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("HOLD_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CHECK AND UPDATE CHILDREN
                                                                                                                                                                          //Natural: PERFORM CHECK-AND-UPDATE-OBJECT
            sub_Check_And_Update_Object();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("HOLD_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("HOLD_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            short decideConditionsMet1062 = 0;                                                                                                                            //Natural: DECIDE ON EVERY VALUE CDAOBJ.#FUNCTION;//Natural: VALUE 'UPDATE'
            if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE")))
            {
                decideConditionsMet1062++;
                cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                            //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.UPDTE-DTE-TME = *TIMX
                vw_cwf_Wp_Work_Prcss_Id.updateDBRow("HOLD_PRIME");                                                                                                        //Natural: UPDATE ( HOLD-PRIME. )
            }                                                                                                                                                             //Natural: VALUE 'DELETE'
            if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("DELETE")))
            {
                decideConditionsMet1062++;
                vw_cwf_Wp_Work_Prcss_Id.deleteDBRow("HOLD_PRIME");                                                                                                        //Natural: DELETE ( HOLD-PRIME. )
                pdaCwfa1401.getCwfa1401_Held_Id().reset();                                                                                                                //Natural: RESET CWFA1401.HELD-ID
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet1062 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  HOLD-OBJECT
    }
    //*  PROCESS ALL SUB-ENTITIES
    private void sub_Check_And_Update_Object() throws Exception                                                                                                           //Natural: CHECK-AND-UPDATE-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  VALIDATE AND UPDATE ALL SUB ENTITIES.
                                                                                                                                                                          //Natural: PERFORM C-CWF-WP-ROUTING
        sub_C_Cwf_Wp_Routing();
        if (condition(Global.isEscape())) {return;}
        //*  VALIDATE THE ENTITY AFTER ALL THE CHILDREN HAVE BEEN PROCESSED.
        if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") || pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE")))                                         //Natural: IF CDAOBJ.#FUNCTION = 'UPDATE' OR = 'STORE'
        {
                                                                                                                                                                          //Natural: PERFORM V2-CWF-WP-WORK-PRCSS-ID
            sub_V2_Cwf_Wp_Work_Prcss_Id();
            if (condition(Global.isEscape())) {return;}
            pdaCwfa1400.getCwfa1400().setValuesByName(vw_cwf_Wp_Work_Prcss_Id);                                                                                           //Natural: MOVE BY NAME CWF-WP-WORK-PRCSS-ID TO CWFA1400
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ")))                                                                                      //Natural: IF MSG-INFO-SUB.##MSG NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-AND-UPDATE-OBJECT
    }
    //*  PRE-EDITING OBJECT HEADER
    private void sub_Edit_Object() throws Exception                                                                                                                       //Natural: EDIT-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") || pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE")))                                         //Natural: IF CDAOBJ.#FUNCTION = 'UPDATE' OR = 'STORE'
        {
            //*  BUILD UP THE HEADER RECORD FOR EDITING OR UPDATING
            pdaCwfl1400.getCwfl1400_Struct().setValuesByName(pdaCwfa1400.getCwfa1400());                                                                                  //Natural: MOVE BY NAME CWFA1400 TO CWFL1400.STRUCT
            vw_cwf_Wp_Work_Prcss_Id.setValuesByName(pdaCwfl1400.getCwfl1400());                                                                                           //Natural: MOVE BY NAME CWFL1400 TO CWF-WP-WORK-PRCSS-ID
                                                                                                                                                                          //Natural: PERFORM V0-CWF-WP-WORK-PRCSS-ID
            sub_V0_Cwf_Wp_Work_Prcss_Id();
            if (condition(Global.isEscape())) {return;}
            pdaCwfa1400.getCwfa1400().setValuesByName(vw_cwf_Wp_Work_Prcss_Id);                                                                                           //Natural: MOVE BY NAME CWF-WP-WORK-PRCSS-ID TO CWFA1400
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ")))                                                                                      //Natural: IF MSG-INFO-SUB.##MSG NE ' ' THEN
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM V1-CWF-WP-WORK-PRCSS-ID
            sub_V1_Cwf_Wp_Work_Prcss_Id();
            if (condition(Global.isEscape())) {return;}
            pdaCwfa1400.getCwfa1400().setValuesByName(vw_cwf_Wp_Work_Prcss_Id);                                                                                           //Natural: MOVE BY NAME CWF-WP-WORK-PRCSS-ID TO CWFA1400
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ")))                                                                                      //Natural: IF MSG-INFO-SUB.##MSG NE ' ' THEN
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT-OBJECT
    }
    //*  CHECK AND UPDATE
    private void sub_C_Cwf_Wp_Routing() throws Exception                                                                                                                  //Natural: C-CWF-WP-ROUTING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cwf_Wp_Routing_Rel_Mbn_Structure.reset();                                                                                                                     //Natural: RESET #CWF-WP-ROUTING-REL.MBN-STRUCTURE
        pnd_Cwf_Wp_Routing_Rel_Related_Key.setValue(pdaCwfa1400.getCwfa1400_Id());                                                                                        //Natural: ASSIGN #CWF-WP-ROUTING-REL.RELATED-KEY = CWFA1400-ID
                                                                                                                                                                          //Natural: PERFORM AS-CWF-WP-ROUTING
        sub_As_Cwf_Wp_Routing();
        if (condition(Global.isEscape())) {return;}
        //*  PROCESS ALL ENTITIES RELATED TO THE PARENT ENTITY.
        pnd_Indx_Pnd_Cwf_Wp_Routing.reset();                                                                                                                              //Natural: RESET #INDX.#CWF-WP-ROUTING
        FOR01:                                                                                                                                                            //Natural: FOR #L2 = 1 TO #MAX.#CWF-WP-ROUTING
        for (pnd_L2.setValue(1); condition(pnd_L2.lessOrEqual(pnd_Max_Pnd_Cwf_Wp_Routing)); pnd_L2.nadd(1))
        {
            //*  IF UPDATING/DELETING AN EXISTING RECORD SET #OLD-REC.
            if (condition(pdaCwfa1401.getCwfa1401_Pnd_Cwf_Wp_Routing().getValue(pnd_L2).equals(getZero())))                                                               //Natural: IF CWFA1401.#CWF-WP-ROUTING ( #L2 ) = 0 THEN
            {
                pnd_Old_Rec.setValue(false);                                                                                                                              //Natural: ASSIGN #OLD-REC = FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Old_Rec.setValue(true);                                                                                                                               //Natural: ASSIGN #OLD-REC = TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  DECIDE WHETHER CURRENT RECORD LINE IS NON-NULL.
            pnd_Save_Rec.reset();                                                                                                                                         //Natural: RESET #SAVE-REC
            if (condition(pdaCdaobj.getCdaobj_Pnd_Function().notEquals("DELETE")))                                                                                        //Natural: IF CDAOBJ.#FUNCTION NE 'DELETE'
            {
                //*  DETERMINE WHETHER CURRENT RECORD IS A CANDIDATE FOR SAVING TO
                //*  THE DATA BASE BY CHECKING IF NON-KEY VALUES ARE ENTERED.
                if (condition(pdaCwfa1400.getCwfa1400_Rt_Sqnce_Nbr().getValue(pnd_L2).notEquals(getZero()) || pdaCwfa1400.getCwfa1400_Unit_Id_Cde().getValue(pnd_L2).notEquals(" ")  //Natural: IF CWFA1400.RT-SQNCE-NBR ( #L2 ) NE 0 OR CWFA1400.UNIT-ID-CDE ( #L2 ) NE ' ' OR CWFA1400.UNIT-RGN-CDE ( #L2 ) NE ' ' OR CWFA1400.UNIT-SPCL-DSGNTN-CDE ( #L2 ) NE ' ' OR CWFA1400.UNIT-BRNCH-GROUP-CDE ( #L2 ) NE ' ' OR CWFA1400.ENTRY-DTE-TME ( #L2 ) NE 0 OR CWFA1400.ENTRY-OPRTR-CDE ( #L2 ) NE ' ' OR CWFA1400.UPDTE-DTE ( #L2 ) NE 0 OR CWFA1400.UPDTE-DTE-TME ( #L2 ) NE 0 OR CWFA1400.UPDTE-OPRTR-CDE ( #L2 ) NE ' ' OR CWFA1400.DLTE-DTE-TME ( #L2 ) NE 0 OR CWFA1400.DLTE-OPRTR-CDE ( #L2 ) NE ' '
                    || pdaCwfa1400.getCwfa1400_Unit_Rgn_Cde().getValue(pnd_L2).notEquals(" ") || pdaCwfa1400.getCwfa1400_Unit_Spcl_Dsgntn_Cde().getValue(pnd_L2).notEquals(" ") 
                    || pdaCwfa1400.getCwfa1400_Unit_Brnch_Group_Cde().getValue(pnd_L2).notEquals(" ") || pdaCwfa1400.getCwfa1400_Entry_Dte_Tme().getValue(pnd_L2).notEquals(getZero()) 
                    || pdaCwfa1400.getCwfa1400_Entry_Oprtr_Cde().getValue(pnd_L2).notEquals(" ") || pdaCwfa1400.getCwfa1400_Updte_Dte().getValue(pnd_L2).notEquals(getZero()) 
                    || pdaCwfa1400.getCwfa1400_Updte_Dte_Tme().getValue(pnd_L2).notEquals(getZero()) || pdaCwfa1400.getCwfa1400_Updte_Oprtr_Cde().getValue(pnd_L2).notEquals(" ") 
                    || pdaCwfa1400.getCwfa1400_Dlte_Dte_Tme().getValue(pnd_L2).notEquals(getZero()) || pdaCwfa1400.getCwfa1400_Dlte_Oprtr_Cde().getValue(pnd_L2).notEquals(" ")))
                {
                    pnd_Save_Rec.setValue(true);                                                                                                                          //Natural: ASSIGN #SAVE-REC = #SAVE.#CWF-WP-ROUTING ( #L2 ) = TRUE
                    pnd_Save_Pnd_Cwf_Wp_Routing.getValue(pnd_L2).setValue(true);
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-DB-CALL
            sub_Get_Db_Call();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Call_Pnd_Cwf_Wp_Routing.getValue(pnd_L2).setValue(pnd_Db_Call);                                                                                           //Natural: ASSIGN #CALL.#CWF-WP-ROUTING ( #L2 ) = #DB-CALL
            if (condition(pnd_Call_Pnd_Cwf_Wp_Routing.getValue(pnd_L2).equals("U") || pnd_Call_Pnd_Cwf_Wp_Routing.getValue(pnd_L2).equals("D")))                          //Natural: IF #CALL.#CWF-WP-ROUTING ( #L2 ) = 'U' OR = 'D'
            {
                G_CWF_WP_ROUTING:                                                                                                                                         //Natural: GET CWF-WP-ROUTING RECORD CWFA1401.#CWF-WP-ROUTING ( #L2 )
                vw_cwf_Wp_Routing.readByID(pdaCwfa1401.getCwfa1401_Pnd_Cwf_Wp_Routing().getValue(pnd_L2).getLong(), "G_CWF_WP_ROUTING");
            }                                                                                                                                                             //Natural: END-IF
            //*  CHECK AND UPDATE THE ENTITY AND ITS CHILDREN
            short decideConditionsMet1153 = 0;                                                                                                                            //Natural: DECIDE ON EVERY VALUE #CALL.#CWF-WP-ROUTING ( #L2 );//Natural: VALUE 'U','S','D'
            if (condition((pnd_Call_Pnd_Cwf_Wp_Routing.getValue(pnd_L2).equals("U")) || (pnd_Call_Pnd_Cwf_Wp_Routing.getValue(pnd_L2).equals("S")) || 
                (pnd_Call_Pnd_Cwf_Wp_Routing.getValue(pnd_L2).equals("D"))))
            {
                decideConditionsMet1153++;
                //*  PRE-EDITING
                //*  UPDATE
                //*  STORE
                                                                                                                                                                          //Natural: PERFORM E-CWF-WP-ROUTING
                sub_E_Cwf_Wp_Routing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'U'
            if (condition(pnd_Call_Pnd_Cwf_Wp_Routing.getValue(pnd_L2).equals("U")))
            {
                decideConditionsMet1153++;
                vw_cwf_Wp_Routing.updateDBRow("G_CWF_WP_ROUTING");                                                                                                        //Natural: UPDATE ( G-CWF-WP-ROUTING. )
            }                                                                                                                                                             //Natural: VALUE 'S'
            if (condition(pnd_Call_Pnd_Cwf_Wp_Routing.getValue(pnd_L2).equals("S")))
            {
                decideConditionsMet1153++;
                //*  DELETE
                vw_cwf_Wp_Routing.insertDBRow();                                                                                                                          //Natural: STORE CWF-WP-ROUTING
            }                                                                                                                                                             //Natural: VALUE 'D'
            if (condition(pnd_Call_Pnd_Cwf_Wp_Routing.getValue(pnd_L2).equals("D")))
            {
                decideConditionsMet1153++;
                vw_cwf_Wp_Routing.deleteDBRow("G_CWF_WP_ROUTING");                                                                                                        //Natural: DELETE ( G-CWF-WP-ROUTING. )
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet1153 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CONFIRM MINIMUM CARDINALITY.
        if (condition(((pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") || pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE")) && pnd_Indx_Pnd_Cwf_Wp_Routing.less(pnd_Min_Pnd_Cwf_Wp_Routing)))) //Natural: IF #FUNCTION = 'UPDATE' OR = 'STORE' AND #INDX.#CWF-WP-ROUTING LT #MIN.#CWF-WP-ROUTING
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2).setValue("Cwf Wp Routing");                                                                        //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 2 ) = 'Cwf Wp Routing'
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("A minimum of 1 occurrence of:2:is required:3:");                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'A minimum of 1 occurrence of:2:is required:3:'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  C-CWF-WP-ROUTING
    }
    //*  PRE-EDITING
    private void sub_E_Cwf_Wp_Routing() throws Exception                                                                                                                  //Natural: E-CWF-WP-ROUTING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Call_Pnd_Cwf_Wp_Routing.getValue(pnd_L2).equals("U") || pnd_Call_Pnd_Cwf_Wp_Routing.getValue(pnd_L2).equals("S")))                              //Natural: IF #CALL.#CWF-WP-ROUTING ( #L2 ) = 'U' OR = 'S'
        {
            //*  IF UPDATE OR STORE THE RECORD, DETERMINE THE KEY VALUE
            pnd_Indx_Pnd_Cwf_Wp_Routing.nadd(1);                                                                                                                          //Natural: ADD 1 TO #INDX.#CWF-WP-ROUTING
            pnd_Cwf_Wp_Routing_Rel_Actve_Ind_2_2.setValue(pnd_Indx_Pnd_Cwf_Wp_Routing);                                                                                   //Natural: ASSIGN #CWF-WP-ROUTING-REL.ACTVE-IND-2-2 = #INDX.#CWF-WP-ROUTING
                                                                                                                                                                          //Natural: PERFORM LN-CWF-WP-ROUTING
            sub_Ln_Cwf_Wp_Routing();
            if (condition(Global.isEscape())) {return;}
            //*  BUILD UP THE CURRENT RECORD FOR EDITING OR UPDATING
            vw_cwf_Wp_Routing.setValuesByName(pdaCwfa1400.getCwfa1400_Cwf_Wp_Routing_Rel().getValue(pnd_L2));                                                             //Natural: MOVE BY NAME CWFA1400.CWF-WP-ROUTING-REL ( #L2 ) TO CWF-WP-ROUTING
            cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp.setValue(pdaCwfa1400.getCwfa1400_Rt_Srvce_Time_Stndrd_Grp().getValue(pnd_L2));                                    //Natural: MOVE CWFA1400.RT-SRVCE-TIME-STNDRD-GRP ( #L2 ) TO CWF-WP-ROUTING.CRPRTE-SRVCE-TIME-STNDRD-GRP
            //* *SAG DEFINE EXIT PGC01
            //*  MOVE IN E-CWF-WP-ROUTING SUBROUTINE BEFORE
            //*  MOVE BY NAME #CWF-WP-ROUTING-REL.MBN-STRUCTURE
            //*    TO CWF-WP-ROUTING
            pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Nbr.setValue(pdaCwfa1400.getCwfa1400_Rt_Sqnce_Nbr().getValue(pnd_L2));                                                        //Natural: MOVE CWFA1400.RT-SQNCE-NBR ( #L2 ) TO #CWF-WP-ROUTING-REL.RT-SQNCE-NBR
            //* *SAG END-EXIT
            vw_cwf_Wp_Routing.setValuesByName(pnd_Cwf_Wp_Routing_Rel_Mbn_Structure);                                                                                      //Natural: MOVE BY NAME #CWF-WP-ROUTING-REL.MBN-STRUCTURE TO CWF-WP-ROUTING
                                                                                                                                                                          //Natural: PERFORM V0-CWF-WP-ROUTING
            sub_V0_Cwf_Wp_Routing();
            if (condition(Global.isEscape())) {return;}
            pdaCwfa1400.getCwfa1400_Cwf_Wp_Routing_Rel().getValue(pnd_L2).setValuesByName(vw_cwf_Wp_Routing);                                                             //Natural: MOVE BY NAME CWF-WP-ROUTING TO CWFA1400.CWF-WP-ROUTING-REL ( #L2 )
            pdaCwfa1400.getCwfa1400_Rt_Srvce_Time_Stndrd_Grp().getValue(pnd_L2).setValue(cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp);                                    //Natural: MOVE CWF-WP-ROUTING.CRPRTE-SRVCE-TIME-STNDRD-GRP TO CWFA1400.RT-SRVCE-TIME-STNDRD-GRP ( #L2 )
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ")))                                                                                      //Natural: IF MSG-INFO-SUB.##MSG NE ' ' THEN
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field_Index1().setValue(pnd_L2);                                                                                //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD-INDEX1 = #L2
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM V1-CWF-WP-ROUTING
            sub_V1_Cwf_Wp_Routing();
            if (condition(Global.isEscape())) {return;}
            pdaCwfa1400.getCwfa1400_Cwf_Wp_Routing_Rel().getValue(pnd_L2).setValuesByName(vw_cwf_Wp_Routing);                                                             //Natural: MOVE BY NAME CWF-WP-ROUTING TO CWFA1400.CWF-WP-ROUTING-REL ( #L2 )
            pdaCwfa1400.getCwfa1400_Rt_Srvce_Time_Stndrd_Grp().getValue(pnd_L2).setValue(cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp);                                    //Natural: MOVE CWF-WP-ROUTING.CRPRTE-SRVCE-TIME-STNDRD-GRP TO CWFA1400.RT-SRVCE-TIME-STNDRD-GRP ( #L2 )
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ")))                                                                                      //Natural: IF MSG-INFO-SUB.##MSG NE ' ' THEN
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field_Index1().setValue(pnd_L2);                                                                                //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD-INDEX1 = #L2
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  E-CWF-WP-ROUTING
    }
    private void sub_Check_Existence() throws Exception                                                                                                                   //Natural: CHECK-EXISTENCE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CHECK WHETHER OBJECT CURRENTLY EXISTS.
        vw_next_View.createHistogram                                                                                                                                      //Natural: HISTOGRAM ( 1 ) NEXT-VIEW FOR WPID-UNIQ-KEY FROM CWFA1400-ID THRU CWFA1400-ID
        (
        "EXISTENCE_CHECK",
        "WPID_UNIQ_KEY",
        new Wc[] { new Wc("WPID_UNIQ_KEY", ">=", pdaCwfa1400.getCwfa1400_Id(), "And", WcType.WITH) ,
        new Wc("WPID_UNIQ_KEY", "<=", pdaCwfa1400.getCwfa1400_Id(), WcType.WITH) },
        1
        );
        EXISTENCE_CHECK:
        while (condition(vw_next_View.readNextRow("EXISTENCE_CHECK")))
        {
            //*  OBJECT ID NOT UNIQUE.
            if (condition(vw_next_View.getAstNUMBER().greater(1)))                                                                                                        //Natural: IF *NUMBER ( EXISTENCE-CHECK. ) GT 1 THEN
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Data integrity error,:2:occurrences of:1:");                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'Data integrity error,:2:occurrences of:1:'
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2).setValue(vw_next_View.getAstNUMBER());                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 2 ) = *NUMBER ( EXISTENCE-CHECK. )
                pdaCwfa1401.getCwfa1401().reset();                                                                                                                        //Natural: RESET CWFA1401
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("EXISTENCE_CHECK"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("EXISTENCE_CHECK"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pdaCdaobj.getCdaobj_Pnd_Exists().setValue(true);                                                                                                              //Natural: ASSIGN CDAOBJ.#EXISTS = TRUE
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //*  CHECK-EXISTENCE
    }
    private void sub_Clear() throws Exception                                                                                                                             //Natural: CLEAR
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        //*  CAPTURE KEY.
        pdaCwfa1400.getCwfa1400_Id_Structure().setValuesByName(pdaCwfa1400.getCwfa1400());                                                                                //Natural: MOVE BY NAME CWFA1400 TO CWFA1400-ID.STRUCTURE
        pdaCwfa1400.getCwfa1400().reset();                                                                                                                                //Natural: RESET CWFA1400 CWFA1401
        pdaCwfa1401.getCwfa1401().reset();
        //*  RESTORE KEY.
        pdaCwfa1400.getCwfa1400().setValuesByName(pdaCwfa1400.getCwfa1400_Id_Structure());                                                                                //Natural: MOVE BY NAME CWFA1400-ID.STRUCTURE TO CWFA1400
        //*  CLEAR
    }
    private void sub_V0_Cwf_Wp_Work_Prcss_Id() throws Exception                                                                                                           //Natural: V0-CWF-WP-WORK-PRCSS-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INSERT CODE WHICH IS NECESSARY TO VALIDATE/MANIPULATE THE DATA FROM
        //*  CWF-WP-WORK-PRCSS-ID RECORD BEFORE AN UPDATE/STORE OPERATION,
        //*  BEFORE ITS CHILDREN's processing and before its PREDICT rules if any.
        cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme.setValue(pdaCwfa1400.getCwfa1400_Entry_Dte_Tmex());                                                                            //Natural: MOVE CWFA1400.ENTRY-DTE-TMEX TO CWF-WP-WORK-PRCSS-ID.ENTRY-DTE-TME
        cwf_Wp_Work_Prcss_Id_Entry_Oprtr_Cde.setValue(pdaCwfa1400.getCwfa1400_Entry_Oprtr_Cdex());                                                                        //Natural: MOVE CWFA1400.ENTRY-OPRTR-CDEX TO CWF-WP-WORK-PRCSS-ID.ENTRY-OPRTR-CDE
        cwf_Wp_Work_Prcss_Id_Updte_Dte.setValue(pdaCwfa1400.getCwfa1400_Updte_Dtex());                                                                                    //Natural: MOVE CWFA1400.UPDTE-DTEX TO CWF-WP-WORK-PRCSS-ID.UPDTE-DTE
        cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme.setValue(pdaCwfa1400.getCwfa1400_Updte_Dte_Tmex());                                                                            //Natural: MOVE CWFA1400.UPDTE-DTE-TMEX TO CWF-WP-WORK-PRCSS-ID.UPDTE-DTE-TME
        cwf_Wp_Work_Prcss_Id_Updte_Oprtr_Cde.setValue(pdaCwfa1400.getCwfa1400_Updte_Oprtr_Cdex());                                                                        //Natural: MOVE CWFA1400.UPDTE-OPRTR-CDEX TO CWF-WP-WORK-PRCSS-ID.UPDTE-OPRTR-CDE
        cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme.setValue(pdaCwfa1400.getCwfa1400_Dlte_Dte_Tmex());                                                                              //Natural: MOVE CWFA1400.DLTE-DTE-TMEX TO CWF-WP-WORK-PRCSS-ID.DLTE-DTE-TME
        cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde.setValue(pdaCwfa1400.getCwfa1400_Dlte_Oprtr_Cdex());                                                                          //Natural: MOVE CWFA1400.DLTE-OPRTR-CDEX TO CWF-WP-WORK-PRCSS-ID.DLTE-OPRTR-CDE
        short decideConditionsMet1263 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-WP-WORK-PRCSS-ID.WORK-PRCSS-LONG-NME = ' '
        if (condition(cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme.equals(" ")))
        {
            decideConditionsMet1263++;
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:is required");                                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Long Name");                                                                             //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Long Name'
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("WORK-PRCSS-LONG-NME");                                                                            //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'WORK-PRCSS-LONG-NME'
        }                                                                                                                                                                 //Natural: WHEN CWF-WP-WORK-PRCSS-ID.WORK-PRCSS-SHORT-NME = ' '
        else if (condition(cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme.equals(" ")))
        {
            decideConditionsMet1263++;
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:is required");                                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Short Name");                                                                            //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Short Name'
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("WORK-PRCSS-SHORT-NME");                                                                           //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'WORK-PRCSS-SHORT-NME'
        }                                                                                                                                                                 //Natural: WHEN CWF-WP-WORK-PRCSS-ID.ACTVE-IND EQ ' '
        else if (condition(cwf_Wp_Work_Prcss_Id_Actve_Ind.equals(" ")))
        {
            decideConditionsMet1263++;
            cwf_Wp_Work_Prcss_Id_Actve_Ind.setValue("A ");                                                                                                                //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.ACTVE-IND = 'A '
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'STORE'
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE")))
        {
            decideConditionsMet1263++;
            cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme.setValue(Global.getTIMX());                                                                                                //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.ENTRY-DTE-TME = *TIMX
            cwf_Wp_Work_Prcss_Id_Entry_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                         //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.ENTRY-OPRTR-CDE = *INIT-USER
            cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme.setValue(Global.getTIMX());                                                                                                //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.ENTRY-DTE-TME = *TIMX
            cwf_Wp_Work_Prcss_Id_Entry_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                         //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.ENTRY-OPRTR-CDE = *INIT-USER
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'UPDATE'
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE")))
        {
            decideConditionsMet1263++;
            cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.UPDTE-DTE-TME = *TIMX
            cwf_Wp_Work_Prcss_Id_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                         //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.UPDTE-OPRTR-CDE = *INIT-USER
            cwf_Wp_Work_Prcss_Id_Updte_Dte.setValue(Global.getDATN());                                                                                                    //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.UPDTE-DTE = *DATN
            cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.UPDTE-DTE-TME = *TIMX
            cwf_Wp_Work_Prcss_Id_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                         //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.UPDTE-OPRTR-CDE = *INIT-USER
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1263 > 0))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  V0-CWF-WP-WORK-PRCSS-ID
    }
    private void sub_V1_Cwf_Wp_Work_Prcss_Id() throws Exception                                                                                                           //Natural: V1-CWF-WP-WORK-PRCSS-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INSERT CODE WHICH IS NECESSARY TO VALIDATE/MANIPULATE THE DATA FROM
        //*  CWF-WP-WORK-PRCSS-ID RECORD BEFORE AN UPDATE/STORE OPERATION,
        //*  BEFORE ITS CHILDREN's processing and after its PREDICT rules if any.
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  V1-CWF-WP-WORK-PRCSS-ID
    }
    private void sub_V2_Cwf_Wp_Work_Prcss_Id() throws Exception                                                                                                           //Natural: V2-CWF-WP-WORK-PRCSS-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INSERT CODE WHICH IS NECESSARY TO VALIDATE/MANIPULATE THE DATA FROM
        //*  CWF-WP-WORK-PRCSS-ID RECORD BEFORE AN UPDATE/STORE OPERATION
        //*  AFTER ALL OF ITS CHILDREN HAVE BEEN PROCESSED.
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  V2-CWF-WP-WORK-PRCSS-ID
    }
    private void sub_V0_Cwf_Wp_Routing() throws Exception                                                                                                                 //Natural: V0-CWF-WP-ROUTING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INSERT CODE WHICH IS NECESSARY TO VALIDATE/MANIPULATE THE DATA FROM
        //*  CWF-WP-ROUTING RECORD BEFORE AN UPDATE/STORE OPERATION,
        //*  BEFORE ITS CHILDREN's processing and before its PREDICT rules if any.
        if (condition(cwf_Wp_Routing_Rt_Sqnce_Nbr.greater(getZero()) && cwf_Wp_Routing_Unit_Cde.greater(" ")))                                                            //Natural: IF CWF-WP-ROUTING.RT-SQNCE-NBR GT 0 AND CWF-WP-ROUTING.UNIT-CDE GT ' '
        {
            short decideConditionsMet1320 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-WP-ROUTING.ACTVE-IND EQ ' '
            if (condition(cwf_Wp_Routing_Actve_Ind.equals(" ")))
            {
                decideConditionsMet1320++;
                cwf_Wp_Routing_Actve_Ind.setValue("A ");                                                                                                                  //Natural: ASSIGN CWF-WP-ROUTING.ACTVE-IND = 'A '
            }                                                                                                                                                             //Natural: WHEN CWF-WP-ROUTING.ENTRY-DTE-TME EQ 0
            else if (condition(cwf_Wp_Routing_Entry_Dte_Tme.equals(getZero())))
            {
                decideConditionsMet1320++;
                if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE")))                                                                                        //Natural: IF CDAOBJ.#FUNCTION = 'STORE'
                {
                    cwf_Wp_Routing_Entry_Dte_Tme.setValue(Global.getTIMX());                                                                                              //Natural: ASSIGN CWF-WP-ROUTING.ENTRY-DTE-TME = *TIMX
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN CWF-WP-ROUTING.ENTRY-OPRTR-CDE EQ ' '
            else if (condition(cwf_Wp_Routing_Entry_Oprtr_Cde.equals(" ")))
            {
                decideConditionsMet1320++;
                if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE")))                                                                                        //Natural: IF CDAOBJ.#FUNCTION = 'STORE'
                {
                    if (condition(Global.getTPSYS().equals("VM/CMS")))                                                                                                    //Natural: IF *TPSYS = 'VM/CMS'
                    {
                        cwf_Wp_Routing_Entry_Oprtr_Cde.setValue(Global.getUSER());                                                                                        //Natural: ASSIGN CWF-WP-ROUTING.ENTRY-OPRTR-CDE = *USER
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        cwf_Wp_Routing_Entry_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                   //Natural: ASSIGN CWF-WP-ROUTING.ENTRY-OPRTR-CDE = *INIT-USER
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN CWF-WP-ROUTING.UPDTE-DTE EQ 0
            else if (condition(cwf_Wp_Routing_Updte_Dte.equals(getZero())))
            {
                decideConditionsMet1320++;
                if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE")))                                                                                       //Natural: IF CDAOBJ.#FUNCTION = 'UPDATE'
                {
                    cwf_Wp_Routing_Updte_Dte.setValue(Global.getDATN());                                                                                                  //Natural: MOVE *DATN TO CWF-WP-ROUTING.UPDTE-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN CWF-WP-ROUTING.UPDTE-OPRTR-CDE EQ ' '
            else if (condition(cwf_Wp_Routing_Updte_Oprtr_Cde.equals(" ")))
            {
                decideConditionsMet1320++;
                if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE")))                                                                                       //Natural: IF CDAOBJ.#FUNCTION = 'UPDATE'
                {
                    if (condition(Global.getTPSYS().equals("VM/CMS")))                                                                                                    //Natural: IF *TPSYS = 'VM/CMS'
                    {
                        cwf_Wp_Routing_Updte_Oprtr_Cde.setValue(Global.getUSER());                                                                                        //Natural: ASSIGN CWF-WP-ROUTING.UPDTE-OPRTR-CDE = *USER
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        cwf_Wp_Routing_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                   //Natural: ASSIGN CWF-WP-ROUTING.UPDTE-OPRTR-CDE = *INIT-USER
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1320 > 0))
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            vw_cwf_Wp_Routing.reset();                                                                                                                                    //Natural: RESET CWF-WP-ROUTING
        }                                                                                                                                                                 //Natural: END-IF
        //*  V0-CWF-WP-ROUTING
    }
    private void sub_V1_Cwf_Wp_Routing() throws Exception                                                                                                                 //Natural: V1-CWF-WP-ROUTING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INSERT CODE WHICH IS NECESSARY TO VALIDATE/MANIPULATE THE DATA FROM
        //*  CWF-WP-ROUTING RECORD BEFORE AN UPDATE/STORE OPERATION,
        //*  BEFORE ITS CHILDREN's processing and after its PREDICT rules if any.
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  V1-CWF-WP-ROUTING
    }
    private void sub_A_Cwf_Wp_Work_Prcss_Id() throws Exception                                                                                                            //Natural: A-CWF-WP-WORK-PRCSS-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INSERT CODE WHICH IS NECESSARY TO MANIPULATE THE DATA FROM
        //*  CWF-WP-WORK-PRCSS-ID RECORD BEFORE RETURNING TO THE OBJECT.
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  A-CWF-WP-WORK-PRCSS-ID
    }
    private void sub_A_Cwf_Wp_Routing() throws Exception                                                                                                                  //Natural: A-CWF-WP-ROUTING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INSERT CODE WHICH IS NECESSARY TO MANIPULATE THE DATA FROM
        //*  CWF-WP-ROUTING RECORD BEFORE RETURNING TO THE OBJECT.
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  A-CWF-WP-ROUTING
    }
    private void sub_Bd_Cwf_Wp_Routing() throws Exception                                                                                                                 //Natural: BD-CWF-WP-ROUTING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cwf_Wp_Routing_Rel_Related_Key.reset();                                                                                                                       //Natural: RESET #CWF-WP-ROUTING-REL.RELATED-KEY
        pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Actn_Rqstd_Cde.setValue(pdaCwfa1400.getCwfa1400_Id_Work_Actn_Rqstd_Cde());                                                        //Natural: MOVE CWFA1400-ID.WORK-ACTN-RQSTD-CDE TO #CWF-WP-ROUTING-REL.RT-SQNCE-ACTN-RQSTD-CDE
        pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Lob_Cmpny_Prdct_Cde.setValue(pdaCwfa1400.getCwfa1400_Id_Work_Lob_Cmpny_Prdct_Cde());                                              //Natural: MOVE CWFA1400-ID.WORK-LOB-CMPNY-PRDCT-CDE TO #CWF-WP-ROUTING-REL.RT-SQNCE-LOB-CMPNY-PRDCT-CDE
        pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Mjr_Bsnss_Prcss_Cde.setValue(pdaCwfa1400.getCwfa1400_Id_Work_Mjr_Bsnss_Prcss_Cde());                                              //Natural: MOVE CWFA1400-ID.WORK-MJR-BSNSS-PRCSS-CDE TO #CWF-WP-ROUTING-REL.RT-SQNCE-MJR-BSNSS-PRCSS-CDE
        pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Spcfc_Bsnss_Prcss_Cde.setValue(pdaCwfa1400.getCwfa1400_Id_Work_Spcfc_Bsnss_Prcss_Cde());                                          //Natural: MOVE CWFA1400-ID.WORK-SPCFC-BSNSS-PRCSS-CDE TO #CWF-WP-ROUTING-REL.RT-SQNCE-SPCFC-BSNSS-PRCSS-CDE
        pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Nbr.setValue(0);                                                                                                                  //Natural: MOVE 000 TO #CWF-WP-ROUTING-REL.RT-SQNCE-NBR
        pnd_From_Cwf_Wp_Routing_Rel.setValue(pnd_Cwf_Wp_Routing_Rel);                                                                                                     //Natural: ASSIGN #FROM.CWF-WP-ROUTING-REL = #CWF-WP-ROUTING-REL
        pnd_Cwf_Wp_Routing_Rel_Rt_Sqnce_Nbr.setValue(999);                                                                                                                //Natural: MOVE 999 TO #CWF-WP-ROUTING-REL.RT-SQNCE-NBR
        pnd_Thru_Cwf_Wp_Routing_Rel.setValue(pnd_Cwf_Wp_Routing_Rel);                                                                                                     //Natural: ASSIGN #THRU.CWF-WP-ROUTING-REL = #CWF-WP-ROUTING-REL
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  BD-CWF-WP-ROUTING
    }
    private void sub_As_Cwf_Wp_Routing() throws Exception                                                                                                                 //Natural: AS-CWF-WP-ROUTING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cwf_Wp_Routing_Rel_Related_Key.reset();                                                                                                                       //Natural: RESET #CWF-WP-ROUTING-REL.RELATED-KEY
        pnd_Cwf_Wp_Routing_Rel_Related_Key.setValue(pdaCwfa1400.getCwfa1400_Id());                                                                                        //Natural: MOVE CWFA1400-ID TO #CWF-WP-ROUTING-REL.RELATED-KEY
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  AS-CWF-WP-ROUTING
    }
    private void sub_Ln_Cwf_Wp_Routing() throws Exception                                                                                                                 //Natural: LN-CWF-WP-ROUTING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cwf_Wp_Routing_Rel_Actve_Ind_2_2.setValue(" ");                                                                                                               //Natural: ASSIGN #CWF-WP-ROUTING-REL.ACTVE-IND-2-2 = ' '
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  LN-CWF-WP-ROUTING
    }
    private void sub_Get_Db_Call() throws Exception                                                                                                                       //Natural: GET-DB-CALL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1422 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #OLD-REC AND ( CDAOBJ.#FUNCTION = 'DELETE' OR ( CDAOBJ.#FUNCTION = 'UPDATE' AND NOT #SAVE-REC ) )
        if (condition((pnd_Old_Rec.getBoolean() && (pdaCdaobj.getCdaobj_Pnd_Function().equals("DELETE") || (pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") 
            && ! (pnd_Save_Rec.getBoolean()))))))
        {
            decideConditionsMet1422++;
            //*  DELETE
            pnd_Db_Call.setValue("D");                                                                                                                                    //Natural: ASSIGN #DB-CALL = 'D'
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'UPDATE' AND #SAVE-REC AND #OLD-REC
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") && pnd_Save_Rec.getBoolean() && pnd_Old_Rec.getBoolean()))
        {
            decideConditionsMet1422++;
            //*  UPDATE
            pnd_Db_Call.setValue("U");                                                                                                                                    //Natural: ASSIGN #DB-CALL = 'U'
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'UPDATE' AND #SAVE-REC AND NOT #OLD-REC
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") && pnd_Save_Rec.getBoolean() && ! (pnd_Old_Rec.getBoolean())))
        {
            decideConditionsMet1422++;
            pnd_Db_Call.setValue("S");                                                                                                                                    //Natural: ASSIGN #DB-CALL = 'S'
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'STORE' AND #SAVE-REC
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE") && pnd_Save_Rec.getBoolean()))
        {
            decideConditionsMet1422++;
            pnd_Db_Call.setValue("S");                                                                                                                                    //Natural: ASSIGN #DB-CALL = 'S'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Db_Call.reset();                                                                                                                                          //Natural: RESET #DB-CALL
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  GET-DB-CALL
    }
    private void sub_Set_Object_Id() throws Exception                                                                                                                     //Natural: SET-OBJECT-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaCwfa1400.getCwfa1400_Work_Actn_Rqstd_Cde(),  //Natural: COMPRESS CWFA1400.WORK-ACTN-RQSTD-CDE '-' CWFA1400.WORK-LOB-CMPNY-PRDCT-CDE '-' CWFA1400.WORK-MJR-BSNSS-PRCSS-CDE '-' CWFA1400.WORK-SPCFC-BSNSS-PRCSS-CDE '-' CWFA1400.ACTVE-IND-2-2 TO MSG-INFO-SUB.##MSG-DATA ( 1 ) LEAVING NO
            "-", pdaCwfa1400.getCwfa1400_Work_Lob_Cmpny_Prdct_Cde(), "-", pdaCwfa1400.getCwfa1400_Work_Mjr_Bsnss_Prcss_Cde(), "-", pdaCwfa1400.getCwfa1400_Work_Spcfc_Bsnss_Prcss_Cde(), 
            "-", pdaCwfa1400.getCwfa1400_Actve_Ind_2_2()));
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(DbsUtil.compress(pnd_Object, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1)));    //Natural: COMPRESS #OBJECT MSG-INFO-SUB.##MSG-DATA ( 1 ) TO MSG-INFO-SUB.##MSG-DATA ( 1 )
        //* *SAG DEFINE EXIT ADJUST-OBJECT-ID-IN-MSG
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValueEdited(pdaCwfa1400.getCwfa1400_Id(),new ReportEditMask("X-XX-X-XX-XXX"));                      //Natural: MOVE EDITED CWFA1400-ID ( EM = X-XX-X-XX-XXX ) TO MSG-INFO-SUB.##MSG-DATA ( 1 )
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(DbsUtil.compress(pnd_Object, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1)));    //Natural: COMPRESS #OBJECT MSG-INFO-SUB.##MSG-DATA ( 1 ) TO MSG-INFO-SUB.##MSG-DATA ( 1 )
        //* *SAG END-EXIT
        //*  SET-OBJECT-ID
    }

    //
}
