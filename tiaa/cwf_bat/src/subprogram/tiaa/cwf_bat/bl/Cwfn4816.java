/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:31:23 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn4816
************************************************************
**        * FILE NAME            : Cwfn4816.java
**        * CLASS NAME           : Cwfn4816
**        * INSTANCE NAME        : Cwfn4816
************************************************************
************************************************************************
* PROGRAM  : CWFN4816
* AUTHOR   : LEON EPSTEIN
* SYSTEM   : CORPORATE WORK FLOW REPORTING
* TITLE    : EXAMINE RUN CONTROL FOR THE MIT EXTRACT PROCESS (REPORTING)
* GENERATED: FEB 15,99 AT 15:01 PM
* FUNCTION : THIS SUB-PROGRAM CALLED BY THE EXTRACT PROCESS TO MAKE SURE
*          : THAT AN ACTIVE RUN CONTROL RECORD EXISTS.
*
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn4816 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCwfa4815 pdaCwfa4815;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaCwfl4815 ldaCwfl4815;
    private LdaCwfl4816 ldaCwfl4816;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl_Histogram;
    private DbsField cwf_Support_Tbl_Histogram_Tbl_Prime_Key;
    private DbsField pnd_Last_Run_Isn;
    private DbsField pnd_New_Run_Id;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_1;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_2;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key_From;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_3;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_4;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key_To;

    private DbsGroup pnd_Tbl_Prime_Key_To__R_Field_5;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_To__R_Field_6;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Work_Rqst_Retaining_Days;
    private DbsField pnd_Time_Out_T;
    private DbsField pnd_Time_Out_A;

    private DbsGroup pnd_Time_Out_A__R_Field_7;
    private DbsField pnd_Time_Out_A_Pnd_Time_Out_N;

    private DbsGroup pnd_Time_Out_A__R_Field_8;
    private DbsField pnd_Time_Out_A_Pnd_Time_Out_N_Yyyy;
    private DbsField pnd_Time_Out_A_Pnd_Time_Out_N_Mm;
    private DbsField pnd_Time_Out_A_Pnd_Time_Out_N_Dd;
    private DbsField pnd_Time_Out_A_Pnd_Time_Out_N_Hours;
    private DbsField pnd_Time_Out_A_Pnd_Time_Out_N_Minutes;
    private DbsField pnd_Time_Out_A_Pnd_Time_Out_N_Seconds;
    private DbsField pnd_Time_Out_A_Pnd_Time_Out_N_Tenths;
    private DbsField pnd_Starting_Date_Save;
    private DbsField pnd_Ending_Date_Save;
    private DbsField pnd_Starting_Time_A;

    private DbsGroup pnd_Starting_Time_A__R_Field_9;
    private DbsField pnd_Starting_Time_A_Pnd_Starting_Time_N;

    private DbsGroup pnd_Starting_Time_A__R_Field_10;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Yyyy;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Mm;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Dd;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Hours;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Minutes;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Seconds;
    private DbsField pnd_Starting_Time_A_Pnd_Strt_Time_N_Tenths;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCwfl4815 = new LdaCwfl4815();
        registerRecord(ldaCwfl4815);
        registerRecord(ldaCwfl4815.getVw_cwf_Support_Tbl());
        ldaCwfl4816 = new LdaCwfl4816();
        registerRecord(ldaCwfl4816);
        registerRecord(ldaCwfl4816.getVw_cwf_Support_Tbl_Last_Run_Id());

        // parameters
        parameters = new DbsRecord();
        pdaCwfa4815 = new PdaCwfa4815(parameters);
        pdaCdaobj = new PdaCdaobj(parameters);
        pdaCwfpda_D = new PdaCwfpda_D(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pdaCwfpda_P = new PdaCwfpda_P(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Support_Tbl_Histogram = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl_Histogram", "CWF-SUPPORT-TBL-HISTOGRAM"), "CWF_SUPPORT_TBL", 
            "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Histogram_Tbl_Prime_Key = vw_cwf_Support_Tbl_Histogram.getRecord().newFieldInGroup("cwf_Support_Tbl_Histogram_Tbl_Prime_Key", 
            "TBL-PRIME-KEY", FieldType.STRING, 53, RepeatingFieldStrategy.None, "TBL_PRIME_KEY");
        cwf_Support_Tbl_Histogram_Tbl_Prime_Key.setDdmHeader("RECORD KEY");
        cwf_Support_Tbl_Histogram_Tbl_Prime_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Support_Tbl_Histogram);

        pnd_Last_Run_Isn = localVariables.newFieldInRecord("pnd_Last_Run_Isn", "#LAST-RUN-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_New_Run_Id = localVariables.newFieldInRecord("pnd_New_Run_Id", "#NEW-RUN-ID", FieldType.NUMERIC, 9);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_1", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);

        pnd_Tbl_Prime_Key__R_Field_2 = pnd_Tbl_Prime_Key__R_Field_1.newGroupInGroup("pnd_Tbl_Prime_Key__R_Field_2", "REDEFINE", pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_Pnd_Run_Status = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 8);
        pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field", "#REST-OF-TBL-KEY-FIELD", 
            FieldType.STRING, 21);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_From = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_From", "#TBL-PRIME-KEY-FROM", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_From__R_Field_3 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_From__R_Field_3", "REDEFINE", pnd_Tbl_Prime_Key_From);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_From__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_From__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_From__R_Field_4 = pnd_Tbl_Prime_Key_From__R_Field_3.newGroupInGroup("pnd_Tbl_Prime_Key_From__R_Field_4", "REDEFINE", pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Run_Status", "#RUN-STATUS", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 15);
        pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 14);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_From__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_To = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_To", "#TBL-PRIME-KEY-TO", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_To__R_Field_5 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_To__R_Field_5", "REDEFINE", pnd_Tbl_Prime_Key_To);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_To__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_To__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_To__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_To__R_Field_6 = pnd_Tbl_Prime_Key_To__R_Field_5.newGroupInGroup("pnd_Tbl_Prime_Key_To__R_Field_6", "REDEFINE", pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 15);
        pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 14);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_To__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Work_Rqst_Retaining_Days = localVariables.newFieldInRecord("pnd_Work_Rqst_Retaining_Days", "#WORK-RQST-RETAINING-DAYS", FieldType.NUMERIC, 
            3);
        pnd_Time_Out_T = localVariables.newFieldInRecord("pnd_Time_Out_T", "#TIME-OUT-T", FieldType.TIME);
        pnd_Time_Out_A = localVariables.newFieldInRecord("pnd_Time_Out_A", "#TIME-OUT-A", FieldType.STRING, 15);

        pnd_Time_Out_A__R_Field_7 = localVariables.newGroupInRecord("pnd_Time_Out_A__R_Field_7", "REDEFINE", pnd_Time_Out_A);
        pnd_Time_Out_A_Pnd_Time_Out_N = pnd_Time_Out_A__R_Field_7.newFieldInGroup("pnd_Time_Out_A_Pnd_Time_Out_N", "#TIME-OUT-N", FieldType.NUMERIC, 15);

        pnd_Time_Out_A__R_Field_8 = pnd_Time_Out_A__R_Field_7.newGroupInGroup("pnd_Time_Out_A__R_Field_8", "REDEFINE", pnd_Time_Out_A_Pnd_Time_Out_N);
        pnd_Time_Out_A_Pnd_Time_Out_N_Yyyy = pnd_Time_Out_A__R_Field_8.newFieldInGroup("pnd_Time_Out_A_Pnd_Time_Out_N_Yyyy", "#TIME-OUT-N-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Time_Out_A_Pnd_Time_Out_N_Mm = pnd_Time_Out_A__R_Field_8.newFieldInGroup("pnd_Time_Out_A_Pnd_Time_Out_N_Mm", "#TIME-OUT-N-MM", FieldType.NUMERIC, 
            2);
        pnd_Time_Out_A_Pnd_Time_Out_N_Dd = pnd_Time_Out_A__R_Field_8.newFieldInGroup("pnd_Time_Out_A_Pnd_Time_Out_N_Dd", "#TIME-OUT-N-DD", FieldType.NUMERIC, 
            2);
        pnd_Time_Out_A_Pnd_Time_Out_N_Hours = pnd_Time_Out_A__R_Field_8.newFieldInGroup("pnd_Time_Out_A_Pnd_Time_Out_N_Hours", "#TIME-OUT-N-HOURS", FieldType.NUMERIC, 
            2);
        pnd_Time_Out_A_Pnd_Time_Out_N_Minutes = pnd_Time_Out_A__R_Field_8.newFieldInGroup("pnd_Time_Out_A_Pnd_Time_Out_N_Minutes", "#TIME-OUT-N-MINUTES", 
            FieldType.NUMERIC, 2);
        pnd_Time_Out_A_Pnd_Time_Out_N_Seconds = pnd_Time_Out_A__R_Field_8.newFieldInGroup("pnd_Time_Out_A_Pnd_Time_Out_N_Seconds", "#TIME-OUT-N-SECONDS", 
            FieldType.NUMERIC, 2);
        pnd_Time_Out_A_Pnd_Time_Out_N_Tenths = pnd_Time_Out_A__R_Field_8.newFieldInGroup("pnd_Time_Out_A_Pnd_Time_Out_N_Tenths", "#TIME-OUT-N-TENTHS", 
            FieldType.NUMERIC, 1);
        pnd_Starting_Date_Save = localVariables.newFieldInRecord("pnd_Starting_Date_Save", "#STARTING-DATE-SAVE", FieldType.TIME);
        pnd_Ending_Date_Save = localVariables.newFieldInRecord("pnd_Ending_Date_Save", "#ENDING-DATE-SAVE", FieldType.TIME);
        pnd_Starting_Time_A = localVariables.newFieldInRecord("pnd_Starting_Time_A", "#STARTING-TIME-A", FieldType.STRING, 15);

        pnd_Starting_Time_A__R_Field_9 = localVariables.newGroupInRecord("pnd_Starting_Time_A__R_Field_9", "REDEFINE", pnd_Starting_Time_A);
        pnd_Starting_Time_A_Pnd_Starting_Time_N = pnd_Starting_Time_A__R_Field_9.newFieldInGroup("pnd_Starting_Time_A_Pnd_Starting_Time_N", "#STARTING-TIME-N", 
            FieldType.NUMERIC, 15);

        pnd_Starting_Time_A__R_Field_10 = pnd_Starting_Time_A__R_Field_9.newGroupInGroup("pnd_Starting_Time_A__R_Field_10", "REDEFINE", pnd_Starting_Time_A_Pnd_Starting_Time_N);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Yyyy = pnd_Starting_Time_A__R_Field_10.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Yyyy", "#STRT-TIME-N-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Mm = pnd_Starting_Time_A__R_Field_10.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Mm", "#STRT-TIME-N-MM", 
            FieldType.NUMERIC, 2);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Dd = pnd_Starting_Time_A__R_Field_10.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Dd", "#STRT-TIME-N-DD", 
            FieldType.NUMERIC, 2);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Hours = pnd_Starting_Time_A__R_Field_10.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Hours", "#STRT-TIME-N-HOURS", 
            FieldType.NUMERIC, 2);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Minutes = pnd_Starting_Time_A__R_Field_10.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Minutes", "#STRT-TIME-N-MINUTES", 
            FieldType.NUMERIC, 2);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Seconds = pnd_Starting_Time_A__R_Field_10.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Seconds", "#STRT-TIME-N-SECONDS", 
            FieldType.NUMERIC, 2);
        pnd_Starting_Time_A_Pnd_Strt_Time_N_Tenths = pnd_Starting_Time_A__R_Field_10.newFieldInGroup("pnd_Starting_Time_A_Pnd_Strt_Time_N_Tenths", "#STRT-TIME-N-TENTHS", 
            FieldType.NUMERIC, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl_Histogram.reset();

        ldaCwfl4815.initializeValues();
        ldaCwfl4816.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn4816() throws Exception
    {
        super("Cwfn4816");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFN4816", onError);
        //* (TO ALLOW EASY ESCAPE FROM THE ROUTINE)
        ROUTINE:                                                                                                                                                          //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM RESET-THE-OUTPUT-PARAMETERS
            sub_Reset_The_Output_Parameters();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM EXAMINE-THE-RUN-CONTROL-RECORD
            sub_Examine_The_Run_Control_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break ROUTINE;                                                                                                                                      //Natural: ESCAPE BOTTOM ( ROUTINE. )
            //*  SUBROUTINE JUST FOR ENDING THIS SUBPROGRAM IMMEDIATELY
            //*  ------------------------------------------------------
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-SUBPROGRAM
            //* (ROUTINE.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  SUBROUTINES
        //*  ===========
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-THE-OUTPUT-PARAMETERS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXAMINE-THE-RUN-CONTROL-RECORD
        //* ************************************************
        //*  CHECK TO SEE IF AN ACTIVE RECORD REALLY EXISTS
        //* ************************************************
        //*  -------- DEFINING FROM KEY --------------------
        //*  -------- DEFINING TO KEY --------------------
        //*  ON NATURAL RUN TIME ERROR, RETURN WITH AN APPROPRIATE ERROR MESSAGE
        //*  ===================================================================
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Subprogram() throws Exception                                                                                                                 //Natural: ESCAPE-SUBPROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( ROUTINE. )
        Global.setEscapeCode(EscapeType.Bottom, "ROUTINE");
        if (true) return;
        //* (1010)
    }
    private void sub_Reset_The_Output_Parameters() throws Exception                                                                                                       //Natural: RESET-THE-OUTPUT-PARAMETERS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===========================================
        pdaCwfa4815.getCwfa4815_Output().reset();                                                                                                                         //Natural: RESET CWFA4815-OUTPUT
        //* (1100)
    }
    //*  ACTIVE STATUS
    private void sub_Examine_The_Run_Control_Record() throws Exception                                                                                                    //Natural: EXAMINE-THE-RUN-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                     //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme.setValue("CWF-EXTR-RPT-NXT-RUN");                                                                                        //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-TABLE-NME := 'CWF-EXTR-RPT-NXT-RUN'
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status.setValue("A ");                                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#RUN-STATUS := 'A '
        pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement.setValue(0);                                                                                               //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#STARTING-DATE-9S-COMPLEMENT := 0
        pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field.setValue(" ");                                                                                                   //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#REST-OF-TBL-KEY-FIELD := ' '
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind.setValue(" ");                                                                                                           //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-ACTVE-IND := ' '
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                       //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme.setValue("CWF-EXTR-RPT-NXT-RUN");                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-TABLE-NME := 'CWF-EXTR-RPT-NXT-RUN'
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status.setValue("A ");                                                                                                               //Natural: ASSIGN #TBL-PRIME-KEY-TO.#RUN-STATUS := 'A '
        pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement.setValue(999999999999999L);                                                                                  //Natural: ASSIGN #TBL-PRIME-KEY-TO.#STARTING-DATE-9S-COMPLEMENT := 999999999999999
        pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field.setValue("99999999999999");                                                                                        //Natural: ASSIGN #TBL-PRIME-KEY-TO.#REST-OF-TBL-KEY-FIELD := '99999999999999'
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind.setValue("9");                                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-ACTVE-IND := '9'
        vw_cwf_Support_Tbl_Histogram.createHistogram                                                                                                                      //Natural: HISTOGRAM CWF-SUPPORT-TBL-HISTOGRAM FOR TBL-PRIME-KEY FROM #TBL-PRIME-KEY-FROM THRU #TBL-PRIME-KEY-TO
        (
        "ACTIVE_HISTOGRAM",
        "TBL_PRIME_KEY",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key_From, "And", WcType.WITH) ,
        new Wc("TBL_PRIME_KEY", "<=", pnd_Tbl_Prime_Key_To, WcType.WITH) }
        );
        ACTIVE_HISTOGRAM:
        while (condition(vw_cwf_Support_Tbl_Histogram.readNextRow("ACTIVE_HISTOGRAM")))
        {
            if (condition(vw_cwf_Support_Tbl_Histogram.getAstNUMBER().notEquals(1)))                                                                                      //Natural: IF *NUMBER ( ACTIVE-HISTOGRAM. ) NE 1
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(25);                                                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 25
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Active control records does Not ",                 //Natural: COMPRESS 'Program' *PROGRAM '- Active control records does Not ' 'exist' INTO MSG-INFO-SUB.##MSG
                    "exist"));
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* (1390)
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tbl_Prime_Key.setValue(cwf_Support_Tbl_Histogram_Tbl_Prime_Key);                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY := CWF-SUPPORT-TBL-HISTOGRAM.TBL-PRIME-KEY
            //*  ---------------------------------------------------------------------
            ldaCwfl4815.getVw_cwf_Support_Tbl().startDatabaseFind                                                                                                         //Natural: FIND ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
            (
            "ACTIVE_FIND",
            new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
            1
            );
            ACTIVE_FIND:
            while (condition(ldaCwfl4815.getVw_cwf_Support_Tbl().readNextRow("ACTIVE_FIND")))
            {
                ldaCwfl4815.getVw_cwf_Support_Tbl().setIfNotFoundControlFlag(false);
                pdaCwfa4815.getCwfa4815_Output_Run_Control_Isn().setValue(ldaCwfl4815.getVw_cwf_Support_Tbl().getAstISN("ACTIVE_FIND"));                                  //Natural: ASSIGN CWFA4815-OUTPUT.RUN-CONTROL-ISN := *ISN ( ACTIVE-FIND. )
                pdaCwfa4815.getCwfa4815_Output_Control_Rec_Status().setValue("A");                                                                                        //Natural: ASSIGN CWFA4815-OUTPUT.CONTROL-REC-STATUS := 'A'
                //* (ACTIVE-FIND.)
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  "A" ESTABLISHED SO GET OUT
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* (ACTIVE-HISTOGRAM.)
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //* (1160)
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(999);                                                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR = 999
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("NATURAL ERROR", Global.getERROR_NR(), "IN", Global.getPROGRAM(), "....LINE",                 //Natural: COMPRESS 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE INTO MSG-INFO-SUB.##MSG
            Global.getERROR_LINE()));
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
        //* (1630)
    };                                                                                                                                                                    //Natural: END-ERROR
}
