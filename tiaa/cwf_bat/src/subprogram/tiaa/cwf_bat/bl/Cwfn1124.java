/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:13:07 PM
**        * FROM NATURAL SUBPROGRAM : Cwfn1124
************************************************************
**        * FILE NAME            : Cwfn1124.java
**        * CLASS NAME           : Cwfn1124
**        * INSTANCE NAME        : Cwfn1124
************************************************************
************************************************************************
* PROGRAM  : CWFN1124
* SYSTEM   : CRPCWF
* TITLE    : BATCH
* GENERATED: SEP 08,92 AT 10:41 AM
* FUNCTION : THIS PROGRAM ...
*             RETRIEVES EMPLOYEE NAME BASED ON RACF ID
* HISTORY  : 06/17/2002 JRF INITIALIZE TO BLANK SITE-CDE PARAMETER
*                           AT THE START OF PROGRAM
*
************************************************************************
* DATE WRITTEN : 27 AUGUST 1992
* AUTHOR       : JOSEPH S. PATINGO
*
*
* SUBROUTINE TO RETRIEVE UNIT CODE BASED ON USER ID
*
* HISTORY
* 10/10/01  GY  THIS IS A CLONE OF CWFN1107. SITE CODE IS ALSO RETURNED.
* 06/14/02  JRF INITIALIZE SITE-CDE PARAMETER AT START OF PROGRAM
* ----------------------------------------------------------------------

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfn1124 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Parm_Racf_Id;
    private DbsField pnd_Parm_Empl_Nme;

    private DbsGroup pnd_Parm_Empl_Nme__R_Field_1;
    private DbsField pnd_Parm_Empl_Nme_Pnd_Parm_Unit_Cde;
    private DbsField pnd_Parm_Site_Cde;

    private DataAccessProgramView vw_cwf_Org_Empl_Tbl;
    private DbsField cwf_Org_Empl_Tbl_Empl_Unit_Cde;
    private DbsField cwf_Org_Empl_Tbl_Empl_Racf_Id;
    private DbsField cwf_Org_Empl_Tbl_Empl_Nme;

    private DbsGroup cwf_Org_Empl_Tbl__R_Field_2;
    private DbsField cwf_Org_Empl_Tbl_Empl_First_Nme;
    private DbsField cwf_Org_Empl_Tbl_Empl_Last_Nme;
    private DbsField cwf_Org_Empl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Org_Empl_Tbl_Empl_Info;

    private DbsGroup cwf_Org_Empl_Tbl__R_Field_3;
    private DbsField cwf_Org_Empl_Tbl_Empl_Filler1;
    private DbsField cwf_Org_Empl_Tbl_Empl_More_Scrty_Ind;
    private DbsField cwf_Org_Empl_Tbl_Empl_Prjct_Id;
    private DbsField cwf_Org_Empl_Tbl_Empl_Loc;

    private DataAccessProgramView vw_cwf_Org_Unit_Tbl;
    private DbsField cwf_Org_Unit_Tbl_Unit_Cde;
    private DbsField pnd_Read_Type_Ind;
    private DbsField pnd_Search_Key1;

    private DbsGroup pnd_Search_Key1__R_Field_4;
    private DbsField pnd_Search_Key1_Pnd_Racf_Id;
    private DbsField pnd_Search_Key2;

    private DbsGroup pnd_Search_Key2__R_Field_5;
    private DbsField pnd_Search_Key2_Pnd_Unit_Cde;
    private DbsField pnd_Search_Key2_Pnd_Racf_Id;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Parm_Racf_Id = parameters.newFieldInRecord("pnd_Parm_Racf_Id", "#PARM-RACF-ID", FieldType.STRING, 8);
        pnd_Parm_Racf_Id.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Empl_Nme = parameters.newFieldInRecord("pnd_Parm_Empl_Nme", "#PARM-EMPL-NME", FieldType.STRING, 30);
        pnd_Parm_Empl_Nme.setParameterOption(ParameterOption.ByReference);

        pnd_Parm_Empl_Nme__R_Field_1 = parameters.newGroupInRecord("pnd_Parm_Empl_Nme__R_Field_1", "REDEFINE", pnd_Parm_Empl_Nme);
        pnd_Parm_Empl_Nme_Pnd_Parm_Unit_Cde = pnd_Parm_Empl_Nme__R_Field_1.newFieldInGroup("pnd_Parm_Empl_Nme_Pnd_Parm_Unit_Cde", "#PARM-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Parm_Site_Cde = parameters.newFieldInRecord("pnd_Parm_Site_Cde", "#PARM-SITE-CDE", FieldType.STRING, 2);
        pnd_Parm_Site_Cde.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Org_Empl_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Empl_Tbl", "CWF-ORG-EMPL-TBL"), "CWF_ORG_EMPL_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Empl_Tbl_Empl_Unit_Cde = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Unit_Cde", "EMPL-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_UNIT_CDE");
        cwf_Org_Empl_Tbl_Empl_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Empl_Tbl_Empl_Racf_Id = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        cwf_Org_Empl_Tbl_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF  ID");
        cwf_Org_Empl_Tbl_Empl_Nme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Nme", "EMPL-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "EMPL_NME");
        cwf_Org_Empl_Tbl_Empl_Nme.setDdmHeader("EMPLOYEE NAME");

        cwf_Org_Empl_Tbl__R_Field_2 = vw_cwf_Org_Empl_Tbl.getRecord().newGroupInGroup("cwf_Org_Empl_Tbl__R_Field_2", "REDEFINE", cwf_Org_Empl_Tbl_Empl_Nme);
        cwf_Org_Empl_Tbl_Empl_First_Nme = cwf_Org_Empl_Tbl__R_Field_2.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_First_Nme", "EMPL-FIRST-NME", FieldType.STRING, 
            20);
        cwf_Org_Empl_Tbl_Empl_Last_Nme = cwf_Org_Empl_Tbl__R_Field_2.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Last_Nme", "EMPL-LAST-NME", FieldType.STRING, 
            20);
        cwf_Org_Empl_Tbl_Dlte_Dte_Tme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Org_Empl_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        cwf_Org_Empl_Tbl_Empl_Info = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Info", "EMPL-INFO", FieldType.STRING, 100, 
            RepeatingFieldStrategy.None, "EMPL_INFO");

        cwf_Org_Empl_Tbl__R_Field_3 = vw_cwf_Org_Empl_Tbl.getRecord().newGroupInGroup("cwf_Org_Empl_Tbl__R_Field_3", "REDEFINE", cwf_Org_Empl_Tbl_Empl_Info);
        cwf_Org_Empl_Tbl_Empl_Filler1 = cwf_Org_Empl_Tbl__R_Field_3.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Filler1", "EMPL-FILLER1", FieldType.STRING, 
            8);
        cwf_Org_Empl_Tbl_Empl_More_Scrty_Ind = cwf_Org_Empl_Tbl__R_Field_3.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_More_Scrty_Ind", "EMPL-MORE-SCRTY-IND", 
            FieldType.STRING, 1);
        cwf_Org_Empl_Tbl_Empl_Prjct_Id = cwf_Org_Empl_Tbl__R_Field_3.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Prjct_Id", "EMPL-PRJCT-ID", FieldType.STRING, 
            28);
        cwf_Org_Empl_Tbl_Empl_Loc = cwf_Org_Empl_Tbl__R_Field_3.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Loc", "EMPL-LOC", FieldType.STRING, 2);
        registerRecord(vw_cwf_Org_Empl_Tbl);

        vw_cwf_Org_Unit_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Unit_Tbl", "CWF-ORG-UNIT-TBL"), "CWF_ORG_UNIT_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Unit_Tbl_Unit_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Org_Unit_Tbl_Unit_Cde.setDdmHeader("UNIT/CODE");
        registerRecord(vw_cwf_Org_Unit_Tbl);

        pnd_Read_Type_Ind = localVariables.newFieldInRecord("pnd_Read_Type_Ind", "#READ-TYPE-IND", FieldType.BOOLEAN, 1);
        pnd_Search_Key1 = localVariables.newFieldInRecord("pnd_Search_Key1", "#SEARCH-KEY1", FieldType.STRING, 9);

        pnd_Search_Key1__R_Field_4 = localVariables.newGroupInRecord("pnd_Search_Key1__R_Field_4", "REDEFINE", pnd_Search_Key1);
        pnd_Search_Key1_Pnd_Racf_Id = pnd_Search_Key1__R_Field_4.newFieldInGroup("pnd_Search_Key1_Pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Search_Key2 = localVariables.newFieldInRecord("pnd_Search_Key2", "#SEARCH-KEY2", FieldType.STRING, 18);

        pnd_Search_Key2__R_Field_5 = localVariables.newGroupInRecord("pnd_Search_Key2__R_Field_5", "REDEFINE", pnd_Search_Key2);
        pnd_Search_Key2_Pnd_Unit_Cde = pnd_Search_Key2__R_Field_5.newFieldInGroup("pnd_Search_Key2_Pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 8);
        pnd_Search_Key2_Pnd_Racf_Id = pnd_Search_Key2__R_Field_5.newFieldInGroup("pnd_Search_Key2_Pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Org_Empl_Tbl.reset();
        vw_cwf_Org_Unit_Tbl.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cwfn1124() throws Exception
    {
        super("Cwfn1124");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Read_Type_Ind.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #READ-TYPE-IND
        //* -- JF061402
        pnd_Parm_Site_Cde.reset();                                                                                                                                        //Natural: RESET #PARM-SITE-CDE
        vw_cwf_Org_Unit_Tbl.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) CWF-ORG-UNIT-TBL BY UNIQ-UNIT-CDE FROM #PARM-UNIT-CDE
        (
        "FIND_UNIT",
        new Wc[] { new Wc("UNIQ_UNIT_CDE", ">=", pnd_Parm_Empl_Nme_Pnd_Parm_Unit_Cde, WcType.BY) },
        new Oc[] { new Oc("UNIQ_UNIT_CDE", "ASC") },
        1
        );
        FIND_UNIT:
        while (condition(vw_cwf_Org_Unit_Tbl.readNextRow("FIND_UNIT")))
        {
            if (condition(cwf_Org_Unit_Tbl_Unit_Cde.greater(pnd_Parm_Empl_Nme_Pnd_Parm_Unit_Cde)))                                                                        //Natural: IF CWF-ORG-UNIT-TBL.UNIT-CDE GT #PARM-UNIT-CDE
            {
                //*  READ BY EMPL-RACF-ID-KEY ONLY
                pnd_Read_Type_Ind.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #READ-TYPE-IND
            }                                                                                                                                                             //Natural: END-IF
            //*  OTHERWISE READ BY UNIT-EMPL-RACF-ID-KEY
            //*  FIND-UNIT.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Search_Key1_Pnd_Racf_Id.setValue(pnd_Parm_Racf_Id);                                                                                                           //Natural: MOVE #PARM-RACF-ID TO #SEARCH-KEY1.#RACF-ID #SEARCH-KEY2.#RACF-ID
        pnd_Search_Key2_Pnd_Racf_Id.setValue(pnd_Parm_Racf_Id);
        if (condition(pnd_Read_Type_Ind.getBoolean()))                                                                                                                    //Natural: IF #READ-TYPE-IND
        {
            //*  FIND CWF-ORG-EMPLYE-TBL BY EMPL-RACF-ID-KEY
            vw_cwf_Org_Empl_Tbl.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) CWF-ORG-EMPL-TBL BY EMPL-RACF-ID-KEY FROM #PARM-RACF-ID
            (
            "FIND_EMPL",
            new Wc[] { new Wc("EMPL_RACF_ID_KEY", ">=", pnd_Parm_Racf_Id, WcType.BY) },
            new Oc[] { new Oc("EMPL_RACF_ID_KEY", "ASC") },
            1
            );
            FIND_EMPL:
            while (condition(vw_cwf_Org_Empl_Tbl.readNextRow("FIND_EMPL")))
            {
                if (condition(cwf_Org_Empl_Tbl_Empl_Racf_Id.greater(pnd_Parm_Racf_Id)))                                                                                   //Natural: IF CWF-ORG-EMPL-TBL.EMPL-RACF-ID GT #PARM-RACF-ID
                {
                    pnd_Parm_Empl_Nme.setValue(pnd_Parm_Racf_Id);                                                                                                         //Natural: MOVE #PARM-RACF-ID TO #PARM-EMPL-NME
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cwf_Org_Empl_Tbl_Dlte_Dte_Tme.greater(getZero())))                                                                                          //Natural: IF CWF-ORG-EMPL-TBL.DLTE-DTE-TME GT 0
                {
                    pnd_Parm_Empl_Nme.setValue(DbsUtil.compress("*", cwf_Org_Empl_Tbl_Empl_First_Nme, cwf_Org_Empl_Tbl_Empl_Last_Nme));                                   //Natural: COMPRESS '*' EMPL-FIRST-NME EMPL-LAST-NME INTO #PARM-EMPL-NME
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Parm_Empl_Nme.setValue(DbsUtil.compress(cwf_Org_Empl_Tbl_Empl_First_Nme, cwf_Org_Empl_Tbl_Empl_Last_Nme));                                        //Natural: COMPRESS EMPL-FIRST-NME EMPL-LAST-NME INTO #PARM-EMPL-NME
                }                                                                                                                                                         //Natural: END-IF
                pnd_Parm_Site_Cde.setValue(cwf_Org_Empl_Tbl_Empl_Loc);                                                                                                    //Natural: MOVE EMPL-LOC TO #PARM-SITE-CDE
                //*  FIND-EMPL.
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Search_Key2_Pnd_Unit_Cde.setValue(pnd_Parm_Empl_Nme_Pnd_Parm_Unit_Cde);                                                                                   //Natural: MOVE #PARM-UNIT-CDE TO #SEARCH-KEY2.#UNIT-CDE
            if (condition(pnd_Parm_Racf_Id.greater(" ")))                                                                                                                 //Natural: IF #PARM-RACF-ID GT ' '
            {
                vw_cwf_Org_Empl_Tbl.startDatabaseRead                                                                                                                     //Natural: READ ( 1 ) CWF-ORG-EMPL-TBL BY UNIT-EMPL-RACF-ID-KEY FROM #SEARCH-KEY2
                (
                "FIND_UNIT_EMPL",
                new Wc[] { new Wc("UNIT_EMPL_RACF_ID_KEY", ">=", pnd_Search_Key2, WcType.BY) },
                new Oc[] { new Oc("UNIT_EMPL_RACF_ID_KEY", "ASC") },
                1
                );
                FIND_UNIT_EMPL:
                while (condition(vw_cwf_Org_Empl_Tbl.readNextRow("FIND_UNIT_EMPL")))
                {
                    if (condition(cwf_Org_Empl_Tbl_Empl_Unit_Cde.greater(pnd_Search_Key2_Pnd_Unit_Cde) || cwf_Org_Empl_Tbl_Empl_Racf_Id.greater(pnd_Search_Key2_Pnd_Racf_Id))) //Natural: IF CWF-ORG-EMPL-TBL.EMPL-UNIT-CDE GT #SEARCH-KEY2.#UNIT-CDE OR CWF-ORG-EMPL-TBL.EMPL-RACF-ID GT #SEARCH-KEY2.#RACF-ID
                    {
                        pnd_Parm_Empl_Nme.setValue(pnd_Parm_Racf_Id);                                                                                                     //Natural: MOVE #PARM-RACF-ID TO #PARM-EMPL-NME
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cwf_Org_Empl_Tbl_Dlte_Dte_Tme.greater(getZero())))                                                                                      //Natural: IF CWF-ORG-EMPL-TBL.DLTE-DTE-TME GT 0
                    {
                        pnd_Parm_Empl_Nme.setValue(DbsUtil.compress("*", cwf_Org_Empl_Tbl_Empl_First_Nme, cwf_Org_Empl_Tbl_Empl_Last_Nme));                               //Natural: COMPRESS '*' EMPL-FIRST-NME EMPL-LAST-NME INTO #PARM-EMPL-NME
                        pnd_Parm_Site_Cde.setValue(cwf_Org_Empl_Tbl_Empl_Loc);                                                                                            //Natural: MOVE EMPL-LOC TO #PARM-SITE-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Parm_Empl_Nme.setValue(DbsUtil.compress(cwf_Org_Empl_Tbl_Empl_First_Nme, cwf_Org_Empl_Tbl_Empl_Last_Nme));                                    //Natural: COMPRESS EMPL-FIRST-NME EMPL-LAST-NME INTO #PARM-EMPL-NME
                        pnd_Parm_Site_Cde.setValue(cwf_Org_Empl_Tbl_Empl_Loc);                                                                                            //Natural: MOVE EMPL-LOC TO #PARM-SITE-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FIND-UNIT-EMPL.
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Parm_Empl_Nme.setValue("UNASSIGNED");                                                                                                                 //Natural: MOVE 'UNASSIGNED' TO #PARM-EMPL-NME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
