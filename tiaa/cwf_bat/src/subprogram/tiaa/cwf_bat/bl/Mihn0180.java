/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:31:00 AM
**        * FROM NATURAL SUBPROGRAM : Mihn0180
************************************************************
**        * FILE NAME            : Mihn0180.java
**        * CLASS NAME           : Mihn0180
**        * INSTANCE NAME        : Mihn0180
************************************************************
************************************************************************
* PROGRAM  : MIHN0180
* SYSTEM   :
* TITLE    : SET UP RUN CONTROL FOR THE MIT PURGE PROCESSES
* GENERATED: JAN 31,96 AT 15:01 PM
* FUNCTION : THIS COMMON ROUTINE CAN BE CALLED BY THE PURGE PROCESS
*          : TO DO THE INITIAL SET UP OF THE RUN CONTROL RECORD.
*
* CHANGES  : 09/19/96 - IF END-DATE LESS THAN START-DATE - DO NOT CREATE
*          : A CONTROL RECORD. ESCAPE PROGRAM WITH AN ERROR.
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mihn0180 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaMiha0180 pdaMiha0180;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaMihl0180 ldaMihl0180;
    private LdaMihl0181 ldaMihl0181;
    private LdaMihl0110 ldaMihl0110;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Who_Support_Tbl_Histogram;
    private DbsField cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key;
    private DbsField pnd_Cut_Off_Date_D;
    private DbsField pnd_Cut_Off_Date;

    private DbsGroup pnd_Cut_Off_Date__R_Field_1;
    private DbsField pnd_Cut_Off_Date_Pnd_Alpha;
    private DbsField pnd_Last_Run_Isn;
    private DbsField pnd_New_Run_Id;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_2;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_3;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key_From;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_4;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_5;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key_To;

    private DbsGroup pnd_Tbl_Prime_Key_To__R_Field_6;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_To__R_Field_7;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Work_Rqst_Retaining_Days;
    private DbsField pnd_Start_Date_Alpha;

    private DbsGroup pnd_Start_Date_Alpha__R_Field_8;
    private DbsField pnd_Start_Date_Alpha_Pnd_Start_Date_Numeric;
    private DbsField pnd_Start_Date_D;
    private DbsField pnd_End_Dte;
    private DbsField pnd_Strt_Dte;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaMihl0180 = new LdaMihl0180();
        registerRecord(ldaMihl0180);
        registerRecord(ldaMihl0180.getVw_cwf_Who_Support_Tbl());
        ldaMihl0181 = new LdaMihl0181();
        registerRecord(ldaMihl0181);
        ldaMihl0110 = new LdaMihl0110();
        registerRecord(ldaMihl0110);
        registerRecord(ldaMihl0110.getVw_cwf_Who_Support_Tbl_Last_Run_Id());

        // parameters
        parameters = new DbsRecord();
        pdaMiha0180 = new PdaMiha0180(parameters);
        pdaCdaobj = new PdaCdaobj(parameters);
        pdaCwfpda_D = new PdaCwfpda_D(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pdaCwfpda_P = new PdaCwfpda_P(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Who_Support_Tbl_Histogram = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Support_Tbl_Histogram", "CWF-WHO-SUPPORT-TBL-HISTOGRAM"), 
            "CWF_WHO_SUPPORT_TBL", "CWF_WHO_SUPPORT");
        cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key = vw_cwf_Who_Support_Tbl_Histogram.getRecord().newFieldInGroup("cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key", 
            "TBL-PRIME-KEY", FieldType.STRING, 53, RepeatingFieldStrategy.None, "TBL_PRIME_KEY");
        cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key.setDdmHeader("RECORD KEY");
        cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Who_Support_Tbl_Histogram);

        pnd_Cut_Off_Date_D = localVariables.newFieldInRecord("pnd_Cut_Off_Date_D", "#CUT-OFF-DATE-D", FieldType.DATE);
        pnd_Cut_Off_Date = localVariables.newFieldInRecord("pnd_Cut_Off_Date", "#CUT-OFF-DATE", FieldType.NUMERIC, 8);

        pnd_Cut_Off_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Cut_Off_Date__R_Field_1", "REDEFINE", pnd_Cut_Off_Date);
        pnd_Cut_Off_Date_Pnd_Alpha = pnd_Cut_Off_Date__R_Field_1.newFieldInGroup("pnd_Cut_Off_Date_Pnd_Alpha", "#ALPHA", FieldType.STRING, 8);
        pnd_Last_Run_Isn = localVariables.newFieldInRecord("pnd_Last_Run_Isn", "#LAST-RUN-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_New_Run_Id = localVariables.newFieldInRecord("pnd_New_Run_Id", "#NEW-RUN-ID", FieldType.NUMERIC, 9);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_2", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);

        pnd_Tbl_Prime_Key__R_Field_3 = pnd_Tbl_Prime_Key__R_Field_2.newGroupInGroup("pnd_Tbl_Prime_Key__R_Field_3", "REDEFINE", pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_Pnd_Run_Status = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 8);
        pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field", "#REST-OF-TBL-KEY-FIELD", 
            FieldType.STRING, 21);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_From = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_From", "#TBL-PRIME-KEY-FROM", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_From__R_Field_4 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_From__R_Field_4", "REDEFINE", pnd_Tbl_Prime_Key_From);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_From__R_Field_5 = pnd_Tbl_Prime_Key_From__R_Field_4.newGroupInGroup("pnd_Tbl_Prime_Key_From__R_Field_5", "REDEFINE", pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status = pnd_Tbl_Prime_Key_From__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Run_Status", "#RUN-STATUS", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key_From__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 8);
        pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 21);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_From__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_To = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_To", "#TBL-PRIME-KEY-TO", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_To__R_Field_6 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_To__R_Field_6", "REDEFINE", pnd_Tbl_Prime_Key_To);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_To__R_Field_7 = pnd_Tbl_Prime_Key_To__R_Field_6.newGroupInGroup("pnd_Tbl_Prime_Key_To__R_Field_7", "REDEFINE", pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status = pnd_Tbl_Prime_Key_To__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key_To__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 8);
        pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_To__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 21);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_To__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Work_Rqst_Retaining_Days = localVariables.newFieldInRecord("pnd_Work_Rqst_Retaining_Days", "#WORK-RQST-RETAINING-DAYS", FieldType.NUMERIC, 
            3);
        pnd_Start_Date_Alpha = localVariables.newFieldInRecord("pnd_Start_Date_Alpha", "#START-DATE-ALPHA", FieldType.STRING, 8);

        pnd_Start_Date_Alpha__R_Field_8 = localVariables.newGroupInRecord("pnd_Start_Date_Alpha__R_Field_8", "REDEFINE", pnd_Start_Date_Alpha);
        pnd_Start_Date_Alpha_Pnd_Start_Date_Numeric = pnd_Start_Date_Alpha__R_Field_8.newFieldInGroup("pnd_Start_Date_Alpha_Pnd_Start_Date_Numeric", "#START-DATE-NUMERIC", 
            FieldType.NUMERIC, 8);
        pnd_Start_Date_D = localVariables.newFieldInRecord("pnd_Start_Date_D", "#START-DATE-D", FieldType.DATE);
        pnd_End_Dte = localVariables.newFieldInRecord("pnd_End_Dte", "#END-DTE", FieldType.STRING, 8);
        pnd_Strt_Dte = localVariables.newFieldInRecord("pnd_Strt_Dte", "#STRT-DTE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Who_Support_Tbl_Histogram.reset();

        ldaMihl0180.initializeValues();
        ldaMihl0181.initializeValues();
        ldaMihl0110.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Mihn0180() throws Exception
    {
        super("Mihn0180");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("MIHN0180", onError);
        //* (TO ALLOW EASY ESCAPE FROM THE ROUTINE)
        ROUTINE:                                                                                                                                                          //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM RESET-THE-OUTPUT-PARAMETERS
            sub_Reset_The_Output_Parameters();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM SET-UP-THE-RUN-CONTROL-RECORD
            sub_Set_Up_The_Run_Control_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break ROUTINE;                                                                                                                                      //Natural: ESCAPE BOTTOM ( ROUTINE. )
            //*  SUBROUTINE JUST FOR ENDING THIS SUBPROGRAM IMMEDIATELY
            //*  ------------------------------------------------------
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-SUBPROGRAM
            //* (ROUTINE.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  SUBROUTINES
        //*  ===========
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-THE-OUTPUT-PARAMETERS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-UP-THE-RUN-CONTROL-RECORD
        //*  =============================================
        //*  DETERMINE THE CUT-OFF DATE USING THE NUMBER OF RETAINING DAYS FOR A
        //*  WORK REQUEST, WHICH IS STORED AS A VARIABLE ON THE LAST-RUN-ID
        //*  RUN CONTROL RECORD.
        //*  ----------------------------------------------------------------------
        //*  CHECK TO SEE IF AN ACTIVE RECORD ALREADY EXISTS
        //*  -----------------------------------------------
        //*  IF AN ACTIVE RECORD IS FOUND, DO NOT POPULATE ANY RESTART PARAMETERS
        //*  RESET NUMBER OF WORK REQUESTS AND NUMBER OF EVENTS PURGED AND
        //*  RETURN TO THE CALLING PROGRAM.
        //*  ---------------------------------------------------------------------
        //*  CHECK TO SEE IF A PENDING RECORD ALREADY EXISTS
        //*  -----------------------------------------------
        //*  IF A PENDING RECORD IS FOUND, UPDATE THE RECORD TO ACTIVE,
        //*  POPULATE THE OUTPUT PARAMETERS AND RETURN TO THE CALLING PROGRAM
        //*  ----------------------------------------------------------------
        //*  THERE ARE NEITHER ACTIVE NOR PENDING CONTROL RECORDS IF THIS POINT IN
        //*  THE PROCESS IS REACHED.  CHECK THE CONTROL RECORD FOR THE LAST RUN ID
        //*  AND, IF THE FLAG THERE TO CONTROL AUTOMATIC RUN CONTROL RECORD
        //*  CREATION ALLOWS, CREATE A NEW ACTIVE RECORD.
        //*  ---------------------------------------------------------------------
        //*  FIRST ALLOCATE A NEW RUN ID
        //*  ---------------------------
        //*  LOOK UP THE MOST RECENT PERIOD ARCHIVED TO CREATE THE NEW RECORD
        //*  ----------------------------------------------------------------
        //*  USE THIS RECORD TO CREATE THE NEW ONE
        //*  -------------------------------------
        //* ********
        //*    NMBR-OF-TRY-CNTR(LAST-COMPLETED-READ.)
        //*  ON NATURAL RUN TIME ERROR, RETURN WITH AN APPROPRIATE ERROR MESSAGE
        //*  ===================================================================
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Subprogram() throws Exception                                                                                                                 //Natural: ESCAPE-SUBPROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( ROUTINE. )
        Global.setEscapeCode(EscapeType.Bottom, "ROUTINE");
        if (true) return;
        //* (0840)
    }
    private void sub_Reset_The_Output_Parameters() throws Exception                                                                                                       //Natural: RESET-THE-OUTPUT-PARAMETERS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===========================================
        pdaMiha0180.getMiha0180_Output().reset();                                                                                                                         //Natural: RESET MIHA0180-OUTPUT
        //* (0930)
    }
    private void sub_Set_Up_The_Run_Control_Record() throws Exception                                                                                                     //Natural: SET-UP-THE-RUN-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue(ldaMihl0181.getMihl0181_Tbl_Scrty_Level_Ind());                                                                //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND := MIHL0181.TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue(ldaMihl0181.getMihl0181_Mit_Purge_Last_Run_Table_Name());                                                            //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME := MIHL0181.MIT-PURGE-LAST-RUN-TABLE-NAME
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue(ldaMihl0181.getMihl0181_Purge_Last_Run_Tbl_Key_Field());                                                             //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-KEY-FIELD := MIHL0181.PURGE-LAST-RUN-TBL-KEY-FIELD
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind.setValue(ldaMihl0181.getMihl0181_Prime_Key_Tbl_Actve_Ind());                                                                  //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-ACTVE-IND := MIHL0181.PRIME-KEY-TBL-ACTVE-IND
        ldaMihl0110.getVw_cwf_Who_Support_Tbl_Last_Run_Id().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) CWF-WHO-SUPPORT-TBL-LAST-RUN-ID WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "LAST_RUN_ID_FIND",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        LAST_RUN_ID_FIND:
        while (condition(ldaMihl0110.getVw_cwf_Who_Support_Tbl_Last_Run_Id().readNextRow("LAST_RUN_ID_FIND")))
        {
            ldaMihl0110.getVw_cwf_Who_Support_Tbl_Last_Run_Id().setIfNotFoundControlFlag(false);
            pnd_Last_Run_Isn.setValue(ldaMihl0110.getVw_cwf_Who_Support_Tbl_Last_Run_Id().getAstISN("LAST_RUN_ID_FIND"));                                                 //Natural: ASSIGN #LAST-RUN-ISN := *ISN ( LAST-RUN-ID-FIND. )
            pnd_Work_Rqst_Retaining_Days.setValue(ldaMihl0110.getCwf_Who_Support_Tbl_Last_Run_Id_Work_Rqst_Retaining_Days());                                             //Natural: ASSIGN #WORK-RQST-RETAINING-DAYS := CWF-WHO-SUPPORT-TBL-LAST-RUN-ID.WORK-RQST-RETAINING-DAYS
            pnd_Cut_Off_Date_D.compute(new ComputeParameters(false, pnd_Cut_Off_Date_D), Global.getDATX().subtract(pnd_Work_Rqst_Retaining_Days));                        //Natural: COMPUTE #CUT-OFF-DATE-D = *DATX - #WORK-RQST-RETAINING-DAYS
            pnd_Cut_Off_Date_Pnd_Alpha.setValueEdited(pnd_Cut_Off_Date_D,new ReportEditMask("YYYYMMDD"));                                                                 //Natural: MOVE EDITED #CUT-OFF-DATE-D ( EM = YYYYMMDD ) TO #CUT-OFF-DATE.#ALPHA
            //* (LAST-RUN-ID-FIND.)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(ldaMihl0110.getVw_cwf_Who_Support_Tbl_Last_Run_Id().getAstCOUNTER().equals(getZero())))                                                             //Natural: IF *COUNTER ( LAST-RUN-ID-FIND. ) = 0
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(26);                                                                                                    //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 26
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Last-Run-Id control record not found"));               //Natural: COMPRESS 'Program' *PROGRAM '- Last-Run-Id control record not found' INTO MSG-INFO-SUB.##MSG
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape())) {return;}
            //* (1200)
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind.setValue(ldaMihl0181.getMihl0181_Tbl_Scrty_Level_Ind());                                                           //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-SCRTY-LEVEL-IND := MIHL0181.TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme.setValue(ldaMihl0181.getMihl0181_Mit_Purge_Job_Type());                                                                  //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-TABLE-NME := MIHL0181.MIT-PURGE-JOB-TYPE
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status.setValue(ldaMihl0181.getMihl0181_Active_Status());                                                                          //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#RUN-STATUS := MIHL0181.ACTIVE-STATUS
        pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement.setValue(0);                                                                                               //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#STARTING-DATE-9S-COMPLEMENT := 0
        pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field.setValue(" ");                                                                                                   //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#REST-OF-TBL-KEY-FIELD := ' '
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind.setValue(" ");                                                                                                           //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-ACTVE-IND := ' '
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind.setValue(ldaMihl0181.getMihl0181_Tbl_Scrty_Level_Ind());                                                             //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-SCRTY-LEVEL-IND := MIHL0181.TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme.setValue(ldaMihl0181.getMihl0181_Mit_Purge_Job_Type());                                                                    //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-TABLE-NME := MIHL0181.MIT-PURGE-JOB-TYPE
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status.setValue(ldaMihl0181.getMihl0181_Active_Status());                                                                            //Natural: ASSIGN #TBL-PRIME-KEY-TO.#RUN-STATUS := MIHL0181.ACTIVE-STATUS
        pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement.setValue(99999999);                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY-TO.#STARTING-DATE-9S-COMPLEMENT := 99999999
        pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field.setValue("999999999999999999999");                                                                                 //Natural: ASSIGN #TBL-PRIME-KEY-TO.#REST-OF-TBL-KEY-FIELD := '999999999999999999999'
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind.setValue("9");                                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-ACTVE-IND := '9'
        vw_cwf_Who_Support_Tbl_Histogram.createHistogram                                                                                                                  //Natural: HISTOGRAM CWF-WHO-SUPPORT-TBL-HISTOGRAM FOR TBL-PRIME-KEY FROM #TBL-PRIME-KEY-FROM THRU #TBL-PRIME-KEY-TO
        (
        "ACTIVE_HISTOGRAM",
        "TBL_PRIME_KEY",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key_From, "And", WcType.WITH) ,
        new Wc("TBL_PRIME_KEY", "<=", pnd_Tbl_Prime_Key_To, WcType.WITH) }
        );
        ACTIVE_HISTOGRAM:
        while (condition(vw_cwf_Who_Support_Tbl_Histogram.readNextRow("ACTIVE_HISTOGRAM")))
        {
            if (condition(vw_cwf_Who_Support_Tbl_Histogram.getAstNUMBER().greater(getZero())))                                                                            //Natural: IF *NUMBER ( ACTIVE-HISTOGRAM. ) GT 0
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(23);                                                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 23
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Cycle Not finished, Control Active,",              //Natural: COMPRESS 'Program' *PROGRAM '- Cycle Not finished, Control Active,' ' Check P9983CWD' INTO MSG-INFO-SUB.##MSG
                    " Check P9983CWD"));
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* (1530)
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tbl_Prime_Key.setValue(cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key);                                                                                      //Natural: ASSIGN #TBL-PRIME-KEY := CWF-WHO-SUPPORT-TBL-HISTOGRAM.TBL-PRIME-KEY
            ldaMihl0180.getVw_cwf_Who_Support_Tbl().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) CWF-WHO-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
            (
            "ACTIVE_FIND",
            new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
            1
            );
            ACTIVE_FIND:
            while (condition(ldaMihl0180.getVw_cwf_Who_Support_Tbl().readNextRow("ACTIVE_FIND")))
            {
                ldaMihl0180.getVw_cwf_Who_Support_Tbl().setIfNotFoundControlFlag(false);
                pdaMiha0180.getMiha0180_Output_Run_Control_Isn().setValue(ldaMihl0180.getVw_cwf_Who_Support_Tbl().getAstISN("ACTIVE_FIND"));                              //Natural: ASSIGN MIHA0180-OUTPUT.RUN-CONTROL-ISN := *ISN ( ACTIVE-FIND. )
                ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Work_Requests_Purged().setValue(0);                                                                          //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.NUMBER-OF-WORK-REQUESTS-PURGED := 0
                ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Events_Purged().setValue(0);                                                                                 //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.NUMBER-OF-EVENTS-PURGED := 0
                ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Isn_Listed().setValue(0);                                                                                    //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.NUMBER-OF-ISN-LISTED := 0
                //*  CHECK THAT THE ENDING DATE IS LESS THAN OR EQUAL TO THE CUT-OFF-DATE.
                //*  IF NOT, UPDATE THE ENDING DATE TO THE CUT-OFF-DATE SO THAT THE CUT OFF
                //*  PERIOD IS NOT COMPROMISED.
                //*  ----------------------------------------------------------------------
                if (condition(ldaMihl0180.getCwf_Who_Support_Tbl_Ending_Date().greater(pnd_Cut_Off_Date)))                                                                //Natural: IF CWF-WHO-SUPPORT-TBL.ENDING-DATE GT #CUT-OFF-DATE
                {
                    ldaMihl0180.getCwf_Who_Support_Tbl_Ending_Date().setValue(pnd_Cut_Off_Date);                                                                          //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.ENDING-DATE := #CUT-OFF-DATE
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(27);                                                                                            //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 27
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Ending Date modified to conform to",           //Natural: COMPRESS 'Program' *PROGRAM '- Ending Date modified to conform to' 'Cut-Off Date' INTO MSG-INFO-SUB.##MSG
                        "Cut-Off Date"));
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("W");                                                                                      //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'W'
                }                                                                                                                                                         //Natural: END-IF
                ldaMihl0180.getVw_cwf_Who_Support_Tbl().updateDBRow("ACTIVE_FIND");                                                                                       //Natural: UPDATE ( ACTIVE-FIND. )
                //* (ACTIVE-FIND.)
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* (ACTIVE-HISTOGRAM.)
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        if (condition(vw_cwf_Who_Support_Tbl_Histogram.getAstCOUNTER().greater(1)))                                                                                       //Natural: IF *COUNTER ( ACTIVE-HISTOGRAM. ) GT 1
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(23);                                                                                                    //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 23
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- More than one active control records",                 //Natural: COMPRESS 'Program' *PROGRAM '- More than one active control records' 'exist' INTO MSG-INFO-SUB.##MSG
                "exist"));
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape())) {return;}
            //* (1930)
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status.setValue(ldaMihl0181.getMihl0181_Pending_Status());                                                                         //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#RUN-STATUS := MIHL0181.PENDING-STATUS
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status.setValue(ldaMihl0181.getMihl0181_Pending_Status());                                                                           //Natural: ASSIGN #TBL-PRIME-KEY-TO.#RUN-STATUS := MIHL0181.PENDING-STATUS
        //*  (THE REST OF THE KEY SET UP VARIABLES ARE ALREADY SET AS A RESULT OF
        //*  THE ACTIVE STATUS CHECK ABOVE)
        vw_cwf_Who_Support_Tbl_Histogram.createHistogram                                                                                                                  //Natural: HISTOGRAM CWF-WHO-SUPPORT-TBL-HISTOGRAM FOR TBL-PRIME-KEY FROM #TBL-PRIME-KEY-FROM THRU #TBL-PRIME-KEY-TO
        (
        "PENDING_HISTOGRAM",
        "TBL_PRIME_KEY",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key_From, "And", WcType.WITH) ,
        new Wc("TBL_PRIME_KEY", "<=", pnd_Tbl_Prime_Key_To, WcType.WITH) }
        );
        PENDING_HISTOGRAM:
        while (condition(vw_cwf_Who_Support_Tbl_Histogram.readNextRow("PENDING_HISTOGRAM")))
        {
            pnd_Tbl_Prime_Key.setValue(cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key);                                                                                      //Natural: ASSIGN #TBL-PRIME-KEY := CWF-WHO-SUPPORT-TBL-HISTOGRAM.TBL-PRIME-KEY
            ldaMihl0180.getVw_cwf_Who_Support_Tbl().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) CWF-WHO-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
            (
            "PENDING_FIND",
            new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
            1
            );
            PENDING_FIND:
            while (condition(ldaMihl0180.getVw_cwf_Who_Support_Tbl().readNextRow("PENDING_FIND")))
            {
                ldaMihl0180.getVw_cwf_Who_Support_Tbl().setIfNotFoundControlFlag(false);
                ldaMihl0180.getCwf_Who_Support_Tbl_Run_Status().setValue(ldaMihl0181.getMihl0181_Active_Status());                                                        //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.RUN-STATUS := MIHL0181.ACTIVE-STATUS
                ldaMihl0180.getCwf_Who_Support_Tbl_Tbl_Updte_Dte().setValue(Global.getDATX());                                                                            //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.TBL-UPDTE-DTE := *DATX
                //*  CHECK THAT THE ENDING DATE IS LESS THAN OR EQUAL TO THE CUT-OFF-DATE.
                //*  IF NOT, UPDATE THE ENDING DATE TO THE CUT-OFF-DATE SO THAT THE CUT OFF
                //*  PERIOD IS NOT COMPROMISED.
                //*  ----------------------------------------------------------------------
                if (condition(ldaMihl0180.getCwf_Who_Support_Tbl_Ending_Date().greater(pnd_Cut_Off_Date)))                                                                //Natural: IF CWF-WHO-SUPPORT-TBL.ENDING-DATE GT #CUT-OFF-DATE
                {
                    ldaMihl0180.getCwf_Who_Support_Tbl_Ending_Date().setValue(pnd_Cut_Off_Date);                                                                          //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.ENDING-DATE := #CUT-OFF-DATE
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(27);                                                                                            //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 27
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Ending Date modified to conform to",           //Natural: COMPRESS 'Program' *PROGRAM '- Ending Date modified to conform to' 'Cut-Off Date' INTO MSG-INFO-SUB.##MSG
                        "Cut-Off Date"));
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("W");                                                                                      //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'W'
                }                                                                                                                                                         //Natural: END-IF
                ldaMihl0180.getVw_cwf_Who_Support_Tbl().updateDBRow("PENDING_FIND");                                                                                      //Natural: UPDATE ( PENDING-FIND. )
                pdaMiha0180.getMiha0180_Output_Run_Control_Isn().setValue(ldaMihl0180.getVw_cwf_Who_Support_Tbl().getAstISN("PENDING_FIND"));                             //Natural: ASSIGN MIHA0180-OUTPUT.RUN-CONTROL-ISN := *ISN ( PENDING-FIND. )
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PENDING_FIND"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PENDING_FIND"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* (PENDING-FIND.)
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PENDING_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PENDING_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* (PENDING-HISTOGRAM.)
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        LAST_RUN_ID_GET:                                                                                                                                                  //Natural: GET CWF-WHO-SUPPORT-TBL-LAST-RUN-ID #LAST-RUN-ISN
        ldaMihl0110.getVw_cwf_Who_Support_Tbl_Last_Run_Id().readByID(pnd_Last_Run_Isn.getLong(), "LAST_RUN_ID_GET");
        if (condition(ldaMihl0110.getCwf_Who_Support_Tbl_Last_Run_Id_Enable_Auto_Run_Control_Creation().equals(false) || ldaMihl0110.getCwf_Who_Support_Tbl_Last_Run_Id_Last_Run_Id().greater(999999998))) //Natural: IF CWF-WHO-SUPPORT-TBL-LAST-RUN-ID.ENABLE-AUTO-RUN-CONTROL-CREATION = FALSE OR CWF-WHO-SUPPORT-TBL-LAST-RUN-ID.LAST-RUN-ID GT 999999998
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(25);                                                                                                    //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 25
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Automatic run control creation denied"));              //Natural: COMPRESS 'Program' *PROGRAM '- Automatic run control creation denied' INTO MSG-INFO-SUB.##MSG
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape())) {return;}
            //* (2540)
        }                                                                                                                                                                 //Natural: END-IF
        ldaMihl0110.getCwf_Who_Support_Tbl_Last_Run_Id_Last_Run_Id().nadd(1);                                                                                             //Natural: ADD 1 TO CWF-WHO-SUPPORT-TBL-LAST-RUN-ID.LAST-RUN-ID
        ldaMihl0110.getVw_cwf_Who_Support_Tbl_Last_Run_Id().updateDBRow("LAST_RUN_ID_GET");                                                                               //Natural: UPDATE ( LAST-RUN-ID-GET. )
        pnd_New_Run_Id.setValue(ldaMihl0110.getCwf_Who_Support_Tbl_Last_Run_Id_Last_Run_Id());                                                                            //Natural: ASSIGN #NEW-RUN-ID := CWF-WHO-SUPPORT-TBL-LAST-RUN-ID.LAST-RUN-ID
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue(ldaMihl0181.getMihl0181_Tbl_Scrty_Level_Ind());                                                                //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND := MIHL0181.TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue(ldaMihl0181.getMihl0181_Mit_Purge_Job_Type());                                                                       //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME := MIHL0181.MIT-PURGE-JOB-TYPE
        pnd_Tbl_Prime_Key_Pnd_Run_Status.setValue(ldaMihl0181.getMihl0181_Completed_Status());                                                                            //Natural: ASSIGN #TBL-PRIME-KEY.#RUN-STATUS := MIHL0181.COMPLETED-STATUS
        pnd_Tbl_Prime_Key_Pnd_Starting_Date_9s_Complement.reset();                                                                                                        //Natural: RESET #TBL-PRIME-KEY.#STARTING-DATE-9S-COMPLEMENT #TBL-PRIME-KEY.#REST-OF-TBL-KEY-FIELD #TBL-PRIME-KEY.#TBL-ACTVE-IND
        pnd_Tbl_Prime_Key_Pnd_Rest_Of_Tbl_Key_Field.reset();
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind.reset();
        ldaMihl0180.getVw_cwf_Who_Support_Tbl().startDatabaseRead                                                                                                         //Natural: READ ( 1 ) CWF-WHO-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "LAST_COMPLETED_READ",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        LAST_COMPLETED_READ:
        while (condition(ldaMihl0180.getVw_cwf_Who_Support_Tbl().readNextRow("LAST_COMPLETED_READ")))
        {
            if (condition(ldaMihl0180.getCwf_Who_Support_Tbl_Tbl_Scrty_Level_Ind().notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind) || ldaMihl0180.getCwf_Who_Support_Tbl_Tbl_Table_Nme().notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)  //Natural: IF CWF-WHO-SUPPORT-TBL.TBL-SCRTY-LEVEL-IND NE #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND OR CWF-WHO-SUPPORT-TBL.TBL-TABLE-NME NE #TBL-PRIME-KEY.#TBL-TABLE-NME OR CWF-WHO-SUPPORT-TBL.RUN-STATUS NE #TBL-PRIME-KEY.#RUN-STATUS
                || ldaMihl0180.getCwf_Who_Support_Tbl_Run_Status().notEquals(pnd_Tbl_Prime_Key_Pnd_Run_Status)))
            {
                ldaMihl0180.getVw_cwf_Who_Support_Tbl().getAstCOUNTER().nsubtract(1);                                                                                     //Natural: SUBTRACT 1 FROM *COUNTER ( LAST-COMPLETED-READ. )
                if (true) break LAST_COMPLETED_READ;                                                                                                                      //Natural: ESCAPE BOTTOM ( LAST-COMPLETED-READ. )
                //* (2820)
            }                                                                                                                                                             //Natural: END-IF
            ldaMihl0180.getCwf_Who_Support_Tbl_Run_Status().setValue(ldaMihl0181.getMihl0181_Active_Status());                                                            //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.RUN-STATUS := MIHL0181.ACTIVE-STATUS
            ldaMihl0180.getCwf_Who_Support_Tbl_Starting_Date().setValue(ldaMihl0180.getCwf_Who_Support_Tbl_Ending_Date());                                                //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.STARTING-DATE := CWF-WHO-SUPPORT-TBL.ENDING-DATE
            //* ******** CHANGE ADDED BY L.E. 08/06/96
            pnd_Start_Date_Alpha.setValue(ldaMihl0180.getCwf_Who_Support_Tbl_Starting_Date());                                                                            //Natural: MOVE CWF-WHO-SUPPORT-TBL.STARTING-DATE TO #START-DATE-ALPHA
            pnd_Start_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Start_Date_Alpha);                                                                         //Natural: MOVE EDITED #START-DATE-ALPHA TO #START-DATE-D ( EM = YYYYMMDD )
            pnd_Start_Date_D.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #START-DATE-D
            pnd_Start_Date_Alpha.setValueEdited(pnd_Start_Date_D,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED #START-DATE-D ( EM = YYYYMMDD ) TO #START-DATE-ALPHA
            ldaMihl0180.getCwf_Who_Support_Tbl_Starting_Date().setValue(pnd_Start_Date_Alpha_Pnd_Start_Date_Numeric);                                                     //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.STARTING-DATE := #START-DATE-NUMERIC
            ldaMihl0180.getCwf_Who_Support_Tbl_Ending_Date().setValue(pnd_Cut_Off_Date);                                                                                  //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.ENDING-DATE := #CUT-OFF-DATE
            ldaMihl0180.getCwf_Who_Support_Tbl_Starting_Date_9s_Complement().compute(new ComputeParameters(false, ldaMihl0180.getCwf_Who_Support_Tbl_Starting_Date_9s_Complement()),  //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.STARTING-DATE-9S-COMPLEMENT := 99999999 - CWF-WHO-SUPPORT-TBL.STARTING-DATE
                DbsField.subtract(99999999,ldaMihl0180.getCwf_Who_Support_Tbl_Starting_Date()));
            ldaMihl0180.getCwf_Who_Support_Tbl_Run_Id().setValue(pnd_New_Run_Id);                                                                                         //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.RUN-ID := #NEW-RUN-ID
            ldaMihl0180.getCwf_Who_Support_Tbl_Tbl_Entry_Dte_Tme().setValue(Global.getTIMX());                                                                            //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.TBL-ENTRY-DTE-TME := *TIMX
            ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Work_Requests_Purged().reset();                                                                                  //Natural: RESET CWF-WHO-SUPPORT-TBL.NUMBER-OF-WORK-REQUESTS-PURGED CWF-WHO-SUPPORT-TBL.NUMBER-OF-EVENTS-PURGED CWF-WHO-SUPPORT-TBL.NUMBER-OF-ISN-LISTED CWF-WHO-SUPPORT-TBL.NUMBER-OF-ISN-DELETED CWF-WHO-SUPPORT-TBL.LAST-ISN-DELETED CWF-WHO-SUPPORT-TBL.FRST-ISN-OF-BLCK CWF-WHO-SUPPORT-TBL.RUN-DATE CWF-WHO-SUPPORT-TBL.TBL-ENTRY-OPRTR-CDE CWF-WHO-SUPPORT-TBL.TBL-UPDTE-DTE-TME CWF-WHO-SUPPORT-TBL.TBL-UPDTE-DTE CWF-WHO-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE CWF-WHO-SUPPORT-TBL.TBL-DLTE-DTE-TME CWF-WHO-SUPPORT-TBL.TBL-DLTE-OPRTR-CDE
            ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Events_Purged().reset();
            ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Isn_Listed().reset();
            ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Isn_Deleted().reset();
            ldaMihl0180.getCwf_Who_Support_Tbl_Last_Isn_Deleted().reset();
            ldaMihl0180.getCwf_Who_Support_Tbl_Frst_Isn_Of_Blck().reset();
            ldaMihl0180.getCwf_Who_Support_Tbl_Run_Date().reset();
            ldaMihl0180.getCwf_Who_Support_Tbl_Tbl_Entry_Oprtr_Cde().reset();
            ldaMihl0180.getCwf_Who_Support_Tbl_Tbl_Updte_Dte_Tme().reset();
            ldaMihl0180.getCwf_Who_Support_Tbl_Tbl_Updte_Dte().reset();
            ldaMihl0180.getCwf_Who_Support_Tbl_Tbl_Updte_Oprtr_Cde().reset();
            ldaMihl0180.getCwf_Who_Support_Tbl_Tbl_Dlte_Dte_Tme().reset();
            ldaMihl0180.getCwf_Who_Support_Tbl_Tbl_Dlte_Oprtr_Cde().reset();
            //*  ******* ADDED BY L.E. 09/19/96
            //*  CHECK IF ENDING-DATE LT STARTING-DATE. IF IT IS - DO NOT CREATE
            //*  A NEW CONTROL RECORD - ESCAPE PROGRAM WITH AN ERROR.
            //*  --------------------------------------------------------------
            if (condition(ldaMihl0180.getCwf_Who_Support_Tbl_Ending_Date().less(ldaMihl0180.getCwf_Who_Support_Tbl_Starting_Date())))                                     //Natural: IF CWF-WHO-SUPPORT-TBL.ENDING-DATE LT CWF-WHO-SUPPORT-TBL.STARTING-DATE
            {
                pnd_End_Dte.setValue(ldaMihl0180.getCwf_Who_Support_Tbl_Ending_Date());                                                                                   //Natural: ASSIGN #END-DTE := CWF-WHO-SUPPORT-TBL.ENDING-DATE
                pnd_Strt_Dte.setValue(ldaMihl0180.getCwf_Who_Support_Tbl_Starting_Date());                                                                                //Natural: ASSIGN #STRT-DTE := CWF-WHO-SUPPORT-TBL.STARTING-DATE
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- End-Date", pnd_End_Dte, "LT Start-Date",           //Natural: COMPRESS 'Program' *PROGRAM '- End-Date' #END-DTE 'LT Start-Date' #STRT-DTE ', Do Not Run Purge' INTO MSG-INFO-SUB.##MSG
                    pnd_Strt_Dte, ", Do Not Run Purge"));
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LAST_COMPLETED_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LAST_COMPLETED_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                CREATED_CONTROL_RECORD:                                                                                                                                   //Natural: STORE CWF-WHO-SUPPORT-TBL
                ldaMihl0180.getVw_cwf_Who_Support_Tbl().insertDBRow("CREATED_CONTROL_RECORD");
                pdaMiha0180.getMiha0180_Output_Run_Control_Isn().setValue(ldaMihl0180.getVw_cwf_Who_Support_Tbl().getAstISN("LAST_COMPLETED_READ"));                      //Natural: ASSIGN MIHA0180-OUTPUT.RUN-CONTROL-ISN := *ISN ( CREATED-CONTROL-RECORD. )
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
                sub_Escape_Subprogram();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LAST_COMPLETED_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LAST_COMPLETED_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* (LAST-COMPLETED-READ.)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaMihl0180.getVw_cwf_Who_Support_Tbl().getAstCOUNTER().equals(getZero())))                                                                         //Natural: IF *COUNTER ( LAST-COMPLETED-READ. ) = 0
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(25);                                                                                                    //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR := 25
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Program", Global.getPROGRAM(), "- Automatic run control creation denied"));              //Natural: COMPRESS 'Program' *PROGRAM '- Automatic run control creation denied' INTO MSG-INFO-SUB.##MSG
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                                                                                                                                                                          //Natural: PERFORM ESCAPE-SUBPROGRAM
            sub_Escape_Subprogram();
            if (condition(Global.isEscape())) {return;}
            //* (3480)
        }                                                                                                                                                                 //Natural: END-IF
        //* (0990)
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(999);                                                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG-NR = 999
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("NATURAL ERROR", Global.getERROR_NR(), "IN", Global.getPROGRAM(), "....LINE",                 //Natural: COMPRESS 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE INTO MSG-INFO-SUB.##MSG
            Global.getERROR_LINE()));
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
        //* (3610)
    };                                                                                                                                                                    //Natural: END-ERROR
}
