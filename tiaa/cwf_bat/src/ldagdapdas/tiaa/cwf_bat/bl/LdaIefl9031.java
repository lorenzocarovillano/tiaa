/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:05:56 PM
**        * FROM NATURAL LDA     : IEFL9031
************************************************************
**        * FILE NAME            : LdaIefl9031.java
**        * CLASS NAME           : LdaIefl9031
**        * INSTANCE NAME        : LdaIefl9031
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIefl9031 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_icw_Efm_Stats;
    private DbsField icw_Efm_Stats_Log_Dte;
    private DbsField icw_Efm_Stats_System_Cde;
    private DbsField icw_Efm_Stats_Count_Castupdate_Counts;
    private DbsGroup icw_Efm_Stats_Update_Counts;
    private DbsField icw_Efm_Stats_Action_Cde;
    private DbsField icw_Efm_Stats_Xtn_Cnt;
    private DbsField icw_Efm_Stats_Mit_Added_Cnt;
    private DbsField icw_Efm_Stats_Mit_Updated_Cnt;
    private DbsField icw_Efm_Stats_Dcmnt_Added_Cnt;
    private DbsField icw_Efm_Stats_Dcmnt_Renamed_Cnt;

    public DataAccessProgramView getVw_icw_Efm_Stats() { return vw_icw_Efm_Stats; }

    public DbsField getIcw_Efm_Stats_Log_Dte() { return icw_Efm_Stats_Log_Dte; }

    public DbsField getIcw_Efm_Stats_System_Cde() { return icw_Efm_Stats_System_Cde; }

    public DbsField getIcw_Efm_Stats_Count_Castupdate_Counts() { return icw_Efm_Stats_Count_Castupdate_Counts; }

    public DbsGroup getIcw_Efm_Stats_Update_Counts() { return icw_Efm_Stats_Update_Counts; }

    public DbsField getIcw_Efm_Stats_Action_Cde() { return icw_Efm_Stats_Action_Cde; }

    public DbsField getIcw_Efm_Stats_Xtn_Cnt() { return icw_Efm_Stats_Xtn_Cnt; }

    public DbsField getIcw_Efm_Stats_Mit_Added_Cnt() { return icw_Efm_Stats_Mit_Added_Cnt; }

    public DbsField getIcw_Efm_Stats_Mit_Updated_Cnt() { return icw_Efm_Stats_Mit_Updated_Cnt; }

    public DbsField getIcw_Efm_Stats_Dcmnt_Added_Cnt() { return icw_Efm_Stats_Dcmnt_Added_Cnt; }

    public DbsField getIcw_Efm_Stats_Dcmnt_Renamed_Cnt() { return icw_Efm_Stats_Dcmnt_Renamed_Cnt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_icw_Efm_Stats = new DataAccessProgramView(new NameInfo("vw_icw_Efm_Stats", "ICW-EFM-STATS"), "ICW_EFM_STATS", "ICW_EFM_STATS", DdmPeriodicGroups.getInstance().getGroups("ICW_EFM_STATS"));
        icw_Efm_Stats_Log_Dte = vw_icw_Efm_Stats.getRecord().newFieldInGroup("icw_Efm_Stats_Log_Dte", "LOG-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LOG_DTE");
        icw_Efm_Stats_Log_Dte.setDdmHeader("LOG/DATE");
        icw_Efm_Stats_System_Cde = vw_icw_Efm_Stats.getRecord().newFieldInGroup("icw_Efm_Stats_System_Cde", "SYSTEM-CDE", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTEM_CDE");
        icw_Efm_Stats_System_Cde.setDdmHeader("SYSTEM/CODE");
        icw_Efm_Stats_Count_Castupdate_Counts = vw_icw_Efm_Stats.getRecord().newFieldInGroup("icw_Efm_Stats_Count_Castupdate_Counts", "C*UPDATE-COUNTS", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "ICW_EFM_STATS_UPDATE_COUNTS");
        icw_Efm_Stats_Update_Counts = vw_icw_Efm_Stats.getRecord().newGroupInGroup("icw_Efm_Stats_Update_Counts", "UPDATE-COUNTS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ICW_EFM_STATS_UPDATE_COUNTS");
        icw_Efm_Stats_Action_Cde = icw_Efm_Stats_Update_Counts.newFieldArrayInGroup("icw_Efm_Stats_Action_Cde", "ACTION-CDE", FieldType.STRING, 2, new 
            DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACTION_CDE", "ICW_EFM_STATS_UPDATE_COUNTS");
        icw_Efm_Stats_Action_Cde.setDdmHeader("ACTION");
        icw_Efm_Stats_Xtn_Cnt = icw_Efm_Stats_Update_Counts.newFieldArrayInGroup("icw_Efm_Stats_Xtn_Cnt", "XTN-CNT", FieldType.NUMERIC, 7, new DbsArrayController(1,99) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XTN_CNT", "ICW_EFM_STATS_UPDATE_COUNTS");
        icw_Efm_Stats_Xtn_Cnt.setDdmHeader("XTN/COUNT");
        icw_Efm_Stats_Mit_Added_Cnt = icw_Efm_Stats_Update_Counts.newFieldArrayInGroup("icw_Efm_Stats_Mit_Added_Cnt", "MIT-ADDED-CNT", FieldType.NUMERIC, 
            7, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_ADDED_CNT", "ICW_EFM_STATS_UPDATE_COUNTS");
        icw_Efm_Stats_Mit_Added_Cnt.setDdmHeader("NBR OF MIT/RECS ADDED");
        icw_Efm_Stats_Mit_Updated_Cnt = icw_Efm_Stats_Update_Counts.newFieldArrayInGroup("icw_Efm_Stats_Mit_Updated_Cnt", "MIT-UPDATED-CNT", FieldType.NUMERIC, 
            7, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_UPDATED_CNT", "ICW_EFM_STATS_UPDATE_COUNTS");
        icw_Efm_Stats_Mit_Updated_Cnt.setDdmHeader("NBR OF MIT/RECS UPDATED");
        icw_Efm_Stats_Dcmnt_Added_Cnt = icw_Efm_Stats_Update_Counts.newFieldArrayInGroup("icw_Efm_Stats_Dcmnt_Added_Cnt", "DCMNT-ADDED-CNT", FieldType.NUMERIC, 
            7, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "DCMNT_ADDED_CNT", "ICW_EFM_STATS_UPDATE_COUNTS");
        icw_Efm_Stats_Dcmnt_Added_Cnt.setDdmHeader("NBR OF DCMT/ADDED");
        icw_Efm_Stats_Dcmnt_Renamed_Cnt = icw_Efm_Stats_Update_Counts.newFieldArrayInGroup("icw_Efm_Stats_Dcmnt_Renamed_Cnt", "DCMNT-RENAMED-CNT", FieldType.NUMERIC, 
            7, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "DCMNT_RENAMED_CNT", "ICW_EFM_STATS_UPDATE_COUNTS");
        icw_Efm_Stats_Dcmnt_Renamed_Cnt.setDdmHeader("NBR OF DCMT/RENAMED");
        vw_icw_Efm_Stats.setUniquePeList();

        this.setRecordName("LdaIefl9031");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_icw_Efm_Stats.reset();
    }

    // Constructor
    public LdaIefl9031() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
