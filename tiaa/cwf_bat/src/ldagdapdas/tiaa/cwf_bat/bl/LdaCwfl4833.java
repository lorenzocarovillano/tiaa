/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:56 PM
**        * FROM NATURAL LDA     : CWFL4833
************************************************************
**        * FILE NAME            : LdaCwfl4833.java
**        * CLASS NAME           : LdaCwfl4833
**        * INSTANCE NAME        : LdaCwfl4833
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4833 extends DbsRecord
{
    // Properties
    private DbsGroup customer;
    private DbsField customer_Pin_Npin;
    private DbsField customer_B1;
    private DbsField customer_Ssn;
    private DbsField customer_B2;
    private DbsField customer_Part_Name;
    private DbsField customer_B3;
    private DbsField customer_State_Of_Res;
    private DbsField customer_B4;
    private DbsField customer_Dte_Of_Birth;
    private DbsField customer_B5;
    private DbsField customer_Pin_Type;
    private DbsField customer_B6;
    private DbsField customer_Last_Chnge_Dte_Tme_A;

    public DbsGroup getCustomer() { return customer; }

    public DbsField getCustomer_Pin_Npin() { return customer_Pin_Npin; }

    public DbsField getCustomer_B1() { return customer_B1; }

    public DbsField getCustomer_Ssn() { return customer_Ssn; }

    public DbsField getCustomer_B2() { return customer_B2; }

    public DbsField getCustomer_Part_Name() { return customer_Part_Name; }

    public DbsField getCustomer_B3() { return customer_B3; }

    public DbsField getCustomer_State_Of_Res() { return customer_State_Of_Res; }

    public DbsField getCustomer_B4() { return customer_B4; }

    public DbsField getCustomer_Dte_Of_Birth() { return customer_Dte_Of_Birth; }

    public DbsField getCustomer_B5() { return customer_B5; }

    public DbsField getCustomer_Pin_Type() { return customer_Pin_Type; }

    public DbsField getCustomer_B6() { return customer_B6; }

    public DbsField getCustomer_Last_Chnge_Dte_Tme_A() { return customer_Last_Chnge_Dte_Tme_A; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        customer = newGroupInRecord("customer", "CUSTOMER");
        customer_Pin_Npin = customer.newFieldInGroup("customer_Pin_Npin", "PIN-NPIN", FieldType.STRING, 12);
        customer_B1 = customer.newFieldInGroup("customer_B1", "B1", FieldType.STRING, 1);
        customer_Ssn = customer.newFieldInGroup("customer_Ssn", "SSN", FieldType.NUMERIC, 9);
        customer_B2 = customer.newFieldInGroup("customer_B2", "B2", FieldType.STRING, 1);
        customer_Part_Name = customer.newFieldInGroup("customer_Part_Name", "PART-NAME", FieldType.STRING, 40);
        customer_B3 = customer.newFieldInGroup("customer_B3", "B3", FieldType.STRING, 1);
        customer_State_Of_Res = customer.newFieldInGroup("customer_State_Of_Res", "STATE-OF-RES", FieldType.STRING, 2);
        customer_B4 = customer.newFieldInGroup("customer_B4", "B4", FieldType.STRING, 1);
        customer_Dte_Of_Birth = customer.newFieldInGroup("customer_Dte_Of_Birth", "DTE-OF-BIRTH", FieldType.STRING, 12);
        customer_B5 = customer.newFieldInGroup("customer_B5", "B5", FieldType.STRING, 1);
        customer_Pin_Type = customer.newFieldInGroup("customer_Pin_Type", "PIN-TYPE", FieldType.STRING, 1);
        customer_B6 = customer.newFieldInGroup("customer_B6", "B6", FieldType.STRING, 1);
        customer_Last_Chnge_Dte_Tme_A = customer.newFieldInGroup("customer_Last_Chnge_Dte_Tme_A", "LAST-CHNGE-DTE-TME-A", FieldType.STRING, 15);

        this.setRecordName("LdaCwfl4833");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4833() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
