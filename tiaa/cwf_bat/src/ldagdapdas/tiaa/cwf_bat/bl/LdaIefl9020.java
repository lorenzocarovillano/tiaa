/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:05:55 PM
**        * FROM NATURAL LDA     : IEFL9020
************************************************************
**        * FILE NAME            : LdaIefl9020.java
**        * CLASS NAME           : LdaIefl9020
**        * INSTANCE NAME        : LdaIefl9020
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIefl9020 extends DbsRecord
{
    // Properties
    private DbsField pnd_Work_Record;
    private DbsGroup pnd_Work_RecordRedef1;
    private DbsGroup pnd_Work_Record_Pnd_Work_Record_Grp;
    private DbsField pnd_Work_Record_Log_Dte;
    private DbsField pnd_Work_Record_System_Cde;
    private DbsField pnd_Work_Record_Action_Cde;
    private DbsField pnd_Work_Record_Recursive_Ind;
    private DbsField pnd_Work_Record_Mit_Added_Cnt;
    private DbsField pnd_Work_Record_Mit_Updated_Cnt;
    private DbsField pnd_Work_Record_Fldr_Added_Cnt;
    private DbsField pnd_Work_Record_Fldr_Updated_Cnt;
    private DbsField pnd_Work_Record_Fldr_Deleted_Cnt;
    private DbsField pnd_Work_Record_Dcmnt_Added_Cnt;
    private DbsField pnd_Work_Record_Dcmnt_Renamed_Cnt;
    private DbsField filler01;

    public DbsField getPnd_Work_Record() { return pnd_Work_Record; }

    public DbsGroup getPnd_Work_RecordRedef1() { return pnd_Work_RecordRedef1; }

    public DbsGroup getPnd_Work_Record_Pnd_Work_Record_Grp() { return pnd_Work_Record_Pnd_Work_Record_Grp; }

    public DbsField getPnd_Work_Record_Log_Dte() { return pnd_Work_Record_Log_Dte; }

    public DbsField getPnd_Work_Record_System_Cde() { return pnd_Work_Record_System_Cde; }

    public DbsField getPnd_Work_Record_Action_Cde() { return pnd_Work_Record_Action_Cde; }

    public DbsField getPnd_Work_Record_Recursive_Ind() { return pnd_Work_Record_Recursive_Ind; }

    public DbsField getPnd_Work_Record_Mit_Added_Cnt() { return pnd_Work_Record_Mit_Added_Cnt; }

    public DbsField getPnd_Work_Record_Mit_Updated_Cnt() { return pnd_Work_Record_Mit_Updated_Cnt; }

    public DbsField getPnd_Work_Record_Fldr_Added_Cnt() { return pnd_Work_Record_Fldr_Added_Cnt; }

    public DbsField getPnd_Work_Record_Fldr_Updated_Cnt() { return pnd_Work_Record_Fldr_Updated_Cnt; }

    public DbsField getPnd_Work_Record_Fldr_Deleted_Cnt() { return pnd_Work_Record_Fldr_Deleted_Cnt; }

    public DbsField getPnd_Work_Record_Dcmnt_Added_Cnt() { return pnd_Work_Record_Dcmnt_Added_Cnt; }

    public DbsField getPnd_Work_Record_Dcmnt_Renamed_Cnt() { return pnd_Work_Record_Dcmnt_Renamed_Cnt; }

    public DbsField getFiller01() { return filler01; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Work_Record = newFieldInRecord("pnd_Work_Record", "#WORK-RECORD", FieldType.STRING, 60);
        pnd_Work_RecordRedef1 = newGroupInRecord("pnd_Work_RecordRedef1", "Redefines", pnd_Work_Record);
        pnd_Work_Record_Pnd_Work_Record_Grp = pnd_Work_RecordRedef1.newGroupInGroup("pnd_Work_Record_Pnd_Work_Record_Grp", "#WORK-RECORD-GRP");
        pnd_Work_Record_Log_Dte = pnd_Work_Record_Pnd_Work_Record_Grp.newFieldInGroup("pnd_Work_Record_Log_Dte", "LOG-DTE", FieldType.STRING, 8);
        pnd_Work_Record_System_Cde = pnd_Work_Record_Pnd_Work_Record_Grp.newFieldInGroup("pnd_Work_Record_System_Cde", "SYSTEM-CDE", FieldType.STRING, 
            15);
        pnd_Work_Record_Action_Cde = pnd_Work_Record_Pnd_Work_Record_Grp.newFieldInGroup("pnd_Work_Record_Action_Cde", "ACTION-CDE", FieldType.STRING, 
            2);
        pnd_Work_Record_Recursive_Ind = pnd_Work_Record_Pnd_Work_Record_Grp.newFieldInGroup("pnd_Work_Record_Recursive_Ind", "RECURSIVE-IND", FieldType.STRING, 
            1);
        pnd_Work_Record_Mit_Added_Cnt = pnd_Work_Record_Pnd_Work_Record_Grp.newFieldInGroup("pnd_Work_Record_Mit_Added_Cnt", "MIT-ADDED-CNT", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_Mit_Updated_Cnt = pnd_Work_Record_Pnd_Work_Record_Grp.newFieldInGroup("pnd_Work_Record_Mit_Updated_Cnt", "MIT-UPDATED-CNT", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_Fldr_Added_Cnt = pnd_Work_Record_Pnd_Work_Record_Grp.newFieldInGroup("pnd_Work_Record_Fldr_Added_Cnt", "FLDR-ADDED-CNT", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_Fldr_Updated_Cnt = pnd_Work_Record_Pnd_Work_Record_Grp.newFieldInGroup("pnd_Work_Record_Fldr_Updated_Cnt", "FLDR-UPDATED-CNT", 
            FieldType.NUMERIC, 3);
        pnd_Work_Record_Fldr_Deleted_Cnt = pnd_Work_Record_Pnd_Work_Record_Grp.newFieldInGroup("pnd_Work_Record_Fldr_Deleted_Cnt", "FLDR-DELETED-CNT", 
            FieldType.NUMERIC, 3);
        pnd_Work_Record_Dcmnt_Added_Cnt = pnd_Work_Record_Pnd_Work_Record_Grp.newFieldInGroup("pnd_Work_Record_Dcmnt_Added_Cnt", "DCMNT-ADDED-CNT", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_Dcmnt_Renamed_Cnt = pnd_Work_Record_Pnd_Work_Record_Grp.newFieldInGroup("pnd_Work_Record_Dcmnt_Renamed_Cnt", "DCMNT-RENAMED-CNT", 
            FieldType.NUMERIC, 3);
        filler01 = pnd_Work_Record_Pnd_Work_Record_Grp.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 13);

        this.setRecordName("LdaIefl9020");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIefl9020() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
