/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:46 PM
**        * FROM NATURAL PDA     : ANTA002
************************************************************
**        * FILE NAME            : PdaAnta002.java
**        * CLASS NAME           : PdaAnta002
**        * INSTANCE NAME        : PdaAnta002
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAnta002 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Ls_Pamp_I_Info;
    private DbsField pnd_Ls_Pamp_I_Info_Pnd_Ls_Curr_Bus_Date;
    private DbsField pnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Tiaa_Rate;
    private DbsField pnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Cref_Rate;
    private DbsField pnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Insr_Rate;
    private DbsField pnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Code;
    private DbsField pnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Msg;

    public DbsGroup getPnd_Ls_Pamp_I_Info() { return pnd_Ls_Pamp_I_Info; }

    public DbsField getPnd_Ls_Pamp_I_Info_Pnd_Ls_Curr_Bus_Date() { return pnd_Ls_Pamp_I_Info_Pnd_Ls_Curr_Bus_Date; }

    public DbsField getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Tiaa_Rate() { return pnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Tiaa_Rate; }

    public DbsField getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Cref_Rate() { return pnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Cref_Rate; }

    public DbsField getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Insr_Rate() { return pnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Insr_Rate; }

    public DbsField getPnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Code() { return pnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Code; }

    public DbsField getPnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Msg() { return pnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Msg; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Ls_Pamp_I_Info = dbsRecord.newGroupInRecord("pnd_Ls_Pamp_I_Info", "#LS-P&I-INFO");
        pnd_Ls_Pamp_I_Info.setParameterOption(ParameterOption.ByReference);
        pnd_Ls_Pamp_I_Info_Pnd_Ls_Curr_Bus_Date = pnd_Ls_Pamp_I_Info.newFieldInGroup("pnd_Ls_Pamp_I_Info_Pnd_Ls_Curr_Bus_Date", "#LS-CURR-BUS-DATE", FieldType.NUMERIC, 
            8);
        pnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Tiaa_Rate = pnd_Ls_Pamp_I_Info.newFieldInGroup("pnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Tiaa_Rate", "#LS-P&I-TIAA-RATE", 
            FieldType.DECIMAL, 7,6);
        pnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Cref_Rate = pnd_Ls_Pamp_I_Info.newFieldInGroup("pnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Cref_Rate", "#LS-P&I-CREF-RATE", 
            FieldType.DECIMAL, 7,6);
        pnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Insr_Rate = pnd_Ls_Pamp_I_Info.newFieldInGroup("pnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Insr_Rate", "#LS-P&I-INSR-RATE", 
            FieldType.DECIMAL, 7,6);
        pnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Code = pnd_Ls_Pamp_I_Info.newFieldInGroup("pnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Code", "#LS-RET-CODE", FieldType.NUMERIC, 
            4);
        pnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Msg = pnd_Ls_Pamp_I_Info.newFieldInGroup("pnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Msg", "#LS-RET-MSG", FieldType.STRING, 50);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAnta002(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

