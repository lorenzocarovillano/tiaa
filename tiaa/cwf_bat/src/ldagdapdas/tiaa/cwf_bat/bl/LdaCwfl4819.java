/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:46 PM
**        * FROM NATURAL LDA     : CWFL4819
************************************************************
**        * FILE NAME            : LdaCwfl4819.java
**        * CLASS NAME           : LdaCwfl4819
**        * INSTANCE NAME        : LdaCwfl4819
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4819 extends DbsRecord
{
    // Properties
    private DbsGroup work_Request;
    private DbsField work_Request_Rqst_Log_Dte_Tme_A;
    private DbsField work_Request_C1;
    private DbsField work_Request_Work_Prcss_Id;
    private DbsGroup work_Request_Work_Prcss_IdRedef1;
    private DbsField work_Request_Work_Actn_Rqstd_Cde;
    private DbsField work_Request_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField work_Request_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField work_Request_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField work_Request_C2;
    private DbsField work_Request_Pin_Nbr;
    private DbsField work_Request_C3;
    private DbsField work_Request_Part_Name;
    private DbsField work_Request_C4;
    private DbsField work_Request_Ssn;
    private DbsField work_Request_C5;
    private DbsField work_Request_Tiaa_Rcvd_Dte_Tme_A;
    private DbsField work_Request_C6;
    private DbsField work_Request_Tiaa_Rcvd_Dte_A;
    private DbsField work_Request_C7;
    private DbsField work_Request_Orgnl_Unit_Cde;
    private DbsField work_Request_C8;
    private DbsField work_Request_Rqst_Log_Oprtr_Cde;
    private DbsField work_Request_C9;
    private DbsField work_Request_Rqst_Orgn_Cde;
    private DbsField work_Request_C10;
    private DbsField work_Request_Crprte_Status_Ind;
    private DbsField work_Request_C11;
    private DbsField work_Request_Crprte_Clock_End_Dte_Tme_A;
    private DbsField work_Request_C12;
    private DbsField work_Request_Final_Close_Out_Dte_Tme_A;
    private DbsField work_Request_C13;
    private DbsField work_Request_Effctve_Dte_A;
    private DbsField work_Request_C14;
    private DbsField work_Request_Trans_Dte_A;
    private DbsField work_Request_C15;
    private DbsField work_Request_Trnsctn_Dte_A;
    private DbsField work_Request_C16;
    private DbsField work_Request_Owner_Unit_Cde;
    private DbsField work_Request_C17;
    private DbsField work_Request_Owner_Division;
    private DbsField work_Request_C18;
    private DbsField work_Request_Shphrd_Id;
    private DbsField work_Request_C19;
    private DbsField work_Request_Sec_Ind;
    private DbsField work_Request_C20;
    private DbsField work_Request_Admin_Work_Ind;
    private DbsField work_Request_C21;
    private DbsField work_Request_Sub_Rqst_Ind;
    private DbsField work_Request_C22;
    private DbsField work_Request_Prnt_Rqst_Log_Dte_Tme_A;
    private DbsField work_Request_C23;
    private DbsField work_Request_Prnt_Work_Prcss_Id;
    private DbsField work_Request_C24;
    private DbsField work_Request_Multi_Rqst_Ind;
    private DbsField work_Request_C25;
    private DbsField work_Request_Cmplnt_Ind;
    private DbsField work_Request_C26;
    private DbsField work_Request_Elctrnc_Fldr_Ind;
    private DbsField work_Request_C27;
    private DbsField work_Request_Mj_Pull_Ind;
    private DbsField work_Request_C28;
    private DbsField work_Request_Check_Ind;
    private DbsField work_Request_C29;
    private DbsField work_Request_Bsnss_Reply_Ind;
    private DbsField work_Request_C30;
    private DbsField work_Request_Tlc_Ind;
    private DbsField work_Request_C31;
    private DbsField work_Request_Redo_Ind;
    private DbsField work_Request_C32;
    private DbsField work_Request_Crprte_On_Tme_Ind;
    private DbsField work_Request_C33;
    private DbsField work_Request_Off_Rtng_Ind;
    private DbsField work_Request_C34;
    private DbsField work_Request_Prcssng_Type_Cde;
    private DbsField work_Request_C35;
    private DbsField work_Request_Log_Rqstr_Cde;
    private DbsField work_Request_C36;
    private DbsField work_Request_Log_Insttn_Srce_Cde;
    private DbsField work_Request_C37;
    private DbsField work_Request_Instn_Cde;
    private DbsField work_Request_C38;
    private DbsField work_Request_Rqst_Instn_Cde;
    private DbsField work_Request_C39;
    private DbsField work_Request_Rqst_Rgn_Cde;
    private DbsField work_Request_C40;
    private DbsField work_Request_Rqst_Spcl_Dsgntn_Cde;
    private DbsField work_Request_C41;
    private DbsField work_Request_Rqst_Brnch_Cde;
    private DbsField work_Request_C42;
    private DbsField work_Request_Extrnl_Pend_Ind;
    private DbsField work_Request_C43;
    private DbsField work_Request_Crprte_Due_Dte_Tme_A;
    private DbsField work_Request_C44;
    private DbsField work_Request_Mis_Routed_Ind;
    private DbsField work_Request_C45;
    private DbsField work_Request_Dte_Of_Birth;
    private DbsField work_Request_C46;
    private DbsField work_Request_Trade_Dte_Tme_A;
    private DbsField work_Request_C47;
    private DbsField work_Request_Mj_Media_Ind;
    private DbsField work_Request_C48;
    private DbsField work_Request_State_Of_Res;
    private DbsField work_Request_C49;
    private DbsField work_Request_Sec_Turnaround_Tme_A;
    private DbsField work_Request_C50;
    private DbsField work_Request_Sec_Updte_Dte_A;
    private DbsField work_Request_C51;
    private DbsField work_Request_Rqst_Indicators;
    private DbsField work_Request_C52;
    private DbsField work_Request_Part_Bus_Days;
    private DbsField work_Request_C53;
    private DbsField work_Request_Part_Cal_Days;
    private DbsField work_Request_C54;
    private DbsField work_Request_Start_Dte_Type;
    private DbsField work_Request_C55;
    private DbsField work_Request_Acknldg_Dte_Tme_A;
    private DbsField work_Request_C56;
    private DbsField work_Request_Acknldg_Tat_Tme;
    private DbsField work_Request_C57;
    private DbsField work_Request_Acknldg_Reqd_Ind;
    private DbsField work_Request_C58;
    private DbsField work_Request_Acknldg_On_Time_Ind;
    private DbsField work_Request_C59;
    private DbsField work_Request_Last_Chnge_Dte_Tme_A;
    private DbsField work_Request_C60;
    private DbsField work_Request_Part_Close_Status_A;
    private DbsField work_Request_C61;
    private DbsField work_Request_Cnnctd_Rqst_Ind;
    private DbsField work_Request_C62;
    private DbsField work_Request_Prvte_Wrk_Rqst_Ind;
    private DbsField work_Request_C63;
    private DbsField work_Request_Instn_Rqst_Log_Dte_Tme_A;
    private DbsField work_Request_C64;
    private DbsField work_Request_Log_Sqnce_Nbr;
    private DbsField work_Request_C65;
    private DbsField work_Request_Start_Dte_Tme_A;
    private DbsField work_Request_C66;
    private DbsField work_Request_Admin_Status_Cde;
    private DbsField work_Request_C67;
    private DbsField work_Request_Crrnt_Cmt_Ind;
    private DbsField work_Request_C68;
    private DbsField work_Request_Np_Pin;
    private DbsField work_Request_C69;
    private DbsField work_Request_Dod_Not_Dte;
    private DbsField work_Request_C70;
    private DbsField work_Request_Da_Accum_Cntr;
    private DbsField work_Request_C71;
    private DbsField work_Request_Tpa_Accum_Cntr;
    private DbsField work_Request_C72;
    private DbsField work_Request_P_I_Accum_Cntr;
    private DbsField work_Request_C73;
    private DbsField work_Request_Ipro_Accum_Cntr;
    private DbsField work_Request_C74;
    private DbsField work_Request_Tot_Accum_Cntr;
    private DbsField work_Request_C75;
    private DbsField work_Request_Multi_Prod_Cde;
    private DbsField work_Request_C76;
    private DbsField work_Request_Complex_Txn_Cde;
    private DbsField work_Request_C77;
    private DbsField work_Request_Part_Age;
    private DbsField work_Request_C78;
    private DbsField work_Request_Pin_Npin;
    private DbsField work_Request_C79;
    private DbsField work_Request_Pin_Type_Ind;
    private DbsField work_Request_C80;
    private DbsField work_Request_Rqst_Log_Dte_Tme_D;
    private DbsField work_Request_C81;
    private DbsField work_Request_Admin_Unit_Cde;
    private DbsField work_Request_C82;
    private DbsField work_Request_Last_Updte_Oprtr_Cde;
    private DbsField work_Request_C83;
    private DbsField work_Request_Due_Dte_Prty;

    public DbsGroup getWork_Request() { return work_Request; }

    public DbsField getWork_Request_Rqst_Log_Dte_Tme_A() { return work_Request_Rqst_Log_Dte_Tme_A; }

    public DbsField getWork_Request_C1() { return work_Request_C1; }

    public DbsField getWork_Request_Work_Prcss_Id() { return work_Request_Work_Prcss_Id; }

    public DbsGroup getWork_Request_Work_Prcss_IdRedef1() { return work_Request_Work_Prcss_IdRedef1; }

    public DbsField getWork_Request_Work_Actn_Rqstd_Cde() { return work_Request_Work_Actn_Rqstd_Cde; }

    public DbsField getWork_Request_Work_Lob_Cmpny_Prdct_Cde() { return work_Request_Work_Lob_Cmpny_Prdct_Cde; }

    public DbsField getWork_Request_Work_Mjr_Bsnss_Prcss_Cde() { return work_Request_Work_Mjr_Bsnss_Prcss_Cde; }

    public DbsField getWork_Request_Work_Spcfc_Bsnss_Prcss_Cde() { return work_Request_Work_Spcfc_Bsnss_Prcss_Cde; }

    public DbsField getWork_Request_C2() { return work_Request_C2; }

    public DbsField getWork_Request_Pin_Nbr() { return work_Request_Pin_Nbr; }

    public DbsField getWork_Request_C3() { return work_Request_C3; }

    public DbsField getWork_Request_Part_Name() { return work_Request_Part_Name; }

    public DbsField getWork_Request_C4() { return work_Request_C4; }

    public DbsField getWork_Request_Ssn() { return work_Request_Ssn; }

    public DbsField getWork_Request_C5() { return work_Request_C5; }

    public DbsField getWork_Request_Tiaa_Rcvd_Dte_Tme_A() { return work_Request_Tiaa_Rcvd_Dte_Tme_A; }

    public DbsField getWork_Request_C6() { return work_Request_C6; }

    public DbsField getWork_Request_Tiaa_Rcvd_Dte_A() { return work_Request_Tiaa_Rcvd_Dte_A; }

    public DbsField getWork_Request_C7() { return work_Request_C7; }

    public DbsField getWork_Request_Orgnl_Unit_Cde() { return work_Request_Orgnl_Unit_Cde; }

    public DbsField getWork_Request_C8() { return work_Request_C8; }

    public DbsField getWork_Request_Rqst_Log_Oprtr_Cde() { return work_Request_Rqst_Log_Oprtr_Cde; }

    public DbsField getWork_Request_C9() { return work_Request_C9; }

    public DbsField getWork_Request_Rqst_Orgn_Cde() { return work_Request_Rqst_Orgn_Cde; }

    public DbsField getWork_Request_C10() { return work_Request_C10; }

    public DbsField getWork_Request_Crprte_Status_Ind() { return work_Request_Crprte_Status_Ind; }

    public DbsField getWork_Request_C11() { return work_Request_C11; }

    public DbsField getWork_Request_Crprte_Clock_End_Dte_Tme_A() { return work_Request_Crprte_Clock_End_Dte_Tme_A; }

    public DbsField getWork_Request_C12() { return work_Request_C12; }

    public DbsField getWork_Request_Final_Close_Out_Dte_Tme_A() { return work_Request_Final_Close_Out_Dte_Tme_A; }

    public DbsField getWork_Request_C13() { return work_Request_C13; }

    public DbsField getWork_Request_Effctve_Dte_A() { return work_Request_Effctve_Dte_A; }

    public DbsField getWork_Request_C14() { return work_Request_C14; }

    public DbsField getWork_Request_Trans_Dte_A() { return work_Request_Trans_Dte_A; }

    public DbsField getWork_Request_C15() { return work_Request_C15; }

    public DbsField getWork_Request_Trnsctn_Dte_A() { return work_Request_Trnsctn_Dte_A; }

    public DbsField getWork_Request_C16() { return work_Request_C16; }

    public DbsField getWork_Request_Owner_Unit_Cde() { return work_Request_Owner_Unit_Cde; }

    public DbsField getWork_Request_C17() { return work_Request_C17; }

    public DbsField getWork_Request_Owner_Division() { return work_Request_Owner_Division; }

    public DbsField getWork_Request_C18() { return work_Request_C18; }

    public DbsField getWork_Request_Shphrd_Id() { return work_Request_Shphrd_Id; }

    public DbsField getWork_Request_C19() { return work_Request_C19; }

    public DbsField getWork_Request_Sec_Ind() { return work_Request_Sec_Ind; }

    public DbsField getWork_Request_C20() { return work_Request_C20; }

    public DbsField getWork_Request_Admin_Work_Ind() { return work_Request_Admin_Work_Ind; }

    public DbsField getWork_Request_C21() { return work_Request_C21; }

    public DbsField getWork_Request_Sub_Rqst_Ind() { return work_Request_Sub_Rqst_Ind; }

    public DbsField getWork_Request_C22() { return work_Request_C22; }

    public DbsField getWork_Request_Prnt_Rqst_Log_Dte_Tme_A() { return work_Request_Prnt_Rqst_Log_Dte_Tme_A; }

    public DbsField getWork_Request_C23() { return work_Request_C23; }

    public DbsField getWork_Request_Prnt_Work_Prcss_Id() { return work_Request_Prnt_Work_Prcss_Id; }

    public DbsField getWork_Request_C24() { return work_Request_C24; }

    public DbsField getWork_Request_Multi_Rqst_Ind() { return work_Request_Multi_Rqst_Ind; }

    public DbsField getWork_Request_C25() { return work_Request_C25; }

    public DbsField getWork_Request_Cmplnt_Ind() { return work_Request_Cmplnt_Ind; }

    public DbsField getWork_Request_C26() { return work_Request_C26; }

    public DbsField getWork_Request_Elctrnc_Fldr_Ind() { return work_Request_Elctrnc_Fldr_Ind; }

    public DbsField getWork_Request_C27() { return work_Request_C27; }

    public DbsField getWork_Request_Mj_Pull_Ind() { return work_Request_Mj_Pull_Ind; }

    public DbsField getWork_Request_C28() { return work_Request_C28; }

    public DbsField getWork_Request_Check_Ind() { return work_Request_Check_Ind; }

    public DbsField getWork_Request_C29() { return work_Request_C29; }

    public DbsField getWork_Request_Bsnss_Reply_Ind() { return work_Request_Bsnss_Reply_Ind; }

    public DbsField getWork_Request_C30() { return work_Request_C30; }

    public DbsField getWork_Request_Tlc_Ind() { return work_Request_Tlc_Ind; }

    public DbsField getWork_Request_C31() { return work_Request_C31; }

    public DbsField getWork_Request_Redo_Ind() { return work_Request_Redo_Ind; }

    public DbsField getWork_Request_C32() { return work_Request_C32; }

    public DbsField getWork_Request_Crprte_On_Tme_Ind() { return work_Request_Crprte_On_Tme_Ind; }

    public DbsField getWork_Request_C33() { return work_Request_C33; }

    public DbsField getWork_Request_Off_Rtng_Ind() { return work_Request_Off_Rtng_Ind; }

    public DbsField getWork_Request_C34() { return work_Request_C34; }

    public DbsField getWork_Request_Prcssng_Type_Cde() { return work_Request_Prcssng_Type_Cde; }

    public DbsField getWork_Request_C35() { return work_Request_C35; }

    public DbsField getWork_Request_Log_Rqstr_Cde() { return work_Request_Log_Rqstr_Cde; }

    public DbsField getWork_Request_C36() { return work_Request_C36; }

    public DbsField getWork_Request_Log_Insttn_Srce_Cde() { return work_Request_Log_Insttn_Srce_Cde; }

    public DbsField getWork_Request_C37() { return work_Request_C37; }

    public DbsField getWork_Request_Instn_Cde() { return work_Request_Instn_Cde; }

    public DbsField getWork_Request_C38() { return work_Request_C38; }

    public DbsField getWork_Request_Rqst_Instn_Cde() { return work_Request_Rqst_Instn_Cde; }

    public DbsField getWork_Request_C39() { return work_Request_C39; }

    public DbsField getWork_Request_Rqst_Rgn_Cde() { return work_Request_Rqst_Rgn_Cde; }

    public DbsField getWork_Request_C40() { return work_Request_C40; }

    public DbsField getWork_Request_Rqst_Spcl_Dsgntn_Cde() { return work_Request_Rqst_Spcl_Dsgntn_Cde; }

    public DbsField getWork_Request_C41() { return work_Request_C41; }

    public DbsField getWork_Request_Rqst_Brnch_Cde() { return work_Request_Rqst_Brnch_Cde; }

    public DbsField getWork_Request_C42() { return work_Request_C42; }

    public DbsField getWork_Request_Extrnl_Pend_Ind() { return work_Request_Extrnl_Pend_Ind; }

    public DbsField getWork_Request_C43() { return work_Request_C43; }

    public DbsField getWork_Request_Crprte_Due_Dte_Tme_A() { return work_Request_Crprte_Due_Dte_Tme_A; }

    public DbsField getWork_Request_C44() { return work_Request_C44; }

    public DbsField getWork_Request_Mis_Routed_Ind() { return work_Request_Mis_Routed_Ind; }

    public DbsField getWork_Request_C45() { return work_Request_C45; }

    public DbsField getWork_Request_Dte_Of_Birth() { return work_Request_Dte_Of_Birth; }

    public DbsField getWork_Request_C46() { return work_Request_C46; }

    public DbsField getWork_Request_Trade_Dte_Tme_A() { return work_Request_Trade_Dte_Tme_A; }

    public DbsField getWork_Request_C47() { return work_Request_C47; }

    public DbsField getWork_Request_Mj_Media_Ind() { return work_Request_Mj_Media_Ind; }

    public DbsField getWork_Request_C48() { return work_Request_C48; }

    public DbsField getWork_Request_State_Of_Res() { return work_Request_State_Of_Res; }

    public DbsField getWork_Request_C49() { return work_Request_C49; }

    public DbsField getWork_Request_Sec_Turnaround_Tme_A() { return work_Request_Sec_Turnaround_Tme_A; }

    public DbsField getWork_Request_C50() { return work_Request_C50; }

    public DbsField getWork_Request_Sec_Updte_Dte_A() { return work_Request_Sec_Updte_Dte_A; }

    public DbsField getWork_Request_C51() { return work_Request_C51; }

    public DbsField getWork_Request_Rqst_Indicators() { return work_Request_Rqst_Indicators; }

    public DbsField getWork_Request_C52() { return work_Request_C52; }

    public DbsField getWork_Request_Part_Bus_Days() { return work_Request_Part_Bus_Days; }

    public DbsField getWork_Request_C53() { return work_Request_C53; }

    public DbsField getWork_Request_Part_Cal_Days() { return work_Request_Part_Cal_Days; }

    public DbsField getWork_Request_C54() { return work_Request_C54; }

    public DbsField getWork_Request_Start_Dte_Type() { return work_Request_Start_Dte_Type; }

    public DbsField getWork_Request_C55() { return work_Request_C55; }

    public DbsField getWork_Request_Acknldg_Dte_Tme_A() { return work_Request_Acknldg_Dte_Tme_A; }

    public DbsField getWork_Request_C56() { return work_Request_C56; }

    public DbsField getWork_Request_Acknldg_Tat_Tme() { return work_Request_Acknldg_Tat_Tme; }

    public DbsField getWork_Request_C57() { return work_Request_C57; }

    public DbsField getWork_Request_Acknldg_Reqd_Ind() { return work_Request_Acknldg_Reqd_Ind; }

    public DbsField getWork_Request_C58() { return work_Request_C58; }

    public DbsField getWork_Request_Acknldg_On_Time_Ind() { return work_Request_Acknldg_On_Time_Ind; }

    public DbsField getWork_Request_C59() { return work_Request_C59; }

    public DbsField getWork_Request_Last_Chnge_Dte_Tme_A() { return work_Request_Last_Chnge_Dte_Tme_A; }

    public DbsField getWork_Request_C60() { return work_Request_C60; }

    public DbsField getWork_Request_Part_Close_Status_A() { return work_Request_Part_Close_Status_A; }

    public DbsField getWork_Request_C61() { return work_Request_C61; }

    public DbsField getWork_Request_Cnnctd_Rqst_Ind() { return work_Request_Cnnctd_Rqst_Ind; }

    public DbsField getWork_Request_C62() { return work_Request_C62; }

    public DbsField getWork_Request_Prvte_Wrk_Rqst_Ind() { return work_Request_Prvte_Wrk_Rqst_Ind; }

    public DbsField getWork_Request_C63() { return work_Request_C63; }

    public DbsField getWork_Request_Instn_Rqst_Log_Dte_Tme_A() { return work_Request_Instn_Rqst_Log_Dte_Tme_A; }

    public DbsField getWork_Request_C64() { return work_Request_C64; }

    public DbsField getWork_Request_Log_Sqnce_Nbr() { return work_Request_Log_Sqnce_Nbr; }

    public DbsField getWork_Request_C65() { return work_Request_C65; }

    public DbsField getWork_Request_Start_Dte_Tme_A() { return work_Request_Start_Dte_Tme_A; }

    public DbsField getWork_Request_C66() { return work_Request_C66; }

    public DbsField getWork_Request_Admin_Status_Cde() { return work_Request_Admin_Status_Cde; }

    public DbsField getWork_Request_C67() { return work_Request_C67; }

    public DbsField getWork_Request_Crrnt_Cmt_Ind() { return work_Request_Crrnt_Cmt_Ind; }

    public DbsField getWork_Request_C68() { return work_Request_C68; }

    public DbsField getWork_Request_Np_Pin() { return work_Request_Np_Pin; }

    public DbsField getWork_Request_C69() { return work_Request_C69; }

    public DbsField getWork_Request_Dod_Not_Dte() { return work_Request_Dod_Not_Dte; }

    public DbsField getWork_Request_C70() { return work_Request_C70; }

    public DbsField getWork_Request_Da_Accum_Cntr() { return work_Request_Da_Accum_Cntr; }

    public DbsField getWork_Request_C71() { return work_Request_C71; }

    public DbsField getWork_Request_Tpa_Accum_Cntr() { return work_Request_Tpa_Accum_Cntr; }

    public DbsField getWork_Request_C72() { return work_Request_C72; }

    public DbsField getWork_Request_P_I_Accum_Cntr() { return work_Request_P_I_Accum_Cntr; }

    public DbsField getWork_Request_C73() { return work_Request_C73; }

    public DbsField getWork_Request_Ipro_Accum_Cntr() { return work_Request_Ipro_Accum_Cntr; }

    public DbsField getWork_Request_C74() { return work_Request_C74; }

    public DbsField getWork_Request_Tot_Accum_Cntr() { return work_Request_Tot_Accum_Cntr; }

    public DbsField getWork_Request_C75() { return work_Request_C75; }

    public DbsField getWork_Request_Multi_Prod_Cde() { return work_Request_Multi_Prod_Cde; }

    public DbsField getWork_Request_C76() { return work_Request_C76; }

    public DbsField getWork_Request_Complex_Txn_Cde() { return work_Request_Complex_Txn_Cde; }

    public DbsField getWork_Request_C77() { return work_Request_C77; }

    public DbsField getWork_Request_Part_Age() { return work_Request_Part_Age; }

    public DbsField getWork_Request_C78() { return work_Request_C78; }

    public DbsField getWork_Request_Pin_Npin() { return work_Request_Pin_Npin; }

    public DbsField getWork_Request_C79() { return work_Request_C79; }

    public DbsField getWork_Request_Pin_Type_Ind() { return work_Request_Pin_Type_Ind; }

    public DbsField getWork_Request_C80() { return work_Request_C80; }

    public DbsField getWork_Request_Rqst_Log_Dte_Tme_D() { return work_Request_Rqst_Log_Dte_Tme_D; }

    public DbsField getWork_Request_C81() { return work_Request_C81; }

    public DbsField getWork_Request_Admin_Unit_Cde() { return work_Request_Admin_Unit_Cde; }

    public DbsField getWork_Request_C82() { return work_Request_C82; }

    public DbsField getWork_Request_Last_Updte_Oprtr_Cde() { return work_Request_Last_Updte_Oprtr_Cde; }

    public DbsField getWork_Request_C83() { return work_Request_C83; }

    public DbsField getWork_Request_Due_Dte_Prty() { return work_Request_Due_Dte_Prty; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        work_Request = newGroupInRecord("work_Request", "WORK-REQUEST");
        work_Request_Rqst_Log_Dte_Tme_A = work_Request.newFieldInGroup("work_Request_Rqst_Log_Dte_Tme_A", "RQST-LOG-DTE-TME-A", FieldType.STRING, 28);
        work_Request_C1 = work_Request.newFieldInGroup("work_Request_C1", "C1", FieldType.STRING, 1);
        work_Request_Work_Prcss_Id = work_Request.newFieldInGroup("work_Request_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        work_Request_Work_Prcss_IdRedef1 = work_Request.newGroupInGroup("work_Request_Work_Prcss_IdRedef1", "Redefines", work_Request_Work_Prcss_Id);
        work_Request_Work_Actn_Rqstd_Cde = work_Request_Work_Prcss_IdRedef1.newFieldInGroup("work_Request_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        work_Request_Work_Lob_Cmpny_Prdct_Cde = work_Request_Work_Prcss_IdRedef1.newFieldInGroup("work_Request_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        work_Request_Work_Mjr_Bsnss_Prcss_Cde = work_Request_Work_Prcss_IdRedef1.newFieldInGroup("work_Request_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        work_Request_Work_Spcfc_Bsnss_Prcss_Cde = work_Request_Work_Prcss_IdRedef1.newFieldInGroup("work_Request_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        work_Request_C2 = work_Request.newFieldInGroup("work_Request_C2", "C2", FieldType.STRING, 1);
        work_Request_Pin_Nbr = work_Request.newFieldInGroup("work_Request_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        work_Request_C3 = work_Request.newFieldInGroup("work_Request_C3", "C3", FieldType.STRING, 1);
        work_Request_Part_Name = work_Request.newFieldInGroup("work_Request_Part_Name", "PART-NAME", FieldType.STRING, 40);
        work_Request_C4 = work_Request.newFieldInGroup("work_Request_C4", "C4", FieldType.STRING, 1);
        work_Request_Ssn = work_Request.newFieldInGroup("work_Request_Ssn", "SSN", FieldType.NUMERIC, 9);
        work_Request_C5 = work_Request.newFieldInGroup("work_Request_C5", "C5", FieldType.STRING, 1);
        work_Request_Tiaa_Rcvd_Dte_Tme_A = work_Request.newFieldInGroup("work_Request_Tiaa_Rcvd_Dte_Tme_A", "TIAA-RCVD-DTE-TME-A", FieldType.STRING, 28);
        work_Request_C6 = work_Request.newFieldInGroup("work_Request_C6", "C6", FieldType.STRING, 1);
        work_Request_Tiaa_Rcvd_Dte_A = work_Request.newFieldInGroup("work_Request_Tiaa_Rcvd_Dte_A", "TIAA-RCVD-DTE-A", FieldType.STRING, 12);
        work_Request_C7 = work_Request.newFieldInGroup("work_Request_C7", "C7", FieldType.STRING, 1);
        work_Request_Orgnl_Unit_Cde = work_Request.newFieldInGroup("work_Request_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        work_Request_C8 = work_Request.newFieldInGroup("work_Request_C8", "C8", FieldType.STRING, 1);
        work_Request_Rqst_Log_Oprtr_Cde = work_Request.newFieldInGroup("work_Request_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 8);
        work_Request_C9 = work_Request.newFieldInGroup("work_Request_C9", "C9", FieldType.STRING, 1);
        work_Request_Rqst_Orgn_Cde = work_Request.newFieldInGroup("work_Request_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1);
        work_Request_C10 = work_Request.newFieldInGroup("work_Request_C10", "C10", FieldType.STRING, 1);
        work_Request_Crprte_Status_Ind = work_Request.newFieldInGroup("work_Request_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1);
        work_Request_C11 = work_Request.newFieldInGroup("work_Request_C11", "C11", FieldType.STRING, 1);
        work_Request_Crprte_Clock_End_Dte_Tme_A = work_Request.newFieldInGroup("work_Request_Crprte_Clock_End_Dte_Tme_A", "CRPRTE-CLOCK-END-DTE-TME-A", 
            FieldType.STRING, 28);
        work_Request_C12 = work_Request.newFieldInGroup("work_Request_C12", "C12", FieldType.STRING, 1);
        work_Request_Final_Close_Out_Dte_Tme_A = work_Request.newFieldInGroup("work_Request_Final_Close_Out_Dte_Tme_A", "FINAL-CLOSE-OUT-DTE-TME-A", FieldType.STRING, 
            26);
        work_Request_C13 = work_Request.newFieldInGroup("work_Request_C13", "C13", FieldType.STRING, 1);
        work_Request_Effctve_Dte_A = work_Request.newFieldInGroup("work_Request_Effctve_Dte_A", "EFFCTVE-DTE-A", FieldType.STRING, 12);
        work_Request_C14 = work_Request.newFieldInGroup("work_Request_C14", "C14", FieldType.STRING, 1);
        work_Request_Trans_Dte_A = work_Request.newFieldInGroup("work_Request_Trans_Dte_A", "TRANS-DTE-A", FieldType.STRING, 12);
        work_Request_C15 = work_Request.newFieldInGroup("work_Request_C15", "C15", FieldType.STRING, 1);
        work_Request_Trnsctn_Dte_A = work_Request.newFieldInGroup("work_Request_Trnsctn_Dte_A", "TRNSCTN-DTE-A", FieldType.STRING, 12);
        work_Request_C16 = work_Request.newFieldInGroup("work_Request_C16", "C16", FieldType.STRING, 1);
        work_Request_Owner_Unit_Cde = work_Request.newFieldInGroup("work_Request_Owner_Unit_Cde", "OWNER-UNIT-CDE", FieldType.STRING, 8);
        work_Request_C17 = work_Request.newFieldInGroup("work_Request_C17", "C17", FieldType.STRING, 1);
        work_Request_Owner_Division = work_Request.newFieldInGroup("work_Request_Owner_Division", "OWNER-DIVISION", FieldType.STRING, 6);
        work_Request_C18 = work_Request.newFieldInGroup("work_Request_C18", "C18", FieldType.STRING, 1);
        work_Request_Shphrd_Id = work_Request.newFieldInGroup("work_Request_Shphrd_Id", "SHPHRD-ID", FieldType.STRING, 8);
        work_Request_C19 = work_Request.newFieldInGroup("work_Request_C19", "C19", FieldType.STRING, 1);
        work_Request_Sec_Ind = work_Request.newFieldInGroup("work_Request_Sec_Ind", "SEC-IND", FieldType.STRING, 1);
        work_Request_C20 = work_Request.newFieldInGroup("work_Request_C20", "C20", FieldType.STRING, 1);
        work_Request_Admin_Work_Ind = work_Request.newFieldInGroup("work_Request_Admin_Work_Ind", "ADMIN-WORK-IND", FieldType.STRING, 1);
        work_Request_C21 = work_Request.newFieldInGroup("work_Request_C21", "C21", FieldType.STRING, 1);
        work_Request_Sub_Rqst_Ind = work_Request.newFieldInGroup("work_Request_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 1);
        work_Request_C22 = work_Request.newFieldInGroup("work_Request_C22", "C22", FieldType.STRING, 1);
        work_Request_Prnt_Rqst_Log_Dte_Tme_A = work_Request.newFieldInGroup("work_Request_Prnt_Rqst_Log_Dte_Tme_A", "PRNT-RQST-LOG-DTE-TME-A", FieldType.STRING, 
            28);
        work_Request_C23 = work_Request.newFieldInGroup("work_Request_C23", "C23", FieldType.STRING, 1);
        work_Request_Prnt_Work_Prcss_Id = work_Request.newFieldInGroup("work_Request_Prnt_Work_Prcss_Id", "PRNT-WORK-PRCSS-ID", FieldType.STRING, 6);
        work_Request_C24 = work_Request.newFieldInGroup("work_Request_C24", "C24", FieldType.STRING, 1);
        work_Request_Multi_Rqst_Ind = work_Request.newFieldInGroup("work_Request_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 1);
        work_Request_C25 = work_Request.newFieldInGroup("work_Request_C25", "C25", FieldType.STRING, 1);
        work_Request_Cmplnt_Ind = work_Request.newFieldInGroup("work_Request_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 1);
        work_Request_C26 = work_Request.newFieldInGroup("work_Request_C26", "C26", FieldType.STRING, 1);
        work_Request_Elctrnc_Fldr_Ind = work_Request.newFieldInGroup("work_Request_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 1);
        work_Request_C27 = work_Request.newFieldInGroup("work_Request_C27", "C27", FieldType.STRING, 1);
        work_Request_Mj_Pull_Ind = work_Request.newFieldInGroup("work_Request_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1);
        work_Request_C28 = work_Request.newFieldInGroup("work_Request_C28", "C28", FieldType.STRING, 1);
        work_Request_Check_Ind = work_Request.newFieldInGroup("work_Request_Check_Ind", "CHECK-IND", FieldType.STRING, 1);
        work_Request_C29 = work_Request.newFieldInGroup("work_Request_C29", "C29", FieldType.STRING, 1);
        work_Request_Bsnss_Reply_Ind = work_Request.newFieldInGroup("work_Request_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", FieldType.STRING, 1);
        work_Request_C30 = work_Request.newFieldInGroup("work_Request_C30", "C30", FieldType.STRING, 1);
        work_Request_Tlc_Ind = work_Request.newFieldInGroup("work_Request_Tlc_Ind", "TLC-IND", FieldType.STRING, 1);
        work_Request_C31 = work_Request.newFieldInGroup("work_Request_C31", "C31", FieldType.STRING, 1);
        work_Request_Redo_Ind = work_Request.newFieldInGroup("work_Request_Redo_Ind", "REDO-IND", FieldType.STRING, 1);
        work_Request_C32 = work_Request.newFieldInGroup("work_Request_C32", "C32", FieldType.STRING, 1);
        work_Request_Crprte_On_Tme_Ind = work_Request.newFieldInGroup("work_Request_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", FieldType.STRING, 1);
        work_Request_C33 = work_Request.newFieldInGroup("work_Request_C33", "C33", FieldType.STRING, 1);
        work_Request_Off_Rtng_Ind = work_Request.newFieldInGroup("work_Request_Off_Rtng_Ind", "OFF-RTNG-IND", FieldType.STRING, 1);
        work_Request_C34 = work_Request.newFieldInGroup("work_Request_C34", "C34", FieldType.STRING, 1);
        work_Request_Prcssng_Type_Cde = work_Request.newFieldInGroup("work_Request_Prcssng_Type_Cde", "PRCSSNG-TYPE-CDE", FieldType.STRING, 1);
        work_Request_C35 = work_Request.newFieldInGroup("work_Request_C35", "C35", FieldType.STRING, 1);
        work_Request_Log_Rqstr_Cde = work_Request.newFieldInGroup("work_Request_Log_Rqstr_Cde", "LOG-RQSTR-CDE", FieldType.STRING, 1);
        work_Request_C36 = work_Request.newFieldInGroup("work_Request_C36", "C36", FieldType.STRING, 1);
        work_Request_Log_Insttn_Srce_Cde = work_Request.newFieldInGroup("work_Request_Log_Insttn_Srce_Cde", "LOG-INSTTN-SRCE-CDE", FieldType.STRING, 5);
        work_Request_C37 = work_Request.newFieldInGroup("work_Request_C37", "C37", FieldType.STRING, 1);
        work_Request_Instn_Cde = work_Request.newFieldInGroup("work_Request_Instn_Cde", "INSTN-CDE", FieldType.STRING, 5);
        work_Request_C38 = work_Request.newFieldInGroup("work_Request_C38", "C38", FieldType.STRING, 1);
        work_Request_Rqst_Instn_Cde = work_Request.newFieldInGroup("work_Request_Rqst_Instn_Cde", "RQST-INSTN-CDE", FieldType.STRING, 5);
        work_Request_C39 = work_Request.newFieldInGroup("work_Request_C39", "C39", FieldType.STRING, 1);
        work_Request_Rqst_Rgn_Cde = work_Request.newFieldInGroup("work_Request_Rqst_Rgn_Cde", "RQST-RGN-CDE", FieldType.STRING, 1);
        work_Request_C40 = work_Request.newFieldInGroup("work_Request_C40", "C40", FieldType.STRING, 1);
        work_Request_Rqst_Spcl_Dsgntn_Cde = work_Request.newFieldInGroup("work_Request_Rqst_Spcl_Dsgntn_Cde", "RQST-SPCL-DSGNTN-CDE", FieldType.STRING, 
            1);
        work_Request_C41 = work_Request.newFieldInGroup("work_Request_C41", "C41", FieldType.STRING, 1);
        work_Request_Rqst_Brnch_Cde = work_Request.newFieldInGroup("work_Request_Rqst_Brnch_Cde", "RQST-BRNCH-CDE", FieldType.STRING, 1);
        work_Request_C42 = work_Request.newFieldInGroup("work_Request_C42", "C42", FieldType.STRING, 1);
        work_Request_Extrnl_Pend_Ind = work_Request.newFieldInGroup("work_Request_Extrnl_Pend_Ind", "EXTRNL-PEND-IND", FieldType.STRING, 1);
        work_Request_C43 = work_Request.newFieldInGroup("work_Request_C43", "C43", FieldType.STRING, 1);
        work_Request_Crprte_Due_Dte_Tme_A = work_Request.newFieldInGroup("work_Request_Crprte_Due_Dte_Tme_A", "CRPRTE-DUE-DTE-TME-A", FieldType.STRING, 
            28);
        work_Request_C44 = work_Request.newFieldInGroup("work_Request_C44", "C44", FieldType.STRING, 1);
        work_Request_Mis_Routed_Ind = work_Request.newFieldInGroup("work_Request_Mis_Routed_Ind", "MIS-ROUTED-IND", FieldType.STRING, 1);
        work_Request_C45 = work_Request.newFieldInGroup("work_Request_C45", "C45", FieldType.STRING, 1);
        work_Request_Dte_Of_Birth = work_Request.newFieldInGroup("work_Request_Dte_Of_Birth", "DTE-OF-BIRTH", FieldType.STRING, 12);
        work_Request_C46 = work_Request.newFieldInGroup("work_Request_C46", "C46", FieldType.STRING, 1);
        work_Request_Trade_Dte_Tme_A = work_Request.newFieldInGroup("work_Request_Trade_Dte_Tme_A", "TRADE-DTE-TME-A", FieldType.STRING, 28);
        work_Request_C47 = work_Request.newFieldInGroup("work_Request_C47", "C47", FieldType.STRING, 1);
        work_Request_Mj_Media_Ind = work_Request.newFieldInGroup("work_Request_Mj_Media_Ind", "MJ-MEDIA-IND", FieldType.STRING, 1);
        work_Request_C48 = work_Request.newFieldInGroup("work_Request_C48", "C48", FieldType.STRING, 1);
        work_Request_State_Of_Res = work_Request.newFieldInGroup("work_Request_State_Of_Res", "STATE-OF-RES", FieldType.STRING, 2);
        work_Request_C49 = work_Request.newFieldInGroup("work_Request_C49", "C49", FieldType.STRING, 1);
        work_Request_Sec_Turnaround_Tme_A = work_Request.newFieldInGroup("work_Request_Sec_Turnaround_Tme_A", "SEC-TURNAROUND-TME-A", FieldType.STRING, 
            10);
        work_Request_C50 = work_Request.newFieldInGroup("work_Request_C50", "C50", FieldType.STRING, 1);
        work_Request_Sec_Updte_Dte_A = work_Request.newFieldInGroup("work_Request_Sec_Updte_Dte_A", "SEC-UPDTE-DTE-A", FieldType.STRING, 12);
        work_Request_C51 = work_Request.newFieldInGroup("work_Request_C51", "C51", FieldType.STRING, 1);
        work_Request_Rqst_Indicators = work_Request.newFieldInGroup("work_Request_Rqst_Indicators", "RQST-INDICATORS", FieldType.STRING, 25);
        work_Request_C52 = work_Request.newFieldInGroup("work_Request_C52", "C52", FieldType.STRING, 1);
        work_Request_Part_Bus_Days = work_Request.newFieldInGroup("work_Request_Part_Bus_Days", "PART-BUS-DAYS", FieldType.STRING, 7);
        work_Request_C53 = work_Request.newFieldInGroup("work_Request_C53", "C53", FieldType.STRING, 1);
        work_Request_Part_Cal_Days = work_Request.newFieldInGroup("work_Request_Part_Cal_Days", "PART-CAL-DAYS", FieldType.STRING, 7);
        work_Request_C54 = work_Request.newFieldInGroup("work_Request_C54", "C54", FieldType.STRING, 1);
        work_Request_Start_Dte_Type = work_Request.newFieldInGroup("work_Request_Start_Dte_Type", "START-DTE-TYPE", FieldType.STRING, 1);
        work_Request_C55 = work_Request.newFieldInGroup("work_Request_C55", "C55", FieldType.STRING, 1);
        work_Request_Acknldg_Dte_Tme_A = work_Request.newFieldInGroup("work_Request_Acknldg_Dte_Tme_A", "ACKNLDG-DTE-TME-A", FieldType.STRING, 28);
        work_Request_C56 = work_Request.newFieldInGroup("work_Request_C56", "C56", FieldType.STRING, 1);
        work_Request_Acknldg_Tat_Tme = work_Request.newFieldInGroup("work_Request_Acknldg_Tat_Tme", "ACKNLDG-TAT-TME", FieldType.STRING, 10);
        work_Request_C57 = work_Request.newFieldInGroup("work_Request_C57", "C57", FieldType.STRING, 1);
        work_Request_Acknldg_Reqd_Ind = work_Request.newFieldInGroup("work_Request_Acknldg_Reqd_Ind", "ACKNLDG-REQD-IND", FieldType.STRING, 1);
        work_Request_C58 = work_Request.newFieldInGroup("work_Request_C58", "C58", FieldType.STRING, 1);
        work_Request_Acknldg_On_Time_Ind = work_Request.newFieldInGroup("work_Request_Acknldg_On_Time_Ind", "ACKNLDG-ON-TIME-IND", FieldType.STRING, 1);
        work_Request_C59 = work_Request.newFieldInGroup("work_Request_C59", "C59", FieldType.STRING, 1);
        work_Request_Last_Chnge_Dte_Tme_A = work_Request.newFieldInGroup("work_Request_Last_Chnge_Dte_Tme_A", "LAST-CHNGE-DTE-TME-A", FieldType.STRING, 
            28);
        work_Request_C60 = work_Request.newFieldInGroup("work_Request_C60", "C60", FieldType.STRING, 1);
        work_Request_Part_Close_Status_A = work_Request.newFieldInGroup("work_Request_Part_Close_Status_A", "PART-CLOSE-STATUS-A", FieldType.STRING, 4);
        work_Request_C61 = work_Request.newFieldInGroup("work_Request_C61", "C61", FieldType.STRING, 1);
        work_Request_Cnnctd_Rqst_Ind = work_Request.newFieldInGroup("work_Request_Cnnctd_Rqst_Ind", "CNNCTD-RQST-IND", FieldType.STRING, 2);
        work_Request_C62 = work_Request.newFieldInGroup("work_Request_C62", "C62", FieldType.STRING, 1);
        work_Request_Prvte_Wrk_Rqst_Ind = work_Request.newFieldInGroup("work_Request_Prvte_Wrk_Rqst_Ind", "PRVTE-WRK-RQST-IND", FieldType.STRING, 1);
        work_Request_C63 = work_Request.newFieldInGroup("work_Request_C63", "C63", FieldType.STRING, 1);
        work_Request_Instn_Rqst_Log_Dte_Tme_A = work_Request.newFieldInGroup("work_Request_Instn_Rqst_Log_Dte_Tme_A", "INSTN-RQST-LOG-DTE-TME-A", FieldType.STRING, 
            28);
        work_Request_C64 = work_Request.newFieldInGroup("work_Request_C64", "C64", FieldType.STRING, 1);
        work_Request_Log_Sqnce_Nbr = work_Request.newFieldInGroup("work_Request_Log_Sqnce_Nbr", "LOG-SQNCE-NBR", FieldType.NUMERIC, 3);
        work_Request_C65 = work_Request.newFieldInGroup("work_Request_C65", "C65", FieldType.STRING, 1);
        work_Request_Start_Dte_Tme_A = work_Request.newFieldInGroup("work_Request_Start_Dte_Tme_A", "START-DTE-TME-A", FieldType.STRING, 28);
        work_Request_C66 = work_Request.newFieldInGroup("work_Request_C66", "C66", FieldType.STRING, 1);
        work_Request_Admin_Status_Cde = work_Request.newFieldInGroup("work_Request_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4);
        work_Request_C67 = work_Request.newFieldInGroup("work_Request_C67", "C67", FieldType.STRING, 1);
        work_Request_Crrnt_Cmt_Ind = work_Request.newFieldInGroup("work_Request_Crrnt_Cmt_Ind", "CRRNT-CMT-IND", FieldType.STRING, 1);
        work_Request_C68 = work_Request.newFieldInGroup("work_Request_C68", "C68", FieldType.STRING, 1);
        work_Request_Np_Pin = work_Request.newFieldInGroup("work_Request_Np_Pin", "NP-PIN", FieldType.STRING, 7);
        work_Request_C69 = work_Request.newFieldInGroup("work_Request_C69", "C69", FieldType.STRING, 1);
        work_Request_Dod_Not_Dte = work_Request.newFieldInGroup("work_Request_Dod_Not_Dte", "DOD-NOT-DTE", FieldType.STRING, 10);
        work_Request_C70 = work_Request.newFieldInGroup("work_Request_C70", "C70", FieldType.STRING, 1);
        work_Request_Da_Accum_Cntr = work_Request.newFieldInGroup("work_Request_Da_Accum_Cntr", "DA-ACCUM-CNTR", FieldType.STRING, 14);
        work_Request_C71 = work_Request.newFieldInGroup("work_Request_C71", "C71", FieldType.STRING, 1);
        work_Request_Tpa_Accum_Cntr = work_Request.newFieldInGroup("work_Request_Tpa_Accum_Cntr", "TPA-ACCUM-CNTR", FieldType.STRING, 14);
        work_Request_C72 = work_Request.newFieldInGroup("work_Request_C72", "C72", FieldType.STRING, 1);
        work_Request_P_I_Accum_Cntr = work_Request.newFieldInGroup("work_Request_P_I_Accum_Cntr", "P-I-ACCUM-CNTR", FieldType.STRING, 14);
        work_Request_C73 = work_Request.newFieldInGroup("work_Request_C73", "C73", FieldType.STRING, 1);
        work_Request_Ipro_Accum_Cntr = work_Request.newFieldInGroup("work_Request_Ipro_Accum_Cntr", "IPRO-ACCUM-CNTR", FieldType.STRING, 14);
        work_Request_C74 = work_Request.newFieldInGroup("work_Request_C74", "C74", FieldType.STRING, 1);
        work_Request_Tot_Accum_Cntr = work_Request.newFieldInGroup("work_Request_Tot_Accum_Cntr", "TOT-ACCUM-CNTR", FieldType.STRING, 14);
        work_Request_C75 = work_Request.newFieldInGroup("work_Request_C75", "C75", FieldType.STRING, 1);
        work_Request_Multi_Prod_Cde = work_Request.newFieldInGroup("work_Request_Multi_Prod_Cde", "MULTI-PROD-CDE", FieldType.STRING, 5);
        work_Request_C76 = work_Request.newFieldInGroup("work_Request_C76", "C76", FieldType.STRING, 1);
        work_Request_Complex_Txn_Cde = work_Request.newFieldInGroup("work_Request_Complex_Txn_Cde", "COMPLEX-TXN-CDE", FieldType.STRING, 5);
        work_Request_C77 = work_Request.newFieldInGroup("work_Request_C77", "C77", FieldType.STRING, 1);
        work_Request_Part_Age = work_Request.newFieldInGroup("work_Request_Part_Age", "PART-AGE", FieldType.NUMERIC, 3);
        work_Request_C78 = work_Request.newFieldInGroup("work_Request_C78", "C78", FieldType.STRING, 1);
        work_Request_Pin_Npin = work_Request.newFieldInGroup("work_Request_Pin_Npin", "PIN-NPIN", FieldType.STRING, 12);
        work_Request_C79 = work_Request.newFieldInGroup("work_Request_C79", "C79", FieldType.STRING, 1);
        work_Request_Pin_Type_Ind = work_Request.newFieldInGroup("work_Request_Pin_Type_Ind", "PIN-TYPE-IND", FieldType.STRING, 1);
        work_Request_C80 = work_Request.newFieldInGroup("work_Request_C80", "C80", FieldType.STRING, 1);
        work_Request_Rqst_Log_Dte_Tme_D = work_Request.newFieldInGroup("work_Request_Rqst_Log_Dte_Tme_D", "RQST-LOG-DTE-TME-D", FieldType.STRING, 24);
        work_Request_C81 = work_Request.newFieldInGroup("work_Request_C81", "C81", FieldType.STRING, 1);
        work_Request_Admin_Unit_Cde = work_Request.newFieldInGroup("work_Request_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        work_Request_C82 = work_Request.newFieldInGroup("work_Request_C82", "C82", FieldType.STRING, 1);
        work_Request_Last_Updte_Oprtr_Cde = work_Request.newFieldInGroup("work_Request_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        work_Request_C83 = work_Request.newFieldInGroup("work_Request_C83", "C83", FieldType.STRING, 1);
        work_Request_Due_Dte_Prty = work_Request.newFieldInGroup("work_Request_Due_Dte_Prty", "DUE-DTE-PRTY", FieldType.STRING, 1);

        this.setRecordName("LdaCwfl4819");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4819() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
