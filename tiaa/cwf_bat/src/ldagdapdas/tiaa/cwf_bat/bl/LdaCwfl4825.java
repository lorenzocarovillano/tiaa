/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:47 PM
**        * FROM NATURAL LDA     : CWFL4825
************************************************************
**        * FILE NAME            : LdaCwfl4825.java
**        * CLASS NAME           : LdaCwfl4825
**        * INSTANCE NAME        : LdaCwfl4825
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4825 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsGroup cwf_Support_Tbl_Tbl_Key_FieldRedef1;
    private DbsField cwf_Support_Tbl_Run_Status;
    private DbsField cwf_Support_Tbl_Starting_Date_9s_Complement;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;
    private DbsGroup cwf_Support_Tbl_Tbl_Data_FieldRedef2;
    private DbsField cwf_Support_Tbl_Starting_Date;
    private DbsField cwf_Support_Tbl_Ending_Date;
    private DbsField cwf_Support_Tbl_Program_Name;
    private DbsField cwf_Support_Tbl_Run_Id;
    private DbsField cwf_Support_Tbl_Number_Of_Ncw_Wr_Selected;
    private DbsField cwf_Support_Tbl_Number_Of_Recs_Processed;
    private DbsField cwf_Support_Tbl_Number_Of_Wr_Added;
    private DbsField cwf_Support_Tbl_Number_Of_Wr_Updtd;
    private DbsField cwf_Support_Tbl_Number_Of_Unit_Act_Added;
    private DbsField cwf_Support_Tbl_Number_Of_Unit_Act_Updtd;
    private DbsField cwf_Support_Tbl_Number_Of_Empl_Act_Added;
    private DbsField cwf_Support_Tbl_Number_Of_Empl_Act_Updtd;
    private DbsField cwf_Support_Tbl_Number_Of_Step_Act_Added;
    private DbsField cwf_Support_Tbl_Number_Of_Step_Act_Updtd;
    private DbsField cwf_Support_Tbl_Number_Of_Stat_Act_Added;
    private DbsField cwf_Support_Tbl_Number_Of_Stat_Act_Updtd;
    private DbsField cwf_Support_Tbl_Number_Of_Extr_Act_Added;
    private DbsField cwf_Support_Tbl_Number_Of_Extr_Act_Updtd;
    private DbsField cwf_Support_Tbl_Number_Of_Intr_Act_Added;
    private DbsField cwf_Support_Tbl_Number_Of_Intr_Act_Updtd;
    private DbsField cwf_Support_Tbl_Number_Of_Enrt_Act_Added;
    private DbsField cwf_Support_Tbl_Number_Of_Enrt_Act_Updtd;
    private DbsField cwf_Support_Tbl_Number_Of_Wr_Deleted;
    private DbsField cwf_Support_Tbl_Run_Date;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Tbl_Table_Access_Level;

    public DataAccessProgramView getVw_cwf_Support_Tbl() { return vw_cwf_Support_Tbl; }

    public DbsField getCwf_Support_Tbl_Tbl_Scrty_Level_Ind() { return cwf_Support_Tbl_Tbl_Scrty_Level_Ind; }

    public DbsField getCwf_Support_Tbl_Tbl_Table_Nme() { return cwf_Support_Tbl_Tbl_Table_Nme; }

    public DbsField getCwf_Support_Tbl_Tbl_Key_Field() { return cwf_Support_Tbl_Tbl_Key_Field; }

    public DbsGroup getCwf_Support_Tbl_Tbl_Key_FieldRedef1() { return cwf_Support_Tbl_Tbl_Key_FieldRedef1; }

    public DbsField getCwf_Support_Tbl_Run_Status() { return cwf_Support_Tbl_Run_Status; }

    public DbsField getCwf_Support_Tbl_Starting_Date_9s_Complement() { return cwf_Support_Tbl_Starting_Date_9s_Complement; }

    public DbsField getCwf_Support_Tbl_Tbl_Data_Field() { return cwf_Support_Tbl_Tbl_Data_Field; }

    public DbsGroup getCwf_Support_Tbl_Tbl_Data_FieldRedef2() { return cwf_Support_Tbl_Tbl_Data_FieldRedef2; }

    public DbsField getCwf_Support_Tbl_Starting_Date() { return cwf_Support_Tbl_Starting_Date; }

    public DbsField getCwf_Support_Tbl_Ending_Date() { return cwf_Support_Tbl_Ending_Date; }

    public DbsField getCwf_Support_Tbl_Program_Name() { return cwf_Support_Tbl_Program_Name; }

    public DbsField getCwf_Support_Tbl_Run_Id() { return cwf_Support_Tbl_Run_Id; }

    public DbsField getCwf_Support_Tbl_Number_Of_Ncw_Wr_Selected() { return cwf_Support_Tbl_Number_Of_Ncw_Wr_Selected; }

    public DbsField getCwf_Support_Tbl_Number_Of_Recs_Processed() { return cwf_Support_Tbl_Number_Of_Recs_Processed; }

    public DbsField getCwf_Support_Tbl_Number_Of_Wr_Added() { return cwf_Support_Tbl_Number_Of_Wr_Added; }

    public DbsField getCwf_Support_Tbl_Number_Of_Wr_Updtd() { return cwf_Support_Tbl_Number_Of_Wr_Updtd; }

    public DbsField getCwf_Support_Tbl_Number_Of_Unit_Act_Added() { return cwf_Support_Tbl_Number_Of_Unit_Act_Added; }

    public DbsField getCwf_Support_Tbl_Number_Of_Unit_Act_Updtd() { return cwf_Support_Tbl_Number_Of_Unit_Act_Updtd; }

    public DbsField getCwf_Support_Tbl_Number_Of_Empl_Act_Added() { return cwf_Support_Tbl_Number_Of_Empl_Act_Added; }

    public DbsField getCwf_Support_Tbl_Number_Of_Empl_Act_Updtd() { return cwf_Support_Tbl_Number_Of_Empl_Act_Updtd; }

    public DbsField getCwf_Support_Tbl_Number_Of_Step_Act_Added() { return cwf_Support_Tbl_Number_Of_Step_Act_Added; }

    public DbsField getCwf_Support_Tbl_Number_Of_Step_Act_Updtd() { return cwf_Support_Tbl_Number_Of_Step_Act_Updtd; }

    public DbsField getCwf_Support_Tbl_Number_Of_Stat_Act_Added() { return cwf_Support_Tbl_Number_Of_Stat_Act_Added; }

    public DbsField getCwf_Support_Tbl_Number_Of_Stat_Act_Updtd() { return cwf_Support_Tbl_Number_Of_Stat_Act_Updtd; }

    public DbsField getCwf_Support_Tbl_Number_Of_Extr_Act_Added() { return cwf_Support_Tbl_Number_Of_Extr_Act_Added; }

    public DbsField getCwf_Support_Tbl_Number_Of_Extr_Act_Updtd() { return cwf_Support_Tbl_Number_Of_Extr_Act_Updtd; }

    public DbsField getCwf_Support_Tbl_Number_Of_Intr_Act_Added() { return cwf_Support_Tbl_Number_Of_Intr_Act_Added; }

    public DbsField getCwf_Support_Tbl_Number_Of_Intr_Act_Updtd() { return cwf_Support_Tbl_Number_Of_Intr_Act_Updtd; }

    public DbsField getCwf_Support_Tbl_Number_Of_Enrt_Act_Added() { return cwf_Support_Tbl_Number_Of_Enrt_Act_Added; }

    public DbsField getCwf_Support_Tbl_Number_Of_Enrt_Act_Updtd() { return cwf_Support_Tbl_Number_Of_Enrt_Act_Updtd; }

    public DbsField getCwf_Support_Tbl_Number_Of_Wr_Deleted() { return cwf_Support_Tbl_Number_Of_Wr_Deleted; }

    public DbsField getCwf_Support_Tbl_Run_Date() { return cwf_Support_Tbl_Run_Date; }

    public DbsField getCwf_Support_Tbl_Tbl_Actve_Ind() { return cwf_Support_Tbl_Tbl_Actve_Ind; }

    public DbsField getCwf_Support_Tbl_Tbl_Entry_Dte_Tme() { return cwf_Support_Tbl_Tbl_Entry_Dte_Tme; }

    public DbsField getCwf_Support_Tbl_Tbl_Entry_Oprtr_Cde() { return cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde; }

    public DbsField getCwf_Support_Tbl_Tbl_Updte_Dte_Tme() { return cwf_Support_Tbl_Tbl_Updte_Dte_Tme; }

    public DbsField getCwf_Support_Tbl_Tbl_Updte_Dte() { return cwf_Support_Tbl_Tbl_Updte_Dte; }

    public DbsField getCwf_Support_Tbl_Tbl_Updte_Oprtr_Cde() { return cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde; }

    public DbsField getCwf_Support_Tbl_Tbl_Dlte_Dte_Tme() { return cwf_Support_Tbl_Tbl_Dlte_Dte_Tme; }

    public DbsField getCwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde() { return cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde; }

    public DbsField getCwf_Support_Tbl_Tbl_Table_Rectype() { return cwf_Support_Tbl_Tbl_Table_Rectype; }

    public DbsField getCwf_Support_Tbl_Tbl_Table_Access_Level() { return cwf_Support_Tbl_Tbl_Table_Access_Level; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Key_FieldRedef1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl_Tbl_Key_FieldRedef1", "Redefines", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Run_Status = cwf_Support_Tbl_Tbl_Key_FieldRedef1.newFieldInGroup("cwf_Support_Tbl_Run_Status", "RUN-STATUS", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Starting_Date_9s_Complement = cwf_Support_Tbl_Tbl_Key_FieldRedef1.newFieldInGroup("cwf_Support_Tbl_Starting_Date_9s_Complement", 
            "STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 15);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        cwf_Support_Tbl_Tbl_Data_FieldRedef2 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl_Tbl_Data_FieldRedef2", "Redefines", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Starting_Date = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Starting_Date", "STARTING-DATE", FieldType.TIME);
        cwf_Support_Tbl_Ending_Date = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Ending_Date", "ENDING-DATE", FieldType.TIME);
        cwf_Support_Tbl_Program_Name = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Program_Name", "PROGRAM-NAME", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Run_Id = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Run_Id", "RUN-ID", FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Ncw_Wr_Selected = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Ncw_Wr_Selected", 
            "NUMBER-OF-NCW-WR-SELECTED", FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Recs_Processed = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Recs_Processed", "NUMBER-OF-RECS-PROCESSED", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Wr_Added = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Wr_Added", "NUMBER-OF-WR-ADDED", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Wr_Updtd = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Wr_Updtd", "NUMBER-OF-WR-UPDTD", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Unit_Act_Added = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Unit_Act_Added", "NUMBER-OF-UNIT-ACT-ADDED", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Unit_Act_Updtd = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Unit_Act_Updtd", "NUMBER-OF-UNIT-ACT-UPDTD", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Empl_Act_Added = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Empl_Act_Added", "NUMBER-OF-EMPL-ACT-ADDED", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Empl_Act_Updtd = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Empl_Act_Updtd", "NUMBER-OF-EMPL-ACT-UPDTD", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Step_Act_Added = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Step_Act_Added", "NUMBER-OF-STEP-ACT-ADDED", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Step_Act_Updtd = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Step_Act_Updtd", "NUMBER-OF-STEP-ACT-UPDTD", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Stat_Act_Added = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Stat_Act_Added", "NUMBER-OF-STAT-ACT-ADDED", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Stat_Act_Updtd = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Stat_Act_Updtd", "NUMBER-OF-STAT-ACT-UPDTD", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Extr_Act_Added = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Extr_Act_Added", "NUMBER-OF-EXTR-ACT-ADDED", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Extr_Act_Updtd = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Extr_Act_Updtd", "NUMBER-OF-EXTR-ACT-UPDTD", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Intr_Act_Added = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Intr_Act_Added", "NUMBER-OF-INTR-ACT-ADDED", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Intr_Act_Updtd = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Intr_Act_Updtd", "NUMBER-OF-INTR-ACT-UPDTD", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Enrt_Act_Added = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Enrt_Act_Added", "NUMBER-OF-ENRT-ACT-ADDED", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Enrt_Act_Updtd = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Enrt_Act_Updtd", "NUMBER-OF-ENRT-ACT-UPDTD", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Number_Of_Wr_Deleted = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Number_Of_Wr_Deleted", "NUMBER-OF-WR-DELETED", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Run_Date = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Run_Date", "RUN-DATE", FieldType.TIME);
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");

        this.setRecordName("LdaCwfl4825");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cwf_Support_Tbl.reset();
    }

    // Constructor
    public LdaCwfl4825() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
