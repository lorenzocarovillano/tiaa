/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:05:13 PM
**        * FROM NATURAL LDA     : ICWL3016
************************************************************
**        * FILE NAME            : LdaIcwl3016.java
**        * CLASS NAME           : LdaIcwl3016
**        * INSTANCE NAME        : LdaIcwl3016
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIcwl3016 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iis_Orgnztn_Hrchy_View;
    private DbsGroup iis_Orgnztn_Hrchy_View_Audit_Grp;
    private DbsField iis_Orgnztn_Hrchy_View_Audit_Mdfy_By;
    private DbsField iis_Orgnztn_Hrchy_View_Audit_Last_Mdfy_Dte;
    private DbsField iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Lvl_Cde;
    private DbsField iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Link_Public_Cde;
    private DbsField iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Link_System_Cde;
    private DbsField iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Key_Cde;
    private DbsField iis_Orgnztn_Hrchy_View_Orgnztn_Legal_Nme;
    private DbsField iis_Orgnztn_Hrchy_View_Orgnztn_Spcl_Needs_Ind;
    private DbsField iis_Orgnztn_Hrchy_View_Orgnztn_Prty_Ind;
    private DbsField iis_Orgnztn_Hrchy_View_Tiaa_Brnch_Cde;
    private DbsField iis_Orgnztn_Hrchy_View_State_Cde;

    public DataAccessProgramView getVw_iis_Orgnztn_Hrchy_View() { return vw_iis_Orgnztn_Hrchy_View; }

    public DbsGroup getIis_Orgnztn_Hrchy_View_Audit_Grp() { return iis_Orgnztn_Hrchy_View_Audit_Grp; }

    public DbsField getIis_Orgnztn_Hrchy_View_Audit_Mdfy_By() { return iis_Orgnztn_Hrchy_View_Audit_Mdfy_By; }

    public DbsField getIis_Orgnztn_Hrchy_View_Audit_Last_Mdfy_Dte() { return iis_Orgnztn_Hrchy_View_Audit_Last_Mdfy_Dte; }

    public DbsField getIis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Lvl_Cde() { return iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Lvl_Cde; }

    public DbsField getIis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Link_Public_Cde() { return iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Link_Public_Cde; }

    public DbsField getIis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Link_System_Cde() { return iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Link_System_Cde; }

    public DbsField getIis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Key_Cde() { return iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Key_Cde; }

    public DbsField getIis_Orgnztn_Hrchy_View_Orgnztn_Legal_Nme() { return iis_Orgnztn_Hrchy_View_Orgnztn_Legal_Nme; }

    public DbsField getIis_Orgnztn_Hrchy_View_Orgnztn_Spcl_Needs_Ind() { return iis_Orgnztn_Hrchy_View_Orgnztn_Spcl_Needs_Ind; }

    public DbsField getIis_Orgnztn_Hrchy_View_Orgnztn_Prty_Ind() { return iis_Orgnztn_Hrchy_View_Orgnztn_Prty_Ind; }

    public DbsField getIis_Orgnztn_Hrchy_View_Tiaa_Brnch_Cde() { return iis_Orgnztn_Hrchy_View_Tiaa_Brnch_Cde; }

    public DbsField getIis_Orgnztn_Hrchy_View_State_Cde() { return iis_Orgnztn_Hrchy_View_State_Cde; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iis_Orgnztn_Hrchy_View = new DataAccessProgramView(new NameInfo("vw_iis_Orgnztn_Hrchy_View", "IIS-ORGNZTN-HRCHY-VIEW"), "IIS_ORGNZTN_HRCHY", 
            "IIS_ORG_HRCY_BRH");
        iis_Orgnztn_Hrchy_View_Audit_Grp = vw_iis_Orgnztn_Hrchy_View.getRecord().newGroupInGroup("iis_Orgnztn_Hrchy_View_Audit_Grp", "AUDIT-GRP");
        iis_Orgnztn_Hrchy_View_Audit_Mdfy_By = iis_Orgnztn_Hrchy_View_Audit_Grp.newFieldInGroup("iis_Orgnztn_Hrchy_View_Audit_Mdfy_By", "AUDIT-MDFY-BY", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AUDIT_MDFY_BY");
        iis_Orgnztn_Hrchy_View_Audit_Mdfy_By.setDdmHeader("MODIFIED/BY");
        iis_Orgnztn_Hrchy_View_Audit_Last_Mdfy_Dte = iis_Orgnztn_Hrchy_View_Audit_Grp.newFieldInGroup("iis_Orgnztn_Hrchy_View_Audit_Last_Mdfy_Dte", "AUDIT-LAST-MDFY-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "AUDIT_LAST_MDFY_DTE");
        iis_Orgnztn_Hrchy_View_Audit_Last_Mdfy_Dte.setDdmHeader("LAST MODIFIED");
        iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Lvl_Cde = vw_iis_Orgnztn_Hrchy_View.getRecord().newFieldInGroup("iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Lvl_Cde", 
            "ORGNZTN-HRCHY-LVL-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ORGNZTN_HRCHY_LVL_CDE");
        iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Lvl_Cde.setDdmHeader("HIERARCHY/LEVEL CODE");
        iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Link_Public_Cde = vw_iis_Orgnztn_Hrchy_View.getRecord().newFieldInGroup("iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Link_Public_Cde", 
            "ORGNZTN-HRCHY-LINK-PUBLIC-CDE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ORGNZTN_HRCHY_LINK_PUBLIC_CDE");
        iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Link_Public_Cde.setDdmHeader("HIERARCHY/LINK PUBLIC/CODE");
        iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Link_System_Cde = vw_iis_Orgnztn_Hrchy_View.getRecord().newFieldInGroup("iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Link_System_Cde", 
            "ORGNZTN-HRCHY-LINK-SYSTEM-CDE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ORGNZTN_HRCHY_LINK_SYSTEM_CDE");
        iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Link_System_Cde.setDdmHeader("HIERARCHY/LINK SYSTEM/CODE");
        iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Key_Cde = vw_iis_Orgnztn_Hrchy_View.getRecord().newFieldInGroup("iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Key_Cde", 
            "ORGNZTN-HRCHY-KEY-CDE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ORGNZTN_HRCHY_KEY_CDE");
        iis_Orgnztn_Hrchy_View_Orgnztn_Hrchy_Key_Cde.setDdmHeader("HIERARCHY/KEY CODE");
        iis_Orgnztn_Hrchy_View_Orgnztn_Legal_Nme = vw_iis_Orgnztn_Hrchy_View.getRecord().newFieldInGroup("iis_Orgnztn_Hrchy_View_Orgnztn_Legal_Nme", "ORGNZTN-LEGAL-NME", 
            FieldType.STRING, 150, RepeatingFieldStrategy.None, "ORGNZTN_LEGAL_NME");
        iis_Orgnztn_Hrchy_View_Orgnztn_Legal_Nme.setDdmHeader("LEGAL NAME");
        iis_Orgnztn_Hrchy_View_Orgnztn_Spcl_Needs_Ind = vw_iis_Orgnztn_Hrchy_View.getRecord().newFieldInGroup("iis_Orgnztn_Hrchy_View_Orgnztn_Spcl_Needs_Ind", 
            "ORGNZTN-SPCL-NEEDS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ORGNZTN_SPCL_NEEDS_IND");
        iis_Orgnztn_Hrchy_View_Orgnztn_Spcl_Needs_Ind.setDdmHeader("SPECIAL/NEEDS IND");
        iis_Orgnztn_Hrchy_View_Orgnztn_Prty_Ind = vw_iis_Orgnztn_Hrchy_View.getRecord().newFieldInGroup("iis_Orgnztn_Hrchy_View_Orgnztn_Prty_Ind", "ORGNZTN-PRTY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ORGNZTN_PRTY_IND");
        iis_Orgnztn_Hrchy_View_Orgnztn_Prty_Ind.setDdmHeader("PRIORITY/IND");
        iis_Orgnztn_Hrchy_View_Tiaa_Brnch_Cde = vw_iis_Orgnztn_Hrchy_View.getRecord().newFieldInGroup("iis_Orgnztn_Hrchy_View_Tiaa_Brnch_Cde", "TIAA-BRNCH-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_BRNCH_CDE");
        iis_Orgnztn_Hrchy_View_Tiaa_Brnch_Cde.setDdmHeader("TIAA BRANCH/CODE");
        iis_Orgnztn_Hrchy_View_State_Cde = vw_iis_Orgnztn_Hrchy_View.getRecord().newFieldInGroup("iis_Orgnztn_Hrchy_View_State_Cde", "STATE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "STATE_CDE");
        iis_Orgnztn_Hrchy_View_State_Cde.setDdmHeader("STATE/CODE");

        this.setRecordName("LdaIcwl3016");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iis_Orgnztn_Hrchy_View.reset();
    }

    // Constructor
    public LdaIcwl3016() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
