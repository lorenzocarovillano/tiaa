/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:57 PM
**        * FROM NATURAL LDA     : CWFL4898
************************************************************
**        * FILE NAME            : LdaCwfl4898.java
**        * CLASS NAME           : LdaCwfl4898
**        * INSTANCE NAME        : LdaCwfl4898
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4898 extends DbsRecord
{
    // Properties
    private DbsGroup work_Lead_New;
    private DbsField work_Lead_New_Pin_Npin;
    private DbsField work_Lead_New_C1;
    private DbsField work_Lead_New_Rqst_Log_Dte_Tme_A_15;
    private DbsField work_Lead_New_C2;
    private DbsField work_Lead_New_Last_Chnge_Dte_Tme_A;
    private DbsField work_Lead_New_C3;
    private DbsField work_Lead_New_Work_Prcss_Id;
    private DbsField work_Lead_New_C4;
    private DbsField work_Lead_New_Tiaa_Rcvd_Dte_Tme_A;
    private DbsField work_Lead_New_C5;
    private DbsField work_Lead_New_Rqst_Log_Oprtr_Cde;
    private DbsField work_Lead_New_C6;
    private DbsField work_Lead_New_Orgnl_Unit_Cde;
    private DbsField work_Lead_New_C7;
    private DbsField work_Lead_New_Admin_Unit_Cde;
    private DbsField work_Lead_New_C8;
    private DbsField work_Lead_New_Admin_Status_Cde;
    private DbsField work_Lead_New_C9;
    private DbsField work_Lead_New_Document_Type;
    private DbsField work_Lead_New_C10;
    private DbsField work_Lead_New_Empl_Oprtr_Cde;
    private DbsField work_Lead_New_C11;
    private DbsField work_Lead_New_Crprte_Due_Dte_Tme_A;
    private DbsField work_Lead_New_C12;
    private DbsField work_Lead_New_Crprte_Status_Ind;
    private DbsField work_Lead_New_C13;
    private DbsField work_Lead_New_Part_Close_Status_A;
    private DbsField work_Lead_New_C14;
    private DbsField work_Lead_New_Crprte_Clock_End_Dte_Tme_A;
    private DbsField work_Lead_New_C15;
    private DbsField work_Lead_New_Moc;
    private DbsField work_Lead_New_C16;
    private DbsField work_Lead_New_Part_Age;
    private DbsField work_Lead_New_C17;
    private DbsField work_Lead_New_Last_Chnge_Oprtr_Cde;
    private DbsField work_Lead_New_C18;
    private DbsField work_Lead_New_Crprte_Rqst_Log_Dte_Tme_A;
    private DbsField work_Lead_New_C19;
    private DbsField work_Lead_New_Actve_Ind;
    private DbsField work_Lead_New_C20;
    private DbsField work_Lead_New_Status_Cde;
    private DbsField work_Lead_New_C21;
    private DbsField work_Lead_New_Admin_Status_Updte_Dte_Tme_A;

    public DbsGroup getWork_Lead_New() { return work_Lead_New; }

    public DbsField getWork_Lead_New_Pin_Npin() { return work_Lead_New_Pin_Npin; }

    public DbsField getWork_Lead_New_C1() { return work_Lead_New_C1; }

    public DbsField getWork_Lead_New_Rqst_Log_Dte_Tme_A_15() { return work_Lead_New_Rqst_Log_Dte_Tme_A_15; }

    public DbsField getWork_Lead_New_C2() { return work_Lead_New_C2; }

    public DbsField getWork_Lead_New_Last_Chnge_Dte_Tme_A() { return work_Lead_New_Last_Chnge_Dte_Tme_A; }

    public DbsField getWork_Lead_New_C3() { return work_Lead_New_C3; }

    public DbsField getWork_Lead_New_Work_Prcss_Id() { return work_Lead_New_Work_Prcss_Id; }

    public DbsField getWork_Lead_New_C4() { return work_Lead_New_C4; }

    public DbsField getWork_Lead_New_Tiaa_Rcvd_Dte_Tme_A() { return work_Lead_New_Tiaa_Rcvd_Dte_Tme_A; }

    public DbsField getWork_Lead_New_C5() { return work_Lead_New_C5; }

    public DbsField getWork_Lead_New_Rqst_Log_Oprtr_Cde() { return work_Lead_New_Rqst_Log_Oprtr_Cde; }

    public DbsField getWork_Lead_New_C6() { return work_Lead_New_C6; }

    public DbsField getWork_Lead_New_Orgnl_Unit_Cde() { return work_Lead_New_Orgnl_Unit_Cde; }

    public DbsField getWork_Lead_New_C7() { return work_Lead_New_C7; }

    public DbsField getWork_Lead_New_Admin_Unit_Cde() { return work_Lead_New_Admin_Unit_Cde; }

    public DbsField getWork_Lead_New_C8() { return work_Lead_New_C8; }

    public DbsField getWork_Lead_New_Admin_Status_Cde() { return work_Lead_New_Admin_Status_Cde; }

    public DbsField getWork_Lead_New_C9() { return work_Lead_New_C9; }

    public DbsField getWork_Lead_New_Document_Type() { return work_Lead_New_Document_Type; }

    public DbsField getWork_Lead_New_C10() { return work_Lead_New_C10; }

    public DbsField getWork_Lead_New_Empl_Oprtr_Cde() { return work_Lead_New_Empl_Oprtr_Cde; }

    public DbsField getWork_Lead_New_C11() { return work_Lead_New_C11; }

    public DbsField getWork_Lead_New_Crprte_Due_Dte_Tme_A() { return work_Lead_New_Crprte_Due_Dte_Tme_A; }

    public DbsField getWork_Lead_New_C12() { return work_Lead_New_C12; }

    public DbsField getWork_Lead_New_Crprte_Status_Ind() { return work_Lead_New_Crprte_Status_Ind; }

    public DbsField getWork_Lead_New_C13() { return work_Lead_New_C13; }

    public DbsField getWork_Lead_New_Part_Close_Status_A() { return work_Lead_New_Part_Close_Status_A; }

    public DbsField getWork_Lead_New_C14() { return work_Lead_New_C14; }

    public DbsField getWork_Lead_New_Crprte_Clock_End_Dte_Tme_A() { return work_Lead_New_Crprte_Clock_End_Dte_Tme_A; }

    public DbsField getWork_Lead_New_C15() { return work_Lead_New_C15; }

    public DbsField getWork_Lead_New_Moc() { return work_Lead_New_Moc; }

    public DbsField getWork_Lead_New_C16() { return work_Lead_New_C16; }

    public DbsField getWork_Lead_New_Part_Age() { return work_Lead_New_Part_Age; }

    public DbsField getWork_Lead_New_C17() { return work_Lead_New_C17; }

    public DbsField getWork_Lead_New_Last_Chnge_Oprtr_Cde() { return work_Lead_New_Last_Chnge_Oprtr_Cde; }

    public DbsField getWork_Lead_New_C18() { return work_Lead_New_C18; }

    public DbsField getWork_Lead_New_Crprte_Rqst_Log_Dte_Tme_A() { return work_Lead_New_Crprte_Rqst_Log_Dte_Tme_A; }

    public DbsField getWork_Lead_New_C19() { return work_Lead_New_C19; }

    public DbsField getWork_Lead_New_Actve_Ind() { return work_Lead_New_Actve_Ind; }

    public DbsField getWork_Lead_New_C20() { return work_Lead_New_C20; }

    public DbsField getWork_Lead_New_Status_Cde() { return work_Lead_New_Status_Cde; }

    public DbsField getWork_Lead_New_C21() { return work_Lead_New_C21; }

    public DbsField getWork_Lead_New_Admin_Status_Updte_Dte_Tme_A() { return work_Lead_New_Admin_Status_Updte_Dte_Tme_A; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        work_Lead_New = newGroupInRecord("work_Lead_New", "WORK-LEAD-NEW");
        work_Lead_New_Pin_Npin = work_Lead_New.newFieldInGroup("work_Lead_New_Pin_Npin", "PIN-NPIN", FieldType.STRING, 7);
        work_Lead_New_C1 = work_Lead_New.newFieldInGroup("work_Lead_New_C1", "C1", FieldType.STRING, 1);
        work_Lead_New_Rqst_Log_Dte_Tme_A_15 = work_Lead_New.newFieldInGroup("work_Lead_New_Rqst_Log_Dte_Tme_A_15", "RQST-LOG-DTE-TME-A-15", FieldType.STRING, 
            15);
        work_Lead_New_C2 = work_Lead_New.newFieldInGroup("work_Lead_New_C2", "C2", FieldType.STRING, 1);
        work_Lead_New_Last_Chnge_Dte_Tme_A = work_Lead_New.newFieldInGroup("work_Lead_New_Last_Chnge_Dte_Tme_A", "LAST-CHNGE-DTE-TME-A", FieldType.STRING, 
            15);
        work_Lead_New_C3 = work_Lead_New.newFieldInGroup("work_Lead_New_C3", "C3", FieldType.STRING, 1);
        work_Lead_New_Work_Prcss_Id = work_Lead_New.newFieldInGroup("work_Lead_New_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        work_Lead_New_C4 = work_Lead_New.newFieldInGroup("work_Lead_New_C4", "C4", FieldType.STRING, 1);
        work_Lead_New_Tiaa_Rcvd_Dte_Tme_A = work_Lead_New.newFieldInGroup("work_Lead_New_Tiaa_Rcvd_Dte_Tme_A", "TIAA-RCVD-DTE-TME-A", FieldType.STRING, 
            28);
        work_Lead_New_C5 = work_Lead_New.newFieldInGroup("work_Lead_New_C5", "C5", FieldType.STRING, 1);
        work_Lead_New_Rqst_Log_Oprtr_Cde = work_Lead_New.newFieldInGroup("work_Lead_New_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 8);
        work_Lead_New_C6 = work_Lead_New.newFieldInGroup("work_Lead_New_C6", "C6", FieldType.STRING, 1);
        work_Lead_New_Orgnl_Unit_Cde = work_Lead_New.newFieldInGroup("work_Lead_New_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        work_Lead_New_C7 = work_Lead_New.newFieldInGroup("work_Lead_New_C7", "C7", FieldType.STRING, 1);
        work_Lead_New_Admin_Unit_Cde = work_Lead_New.newFieldInGroup("work_Lead_New_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        work_Lead_New_C8 = work_Lead_New.newFieldInGroup("work_Lead_New_C8", "C8", FieldType.STRING, 1);
        work_Lead_New_Admin_Status_Cde = work_Lead_New.newFieldInGroup("work_Lead_New_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4);
        work_Lead_New_C9 = work_Lead_New.newFieldInGroup("work_Lead_New_C9", "C9", FieldType.STRING, 1);
        work_Lead_New_Document_Type = work_Lead_New.newFieldInGroup("work_Lead_New_Document_Type", "DOCUMENT-TYPE", FieldType.STRING, 8);
        work_Lead_New_C10 = work_Lead_New.newFieldInGroup("work_Lead_New_C10", "C10", FieldType.STRING, 1);
        work_Lead_New_Empl_Oprtr_Cde = work_Lead_New.newFieldInGroup("work_Lead_New_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 8);
        work_Lead_New_C11 = work_Lead_New.newFieldInGroup("work_Lead_New_C11", "C11", FieldType.STRING, 1);
        work_Lead_New_Crprte_Due_Dte_Tme_A = work_Lead_New.newFieldInGroup("work_Lead_New_Crprte_Due_Dte_Tme_A", "CRPRTE-DUE-DTE-TME-A", FieldType.STRING, 
            28);
        work_Lead_New_C12 = work_Lead_New.newFieldInGroup("work_Lead_New_C12", "C12", FieldType.STRING, 1);
        work_Lead_New_Crprte_Status_Ind = work_Lead_New.newFieldInGroup("work_Lead_New_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1);
        work_Lead_New_C13 = work_Lead_New.newFieldInGroup("work_Lead_New_C13", "C13", FieldType.STRING, 1);
        work_Lead_New_Part_Close_Status_A = work_Lead_New.newFieldInGroup("work_Lead_New_Part_Close_Status_A", "PART-CLOSE-STATUS-A", FieldType.STRING, 
            4);
        work_Lead_New_C14 = work_Lead_New.newFieldInGroup("work_Lead_New_C14", "C14", FieldType.STRING, 1);
        work_Lead_New_Crprte_Clock_End_Dte_Tme_A = work_Lead_New.newFieldInGroup("work_Lead_New_Crprte_Clock_End_Dte_Tme_A", "CRPRTE-CLOCK-END-DTE-TME-A", 
            FieldType.STRING, 28);
        work_Lead_New_C15 = work_Lead_New.newFieldInGroup("work_Lead_New_C15", "C15", FieldType.STRING, 1);
        work_Lead_New_Moc = work_Lead_New.newFieldInGroup("work_Lead_New_Moc", "MOC", FieldType.STRING, 1);
        work_Lead_New_C16 = work_Lead_New.newFieldInGroup("work_Lead_New_C16", "C16", FieldType.STRING, 1);
        work_Lead_New_Part_Age = work_Lead_New.newFieldInGroup("work_Lead_New_Part_Age", "PART-AGE", FieldType.NUMERIC, 3);
        work_Lead_New_C17 = work_Lead_New.newFieldInGroup("work_Lead_New_C17", "C17", FieldType.STRING, 1);
        work_Lead_New_Last_Chnge_Oprtr_Cde = work_Lead_New.newFieldInGroup("work_Lead_New_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8);
        work_Lead_New_C18 = work_Lead_New.newFieldInGroup("work_Lead_New_C18", "C18", FieldType.STRING, 1);
        work_Lead_New_Crprte_Rqst_Log_Dte_Tme_A = work_Lead_New.newFieldInGroup("work_Lead_New_Crprte_Rqst_Log_Dte_Tme_A", "CRPRTE-RQST-LOG-DTE-TME-A", 
            FieldType.STRING, 28);
        work_Lead_New_C19 = work_Lead_New.newFieldInGroup("work_Lead_New_C19", "C19", FieldType.STRING, 1);
        work_Lead_New_Actve_Ind = work_Lead_New.newFieldInGroup("work_Lead_New_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2);
        work_Lead_New_C20 = work_Lead_New.newFieldInGroup("work_Lead_New_C20", "C20", FieldType.STRING, 1);
        work_Lead_New_Status_Cde = work_Lead_New.newFieldInGroup("work_Lead_New_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        work_Lead_New_C21 = work_Lead_New.newFieldInGroup("work_Lead_New_C21", "C21", FieldType.STRING, 1);
        work_Lead_New_Admin_Status_Updte_Dte_Tme_A = work_Lead_New.newFieldInGroup("work_Lead_New_Admin_Status_Updte_Dte_Tme_A", "ADMIN-STATUS-UPDTE-DTE-TME-A", 
            FieldType.STRING, 15);

        this.setRecordName("LdaCwfl4898");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4898() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
