/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:19:04 PM
**        * FROM NATURAL PDA     : CWFA4825
************************************************************
**        * FILE NAME            : PdaCwfa4825.java
**        * CLASS NAME           : PdaCwfa4825
**        * INSTANCE NAME        : PdaCwfa4825
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCwfa4825 extends PdaBase
{
    // Properties
    private DbsGroup cwfa4825_Output;
    private DbsField cwfa4825_Output_Control_Rec_Status;
    private DbsField cwfa4825_Output_Run_Control_Isn;

    public DbsGroup getCwfa4825_Output() { return cwfa4825_Output; }

    public DbsField getCwfa4825_Output_Control_Rec_Status() { return cwfa4825_Output_Control_Rec_Status; }

    public DbsField getCwfa4825_Output_Run_Control_Isn() { return cwfa4825_Output_Run_Control_Isn; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cwfa4825_Output = dbsRecord.newGroupInRecord("cwfa4825_Output", "CWFA4825-OUTPUT");
        cwfa4825_Output.setParameterOption(ParameterOption.ByReference);
        cwfa4825_Output_Control_Rec_Status = cwfa4825_Output.newFieldInGroup("cwfa4825_Output_Control_Rec_Status", "CONTROL-REC-STATUS", FieldType.STRING, 
            1);
        cwfa4825_Output_Run_Control_Isn = cwfa4825_Output.newFieldInGroup("cwfa4825_Output_Run_Control_Isn", "RUN-CONTROL-ISN", FieldType.PACKED_DECIMAL, 
            10);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCwfa4825(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

