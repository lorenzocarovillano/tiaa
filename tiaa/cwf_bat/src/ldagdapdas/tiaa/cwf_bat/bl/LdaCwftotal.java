/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:57:13 PM
**        * FROM NATURAL LDA     : CWFTOTAL
************************************************************
**        * FILE NAME            : LdaCwftotal.java
**        * CLASS NAME           : LdaCwftotal
**        * INSTANCE NAME        : LdaCwftotal
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwftotal extends DbsRecord
{
    // Properties
    private DbsGroup total;
    private DbsField total_Program_Name;
    private DbsField total_T01;
    private DbsField total_Start_Of_Window;
    private DbsField total_T02;
    private DbsField total_End_Of_Window;
    private DbsField total_T03;
    private DbsField total_Work_Read_Cntr;
    private DbsField total_T04;
    private DbsField total_Wrqst_Add_Cntr;
    private DbsField total_T05;
    private DbsField total_Wrqst_Upd_Cntr;
    private DbsField total_T06;
    private DbsField total_Unita_Add_Cntr;
    private DbsField total_T07;
    private DbsField total_Unita_Upd_Cntr;
    private DbsField total_T08;
    private DbsField total_Empla_Add_Cntr;
    private DbsField total_T09;
    private DbsField total_Empla_Upd_Cntr;
    private DbsField total_T10;
    private DbsField total_Stepa_Add_Cntr;
    private DbsField total_T11;
    private DbsField total_Stepa_Upd_Cntr;
    private DbsField total_T12;
    private DbsField total_Intra_Add_Cntr;
    private DbsField total_T13;
    private DbsField total_Intra_Upd_Cntr;
    private DbsField total_T14;
    private DbsField total_Extra_Add_Cntr;
    private DbsField total_T15;
    private DbsField total_Extra_Upd_Cntr;
    private DbsField total_T16;
    private DbsField total_Enrta_Add_Cntr;
    private DbsField total_T17;
    private DbsField total_Enrta_Upd_Cntr;
    private DbsField total_T18;
    private DbsField total_Contr_Add_Cntr;
    private DbsField total_T19;
    private DbsField total_Contr_Upd_Cntr;
    private DbsField total_T20;
    private DbsField total_Cstat_Add_Cntr;
    private DbsField total_T21;
    private DbsField total_Merg_Delt_Cntr;
    private DbsField total_T22;
    private DbsField total_Adtnl_Upd_Cntr;
    private DbsField total_T23;
    private DbsField total_Adtnl_Add_Cntr;
    private DbsField total_T24;
    private DbsField total_Relat_Add_Cntr;
    private DbsField total_T25;
    private DbsField total_Efmcab_Add_Cntr;
    private DbsField total_T26;
    private DbsField total_Wpidind_Add_Cntr;
    private DbsField total_T27;
    private DbsField total_Run_Date;

    public DbsGroup getTotal() { return total; }

    public DbsField getTotal_Program_Name() { return total_Program_Name; }

    public DbsField getTotal_T01() { return total_T01; }

    public DbsField getTotal_Start_Of_Window() { return total_Start_Of_Window; }

    public DbsField getTotal_T02() { return total_T02; }

    public DbsField getTotal_End_Of_Window() { return total_End_Of_Window; }

    public DbsField getTotal_T03() { return total_T03; }

    public DbsField getTotal_Work_Read_Cntr() { return total_Work_Read_Cntr; }

    public DbsField getTotal_T04() { return total_T04; }

    public DbsField getTotal_Wrqst_Add_Cntr() { return total_Wrqst_Add_Cntr; }

    public DbsField getTotal_T05() { return total_T05; }

    public DbsField getTotal_Wrqst_Upd_Cntr() { return total_Wrqst_Upd_Cntr; }

    public DbsField getTotal_T06() { return total_T06; }

    public DbsField getTotal_Unita_Add_Cntr() { return total_Unita_Add_Cntr; }

    public DbsField getTotal_T07() { return total_T07; }

    public DbsField getTotal_Unita_Upd_Cntr() { return total_Unita_Upd_Cntr; }

    public DbsField getTotal_T08() { return total_T08; }

    public DbsField getTotal_Empla_Add_Cntr() { return total_Empla_Add_Cntr; }

    public DbsField getTotal_T09() { return total_T09; }

    public DbsField getTotal_Empla_Upd_Cntr() { return total_Empla_Upd_Cntr; }

    public DbsField getTotal_T10() { return total_T10; }

    public DbsField getTotal_Stepa_Add_Cntr() { return total_Stepa_Add_Cntr; }

    public DbsField getTotal_T11() { return total_T11; }

    public DbsField getTotal_Stepa_Upd_Cntr() { return total_Stepa_Upd_Cntr; }

    public DbsField getTotal_T12() { return total_T12; }

    public DbsField getTotal_Intra_Add_Cntr() { return total_Intra_Add_Cntr; }

    public DbsField getTotal_T13() { return total_T13; }

    public DbsField getTotal_Intra_Upd_Cntr() { return total_Intra_Upd_Cntr; }

    public DbsField getTotal_T14() { return total_T14; }

    public DbsField getTotal_Extra_Add_Cntr() { return total_Extra_Add_Cntr; }

    public DbsField getTotal_T15() { return total_T15; }

    public DbsField getTotal_Extra_Upd_Cntr() { return total_Extra_Upd_Cntr; }

    public DbsField getTotal_T16() { return total_T16; }

    public DbsField getTotal_Enrta_Add_Cntr() { return total_Enrta_Add_Cntr; }

    public DbsField getTotal_T17() { return total_T17; }

    public DbsField getTotal_Enrta_Upd_Cntr() { return total_Enrta_Upd_Cntr; }

    public DbsField getTotal_T18() { return total_T18; }

    public DbsField getTotal_Contr_Add_Cntr() { return total_Contr_Add_Cntr; }

    public DbsField getTotal_T19() { return total_T19; }

    public DbsField getTotal_Contr_Upd_Cntr() { return total_Contr_Upd_Cntr; }

    public DbsField getTotal_T20() { return total_T20; }

    public DbsField getTotal_Cstat_Add_Cntr() { return total_Cstat_Add_Cntr; }

    public DbsField getTotal_T21() { return total_T21; }

    public DbsField getTotal_Merg_Delt_Cntr() { return total_Merg_Delt_Cntr; }

    public DbsField getTotal_T22() { return total_T22; }

    public DbsField getTotal_Adtnl_Upd_Cntr() { return total_Adtnl_Upd_Cntr; }

    public DbsField getTotal_T23() { return total_T23; }

    public DbsField getTotal_Adtnl_Add_Cntr() { return total_Adtnl_Add_Cntr; }

    public DbsField getTotal_T24() { return total_T24; }

    public DbsField getTotal_Relat_Add_Cntr() { return total_Relat_Add_Cntr; }

    public DbsField getTotal_T25() { return total_T25; }

    public DbsField getTotal_Efmcab_Add_Cntr() { return total_Efmcab_Add_Cntr; }

    public DbsField getTotal_T26() { return total_T26; }

    public DbsField getTotal_Wpidind_Add_Cntr() { return total_Wpidind_Add_Cntr; }

    public DbsField getTotal_T27() { return total_T27; }

    public DbsField getTotal_Run_Date() { return total_Run_Date; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        total = newGroupInRecord("total", "TOTAL");
        total_Program_Name = total.newFieldInGroup("total_Program_Name", "PROGRAM-NAME", FieldType.STRING, 8);
        total_T01 = total.newFieldInGroup("total_T01", "T01", FieldType.STRING, 1);
        total_Start_Of_Window = total.newFieldInGroup("total_Start_Of_Window", "START-OF-WINDOW", FieldType.STRING, 28);
        total_T02 = total.newFieldInGroup("total_T02", "T02", FieldType.STRING, 1);
        total_End_Of_Window = total.newFieldInGroup("total_End_Of_Window", "END-OF-WINDOW", FieldType.STRING, 28);
        total_T03 = total.newFieldInGroup("total_T03", "T03", FieldType.STRING, 1);
        total_Work_Read_Cntr = total.newFieldInGroup("total_Work_Read_Cntr", "WORK-READ-CNTR", FieldType.NUMERIC, 9);
        total_T04 = total.newFieldInGroup("total_T04", "T04", FieldType.STRING, 1);
        total_Wrqst_Add_Cntr = total.newFieldInGroup("total_Wrqst_Add_Cntr", "WRQST-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T05 = total.newFieldInGroup("total_T05", "T05", FieldType.STRING, 1);
        total_Wrqst_Upd_Cntr = total.newFieldInGroup("total_Wrqst_Upd_Cntr", "WRQST-UPD-CNTR", FieldType.NUMERIC, 9);
        total_T06 = total.newFieldInGroup("total_T06", "T06", FieldType.STRING, 1);
        total_Unita_Add_Cntr = total.newFieldInGroup("total_Unita_Add_Cntr", "UNITA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T07 = total.newFieldInGroup("total_T07", "T07", FieldType.STRING, 1);
        total_Unita_Upd_Cntr = total.newFieldInGroup("total_Unita_Upd_Cntr", "UNITA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_T08 = total.newFieldInGroup("total_T08", "T08", FieldType.STRING, 1);
        total_Empla_Add_Cntr = total.newFieldInGroup("total_Empla_Add_Cntr", "EMPLA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T09 = total.newFieldInGroup("total_T09", "T09", FieldType.STRING, 1);
        total_Empla_Upd_Cntr = total.newFieldInGroup("total_Empla_Upd_Cntr", "EMPLA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_T10 = total.newFieldInGroup("total_T10", "T10", FieldType.STRING, 1);
        total_Stepa_Add_Cntr = total.newFieldInGroup("total_Stepa_Add_Cntr", "STEPA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T11 = total.newFieldInGroup("total_T11", "T11", FieldType.STRING, 1);
        total_Stepa_Upd_Cntr = total.newFieldInGroup("total_Stepa_Upd_Cntr", "STEPA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_T12 = total.newFieldInGroup("total_T12", "T12", FieldType.STRING, 1);
        total_Intra_Add_Cntr = total.newFieldInGroup("total_Intra_Add_Cntr", "INTRA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T13 = total.newFieldInGroup("total_T13", "T13", FieldType.STRING, 1);
        total_Intra_Upd_Cntr = total.newFieldInGroup("total_Intra_Upd_Cntr", "INTRA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_T14 = total.newFieldInGroup("total_T14", "T14", FieldType.STRING, 1);
        total_Extra_Add_Cntr = total.newFieldInGroup("total_Extra_Add_Cntr", "EXTRA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T15 = total.newFieldInGroup("total_T15", "T15", FieldType.STRING, 1);
        total_Extra_Upd_Cntr = total.newFieldInGroup("total_Extra_Upd_Cntr", "EXTRA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_T16 = total.newFieldInGroup("total_T16", "T16", FieldType.STRING, 1);
        total_Enrta_Add_Cntr = total.newFieldInGroup("total_Enrta_Add_Cntr", "ENRTA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T17 = total.newFieldInGroup("total_T17", "T17", FieldType.STRING, 1);
        total_Enrta_Upd_Cntr = total.newFieldInGroup("total_Enrta_Upd_Cntr", "ENRTA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_T18 = total.newFieldInGroup("total_T18", "T18", FieldType.STRING, 1);
        total_Contr_Add_Cntr = total.newFieldInGroup("total_Contr_Add_Cntr", "CONTR-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T19 = total.newFieldInGroup("total_T19", "T19", FieldType.STRING, 1);
        total_Contr_Upd_Cntr = total.newFieldInGroup("total_Contr_Upd_Cntr", "CONTR-UPD-CNTR", FieldType.NUMERIC, 9);
        total_T20 = total.newFieldInGroup("total_T20", "T20", FieldType.STRING, 1);
        total_Cstat_Add_Cntr = total.newFieldInGroup("total_Cstat_Add_Cntr", "CSTAT-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T21 = total.newFieldInGroup("total_T21", "T21", FieldType.STRING, 1);
        total_Merg_Delt_Cntr = total.newFieldInGroup("total_Merg_Delt_Cntr", "MERG-DELT-CNTR", FieldType.NUMERIC, 9);
        total_T22 = total.newFieldInGroup("total_T22", "T22", FieldType.STRING, 1);
        total_Adtnl_Upd_Cntr = total.newFieldInGroup("total_Adtnl_Upd_Cntr", "ADTNL-UPD-CNTR", FieldType.NUMERIC, 9);
        total_T23 = total.newFieldInGroup("total_T23", "T23", FieldType.STRING, 1);
        total_Adtnl_Add_Cntr = total.newFieldInGroup("total_Adtnl_Add_Cntr", "ADTNL-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T24 = total.newFieldInGroup("total_T24", "T24", FieldType.STRING, 1);
        total_Relat_Add_Cntr = total.newFieldInGroup("total_Relat_Add_Cntr", "RELAT-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T25 = total.newFieldInGroup("total_T25", "T25", FieldType.STRING, 1);
        total_Efmcab_Add_Cntr = total.newFieldInGroup("total_Efmcab_Add_Cntr", "EFMCAB-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T26 = total.newFieldInGroup("total_T26", "T26", FieldType.STRING, 1);
        total_Wpidind_Add_Cntr = total.newFieldInGroup("total_Wpidind_Add_Cntr", "WPIDIND-ADD-CNTR", FieldType.NUMERIC, 9);
        total_T27 = total.newFieldInGroup("total_T27", "T27", FieldType.STRING, 1);
        total_Run_Date = total.newFieldInGroup("total_Run_Date", "RUN-DATE", FieldType.STRING, 10);

        this.setRecordName("LdaCwftotal");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwftotal() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
