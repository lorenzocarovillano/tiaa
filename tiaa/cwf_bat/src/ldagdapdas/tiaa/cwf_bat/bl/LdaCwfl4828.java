/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:55 PM
**        * FROM NATURAL LDA     : CWFL4828
************************************************************
**        * FILE NAME            : LdaCwfl4828.java
**        * CLASS NAME           : LdaCwfl4828
**        * INSTANCE NAME        : LdaCwfl4828
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4828 extends DbsRecord
{
    // Properties
    private DbsGroup extra_Table;
    private DbsField extra_Table_Rqst_Log_Dte_Tme_A;
    private DbsField extra_Table_Q1;
    private DbsField extra_Table_Admin_Status_Cde;

    public DbsGroup getExtra_Table() { return extra_Table; }

    public DbsField getExtra_Table_Rqst_Log_Dte_Tme_A() { return extra_Table_Rqst_Log_Dte_Tme_A; }

    public DbsField getExtra_Table_Q1() { return extra_Table_Q1; }

    public DbsField getExtra_Table_Admin_Status_Cde() { return extra_Table_Admin_Status_Cde; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        extra_Table = newGroupInRecord("extra_Table", "EXTRA-TABLE");
        extra_Table_Rqst_Log_Dte_Tme_A = extra_Table.newFieldInGroup("extra_Table_Rqst_Log_Dte_Tme_A", "RQST-LOG-DTE-TME-A", FieldType.STRING, 28);
        extra_Table_Q1 = extra_Table.newFieldInGroup("extra_Table_Q1", "Q1", FieldType.STRING, 1);
        extra_Table_Admin_Status_Cde = extra_Table.newFieldInGroup("extra_Table_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4);

        this.setRecordName("LdaCwfl4828");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4828() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
