/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:21:00 PM
**        * FROM NATURAL PDA     : ICWA2613
************************************************************
**        * FILE NAME            : PdaIcwa2613.java
**        * CLASS NAME           : PdaIcwa2613
**        * INSTANCE NAME        : PdaIcwa2613
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIcwa2613 extends PdaBase
{
    // Properties
    private DbsGroup icwa2613;
    private DbsField icwa2613_Tbl_Scrty_Level_Ind;
    private DbsField icwa2613_Tbl_Table_Nme;
    private DbsField icwa2613_Tbl_Key_Field;
    private DbsGroup icwa2613_Tbl_Key_FieldRedef1;
    private DbsField icwa2613_File_Date;
    private DbsField icwa2613_Records_Loaded;
    private DbsField icwa2613_Tbl_Data_Field;
    private DbsField icwa2613_Tbl_Actve_Ind;
    private DbsGroup icwa2613_Tbl_Actve_IndRedef2;
    private DbsField icwa2613_Fill_50c940;
    private DbsField icwa2613_Tbl_Actve_Ind_2_2;
    private DbsField icwa2613_Tbl_Entry_Dte_Tme;
    private DbsField icwa2613_Tbl_Entry_Oprtr_Cde;
    private DbsField icwa2613_Tbl_Updte_Dte_Tme;
    private DbsField icwa2613_Tbl_Updte_Dte;
    private DbsField icwa2613_Tbl_Updte_Oprtr_Cde;
    private DbsField icwa2613_Tbl_Dlte_Dte_Tme;
    private DbsField icwa2613_Tbl_Dlte_Oprtr_Cde;
    private DbsField icwa2613_Tbl_Table_Rectype;
    private DbsField icwa2613_Tbl_Table_Access_Level;
    private DbsField icwa2613_Id;
    private DbsGroup icwa2613_IdRedef3;
    private DbsGroup icwa2613_Id_Structure;
    private DbsField icwa2613_Id_Tbl_Scrty_Level_Ind;
    private DbsField icwa2613_Id_Tbl_Table_Nme;
    private DbsField icwa2613_Id_Tbl_Key_Field;
    private DbsField icwa2613_Id_Tbl_Actve_Ind_2_2;

    public DbsGroup getIcwa2613() { return icwa2613; }

    public DbsField getIcwa2613_Tbl_Scrty_Level_Ind() { return icwa2613_Tbl_Scrty_Level_Ind; }

    public DbsField getIcwa2613_Tbl_Table_Nme() { return icwa2613_Tbl_Table_Nme; }

    public DbsField getIcwa2613_Tbl_Key_Field() { return icwa2613_Tbl_Key_Field; }

    public DbsGroup getIcwa2613_Tbl_Key_FieldRedef1() { return icwa2613_Tbl_Key_FieldRedef1; }

    public DbsField getIcwa2613_File_Date() { return icwa2613_File_Date; }

    public DbsField getIcwa2613_Records_Loaded() { return icwa2613_Records_Loaded; }

    public DbsField getIcwa2613_Tbl_Data_Field() { return icwa2613_Tbl_Data_Field; }

    public DbsField getIcwa2613_Tbl_Actve_Ind() { return icwa2613_Tbl_Actve_Ind; }

    public DbsGroup getIcwa2613_Tbl_Actve_IndRedef2() { return icwa2613_Tbl_Actve_IndRedef2; }

    public DbsField getIcwa2613_Fill_50c940() { return icwa2613_Fill_50c940; }

    public DbsField getIcwa2613_Tbl_Actve_Ind_2_2() { return icwa2613_Tbl_Actve_Ind_2_2; }

    public DbsField getIcwa2613_Tbl_Entry_Dte_Tme() { return icwa2613_Tbl_Entry_Dte_Tme; }

    public DbsField getIcwa2613_Tbl_Entry_Oprtr_Cde() { return icwa2613_Tbl_Entry_Oprtr_Cde; }

    public DbsField getIcwa2613_Tbl_Updte_Dte_Tme() { return icwa2613_Tbl_Updte_Dte_Tme; }

    public DbsField getIcwa2613_Tbl_Updte_Dte() { return icwa2613_Tbl_Updte_Dte; }

    public DbsField getIcwa2613_Tbl_Updte_Oprtr_Cde() { return icwa2613_Tbl_Updte_Oprtr_Cde; }

    public DbsField getIcwa2613_Tbl_Dlte_Dte_Tme() { return icwa2613_Tbl_Dlte_Dte_Tme; }

    public DbsField getIcwa2613_Tbl_Dlte_Oprtr_Cde() { return icwa2613_Tbl_Dlte_Oprtr_Cde; }

    public DbsField getIcwa2613_Tbl_Table_Rectype() { return icwa2613_Tbl_Table_Rectype; }

    public DbsField getIcwa2613_Tbl_Table_Access_Level() { return icwa2613_Tbl_Table_Access_Level; }

    public DbsField getIcwa2613_Id() { return icwa2613_Id; }

    public DbsGroup getIcwa2613_IdRedef3() { return icwa2613_IdRedef3; }

    public DbsGroup getIcwa2613_Id_Structure() { return icwa2613_Id_Structure; }

    public DbsField getIcwa2613_Id_Tbl_Scrty_Level_Ind() { return icwa2613_Id_Tbl_Scrty_Level_Ind; }

    public DbsField getIcwa2613_Id_Tbl_Table_Nme() { return icwa2613_Id_Tbl_Table_Nme; }

    public DbsField getIcwa2613_Id_Tbl_Key_Field() { return icwa2613_Id_Tbl_Key_Field; }

    public DbsField getIcwa2613_Id_Tbl_Actve_Ind_2_2() { return icwa2613_Id_Tbl_Actve_Ind_2_2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        icwa2613 = dbsRecord.newGroupInRecord("icwa2613", "ICWA2613");
        icwa2613.setParameterOption(ParameterOption.ByReference);
        icwa2613_Tbl_Scrty_Level_Ind = icwa2613.newFieldInGroup("icwa2613_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        icwa2613_Tbl_Table_Nme = icwa2613.newFieldInGroup("icwa2613_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        icwa2613_Tbl_Key_Field = icwa2613.newFieldInGroup("icwa2613_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        icwa2613_Tbl_Key_FieldRedef1 = icwa2613.newGroupInGroup("icwa2613_Tbl_Key_FieldRedef1", "Redefines", icwa2613_Tbl_Key_Field);
        icwa2613_File_Date = icwa2613_Tbl_Key_FieldRedef1.newFieldInGroup("icwa2613_File_Date", "FILE-DATE", FieldType.STRING, 8);
        icwa2613_Records_Loaded = icwa2613_Tbl_Key_FieldRedef1.newFieldInGroup("icwa2613_Records_Loaded", "RECORDS-LOADED", FieldType.NUMERIC, 5);
        icwa2613_Tbl_Data_Field = icwa2613.newFieldInGroup("icwa2613_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253);
        icwa2613_Tbl_Actve_Ind = icwa2613.newFieldInGroup("icwa2613_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 2);
        icwa2613_Tbl_Actve_IndRedef2 = icwa2613.newGroupInGroup("icwa2613_Tbl_Actve_IndRedef2", "Redefines", icwa2613_Tbl_Actve_Ind);
        icwa2613_Fill_50c940 = icwa2613_Tbl_Actve_IndRedef2.newFieldInGroup("icwa2613_Fill_50c940", "FILL-50C940", FieldType.STRING, 1);
        icwa2613_Tbl_Actve_Ind_2_2 = icwa2613_Tbl_Actve_IndRedef2.newFieldInGroup("icwa2613_Tbl_Actve_Ind_2_2", "TBL-ACTVE-IND-2-2", FieldType.STRING, 
            1);
        icwa2613_Tbl_Entry_Dte_Tme = icwa2613.newFieldInGroup("icwa2613_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME);
        icwa2613_Tbl_Entry_Oprtr_Cde = icwa2613.newFieldInGroup("icwa2613_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", FieldType.STRING, 8);
        icwa2613_Tbl_Updte_Dte_Tme = icwa2613.newFieldInGroup("icwa2613_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME);
        icwa2613_Tbl_Updte_Dte = icwa2613.newFieldInGroup("icwa2613_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE);
        icwa2613_Tbl_Updte_Oprtr_Cde = icwa2613.newFieldInGroup("icwa2613_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 8);
        icwa2613_Tbl_Dlte_Dte_Tme = icwa2613.newFieldInGroup("icwa2613_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME);
        icwa2613_Tbl_Dlte_Oprtr_Cde = icwa2613.newFieldInGroup("icwa2613_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", FieldType.STRING, 8);
        icwa2613_Tbl_Table_Rectype = icwa2613.newFieldInGroup("icwa2613_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 1);
        icwa2613_Tbl_Table_Access_Level = icwa2613.newFieldInGroup("icwa2613_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", FieldType.STRING, 1);

        icwa2613_Id = dbsRecord.newFieldInRecord("icwa2613_Id", "ICWA2613-ID", FieldType.STRING, 53);
        icwa2613_Id.setParameterOption(ParameterOption.ByReference);
        icwa2613_IdRedef3 = dbsRecord.newGroupInRecord("icwa2613_IdRedef3", "Redefines", icwa2613_Id);
        icwa2613_Id_Structure = icwa2613_IdRedef3.newGroupInGroup("icwa2613_Id_Structure", "STRUCTURE");
        icwa2613_Id_Tbl_Scrty_Level_Ind = icwa2613_Id_Structure.newFieldInGroup("icwa2613_Id_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        icwa2613_Id_Tbl_Table_Nme = icwa2613_Id_Structure.newFieldInGroup("icwa2613_Id_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        icwa2613_Id_Tbl_Key_Field = icwa2613_Id_Structure.newFieldInGroup("icwa2613_Id_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        icwa2613_Id_Tbl_Actve_Ind_2_2 = icwa2613_Id_Structure.newFieldInGroup("icwa2613_Id_Tbl_Actve_Ind_2_2", "TBL-ACTVE-IND-2-2", FieldType.STRING, 
            1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIcwa2613(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

