/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:47 PM
**        * FROM NATURAL LDA     : CWFL4822
************************************************************
**        * FILE NAME            : LdaCwfl4822.java
**        * CLASS NAME           : LdaCwfl4822
**        * INSTANCE NAME        : LdaCwfl4822
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4822 extends DbsRecord
{
    // Properties
    private DbsGroup cstatus;
    private DbsField cstatus_Rqst_Log_Dte_Tme_A;
    private DbsField cstatus_F1;
    private DbsField cstatus_Admin_Unit_Cde;
    private DbsField cstatus_F2;
    private DbsField cstatus_Admin_Status_Cde;
    private DbsField cstatus_F3;
    private DbsField cstatus_Last_Chnge_Dte_Tme_A;
    private DbsField cstatus_F4;
    private DbsField cstatus_Last_Chnge_Oprtr_Cde;
    private DbsField cstatus_F5;
    private DbsField cstatus_Status_Description;

    public DbsGroup getCstatus() { return cstatus; }

    public DbsField getCstatus_Rqst_Log_Dte_Tme_A() { return cstatus_Rqst_Log_Dte_Tme_A; }

    public DbsField getCstatus_F1() { return cstatus_F1; }

    public DbsField getCstatus_Admin_Unit_Cde() { return cstatus_Admin_Unit_Cde; }

    public DbsField getCstatus_F2() { return cstatus_F2; }

    public DbsField getCstatus_Admin_Status_Cde() { return cstatus_Admin_Status_Cde; }

    public DbsField getCstatus_F3() { return cstatus_F3; }

    public DbsField getCstatus_Last_Chnge_Dte_Tme_A() { return cstatus_Last_Chnge_Dte_Tme_A; }

    public DbsField getCstatus_F4() { return cstatus_F4; }

    public DbsField getCstatus_Last_Chnge_Oprtr_Cde() { return cstatus_Last_Chnge_Oprtr_Cde; }

    public DbsField getCstatus_F5() { return cstatus_F5; }

    public DbsField getCstatus_Status_Description() { return cstatus_Status_Description; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cstatus = newGroupInRecord("cstatus", "CSTATUS");
        cstatus_Rqst_Log_Dte_Tme_A = cstatus.newFieldInGroup("cstatus_Rqst_Log_Dte_Tme_A", "RQST-LOG-DTE-TME-A", FieldType.STRING, 28);
        cstatus_F1 = cstatus.newFieldInGroup("cstatus_F1", "F1", FieldType.STRING, 1);
        cstatus_Admin_Unit_Cde = cstatus.newFieldInGroup("cstatus_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        cstatus_F2 = cstatus.newFieldInGroup("cstatus_F2", "F2", FieldType.STRING, 1);
        cstatus_Admin_Status_Cde = cstatus.newFieldInGroup("cstatus_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4);
        cstatus_F3 = cstatus.newFieldInGroup("cstatus_F3", "F3", FieldType.STRING, 1);
        cstatus_Last_Chnge_Dte_Tme_A = cstatus.newFieldInGroup("cstatus_Last_Chnge_Dte_Tme_A", "LAST-CHNGE-DTE-TME-A", FieldType.STRING, 28);
        cstatus_F4 = cstatus.newFieldInGroup("cstatus_F4", "F4", FieldType.STRING, 1);
        cstatus_Last_Chnge_Oprtr_Cde = cstatus.newFieldInGroup("cstatus_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8);
        cstatus_F5 = cstatus.newFieldInGroup("cstatus_F5", "F5", FieldType.STRING, 1);
        cstatus_Status_Description = cstatus.newFieldInGroup("cstatus_Status_Description", "STATUS-DESCRIPTION", FieldType.STRING, 40);

        this.setRecordName("LdaCwfl4822");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4822() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
