/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:22:31 PM
**        * FROM NATURAL PDA     : NSTA1245
************************************************************
**        * FILE NAME            : PdaNsta1245.java
**        * CLASS NAME           : PdaNsta1245
**        * INSTANCE NAME        : PdaNsta1245
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaNsta1245 extends PdaBase
{
    // Properties
    private DbsGroup nsta1245;
    private DbsField nsta1245_Pnd_Eff_Dte;
    private DbsGroup nsta1245_Pnd_Eff_DteRedef1;
    private DbsField nsta1245_Pnd_Eff_Dte_Yyyy;
    private DbsGroup nsta1245_Pnd_Eff_Dte_YyyyRedef2;
    private DbsField filler01;
    private DbsField nsta1245_Pnd_Eff_Dte_Yy;
    private DbsField nsta1245_Pnd_Eff_Dte_Mmdd;
    private DbsGroup nsta1245_Pnd_Eff_Dte_MmddRedef3;
    private DbsField nsta1245_Pnd_Eff_Dte_Mm;
    private DbsField nsta1245_Pnd_Eff_Dte_Dd;
    private DbsField nsta1245_Pnd_Dob_Dte;
    private DbsGroup nsta1245_Pnd_Dob_DteRedef4;
    private DbsField nsta1245_Pnd_Dob_Dte_Yyyy;
    private DbsField nsta1245_Pnd_Dob_Dte_Mmdd;
    private DbsGroup nsta1245_Pnd_Dob_Dte_MmddRedef5;
    private DbsField nsta1245_Pnd_Dob_Dte_Mm;
    private DbsField nsta1245_Pnd_Dob_Dte_Dd;
    private DbsField nsta1245_Pnd_Years;
    private DbsField nsta1245_Pnd_Months;
    private DbsField nsta1245_Pnd_Days;

    public DbsGroup getNsta1245() { return nsta1245; }

    public DbsField getNsta1245_Pnd_Eff_Dte() { return nsta1245_Pnd_Eff_Dte; }

    public DbsGroup getNsta1245_Pnd_Eff_DteRedef1() { return nsta1245_Pnd_Eff_DteRedef1; }

    public DbsField getNsta1245_Pnd_Eff_Dte_Yyyy() { return nsta1245_Pnd_Eff_Dte_Yyyy; }

    public DbsGroup getNsta1245_Pnd_Eff_Dte_YyyyRedef2() { return nsta1245_Pnd_Eff_Dte_YyyyRedef2; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getNsta1245_Pnd_Eff_Dte_Yy() { return nsta1245_Pnd_Eff_Dte_Yy; }

    public DbsField getNsta1245_Pnd_Eff_Dte_Mmdd() { return nsta1245_Pnd_Eff_Dte_Mmdd; }

    public DbsGroup getNsta1245_Pnd_Eff_Dte_MmddRedef3() { return nsta1245_Pnd_Eff_Dte_MmddRedef3; }

    public DbsField getNsta1245_Pnd_Eff_Dte_Mm() { return nsta1245_Pnd_Eff_Dte_Mm; }

    public DbsField getNsta1245_Pnd_Eff_Dte_Dd() { return nsta1245_Pnd_Eff_Dte_Dd; }

    public DbsField getNsta1245_Pnd_Dob_Dte() { return nsta1245_Pnd_Dob_Dte; }

    public DbsGroup getNsta1245_Pnd_Dob_DteRedef4() { return nsta1245_Pnd_Dob_DteRedef4; }

    public DbsField getNsta1245_Pnd_Dob_Dte_Yyyy() { return nsta1245_Pnd_Dob_Dte_Yyyy; }

    public DbsField getNsta1245_Pnd_Dob_Dte_Mmdd() { return nsta1245_Pnd_Dob_Dte_Mmdd; }

    public DbsGroup getNsta1245_Pnd_Dob_Dte_MmddRedef5() { return nsta1245_Pnd_Dob_Dte_MmddRedef5; }

    public DbsField getNsta1245_Pnd_Dob_Dte_Mm() { return nsta1245_Pnd_Dob_Dte_Mm; }

    public DbsField getNsta1245_Pnd_Dob_Dte_Dd() { return nsta1245_Pnd_Dob_Dte_Dd; }

    public DbsField getNsta1245_Pnd_Years() { return nsta1245_Pnd_Years; }

    public DbsField getNsta1245_Pnd_Months() { return nsta1245_Pnd_Months; }

    public DbsField getNsta1245_Pnd_Days() { return nsta1245_Pnd_Days; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        nsta1245 = dbsRecord.newGroupInRecord("nsta1245", "NSTA1245");
        nsta1245.setParameterOption(ParameterOption.ByReference);
        nsta1245_Pnd_Eff_Dte = nsta1245.newFieldInGroup("nsta1245_Pnd_Eff_Dte", "#EFF-DTE", FieldType.STRING, 8);
        nsta1245_Pnd_Eff_DteRedef1 = nsta1245.newGroupInGroup("nsta1245_Pnd_Eff_DteRedef1", "Redefines", nsta1245_Pnd_Eff_Dte);
        nsta1245_Pnd_Eff_Dte_Yyyy = nsta1245_Pnd_Eff_DteRedef1.newFieldInGroup("nsta1245_Pnd_Eff_Dte_Yyyy", "#EFF-DTE-YYYY", FieldType.NUMERIC, 4);
        nsta1245_Pnd_Eff_Dte_YyyyRedef2 = nsta1245_Pnd_Eff_DteRedef1.newGroupInGroup("nsta1245_Pnd_Eff_Dte_YyyyRedef2", "Redefines", nsta1245_Pnd_Eff_Dte_Yyyy);
        filler01 = nsta1245_Pnd_Eff_Dte_YyyyRedef2.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 2);
        nsta1245_Pnd_Eff_Dte_Yy = nsta1245_Pnd_Eff_Dte_YyyyRedef2.newFieldInGroup("nsta1245_Pnd_Eff_Dte_Yy", "#EFF-DTE-YY", FieldType.NUMERIC, 2);
        nsta1245_Pnd_Eff_Dte_Mmdd = nsta1245_Pnd_Eff_DteRedef1.newFieldInGroup("nsta1245_Pnd_Eff_Dte_Mmdd", "#EFF-DTE-MMDD", FieldType.NUMERIC, 4);
        nsta1245_Pnd_Eff_Dte_MmddRedef3 = nsta1245_Pnd_Eff_DteRedef1.newGroupInGroup("nsta1245_Pnd_Eff_Dte_MmddRedef3", "Redefines", nsta1245_Pnd_Eff_Dte_Mmdd);
        nsta1245_Pnd_Eff_Dte_Mm = nsta1245_Pnd_Eff_Dte_MmddRedef3.newFieldInGroup("nsta1245_Pnd_Eff_Dte_Mm", "#EFF-DTE-MM", FieldType.NUMERIC, 2);
        nsta1245_Pnd_Eff_Dte_Dd = nsta1245_Pnd_Eff_Dte_MmddRedef3.newFieldInGroup("nsta1245_Pnd_Eff_Dte_Dd", "#EFF-DTE-DD", FieldType.NUMERIC, 2);
        nsta1245_Pnd_Dob_Dte = nsta1245.newFieldInGroup("nsta1245_Pnd_Dob_Dte", "#DOB-DTE", FieldType.STRING, 8);
        nsta1245_Pnd_Dob_DteRedef4 = nsta1245.newGroupInGroup("nsta1245_Pnd_Dob_DteRedef4", "Redefines", nsta1245_Pnd_Dob_Dte);
        nsta1245_Pnd_Dob_Dte_Yyyy = nsta1245_Pnd_Dob_DteRedef4.newFieldInGroup("nsta1245_Pnd_Dob_Dte_Yyyy", "#DOB-DTE-YYYY", FieldType.NUMERIC, 4);
        nsta1245_Pnd_Dob_Dte_Mmdd = nsta1245_Pnd_Dob_DteRedef4.newFieldInGroup("nsta1245_Pnd_Dob_Dte_Mmdd", "#DOB-DTE-MMDD", FieldType.NUMERIC, 4);
        nsta1245_Pnd_Dob_Dte_MmddRedef5 = nsta1245_Pnd_Dob_DteRedef4.newGroupInGroup("nsta1245_Pnd_Dob_Dte_MmddRedef5", "Redefines", nsta1245_Pnd_Dob_Dte_Mmdd);
        nsta1245_Pnd_Dob_Dte_Mm = nsta1245_Pnd_Dob_Dte_MmddRedef5.newFieldInGroup("nsta1245_Pnd_Dob_Dte_Mm", "#DOB-DTE-MM", FieldType.NUMERIC, 2);
        nsta1245_Pnd_Dob_Dte_Dd = nsta1245_Pnd_Dob_Dte_MmddRedef5.newFieldInGroup("nsta1245_Pnd_Dob_Dte_Dd", "#DOB-DTE-DD", FieldType.NUMERIC, 2);
        nsta1245_Pnd_Years = nsta1245.newFieldInGroup("nsta1245_Pnd_Years", "#YEARS", FieldType.NUMERIC, 3);
        nsta1245_Pnd_Months = nsta1245.newFieldInGroup("nsta1245_Pnd_Months", "#MONTHS", FieldType.NUMERIC, 2);
        nsta1245_Pnd_Days = nsta1245.newFieldInGroup("nsta1245_Pnd_Days", "#DAYS", FieldType.NUMERIC, 2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaNsta1245(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

