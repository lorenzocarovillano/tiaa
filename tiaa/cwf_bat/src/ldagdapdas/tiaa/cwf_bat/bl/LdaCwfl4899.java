/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:57 PM
**        * FROM NATURAL LDA     : CWFL4899
************************************************************
**        * FILE NAME            : LdaCwfl4899.java
**        * CLASS NAME           : LdaCwfl4899
**        * INSTANCE NAME        : LdaCwfl4899
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4899 extends DbsRecord
{
    // Properties
    private DbsGroup work_Request_New;
    private DbsField work_Request_New_Rqst_Log_Dte_Tme_A;
    private DbsField work_Request_New_D1;
    private DbsField work_Request_New_Work_Prcss_Id;
    private DbsGroup work_Request_New_Work_Prcss_IdRedef1;
    private DbsField work_Request_New_Work_Actn_Rqstd_Cde;
    private DbsField work_Request_New_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField work_Request_New_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField work_Request_New_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField work_Request_New_D2;
    private DbsField work_Request_New_Pin_Nbr;
    private DbsField work_Request_New_D3;
    private DbsField work_Request_New_Part_Name;
    private DbsField work_Request_New_D4;
    private DbsField work_Request_New_Ssn;
    private DbsField work_Request_New_D5;
    private DbsField work_Request_New_Tiaa_Rcvd_Dte_Tme_A;
    private DbsField work_Request_New_D6;
    private DbsField work_Request_New_Tiaa_Rcvd_Dte_A;
    private DbsField work_Request_New_D7;
    private DbsField work_Request_New_Orgnl_Unit_Cde;
    private DbsField work_Request_New_D8;
    private DbsField work_Request_New_Rqst_Log_Oprtr_Cde;
    private DbsField work_Request_New_D9;
    private DbsField work_Request_New_Rqst_Orgn_Cde;
    private DbsField work_Request_New_D10;
    private DbsField work_Request_New_Crprte_Status_Ind;
    private DbsField work_Request_New_D11;
    private DbsField work_Request_New_Crprte_Clock_End_Dte_Tme_A;
    private DbsField work_Request_New_D12;
    private DbsField work_Request_New_Final_Close_Out_Dte_Tme_A;
    private DbsField work_Request_New_D13;
    private DbsField work_Request_New_Effctve_Dte_A;
    private DbsField work_Request_New_D14;
    private DbsField work_Request_New_Trans_Dte_A;
    private DbsField work_Request_New_D15;
    private DbsField work_Request_New_Trnsctn_Dte_A;
    private DbsField work_Request_New_D16;
    private DbsField work_Request_New_Owner_Unit_Cde;
    private DbsField work_Request_New_D17;
    private DbsField work_Request_New_Owner_Division;
    private DbsField work_Request_New_D18;
    private DbsField work_Request_New_Shphrd_Id;
    private DbsField work_Request_New_D19;
    private DbsField work_Request_New_Sec_Ind;
    private DbsField work_Request_New_D20;
    private DbsField work_Request_New_Admin_Work_Ind;
    private DbsField work_Request_New_D21;
    private DbsField work_Request_New_Sub_Rqst_Ind;
    private DbsField work_Request_New_D22;
    private DbsField work_Request_New_Prnt_Rqst_Log_Dte_Tme_A;
    private DbsField work_Request_New_D23;
    private DbsField work_Request_New_Prnt_Work_Prcss_Id;
    private DbsField work_Request_New_D24;
    private DbsField work_Request_New_Multi_Rqst_Ind;
    private DbsField work_Request_New_D25;
    private DbsField work_Request_New_Cmplnt_Ind;
    private DbsField work_Request_New_D26;
    private DbsField work_Request_New_Elctrnc_Fldr_Ind;
    private DbsField work_Request_New_D27;
    private DbsField work_Request_New_Mj_Pull_Ind;
    private DbsField work_Request_New_D28;
    private DbsField work_Request_New_Check_Ind;
    private DbsField work_Request_New_D29;
    private DbsField work_Request_New_Bsnss_Reply_Ind;
    private DbsField work_Request_New_D30;
    private DbsField work_Request_New_Tlc_Ind;
    private DbsField work_Request_New_D31;
    private DbsField work_Request_New_Redo_Ind;
    private DbsField work_Request_New_D32;
    private DbsField work_Request_New_Crprte_On_Tme_Ind;
    private DbsField work_Request_New_D33;
    private DbsField work_Request_New_Off_Rtng_Ind;
    private DbsField work_Request_New_D34;
    private DbsField work_Request_New_Prcssng_Type_Cde;
    private DbsField work_Request_New_D35;
    private DbsField work_Request_New_Log_Rqstr_Cde;
    private DbsField work_Request_New_D36;
    private DbsField work_Request_New_Log_Insttn_Srce_Cde;
    private DbsField work_Request_New_D37;
    private DbsField work_Request_New_Instn_Cde;
    private DbsField work_Request_New_D38;
    private DbsField work_Request_New_Rqst_Instn_Cde;
    private DbsField work_Request_New_D39;
    private DbsField work_Request_New_Rqst_Rgn_Cde;
    private DbsField work_Request_New_D40;
    private DbsField work_Request_New_Rqst_Spcl_Dsgntn_Cde;
    private DbsField work_Request_New_D41;
    private DbsField work_Request_New_Rqst_Brnch_Cde;
    private DbsField work_Request_New_D42;
    private DbsField work_Request_New_Extrnl_Pend_Ind;
    private DbsField work_Request_New_D43;
    private DbsField work_Request_New_Crprte_Due_Dte_Tme_A;
    private DbsField work_Request_New_D44;
    private DbsField work_Request_New_Mis_Routed_Ind;
    private DbsField work_Request_New_D45;
    private DbsField work_Request_New_Dte_Of_Birth;
    private DbsField work_Request_New_D46;
    private DbsField work_Request_New_Trade_Dte_Tme_A;
    private DbsField work_Request_New_D47;
    private DbsField work_Request_New_Mj_Media_Ind;
    private DbsField work_Request_New_D48;
    private DbsField work_Request_New_State_Of_Res;
    private DbsField work_Request_New_D49;
    private DbsField work_Request_New_Sec_Turnaround_Tme_A;
    private DbsField work_Request_New_D50;
    private DbsField work_Request_New_Sec_Updte_Dte_A;
    private DbsField work_Request_New_D51;
    private DbsField work_Request_New_Rqst_Indicators;
    private DbsField work_Request_New_D52;
    private DbsField work_Request_New_Part_Bus_Days;
    private DbsField work_Request_New_D53;
    private DbsField work_Request_New_Part_Cal_Days;
    private DbsField work_Request_New_D54;
    private DbsField work_Request_New_Start_Dte_Type;
    private DbsField work_Request_New_D55;
    private DbsField work_Request_New_Acknldg_Dte_Tme_A;
    private DbsField work_Request_New_D56;
    private DbsField work_Request_New_Acknldg_Tat_Tme;
    private DbsField work_Request_New_D57;
    private DbsField work_Request_New_Acknldg_Reqd_Ind;
    private DbsField work_Request_New_D58;
    private DbsField work_Request_New_Acknldg_On_Time_Ind;
    private DbsField work_Request_New_D59;
    private DbsField work_Request_New_Last_Chnge_Dte_Tme_A;
    private DbsField work_Request_New_D60;
    private DbsField work_Request_New_Part_Close_Status_A;
    private DbsField work_Request_New_D61;
    private DbsField work_Request_New_Cnnctd_Rqst_Ind;
    private DbsField work_Request_New_D62;
    private DbsField work_Request_New_Prvte_Wrk_Rqst_Ind;
    private DbsField work_Request_New_D63;
    private DbsField work_Request_New_Instn_Rqst_Log_Dte_Tme_A;
    private DbsField work_Request_New_D64;
    private DbsField work_Request_New_Log_Sqnce_Nbr;
    private DbsField work_Request_New_D65;
    private DbsField work_Request_New_Start_Dte_Tme_A;
    private DbsField work_Request_New_D66;
    private DbsField work_Request_New_Admin_Status_Cde;
    private DbsField work_Request_New_D67;
    private DbsField work_Request_New_Crrnt_Cmt_Ind;
    private DbsField work_Request_New_D68;
    private DbsField work_Request_New_Np_Pin;
    private DbsField work_Request_New_D69;
    private DbsField work_Request_New_Dod_Not_Dte;
    private DbsField work_Request_New_D70;
    private DbsField work_Request_New_Da_Accum_Cntr;
    private DbsField work_Request_New_D71;
    private DbsField work_Request_New_Tpa_Accum_Cntr;
    private DbsField work_Request_New_D72;
    private DbsField work_Request_New_P_I_Accum_Cntr;
    private DbsField work_Request_New_D73;
    private DbsField work_Request_New_Ipro_Accum_Cntr;
    private DbsField work_Request_New_D74;
    private DbsField work_Request_New_Tot_Accum_Cntr;
    private DbsField work_Request_New_D75;
    private DbsField work_Request_New_Multi_Prod_Cde;
    private DbsField work_Request_New_D76;
    private DbsField work_Request_New_Complex_Txn_Cde;
    private DbsField work_Request_New_D77;
    private DbsField work_Request_New_Part_Age;
    private DbsField work_Request_New_D78;
    private DbsField work_Request_New_Pin_Npin;
    private DbsField work_Request_New_D79;
    private DbsField work_Request_New_Pin_Type_Ind;
    private DbsField work_Request_New_D80;
    private DbsField work_Request_New_Rqst_Log_Dte_Tme_D;
    private DbsField work_Request_New_D81;
    private DbsField work_Request_New_Admin_Unit_Cde;
    private DbsField work_Request_New_D82;
    private DbsField work_Request_New_Last_Updte_Oprtr_Cde;
    private DbsField work_Request_New_D83;
    private DbsField work_Request_New_Due_Dte_Prty;

    public DbsGroup getWork_Request_New() { return work_Request_New; }

    public DbsField getWork_Request_New_Rqst_Log_Dte_Tme_A() { return work_Request_New_Rqst_Log_Dte_Tme_A; }

    public DbsField getWork_Request_New_D1() { return work_Request_New_D1; }

    public DbsField getWork_Request_New_Work_Prcss_Id() { return work_Request_New_Work_Prcss_Id; }

    public DbsGroup getWork_Request_New_Work_Prcss_IdRedef1() { return work_Request_New_Work_Prcss_IdRedef1; }

    public DbsField getWork_Request_New_Work_Actn_Rqstd_Cde() { return work_Request_New_Work_Actn_Rqstd_Cde; }

    public DbsField getWork_Request_New_Work_Lob_Cmpny_Prdct_Cde() { return work_Request_New_Work_Lob_Cmpny_Prdct_Cde; }

    public DbsField getWork_Request_New_Work_Mjr_Bsnss_Prcss_Cde() { return work_Request_New_Work_Mjr_Bsnss_Prcss_Cde; }

    public DbsField getWork_Request_New_Work_Spcfc_Bsnss_Prcss_Cde() { return work_Request_New_Work_Spcfc_Bsnss_Prcss_Cde; }

    public DbsField getWork_Request_New_D2() { return work_Request_New_D2; }

    public DbsField getWork_Request_New_Pin_Nbr() { return work_Request_New_Pin_Nbr; }

    public DbsField getWork_Request_New_D3() { return work_Request_New_D3; }

    public DbsField getWork_Request_New_Part_Name() { return work_Request_New_Part_Name; }

    public DbsField getWork_Request_New_D4() { return work_Request_New_D4; }

    public DbsField getWork_Request_New_Ssn() { return work_Request_New_Ssn; }

    public DbsField getWork_Request_New_D5() { return work_Request_New_D5; }

    public DbsField getWork_Request_New_Tiaa_Rcvd_Dte_Tme_A() { return work_Request_New_Tiaa_Rcvd_Dte_Tme_A; }

    public DbsField getWork_Request_New_D6() { return work_Request_New_D6; }

    public DbsField getWork_Request_New_Tiaa_Rcvd_Dte_A() { return work_Request_New_Tiaa_Rcvd_Dte_A; }

    public DbsField getWork_Request_New_D7() { return work_Request_New_D7; }

    public DbsField getWork_Request_New_Orgnl_Unit_Cde() { return work_Request_New_Orgnl_Unit_Cde; }

    public DbsField getWork_Request_New_D8() { return work_Request_New_D8; }

    public DbsField getWork_Request_New_Rqst_Log_Oprtr_Cde() { return work_Request_New_Rqst_Log_Oprtr_Cde; }

    public DbsField getWork_Request_New_D9() { return work_Request_New_D9; }

    public DbsField getWork_Request_New_Rqst_Orgn_Cde() { return work_Request_New_Rqst_Orgn_Cde; }

    public DbsField getWork_Request_New_D10() { return work_Request_New_D10; }

    public DbsField getWork_Request_New_Crprte_Status_Ind() { return work_Request_New_Crprte_Status_Ind; }

    public DbsField getWork_Request_New_D11() { return work_Request_New_D11; }

    public DbsField getWork_Request_New_Crprte_Clock_End_Dte_Tme_A() { return work_Request_New_Crprte_Clock_End_Dte_Tme_A; }

    public DbsField getWork_Request_New_D12() { return work_Request_New_D12; }

    public DbsField getWork_Request_New_Final_Close_Out_Dte_Tme_A() { return work_Request_New_Final_Close_Out_Dte_Tme_A; }

    public DbsField getWork_Request_New_D13() { return work_Request_New_D13; }

    public DbsField getWork_Request_New_Effctve_Dte_A() { return work_Request_New_Effctve_Dte_A; }

    public DbsField getWork_Request_New_D14() { return work_Request_New_D14; }

    public DbsField getWork_Request_New_Trans_Dte_A() { return work_Request_New_Trans_Dte_A; }

    public DbsField getWork_Request_New_D15() { return work_Request_New_D15; }

    public DbsField getWork_Request_New_Trnsctn_Dte_A() { return work_Request_New_Trnsctn_Dte_A; }

    public DbsField getWork_Request_New_D16() { return work_Request_New_D16; }

    public DbsField getWork_Request_New_Owner_Unit_Cde() { return work_Request_New_Owner_Unit_Cde; }

    public DbsField getWork_Request_New_D17() { return work_Request_New_D17; }

    public DbsField getWork_Request_New_Owner_Division() { return work_Request_New_Owner_Division; }

    public DbsField getWork_Request_New_D18() { return work_Request_New_D18; }

    public DbsField getWork_Request_New_Shphrd_Id() { return work_Request_New_Shphrd_Id; }

    public DbsField getWork_Request_New_D19() { return work_Request_New_D19; }

    public DbsField getWork_Request_New_Sec_Ind() { return work_Request_New_Sec_Ind; }

    public DbsField getWork_Request_New_D20() { return work_Request_New_D20; }

    public DbsField getWork_Request_New_Admin_Work_Ind() { return work_Request_New_Admin_Work_Ind; }

    public DbsField getWork_Request_New_D21() { return work_Request_New_D21; }

    public DbsField getWork_Request_New_Sub_Rqst_Ind() { return work_Request_New_Sub_Rqst_Ind; }

    public DbsField getWork_Request_New_D22() { return work_Request_New_D22; }

    public DbsField getWork_Request_New_Prnt_Rqst_Log_Dte_Tme_A() { return work_Request_New_Prnt_Rqst_Log_Dte_Tme_A; }

    public DbsField getWork_Request_New_D23() { return work_Request_New_D23; }

    public DbsField getWork_Request_New_Prnt_Work_Prcss_Id() { return work_Request_New_Prnt_Work_Prcss_Id; }

    public DbsField getWork_Request_New_D24() { return work_Request_New_D24; }

    public DbsField getWork_Request_New_Multi_Rqst_Ind() { return work_Request_New_Multi_Rqst_Ind; }

    public DbsField getWork_Request_New_D25() { return work_Request_New_D25; }

    public DbsField getWork_Request_New_Cmplnt_Ind() { return work_Request_New_Cmplnt_Ind; }

    public DbsField getWork_Request_New_D26() { return work_Request_New_D26; }

    public DbsField getWork_Request_New_Elctrnc_Fldr_Ind() { return work_Request_New_Elctrnc_Fldr_Ind; }

    public DbsField getWork_Request_New_D27() { return work_Request_New_D27; }

    public DbsField getWork_Request_New_Mj_Pull_Ind() { return work_Request_New_Mj_Pull_Ind; }

    public DbsField getWork_Request_New_D28() { return work_Request_New_D28; }

    public DbsField getWork_Request_New_Check_Ind() { return work_Request_New_Check_Ind; }

    public DbsField getWork_Request_New_D29() { return work_Request_New_D29; }

    public DbsField getWork_Request_New_Bsnss_Reply_Ind() { return work_Request_New_Bsnss_Reply_Ind; }

    public DbsField getWork_Request_New_D30() { return work_Request_New_D30; }

    public DbsField getWork_Request_New_Tlc_Ind() { return work_Request_New_Tlc_Ind; }

    public DbsField getWork_Request_New_D31() { return work_Request_New_D31; }

    public DbsField getWork_Request_New_Redo_Ind() { return work_Request_New_Redo_Ind; }

    public DbsField getWork_Request_New_D32() { return work_Request_New_D32; }

    public DbsField getWork_Request_New_Crprte_On_Tme_Ind() { return work_Request_New_Crprte_On_Tme_Ind; }

    public DbsField getWork_Request_New_D33() { return work_Request_New_D33; }

    public DbsField getWork_Request_New_Off_Rtng_Ind() { return work_Request_New_Off_Rtng_Ind; }

    public DbsField getWork_Request_New_D34() { return work_Request_New_D34; }

    public DbsField getWork_Request_New_Prcssng_Type_Cde() { return work_Request_New_Prcssng_Type_Cde; }

    public DbsField getWork_Request_New_D35() { return work_Request_New_D35; }

    public DbsField getWork_Request_New_Log_Rqstr_Cde() { return work_Request_New_Log_Rqstr_Cde; }

    public DbsField getWork_Request_New_D36() { return work_Request_New_D36; }

    public DbsField getWork_Request_New_Log_Insttn_Srce_Cde() { return work_Request_New_Log_Insttn_Srce_Cde; }

    public DbsField getWork_Request_New_D37() { return work_Request_New_D37; }

    public DbsField getWork_Request_New_Instn_Cde() { return work_Request_New_Instn_Cde; }

    public DbsField getWork_Request_New_D38() { return work_Request_New_D38; }

    public DbsField getWork_Request_New_Rqst_Instn_Cde() { return work_Request_New_Rqst_Instn_Cde; }

    public DbsField getWork_Request_New_D39() { return work_Request_New_D39; }

    public DbsField getWork_Request_New_Rqst_Rgn_Cde() { return work_Request_New_Rqst_Rgn_Cde; }

    public DbsField getWork_Request_New_D40() { return work_Request_New_D40; }

    public DbsField getWork_Request_New_Rqst_Spcl_Dsgntn_Cde() { return work_Request_New_Rqst_Spcl_Dsgntn_Cde; }

    public DbsField getWork_Request_New_D41() { return work_Request_New_D41; }

    public DbsField getWork_Request_New_Rqst_Brnch_Cde() { return work_Request_New_Rqst_Brnch_Cde; }

    public DbsField getWork_Request_New_D42() { return work_Request_New_D42; }

    public DbsField getWork_Request_New_Extrnl_Pend_Ind() { return work_Request_New_Extrnl_Pend_Ind; }

    public DbsField getWork_Request_New_D43() { return work_Request_New_D43; }

    public DbsField getWork_Request_New_Crprte_Due_Dte_Tme_A() { return work_Request_New_Crprte_Due_Dte_Tme_A; }

    public DbsField getWork_Request_New_D44() { return work_Request_New_D44; }

    public DbsField getWork_Request_New_Mis_Routed_Ind() { return work_Request_New_Mis_Routed_Ind; }

    public DbsField getWork_Request_New_D45() { return work_Request_New_D45; }

    public DbsField getWork_Request_New_Dte_Of_Birth() { return work_Request_New_Dte_Of_Birth; }

    public DbsField getWork_Request_New_D46() { return work_Request_New_D46; }

    public DbsField getWork_Request_New_Trade_Dte_Tme_A() { return work_Request_New_Trade_Dte_Tme_A; }

    public DbsField getWork_Request_New_D47() { return work_Request_New_D47; }

    public DbsField getWork_Request_New_Mj_Media_Ind() { return work_Request_New_Mj_Media_Ind; }

    public DbsField getWork_Request_New_D48() { return work_Request_New_D48; }

    public DbsField getWork_Request_New_State_Of_Res() { return work_Request_New_State_Of_Res; }

    public DbsField getWork_Request_New_D49() { return work_Request_New_D49; }

    public DbsField getWork_Request_New_Sec_Turnaround_Tme_A() { return work_Request_New_Sec_Turnaround_Tme_A; }

    public DbsField getWork_Request_New_D50() { return work_Request_New_D50; }

    public DbsField getWork_Request_New_Sec_Updte_Dte_A() { return work_Request_New_Sec_Updte_Dte_A; }

    public DbsField getWork_Request_New_D51() { return work_Request_New_D51; }

    public DbsField getWork_Request_New_Rqst_Indicators() { return work_Request_New_Rqst_Indicators; }

    public DbsField getWork_Request_New_D52() { return work_Request_New_D52; }

    public DbsField getWork_Request_New_Part_Bus_Days() { return work_Request_New_Part_Bus_Days; }

    public DbsField getWork_Request_New_D53() { return work_Request_New_D53; }

    public DbsField getWork_Request_New_Part_Cal_Days() { return work_Request_New_Part_Cal_Days; }

    public DbsField getWork_Request_New_D54() { return work_Request_New_D54; }

    public DbsField getWork_Request_New_Start_Dte_Type() { return work_Request_New_Start_Dte_Type; }

    public DbsField getWork_Request_New_D55() { return work_Request_New_D55; }

    public DbsField getWork_Request_New_Acknldg_Dte_Tme_A() { return work_Request_New_Acknldg_Dte_Tme_A; }

    public DbsField getWork_Request_New_D56() { return work_Request_New_D56; }

    public DbsField getWork_Request_New_Acknldg_Tat_Tme() { return work_Request_New_Acknldg_Tat_Tme; }

    public DbsField getWork_Request_New_D57() { return work_Request_New_D57; }

    public DbsField getWork_Request_New_Acknldg_Reqd_Ind() { return work_Request_New_Acknldg_Reqd_Ind; }

    public DbsField getWork_Request_New_D58() { return work_Request_New_D58; }

    public DbsField getWork_Request_New_Acknldg_On_Time_Ind() { return work_Request_New_Acknldg_On_Time_Ind; }

    public DbsField getWork_Request_New_D59() { return work_Request_New_D59; }

    public DbsField getWork_Request_New_Last_Chnge_Dte_Tme_A() { return work_Request_New_Last_Chnge_Dte_Tme_A; }

    public DbsField getWork_Request_New_D60() { return work_Request_New_D60; }

    public DbsField getWork_Request_New_Part_Close_Status_A() { return work_Request_New_Part_Close_Status_A; }

    public DbsField getWork_Request_New_D61() { return work_Request_New_D61; }

    public DbsField getWork_Request_New_Cnnctd_Rqst_Ind() { return work_Request_New_Cnnctd_Rqst_Ind; }

    public DbsField getWork_Request_New_D62() { return work_Request_New_D62; }

    public DbsField getWork_Request_New_Prvte_Wrk_Rqst_Ind() { return work_Request_New_Prvte_Wrk_Rqst_Ind; }

    public DbsField getWork_Request_New_D63() { return work_Request_New_D63; }

    public DbsField getWork_Request_New_Instn_Rqst_Log_Dte_Tme_A() { return work_Request_New_Instn_Rqst_Log_Dte_Tme_A; }

    public DbsField getWork_Request_New_D64() { return work_Request_New_D64; }

    public DbsField getWork_Request_New_Log_Sqnce_Nbr() { return work_Request_New_Log_Sqnce_Nbr; }

    public DbsField getWork_Request_New_D65() { return work_Request_New_D65; }

    public DbsField getWork_Request_New_Start_Dte_Tme_A() { return work_Request_New_Start_Dte_Tme_A; }

    public DbsField getWork_Request_New_D66() { return work_Request_New_D66; }

    public DbsField getWork_Request_New_Admin_Status_Cde() { return work_Request_New_Admin_Status_Cde; }

    public DbsField getWork_Request_New_D67() { return work_Request_New_D67; }

    public DbsField getWork_Request_New_Crrnt_Cmt_Ind() { return work_Request_New_Crrnt_Cmt_Ind; }

    public DbsField getWork_Request_New_D68() { return work_Request_New_D68; }

    public DbsField getWork_Request_New_Np_Pin() { return work_Request_New_Np_Pin; }

    public DbsField getWork_Request_New_D69() { return work_Request_New_D69; }

    public DbsField getWork_Request_New_Dod_Not_Dte() { return work_Request_New_Dod_Not_Dte; }

    public DbsField getWork_Request_New_D70() { return work_Request_New_D70; }

    public DbsField getWork_Request_New_Da_Accum_Cntr() { return work_Request_New_Da_Accum_Cntr; }

    public DbsField getWork_Request_New_D71() { return work_Request_New_D71; }

    public DbsField getWork_Request_New_Tpa_Accum_Cntr() { return work_Request_New_Tpa_Accum_Cntr; }

    public DbsField getWork_Request_New_D72() { return work_Request_New_D72; }

    public DbsField getWork_Request_New_P_I_Accum_Cntr() { return work_Request_New_P_I_Accum_Cntr; }

    public DbsField getWork_Request_New_D73() { return work_Request_New_D73; }

    public DbsField getWork_Request_New_Ipro_Accum_Cntr() { return work_Request_New_Ipro_Accum_Cntr; }

    public DbsField getWork_Request_New_D74() { return work_Request_New_D74; }

    public DbsField getWork_Request_New_Tot_Accum_Cntr() { return work_Request_New_Tot_Accum_Cntr; }

    public DbsField getWork_Request_New_D75() { return work_Request_New_D75; }

    public DbsField getWork_Request_New_Multi_Prod_Cde() { return work_Request_New_Multi_Prod_Cde; }

    public DbsField getWork_Request_New_D76() { return work_Request_New_D76; }

    public DbsField getWork_Request_New_Complex_Txn_Cde() { return work_Request_New_Complex_Txn_Cde; }

    public DbsField getWork_Request_New_D77() { return work_Request_New_D77; }

    public DbsField getWork_Request_New_Part_Age() { return work_Request_New_Part_Age; }

    public DbsField getWork_Request_New_D78() { return work_Request_New_D78; }

    public DbsField getWork_Request_New_Pin_Npin() { return work_Request_New_Pin_Npin; }

    public DbsField getWork_Request_New_D79() { return work_Request_New_D79; }

    public DbsField getWork_Request_New_Pin_Type_Ind() { return work_Request_New_Pin_Type_Ind; }

    public DbsField getWork_Request_New_D80() { return work_Request_New_D80; }

    public DbsField getWork_Request_New_Rqst_Log_Dte_Tme_D() { return work_Request_New_Rqst_Log_Dte_Tme_D; }

    public DbsField getWork_Request_New_D81() { return work_Request_New_D81; }

    public DbsField getWork_Request_New_Admin_Unit_Cde() { return work_Request_New_Admin_Unit_Cde; }

    public DbsField getWork_Request_New_D82() { return work_Request_New_D82; }

    public DbsField getWork_Request_New_Last_Updte_Oprtr_Cde() { return work_Request_New_Last_Updte_Oprtr_Cde; }

    public DbsField getWork_Request_New_D83() { return work_Request_New_D83; }

    public DbsField getWork_Request_New_Due_Dte_Prty() { return work_Request_New_Due_Dte_Prty; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        work_Request_New = newGroupInRecord("work_Request_New", "WORK-REQUEST-NEW");
        work_Request_New_Rqst_Log_Dte_Tme_A = work_Request_New.newFieldInGroup("work_Request_New_Rqst_Log_Dte_Tme_A", "RQST-LOG-DTE-TME-A", FieldType.STRING, 
            28);
        work_Request_New_D1 = work_Request_New.newFieldInGroup("work_Request_New_D1", "D1", FieldType.STRING, 1);
        work_Request_New_Work_Prcss_Id = work_Request_New.newFieldInGroup("work_Request_New_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        work_Request_New_Work_Prcss_IdRedef1 = work_Request_New.newGroupInGroup("work_Request_New_Work_Prcss_IdRedef1", "Redefines", work_Request_New_Work_Prcss_Id);
        work_Request_New_Work_Actn_Rqstd_Cde = work_Request_New_Work_Prcss_IdRedef1.newFieldInGroup("work_Request_New_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        work_Request_New_Work_Lob_Cmpny_Prdct_Cde = work_Request_New_Work_Prcss_IdRedef1.newFieldInGroup("work_Request_New_Work_Lob_Cmpny_Prdct_Cde", 
            "WORK-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        work_Request_New_Work_Mjr_Bsnss_Prcss_Cde = work_Request_New_Work_Prcss_IdRedef1.newFieldInGroup("work_Request_New_Work_Mjr_Bsnss_Prcss_Cde", 
            "WORK-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        work_Request_New_Work_Spcfc_Bsnss_Prcss_Cde = work_Request_New_Work_Prcss_IdRedef1.newFieldInGroup("work_Request_New_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        work_Request_New_D2 = work_Request_New.newFieldInGroup("work_Request_New_D2", "D2", FieldType.STRING, 1);
        work_Request_New_Pin_Nbr = work_Request_New.newFieldInGroup("work_Request_New_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 7);
        work_Request_New_D3 = work_Request_New.newFieldInGroup("work_Request_New_D3", "D3", FieldType.STRING, 1);
        work_Request_New_Part_Name = work_Request_New.newFieldInGroup("work_Request_New_Part_Name", "PART-NAME", FieldType.STRING, 40);
        work_Request_New_D4 = work_Request_New.newFieldInGroup("work_Request_New_D4", "D4", FieldType.STRING, 1);
        work_Request_New_Ssn = work_Request_New.newFieldInGroup("work_Request_New_Ssn", "SSN", FieldType.NUMERIC, 9);
        work_Request_New_D5 = work_Request_New.newFieldInGroup("work_Request_New_D5", "D5", FieldType.STRING, 1);
        work_Request_New_Tiaa_Rcvd_Dte_Tme_A = work_Request_New.newFieldInGroup("work_Request_New_Tiaa_Rcvd_Dte_Tme_A", "TIAA-RCVD-DTE-TME-A", FieldType.STRING, 
            28);
        work_Request_New_D6 = work_Request_New.newFieldInGroup("work_Request_New_D6", "D6", FieldType.STRING, 1);
        work_Request_New_Tiaa_Rcvd_Dte_A = work_Request_New.newFieldInGroup("work_Request_New_Tiaa_Rcvd_Dte_A", "TIAA-RCVD-DTE-A", FieldType.STRING, 12);
        work_Request_New_D7 = work_Request_New.newFieldInGroup("work_Request_New_D7", "D7", FieldType.STRING, 1);
        work_Request_New_Orgnl_Unit_Cde = work_Request_New.newFieldInGroup("work_Request_New_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        work_Request_New_D8 = work_Request_New.newFieldInGroup("work_Request_New_D8", "D8", FieldType.STRING, 1);
        work_Request_New_Rqst_Log_Oprtr_Cde = work_Request_New.newFieldInGroup("work_Request_New_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8);
        work_Request_New_D9 = work_Request_New.newFieldInGroup("work_Request_New_D9", "D9", FieldType.STRING, 1);
        work_Request_New_Rqst_Orgn_Cde = work_Request_New.newFieldInGroup("work_Request_New_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1);
        work_Request_New_D10 = work_Request_New.newFieldInGroup("work_Request_New_D10", "D10", FieldType.STRING, 1);
        work_Request_New_Crprte_Status_Ind = work_Request_New.newFieldInGroup("work_Request_New_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1);
        work_Request_New_D11 = work_Request_New.newFieldInGroup("work_Request_New_D11", "D11", FieldType.STRING, 1);
        work_Request_New_Crprte_Clock_End_Dte_Tme_A = work_Request_New.newFieldInGroup("work_Request_New_Crprte_Clock_End_Dte_Tme_A", "CRPRTE-CLOCK-END-DTE-TME-A", 
            FieldType.STRING, 28);
        work_Request_New_D12 = work_Request_New.newFieldInGroup("work_Request_New_D12", "D12", FieldType.STRING, 1);
        work_Request_New_Final_Close_Out_Dte_Tme_A = work_Request_New.newFieldInGroup("work_Request_New_Final_Close_Out_Dte_Tme_A", "FINAL-CLOSE-OUT-DTE-TME-A", 
            FieldType.STRING, 26);
        work_Request_New_D13 = work_Request_New.newFieldInGroup("work_Request_New_D13", "D13", FieldType.STRING, 1);
        work_Request_New_Effctve_Dte_A = work_Request_New.newFieldInGroup("work_Request_New_Effctve_Dte_A", "EFFCTVE-DTE-A", FieldType.STRING, 12);
        work_Request_New_D14 = work_Request_New.newFieldInGroup("work_Request_New_D14", "D14", FieldType.STRING, 1);
        work_Request_New_Trans_Dte_A = work_Request_New.newFieldInGroup("work_Request_New_Trans_Dte_A", "TRANS-DTE-A", FieldType.STRING, 12);
        work_Request_New_D15 = work_Request_New.newFieldInGroup("work_Request_New_D15", "D15", FieldType.STRING, 1);
        work_Request_New_Trnsctn_Dte_A = work_Request_New.newFieldInGroup("work_Request_New_Trnsctn_Dte_A", "TRNSCTN-DTE-A", FieldType.STRING, 12);
        work_Request_New_D16 = work_Request_New.newFieldInGroup("work_Request_New_D16", "D16", FieldType.STRING, 1);
        work_Request_New_Owner_Unit_Cde = work_Request_New.newFieldInGroup("work_Request_New_Owner_Unit_Cde", "OWNER-UNIT-CDE", FieldType.STRING, 8);
        work_Request_New_D17 = work_Request_New.newFieldInGroup("work_Request_New_D17", "D17", FieldType.STRING, 1);
        work_Request_New_Owner_Division = work_Request_New.newFieldInGroup("work_Request_New_Owner_Division", "OWNER-DIVISION", FieldType.STRING, 6);
        work_Request_New_D18 = work_Request_New.newFieldInGroup("work_Request_New_D18", "D18", FieldType.STRING, 1);
        work_Request_New_Shphrd_Id = work_Request_New.newFieldInGroup("work_Request_New_Shphrd_Id", "SHPHRD-ID", FieldType.STRING, 8);
        work_Request_New_D19 = work_Request_New.newFieldInGroup("work_Request_New_D19", "D19", FieldType.STRING, 1);
        work_Request_New_Sec_Ind = work_Request_New.newFieldInGroup("work_Request_New_Sec_Ind", "SEC-IND", FieldType.STRING, 1);
        work_Request_New_D20 = work_Request_New.newFieldInGroup("work_Request_New_D20", "D20", FieldType.STRING, 1);
        work_Request_New_Admin_Work_Ind = work_Request_New.newFieldInGroup("work_Request_New_Admin_Work_Ind", "ADMIN-WORK-IND", FieldType.STRING, 1);
        work_Request_New_D21 = work_Request_New.newFieldInGroup("work_Request_New_D21", "D21", FieldType.STRING, 1);
        work_Request_New_Sub_Rqst_Ind = work_Request_New.newFieldInGroup("work_Request_New_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 1);
        work_Request_New_D22 = work_Request_New.newFieldInGroup("work_Request_New_D22", "D22", FieldType.STRING, 1);
        work_Request_New_Prnt_Rqst_Log_Dte_Tme_A = work_Request_New.newFieldInGroup("work_Request_New_Prnt_Rqst_Log_Dte_Tme_A", "PRNT-RQST-LOG-DTE-TME-A", 
            FieldType.STRING, 28);
        work_Request_New_D23 = work_Request_New.newFieldInGroup("work_Request_New_D23", "D23", FieldType.STRING, 1);
        work_Request_New_Prnt_Work_Prcss_Id = work_Request_New.newFieldInGroup("work_Request_New_Prnt_Work_Prcss_Id", "PRNT-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        work_Request_New_D24 = work_Request_New.newFieldInGroup("work_Request_New_D24", "D24", FieldType.STRING, 1);
        work_Request_New_Multi_Rqst_Ind = work_Request_New.newFieldInGroup("work_Request_New_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 1);
        work_Request_New_D25 = work_Request_New.newFieldInGroup("work_Request_New_D25", "D25", FieldType.STRING, 1);
        work_Request_New_Cmplnt_Ind = work_Request_New.newFieldInGroup("work_Request_New_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 1);
        work_Request_New_D26 = work_Request_New.newFieldInGroup("work_Request_New_D26", "D26", FieldType.STRING, 1);
        work_Request_New_Elctrnc_Fldr_Ind = work_Request_New.newFieldInGroup("work_Request_New_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 
            1);
        work_Request_New_D27 = work_Request_New.newFieldInGroup("work_Request_New_D27", "D27", FieldType.STRING, 1);
        work_Request_New_Mj_Pull_Ind = work_Request_New.newFieldInGroup("work_Request_New_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1);
        work_Request_New_D28 = work_Request_New.newFieldInGroup("work_Request_New_D28", "D28", FieldType.STRING, 1);
        work_Request_New_Check_Ind = work_Request_New.newFieldInGroup("work_Request_New_Check_Ind", "CHECK-IND", FieldType.STRING, 1);
        work_Request_New_D29 = work_Request_New.newFieldInGroup("work_Request_New_D29", "D29", FieldType.STRING, 1);
        work_Request_New_Bsnss_Reply_Ind = work_Request_New.newFieldInGroup("work_Request_New_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", FieldType.STRING, 1);
        work_Request_New_D30 = work_Request_New.newFieldInGroup("work_Request_New_D30", "D30", FieldType.STRING, 1);
        work_Request_New_Tlc_Ind = work_Request_New.newFieldInGroup("work_Request_New_Tlc_Ind", "TLC-IND", FieldType.STRING, 1);
        work_Request_New_D31 = work_Request_New.newFieldInGroup("work_Request_New_D31", "D31", FieldType.STRING, 1);
        work_Request_New_Redo_Ind = work_Request_New.newFieldInGroup("work_Request_New_Redo_Ind", "REDO-IND", FieldType.STRING, 1);
        work_Request_New_D32 = work_Request_New.newFieldInGroup("work_Request_New_D32", "D32", FieldType.STRING, 1);
        work_Request_New_Crprte_On_Tme_Ind = work_Request_New.newFieldInGroup("work_Request_New_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", FieldType.STRING, 
            1);
        work_Request_New_D33 = work_Request_New.newFieldInGroup("work_Request_New_D33", "D33", FieldType.STRING, 1);
        work_Request_New_Off_Rtng_Ind = work_Request_New.newFieldInGroup("work_Request_New_Off_Rtng_Ind", "OFF-RTNG-IND", FieldType.STRING, 1);
        work_Request_New_D34 = work_Request_New.newFieldInGroup("work_Request_New_D34", "D34", FieldType.STRING, 1);
        work_Request_New_Prcssng_Type_Cde = work_Request_New.newFieldInGroup("work_Request_New_Prcssng_Type_Cde", "PRCSSNG-TYPE-CDE", FieldType.STRING, 
            1);
        work_Request_New_D35 = work_Request_New.newFieldInGroup("work_Request_New_D35", "D35", FieldType.STRING, 1);
        work_Request_New_Log_Rqstr_Cde = work_Request_New.newFieldInGroup("work_Request_New_Log_Rqstr_Cde", "LOG-RQSTR-CDE", FieldType.STRING, 1);
        work_Request_New_D36 = work_Request_New.newFieldInGroup("work_Request_New_D36", "D36", FieldType.STRING, 1);
        work_Request_New_Log_Insttn_Srce_Cde = work_Request_New.newFieldInGroup("work_Request_New_Log_Insttn_Srce_Cde", "LOG-INSTTN-SRCE-CDE", FieldType.STRING, 
            5);
        work_Request_New_D37 = work_Request_New.newFieldInGroup("work_Request_New_D37", "D37", FieldType.STRING, 1);
        work_Request_New_Instn_Cde = work_Request_New.newFieldInGroup("work_Request_New_Instn_Cde", "INSTN-CDE", FieldType.STRING, 5);
        work_Request_New_D38 = work_Request_New.newFieldInGroup("work_Request_New_D38", "D38", FieldType.STRING, 1);
        work_Request_New_Rqst_Instn_Cde = work_Request_New.newFieldInGroup("work_Request_New_Rqst_Instn_Cde", "RQST-INSTN-CDE", FieldType.STRING, 5);
        work_Request_New_D39 = work_Request_New.newFieldInGroup("work_Request_New_D39", "D39", FieldType.STRING, 1);
        work_Request_New_Rqst_Rgn_Cde = work_Request_New.newFieldInGroup("work_Request_New_Rqst_Rgn_Cde", "RQST-RGN-CDE", FieldType.STRING, 1);
        work_Request_New_D40 = work_Request_New.newFieldInGroup("work_Request_New_D40", "D40", FieldType.STRING, 1);
        work_Request_New_Rqst_Spcl_Dsgntn_Cde = work_Request_New.newFieldInGroup("work_Request_New_Rqst_Spcl_Dsgntn_Cde", "RQST-SPCL-DSGNTN-CDE", FieldType.STRING, 
            1);
        work_Request_New_D41 = work_Request_New.newFieldInGroup("work_Request_New_D41", "D41", FieldType.STRING, 1);
        work_Request_New_Rqst_Brnch_Cde = work_Request_New.newFieldInGroup("work_Request_New_Rqst_Brnch_Cde", "RQST-BRNCH-CDE", FieldType.STRING, 1);
        work_Request_New_D42 = work_Request_New.newFieldInGroup("work_Request_New_D42", "D42", FieldType.STRING, 1);
        work_Request_New_Extrnl_Pend_Ind = work_Request_New.newFieldInGroup("work_Request_New_Extrnl_Pend_Ind", "EXTRNL-PEND-IND", FieldType.STRING, 1);
        work_Request_New_D43 = work_Request_New.newFieldInGroup("work_Request_New_D43", "D43", FieldType.STRING, 1);
        work_Request_New_Crprte_Due_Dte_Tme_A = work_Request_New.newFieldInGroup("work_Request_New_Crprte_Due_Dte_Tme_A", "CRPRTE-DUE-DTE-TME-A", FieldType.STRING, 
            28);
        work_Request_New_D44 = work_Request_New.newFieldInGroup("work_Request_New_D44", "D44", FieldType.STRING, 1);
        work_Request_New_Mis_Routed_Ind = work_Request_New.newFieldInGroup("work_Request_New_Mis_Routed_Ind", "MIS-ROUTED-IND", FieldType.STRING, 1);
        work_Request_New_D45 = work_Request_New.newFieldInGroup("work_Request_New_D45", "D45", FieldType.STRING, 1);
        work_Request_New_Dte_Of_Birth = work_Request_New.newFieldInGroup("work_Request_New_Dte_Of_Birth", "DTE-OF-BIRTH", FieldType.STRING, 12);
        work_Request_New_D46 = work_Request_New.newFieldInGroup("work_Request_New_D46", "D46", FieldType.STRING, 1);
        work_Request_New_Trade_Dte_Tme_A = work_Request_New.newFieldInGroup("work_Request_New_Trade_Dte_Tme_A", "TRADE-DTE-TME-A", FieldType.STRING, 28);
        work_Request_New_D47 = work_Request_New.newFieldInGroup("work_Request_New_D47", "D47", FieldType.STRING, 1);
        work_Request_New_Mj_Media_Ind = work_Request_New.newFieldInGroup("work_Request_New_Mj_Media_Ind", "MJ-MEDIA-IND", FieldType.STRING, 1);
        work_Request_New_D48 = work_Request_New.newFieldInGroup("work_Request_New_D48", "D48", FieldType.STRING, 1);
        work_Request_New_State_Of_Res = work_Request_New.newFieldInGroup("work_Request_New_State_Of_Res", "STATE-OF-RES", FieldType.STRING, 2);
        work_Request_New_D49 = work_Request_New.newFieldInGroup("work_Request_New_D49", "D49", FieldType.STRING, 1);
        work_Request_New_Sec_Turnaround_Tme_A = work_Request_New.newFieldInGroup("work_Request_New_Sec_Turnaround_Tme_A", "SEC-TURNAROUND-TME-A", FieldType.STRING, 
            10);
        work_Request_New_D50 = work_Request_New.newFieldInGroup("work_Request_New_D50", "D50", FieldType.STRING, 1);
        work_Request_New_Sec_Updte_Dte_A = work_Request_New.newFieldInGroup("work_Request_New_Sec_Updte_Dte_A", "SEC-UPDTE-DTE-A", FieldType.STRING, 12);
        work_Request_New_D51 = work_Request_New.newFieldInGroup("work_Request_New_D51", "D51", FieldType.STRING, 1);
        work_Request_New_Rqst_Indicators = work_Request_New.newFieldInGroup("work_Request_New_Rqst_Indicators", "RQST-INDICATORS", FieldType.STRING, 25);
        work_Request_New_D52 = work_Request_New.newFieldInGroup("work_Request_New_D52", "D52", FieldType.STRING, 1);
        work_Request_New_Part_Bus_Days = work_Request_New.newFieldInGroup("work_Request_New_Part_Bus_Days", "PART-BUS-DAYS", FieldType.STRING, 7);
        work_Request_New_D53 = work_Request_New.newFieldInGroup("work_Request_New_D53", "D53", FieldType.STRING, 1);
        work_Request_New_Part_Cal_Days = work_Request_New.newFieldInGroup("work_Request_New_Part_Cal_Days", "PART-CAL-DAYS", FieldType.STRING, 7);
        work_Request_New_D54 = work_Request_New.newFieldInGroup("work_Request_New_D54", "D54", FieldType.STRING, 1);
        work_Request_New_Start_Dte_Type = work_Request_New.newFieldInGroup("work_Request_New_Start_Dte_Type", "START-DTE-TYPE", FieldType.STRING, 1);
        work_Request_New_D55 = work_Request_New.newFieldInGroup("work_Request_New_D55", "D55", FieldType.STRING, 1);
        work_Request_New_Acknldg_Dte_Tme_A = work_Request_New.newFieldInGroup("work_Request_New_Acknldg_Dte_Tme_A", "ACKNLDG-DTE-TME-A", FieldType.STRING, 
            28);
        work_Request_New_D56 = work_Request_New.newFieldInGroup("work_Request_New_D56", "D56", FieldType.STRING, 1);
        work_Request_New_Acknldg_Tat_Tme = work_Request_New.newFieldInGroup("work_Request_New_Acknldg_Tat_Tme", "ACKNLDG-TAT-TME", FieldType.STRING, 10);
        work_Request_New_D57 = work_Request_New.newFieldInGroup("work_Request_New_D57", "D57", FieldType.STRING, 1);
        work_Request_New_Acknldg_Reqd_Ind = work_Request_New.newFieldInGroup("work_Request_New_Acknldg_Reqd_Ind", "ACKNLDG-REQD-IND", FieldType.STRING, 
            1);
        work_Request_New_D58 = work_Request_New.newFieldInGroup("work_Request_New_D58", "D58", FieldType.STRING, 1);
        work_Request_New_Acknldg_On_Time_Ind = work_Request_New.newFieldInGroup("work_Request_New_Acknldg_On_Time_Ind", "ACKNLDG-ON-TIME-IND", FieldType.STRING, 
            1);
        work_Request_New_D59 = work_Request_New.newFieldInGroup("work_Request_New_D59", "D59", FieldType.STRING, 1);
        work_Request_New_Last_Chnge_Dte_Tme_A = work_Request_New.newFieldInGroup("work_Request_New_Last_Chnge_Dte_Tme_A", "LAST-CHNGE-DTE-TME-A", FieldType.STRING, 
            28);
        work_Request_New_D60 = work_Request_New.newFieldInGroup("work_Request_New_D60", "D60", FieldType.STRING, 1);
        work_Request_New_Part_Close_Status_A = work_Request_New.newFieldInGroup("work_Request_New_Part_Close_Status_A", "PART-CLOSE-STATUS-A", FieldType.STRING, 
            4);
        work_Request_New_D61 = work_Request_New.newFieldInGroup("work_Request_New_D61", "D61", FieldType.STRING, 1);
        work_Request_New_Cnnctd_Rqst_Ind = work_Request_New.newFieldInGroup("work_Request_New_Cnnctd_Rqst_Ind", "CNNCTD-RQST-IND", FieldType.STRING, 2);
        work_Request_New_D62 = work_Request_New.newFieldInGroup("work_Request_New_D62", "D62", FieldType.STRING, 1);
        work_Request_New_Prvte_Wrk_Rqst_Ind = work_Request_New.newFieldInGroup("work_Request_New_Prvte_Wrk_Rqst_Ind", "PRVTE-WRK-RQST-IND", FieldType.STRING, 
            1);
        work_Request_New_D63 = work_Request_New.newFieldInGroup("work_Request_New_D63", "D63", FieldType.STRING, 1);
        work_Request_New_Instn_Rqst_Log_Dte_Tme_A = work_Request_New.newFieldInGroup("work_Request_New_Instn_Rqst_Log_Dte_Tme_A", "INSTN-RQST-LOG-DTE-TME-A", 
            FieldType.STRING, 28);
        work_Request_New_D64 = work_Request_New.newFieldInGroup("work_Request_New_D64", "D64", FieldType.STRING, 1);
        work_Request_New_Log_Sqnce_Nbr = work_Request_New.newFieldInGroup("work_Request_New_Log_Sqnce_Nbr", "LOG-SQNCE-NBR", FieldType.NUMERIC, 3);
        work_Request_New_D65 = work_Request_New.newFieldInGroup("work_Request_New_D65", "D65", FieldType.STRING, 1);
        work_Request_New_Start_Dte_Tme_A = work_Request_New.newFieldInGroup("work_Request_New_Start_Dte_Tme_A", "START-DTE-TME-A", FieldType.STRING, 28);
        work_Request_New_D66 = work_Request_New.newFieldInGroup("work_Request_New_D66", "D66", FieldType.STRING, 1);
        work_Request_New_Admin_Status_Cde = work_Request_New.newFieldInGroup("work_Request_New_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4);
        work_Request_New_D67 = work_Request_New.newFieldInGroup("work_Request_New_D67", "D67", FieldType.STRING, 1);
        work_Request_New_Crrnt_Cmt_Ind = work_Request_New.newFieldInGroup("work_Request_New_Crrnt_Cmt_Ind", "CRRNT-CMT-IND", FieldType.STRING, 1);
        work_Request_New_D68 = work_Request_New.newFieldInGroup("work_Request_New_D68", "D68", FieldType.STRING, 1);
        work_Request_New_Np_Pin = work_Request_New.newFieldInGroup("work_Request_New_Np_Pin", "NP-PIN", FieldType.STRING, 7);
        work_Request_New_D69 = work_Request_New.newFieldInGroup("work_Request_New_D69", "D69", FieldType.STRING, 1);
        work_Request_New_Dod_Not_Dte = work_Request_New.newFieldInGroup("work_Request_New_Dod_Not_Dte", "DOD-NOT-DTE", FieldType.STRING, 10);
        work_Request_New_D70 = work_Request_New.newFieldInGroup("work_Request_New_D70", "D70", FieldType.STRING, 1);
        work_Request_New_Da_Accum_Cntr = work_Request_New.newFieldInGroup("work_Request_New_Da_Accum_Cntr", "DA-ACCUM-CNTR", FieldType.STRING, 14);
        work_Request_New_D71 = work_Request_New.newFieldInGroup("work_Request_New_D71", "D71", FieldType.STRING, 1);
        work_Request_New_Tpa_Accum_Cntr = work_Request_New.newFieldInGroup("work_Request_New_Tpa_Accum_Cntr", "TPA-ACCUM-CNTR", FieldType.STRING, 14);
        work_Request_New_D72 = work_Request_New.newFieldInGroup("work_Request_New_D72", "D72", FieldType.STRING, 1);
        work_Request_New_P_I_Accum_Cntr = work_Request_New.newFieldInGroup("work_Request_New_P_I_Accum_Cntr", "P-I-ACCUM-CNTR", FieldType.STRING, 14);
        work_Request_New_D73 = work_Request_New.newFieldInGroup("work_Request_New_D73", "D73", FieldType.STRING, 1);
        work_Request_New_Ipro_Accum_Cntr = work_Request_New.newFieldInGroup("work_Request_New_Ipro_Accum_Cntr", "IPRO-ACCUM-CNTR", FieldType.STRING, 14);
        work_Request_New_D74 = work_Request_New.newFieldInGroup("work_Request_New_D74", "D74", FieldType.STRING, 1);
        work_Request_New_Tot_Accum_Cntr = work_Request_New.newFieldInGroup("work_Request_New_Tot_Accum_Cntr", "TOT-ACCUM-CNTR", FieldType.STRING, 14);
        work_Request_New_D75 = work_Request_New.newFieldInGroup("work_Request_New_D75", "D75", FieldType.STRING, 1);
        work_Request_New_Multi_Prod_Cde = work_Request_New.newFieldInGroup("work_Request_New_Multi_Prod_Cde", "MULTI-PROD-CDE", FieldType.STRING, 5);
        work_Request_New_D76 = work_Request_New.newFieldInGroup("work_Request_New_D76", "D76", FieldType.STRING, 1);
        work_Request_New_Complex_Txn_Cde = work_Request_New.newFieldInGroup("work_Request_New_Complex_Txn_Cde", "COMPLEX-TXN-CDE", FieldType.STRING, 5);
        work_Request_New_D77 = work_Request_New.newFieldInGroup("work_Request_New_D77", "D77", FieldType.STRING, 1);
        work_Request_New_Part_Age = work_Request_New.newFieldInGroup("work_Request_New_Part_Age", "PART-AGE", FieldType.NUMERIC, 3);
        work_Request_New_D78 = work_Request_New.newFieldInGroup("work_Request_New_D78", "D78", FieldType.STRING, 1);
        work_Request_New_Pin_Npin = work_Request_New.newFieldInGroup("work_Request_New_Pin_Npin", "PIN-NPIN", FieldType.STRING, 7);
        work_Request_New_D79 = work_Request_New.newFieldInGroup("work_Request_New_D79", "D79", FieldType.STRING, 1);
        work_Request_New_Pin_Type_Ind = work_Request_New.newFieldInGroup("work_Request_New_Pin_Type_Ind", "PIN-TYPE-IND", FieldType.STRING, 1);
        work_Request_New_D80 = work_Request_New.newFieldInGroup("work_Request_New_D80", "D80", FieldType.STRING, 1);
        work_Request_New_Rqst_Log_Dte_Tme_D = work_Request_New.newFieldInGroup("work_Request_New_Rqst_Log_Dte_Tme_D", "RQST-LOG-DTE-TME-D", FieldType.STRING, 
            24);
        work_Request_New_D81 = work_Request_New.newFieldInGroup("work_Request_New_D81", "D81", FieldType.STRING, 1);
        work_Request_New_Admin_Unit_Cde = work_Request_New.newFieldInGroup("work_Request_New_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        work_Request_New_D82 = work_Request_New.newFieldInGroup("work_Request_New_D82", "D82", FieldType.STRING, 1);
        work_Request_New_Last_Updte_Oprtr_Cde = work_Request_New.newFieldInGroup("work_Request_New_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        work_Request_New_D83 = work_Request_New.newFieldInGroup("work_Request_New_D83", "D83", FieldType.STRING, 1);
        work_Request_New_Due_Dte_Prty = work_Request_New.newFieldInGroup("work_Request_New_Due_Dte_Prty", "DUE-DTE-PRTY", FieldType.STRING, 1);

        this.setRecordName("LdaCwfl4899");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4899() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
