/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:05:05 PM
**        * FROM NATURAL LDA     : ICWL3010
************************************************************
**        * FILE NAME            : LdaIcwl3010.java
**        * CLASS NAME           : LdaIcwl3010
**        * INSTANCE NAME        : LdaIcwl3010
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIcwl3010 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_icw_Master_Index_View;
    private DbsField icw_Master_Index_View_Rqst_Log_Dte_Tme;
    private DbsGroup icw_Master_Index_View_Rqst_Log_Dte_TmeRedef1;
    private DbsField icw_Master_Index_View_Rqst_Log_Index_Dte;
    private DbsField icw_Master_Index_View_Rqst_Log_Index_Tme;
    private DbsField icw_Master_Index_View_Rqst_Log_Unit_Cde;
    private DbsField icw_Master_Index_View_Rqst_Log_Oprtr_Cde;
    private DbsField icw_Master_Index_View_Rqst_Orgn_Cde;
    private DbsField icw_Master_Index_View_Rqst_Log_Invrt_Dte_Tme;
    private DbsField icw_Master_Index_View_Modify_Unit_Cde;
    private DbsField icw_Master_Index_View_Modify_Oprtr_Cde;
    private DbsField icw_Master_Index_View_Modify_Applctn_Cde;
    private DbsField icw_Master_Index_View_System_Updte_Dte_Tme;
    private DbsField icw_Master_Index_View_Ppg_Cde;
    private DbsField icw_Master_Index_View_Iis_Hrchy_Level_Cde;
    private DbsField icw_Master_Index_View_Iis_Hrchy_Key_Cde;
    private DbsField icw_Master_Index_View_Iis_Instn_Link_Cde;
    private DbsField icw_Master_Index_View_Iis_Rgn_Cde;
    private DbsField icw_Master_Index_View_Iis_Spcl_Dsgntn_Ind;
    private DbsField icw_Master_Index_View_Iis_Tiaa_Brnch_Cde;
    private DbsField icw_Master_Index_View_Ny_Denver_Ind;
    private DbsField icw_Master_Index_View_Pbs_Apps_Ind;
    private DbsField icw_Master_Index_View_Default_Ppg_Cde_Ind;
    private DbsField icw_Master_Index_View_Work_Prcss_Id;
    private DbsGroup icw_Master_Index_View_Work_Prcss_IdRedef2;
    private DbsField icw_Master_Index_View_Work_Actn_Rqstd_Cde;
    private DbsField icw_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField icw_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField icw_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField icw_Master_Index_View_Work_Rqst_Prty_Cde;
    private DbsField icw_Master_Index_View_Rt_Sqnce_Nbr;
    private DbsField icw_Master_Index_View_Wpid_Vldte_Ind;
    private DbsField icw_Master_Index_View_Work_List_Sqnce;
    private DbsGroup icw_Master_Index_View_Work_List_SqnceRedef3;
    private DbsField icw_Master_Index_View_Work_List_Sqnce_Dte;
    private DbsField icw_Master_Index_View_Work_List_Prty_Cde;
    private DbsField icw_Master_Index_View_Work_List_Schdle_Pay_Dte;
    private DbsField icw_Master_Index_View_Work_List_Filler;
    private DbsField icw_Master_Index_View_Unit_Cde;
    private DbsGroup icw_Master_Index_View_Unit_CdeRedef4;
    private DbsField icw_Master_Index_View_Unit_Id_Cde;
    private DbsField icw_Master_Index_View_Unit_Rgn_Cde;
    private DbsField icw_Master_Index_View_Unit_Spcl_Dsgntn_Cde;
    private DbsField icw_Master_Index_View_Unit_Brnch_Group_Cde;
    private DbsField icw_Master_Index_View_Unit_Updte_Dte_Tme;
    private DbsField icw_Master_Index_View_Owner_Unit;
    private DbsGroup icw_Master_Index_View_Owner_UnitRedef5;
    private DbsField icw_Master_Index_View_Owner_Unit_Id_Cde;
    private DbsField icw_Master_Index_View_Owner_Unit_Rgn_Cde;
    private DbsField icw_Master_Index_View_Owner_Unit_Spcl_Dsgntn_Cde;
    private DbsField icw_Master_Index_View_Owner_Unit_Brnch_Group_Cde;
    private DbsField icw_Master_Index_View_Empl_Racf_Id;
    private DbsField icw_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde;
    private DbsField icw_Master_Index_View_Step_Id;
    private DbsField icw_Master_Index_View_Step_Sqnce_Nbr;
    private DbsField icw_Master_Index_View_Step_Updte_Dte_Tme;
    private DbsField icw_Master_Index_View_Status_Cde;
    private DbsField icw_Master_Index_View_Status_Updte_Dte_Tme;
    private DbsField icw_Master_Index_View_Status_Updte_Oprtr_Cde;
    private DbsField icw_Master_Index_View_Status_Freeze_Ind;
    private DbsField icw_Master_Index_View_Admin_Unit_Cde;
    private DbsField icw_Master_Index_View_Admin_Status_Cde;
    private DbsField icw_Master_Index_View_Admin_Status_Updte_Dte_Tme;
    private DbsField icw_Master_Index_View_Admin_Status_Updte_Oprtr_Cde;
    private DbsField icw_Master_Index_View_Applctns_Rcvd_Ind;
    private DbsField icw_Master_Index_View_Check_Ind;
    private DbsField icw_Master_Index_View_Cmplnt_Ind;
    private DbsField icw_Master_Index_View_Unit_On_Time_Ind;
    private DbsField icw_Master_Index_View_Crprte_On_Tme_Ind;
    private DbsField icw_Master_Index_View_Crprte_Status_Ind;
    private DbsField icw_Master_Index_View_Dcmnt_Anttn_Ind;
    private DbsField icw_Master_Index_View_Dup_Ind;
    private DbsField icw_Master_Index_View_Elctrn_Fld_Ind;
    private DbsField icw_Master_Index_View_Img_Paper_Prcssng_Ind;
    private DbsField icw_Master_Index_View_List_Page_Cnt;
    private DbsField icw_Master_Index_View_Paid_As_Billed_Ind;
    private DbsField icw_Master_Index_View_Physcl_Fldr_Id_Nbr;
    private DbsField icw_Master_Index_View_Prcssng_Type_Ind;
    private DbsField icw_Master_Index_View_Rescan_Ind;
    private DbsField icw_Master_Index_View_Sap_Ind;
    private DbsField icw_Master_Index_View_Step_Re_Do_Ind;
    private DbsField icw_Master_Index_View_Sub_Rqst_Ind;
    private DbsField icw_Master_Index_View_Work_List_Ind;
    private DbsField icw_Master_Index_View_Work_Type_Ind;
    private DbsField icw_Master_Index_View_Cash_Match_Ind;
    private DbsField icw_Master_Index_View_Iris_Ind;
    private DbsField icw_Master_Index_View_Attntn_Txt;
    private DbsField icw_Master_Index_View_Spcl_Hndlng_Txt;
    private DbsField icw_Master_Index_View_Print_Q_Ind;
    private DbsField icw_Master_Index_View_Print_Dte_Tme;
    private DbsField icw_Master_Index_View_Print_Batch_Id_Nbr;
    private DbsGroup icw_Master_Index_View_Print_Batch_Id_NbrRedef6;
    private DbsField icw_Master_Index_View_Print_Prefix_Ind;
    private DbsField icw_Master_Index_View_Print_Batch_Nbr;
    private DbsField icw_Master_Index_View_Print_Batch_Sqnce_Nbr;
    private DbsField icw_Master_Index_View_Print_Batch_Ind;
    private DbsField icw_Master_Index_View_Printer_Id_Cde;
    private DbsField icw_Master_Index_View_Mstr_Indx_Actn_Cde;
    private DbsField icw_Master_Index_View_Case_Id_Cde;
    private DbsGroup icw_Master_Index_View_Case_Id_CdeRedef7;
    private DbsField icw_Master_Index_View_Case_Ind;
    private DbsField icw_Master_Index_View_Sub_Rqst_Sqnce_Ind;
    private DbsField icw_Master_Index_View_Rqst_Id;
    private DbsGroup icw_Master_Index_View_Rqst_IdRedef8;
    private DbsField icw_Master_Index_View_Rqst_Work_Prcss_Id;
    private DbsField icw_Master_Index_View_Rqst_Tiaa_Rcvd_Dte;
    private DbsField icw_Master_Index_View_Rqst_Case_Id_Cde;
    private DbsField icw_Master_Index_View_Rqst_Ppg_Or_Hrchy_Key_Cde;
    private DbsField icw_Master_Index_View_Multi_Rqst_Ind;
    private DbsField icw_Master_Index_View_Cntct_Orgn_Type_Cde;
    private DbsField icw_Master_Index_View_Cntct_Dte_Tme;
    private DbsField icw_Master_Index_View_Cntct_Oprtr_Id;
    private DbsField icw_Master_Index_View_Tiaa_Rcvd_Dte_Tme;
    private DbsField icw_Master_Index_View_Invrt_Tiaa_Rcvd_Dte;
    private DbsField icw_Master_Index_View_List_Data_Rcvd_Dte;
    private DbsField icw_Master_Index_View_Prtcpnt_Dte;
    private DbsField icw_Master_Index_View_Schdle_Pay_Dte;
    private DbsField icw_Master_Index_View_Invrt_Schdle_Pay_Dte;
    private DbsField icw_Master_Index_View_Acct_Dte;
    private DbsField icw_Master_Index_View_Premium_Balance_Dte;
    private DbsField icw_Master_Index_View_Crrnt_Due_Dte_Tme;
    private DbsField icw_Master_Index_View_Crprte_Due_Dte_Tme;
    private DbsField icw_Master_Index_View_Instn_Close_Dte_Tme;
    private DbsField icw_Master_Index_View_Crprte_Close_Dte_Tme;
    private DbsField icw_Master_Index_View_Parent_Log_Dte_Tme;
    private DbsField icw_Master_Index_View_Strtng_Event_Dte_Tme;
    private DbsField icw_Master_Index_View_Invrt_Strtng_Event;
    private DbsField icw_Master_Index_View_Endng_Event_Dte_Tme;
    private DbsField icw_Master_Index_View_Actvty_Elpsd_Clndr_Hours;
    private DbsField icw_Master_Index_View_Actvty_Elpsd_Bsnss_Hours;
    private DbsField icw_Master_Index_View_Trade_Dte_Tme;
    private DbsField icw_Master_Index_View_Spcl_Prcss_Log_Dte_Tme;
    private DbsField icw_Master_Index_View_Spcl_Prcss_Ind;
    private DbsField icw_Master_Index_View_Accum_Corp_Wait_Hours_Overall;
    private DbsField icw_Master_Index_View_Accum_Corp_Prod_Hours_Overall;
    private DbsField icw_Master_Index_View_Accum_Corp_Wait_Hours;
    private DbsField icw_Master_Index_View_Accum_Corp_Prod_Hours;
    private DbsField icw_Master_Index_View_Accum_Unit_Wait_Hours;
    private DbsField icw_Master_Index_View_Accum_Unit_Prod_Hours;
    private DbsField icw_Master_Index_View_Accum_Empl_Wait_Hours;
    private DbsField icw_Master_Index_View_Accum_Empl_Prod_Hours;
    private DbsField icw_Master_Index_View_Accum_Step_Wait_Hours;
    private DbsField icw_Master_Index_View_Accum_Step_Prod_Hours;
    private DbsField icw_Master_Index_View_Accum_Status_Hours;
    private DbsField icw_Master_Index_View_Unit_Start_Dte_Tme;
    private DbsField icw_Master_Index_View_Step_Start_Dte_Tme;
    private DbsField icw_Master_Index_View_Empl_Start_Dte_Tme;
    private DbsField icw_Master_Index_View_Unit_Actvty_Start_Status;
    private DbsField icw_Master_Index_View_Step_Actvty_Start_Status;
    private DbsField icw_Master_Index_View_Empl_Actvty_Start_Status;
    private DbsField icw_Master_Index_View_Unit_Turnaround_Start_Dte_Tme;
    private DbsField icw_Master_Index_View_Corp_Turnaround;
    private DbsField icw_Master_Index_View_Actve_Ind;
    private DbsGroup icw_Master_Index_View_Rsn_CdeMuGroup;
    private DbsField icw_Master_Index_View_Rsn_Cde;
    private DbsField icw_Master_Index_View_Alwys_Vrfy;
    private DbsField icw_Master_Index_View_Crs_Memo;
    private DbsField icw_Master_Index_View_Pas_Ovrrde;
    private DbsField icw_Master_Index_View_Pas_Ind;
    private DbsField icw_Master_Index_View_Cnnctd_Rqst_Ind;
    private DbsField icw_Master_Index_View_Init_Undrlyng_Rqst_Cnt;
    private DbsField icw_Master_Index_View_Crrnt_Cmt_Ind;
    private DbsField icw_Master_Index_View_Log_Sqnce_Nbr;
    private DbsField icw_Master_Index_View_Response_Ind;

    public DataAccessProgramView getVw_icw_Master_Index_View() { return vw_icw_Master_Index_View; }

    public DbsField getIcw_Master_Index_View_Rqst_Log_Dte_Tme() { return icw_Master_Index_View_Rqst_Log_Dte_Tme; }

    public DbsGroup getIcw_Master_Index_View_Rqst_Log_Dte_TmeRedef1() { return icw_Master_Index_View_Rqst_Log_Dte_TmeRedef1; }

    public DbsField getIcw_Master_Index_View_Rqst_Log_Index_Dte() { return icw_Master_Index_View_Rqst_Log_Index_Dte; }

    public DbsField getIcw_Master_Index_View_Rqst_Log_Index_Tme() { return icw_Master_Index_View_Rqst_Log_Index_Tme; }

    public DbsField getIcw_Master_Index_View_Rqst_Log_Unit_Cde() { return icw_Master_Index_View_Rqst_Log_Unit_Cde; }

    public DbsField getIcw_Master_Index_View_Rqst_Log_Oprtr_Cde() { return icw_Master_Index_View_Rqst_Log_Oprtr_Cde; }

    public DbsField getIcw_Master_Index_View_Rqst_Orgn_Cde() { return icw_Master_Index_View_Rqst_Orgn_Cde; }

    public DbsField getIcw_Master_Index_View_Rqst_Log_Invrt_Dte_Tme() { return icw_Master_Index_View_Rqst_Log_Invrt_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Modify_Unit_Cde() { return icw_Master_Index_View_Modify_Unit_Cde; }

    public DbsField getIcw_Master_Index_View_Modify_Oprtr_Cde() { return icw_Master_Index_View_Modify_Oprtr_Cde; }

    public DbsField getIcw_Master_Index_View_Modify_Applctn_Cde() { return icw_Master_Index_View_Modify_Applctn_Cde; }

    public DbsField getIcw_Master_Index_View_System_Updte_Dte_Tme() { return icw_Master_Index_View_System_Updte_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Ppg_Cde() { return icw_Master_Index_View_Ppg_Cde; }

    public DbsField getIcw_Master_Index_View_Iis_Hrchy_Level_Cde() { return icw_Master_Index_View_Iis_Hrchy_Level_Cde; }

    public DbsField getIcw_Master_Index_View_Iis_Hrchy_Key_Cde() { return icw_Master_Index_View_Iis_Hrchy_Key_Cde; }

    public DbsField getIcw_Master_Index_View_Iis_Instn_Link_Cde() { return icw_Master_Index_View_Iis_Instn_Link_Cde; }

    public DbsField getIcw_Master_Index_View_Iis_Rgn_Cde() { return icw_Master_Index_View_Iis_Rgn_Cde; }

    public DbsField getIcw_Master_Index_View_Iis_Spcl_Dsgntn_Ind() { return icw_Master_Index_View_Iis_Spcl_Dsgntn_Ind; }

    public DbsField getIcw_Master_Index_View_Iis_Tiaa_Brnch_Cde() { return icw_Master_Index_View_Iis_Tiaa_Brnch_Cde; }

    public DbsField getIcw_Master_Index_View_Ny_Denver_Ind() { return icw_Master_Index_View_Ny_Denver_Ind; }

    public DbsField getIcw_Master_Index_View_Pbs_Apps_Ind() { return icw_Master_Index_View_Pbs_Apps_Ind; }

    public DbsField getIcw_Master_Index_View_Default_Ppg_Cde_Ind() { return icw_Master_Index_View_Default_Ppg_Cde_Ind; }

    public DbsField getIcw_Master_Index_View_Work_Prcss_Id() { return icw_Master_Index_View_Work_Prcss_Id; }

    public DbsGroup getIcw_Master_Index_View_Work_Prcss_IdRedef2() { return icw_Master_Index_View_Work_Prcss_IdRedef2; }

    public DbsField getIcw_Master_Index_View_Work_Actn_Rqstd_Cde() { return icw_Master_Index_View_Work_Actn_Rqstd_Cde; }

    public DbsField getIcw_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde() { return icw_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde; }

    public DbsField getIcw_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde() { return icw_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde; }

    public DbsField getIcw_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde() { return icw_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde; }

    public DbsField getIcw_Master_Index_View_Work_Rqst_Prty_Cde() { return icw_Master_Index_View_Work_Rqst_Prty_Cde; }

    public DbsField getIcw_Master_Index_View_Rt_Sqnce_Nbr() { return icw_Master_Index_View_Rt_Sqnce_Nbr; }

    public DbsField getIcw_Master_Index_View_Wpid_Vldte_Ind() { return icw_Master_Index_View_Wpid_Vldte_Ind; }

    public DbsField getIcw_Master_Index_View_Work_List_Sqnce() { return icw_Master_Index_View_Work_List_Sqnce; }

    public DbsGroup getIcw_Master_Index_View_Work_List_SqnceRedef3() { return icw_Master_Index_View_Work_List_SqnceRedef3; }

    public DbsField getIcw_Master_Index_View_Work_List_Sqnce_Dte() { return icw_Master_Index_View_Work_List_Sqnce_Dte; }

    public DbsField getIcw_Master_Index_View_Work_List_Prty_Cde() { return icw_Master_Index_View_Work_List_Prty_Cde; }

    public DbsField getIcw_Master_Index_View_Work_List_Schdle_Pay_Dte() { return icw_Master_Index_View_Work_List_Schdle_Pay_Dte; }

    public DbsField getIcw_Master_Index_View_Work_List_Filler() { return icw_Master_Index_View_Work_List_Filler; }

    public DbsField getIcw_Master_Index_View_Unit_Cde() { return icw_Master_Index_View_Unit_Cde; }

    public DbsGroup getIcw_Master_Index_View_Unit_CdeRedef4() { return icw_Master_Index_View_Unit_CdeRedef4; }

    public DbsField getIcw_Master_Index_View_Unit_Id_Cde() { return icw_Master_Index_View_Unit_Id_Cde; }

    public DbsField getIcw_Master_Index_View_Unit_Rgn_Cde() { return icw_Master_Index_View_Unit_Rgn_Cde; }

    public DbsField getIcw_Master_Index_View_Unit_Spcl_Dsgntn_Cde() { return icw_Master_Index_View_Unit_Spcl_Dsgntn_Cde; }

    public DbsField getIcw_Master_Index_View_Unit_Brnch_Group_Cde() { return icw_Master_Index_View_Unit_Brnch_Group_Cde; }

    public DbsField getIcw_Master_Index_View_Unit_Updte_Dte_Tme() { return icw_Master_Index_View_Unit_Updte_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Owner_Unit() { return icw_Master_Index_View_Owner_Unit; }

    public DbsGroup getIcw_Master_Index_View_Owner_UnitRedef5() { return icw_Master_Index_View_Owner_UnitRedef5; }

    public DbsField getIcw_Master_Index_View_Owner_Unit_Id_Cde() { return icw_Master_Index_View_Owner_Unit_Id_Cde; }

    public DbsField getIcw_Master_Index_View_Owner_Unit_Rgn_Cde() { return icw_Master_Index_View_Owner_Unit_Rgn_Cde; }

    public DbsField getIcw_Master_Index_View_Owner_Unit_Spcl_Dsgntn_Cde() { return icw_Master_Index_View_Owner_Unit_Spcl_Dsgntn_Cde; }

    public DbsField getIcw_Master_Index_View_Owner_Unit_Brnch_Group_Cde() { return icw_Master_Index_View_Owner_Unit_Brnch_Group_Cde; }

    public DbsField getIcw_Master_Index_View_Empl_Racf_Id() { return icw_Master_Index_View_Empl_Racf_Id; }

    public DbsField getIcw_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde() { return icw_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde; }

    public DbsField getIcw_Master_Index_View_Step_Id() { return icw_Master_Index_View_Step_Id; }

    public DbsField getIcw_Master_Index_View_Step_Sqnce_Nbr() { return icw_Master_Index_View_Step_Sqnce_Nbr; }

    public DbsField getIcw_Master_Index_View_Step_Updte_Dte_Tme() { return icw_Master_Index_View_Step_Updte_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Status_Cde() { return icw_Master_Index_View_Status_Cde; }

    public DbsField getIcw_Master_Index_View_Status_Updte_Dte_Tme() { return icw_Master_Index_View_Status_Updte_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Status_Updte_Oprtr_Cde() { return icw_Master_Index_View_Status_Updte_Oprtr_Cde; }

    public DbsField getIcw_Master_Index_View_Status_Freeze_Ind() { return icw_Master_Index_View_Status_Freeze_Ind; }

    public DbsField getIcw_Master_Index_View_Admin_Unit_Cde() { return icw_Master_Index_View_Admin_Unit_Cde; }

    public DbsField getIcw_Master_Index_View_Admin_Status_Cde() { return icw_Master_Index_View_Admin_Status_Cde; }

    public DbsField getIcw_Master_Index_View_Admin_Status_Updte_Dte_Tme() { return icw_Master_Index_View_Admin_Status_Updte_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Admin_Status_Updte_Oprtr_Cde() { return icw_Master_Index_View_Admin_Status_Updte_Oprtr_Cde; }

    public DbsField getIcw_Master_Index_View_Applctns_Rcvd_Ind() { return icw_Master_Index_View_Applctns_Rcvd_Ind; }

    public DbsField getIcw_Master_Index_View_Check_Ind() { return icw_Master_Index_View_Check_Ind; }

    public DbsField getIcw_Master_Index_View_Cmplnt_Ind() { return icw_Master_Index_View_Cmplnt_Ind; }

    public DbsField getIcw_Master_Index_View_Unit_On_Time_Ind() { return icw_Master_Index_View_Unit_On_Time_Ind; }

    public DbsField getIcw_Master_Index_View_Crprte_On_Tme_Ind() { return icw_Master_Index_View_Crprte_On_Tme_Ind; }

    public DbsField getIcw_Master_Index_View_Crprte_Status_Ind() { return icw_Master_Index_View_Crprte_Status_Ind; }

    public DbsField getIcw_Master_Index_View_Dcmnt_Anttn_Ind() { return icw_Master_Index_View_Dcmnt_Anttn_Ind; }

    public DbsField getIcw_Master_Index_View_Dup_Ind() { return icw_Master_Index_View_Dup_Ind; }

    public DbsField getIcw_Master_Index_View_Elctrn_Fld_Ind() { return icw_Master_Index_View_Elctrn_Fld_Ind; }

    public DbsField getIcw_Master_Index_View_Img_Paper_Prcssng_Ind() { return icw_Master_Index_View_Img_Paper_Prcssng_Ind; }

    public DbsField getIcw_Master_Index_View_List_Page_Cnt() { return icw_Master_Index_View_List_Page_Cnt; }

    public DbsField getIcw_Master_Index_View_Paid_As_Billed_Ind() { return icw_Master_Index_View_Paid_As_Billed_Ind; }

    public DbsField getIcw_Master_Index_View_Physcl_Fldr_Id_Nbr() { return icw_Master_Index_View_Physcl_Fldr_Id_Nbr; }

    public DbsField getIcw_Master_Index_View_Prcssng_Type_Ind() { return icw_Master_Index_View_Prcssng_Type_Ind; }

    public DbsField getIcw_Master_Index_View_Rescan_Ind() { return icw_Master_Index_View_Rescan_Ind; }

    public DbsField getIcw_Master_Index_View_Sap_Ind() { return icw_Master_Index_View_Sap_Ind; }

    public DbsField getIcw_Master_Index_View_Step_Re_Do_Ind() { return icw_Master_Index_View_Step_Re_Do_Ind; }

    public DbsField getIcw_Master_Index_View_Sub_Rqst_Ind() { return icw_Master_Index_View_Sub_Rqst_Ind; }

    public DbsField getIcw_Master_Index_View_Work_List_Ind() { return icw_Master_Index_View_Work_List_Ind; }

    public DbsField getIcw_Master_Index_View_Work_Type_Ind() { return icw_Master_Index_View_Work_Type_Ind; }

    public DbsField getIcw_Master_Index_View_Cash_Match_Ind() { return icw_Master_Index_View_Cash_Match_Ind; }

    public DbsField getIcw_Master_Index_View_Iris_Ind() { return icw_Master_Index_View_Iris_Ind; }

    public DbsField getIcw_Master_Index_View_Attntn_Txt() { return icw_Master_Index_View_Attntn_Txt; }

    public DbsField getIcw_Master_Index_View_Spcl_Hndlng_Txt() { return icw_Master_Index_View_Spcl_Hndlng_Txt; }

    public DbsField getIcw_Master_Index_View_Print_Q_Ind() { return icw_Master_Index_View_Print_Q_Ind; }

    public DbsField getIcw_Master_Index_View_Print_Dte_Tme() { return icw_Master_Index_View_Print_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Print_Batch_Id_Nbr() { return icw_Master_Index_View_Print_Batch_Id_Nbr; }

    public DbsGroup getIcw_Master_Index_View_Print_Batch_Id_NbrRedef6() { return icw_Master_Index_View_Print_Batch_Id_NbrRedef6; }

    public DbsField getIcw_Master_Index_View_Print_Prefix_Ind() { return icw_Master_Index_View_Print_Prefix_Ind; }

    public DbsField getIcw_Master_Index_View_Print_Batch_Nbr() { return icw_Master_Index_View_Print_Batch_Nbr; }

    public DbsField getIcw_Master_Index_View_Print_Batch_Sqnce_Nbr() { return icw_Master_Index_View_Print_Batch_Sqnce_Nbr; }

    public DbsField getIcw_Master_Index_View_Print_Batch_Ind() { return icw_Master_Index_View_Print_Batch_Ind; }

    public DbsField getIcw_Master_Index_View_Printer_Id_Cde() { return icw_Master_Index_View_Printer_Id_Cde; }

    public DbsField getIcw_Master_Index_View_Mstr_Indx_Actn_Cde() { return icw_Master_Index_View_Mstr_Indx_Actn_Cde; }

    public DbsField getIcw_Master_Index_View_Case_Id_Cde() { return icw_Master_Index_View_Case_Id_Cde; }

    public DbsGroup getIcw_Master_Index_View_Case_Id_CdeRedef7() { return icw_Master_Index_View_Case_Id_CdeRedef7; }

    public DbsField getIcw_Master_Index_View_Case_Ind() { return icw_Master_Index_View_Case_Ind; }

    public DbsField getIcw_Master_Index_View_Sub_Rqst_Sqnce_Ind() { return icw_Master_Index_View_Sub_Rqst_Sqnce_Ind; }

    public DbsField getIcw_Master_Index_View_Rqst_Id() { return icw_Master_Index_View_Rqst_Id; }

    public DbsGroup getIcw_Master_Index_View_Rqst_IdRedef8() { return icw_Master_Index_View_Rqst_IdRedef8; }

    public DbsField getIcw_Master_Index_View_Rqst_Work_Prcss_Id() { return icw_Master_Index_View_Rqst_Work_Prcss_Id; }

    public DbsField getIcw_Master_Index_View_Rqst_Tiaa_Rcvd_Dte() { return icw_Master_Index_View_Rqst_Tiaa_Rcvd_Dte; }

    public DbsField getIcw_Master_Index_View_Rqst_Case_Id_Cde() { return icw_Master_Index_View_Rqst_Case_Id_Cde; }

    public DbsField getIcw_Master_Index_View_Rqst_Ppg_Or_Hrchy_Key_Cde() { return icw_Master_Index_View_Rqst_Ppg_Or_Hrchy_Key_Cde; }

    public DbsField getIcw_Master_Index_View_Multi_Rqst_Ind() { return icw_Master_Index_View_Multi_Rqst_Ind; }

    public DbsField getIcw_Master_Index_View_Cntct_Orgn_Type_Cde() { return icw_Master_Index_View_Cntct_Orgn_Type_Cde; }

    public DbsField getIcw_Master_Index_View_Cntct_Dte_Tme() { return icw_Master_Index_View_Cntct_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Cntct_Oprtr_Id() { return icw_Master_Index_View_Cntct_Oprtr_Id; }

    public DbsField getIcw_Master_Index_View_Tiaa_Rcvd_Dte_Tme() { return icw_Master_Index_View_Tiaa_Rcvd_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Invrt_Tiaa_Rcvd_Dte() { return icw_Master_Index_View_Invrt_Tiaa_Rcvd_Dte; }

    public DbsField getIcw_Master_Index_View_List_Data_Rcvd_Dte() { return icw_Master_Index_View_List_Data_Rcvd_Dte; }

    public DbsField getIcw_Master_Index_View_Prtcpnt_Dte() { return icw_Master_Index_View_Prtcpnt_Dte; }

    public DbsField getIcw_Master_Index_View_Schdle_Pay_Dte() { return icw_Master_Index_View_Schdle_Pay_Dte; }

    public DbsField getIcw_Master_Index_View_Invrt_Schdle_Pay_Dte() { return icw_Master_Index_View_Invrt_Schdle_Pay_Dte; }

    public DbsField getIcw_Master_Index_View_Acct_Dte() { return icw_Master_Index_View_Acct_Dte; }

    public DbsField getIcw_Master_Index_View_Premium_Balance_Dte() { return icw_Master_Index_View_Premium_Balance_Dte; }

    public DbsField getIcw_Master_Index_View_Crrnt_Due_Dte_Tme() { return icw_Master_Index_View_Crrnt_Due_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Crprte_Due_Dte_Tme() { return icw_Master_Index_View_Crprte_Due_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Instn_Close_Dte_Tme() { return icw_Master_Index_View_Instn_Close_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Crprte_Close_Dte_Tme() { return icw_Master_Index_View_Crprte_Close_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Parent_Log_Dte_Tme() { return icw_Master_Index_View_Parent_Log_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Strtng_Event_Dte_Tme() { return icw_Master_Index_View_Strtng_Event_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Invrt_Strtng_Event() { return icw_Master_Index_View_Invrt_Strtng_Event; }

    public DbsField getIcw_Master_Index_View_Endng_Event_Dte_Tme() { return icw_Master_Index_View_Endng_Event_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Actvty_Elpsd_Clndr_Hours() { return icw_Master_Index_View_Actvty_Elpsd_Clndr_Hours; }

    public DbsField getIcw_Master_Index_View_Actvty_Elpsd_Bsnss_Hours() { return icw_Master_Index_View_Actvty_Elpsd_Bsnss_Hours; }

    public DbsField getIcw_Master_Index_View_Trade_Dte_Tme() { return icw_Master_Index_View_Trade_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Spcl_Prcss_Log_Dte_Tme() { return icw_Master_Index_View_Spcl_Prcss_Log_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Spcl_Prcss_Ind() { return icw_Master_Index_View_Spcl_Prcss_Ind; }

    public DbsField getIcw_Master_Index_View_Accum_Corp_Wait_Hours_Overall() { return icw_Master_Index_View_Accum_Corp_Wait_Hours_Overall; }

    public DbsField getIcw_Master_Index_View_Accum_Corp_Prod_Hours_Overall() { return icw_Master_Index_View_Accum_Corp_Prod_Hours_Overall; }

    public DbsField getIcw_Master_Index_View_Accum_Corp_Wait_Hours() { return icw_Master_Index_View_Accum_Corp_Wait_Hours; }

    public DbsField getIcw_Master_Index_View_Accum_Corp_Prod_Hours() { return icw_Master_Index_View_Accum_Corp_Prod_Hours; }

    public DbsField getIcw_Master_Index_View_Accum_Unit_Wait_Hours() { return icw_Master_Index_View_Accum_Unit_Wait_Hours; }

    public DbsField getIcw_Master_Index_View_Accum_Unit_Prod_Hours() { return icw_Master_Index_View_Accum_Unit_Prod_Hours; }

    public DbsField getIcw_Master_Index_View_Accum_Empl_Wait_Hours() { return icw_Master_Index_View_Accum_Empl_Wait_Hours; }

    public DbsField getIcw_Master_Index_View_Accum_Empl_Prod_Hours() { return icw_Master_Index_View_Accum_Empl_Prod_Hours; }

    public DbsField getIcw_Master_Index_View_Accum_Step_Wait_Hours() { return icw_Master_Index_View_Accum_Step_Wait_Hours; }

    public DbsField getIcw_Master_Index_View_Accum_Step_Prod_Hours() { return icw_Master_Index_View_Accum_Step_Prod_Hours; }

    public DbsField getIcw_Master_Index_View_Accum_Status_Hours() { return icw_Master_Index_View_Accum_Status_Hours; }

    public DbsField getIcw_Master_Index_View_Unit_Start_Dte_Tme() { return icw_Master_Index_View_Unit_Start_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Step_Start_Dte_Tme() { return icw_Master_Index_View_Step_Start_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Empl_Start_Dte_Tme() { return icw_Master_Index_View_Empl_Start_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Unit_Actvty_Start_Status() { return icw_Master_Index_View_Unit_Actvty_Start_Status; }

    public DbsField getIcw_Master_Index_View_Step_Actvty_Start_Status() { return icw_Master_Index_View_Step_Actvty_Start_Status; }

    public DbsField getIcw_Master_Index_View_Empl_Actvty_Start_Status() { return icw_Master_Index_View_Empl_Actvty_Start_Status; }

    public DbsField getIcw_Master_Index_View_Unit_Turnaround_Start_Dte_Tme() { return icw_Master_Index_View_Unit_Turnaround_Start_Dte_Tme; }

    public DbsField getIcw_Master_Index_View_Corp_Turnaround() { return icw_Master_Index_View_Corp_Turnaround; }

    public DbsField getIcw_Master_Index_View_Actve_Ind() { return icw_Master_Index_View_Actve_Ind; }

    public DbsGroup getIcw_Master_Index_View_Rsn_CdeMuGroup() { return icw_Master_Index_View_Rsn_CdeMuGroup; }

    public DbsField getIcw_Master_Index_View_Rsn_Cde() { return icw_Master_Index_View_Rsn_Cde; }

    public DbsField getIcw_Master_Index_View_Alwys_Vrfy() { return icw_Master_Index_View_Alwys_Vrfy; }

    public DbsField getIcw_Master_Index_View_Crs_Memo() { return icw_Master_Index_View_Crs_Memo; }

    public DbsField getIcw_Master_Index_View_Pas_Ovrrde() { return icw_Master_Index_View_Pas_Ovrrde; }

    public DbsField getIcw_Master_Index_View_Pas_Ind() { return icw_Master_Index_View_Pas_Ind; }

    public DbsField getIcw_Master_Index_View_Cnnctd_Rqst_Ind() { return icw_Master_Index_View_Cnnctd_Rqst_Ind; }

    public DbsField getIcw_Master_Index_View_Init_Undrlyng_Rqst_Cnt() { return icw_Master_Index_View_Init_Undrlyng_Rqst_Cnt; }

    public DbsField getIcw_Master_Index_View_Crrnt_Cmt_Ind() { return icw_Master_Index_View_Crrnt_Cmt_Ind; }

    public DbsField getIcw_Master_Index_View_Log_Sqnce_Nbr() { return icw_Master_Index_View_Log_Sqnce_Nbr; }

    public DbsField getIcw_Master_Index_View_Response_Ind() { return icw_Master_Index_View_Response_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_icw_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_icw_Master_Index_View", "ICW-MASTER-INDEX-VIEW"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        icw_Master_Index_View_Rqst_Log_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        icw_Master_Index_View_Rqst_Log_Dte_TmeRedef1 = vw_icw_Master_Index_View.getRecord().newGroupInGroup("icw_Master_Index_View_Rqst_Log_Dte_TmeRedef1", 
            "Redefines", icw_Master_Index_View_Rqst_Log_Dte_Tme);
        icw_Master_Index_View_Rqst_Log_Index_Dte = icw_Master_Index_View_Rqst_Log_Dte_TmeRedef1.newFieldInGroup("icw_Master_Index_View_Rqst_Log_Index_Dte", 
            "RQST-LOG-INDEX-DTE", FieldType.STRING, 8);
        icw_Master_Index_View_Rqst_Log_Index_Tme = icw_Master_Index_View_Rqst_Log_Dte_TmeRedef1.newFieldInGroup("icw_Master_Index_View_Rqst_Log_Index_Tme", 
            "RQST-LOG-INDEX-TME", FieldType.STRING, 7);
        icw_Master_Index_View_Rqst_Log_Unit_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Rqst_Log_Unit_Cde", "RQST-LOG-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_UNIT_CDE");
        icw_Master_Index_View_Rqst_Log_Oprtr_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        icw_Master_Index_View_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/TIME");
        icw_Master_Index_View_Rqst_Orgn_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Rqst_Orgn_Cde", "RQST-ORGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        icw_Master_Index_View_Rqst_Orgn_Cde.setDdmHeader("ENTRY/OPERATOR");
        icw_Master_Index_View_Rqst_Log_Invrt_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Rqst_Log_Invrt_Dte_Tme", 
            "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        icw_Master_Index_View_Modify_Unit_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Modify_Unit_Cde", "MODIFY-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MODIFY_UNIT_CDE");
        icw_Master_Index_View_Modify_Oprtr_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Modify_Oprtr_Cde", "MODIFY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MODIFY_OPRTR_CDE");
        icw_Master_Index_View_Modify_Applctn_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Modify_Applctn_Cde", "MODIFY-APPLCTN-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MODIFY_APPLCTN_CDE");
        icw_Master_Index_View_System_Updte_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_System_Updte_Dte_Tme", 
            "SYSTEM-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "SYSTEM_UPDTE_DTE_TME");
        icw_Master_Index_View_Ppg_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Ppg_Cde", "PPG-CDE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "PPG_CDE");
        icw_Master_Index_View_Iis_Hrchy_Level_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Iis_Hrchy_Level_Cde", 
            "IIS-HRCHY-LEVEL-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IIS_HRCHY_LEVEL_CDE");
        icw_Master_Index_View_Iis_Hrchy_Key_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Iis_Hrchy_Key_Cde", "IIS-HRCHY-KEY-CDE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "IIS_HRCHY_KEY_CDE");
        icw_Master_Index_View_Iis_Instn_Link_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Iis_Instn_Link_Cde", "IIS-INSTN-LINK-CDE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "IIS_INSTN_LINK_CDE");
        icw_Master_Index_View_Iis_Rgn_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Iis_Rgn_Cde", "IIS-RGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "IIS_RGN_CDE");
        icw_Master_Index_View_Iis_Spcl_Dsgntn_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Iis_Spcl_Dsgntn_Ind", 
            "IIS-SPCL-DSGNTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IIS_SPCL_DSGNTN_IND");
        icw_Master_Index_View_Iis_Tiaa_Brnch_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Iis_Tiaa_Brnch_Cde", "IIS-TIAA-BRNCH-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IIS_TIAA_BRNCH_CDE");
        icw_Master_Index_View_Ny_Denver_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Ny_Denver_Ind", "NY-DENVER-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NY_DENVER_IND");
        icw_Master_Index_View_Pbs_Apps_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Pbs_Apps_Ind", "PBS-APPS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PBS_APPS_IND");
        icw_Master_Index_View_Default_Ppg_Cde_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Default_Ppg_Cde_Ind", 
            "DEFAULT-PPG-CDE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "DEFAULT_PPG_CDE_IND");
        icw_Master_Index_View_Work_Prcss_Id = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        icw_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");
        icw_Master_Index_View_Work_Prcss_IdRedef2 = vw_icw_Master_Index_View.getRecord().newGroupInGroup("icw_Master_Index_View_Work_Prcss_IdRedef2", 
            "Redefines", icw_Master_Index_View_Work_Prcss_Id);
        icw_Master_Index_View_Work_Actn_Rqstd_Cde = icw_Master_Index_View_Work_Prcss_IdRedef2.newFieldInGroup("icw_Master_Index_View_Work_Actn_Rqstd_Cde", 
            "WORK-ACTN-RQSTD-CDE", FieldType.STRING, 1);
        icw_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde = icw_Master_Index_View_Work_Prcss_IdRedef2.newFieldInGroup("icw_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde", 
            "WORK-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        icw_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde = icw_Master_Index_View_Work_Prcss_IdRedef2.newFieldInGroup("icw_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde", 
            "WORK-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        icw_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde = icw_Master_Index_View_Work_Prcss_IdRedef2.newFieldInGroup("icw_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        icw_Master_Index_View_Work_Rqst_Prty_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Work_Rqst_Prty_Cde", "WORK-RQST-PRTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WORK_RQST_PRTY_CDE");
        icw_Master_Index_View_Work_Rqst_Prty_Cde.setDdmHeader("PRIO");
        icw_Master_Index_View_Rt_Sqnce_Nbr = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        icw_Master_Index_View_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        icw_Master_Index_View_Wpid_Vldte_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Wpid_Vldte_Ind", "WPID-VLDTE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WPID_VLDTE_IND");
        icw_Master_Index_View_Work_List_Sqnce = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Work_List_Sqnce", "WORK-LIST-SQNCE", 
            FieldType.STRING, 24, RepeatingFieldStrategy.None, "WORK_LIST_SQNCE");
        icw_Master_Index_View_Work_List_SqnceRedef3 = vw_icw_Master_Index_View.getRecord().newGroupInGroup("icw_Master_Index_View_Work_List_SqnceRedef3", 
            "Redefines", icw_Master_Index_View_Work_List_Sqnce);
        icw_Master_Index_View_Work_List_Sqnce_Dte = icw_Master_Index_View_Work_List_SqnceRedef3.newFieldInGroup("icw_Master_Index_View_Work_List_Sqnce_Dte", 
            "WORK-LIST-SQNCE-DTE", FieldType.STRING, 8);
        icw_Master_Index_View_Work_List_Prty_Cde = icw_Master_Index_View_Work_List_SqnceRedef3.newFieldInGroup("icw_Master_Index_View_Work_List_Prty_Cde", 
            "WORK-LIST-PRTY-CDE", FieldType.STRING, 1);
        icw_Master_Index_View_Work_List_Schdle_Pay_Dte = icw_Master_Index_View_Work_List_SqnceRedef3.newFieldInGroup("icw_Master_Index_View_Work_List_Schdle_Pay_Dte", 
            "WORK-LIST-SCHDLE-PAY-DTE", FieldType.STRING, 8);
        icw_Master_Index_View_Work_List_Filler = icw_Master_Index_View_Work_List_SqnceRedef3.newFieldInGroup("icw_Master_Index_View_Work_List_Filler", 
            "WORK-LIST-FILLER", FieldType.STRING, 7);
        icw_Master_Index_View_Unit_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Unit_Cde", "UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_CDE");
        icw_Master_Index_View_Unit_Cde.setDdmHeader("UNIT/CODE");
        icw_Master_Index_View_Unit_CdeRedef4 = vw_icw_Master_Index_View.getRecord().newGroupInGroup("icw_Master_Index_View_Unit_CdeRedef4", "Redefines", 
            icw_Master_Index_View_Unit_Cde);
        icw_Master_Index_View_Unit_Id_Cde = icw_Master_Index_View_Unit_CdeRedef4.newFieldInGroup("icw_Master_Index_View_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 
            5);
        icw_Master_Index_View_Unit_Rgn_Cde = icw_Master_Index_View_Unit_CdeRedef4.newFieldInGroup("icw_Master_Index_View_Unit_Rgn_Cde", "UNIT-RGN-CDE", 
            FieldType.STRING, 1);
        icw_Master_Index_View_Unit_Spcl_Dsgntn_Cde = icw_Master_Index_View_Unit_CdeRedef4.newFieldInGroup("icw_Master_Index_View_Unit_Spcl_Dsgntn_Cde", 
            "UNIT-SPCL-DSGNTN-CDE", FieldType.STRING, 1);
        icw_Master_Index_View_Unit_Brnch_Group_Cde = icw_Master_Index_View_Unit_CdeRedef4.newFieldInGroup("icw_Master_Index_View_Unit_Brnch_Group_Cde", 
            "UNIT-BRNCH-GROUP-CDE", FieldType.STRING, 1);
        icw_Master_Index_View_Unit_Updte_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        icw_Master_Index_View_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        icw_Master_Index_View_Owner_Unit = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Owner_Unit", "OWNER-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "OWNER_UNIT");
        icw_Master_Index_View_Owner_Unit.setDdmHeader("OWNER/UNIT");
        icw_Master_Index_View_Owner_UnitRedef5 = vw_icw_Master_Index_View.getRecord().newGroupInGroup("icw_Master_Index_View_Owner_UnitRedef5", "Redefines", 
            icw_Master_Index_View_Owner_Unit);
        icw_Master_Index_View_Owner_Unit_Id_Cde = icw_Master_Index_View_Owner_UnitRedef5.newFieldInGroup("icw_Master_Index_View_Owner_Unit_Id_Cde", "OWNER-UNIT-ID-CDE", 
            FieldType.STRING, 5);
        icw_Master_Index_View_Owner_Unit_Rgn_Cde = icw_Master_Index_View_Owner_UnitRedef5.newFieldInGroup("icw_Master_Index_View_Owner_Unit_Rgn_Cde", 
            "OWNER-UNIT-RGN-CDE", FieldType.STRING, 1);
        icw_Master_Index_View_Owner_Unit_Spcl_Dsgntn_Cde = icw_Master_Index_View_Owner_UnitRedef5.newFieldInGroup("icw_Master_Index_View_Owner_Unit_Spcl_Dsgntn_Cde", 
            "OWNER-UNIT-SPCL-DSGNTN-CDE", FieldType.STRING, 1);
        icw_Master_Index_View_Owner_Unit_Brnch_Group_Cde = icw_Master_Index_View_Owner_UnitRedef5.newFieldInGroup("icw_Master_Index_View_Owner_Unit_Brnch_Group_Cde", 
            "OWNER-UNIT-BRNCH-GROUP-CDE", FieldType.STRING, 1);
        icw_Master_Index_View_Empl_Racf_Id = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Empl_Racf_Id", "EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        icw_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde", 
            "ASSGN-SPRVSR-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ASSGN_SPRVSR_OPRTR_CDE");
        icw_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde.setDdmHeader("SUPER/VISOR");
        icw_Master_Index_View_Step_Id = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Step_Id", "STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "STEP_ID");
        icw_Master_Index_View_Step_Id.setDdmHeader("STEP/ID");
        icw_Master_Index_View_Step_Sqnce_Nbr = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Step_Sqnce_Nbr", "STEP-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "STEP_SQNCE_NBR");
        icw_Master_Index_View_Step_Updte_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Step_Updte_Dte_Tme", "STEP-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STEP_UPDTE_DTE_TME");
        icw_Master_Index_View_Step_Updte_Dte_Tme.setDdmHeader("STEP UPDATE/DATE-TIME");
        icw_Master_Index_View_Status_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        icw_Master_Index_View_Status_Updte_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Status_Updte_Dte_Tme", 
            "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        icw_Master_Index_View_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        icw_Master_Index_View_Status_Updte_Oprtr_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Status_Updte_Oprtr_Cde", 
            "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        icw_Master_Index_View_Status_Freeze_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        icw_Master_Index_View_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        icw_Master_Index_View_Admin_Unit_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        icw_Master_Index_View_Admin_Status_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        icw_Master_Index_View_Admin_Status_Updte_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Admin_Status_Updte_Dte_Tme", 
            "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        icw_Master_Index_View_Admin_Status_Updte_Oprtr_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        icw_Master_Index_View_Applctns_Rcvd_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Applctns_Rcvd_Ind", "APPLCTNS-RCVD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "APPLCTNS_RCVD_IND");
        icw_Master_Index_View_Check_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Check_Ind", "CHECK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CHECK_IND");
        icw_Master_Index_View_Check_Ind.setDdmHeader("CHK");
        icw_Master_Index_View_Cmplnt_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CMPLNT_IND");
        icw_Master_Index_View_Unit_On_Time_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Unit_On_Time_Ind", "UNIT-ON-TIME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "UNIT_ON_TIME_IND");
        icw_Master_Index_View_Crprte_On_Tme_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_ON_TME_IND");
        icw_Master_Index_View_Crprte_On_Tme_Ind.setDdmHeader("CRPRTE/ON TIME");
        icw_Master_Index_View_Crprte_Status_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        icw_Master_Index_View_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        icw_Master_Index_View_Dcmnt_Anttn_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Dcmnt_Anttn_Ind", "DCMNT-ANTTN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DCMNT_ANTTN_IND");
        icw_Master_Index_View_Dcmnt_Anttn_Ind.setDdmHeader("ANOTATION/IND");
        icw_Master_Index_View_Dup_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Dup_Ind", "DUP-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DUP_IND");
        icw_Master_Index_View_Dup_Ind.setDdmHeader("DUPL/IND");
        icw_Master_Index_View_Elctrn_Fld_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Elctrn_Fld_Ind", "ELCTRN-FLD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ELCTRN_FLD_IND");
        icw_Master_Index_View_Elctrn_Fld_Ind.setDdmHeader("ELECTRNC/FLDR IND");
        icw_Master_Index_View_Img_Paper_Prcssng_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Img_Paper_Prcssng_Ind", 
            "IMG-PAPER-PRCSSNG-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IMG_PAPER_PRCSSNG_IND");
        icw_Master_Index_View_List_Page_Cnt = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_List_Page_Cnt", "LIST-PAGE-CNT", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "LIST_PAGE_CNT");
        icw_Master_Index_View_List_Page_Cnt.setDdmHeader("LIST/PAGE/COUNT");
        icw_Master_Index_View_Paid_As_Billed_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Paid_As_Billed_Ind", "PAID-AS-BILLED-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PAID_AS_BILLED_IND");
        icw_Master_Index_View_Physcl_Fldr_Id_Nbr = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        icw_Master_Index_View_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL/FLDR ID");
        icw_Master_Index_View_Prcssng_Type_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Prcssng_Type_Ind", "PRCSSNG-TYPE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRCSSNG_TYPE_IND");
        icw_Master_Index_View_Prcssng_Type_Ind.setDdmHeader("PROC/TYPE");
        icw_Master_Index_View_Rescan_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RESCAN_IND");
        icw_Master_Index_View_Rescan_Ind.setDdmHeader("RESCAN/IND");
        icw_Master_Index_View_Sap_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Sap_Ind", "SAP-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SAP_IND");
        icw_Master_Index_View_Step_Re_Do_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Step_Re_Do_Ind", "STEP-RE-DO-IND", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "STEP_RE_DO_IND");
        icw_Master_Index_View_Step_Re_Do_Ind.setDdmHeader("STEP/REDO");
        icw_Master_Index_View_Sub_Rqst_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Sub_Rqst_Ind", "SUB-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SUB_RQST_IND");
        icw_Master_Index_View_Sub_Rqst_Ind.setDdmHeader("SUB-REQUEST COUNT");
        icw_Master_Index_View_Work_List_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Work_List_Ind", "WORK-LIST-IND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "WORK_LIST_IND");
        icw_Master_Index_View_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        icw_Master_Index_View_Work_Type_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Work_Type_Ind", "WORK-TYPE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WORK_TYPE_IND");
        icw_Master_Index_View_Cash_Match_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Cash_Match_Ind", "CASH-MATCH-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CASH_MATCH_IND");
        icw_Master_Index_View_Cash_Match_Ind.setDdmHeader("CASH/MATCH");
        icw_Master_Index_View_Iris_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Iris_Ind", "IRIS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IRIS_IND");
        icw_Master_Index_View_Iris_Ind.setDdmHeader("IRIS/IND");
        icw_Master_Index_View_Attntn_Txt = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Attntn_Txt", "ATTNTN-TXT", FieldType.STRING, 
            25, RepeatingFieldStrategy.None, "ATTNTN_TXT");
        icw_Master_Index_View_Attntn_Txt.setDdmHeader("ATTENTION");
        icw_Master_Index_View_Spcl_Hndlng_Txt = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", 
            FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        icw_Master_Index_View_Spcl_Hndlng_Txt.setDdmHeader("MESSAGES");
        icw_Master_Index_View_Print_Q_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Print_Q_Ind", "PRINT-Q-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PRINT_Q_IND");
        icw_Master_Index_View_Print_Q_Ind.setDdmHeader("MJ PRINT/QUEUE");
        icw_Master_Index_View_Print_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Print_Dte_Tme", "PRINT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "PRINT_DTE_TME");
        icw_Master_Index_View_Print_Dte_Tme.setDdmHeader("MJ PRINT/DATE-TIME");
        icw_Master_Index_View_Print_Batch_Id_Nbr = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Print_Batch_Id_Nbr", "PRINT-BATCH-ID-NBR", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "PRINT_BATCH_ID_NBR");
        icw_Master_Index_View_Print_Batch_Id_NbrRedef6 = vw_icw_Master_Index_View.getRecord().newGroupInGroup("icw_Master_Index_View_Print_Batch_Id_NbrRedef6", 
            "Redefines", icw_Master_Index_View_Print_Batch_Id_Nbr);
        icw_Master_Index_View_Print_Prefix_Ind = icw_Master_Index_View_Print_Batch_Id_NbrRedef6.newFieldInGroup("icw_Master_Index_View_Print_Prefix_Ind", 
            "PRINT-PREFIX-IND", FieldType.STRING, 1);
        icw_Master_Index_View_Print_Batch_Nbr = icw_Master_Index_View_Print_Batch_Id_NbrRedef6.newFieldInGroup("icw_Master_Index_View_Print_Batch_Nbr", 
            "PRINT-BATCH-NBR", FieldType.NUMERIC, 8);
        icw_Master_Index_View_Print_Batch_Sqnce_Nbr = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Print_Batch_Sqnce_Nbr", 
            "PRINT-BATCH-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRINT_BATCH_SQNCE_NBR");
        icw_Master_Index_View_Print_Batch_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Print_Batch_Ind", "PRINT-BATCH-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRINT_BATCH_IND");
        icw_Master_Index_View_Printer_Id_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Printer_Id_Cde", "PRINTER-ID-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "PRINTER_ID_CDE");
        icw_Master_Index_View_Mstr_Indx_Actn_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Mstr_Indx_Actn_Cde", "MSTR-INDX-ACTN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "MSTR_INDX_ACTN_CDE");
        icw_Master_Index_View_Mstr_Indx_Actn_Cde.setDdmHeader("BATCH PROCES/ACTION CODE");
        icw_Master_Index_View_Case_Id_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CASE_ID_CDE");
        icw_Master_Index_View_Case_Id_Cde.setDdmHeader("CASE/ID");
        icw_Master_Index_View_Case_Id_CdeRedef7 = vw_icw_Master_Index_View.getRecord().newGroupInGroup("icw_Master_Index_View_Case_Id_CdeRedef7", "Redefines", 
            icw_Master_Index_View_Case_Id_Cde);
        icw_Master_Index_View_Case_Ind = icw_Master_Index_View_Case_Id_CdeRedef7.newFieldInGroup("icw_Master_Index_View_Case_Ind", "CASE-IND", FieldType.STRING, 
            1);
        icw_Master_Index_View_Sub_Rqst_Sqnce_Ind = icw_Master_Index_View_Case_Id_CdeRedef7.newFieldInGroup("icw_Master_Index_View_Sub_Rqst_Sqnce_Ind", 
            "SUB-RQST-SQNCE-IND", FieldType.STRING, 1);
        icw_Master_Index_View_Rqst_Id = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Rqst_Id", "RQST-ID", FieldType.STRING, 
            22, RepeatingFieldStrategy.None, "RQST_ID");
        icw_Master_Index_View_Rqst_Id.setDdmHeader("REQUEST ID");
        icw_Master_Index_View_Rqst_IdRedef8 = vw_icw_Master_Index_View.getRecord().newGroupInGroup("icw_Master_Index_View_Rqst_IdRedef8", "Redefines", 
            icw_Master_Index_View_Rqst_Id);
        icw_Master_Index_View_Rqst_Work_Prcss_Id = icw_Master_Index_View_Rqst_IdRedef8.newFieldInGroup("icw_Master_Index_View_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        icw_Master_Index_View_Rqst_Tiaa_Rcvd_Dte = icw_Master_Index_View_Rqst_IdRedef8.newFieldInGroup("icw_Master_Index_View_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        icw_Master_Index_View_Rqst_Case_Id_Cde = icw_Master_Index_View_Rqst_IdRedef8.newFieldInGroup("icw_Master_Index_View_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", 
            FieldType.STRING, 2);
        icw_Master_Index_View_Rqst_Ppg_Or_Hrchy_Key_Cde = icw_Master_Index_View_Rqst_IdRedef8.newFieldInGroup("icw_Master_Index_View_Rqst_Ppg_Or_Hrchy_Key_Cde", 
            "RQST-PPG-OR-HRCHY-KEY-CDE", FieldType.STRING, 6);
        icw_Master_Index_View_Multi_Rqst_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Multi_Rqst_Ind", "MULTI-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        icw_Master_Index_View_Cntct_Orgn_Type_Cde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Cntct_Orgn_Type_Cde", 
            "CNTCT-ORGN-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTCT_ORGN_TYPE_CDE");
        icw_Master_Index_View_Cntct_Orgn_Type_Cde.setDdmHeader("CONTACT/MOC");
        icw_Master_Index_View_Cntct_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Cntct_Dte_Tme", "CNTCT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "CNTCT_DTE_TME");
        icw_Master_Index_View_Cntct_Dte_Tme.setDdmHeader("CONTACT/DATE/TIME");
        icw_Master_Index_View_Cntct_Oprtr_Id = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Cntct_Oprtr_Id", "CNTCT-OPRTR-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTCT_OPRTR_ID");
        icw_Master_Index_View_Cntct_Oprtr_Id.setDdmHeader("CONTACT/OPRTR ID");
        icw_Master_Index_View_Tiaa_Rcvd_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        icw_Master_Index_View_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        icw_Master_Index_View_Invrt_Tiaa_Rcvd_Dte = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Invrt_Tiaa_Rcvd_Dte", 
            "INVRT-TIAA-RCVD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "INVRT_TIAA_RCVD_DTE");
        icw_Master_Index_View_Invrt_Tiaa_Rcvd_Dte.setDdmHeader("INVERT TIAA/RECVD DATE");
        icw_Master_Index_View_List_Data_Rcvd_Dte = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_List_Data_Rcvd_Dte", "LIST-DATA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "LIST_DATA_RCVD_DTE");
        icw_Master_Index_View_Prtcpnt_Dte = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Prtcpnt_Dte", "PRTCPNT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PRTCPNT_DTE");
        icw_Master_Index_View_Schdle_Pay_Dte = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Schdle_Pay_Dte", "SCHDLE-PAY-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "SCHDLE_PAY_DTE");
        icw_Master_Index_View_Invrt_Schdle_Pay_Dte = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Invrt_Schdle_Pay_Dte", 
            "INVRT-SCHDLE-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "INVRT_SCHDLE_PAY_DTE");
        icw_Master_Index_View_Acct_Dte = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Acct_Dte", "ACCT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ACCT_DTE");
        icw_Master_Index_View_Premium_Balance_Dte = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Premium_Balance_Dte", 
            "PREMIUM-BALANCE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "PREMIUM_BALANCE_DTE");
        icw_Master_Index_View_Crrnt_Due_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Crrnt_Due_Dte_Tme", "CRRNT-DUE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CRRNT_DUE_DTE_TME");
        icw_Master_Index_View_Crprte_Due_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CRPRTE_DUE_DTE_TME");
        icw_Master_Index_View_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        icw_Master_Index_View_Instn_Close_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Instn_Close_Dte_Tme", 
            "INSTN-CLOSE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "INSTN_CLOSE_DTE_TME");
        icw_Master_Index_View_Crprte_Close_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Crprte_Close_Dte_Tme", 
            "CRPRTE-CLOSE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "CRPRTE_CLOSE_DTE_TME");
        icw_Master_Index_View_Parent_Log_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Parent_Log_Dte_Tme", "PARENT-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "PARENT_LOG_DTE_TME");
        icw_Master_Index_View_Parent_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        icw_Master_Index_View_Strtng_Event_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Strtng_Event_Dte_Tme", 
            "STRTNG-EVENT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "STRTNG_EVENT_DTE_TME");
        icw_Master_Index_View_Invrt_Strtng_Event = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Invrt_Strtng_Event", "INVRT-STRTNG-EVENT", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "INVRT_STRTNG_EVENT");
        icw_Master_Index_View_Endng_Event_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Endng_Event_Dte_Tme", 
            "ENDNG-EVENT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ENDNG_EVENT_DTE_TME");
        icw_Master_Index_View_Actvty_Elpsd_Clndr_Hours = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Actvty_Elpsd_Clndr_Hours", 
            "ACTVTY-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 6, 2, RepeatingFieldStrategy.None, "ACTVTY_ELPSD_CLNDR_HOURS");
        icw_Master_Index_View_Actvty_Elpsd_Bsnss_Hours = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Actvty_Elpsd_Bsnss_Hours", 
            "ACTVTY-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 6, 2, RepeatingFieldStrategy.None, "ACTVTY_ELPSD_BSNSS_HOURS");
        icw_Master_Index_View_Trade_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Trade_Dte_Tme", "TRADE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TRADE_DTE_TME");
        icw_Master_Index_View_Spcl_Prcss_Log_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Spcl_Prcss_Log_Dte_Tme", 
            "SPCL-PRCSS-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "SPCL_PRCSS_LOG_DTE_TME");
        icw_Master_Index_View_Spcl_Prcss_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Spcl_Prcss_Ind", "SPCL-PRCSS-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "SPCL_PRCSS_IND");
        icw_Master_Index_View_Accum_Corp_Wait_Hours_Overall = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Accum_Corp_Wait_Hours_Overall", 
            "ACCUM-CORP-WAIT-HOURS-OVERALL", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACCUM_CORP_WAIT_HOURS_OVERALL");
        icw_Master_Index_View_Accum_Corp_Prod_Hours_Overall = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Accum_Corp_Prod_Hours_Overall", 
            "ACCUM-CORP-PROD-HOURS-OVERALL", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACCUM_CORP_PROD_HOURS_OVERALL");
        icw_Master_Index_View_Accum_Corp_Wait_Hours = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Accum_Corp_Wait_Hours", 
            "ACCUM-CORP-WAIT-HOURS", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACCUM_CORP_WAIT_HOURS");
        icw_Master_Index_View_Accum_Corp_Prod_Hours = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Accum_Corp_Prod_Hours", 
            "ACCUM-CORP-PROD-HOURS", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACCUM_CORP_PROD_HOURS");
        icw_Master_Index_View_Accum_Unit_Wait_Hours = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Accum_Unit_Wait_Hours", 
            "ACCUM-UNIT-WAIT-HOURS", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACCUM_UNIT_WAIT_HOURS");
        icw_Master_Index_View_Accum_Unit_Prod_Hours = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Accum_Unit_Prod_Hours", 
            "ACCUM-UNIT-PROD-HOURS", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACCUM_UNIT_PROD_HOURS");
        icw_Master_Index_View_Accum_Empl_Wait_Hours = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Accum_Empl_Wait_Hours", 
            "ACCUM-EMPL-WAIT-HOURS", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACCUM_EMPL_WAIT_HOURS");
        icw_Master_Index_View_Accum_Empl_Prod_Hours = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Accum_Empl_Prod_Hours", 
            "ACCUM-EMPL-PROD-HOURS", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACCUM_EMPL_PROD_HOURS");
        icw_Master_Index_View_Accum_Step_Wait_Hours = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Accum_Step_Wait_Hours", 
            "ACCUM-STEP-WAIT-HOURS", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACCUM_STEP_WAIT_HOURS");
        icw_Master_Index_View_Accum_Step_Prod_Hours = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Accum_Step_Prod_Hours", 
            "ACCUM-STEP-PROD-HOURS", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACCUM_STEP_PROD_HOURS");
        icw_Master_Index_View_Accum_Status_Hours = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Accum_Status_Hours", "ACCUM-STATUS-HOURS", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACCUM_STATUS_HOURS");
        icw_Master_Index_View_Unit_Start_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Unit_Start_Dte_Tme", "UNIT-START-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_START_DTE_TME");
        icw_Master_Index_View_Step_Start_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Step_Start_Dte_Tme", "STEP-START-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STEP_START_DTE_TME");
        icw_Master_Index_View_Empl_Start_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Empl_Start_Dte_Tme", "EMPL-START-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "EMPL_START_DTE_TME");
        icw_Master_Index_View_Unit_Actvty_Start_Status = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Unit_Actvty_Start_Status", 
            "UNIT-ACTVTY-START-STATUS", FieldType.STRING, 4, RepeatingFieldStrategy.None, "UNIT_ACTVTY_START_STATUS");
        icw_Master_Index_View_Step_Actvty_Start_Status = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Step_Actvty_Start_Status", 
            "STEP-ACTVTY-START-STATUS", FieldType.STRING, 4, RepeatingFieldStrategy.None, "STEP_ACTVTY_START_STATUS");
        icw_Master_Index_View_Step_Actvty_Start_Status.setDdmHeader("STEP/START/STATUS");
        icw_Master_Index_View_Empl_Actvty_Start_Status = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Empl_Actvty_Start_Status", 
            "EMPL-ACTVTY-START-STATUS", FieldType.STRING, 4, RepeatingFieldStrategy.None, "EMPL_ACTVTY_START_STATUS");
        icw_Master_Index_View_Unit_Turnaround_Start_Dte_Tme = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Unit_Turnaround_Start_Dte_Tme", 
            "UNIT-TURNAROUND-START-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_TURNAROUND_START_DTE_TME");
        icw_Master_Index_View_Corp_Turnaround = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Corp_Turnaround", "CORP-TURNAROUND", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "CORP_TURNAROUND");
        icw_Master_Index_View_Actve_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");
        icw_Master_Index_View_Rsn_CdeMuGroup = vw_icw_Master_Index_View.getRecord().newGroupInGroup("icw_Master_Index_View_Rsn_CdeMuGroup", "RSN_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ICW_MASTER_INDEX_RSN_CDE");
        icw_Master_Index_View_Rsn_Cde = icw_Master_Index_View_Rsn_CdeMuGroup.newFieldArrayInGroup("icw_Master_Index_View_Rsn_Cde", "RSN-CDE", FieldType.STRING, 
            4, new DbsArrayController(1,20), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RSN_CDE");
        icw_Master_Index_View_Alwys_Vrfy = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Alwys_Vrfy", "ALWYS-VRFY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ALWYS_VRFY");
        icw_Master_Index_View_Crs_Memo = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Crs_Memo", "CRS-MEMO", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRS_MEMO");
        icw_Master_Index_View_Pas_Ovrrde = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Pas_Ovrrde", "PAS-OVRRDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PAS_OVRRDE");
        icw_Master_Index_View_Pas_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Pas_Ind", "PAS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PAS_IND");
        icw_Master_Index_View_Cnnctd_Rqst_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Cnnctd_Rqst_Ind", "CNNCTD-RQST-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNNCTD_RQST_IND");
        icw_Master_Index_View_Init_Undrlyng_Rqst_Cnt = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Init_Undrlyng_Rqst_Cnt", 
            "INIT-UNDRLYNG-RQST-CNT", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "INIT_UNDRLYNG_RQST_CNT");
        icw_Master_Index_View_Crrnt_Cmt_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Crrnt_Cmt_Ind", "CRRNT-CMT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRRNT_CMT_IND");
        icw_Master_Index_View_Log_Sqnce_Nbr = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Log_Sqnce_Nbr", "LOG-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "LOG_SQNCE_NBR");
        icw_Master_Index_View_Response_Ind = vw_icw_Master_Index_View.getRecord().newFieldInGroup("icw_Master_Index_View_Response_Ind", "RESPONSE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RESPONSE_IND");

        this.setRecordName("LdaIcwl3010");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_icw_Master_Index_View.reset();
    }

    // Constructor
    public LdaIcwl3010() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
