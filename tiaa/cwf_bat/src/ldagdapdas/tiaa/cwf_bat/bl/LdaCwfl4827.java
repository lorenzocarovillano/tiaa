/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:49 PM
**        * FROM NATURAL LDA     : CWFL4827
************************************************************
**        * FILE NAME            : LdaCwfl4827.java
**        * CLASS NAME           : LdaCwfl4827
**        * INSTANCE NAME        : LdaCwfl4827
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4827 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_ncw_Master;
    private DbsField ncw_Master_Np_Pin;
    private DbsField ncw_Master_Rqst_Log_Dte_Tme;
    private DbsGroup ncw_Master_Rqst_Log_Dte_TmeRedef1;
    private DbsField ncw_Master_Rqst_Log_Index_Dte;
    private DbsField ncw_Master_Rqst_Log_Index_Tme;
    private DbsField ncw_Master_Rqst_Log_Unit_Cde;
    private DbsField ncw_Master_Rqst_Log_Oprtr_Cde;
    private DbsField ncw_Master_Rqst_Orgn_Cde;
    private DbsField ncw_Master_Rqst_Log_Invrt_Dte_Tme;
    private DbsField ncw_Master_Modify_Unit_Cde;
    private DbsField ncw_Master_Modify_Oprtr_Cde;
    private DbsField ncw_Master_Modify_Applctn_Cde;
    private DbsField ncw_Master_System_Updte_Dte_Tme;
    private DbsField ncw_Master_Work_Prcss_Id;
    private DbsGroup ncw_Master_Work_Prcss_IdRedef2;
    private DbsField ncw_Master_Work_Actn_Rqstd_Cde;
    private DbsField ncw_Master_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField ncw_Master_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField ncw_Master_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField ncw_Master_Work_Rqst_Prty_Cde;
    private DbsField ncw_Master_Rt_Sqnce_Nbr;
    private DbsField ncw_Master_Tiaa_Rcvd_Dte_Tme;
    private DbsField ncw_Master_Case_Id_Cde;
    private DbsGroup ncw_Master_Case_Id_CdeRedef3;
    private DbsField ncw_Master_Case_Ind;
    private DbsField ncw_Master_Sub_Rqst_Sqnce_Ind;
    private DbsField ncw_Master_Rqst_Id;
    private DbsGroup ncw_Master_Rqst_IdRedef4;
    private DbsField ncw_Master_Rqst_Work_Prcss_Id;
    private DbsField ncw_Master_Rqst_Tiaa_Rcvd_Dte;
    private DbsField ncw_Master_Rqst_Case_Id_Cde;
    private DbsField ncw_Master_Rqst_Np_Pin;
    private DbsField ncw_Master_Unit_Cde;
    private DbsGroup ncw_Master_Unit_CdeRedef5;
    private DbsField ncw_Master_Unit_Id_Cde;
    private DbsField ncw_Master_Unit_Rgn_Cde;
    private DbsField ncw_Master_Unit_Spcl_Dsgntn_Cde;
    private DbsField ncw_Master_Unit_Brnch_Group_Cde;
    private DbsField ncw_Master_Unit_Updte_Dte_Tme;
    private DbsField ncw_Master_Admin_Unit_Cde;
    private DbsField ncw_Master_Admin_Status_Cde;
    private DbsGroup ncw_Master_Admin_Status_CdeRedef6;
    private DbsField ncw_Master_Admin_Status_Cde_1;
    private DbsField ncw_Master_Admin_Status_Updte_Dte_Tme;
    private DbsField ncw_Master_Admin_Status_Updte_Oprtr_Cde;
    private DbsField ncw_Master_Empl_Racf_Id;
    private DbsField ncw_Master_Status_Cde;
    private DbsField ncw_Master_Status_Updte_Dte_Tme;
    private DbsField ncw_Master_Status_Updte_Oprtr_Cde;
    private DbsField ncw_Master_Status_Freeze_Ind;
    private DbsField ncw_Master_Step_Id;
    private DbsField ncw_Master_Step_Sqnce_Nbr;
    private DbsField ncw_Master_Step_Updte_Dte_Tme;
    private DbsField ncw_Master_Cmplnt_Ind;
    private DbsField ncw_Master_Sub_Rqst_Ind;
    private DbsField ncw_Master_Work_List_Ind;
    private DbsField ncw_Master_Wpid_Vldte_Ind;
    private DbsField ncw_Master_Crprte_Status_Ind;
    private DbsField ncw_Master_Crprte_Due_Dte_Tme;
    private DbsField ncw_Master_Crprte_Close_Dte_Tme;
    private DbsField ncw_Master_Parent_Log_Dte_Tme;
    private DbsField ncw_Master_Spcl_Hndlng_Txt;
    private DbsField ncw_Master_Strtng_Event_Dte_Tme;
    private DbsField ncw_Master_Endng_Event_Dte_Tme;
    private DbsField ncw_Master_Actvty_Elpsd_Clndr_Hours;
    private DbsField ncw_Master_Actvty_Elpsd_Bsnss_Hours;
    private DbsField ncw_Master_Invrt_Strtng_Event;
    private DbsField ncw_Master_Actve_Ind;
    private DbsField ncw_Master_Multi_Rqst_Ind;
    private DbsField ncw_Master_Cntct_Orgn_Type_Cde;
    private DbsField ncw_Master_Cntct_Dte_Tme;
    private DbsField ncw_Master_Cntct_Oprtr_Id;
    private DbsField ncw_Master_Dup_Ind;
    private DbsField ncw_Master_Elctrn_Fld_Ind;
    private DbsField ncw_Master_Crprte_On_Tme_Ind;
    private DbsField ncw_Master_Prcssng_Type_Ind;
    private DbsField ncw_Master_Invrt_Tiaa_Rcvd_Dte_Tme;
    private DbsField ncw_Master_Crrnt_Due_Dte_Prty_Tme;
    private DbsGroup ncw_Master_Crrnt_Due_Dte_Prty_TmeRedef7;
    private DbsField ncw_Master_Crrnt_Due_Dte;
    private DbsField ncw_Master_Crrnt_Prty_Cde;
    private DbsField ncw_Master_Crrnt_Due_Tme;
    private DbsField ncw_Master_Cntct_Sheet_Print_Cde;
    private DbsField ncw_Master_Cntct_Sheet_Print_Dte_Tme;
    private DbsField ncw_Master_Cntct_Sheet_Printer_Id_Cde;
    private DbsField ncw_Master_Cntct_Invrt_Dte_Tme;
    private DbsField ncw_Master_Cnnctd_Rqst_Ind;
    private DbsField ncw_Master_Crrnt_Cmt_Ind;
    private DbsField ncw_Master_Prvte_Wrk_Rqst_Ind;
    private DbsField ncw_Master_Instn_Rqst_Log_Dte_Tme;
    private DbsField ncw_Master_Log_Sqnce_Nbr;
    private DbsField ncw_Master_Rescan_Ind;
    private DbsField ncw_Master_Step_Re_Do_Ind;
    private DbsField ncw_Master_Due_Dte_Chg_Prty_Cde;
    private DbsField ncw_Master_Effctve_Dte;
    private DbsField ncw_Master_Extrnl_Pend_Rcv_Dte;
    private DbsField ncw_Master_Trade_Dte_Tme;
    private DbsField ncw_Master_Owner_Unit_Cde;
    private DbsField ncw_Master_Assgn_Dte_Tme;
    private DbsField ncw_Master_Intrnl_Pnd_Start_Dte_Tme;
    private DbsGroup ncw_Master_Intrnl_Pnd_Start_Dte_TmeRedef8;
    private DbsField ncw_Master_Intrnl_Pnd_Start_Dte;
    private DbsField ncw_Master_Intrnl_Pnd_Start_Tme;
    private DbsField ncw_Master_Intrnl_Pnd_End_Dte_Tme;
    private DbsGroup ncw_Master_Intrnl_Pnd_End_Dte_TmeRedef9;
    private DbsField ncw_Master_Intrnl_Pnd_End_Dte;
    private DbsField ncw_Master_Intrnl_Pnd_End_Tme;
    private DbsField ncw_Master_Intrnl_Pnd_Days;
    private DbsField ncw_Master_Unit_Start_Status;
    private DbsField ncw_Master_Empl_Start_Status;
    private DbsField ncw_Master_Step_Start_Status;
    private DbsField ncw_Master_Activity_End_Status;
    private DbsField ncw_Master_Unit_On_Tme_Ind;
    private DbsField ncw_Master_Crprte_Clock_End_Dte_Tme;
    private DbsGroup ncw_Master_Crprte_Clock_End_Dte_TmeRedef10;
    private DbsField ncw_Master_Crprte_Clock_End_Dte;
    private DbsField ncw_Master_Crprte_Clock_End_Tme;
    private DbsField ncw_Master_Crprte_Close_Oprtr_Cde;
    private DbsField ncw_Master_Status_Clock_Start_Dte_Tme;
    private DbsField ncw_Master_Status_Clock_End_Dte_Tme;
    private DbsField ncw_Master_Status_Elpsd_Clndr_Days_Tme;
    private DbsGroup ncw_Master_Status_Elpsd_Clndr_Days_TmeRedef11;
    private DbsField ncw_Master_Status_Elpsd_Clndr_Days;
    private DbsField ncw_Master_Status_Elpsd_Clndr_Hours;
    private DbsField ncw_Master_Status_Elpsd_Clndr_Minutes;
    private DbsField ncw_Master_Status_Elpsd_Bsnss_Days_Tme;
    private DbsGroup ncw_Master_Status_Elpsd_Bsnss_Days_TmeRedef12;
    private DbsField ncw_Master_Status_Elpsd_Bsnss_Days;
    private DbsField ncw_Master_Status_Elpsd_Bsnss_Hours;
    private DbsField ncw_Master_Status_Elpsd_Bsnss_Minutes;
    private DbsField ncw_Master_Step_Clock_Start_Dte_Tme;
    private DbsField ncw_Master_Step_Clock_End_Dte_Tme;
    private DbsField ncw_Master_Step_Elpsd_Clndr_Days_Tme;
    private DbsGroup ncw_Master_Step_Elpsd_Clndr_Days_TmeRedef13;
    private DbsField ncw_Master_Step_Elpsd_Clndr_Days;
    private DbsField ncw_Master_Step_Elpsd_Clndr_Hours;
    private DbsField ncw_Master_Step_Elpsd_Clndr_Minutes;
    private DbsField ncw_Master_Step_Elpsd_Bsnss_Days_Tme;
    private DbsGroup ncw_Master_Step_Elpsd_Bsnss_Days_TmeRedef14;
    private DbsField ncw_Master_Step_Elpsd_Bsnss_Days;
    private DbsField ncw_Master_Step_Elpsd_Bsnss_Hours;
    private DbsField ncw_Master_Step_Elpsd_Bsnss_Minutes;
    private DbsField ncw_Master_Empl_Clock_Start_Dte_Tme;
    private DbsField ncw_Master_Empl_Clock_End_Dte_Tme;
    private DbsField ncw_Master_Empl_Elpsd_Clndr_Days_Tme;
    private DbsGroup ncw_Master_Empl_Elpsd_Clndr_Days_TmeRedef15;
    private DbsField ncw_Master_Empl_Elpsd_Clndr_Days;
    private DbsField ncw_Master_Empl_Elpsd_Clndr_Hours;
    private DbsField ncw_Master_Empl_Elpsd_Clndr_Minutes;
    private DbsField ncw_Master_Empl_Elpsd_Bsnss_Days_Tme;
    private DbsGroup ncw_Master_Empl_Elpsd_Bsnss_Days_TmeRedef16;
    private DbsField ncw_Master_Empl_Elpsd_Bsnss_Days;
    private DbsField ncw_Master_Empl_Elpsd_Bsnss_Hours;
    private DbsField ncw_Master_Empl_Elpsd_Bsnss_Minutes;
    private DbsField ncw_Master_Unit_Clock_Start_Dte_Tme;
    private DbsField ncw_Master_Unit_Clock_End_Dte_Tme;
    private DbsField ncw_Master_Unit_Elpsd_Clndr_Days_Tme;
    private DbsGroup ncw_Master_Unit_Elpsd_Clndr_Days_TmeRedef17;
    private DbsField ncw_Master_Unit_Elpsd_Clndr_Days;
    private DbsField ncw_Master_Unit_Elpsd_Clndr_Hours;
    private DbsField ncw_Master_Unit_Elpsd_Clndr_Minutes;
    private DbsField ncw_Master_Unit_Elpsd_Bsnss_Days_Tme;
    private DbsGroup ncw_Master_Unit_Elpsd_Bsnss_Days_TmeRedef18;
    private DbsField ncw_Master_Unit_Elpsd_Bsnss_Days;
    private DbsField ncw_Master_Unit_Elpsd_Bsnss_Hours;
    private DbsField ncw_Master_Unit_Elpsd_Bsnss_Minutes;

    public DataAccessProgramView getVw_ncw_Master() { return vw_ncw_Master; }

    public DbsField getNcw_Master_Np_Pin() { return ncw_Master_Np_Pin; }

    public DbsField getNcw_Master_Rqst_Log_Dte_Tme() { return ncw_Master_Rqst_Log_Dte_Tme; }

    public DbsGroup getNcw_Master_Rqst_Log_Dte_TmeRedef1() { return ncw_Master_Rqst_Log_Dte_TmeRedef1; }

    public DbsField getNcw_Master_Rqst_Log_Index_Dte() { return ncw_Master_Rqst_Log_Index_Dte; }

    public DbsField getNcw_Master_Rqst_Log_Index_Tme() { return ncw_Master_Rqst_Log_Index_Tme; }

    public DbsField getNcw_Master_Rqst_Log_Unit_Cde() { return ncw_Master_Rqst_Log_Unit_Cde; }

    public DbsField getNcw_Master_Rqst_Log_Oprtr_Cde() { return ncw_Master_Rqst_Log_Oprtr_Cde; }

    public DbsField getNcw_Master_Rqst_Orgn_Cde() { return ncw_Master_Rqst_Orgn_Cde; }

    public DbsField getNcw_Master_Rqst_Log_Invrt_Dte_Tme() { return ncw_Master_Rqst_Log_Invrt_Dte_Tme; }

    public DbsField getNcw_Master_Modify_Unit_Cde() { return ncw_Master_Modify_Unit_Cde; }

    public DbsField getNcw_Master_Modify_Oprtr_Cde() { return ncw_Master_Modify_Oprtr_Cde; }

    public DbsField getNcw_Master_Modify_Applctn_Cde() { return ncw_Master_Modify_Applctn_Cde; }

    public DbsField getNcw_Master_System_Updte_Dte_Tme() { return ncw_Master_System_Updte_Dte_Tme; }

    public DbsField getNcw_Master_Work_Prcss_Id() { return ncw_Master_Work_Prcss_Id; }

    public DbsGroup getNcw_Master_Work_Prcss_IdRedef2() { return ncw_Master_Work_Prcss_IdRedef2; }

    public DbsField getNcw_Master_Work_Actn_Rqstd_Cde() { return ncw_Master_Work_Actn_Rqstd_Cde; }

    public DbsField getNcw_Master_Work_Lob_Cmpny_Prdct_Cde() { return ncw_Master_Work_Lob_Cmpny_Prdct_Cde; }

    public DbsField getNcw_Master_Work_Mjr_Bsnss_Prcss_Cde() { return ncw_Master_Work_Mjr_Bsnss_Prcss_Cde; }

    public DbsField getNcw_Master_Work_Spcfc_Bsnss_Prcss_Cde() { return ncw_Master_Work_Spcfc_Bsnss_Prcss_Cde; }

    public DbsField getNcw_Master_Work_Rqst_Prty_Cde() { return ncw_Master_Work_Rqst_Prty_Cde; }

    public DbsField getNcw_Master_Rt_Sqnce_Nbr() { return ncw_Master_Rt_Sqnce_Nbr; }

    public DbsField getNcw_Master_Tiaa_Rcvd_Dte_Tme() { return ncw_Master_Tiaa_Rcvd_Dte_Tme; }

    public DbsField getNcw_Master_Case_Id_Cde() { return ncw_Master_Case_Id_Cde; }

    public DbsGroup getNcw_Master_Case_Id_CdeRedef3() { return ncw_Master_Case_Id_CdeRedef3; }

    public DbsField getNcw_Master_Case_Ind() { return ncw_Master_Case_Ind; }

    public DbsField getNcw_Master_Sub_Rqst_Sqnce_Ind() { return ncw_Master_Sub_Rqst_Sqnce_Ind; }

    public DbsField getNcw_Master_Rqst_Id() { return ncw_Master_Rqst_Id; }

    public DbsGroup getNcw_Master_Rqst_IdRedef4() { return ncw_Master_Rqst_IdRedef4; }

    public DbsField getNcw_Master_Rqst_Work_Prcss_Id() { return ncw_Master_Rqst_Work_Prcss_Id; }

    public DbsField getNcw_Master_Rqst_Tiaa_Rcvd_Dte() { return ncw_Master_Rqst_Tiaa_Rcvd_Dte; }

    public DbsField getNcw_Master_Rqst_Case_Id_Cde() { return ncw_Master_Rqst_Case_Id_Cde; }

    public DbsField getNcw_Master_Rqst_Np_Pin() { return ncw_Master_Rqst_Np_Pin; }

    public DbsField getNcw_Master_Unit_Cde() { return ncw_Master_Unit_Cde; }

    public DbsGroup getNcw_Master_Unit_CdeRedef5() { return ncw_Master_Unit_CdeRedef5; }

    public DbsField getNcw_Master_Unit_Id_Cde() { return ncw_Master_Unit_Id_Cde; }

    public DbsField getNcw_Master_Unit_Rgn_Cde() { return ncw_Master_Unit_Rgn_Cde; }

    public DbsField getNcw_Master_Unit_Spcl_Dsgntn_Cde() { return ncw_Master_Unit_Spcl_Dsgntn_Cde; }

    public DbsField getNcw_Master_Unit_Brnch_Group_Cde() { return ncw_Master_Unit_Brnch_Group_Cde; }

    public DbsField getNcw_Master_Unit_Updte_Dte_Tme() { return ncw_Master_Unit_Updte_Dte_Tme; }

    public DbsField getNcw_Master_Admin_Unit_Cde() { return ncw_Master_Admin_Unit_Cde; }

    public DbsField getNcw_Master_Admin_Status_Cde() { return ncw_Master_Admin_Status_Cde; }

    public DbsGroup getNcw_Master_Admin_Status_CdeRedef6() { return ncw_Master_Admin_Status_CdeRedef6; }

    public DbsField getNcw_Master_Admin_Status_Cde_1() { return ncw_Master_Admin_Status_Cde_1; }

    public DbsField getNcw_Master_Admin_Status_Updte_Dte_Tme() { return ncw_Master_Admin_Status_Updte_Dte_Tme; }

    public DbsField getNcw_Master_Admin_Status_Updte_Oprtr_Cde() { return ncw_Master_Admin_Status_Updte_Oprtr_Cde; }

    public DbsField getNcw_Master_Empl_Racf_Id() { return ncw_Master_Empl_Racf_Id; }

    public DbsField getNcw_Master_Status_Cde() { return ncw_Master_Status_Cde; }

    public DbsField getNcw_Master_Status_Updte_Dte_Tme() { return ncw_Master_Status_Updte_Dte_Tme; }

    public DbsField getNcw_Master_Status_Updte_Oprtr_Cde() { return ncw_Master_Status_Updte_Oprtr_Cde; }

    public DbsField getNcw_Master_Status_Freeze_Ind() { return ncw_Master_Status_Freeze_Ind; }

    public DbsField getNcw_Master_Step_Id() { return ncw_Master_Step_Id; }

    public DbsField getNcw_Master_Step_Sqnce_Nbr() { return ncw_Master_Step_Sqnce_Nbr; }

    public DbsField getNcw_Master_Step_Updte_Dte_Tme() { return ncw_Master_Step_Updte_Dte_Tme; }

    public DbsField getNcw_Master_Cmplnt_Ind() { return ncw_Master_Cmplnt_Ind; }

    public DbsField getNcw_Master_Sub_Rqst_Ind() { return ncw_Master_Sub_Rqst_Ind; }

    public DbsField getNcw_Master_Work_List_Ind() { return ncw_Master_Work_List_Ind; }

    public DbsField getNcw_Master_Wpid_Vldte_Ind() { return ncw_Master_Wpid_Vldte_Ind; }

    public DbsField getNcw_Master_Crprte_Status_Ind() { return ncw_Master_Crprte_Status_Ind; }

    public DbsField getNcw_Master_Crprte_Due_Dte_Tme() { return ncw_Master_Crprte_Due_Dte_Tme; }

    public DbsField getNcw_Master_Crprte_Close_Dte_Tme() { return ncw_Master_Crprte_Close_Dte_Tme; }

    public DbsField getNcw_Master_Parent_Log_Dte_Tme() { return ncw_Master_Parent_Log_Dte_Tme; }

    public DbsField getNcw_Master_Spcl_Hndlng_Txt() { return ncw_Master_Spcl_Hndlng_Txt; }

    public DbsField getNcw_Master_Strtng_Event_Dte_Tme() { return ncw_Master_Strtng_Event_Dte_Tme; }

    public DbsField getNcw_Master_Endng_Event_Dte_Tme() { return ncw_Master_Endng_Event_Dte_Tme; }

    public DbsField getNcw_Master_Actvty_Elpsd_Clndr_Hours() { return ncw_Master_Actvty_Elpsd_Clndr_Hours; }

    public DbsField getNcw_Master_Actvty_Elpsd_Bsnss_Hours() { return ncw_Master_Actvty_Elpsd_Bsnss_Hours; }

    public DbsField getNcw_Master_Invrt_Strtng_Event() { return ncw_Master_Invrt_Strtng_Event; }

    public DbsField getNcw_Master_Actve_Ind() { return ncw_Master_Actve_Ind; }

    public DbsField getNcw_Master_Multi_Rqst_Ind() { return ncw_Master_Multi_Rqst_Ind; }

    public DbsField getNcw_Master_Cntct_Orgn_Type_Cde() { return ncw_Master_Cntct_Orgn_Type_Cde; }

    public DbsField getNcw_Master_Cntct_Dte_Tme() { return ncw_Master_Cntct_Dte_Tme; }

    public DbsField getNcw_Master_Cntct_Oprtr_Id() { return ncw_Master_Cntct_Oprtr_Id; }

    public DbsField getNcw_Master_Dup_Ind() { return ncw_Master_Dup_Ind; }

    public DbsField getNcw_Master_Elctrn_Fld_Ind() { return ncw_Master_Elctrn_Fld_Ind; }

    public DbsField getNcw_Master_Crprte_On_Tme_Ind() { return ncw_Master_Crprte_On_Tme_Ind; }

    public DbsField getNcw_Master_Prcssng_Type_Ind() { return ncw_Master_Prcssng_Type_Ind; }

    public DbsField getNcw_Master_Invrt_Tiaa_Rcvd_Dte_Tme() { return ncw_Master_Invrt_Tiaa_Rcvd_Dte_Tme; }

    public DbsField getNcw_Master_Crrnt_Due_Dte_Prty_Tme() { return ncw_Master_Crrnt_Due_Dte_Prty_Tme; }

    public DbsGroup getNcw_Master_Crrnt_Due_Dte_Prty_TmeRedef7() { return ncw_Master_Crrnt_Due_Dte_Prty_TmeRedef7; }

    public DbsField getNcw_Master_Crrnt_Due_Dte() { return ncw_Master_Crrnt_Due_Dte; }

    public DbsField getNcw_Master_Crrnt_Prty_Cde() { return ncw_Master_Crrnt_Prty_Cde; }

    public DbsField getNcw_Master_Crrnt_Due_Tme() { return ncw_Master_Crrnt_Due_Tme; }

    public DbsField getNcw_Master_Cntct_Sheet_Print_Cde() { return ncw_Master_Cntct_Sheet_Print_Cde; }

    public DbsField getNcw_Master_Cntct_Sheet_Print_Dte_Tme() { return ncw_Master_Cntct_Sheet_Print_Dte_Tme; }

    public DbsField getNcw_Master_Cntct_Sheet_Printer_Id_Cde() { return ncw_Master_Cntct_Sheet_Printer_Id_Cde; }

    public DbsField getNcw_Master_Cntct_Invrt_Dte_Tme() { return ncw_Master_Cntct_Invrt_Dte_Tme; }

    public DbsField getNcw_Master_Cnnctd_Rqst_Ind() { return ncw_Master_Cnnctd_Rqst_Ind; }

    public DbsField getNcw_Master_Crrnt_Cmt_Ind() { return ncw_Master_Crrnt_Cmt_Ind; }

    public DbsField getNcw_Master_Prvte_Wrk_Rqst_Ind() { return ncw_Master_Prvte_Wrk_Rqst_Ind; }

    public DbsField getNcw_Master_Instn_Rqst_Log_Dte_Tme() { return ncw_Master_Instn_Rqst_Log_Dte_Tme; }

    public DbsField getNcw_Master_Log_Sqnce_Nbr() { return ncw_Master_Log_Sqnce_Nbr; }

    public DbsField getNcw_Master_Rescan_Ind() { return ncw_Master_Rescan_Ind; }

    public DbsField getNcw_Master_Step_Re_Do_Ind() { return ncw_Master_Step_Re_Do_Ind; }

    public DbsField getNcw_Master_Due_Dte_Chg_Prty_Cde() { return ncw_Master_Due_Dte_Chg_Prty_Cde; }

    public DbsField getNcw_Master_Effctve_Dte() { return ncw_Master_Effctve_Dte; }

    public DbsField getNcw_Master_Extrnl_Pend_Rcv_Dte() { return ncw_Master_Extrnl_Pend_Rcv_Dte; }

    public DbsField getNcw_Master_Trade_Dte_Tme() { return ncw_Master_Trade_Dte_Tme; }

    public DbsField getNcw_Master_Owner_Unit_Cde() { return ncw_Master_Owner_Unit_Cde; }

    public DbsField getNcw_Master_Assgn_Dte_Tme() { return ncw_Master_Assgn_Dte_Tme; }

    public DbsField getNcw_Master_Intrnl_Pnd_Start_Dte_Tme() { return ncw_Master_Intrnl_Pnd_Start_Dte_Tme; }

    public DbsGroup getNcw_Master_Intrnl_Pnd_Start_Dte_TmeRedef8() { return ncw_Master_Intrnl_Pnd_Start_Dte_TmeRedef8; }

    public DbsField getNcw_Master_Intrnl_Pnd_Start_Dte() { return ncw_Master_Intrnl_Pnd_Start_Dte; }

    public DbsField getNcw_Master_Intrnl_Pnd_Start_Tme() { return ncw_Master_Intrnl_Pnd_Start_Tme; }

    public DbsField getNcw_Master_Intrnl_Pnd_End_Dte_Tme() { return ncw_Master_Intrnl_Pnd_End_Dte_Tme; }

    public DbsGroup getNcw_Master_Intrnl_Pnd_End_Dte_TmeRedef9() { return ncw_Master_Intrnl_Pnd_End_Dte_TmeRedef9; }

    public DbsField getNcw_Master_Intrnl_Pnd_End_Dte() { return ncw_Master_Intrnl_Pnd_End_Dte; }

    public DbsField getNcw_Master_Intrnl_Pnd_End_Tme() { return ncw_Master_Intrnl_Pnd_End_Tme; }

    public DbsField getNcw_Master_Intrnl_Pnd_Days() { return ncw_Master_Intrnl_Pnd_Days; }

    public DbsField getNcw_Master_Unit_Start_Status() { return ncw_Master_Unit_Start_Status; }

    public DbsField getNcw_Master_Empl_Start_Status() { return ncw_Master_Empl_Start_Status; }

    public DbsField getNcw_Master_Step_Start_Status() { return ncw_Master_Step_Start_Status; }

    public DbsField getNcw_Master_Activity_End_Status() { return ncw_Master_Activity_End_Status; }

    public DbsField getNcw_Master_Unit_On_Tme_Ind() { return ncw_Master_Unit_On_Tme_Ind; }

    public DbsField getNcw_Master_Crprte_Clock_End_Dte_Tme() { return ncw_Master_Crprte_Clock_End_Dte_Tme; }

    public DbsGroup getNcw_Master_Crprte_Clock_End_Dte_TmeRedef10() { return ncw_Master_Crprte_Clock_End_Dte_TmeRedef10; }

    public DbsField getNcw_Master_Crprte_Clock_End_Dte() { return ncw_Master_Crprte_Clock_End_Dte; }

    public DbsField getNcw_Master_Crprte_Clock_End_Tme() { return ncw_Master_Crprte_Clock_End_Tme; }

    public DbsField getNcw_Master_Crprte_Close_Oprtr_Cde() { return ncw_Master_Crprte_Close_Oprtr_Cde; }

    public DbsField getNcw_Master_Status_Clock_Start_Dte_Tme() { return ncw_Master_Status_Clock_Start_Dte_Tme; }

    public DbsField getNcw_Master_Status_Clock_End_Dte_Tme() { return ncw_Master_Status_Clock_End_Dte_Tme; }

    public DbsField getNcw_Master_Status_Elpsd_Clndr_Days_Tme() { return ncw_Master_Status_Elpsd_Clndr_Days_Tme; }

    public DbsGroup getNcw_Master_Status_Elpsd_Clndr_Days_TmeRedef11() { return ncw_Master_Status_Elpsd_Clndr_Days_TmeRedef11; }

    public DbsField getNcw_Master_Status_Elpsd_Clndr_Days() { return ncw_Master_Status_Elpsd_Clndr_Days; }

    public DbsField getNcw_Master_Status_Elpsd_Clndr_Hours() { return ncw_Master_Status_Elpsd_Clndr_Hours; }

    public DbsField getNcw_Master_Status_Elpsd_Clndr_Minutes() { return ncw_Master_Status_Elpsd_Clndr_Minutes; }

    public DbsField getNcw_Master_Status_Elpsd_Bsnss_Days_Tme() { return ncw_Master_Status_Elpsd_Bsnss_Days_Tme; }

    public DbsGroup getNcw_Master_Status_Elpsd_Bsnss_Days_TmeRedef12() { return ncw_Master_Status_Elpsd_Bsnss_Days_TmeRedef12; }

    public DbsField getNcw_Master_Status_Elpsd_Bsnss_Days() { return ncw_Master_Status_Elpsd_Bsnss_Days; }

    public DbsField getNcw_Master_Status_Elpsd_Bsnss_Hours() { return ncw_Master_Status_Elpsd_Bsnss_Hours; }

    public DbsField getNcw_Master_Status_Elpsd_Bsnss_Minutes() { return ncw_Master_Status_Elpsd_Bsnss_Minutes; }

    public DbsField getNcw_Master_Step_Clock_Start_Dte_Tme() { return ncw_Master_Step_Clock_Start_Dte_Tme; }

    public DbsField getNcw_Master_Step_Clock_End_Dte_Tme() { return ncw_Master_Step_Clock_End_Dte_Tme; }

    public DbsField getNcw_Master_Step_Elpsd_Clndr_Days_Tme() { return ncw_Master_Step_Elpsd_Clndr_Days_Tme; }

    public DbsGroup getNcw_Master_Step_Elpsd_Clndr_Days_TmeRedef13() { return ncw_Master_Step_Elpsd_Clndr_Days_TmeRedef13; }

    public DbsField getNcw_Master_Step_Elpsd_Clndr_Days() { return ncw_Master_Step_Elpsd_Clndr_Days; }

    public DbsField getNcw_Master_Step_Elpsd_Clndr_Hours() { return ncw_Master_Step_Elpsd_Clndr_Hours; }

    public DbsField getNcw_Master_Step_Elpsd_Clndr_Minutes() { return ncw_Master_Step_Elpsd_Clndr_Minutes; }

    public DbsField getNcw_Master_Step_Elpsd_Bsnss_Days_Tme() { return ncw_Master_Step_Elpsd_Bsnss_Days_Tme; }

    public DbsGroup getNcw_Master_Step_Elpsd_Bsnss_Days_TmeRedef14() { return ncw_Master_Step_Elpsd_Bsnss_Days_TmeRedef14; }

    public DbsField getNcw_Master_Step_Elpsd_Bsnss_Days() { return ncw_Master_Step_Elpsd_Bsnss_Days; }

    public DbsField getNcw_Master_Step_Elpsd_Bsnss_Hours() { return ncw_Master_Step_Elpsd_Bsnss_Hours; }

    public DbsField getNcw_Master_Step_Elpsd_Bsnss_Minutes() { return ncw_Master_Step_Elpsd_Bsnss_Minutes; }

    public DbsField getNcw_Master_Empl_Clock_Start_Dte_Tme() { return ncw_Master_Empl_Clock_Start_Dte_Tme; }

    public DbsField getNcw_Master_Empl_Clock_End_Dte_Tme() { return ncw_Master_Empl_Clock_End_Dte_Tme; }

    public DbsField getNcw_Master_Empl_Elpsd_Clndr_Days_Tme() { return ncw_Master_Empl_Elpsd_Clndr_Days_Tme; }

    public DbsGroup getNcw_Master_Empl_Elpsd_Clndr_Days_TmeRedef15() { return ncw_Master_Empl_Elpsd_Clndr_Days_TmeRedef15; }

    public DbsField getNcw_Master_Empl_Elpsd_Clndr_Days() { return ncw_Master_Empl_Elpsd_Clndr_Days; }

    public DbsField getNcw_Master_Empl_Elpsd_Clndr_Hours() { return ncw_Master_Empl_Elpsd_Clndr_Hours; }

    public DbsField getNcw_Master_Empl_Elpsd_Clndr_Minutes() { return ncw_Master_Empl_Elpsd_Clndr_Minutes; }

    public DbsField getNcw_Master_Empl_Elpsd_Bsnss_Days_Tme() { return ncw_Master_Empl_Elpsd_Bsnss_Days_Tme; }

    public DbsGroup getNcw_Master_Empl_Elpsd_Bsnss_Days_TmeRedef16() { return ncw_Master_Empl_Elpsd_Bsnss_Days_TmeRedef16; }

    public DbsField getNcw_Master_Empl_Elpsd_Bsnss_Days() { return ncw_Master_Empl_Elpsd_Bsnss_Days; }

    public DbsField getNcw_Master_Empl_Elpsd_Bsnss_Hours() { return ncw_Master_Empl_Elpsd_Bsnss_Hours; }

    public DbsField getNcw_Master_Empl_Elpsd_Bsnss_Minutes() { return ncw_Master_Empl_Elpsd_Bsnss_Minutes; }

    public DbsField getNcw_Master_Unit_Clock_Start_Dte_Tme() { return ncw_Master_Unit_Clock_Start_Dte_Tme; }

    public DbsField getNcw_Master_Unit_Clock_End_Dte_Tme() { return ncw_Master_Unit_Clock_End_Dte_Tme; }

    public DbsField getNcw_Master_Unit_Elpsd_Clndr_Days_Tme() { return ncw_Master_Unit_Elpsd_Clndr_Days_Tme; }

    public DbsGroup getNcw_Master_Unit_Elpsd_Clndr_Days_TmeRedef17() { return ncw_Master_Unit_Elpsd_Clndr_Days_TmeRedef17; }

    public DbsField getNcw_Master_Unit_Elpsd_Clndr_Days() { return ncw_Master_Unit_Elpsd_Clndr_Days; }

    public DbsField getNcw_Master_Unit_Elpsd_Clndr_Hours() { return ncw_Master_Unit_Elpsd_Clndr_Hours; }

    public DbsField getNcw_Master_Unit_Elpsd_Clndr_Minutes() { return ncw_Master_Unit_Elpsd_Clndr_Minutes; }

    public DbsField getNcw_Master_Unit_Elpsd_Bsnss_Days_Tme() { return ncw_Master_Unit_Elpsd_Bsnss_Days_Tme; }

    public DbsGroup getNcw_Master_Unit_Elpsd_Bsnss_Days_TmeRedef18() { return ncw_Master_Unit_Elpsd_Bsnss_Days_TmeRedef18; }

    public DbsField getNcw_Master_Unit_Elpsd_Bsnss_Days() { return ncw_Master_Unit_Elpsd_Bsnss_Days; }

    public DbsField getNcw_Master_Unit_Elpsd_Bsnss_Hours() { return ncw_Master_Unit_Elpsd_Bsnss_Hours; }

    public DbsField getNcw_Master_Unit_Elpsd_Bsnss_Minutes() { return ncw_Master_Unit_Elpsd_Bsnss_Minutes; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_ncw_Master = new DataAccessProgramView(new NameInfo("vw_ncw_Master", "NCW-MASTER"), "NCW_MASTER_INDEX", "NCW_MASTER_INDEX");
        ncw_Master_Np_Pin = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Master_Rqst_Log_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        ncw_Master_Rqst_Log_Dte_TmeRedef1 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Rqst_Log_Dte_TmeRedef1", "Redefines", ncw_Master_Rqst_Log_Dte_Tme);
        ncw_Master_Rqst_Log_Index_Dte = ncw_Master_Rqst_Log_Dte_TmeRedef1.newFieldInGroup("ncw_Master_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 
            8);
        ncw_Master_Rqst_Log_Index_Tme = ncw_Master_Rqst_Log_Dte_TmeRedef1.newFieldInGroup("ncw_Master_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", FieldType.STRING, 
            7);
        ncw_Master_Rqst_Log_Unit_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Rqst_Log_Unit_Cde", "RQST-LOG-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_LOG_UNIT_CDE");
        ncw_Master_Rqst_Log_Unit_Cde.setDdmHeader("LOG/UNIT");
        ncw_Master_Rqst_Log_Oprtr_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        ncw_Master_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        ncw_Master_Rqst_Orgn_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_ORGN_CDE");
        ncw_Master_Rqst_Log_Invrt_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Rqst_Log_Invrt_Dte_Tme", "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        ncw_Master_Modify_Unit_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Modify_Unit_Cde", "MODIFY-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "MODIFY_UNIT_CDE");
        ncw_Master_Modify_Oprtr_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Modify_Oprtr_Cde", "MODIFY-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "MODIFY_OPRTR_CDE");
        ncw_Master_Modify_Applctn_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Modify_Applctn_Cde", "MODIFY-APPLCTN-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "MODIFY_APPLCTN_CDE");
        ncw_Master_System_Updte_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_System_Updte_Dte_Tme", "SYSTEM-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "SYSTEM_UPDTE_DTE_TME");
        ncw_Master_Work_Prcss_Id = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        ncw_Master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        ncw_Master_Work_Prcss_IdRedef2 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Work_Prcss_IdRedef2", "Redefines", ncw_Master_Work_Prcss_Id);
        ncw_Master_Work_Actn_Rqstd_Cde = ncw_Master_Work_Prcss_IdRedef2.newFieldInGroup("ncw_Master_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", FieldType.STRING, 
            1);
        ncw_Master_Work_Lob_Cmpny_Prdct_Cde = ncw_Master_Work_Prcss_IdRedef2.newFieldInGroup("ncw_Master_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        ncw_Master_Work_Mjr_Bsnss_Prcss_Cde = ncw_Master_Work_Prcss_IdRedef2.newFieldInGroup("ncw_Master_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        ncw_Master_Work_Spcfc_Bsnss_Prcss_Cde = ncw_Master_Work_Prcss_IdRedef2.newFieldInGroup("ncw_Master_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        ncw_Master_Work_Rqst_Prty_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Work_Rqst_Prty_Cde", "WORK-RQST-PRTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "WORK_RQST_PRTY_CDE");
        ncw_Master_Work_Rqst_Prty_Cde.setDdmHeader("PRIO");
        ncw_Master_Rt_Sqnce_Nbr = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "RT_SQNCE_NBR");
        ncw_Master_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        ncw_Master_Tiaa_Rcvd_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        ncw_Master_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        ncw_Master_Case_Id_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CASE_ID_CDE");
        ncw_Master_Case_Id_Cde.setDdmHeader("CASE/ID");
        ncw_Master_Case_Id_CdeRedef3 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Case_Id_CdeRedef3", "Redefines", ncw_Master_Case_Id_Cde);
        ncw_Master_Case_Ind = ncw_Master_Case_Id_CdeRedef3.newFieldInGroup("ncw_Master_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        ncw_Master_Sub_Rqst_Sqnce_Ind = ncw_Master_Case_Id_CdeRedef3.newFieldInGroup("ncw_Master_Sub_Rqst_Sqnce_Ind", "SUB-RQST-SQNCE-IND", FieldType.STRING, 
            1);
        ncw_Master_Rqst_Id = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Rqst_Id", "RQST-ID", FieldType.STRING, 23, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ncw_Master_Rqst_Id.setDdmHeader("REQUEST ID");
        ncw_Master_Rqst_IdRedef4 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Rqst_IdRedef4", "Redefines", ncw_Master_Rqst_Id);
        ncw_Master_Rqst_Work_Prcss_Id = ncw_Master_Rqst_IdRedef4.newFieldInGroup("ncw_Master_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        ncw_Master_Rqst_Tiaa_Rcvd_Dte = ncw_Master_Rqst_IdRedef4.newFieldInGroup("ncw_Master_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", FieldType.STRING, 
            8);
        ncw_Master_Rqst_Case_Id_Cde = ncw_Master_Rqst_IdRedef4.newFieldInGroup("ncw_Master_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", FieldType.STRING, 2);
        ncw_Master_Rqst_Np_Pin = ncw_Master_Rqst_IdRedef4.newFieldInGroup("ncw_Master_Rqst_Np_Pin", "RQST-NP-PIN", FieldType.STRING, 7);
        ncw_Master_Unit_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        ncw_Master_Unit_Cde.setDdmHeader("UNIT/CODE");
        ncw_Master_Unit_CdeRedef5 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Unit_CdeRedef5", "Redefines", ncw_Master_Unit_Cde);
        ncw_Master_Unit_Id_Cde = ncw_Master_Unit_CdeRedef5.newFieldInGroup("ncw_Master_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        ncw_Master_Unit_Rgn_Cde = ncw_Master_Unit_CdeRedef5.newFieldInGroup("ncw_Master_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 1);
        ncw_Master_Unit_Spcl_Dsgntn_Cde = ncw_Master_Unit_CdeRedef5.newFieldInGroup("ncw_Master_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", FieldType.STRING, 
            1);
        ncw_Master_Unit_Brnch_Group_Cde = ncw_Master_Unit_CdeRedef5.newFieldInGroup("ncw_Master_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", FieldType.STRING, 
            1);
        ncw_Master_Unit_Updte_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        ncw_Master_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        ncw_Master_Admin_Unit_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        ncw_Master_Admin_Status_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        ncw_Master_Admin_Status_CdeRedef6 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Admin_Status_CdeRedef6", "Redefines", ncw_Master_Admin_Status_Cde);
        ncw_Master_Admin_Status_Cde_1 = ncw_Master_Admin_Status_CdeRedef6.newFieldInGroup("ncw_Master_Admin_Status_Cde_1", "ADMIN-STATUS-CDE-1", FieldType.STRING, 
            1);
        ncw_Master_Admin_Status_Updte_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        ncw_Master_Admin_Status_Updte_Oprtr_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        ncw_Master_Empl_Racf_Id = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "EMPL_RACF_ID");
        ncw_Master_Status_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        ncw_Master_Status_Updte_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        ncw_Master_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        ncw_Master_Status_Updte_Oprtr_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        ncw_Master_Status_Freeze_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Status_Freeze_Ind", "STATUS-FREEZE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        ncw_Master_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        ncw_Master_Step_Id = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        ncw_Master_Step_Id.setDdmHeader("STEP/ID");
        ncw_Master_Step_Sqnce_Nbr = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Step_Sqnce_Nbr", "STEP-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "STEP_SQNCE_NBR");
        ncw_Master_Step_Updte_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Step_Updte_Dte_Tme", "STEP-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STEP_UPDTE_DTE_TME");
        ncw_Master_Step_Updte_Dte_Tme.setDdmHeader("STEP UPDATE/DATE-TIME");
        ncw_Master_Cmplnt_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CMPLNT_IND");
        ncw_Master_Sub_Rqst_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SUB_RQST_IND");
        ncw_Master_Sub_Rqst_Ind.setDdmHeader("SUB-REQUEST COUNT");
        ncw_Master_Work_List_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "WORK_LIST_IND");
        ncw_Master_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        ncw_Master_Wpid_Vldte_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Wpid_Vldte_Ind", "WPID-VLDTE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "WPID_VLDTE_IND");
        ncw_Master_Crprte_Status_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        ncw_Master_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        ncw_Master_Crprte_Due_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRPRTE_DUE_DTE_TME");
        ncw_Master_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        ncw_Master_Crprte_Close_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Crprte_Close_Dte_Tme", "CRPRTE-CLOSE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRPRTE_CLOSE_DTE_TME");
        ncw_Master_Parent_Log_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Parent_Log_Dte_Tme", "PARENT-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "PARENT_LOG_DTE_TME");
        ncw_Master_Parent_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        ncw_Master_Spcl_Hndlng_Txt = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", FieldType.STRING, 72, 
            RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        ncw_Master_Spcl_Hndlng_Txt.setDdmHeader("MESSAGES");
        ncw_Master_Strtng_Event_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Strtng_Event_Dte_Tme", "STRTNG-EVENT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STRTNG_EVENT_DTE_TME");
        ncw_Master_Endng_Event_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Endng_Event_Dte_Tme", "ENDNG-EVENT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENDNG_EVENT_DTE_TME");
        ncw_Master_Actvty_Elpsd_Clndr_Hours = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Actvty_Elpsd_Clndr_Hours", "ACTVTY-ELPSD-CLNDR-HOURS", 
            FieldType.NUMERIC, 6, 2, RepeatingFieldStrategy.None, "ACTVTY_ELPSD_CLNDR_HOURS");
        ncw_Master_Actvty_Elpsd_Bsnss_Hours = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Actvty_Elpsd_Bsnss_Hours", "ACTVTY-ELPSD-BSNSS-HOURS", 
            FieldType.NUMERIC, 6, 2, RepeatingFieldStrategy.None, "ACTVTY_ELPSD_BSNSS_HOURS");
        ncw_Master_Invrt_Strtng_Event = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Invrt_Strtng_Event", "INVRT-STRTNG-EVENT", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "INVRT_STRTNG_EVENT");
        ncw_Master_Actve_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        ncw_Master_Multi_Rqst_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MULTI_RQST_IND");
        ncw_Master_Cntct_Orgn_Type_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Cntct_Orgn_Type_Cde", "CNTCT-ORGN-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTCT_ORGN_TYPE_CDE");
        ncw_Master_Cntct_Orgn_Type_Cde.setDdmHeader("CONTACT/MOC");
        ncw_Master_Cntct_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Cntct_Dte_Tme", "CNTCT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, 
            "CNTCT_DTE_TME");
        ncw_Master_Cntct_Dte_Tme.setDdmHeader("CONTACT/DATE/TIME");
        ncw_Master_Cntct_Oprtr_Id = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Cntct_Oprtr_Id", "CNTCT-OPRTR-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTCT_OPRTR_ID");
        ncw_Master_Cntct_Oprtr_Id.setDdmHeader("CONTACT/OPRTR ID");
        ncw_Master_Dup_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Dup_Ind", "DUP-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DUP_IND");
        ncw_Master_Dup_Ind.setDdmHeader("DUPL/IND");
        ncw_Master_Elctrn_Fld_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Elctrn_Fld_Ind", "ELCTRN-FLD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ELCTRN_FLD_IND");
        ncw_Master_Elctrn_Fld_Ind.setDdmHeader("ELECTRNC/FLDR IND");
        ncw_Master_Crprte_On_Tme_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRPRTE_ON_TME_IND");
        ncw_Master_Crprte_On_Tme_Ind.setDdmHeader("CRPRTE/ON TIME");
        ncw_Master_Prcssng_Type_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Prcssng_Type_Ind", "PRCSSNG-TYPE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "PRCSSNG_TYPE_IND");
        ncw_Master_Prcssng_Type_Ind.setDdmHeader("PROC/TYPE");
        ncw_Master_Invrt_Tiaa_Rcvd_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Invrt_Tiaa_Rcvd_Dte_Tme", "INVRT-TIAA-RCVD-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "INVRT_TIAA_RCVD_DTE_TME");
        ncw_Master_Invrt_Tiaa_Rcvd_Dte_Tme.setDdmHeader("INVERT TIAA/RECVD DATE");
        ncw_Master_Crrnt_Due_Dte_Prty_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Crrnt_Due_Dte_Prty_Tme", "CRRNT-DUE-DTE-PRTY-TME", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "CRRNT_DUE_DTE_PRTY_TME");
        ncw_Master_Crrnt_Due_Dte_Prty_TmeRedef7 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Crrnt_Due_Dte_Prty_TmeRedef7", "Redefines", ncw_Master_Crrnt_Due_Dte_Prty_Tme);
        ncw_Master_Crrnt_Due_Dte = ncw_Master_Crrnt_Due_Dte_Prty_TmeRedef7.newFieldInGroup("ncw_Master_Crrnt_Due_Dte", "CRRNT-DUE-DTE", FieldType.STRING, 
            8);
        ncw_Master_Crrnt_Prty_Cde = ncw_Master_Crrnt_Due_Dte_Prty_TmeRedef7.newFieldInGroup("ncw_Master_Crrnt_Prty_Cde", "CRRNT-PRTY-CDE", FieldType.STRING, 
            1);
        ncw_Master_Crrnt_Due_Tme = ncw_Master_Crrnt_Due_Dte_Prty_TmeRedef7.newFieldInGroup("ncw_Master_Crrnt_Due_Tme", "CRRNT-DUE-TME", FieldType.STRING, 
            7);
        ncw_Master_Cntct_Sheet_Print_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Cntct_Sheet_Print_Cde", "CNTCT-SHEET-PRINT-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_CDE");
        ncw_Master_Cntct_Sheet_Print_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Cntct_Sheet_Print_Dte_Tme", "CNTCT-SHEET-PRINT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_DTE_TME");
        ncw_Master_Cntct_Sheet_Printer_Id_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Cntct_Sheet_Printer_Id_Cde", "CNTCT-SHEET-PRINTER-ID-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINTER_ID_CDE");
        ncw_Master_Cntct_Invrt_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Cntct_Invrt_Dte_Tme", "CNTCT-INVRT-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "CNTCT_INVRT_DTE_TME");
        ncw_Master_Cnnctd_Rqst_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Cnnctd_Rqst_Ind", "CNNCTD-RQST-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNNCTD_RQST_IND");
        ncw_Master_Cnnctd_Rqst_Ind.setDdmHeader("CONNECTED REQUEST INDICATOR");
        ncw_Master_Crrnt_Cmt_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Crrnt_Cmt_Ind", "CRRNT-CMT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRRNT_CMT_IND");
        ncw_Master_Crrnt_Cmt_Ind.setDdmHeader("COMMITMENT INDICATOR");
        ncw_Master_Prvte_Wrk_Rqst_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Prvte_Wrk_Rqst_Ind", "PRVTE-WRK-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRVTE_WRK_RQST_IND");
        ncw_Master_Prvte_Wrk_Rqst_Ind.setDdmHeader("PRIVATE WORK REQUEST INDICATOR");
        ncw_Master_Instn_Rqst_Log_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Instn_Rqst_Log_Dte_Tme", "INSTN-RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "INSTN_RQST_LOG_DTE_TME");
        ncw_Master_Instn_Rqst_Log_Dte_Tme.setDdmHeader("GUARDIAN INST RQST LOG DTE/TME");
        ncw_Master_Log_Sqnce_Nbr = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Log_Sqnce_Nbr", "LOG-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "LOG_SQNCE_NBR");
        ncw_Master_Log_Sqnce_Nbr.setDdmHeader("LOG SEQUENCE NUMBER");
        ncw_Master_Rescan_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RESCAN_IND");
        ncw_Master_Step_Re_Do_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Step_Re_Do_Ind", "STEP-RE-DO-IND", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "STEP_RE_DO_IND");
        ncw_Master_Due_Dte_Chg_Prty_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Due_Dte_Chg_Prty_Cde", "DUE-DTE-CHG-PRTY-CDE", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        ncw_Master_Effctve_Dte = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "EFFCTVE_DTE");
        ncw_Master_Extrnl_Pend_Rcv_Dte = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        ncw_Master_Trade_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Trade_Dte_Tme", "TRADE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRADE_DTE_TME");
        ncw_Master_Owner_Unit_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Owner_Unit_Cde", "OWNER-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "OWNER_UNIT_CDE");
        ncw_Master_Assgn_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Assgn_Dte_Tme", "ASSGN-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ASSGN_DTE_TME");
        ncw_Master_Intrnl_Pnd_Start_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Intrnl_Pnd_Start_Dte_Tme", "INTRNL-PND-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_START_DTE_TME");
        ncw_Master_Intrnl_Pnd_Start_Dte_TmeRedef8 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Intrnl_Pnd_Start_Dte_TmeRedef8", "Redefines", 
            ncw_Master_Intrnl_Pnd_Start_Dte_Tme);
        ncw_Master_Intrnl_Pnd_Start_Dte = ncw_Master_Intrnl_Pnd_Start_Dte_TmeRedef8.newFieldInGroup("ncw_Master_Intrnl_Pnd_Start_Dte", "INTRNL-PND-START-DTE", 
            FieldType.STRING, 8);
        ncw_Master_Intrnl_Pnd_Start_Tme = ncw_Master_Intrnl_Pnd_Start_Dte_TmeRedef8.newFieldInGroup("ncw_Master_Intrnl_Pnd_Start_Tme", "INTRNL-PND-START-TME", 
            FieldType.STRING, 7);
        ncw_Master_Intrnl_Pnd_End_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Intrnl_Pnd_End_Dte_Tme", "INTRNL-PND-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "INTRNL_PND_END_DTE_TME");
        ncw_Master_Intrnl_Pnd_End_Dte_TmeRedef9 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Intrnl_Pnd_End_Dte_TmeRedef9", "Redefines", ncw_Master_Intrnl_Pnd_End_Dte_Tme);
        ncw_Master_Intrnl_Pnd_End_Dte = ncw_Master_Intrnl_Pnd_End_Dte_TmeRedef9.newFieldInGroup("ncw_Master_Intrnl_Pnd_End_Dte", "INTRNL-PND-END-DTE", 
            FieldType.STRING, 8);
        ncw_Master_Intrnl_Pnd_End_Tme = ncw_Master_Intrnl_Pnd_End_Dte_TmeRedef9.newFieldInGroup("ncw_Master_Intrnl_Pnd_End_Tme", "INTRNL-PND-END-TME", 
            FieldType.STRING, 7);
        ncw_Master_Intrnl_Pnd_Days = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Intrnl_Pnd_Days", "INTRNL-PND-DAYS", FieldType.PACKED_DECIMAL, 
            5, RepeatingFieldStrategy.None, "INTRNL_PND_DAYS");
        ncw_Master_Unit_Start_Status = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Unit_Start_Status", "UNIT-START-STATUS", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "UNIT_START_STATUS");
        ncw_Master_Empl_Start_Status = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Empl_Start_Status", "EMPL-START-STATUS", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "EMPL_START_STATUS");
        ncw_Master_Step_Start_Status = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Step_Start_Status", "STEP-START-STATUS", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STEP_START_STATUS");
        ncw_Master_Activity_End_Status = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Activity_End_Status", "ACTIVITY-END-STATUS", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ACTIVITY_END_STATUS");
        ncw_Master_Unit_On_Tme_Ind = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Unit_On_Tme_Ind", "UNIT-ON-TME-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "UNIT_ON_TME_IND");
        ncw_Master_Crprte_Clock_End_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        ncw_Master_Crprte_Clock_End_Dte_TmeRedef10 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Crprte_Clock_End_Dte_TmeRedef10", "Redefines", 
            ncw_Master_Crprte_Clock_End_Dte_Tme);
        ncw_Master_Crprte_Clock_End_Dte = ncw_Master_Crprte_Clock_End_Dte_TmeRedef10.newFieldInGroup("ncw_Master_Crprte_Clock_End_Dte", "CRPRTE-CLOCK-END-DTE", 
            FieldType.STRING, 8);
        ncw_Master_Crprte_Clock_End_Tme = ncw_Master_Crprte_Clock_End_Dte_TmeRedef10.newFieldInGroup("ncw_Master_Crprte_Clock_End_Tme", "CRPRTE-CLOCK-END-TME", 
            FieldType.STRING, 7);
        ncw_Master_Crprte_Close_Oprtr_Cde = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Crprte_Close_Oprtr_Cde", "CRPRTE-CLOSE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRPRTE_CLOSE_OPRTR_CDE");
        ncw_Master_Status_Clock_Start_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Status_Clock_Start_Dte_Tme", "STATUS-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STATUS_CLOCK_START_DTE_TME");
        ncw_Master_Status_Clock_End_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Status_Clock_End_Dte_Tme", "STATUS-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STATUS_CLOCK_END_DTE_TME");
        ncw_Master_Status_Elpsd_Clndr_Days_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Status_Elpsd_Clndr_Days_Tme", "STATUS-ELPSD-CLNDR-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "STATUS_ELPSD_CLNDR_DAYS_TME");
        ncw_Master_Status_Elpsd_Clndr_Days_TmeRedef11 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Status_Elpsd_Clndr_Days_TmeRedef11", "Redefines", 
            ncw_Master_Status_Elpsd_Clndr_Days_Tme);
        ncw_Master_Status_Elpsd_Clndr_Days = ncw_Master_Status_Elpsd_Clndr_Days_TmeRedef11.newFieldInGroup("ncw_Master_Status_Elpsd_Clndr_Days", "STATUS-ELPSD-CLNDR-DAYS", 
            FieldType.NUMERIC, 3);
        ncw_Master_Status_Elpsd_Clndr_Hours = ncw_Master_Status_Elpsd_Clndr_Days_TmeRedef11.newFieldInGroup("ncw_Master_Status_Elpsd_Clndr_Hours", "STATUS-ELPSD-CLNDR-HOURS", 
            FieldType.NUMERIC, 2);
        ncw_Master_Status_Elpsd_Clndr_Minutes = ncw_Master_Status_Elpsd_Clndr_Days_TmeRedef11.newFieldInGroup("ncw_Master_Status_Elpsd_Clndr_Minutes", 
            "STATUS-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        ncw_Master_Status_Elpsd_Bsnss_Days_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Status_Elpsd_Bsnss_Days_Tme", "STATUS-ELPSD-BSNSS-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "STATUS_ELPSD_BSNSS_DAYS_TME");
        ncw_Master_Status_Elpsd_Bsnss_Days_TmeRedef12 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Status_Elpsd_Bsnss_Days_TmeRedef12", "Redefines", 
            ncw_Master_Status_Elpsd_Bsnss_Days_Tme);
        ncw_Master_Status_Elpsd_Bsnss_Days = ncw_Master_Status_Elpsd_Bsnss_Days_TmeRedef12.newFieldInGroup("ncw_Master_Status_Elpsd_Bsnss_Days", "STATUS-ELPSD-BSNSS-DAYS", 
            FieldType.NUMERIC, 3);
        ncw_Master_Status_Elpsd_Bsnss_Hours = ncw_Master_Status_Elpsd_Bsnss_Days_TmeRedef12.newFieldInGroup("ncw_Master_Status_Elpsd_Bsnss_Hours", "STATUS-ELPSD-BSNSS-HOURS", 
            FieldType.NUMERIC, 2);
        ncw_Master_Status_Elpsd_Bsnss_Minutes = ncw_Master_Status_Elpsd_Bsnss_Days_TmeRedef12.newFieldInGroup("ncw_Master_Status_Elpsd_Bsnss_Minutes", 
            "STATUS-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        ncw_Master_Step_Clock_Start_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Step_Clock_Start_Dte_Tme", "STEP-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        ncw_Master_Step_Clock_End_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Step_Clock_End_Dte_Tme", "STEP-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        ncw_Master_Step_Elpsd_Clndr_Days_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Step_Elpsd_Clndr_Days_Tme", "STEP-ELPSD-CLNDR-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "STEP_ELPSD_CLNDR_DAYS_TME");
        ncw_Master_Step_Elpsd_Clndr_Days_TmeRedef13 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Step_Elpsd_Clndr_Days_TmeRedef13", "Redefines", 
            ncw_Master_Step_Elpsd_Clndr_Days_Tme);
        ncw_Master_Step_Elpsd_Clndr_Days = ncw_Master_Step_Elpsd_Clndr_Days_TmeRedef13.newFieldInGroup("ncw_Master_Step_Elpsd_Clndr_Days", "STEP-ELPSD-CLNDR-DAYS", 
            FieldType.NUMERIC, 3);
        ncw_Master_Step_Elpsd_Clndr_Hours = ncw_Master_Step_Elpsd_Clndr_Days_TmeRedef13.newFieldInGroup("ncw_Master_Step_Elpsd_Clndr_Hours", "STEP-ELPSD-CLNDR-HOURS", 
            FieldType.NUMERIC, 2);
        ncw_Master_Step_Elpsd_Clndr_Minutes = ncw_Master_Step_Elpsd_Clndr_Days_TmeRedef13.newFieldInGroup("ncw_Master_Step_Elpsd_Clndr_Minutes", "STEP-ELPSD-CLNDR-MINUTES", 
            FieldType.NUMERIC, 2);
        ncw_Master_Step_Elpsd_Bsnss_Days_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Step_Elpsd_Bsnss_Days_Tme", "STEP-ELPSD-BSNSS-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "STEP_ELPSD_BSNSS_DAYS_TME");
        ncw_Master_Step_Elpsd_Bsnss_Days_TmeRedef14 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Step_Elpsd_Bsnss_Days_TmeRedef14", "Redefines", 
            ncw_Master_Step_Elpsd_Bsnss_Days_Tme);
        ncw_Master_Step_Elpsd_Bsnss_Days = ncw_Master_Step_Elpsd_Bsnss_Days_TmeRedef14.newFieldInGroup("ncw_Master_Step_Elpsd_Bsnss_Days", "STEP-ELPSD-BSNSS-DAYS", 
            FieldType.NUMERIC, 3);
        ncw_Master_Step_Elpsd_Bsnss_Hours = ncw_Master_Step_Elpsd_Bsnss_Days_TmeRedef14.newFieldInGroup("ncw_Master_Step_Elpsd_Bsnss_Hours", "STEP-ELPSD-BSNSS-HOURS", 
            FieldType.NUMERIC, 2);
        ncw_Master_Step_Elpsd_Bsnss_Minutes = ncw_Master_Step_Elpsd_Bsnss_Days_TmeRedef14.newFieldInGroup("ncw_Master_Step_Elpsd_Bsnss_Minutes", "STEP-ELPSD-BSNSS-MINUTES", 
            FieldType.NUMERIC, 2);
        ncw_Master_Empl_Clock_Start_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Empl_Clock_Start_Dte_Tme", "EMPL-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        ncw_Master_Empl_Clock_End_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Empl_Clock_End_Dte_Tme", "EMPL-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        ncw_Master_Empl_Elpsd_Clndr_Days_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Empl_Elpsd_Clndr_Days_Tme", "EMPL-ELPSD-CLNDR-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "EMPL_ELPSD_CLNDR_DAYS_TME");
        ncw_Master_Empl_Elpsd_Clndr_Days_TmeRedef15 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Empl_Elpsd_Clndr_Days_TmeRedef15", "Redefines", 
            ncw_Master_Empl_Elpsd_Clndr_Days_Tme);
        ncw_Master_Empl_Elpsd_Clndr_Days = ncw_Master_Empl_Elpsd_Clndr_Days_TmeRedef15.newFieldInGroup("ncw_Master_Empl_Elpsd_Clndr_Days", "EMPL-ELPSD-CLNDR-DAYS", 
            FieldType.NUMERIC, 3);
        ncw_Master_Empl_Elpsd_Clndr_Hours = ncw_Master_Empl_Elpsd_Clndr_Days_TmeRedef15.newFieldInGroup("ncw_Master_Empl_Elpsd_Clndr_Hours", "EMPL-ELPSD-CLNDR-HOURS", 
            FieldType.NUMERIC, 2);
        ncw_Master_Empl_Elpsd_Clndr_Minutes = ncw_Master_Empl_Elpsd_Clndr_Days_TmeRedef15.newFieldInGroup("ncw_Master_Empl_Elpsd_Clndr_Minutes", "EMPL-ELPSD-CLNDR-MINUTES", 
            FieldType.NUMERIC, 2);
        ncw_Master_Empl_Elpsd_Bsnss_Days_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Empl_Elpsd_Bsnss_Days_Tme", "EMPL-ELPSD-BSNSS-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "EMPL_ELPSD_BSNSS_DAYS_TME");
        ncw_Master_Empl_Elpsd_Bsnss_Days_TmeRedef16 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Empl_Elpsd_Bsnss_Days_TmeRedef16", "Redefines", 
            ncw_Master_Empl_Elpsd_Bsnss_Days_Tme);
        ncw_Master_Empl_Elpsd_Bsnss_Days = ncw_Master_Empl_Elpsd_Bsnss_Days_TmeRedef16.newFieldInGroup("ncw_Master_Empl_Elpsd_Bsnss_Days", "EMPL-ELPSD-BSNSS-DAYS", 
            FieldType.NUMERIC, 3);
        ncw_Master_Empl_Elpsd_Bsnss_Hours = ncw_Master_Empl_Elpsd_Bsnss_Days_TmeRedef16.newFieldInGroup("ncw_Master_Empl_Elpsd_Bsnss_Hours", "EMPL-ELPSD-BSNSS-HOURS", 
            FieldType.NUMERIC, 2);
        ncw_Master_Empl_Elpsd_Bsnss_Minutes = ncw_Master_Empl_Elpsd_Bsnss_Days_TmeRedef16.newFieldInGroup("ncw_Master_Empl_Elpsd_Bsnss_Minutes", "EMPL-ELPSD-BSNSS-MINUTES", 
            FieldType.NUMERIC, 2);
        ncw_Master_Unit_Clock_Start_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Unit_Clock_Start_Dte_Tme", "UNIT-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        ncw_Master_Unit_Clock_End_Dte_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Unit_Clock_End_Dte_Tme", "UNIT-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        ncw_Master_Unit_Elpsd_Clndr_Days_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Unit_Elpsd_Clndr_Days_Tme", "UNIT-ELPSD-CLNDR-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "UNIT_ELPSD_CLNDR_DAYS_TME");
        ncw_Master_Unit_Elpsd_Clndr_Days_TmeRedef17 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Unit_Elpsd_Clndr_Days_TmeRedef17", "Redefines", 
            ncw_Master_Unit_Elpsd_Clndr_Days_Tme);
        ncw_Master_Unit_Elpsd_Clndr_Days = ncw_Master_Unit_Elpsd_Clndr_Days_TmeRedef17.newFieldInGroup("ncw_Master_Unit_Elpsd_Clndr_Days", "UNIT-ELPSD-CLNDR-DAYS", 
            FieldType.NUMERIC, 3);
        ncw_Master_Unit_Elpsd_Clndr_Hours = ncw_Master_Unit_Elpsd_Clndr_Days_TmeRedef17.newFieldInGroup("ncw_Master_Unit_Elpsd_Clndr_Hours", "UNIT-ELPSD-CLNDR-HOURS", 
            FieldType.NUMERIC, 2);
        ncw_Master_Unit_Elpsd_Clndr_Minutes = ncw_Master_Unit_Elpsd_Clndr_Days_TmeRedef17.newFieldInGroup("ncw_Master_Unit_Elpsd_Clndr_Minutes", "UNIT-ELPSD-CLNDR-MINUTES", 
            FieldType.NUMERIC, 2);
        ncw_Master_Unit_Elpsd_Bsnss_Days_Tme = vw_ncw_Master.getRecord().newFieldInGroup("ncw_Master_Unit_Elpsd_Bsnss_Days_Tme", "UNIT-ELPSD-BSNSS-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "UNIT_ELPSD_BSNSS_DAYS_TME");
        ncw_Master_Unit_Elpsd_Bsnss_Days_TmeRedef18 = vw_ncw_Master.getRecord().newGroupInGroup("ncw_Master_Unit_Elpsd_Bsnss_Days_TmeRedef18", "Redefines", 
            ncw_Master_Unit_Elpsd_Bsnss_Days_Tme);
        ncw_Master_Unit_Elpsd_Bsnss_Days = ncw_Master_Unit_Elpsd_Bsnss_Days_TmeRedef18.newFieldInGroup("ncw_Master_Unit_Elpsd_Bsnss_Days", "UNIT-ELPSD-BSNSS-DAYS", 
            FieldType.NUMERIC, 3);
        ncw_Master_Unit_Elpsd_Bsnss_Hours = ncw_Master_Unit_Elpsd_Bsnss_Days_TmeRedef18.newFieldInGroup("ncw_Master_Unit_Elpsd_Bsnss_Hours", "UNIT-ELPSD-BSNSS-HOURS", 
            FieldType.NUMERIC, 2);
        ncw_Master_Unit_Elpsd_Bsnss_Minutes = ncw_Master_Unit_Elpsd_Bsnss_Days_TmeRedef18.newFieldInGroup("ncw_Master_Unit_Elpsd_Bsnss_Minutes", "UNIT-ELPSD-BSNSS-MINUTES", 
            FieldType.NUMERIC, 2);

        this.setRecordName("LdaCwfl4827");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_ncw_Master.reset();
    }

    // Constructor
    public LdaCwfl4827() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
