/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:47 PM
**        * FROM NATURAL LDA     : CWFL4821
************************************************************
**        * FILE NAME            : LdaCwfl4821.java
**        * CLASS NAME           : LdaCwfl4821
**        * INSTANCE NAME        : LdaCwfl4821
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4821 extends DbsRecord
{
    // Properties
    private DbsGroup contract;
    private DbsField contract_Rqst_Log_Dte_Tme;
    private DbsField contract_Pnd_A;
    private DbsField contract_Cntrct_Seq_Num;
    private DbsField contract_Pnd_B;
    private DbsField contract_Cntrct_Num;

    public DbsGroup getContract() { return contract; }

    public DbsField getContract_Rqst_Log_Dte_Tme() { return contract_Rqst_Log_Dte_Tme; }

    public DbsField getContract_Pnd_A() { return contract_Pnd_A; }

    public DbsField getContract_Cntrct_Seq_Num() { return contract_Cntrct_Seq_Num; }

    public DbsField getContract_Pnd_B() { return contract_Pnd_B; }

    public DbsField getContract_Cntrct_Num() { return contract_Cntrct_Num; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        contract = newGroupInRecord("contract", "CONTRACT");
        contract_Rqst_Log_Dte_Tme = contract.newFieldInGroup("contract_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 28);
        contract_Pnd_A = contract.newFieldInGroup("contract_Pnd_A", "#A", FieldType.STRING, 1);
        contract_Cntrct_Seq_Num = contract.newFieldInGroup("contract_Cntrct_Seq_Num", "CNTRCT-SEQ-NUM", FieldType.NUMERIC, 2);
        contract_Pnd_B = contract.newFieldInGroup("contract_Pnd_B", "#B", FieldType.STRING, 1);
        contract_Cntrct_Num = contract.newFieldInGroup("contract_Cntrct_Num", "CNTRCT-NUM", FieldType.STRING, 8);

        this.setRecordName("LdaCwfl4821");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4821() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
