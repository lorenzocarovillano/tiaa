/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:57 PM
**        * FROM NATURAL PDA     : CWFA3300
************************************************************
**        * FILE NAME            : PdaCwfa3300.java
**        * CLASS NAME           : PdaCwfa3300
**        * INSTANCE NAME        : PdaCwfa3300
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCwfa3300 extends PdaBase
{
    // Properties
    private DbsField pnd_Parm_Data;
    private DbsGroup pnd_Parm_DataRedef1;
    private DbsField pnd_Parm_Data_Pnd_Rqst_Count;
    private DbsField pnd_Parm_Data_Pnd_Rprt_Count;
    private DbsField pnd_Parm_Data_Pnd_Extrct_Count;
    private DbsField pnd_Parm_Data_Pnd_Rqst_Unit;
    private DbsField pnd_Parm_Data_Pnd_Key_Field;

    public DbsField getPnd_Parm_Data() { return pnd_Parm_Data; }

    public DbsGroup getPnd_Parm_DataRedef1() { return pnd_Parm_DataRedef1; }

    public DbsField getPnd_Parm_Data_Pnd_Rqst_Count() { return pnd_Parm_Data_Pnd_Rqst_Count; }

    public DbsField getPnd_Parm_Data_Pnd_Rprt_Count() { return pnd_Parm_Data_Pnd_Rprt_Count; }

    public DbsField getPnd_Parm_Data_Pnd_Extrct_Count() { return pnd_Parm_Data_Pnd_Extrct_Count; }

    public DbsField getPnd_Parm_Data_Pnd_Rqst_Unit() { return pnd_Parm_Data_Pnd_Rqst_Unit; }

    public DbsField getPnd_Parm_Data_Pnd_Key_Field() { return pnd_Parm_Data_Pnd_Key_Field; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Parm_Data = dbsRecord.newFieldInRecord("pnd_Parm_Data", "#PARM-DATA", FieldType.STRING, 53);
        pnd_Parm_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_DataRedef1 = dbsRecord.newGroupInRecord("pnd_Parm_DataRedef1", "Redefines", pnd_Parm_Data);
        pnd_Parm_Data_Pnd_Rqst_Count = pnd_Parm_DataRedef1.newFieldInGroup("pnd_Parm_Data_Pnd_Rqst_Count", "#RQST-COUNT", FieldType.NUMERIC, 5);
        pnd_Parm_Data_Pnd_Rprt_Count = pnd_Parm_DataRedef1.newFieldInGroup("pnd_Parm_Data_Pnd_Rprt_Count", "#RPRT-COUNT", FieldType.NUMERIC, 5);
        pnd_Parm_Data_Pnd_Extrct_Count = pnd_Parm_DataRedef1.newFieldInGroup("pnd_Parm_Data_Pnd_Extrct_Count", "#EXTRCT-COUNT", FieldType.NUMERIC, 5);
        pnd_Parm_Data_Pnd_Rqst_Unit = pnd_Parm_DataRedef1.newFieldInGroup("pnd_Parm_Data_Pnd_Rqst_Unit", "#RQST-UNIT", FieldType.STRING, 8);
        pnd_Parm_Data_Pnd_Key_Field = pnd_Parm_DataRedef1.newFieldInGroup("pnd_Parm_Data_Pnd_Key_Field", "#KEY-FIELD", FieldType.STRING, 30);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCwfa3300(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

