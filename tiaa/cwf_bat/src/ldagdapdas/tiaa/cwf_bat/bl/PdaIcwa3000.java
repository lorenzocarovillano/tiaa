/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:21:01 PM
**        * FROM NATURAL PDA     : ICWA3000
************************************************************
**        * FILE NAME            : PdaIcwa3000.java
**        * CLASS NAME           : PdaIcwa3000
**        * INSTANCE NAME        : PdaIcwa3000
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIcwa3000 extends PdaBase
{
    // Properties
    private DbsGroup icwa3000_Input;
    private DbsField icwa3000_Input_Job_Name;
    private DbsField icwa3000_Input_Program_Name;
    private DbsGroup icwa3000_Output;
    private DbsField icwa3000_Output_Run_Control_Isn;
    private DbsField icwa3000_Output_Restarting_Flag;
    private DbsField icwa3000_Output_Active_Program_Name;

    public DbsGroup getIcwa3000_Input() { return icwa3000_Input; }

    public DbsField getIcwa3000_Input_Job_Name() { return icwa3000_Input_Job_Name; }

    public DbsField getIcwa3000_Input_Program_Name() { return icwa3000_Input_Program_Name; }

    public DbsGroup getIcwa3000_Output() { return icwa3000_Output; }

    public DbsField getIcwa3000_Output_Run_Control_Isn() { return icwa3000_Output_Run_Control_Isn; }

    public DbsField getIcwa3000_Output_Restarting_Flag() { return icwa3000_Output_Restarting_Flag; }

    public DbsField getIcwa3000_Output_Active_Program_Name() { return icwa3000_Output_Active_Program_Name; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        icwa3000_Input = dbsRecord.newGroupInRecord("icwa3000_Input", "ICWA3000-INPUT");
        icwa3000_Input.setParameterOption(ParameterOption.ByReference);
        icwa3000_Input_Job_Name = icwa3000_Input.newFieldInGroup("icwa3000_Input_Job_Name", "JOB-NAME", FieldType.STRING, 8);
        icwa3000_Input_Program_Name = icwa3000_Input.newFieldInGroup("icwa3000_Input_Program_Name", "PROGRAM-NAME", FieldType.STRING, 8);

        icwa3000_Output = dbsRecord.newGroupInRecord("icwa3000_Output", "ICWA3000-OUTPUT");
        icwa3000_Output.setParameterOption(ParameterOption.ByReference);
        icwa3000_Output_Run_Control_Isn = icwa3000_Output.newFieldInGroup("icwa3000_Output_Run_Control_Isn", "RUN-CONTROL-ISN", FieldType.PACKED_DECIMAL, 
            10);
        icwa3000_Output_Restarting_Flag = icwa3000_Output.newFieldInGroup("icwa3000_Output_Restarting_Flag", "RESTARTING-FLAG", FieldType.BOOLEAN);
        icwa3000_Output_Active_Program_Name = icwa3000_Output.newFieldInGroup("icwa3000_Output_Active_Program_Name", "ACTIVE-PROGRAM-NAME", FieldType.STRING, 
            8);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIcwa3000(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

