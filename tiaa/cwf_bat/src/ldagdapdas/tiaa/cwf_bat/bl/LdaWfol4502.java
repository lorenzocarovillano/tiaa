/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:16:43 PM
**        * FROM NATURAL LDA     : WFOL4502
************************************************************
**        * FILE NAME            : LdaWfol4502.java
**        * CLASS NAME           : LdaWfol4502
**        * INSTANCE NAME        : LdaWfol4502
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaWfol4502 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cmcv3;
    private DbsField cmcv3_Rcrd_Type_Cde;
    private DbsField cmcv3_Container_Id;
    private DbsField cmcv3_Error_Msg_Txt;
    private DbsGroup cmcv3_Tracking_Rcrd_NbrMuGroup;
    private DbsField cmcv3_Tracking_Rcrd_Nbr;
    private DbsField cmcv3_Wfo_Ind;

    public DataAccessProgramView getVw_cmcv3() { return vw_cmcv3; }

    public DbsField getCmcv3_Rcrd_Type_Cde() { return cmcv3_Rcrd_Type_Cde; }

    public DbsField getCmcv3_Container_Id() { return cmcv3_Container_Id; }

    public DbsField getCmcv3_Error_Msg_Txt() { return cmcv3_Error_Msg_Txt; }

    public DbsGroup getCmcv3_Tracking_Rcrd_NbrMuGroup() { return cmcv3_Tracking_Rcrd_NbrMuGroup; }

    public DbsField getCmcv3_Tracking_Rcrd_Nbr() { return cmcv3_Tracking_Rcrd_Nbr; }

    public DbsField getCmcv3_Wfo_Ind() { return cmcv3_Wfo_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cmcv3 = new DataAccessProgramView(new NameInfo("vw_cmcv3", "CMCV3"), "CWF_MCSS_CALLS", "CWF_MCSS_CALLS");
        cmcv3_Rcrd_Type_Cde = vw_cmcv3.getRecord().newFieldInGroup("cmcv3_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE_CDE");
        cmcv3_Rcrd_Type_Cde.setDdmHeader("RECORD/TYPE");
        cmcv3_Container_Id = vw_cmcv3.getRecord().newFieldInGroup("cmcv3_Container_Id", "CONTAINER-ID", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "CONTAINER_ID");
        cmcv3_Error_Msg_Txt = vw_cmcv3.getRecord().newFieldInGroup("cmcv3_Error_Msg_Txt", "ERROR-MSG-TXT", FieldType.STRING, 60, RepeatingFieldStrategy.None, 
            "ERROR_MSG_TXT");
        cmcv3_Error_Msg_Txt.setDdmHeader("ERROR/MSG.");
        cmcv3_Tracking_Rcrd_NbrMuGroup = vw_cmcv3.getRecord().newGroupInGroup("cmcv3_Tracking_Rcrd_NbrMuGroup", "TRACKING_RCRD_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CWF_MCSS_CALLS_TRACKING_RCRD_NBR");
        cmcv3_Tracking_Rcrd_Nbr = cmcv3_Tracking_Rcrd_NbrMuGroup.newFieldArrayInGroup("cmcv3_Tracking_Rcrd_Nbr", "TRACKING-RCRD-NBR", FieldType.STRING, 
            20, new DbsArrayController(1,10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TRACKING_RCRD_NBR");
        cmcv3_Wfo_Ind = vw_cmcv3.getRecord().newFieldInGroup("cmcv3_Wfo_Ind", "WFO-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "WFO_IND");

        this.setRecordName("LdaWfol4502");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cmcv3.reset();
    }

    // Constructor
    public LdaWfol4502() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
