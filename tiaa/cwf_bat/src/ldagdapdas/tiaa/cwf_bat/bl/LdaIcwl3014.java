/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:05:12 PM
**        * FROM NATURAL LDA     : ICWL3014
************************************************************
**        * FILE NAME            : LdaIcwl3014.java
**        * CLASS NAME           : LdaIcwl3014
**        * INSTANCE NAME        : LdaIcwl3014
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIcwl3014 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iis_Prem_Pymnt_Grp_View;
    private DbsGroup iis_Prem_Pymnt_Grp_View_Audit_Grp;
    private DbsField iis_Prem_Pymnt_Grp_View_Audit_Mdfy_By;
    private DbsField iis_Prem_Pymnt_Grp_View_Audit_Last_Mdfy_Dte;
    private DbsField iis_Prem_Pymnt_Grp_View_Ppg_Cde;
    private DbsField iis_Prem_Pymnt_Grp_View_Ppg_Nme;
    private DbsField iis_Prem_Pymnt_Grp_View_Orgnztn_Hrchy_Key_Cde;
    private DbsField iis_Prem_Pymnt_Grp_View_Orgnztn_Hrchy_Link_Instn_Cde;
    private DbsField iis_Prem_Pymnt_Grp_View_Rmttr_Type_Cde;
    private DbsField iis_Prem_Pymnt_Grp_View_Remit_Locn_Cde;
    private DbsField iis_Prem_Pymnt_Grp_View_Prem_Process_Cde;
    private DbsField iis_Prem_Pymnt_Grp_View_Prem_Grp_Cde;

    public DataAccessProgramView getVw_iis_Prem_Pymnt_Grp_View() { return vw_iis_Prem_Pymnt_Grp_View; }

    public DbsGroup getIis_Prem_Pymnt_Grp_View_Audit_Grp() { return iis_Prem_Pymnt_Grp_View_Audit_Grp; }

    public DbsField getIis_Prem_Pymnt_Grp_View_Audit_Mdfy_By() { return iis_Prem_Pymnt_Grp_View_Audit_Mdfy_By; }

    public DbsField getIis_Prem_Pymnt_Grp_View_Audit_Last_Mdfy_Dte() { return iis_Prem_Pymnt_Grp_View_Audit_Last_Mdfy_Dte; }

    public DbsField getIis_Prem_Pymnt_Grp_View_Ppg_Cde() { return iis_Prem_Pymnt_Grp_View_Ppg_Cde; }

    public DbsField getIis_Prem_Pymnt_Grp_View_Ppg_Nme() { return iis_Prem_Pymnt_Grp_View_Ppg_Nme; }

    public DbsField getIis_Prem_Pymnt_Grp_View_Orgnztn_Hrchy_Key_Cde() { return iis_Prem_Pymnt_Grp_View_Orgnztn_Hrchy_Key_Cde; }

    public DbsField getIis_Prem_Pymnt_Grp_View_Orgnztn_Hrchy_Link_Instn_Cde() { return iis_Prem_Pymnt_Grp_View_Orgnztn_Hrchy_Link_Instn_Cde; }

    public DbsField getIis_Prem_Pymnt_Grp_View_Rmttr_Type_Cde() { return iis_Prem_Pymnt_Grp_View_Rmttr_Type_Cde; }

    public DbsField getIis_Prem_Pymnt_Grp_View_Remit_Locn_Cde() { return iis_Prem_Pymnt_Grp_View_Remit_Locn_Cde; }

    public DbsField getIis_Prem_Pymnt_Grp_View_Prem_Process_Cde() { return iis_Prem_Pymnt_Grp_View_Prem_Process_Cde; }

    public DbsField getIis_Prem_Pymnt_Grp_View_Prem_Grp_Cde() { return iis_Prem_Pymnt_Grp_View_Prem_Grp_Cde; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iis_Prem_Pymnt_Grp_View = new DataAccessProgramView(new NameInfo("vw_iis_Prem_Pymnt_Grp_View", "IIS-PREM-PYMNT-GRP-VIEW"), "IIS_PREM_PYMNT_GRP", 
            "IIS_PPG_PPGP_GRP");
        iis_Prem_Pymnt_Grp_View_Audit_Grp = vw_iis_Prem_Pymnt_Grp_View.getRecord().newGroupInGroup("iis_Prem_Pymnt_Grp_View_Audit_Grp", "AUDIT-GRP");
        iis_Prem_Pymnt_Grp_View_Audit_Mdfy_By = iis_Prem_Pymnt_Grp_View_Audit_Grp.newFieldInGroup("iis_Prem_Pymnt_Grp_View_Audit_Mdfy_By", "AUDIT-MDFY-BY", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AUDIT_MDFY_BY");
        iis_Prem_Pymnt_Grp_View_Audit_Mdfy_By.setDdmHeader("MODIFIED/BY");
        iis_Prem_Pymnt_Grp_View_Audit_Last_Mdfy_Dte = iis_Prem_Pymnt_Grp_View_Audit_Grp.newFieldInGroup("iis_Prem_Pymnt_Grp_View_Audit_Last_Mdfy_Dte", 
            "AUDIT-LAST-MDFY-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "AUDIT_LAST_MDFY_DTE");
        iis_Prem_Pymnt_Grp_View_Audit_Last_Mdfy_Dte.setDdmHeader("LAST MODIFIED");
        iis_Prem_Pymnt_Grp_View_Ppg_Cde = vw_iis_Prem_Pymnt_Grp_View.getRecord().newFieldInGroup("iis_Prem_Pymnt_Grp_View_Ppg_Cde", "PPG-CDE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "PPG_CDE");
        iis_Prem_Pymnt_Grp_View_Ppg_Cde.setDdmHeader("PPG CODE");
        iis_Prem_Pymnt_Grp_View_Ppg_Nme = vw_iis_Prem_Pymnt_Grp_View.getRecord().newFieldInGroup("iis_Prem_Pymnt_Grp_View_Ppg_Nme", "PPG-NME", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "PPG_NME");
        iis_Prem_Pymnt_Grp_View_Ppg_Nme.setDdmHeader("PPG NAME");
        iis_Prem_Pymnt_Grp_View_Orgnztn_Hrchy_Key_Cde = vw_iis_Prem_Pymnt_Grp_View.getRecord().newFieldInGroup("iis_Prem_Pymnt_Grp_View_Orgnztn_Hrchy_Key_Cde", 
            "ORGNZTN-HRCHY-KEY-CDE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ORGNZTN_HRCHY_KEY_CDE");
        iis_Prem_Pymnt_Grp_View_Orgnztn_Hrchy_Key_Cde.setDdmHeader("HIERARCHY/KEY CODE");
        iis_Prem_Pymnt_Grp_View_Orgnztn_Hrchy_Link_Instn_Cde = vw_iis_Prem_Pymnt_Grp_View.getRecord().newFieldInGroup("iis_Prem_Pymnt_Grp_View_Orgnztn_Hrchy_Link_Instn_Cde", 
            "ORGNZTN-HRCHY-LINK-INSTN-CDE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ORGNZTN_HRCHY_LINK_INSTN_CDE");
        iis_Prem_Pymnt_Grp_View_Orgnztn_Hrchy_Link_Instn_Cde.setDdmHeader("HIERARCHY/KEY CODE/(INSTITUTION)");
        iis_Prem_Pymnt_Grp_View_Rmttr_Type_Cde = vw_iis_Prem_Pymnt_Grp_View.getRecord().newFieldInGroup("iis_Prem_Pymnt_Grp_View_Rmttr_Type_Cde", "RMTTR-TYPE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "RMTTR_TYPE_CDE");
        iis_Prem_Pymnt_Grp_View_Rmttr_Type_Cde.setDdmHeader("TYPE OF/REMITTER");
        iis_Prem_Pymnt_Grp_View_Remit_Locn_Cde = vw_iis_Prem_Pymnt_Grp_View.getRecord().newFieldInGroup("iis_Prem_Pymnt_Grp_View_Remit_Locn_Cde", "REMIT-LOCN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "REMIT_LOCN_CDE");
        iis_Prem_Pymnt_Grp_View_Remit_Locn_Cde.setDdmHeader("REMITTANCE/LOCATION/CODE");
        iis_Prem_Pymnt_Grp_View_Prem_Process_Cde = vw_iis_Prem_Pymnt_Grp_View.getRecord().newFieldInGroup("iis_Prem_Pymnt_Grp_View_Prem_Process_Cde", 
            "PREM-PROCESS-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "PREM_PROCESS_CDE");
        iis_Prem_Pymnt_Grp_View_Prem_Process_Cde.setDdmHeader("PREMIUM/PROCESSING/CODE");
        iis_Prem_Pymnt_Grp_View_Prem_Grp_Cde = vw_iis_Prem_Pymnt_Grp_View.getRecord().newFieldInGroup("iis_Prem_Pymnt_Grp_View_Prem_Grp_Cde", "PREM-GRP-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "PREM_GRP_CDE");
        iis_Prem_Pymnt_Grp_View_Prem_Grp_Cde.setDdmHeader("PREMIUM/GROUP/CODE");

        this.setRecordName("LdaIcwl3014");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iis_Prem_Pymnt_Grp_View.reset();
    }

    // Constructor
    public LdaIcwl3014() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
