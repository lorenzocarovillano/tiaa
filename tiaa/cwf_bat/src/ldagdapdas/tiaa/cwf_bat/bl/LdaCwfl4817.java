/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:36 PM
**        * FROM NATURAL LDA     : CWFL4817
************************************************************
**        * FILE NAME            : LdaCwfl4817.java
**        * CLASS NAME           : LdaCwfl4817
**        * INSTANCE NAME        : LdaCwfl4817
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4817 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cwf_Master;
    private DbsField cwf_Master_Pin_Nbr;
    private DbsField cwf_Master_Rqst_Log_Dte_Tme;
    private DbsGroup cwf_Master_Rqst_Log_Dte_TmeRedef1;
    private DbsField cwf_Master_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Rqst_Log_Invrt_Dte_Tme;
    private DbsField cwf_Master_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Rqst_Rgn_Cde;
    private DbsField cwf_Master_Rqst_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Rqst_Brnch_Cde;
    private DbsField cwf_Master_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Sub_Rqst_Ind;
    private DbsField cwf_Master_Case_Id_Cde;
    private DbsGroup cwf_Master_Case_Id_CdeRedef2;
    private DbsField cwf_Master_Case_Ind;
    private DbsField cwf_Master_Sub_Rqst_Sqnce_Ind;
    private DbsField cwf_Master_Multi_Rqst_Ind;
    private DbsField cwf_Master_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Work_Prcss_Id;
    private DbsGroup cwf_Master_Work_Prcss_IdRedef3;
    private DbsField cwf_Master_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Master_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Master_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Wpid_Vldte_Ind;
    private DbsField cwf_Master_Unit_Cde;
    private DbsGroup cwf_Master_Unit_CdeRedef4;
    private DbsField cwf_Master_Unit_Id_Cde;
    private DbsField cwf_Master_Unit_Rgn_Cde;
    private DbsField cwf_Master_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Unit_Brnch_Group_Cde;
    private DbsField cwf_Master_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Old_Route_Cde;
    private DbsField cwf_Master_Work_Rqst_Prty_Cde;
    private DbsField cwf_Master_Assgn_Sprvsr_Oprtr_Cde;
    private DbsField cwf_Master_Assgn_Dte_Tme;
    private DbsField cwf_Master_Empl_Oprtr_Cde;
    private DbsGroup cwf_Master_Empl_Oprtr_CdeRedef5;
    private DbsField cwf_Master_Empl_Racf_Id;
    private DbsField cwf_Master_Empl_Sffx_Cde;
    private DbsField cwf_Master_Last_Chnge_Dte_Tme;
    private DbsGroup cwf_Master_Last_Chnge_Dte_TmeRedef6;
    private DbsField cwf_Master_Last_Chnge_Dte_Tme_A;
    private DbsField cwf_Master_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Last_Chnge_Invrt_Dte_Tme;
    private DbsField cwf_Master_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Step_Id;
    private DbsField cwf_Master_Rt_Sqnce_Nbr;
    private DbsField cwf_Master_Step_Sqnce_Nbr;
    private DbsField cwf_Master_Step_Updte_Dte_Tme;
    private DbsField cwf_Master_Admin_Unit_Cde;
    private DbsField cwf_Master_Admin_Status_Cde;
    private DbsGroup cwf_Master_Admin_Status_CdeRedef7;
    private DbsField cwf_Master_Admin_Status_Cde_1;
    private DbsField cwf_Master_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Status_Cde;
    private DbsField cwf_Master_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Last_Updte_Dte;
    private DbsField cwf_Master_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Cntct_Orgn_Type_Cde;
    private DbsField cwf_Master_Cntct_Dte_Tme;
    private DbsField cwf_Master_Cntct_Invrt_Dte_Tme;
    private DbsField cwf_Master_Cntct_Oprtr_Id;
    private DbsField cwf_Master_Actve_Ind;
    private DbsField cwf_Master_Crprte_Status_Ind;
    private DbsField cwf_Master_Cnflct_Ind;
    private DbsField cwf_Master_Spcl_Hndlng_Txt;
    private DbsGroup cwf_Master_Spcl_Hndlng_TxtRedef8;
    private DbsField filler01;
    private DbsField cwf_Master_Merged_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Instn_Cde;
    private DbsField cwf_Master_Rqst_Instn_Cde;
    private DbsGroup cwf_Master_Spcl_Policy_Srce_CdeMuGroup;
    private DbsField cwf_Master_Spcl_Policy_Srce_Cde;
    private DbsField cwf_Master_Attntn_Txt;
    private DbsField cwf_Master_Check_Ind;
    private DbsField cwf_Master_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Rqst_Invrt_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Effctve_Dte;
    private DbsField cwf_Master_Trnsctn_Dte;
    private DbsField cwf_Master_Trans_Dte;
    private DbsField cwf_Master_Physcl_Fldr_Id_Nbr;
    private DbsField cwf_Master_Mj_Chrge_Dte_Tme;
    private DbsField cwf_Master_Mj_Chrge_Oprtr_Cde;
    private DbsField cwf_Master_Mstr_Indx_Actn_Cde;
    private DbsField cwf_Master_Mj_Pull_Ind;
    private DbsField cwf_Master_Final_Close_Out_Dte_Tme;
    private DbsField cwf_Master_Final_Close_Out_Oprtr_Cde;
    private DbsField cwf_Master_Mj_Emrgncy_Rqst_Dte_Tme;
    private DbsField cwf_Master_Mj_Emrgncy_Rqst_Oprtr_Cde;
    private DbsField cwf_Master_Print_Q_Ind;
    private DbsField cwf_Master_Print_Dte_Tme;
    private DbsField cwf_Master_Print_Batch_Id_Nbr;
    private DbsGroup cwf_Master_Print_Batch_Id_NbrRedef9;
    private DbsField cwf_Master_Print_Prefix_Ind;
    private DbsField cwf_Master_Print_Batch_Nbr;
    private DbsField cwf_Master_Print_Batch_Sqnce_Nbr;
    private DbsField cwf_Master_Print_Batch_Ind;
    private DbsField cwf_Master_Printer_Id_Cde;
    private DbsField cwf_Master_Rescan_Ind;
    private DbsField cwf_Master_Dup_Ind;
    private DbsField cwf_Master_Cmplnt_Ind;
    private DbsGroup cwf_Master_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Cntrct_Nbr;
    private DbsGroup cwf_Master_Mail_Item_NbrMuGroup;
    private DbsField cwf_Master_Mail_Item_Nbr;
    private DbsField cwf_Master_Rqst_Id;
    private DbsGroup cwf_Master_Rqst_IdRedef10;
    private DbsField cwf_Master_Rqst_Work_Prcss_Id;
    private DbsField cwf_Master_Rqst_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Rqst_Case_Id_Cde;
    private DbsField cwf_Master_Rqst_Pin_Nbr;
    private DbsField cwf_Master_Rlte_Rqst_Id;
    private DbsGroup cwf_Master_Rlte_Rqst_IdRedef11;
    private DbsField cwf_Master_Rlte_Work_Prcss_Id;
    private DbsField cwf_Master_Rlte_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Rlte_Case_Id_Cde;
    private DbsField cwf_Master_Rlte_Pin_Nbr;
    private DbsField cwf_Master_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Work_List_Ind;
    private DbsField cwf_Master_Due_Dte_Chg_Prty_Cde;
    private DbsGroup cwf_Master_Due_Dte_Chg_Prty_CdeRedef12;
    private DbsField cwf_Master_Due_Dte;
    private DbsField cwf_Master_Due_Dte_Chg_Ind;
    private DbsField cwf_Master_Due_Dte_Prty;
    private DbsField cwf_Master_Prcss_Time_Ind;
    private DbsField filler02;
    private DbsField cwf_Master_Bypss_Mutual_Fund_Ind;
    private DbsField cwf_Master_Sbsqnt_Cntct_Actn_Rqrd;
    private DbsField cwf_Master_Sbsqnt_Cntct_Ind;
    private DbsField cwf_Master_Prcssng_Type;
    private DbsField cwf_Master_Bsnss_Reply_Ind;
    private DbsField cwf_Master_Status_Freeze_Ind;
    private DbsField cwf_Master_Elctrnc_Fldr_Ind;
    private DbsField cwf_Master_Log_Insttn_Srce_Cde;
    private DbsField cwf_Master_Log_Rqstr_Cde;
    private DbsField cwf_Master_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Empl_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Empl_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Intrnl_Pnd_Start_Dte_Tme;
    private DbsGroup cwf_Master_Intrnl_Pnd_Start_Dte_TmeRedef13;
    private DbsField cwf_Master_Intrnl_Pnd_Start_Dte;
    private DbsField cwf_Master_Intrnl_Pnd_Start_Tme;
    private DbsField cwf_Master_Intrnl_Pnd_End_Dte_Tme;
    private DbsGroup cwf_Master_Intrnl_Pnd_End_Dte_TmeRedef14;
    private DbsField cwf_Master_Intrnl_Pnd_End_Dte;
    private DbsField cwf_Master_Intrnl_Pnd_End_Tme;
    private DbsField cwf_Master_Intrnl_Pnd_Days;
    private DbsField cwf_Master_Step_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Step_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Crprte_Clock_End_Dte_Tme;
    private DbsGroup cwf_Master_Crprte_Clock_End_Dte_TmeRedef15;
    private DbsField cwf_Master_Crprte_Clock_End_Dte;
    private DbsField cwf_Master_Crprte_Clock_End_Tme;
    private DbsField cwf_Master_Unit_En_Rte_To_Dte_Tme;
    private DbsGroup cwf_Master_Unit_En_Rte_To_Dte_TmeRedef16;
    private DbsField cwf_Master_Unit_Begin_Status;
    private DbsField cwf_Master_Empl_Begin_Status;
    private DbsField cwf_Master_Step_Begin_Status;
    private DbsField cwf_Master_Acknwldgmnt_Cde;
    private DbsField cwf_Master_Acknwldgmnt_Oprtr_Cde;
    private DbsField cwf_Master_Acknwldgmnt_Dte_Tme;
    private DbsField cwf_Master_Cntct_Sheet_Print_Cde;
    private DbsField cwf_Master_Cntct_Sheet_Print_Dte_Tme;
    private DbsField cwf_Master_Cntct_Sheet_Printer_Id_Cde;
    private DbsField cwf_Master_Shphrd_Id;
    private DbsField cwf_Master_Crrnt_Due_Dte_Cmt_Prty_Tme;
    private DbsGroup cwf_Master_Crrnt_Due_Dte_Cmt_Prty_TmeRedef17;
    private DbsField cwf_Master_Crrnt_Due_Dte;
    private DbsField cwf_Master_Crrnt_Cmt_Ind;
    private DbsField cwf_Master_Crrnt_Prrty_Cde;
    private DbsField cwf_Master_Crrnt_Due_Tme;
    private DbsField cwf_Master_Crprte_Due_Dte_Tme;
    private DbsField cwf_Master_Archvd_Dte;
    private DbsField cwf_Master_Rstr_To_Crrnt_Dte;
    private DbsField cwf_Master_Off_Rtng_Ind;
    private DbsField cwf_Master_Unit_On_Tme_Ind;
    private DbsField cwf_Master_Crprte_On_Tme_Ind;
    private DbsField cwf_Master_Owner_Unit_Cde;
    private DbsField cwf_Master_Physcl_Fldr_Owner_Id;
    private DbsField cwf_Master_Physcl_Fldr_Owner_Unit_Cde;
    private DbsField cwf_Master_Step_Re_Do_Ind;
    private DbsField cwf_Master_Tiaa_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Trade_Dte_Tme;
    private DbsField cwf_Master_Future_Pymnt_Ind;
    private DbsField cwf_Master_Status_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Status_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Status_Elpsd_Clndr_Days_Tme;
    private DbsGroup cwf_Master_Status_Elpsd_Clndr_Days_TmeRedef18;
    private DbsField cwf_Master_Status_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Status_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Status_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Status_Elpsd_Bsnss_Days_Tme;
    private DbsGroup cwf_Master_Status_Elpsd_Bsnss_Days_TmeRedef19;
    private DbsField cwf_Master_Status_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Status_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Status_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Step_Elpsd_Clndr_Days_Tme;
    private DbsGroup cwf_Master_Step_Elpsd_Clndr_Days_TmeRedef20;
    private DbsField cwf_Master_Step_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Step_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Step_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Step_Elpsd_Bsnss_Days_Tme;
    private DbsGroup cwf_Master_Step_Elpsd_Bsnss_Days_TmeRedef21;
    private DbsField cwf_Master_Step_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Step_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Step_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Empl_Elpsd_Clndr_Days_Tme;
    private DbsGroup cwf_Master_Empl_Elpsd_Clndr_Days_TmeRedef22;
    private DbsField cwf_Master_Empl_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Empl_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Empl_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Empl_Elpsd_Bsnss_Days_Tme;
    private DbsGroup cwf_Master_Empl_Elpsd_Bsnss_Days_TmeRedef23;
    private DbsField cwf_Master_Empl_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Empl_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Empl_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Unit_Elpsd_Clndr_Days_Tme;
    private DbsGroup cwf_Master_Unit_Elpsd_Clndr_Days_TmeRedef24;
    private DbsField cwf_Master_Unit_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Unit_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Unit_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Unit_Elpsd_Bsnss_Days_Tme;
    private DbsGroup cwf_Master_Unit_Elpsd_Bsnss_Days_TmeRedef25;
    private DbsField cwf_Master_Unit_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Unit_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Unit_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Intrnl_Pnd_Clndr_Days_Tme;
    private DbsGroup cwf_Master_Intrnl_Pnd_Clndr_Days_TmeRedef26;
    private DbsField cwf_Master_Intrnl_Pnd_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Intrnl_Pnd_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Intrnl_Pnd_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Intrnl_Pnd_Bsnss_Days_Tme;
    private DbsGroup cwf_Master_Intrnl_Pnd_Bsnss_Days_TmeRedef27;
    private DbsField cwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Cnnctd_Rqst_Ind;
    private DbsField cwf_Master_Prvte_Wrk_Rqst_Ind;
    private DbsField cwf_Master_Instn_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Log_Sqnce_Nbr;

    public DataAccessProgramView getVw_cwf_Master() { return vw_cwf_Master; }

    public DbsField getCwf_Master_Pin_Nbr() { return cwf_Master_Pin_Nbr; }

    public DbsField getCwf_Master_Rqst_Log_Dte_Tme() { return cwf_Master_Rqst_Log_Dte_Tme; }

    public DbsGroup getCwf_Master_Rqst_Log_Dte_TmeRedef1() { return cwf_Master_Rqst_Log_Dte_TmeRedef1; }

    public DbsField getCwf_Master_Rqst_Log_Index_Dte() { return cwf_Master_Rqst_Log_Index_Dte; }

    public DbsField getCwf_Master_Rqst_Log_Index_Tme() { return cwf_Master_Rqst_Log_Index_Tme; }

    public DbsField getCwf_Master_Rqst_Log_Invrt_Dte_Tme() { return cwf_Master_Rqst_Log_Invrt_Dte_Tme; }

    public DbsField getCwf_Master_Rqst_Log_Oprtr_Cde() { return cwf_Master_Rqst_Log_Oprtr_Cde; }

    public DbsField getCwf_Master_Rqst_Orgn_Cde() { return cwf_Master_Rqst_Orgn_Cde; }

    public DbsField getCwf_Master_Rqst_Rgn_Cde() { return cwf_Master_Rqst_Rgn_Cde; }

    public DbsField getCwf_Master_Rqst_Spcl_Dsgntn_Cde() { return cwf_Master_Rqst_Spcl_Dsgntn_Cde; }

    public DbsField getCwf_Master_Rqst_Brnch_Cde() { return cwf_Master_Rqst_Brnch_Cde; }

    public DbsField getCwf_Master_Orgnl_Log_Dte_Tme() { return cwf_Master_Orgnl_Log_Dte_Tme; }

    public DbsField getCwf_Master_Sub_Rqst_Ind() { return cwf_Master_Sub_Rqst_Ind; }

    public DbsField getCwf_Master_Case_Id_Cde() { return cwf_Master_Case_Id_Cde; }

    public DbsGroup getCwf_Master_Case_Id_CdeRedef2() { return cwf_Master_Case_Id_CdeRedef2; }

    public DbsField getCwf_Master_Case_Ind() { return cwf_Master_Case_Ind; }

    public DbsField getCwf_Master_Sub_Rqst_Sqnce_Ind() { return cwf_Master_Sub_Rqst_Sqnce_Ind; }

    public DbsField getCwf_Master_Multi_Rqst_Ind() { return cwf_Master_Multi_Rqst_Ind; }

    public DbsField getCwf_Master_Orgnl_Unit_Cde() { return cwf_Master_Orgnl_Unit_Cde; }

    public DbsField getCwf_Master_Work_Prcss_Id() { return cwf_Master_Work_Prcss_Id; }

    public DbsGroup getCwf_Master_Work_Prcss_IdRedef3() { return cwf_Master_Work_Prcss_IdRedef3; }

    public DbsField getCwf_Master_Work_Actn_Rqstd_Cde() { return cwf_Master_Work_Actn_Rqstd_Cde; }

    public DbsField getCwf_Master_Work_Lob_Cmpny_Prdct_Cde() { return cwf_Master_Work_Lob_Cmpny_Prdct_Cde; }

    public DbsField getCwf_Master_Work_Mjr_Bsnss_Prcss_Cde() { return cwf_Master_Work_Mjr_Bsnss_Prcss_Cde; }

    public DbsField getCwf_Master_Work_Spcfc_Bsnss_Prcss_Cde() { return cwf_Master_Work_Spcfc_Bsnss_Prcss_Cde; }

    public DbsField getCwf_Master_Wpid_Vldte_Ind() { return cwf_Master_Wpid_Vldte_Ind; }

    public DbsField getCwf_Master_Unit_Cde() { return cwf_Master_Unit_Cde; }

    public DbsGroup getCwf_Master_Unit_CdeRedef4() { return cwf_Master_Unit_CdeRedef4; }

    public DbsField getCwf_Master_Unit_Id_Cde() { return cwf_Master_Unit_Id_Cde; }

    public DbsField getCwf_Master_Unit_Rgn_Cde() { return cwf_Master_Unit_Rgn_Cde; }

    public DbsField getCwf_Master_Unit_Spcl_Dsgntn_Cde() { return cwf_Master_Unit_Spcl_Dsgntn_Cde; }

    public DbsField getCwf_Master_Unit_Brnch_Group_Cde() { return cwf_Master_Unit_Brnch_Group_Cde; }

    public DbsField getCwf_Master_Unit_Updte_Dte_Tme() { return cwf_Master_Unit_Updte_Dte_Tme; }

    public DbsField getCwf_Master_Old_Route_Cde() { return cwf_Master_Old_Route_Cde; }

    public DbsField getCwf_Master_Work_Rqst_Prty_Cde() { return cwf_Master_Work_Rqst_Prty_Cde; }

    public DbsField getCwf_Master_Assgn_Sprvsr_Oprtr_Cde() { return cwf_Master_Assgn_Sprvsr_Oprtr_Cde; }

    public DbsField getCwf_Master_Assgn_Dte_Tme() { return cwf_Master_Assgn_Dte_Tme; }

    public DbsField getCwf_Master_Empl_Oprtr_Cde() { return cwf_Master_Empl_Oprtr_Cde; }

    public DbsGroup getCwf_Master_Empl_Oprtr_CdeRedef5() { return cwf_Master_Empl_Oprtr_CdeRedef5; }

    public DbsField getCwf_Master_Empl_Racf_Id() { return cwf_Master_Empl_Racf_Id; }

    public DbsField getCwf_Master_Empl_Sffx_Cde() { return cwf_Master_Empl_Sffx_Cde; }

    public DbsField getCwf_Master_Last_Chnge_Dte_Tme() { return cwf_Master_Last_Chnge_Dte_Tme; }

    public DbsGroup getCwf_Master_Last_Chnge_Dte_TmeRedef6() { return cwf_Master_Last_Chnge_Dte_TmeRedef6; }

    public DbsField getCwf_Master_Last_Chnge_Dte_Tme_A() { return cwf_Master_Last_Chnge_Dte_Tme_A; }

    public DbsField getCwf_Master_Last_Chnge_Oprtr_Cde() { return cwf_Master_Last_Chnge_Oprtr_Cde; }

    public DbsField getCwf_Master_Last_Chnge_Invrt_Dte_Tme() { return cwf_Master_Last_Chnge_Invrt_Dte_Tme; }

    public DbsField getCwf_Master_Last_Chnge_Unit_Cde() { return cwf_Master_Last_Chnge_Unit_Cde; }

    public DbsField getCwf_Master_Step_Id() { return cwf_Master_Step_Id; }

    public DbsField getCwf_Master_Rt_Sqnce_Nbr() { return cwf_Master_Rt_Sqnce_Nbr; }

    public DbsField getCwf_Master_Step_Sqnce_Nbr() { return cwf_Master_Step_Sqnce_Nbr; }

    public DbsField getCwf_Master_Step_Updte_Dte_Tme() { return cwf_Master_Step_Updte_Dte_Tme; }

    public DbsField getCwf_Master_Admin_Unit_Cde() { return cwf_Master_Admin_Unit_Cde; }

    public DbsField getCwf_Master_Admin_Status_Cde() { return cwf_Master_Admin_Status_Cde; }

    public DbsGroup getCwf_Master_Admin_Status_CdeRedef7() { return cwf_Master_Admin_Status_CdeRedef7; }

    public DbsField getCwf_Master_Admin_Status_Cde_1() { return cwf_Master_Admin_Status_Cde_1; }

    public DbsField getCwf_Master_Admin_Status_Updte_Dte_Tme() { return cwf_Master_Admin_Status_Updte_Dte_Tme; }

    public DbsField getCwf_Master_Admin_Status_Updte_Oprtr_Cde() { return cwf_Master_Admin_Status_Updte_Oprtr_Cde; }

    public DbsField getCwf_Master_Status_Cde() { return cwf_Master_Status_Cde; }

    public DbsField getCwf_Master_Status_Updte_Dte_Tme() { return cwf_Master_Status_Updte_Dte_Tme; }

    public DbsField getCwf_Master_Status_Updte_Oprtr_Cde() { return cwf_Master_Status_Updte_Oprtr_Cde; }

    public DbsField getCwf_Master_Last_Updte_Dte() { return cwf_Master_Last_Updte_Dte; }

    public DbsField getCwf_Master_Last_Updte_Dte_Tme() { return cwf_Master_Last_Updte_Dte_Tme; }

    public DbsField getCwf_Master_Last_Updte_Oprtr_Cde() { return cwf_Master_Last_Updte_Oprtr_Cde; }

    public DbsField getCwf_Master_Cntct_Orgn_Type_Cde() { return cwf_Master_Cntct_Orgn_Type_Cde; }

    public DbsField getCwf_Master_Cntct_Dte_Tme() { return cwf_Master_Cntct_Dte_Tme; }

    public DbsField getCwf_Master_Cntct_Invrt_Dte_Tme() { return cwf_Master_Cntct_Invrt_Dte_Tme; }

    public DbsField getCwf_Master_Cntct_Oprtr_Id() { return cwf_Master_Cntct_Oprtr_Id; }

    public DbsField getCwf_Master_Actve_Ind() { return cwf_Master_Actve_Ind; }

    public DbsField getCwf_Master_Crprte_Status_Ind() { return cwf_Master_Crprte_Status_Ind; }

    public DbsField getCwf_Master_Cnflct_Ind() { return cwf_Master_Cnflct_Ind; }

    public DbsField getCwf_Master_Spcl_Hndlng_Txt() { return cwf_Master_Spcl_Hndlng_Txt; }

    public DbsGroup getCwf_Master_Spcl_Hndlng_TxtRedef8() { return cwf_Master_Spcl_Hndlng_TxtRedef8; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getCwf_Master_Merged_Rqst_Log_Dte_Tme() { return cwf_Master_Merged_Rqst_Log_Dte_Tme; }

    public DbsField getCwf_Master_Instn_Cde() { return cwf_Master_Instn_Cde; }

    public DbsField getCwf_Master_Rqst_Instn_Cde() { return cwf_Master_Rqst_Instn_Cde; }

    public DbsGroup getCwf_Master_Spcl_Policy_Srce_CdeMuGroup() { return cwf_Master_Spcl_Policy_Srce_CdeMuGroup; }

    public DbsField getCwf_Master_Spcl_Policy_Srce_Cde() { return cwf_Master_Spcl_Policy_Srce_Cde; }

    public DbsField getCwf_Master_Attntn_Txt() { return cwf_Master_Attntn_Txt; }

    public DbsField getCwf_Master_Check_Ind() { return cwf_Master_Check_Ind; }

    public DbsField getCwf_Master_Tiaa_Rcvd_Dte() { return cwf_Master_Tiaa_Rcvd_Dte; }

    public DbsField getCwf_Master_Rqst_Invrt_Rcvd_Dte_Tme() { return cwf_Master_Rqst_Invrt_Rcvd_Dte_Tme; }

    public DbsField getCwf_Master_Effctve_Dte() { return cwf_Master_Effctve_Dte; }

    public DbsField getCwf_Master_Trnsctn_Dte() { return cwf_Master_Trnsctn_Dte; }

    public DbsField getCwf_Master_Trans_Dte() { return cwf_Master_Trans_Dte; }

    public DbsField getCwf_Master_Physcl_Fldr_Id_Nbr() { return cwf_Master_Physcl_Fldr_Id_Nbr; }

    public DbsField getCwf_Master_Mj_Chrge_Dte_Tme() { return cwf_Master_Mj_Chrge_Dte_Tme; }

    public DbsField getCwf_Master_Mj_Chrge_Oprtr_Cde() { return cwf_Master_Mj_Chrge_Oprtr_Cde; }

    public DbsField getCwf_Master_Mstr_Indx_Actn_Cde() { return cwf_Master_Mstr_Indx_Actn_Cde; }

    public DbsField getCwf_Master_Mj_Pull_Ind() { return cwf_Master_Mj_Pull_Ind; }

    public DbsField getCwf_Master_Final_Close_Out_Dte_Tme() { return cwf_Master_Final_Close_Out_Dte_Tme; }

    public DbsField getCwf_Master_Final_Close_Out_Oprtr_Cde() { return cwf_Master_Final_Close_Out_Oprtr_Cde; }

    public DbsField getCwf_Master_Mj_Emrgncy_Rqst_Dte_Tme() { return cwf_Master_Mj_Emrgncy_Rqst_Dte_Tme; }

    public DbsField getCwf_Master_Mj_Emrgncy_Rqst_Oprtr_Cde() { return cwf_Master_Mj_Emrgncy_Rqst_Oprtr_Cde; }

    public DbsField getCwf_Master_Print_Q_Ind() { return cwf_Master_Print_Q_Ind; }

    public DbsField getCwf_Master_Print_Dte_Tme() { return cwf_Master_Print_Dte_Tme; }

    public DbsField getCwf_Master_Print_Batch_Id_Nbr() { return cwf_Master_Print_Batch_Id_Nbr; }

    public DbsGroup getCwf_Master_Print_Batch_Id_NbrRedef9() { return cwf_Master_Print_Batch_Id_NbrRedef9; }

    public DbsField getCwf_Master_Print_Prefix_Ind() { return cwf_Master_Print_Prefix_Ind; }

    public DbsField getCwf_Master_Print_Batch_Nbr() { return cwf_Master_Print_Batch_Nbr; }

    public DbsField getCwf_Master_Print_Batch_Sqnce_Nbr() { return cwf_Master_Print_Batch_Sqnce_Nbr; }

    public DbsField getCwf_Master_Print_Batch_Ind() { return cwf_Master_Print_Batch_Ind; }

    public DbsField getCwf_Master_Printer_Id_Cde() { return cwf_Master_Printer_Id_Cde; }

    public DbsField getCwf_Master_Rescan_Ind() { return cwf_Master_Rescan_Ind; }

    public DbsField getCwf_Master_Dup_Ind() { return cwf_Master_Dup_Ind; }

    public DbsField getCwf_Master_Cmplnt_Ind() { return cwf_Master_Cmplnt_Ind; }

    public DbsGroup getCwf_Master_Cntrct_NbrMuGroup() { return cwf_Master_Cntrct_NbrMuGroup; }

    public DbsField getCwf_Master_Cntrct_Nbr() { return cwf_Master_Cntrct_Nbr; }

    public DbsGroup getCwf_Master_Mail_Item_NbrMuGroup() { return cwf_Master_Mail_Item_NbrMuGroup; }

    public DbsField getCwf_Master_Mail_Item_Nbr() { return cwf_Master_Mail_Item_Nbr; }

    public DbsField getCwf_Master_Rqst_Id() { return cwf_Master_Rqst_Id; }

    public DbsGroup getCwf_Master_Rqst_IdRedef10() { return cwf_Master_Rqst_IdRedef10; }

    public DbsField getCwf_Master_Rqst_Work_Prcss_Id() { return cwf_Master_Rqst_Work_Prcss_Id; }

    public DbsField getCwf_Master_Rqst_Tiaa_Rcvd_Dte() { return cwf_Master_Rqst_Tiaa_Rcvd_Dte; }

    public DbsField getCwf_Master_Rqst_Case_Id_Cde() { return cwf_Master_Rqst_Case_Id_Cde; }

    public DbsField getCwf_Master_Rqst_Pin_Nbr() { return cwf_Master_Rqst_Pin_Nbr; }

    public DbsField getCwf_Master_Rlte_Rqst_Id() { return cwf_Master_Rlte_Rqst_Id; }

    public DbsGroup getCwf_Master_Rlte_Rqst_IdRedef11() { return cwf_Master_Rlte_Rqst_IdRedef11; }

    public DbsField getCwf_Master_Rlte_Work_Prcss_Id() { return cwf_Master_Rlte_Work_Prcss_Id; }

    public DbsField getCwf_Master_Rlte_Tiaa_Rcvd_Dte() { return cwf_Master_Rlte_Tiaa_Rcvd_Dte; }

    public DbsField getCwf_Master_Rlte_Case_Id_Cde() { return cwf_Master_Rlte_Case_Id_Cde; }

    public DbsField getCwf_Master_Rlte_Pin_Nbr() { return cwf_Master_Rlte_Pin_Nbr; }

    public DbsField getCwf_Master_Extrnl_Pend_Rcv_Dte() { return cwf_Master_Extrnl_Pend_Rcv_Dte; }

    public DbsField getCwf_Master_Work_List_Ind() { return cwf_Master_Work_List_Ind; }

    public DbsField getCwf_Master_Due_Dte_Chg_Prty_Cde() { return cwf_Master_Due_Dte_Chg_Prty_Cde; }

    public DbsGroup getCwf_Master_Due_Dte_Chg_Prty_CdeRedef12() { return cwf_Master_Due_Dte_Chg_Prty_CdeRedef12; }

    public DbsField getCwf_Master_Due_Dte() { return cwf_Master_Due_Dte; }

    public DbsField getCwf_Master_Due_Dte_Chg_Ind() { return cwf_Master_Due_Dte_Chg_Ind; }

    public DbsField getCwf_Master_Due_Dte_Prty() { return cwf_Master_Due_Dte_Prty; }

    public DbsField getCwf_Master_Prcss_Time_Ind() { return cwf_Master_Prcss_Time_Ind; }

    public DbsField getFiller02() { return filler02; }

    public DbsField getCwf_Master_Bypss_Mutual_Fund_Ind() { return cwf_Master_Bypss_Mutual_Fund_Ind; }

    public DbsField getCwf_Master_Sbsqnt_Cntct_Actn_Rqrd() { return cwf_Master_Sbsqnt_Cntct_Actn_Rqrd; }

    public DbsField getCwf_Master_Sbsqnt_Cntct_Ind() { return cwf_Master_Sbsqnt_Cntct_Ind; }

    public DbsField getCwf_Master_Prcssng_Type() { return cwf_Master_Prcssng_Type; }

    public DbsField getCwf_Master_Bsnss_Reply_Ind() { return cwf_Master_Bsnss_Reply_Ind; }

    public DbsField getCwf_Master_Status_Freeze_Ind() { return cwf_Master_Status_Freeze_Ind; }

    public DbsField getCwf_Master_Elctrnc_Fldr_Ind() { return cwf_Master_Elctrnc_Fldr_Ind; }

    public DbsField getCwf_Master_Log_Insttn_Srce_Cde() { return cwf_Master_Log_Insttn_Srce_Cde; }

    public DbsField getCwf_Master_Log_Rqstr_Cde() { return cwf_Master_Log_Rqstr_Cde; }

    public DbsField getCwf_Master_Unit_Clock_Start_Dte_Tme() { return cwf_Master_Unit_Clock_Start_Dte_Tme; }

    public DbsField getCwf_Master_Unit_Clock_End_Dte_Tme() { return cwf_Master_Unit_Clock_End_Dte_Tme; }

    public DbsField getCwf_Master_Empl_Clock_Start_Dte_Tme() { return cwf_Master_Empl_Clock_Start_Dte_Tme; }

    public DbsField getCwf_Master_Empl_Clock_End_Dte_Tme() { return cwf_Master_Empl_Clock_End_Dte_Tme; }

    public DbsField getCwf_Master_Intrnl_Pnd_Start_Dte_Tme() { return cwf_Master_Intrnl_Pnd_Start_Dte_Tme; }

    public DbsGroup getCwf_Master_Intrnl_Pnd_Start_Dte_TmeRedef13() { return cwf_Master_Intrnl_Pnd_Start_Dte_TmeRedef13; }

    public DbsField getCwf_Master_Intrnl_Pnd_Start_Dte() { return cwf_Master_Intrnl_Pnd_Start_Dte; }

    public DbsField getCwf_Master_Intrnl_Pnd_Start_Tme() { return cwf_Master_Intrnl_Pnd_Start_Tme; }

    public DbsField getCwf_Master_Intrnl_Pnd_End_Dte_Tme() { return cwf_Master_Intrnl_Pnd_End_Dte_Tme; }

    public DbsGroup getCwf_Master_Intrnl_Pnd_End_Dte_TmeRedef14() { return cwf_Master_Intrnl_Pnd_End_Dte_TmeRedef14; }

    public DbsField getCwf_Master_Intrnl_Pnd_End_Dte() { return cwf_Master_Intrnl_Pnd_End_Dte; }

    public DbsField getCwf_Master_Intrnl_Pnd_End_Tme() { return cwf_Master_Intrnl_Pnd_End_Tme; }

    public DbsField getCwf_Master_Intrnl_Pnd_Days() { return cwf_Master_Intrnl_Pnd_Days; }

    public DbsField getCwf_Master_Step_Clock_Start_Dte_Tme() { return cwf_Master_Step_Clock_Start_Dte_Tme; }

    public DbsField getCwf_Master_Step_Clock_End_Dte_Tme() { return cwf_Master_Step_Clock_End_Dte_Tme; }

    public DbsField getCwf_Master_Crprte_Clock_End_Dte_Tme() { return cwf_Master_Crprte_Clock_End_Dte_Tme; }

    public DbsGroup getCwf_Master_Crprte_Clock_End_Dte_TmeRedef15() { return cwf_Master_Crprte_Clock_End_Dte_TmeRedef15; }

    public DbsField getCwf_Master_Crprte_Clock_End_Dte() { return cwf_Master_Crprte_Clock_End_Dte; }

    public DbsField getCwf_Master_Crprte_Clock_End_Tme() { return cwf_Master_Crprte_Clock_End_Tme; }

    public DbsField getCwf_Master_Unit_En_Rte_To_Dte_Tme() { return cwf_Master_Unit_En_Rte_To_Dte_Tme; }

    public DbsGroup getCwf_Master_Unit_En_Rte_To_Dte_TmeRedef16() { return cwf_Master_Unit_En_Rte_To_Dte_TmeRedef16; }

    public DbsField getCwf_Master_Unit_Begin_Status() { return cwf_Master_Unit_Begin_Status; }

    public DbsField getCwf_Master_Empl_Begin_Status() { return cwf_Master_Empl_Begin_Status; }

    public DbsField getCwf_Master_Step_Begin_Status() { return cwf_Master_Step_Begin_Status; }

    public DbsField getCwf_Master_Acknwldgmnt_Cde() { return cwf_Master_Acknwldgmnt_Cde; }

    public DbsField getCwf_Master_Acknwldgmnt_Oprtr_Cde() { return cwf_Master_Acknwldgmnt_Oprtr_Cde; }

    public DbsField getCwf_Master_Acknwldgmnt_Dte_Tme() { return cwf_Master_Acknwldgmnt_Dte_Tme; }

    public DbsField getCwf_Master_Cntct_Sheet_Print_Cde() { return cwf_Master_Cntct_Sheet_Print_Cde; }

    public DbsField getCwf_Master_Cntct_Sheet_Print_Dte_Tme() { return cwf_Master_Cntct_Sheet_Print_Dte_Tme; }

    public DbsField getCwf_Master_Cntct_Sheet_Printer_Id_Cde() { return cwf_Master_Cntct_Sheet_Printer_Id_Cde; }

    public DbsField getCwf_Master_Shphrd_Id() { return cwf_Master_Shphrd_Id; }

    public DbsField getCwf_Master_Crrnt_Due_Dte_Cmt_Prty_Tme() { return cwf_Master_Crrnt_Due_Dte_Cmt_Prty_Tme; }

    public DbsGroup getCwf_Master_Crrnt_Due_Dte_Cmt_Prty_TmeRedef17() { return cwf_Master_Crrnt_Due_Dte_Cmt_Prty_TmeRedef17; }

    public DbsField getCwf_Master_Crrnt_Due_Dte() { return cwf_Master_Crrnt_Due_Dte; }

    public DbsField getCwf_Master_Crrnt_Cmt_Ind() { return cwf_Master_Crrnt_Cmt_Ind; }

    public DbsField getCwf_Master_Crrnt_Prrty_Cde() { return cwf_Master_Crrnt_Prrty_Cde; }

    public DbsField getCwf_Master_Crrnt_Due_Tme() { return cwf_Master_Crrnt_Due_Tme; }

    public DbsField getCwf_Master_Crprte_Due_Dte_Tme() { return cwf_Master_Crprte_Due_Dte_Tme; }

    public DbsField getCwf_Master_Archvd_Dte() { return cwf_Master_Archvd_Dte; }

    public DbsField getCwf_Master_Rstr_To_Crrnt_Dte() { return cwf_Master_Rstr_To_Crrnt_Dte; }

    public DbsField getCwf_Master_Off_Rtng_Ind() { return cwf_Master_Off_Rtng_Ind; }

    public DbsField getCwf_Master_Unit_On_Tme_Ind() { return cwf_Master_Unit_On_Tme_Ind; }

    public DbsField getCwf_Master_Crprte_On_Tme_Ind() { return cwf_Master_Crprte_On_Tme_Ind; }

    public DbsField getCwf_Master_Owner_Unit_Cde() { return cwf_Master_Owner_Unit_Cde; }

    public DbsField getCwf_Master_Physcl_Fldr_Owner_Id() { return cwf_Master_Physcl_Fldr_Owner_Id; }

    public DbsField getCwf_Master_Physcl_Fldr_Owner_Unit_Cde() { return cwf_Master_Physcl_Fldr_Owner_Unit_Cde; }

    public DbsField getCwf_Master_Step_Re_Do_Ind() { return cwf_Master_Step_Re_Do_Ind; }

    public DbsField getCwf_Master_Tiaa_Rcvd_Dte_Tme() { return cwf_Master_Tiaa_Rcvd_Dte_Tme; }

    public DbsField getCwf_Master_Trade_Dte_Tme() { return cwf_Master_Trade_Dte_Tme; }

    public DbsField getCwf_Master_Future_Pymnt_Ind() { return cwf_Master_Future_Pymnt_Ind; }

    public DbsField getCwf_Master_Status_Clock_Start_Dte_Tme() { return cwf_Master_Status_Clock_Start_Dte_Tme; }

    public DbsField getCwf_Master_Status_Clock_End_Dte_Tme() { return cwf_Master_Status_Clock_End_Dte_Tme; }

    public DbsField getCwf_Master_Status_Elpsd_Clndr_Days_Tme() { return cwf_Master_Status_Elpsd_Clndr_Days_Tme; }

    public DbsGroup getCwf_Master_Status_Elpsd_Clndr_Days_TmeRedef18() { return cwf_Master_Status_Elpsd_Clndr_Days_TmeRedef18; }

    public DbsField getCwf_Master_Status_Elpsd_Clndr_Days() { return cwf_Master_Status_Elpsd_Clndr_Days; }

    public DbsField getCwf_Master_Status_Elpsd_Clndr_Hours() { return cwf_Master_Status_Elpsd_Clndr_Hours; }

    public DbsField getCwf_Master_Status_Elpsd_Clndr_Minutes() { return cwf_Master_Status_Elpsd_Clndr_Minutes; }

    public DbsField getCwf_Master_Status_Elpsd_Bsnss_Days_Tme() { return cwf_Master_Status_Elpsd_Bsnss_Days_Tme; }

    public DbsGroup getCwf_Master_Status_Elpsd_Bsnss_Days_TmeRedef19() { return cwf_Master_Status_Elpsd_Bsnss_Days_TmeRedef19; }

    public DbsField getCwf_Master_Status_Elpsd_Bsnss_Days() { return cwf_Master_Status_Elpsd_Bsnss_Days; }

    public DbsField getCwf_Master_Status_Elpsd_Bsnss_Hours() { return cwf_Master_Status_Elpsd_Bsnss_Hours; }

    public DbsField getCwf_Master_Status_Elpsd_Bsnss_Minutes() { return cwf_Master_Status_Elpsd_Bsnss_Minutes; }

    public DbsField getCwf_Master_Step_Elpsd_Clndr_Days_Tme() { return cwf_Master_Step_Elpsd_Clndr_Days_Tme; }

    public DbsGroup getCwf_Master_Step_Elpsd_Clndr_Days_TmeRedef20() { return cwf_Master_Step_Elpsd_Clndr_Days_TmeRedef20; }

    public DbsField getCwf_Master_Step_Elpsd_Clndr_Days() { return cwf_Master_Step_Elpsd_Clndr_Days; }

    public DbsField getCwf_Master_Step_Elpsd_Clndr_Hours() { return cwf_Master_Step_Elpsd_Clndr_Hours; }

    public DbsField getCwf_Master_Step_Elpsd_Clndr_Minutes() { return cwf_Master_Step_Elpsd_Clndr_Minutes; }

    public DbsField getCwf_Master_Step_Elpsd_Bsnss_Days_Tme() { return cwf_Master_Step_Elpsd_Bsnss_Days_Tme; }

    public DbsGroup getCwf_Master_Step_Elpsd_Bsnss_Days_TmeRedef21() { return cwf_Master_Step_Elpsd_Bsnss_Days_TmeRedef21; }

    public DbsField getCwf_Master_Step_Elpsd_Bsnss_Days() { return cwf_Master_Step_Elpsd_Bsnss_Days; }

    public DbsField getCwf_Master_Step_Elpsd_Bsnss_Hours() { return cwf_Master_Step_Elpsd_Bsnss_Hours; }

    public DbsField getCwf_Master_Step_Elpsd_Bsnss_Minutes() { return cwf_Master_Step_Elpsd_Bsnss_Minutes; }

    public DbsField getCwf_Master_Empl_Elpsd_Clndr_Days_Tme() { return cwf_Master_Empl_Elpsd_Clndr_Days_Tme; }

    public DbsGroup getCwf_Master_Empl_Elpsd_Clndr_Days_TmeRedef22() { return cwf_Master_Empl_Elpsd_Clndr_Days_TmeRedef22; }

    public DbsField getCwf_Master_Empl_Elpsd_Clndr_Days() { return cwf_Master_Empl_Elpsd_Clndr_Days; }

    public DbsField getCwf_Master_Empl_Elpsd_Clndr_Hours() { return cwf_Master_Empl_Elpsd_Clndr_Hours; }

    public DbsField getCwf_Master_Empl_Elpsd_Clndr_Minutes() { return cwf_Master_Empl_Elpsd_Clndr_Minutes; }

    public DbsField getCwf_Master_Empl_Elpsd_Bsnss_Days_Tme() { return cwf_Master_Empl_Elpsd_Bsnss_Days_Tme; }

    public DbsGroup getCwf_Master_Empl_Elpsd_Bsnss_Days_TmeRedef23() { return cwf_Master_Empl_Elpsd_Bsnss_Days_TmeRedef23; }

    public DbsField getCwf_Master_Empl_Elpsd_Bsnss_Days() { return cwf_Master_Empl_Elpsd_Bsnss_Days; }

    public DbsField getCwf_Master_Empl_Elpsd_Bsnss_Hours() { return cwf_Master_Empl_Elpsd_Bsnss_Hours; }

    public DbsField getCwf_Master_Empl_Elpsd_Bsnss_Minutes() { return cwf_Master_Empl_Elpsd_Bsnss_Minutes; }

    public DbsField getCwf_Master_Unit_Elpsd_Clndr_Days_Tme() { return cwf_Master_Unit_Elpsd_Clndr_Days_Tme; }

    public DbsGroup getCwf_Master_Unit_Elpsd_Clndr_Days_TmeRedef24() { return cwf_Master_Unit_Elpsd_Clndr_Days_TmeRedef24; }

    public DbsField getCwf_Master_Unit_Elpsd_Clndr_Days() { return cwf_Master_Unit_Elpsd_Clndr_Days; }

    public DbsField getCwf_Master_Unit_Elpsd_Clndr_Hours() { return cwf_Master_Unit_Elpsd_Clndr_Hours; }

    public DbsField getCwf_Master_Unit_Elpsd_Clndr_Minutes() { return cwf_Master_Unit_Elpsd_Clndr_Minutes; }

    public DbsField getCwf_Master_Unit_Elpsd_Bsnss_Days_Tme() { return cwf_Master_Unit_Elpsd_Bsnss_Days_Tme; }

    public DbsGroup getCwf_Master_Unit_Elpsd_Bsnss_Days_TmeRedef25() { return cwf_Master_Unit_Elpsd_Bsnss_Days_TmeRedef25; }

    public DbsField getCwf_Master_Unit_Elpsd_Bsnss_Days() { return cwf_Master_Unit_Elpsd_Bsnss_Days; }

    public DbsField getCwf_Master_Unit_Elpsd_Bsnss_Hours() { return cwf_Master_Unit_Elpsd_Bsnss_Hours; }

    public DbsField getCwf_Master_Unit_Elpsd_Bsnss_Minutes() { return cwf_Master_Unit_Elpsd_Bsnss_Minutes; }

    public DbsField getCwf_Master_Intrnl_Pnd_Clndr_Days_Tme() { return cwf_Master_Intrnl_Pnd_Clndr_Days_Tme; }

    public DbsGroup getCwf_Master_Intrnl_Pnd_Clndr_Days_TmeRedef26() { return cwf_Master_Intrnl_Pnd_Clndr_Days_TmeRedef26; }

    public DbsField getCwf_Master_Intrnl_Pnd_Elpsd_Clndr_Days() { return cwf_Master_Intrnl_Pnd_Elpsd_Clndr_Days; }

    public DbsField getCwf_Master_Intrnl_Pnd_Elpsd_Clndr_Hours() { return cwf_Master_Intrnl_Pnd_Elpsd_Clndr_Hours; }

    public DbsField getCwf_Master_Intrnl_Pnd_Elpsd_Clndr_Minutes() { return cwf_Master_Intrnl_Pnd_Elpsd_Clndr_Minutes; }

    public DbsField getCwf_Master_Intrnl_Pnd_Bsnss_Days_Tme() { return cwf_Master_Intrnl_Pnd_Bsnss_Days_Tme; }

    public DbsGroup getCwf_Master_Intrnl_Pnd_Bsnss_Days_TmeRedef27() { return cwf_Master_Intrnl_Pnd_Bsnss_Days_TmeRedef27; }

    public DbsField getCwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Days() { return cwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Days; }

    public DbsField getCwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Hours() { return cwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Hours; }

    public DbsField getCwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Minutes() { return cwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Minutes; }

    public DbsField getCwf_Master_Cnnctd_Rqst_Ind() { return cwf_Master_Cnnctd_Rqst_Ind; }

    public DbsField getCwf_Master_Prvte_Wrk_Rqst_Ind() { return cwf_Master_Prvte_Wrk_Rqst_Ind; }

    public DbsField getCwf_Master_Instn_Rqst_Log_Dte_Tme() { return cwf_Master_Instn_Rqst_Log_Dte_Tme; }

    public DbsField getCwf_Master_Log_Sqnce_Nbr() { return cwf_Master_Log_Sqnce_Nbr; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cwf_Master = new DataAccessProgramView(new NameInfo("vw_cwf_Master", "CWF-MASTER"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Pin_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Rqst_Log_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Rqst_Log_Dte_TmeRedef1 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Rqst_Log_Dte_TmeRedef1", "Redefines", cwf_Master_Rqst_Log_Dte_Tme);
        cwf_Master_Rqst_Log_Index_Dte = cwf_Master_Rqst_Log_Dte_TmeRedef1.newFieldInGroup("cwf_Master_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 
            8);
        cwf_Master_Rqst_Log_Index_Tme = cwf_Master_Rqst_Log_Dte_TmeRedef1.newFieldInGroup("cwf_Master_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", FieldType.STRING, 
            7);
        cwf_Master_Rqst_Log_Invrt_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Log_Invrt_Dte_Tme", "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        cwf_Master_Rqst_Log_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Rqst_Orgn_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_ORGN_CDE");
        cwf_Master_Rqst_Rgn_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Rgn_Cde", "RQST-RGN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_RGN_CDE");
        cwf_Master_Rqst_Spcl_Dsgntn_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Spcl_Dsgntn_Cde", "RQST-SPCL-DSGNTN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_SPCL_DSGNTN_CDE");
        cwf_Master_Rqst_Brnch_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Brnch_Cde", "RQST-BRNCH-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_BRNCH_CDE");
        cwf_Master_Orgnl_Log_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Sub_Rqst_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SUB_RQST_IND");
        cwf_Master_Sub_Rqst_Ind.setDdmHeader("SUB-REQUEST COUNT");
        cwf_Master_Case_Id_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CASE_ID_CDE");
        cwf_Master_Case_Id_Cde.setDdmHeader("CASE/ID");
        cwf_Master_Case_Id_CdeRedef2 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Case_Id_CdeRedef2", "Redefines", cwf_Master_Case_Id_Cde);
        cwf_Master_Case_Ind = cwf_Master_Case_Id_CdeRedef2.newFieldInGroup("cwf_Master_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        cwf_Master_Sub_Rqst_Sqnce_Ind = cwf_Master_Case_Id_CdeRedef2.newFieldInGroup("cwf_Master_Sub_Rqst_Sqnce_Ind", "SUB-RQST-SQNCE-IND", FieldType.STRING, 
            1);
        cwf_Master_Multi_Rqst_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MULTI_RQST_IND");
        cwf_Master_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Master_Orgnl_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ORGNL_UNIT_CDE");
        cwf_Master_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Work_Prcss_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Work_Prcss_IdRedef3 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Work_Prcss_IdRedef3", "Redefines", cwf_Master_Work_Prcss_Id);
        cwf_Master_Work_Actn_Rqstd_Cde = cwf_Master_Work_Prcss_IdRedef3.newFieldInGroup("cwf_Master_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", FieldType.STRING, 
            1);
        cwf_Master_Work_Lob_Cmpny_Prdct_Cde = cwf_Master_Work_Prcss_IdRedef3.newFieldInGroup("cwf_Master_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Work_Mjr_Bsnss_Prcss_Cde = cwf_Master_Work_Prcss_IdRedef3.newFieldInGroup("cwf_Master_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Master_Work_Prcss_IdRedef3.newFieldInGroup("cwf_Master_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Wpid_Vldte_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Wpid_Vldte_Ind", "WPID-VLDTE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "WPID_VLDTE_IND");
        cwf_Master_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Master_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Master_Unit_CdeRedef4 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Unit_CdeRedef4", "Redefines", cwf_Master_Unit_Cde);
        cwf_Master_Unit_Id_Cde = cwf_Master_Unit_CdeRedef4.newFieldInGroup("cwf_Master_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        cwf_Master_Unit_Rgn_Cde = cwf_Master_Unit_CdeRedef4.newFieldInGroup("cwf_Master_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 1);
        cwf_Master_Unit_Spcl_Dsgntn_Cde = cwf_Master_Unit_CdeRedef4.newFieldInGroup("cwf_Master_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", FieldType.STRING, 
            1);
        cwf_Master_Unit_Brnch_Group_Cde = cwf_Master_Unit_CdeRedef4.newFieldInGroup("cwf_Master_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", FieldType.STRING, 
            1);
        cwf_Master_Unit_Updte_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Old_Route_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Old_Route_Cde", "OLD-ROUTE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "OLD_ROUTE_CDE");
        cwf_Master_Old_Route_Cde.setDdmHeader("ACTIVITY-END/STATUS");
        cwf_Master_Work_Rqst_Prty_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Work_Rqst_Prty_Cde", "WORK-RQST-PRTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "WORK_RQST_PRTY_CDE");
        cwf_Master_Work_Rqst_Prty_Cde.setDdmHeader("PRIO");
        cwf_Master_Assgn_Sprvsr_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Assgn_Sprvsr_Oprtr_Cde", "ASSGN-SPRVSR-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ASSGN_SPRVSR_OPRTR_CDE");
        cwf_Master_Assgn_Sprvsr_Oprtr_Cde.setDdmHeader("SUPER/VISOR");
        cwf_Master_Assgn_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Assgn_Dte_Tme", "ASSGN-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ASSGN_DTE_TME");
        cwf_Master_Assgn_Dte_Tme.setDdmHeader("ASSIGNMENT/DATE-TIME");
        cwf_Master_Empl_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "EMPL_OPRTR_CDE");
        cwf_Master_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        cwf_Master_Empl_Oprtr_CdeRedef5 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Empl_Oprtr_CdeRedef5", "Redefines", cwf_Master_Empl_Oprtr_Cde);
        cwf_Master_Empl_Racf_Id = cwf_Master_Empl_Oprtr_CdeRedef5.newFieldInGroup("cwf_Master_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        cwf_Master_Empl_Sffx_Cde = cwf_Master_Empl_Oprtr_CdeRedef5.newFieldInGroup("cwf_Master_Empl_Sffx_Cde", "EMPL-SFFX-CDE", FieldType.STRING, 2);
        cwf_Master_Last_Chnge_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Last_Chnge_Dte_TmeRedef6 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Last_Chnge_Dte_TmeRedef6", "Redefines", cwf_Master_Last_Chnge_Dte_Tme);
        cwf_Master_Last_Chnge_Dte_Tme_A = cwf_Master_Last_Chnge_Dte_TmeRedef6.newFieldInGroup("cwf_Master_Last_Chnge_Dte_Tme_A", "LAST-CHNGE-DTE-TME-A", 
            FieldType.STRING, 15);
        cwf_Master_Last_Chnge_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Last_Chnge_Invrt_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Invrt_Dte_Tme", "LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        cwf_Master_Last_Chnge_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Step_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        cwf_Master_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Rt_Sqnce_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "RT_SQNCE_NBR");
        cwf_Master_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        cwf_Master_Step_Sqnce_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Sqnce_Nbr", "STEP-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "STEP_SQNCE_NBR");
        cwf_Master_Step_Updte_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Updte_Dte_Tme", "STEP-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STEP_UPDTE_DTE_TME");
        cwf_Master_Step_Updte_Dte_Tme.setDdmHeader("STEP UPDATE/DATE-TIME");
        cwf_Master_Admin_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        cwf_Master_Admin_Status_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Admin_Status_CdeRedef7 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Admin_Status_CdeRedef7", "Redefines", cwf_Master_Admin_Status_Cde);
        cwf_Master_Admin_Status_Cde_1 = cwf_Master_Admin_Status_CdeRedef7.newFieldInGroup("cwf_Master_Admin_Status_Cde_1", "ADMIN-STATUS-CDE-1", FieldType.STRING, 
            1);
        cwf_Master_Admin_Status_Updte_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Admin_Status_Updte_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Status_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        cwf_Master_Status_Updte_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Status_Updte_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Last_Updte_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "LAST_UPDTE_DTE");
        cwf_Master_Last_Updte_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Last_Updte_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Cntct_Orgn_Type_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Cntct_Orgn_Type_Cde", "CNTCT-ORGN-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTCT_ORGN_TYPE_CDE");
        cwf_Master_Cntct_Orgn_Type_Cde.setDdmHeader("CONTACT/CODE");
        cwf_Master_Cntct_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Cntct_Dte_Tme", "CNTCT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, 
            "CNTCT_DTE_TME");
        cwf_Master_Cntct_Invrt_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Cntct_Invrt_Dte_Tme", "CNTCT-INVRT-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "CNTCT_INVRT_DTE_TME");
        cwf_Master_Cntct_Oprtr_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Cntct_Oprtr_Id", "CNTCT-OPRTR-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTCT_OPRTR_ID");
        cwf_Master_Actve_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Master_Crprte_Status_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Cnflct_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Cnflct_Ind", "CNFLCT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNFLCT_IND");
        cwf_Master_Cnflct_Ind.setDdmHeader("CONFLICT/REQUEST");
        cwf_Master_Spcl_Hndlng_Txt = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", FieldType.STRING, 72, 
            RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        cwf_Master_Spcl_Hndlng_Txt.setDdmHeader("MESSAGES");
        cwf_Master_Spcl_Hndlng_TxtRedef8 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Spcl_Hndlng_TxtRedef8", "Redefines", cwf_Master_Spcl_Hndlng_Txt);
        filler01 = cwf_Master_Spcl_Hndlng_TxtRedef8.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 57);
        cwf_Master_Merged_Rqst_Log_Dte_Tme = cwf_Master_Spcl_Hndlng_TxtRedef8.newFieldInGroup("cwf_Master_Merged_Rqst_Log_Dte_Tme", "MERGED-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        cwf_Master_Instn_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Instn_Cde", "INSTN-CDE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "INSTN_CDE");
        cwf_Master_Rqst_Instn_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Instn_Cde", "RQST-INSTN-CDE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RQST_INSTN_CDE");
        cwf_Master_Spcl_Policy_Srce_CdeMuGroup = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Spcl_Policy_Srce_CdeMuGroup", "SPCL_POLICY_SRCE_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_SPCL_POLICY_SRCE_CDE");
        cwf_Master_Spcl_Policy_Srce_Cde = cwf_Master_Spcl_Policy_Srce_CdeMuGroup.newFieldArrayInGroup("cwf_Master_Spcl_Policy_Srce_Cde", "SPCL-POLICY-SRCE-CDE", 
            FieldType.STRING, 5, new DbsArrayController(1,10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "SPCL_POLICY_SRCE_CDE");
        cwf_Master_Attntn_Txt = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Attntn_Txt", "ATTNTN-TXT", FieldType.STRING, 25, RepeatingFieldStrategy.None, 
            "ATTNTN_TXT");
        cwf_Master_Attntn_Txt.setDdmHeader("ATTENTION");
        cwf_Master_Check_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Check_Ind", "CHECK-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHECK_IND");
        cwf_Master_Check_Ind.setDdmHeader("CHK");
        cwf_Master_Tiaa_Rcvd_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Master_Rqst_Invrt_Rcvd_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Invrt_Rcvd_Dte_Tme", "RQST-INVRT-RCVD-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_INVRT_RCVD_DTE_TME");
        cwf_Master_Effctve_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "EFFCTVE_DTE");
        cwf_Master_Trnsctn_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Trnsctn_Dte", "TRNSCTN-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRNSCTN_DTE");
        cwf_Master_Trnsctn_Dte.setDdmHeader("TRANSACTION/DATE");
        cwf_Master_Trans_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Trans_Dte", "TRANS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cwf_Master_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Physcl_Fldr_Id_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Mj_Chrge_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Chrge_Dte_Tme", "MJ-CHRGE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "MJ_CHRGE_DTE_TME");
        cwf_Master_Mj_Chrge_Dte_Tme.setDdmHeader("CHARGEOUT/DATE-TIME");
        cwf_Master_Mj_Chrge_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Chrge_Oprtr_Cde", "MJ-CHRGE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "MJ_CHRGE_OPRTR_CDE");
        cwf_Master_Mj_Chrge_Oprtr_Cde.setDdmHeader("CHARGEOUT/OPERATOR");
        cwf_Master_Mstr_Indx_Actn_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mstr_Indx_Actn_Cde", "MSTR-INDX-ACTN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "MSTR_INDX_ACTN_CDE");
        cwf_Master_Mstr_Indx_Actn_Cde.setDdmHeader("CHARGEOUT/ACTION CODE");
        cwf_Master_Mj_Pull_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MJ_PULL_IND");
        cwf_Master_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Final_Close_Out_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Final_Close_Out_Dte_Tme", "FINAL-CLOSE-OUT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_DTE_TME");
        cwf_Master_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        cwf_Master_Final_Close_Out_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Final_Close_Out_Oprtr_Cde", "FINAL-CLOSE-OUT-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_OPRTR_CDE");
        cwf_Master_Mj_Emrgncy_Rqst_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Emrgncy_Rqst_Dte_Tme", "MJ-EMRGNCY-RQST-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_DTE_TME");
        cwf_Master_Mj_Emrgncy_Rqst_Dte_Tme.setDdmHeader("EMERGENCY REQ/DATE-TIME");
        cwf_Master_Mj_Emrgncy_Rqst_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Emrgncy_Rqst_Oprtr_Cde", "MJ-EMRGNCY-RQST-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_OPRTR_CDE");
        cwf_Master_Mj_Emrgncy_Rqst_Oprtr_Cde.setDdmHeader("EMERGENCY/REQUESTOR");
        cwf_Master_Print_Q_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Print_Q_Ind", "PRINT-Q-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "PRINT_Q_IND");
        cwf_Master_Print_Q_Ind.setDdmHeader("MJ PRINT/QUEUE");
        cwf_Master_Print_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Print_Dte_Tme", "PRINT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "PRINT_DTE_TME");
        cwf_Master_Print_Dte_Tme.setDdmHeader("MJ PRINT/DATE-TIME");
        cwf_Master_Print_Batch_Id_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Print_Batch_Id_Nbr", "PRINT-BATCH-ID-NBR", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "PRINT_BATCH_ID_NBR");
        cwf_Master_Print_Batch_Id_NbrRedef9 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Print_Batch_Id_NbrRedef9", "Redefines", cwf_Master_Print_Batch_Id_Nbr);
        cwf_Master_Print_Prefix_Ind = cwf_Master_Print_Batch_Id_NbrRedef9.newFieldInGroup("cwf_Master_Print_Prefix_Ind", "PRINT-PREFIX-IND", FieldType.STRING, 
            1);
        cwf_Master_Print_Batch_Nbr = cwf_Master_Print_Batch_Id_NbrRedef9.newFieldInGroup("cwf_Master_Print_Batch_Nbr", "PRINT-BATCH-NBR", FieldType.NUMERIC, 
            8);
        cwf_Master_Print_Batch_Sqnce_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Print_Batch_Sqnce_Nbr", "PRINT-BATCH-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRINT_BATCH_SQNCE_NBR");
        cwf_Master_Print_Batch_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Print_Batch_Ind", "PRINT-BATCH-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRINT_BATCH_IND");
        cwf_Master_Printer_Id_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Printer_Id_Cde", "PRINTER-ID-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "PRINTER_ID_CDE");
        cwf_Master_Rescan_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RESCAN_IND");
        cwf_Master_Rescan_Ind.setDdmHeader("RE-SCAN/INDICATOR");
        cwf_Master_Dup_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Dup_Ind", "DUP-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DUP_IND");
        cwf_Master_Dup_Ind.setDdmHeader("DUP/IND");
        cwf_Master_Cmplnt_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CMPLNT_IND");
        cwf_Master_Cntrct_NbrMuGroup = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Cntrct_NbrMuGroup", "CNTRCT_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Cntrct_Nbr = cwf_Master_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8, new DbsArrayController(1,10), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Mail_Item_NbrMuGroup = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Mail_Item_NbrMuGroup", "MAIL_ITEM_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CWF_MASTER_INDEX_MAIL_ITEM_NBR");
        cwf_Master_Mail_Item_Nbr = cwf_Master_Mail_Item_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Mail_Item_Nbr", "MAIL-ITEM-NBR", FieldType.STRING, 
            11, new DbsArrayController(1,10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "MAIL_ITEM_NBR");
        cwf_Master_Mail_Item_Nbr.setDdmHeader("MIN");
        cwf_Master_Rqst_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Id", "RQST-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, 
            "RQST_ID");
        cwf_Master_Rqst_Id.setDdmHeader("REQUEST ID");
        cwf_Master_Rqst_IdRedef10 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Rqst_IdRedef10", "Redefines", cwf_Master_Rqst_Id);
        cwf_Master_Rqst_Work_Prcss_Id = cwf_Master_Rqst_IdRedef10.newFieldInGroup("cwf_Master_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        cwf_Master_Rqst_Tiaa_Rcvd_Dte = cwf_Master_Rqst_IdRedef10.newFieldInGroup("cwf_Master_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", FieldType.STRING, 
            8);
        cwf_Master_Rqst_Case_Id_Cde = cwf_Master_Rqst_IdRedef10.newFieldInGroup("cwf_Master_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", FieldType.STRING, 2);
        cwf_Master_Rqst_Pin_Nbr = cwf_Master_Rqst_IdRedef10.newFieldInGroup("cwf_Master_Rqst_Pin_Nbr", "RQST-PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Master_Rlte_Rqst_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rlte_Rqst_Id", "RLTE-RQST-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, 
            "RLTE_RQST_ID");
        cwf_Master_Rlte_Rqst_IdRedef11 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Rlte_Rqst_IdRedef11", "Redefines", cwf_Master_Rlte_Rqst_Id);
        cwf_Master_Rlte_Work_Prcss_Id = cwf_Master_Rlte_Rqst_IdRedef11.newFieldInGroup("cwf_Master_Rlte_Work_Prcss_Id", "RLTE-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        cwf_Master_Rlte_Tiaa_Rcvd_Dte = cwf_Master_Rlte_Rqst_IdRedef11.newFieldInGroup("cwf_Master_Rlte_Tiaa_Rcvd_Dte", "RLTE-TIAA-RCVD-DTE", FieldType.STRING, 
            8);
        cwf_Master_Rlte_Case_Id_Cde = cwf_Master_Rlte_Rqst_IdRedef11.newFieldInGroup("cwf_Master_Rlte_Case_Id_Cde", "RLTE-CASE-ID-CDE", FieldType.STRING, 
            2);
        cwf_Master_Rlte_Pin_Nbr = cwf_Master_Rlte_Rqst_IdRedef11.newFieldInGroup("cwf_Master_Rlte_Pin_Nbr", "RLTE-PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Master_Extrnl_Pend_Rcv_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Work_List_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "WORK_LIST_IND");
        cwf_Master_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        cwf_Master_Due_Dte_Chg_Prty_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Due_Dte_Chg_Prty_Cde", "DUE-DTE-CHG-PRTY-CDE", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");
        cwf_Master_Due_Dte_Chg_Prty_CdeRedef12 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Due_Dte_Chg_Prty_CdeRedef12", "Redefines", cwf_Master_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Due_Dte = cwf_Master_Due_Dte_Chg_Prty_CdeRedef12.newFieldInGroup("cwf_Master_Due_Dte", "DUE-DTE", FieldType.STRING, 8);
        cwf_Master_Due_Dte_Chg_Ind = cwf_Master_Due_Dte_Chg_Prty_CdeRedef12.newFieldInGroup("cwf_Master_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", FieldType.STRING, 
            1);
        cwf_Master_Due_Dte_Prty = cwf_Master_Due_Dte_Chg_Prty_CdeRedef12.newFieldInGroup("cwf_Master_Due_Dte_Prty", "DUE-DTE-PRTY", FieldType.STRING, 
            1);
        cwf_Master_Prcss_Time_Ind = cwf_Master_Due_Dte_Chg_Prty_CdeRedef12.newFieldInGroup("cwf_Master_Prcss_Time_Ind", "PRCSS-TIME-IND", FieldType.STRING, 
            1);
        filler02 = cwf_Master_Due_Dte_Chg_Prty_CdeRedef12.newFieldInGroup("filler02", "FILLER", FieldType.STRING, 1);
        cwf_Master_Bypss_Mutual_Fund_Ind = cwf_Master_Due_Dte_Chg_Prty_CdeRedef12.newFieldInGroup("cwf_Master_Bypss_Mutual_Fund_Ind", "BYPSS-MUTUAL-FUND-IND", 
            FieldType.STRING, 1);
        cwf_Master_Sbsqnt_Cntct_Actn_Rqrd = cwf_Master_Due_Dte_Chg_Prty_CdeRedef12.newFieldInGroup("cwf_Master_Sbsqnt_Cntct_Actn_Rqrd", "SBSQNT-CNTCT-ACTN-RQRD", 
            FieldType.STRING, 1);
        cwf_Master_Sbsqnt_Cntct_Ind = cwf_Master_Due_Dte_Chg_Prty_CdeRedef12.newFieldInGroup("cwf_Master_Sbsqnt_Cntct_Ind", "SBSQNT-CNTCT-IND", FieldType.STRING, 
            1);
        cwf_Master_Prcssng_Type = cwf_Master_Due_Dte_Chg_Prty_CdeRedef12.newFieldInGroup("cwf_Master_Prcssng_Type", "PRCSSNG-TYPE", FieldType.STRING, 
            1);
        cwf_Master_Bsnss_Reply_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BSNSS_REPLY_IND");
        cwf_Master_Bsnss_Reply_Ind.setDdmHeader("BUSINESS/REPLY");
        cwf_Master_Status_Freeze_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Freeze_Ind", "STATUS-FREEZE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Elctrnc_Fldr_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        cwf_Master_Log_Insttn_Srce_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Log_Insttn_Srce_Cde", "LOG-INSTTN-SRCE-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "LOG_INSTTN_SRCE_CDE");
        cwf_Master_Log_Insttn_Srce_Cde.setDdmHeader("REQUESTOR/INSTITUTION");
        cwf_Master_Log_Rqstr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Log_Rqstr_Cde", "LOG-RQSTR-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LOG_RQSTR_CDE");
        cwf_Master_Log_Rqstr_Cde.setDdmHeader("REQUESTOR/TYPE");
        cwf_Master_Unit_Clock_Start_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Clock_Start_Dte_Tme", "UNIT-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Unit_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Clock_End_Dte_Tme", "UNIT-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Empl_Clock_Start_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Empl_Clock_Start_Dte_Tme", "EMPL-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        cwf_Master_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        cwf_Master_Empl_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Empl_Clock_End_Dte_Tme", "EMPL-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        cwf_Master_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        cwf_Master_Intrnl_Pnd_Start_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Intrnl_Pnd_Start_Dte_Tme", "INTRNL-PND-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_START_DTE_TME");
        cwf_Master_Intrnl_Pnd_Start_Dte_Tme.setDdmHeader("ACKNOWLEDGEMENT DATE TIME");
        cwf_Master_Intrnl_Pnd_Start_Dte_TmeRedef13 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Intrnl_Pnd_Start_Dte_TmeRedef13", "Redefines", 
            cwf_Master_Intrnl_Pnd_Start_Dte_Tme);
        cwf_Master_Intrnl_Pnd_Start_Dte = cwf_Master_Intrnl_Pnd_Start_Dte_TmeRedef13.newFieldInGroup("cwf_Master_Intrnl_Pnd_Start_Dte", "INTRNL-PND-START-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Intrnl_Pnd_Start_Tme = cwf_Master_Intrnl_Pnd_Start_Dte_TmeRedef13.newFieldInGroup("cwf_Master_Intrnl_Pnd_Start_Tme", "INTRNL-PND-START-TME", 
            FieldType.STRING, 7);
        cwf_Master_Intrnl_Pnd_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Intrnl_Pnd_End_Dte_Tme", "INTRNL-PND-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "INTRNL_PND_END_DTE_TME");
        cwf_Master_Intrnl_Pnd_End_Dte_Tme.setDdmHeader("INTERNAL PEND END DATE TIME");
        cwf_Master_Intrnl_Pnd_End_Dte_TmeRedef14 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Intrnl_Pnd_End_Dte_TmeRedef14", "Redefines", 
            cwf_Master_Intrnl_Pnd_End_Dte_Tme);
        cwf_Master_Intrnl_Pnd_End_Dte = cwf_Master_Intrnl_Pnd_End_Dte_TmeRedef14.newFieldInGroup("cwf_Master_Intrnl_Pnd_End_Dte", "INTRNL-PND-END-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Intrnl_Pnd_End_Tme = cwf_Master_Intrnl_Pnd_End_Dte_TmeRedef14.newFieldInGroup("cwf_Master_Intrnl_Pnd_End_Tme", "INTRNL-PND-END-TME", 
            FieldType.STRING, 7);
        cwf_Master_Intrnl_Pnd_Days = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Intrnl_Pnd_Days", "INTRNL-PND-DAYS", FieldType.PACKED_DECIMAL, 
            5, RepeatingFieldStrategy.None, "INTRNL_PND_DAYS");
        cwf_Master_Intrnl_Pnd_Days.setDdmHeader("INTERNAL PEND DAYS");
        cwf_Master_Step_Clock_Start_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Clock_Start_Dte_Tme", "STEP-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        cwf_Master_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        cwf_Master_Step_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Clock_End_Dte_Tme", "STEP-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        cwf_Master_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        cwf_Master_Crprte_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        cwf_Master_Crprte_Clock_End_Dte_TmeRedef15 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Crprte_Clock_End_Dte_TmeRedef15", "Redefines", 
            cwf_Master_Crprte_Clock_End_Dte_Tme);
        cwf_Master_Crprte_Clock_End_Dte = cwf_Master_Crprte_Clock_End_Dte_TmeRedef15.newFieldInGroup("cwf_Master_Crprte_Clock_End_Dte", "CRPRTE-CLOCK-END-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Crprte_Clock_End_Tme = cwf_Master_Crprte_Clock_End_Dte_TmeRedef15.newFieldInGroup("cwf_Master_Crprte_Clock_End_Tme", "CRPRTE-CLOCK-END-TME", 
            FieldType.STRING, 7);
        cwf_Master_Unit_En_Rte_To_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_En_Rte_To_Dte_Tme", "UNIT-EN-RTE-TO-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_EN_RTE_TO_DTE_TME");
        cwf_Master_Unit_En_Rte_To_Dte_Tme.setDdmHeader("UNIT EN ROUTE TO DATE TIME");
        cwf_Master_Unit_En_Rte_To_Dte_TmeRedef16 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Unit_En_Rte_To_Dte_TmeRedef16", "Redefines", 
            cwf_Master_Unit_En_Rte_To_Dte_Tme);
        cwf_Master_Unit_Begin_Status = cwf_Master_Unit_En_Rte_To_Dte_TmeRedef16.newFieldInGroup("cwf_Master_Unit_Begin_Status", "UNIT-BEGIN-STATUS", FieldType.STRING, 
            4);
        cwf_Master_Empl_Begin_Status = cwf_Master_Unit_En_Rte_To_Dte_TmeRedef16.newFieldInGroup("cwf_Master_Empl_Begin_Status", "EMPL-BEGIN-STATUS", FieldType.STRING, 
            4);
        cwf_Master_Step_Begin_Status = cwf_Master_Unit_En_Rte_To_Dte_TmeRedef16.newFieldInGroup("cwf_Master_Step_Begin_Status", "STEP-BEGIN-STATUS", FieldType.STRING, 
            4);
        cwf_Master_Acknwldgmnt_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Acknwldgmnt_Cde", "ACKNWLDGMNT-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "ACKNWLDGMNT_CDE");
        cwf_Master_Acknwldgmnt_Cde.setDdmHeader("ACKNOWLEDGEMENT/CODE");
        cwf_Master_Acknwldgmnt_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Acknwldgmnt_Oprtr_Cde", "ACKNWLDGMNT-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ACKNWLDGMNT_OPRTR_CDE");
        cwf_Master_Acknwldgmnt_Oprtr_Cde.setDdmHeader("ACKNOWLEDGED/BY");
        cwf_Master_Acknwldgmnt_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Acknwldgmnt_Dte_Tme", "ACKNWLDGMNT-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "ACKNWLDGMNT_DTE_TME");
        cwf_Master_Acknwldgmnt_Dte_Tme.setDdmHeader("ACKNOWLEDGED/ON");
        cwf_Master_Cntct_Sheet_Print_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Cntct_Sheet_Print_Cde", "CNTCT-SHEET-PRINT-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_CDE");
        cwf_Master_Cntct_Sheet_Print_Cde.setDdmHeader("CONTACT SHEET/PRINT CODE");
        cwf_Master_Cntct_Sheet_Print_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Cntct_Sheet_Print_Dte_Tme", "CNTCT-SHEET-PRINT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_DTE_TME");
        cwf_Master_Cntct_Sheet_Printer_Id_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Cntct_Sheet_Printer_Id_Cde", "CNTCT-SHEET-PRINTER-ID-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINTER_ID_CDE");
        cwf_Master_Shphrd_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Shphrd_Id", "SHPHRD-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "SHPHRD_ID");
        cwf_Master_Shphrd_Id.setDdmHeader("SHEPHERD");
        cwf_Master_Crrnt_Due_Dte_Cmt_Prty_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Crrnt_Due_Dte_Cmt_Prty_Tme", "CRRNT-DUE-DTE-CMT-PRTY-TME", 
            FieldType.STRING, 17, RepeatingFieldStrategy.None, "CRRNT_DUE_DTE_CMT_PRTY_TME");
        cwf_Master_Crrnt_Due_Dte_Cmt_Prty_Tme.setDdmHeader("CURR-DUE/TME-FIELD");
        cwf_Master_Crrnt_Due_Dte_Cmt_Prty_TmeRedef17 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Crrnt_Due_Dte_Cmt_Prty_TmeRedef17", "Redefines", 
            cwf_Master_Crrnt_Due_Dte_Cmt_Prty_Tme);
        cwf_Master_Crrnt_Due_Dte = cwf_Master_Crrnt_Due_Dte_Cmt_Prty_TmeRedef17.newFieldInGroup("cwf_Master_Crrnt_Due_Dte", "CRRNT-DUE-DTE", FieldType.STRING, 
            8);
        cwf_Master_Crrnt_Cmt_Ind = cwf_Master_Crrnt_Due_Dte_Cmt_Prty_TmeRedef17.newFieldInGroup("cwf_Master_Crrnt_Cmt_Ind", "CRRNT-CMT-IND", FieldType.STRING, 
            1);
        cwf_Master_Crrnt_Prrty_Cde = cwf_Master_Crrnt_Due_Dte_Cmt_Prty_TmeRedef17.newFieldInGroup("cwf_Master_Crrnt_Prrty_Cde", "CRRNT-PRRTY-CDE", FieldType.STRING, 
            1);
        cwf_Master_Crrnt_Due_Tme = cwf_Master_Crrnt_Due_Dte_Cmt_Prty_TmeRedef17.newFieldInGroup("cwf_Master_Crrnt_Due_Tme", "CRRNT-DUE-TME", FieldType.STRING, 
            7);
        cwf_Master_Crprte_Due_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRPRTE_DUE_DTE_TME");
        cwf_Master_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Master_Archvd_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Archvd_Dte", "ARCHVD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ARCHVD_DTE");
        cwf_Master_Archvd_Dte.setDdmHeader("ARCHIVED/DATE");
        cwf_Master_Rstr_To_Crrnt_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rstr_To_Crrnt_Dte", "RSTR-TO-CRRNT-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RSTR_TO_CRRNT_DTE");
        cwf_Master_Rstr_To_Crrnt_Dte.setDdmHeader("RESTORE/DATE");
        cwf_Master_Off_Rtng_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Off_Rtng_Ind", "OFF-RTNG-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "OFF_RTNG_IND");
        cwf_Master_Off_Rtng_Ind.setDdmHeader("OFF-ROUTING");
        cwf_Master_Unit_On_Tme_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_On_Tme_Ind", "UNIT-ON-TME-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "UNIT_ON_TME_IND");
        cwf_Master_Crprte_On_Tme_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRPRTE_ON_TME_IND");
        cwf_Master_Crprte_On_Tme_Ind.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Master_Owner_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Owner_Unit_Cde", "OWNER-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "OWNER_UNIT_CDE");
        cwf_Master_Owner_Unit_Cde.setDdmHeader("OWNERUNIT");
        cwf_Master_Physcl_Fldr_Owner_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Physcl_Fldr_Owner_Id", "PHYSCL-FLDR-OWNER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "PHYSCL_FLDR_OWNER_ID");
        cwf_Master_Physcl_Fldr_Owner_Id.setDdmHeader("PHYSICAL/FOLDER-OWNER");
        cwf_Master_Physcl_Fldr_Owner_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Physcl_Fldr_Owner_Unit_Cde", "PHYSCL-FLDR-OWNER-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PHYSCL_FLDR_OWNER_UNIT_CDE");
        cwf_Master_Physcl_Fldr_Owner_Unit_Cde.setDdmHeader("PHYSICAL/FOLDER-UNIT");
        cwf_Master_Step_Re_Do_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Re_Do_Ind", "STEP-RE-DO-IND", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "STEP_RE_DO_IND");
        cwf_Master_Step_Re_Do_Ind.setDdmHeader("REDO/COUNT");
        cwf_Master_Tiaa_Rcvd_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        cwf_Master_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        cwf_Master_Trade_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Trade_Dte_Tme", "TRADE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRADE_DTE_TME");
        cwf_Master_Trade_Dte_Tme.setDdmHeader("TRADE/DTE-TME");
        cwf_Master_Future_Pymnt_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Future_Pymnt_Ind", "FUTURE-PYMNT-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "FUTURE_PYMNT_IND");
        cwf_Master_Future_Pymnt_Ind.setDdmHeader("ACKNOWLEDGEMENT/PAYMENT-DTE");
        cwf_Master_Status_Clock_Start_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Clock_Start_Dte_Tme", "STATUS-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STATUS_CLOCK_START_DTE_TME");
        cwf_Master_Status_Clock_Start_Dte_Tme.setDdmHeader("STATUS/START CLOCK");
        cwf_Master_Status_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Clock_End_Dte_Tme", "STATUS-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STATUS_CLOCK_END_DTE_TME");
        cwf_Master_Status_Clock_End_Dte_Tme.setDdmHeader("STATUS/CLOCK END");
        cwf_Master_Status_Elpsd_Clndr_Days_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Elpsd_Clndr_Days_Tme", "STATUS-ELPSD-CLNDR-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "STATUS_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Status_Elpsd_Clndr_Days_Tme.setDdmHeader("STATUS ACTIVITY/TURNAROUND");
        cwf_Master_Status_Elpsd_Clndr_Days_TmeRedef18 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Status_Elpsd_Clndr_Days_TmeRedef18", "Redefines", 
            cwf_Master_Status_Elpsd_Clndr_Days_Tme);
        cwf_Master_Status_Elpsd_Clndr_Days = cwf_Master_Status_Elpsd_Clndr_Days_TmeRedef18.newFieldInGroup("cwf_Master_Status_Elpsd_Clndr_Days", "STATUS-ELPSD-CLNDR-DAYS", 
            FieldType.NUMERIC, 3);
        cwf_Master_Status_Elpsd_Clndr_Hours = cwf_Master_Status_Elpsd_Clndr_Days_TmeRedef18.newFieldInGroup("cwf_Master_Status_Elpsd_Clndr_Hours", "STATUS-ELPSD-CLNDR-HOURS", 
            FieldType.NUMERIC, 2);
        cwf_Master_Status_Elpsd_Clndr_Minutes = cwf_Master_Status_Elpsd_Clndr_Days_TmeRedef18.newFieldInGroup("cwf_Master_Status_Elpsd_Clndr_Minutes", 
            "STATUS-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Status_Elpsd_Bsnss_Days_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Elpsd_Bsnss_Days_Tme", "STATUS-ELPSD-BSNSS-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "STATUS_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Status_Elpsd_Bsnss_Days_Tme.setDdmHeader("STATUS/(BUSINESS DAYS)");
        cwf_Master_Status_Elpsd_Bsnss_Days_TmeRedef19 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Status_Elpsd_Bsnss_Days_TmeRedef19", "Redefines", 
            cwf_Master_Status_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Status_Elpsd_Bsnss_Days = cwf_Master_Status_Elpsd_Bsnss_Days_TmeRedef19.newFieldInGroup("cwf_Master_Status_Elpsd_Bsnss_Days", "STATUS-ELPSD-BSNSS-DAYS", 
            FieldType.NUMERIC, 3);
        cwf_Master_Status_Elpsd_Bsnss_Hours = cwf_Master_Status_Elpsd_Bsnss_Days_TmeRedef19.newFieldInGroup("cwf_Master_Status_Elpsd_Bsnss_Hours", "STATUS-ELPSD-BSNSS-HOURS", 
            FieldType.NUMERIC, 2);
        cwf_Master_Status_Elpsd_Bsnss_Minutes = cwf_Master_Status_Elpsd_Bsnss_Days_TmeRedef19.newFieldInGroup("cwf_Master_Status_Elpsd_Bsnss_Minutes", 
            "STATUS-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Step_Elpsd_Clndr_Days_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Elpsd_Clndr_Days_Tme", "STEP-ELPSD-CLNDR-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "STEP_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Step_Elpsd_Clndr_Days_Tme.setDdmHeader("STEP WAIT TIME");
        cwf_Master_Step_Elpsd_Clndr_Days_TmeRedef20 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Step_Elpsd_Clndr_Days_TmeRedef20", "Redefines", 
            cwf_Master_Step_Elpsd_Clndr_Days_Tme);
        cwf_Master_Step_Elpsd_Clndr_Days = cwf_Master_Step_Elpsd_Clndr_Days_TmeRedef20.newFieldInGroup("cwf_Master_Step_Elpsd_Clndr_Days", "STEP-ELPSD-CLNDR-DAYS", 
            FieldType.NUMERIC, 3);
        cwf_Master_Step_Elpsd_Clndr_Hours = cwf_Master_Step_Elpsd_Clndr_Days_TmeRedef20.newFieldInGroup("cwf_Master_Step_Elpsd_Clndr_Hours", "STEP-ELPSD-CLNDR-HOURS", 
            FieldType.NUMERIC, 2);
        cwf_Master_Step_Elpsd_Clndr_Minutes = cwf_Master_Step_Elpsd_Clndr_Days_TmeRedef20.newFieldInGroup("cwf_Master_Step_Elpsd_Clndr_Minutes", "STEP-ELPSD-CLNDR-MINUTES", 
            FieldType.NUMERIC, 2);
        cwf_Master_Step_Elpsd_Bsnss_Days_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Elpsd_Bsnss_Days_Tme", "STEP-ELPSD-BSNSS-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "STEP_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Step_Elpsd_Bsnss_Days_Tme.setDdmHeader("STEP PROCESS TIME");
        cwf_Master_Step_Elpsd_Bsnss_Days_TmeRedef21 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Step_Elpsd_Bsnss_Days_TmeRedef21", "Redefines", 
            cwf_Master_Step_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Step_Elpsd_Bsnss_Days = cwf_Master_Step_Elpsd_Bsnss_Days_TmeRedef21.newFieldInGroup("cwf_Master_Step_Elpsd_Bsnss_Days", "STEP-ELPSD-BSNSS-DAYS", 
            FieldType.NUMERIC, 3);
        cwf_Master_Step_Elpsd_Bsnss_Hours = cwf_Master_Step_Elpsd_Bsnss_Days_TmeRedef21.newFieldInGroup("cwf_Master_Step_Elpsd_Bsnss_Hours", "STEP-ELPSD-BSNSS-HOURS", 
            FieldType.NUMERIC, 2);
        cwf_Master_Step_Elpsd_Bsnss_Minutes = cwf_Master_Step_Elpsd_Bsnss_Days_TmeRedef21.newFieldInGroup("cwf_Master_Step_Elpsd_Bsnss_Minutes", "STEP-ELPSD-BSNSS-MINUTES", 
            FieldType.NUMERIC, 2);
        cwf_Master_Empl_Elpsd_Clndr_Days_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Empl_Elpsd_Clndr_Days_Tme", "EMPL-ELPSD-CLNDR-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "EMPL_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Empl_Elpsd_Clndr_Days_Tme.setDdmHeader("ASSOCIATE WAIT TIME");
        cwf_Master_Empl_Elpsd_Clndr_Days_TmeRedef22 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Empl_Elpsd_Clndr_Days_TmeRedef22", "Redefines", 
            cwf_Master_Empl_Elpsd_Clndr_Days_Tme);
        cwf_Master_Empl_Elpsd_Clndr_Days = cwf_Master_Empl_Elpsd_Clndr_Days_TmeRedef22.newFieldInGroup("cwf_Master_Empl_Elpsd_Clndr_Days", "EMPL-ELPSD-CLNDR-DAYS", 
            FieldType.NUMERIC, 3);
        cwf_Master_Empl_Elpsd_Clndr_Hours = cwf_Master_Empl_Elpsd_Clndr_Days_TmeRedef22.newFieldInGroup("cwf_Master_Empl_Elpsd_Clndr_Hours", "EMPL-ELPSD-CLNDR-HOURS", 
            FieldType.NUMERIC, 2);
        cwf_Master_Empl_Elpsd_Clndr_Minutes = cwf_Master_Empl_Elpsd_Clndr_Days_TmeRedef22.newFieldInGroup("cwf_Master_Empl_Elpsd_Clndr_Minutes", "EMPL-ELPSD-CLNDR-MINUTES", 
            FieldType.NUMERIC, 2);
        cwf_Master_Empl_Elpsd_Bsnss_Days_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Empl_Elpsd_Bsnss_Days_Tme", "EMPL-ELPSD-BSNSS-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "EMPL_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Empl_Elpsd_Bsnss_Days_Tme.setDdmHeader("ASSOCIATE PROCESS TIME");
        cwf_Master_Empl_Elpsd_Bsnss_Days_TmeRedef23 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Empl_Elpsd_Bsnss_Days_TmeRedef23", "Redefines", 
            cwf_Master_Empl_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Empl_Elpsd_Bsnss_Days = cwf_Master_Empl_Elpsd_Bsnss_Days_TmeRedef23.newFieldInGroup("cwf_Master_Empl_Elpsd_Bsnss_Days", "EMPL-ELPSD-BSNSS-DAYS", 
            FieldType.NUMERIC, 3);
        cwf_Master_Empl_Elpsd_Bsnss_Hours = cwf_Master_Empl_Elpsd_Bsnss_Days_TmeRedef23.newFieldInGroup("cwf_Master_Empl_Elpsd_Bsnss_Hours", "EMPL-ELPSD-BSNSS-HOURS", 
            FieldType.NUMERIC, 2);
        cwf_Master_Empl_Elpsd_Bsnss_Minutes = cwf_Master_Empl_Elpsd_Bsnss_Days_TmeRedef23.newFieldInGroup("cwf_Master_Empl_Elpsd_Bsnss_Minutes", "EMPL-ELPSD-BSNSS-MINUTES", 
            FieldType.NUMERIC, 2);
        cwf_Master_Unit_Elpsd_Clndr_Days_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Elpsd_Clndr_Days_Tme", "UNIT-ELPSD-CLNDR-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "UNIT_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Unit_Elpsd_Clndr_Days_Tme.setDdmHeader("UNIT WAIT TIME");
        cwf_Master_Unit_Elpsd_Clndr_Days_TmeRedef24 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Unit_Elpsd_Clndr_Days_TmeRedef24", "Redefines", 
            cwf_Master_Unit_Elpsd_Clndr_Days_Tme);
        cwf_Master_Unit_Elpsd_Clndr_Days = cwf_Master_Unit_Elpsd_Clndr_Days_TmeRedef24.newFieldInGroup("cwf_Master_Unit_Elpsd_Clndr_Days", "UNIT-ELPSD-CLNDR-DAYS", 
            FieldType.NUMERIC, 3);
        cwf_Master_Unit_Elpsd_Clndr_Hours = cwf_Master_Unit_Elpsd_Clndr_Days_TmeRedef24.newFieldInGroup("cwf_Master_Unit_Elpsd_Clndr_Hours", "UNIT-ELPSD-CLNDR-HOURS", 
            FieldType.NUMERIC, 2);
        cwf_Master_Unit_Elpsd_Clndr_Minutes = cwf_Master_Unit_Elpsd_Clndr_Days_TmeRedef24.newFieldInGroup("cwf_Master_Unit_Elpsd_Clndr_Minutes", "UNIT-ELPSD-CLNDR-MINUTES", 
            FieldType.NUMERIC, 2);
        cwf_Master_Unit_Elpsd_Bsnss_Days_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Elpsd_Bsnss_Days_Tme", "UNIT-ELPSD-BSNSS-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "UNIT_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Unit_Elpsd_Bsnss_Days_Tme.setDdmHeader("UNIT PROCESS TIME");
        cwf_Master_Unit_Elpsd_Bsnss_Days_TmeRedef25 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Unit_Elpsd_Bsnss_Days_TmeRedef25", "Redefines", 
            cwf_Master_Unit_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Unit_Elpsd_Bsnss_Days = cwf_Master_Unit_Elpsd_Bsnss_Days_TmeRedef25.newFieldInGroup("cwf_Master_Unit_Elpsd_Bsnss_Days", "UNIT-ELPSD-BSNSS-DAYS", 
            FieldType.NUMERIC, 3);
        cwf_Master_Unit_Elpsd_Bsnss_Hours = cwf_Master_Unit_Elpsd_Bsnss_Days_TmeRedef25.newFieldInGroup("cwf_Master_Unit_Elpsd_Bsnss_Hours", "UNIT-ELPSD-BSNSS-HOURS", 
            FieldType.NUMERIC, 2);
        cwf_Master_Unit_Elpsd_Bsnss_Minutes = cwf_Master_Unit_Elpsd_Bsnss_Days_TmeRedef25.newFieldInGroup("cwf_Master_Unit_Elpsd_Bsnss_Minutes", "UNIT-ELPSD-BSNSS-MINUTES", 
            FieldType.NUMERIC, 2);
        cwf_Master_Intrnl_Pnd_Clndr_Days_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Intrnl_Pnd_Clndr_Days_Tme", "INTRNL-PND-CLNDR-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "INTRNL_PND_CLNDR_DAYS_TME");
        cwf_Master_Intrnl_Pnd_Clndr_Days_Tme.setDdmHeader("INTERNAL-PEND/(CALENDAR DAYS)");
        cwf_Master_Intrnl_Pnd_Clndr_Days_TmeRedef26 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Intrnl_Pnd_Clndr_Days_TmeRedef26", "Redefines", 
            cwf_Master_Intrnl_Pnd_Clndr_Days_Tme);
        cwf_Master_Intrnl_Pnd_Elpsd_Clndr_Days = cwf_Master_Intrnl_Pnd_Clndr_Days_TmeRedef26.newFieldInGroup("cwf_Master_Intrnl_Pnd_Elpsd_Clndr_Days", 
            "INTRNL-PND-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Intrnl_Pnd_Elpsd_Clndr_Hours = cwf_Master_Intrnl_Pnd_Clndr_Days_TmeRedef26.newFieldInGroup("cwf_Master_Intrnl_Pnd_Elpsd_Clndr_Hours", 
            "INTRNL-PND-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Intrnl_Pnd_Elpsd_Clndr_Minutes = cwf_Master_Intrnl_Pnd_Clndr_Days_TmeRedef26.newFieldInGroup("cwf_Master_Intrnl_Pnd_Elpsd_Clndr_Minutes", 
            "INTRNL-PND-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Intrnl_Pnd_Bsnss_Days_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Intrnl_Pnd_Bsnss_Days_Tme", "INTRNL-PND-BSNSS-DAYS-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "INTRNL_PND_BSNSS_DAYS_TME");
        cwf_Master_Intrnl_Pnd_Bsnss_Days_Tme.setDdmHeader("INTERNAL PEND/(BUSSINESS DAYS)");
        cwf_Master_Intrnl_Pnd_Bsnss_Days_TmeRedef27 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master_Intrnl_Pnd_Bsnss_Days_TmeRedef27", "Redefines", 
            cwf_Master_Intrnl_Pnd_Bsnss_Days_Tme);
        cwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Days = cwf_Master_Intrnl_Pnd_Bsnss_Days_TmeRedef27.newFieldInGroup("cwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Days", 
            "INTRNL-PND-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Hours = cwf_Master_Intrnl_Pnd_Bsnss_Days_TmeRedef27.newFieldInGroup("cwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Hours", 
            "INTRNL-PND-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Minutes = cwf_Master_Intrnl_Pnd_Bsnss_Days_TmeRedef27.newFieldInGroup("cwf_Master_Intrnl_Pnd_Elpsd_Bsnss_Minutes", 
            "INTRNL-PND-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Cnnctd_Rqst_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Cnnctd_Rqst_Ind", "CNNCTD-RQST-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNNCTD_RQST_IND");
        cwf_Master_Cnnctd_Rqst_Ind.setDdmHeader("CONNECTED RQST IND");
        cwf_Master_Prvte_Wrk_Rqst_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Prvte_Wrk_Rqst_Ind", "PRVTE-WRK-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRVTE_WRK_RQST_IND");
        cwf_Master_Prvte_Wrk_Rqst_Ind.setDdmHeader("PRIVATE WORK REQUEST INDICATOR");
        cwf_Master_Instn_Rqst_Log_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Instn_Rqst_Log_Dte_Tme", "INSTN-RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "INSTN_RQST_LOG_DTE_TME");
        cwf_Master_Instn_Rqst_Log_Dte_Tme.setDdmHeader("GUARDIAN INST RQST LOG DTE/TME");
        cwf_Master_Log_Sqnce_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Log_Sqnce_Nbr", "LOG-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "LOG_SQNCE_NBR");
        cwf_Master_Log_Sqnce_Nbr.setDdmHeader("LOG SEQUENCE NUMBER");

        this.setRecordName("LdaCwfl4817");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cwf_Master.reset();
    }

    // Constructor
    public LdaCwfl4817() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
