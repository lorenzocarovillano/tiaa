/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:46 PM
**        * FROM NATURAL LDA     : CWFL4820
************************************************************
**        * FILE NAME            : LdaCwfl4820.java
**        * CLASS NAME           : LdaCwfl4820
**        * INSTANCE NAME        : LdaCwfl4820
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4820 extends DbsRecord
{
    // Properties
    private DbsGroup activity;
    private DbsField activity_Rqst_Log_Dte_Tme_A;
    private DbsField activity_A1;
    private DbsField activity_Strt_Dte_Tme_A;
    private DbsField activity_A2;
    private DbsField activity_Stop_Dte_Tme_A;
    private DbsField activity_A3;
    private DbsField activity_Prcss_Time;
    private DbsField activity_A4;
    private DbsField activity_Wait_Time;
    private DbsField activity_A5;
    private DbsField activity_Admin_Unit_Cde;
    private DbsField activity_A6;
    private DbsField activity_Status_Cde;
    private DbsField activity_A7;
    private DbsField activity_Old_Route_Cde;
    private DbsField activity_A8;
    private DbsField activity_Rt_Sqnce_Nbr;
    private DbsField activity_A9;
    private DbsField activity_Empl_Oprtr_Cde;
    private DbsField activity_A10;
    private DbsField activity_Step_Id;

    public DbsGroup getActivity() { return activity; }

    public DbsField getActivity_Rqst_Log_Dte_Tme_A() { return activity_Rqst_Log_Dte_Tme_A; }

    public DbsField getActivity_A1() { return activity_A1; }

    public DbsField getActivity_Strt_Dte_Tme_A() { return activity_Strt_Dte_Tme_A; }

    public DbsField getActivity_A2() { return activity_A2; }

    public DbsField getActivity_Stop_Dte_Tme_A() { return activity_Stop_Dte_Tme_A; }

    public DbsField getActivity_A3() { return activity_A3; }

    public DbsField getActivity_Prcss_Time() { return activity_Prcss_Time; }

    public DbsField getActivity_A4() { return activity_A4; }

    public DbsField getActivity_Wait_Time() { return activity_Wait_Time; }

    public DbsField getActivity_A5() { return activity_A5; }

    public DbsField getActivity_Admin_Unit_Cde() { return activity_Admin_Unit_Cde; }

    public DbsField getActivity_A6() { return activity_A6; }

    public DbsField getActivity_Status_Cde() { return activity_Status_Cde; }

    public DbsField getActivity_A7() { return activity_A7; }

    public DbsField getActivity_Old_Route_Cde() { return activity_Old_Route_Cde; }

    public DbsField getActivity_A8() { return activity_A8; }

    public DbsField getActivity_Rt_Sqnce_Nbr() { return activity_Rt_Sqnce_Nbr; }

    public DbsField getActivity_A9() { return activity_A9; }

    public DbsField getActivity_Empl_Oprtr_Cde() { return activity_Empl_Oprtr_Cde; }

    public DbsField getActivity_A10() { return activity_A10; }

    public DbsField getActivity_Step_Id() { return activity_Step_Id; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        activity = newGroupInRecord("activity", "ACTIVITY");
        activity_Rqst_Log_Dte_Tme_A = activity.newFieldInGroup("activity_Rqst_Log_Dte_Tme_A", "RQST-LOG-DTE-TME-A", FieldType.STRING, 28);
        activity_A1 = activity.newFieldInGroup("activity_A1", "A1", FieldType.STRING, 1);
        activity_Strt_Dte_Tme_A = activity.newFieldInGroup("activity_Strt_Dte_Tme_A", "STRT-DTE-TME-A", FieldType.STRING, 28);
        activity_A2 = activity.newFieldInGroup("activity_A2", "A2", FieldType.STRING, 1);
        activity_Stop_Dte_Tme_A = activity.newFieldInGroup("activity_Stop_Dte_Tme_A", "STOP-DTE-TME-A", FieldType.STRING, 28);
        activity_A3 = activity.newFieldInGroup("activity_A3", "A3", FieldType.STRING, 1);
        activity_Prcss_Time = activity.newFieldInGroup("activity_Prcss_Time", "PRCSS-TIME", FieldType.STRING, 7);
        activity_A4 = activity.newFieldInGroup("activity_A4", "A4", FieldType.STRING, 1);
        activity_Wait_Time = activity.newFieldInGroup("activity_Wait_Time", "WAIT-TIME", FieldType.STRING, 7);
        activity_A5 = activity.newFieldInGroup("activity_A5", "A5", FieldType.STRING, 1);
        activity_Admin_Unit_Cde = activity.newFieldInGroup("activity_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        activity_A6 = activity.newFieldInGroup("activity_A6", "A6", FieldType.STRING, 1);
        activity_Status_Cde = activity.newFieldInGroup("activity_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        activity_A7 = activity.newFieldInGroup("activity_A7", "A7", FieldType.STRING, 1);
        activity_Old_Route_Cde = activity.newFieldInGroup("activity_Old_Route_Cde", "OLD-ROUTE-CDE", FieldType.STRING, 4);
        activity_A8 = activity.newFieldInGroup("activity_A8", "A8", FieldType.STRING, 1);
        activity_Rt_Sqnce_Nbr = activity.newFieldInGroup("activity_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 2);
        activity_A9 = activity.newFieldInGroup("activity_A9", "A9", FieldType.STRING, 1);
        activity_Empl_Oprtr_Cde = activity.newFieldInGroup("activity_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 8);
        activity_A10 = activity.newFieldInGroup("activity_A10", "A10", FieldType.STRING, 1);
        activity_Step_Id = activity.newFieldInGroup("activity_Step_Id", "STEP-ID", FieldType.STRING, 6);

        this.setRecordName("LdaCwfl4820");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4820() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
