/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:56 PM
**        * FROM NATURAL LDA     : CWFL4830
************************************************************
**        * FILE NAME            : LdaCwfl4830.java
**        * CLASS NAME           : LdaCwfl4830
**        * INSTANCE NAME        : LdaCwfl4830
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4830 extends DbsRecord
{
    // Properties
    private DbsGroup tlctable;
    private DbsField tlctable_Cust_Code;
    private DbsField tlctable_D1;
    private DbsField tlctable_Tlc_Code;
    private DbsField tlctable_D2;
    private DbsField tlctable_Tlc_Description;

    public DbsGroup getTlctable() { return tlctable; }

    public DbsField getTlctable_Cust_Code() { return tlctable_Cust_Code; }

    public DbsField getTlctable_D1() { return tlctable_D1; }

    public DbsField getTlctable_Tlc_Code() { return tlctable_Tlc_Code; }

    public DbsField getTlctable_D2() { return tlctable_D2; }

    public DbsField getTlctable_Tlc_Description() { return tlctable_Tlc_Description; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        tlctable = newGroupInRecord("tlctable", "TLCTABLE");
        tlctable_Cust_Code = tlctable.newFieldInGroup("tlctable_Cust_Code", "CUST-CODE", FieldType.STRING, 12);
        tlctable_D1 = tlctable.newFieldInGroup("tlctable_D1", "D1", FieldType.STRING, 1);
        tlctable_Tlc_Code = tlctable.newFieldInGroup("tlctable_Tlc_Code", "TLC-CODE", FieldType.STRING, 2);
        tlctable_D2 = tlctable.newFieldInGroup("tlctable_D2", "D2", FieldType.STRING, 1);
        tlctable_Tlc_Description = tlctable.newFieldInGroup("tlctable_Tlc_Description", "TLC-DESCRIPTION", FieldType.STRING, 35);

        this.setRecordName("LdaCwfl4830");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4830() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
