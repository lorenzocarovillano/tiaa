/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:03:02 PM
**        * FROM NATURAL LDA     : IAAL9125
************************************************************
**        * FILE NAME            : LdaIaal9125.java
**        * CLASS NAME           : LdaIaal9125
**        * INSTANCE NAME        : LdaIaal9125
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal9125 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Todays_Dte;
    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;
    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Ssnng_Dte;
    private DbsField iaa_Cntrct_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_Plan_Nmbr;
    private DbsField iaa_Cntrct_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Orgntng_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Orgntng_Cntrct_Nmbr;
    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Trans_Trans_Check_Dte;
    private DbsField iaa_Cntrct_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ssnng_Dte;
    private DbsField iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_Trans_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_Trans_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_Trans_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_Trans_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_Trans_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr;
    private DbsField iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr;
    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cpr_Id_Nbr;
    private DbsField cpr_Lst_Trans_Dte;
    private DbsField cpr_Prtcpnt_Ctznshp_Cde;
    private DbsField cpr_Prtcpnt_Rsdncy_Cde;
    private DbsField cpr_Prtcpnt_Rsdncy_Sw;
    private DbsField cpr_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr_Prtcpnt_Tax_Id_Typ;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Cntrct_Trmnte_Rsn;
    private DbsField cpr_Cntrct_Rwrttn_Ind;
    private DbsField cpr_Cntrct_Cash_Cde;
    private DbsField cpr_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsGroup cpr_Cntrct_Company_Data;
    private DbsField cpr_Cntrct_Company_Cd;
    private DbsField cpr_Cntrct_Rcvry_Type_Ind;
    private DbsField cpr_Cntrct_Per_Ivc_Amt;
    private DbsField cpr_Cntrct_Resdl_Ivc_Amt;
    private DbsField cpr_Cntrct_Ivc_Amt;
    private DbsField cpr_Cntrct_Ivc_Used_Amt;
    private DbsField cpr_Cntrct_Rtb_Amt;
    private DbsField cpr_Cntrct_Rtb_Percent;
    private DbsField cpr_Cntrct_Mode_Ind;
    private DbsField cpr_Cntrct_Wthdrwl_Dte;
    private DbsField cpr_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr_Cntrct_Final_Pay_Dte;
    private DbsField cpr_Bnfcry_Xref_Ind;
    private DbsField cpr_Bnfcry_Dod_Dte;
    private DbsField cpr_Cntrct_Pend_Cde;
    private DbsField cpr_Cntrct_Hold_Cde;
    private DbsField cpr_Cntrct_Pend_Dte;
    private DbsField cpr_Cntrct_Prev_Dist_Cde;
    private DbsField cpr_Cntrct_Curr_Dist_Cde;
    private DbsField cpr_Cntrct_Cmbne_Cde;
    private DbsField cpr_Cntrct_Spirt_Cde;
    private DbsField cpr_Cntrct_Spirt_Amt;
    private DbsField cpr_Cntrct_Spirt_Srce;
    private DbsField cpr_Cntrct_Spirt_Arr_Dte;
    private DbsField cpr_Cntrct_Spirt_Prcss_Dte;
    private DbsField cpr_Cntrct_Fed_Tax_Amt;
    private DbsField cpr_Cntrct_State_Cde;
    private DbsField cpr_Cntrct_State_Tax_Amt;
    private DbsField cpr_Cntrct_Local_Cde;
    private DbsField cpr_Cntrct_Local_Tax_Amt;
    private DbsField cpr_Cntrct_Lst_Chnge_Dte;
    private DbsField cpr_Cpr_Xfr_Term_Cde;
    private DbsField cpr_Cpr_Lgl_Res_Cde;
    private DbsField cpr_Cpr_Xfr_Iss_Dte;
    private DbsField cpr_Rllvr_Cntrct_Nbr;
    private DbsField cpr_Rllvr_Ivc_Ind;
    private DbsField cpr_Rllvr_Elgble_Ind;
    private DbsGroup cpr_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField cpr_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField cpr_Rllvr_Accptng_Irc_Cde;
    private DbsField cpr_Rllvr_Pln_Admn_Ind;
    private DbsField cpr_Roth_Dsblty_Dte;
    private DataAccessProgramView vw_cpr_Trans;
    private DbsField cpr_Trans_Trans_Dte;
    private DbsField cpr_Trans_Invrse_Trans_Dte;
    private DbsField cpr_Trans_Lst_Trans_Dte;
    private DbsField cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Trans_Cpr_Id_Nbr;
    private DbsField cpr_Trans_Prtcpnt_Ctznshp_Cde;
    private DbsField cpr_Trans_Prtcpnt_Rsdncy_Cde;
    private DbsField cpr_Trans_Prtcpnt_Rsdncy_Sw;
    private DbsField cpr_Trans_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr_Trans_Prtcpnt_Tax_Id_Typ;
    private DbsField cpr_Trans_Cntrct_Actvty_Cde;
    private DbsField cpr_Trans_Cntrct_Trmnte_Rsn;
    private DbsField cpr_Trans_Cntrct_Rwrttn_Ind;
    private DbsField cpr_Trans_Cntrct_Cash_Cde;
    private DbsField cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsGroup cpr_Trans_Cntrct_Company_Data;
    private DbsField cpr_Trans_Cntrct_Company_Cd;
    private DbsField cpr_Trans_Cntrct_Rcvry_Type_Ind;
    private DbsField cpr_Trans_Cntrct_Per_Ivc_Amt;
    private DbsField cpr_Trans_Cntrct_Resdl_Ivc_Amt;
    private DbsField cpr_Trans_Cntrct_Ivc_Amt;
    private DbsField cpr_Trans_Cntrct_Ivc_Used_Amt;
    private DbsField cpr_Trans_Cntrct_Rtb_Amt;
    private DbsField cpr_Trans_Cntrct_Rtb_Percent;
    private DbsField cpr_Trans_Cntrct_Mode_Ind;
    private DbsField cpr_Trans_Cntrct_Wthdrwl_Dte;
    private DbsField cpr_Trans_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr_Trans_Cntrct_Final_Pay_Dte;
    private DbsField cpr_Trans_Bnfcry_Xref_Ind;
    private DbsField cpr_Trans_Bnfcry_Dod_Dte;
    private DbsField cpr_Trans_Cntrct_Pend_Cde;
    private DbsField cpr_Trans_Cntrct_Hold_Cde;
    private DbsField cpr_Trans_Cntrct_Pend_Dte;
    private DbsField cpr_Trans_Cntrct_Prev_Dist_Cde;
    private DbsField cpr_Trans_Cntrct_Curr_Dist_Cde;
    private DbsField cpr_Trans_Cntrct_Cmbne_Cde;
    private DbsField cpr_Trans_Cntrct_Spirt_Cde;
    private DbsField cpr_Trans_Cntrct_Spirt_Amt;
    private DbsField cpr_Trans_Cntrct_Spirt_Srce;
    private DbsField cpr_Trans_Cntrct_Spirt_Arr_Dte;
    private DbsField cpr_Trans_Cntrct_Spirt_Prcss_Dte;
    private DbsField cpr_Trans_Cntrct_Fed_Tax_Amt;
    private DbsField cpr_Trans_Cntrct_State_Cde;
    private DbsField cpr_Trans_Cntrct_State_Tax_Amt;
    private DbsField cpr_Trans_Cntrct_Local_Cde;
    private DbsField cpr_Trans_Cntrct_Local_Tax_Amt;
    private DbsField cpr_Trans_Cntrct_Lst_Chnge_Dte;
    private DbsField cpr_Trans_Trans_Check_Dte;
    private DbsField cpr_Trans_Bfre_Imge_Id;
    private DbsField cpr_Trans_Aftr_Imge_Id;
    private DbsField cpr_Trans_Cpr_Xfr_Term_Cde;
    private DbsField cpr_Trans_Cpr_Lgl_Res_Cde;
    private DbsField cpr_Trans_Cpr_Xfr_Iss_Dte;
    private DbsField cpr_Trans_Rllvr_Cntrct_Nbr;
    private DbsField cpr_Trans_Rllvr_Ivc_Ind;
    private DbsField cpr_Trans_Rllvr_Elgble_Ind;
    private DbsGroup cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField cpr_Trans_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField cpr_Trans_Rllvr_Accptng_Irc_Cde;
    private DbsField cpr_Trans_Rllvr_Pln_Admn_Ind;
    private DbsField cpr_Trans_Roth_Dsblty_Dte;
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund;
    private DbsField iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tckr_Symbl;
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Trans;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef2;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Check_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Bfre_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_Aftr_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tckr_Symbl;
    private DataAccessProgramView vw_iaa_Dc_Sttlmnt_Req;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Id_Nbr;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Tax_Id_Nbr;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Req_Seq_Nbr;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Process_Type;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Timestamp;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Status_Cde;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Status_Timestamp;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Cwf_Wpid;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Name;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Type;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Dod_Dte;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_2nd_Dod_Dte;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Name;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr1;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr2;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr3;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr4;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_City;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_State;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Zip;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Phone;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Notify_Date;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Rltnshp;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Cntrct_Count;
    private DbsField iaa_Dc_Sttlmnt_Req_Oper_Id;
    private DbsField iaa_Dc_Sttlmnt_Req_Oper_Timestamp;
    private DbsField iaa_Dc_Sttlmnt_Req_Oper_User_Grp;
    private DbsField iaa_Dc_Sttlmnt_Req_Verf_Id;
    private DbsField iaa_Dc_Sttlmnt_Req_Verf_Timestamp;
    private DbsField iaa_Dc_Sttlmnt_Req_Verf_User_Grp;
    private DbsField iaa_Dc_Sttlmnt_Req_Sttlmnt_Coding_Dte;
    private DataAccessProgramView vw_iaa_Dc_Cntrct;
    private DbsField iaa_Dc_Cntrct_Cntrct_Id_Nbr;
    private DbsField iaa_Dc_Cntrct_Cntrct_Tax_Id_Nbr;
    private DbsField iaa_Dc_Cntrct_Cntrct_Process_Type;
    private DbsField iaa_Dc_Cntrct_Cntrct_Req_Seq_Nbr;
    private DbsField iaa_Dc_Cntrct_Cntrct_Timestamp;
    private DbsField iaa_Dc_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Dc_Cntrct_Cntrct_Payee_Cde;
    private DbsField iaa_Dc_Cntrct_Cntrct_Status_Cde;
    private DbsField iaa_Dc_Cntrct_Cntrct_Status_Timestamp;
    private DbsField iaa_Dc_Cntrct_Cntrct_Error_Msg;
    private DbsField iaa_Dc_Cntrct_Cntrct_Annt_Type;
    private DbsField iaa_Dc_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Dc_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Dc_Cntrct_Count_Castcntrct_Sttlmnt_Info_Cde;
    private DbsGroup iaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_CdeMuGroup;
    private DbsField iaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_Cde;
    private DbsField iaa_Dc_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Dc_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Dc_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Dc_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Dc_Cntrct_Cntrct_Bnfcry_Xref_Ind;
    private DbsField iaa_Dc_Cntrct_Cntrct_Bnfcry_Dod_Dte;
    private DbsField iaa_Dc_Cntrct_Cntrct_Bnfcry_Grp_Nmbr;
    private DbsField iaa_Dc_Cntrct_Cntrct_Bnfcry_Grp_Seq;
    private DbsField iaa_Dc_Cntrct_Cntrct_Pmt_Data_Nmbr;
    private DbsField iaa_Dc_Cntrct_Cntrct_Bnfcry_Name;
    private DbsField iaa_Dc_Cntrct_Cntrct_Spirt_Code;
    private DbsField iaa_Dc_Cntrct_Cntrct_Delete_Code;
    private DbsField iaa_Dc_Cntrct_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Dc_Cntrct_Cntrct_Brkdwn_Pct;
    private DbsField iaa_Dc_Cntrct_Cntrct_Lump_Sum_Avail;
    private DbsField iaa_Dc_Cntrct_Cntrct_Cont_Pmt_Avail;
    private DbsField iaa_Dc_Cntrct_Cntrct_Pymnt_Mode;
    private DbsField iaa_Dc_Cntrct_Cntrct_Hist_Rate_Needed_Ind;
    private DbsField iaa_Dc_Cntrct_Cntrct_Hist_Rate_Entered_Ind;

    public DataAccessProgramView getVw_iaa_Cntrl_Rcrd() { return vw_iaa_Cntrl_Rcrd; }

    public DbsField getIaa_Cntrl_Rcrd_Cntrl_Check_Dte() { return iaa_Cntrl_Rcrd_Cntrl_Check_Dte; }

    public DbsField getIaa_Cntrl_Rcrd_Cntrl_Invrse_Dte() { return iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte; }

    public DbsField getIaa_Cntrl_Rcrd_Cntrl_Cde() { return iaa_Cntrl_Rcrd_Cntrl_Cde; }

    public DbsField getIaa_Cntrl_Rcrd_Cntrl_Todays_Dte() { return iaa_Cntrl_Rcrd_Cntrl_Todays_Dte; }

    public DataAccessProgramView getVw_iaa_Trans_Rcrd() { return vw_iaa_Trans_Rcrd; }

    public DbsField getIaa_Trans_Rcrd_Trans_Dte() { return iaa_Trans_Rcrd_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Invrse_Trans_Dte() { return iaa_Trans_Rcrd_Invrse_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Lst_Trans_Dte() { return iaa_Trans_Rcrd_Lst_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_Ppcn_Nbr() { return iaa_Trans_Rcrd_Trans_Ppcn_Nbr; }

    public DbsField getIaa_Trans_Rcrd_Trans_Payee_Cde() { return iaa_Trans_Rcrd_Trans_Payee_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Sub_Cde() { return iaa_Trans_Rcrd_Trans_Sub_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cde() { return iaa_Trans_Rcrd_Trans_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Actvty_Cde() { return iaa_Trans_Rcrd_Trans_Actvty_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Check_Dte() { return iaa_Trans_Rcrd_Trans_Check_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_Todays_Dte() { return iaa_Trans_Rcrd_Trans_Todays_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_User_Area() { return iaa_Trans_Rcrd_Trans_User_Area; }

    public DbsField getIaa_Trans_Rcrd_Trans_User_Id() { return iaa_Trans_Rcrd_Trans_User_Id; }

    public DbsField getIaa_Trans_Rcrd_Trans_Verify_Cde() { return iaa_Trans_Rcrd_Trans_Verify_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Verify_Id() { return iaa_Trans_Rcrd_Trans_Verify_Id; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cmbne_Cde() { return iaa_Trans_Rcrd_Trans_Cmbne_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cwf_Wpid() { return iaa_Trans_Rcrd_Trans_Cwf_Wpid; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cwf_Id_Nbr() { return iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr; }

    public DbsField getIaa_Trans_Rcrd_Trans_Effective_Dte() { return iaa_Trans_Rcrd_Trans_Effective_Dte; }

    public DataAccessProgramView getVw_iaa_Cntrct() { return vw_iaa_Cntrct; }

    public DbsField getIaa_Cntrct_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Optn_Cde() { return iaa_Cntrct_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Orgn_Cde() { return iaa_Cntrct_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Acctng_Cde() { return iaa_Cntrct_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Issue_Dte() { return iaa_Cntrct_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Crrncy_Cde() { return iaa_Cntrct_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Type_Cde() { return iaa_Cntrct_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Pymnt_Mthd() { return iaa_Cntrct_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Cntrct_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Ssn() { return iaa_Cntrct_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Cntrct_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_Lst_Trans_Dte() { return iaa_Cntrct_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Type() { return iaa_Cntrct_Cntrct_Type; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Mtch_Ppcn() { return iaa_Cntrct_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Cntrct_Cntrct_Annty_Strt_Dte() { return iaa_Cntrct_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Fp_Due_Dte_Dd() { return iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Ssnng_Dte() { return iaa_Cntrct_Cntrct_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Roth_Frst_Cntrb_Dte() { return iaa_Cntrct_Roth_Frst_Cntrb_Dte; }

    public DbsField getIaa_Cntrct_Roth_Ssnng_Dte() { return iaa_Cntrct_Roth_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Plan_Nmbr() { return iaa_Cntrct_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Tax_Exmpt_Ind() { return iaa_Cntrct_Tax_Exmpt_Ind; }

    public DbsField getIaa_Cntrct_Orig_Ownr_Dob() { return iaa_Cntrct_Orig_Ownr_Dob; }

    public DbsField getIaa_Cntrct_Orig_Ownr_Dod() { return iaa_Cntrct_Orig_Ownr_Dod; }

    public DbsField getIaa_Cntrct_Sub_Plan_Nmbr() { return iaa_Cntrct_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Orgntng_Sub_Plan_Nmbr() { return iaa_Cntrct_Orgntng_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Orgntng_Cntrct_Nmbr() { return iaa_Cntrct_Orgntng_Cntrct_Nmbr; }

    public DataAccessProgramView getVw_iaa_Cntrct_Trans() { return vw_iaa_Cntrct_Trans; }

    public DbsField getIaa_Cntrct_Trans_Trans_Dte() { return iaa_Cntrct_Trans_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Invrse_Trans_Dte() { return iaa_Cntrct_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Lst_Trans_Dte() { return iaa_Cntrct_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Optn_Cde() { return iaa_Cntrct_Trans_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Orgn_Cde() { return iaa_Cntrct_Trans_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Acctng_Cde() { return iaa_Cntrct_Trans_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Issue_Dte() { return iaa_Cntrct_Trans_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Crrncy_Cde() { return iaa_Cntrct_Trans_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Type_Cde() { return iaa_Cntrct_Trans_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Pymnt_Mthd() { return iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_Trans_Trans_Check_Dte() { return iaa_Cntrct_Trans_Trans_Check_Dte; }

    public DbsField getIaa_Cntrct_Trans_Bfre_Imge_Id() { return iaa_Cntrct_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Aftr_Imge_Id() { return iaa_Cntrct_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Type() { return iaa_Cntrct_Trans_Cntrct_Type; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Mtch_Ppcn() { return iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte() { return iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ssnng_Dte() { return iaa_Cntrct_Trans_Cntrct_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte() { return iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte; }

    public DbsField getIaa_Cntrct_Trans_Roth_Ssnng_Dte() { return iaa_Cntrct_Trans_Roth_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Trans_Plan_Nmbr() { return iaa_Cntrct_Trans_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Tax_Exmpt_Ind() { return iaa_Cntrct_Trans_Tax_Exmpt_Ind; }

    public DbsField getIaa_Cntrct_Trans_Orig_Ownr_Dob() { return iaa_Cntrct_Trans_Orig_Ownr_Dob; }

    public DbsField getIaa_Cntrct_Trans_Orig_Ownr_Dod() { return iaa_Cntrct_Trans_Orig_Ownr_Dod; }

    public DbsField getIaa_Cntrct_Trans_Sub_Plan_Nmbr() { return iaa_Cntrct_Trans_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr() { return iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr() { return iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr; }

    public DataAccessProgramView getVw_cpr() { return vw_cpr; }

    public DbsField getCpr_Cntrct_Part_Ppcn_Nbr() { return cpr_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getCpr_Cntrct_Part_Payee_Cde() { return cpr_Cntrct_Part_Payee_Cde; }

    public DbsField getCpr_Cpr_Id_Nbr() { return cpr_Cpr_Id_Nbr; }

    public DbsField getCpr_Lst_Trans_Dte() { return cpr_Lst_Trans_Dte; }

    public DbsField getCpr_Prtcpnt_Ctznshp_Cde() { return cpr_Prtcpnt_Ctznshp_Cde; }

    public DbsField getCpr_Prtcpnt_Rsdncy_Cde() { return cpr_Prtcpnt_Rsdncy_Cde; }

    public DbsField getCpr_Prtcpnt_Rsdncy_Sw() { return cpr_Prtcpnt_Rsdncy_Sw; }

    public DbsField getCpr_Prtcpnt_Tax_Id_Nbr() { return cpr_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getCpr_Prtcpnt_Tax_Id_Typ() { return cpr_Prtcpnt_Tax_Id_Typ; }

    public DbsField getCpr_Cntrct_Actvty_Cde() { return cpr_Cntrct_Actvty_Cde; }

    public DbsField getCpr_Cntrct_Trmnte_Rsn() { return cpr_Cntrct_Trmnte_Rsn; }

    public DbsField getCpr_Cntrct_Rwrttn_Ind() { return cpr_Cntrct_Rwrttn_Ind; }

    public DbsField getCpr_Cntrct_Cash_Cde() { return cpr_Cntrct_Cash_Cde; }

    public DbsField getCpr_Cntrct_Emplymnt_Trmnt_Cde() { return cpr_Cntrct_Emplymnt_Trmnt_Cde; }

    public DbsGroup getCpr_Cntrct_Company_Data() { return cpr_Cntrct_Company_Data; }

    public DbsField getCpr_Cntrct_Company_Cd() { return cpr_Cntrct_Company_Cd; }

    public DbsField getCpr_Cntrct_Rcvry_Type_Ind() { return cpr_Cntrct_Rcvry_Type_Ind; }

    public DbsField getCpr_Cntrct_Per_Ivc_Amt() { return cpr_Cntrct_Per_Ivc_Amt; }

    public DbsField getCpr_Cntrct_Resdl_Ivc_Amt() { return cpr_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getCpr_Cntrct_Ivc_Amt() { return cpr_Cntrct_Ivc_Amt; }

    public DbsField getCpr_Cntrct_Ivc_Used_Amt() { return cpr_Cntrct_Ivc_Used_Amt; }

    public DbsField getCpr_Cntrct_Rtb_Amt() { return cpr_Cntrct_Rtb_Amt; }

    public DbsField getCpr_Cntrct_Rtb_Percent() { return cpr_Cntrct_Rtb_Percent; }

    public DbsField getCpr_Cntrct_Mode_Ind() { return cpr_Cntrct_Mode_Ind; }

    public DbsField getCpr_Cntrct_Wthdrwl_Dte() { return cpr_Cntrct_Wthdrwl_Dte; }

    public DbsField getCpr_Cntrct_Final_Per_Pay_Dte() { return cpr_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getCpr_Cntrct_Final_Pay_Dte() { return cpr_Cntrct_Final_Pay_Dte; }

    public DbsField getCpr_Bnfcry_Xref_Ind() { return cpr_Bnfcry_Xref_Ind; }

    public DbsField getCpr_Bnfcry_Dod_Dte() { return cpr_Bnfcry_Dod_Dte; }

    public DbsField getCpr_Cntrct_Pend_Cde() { return cpr_Cntrct_Pend_Cde; }

    public DbsField getCpr_Cntrct_Hold_Cde() { return cpr_Cntrct_Hold_Cde; }

    public DbsField getCpr_Cntrct_Pend_Dte() { return cpr_Cntrct_Pend_Dte; }

    public DbsField getCpr_Cntrct_Prev_Dist_Cde() { return cpr_Cntrct_Prev_Dist_Cde; }

    public DbsField getCpr_Cntrct_Curr_Dist_Cde() { return cpr_Cntrct_Curr_Dist_Cde; }

    public DbsField getCpr_Cntrct_Cmbne_Cde() { return cpr_Cntrct_Cmbne_Cde; }

    public DbsField getCpr_Cntrct_Spirt_Cde() { return cpr_Cntrct_Spirt_Cde; }

    public DbsField getCpr_Cntrct_Spirt_Amt() { return cpr_Cntrct_Spirt_Amt; }

    public DbsField getCpr_Cntrct_Spirt_Srce() { return cpr_Cntrct_Spirt_Srce; }

    public DbsField getCpr_Cntrct_Spirt_Arr_Dte() { return cpr_Cntrct_Spirt_Arr_Dte; }

    public DbsField getCpr_Cntrct_Spirt_Prcss_Dte() { return cpr_Cntrct_Spirt_Prcss_Dte; }

    public DbsField getCpr_Cntrct_Fed_Tax_Amt() { return cpr_Cntrct_Fed_Tax_Amt; }

    public DbsField getCpr_Cntrct_State_Cde() { return cpr_Cntrct_State_Cde; }

    public DbsField getCpr_Cntrct_State_Tax_Amt() { return cpr_Cntrct_State_Tax_Amt; }

    public DbsField getCpr_Cntrct_Local_Cde() { return cpr_Cntrct_Local_Cde; }

    public DbsField getCpr_Cntrct_Local_Tax_Amt() { return cpr_Cntrct_Local_Tax_Amt; }

    public DbsField getCpr_Cntrct_Lst_Chnge_Dte() { return cpr_Cntrct_Lst_Chnge_Dte; }

    public DbsField getCpr_Cpr_Xfr_Term_Cde() { return cpr_Cpr_Xfr_Term_Cde; }

    public DbsField getCpr_Cpr_Lgl_Res_Cde() { return cpr_Cpr_Lgl_Res_Cde; }

    public DbsField getCpr_Cpr_Xfr_Iss_Dte() { return cpr_Cpr_Xfr_Iss_Dte; }

    public DbsField getCpr_Rllvr_Cntrct_Nbr() { return cpr_Rllvr_Cntrct_Nbr; }

    public DbsField getCpr_Rllvr_Ivc_Ind() { return cpr_Rllvr_Ivc_Ind; }

    public DbsField getCpr_Rllvr_Elgble_Ind() { return cpr_Rllvr_Elgble_Ind; }

    public DbsGroup getCpr_Rllvr_Dstrbtng_Irc_CdeMuGroup() { return cpr_Rllvr_Dstrbtng_Irc_CdeMuGroup; }

    public DbsField getCpr_Rllvr_Dstrbtng_Irc_Cde() { return cpr_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getCpr_Rllvr_Accptng_Irc_Cde() { return cpr_Rllvr_Accptng_Irc_Cde; }

    public DbsField getCpr_Rllvr_Pln_Admn_Ind() { return cpr_Rllvr_Pln_Admn_Ind; }

    public DbsField getCpr_Roth_Dsblty_Dte() { return cpr_Roth_Dsblty_Dte; }

    public DataAccessProgramView getVw_cpr_Trans() { return vw_cpr_Trans; }

    public DbsField getCpr_Trans_Trans_Dte() { return cpr_Trans_Trans_Dte; }

    public DbsField getCpr_Trans_Invrse_Trans_Dte() { return cpr_Trans_Invrse_Trans_Dte; }

    public DbsField getCpr_Trans_Lst_Trans_Dte() { return cpr_Trans_Lst_Trans_Dte; }

    public DbsField getCpr_Trans_Cntrct_Part_Ppcn_Nbr() { return cpr_Trans_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getCpr_Trans_Cntrct_Part_Payee_Cde() { return cpr_Trans_Cntrct_Part_Payee_Cde; }

    public DbsField getCpr_Trans_Cpr_Id_Nbr() { return cpr_Trans_Cpr_Id_Nbr; }

    public DbsField getCpr_Trans_Prtcpnt_Ctznshp_Cde() { return cpr_Trans_Prtcpnt_Ctznshp_Cde; }

    public DbsField getCpr_Trans_Prtcpnt_Rsdncy_Cde() { return cpr_Trans_Prtcpnt_Rsdncy_Cde; }

    public DbsField getCpr_Trans_Prtcpnt_Rsdncy_Sw() { return cpr_Trans_Prtcpnt_Rsdncy_Sw; }

    public DbsField getCpr_Trans_Prtcpnt_Tax_Id_Nbr() { return cpr_Trans_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getCpr_Trans_Prtcpnt_Tax_Id_Typ() { return cpr_Trans_Prtcpnt_Tax_Id_Typ; }

    public DbsField getCpr_Trans_Cntrct_Actvty_Cde() { return cpr_Trans_Cntrct_Actvty_Cde; }

    public DbsField getCpr_Trans_Cntrct_Trmnte_Rsn() { return cpr_Trans_Cntrct_Trmnte_Rsn; }

    public DbsField getCpr_Trans_Cntrct_Rwrttn_Ind() { return cpr_Trans_Cntrct_Rwrttn_Ind; }

    public DbsField getCpr_Trans_Cntrct_Cash_Cde() { return cpr_Trans_Cntrct_Cash_Cde; }

    public DbsField getCpr_Trans_Cntrct_Emplymnt_Trmnt_Cde() { return cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde; }

    public DbsGroup getCpr_Trans_Cntrct_Company_Data() { return cpr_Trans_Cntrct_Company_Data; }

    public DbsField getCpr_Trans_Cntrct_Company_Cd() { return cpr_Trans_Cntrct_Company_Cd; }

    public DbsField getCpr_Trans_Cntrct_Rcvry_Type_Ind() { return cpr_Trans_Cntrct_Rcvry_Type_Ind; }

    public DbsField getCpr_Trans_Cntrct_Per_Ivc_Amt() { return cpr_Trans_Cntrct_Per_Ivc_Amt; }

    public DbsField getCpr_Trans_Cntrct_Resdl_Ivc_Amt() { return cpr_Trans_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getCpr_Trans_Cntrct_Ivc_Amt() { return cpr_Trans_Cntrct_Ivc_Amt; }

    public DbsField getCpr_Trans_Cntrct_Ivc_Used_Amt() { return cpr_Trans_Cntrct_Ivc_Used_Amt; }

    public DbsField getCpr_Trans_Cntrct_Rtb_Amt() { return cpr_Trans_Cntrct_Rtb_Amt; }

    public DbsField getCpr_Trans_Cntrct_Rtb_Percent() { return cpr_Trans_Cntrct_Rtb_Percent; }

    public DbsField getCpr_Trans_Cntrct_Mode_Ind() { return cpr_Trans_Cntrct_Mode_Ind; }

    public DbsField getCpr_Trans_Cntrct_Wthdrwl_Dte() { return cpr_Trans_Cntrct_Wthdrwl_Dte; }

    public DbsField getCpr_Trans_Cntrct_Final_Per_Pay_Dte() { return cpr_Trans_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getCpr_Trans_Cntrct_Final_Pay_Dte() { return cpr_Trans_Cntrct_Final_Pay_Dte; }

    public DbsField getCpr_Trans_Bnfcry_Xref_Ind() { return cpr_Trans_Bnfcry_Xref_Ind; }

    public DbsField getCpr_Trans_Bnfcry_Dod_Dte() { return cpr_Trans_Bnfcry_Dod_Dte; }

    public DbsField getCpr_Trans_Cntrct_Pend_Cde() { return cpr_Trans_Cntrct_Pend_Cde; }

    public DbsField getCpr_Trans_Cntrct_Hold_Cde() { return cpr_Trans_Cntrct_Hold_Cde; }

    public DbsField getCpr_Trans_Cntrct_Pend_Dte() { return cpr_Trans_Cntrct_Pend_Dte; }

    public DbsField getCpr_Trans_Cntrct_Prev_Dist_Cde() { return cpr_Trans_Cntrct_Prev_Dist_Cde; }

    public DbsField getCpr_Trans_Cntrct_Curr_Dist_Cde() { return cpr_Trans_Cntrct_Curr_Dist_Cde; }

    public DbsField getCpr_Trans_Cntrct_Cmbne_Cde() { return cpr_Trans_Cntrct_Cmbne_Cde; }

    public DbsField getCpr_Trans_Cntrct_Spirt_Cde() { return cpr_Trans_Cntrct_Spirt_Cde; }

    public DbsField getCpr_Trans_Cntrct_Spirt_Amt() { return cpr_Trans_Cntrct_Spirt_Amt; }

    public DbsField getCpr_Trans_Cntrct_Spirt_Srce() { return cpr_Trans_Cntrct_Spirt_Srce; }

    public DbsField getCpr_Trans_Cntrct_Spirt_Arr_Dte() { return cpr_Trans_Cntrct_Spirt_Arr_Dte; }

    public DbsField getCpr_Trans_Cntrct_Spirt_Prcss_Dte() { return cpr_Trans_Cntrct_Spirt_Prcss_Dte; }

    public DbsField getCpr_Trans_Cntrct_Fed_Tax_Amt() { return cpr_Trans_Cntrct_Fed_Tax_Amt; }

    public DbsField getCpr_Trans_Cntrct_State_Cde() { return cpr_Trans_Cntrct_State_Cde; }

    public DbsField getCpr_Trans_Cntrct_State_Tax_Amt() { return cpr_Trans_Cntrct_State_Tax_Amt; }

    public DbsField getCpr_Trans_Cntrct_Local_Cde() { return cpr_Trans_Cntrct_Local_Cde; }

    public DbsField getCpr_Trans_Cntrct_Local_Tax_Amt() { return cpr_Trans_Cntrct_Local_Tax_Amt; }

    public DbsField getCpr_Trans_Cntrct_Lst_Chnge_Dte() { return cpr_Trans_Cntrct_Lst_Chnge_Dte; }

    public DbsField getCpr_Trans_Trans_Check_Dte() { return cpr_Trans_Trans_Check_Dte; }

    public DbsField getCpr_Trans_Bfre_Imge_Id() { return cpr_Trans_Bfre_Imge_Id; }

    public DbsField getCpr_Trans_Aftr_Imge_Id() { return cpr_Trans_Aftr_Imge_Id; }

    public DbsField getCpr_Trans_Cpr_Xfr_Term_Cde() { return cpr_Trans_Cpr_Xfr_Term_Cde; }

    public DbsField getCpr_Trans_Cpr_Lgl_Res_Cde() { return cpr_Trans_Cpr_Lgl_Res_Cde; }

    public DbsField getCpr_Trans_Cpr_Xfr_Iss_Dte() { return cpr_Trans_Cpr_Xfr_Iss_Dte; }

    public DbsField getCpr_Trans_Rllvr_Cntrct_Nbr() { return cpr_Trans_Rllvr_Cntrct_Nbr; }

    public DbsField getCpr_Trans_Rllvr_Ivc_Ind() { return cpr_Trans_Rllvr_Ivc_Ind; }

    public DbsField getCpr_Trans_Rllvr_Elgble_Ind() { return cpr_Trans_Rllvr_Elgble_Ind; }

    public DbsGroup getCpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup() { return cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup; }

    public DbsField getCpr_Trans_Rllvr_Dstrbtng_Irc_Cde() { return cpr_Trans_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getCpr_Trans_Rllvr_Accptng_Irc_Cde() { return cpr_Trans_Rllvr_Accptng_Irc_Cde; }

    public DbsField getCpr_Trans_Rllvr_Pln_Admn_Ind() { return cpr_Trans_Rllvr_Pln_Admn_Ind; }

    public DbsField getCpr_Trans_Roth_Dsblty_Dte() { return cpr_Trans_Roth_Dsblty_Dte; }

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Rcrd() { return vw_iaa_Tiaa_Fund_Rcrd; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte() { return iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tckr_Symbl() { return iaa_Tiaa_Fund_Rcrd_Tckr_Symbl; }

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Trans() { return vw_iaa_Tiaa_Fund_Trans; }

    public DbsField getIaa_Tiaa_Fund_Trans_Trans_Dte() { return iaa_Tiaa_Fund_Trans_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte() { return iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Lst_Trans_Dte() { return iaa_Tiaa_Fund_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef2() { return iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef2; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt() { return iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund() { return iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund; }

    public DbsField getIaa_Tiaa_Fund_Trans_Trans_Check_Dte() { return iaa_Tiaa_Fund_Trans_Trans_Check_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id() { return iaa_Tiaa_Fund_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Tiaa_Fund_Trans_Aftr_Imge_Id() { return iaa_Tiaa_Fund_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte() { return iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte() { return iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte() { return iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tckr_Symbl() { return iaa_Tiaa_Fund_Trans_Tckr_Symbl; }

    public DataAccessProgramView getVw_iaa_Dc_Sttlmnt_Req() { return vw_iaa_Dc_Sttlmnt_Req; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Id_Nbr() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Id_Nbr; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Tax_Id_Nbr() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Tax_Id_Nbr; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Req_Seq_Nbr() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Req_Seq_Nbr; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Process_Type() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Process_Type; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Timestamp() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Timestamp; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Status_Cde() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Status_Cde; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Status_Timestamp() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Status_Timestamp; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Cwf_Wpid() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Cwf_Wpid; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Name() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Name; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Type() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Type; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Dod_Dte() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Dod_Dte; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_2nd_Dod_Dte() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_2nd_Dod_Dte; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Name() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Name; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr1() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr1; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr2() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr2; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr3() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr3; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr4() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr4; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_City() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_City; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_State() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_State; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Zip() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Zip; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Phone() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Phone; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notify_Date() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Notify_Date; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Rltnshp() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Rltnshp; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Cntrct_Count() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Cntrct_Count; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Oper_Id() { return iaa_Dc_Sttlmnt_Req_Oper_Id; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Oper_Timestamp() { return iaa_Dc_Sttlmnt_Req_Oper_Timestamp; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Oper_User_Grp() { return iaa_Dc_Sttlmnt_Req_Oper_User_Grp; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Verf_Id() { return iaa_Dc_Sttlmnt_Req_Verf_Id; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Verf_Timestamp() { return iaa_Dc_Sttlmnt_Req_Verf_Timestamp; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Verf_User_Grp() { return iaa_Dc_Sttlmnt_Req_Verf_User_Grp; }

    public DbsField getIaa_Dc_Sttlmnt_Req_Sttlmnt_Coding_Dte() { return iaa_Dc_Sttlmnt_Req_Sttlmnt_Coding_Dte; }

    public DataAccessProgramView getVw_iaa_Dc_Cntrct() { return vw_iaa_Dc_Cntrct; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Id_Nbr() { return iaa_Dc_Cntrct_Cntrct_Id_Nbr; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Tax_Id_Nbr() { return iaa_Dc_Cntrct_Cntrct_Tax_Id_Nbr; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Process_Type() { return iaa_Dc_Cntrct_Cntrct_Process_Type; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Req_Seq_Nbr() { return iaa_Dc_Cntrct_Cntrct_Req_Seq_Nbr; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Timestamp() { return iaa_Dc_Cntrct_Cntrct_Timestamp; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Ppcn_Nbr() { return iaa_Dc_Cntrct_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Payee_Cde() { return iaa_Dc_Cntrct_Cntrct_Payee_Cde; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Status_Cde() { return iaa_Dc_Cntrct_Cntrct_Status_Cde; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Status_Timestamp() { return iaa_Dc_Cntrct_Cntrct_Status_Timestamp; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Error_Msg() { return iaa_Dc_Cntrct_Cntrct_Error_Msg; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Annt_Type() { return iaa_Dc_Cntrct_Cntrct_Annt_Type; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Optn_Cde() { return iaa_Dc_Cntrct_Cntrct_Optn_Cde; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Issue_Dte() { return iaa_Dc_Cntrct_Cntrct_Issue_Dte; }

    public DbsField getIaa_Dc_Cntrct_Count_Castcntrct_Sttlmnt_Info_Cde() { return iaa_Dc_Cntrct_Count_Castcntrct_Sttlmnt_Info_Cde; }

    public DbsGroup getIaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_CdeMuGroup() { return iaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_CdeMuGroup; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_Cde() { return iaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_Cde; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_First_Annt_Xref_Ind() { return iaa_Dc_Cntrct_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_First_Annt_Dod_Dte() { return iaa_Dc_Cntrct_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_First_Annt_Dob_Dte() { return iaa_Dc_Cntrct_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_First_Annt_Sex_Cde() { return iaa_Dc_Cntrct_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Bnfcry_Xref_Ind() { return iaa_Dc_Cntrct_Cntrct_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Bnfcry_Dod_Dte() { return iaa_Dc_Cntrct_Cntrct_Bnfcry_Dod_Dte; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Bnfcry_Grp_Nmbr() { return iaa_Dc_Cntrct_Cntrct_Bnfcry_Grp_Nmbr; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Bnfcry_Grp_Seq() { return iaa_Dc_Cntrct_Cntrct_Bnfcry_Grp_Seq; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Pmt_Data_Nmbr() { return iaa_Dc_Cntrct_Cntrct_Pmt_Data_Nmbr; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Bnfcry_Name() { return iaa_Dc_Cntrct_Cntrct_Bnfcry_Name; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Spirt_Code() { return iaa_Dc_Cntrct_Cntrct_Spirt_Code; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Delete_Code() { return iaa_Dc_Cntrct_Cntrct_Delete_Code; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Rwrttn_Ind() { return iaa_Dc_Cntrct_Cntrct_Rwrttn_Ind; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Brkdwn_Pct() { return iaa_Dc_Cntrct_Cntrct_Brkdwn_Pct; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Lump_Sum_Avail() { return iaa_Dc_Cntrct_Cntrct_Lump_Sum_Avail; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Cont_Pmt_Avail() { return iaa_Dc_Cntrct_Cntrct_Cont_Pmt_Avail; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Pymnt_Mode() { return iaa_Dc_Cntrct_Cntrct_Pymnt_Mode; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Hist_Rate_Needed_Ind() { return iaa_Dc_Cntrct_Cntrct_Hist_Rate_Needed_Ind; }

    public DbsField getIaa_Dc_Cntrct_Cntrct_Hist_Rate_Entered_Ind() { return iaa_Dc_Cntrct_Cntrct_Hist_Rate_Entered_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Cntrct_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRCT_SSNNG_DTE");
        iaa_Cntrct_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_Roth_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ROTH_SSNNG_DTE");
        iaa_Cntrct_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_Tax_Exmpt_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TAX_EXMPT_IND");
        iaa_Cntrct_Orig_Ownr_Dob = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOB");
        iaa_Cntrct_Orig_Ownr_Dod = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOD");
        iaa_Cntrct_Sub_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SUB_PLAN_NMBR");
        iaa_Cntrct_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");
        iaa_Cntrct_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");

        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cntrct_Trans_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cntrct_Trans_Lst_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_Optn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Trans_Cntrct_Orgn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Trans_Cntrct_Acctng_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Trans_Cntrct_Crrncy_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Trans_Cntrct_Type_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind", 
            "CNTRCT-JOINT-CNVRT-RCRD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Trans_Trans_Check_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cntrct_Trans_Bfre_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cntrct_Trans_Aftr_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cntrct_Trans_Cntrct_Type = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct_Trans.getRecord().newGroupInGroup("iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Ssnng_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_SSNNG_DTE");
        iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_Trans_Roth_Ssnng_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_SSNNG_DTE");
        iaa_Cntrct_Trans_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_Trans_Tax_Exmpt_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TAX_EXMPT_IND");
        iaa_Cntrct_Trans_Orig_Ownr_Dob = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ORIG_OWNR_DOB");
        iaa_Cntrct_Trans_Orig_Ownr_Dod = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ORIG_OWNR_DOD");
        iaa_Cntrct_Trans_Sub_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "SUB_PLAN_NMBR");
        iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");
        iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cpr_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        cpr_Lst_Trans_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        cpr_Prtcpnt_Ctznshp_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "PRTCPNT_CTZNSHP_CDE");
        cpr_Prtcpnt_Rsdncy_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "PRTCPNT_RSDNCY_CDE");
        cpr_Prtcpnt_Rsdncy_Sw = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRTCPNT_RSDNCY_SW");
        cpr_Prtcpnt_Tax_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr_Prtcpnt_Tax_Id_Typ = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_TYP");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Cntrct_Trmnte_Rsn = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_TRMNTE_RSN");
        cpr_Cntrct_Rwrttn_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_RWRTTN_IND");
        cpr_Cntrct_Cash_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_CASH_CDE");
        cpr_Cntrct_Emplymnt_Trmnt_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        cpr_Cntrct_Company_Data = vw_cpr.getRecord().newGroupInGroup("cpr_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Company_Cd = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1,5) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Rcvry_Type_Ind = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Per_Ivc_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Resdl_Ivc_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Ivc_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Ivc_Used_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Rtb_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Rtb_Percent = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 
            7, 4, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Mode_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        cpr_Cntrct_Wthdrwl_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_WTHDRWL_DTE");
        cpr_Cntrct_Final_Per_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr_Cntrct_Final_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_FINAL_PAY_DTE");
        cpr_Bnfcry_Xref_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "BNFCRY_XREF_IND");
        cpr_Bnfcry_Dod_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "BNFCRY_DOD_DTE");
        cpr_Cntrct_Pend_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        cpr_Cntrct_Hold_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_CDE");
        cpr_Cntrct_Pend_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_DTE");
        cpr_Cntrct_Prev_Dist_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PREV_DIST_CDE");
        cpr_Cntrct_Curr_Dist_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_CURR_DIST_CDE");
        cpr_Cntrct_Cmbne_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_CMBNE_CDE");
        cpr_Cntrct_Spirt_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_SPIRT_CDE");
        cpr_Cntrct_Spirt_Amt = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_SPIRT_AMT");
        cpr_Cntrct_Spirt_Srce = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_SPIRT_SRCE");
        cpr_Cntrct_Spirt_Arr_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_SPIRT_ARR_DTE");
        cpr_Cntrct_Spirt_Prcss_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, 
            RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        cpr_Cntrct_Fed_Tax_Amt = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_FED_TAX_AMT");
        cpr_Cntrct_State_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_STATE_CDE");
        cpr_Cntrct_State_Tax_Amt = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        cpr_Cntrct_Local_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_LOCAL_CDE");
        cpr_Cntrct_Local_Tax_Amt = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        cpr_Cntrct_Lst_Chnge_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_LST_CHNGE_DTE");
        cpr_Cpr_Xfr_Term_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CPR_XFR_TERM_CDE");
        cpr_Cpr_Lgl_Res_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CPR_LGL_RES_CDE");
        cpr_Cpr_Xfr_Iss_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CPR_XFR_ISS_DTE");
        cpr_Rllvr_Cntrct_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Rllvr_Cntrct_Nbr", "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "RLLVR_CNTRCT_NBR");
        cpr_Rllvr_Ivc_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RLLVR_IVC_IND");
        cpr_Rllvr_Elgble_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Rllvr_Elgble_Ind", "RLLVR-ELGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RLLVR_ELGBLE_IND");
        cpr_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_cpr.getRecord().newGroupInGroup("cpr_Rllvr_Dstrbtng_Irc_CdeMuGroup", "RLLVR_DSTRBTNG_IRC_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "IA_CONTRACT_PART_RLLVR_DSTRBTNG_IRC_CDE");
        cpr_Rllvr_Dstrbtng_Irc_Cde = cpr_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("cpr_Rllvr_Dstrbtng_Irc_Cde", "RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 
            2, new DbsArrayController(1,4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        cpr_Rllvr_Accptng_Irc_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Rllvr_Accptng_Irc_Cde", "RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "RLLVR_ACCPTNG_IRC_CDE");
        cpr_Rllvr_Pln_Admn_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Rllvr_Pln_Admn_Ind", "RLLVR-PLN-ADMN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RLLVR_PLN_ADMN_IND");
        cpr_Roth_Dsblty_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ROTH_DSBLTY_DTE");

        vw_cpr_Trans = new DataAccessProgramView(new NameInfo("vw_cpr_Trans", "CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CPR_TRANS"));
        cpr_Trans_Trans_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cpr_Trans_Invrse_Trans_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        cpr_Trans_Lst_Trans_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        cpr_Trans_Cntrct_Part_Payee_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        cpr_Trans_Cpr_Id_Nbr = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CPR_ID_NBR");
        cpr_Trans_Prtcpnt_Ctznshp_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        cpr_Trans_Prtcpnt_Rsdncy_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        cpr_Trans_Prtcpnt_Rsdncy_Sw = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        cpr_Trans_Prtcpnt_Tax_Id_Nbr = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        cpr_Trans_Prtcpnt_Tax_Id_Typ = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        cpr_Trans_Cntrct_Actvty_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        cpr_Trans_Cntrct_Trmnte_Rsn = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        cpr_Trans_Cntrct_Rwrttn_Ind = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        cpr_Trans_Cntrct_Cash_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_CASH_CDE");
        cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        cpr_Trans_Cntrct_Company_Data = vw_cpr_Trans.getRecord().newGroupInGroup("cpr_Trans_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        cpr_Trans_Cntrct_Company_Cd = cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Trans_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", FieldType.STRING, 
            1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        cpr_Trans_Cntrct_Rcvry_Type_Ind = cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Trans_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        cpr_Trans_Cntrct_Per_Ivc_Amt = cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Trans_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        cpr_Trans_Cntrct_Resdl_Ivc_Amt = cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Trans_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        cpr_Trans_Cntrct_Ivc_Amt = cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Trans_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        cpr_Trans_Cntrct_Ivc_Used_Amt = cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Trans_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        cpr_Trans_Cntrct_Rtb_Amt = cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Trans_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        cpr_Trans_Cntrct_Rtb_Percent = cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Trans_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 
            7, 4, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        cpr_Trans_Cntrct_Mode_Ind = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        cpr_Trans_Cntrct_Wthdrwl_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        cpr_Trans_Cntrct_Final_Per_Pay_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr_Trans_Cntrct_Final_Pay_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        cpr_Trans_Bnfcry_Xref_Ind = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "BNFCRY_XREF_IND");
        cpr_Trans_Bnfcry_Dod_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "BNFCRY_DOD_DTE");
        cpr_Trans_Cntrct_Pend_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        cpr_Trans_Cntrct_Hold_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_CDE");
        cpr_Trans_Cntrct_Pend_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_DTE");
        cpr_Trans_Cntrct_Prev_Dist_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        cpr_Trans_Cntrct_Curr_Dist_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        cpr_Trans_Cntrct_Cmbne_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, 
            RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        cpr_Trans_Cntrct_Spirt_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_SPIRT_CDE");
        cpr_Trans_Cntrct_Spirt_Amt = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        cpr_Trans_Cntrct_Spirt_Srce = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        cpr_Trans_Cntrct_Spirt_Arr_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        cpr_Trans_Cntrct_Spirt_Prcss_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        cpr_Trans_Cntrct_Fed_Tax_Amt = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        cpr_Trans_Cntrct_State_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_STATE_CDE");
        cpr_Trans_Cntrct_State_Tax_Amt = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        cpr_Trans_Cntrct_Local_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_LOCAL_CDE");
        cpr_Trans_Cntrct_Local_Tax_Amt = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        cpr_Trans_Cntrct_Lst_Chnge_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        cpr_Trans_Trans_Check_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "TRANS_CHECK_DTE");
        cpr_Trans_Bfre_Imge_Id = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        cpr_Trans_Aftr_Imge_Id = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        cpr_Trans_Cpr_Xfr_Term_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CPR_XFR_TERM_CDE");
        cpr_Trans_Cpr_Lgl_Res_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CPR_LGL_RES_CDE");
        cpr_Trans_Cpr_Xfr_Iss_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CPR_XFR_ISS_DTE");
        cpr_Trans_Rllvr_Cntrct_Nbr = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Rllvr_Cntrct_Nbr", "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        cpr_Trans_Rllvr_Ivc_Ind = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RLLVR_IVC_IND");
        cpr_Trans_Rllvr_Elgble_Ind = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Rllvr_Elgble_Ind", "RLLVR-ELGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RLLVR_ELGBLE_IND");
        cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_cpr_Trans.getRecord().newGroupInGroup("cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup", "RLLVR_DSTRBTNG_IRC_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_RLLVR_DSTRBTNG_IRC_CDE");
        cpr_Trans_Rllvr_Dstrbtng_Irc_Cde = cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("cpr_Trans_Rllvr_Dstrbtng_Irc_Cde", "RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        cpr_Trans_Rllvr_Accptng_Irc_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Rllvr_Accptng_Irc_Cde", "RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RLLVR_ACCPTNG_IRC_CDE");
        cpr_Trans_Rllvr_Pln_Admn_Ind = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Rllvr_Pln_Admn_Ind", "RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RLLVR_PLN_ADMN_IND");
        cpr_Trans_Roth_Dsblty_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ROTH_DSBLTY_DTE");

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1", 
            "Redefines", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt", "TIAA-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_RTB_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund", "TIAA-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CREF_OLD_CMPNY_FUND");
        iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Rcrd_Tckr_Symbl = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tckr_Symbl", "TCKR-SYMBL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TCKR_SYMBL");

        vw_iaa_Tiaa_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Trans", "IAA-TIAA-FUND-TRANS"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        iaa_Tiaa_Fund_Trans_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef2 = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef2", 
            "Redefines", iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef2.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef2.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde", "TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_DIV_AMT");
        iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund", "TIAA-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_OLD_CMPNY_FUND");
        iaa_Tiaa_Fund_Trans_Trans_Check_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Tiaa_Fund_Trans_Bfre_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Tiaa_Fund_Trans_Aftr_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Trans_Tckr_Symbl = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tckr_Symbl", "TCKR-SYMBL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TCKR_SYMBL");

        vw_iaa_Dc_Sttlmnt_Req = new DataAccessProgramView(new NameInfo("vw_iaa_Dc_Sttlmnt_Req", "IAA-DC-STTLMNT-REQ"), "IAA_DC_STTLMNT_REQ", "IA_DEATH_CLAIMS");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Id_Nbr = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Id_Nbr", "STTLMNT-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "STTLMNT_ID_NBR");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Tax_Id_Nbr = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Tax_Id_Nbr", "STTLMNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "STTLMNT_TAX_ID_NBR");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Req_Seq_Nbr = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Req_Seq_Nbr", "STTLMNT-REQ-SEQ-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "STTLMNT_REQ_SEQ_NBR");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Process_Type = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Process_Type", "STTLMNT-PROCESS-TYPE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "STTLMNT_PROCESS_TYPE");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Timestamp = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Timestamp", "STTLMNT-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STTLMNT_TIMESTAMP");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Status_Cde = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Status_Cde", "STTLMNT-STATUS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "STTLMNT_STATUS_CDE");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Status_Timestamp = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Status_Timestamp", 
            "STTLMNT-STATUS-TIMESTAMP", FieldType.TIME, RepeatingFieldStrategy.None, "STTLMNT_STATUS_TIMESTAMP");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Cwf_Wpid = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Cwf_Wpid", "STTLMNT-CWF-WPID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "STTLMNT_CWF_WPID");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Name = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Name", "STTLMNT-DECEDENT-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "STTLMNT_DECEDENT_NAME");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Type = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Decedent_Type", "STTLMNT-DECEDENT-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STTLMNT_DECEDENT_TYPE");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Dod_Dte = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Dod_Dte", "STTLMNT-DOD-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "STTLMNT_DOD_DTE");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_2nd_Dod_Dte = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_2nd_Dod_Dte", "STTLMNT-2ND-DOD-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "STTLMNT_2ND_DOD_DTE");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Name = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Name", "STTLMNT-NOTIFIER-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_NAME");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr1 = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr1", "STTLMNT-NOTIFIER-ADDR1", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_ADDR1");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr2 = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr2", "STTLMNT-NOTIFIER-ADDR2", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_ADDR2");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr3 = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr3", "STTLMNT-NOTIFIER-ADDR3", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_ADDR3");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr4 = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Addr4", "STTLMNT-NOTIFIER-ADDR4", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_ADDR4");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_City = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_City", "STTLMNT-NOTIFIER-CITY", 
            FieldType.STRING, 21, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_CITY");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_State = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_State", "STTLMNT-NOTIFIER-STATE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_STATE");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Zip = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Zip", "STTLMNT-NOTIFIER-ZIP", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_ZIP");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Phone = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Phone", "STTLMNT-NOTIFIER-PHONE", 
            FieldType.STRING, 14, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_PHONE");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Notify_Date = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Notify_Date", "STTLMNT-NOTIFY-DATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "STTLMNT_NOTIFY_DATE");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Rltnshp = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Notifier_Rltnshp", 
            "STTLMNT-NOTIFIER-RLTNSHP", FieldType.STRING, 20, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_RLTNSHP");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Cntrct_Count = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Cntrct_Count", "STTLMNT-CNTRCT-COUNT", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "STTLMNT_CNTRCT_COUNT");
        iaa_Dc_Sttlmnt_Req_Oper_Id = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Oper_Id", "OPER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "OVRPYMNT_OPER_ID");
        iaa_Dc_Sttlmnt_Req_Oper_Timestamp = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Oper_Timestamp", "OPER-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "FETV_OPER_TIMESTAMP");
        iaa_Dc_Sttlmnt_Req_Oper_User_Grp = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Oper_User_Grp", "OPER-USER-GRP", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "OVRPYMNT_OPER_USER_GRP");
        iaa_Dc_Sttlmnt_Req_Verf_Id = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Verf_Id", "VERF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "FETV_VERF_ID");
        iaa_Dc_Sttlmnt_Req_Verf_Timestamp = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Verf_Timestamp", "VERF-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "BNFCRY_VERF_TIMESTAMP");
        iaa_Dc_Sttlmnt_Req_Verf_User_Grp = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Verf_User_Grp", "VERF-USER-GRP", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "FETV_VERF_USER_GRP");
        iaa_Dc_Sttlmnt_Req_Sttlmnt_Coding_Dte = vw_iaa_Dc_Sttlmnt_Req.getRecord().newFieldInGroup("iaa_Dc_Sttlmnt_Req_Sttlmnt_Coding_Dte", "STTLMNT-CODING-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "STTLMNT_CODING_DTE");

        vw_iaa_Dc_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Dc_Cntrct", "IAA-DC-CNTRCT"), "IAA_DC_CNTRCT", "IA_DEATH_CLAIMS");
        iaa_Dc_Cntrct_Cntrct_Id_Nbr = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Id_Nbr", "CNTRCT-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CNTRCT_ID_NBR");
        iaa_Dc_Cntrct_Cntrct_Tax_Id_Nbr = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Tax_Id_Nbr", "CNTRCT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_TAX_ID_NBR");
        iaa_Dc_Cntrct_Cntrct_Process_Type = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Process_Type", "CNTRCT-PROCESS-TYPE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PROCESS_TYPE");
        iaa_Dc_Cntrct_Cntrct_Req_Seq_Nbr = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Req_Seq_Nbr", "CNTRCT-REQ-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_REQ_SEQ_NBR");
        iaa_Dc_Cntrct_Cntrct_Timestamp = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Timestamp", "CNTRCT-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CNTRCT_TIMESTAMP");
        iaa_Dc_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Dc_Cntrct_Cntrct_Payee_Cde = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        iaa_Dc_Cntrct_Cntrct_Status_Cde = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Status_Cde", "CNTRCT-STATUS-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_STATUS_CDE");
        iaa_Dc_Cntrct_Cntrct_Status_Timestamp = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Status_Timestamp", "CNTRCT-STATUS-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRCT_STATUS_TIMESTAMP");
        iaa_Dc_Cntrct_Cntrct_Error_Msg = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Error_Msg", "CNTRCT-ERROR-MSG", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "CNTRCT_ERROR_MSG");
        iaa_Dc_Cntrct_Cntrct_Annt_Type = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Annt_Type", "CNTRCT-ANNT-TYPE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_ANNT_TYPE");
        iaa_Dc_Cntrct_Cntrct_Optn_Cde = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Dc_Cntrct_Cntrct_Issue_Dte = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Dc_Cntrct_Count_Castcntrct_Sttlmnt_Info_Cde = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Count_Castcntrct_Sttlmnt_Info_Cde", 
            "C*CNTRCT-STTLMNT-INFO-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_DEATH_CLAIMS_CNTRCT_STTLMNT_INFO_CDE");
        iaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_CdeMuGroup = vw_iaa_Dc_Cntrct.getRecord().newGroupInGroup("iaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_CdeMuGroup", "CNTRCT_STTLMNT_INFO_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_DEATH_CLAIMS_CNTRCT_STTLMNT_INFO_CDE");
        iaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_Cde = iaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_CdeMuGroup.newFieldArrayInGroup("iaa_Dc_Cntrct_Cntrct_Sttlmnt_Info_Cde", 
            "CNTRCT-STTLMNT-INFO-CDE", FieldType.STRING, 4, new DbsArrayController(1,10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_STTLMNT_INFO_CDE");
        iaa_Dc_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Dc_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Dc_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Dc_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Dc_Cntrct_Cntrct_Bnfcry_Xref_Ind = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Bnfcry_Xref_Ind", "CNTRCT-BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_XREF_IND");
        iaa_Dc_Cntrct_Cntrct_Bnfcry_Dod_Dte = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Bnfcry_Dod_Dte", "CNTRCT-BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_DOD_DTE");
        iaa_Dc_Cntrct_Cntrct_Bnfcry_Grp_Nmbr = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Bnfcry_Grp_Nmbr", "CNTRCT-BNFCRY-GRP-NMBR", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_GRP_NMBR");
        iaa_Dc_Cntrct_Cntrct_Bnfcry_Grp_Seq = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Bnfcry_Grp_Seq", "CNTRCT-BNFCRY-GRP-SEQ", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_GRP_SEQ");
        iaa_Dc_Cntrct_Cntrct_Pmt_Data_Nmbr = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Pmt_Data_Nmbr", "CNTRCT-PMT-DATA-NMBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_PMT_DATA_NMBR");
        iaa_Dc_Cntrct_Cntrct_Bnfcry_Name = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Bnfcry_Name", "CNTRCT-BNFCRY-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_NAME");
        iaa_Dc_Cntrct_Cntrct_Spirt_Code = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Spirt_Code", "CNTRCT-SPIRT-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CODE");
        iaa_Dc_Cntrct_Cntrct_Delete_Code = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Delete_Code", "CNTRCT-DELETE-CODE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_DELETE_CODE");
        iaa_Dc_Cntrct_Cntrct_Rwrttn_Ind = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Dc_Cntrct_Cntrct_Brkdwn_Pct = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Brkdwn_Pct", "CNTRCT-BRKDWN-PCT", FieldType.NUMERIC, 
            7, 4, RepeatingFieldStrategy.None, "CNTRCT_BRKDWN_PCT");
        iaa_Dc_Cntrct_Cntrct_Lump_Sum_Avail = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Lump_Sum_Avail", "CNTRCT-LUMP-SUM-AVAIL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_LUMP_SUM_AVAIL");
        iaa_Dc_Cntrct_Cntrct_Cont_Pmt_Avail = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Cont_Pmt_Avail", "CNTRCT-CONT-PMT-AVAIL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CONT_PMT_AVAIL");
        iaa_Dc_Cntrct_Cntrct_Pymnt_Mode = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Pymnt_Mode", "CNTRCT-PYMNT-MODE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MODE");
        iaa_Dc_Cntrct_Cntrct_Hist_Rate_Needed_Ind = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Hist_Rate_Needed_Ind", "CNTRCT-HIST-RATE-NEEDED-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HIST_RATE_NEEDED_IND");
        iaa_Dc_Cntrct_Cntrct_Hist_Rate_Entered_Ind = vw_iaa_Dc_Cntrct.getRecord().newFieldInGroup("iaa_Dc_Cntrct_Cntrct_Hist_Rate_Entered_Ind", "CNTRCT-HIST-RATE-ENTERED-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HIST_RATE_ENTERED_IND");
        vw_cpr.setUniquePeList();
        vw_cpr_Trans.setUniquePeList();
        vw_iaa_Tiaa_Fund_Rcrd.setUniquePeList();
        vw_iaa_Tiaa_Fund_Trans.setUniquePeList();

        this.setRecordName("LdaIaal9125");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrl_Rcrd.reset();
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Trans.reset();
        vw_cpr.reset();
        vw_cpr_Trans.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Tiaa_Fund_Trans.reset();
        vw_iaa_Dc_Sttlmnt_Req.reset();
        vw_iaa_Dc_Cntrct.reset();
    }

    // Constructor
    public LdaIaal9125() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
