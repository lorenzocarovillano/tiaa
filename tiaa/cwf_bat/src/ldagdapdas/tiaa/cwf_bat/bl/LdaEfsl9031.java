/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:58:36 PM
**        * FROM NATURAL LDA     : EFSL9031
************************************************************
**        * FILE NAME            : LdaEfsl9031.java
**        * CLASS NAME           : LdaEfsl9031
**        * INSTANCE NAME        : LdaEfsl9031
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaEfsl9031 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cwf_Folder_Stats;
    private DbsField cwf_Folder_Stats_Log_Dte;
    private DbsField cwf_Folder_Stats_System_Cde;
    private DbsField cwf_Folder_Stats_Count_Castupdate_Counts;
    private DbsGroup cwf_Folder_Stats_Update_Counts;
    private DbsField cwf_Folder_Stats_Action_Cde;
    private DbsField cwf_Folder_Stats_Xtn_Cnt;
    private DbsField cwf_Folder_Stats_Mit_Added_Cnt;
    private DbsField cwf_Folder_Stats_Mit_Updated_Cnt;
    private DbsField cwf_Folder_Stats_Fldr_Added_Cnt;
    private DbsField cwf_Folder_Stats_Fldr_Updated_Cnt;
    private DbsField cwf_Folder_Stats_Fldr_Deleted_Cnt;
    private DbsField cwf_Folder_Stats_Dcmt_Added_Cnt;
    private DbsField cwf_Folder_Stats_Dcmt_Renamed_Cnt;

    public DataAccessProgramView getVw_cwf_Folder_Stats() { return vw_cwf_Folder_Stats; }

    public DbsField getCwf_Folder_Stats_Log_Dte() { return cwf_Folder_Stats_Log_Dte; }

    public DbsField getCwf_Folder_Stats_System_Cde() { return cwf_Folder_Stats_System_Cde; }

    public DbsField getCwf_Folder_Stats_Count_Castupdate_Counts() { return cwf_Folder_Stats_Count_Castupdate_Counts; }

    public DbsGroup getCwf_Folder_Stats_Update_Counts() { return cwf_Folder_Stats_Update_Counts; }

    public DbsField getCwf_Folder_Stats_Action_Cde() { return cwf_Folder_Stats_Action_Cde; }

    public DbsField getCwf_Folder_Stats_Xtn_Cnt() { return cwf_Folder_Stats_Xtn_Cnt; }

    public DbsField getCwf_Folder_Stats_Mit_Added_Cnt() { return cwf_Folder_Stats_Mit_Added_Cnt; }

    public DbsField getCwf_Folder_Stats_Mit_Updated_Cnt() { return cwf_Folder_Stats_Mit_Updated_Cnt; }

    public DbsField getCwf_Folder_Stats_Fldr_Added_Cnt() { return cwf_Folder_Stats_Fldr_Added_Cnt; }

    public DbsField getCwf_Folder_Stats_Fldr_Updated_Cnt() { return cwf_Folder_Stats_Fldr_Updated_Cnt; }

    public DbsField getCwf_Folder_Stats_Fldr_Deleted_Cnt() { return cwf_Folder_Stats_Fldr_Deleted_Cnt; }

    public DbsField getCwf_Folder_Stats_Dcmt_Added_Cnt() { return cwf_Folder_Stats_Dcmt_Added_Cnt; }

    public DbsField getCwf_Folder_Stats_Dcmt_Renamed_Cnt() { return cwf_Folder_Stats_Dcmt_Renamed_Cnt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cwf_Folder_Stats = new DataAccessProgramView(new NameInfo("vw_cwf_Folder_Stats", "CWF-FOLDER-STATS"), "CWF_FOLDER_STATS", "CWF_FOLDER_STATS", 
            DdmPeriodicGroups.getInstance().getGroups("CWF_FOLDER_STATS"));
        cwf_Folder_Stats_Log_Dte = vw_cwf_Folder_Stats.getRecord().newFieldInGroup("cwf_Folder_Stats_Log_Dte", "LOG-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LOG_DTE");
        cwf_Folder_Stats_Log_Dte.setDdmHeader("LOG/DATE");
        cwf_Folder_Stats_System_Cde = vw_cwf_Folder_Stats.getRecord().newFieldInGroup("cwf_Folder_Stats_System_Cde", "SYSTEM-CDE", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "SYSTEM_CDE");
        cwf_Folder_Stats_System_Cde.setDdmHeader("SYSTEM/CODE");
        cwf_Folder_Stats_Count_Castupdate_Counts = vw_cwf_Folder_Stats.getRecord().newFieldInGroup("cwf_Folder_Stats_Count_Castupdate_Counts", "C*UPDATE-COUNTS", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "CWF_FOLDER_STATS_UPDATE_COUNTS");
        cwf_Folder_Stats_Update_Counts = vw_cwf_Folder_Stats.getRecord().newGroupInGroup("cwf_Folder_Stats_Update_Counts", "UPDATE-COUNTS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CWF_FOLDER_STATS_UPDATE_COUNTS");
        cwf_Folder_Stats_Action_Cde = cwf_Folder_Stats_Update_Counts.newFieldArrayInGroup("cwf_Folder_Stats_Action_Cde", "ACTION-CDE", FieldType.STRING, 
            2, new DbsArrayController(1,10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACTION_CDE", "CWF_FOLDER_STATS_UPDATE_COUNTS");
        cwf_Folder_Stats_Action_Cde.setDdmHeader("ACTION");
        cwf_Folder_Stats_Xtn_Cnt = cwf_Folder_Stats_Update_Counts.newFieldArrayInGroup("cwf_Folder_Stats_Xtn_Cnt", "XTN-CNT", FieldType.NUMERIC, 7, new 
            DbsArrayController(1,10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XTN_CNT", "CWF_FOLDER_STATS_UPDATE_COUNTS");
        cwf_Folder_Stats_Xtn_Cnt.setDdmHeader("XTN/COUNT");
        cwf_Folder_Stats_Mit_Added_Cnt = cwf_Folder_Stats_Update_Counts.newFieldArrayInGroup("cwf_Folder_Stats_Mit_Added_Cnt", "MIT-ADDED-CNT", FieldType.NUMERIC, 
            7, new DbsArrayController(1,10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_ADDED_CNT", "CWF_FOLDER_STATS_UPDATE_COUNTS");
        cwf_Folder_Stats_Mit_Added_Cnt.setDdmHeader("NBR OF MIT/RECS ADDED");
        cwf_Folder_Stats_Mit_Updated_Cnt = cwf_Folder_Stats_Update_Counts.newFieldArrayInGroup("cwf_Folder_Stats_Mit_Updated_Cnt", "MIT-UPDATED-CNT", 
            FieldType.NUMERIC, 7, new DbsArrayController(1,10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_UPDATED_CNT", "CWF_FOLDER_STATS_UPDATE_COUNTS");
        cwf_Folder_Stats_Mit_Updated_Cnt.setDdmHeader("NBR OF MIT/RECS UPDATED");
        cwf_Folder_Stats_Fldr_Added_Cnt = cwf_Folder_Stats_Update_Counts.newFieldArrayInGroup("cwf_Folder_Stats_Fldr_Added_Cnt", "FLDR-ADDED-CNT", FieldType.NUMERIC, 
            7, new DbsArrayController(1,10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "FLDR_ADDED_CNT", "CWF_FOLDER_STATS_UPDATE_COUNTS");
        cwf_Folder_Stats_Fldr_Added_Cnt.setDdmHeader("NBR OF FLDR/RECS ADDED");
        cwf_Folder_Stats_Fldr_Updated_Cnt = cwf_Folder_Stats_Update_Counts.newFieldArrayInGroup("cwf_Folder_Stats_Fldr_Updated_Cnt", "FLDR-UPDATED-CNT", 
            FieldType.NUMERIC, 7, new DbsArrayController(1,10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "FLDR_UPDATED_CNT", "CWF_FOLDER_STATS_UPDATE_COUNTS");
        cwf_Folder_Stats_Fldr_Updated_Cnt.setDdmHeader("NBR OF FLDR/RECS UPDATED");
        cwf_Folder_Stats_Fldr_Deleted_Cnt = cwf_Folder_Stats_Update_Counts.newFieldArrayInGroup("cwf_Folder_Stats_Fldr_Deleted_Cnt", "FLDR-DELETED-CNT", 
            FieldType.NUMERIC, 7, new DbsArrayController(1,10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "FLDR_DELETED_CNT", "CWF_FOLDER_STATS_UPDATE_COUNTS");
        cwf_Folder_Stats_Fldr_Deleted_Cnt.setDdmHeader("NBR OF FLDR/RECS DELETED");
        cwf_Folder_Stats_Dcmt_Added_Cnt = cwf_Folder_Stats_Update_Counts.newFieldArrayInGroup("cwf_Folder_Stats_Dcmt_Added_Cnt", "DCMT-ADDED-CNT", FieldType.NUMERIC, 
            7, new DbsArrayController(1,10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "DCMT_ADDED_CNT", "CWF_FOLDER_STATS_UPDATE_COUNTS");
        cwf_Folder_Stats_Dcmt_Added_Cnt.setDdmHeader("NBR OF DCMT/ADDED");
        cwf_Folder_Stats_Dcmt_Renamed_Cnt = cwf_Folder_Stats_Update_Counts.newFieldArrayInGroup("cwf_Folder_Stats_Dcmt_Renamed_Cnt", "DCMT-RENAMED-CNT", 
            FieldType.NUMERIC, 7, new DbsArrayController(1,10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "DCMT_RENAMED_CNT", "CWF_FOLDER_STATS_UPDATE_COUNTS");
        cwf_Folder_Stats_Dcmt_Renamed_Cnt.setDdmHeader("NBR OF DCMT/RENAMED");
        vw_cwf_Folder_Stats.setUniquePeList();

        this.setRecordName("LdaEfsl9031");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cwf_Folder_Stats.reset();
    }

    // Constructor
    public LdaEfsl9031() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
