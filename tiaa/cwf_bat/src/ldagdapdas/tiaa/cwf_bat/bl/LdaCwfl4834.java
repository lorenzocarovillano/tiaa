/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:57 PM
**        * FROM NATURAL LDA     : CWFL4834
************************************************************
**        * FILE NAME            : LdaCwfl4834.java
**        * CLASS NAME           : LdaCwfl4834
**        * INSTANCE NAME        : LdaCwfl4834
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4834 extends DbsRecord
{
    // Properties
    private DbsGroup np_Work_Lead;
    private DbsField np_Work_Lead_Pin_Npin;
    private DbsField np_Work_Lead_A1;
    private DbsField np_Work_Lead_Rqst_Log_Dte_Tme_A_15;
    private DbsField np_Work_Lead_A2;
    private DbsField np_Work_Lead_Last_Chnge_Dte_Tme_A;
    private DbsField np_Work_Lead_A3;
    private DbsField np_Work_Lead_Work_Prcss_Id;
    private DbsField np_Work_Lead_A4;
    private DbsField np_Work_Lead_Tiaa_Rcvd_Dte_Tme_A;
    private DbsField np_Work_Lead_A5;
    private DbsField np_Work_Lead_Rqst_Log_Oprtr_Cde;
    private DbsField np_Work_Lead_A6;
    private DbsField np_Work_Lead_Orgnl_Unit_Cde;
    private DbsField np_Work_Lead_A7;
    private DbsField np_Work_Lead_Admin_Unit_Cde;
    private DbsField np_Work_Lead_A8;
    private DbsField np_Work_Lead_Admin_Status_Cde;
    private DbsField np_Work_Lead_A9;
    private DbsField np_Work_Lead_Document_Type;
    private DbsField np_Work_Lead_A10;
    private DbsField np_Work_Lead_Empl_Oprtr_Cde;
    private DbsField np_Work_Lead_A11;
    private DbsField np_Work_Lead_Crprte_Due_Dte_Tme_A;
    private DbsField np_Work_Lead_A12;
    private DbsField np_Work_Lead_Crprte_Status_Ind;
    private DbsField np_Work_Lead_A13;
    private DbsField np_Work_Lead_Part_Close_Status_A;
    private DbsField np_Work_Lead_A14;
    private DbsField np_Work_Lead_Crprte_Clock_End_Dte_Tme_A;
    private DbsField np_Work_Lead_A15;
    private DbsField np_Work_Lead_Moc;
    private DbsField np_Work_Lead_A16;
    private DbsField np_Work_Lead_Part_Age;
    private DbsField np_Work_Lead_A17;
    private DbsField np_Work_Lead_Last_Chnge_Oprtr_Cde;
    private DbsField np_Work_Lead_A18;
    private DbsField np_Work_Lead_Crprte_Rqst_Log_Dte_Tme_A;
    private DbsField np_Work_Lead_A19;
    private DbsField np_Work_Lead_Actve_Ind;
    private DbsField np_Work_Lead_A20;
    private DbsField np_Work_Lead_Status_Cde;
    private DbsField np_Work_Lead_A21;
    private DbsField np_Work_Lead_Admin_Status_Updte_Dte_Tme_A;

    public DbsGroup getNp_Work_Lead() { return np_Work_Lead; }

    public DbsField getNp_Work_Lead_Pin_Npin() { return np_Work_Lead_Pin_Npin; }

    public DbsField getNp_Work_Lead_A1() { return np_Work_Lead_A1; }

    public DbsField getNp_Work_Lead_Rqst_Log_Dte_Tme_A_15() { return np_Work_Lead_Rqst_Log_Dte_Tme_A_15; }

    public DbsField getNp_Work_Lead_A2() { return np_Work_Lead_A2; }

    public DbsField getNp_Work_Lead_Last_Chnge_Dte_Tme_A() { return np_Work_Lead_Last_Chnge_Dte_Tme_A; }

    public DbsField getNp_Work_Lead_A3() { return np_Work_Lead_A3; }

    public DbsField getNp_Work_Lead_Work_Prcss_Id() { return np_Work_Lead_Work_Prcss_Id; }

    public DbsField getNp_Work_Lead_A4() { return np_Work_Lead_A4; }

    public DbsField getNp_Work_Lead_Tiaa_Rcvd_Dte_Tme_A() { return np_Work_Lead_Tiaa_Rcvd_Dte_Tme_A; }

    public DbsField getNp_Work_Lead_A5() { return np_Work_Lead_A5; }

    public DbsField getNp_Work_Lead_Rqst_Log_Oprtr_Cde() { return np_Work_Lead_Rqst_Log_Oprtr_Cde; }

    public DbsField getNp_Work_Lead_A6() { return np_Work_Lead_A6; }

    public DbsField getNp_Work_Lead_Orgnl_Unit_Cde() { return np_Work_Lead_Orgnl_Unit_Cde; }

    public DbsField getNp_Work_Lead_A7() { return np_Work_Lead_A7; }

    public DbsField getNp_Work_Lead_Admin_Unit_Cde() { return np_Work_Lead_Admin_Unit_Cde; }

    public DbsField getNp_Work_Lead_A8() { return np_Work_Lead_A8; }

    public DbsField getNp_Work_Lead_Admin_Status_Cde() { return np_Work_Lead_Admin_Status_Cde; }

    public DbsField getNp_Work_Lead_A9() { return np_Work_Lead_A9; }

    public DbsField getNp_Work_Lead_Document_Type() { return np_Work_Lead_Document_Type; }

    public DbsField getNp_Work_Lead_A10() { return np_Work_Lead_A10; }

    public DbsField getNp_Work_Lead_Empl_Oprtr_Cde() { return np_Work_Lead_Empl_Oprtr_Cde; }

    public DbsField getNp_Work_Lead_A11() { return np_Work_Lead_A11; }

    public DbsField getNp_Work_Lead_Crprte_Due_Dte_Tme_A() { return np_Work_Lead_Crprte_Due_Dte_Tme_A; }

    public DbsField getNp_Work_Lead_A12() { return np_Work_Lead_A12; }

    public DbsField getNp_Work_Lead_Crprte_Status_Ind() { return np_Work_Lead_Crprte_Status_Ind; }

    public DbsField getNp_Work_Lead_A13() { return np_Work_Lead_A13; }

    public DbsField getNp_Work_Lead_Part_Close_Status_A() { return np_Work_Lead_Part_Close_Status_A; }

    public DbsField getNp_Work_Lead_A14() { return np_Work_Lead_A14; }

    public DbsField getNp_Work_Lead_Crprte_Clock_End_Dte_Tme_A() { return np_Work_Lead_Crprte_Clock_End_Dte_Tme_A; }

    public DbsField getNp_Work_Lead_A15() { return np_Work_Lead_A15; }

    public DbsField getNp_Work_Lead_Moc() { return np_Work_Lead_Moc; }

    public DbsField getNp_Work_Lead_A16() { return np_Work_Lead_A16; }

    public DbsField getNp_Work_Lead_Part_Age() { return np_Work_Lead_Part_Age; }

    public DbsField getNp_Work_Lead_A17() { return np_Work_Lead_A17; }

    public DbsField getNp_Work_Lead_Last_Chnge_Oprtr_Cde() { return np_Work_Lead_Last_Chnge_Oprtr_Cde; }

    public DbsField getNp_Work_Lead_A18() { return np_Work_Lead_A18; }

    public DbsField getNp_Work_Lead_Crprte_Rqst_Log_Dte_Tme_A() { return np_Work_Lead_Crprte_Rqst_Log_Dte_Tme_A; }

    public DbsField getNp_Work_Lead_A19() { return np_Work_Lead_A19; }

    public DbsField getNp_Work_Lead_Actve_Ind() { return np_Work_Lead_Actve_Ind; }

    public DbsField getNp_Work_Lead_A20() { return np_Work_Lead_A20; }

    public DbsField getNp_Work_Lead_Status_Cde() { return np_Work_Lead_Status_Cde; }

    public DbsField getNp_Work_Lead_A21() { return np_Work_Lead_A21; }

    public DbsField getNp_Work_Lead_Admin_Status_Updte_Dte_Tme_A() { return np_Work_Lead_Admin_Status_Updte_Dte_Tme_A; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        np_Work_Lead = newGroupInRecord("np_Work_Lead", "NP-WORK-LEAD");
        np_Work_Lead_Pin_Npin = np_Work_Lead.newFieldInGroup("np_Work_Lead_Pin_Npin", "PIN-NPIN", FieldType.STRING, 7);
        np_Work_Lead_A1 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A1", "A1", FieldType.STRING, 1);
        np_Work_Lead_Rqst_Log_Dte_Tme_A_15 = np_Work_Lead.newFieldInGroup("np_Work_Lead_Rqst_Log_Dte_Tme_A_15", "RQST-LOG-DTE-TME-A-15", FieldType.STRING, 
            15);
        np_Work_Lead_A2 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A2", "A2", FieldType.STRING, 1);
        np_Work_Lead_Last_Chnge_Dte_Tme_A = np_Work_Lead.newFieldInGroup("np_Work_Lead_Last_Chnge_Dte_Tme_A", "LAST-CHNGE-DTE-TME-A", FieldType.STRING, 
            15);
        np_Work_Lead_A3 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A3", "A3", FieldType.STRING, 1);
        np_Work_Lead_Work_Prcss_Id = np_Work_Lead.newFieldInGroup("np_Work_Lead_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        np_Work_Lead_A4 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A4", "A4", FieldType.STRING, 1);
        np_Work_Lead_Tiaa_Rcvd_Dte_Tme_A = np_Work_Lead.newFieldInGroup("np_Work_Lead_Tiaa_Rcvd_Dte_Tme_A", "TIAA-RCVD-DTE-TME-A", FieldType.STRING, 28);
        np_Work_Lead_A5 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A5", "A5", FieldType.STRING, 1);
        np_Work_Lead_Rqst_Log_Oprtr_Cde = np_Work_Lead.newFieldInGroup("np_Work_Lead_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 8);
        np_Work_Lead_A6 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A6", "A6", FieldType.STRING, 1);
        np_Work_Lead_Orgnl_Unit_Cde = np_Work_Lead.newFieldInGroup("np_Work_Lead_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        np_Work_Lead_A7 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A7", "A7", FieldType.STRING, 1);
        np_Work_Lead_Admin_Unit_Cde = np_Work_Lead.newFieldInGroup("np_Work_Lead_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        np_Work_Lead_A8 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A8", "A8", FieldType.STRING, 1);
        np_Work_Lead_Admin_Status_Cde = np_Work_Lead.newFieldInGroup("np_Work_Lead_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4);
        np_Work_Lead_A9 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A9", "A9", FieldType.STRING, 1);
        np_Work_Lead_Document_Type = np_Work_Lead.newFieldInGroup("np_Work_Lead_Document_Type", "DOCUMENT-TYPE", FieldType.STRING, 8);
        np_Work_Lead_A10 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A10", "A10", FieldType.STRING, 1);
        np_Work_Lead_Empl_Oprtr_Cde = np_Work_Lead.newFieldInGroup("np_Work_Lead_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 8);
        np_Work_Lead_A11 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A11", "A11", FieldType.STRING, 1);
        np_Work_Lead_Crprte_Due_Dte_Tme_A = np_Work_Lead.newFieldInGroup("np_Work_Lead_Crprte_Due_Dte_Tme_A", "CRPRTE-DUE-DTE-TME-A", FieldType.STRING, 
            28);
        np_Work_Lead_A12 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A12", "A12", FieldType.STRING, 1);
        np_Work_Lead_Crprte_Status_Ind = np_Work_Lead.newFieldInGroup("np_Work_Lead_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1);
        np_Work_Lead_A13 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A13", "A13", FieldType.STRING, 1);
        np_Work_Lead_Part_Close_Status_A = np_Work_Lead.newFieldInGroup("np_Work_Lead_Part_Close_Status_A", "PART-CLOSE-STATUS-A", FieldType.STRING, 4);
        np_Work_Lead_A14 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A14", "A14", FieldType.STRING, 1);
        np_Work_Lead_Crprte_Clock_End_Dte_Tme_A = np_Work_Lead.newFieldInGroup("np_Work_Lead_Crprte_Clock_End_Dte_Tme_A", "CRPRTE-CLOCK-END-DTE-TME-A", 
            FieldType.STRING, 28);
        np_Work_Lead_A15 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A15", "A15", FieldType.STRING, 1);
        np_Work_Lead_Moc = np_Work_Lead.newFieldInGroup("np_Work_Lead_Moc", "MOC", FieldType.STRING, 1);
        np_Work_Lead_A16 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A16", "A16", FieldType.STRING, 1);
        np_Work_Lead_Part_Age = np_Work_Lead.newFieldInGroup("np_Work_Lead_Part_Age", "PART-AGE", FieldType.NUMERIC, 3);
        np_Work_Lead_A17 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A17", "A17", FieldType.STRING, 1);
        np_Work_Lead_Last_Chnge_Oprtr_Cde = np_Work_Lead.newFieldInGroup("np_Work_Lead_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8);
        np_Work_Lead_A18 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A18", "A18", FieldType.STRING, 1);
        np_Work_Lead_Crprte_Rqst_Log_Dte_Tme_A = np_Work_Lead.newFieldInGroup("np_Work_Lead_Crprte_Rqst_Log_Dte_Tme_A", "CRPRTE-RQST-LOG-DTE-TME-A", FieldType.STRING, 
            28);
        np_Work_Lead_A19 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A19", "A19", FieldType.STRING, 1);
        np_Work_Lead_Actve_Ind = np_Work_Lead.newFieldInGroup("np_Work_Lead_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2);
        np_Work_Lead_A20 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A20", "A20", FieldType.STRING, 1);
        np_Work_Lead_Status_Cde = np_Work_Lead.newFieldInGroup("np_Work_Lead_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        np_Work_Lead_A21 = np_Work_Lead.newFieldInGroup("np_Work_Lead_A21", "A21", FieldType.STRING, 1);
        np_Work_Lead_Admin_Status_Updte_Dte_Tme_A = np_Work_Lead.newFieldInGroup("np_Work_Lead_Admin_Status_Updte_Dte_Tme_A", "ADMIN-STATUS-UPDTE-DTE-TME-A", 
            FieldType.STRING, 15);

        this.setRecordName("LdaCwfl4834");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4834() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
