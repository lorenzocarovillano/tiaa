/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:06 PM
**        * FROM NATURAL PDA     : EFSA6110
************************************************************
**        * FILE NAME            : PdaEfsa6110.java
**        * CLASS NAME           : PdaEfsa6110
**        * INSTANCE NAME        : PdaEfsa6110
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaEfsa6110 extends PdaBase
{
    // Properties
    private DbsGroup efsa6110;
    private DbsField efsa6110_Parm_Source_Id;
    private DbsField efsa6110_Parm_Min;
    private DbsGroup efsa6110_Parm_MinRedef1;
    private DbsField efsa6110_Parm_Min_A;
    private DbsField efsa6110_Parm_Return_Code;
    private DbsField efsa6110_Parm_Err_Msg;
    private DbsField efsa6110_Parm_Cabinet_Id;
    private DbsField efsa6110_Parm_System;
    private DbsField efsa6110_Parm_Filler;

    public DbsGroup getEfsa6110() { return efsa6110; }

    public DbsField getEfsa6110_Parm_Source_Id() { return efsa6110_Parm_Source_Id; }

    public DbsField getEfsa6110_Parm_Min() { return efsa6110_Parm_Min; }

    public DbsGroup getEfsa6110_Parm_MinRedef1() { return efsa6110_Parm_MinRedef1; }

    public DbsField getEfsa6110_Parm_Min_A() { return efsa6110_Parm_Min_A; }

    public DbsField getEfsa6110_Parm_Return_Code() { return efsa6110_Parm_Return_Code; }

    public DbsField getEfsa6110_Parm_Err_Msg() { return efsa6110_Parm_Err_Msg; }

    public DbsField getEfsa6110_Parm_Cabinet_Id() { return efsa6110_Parm_Cabinet_Id; }

    public DbsField getEfsa6110_Parm_System() { return efsa6110_Parm_System; }

    public DbsField getEfsa6110_Parm_Filler() { return efsa6110_Parm_Filler; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        efsa6110 = dbsRecord.newGroupInRecord("efsa6110", "EFSA6110");
        efsa6110.setParameterOption(ParameterOption.ByReference);
        efsa6110_Parm_Source_Id = efsa6110.newFieldInGroup("efsa6110_Parm_Source_Id", "PARM-SOURCE-ID", FieldType.STRING, 6);
        efsa6110_Parm_Min = efsa6110.newFieldInGroup("efsa6110_Parm_Min", "PARM-MIN", FieldType.NUMERIC, 11);
        efsa6110_Parm_MinRedef1 = efsa6110.newGroupInGroup("efsa6110_Parm_MinRedef1", "Redefines", efsa6110_Parm_Min);
        efsa6110_Parm_Min_A = efsa6110_Parm_MinRedef1.newFieldInGroup("efsa6110_Parm_Min_A", "PARM-MIN-A", FieldType.STRING, 11);
        efsa6110_Parm_Return_Code = efsa6110.newFieldInGroup("efsa6110_Parm_Return_Code", "PARM-RETURN-CODE", FieldType.STRING, 1);
        efsa6110_Parm_Err_Msg = efsa6110.newFieldInGroup("efsa6110_Parm_Err_Msg", "PARM-ERR-MSG", FieldType.STRING, 50);
        efsa6110_Parm_Cabinet_Id = efsa6110.newFieldInGroup("efsa6110_Parm_Cabinet_Id", "PARM-CABINET-ID", FieldType.STRING, 15);
        efsa6110_Parm_System = efsa6110.newFieldInGroup("efsa6110_Parm_System", "PARM-SYSTEM", FieldType.STRING, 3);
        efsa6110_Parm_Filler = efsa6110.newFieldInGroup("efsa6110_Parm_Filler", "PARM-FILLER", FieldType.STRING, 10);

        dbsRecord.reset();
    }

    // Constructors
    public PdaEfsa6110(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

