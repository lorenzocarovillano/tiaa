/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:48 PM
**        * FROM NATURAL LDA     : CWFL4826
************************************************************
**        * FILE NAME            : LdaCwfl4826.java
**        * CLASS NAME           : LdaCwfl4826
**        * INSTANCE NAME        : LdaCwfl4826
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4826 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cwf_Support_Tbl_Last_Run_Id;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Data_Field;
    private DbsGroup cwf_Support_Tbl_Last_Run_Id_Tbl_Data_FieldRedef1;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Last_Run_Id;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Enable_Auto_Run_Control_Creation;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Work_Rqst_Retaining_Days;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Access_Level;

    public DataAccessProgramView getVw_cwf_Support_Tbl_Last_Run_Id() { return vw_cwf_Support_Tbl_Last_Run_Id; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Scrty_Level_Ind() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Scrty_Level_Ind; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Table_Nme() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Nme; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Key_Field() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Key_Field; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Data_Field() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Data_Field; }

    public DbsGroup getCwf_Support_Tbl_Last_Run_Id_Tbl_Data_FieldRedef1() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Data_FieldRedef1; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Last_Run_Id() { return cwf_Support_Tbl_Last_Run_Id_Last_Run_Id; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Enable_Auto_Run_Control_Creation() { return cwf_Support_Tbl_Last_Run_Id_Enable_Auto_Run_Control_Creation; 
        }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Work_Rqst_Retaining_Days() { return cwf_Support_Tbl_Last_Run_Id_Work_Rqst_Retaining_Days; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Actve_Ind() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Actve_Ind; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Entry_Dte_Tme() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Entry_Dte_Tme; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Entry_Oprtr_Cde() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Entry_Oprtr_Cde; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Dte_Tme() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Dte_Tme; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Dte() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Dte; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Oprtr_Cde() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Oprtr_Cde; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Dlte_Dte_Tme() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Dlte_Dte_Tme; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Dlte_Oprtr_Cde() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Dlte_Oprtr_Cde; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Table_Rectype() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Rectype; }

    public DbsField getCwf_Support_Tbl_Last_Run_Id_Tbl_Table_Access_Level() { return cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Access_Level; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cwf_Support_Tbl_Last_Run_Id = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl_Last_Run_Id", "CWF-SUPPORT-TBL-LAST-RUN-ID"), "CWF_SUPPORT_TBL", 
            "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Scrty_Level_Ind", 
            "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Nme = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Nme", 
            "TBL-TABLE-NME", FieldType.STRING, 20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Key_Field = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Key_Field", 
            "TBL-KEY-FIELD", FieldType.STRING, 30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Data_Field = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Data_Field", 
            "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Data_FieldRedef1 = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newGroupInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Data_FieldRedef1", 
            "Redefines", cwf_Support_Tbl_Last_Run_Id_Tbl_Data_Field);
        cwf_Support_Tbl_Last_Run_Id_Last_Run_Id = cwf_Support_Tbl_Last_Run_Id_Tbl_Data_FieldRedef1.newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Last_Run_Id", 
            "LAST-RUN-ID", FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Last_Run_Id_Enable_Auto_Run_Control_Creation = cwf_Support_Tbl_Last_Run_Id_Tbl_Data_FieldRedef1.newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Enable_Auto_Run_Control_Creation", 
            "ENABLE-AUTO-RUN-CONTROL-CREATION", FieldType.BOOLEAN);
        cwf_Support_Tbl_Last_Run_Id_Work_Rqst_Retaining_Days = cwf_Support_Tbl_Last_Run_Id_Tbl_Data_FieldRedef1.newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Work_Rqst_Retaining_Days", 
            "WORK-RQST-RETAINING-DAYS", FieldType.NUMERIC, 3);
        cwf_Support_Tbl_Last_Run_Id_Tbl_Actve_Ind = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Actve_Ind", 
            "TBL-ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Entry_Dte_Tme", 
            "TBL-ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Entry_Oprtr_Cde", 
            "TBL-ENTRY-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Dte_Tme", 
            "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Dte = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Dte", 
            "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Oprtr_Cde", 
            "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Dlte_Dte_Tme", 
            "TBL-DLTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Dlte_Oprtr_Cde", 
            "TBL-DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Rectype = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Rectype", 
            "TBL-TABLE-RECTYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Access_Level = vw_cwf_Support_Tbl_Last_Run_Id.getRecord().newFieldInGroup("cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Access_Level", 
            "TBL-TABLE-ACCESS-LEVEL", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Last_Run_Id_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");

        this.setRecordName("LdaCwfl4826");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cwf_Support_Tbl_Last_Run_Id.reset();
    }

    // Constructor
    public LdaCwfl4826() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
