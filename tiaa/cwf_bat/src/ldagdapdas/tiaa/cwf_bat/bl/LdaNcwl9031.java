/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:09:13 PM
**        * FROM NATURAL LDA     : NCWL9031
************************************************************
**        * FILE NAME            : LdaNcwl9031.java
**        * CLASS NAME           : LdaNcwl9031
**        * INSTANCE NAME        : LdaNcwl9031
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaNcwl9031 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_ncw_Efm_Stats;
    private DbsField ncw_Efm_Stats_Log_Dte;
    private DbsField ncw_Efm_Stats_System_Cde;
    private DbsField ncw_Efm_Stats_Count_Castupdate_Counts;
    private DbsGroup ncw_Efm_Stats_Update_Counts;
    private DbsField ncw_Efm_Stats_Action_Cde;
    private DbsField ncw_Efm_Stats_Xtn_Cnt;
    private DbsField ncw_Efm_Stats_Mit_Added_Cnt;
    private DbsField ncw_Efm_Stats_Mit_Updated_Cnt;
    private DbsField ncw_Efm_Stats_Dcmnt_Added_Cnt;
    private DbsField ncw_Efm_Stats_Dcmnt_Renamed_Cnt;

    public DataAccessProgramView getVw_ncw_Efm_Stats() { return vw_ncw_Efm_Stats; }

    public DbsField getNcw_Efm_Stats_Log_Dte() { return ncw_Efm_Stats_Log_Dte; }

    public DbsField getNcw_Efm_Stats_System_Cde() { return ncw_Efm_Stats_System_Cde; }

    public DbsField getNcw_Efm_Stats_Count_Castupdate_Counts() { return ncw_Efm_Stats_Count_Castupdate_Counts; }

    public DbsGroup getNcw_Efm_Stats_Update_Counts() { return ncw_Efm_Stats_Update_Counts; }

    public DbsField getNcw_Efm_Stats_Action_Cde() { return ncw_Efm_Stats_Action_Cde; }

    public DbsField getNcw_Efm_Stats_Xtn_Cnt() { return ncw_Efm_Stats_Xtn_Cnt; }

    public DbsField getNcw_Efm_Stats_Mit_Added_Cnt() { return ncw_Efm_Stats_Mit_Added_Cnt; }

    public DbsField getNcw_Efm_Stats_Mit_Updated_Cnt() { return ncw_Efm_Stats_Mit_Updated_Cnt; }

    public DbsField getNcw_Efm_Stats_Dcmnt_Added_Cnt() { return ncw_Efm_Stats_Dcmnt_Added_Cnt; }

    public DbsField getNcw_Efm_Stats_Dcmnt_Renamed_Cnt() { return ncw_Efm_Stats_Dcmnt_Renamed_Cnt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_ncw_Efm_Stats = new DataAccessProgramView(new NameInfo("vw_ncw_Efm_Stats", "NCW-EFM-STATS"), "NCW_EFM_STATS", "NCW_EFM_STATS", DdmPeriodicGroups.getInstance().getGroups("NCW_EFM_STATS"));
        ncw_Efm_Stats_Log_Dte = vw_ncw_Efm_Stats.getRecord().newFieldInGroup("ncw_Efm_Stats_Log_Dte", "LOG-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LOG_DTE");
        ncw_Efm_Stats_Log_Dte.setDdmHeader("LOG/DATE");
        ncw_Efm_Stats_System_Cde = vw_ncw_Efm_Stats.getRecord().newFieldInGroup("ncw_Efm_Stats_System_Cde", "SYSTEM-CDE", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTEM_CDE");
        ncw_Efm_Stats_System_Cde.setDdmHeader("SYSTEM/CODE");
        ncw_Efm_Stats_Count_Castupdate_Counts = vw_ncw_Efm_Stats.getRecord().newFieldInGroup("ncw_Efm_Stats_Count_Castupdate_Counts", "C*UPDATE-COUNTS", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "NCW_EFM_STATS_UPDATE_COUNTS");
        ncw_Efm_Stats_Update_Counts = vw_ncw_Efm_Stats.getRecord().newGroupInGroup("ncw_Efm_Stats_Update_Counts", "UPDATE-COUNTS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "NCW_EFM_STATS_UPDATE_COUNTS");
        ncw_Efm_Stats_Action_Cde = ncw_Efm_Stats_Update_Counts.newFieldArrayInGroup("ncw_Efm_Stats_Action_Cde", "ACTION-CDE", FieldType.STRING, 2, new 
            DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACTION_CDE", "NCW_EFM_STATS_UPDATE_COUNTS");
        ncw_Efm_Stats_Action_Cde.setDdmHeader("ACTION");
        ncw_Efm_Stats_Xtn_Cnt = ncw_Efm_Stats_Update_Counts.newFieldArrayInGroup("ncw_Efm_Stats_Xtn_Cnt", "XTN-CNT", FieldType.NUMERIC, 7, new DbsArrayController(1,99) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XTN_CNT", "NCW_EFM_STATS_UPDATE_COUNTS");
        ncw_Efm_Stats_Xtn_Cnt.setDdmHeader("XTN/COUNT");
        ncw_Efm_Stats_Mit_Added_Cnt = ncw_Efm_Stats_Update_Counts.newFieldArrayInGroup("ncw_Efm_Stats_Mit_Added_Cnt", "MIT-ADDED-CNT", FieldType.NUMERIC, 
            7, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_ADDED_CNT", "NCW_EFM_STATS_UPDATE_COUNTS");
        ncw_Efm_Stats_Mit_Added_Cnt.setDdmHeader("NBR OF MIT/RECS ADDED");
        ncw_Efm_Stats_Mit_Updated_Cnt = ncw_Efm_Stats_Update_Counts.newFieldArrayInGroup("ncw_Efm_Stats_Mit_Updated_Cnt", "MIT-UPDATED-CNT", FieldType.NUMERIC, 
            7, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_UPDATED_CNT", "NCW_EFM_STATS_UPDATE_COUNTS");
        ncw_Efm_Stats_Mit_Updated_Cnt.setDdmHeader("NBR OF MIT/RECS UPDATED");
        ncw_Efm_Stats_Dcmnt_Added_Cnt = ncw_Efm_Stats_Update_Counts.newFieldArrayInGroup("ncw_Efm_Stats_Dcmnt_Added_Cnt", "DCMNT-ADDED-CNT", FieldType.NUMERIC, 
            7, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "DCMNT_ADDED_CNT", "NCW_EFM_STATS_UPDATE_COUNTS");
        ncw_Efm_Stats_Dcmnt_Added_Cnt.setDdmHeader("NBR OF DCMT/ADDED");
        ncw_Efm_Stats_Dcmnt_Renamed_Cnt = ncw_Efm_Stats_Update_Counts.newFieldArrayInGroup("ncw_Efm_Stats_Dcmnt_Renamed_Cnt", "DCMNT-RENAMED-CNT", FieldType.NUMERIC, 
            7, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "DCMNT_RENAMED_CNT", "NCW_EFM_STATS_UPDATE_COUNTS");
        ncw_Efm_Stats_Dcmnt_Renamed_Cnt.setDdmHeader("NBR OF DCMT/RENAMED");
        vw_ncw_Efm_Stats.setUniquePeList();

        this.setRecordName("LdaNcwl9031");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_ncw_Efm_Stats.reset();
    }

    // Constructor
    public LdaNcwl9031() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
