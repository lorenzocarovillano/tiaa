/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:57 PM
**        * FROM NATURAL LDA     : CWFL4897
************************************************************
**        * FILE NAME            : LdaCwfl4897.java
**        * CLASS NAME           : LdaCwfl4897
**        * INSTANCE NAME        : LdaCwfl4897
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4897 extends DbsRecord
{
    // Properties
    private DbsGroup customer_New;
    private DbsField customer_New_Pin_Npin;
    private DbsField customer_New_D1;
    private DbsField customer_New_Ssn;
    private DbsField customer_New_D2;
    private DbsField customer_New_Part_Name;
    private DbsField customer_New_D3;
    private DbsField customer_New_State_Of_Res;
    private DbsField customer_New_D4;
    private DbsField customer_New_Dte_Of_Birth;
    private DbsField customer_New_D5;
    private DbsField customer_New_Pin_Type;
    private DbsField customer_New_D6;
    private DbsField customer_New_Last_Chnge_Dte_Tme_A;

    public DbsGroup getCustomer_New() { return customer_New; }

    public DbsField getCustomer_New_Pin_Npin() { return customer_New_Pin_Npin; }

    public DbsField getCustomer_New_D1() { return customer_New_D1; }

    public DbsField getCustomer_New_Ssn() { return customer_New_Ssn; }

    public DbsField getCustomer_New_D2() { return customer_New_D2; }

    public DbsField getCustomer_New_Part_Name() { return customer_New_Part_Name; }

    public DbsField getCustomer_New_D3() { return customer_New_D3; }

    public DbsField getCustomer_New_State_Of_Res() { return customer_New_State_Of_Res; }

    public DbsField getCustomer_New_D4() { return customer_New_D4; }

    public DbsField getCustomer_New_Dte_Of_Birth() { return customer_New_Dte_Of_Birth; }

    public DbsField getCustomer_New_D5() { return customer_New_D5; }

    public DbsField getCustomer_New_Pin_Type() { return customer_New_Pin_Type; }

    public DbsField getCustomer_New_D6() { return customer_New_D6; }

    public DbsField getCustomer_New_Last_Chnge_Dte_Tme_A() { return customer_New_Last_Chnge_Dte_Tme_A; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        customer_New = newGroupInRecord("customer_New", "CUSTOMER-NEW");
        customer_New_Pin_Npin = customer_New.newFieldInGroup("customer_New_Pin_Npin", "PIN-NPIN", FieldType.STRING, 7);
        customer_New_D1 = customer_New.newFieldInGroup("customer_New_D1", "D1", FieldType.STRING, 1);
        customer_New_Ssn = customer_New.newFieldInGroup("customer_New_Ssn", "SSN", FieldType.NUMERIC, 9);
        customer_New_D2 = customer_New.newFieldInGroup("customer_New_D2", "D2", FieldType.STRING, 1);
        customer_New_Part_Name = customer_New.newFieldInGroup("customer_New_Part_Name", "PART-NAME", FieldType.STRING, 40);
        customer_New_D3 = customer_New.newFieldInGroup("customer_New_D3", "D3", FieldType.STRING, 1);
        customer_New_State_Of_Res = customer_New.newFieldInGroup("customer_New_State_Of_Res", "STATE-OF-RES", FieldType.STRING, 2);
        customer_New_D4 = customer_New.newFieldInGroup("customer_New_D4", "D4", FieldType.STRING, 1);
        customer_New_Dte_Of_Birth = customer_New.newFieldInGroup("customer_New_Dte_Of_Birth", "DTE-OF-BIRTH", FieldType.STRING, 12);
        customer_New_D5 = customer_New.newFieldInGroup("customer_New_D5", "D5", FieldType.STRING, 1);
        customer_New_Pin_Type = customer_New.newFieldInGroup("customer_New_Pin_Type", "PIN-TYPE", FieldType.STRING, 1);
        customer_New_D6 = customer_New.newFieldInGroup("customer_New_D6", "D6", FieldType.STRING, 1);
        customer_New_Last_Chnge_Dte_Tme_A = customer_New.newFieldInGroup("customer_New_Last_Chnge_Dte_Tme_A", "LAST-CHNGE-DTE-TME-A", FieldType.STRING, 
            15);

        this.setRecordName("LdaCwfl4897");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4897() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
