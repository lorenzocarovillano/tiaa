/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:45 PM
**        * FROM NATURAL LDA     : CWFL4818
************************************************************
**        * FILE NAME            : LdaCwfl4818.java
**        * CLASS NAME           : LdaCwfl4818
**        * INSTANCE NAME        : LdaCwfl4818
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4818 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cwf_Route_Support_Tbl;
    private DbsField cwf_Route_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Route_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Route_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Route_Support_Tbl_Tbl_Data_Field;
    private DbsGroup cwf_Route_Support_Tbl_Tbl_Data_FieldRedef1;
    private DbsField cwf_Route_Support_Tbl_Wpid;
    private DbsField cwf_Route_Support_Tbl_Wpid_Desc;
    private DbsField cwf_Route_Support_Tbl_Unit_Code;
    private DbsField cwf_Route_Support_Tbl_Status_Code;
    private DbsField cwf_Route_Support_Tbl_Status_Desc;
    private DbsField cwf_Route_Support_Tbl_Work_Step;
    private DbsField cwf_Route_Support_Tbl_Step_Name;
    private DbsField cwf_Route_Support_Tbl_Route_Desc;
    private DbsField cwf_Route_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Route_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Route_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Route_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Route_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Route_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Route_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Route_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Route_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Route_Support_Tbl_Tbl_Table_Access_Level;

    public DataAccessProgramView getVw_cwf_Route_Support_Tbl() { return vw_cwf_Route_Support_Tbl; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Scrty_Level_Ind() { return cwf_Route_Support_Tbl_Tbl_Scrty_Level_Ind; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Table_Nme() { return cwf_Route_Support_Tbl_Tbl_Table_Nme; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Key_Field() { return cwf_Route_Support_Tbl_Tbl_Key_Field; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Data_Field() { return cwf_Route_Support_Tbl_Tbl_Data_Field; }

    public DbsGroup getCwf_Route_Support_Tbl_Tbl_Data_FieldRedef1() { return cwf_Route_Support_Tbl_Tbl_Data_FieldRedef1; }

    public DbsField getCwf_Route_Support_Tbl_Wpid() { return cwf_Route_Support_Tbl_Wpid; }

    public DbsField getCwf_Route_Support_Tbl_Wpid_Desc() { return cwf_Route_Support_Tbl_Wpid_Desc; }

    public DbsField getCwf_Route_Support_Tbl_Unit_Code() { return cwf_Route_Support_Tbl_Unit_Code; }

    public DbsField getCwf_Route_Support_Tbl_Status_Code() { return cwf_Route_Support_Tbl_Status_Code; }

    public DbsField getCwf_Route_Support_Tbl_Status_Desc() { return cwf_Route_Support_Tbl_Status_Desc; }

    public DbsField getCwf_Route_Support_Tbl_Work_Step() { return cwf_Route_Support_Tbl_Work_Step; }

    public DbsField getCwf_Route_Support_Tbl_Step_Name() { return cwf_Route_Support_Tbl_Step_Name; }

    public DbsField getCwf_Route_Support_Tbl_Route_Desc() { return cwf_Route_Support_Tbl_Route_Desc; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Actve_Ind() { return cwf_Route_Support_Tbl_Tbl_Actve_Ind; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Entry_Dte_Tme() { return cwf_Route_Support_Tbl_Tbl_Entry_Dte_Tme; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Entry_Oprtr_Cde() { return cwf_Route_Support_Tbl_Tbl_Entry_Oprtr_Cde; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Updte_Dte_Tme() { return cwf_Route_Support_Tbl_Tbl_Updte_Dte_Tme; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Updte_Dte() { return cwf_Route_Support_Tbl_Tbl_Updte_Dte; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Updte_Oprtr_Cde() { return cwf_Route_Support_Tbl_Tbl_Updte_Oprtr_Cde; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Dlte_Dte_Tme() { return cwf_Route_Support_Tbl_Tbl_Dlte_Dte_Tme; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Dlte_Oprtr_Cde() { return cwf_Route_Support_Tbl_Tbl_Dlte_Oprtr_Cde; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Table_Rectype() { return cwf_Route_Support_Tbl_Tbl_Table_Rectype; }

    public DbsField getCwf_Route_Support_Tbl_Tbl_Table_Access_Level() { return cwf_Route_Support_Tbl_Tbl_Table_Access_Level; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cwf_Route_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Route_Support_Tbl", "CWF-ROUTE-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Route_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Scrty_Level_Ind", 
            "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Route_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Route_Support_Tbl_Tbl_Table_Nme = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Route_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Route_Support_Tbl_Tbl_Key_Field = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Route_Support_Tbl_Tbl_Data_Field = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", 
            FieldType.STRING, 253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        cwf_Route_Support_Tbl_Tbl_Data_FieldRedef1 = vw_cwf_Route_Support_Tbl.getRecord().newGroupInGroup("cwf_Route_Support_Tbl_Tbl_Data_FieldRedef1", 
            "Redefines", cwf_Route_Support_Tbl_Tbl_Data_Field);
        cwf_Route_Support_Tbl_Wpid = cwf_Route_Support_Tbl_Tbl_Data_FieldRedef1.newFieldInGroup("cwf_Route_Support_Tbl_Wpid", "WPID", FieldType.STRING, 
            6);
        cwf_Route_Support_Tbl_Wpid_Desc = cwf_Route_Support_Tbl_Tbl_Data_FieldRedef1.newFieldInGroup("cwf_Route_Support_Tbl_Wpid_Desc", "WPID-DESC", FieldType.STRING, 
            15);
        cwf_Route_Support_Tbl_Unit_Code = cwf_Route_Support_Tbl_Tbl_Data_FieldRedef1.newFieldInGroup("cwf_Route_Support_Tbl_Unit_Code", "UNIT-CODE", FieldType.STRING, 
            8);
        cwf_Route_Support_Tbl_Status_Code = cwf_Route_Support_Tbl_Tbl_Data_FieldRedef1.newFieldInGroup("cwf_Route_Support_Tbl_Status_Code", "STATUS-CODE", 
            FieldType.STRING, 4);
        cwf_Route_Support_Tbl_Status_Desc = cwf_Route_Support_Tbl_Tbl_Data_FieldRedef1.newFieldInGroup("cwf_Route_Support_Tbl_Status_Desc", "STATUS-DESC", 
            FieldType.STRING, 25);
        cwf_Route_Support_Tbl_Work_Step = cwf_Route_Support_Tbl_Tbl_Data_FieldRedef1.newFieldInGroup("cwf_Route_Support_Tbl_Work_Step", "WORK-STEP", FieldType.STRING, 
            6);
        cwf_Route_Support_Tbl_Step_Name = cwf_Route_Support_Tbl_Tbl_Data_FieldRedef1.newFieldInGroup("cwf_Route_Support_Tbl_Step_Name", "STEP-NAME", FieldType.STRING, 
            15);
        cwf_Route_Support_Tbl_Route_Desc = cwf_Route_Support_Tbl_Tbl_Data_FieldRedef1.newFieldInGroup("cwf_Route_Support_Tbl_Route_Desc", "ROUTE-DESC", 
            FieldType.STRING, 40);
        cwf_Route_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Route_Support_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Route_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Route_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Entry_Oprtr_Cde", 
            "TBL-ENTRY-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Route_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Route_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Route_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Route_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Route_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Route_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Updte_Oprtr_Cde", 
            "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Route_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Route_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Route_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Route_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Route_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Route_Support_Tbl.getRecord().newFieldInGroup("cwf_Route_Support_Tbl_Tbl_Table_Access_Level", 
            "TBL-TABLE-ACCESS-LEVEL", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Route_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");

        this.setRecordName("LdaCwfl4818");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cwf_Route_Support_Tbl.reset();
    }

    // Constructor
    public LdaCwfl4818() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
