/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:57 PM
**        * FROM NATURAL LDA     : CWFL4835
************************************************************
**        * FILE NAME            : LdaCwfl4835.java
**        * CLASS NAME           : LdaCwfl4835
**        * INSTANCE NAME        : LdaCwfl4835
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4835 extends DbsRecord
{
    // Properties
    private DbsGroup np_Customer;
    private DbsField np_Customer_Pin_Npin;
    private DbsField np_Customer_B1;
    private DbsField np_Customer_Ssn;
    private DbsField np_Customer_B2;
    private DbsField np_Customer_Part_Name;
    private DbsField np_Customer_B3;
    private DbsField np_Customer_State_Of_Res;
    private DbsField np_Customer_B4;
    private DbsField np_Customer_Dte_Of_Birth;
    private DbsField np_Customer_B5;
    private DbsField np_Customer_Pin_Type;
    private DbsField np_Customer_B6;
    private DbsField np_Customer_Last_Chnge_Dte_Tme_A;

    public DbsGroup getNp_Customer() { return np_Customer; }

    public DbsField getNp_Customer_Pin_Npin() { return np_Customer_Pin_Npin; }

    public DbsField getNp_Customer_B1() { return np_Customer_B1; }

    public DbsField getNp_Customer_Ssn() { return np_Customer_Ssn; }

    public DbsField getNp_Customer_B2() { return np_Customer_B2; }

    public DbsField getNp_Customer_Part_Name() { return np_Customer_Part_Name; }

    public DbsField getNp_Customer_B3() { return np_Customer_B3; }

    public DbsField getNp_Customer_State_Of_Res() { return np_Customer_State_Of_Res; }

    public DbsField getNp_Customer_B4() { return np_Customer_B4; }

    public DbsField getNp_Customer_Dte_Of_Birth() { return np_Customer_Dte_Of_Birth; }

    public DbsField getNp_Customer_B5() { return np_Customer_B5; }

    public DbsField getNp_Customer_Pin_Type() { return np_Customer_Pin_Type; }

    public DbsField getNp_Customer_B6() { return np_Customer_B6; }

    public DbsField getNp_Customer_Last_Chnge_Dte_Tme_A() { return np_Customer_Last_Chnge_Dte_Tme_A; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        np_Customer = newGroupInRecord("np_Customer", "NP-CUSTOMER");
        np_Customer_Pin_Npin = np_Customer.newFieldInGroup("np_Customer_Pin_Npin", "PIN-NPIN", FieldType.STRING, 7);
        np_Customer_B1 = np_Customer.newFieldInGroup("np_Customer_B1", "B1", FieldType.STRING, 1);
        np_Customer_Ssn = np_Customer.newFieldInGroup("np_Customer_Ssn", "SSN", FieldType.NUMERIC, 9);
        np_Customer_B2 = np_Customer.newFieldInGroup("np_Customer_B2", "B2", FieldType.STRING, 1);
        np_Customer_Part_Name = np_Customer.newFieldInGroup("np_Customer_Part_Name", "PART-NAME", FieldType.STRING, 40);
        np_Customer_B3 = np_Customer.newFieldInGroup("np_Customer_B3", "B3", FieldType.STRING, 1);
        np_Customer_State_Of_Res = np_Customer.newFieldInGroup("np_Customer_State_Of_Res", "STATE-OF-RES", FieldType.STRING, 2);
        np_Customer_B4 = np_Customer.newFieldInGroup("np_Customer_B4", "B4", FieldType.STRING, 1);
        np_Customer_Dte_Of_Birth = np_Customer.newFieldInGroup("np_Customer_Dte_Of_Birth", "DTE-OF-BIRTH", FieldType.STRING, 12);
        np_Customer_B5 = np_Customer.newFieldInGroup("np_Customer_B5", "B5", FieldType.STRING, 1);
        np_Customer_Pin_Type = np_Customer.newFieldInGroup("np_Customer_Pin_Type", "PIN-TYPE", FieldType.STRING, 1);
        np_Customer_B6 = np_Customer.newFieldInGroup("np_Customer_B6", "B6", FieldType.STRING, 1);
        np_Customer_Last_Chnge_Dte_Tme_A = np_Customer.newFieldInGroup("np_Customer_Last_Chnge_Dte_Tme_A", "LAST-CHNGE-DTE-TME-A", FieldType.STRING, 15);

        this.setRecordName("LdaCwfl4835");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4835() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
