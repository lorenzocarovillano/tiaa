/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:03 PM
**        * FROM NATURAL LDA     : CWFL4034
************************************************************
**        * FILE NAME            : LdaCwfl4034.java
**        * CLASS NAME           : LdaCwfl4034
**        * INSTANCE NAME        : LdaCwfl4034
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4034 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cwf_Work_Grp;
    private DbsField cwf_Work_Grp_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Work_Grp_Tbl_Table_Nme;
    private DbsField cwf_Work_Grp_Tbl_Key_Field;
    private DbsGroup cwf_Work_Grp_Tbl_Key_FieldRedef1;
    private DbsField cwf_Work_Grp_Wk_Code;
    private DbsField cwf_Work_Grp_Key_Fill;
    private DbsField cwf_Work_Grp_Tbl_Data_Field;
    private DbsGroup cwf_Work_Grp_Tbl_Data_FieldRedef2;
    private DbsField cwf_Work_Grp_Wk_Desc;
    private DbsField cwf_Work_Grp_Resp_Division;
    private DbsField cwf_Work_Grp_Data_Fill;
    private DbsGroup cwf_Work_Grp_Data_FillRedef3;
    private DbsField cwf_Work_Grp_Work_Prcss_Id;
    private DbsField cwf_Work_Grp_Fill_It;
    private DbsField cwf_Work_Grp_Tbl_Actve_Ind;
    private DbsGroup cwf_Work_Grp_Tbl_Actve_IndRedef4;
    private DbsField cwf_Work_Grp_Fill_Acb770;
    private DbsField cwf_Work_Grp_Tbl_Actve_Ind_2_2;
    private DbsField cwf_Work_Grp_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Work_Grp_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Work_Grp_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Work_Grp_Tbl_Updte_Dte;
    private DbsField cwf_Work_Grp_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Work_Grp_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Work_Grp_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Work_Grp_Tbl_Table_Rectype;
    private DbsField cwf_Work_Grp_Tbl_Table_Name_Key;
    private DbsField cwf_Work_Grp_Tbl_Prime_Key;
    private DbsField cwf_Work_Grp_Tbl_Table_Access_Level;

    public DataAccessProgramView getVw_cwf_Work_Grp() { return vw_cwf_Work_Grp; }

    public DbsField getCwf_Work_Grp_Tbl_Scrty_Level_Ind() { return cwf_Work_Grp_Tbl_Scrty_Level_Ind; }

    public DbsField getCwf_Work_Grp_Tbl_Table_Nme() { return cwf_Work_Grp_Tbl_Table_Nme; }

    public DbsField getCwf_Work_Grp_Tbl_Key_Field() { return cwf_Work_Grp_Tbl_Key_Field; }

    public DbsGroup getCwf_Work_Grp_Tbl_Key_FieldRedef1() { return cwf_Work_Grp_Tbl_Key_FieldRedef1; }

    public DbsField getCwf_Work_Grp_Wk_Code() { return cwf_Work_Grp_Wk_Code; }

    public DbsField getCwf_Work_Grp_Key_Fill() { return cwf_Work_Grp_Key_Fill; }

    public DbsField getCwf_Work_Grp_Tbl_Data_Field() { return cwf_Work_Grp_Tbl_Data_Field; }

    public DbsGroup getCwf_Work_Grp_Tbl_Data_FieldRedef2() { return cwf_Work_Grp_Tbl_Data_FieldRedef2; }

    public DbsField getCwf_Work_Grp_Wk_Desc() { return cwf_Work_Grp_Wk_Desc; }

    public DbsField getCwf_Work_Grp_Resp_Division() { return cwf_Work_Grp_Resp_Division; }

    public DbsField getCwf_Work_Grp_Data_Fill() { return cwf_Work_Grp_Data_Fill; }

    public DbsGroup getCwf_Work_Grp_Data_FillRedef3() { return cwf_Work_Grp_Data_FillRedef3; }

    public DbsField getCwf_Work_Grp_Work_Prcss_Id() { return cwf_Work_Grp_Work_Prcss_Id; }

    public DbsField getCwf_Work_Grp_Fill_It() { return cwf_Work_Grp_Fill_It; }

    public DbsField getCwf_Work_Grp_Tbl_Actve_Ind() { return cwf_Work_Grp_Tbl_Actve_Ind; }

    public DbsGroup getCwf_Work_Grp_Tbl_Actve_IndRedef4() { return cwf_Work_Grp_Tbl_Actve_IndRedef4; }

    public DbsField getCwf_Work_Grp_Fill_Acb770() { return cwf_Work_Grp_Fill_Acb770; }

    public DbsField getCwf_Work_Grp_Tbl_Actve_Ind_2_2() { return cwf_Work_Grp_Tbl_Actve_Ind_2_2; }

    public DbsField getCwf_Work_Grp_Tbl_Entry_Dte_Tme() { return cwf_Work_Grp_Tbl_Entry_Dte_Tme; }

    public DbsField getCwf_Work_Grp_Tbl_Entry_Oprtr_Cde() { return cwf_Work_Grp_Tbl_Entry_Oprtr_Cde; }

    public DbsField getCwf_Work_Grp_Tbl_Updte_Dte_Tme() { return cwf_Work_Grp_Tbl_Updte_Dte_Tme; }

    public DbsField getCwf_Work_Grp_Tbl_Updte_Dte() { return cwf_Work_Grp_Tbl_Updte_Dte; }

    public DbsField getCwf_Work_Grp_Tbl_Updte_Oprtr_Cde() { return cwf_Work_Grp_Tbl_Updte_Oprtr_Cde; }

    public DbsField getCwf_Work_Grp_Tbl_Dlte_Dte_Tme() { return cwf_Work_Grp_Tbl_Dlte_Dte_Tme; }

    public DbsField getCwf_Work_Grp_Tbl_Dlte_Oprtr_Cde() { return cwf_Work_Grp_Tbl_Dlte_Oprtr_Cde; }

    public DbsField getCwf_Work_Grp_Tbl_Table_Rectype() { return cwf_Work_Grp_Tbl_Table_Rectype; }

    public DbsField getCwf_Work_Grp_Tbl_Table_Name_Key() { return cwf_Work_Grp_Tbl_Table_Name_Key; }

    public DbsField getCwf_Work_Grp_Tbl_Prime_Key() { return cwf_Work_Grp_Tbl_Prime_Key; }

    public DbsField getCwf_Work_Grp_Tbl_Table_Access_Level() { return cwf_Work_Grp_Tbl_Table_Access_Level; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cwf_Work_Grp = new DataAccessProgramView(new NameInfo("vw_cwf_Work_Grp", "CWF-WORK-GRP"), "CWF_WORK_GRP_TBL", "CWF_DCMNT_TABLE");
        cwf_Work_Grp_Tbl_Scrty_Level_Ind = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Work_Grp_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Work_Grp_Tbl_Table_Nme = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, 
            RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Work_Grp_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Work_Grp_Tbl_Key_Field = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Work_Grp_Tbl_Key_FieldRedef1 = vw_cwf_Work_Grp.getRecord().newGroupInGroup("cwf_Work_Grp_Tbl_Key_FieldRedef1", "Redefines", cwf_Work_Grp_Tbl_Key_Field);
        cwf_Work_Grp_Wk_Code = cwf_Work_Grp_Tbl_Key_FieldRedef1.newFieldInGroup("cwf_Work_Grp_Wk_Code", "WK-CODE", FieldType.STRING, 6);
        cwf_Work_Grp_Key_Fill = cwf_Work_Grp_Tbl_Key_FieldRedef1.newFieldInGroup("cwf_Work_Grp_Key_Fill", "KEY-FILL", FieldType.STRING, 24);
        cwf_Work_Grp_Tbl_Data_Field = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, 
            RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        cwf_Work_Grp_Tbl_Data_FieldRedef2 = vw_cwf_Work_Grp.getRecord().newGroupInGroup("cwf_Work_Grp_Tbl_Data_FieldRedef2", "Redefines", cwf_Work_Grp_Tbl_Data_Field);
        cwf_Work_Grp_Wk_Desc = cwf_Work_Grp_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Work_Grp_Wk_Desc", "WK-DESC", FieldType.STRING, 30);
        cwf_Work_Grp_Resp_Division = cwf_Work_Grp_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Work_Grp_Resp_Division", "RESP-DIVISION", FieldType.STRING, 
            6);
        cwf_Work_Grp_Data_Fill = cwf_Work_Grp_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Work_Grp_Data_Fill", "DATA-FILL", FieldType.STRING, 217);
        cwf_Work_Grp_Data_FillRedef3 = vw_cwf_Work_Grp.getRecord().newGroupInGroup("cwf_Work_Grp_Data_FillRedef3", "Redefines", cwf_Work_Grp_Data_Fill);
        cwf_Work_Grp_Work_Prcss_Id = cwf_Work_Grp_Data_FillRedef3.newFieldArrayInGroup("cwf_Work_Grp_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, new DbsArrayController(1,36));
        cwf_Work_Grp_Fill_It = cwf_Work_Grp_Data_FillRedef3.newFieldInGroup("cwf_Work_Grp_Fill_It", "FILL-IT", FieldType.STRING, 1);
        cwf_Work_Grp_Tbl_Actve_Ind = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TBL_ACTVE_IND");
        cwf_Work_Grp_Tbl_Actve_IndRedef4 = vw_cwf_Work_Grp.getRecord().newGroupInGroup("cwf_Work_Grp_Tbl_Actve_IndRedef4", "Redefines", cwf_Work_Grp_Tbl_Actve_Ind);
        cwf_Work_Grp_Fill_Acb770 = cwf_Work_Grp_Tbl_Actve_IndRedef4.newFieldInGroup("cwf_Work_Grp_Fill_Acb770", "FILL-ACB770", FieldType.STRING, 1);
        cwf_Work_Grp_Tbl_Actve_Ind_2_2 = cwf_Work_Grp_Tbl_Actve_IndRedef4.newFieldInGroup("cwf_Work_Grp_Tbl_Actve_Ind_2_2", "TBL-ACTVE-IND-2-2", FieldType.STRING, 
            1);
        cwf_Work_Grp_Tbl_Entry_Dte_Tme = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Work_Grp_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Work_Grp_Tbl_Entry_Oprtr_Cde = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Work_Grp_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Work_Grp_Tbl_Updte_Dte_Tme = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Work_Grp_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Work_Grp_Tbl_Updte_Dte = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Work_Grp_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Work_Grp_Tbl_Updte_Oprtr_Cde = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Work_Grp_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Work_Grp_Tbl_Dlte_Dte_Tme = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Work_Grp_Tbl_Dlte_Oprtr_Cde = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Work_Grp_Tbl_Table_Rectype = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Work_Grp_Tbl_Table_Name_Key = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Table_Name_Key", "TBL-TABLE-NAME-KEY", FieldType.STRING, 
            22, RepeatingFieldStrategy.None, "TBL_TABLE_NAME_KEY");
        cwf_Work_Grp_Tbl_Table_Name_Key.setDdmHeader("TABLE NAME");
        cwf_Work_Grp_Tbl_Prime_Key = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Prime_Key", "TBL-PRIME-KEY", FieldType.STRING, 53, 
            RepeatingFieldStrategy.None, "TBL_PRIME_KEY");
        cwf_Work_Grp_Tbl_Prime_Key.setDdmHeader("RECORD KEY");
        cwf_Work_Grp_Tbl_Table_Access_Level = vw_cwf_Work_Grp.getRecord().newFieldInGroup("cwf_Work_Grp_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Work_Grp_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");

        this.setRecordName("LdaCwfl4034");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cwf_Work_Grp.reset();
    }

    // Constructor
    public LdaCwfl4034() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
