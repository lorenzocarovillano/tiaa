/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:47 PM
**        * FROM NATURAL LDA     : CWFL4823
************************************************************
**        * FILE NAME            : LdaCwfl4823.java
**        * CLASS NAME           : LdaCwfl4823
**        * INSTANCE NAME        : LdaCwfl4823
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4823 extends DbsRecord
{
    // Properties
    private DbsGroup wrdel;
    private DbsField wrdel_Rqst_Log_Dte_Tme;

    public DbsGroup getWrdel() { return wrdel; }

    public DbsField getWrdel_Rqst_Log_Dte_Tme() { return wrdel_Rqst_Log_Dte_Tme; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        wrdel = newGroupInRecord("wrdel", "WRDEL");
        wrdel_Rqst_Log_Dte_Tme = wrdel.newFieldInGroup("wrdel_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 28);

        this.setRecordName("LdaCwfl4823");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4823() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
