/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:56 PM
**        * FROM NATURAL LDA     : CWFL4832
************************************************************
**        * FILE NAME            : LdaCwfl4832.java
**        * CLASS NAME           : LdaCwfl4832
**        * INSTANCE NAME        : LdaCwfl4832
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4832 extends DbsRecord
{
    // Properties
    private DbsGroup work_Lead;
    private DbsField work_Lead_Pin_Npin;
    private DbsField work_Lead_A1;
    private DbsField work_Lead_Rqst_Log_Dte_Tme_A_15;
    private DbsField work_Lead_A2;
    private DbsField work_Lead_Last_Chnge_Dte_Tme_A;
    private DbsField work_Lead_A3;
    private DbsField work_Lead_Work_Prcss_Id;
    private DbsField work_Lead_A4;
    private DbsField work_Lead_Tiaa_Rcvd_Dte_Tme_A;
    private DbsField work_Lead_A5;
    private DbsField work_Lead_Rqst_Log_Oprtr_Cde;
    private DbsField work_Lead_A6;
    private DbsField work_Lead_Orgnl_Unit_Cde;
    private DbsField work_Lead_A7;
    private DbsField work_Lead_Admin_Unit_Cde;
    private DbsField work_Lead_A8;
    private DbsField work_Lead_Admin_Status_Cde;
    private DbsField work_Lead_A9;
    private DbsField work_Lead_Document_Type;
    private DbsField work_Lead_A10;
    private DbsField work_Lead_Empl_Oprtr_Cde;
    private DbsField work_Lead_A11;
    private DbsField work_Lead_Crprte_Due_Dte_Tme_A;
    private DbsField work_Lead_A12;
    private DbsField work_Lead_Crprte_Status_Ind;
    private DbsField work_Lead_A13;
    private DbsField work_Lead_Part_Close_Status_A;
    private DbsField work_Lead_A14;
    private DbsField work_Lead_Crprte_Clock_End_Dte_Tme_A;
    private DbsField work_Lead_A15;
    private DbsField work_Lead_Moc;
    private DbsField work_Lead_A16;
    private DbsField work_Lead_Part_Age;
    private DbsField work_Lead_A17;
    private DbsField work_Lead_Last_Chnge_Oprtr_Cde;
    private DbsField work_Lead_A18;
    private DbsField work_Lead_Crprte_Rqst_Log_Dte_Tme_A;
    private DbsField work_Lead_A19;
    private DbsField work_Lead_Actve_Ind;
    private DbsField work_Lead_A20;
    private DbsField work_Lead_Status_Cde;
    private DbsField work_Lead_A21;
    private DbsField work_Lead_Admin_Status_Updte_Dte_Tme_A;

    public DbsGroup getWork_Lead() { return work_Lead; }

    public DbsField getWork_Lead_Pin_Npin() { return work_Lead_Pin_Npin; }

    public DbsField getWork_Lead_A1() { return work_Lead_A1; }

    public DbsField getWork_Lead_Rqst_Log_Dte_Tme_A_15() { return work_Lead_Rqst_Log_Dte_Tme_A_15; }

    public DbsField getWork_Lead_A2() { return work_Lead_A2; }

    public DbsField getWork_Lead_Last_Chnge_Dte_Tme_A() { return work_Lead_Last_Chnge_Dte_Tme_A; }

    public DbsField getWork_Lead_A3() { return work_Lead_A3; }

    public DbsField getWork_Lead_Work_Prcss_Id() { return work_Lead_Work_Prcss_Id; }

    public DbsField getWork_Lead_A4() { return work_Lead_A4; }

    public DbsField getWork_Lead_Tiaa_Rcvd_Dte_Tme_A() { return work_Lead_Tiaa_Rcvd_Dte_Tme_A; }

    public DbsField getWork_Lead_A5() { return work_Lead_A5; }

    public DbsField getWork_Lead_Rqst_Log_Oprtr_Cde() { return work_Lead_Rqst_Log_Oprtr_Cde; }

    public DbsField getWork_Lead_A6() { return work_Lead_A6; }

    public DbsField getWork_Lead_Orgnl_Unit_Cde() { return work_Lead_Orgnl_Unit_Cde; }

    public DbsField getWork_Lead_A7() { return work_Lead_A7; }

    public DbsField getWork_Lead_Admin_Unit_Cde() { return work_Lead_Admin_Unit_Cde; }

    public DbsField getWork_Lead_A8() { return work_Lead_A8; }

    public DbsField getWork_Lead_Admin_Status_Cde() { return work_Lead_Admin_Status_Cde; }

    public DbsField getWork_Lead_A9() { return work_Lead_A9; }

    public DbsField getWork_Lead_Document_Type() { return work_Lead_Document_Type; }

    public DbsField getWork_Lead_A10() { return work_Lead_A10; }

    public DbsField getWork_Lead_Empl_Oprtr_Cde() { return work_Lead_Empl_Oprtr_Cde; }

    public DbsField getWork_Lead_A11() { return work_Lead_A11; }

    public DbsField getWork_Lead_Crprte_Due_Dte_Tme_A() { return work_Lead_Crprte_Due_Dte_Tme_A; }

    public DbsField getWork_Lead_A12() { return work_Lead_A12; }

    public DbsField getWork_Lead_Crprte_Status_Ind() { return work_Lead_Crprte_Status_Ind; }

    public DbsField getWork_Lead_A13() { return work_Lead_A13; }

    public DbsField getWork_Lead_Part_Close_Status_A() { return work_Lead_Part_Close_Status_A; }

    public DbsField getWork_Lead_A14() { return work_Lead_A14; }

    public DbsField getWork_Lead_Crprte_Clock_End_Dte_Tme_A() { return work_Lead_Crprte_Clock_End_Dte_Tme_A; }

    public DbsField getWork_Lead_A15() { return work_Lead_A15; }

    public DbsField getWork_Lead_Moc() { return work_Lead_Moc; }

    public DbsField getWork_Lead_A16() { return work_Lead_A16; }

    public DbsField getWork_Lead_Part_Age() { return work_Lead_Part_Age; }

    public DbsField getWork_Lead_A17() { return work_Lead_A17; }

    public DbsField getWork_Lead_Last_Chnge_Oprtr_Cde() { return work_Lead_Last_Chnge_Oprtr_Cde; }

    public DbsField getWork_Lead_A18() { return work_Lead_A18; }

    public DbsField getWork_Lead_Crprte_Rqst_Log_Dte_Tme_A() { return work_Lead_Crprte_Rqst_Log_Dte_Tme_A; }

    public DbsField getWork_Lead_A19() { return work_Lead_A19; }

    public DbsField getWork_Lead_Actve_Ind() { return work_Lead_Actve_Ind; }

    public DbsField getWork_Lead_A20() { return work_Lead_A20; }

    public DbsField getWork_Lead_Status_Cde() { return work_Lead_Status_Cde; }

    public DbsField getWork_Lead_A21() { return work_Lead_A21; }

    public DbsField getWork_Lead_Admin_Status_Updte_Dte_Tme_A() { return work_Lead_Admin_Status_Updte_Dte_Tme_A; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        work_Lead = newGroupInRecord("work_Lead", "WORK-LEAD");
        work_Lead_Pin_Npin = work_Lead.newFieldInGroup("work_Lead_Pin_Npin", "PIN-NPIN", FieldType.STRING, 12);
        work_Lead_A1 = work_Lead.newFieldInGroup("work_Lead_A1", "A1", FieldType.STRING, 1);
        work_Lead_Rqst_Log_Dte_Tme_A_15 = work_Lead.newFieldInGroup("work_Lead_Rqst_Log_Dte_Tme_A_15", "RQST-LOG-DTE-TME-A-15", FieldType.STRING, 15);
        work_Lead_A2 = work_Lead.newFieldInGroup("work_Lead_A2", "A2", FieldType.STRING, 1);
        work_Lead_Last_Chnge_Dte_Tme_A = work_Lead.newFieldInGroup("work_Lead_Last_Chnge_Dte_Tme_A", "LAST-CHNGE-DTE-TME-A", FieldType.STRING, 15);
        work_Lead_A3 = work_Lead.newFieldInGroup("work_Lead_A3", "A3", FieldType.STRING, 1);
        work_Lead_Work_Prcss_Id = work_Lead.newFieldInGroup("work_Lead_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        work_Lead_A4 = work_Lead.newFieldInGroup("work_Lead_A4", "A4", FieldType.STRING, 1);
        work_Lead_Tiaa_Rcvd_Dte_Tme_A = work_Lead.newFieldInGroup("work_Lead_Tiaa_Rcvd_Dte_Tme_A", "TIAA-RCVD-DTE-TME-A", FieldType.STRING, 28);
        work_Lead_A5 = work_Lead.newFieldInGroup("work_Lead_A5", "A5", FieldType.STRING, 1);
        work_Lead_Rqst_Log_Oprtr_Cde = work_Lead.newFieldInGroup("work_Lead_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 8);
        work_Lead_A6 = work_Lead.newFieldInGroup("work_Lead_A6", "A6", FieldType.STRING, 1);
        work_Lead_Orgnl_Unit_Cde = work_Lead.newFieldInGroup("work_Lead_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        work_Lead_A7 = work_Lead.newFieldInGroup("work_Lead_A7", "A7", FieldType.STRING, 1);
        work_Lead_Admin_Unit_Cde = work_Lead.newFieldInGroup("work_Lead_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        work_Lead_A8 = work_Lead.newFieldInGroup("work_Lead_A8", "A8", FieldType.STRING, 1);
        work_Lead_Admin_Status_Cde = work_Lead.newFieldInGroup("work_Lead_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4);
        work_Lead_A9 = work_Lead.newFieldInGroup("work_Lead_A9", "A9", FieldType.STRING, 1);
        work_Lead_Document_Type = work_Lead.newFieldInGroup("work_Lead_Document_Type", "DOCUMENT-TYPE", FieldType.STRING, 8);
        work_Lead_A10 = work_Lead.newFieldInGroup("work_Lead_A10", "A10", FieldType.STRING, 1);
        work_Lead_Empl_Oprtr_Cde = work_Lead.newFieldInGroup("work_Lead_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 8);
        work_Lead_A11 = work_Lead.newFieldInGroup("work_Lead_A11", "A11", FieldType.STRING, 1);
        work_Lead_Crprte_Due_Dte_Tme_A = work_Lead.newFieldInGroup("work_Lead_Crprte_Due_Dte_Tme_A", "CRPRTE-DUE-DTE-TME-A", FieldType.STRING, 28);
        work_Lead_A12 = work_Lead.newFieldInGroup("work_Lead_A12", "A12", FieldType.STRING, 1);
        work_Lead_Crprte_Status_Ind = work_Lead.newFieldInGroup("work_Lead_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1);
        work_Lead_A13 = work_Lead.newFieldInGroup("work_Lead_A13", "A13", FieldType.STRING, 1);
        work_Lead_Part_Close_Status_A = work_Lead.newFieldInGroup("work_Lead_Part_Close_Status_A", "PART-CLOSE-STATUS-A", FieldType.STRING, 4);
        work_Lead_A14 = work_Lead.newFieldInGroup("work_Lead_A14", "A14", FieldType.STRING, 1);
        work_Lead_Crprte_Clock_End_Dte_Tme_A = work_Lead.newFieldInGroup("work_Lead_Crprte_Clock_End_Dte_Tme_A", "CRPRTE-CLOCK-END-DTE-TME-A", FieldType.STRING, 
            28);
        work_Lead_A15 = work_Lead.newFieldInGroup("work_Lead_A15", "A15", FieldType.STRING, 1);
        work_Lead_Moc = work_Lead.newFieldInGroup("work_Lead_Moc", "MOC", FieldType.STRING, 1);
        work_Lead_A16 = work_Lead.newFieldInGroup("work_Lead_A16", "A16", FieldType.STRING, 1);
        work_Lead_Part_Age = work_Lead.newFieldInGroup("work_Lead_Part_Age", "PART-AGE", FieldType.NUMERIC, 3);
        work_Lead_A17 = work_Lead.newFieldInGroup("work_Lead_A17", "A17", FieldType.STRING, 1);
        work_Lead_Last_Chnge_Oprtr_Cde = work_Lead.newFieldInGroup("work_Lead_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8);
        work_Lead_A18 = work_Lead.newFieldInGroup("work_Lead_A18", "A18", FieldType.STRING, 1);
        work_Lead_Crprte_Rqst_Log_Dte_Tme_A = work_Lead.newFieldInGroup("work_Lead_Crprte_Rqst_Log_Dte_Tme_A", "CRPRTE-RQST-LOG-DTE-TME-A", FieldType.STRING, 
            28);
        work_Lead_A19 = work_Lead.newFieldInGroup("work_Lead_A19", "A19", FieldType.STRING, 1);
        work_Lead_Actve_Ind = work_Lead.newFieldInGroup("work_Lead_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2);
        work_Lead_A20 = work_Lead.newFieldInGroup("work_Lead_A20", "A20", FieldType.STRING, 1);
        work_Lead_Status_Cde = work_Lead.newFieldInGroup("work_Lead_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        work_Lead_A21 = work_Lead.newFieldInGroup("work_Lead_A21", "A21", FieldType.STRING, 1);
        work_Lead_Admin_Status_Updte_Dte_Tme_A = work_Lead.newFieldInGroup("work_Lead_Admin_Status_Updte_Dte_Tme_A", "ADMIN-STATUS-UPDTE-DTE-TME-A", FieldType.STRING, 
            15);

        this.setRecordName("LdaCwfl4832");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwfl4832() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
