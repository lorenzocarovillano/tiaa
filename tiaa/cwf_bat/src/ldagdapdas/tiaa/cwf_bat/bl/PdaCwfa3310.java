/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:57 PM
**        * FROM NATURAL PDA     : CWFA3310
************************************************************
**        * FILE NAME            : PdaCwfa3310.java
**        * CLASS NAME           : PdaCwfa3310
**        * INSTANCE NAME        : PdaCwfa3310
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCwfa3310 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Rqst_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Last_Chnge_Invrt_Dte_Tme;
    private DbsField pnd_Work_Record_Pnd_Wpid_Actn;
    private DbsField pnd_Work_Record_Rqst_Work_Prcss_Id;
    private DbsGroup pnd_Work_Record_Rqst_Work_Prcss_IdRedef1;
    private DbsField pnd_Work_Record_Rqst_Wpid_Actn;
    private DbsField pnd_Work_Record_Rqst_Tiaa_Rcvd_Dte;
    private DbsField pnd_Work_Record_Return_Doc_Rec_Dte_Tme;
    private DbsField pnd_Work_Record_Return_Rcvd_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Business_Days;
    private DbsField pnd_Work_Record_Step_Id;
    private DbsField pnd_Work_Record_Tbl_Status_Key;
    private DbsGroup pnd_Work_Record_Tbl_Status_KeyRedef2;
    private DbsField pnd_Work_Record_Last_Chnge_Unit_Cde;
    private DbsField pnd_Work_Record_Admin_Status_Cde;
    private DbsField pnd_Work_Record_Admin_Status_Updte_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Calendar_Days;
    private DbsField pnd_Work_Record_Rqst_Pin_Nbr;
    private DbsField pnd_Work_Record_Cntrct_Nbr;
    private DbsField pnd_Work_Record_Admin_Status_Updte_Oprtr_Cde;
    private DbsGroup pnd_Rep_Data;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit;
    private DbsGroup pnd_Rep_Data_Pnd_Rep_UnitRedef3;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit_1;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit_2;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit_3;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit_4;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit_5;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit_6;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit_7;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit_8;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit_9;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit_10;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit_11;
    private DbsGroup pnd_Rep_Data_Pnd_Rep_UnitRedef4;
    private DbsField pnd_Rep_Data_Pnd_Rep_Unit_Cde;
    private DbsField pnd_Rep_Data_Pnd_Rep_Start_Dd;
    private DbsGroup pnd_Rep_Data_Pnd_Rep_Start_DdRedef5;
    private DbsField pnd_Rep_Data_Pnd_Rep_Start_Dd_A;
    private DbsField pnd_Rep_Data_Pnd_Rep_Start_Mm;
    private DbsGroup pnd_Rep_Data_Pnd_Rep_Start_MmRedef6;
    private DbsField pnd_Rep_Data_Pnd_Rep_Start_Mm_A;
    private DbsField pnd_Rep_Data_Pnd_Rep_Start_Yy;
    private DbsGroup pnd_Rep_Data_Pnd_Rep_Start_YyRedef7;
    private DbsField pnd_Rep_Data_Pnd_Rep_Start_Yy_A;
    private DbsField pnd_Rep_Data_Pnd_Rep_End_Dd;
    private DbsGroup pnd_Rep_Data_Pnd_Rep_End_DdRedef8;
    private DbsField pnd_Rep_Data_Pnd_Rep_End_Dd_A;
    private DbsField pnd_Rep_Data_Pnd_Rep_End_Mm;
    private DbsGroup pnd_Rep_Data_Pnd_Rep_End_MmRedef9;
    private DbsField pnd_Rep_Data_Pnd_Rep_End_Mm_A;
    private DbsField pnd_Rep_Data_Pnd_Rep_End_Yy;
    private DbsGroup pnd_Rep_Data_Pnd_Rep_End_YyRedef10;
    private DbsField pnd_Rep_Data_Pnd_Rep_End_Yy_A;
    private DbsField pnd_Rep_Data_Pnd_Rep_Wpid;
    private DbsGroup pnd_Rep_Data_Pnd_Rep_WpidRedef11;
    private DbsField pnd_Rep_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde;
    private DbsField pnd_Rep_Data_Pnd_Rep_Wpid_Lob;
    private DbsField pnd_Rep_Data_Pnd_Rep_Wpid_Mbp;
    private DbsField pnd_Rep_Data_Pnd_Rep_Wpid_Sbp;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status;
    private DbsGroup pnd_Rep_Data_Pnd_Rep_StatusRedef12;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_1;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_2;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_3;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_4;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_5;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_6;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_7;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_8;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_9;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_10;
    private DbsGroup pnd_Rep_Data_Pnd_Rep_StatusRedef13;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_Cde;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_From_1;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_To_1;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_From_2;
    private DbsField pnd_Rep_Data_Pnd_Rep_Status_To_2;
    private DbsField pnd_Rep_Data_Pnd_Rep_Racf_Id;
    private DbsField pnd_Rep_Data_Pnd_Rep_Wpid_Name;
    private DbsField pnd_Rep_Data_Pnd_Rep_Empl_Name;
    private DbsField pnd_Rep_Data_Pnd_Rep_Empl_Name_Detail_Line;
    private DbsField pnd_Rep_Data_Pnd_Rep_Break_Ind;
    private DbsGroup pnd_Misc_Parm;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Cde;
    private DbsGroup pnd_Misc_Parm_Pnd_Wrk_Unit_CdeRedef14;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix;
    private DbsField pnd_Misc_Parm_Pnd_Save_Action;
    private DbsField pnd_Misc_Parm_Pnd_Save_Wpid;
    private DbsField pnd_Misc_Parm_Pnd_Wpid;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Action;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Code;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Count;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Desc;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Text;
    private DbsField pnd_Misc_Parm_Pnd_Sub_Count;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count_1;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count_2;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count_3;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count_4;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count_5;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count_Over;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count;
    private DbsField pnd_Misc_Parm_Pnd_Ptotal_Count_1;
    private DbsField pnd_Misc_Parm_Pnd_Ptotal_Count_2;
    private DbsField pnd_Misc_Parm_Pnd_Ptotal_Count_3;
    private DbsField pnd_Misc_Parm_Pnd_Ptotal_Count_4;
    private DbsField pnd_Misc_Parm_Pnd_Ptotal_Count_5;
    private DbsField pnd_Misc_Parm_Pnd_Ptotal_Count_Over;
    private DbsField pnd_Misc_Parm_Pnd_Rep_Ctr;
    private DbsGroup pnd_Misc_Parm_Pnd_Array_Ctrs;
    private DbsField pnd_Misc_Parm_Pnd_Booklet_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Forms_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Inquire_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Research_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Trans_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Complaint_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Other_Ctr;
    private DbsGroup pnd_Misc_Parm_Pnd_Percent_Ctrs;
    private DbsField pnd_Misc_Parm_Pnd_Pbooklet_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Pforms_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Pinquire_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Presearch_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Ptrans_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Pcomplaint_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Pother_Ctr;
    private DbsGroup pnd_Misc_Parm_Pnd_Total_Ctrs;
    private DbsField pnd_Misc_Parm_Pnd_Tbooklet_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Tforms_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Tinquire_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Tresearch_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Ttrans_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Tcomplaint_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Tother_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Read_Count;
    private DbsField pnd_Misc_Parm_Pnd_Actve_Unque_Key;
    private DbsField pnd_Misc_Parm_Pnd_Todays_Time;
    private DbsField pnd_Misc_Parm_Pnd_Return_Code;
    private DbsField pnd_Misc_Parm_Pnd_Return_Msg;
    private DbsField pnd_Misc_Parm_Pnd_Status_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Partic_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Return_Doc_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Return_Rcvd_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Start_Date;
    private DbsGroup pnd_Misc_Parm_Pnd_Start_DateRedef15;
    private DbsField pnd_Misc_Parm_Pnd_Start_Date_N;
    private DbsField pnd_Misc_Parm_Pnd_End_Date;
    private DbsGroup pnd_Misc_Parm_Pnd_End_DateRedef16;
    private DbsField pnd_Misc_Parm_Pnd_End_Date_N;
    private DbsField pnd_Misc_Parm_Pnd_Work_Start_Date_A;
    private DbsGroup pnd_Misc_Parm_Pnd_Work_Start_Date_ARedef17;
    private DbsField pnd_Misc_Parm_Pnd_Work_Mm;
    private DbsField pnd_Misc_Parm_Pnd_Filler1;
    private DbsField pnd_Misc_Parm_Pnd_Work_Dd;
    private DbsField pnd_Misc_Parm_Pnd_Filler2;
    private DbsField pnd_Misc_Parm_Pnd_Work_Yy;
    private DbsField pnd_Misc_Parm_Pnd_Work_End_Date_A;
    private DbsGroup pnd_Misc_Parm_Pnd_Work_End_Date_ARedef18;
    private DbsField pnd_Misc_Parm_Pnd_Work_Mmm;
    private DbsField pnd_Misc_Parm_Pnd_Fillera;
    private DbsField pnd_Misc_Parm_Pnd_Work_Ddd;
    private DbsField pnd_Misc_Parm_Pnd_Fillerb;
    private DbsField pnd_Misc_Parm_Pnd_Work_Yyy;
    private DbsField pnd_Misc_Parm_Pnd_Work_Date_D;
    private DbsField pnd_Misc_Parm_Pnd_Confirmed;
    private DbsField pnd_Misc_Parm_Pnd_Status_Match;
    private DbsField pnd_Misc_Parm_Pnd_I;

    public DbsGroup getPnd_Work_Record() { return pnd_Work_Record; }

    public DbsField getPnd_Work_Record_Rqst_Log_Dte_Tme() { return pnd_Work_Record_Rqst_Log_Dte_Tme; }

    public DbsField getPnd_Work_Record_Last_Chnge_Invrt_Dte_Tme() { return pnd_Work_Record_Last_Chnge_Invrt_Dte_Tme; }

    public DbsField getPnd_Work_Record_Pnd_Wpid_Actn() { return pnd_Work_Record_Pnd_Wpid_Actn; }

    public DbsField getPnd_Work_Record_Rqst_Work_Prcss_Id() { return pnd_Work_Record_Rqst_Work_Prcss_Id; }

    public DbsGroup getPnd_Work_Record_Rqst_Work_Prcss_IdRedef1() { return pnd_Work_Record_Rqst_Work_Prcss_IdRedef1; }

    public DbsField getPnd_Work_Record_Rqst_Wpid_Actn() { return pnd_Work_Record_Rqst_Wpid_Actn; }

    public DbsField getPnd_Work_Record_Rqst_Tiaa_Rcvd_Dte() { return pnd_Work_Record_Rqst_Tiaa_Rcvd_Dte; }

    public DbsField getPnd_Work_Record_Return_Doc_Rec_Dte_Tme() { return pnd_Work_Record_Return_Doc_Rec_Dte_Tme; }

    public DbsField getPnd_Work_Record_Return_Rcvd_Dte_Tme() { return pnd_Work_Record_Return_Rcvd_Dte_Tme; }

    public DbsField getPnd_Work_Record_Tbl_Business_Days() { return pnd_Work_Record_Tbl_Business_Days; }

    public DbsField getPnd_Work_Record_Step_Id() { return pnd_Work_Record_Step_Id; }

    public DbsField getPnd_Work_Record_Tbl_Status_Key() { return pnd_Work_Record_Tbl_Status_Key; }

    public DbsGroup getPnd_Work_Record_Tbl_Status_KeyRedef2() { return pnd_Work_Record_Tbl_Status_KeyRedef2; }

    public DbsField getPnd_Work_Record_Last_Chnge_Unit_Cde() { return pnd_Work_Record_Last_Chnge_Unit_Cde; }

    public DbsField getPnd_Work_Record_Admin_Status_Cde() { return pnd_Work_Record_Admin_Status_Cde; }

    public DbsField getPnd_Work_Record_Admin_Status_Updte_Dte_Tme() { return pnd_Work_Record_Admin_Status_Updte_Dte_Tme; }

    public DbsField getPnd_Work_Record_Tbl_Calendar_Days() { return pnd_Work_Record_Tbl_Calendar_Days; }

    public DbsField getPnd_Work_Record_Rqst_Pin_Nbr() { return pnd_Work_Record_Rqst_Pin_Nbr; }

    public DbsField getPnd_Work_Record_Cntrct_Nbr() { return pnd_Work_Record_Cntrct_Nbr; }

    public DbsField getPnd_Work_Record_Admin_Status_Updte_Oprtr_Cde() { return pnd_Work_Record_Admin_Status_Updte_Oprtr_Cde; }

    public DbsGroup getPnd_Rep_Data() { return pnd_Rep_Data; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit() { return pnd_Rep_Data_Pnd_Rep_Unit; }

    public DbsGroup getPnd_Rep_Data_Pnd_Rep_UnitRedef3() { return pnd_Rep_Data_Pnd_Rep_UnitRedef3; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit_1() { return pnd_Rep_Data_Pnd_Rep_Unit_1; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit_2() { return pnd_Rep_Data_Pnd_Rep_Unit_2; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit_3() { return pnd_Rep_Data_Pnd_Rep_Unit_3; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit_4() { return pnd_Rep_Data_Pnd_Rep_Unit_4; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit_5() { return pnd_Rep_Data_Pnd_Rep_Unit_5; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit_6() { return pnd_Rep_Data_Pnd_Rep_Unit_6; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit_7() { return pnd_Rep_Data_Pnd_Rep_Unit_7; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit_8() { return pnd_Rep_Data_Pnd_Rep_Unit_8; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit_9() { return pnd_Rep_Data_Pnd_Rep_Unit_9; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit_10() { return pnd_Rep_Data_Pnd_Rep_Unit_10; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit_11() { return pnd_Rep_Data_Pnd_Rep_Unit_11; }

    public DbsGroup getPnd_Rep_Data_Pnd_Rep_UnitRedef4() { return pnd_Rep_Data_Pnd_Rep_UnitRedef4; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Unit_Cde() { return pnd_Rep_Data_Pnd_Rep_Unit_Cde; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Start_Dd() { return pnd_Rep_Data_Pnd_Rep_Start_Dd; }

    public DbsGroup getPnd_Rep_Data_Pnd_Rep_Start_DdRedef5() { return pnd_Rep_Data_Pnd_Rep_Start_DdRedef5; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Start_Dd_A() { return pnd_Rep_Data_Pnd_Rep_Start_Dd_A; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Start_Mm() { return pnd_Rep_Data_Pnd_Rep_Start_Mm; }

    public DbsGroup getPnd_Rep_Data_Pnd_Rep_Start_MmRedef6() { return pnd_Rep_Data_Pnd_Rep_Start_MmRedef6; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Start_Mm_A() { return pnd_Rep_Data_Pnd_Rep_Start_Mm_A; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Start_Yy() { return pnd_Rep_Data_Pnd_Rep_Start_Yy; }

    public DbsGroup getPnd_Rep_Data_Pnd_Rep_Start_YyRedef7() { return pnd_Rep_Data_Pnd_Rep_Start_YyRedef7; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Start_Yy_A() { return pnd_Rep_Data_Pnd_Rep_Start_Yy_A; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_End_Dd() { return pnd_Rep_Data_Pnd_Rep_End_Dd; }

    public DbsGroup getPnd_Rep_Data_Pnd_Rep_End_DdRedef8() { return pnd_Rep_Data_Pnd_Rep_End_DdRedef8; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_End_Dd_A() { return pnd_Rep_Data_Pnd_Rep_End_Dd_A; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_End_Mm() { return pnd_Rep_Data_Pnd_Rep_End_Mm; }

    public DbsGroup getPnd_Rep_Data_Pnd_Rep_End_MmRedef9() { return pnd_Rep_Data_Pnd_Rep_End_MmRedef9; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_End_Mm_A() { return pnd_Rep_Data_Pnd_Rep_End_Mm_A; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_End_Yy() { return pnd_Rep_Data_Pnd_Rep_End_Yy; }

    public DbsGroup getPnd_Rep_Data_Pnd_Rep_End_YyRedef10() { return pnd_Rep_Data_Pnd_Rep_End_YyRedef10; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_End_Yy_A() { return pnd_Rep_Data_Pnd_Rep_End_Yy_A; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Wpid() { return pnd_Rep_Data_Pnd_Rep_Wpid; }

    public DbsGroup getPnd_Rep_Data_Pnd_Rep_WpidRedef11() { return pnd_Rep_Data_Pnd_Rep_WpidRedef11; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde() { return pnd_Rep_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Wpid_Lob() { return pnd_Rep_Data_Pnd_Rep_Wpid_Lob; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Wpid_Mbp() { return pnd_Rep_Data_Pnd_Rep_Wpid_Mbp; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Wpid_Sbp() { return pnd_Rep_Data_Pnd_Rep_Wpid_Sbp; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status() { return pnd_Rep_Data_Pnd_Rep_Status; }

    public DbsGroup getPnd_Rep_Data_Pnd_Rep_StatusRedef12() { return pnd_Rep_Data_Pnd_Rep_StatusRedef12; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_1() { return pnd_Rep_Data_Pnd_Rep_Status_1; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_2() { return pnd_Rep_Data_Pnd_Rep_Status_2; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_3() { return pnd_Rep_Data_Pnd_Rep_Status_3; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_4() { return pnd_Rep_Data_Pnd_Rep_Status_4; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_5() { return pnd_Rep_Data_Pnd_Rep_Status_5; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_6() { return pnd_Rep_Data_Pnd_Rep_Status_6; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_7() { return pnd_Rep_Data_Pnd_Rep_Status_7; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_8() { return pnd_Rep_Data_Pnd_Rep_Status_8; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_9() { return pnd_Rep_Data_Pnd_Rep_Status_9; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_10() { return pnd_Rep_Data_Pnd_Rep_Status_10; }

    public DbsGroup getPnd_Rep_Data_Pnd_Rep_StatusRedef13() { return pnd_Rep_Data_Pnd_Rep_StatusRedef13; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_Cde() { return pnd_Rep_Data_Pnd_Rep_Status_Cde; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_From_1() { return pnd_Rep_Data_Pnd_Rep_Status_From_1; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_To_1() { return pnd_Rep_Data_Pnd_Rep_Status_To_1; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_From_2() { return pnd_Rep_Data_Pnd_Rep_Status_From_2; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Status_To_2() { return pnd_Rep_Data_Pnd_Rep_Status_To_2; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Racf_Id() { return pnd_Rep_Data_Pnd_Rep_Racf_Id; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Wpid_Name() { return pnd_Rep_Data_Pnd_Rep_Wpid_Name; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Empl_Name() { return pnd_Rep_Data_Pnd_Rep_Empl_Name; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Empl_Name_Detail_Line() { return pnd_Rep_Data_Pnd_Rep_Empl_Name_Detail_Line; }

    public DbsField getPnd_Rep_Data_Pnd_Rep_Break_Ind() { return pnd_Rep_Data_Pnd_Rep_Break_Ind; }

    public DbsGroup getPnd_Misc_Parm() { return pnd_Misc_Parm; }

    public DbsField getPnd_Misc_Parm_Pnd_Wrk_Unit_Cde() { return pnd_Misc_Parm_Pnd_Wrk_Unit_Cde; }

    public DbsGroup getPnd_Misc_Parm_Pnd_Wrk_Unit_CdeRedef14() { return pnd_Misc_Parm_Pnd_Wrk_Unit_CdeRedef14; }

    public DbsField getPnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde() { return pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde; }

    public DbsField getPnd_Misc_Parm_Pnd_Wrk_Unit_Suffix() { return pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix; }

    public DbsField getPnd_Misc_Parm_Pnd_Save_Action() { return pnd_Misc_Parm_Pnd_Save_Action; }

    public DbsField getPnd_Misc_Parm_Pnd_Save_Wpid() { return pnd_Misc_Parm_Pnd_Save_Wpid; }

    public DbsField getPnd_Misc_Parm_Pnd_Wpid() { return pnd_Misc_Parm_Pnd_Wpid; }

    public DbsField getPnd_Misc_Parm_Pnd_Wpid_Action() { return pnd_Misc_Parm_Pnd_Wpid_Action; }

    public DbsField getPnd_Misc_Parm_Pnd_Wpid_Code() { return pnd_Misc_Parm_Pnd_Wpid_Code; }

    public DbsField getPnd_Misc_Parm_Pnd_Wpid_Count() { return pnd_Misc_Parm_Pnd_Wpid_Count; }

    public DbsField getPnd_Misc_Parm_Pnd_Wpid_Desc() { return pnd_Misc_Parm_Pnd_Wpid_Desc; }

    public DbsField getPnd_Misc_Parm_Pnd_Wpid_Sname() { return pnd_Misc_Parm_Pnd_Wpid_Sname; }

    public DbsField getPnd_Misc_Parm_Pnd_Wpid_Text() { return pnd_Misc_Parm_Pnd_Wpid_Text; }

    public DbsField getPnd_Misc_Parm_Pnd_Sub_Count() { return pnd_Misc_Parm_Pnd_Sub_Count; }

    public DbsField getPnd_Misc_Parm_Pnd_Total_Count_1() { return pnd_Misc_Parm_Pnd_Total_Count_1; }

    public DbsField getPnd_Misc_Parm_Pnd_Total_Count_2() { return pnd_Misc_Parm_Pnd_Total_Count_2; }

    public DbsField getPnd_Misc_Parm_Pnd_Total_Count_3() { return pnd_Misc_Parm_Pnd_Total_Count_3; }

    public DbsField getPnd_Misc_Parm_Pnd_Total_Count_4() { return pnd_Misc_Parm_Pnd_Total_Count_4; }

    public DbsField getPnd_Misc_Parm_Pnd_Total_Count_5() { return pnd_Misc_Parm_Pnd_Total_Count_5; }

    public DbsField getPnd_Misc_Parm_Pnd_Total_Count_Over() { return pnd_Misc_Parm_Pnd_Total_Count_Over; }

    public DbsField getPnd_Misc_Parm_Pnd_Total_Count() { return pnd_Misc_Parm_Pnd_Total_Count; }

    public DbsField getPnd_Misc_Parm_Pnd_Ptotal_Count_1() { return pnd_Misc_Parm_Pnd_Ptotal_Count_1; }

    public DbsField getPnd_Misc_Parm_Pnd_Ptotal_Count_2() { return pnd_Misc_Parm_Pnd_Ptotal_Count_2; }

    public DbsField getPnd_Misc_Parm_Pnd_Ptotal_Count_3() { return pnd_Misc_Parm_Pnd_Ptotal_Count_3; }

    public DbsField getPnd_Misc_Parm_Pnd_Ptotal_Count_4() { return pnd_Misc_Parm_Pnd_Ptotal_Count_4; }

    public DbsField getPnd_Misc_Parm_Pnd_Ptotal_Count_5() { return pnd_Misc_Parm_Pnd_Ptotal_Count_5; }

    public DbsField getPnd_Misc_Parm_Pnd_Ptotal_Count_Over() { return pnd_Misc_Parm_Pnd_Ptotal_Count_Over; }

    public DbsField getPnd_Misc_Parm_Pnd_Rep_Ctr() { return pnd_Misc_Parm_Pnd_Rep_Ctr; }

    public DbsGroup getPnd_Misc_Parm_Pnd_Array_Ctrs() { return pnd_Misc_Parm_Pnd_Array_Ctrs; }

    public DbsField getPnd_Misc_Parm_Pnd_Booklet_Ctr() { return pnd_Misc_Parm_Pnd_Booklet_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Forms_Ctr() { return pnd_Misc_Parm_Pnd_Forms_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Inquire_Ctr() { return pnd_Misc_Parm_Pnd_Inquire_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Research_Ctr() { return pnd_Misc_Parm_Pnd_Research_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Trans_Ctr() { return pnd_Misc_Parm_Pnd_Trans_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Complaint_Ctr() { return pnd_Misc_Parm_Pnd_Complaint_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Other_Ctr() { return pnd_Misc_Parm_Pnd_Other_Ctr; }

    public DbsGroup getPnd_Misc_Parm_Pnd_Percent_Ctrs() { return pnd_Misc_Parm_Pnd_Percent_Ctrs; }

    public DbsField getPnd_Misc_Parm_Pnd_Pbooklet_Ctr() { return pnd_Misc_Parm_Pnd_Pbooklet_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Pforms_Ctr() { return pnd_Misc_Parm_Pnd_Pforms_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Pinquire_Ctr() { return pnd_Misc_Parm_Pnd_Pinquire_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Presearch_Ctr() { return pnd_Misc_Parm_Pnd_Presearch_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Ptrans_Ctr() { return pnd_Misc_Parm_Pnd_Ptrans_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Pcomplaint_Ctr() { return pnd_Misc_Parm_Pnd_Pcomplaint_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Pother_Ctr() { return pnd_Misc_Parm_Pnd_Pother_Ctr; }

    public DbsGroup getPnd_Misc_Parm_Pnd_Total_Ctrs() { return pnd_Misc_Parm_Pnd_Total_Ctrs; }

    public DbsField getPnd_Misc_Parm_Pnd_Tbooklet_Ctr() { return pnd_Misc_Parm_Pnd_Tbooklet_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Tforms_Ctr() { return pnd_Misc_Parm_Pnd_Tforms_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Tinquire_Ctr() { return pnd_Misc_Parm_Pnd_Tinquire_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Tresearch_Ctr() { return pnd_Misc_Parm_Pnd_Tresearch_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Ttrans_Ctr() { return pnd_Misc_Parm_Pnd_Ttrans_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Tcomplaint_Ctr() { return pnd_Misc_Parm_Pnd_Tcomplaint_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Tother_Ctr() { return pnd_Misc_Parm_Pnd_Tother_Ctr; }

    public DbsField getPnd_Misc_Parm_Pnd_Read_Count() { return pnd_Misc_Parm_Pnd_Read_Count; }

    public DbsField getPnd_Misc_Parm_Pnd_Actve_Unque_Key() { return pnd_Misc_Parm_Pnd_Actve_Unque_Key; }

    public DbsField getPnd_Misc_Parm_Pnd_Todays_Time() { return pnd_Misc_Parm_Pnd_Todays_Time; }

    public DbsField getPnd_Misc_Parm_Pnd_Return_Code() { return pnd_Misc_Parm_Pnd_Return_Code; }

    public DbsField getPnd_Misc_Parm_Pnd_Return_Msg() { return pnd_Misc_Parm_Pnd_Return_Msg; }

    public DbsField getPnd_Misc_Parm_Pnd_Status_Sname() { return pnd_Misc_Parm_Pnd_Status_Sname; }

    public DbsField getPnd_Misc_Parm_Pnd_Partic_Sname() { return pnd_Misc_Parm_Pnd_Partic_Sname; }

    public DbsField getPnd_Misc_Parm_Pnd_Return_Doc_Txt() { return pnd_Misc_Parm_Pnd_Return_Doc_Txt; }

    public DbsField getPnd_Misc_Parm_Pnd_Return_Rcvd_Txt() { return pnd_Misc_Parm_Pnd_Return_Rcvd_Txt; }

    public DbsField getPnd_Misc_Parm_Pnd_Start_Date() { return pnd_Misc_Parm_Pnd_Start_Date; }

    public DbsGroup getPnd_Misc_Parm_Pnd_Start_DateRedef15() { return pnd_Misc_Parm_Pnd_Start_DateRedef15; }

    public DbsField getPnd_Misc_Parm_Pnd_Start_Date_N() { return pnd_Misc_Parm_Pnd_Start_Date_N; }

    public DbsField getPnd_Misc_Parm_Pnd_End_Date() { return pnd_Misc_Parm_Pnd_End_Date; }

    public DbsGroup getPnd_Misc_Parm_Pnd_End_DateRedef16() { return pnd_Misc_Parm_Pnd_End_DateRedef16; }

    public DbsField getPnd_Misc_Parm_Pnd_End_Date_N() { return pnd_Misc_Parm_Pnd_End_Date_N; }

    public DbsField getPnd_Misc_Parm_Pnd_Work_Start_Date_A() { return pnd_Misc_Parm_Pnd_Work_Start_Date_A; }

    public DbsGroup getPnd_Misc_Parm_Pnd_Work_Start_Date_ARedef17() { return pnd_Misc_Parm_Pnd_Work_Start_Date_ARedef17; }

    public DbsField getPnd_Misc_Parm_Pnd_Work_Mm() { return pnd_Misc_Parm_Pnd_Work_Mm; }

    public DbsField getPnd_Misc_Parm_Pnd_Filler1() { return pnd_Misc_Parm_Pnd_Filler1; }

    public DbsField getPnd_Misc_Parm_Pnd_Work_Dd() { return pnd_Misc_Parm_Pnd_Work_Dd; }

    public DbsField getPnd_Misc_Parm_Pnd_Filler2() { return pnd_Misc_Parm_Pnd_Filler2; }

    public DbsField getPnd_Misc_Parm_Pnd_Work_Yy() { return pnd_Misc_Parm_Pnd_Work_Yy; }

    public DbsField getPnd_Misc_Parm_Pnd_Work_End_Date_A() { return pnd_Misc_Parm_Pnd_Work_End_Date_A; }

    public DbsGroup getPnd_Misc_Parm_Pnd_Work_End_Date_ARedef18() { return pnd_Misc_Parm_Pnd_Work_End_Date_ARedef18; }

    public DbsField getPnd_Misc_Parm_Pnd_Work_Mmm() { return pnd_Misc_Parm_Pnd_Work_Mmm; }

    public DbsField getPnd_Misc_Parm_Pnd_Fillera() { return pnd_Misc_Parm_Pnd_Fillera; }

    public DbsField getPnd_Misc_Parm_Pnd_Work_Ddd() { return pnd_Misc_Parm_Pnd_Work_Ddd; }

    public DbsField getPnd_Misc_Parm_Pnd_Fillerb() { return pnd_Misc_Parm_Pnd_Fillerb; }

    public DbsField getPnd_Misc_Parm_Pnd_Work_Yyy() { return pnd_Misc_Parm_Pnd_Work_Yyy; }

    public DbsField getPnd_Misc_Parm_Pnd_Work_Date_D() { return pnd_Misc_Parm_Pnd_Work_Date_D; }

    public DbsField getPnd_Misc_Parm_Pnd_Confirmed() { return pnd_Misc_Parm_Pnd_Confirmed; }

    public DbsField getPnd_Misc_Parm_Pnd_Status_Match() { return pnd_Misc_Parm_Pnd_Status_Match; }

    public DbsField getPnd_Misc_Parm_Pnd_I() { return pnd_Misc_Parm_Pnd_I; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Work_Record = dbsRecord.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record.setParameterOption(ParameterOption.ByReference);
        pnd_Work_Record_Rqst_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_Record_Last_Chnge_Invrt_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Last_Chnge_Invrt_Dte_Tme", "LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15);
        pnd_Work_Record_Pnd_Wpid_Actn = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Wpid_Actn", "#WPID-ACTN", FieldType.STRING, 1);
        pnd_Work_Record_Rqst_Work_Prcss_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        pnd_Work_Record_Rqst_Work_Prcss_IdRedef1 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Rqst_Work_Prcss_IdRedef1", "Redefines", pnd_Work_Record_Rqst_Work_Prcss_Id);
        pnd_Work_Record_Rqst_Wpid_Actn = pnd_Work_Record_Rqst_Work_Prcss_IdRedef1.newFieldInGroup("pnd_Work_Record_Rqst_Wpid_Actn", "RQST-WPID-ACTN", 
            FieldType.STRING, 1);
        pnd_Work_Record_Rqst_Tiaa_Rcvd_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", FieldType.STRING, 
            8);
        pnd_Work_Record_Return_Doc_Rec_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Return_Rcvd_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Tbl_Business_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.DECIMAL, 
            6,1);
        pnd_Work_Record_Step_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Step_Id", "STEP-ID", FieldType.STRING, 6);
        pnd_Work_Record_Tbl_Status_Key = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Key", "TBL-STATUS-KEY", FieldType.STRING, 13);
        pnd_Work_Record_Tbl_Status_KeyRedef2 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Tbl_Status_KeyRedef2", "Redefines", pnd_Work_Record_Tbl_Status_Key);
        pnd_Work_Record_Last_Chnge_Unit_Cde = pnd_Work_Record_Tbl_Status_KeyRedef2.newFieldInGroup("pnd_Work_Record_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8);
        pnd_Work_Record_Admin_Status_Cde = pnd_Work_Record_Tbl_Status_KeyRedef2.newFieldInGroup("pnd_Work_Record_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4);
        pnd_Work_Record_Admin_Status_Updte_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME);
        pnd_Work_Record_Tbl_Calendar_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 
            4);
        pnd_Work_Record_Rqst_Pin_Nbr = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Rqst_Pin_Nbr", "RQST-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Work_Record_Cntrct_Nbr = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Work_Record_Admin_Status_Updte_Oprtr_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8);

        pnd_Rep_Data = dbsRecord.newGroupInRecord("pnd_Rep_Data", "#REP-DATA");
        pnd_Rep_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Rep_Data_Pnd_Rep_Unit = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Unit", "#REP-UNIT", FieldType.STRING, 88);
        pnd_Rep_Data_Pnd_Rep_UnitRedef3 = pnd_Rep_Data.newGroupInGroup("pnd_Rep_Data_Pnd_Rep_UnitRedef3", "Redefines", pnd_Rep_Data_Pnd_Rep_Unit);
        pnd_Rep_Data_Pnd_Rep_Unit_1 = pnd_Rep_Data_Pnd_Rep_UnitRedef3.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Unit_1", "#REP-UNIT-1", FieldType.STRING, 
            8);
        pnd_Rep_Data_Pnd_Rep_Unit_2 = pnd_Rep_Data_Pnd_Rep_UnitRedef3.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Unit_2", "#REP-UNIT-2", FieldType.STRING, 
            8);
        pnd_Rep_Data_Pnd_Rep_Unit_3 = pnd_Rep_Data_Pnd_Rep_UnitRedef3.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Unit_3", "#REP-UNIT-3", FieldType.STRING, 
            8);
        pnd_Rep_Data_Pnd_Rep_Unit_4 = pnd_Rep_Data_Pnd_Rep_UnitRedef3.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Unit_4", "#REP-UNIT-4", FieldType.STRING, 
            8);
        pnd_Rep_Data_Pnd_Rep_Unit_5 = pnd_Rep_Data_Pnd_Rep_UnitRedef3.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Unit_5", "#REP-UNIT-5", FieldType.STRING, 
            8);
        pnd_Rep_Data_Pnd_Rep_Unit_6 = pnd_Rep_Data_Pnd_Rep_UnitRedef3.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Unit_6", "#REP-UNIT-6", FieldType.STRING, 
            8);
        pnd_Rep_Data_Pnd_Rep_Unit_7 = pnd_Rep_Data_Pnd_Rep_UnitRedef3.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Unit_7", "#REP-UNIT-7", FieldType.STRING, 
            8);
        pnd_Rep_Data_Pnd_Rep_Unit_8 = pnd_Rep_Data_Pnd_Rep_UnitRedef3.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Unit_8", "#REP-UNIT-8", FieldType.STRING, 
            8);
        pnd_Rep_Data_Pnd_Rep_Unit_9 = pnd_Rep_Data_Pnd_Rep_UnitRedef3.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Unit_9", "#REP-UNIT-9", FieldType.STRING, 
            8);
        pnd_Rep_Data_Pnd_Rep_Unit_10 = pnd_Rep_Data_Pnd_Rep_UnitRedef3.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Unit_10", "#REP-UNIT-10", FieldType.STRING, 
            8);
        pnd_Rep_Data_Pnd_Rep_Unit_11 = pnd_Rep_Data_Pnd_Rep_UnitRedef3.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Unit_11", "#REP-UNIT-11", FieldType.STRING, 
            8);
        pnd_Rep_Data_Pnd_Rep_UnitRedef4 = pnd_Rep_Data.newGroupInGroup("pnd_Rep_Data_Pnd_Rep_UnitRedef4", "Redefines", pnd_Rep_Data_Pnd_Rep_Unit);
        pnd_Rep_Data_Pnd_Rep_Unit_Cde = pnd_Rep_Data_Pnd_Rep_UnitRedef4.newFieldArrayInGroup("pnd_Rep_Data_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 
            8, new DbsArrayController(1,11));
        pnd_Rep_Data_Pnd_Rep_Start_Dd = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Start_Dd", "#REP-START-DD", FieldType.NUMERIC, 2);
        pnd_Rep_Data_Pnd_Rep_Start_DdRedef5 = pnd_Rep_Data.newGroupInGroup("pnd_Rep_Data_Pnd_Rep_Start_DdRedef5", "Redefines", pnd_Rep_Data_Pnd_Rep_Start_Dd);
        pnd_Rep_Data_Pnd_Rep_Start_Dd_A = pnd_Rep_Data_Pnd_Rep_Start_DdRedef5.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Start_Dd_A", "#REP-START-DD-A", FieldType.STRING, 
            2);
        pnd_Rep_Data_Pnd_Rep_Start_Mm = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Start_Mm", "#REP-START-MM", FieldType.NUMERIC, 2);
        pnd_Rep_Data_Pnd_Rep_Start_MmRedef6 = pnd_Rep_Data.newGroupInGroup("pnd_Rep_Data_Pnd_Rep_Start_MmRedef6", "Redefines", pnd_Rep_Data_Pnd_Rep_Start_Mm);
        pnd_Rep_Data_Pnd_Rep_Start_Mm_A = pnd_Rep_Data_Pnd_Rep_Start_MmRedef6.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Start_Mm_A", "#REP-START-MM-A", FieldType.STRING, 
            2);
        pnd_Rep_Data_Pnd_Rep_Start_Yy = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Start_Yy", "#REP-START-YY", FieldType.NUMERIC, 2);
        pnd_Rep_Data_Pnd_Rep_Start_YyRedef7 = pnd_Rep_Data.newGroupInGroup("pnd_Rep_Data_Pnd_Rep_Start_YyRedef7", "Redefines", pnd_Rep_Data_Pnd_Rep_Start_Yy);
        pnd_Rep_Data_Pnd_Rep_Start_Yy_A = pnd_Rep_Data_Pnd_Rep_Start_YyRedef7.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Start_Yy_A", "#REP-START-YY-A", FieldType.STRING, 
            2);
        pnd_Rep_Data_Pnd_Rep_End_Dd = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_End_Dd", "#REP-END-DD", FieldType.NUMERIC, 2);
        pnd_Rep_Data_Pnd_Rep_End_DdRedef8 = pnd_Rep_Data.newGroupInGroup("pnd_Rep_Data_Pnd_Rep_End_DdRedef8", "Redefines", pnd_Rep_Data_Pnd_Rep_End_Dd);
        pnd_Rep_Data_Pnd_Rep_End_Dd_A = pnd_Rep_Data_Pnd_Rep_End_DdRedef8.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_End_Dd_A", "#REP-END-DD-A", FieldType.STRING, 
            2);
        pnd_Rep_Data_Pnd_Rep_End_Mm = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_End_Mm", "#REP-END-MM", FieldType.NUMERIC, 2);
        pnd_Rep_Data_Pnd_Rep_End_MmRedef9 = pnd_Rep_Data.newGroupInGroup("pnd_Rep_Data_Pnd_Rep_End_MmRedef9", "Redefines", pnd_Rep_Data_Pnd_Rep_End_Mm);
        pnd_Rep_Data_Pnd_Rep_End_Mm_A = pnd_Rep_Data_Pnd_Rep_End_MmRedef9.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_End_Mm_A", "#REP-END-MM-A", FieldType.STRING, 
            2);
        pnd_Rep_Data_Pnd_Rep_End_Yy = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_End_Yy", "#REP-END-YY", FieldType.NUMERIC, 2);
        pnd_Rep_Data_Pnd_Rep_End_YyRedef10 = pnd_Rep_Data.newGroupInGroup("pnd_Rep_Data_Pnd_Rep_End_YyRedef10", "Redefines", pnd_Rep_Data_Pnd_Rep_End_Yy);
        pnd_Rep_Data_Pnd_Rep_End_Yy_A = pnd_Rep_Data_Pnd_Rep_End_YyRedef10.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_End_Yy_A", "#REP-END-YY-A", FieldType.STRING, 
            2);
        pnd_Rep_Data_Pnd_Rep_Wpid = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Wpid", "#REP-WPID", FieldType.STRING, 6);
        pnd_Rep_Data_Pnd_Rep_WpidRedef11 = pnd_Rep_Data.newGroupInGroup("pnd_Rep_Data_Pnd_Rep_WpidRedef11", "Redefines", pnd_Rep_Data_Pnd_Rep_Wpid);
        pnd_Rep_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde = pnd_Rep_Data_Pnd_Rep_WpidRedef11.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde", "#REP-WPID-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        pnd_Rep_Data_Pnd_Rep_Wpid_Lob = pnd_Rep_Data_Pnd_Rep_WpidRedef11.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Wpid_Lob", "#REP-WPID-LOB", FieldType.STRING, 
            2);
        pnd_Rep_Data_Pnd_Rep_Wpid_Mbp = pnd_Rep_Data_Pnd_Rep_WpidRedef11.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Wpid_Mbp", "#REP-WPID-MBP", FieldType.STRING, 
            1);
        pnd_Rep_Data_Pnd_Rep_Wpid_Sbp = pnd_Rep_Data_Pnd_Rep_WpidRedef11.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Wpid_Sbp", "#REP-WPID-SBP", FieldType.STRING, 
            2);
        pnd_Rep_Data_Pnd_Rep_Status = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status", "#REP-STATUS", FieldType.STRING, 44);
        pnd_Rep_Data_Pnd_Rep_StatusRedef12 = pnd_Rep_Data.newGroupInGroup("pnd_Rep_Data_Pnd_Rep_StatusRedef12", "Redefines", pnd_Rep_Data_Pnd_Rep_Status);
        pnd_Rep_Data_Pnd_Rep_Status_1 = pnd_Rep_Data_Pnd_Rep_StatusRedef12.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_1", "#REP-STATUS-1", FieldType.STRING, 
            4);
        pnd_Rep_Data_Pnd_Rep_Status_2 = pnd_Rep_Data_Pnd_Rep_StatusRedef12.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_2", "#REP-STATUS-2", FieldType.STRING, 
            4);
        pnd_Rep_Data_Pnd_Rep_Status_3 = pnd_Rep_Data_Pnd_Rep_StatusRedef12.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_3", "#REP-STATUS-3", FieldType.STRING, 
            4);
        pnd_Rep_Data_Pnd_Rep_Status_4 = pnd_Rep_Data_Pnd_Rep_StatusRedef12.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_4", "#REP-STATUS-4", FieldType.STRING, 
            4);
        pnd_Rep_Data_Pnd_Rep_Status_5 = pnd_Rep_Data_Pnd_Rep_StatusRedef12.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_5", "#REP-STATUS-5", FieldType.STRING, 
            4);
        pnd_Rep_Data_Pnd_Rep_Status_6 = pnd_Rep_Data_Pnd_Rep_StatusRedef12.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_6", "#REP-STATUS-6", FieldType.STRING, 
            4);
        pnd_Rep_Data_Pnd_Rep_Status_7 = pnd_Rep_Data_Pnd_Rep_StatusRedef12.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_7", "#REP-STATUS-7", FieldType.STRING, 
            4);
        pnd_Rep_Data_Pnd_Rep_Status_8 = pnd_Rep_Data_Pnd_Rep_StatusRedef12.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_8", "#REP-STATUS-8", FieldType.STRING, 
            4);
        pnd_Rep_Data_Pnd_Rep_Status_9 = pnd_Rep_Data_Pnd_Rep_StatusRedef12.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_9", "#REP-STATUS-9", FieldType.STRING, 
            4);
        pnd_Rep_Data_Pnd_Rep_Status_10 = pnd_Rep_Data_Pnd_Rep_StatusRedef12.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_10", "#REP-STATUS-10", FieldType.STRING, 
            4);
        pnd_Rep_Data_Pnd_Rep_StatusRedef13 = pnd_Rep_Data.newGroupInGroup("pnd_Rep_Data_Pnd_Rep_StatusRedef13", "Redefines", pnd_Rep_Data_Pnd_Rep_Status);
        pnd_Rep_Data_Pnd_Rep_Status_Cde = pnd_Rep_Data_Pnd_Rep_StatusRedef13.newFieldArrayInGroup("pnd_Rep_Data_Pnd_Rep_Status_Cde", "#REP-STATUS-CDE", 
            FieldType.STRING, 4, new DbsArrayController(1,10));
        pnd_Rep_Data_Pnd_Rep_Status_From_1 = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_From_1", "#REP-STATUS-FROM-1", FieldType.STRING, 
            4);
        pnd_Rep_Data_Pnd_Rep_Status_To_1 = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_To_1", "#REP-STATUS-TO-1", FieldType.STRING, 4);
        pnd_Rep_Data_Pnd_Rep_Status_From_2 = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_From_2", "#REP-STATUS-FROM-2", FieldType.STRING, 
            4);
        pnd_Rep_Data_Pnd_Rep_Status_To_2 = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Status_To_2", "#REP-STATUS-TO-2", FieldType.STRING, 4);
        pnd_Rep_Data_Pnd_Rep_Racf_Id = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Racf_Id", "#REP-RACF-ID", FieldType.STRING, 8);
        pnd_Rep_Data_Pnd_Rep_Wpid_Name = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Wpid_Name", "#REP-WPID-NAME", FieldType.STRING, 45);
        pnd_Rep_Data_Pnd_Rep_Empl_Name = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Empl_Name", "#REP-EMPL-NAME", FieldType.STRING, 30);
        pnd_Rep_Data_Pnd_Rep_Empl_Name_Detail_Line = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Empl_Name_Detail_Line", "#REP-EMPL-NAME-DETAIL-LINE", 
            FieldType.STRING, 30);
        pnd_Rep_Data_Pnd_Rep_Break_Ind = pnd_Rep_Data.newFieldInGroup("pnd_Rep_Data_Pnd_Rep_Break_Ind", "#REP-BREAK-IND", FieldType.STRING, 1);

        pnd_Misc_Parm = dbsRecord.newGroupInRecord("pnd_Misc_Parm", "#MISC-PARM");
        pnd_Misc_Parm.setParameterOption(ParameterOption.ByReference);
        pnd_Misc_Parm_Pnd_Wrk_Unit_Cde = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Cde", "#WRK-UNIT-CDE", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Wrk_Unit_CdeRedef14 = pnd_Misc_Parm.newGroupInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_CdeRedef14", "Redefines", pnd_Misc_Parm_Pnd_Wrk_Unit_Cde);
        pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde = pnd_Misc_Parm_Pnd_Wrk_Unit_CdeRedef14.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde", "#WRK-UNIT-ID-CDE", 
            FieldType.STRING, 5);
        pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix = pnd_Misc_Parm_Pnd_Wrk_Unit_CdeRedef14.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix", "#WRK-UNIT-SUFFIX", 
            FieldType.STRING, 3);
        pnd_Misc_Parm_Pnd_Save_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Save_Action", "#SAVE-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Save_Wpid = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Save_Wpid", "#SAVE-WPID", FieldType.STRING, 6);
        pnd_Misc_Parm_Pnd_Wpid = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Misc_Parm_Pnd_Wpid_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Action", "#WPID-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Wpid_Code = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Code", "#WPID-CODE", FieldType.STRING, 6);
        pnd_Misc_Parm_Pnd_Wpid_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Count", "#WPID-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Wpid_Desc = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Desc", "#WPID-DESC", FieldType.STRING, 45);
        pnd_Misc_Parm_Pnd_Wpid_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Sname", "#WPID-SNAME", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Wpid_Text = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Text", "#WPID-TEXT", FieldType.STRING, 66);
        pnd_Misc_Parm_Pnd_Sub_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Sub_Count", "#SUB-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Total_Count_1 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count_1", "#TOTAL-COUNT-1", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Total_Count_2 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count_2", "#TOTAL-COUNT-2", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Total_Count_3 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count_3", "#TOTAL-COUNT-3", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Total_Count_4 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count_4", "#TOTAL-COUNT-4", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Total_Count_5 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count_5", "#TOTAL-COUNT-5", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Total_Count_Over = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count_Over", "#TOTAL-COUNT-OVER", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Total_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count", "#TOTAL-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Ptotal_Count_1 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptotal_Count_1", "#PTOTAL-COUNT-1", FieldType.FLOAT, 8);
        pnd_Misc_Parm_Pnd_Ptotal_Count_2 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptotal_Count_2", "#PTOTAL-COUNT-2", FieldType.FLOAT, 8);
        pnd_Misc_Parm_Pnd_Ptotal_Count_3 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptotal_Count_3", "#PTOTAL-COUNT-3", FieldType.FLOAT, 8);
        pnd_Misc_Parm_Pnd_Ptotal_Count_4 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptotal_Count_4", "#PTOTAL-COUNT-4", FieldType.FLOAT, 8);
        pnd_Misc_Parm_Pnd_Ptotal_Count_5 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptotal_Count_5", "#PTOTAL-COUNT-5", FieldType.FLOAT, 8);
        pnd_Misc_Parm_Pnd_Ptotal_Count_Over = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptotal_Count_Over", "#PTOTAL-COUNT-OVER", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Rep_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Rep_Ctr", "#REP-CTR", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Array_Ctrs = pnd_Misc_Parm.newGroupArrayInGroup("pnd_Misc_Parm_Pnd_Array_Ctrs", "#ARRAY-CTRS", new DbsArrayController(1,13));
        pnd_Misc_Parm_Pnd_Booklet_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Booklet_Ctr", "#BOOKLET-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Forms_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Forms_Ctr", "#FORMS-CTR", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Inquire_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Inquire_Ctr", "#INQUIRE-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Research_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Research_Ctr", "#RESEARCH-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Trans_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Trans_Ctr", "#TRANS-CTR", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Complaint_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Complaint_Ctr", "#COMPLAINT-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Other_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Other_Ctr", "#OTHER-CTR", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Percent_Ctrs = pnd_Misc_Parm.newGroupArrayInGroup("pnd_Misc_Parm_Pnd_Percent_Ctrs", "#PERCENT-CTRS", new DbsArrayController(1,
            13));
        pnd_Misc_Parm_Pnd_Pbooklet_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pbooklet_Ctr", "#PBOOKLET-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pforms_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pforms_Ctr", "#PFORMS-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pinquire_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pinquire_Ctr", "#PINQUIRE-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Presearch_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Presearch_Ctr", "#PRESEARCH-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Ptrans_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptrans_Ctr", "#PTRANS-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pcomplaint_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pcomplaint_Ctr", "#PCOMPLAINT-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pother_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pother_Ctr", "#POTHER-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Total_Ctrs = pnd_Misc_Parm.newGroupInGroup("pnd_Misc_Parm_Pnd_Total_Ctrs", "#TOTAL-CTRS");
        pnd_Misc_Parm_Pnd_Tbooklet_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tbooklet_Ctr", "#TBOOKLET-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tforms_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tforms_Ctr", "#TFORMS-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tinquire_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tinquire_Ctr", "#TINQUIRE-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tresearch_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tresearch_Ctr", "#TRESEARCH-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Ttrans_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Ttrans_Ctr", "#TTRANS-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tcomplaint_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tcomplaint_Ctr", "#TCOMPLAINT-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tother_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tother_Ctr", "#TOTHER-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Read_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Read_Count", "#READ-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Actve_Unque_Key = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Actve_Unque_Key", "#ACTVE-UNQUE-KEY", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Todays_Time = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Todays_Time", "#TODAYS-TIME", FieldType.TIME);
        pnd_Misc_Parm_Pnd_Return_Code = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 1);
        pnd_Misc_Parm_Pnd_Return_Msg = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 20);
        pnd_Misc_Parm_Pnd_Status_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Status_Sname", "#STATUS-SNAME", FieldType.STRING, 25);
        pnd_Misc_Parm_Pnd_Partic_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);
        pnd_Misc_Parm_Pnd_Return_Doc_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Doc_Txt", "#RETURN-DOC-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Return_Rcvd_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Rcvd_Txt", "#RETURN-RCVD-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Start_Date = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Start_DateRedef15 = pnd_Misc_Parm.newGroupInGroup("pnd_Misc_Parm_Pnd_Start_DateRedef15", "Redefines", pnd_Misc_Parm_Pnd_Start_Date);
        pnd_Misc_Parm_Pnd_Start_Date_N = pnd_Misc_Parm_Pnd_Start_DateRedef15.newFieldInGroup("pnd_Misc_Parm_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Misc_Parm_Pnd_End_Date = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_End_DateRedef16 = pnd_Misc_Parm.newGroupInGroup("pnd_Misc_Parm_Pnd_End_DateRedef16", "Redefines", pnd_Misc_Parm_Pnd_End_Date);
        pnd_Misc_Parm_Pnd_End_Date_N = pnd_Misc_Parm_Pnd_End_DateRedef16.newFieldInGroup("pnd_Misc_Parm_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Misc_Parm_Pnd_Work_Start_Date_A = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Work_Start_Date_A", "#WORK-START-DATE-A", FieldType.STRING, 
            8);
        pnd_Misc_Parm_Pnd_Work_Start_Date_ARedef17 = pnd_Misc_Parm.newGroupInGroup("pnd_Misc_Parm_Pnd_Work_Start_Date_ARedef17", "Redefines", pnd_Misc_Parm_Pnd_Work_Start_Date_A);
        pnd_Misc_Parm_Pnd_Work_Mm = pnd_Misc_Parm_Pnd_Work_Start_Date_ARedef17.newFieldInGroup("pnd_Misc_Parm_Pnd_Work_Mm", "#WORK-MM", FieldType.NUMERIC, 
            2);
        pnd_Misc_Parm_Pnd_Filler1 = pnd_Misc_Parm_Pnd_Work_Start_Date_ARedef17.newFieldInGroup("pnd_Misc_Parm_Pnd_Filler1", "#FILLER1", FieldType.STRING, 
            1);
        pnd_Misc_Parm_Pnd_Work_Dd = pnd_Misc_Parm_Pnd_Work_Start_Date_ARedef17.newFieldInGroup("pnd_Misc_Parm_Pnd_Work_Dd", "#WORK-DD", FieldType.NUMERIC, 
            2);
        pnd_Misc_Parm_Pnd_Filler2 = pnd_Misc_Parm_Pnd_Work_Start_Date_ARedef17.newFieldInGroup("pnd_Misc_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 
            1);
        pnd_Misc_Parm_Pnd_Work_Yy = pnd_Misc_Parm_Pnd_Work_Start_Date_ARedef17.newFieldInGroup("pnd_Misc_Parm_Pnd_Work_Yy", "#WORK-YY", FieldType.NUMERIC, 
            2);
        pnd_Misc_Parm_Pnd_Work_End_Date_A = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Work_End_Date_A", "#WORK-END-DATE-A", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Work_End_Date_ARedef18 = pnd_Misc_Parm.newGroupInGroup("pnd_Misc_Parm_Pnd_Work_End_Date_ARedef18", "Redefines", pnd_Misc_Parm_Pnd_Work_End_Date_A);
        pnd_Misc_Parm_Pnd_Work_Mmm = pnd_Misc_Parm_Pnd_Work_End_Date_ARedef18.newFieldInGroup("pnd_Misc_Parm_Pnd_Work_Mmm", "#WORK-MMM", FieldType.NUMERIC, 
            2);
        pnd_Misc_Parm_Pnd_Fillera = pnd_Misc_Parm_Pnd_Work_End_Date_ARedef18.newFieldInGroup("pnd_Misc_Parm_Pnd_Fillera", "#FILLERA", FieldType.STRING, 
            1);
        pnd_Misc_Parm_Pnd_Work_Ddd = pnd_Misc_Parm_Pnd_Work_End_Date_ARedef18.newFieldInGroup("pnd_Misc_Parm_Pnd_Work_Ddd", "#WORK-DDD", FieldType.NUMERIC, 
            2);
        pnd_Misc_Parm_Pnd_Fillerb = pnd_Misc_Parm_Pnd_Work_End_Date_ARedef18.newFieldInGroup("pnd_Misc_Parm_Pnd_Fillerb", "#FILLERB", FieldType.STRING, 
            1);
        pnd_Misc_Parm_Pnd_Work_Yyy = pnd_Misc_Parm_Pnd_Work_End_Date_ARedef18.newFieldInGroup("pnd_Misc_Parm_Pnd_Work_Yyy", "#WORK-YYY", FieldType.NUMERIC, 
            2);
        pnd_Misc_Parm_Pnd_Work_Date_D = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Work_Date_D", "#WORK-DATE-D", FieldType.DATE);
        pnd_Misc_Parm_Pnd_Confirmed = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Confirmed", "#CONFIRMED", FieldType.BOOLEAN);
        pnd_Misc_Parm_Pnd_Status_Match = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Status_Match", "#STATUS-MATCH", FieldType.BOOLEAN);
        pnd_Misc_Parm_Pnd_I = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCwfa3310(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

