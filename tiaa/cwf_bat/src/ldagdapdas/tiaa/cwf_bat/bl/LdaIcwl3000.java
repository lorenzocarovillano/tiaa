/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:05:03 PM
**        * FROM NATURAL LDA     : ICWL3000
************************************************************
**        * FILE NAME            : LdaIcwl3000.java
**        * CLASS NAME           : LdaIcwl3000
**        * INSTANCE NAME        : LdaIcwl3000
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIcwl3000 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsGroup cwf_Support_Tbl_Tbl_Key_FieldRedef1;
    private DbsField cwf_Support_Tbl_Run_Status;
    private DbsField cwf_Support_Tbl_Starting_Date_9s_Complement;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;
    private DbsGroup cwf_Support_Tbl_Tbl_Data_FieldRedef2;
    private DbsField cwf_Support_Tbl_Starting_Date;
    private DbsField cwf_Support_Tbl_Ending_Date;
    private DbsField cwf_Support_Tbl_Run_Id;
    private DbsField cwf_Support_Tbl_Program_Name;
    private DbsField cwf_Support_Tbl_Nbr_Of_Recs_Upd_Wr;
    private DbsField cwf_Support_Tbl_Nbr_Of_Recs_Add_Wr;
    private DbsField cwf_Support_Tbl_Nbr_Of_Recs_Opn_Ac;
    private DbsField cwf_Support_Tbl_Nbr_Of_Recs_Cls_Ac;
    private DbsField cwf_Support_Tbl_Nbr_Of_Recs_Spl_Pr;
    private DbsField cwf_Support_Tbl_Nbr_Of_Recs_Ppg;
    private DbsField cwf_Support_Tbl_Nbr_Of_Recs_Inst;
    private DbsField cwf_Support_Tbl_Run_Date;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Tbl_Table_Access_Level;

    public DataAccessProgramView getVw_cwf_Support_Tbl() { return vw_cwf_Support_Tbl; }

    public DbsField getCwf_Support_Tbl_Tbl_Scrty_Level_Ind() { return cwf_Support_Tbl_Tbl_Scrty_Level_Ind; }

    public DbsField getCwf_Support_Tbl_Tbl_Table_Nme() { return cwf_Support_Tbl_Tbl_Table_Nme; }

    public DbsField getCwf_Support_Tbl_Tbl_Key_Field() { return cwf_Support_Tbl_Tbl_Key_Field; }

    public DbsGroup getCwf_Support_Tbl_Tbl_Key_FieldRedef1() { return cwf_Support_Tbl_Tbl_Key_FieldRedef1; }

    public DbsField getCwf_Support_Tbl_Run_Status() { return cwf_Support_Tbl_Run_Status; }

    public DbsField getCwf_Support_Tbl_Starting_Date_9s_Complement() { return cwf_Support_Tbl_Starting_Date_9s_Complement; }

    public DbsField getCwf_Support_Tbl_Tbl_Data_Field() { return cwf_Support_Tbl_Tbl_Data_Field; }

    public DbsGroup getCwf_Support_Tbl_Tbl_Data_FieldRedef2() { return cwf_Support_Tbl_Tbl_Data_FieldRedef2; }

    public DbsField getCwf_Support_Tbl_Starting_Date() { return cwf_Support_Tbl_Starting_Date; }

    public DbsField getCwf_Support_Tbl_Ending_Date() { return cwf_Support_Tbl_Ending_Date; }

    public DbsField getCwf_Support_Tbl_Run_Id() { return cwf_Support_Tbl_Run_Id; }

    public DbsField getCwf_Support_Tbl_Program_Name() { return cwf_Support_Tbl_Program_Name; }

    public DbsField getCwf_Support_Tbl_Nbr_Of_Recs_Upd_Wr() { return cwf_Support_Tbl_Nbr_Of_Recs_Upd_Wr; }

    public DbsField getCwf_Support_Tbl_Nbr_Of_Recs_Add_Wr() { return cwf_Support_Tbl_Nbr_Of_Recs_Add_Wr; }

    public DbsField getCwf_Support_Tbl_Nbr_Of_Recs_Opn_Ac() { return cwf_Support_Tbl_Nbr_Of_Recs_Opn_Ac; }

    public DbsField getCwf_Support_Tbl_Nbr_Of_Recs_Cls_Ac() { return cwf_Support_Tbl_Nbr_Of_Recs_Cls_Ac; }

    public DbsField getCwf_Support_Tbl_Nbr_Of_Recs_Spl_Pr() { return cwf_Support_Tbl_Nbr_Of_Recs_Spl_Pr; }

    public DbsField getCwf_Support_Tbl_Nbr_Of_Recs_Ppg() { return cwf_Support_Tbl_Nbr_Of_Recs_Ppg; }

    public DbsField getCwf_Support_Tbl_Nbr_Of_Recs_Inst() { return cwf_Support_Tbl_Nbr_Of_Recs_Inst; }

    public DbsField getCwf_Support_Tbl_Run_Date() { return cwf_Support_Tbl_Run_Date; }

    public DbsField getCwf_Support_Tbl_Tbl_Actve_Ind() { return cwf_Support_Tbl_Tbl_Actve_Ind; }

    public DbsField getCwf_Support_Tbl_Tbl_Entry_Dte_Tme() { return cwf_Support_Tbl_Tbl_Entry_Dte_Tme; }

    public DbsField getCwf_Support_Tbl_Tbl_Entry_Oprtr_Cde() { return cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde; }

    public DbsField getCwf_Support_Tbl_Tbl_Updte_Dte_Tme() { return cwf_Support_Tbl_Tbl_Updte_Dte_Tme; }

    public DbsField getCwf_Support_Tbl_Tbl_Updte_Dte() { return cwf_Support_Tbl_Tbl_Updte_Dte; }

    public DbsField getCwf_Support_Tbl_Tbl_Updte_Oprtr_Cde() { return cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde; }

    public DbsField getCwf_Support_Tbl_Tbl_Dlte_Dte_Tme() { return cwf_Support_Tbl_Tbl_Dlte_Dte_Tme; }

    public DbsField getCwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde() { return cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde; }

    public DbsField getCwf_Support_Tbl_Tbl_Table_Rectype() { return cwf_Support_Tbl_Tbl_Table_Rectype; }

    public DbsField getCwf_Support_Tbl_Tbl_Table_Access_Level() { return cwf_Support_Tbl_Tbl_Table_Access_Level; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Key_FieldRedef1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl_Tbl_Key_FieldRedef1", "Redefines", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Run_Status = cwf_Support_Tbl_Tbl_Key_FieldRedef1.newFieldInGroup("cwf_Support_Tbl_Run_Status", "RUN-STATUS", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Starting_Date_9s_Complement = cwf_Support_Tbl_Tbl_Key_FieldRedef1.newFieldInGroup("cwf_Support_Tbl_Starting_Date_9s_Complement", 
            "STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 15);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        cwf_Support_Tbl_Tbl_Data_FieldRedef2 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl_Tbl_Data_FieldRedef2", "Redefines", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Starting_Date = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Starting_Date", "STARTING-DATE", FieldType.TIME);
        cwf_Support_Tbl_Ending_Date = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Ending_Date", "ENDING-DATE", FieldType.TIME);
        cwf_Support_Tbl_Run_Id = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Run_Id", "RUN-ID", FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Program_Name = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Program_Name", "PROGRAM-NAME", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Nbr_Of_Recs_Upd_Wr = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Nbr_Of_Recs_Upd_Wr", "NBR-OF-RECS-UPD-WR", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Nbr_Of_Recs_Add_Wr = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Nbr_Of_Recs_Add_Wr", "NBR-OF-RECS-ADD-WR", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Nbr_Of_Recs_Opn_Ac = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Nbr_Of_Recs_Opn_Ac", "NBR-OF-RECS-OPN-AC", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Nbr_Of_Recs_Cls_Ac = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Nbr_Of_Recs_Cls_Ac", "NBR-OF-RECS-CLS-AC", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Nbr_Of_Recs_Spl_Pr = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Nbr_Of_Recs_Spl_Pr", "NBR-OF-RECS-SPL-PR", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Nbr_Of_Recs_Ppg = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Nbr_Of_Recs_Ppg", "NBR-OF-RECS-PPG", FieldType.NUMERIC, 
            9);
        cwf_Support_Tbl_Nbr_Of_Recs_Inst = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Nbr_Of_Recs_Inst", "NBR-OF-RECS-INST", 
            FieldType.NUMERIC, 9);
        cwf_Support_Tbl_Run_Date = cwf_Support_Tbl_Tbl_Data_FieldRedef2.newFieldInGroup("cwf_Support_Tbl_Run_Date", "RUN-DATE", FieldType.NUMERIC, 8);
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");

        this.setRecordName("LdaIcwl3000");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cwf_Support_Tbl.reset();
    }

    // Constructor
    public LdaIcwl3000() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
