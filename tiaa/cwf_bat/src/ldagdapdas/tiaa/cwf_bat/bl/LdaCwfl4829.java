/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:55 PM
**        * FROM NATURAL LDA     : CWFL4829
************************************************************
**        * FILE NAME            : LdaCwfl4829.java
**        * CLASS NAME           : LdaCwfl4829
**        * INSTANCE NAME        : LdaCwfl4829
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl4829 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cwf_Bsnss_Data_File;
    private DbsField cwf_Bsnss_Data_File_Rqst_Log_Dte_Tme;
    private DbsField cwf_Bsnss_Data_File_Da_Amt;
    private DbsField cwf_Bsnss_Data_File_Tpa_Amt;
    private DbsField cwf_Bsnss_Data_File_Ipro_Amt;
    private DbsField cwf_Bsnss_Data_File_P_And_I_Amt;
    private DbsField cwf_Bsnss_Data_File_Smart_Class;
    private DbsField cwf_Bsnss_Data_File_Proj_Id;
    private DbsField cwf_Bsnss_Data_File_Last_Updte_Dte_Tme;
    private DbsField cwf_Bsnss_Data_File_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Bsnss_Data_File_Entry_Dte_Tme;
    private DbsField cwf_Bsnss_Data_File_Entry_Oprtr_Cde;

    public DataAccessProgramView getVw_cwf_Bsnss_Data_File() { return vw_cwf_Bsnss_Data_File; }

    public DbsField getCwf_Bsnss_Data_File_Rqst_Log_Dte_Tme() { return cwf_Bsnss_Data_File_Rqst_Log_Dte_Tme; }

    public DbsField getCwf_Bsnss_Data_File_Da_Amt() { return cwf_Bsnss_Data_File_Da_Amt; }

    public DbsField getCwf_Bsnss_Data_File_Tpa_Amt() { return cwf_Bsnss_Data_File_Tpa_Amt; }

    public DbsField getCwf_Bsnss_Data_File_Ipro_Amt() { return cwf_Bsnss_Data_File_Ipro_Amt; }

    public DbsField getCwf_Bsnss_Data_File_P_And_I_Amt() { return cwf_Bsnss_Data_File_P_And_I_Amt; }

    public DbsField getCwf_Bsnss_Data_File_Smart_Class() { return cwf_Bsnss_Data_File_Smart_Class; }

    public DbsField getCwf_Bsnss_Data_File_Proj_Id() { return cwf_Bsnss_Data_File_Proj_Id; }

    public DbsField getCwf_Bsnss_Data_File_Last_Updte_Dte_Tme() { return cwf_Bsnss_Data_File_Last_Updte_Dte_Tme; }

    public DbsField getCwf_Bsnss_Data_File_Last_Updte_Oprtr_Cde() { return cwf_Bsnss_Data_File_Last_Updte_Oprtr_Cde; }

    public DbsField getCwf_Bsnss_Data_File_Entry_Dte_Tme() { return cwf_Bsnss_Data_File_Entry_Dte_Tme; }

    public DbsField getCwf_Bsnss_Data_File_Entry_Oprtr_Cde() { return cwf_Bsnss_Data_File_Entry_Oprtr_Cde; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cwf_Bsnss_Data_File = new DataAccessProgramView(new NameInfo("vw_cwf_Bsnss_Data_File", "CWF-BSNSS-DATA-FILE"), "CWF_BSNSS_DATA_FILE", "CWF_BUS_DATA");
        cwf_Bsnss_Data_File_Rqst_Log_Dte_Tme = vw_cwf_Bsnss_Data_File.getRecord().newFieldInGroup("cwf_Bsnss_Data_File_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Bsnss_Data_File_Da_Amt = vw_cwf_Bsnss_Data_File.getRecord().newFieldInGroup("cwf_Bsnss_Data_File_Da_Amt", "DA-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "DA_AMT");
        cwf_Bsnss_Data_File_Tpa_Amt = vw_cwf_Bsnss_Data_File.getRecord().newFieldInGroup("cwf_Bsnss_Data_File_Tpa_Amt", "TPA-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TPA_AMT");
        cwf_Bsnss_Data_File_Ipro_Amt = vw_cwf_Bsnss_Data_File.getRecord().newFieldInGroup("cwf_Bsnss_Data_File_Ipro_Amt", "IPRO-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "IPRO_AMT");
        cwf_Bsnss_Data_File_P_And_I_Amt = vw_cwf_Bsnss_Data_File.getRecord().newFieldInGroup("cwf_Bsnss_Data_File_P_And_I_Amt", "P-AND-I-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "P_AND_I_AMT");
        cwf_Bsnss_Data_File_Smart_Class = vw_cwf_Bsnss_Data_File.getRecord().newFieldInGroup("cwf_Bsnss_Data_File_Smart_Class", "SMART-CLASS", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "SMART_CLASS");
        cwf_Bsnss_Data_File_Proj_Id = vw_cwf_Bsnss_Data_File.getRecord().newFieldInGroup("cwf_Bsnss_Data_File_Proj_Id", "PROJ-ID", FieldType.STRING, 28, 
            RepeatingFieldStrategy.None, "PROJ_ID");
        cwf_Bsnss_Data_File_Last_Updte_Dte_Tme = vw_cwf_Bsnss_Data_File.getRecord().newFieldInGroup("cwf_Bsnss_Data_File_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Bsnss_Data_File_Last_Updte_Oprtr_Cde = vw_cwf_Bsnss_Data_File.getRecord().newFieldInGroup("cwf_Bsnss_Data_File_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Bsnss_Data_File_Entry_Dte_Tme = vw_cwf_Bsnss_Data_File.getRecord().newFieldInGroup("cwf_Bsnss_Data_File_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Bsnss_Data_File_Entry_Oprtr_Cde = vw_cwf_Bsnss_Data_File.getRecord().newFieldInGroup("cwf_Bsnss_Data_File_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");

        this.setRecordName("LdaCwfl4829");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cwf_Bsnss_Data_File.reset();
    }

    // Constructor
    public LdaCwfl4829() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
