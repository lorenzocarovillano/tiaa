/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:55:00 PM
**        * FROM NATURAL LDA     : CWFL3802
************************************************************
**        * FILE NAME            : LdaCwfl3802.java
**        * CLASS NAME           : LdaCwfl3802
**        * INSTANCE NAME        : LdaCwfl3802
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwfl3802 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Tbl_Admin_Unit;
    private DbsField pnd_Work_Record_Tbl_Moc;
    private DbsField pnd_Work_Record_Tbl_Racf;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Bsnss_Days;
    private DbsField pnd_Work_Record_Tbl_Business_Days;
    private DbsField pnd_Work_Record_Tbl_Complete_Days;
    private DbsField pnd_Work_Record_Tbl_Wpid_Act;
    private DbsField pnd_Work_Record_Tbl_Wpid;
    private DbsGroup pnd_Work_Record_Tbl_WpidRedef1;
    private DbsField pnd_Work_Record_Tbl_Wpid_Action;
    private DbsField pnd_Work_Record_Tbl_Status_Key;
    private DbsGroup pnd_Work_Record_Tbl_Status_KeyRedef2;
    private DbsField pnd_Work_Record_Tbl_Last_Chnge_Unit;
    private DbsField pnd_Work_Record_Tbl_Status_Cde;
    private DbsField pnd_Work_Record_Tbl_Close_Unit;
    private DbsGroup pnd_Work_Record_Tbl_Close_UnitRedef3;
    private DbsField pnd_Work_Record_Tbl_Close_Unit_Id;
    private DbsField pnd_Work_Record_Tbl_Pin;
    private DbsField pnd_Work_Record_Tbl_Contracts;
    private DbsField pnd_Work_Record_Tbl_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Dte;
    private DbsField pnd_Work_Record_Tbl_Status_Dte;
    private DbsField pnd_Work_Record_Return_Doc_Rec_Dte_Tme;
    private DbsField pnd_Work_Record_Return_Rcvd_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Last_Chge_Dte;
    private DbsField pnd_Work_Record_Pnd_Partic_Sname;
    private DbsGroup pnd_Work_Record_Pnd_Partic_SnameRedef4;
    private DbsField pnd_Work_Record_Pnd_Fldr_Prefix;
    private DbsField pnd_Work_Record_Pnd_Physcl_Fldr_Id_Nbr;
    private DbsField pnd_Work_Record_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Pnd_Last_Chnge_Dte_Tme;
    private DataAccessProgramView vw_cwf_Master_Index;
    private DbsField cwf_Master_Index_Pin_Nbr;
    private DbsField cwf_Master_Index_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Index_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_Work_Prcss_Id;
    private DbsGroup cwf_Master_Index_Work_Prcss_IdRedef5;
    private DbsField cwf_Master_Index_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Master_Index_Unit_Cde;
    private DbsGroup cwf_Master_Index_Unit_CdeRedef6;
    private DbsField cwf_Master_Index_Unit_Id_Cde;
    private DbsField cwf_Master_Index_Empl_Oprtr_Cde;
    private DbsGroup cwf_Master_Index_Empl_Oprtr_CdeRedef7;
    private DbsField cwf_Master_Index_Empl_Racf_Id;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Status_Cde;
    private DbsField cwf_Master_Index_Last_Updte_Dte;
    private DbsField cwf_Master_Index_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_Effctve_Dte;
    private DbsField cwf_Master_Index_Trnsctn_Dte;
    private DbsField cwf_Master_Index_Trans_Dte;
    private DbsField cwf_Master_Index_Physcl_Fldr_Id_Nbr;
    private DbsField cwf_Master_Index_Mj_Chrge_Oprtr_Cde;
    private DbsField cwf_Master_Index_Mstr_Indx_Actn_Cde;
    private DbsField cwf_Master_Index_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme;
    private DbsField cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde;
    private DbsField cwf_Master_Index_Print_Q_Ind;
    private DbsField cwf_Master_Index_Print_Dte_Tme;
    private DbsField cwf_Master_Index_Print_Batch_Sqnce_Nbr;
    private DbsField cwf_Master_Index_Print_Batch_Ind;
    private DbsField cwf_Master_Index_Printer_Id_Cde;
    private DbsField cwf_Master_Index_Rescan_Ind;
    private DbsField cwf_Master_Index_Dup_Ind;
    private DbsField cwf_Master_Index_Cmplnt_Ind;
    private DbsGroup cwf_Master_Index_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_Cntrct_Nbr;
    private DbsField cwf_Master_Index_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_Work_List_Ind;
    private DbsField cwf_Master_Index_Status_Freeze_Ind;
    private DbsField cwf_Master_Index_Elctrnc_Fldr_Ind;
    private DbsField cwf_Master_Index_Log_Rqstr_Cde;
    private DbsField cwf_Master_Index_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_Days;
    private DbsField cwf_Master_Index_Crprte_Clock_End_Dte_Tme;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Env;
    private DbsField pnd_Base_Date;
    private DbsField pnd_Rep_Break_Ind;
    private DbsField pnd_Save_Action;
    private DbsField pnd_Wpid;
    private DbsField pnd_Wpid_Action;
    private DbsField pnd_Wpid_Count;
    private DbsField pnd_Wpid_Sum;
    private DbsField pnd_Wpid_Ave;
    private DbsField pnd_Wpid_Sname;
    private DbsField pnd_Wpid_Text;
    private DbsField pnd_Wpid_Ave_Text;
    private DbsField pnd_Sub_Count;
    private DbsField pnd_Total_Count_3;
    private DbsField pnd_Total_Count_4;
    private DbsField pnd_Total_Count_5;
    private DbsField pnd_Total_Count_10;
    private DbsField pnd_Total_Count_Over;
    private DbsField pnd_Total_Count;
    private DbsField pnd_Ctot_Count_3;
    private DbsField pnd_Ctot_Count_4;
    private DbsField pnd_Ctot_Count_5;
    private DbsField pnd_Ctot_Count_10;
    private DbsField pnd_Ctot_Count_Over;
    private DbsField pnd_Ctot_Count;
    private DbsField pnd_Ptotal_Count_3;
    private DbsField pnd_Ptotal_Count_4;
    private DbsField pnd_Ptotal_Count_5;
    private DbsField pnd_Ptotal_Count_10;
    private DbsField pnd_Ptotal_Count_Over;
    private DbsField pnd_Rep_Ctr;
    private DbsGroup pnd_Array_Ctrs;
    private DbsField pnd_Array_Ctrs_Pnd_Booklet_Ctr;
    private DbsField pnd_Array_Ctrs_Pnd_Forms_Ctr;
    private DbsField pnd_Array_Ctrs_Pnd_Inquire_Ctr;
    private DbsField pnd_Array_Ctrs_Pnd_Research_Ctr;
    private DbsField pnd_Array_Ctrs_Pnd_Trans_Ctr;
    private DbsField pnd_Array_Ctrs_Pnd_Complaint_Ctr;
    private DbsField pnd_Array_Ctrs_Pnd_Others_Ctr;
    private DbsGroup pnd_Array_Cnts;
    private DbsField pnd_Array_Cnts_Pnd_Booklet_Cnt;
    private DbsField pnd_Array_Cnts_Pnd_Forms_Cnt;
    private DbsField pnd_Array_Cnts_Pnd_Inquire_Cnt;
    private DbsField pnd_Array_Cnts_Pnd_Research_Cnt;
    private DbsField pnd_Array_Cnts_Pnd_Trans_Cnt;
    private DbsField pnd_Array_Cnts_Pnd_Complaint_Cnt;
    private DbsField pnd_Array_Cnts_Pnd_Others_Cnt;
    private DbsGroup pnd_Percent_Ctrs;
    private DbsField pnd_Percent_Ctrs_Pnd_Pbooklet_Ctr;
    private DbsField pnd_Percent_Ctrs_Pnd_Pforms_Ctr;
    private DbsField pnd_Percent_Ctrs_Pnd_Pinquire_Ctr;
    private DbsField pnd_Percent_Ctrs_Pnd_Presearch_Ctr;
    private DbsField pnd_Percent_Ctrs_Pnd_Ptrans_Ctr;
    private DbsField pnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr;
    private DbsField pnd_Percent_Ctrs_Pnd_Pothers_Ctr;
    private DbsGroup pnd_Percent_Cnts;
    private DbsField pnd_Percent_Cnts_Pnd_Pbooklet_Cnt;
    private DbsField pnd_Percent_Cnts_Pnd_Pforms_Cnt;
    private DbsField pnd_Percent_Cnts_Pnd_Pinquire_Cnt;
    private DbsField pnd_Percent_Cnts_Pnd_Presearch_Cnt;
    private DbsField pnd_Percent_Cnts_Pnd_Ptrans_Cnt;
    private DbsField pnd_Percent_Cnts_Pnd_Pcomplaint_Cnt;
    private DbsField pnd_Percent_Cnts_Pnd_Pothers_Cnt;
    private DbsGroup pnd_Total_Ctrs;
    private DbsField pnd_Total_Ctrs_Pnd_Tbooklet_Ctr;
    private DbsField pnd_Total_Ctrs_Pnd_Tforms_Ctr;
    private DbsField pnd_Total_Ctrs_Pnd_Tinquire_Ctr;
    private DbsField pnd_Total_Ctrs_Pnd_Tresearch_Ctr;
    private DbsField pnd_Total_Ctrs_Pnd_Ttrans_Ctr;
    private DbsField pnd_Total_Ctrs_Pnd_Tcomplaint_Ctr;
    private DbsField pnd_Total_Ctrs_Pnd_Tothers_Ctr;
    private DbsField pnd_Todays_Time;
    private DbsField pnd_Status_Sname;
    private DbsField pnd_Return_Doc_Txt;
    private DbsField pnd_Return_Rcvd_Txt;
    private DbsField pnd_Work_Date_D;
    private DbsField pnd_Rep_Unit_Cde;
    private DbsGroup pnd_Rep_Unit_CdeRedef8;
    private DbsField pnd_Rep_Unit_Cde_Pnd_Rep_Unit_Id;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Start_Dte_Tme;
    private DbsField pnd_End_Dte_Tme;
    private DbsField pnd_Work_St_Date;
    private DbsField pnd_Work_End_Date;
    private DbsField pnd_Work_Date_Time;
    private DbsGroup pnd_Work_Date_TimeRedef9;
    private DbsField pnd_Work_Date_Time_Pnd_Work_Date;
    private DbsField pnd_Work_Date_Time_Pnd_Work_Time;
    private DbsField pnd_Old_Moc;
    private DbsField pnd_Save_Moc;
    private DbsField pnd_Save_Moc_1;
    private DbsField pnd_Save_Unit;
    private DbsField pnd_First_Unit;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Page_Num;
    private DbsField pnd_Rep_Unit_Nme;
    private DbsField pnd_Start_Dte_Tme_A;
    private DbsGroup pnd_Start_Dte_Tme_ARedef10;
    private DbsField pnd_Start_Dte_Tme_A_Pnd_Start_Dte_Tme_N;
    private DbsField pnd_End_Dte_Tme_A;
    private DbsGroup pnd_End_Dte_Tme_ARedef11;
    private DbsField pnd_End_Dte_Tme_A_Pnd_End_Dte_Tme_N;
    private DbsField pnd_Sub;
    private DbsField pnd_Start_Key;
    private DbsGroup pnd_Start_KeyRedef12;
    private DbsField pnd_Start_Key_Crprte_Status_Ind;
    private DbsField pnd_Start_Key_Last_Chnge_Invrt_Dte_Tme;
    private DbsField pnd_End_Key;
    private DbsGroup pnd_End_KeyRedef13;
    private DbsField pnd_End_Key_Crprte_Status_Ind;
    private DbsField pnd_End_Key_Last_Chnge_Invrt_Dte_Tme;
    private DbsField pnd_Timx;
    private DbsField pnd_Participant_Closed_Date_Time;
    private DbsField pnd_Receive_Date;
    private DbsField pnd_Report_Date;
    private DbsField pnd_Status_Updte_Dte;
    private DbsField pnd_Grand_Title;
    private DbsField pnd_Grand_Title2;
    private DbsField pnd_Generate_Combined_Totals;
    private DbsField pnd_Mid_Total;
    private DbsField pnd_Compare_Unit;
    private DbsField pnd_Comp_Unit;
    private DbsGroup pnd_Gtot_Registers;
    private DbsField pnd_Gtot_Registers_Pnd_Ave_Total;
    private DbsField pnd_Gtot_Registers_Pnd_Ave_Total_Num;
    private DbsField pnd_Gtot_Registers_Pnd_Total_Moc;
    private DbsField pnd_Gtot_Registers_Pnd_Total_Moc_3;
    private DbsField pnd_Gtot_Registers_Pnd_Total_Moc_4;
    private DbsField pnd_Gtot_Registers_Pnd_Total_Moc_5;
    private DbsField pnd_Gtot_Registers_Pnd_Total_Moc_10;
    private DbsField pnd_Gtot_Registers_Pnd_Total_Moc_Over;
    private DbsGroup pnd_Combined_Totals;
    private DbsField pnd_Combined_Totals_Pnd_Cave_Total;
    private DbsField pnd_Combined_Totals_Pnd_Cave_Total_Num;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_3;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_4;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_5;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_10;
    private DbsField pnd_Combined_Totals_Pnd_Ctotal_Moc_Over;
    private DbsField pnd_Holiday;
    private DbsField pnd_Datx;
    private DbsField pnd_Combine_Unit;
    private DbsField pnd_Exclude_Wpids;
    private DbsField pnd_Valid_Units;
    private DbsField pnd_Combined_Unit;
    private DbsField pnd_Combined_Unit_Hdr;

    public DbsGroup getPnd_Work_Record() { return pnd_Work_Record; }

    public DbsField getPnd_Work_Record_Tbl_Admin_Unit() { return pnd_Work_Record_Tbl_Admin_Unit; }

    public DbsField getPnd_Work_Record_Tbl_Moc() { return pnd_Work_Record_Tbl_Moc; }

    public DbsField getPnd_Work_Record_Tbl_Racf() { return pnd_Work_Record_Tbl_Racf; }

    public DbsField getPnd_Work_Record_Tbl_Tiaa_Bsnss_Days() { return pnd_Work_Record_Tbl_Tiaa_Bsnss_Days; }

    public DbsField getPnd_Work_Record_Tbl_Business_Days() { return pnd_Work_Record_Tbl_Business_Days; }

    public DbsField getPnd_Work_Record_Tbl_Complete_Days() { return pnd_Work_Record_Tbl_Complete_Days; }

    public DbsField getPnd_Work_Record_Tbl_Wpid_Act() { return pnd_Work_Record_Tbl_Wpid_Act; }

    public DbsField getPnd_Work_Record_Tbl_Wpid() { return pnd_Work_Record_Tbl_Wpid; }

    public DbsGroup getPnd_Work_Record_Tbl_WpidRedef1() { return pnd_Work_Record_Tbl_WpidRedef1; }

    public DbsField getPnd_Work_Record_Tbl_Wpid_Action() { return pnd_Work_Record_Tbl_Wpid_Action; }

    public DbsField getPnd_Work_Record_Tbl_Status_Key() { return pnd_Work_Record_Tbl_Status_Key; }

    public DbsGroup getPnd_Work_Record_Tbl_Status_KeyRedef2() { return pnd_Work_Record_Tbl_Status_KeyRedef2; }

    public DbsField getPnd_Work_Record_Tbl_Last_Chnge_Unit() { return pnd_Work_Record_Tbl_Last_Chnge_Unit; }

    public DbsField getPnd_Work_Record_Tbl_Status_Cde() { return pnd_Work_Record_Tbl_Status_Cde; }

    public DbsField getPnd_Work_Record_Tbl_Close_Unit() { return pnd_Work_Record_Tbl_Close_Unit; }

    public DbsGroup getPnd_Work_Record_Tbl_Close_UnitRedef3() { return pnd_Work_Record_Tbl_Close_UnitRedef3; }

    public DbsField getPnd_Work_Record_Tbl_Close_Unit_Id() { return pnd_Work_Record_Tbl_Close_Unit_Id; }

    public DbsField getPnd_Work_Record_Tbl_Pin() { return pnd_Work_Record_Tbl_Pin; }

    public DbsField getPnd_Work_Record_Tbl_Contracts() { return pnd_Work_Record_Tbl_Contracts; }

    public DbsField getPnd_Work_Record_Tbl_Log_Dte_Tme() { return pnd_Work_Record_Tbl_Log_Dte_Tme; }

    public DbsField getPnd_Work_Record_Tbl_Tiaa_Dte() { return pnd_Work_Record_Tbl_Tiaa_Dte; }

    public DbsField getPnd_Work_Record_Tbl_Status_Dte() { return pnd_Work_Record_Tbl_Status_Dte; }

    public DbsField getPnd_Work_Record_Return_Doc_Rec_Dte_Tme() { return pnd_Work_Record_Return_Doc_Rec_Dte_Tme; }

    public DbsField getPnd_Work_Record_Return_Rcvd_Dte_Tme() { return pnd_Work_Record_Return_Rcvd_Dte_Tme; }

    public DbsField getPnd_Work_Record_Tbl_Last_Chge_Dte() { return pnd_Work_Record_Tbl_Last_Chge_Dte; }

    public DbsField getPnd_Work_Record_Pnd_Partic_Sname() { return pnd_Work_Record_Pnd_Partic_Sname; }

    public DbsGroup getPnd_Work_Record_Pnd_Partic_SnameRedef4() { return pnd_Work_Record_Pnd_Partic_SnameRedef4; }

    public DbsField getPnd_Work_Record_Pnd_Fldr_Prefix() { return pnd_Work_Record_Pnd_Fldr_Prefix; }

    public DbsField getPnd_Work_Record_Pnd_Physcl_Fldr_Id_Nbr() { return pnd_Work_Record_Pnd_Physcl_Fldr_Id_Nbr; }

    public DbsField getPnd_Work_Record_Pnd_Rqst_Log_Dte_Tme() { return pnd_Work_Record_Pnd_Rqst_Log_Dte_Tme; }

    public DbsField getPnd_Work_Record_Pnd_Last_Chnge_Dte_Tme() { return pnd_Work_Record_Pnd_Last_Chnge_Dte_Tme; }

    public DataAccessProgramView getVw_cwf_Master_Index() { return vw_cwf_Master_Index; }

    public DbsField getCwf_Master_Index_Pin_Nbr() { return cwf_Master_Index_Pin_Nbr; }

    public DbsField getCwf_Master_Index_Rqst_Log_Dte_Tme() { return cwf_Master_Index_Rqst_Log_Dte_Tme; }

    public DbsField getCwf_Master_Index_Rqst_Orgn_Cde() { return cwf_Master_Index_Rqst_Orgn_Cde; }

    public DbsField getCwf_Master_Index_Work_Prcss_Id() { return cwf_Master_Index_Work_Prcss_Id; }

    public DbsGroup getCwf_Master_Index_Work_Prcss_IdRedef5() { return cwf_Master_Index_Work_Prcss_IdRedef5; }

    public DbsField getCwf_Master_Index_Work_Actn_Rqstd_Cde() { return cwf_Master_Index_Work_Actn_Rqstd_Cde; }

    public DbsField getCwf_Master_Index_Unit_Cde() { return cwf_Master_Index_Unit_Cde; }

    public DbsGroup getCwf_Master_Index_Unit_CdeRedef6() { return cwf_Master_Index_Unit_CdeRedef6; }

    public DbsField getCwf_Master_Index_Unit_Id_Cde() { return cwf_Master_Index_Unit_Id_Cde; }

    public DbsField getCwf_Master_Index_Empl_Oprtr_Cde() { return cwf_Master_Index_Empl_Oprtr_Cde; }

    public DbsGroup getCwf_Master_Index_Empl_Oprtr_CdeRedef7() { return cwf_Master_Index_Empl_Oprtr_CdeRedef7; }

    public DbsField getCwf_Master_Index_Empl_Racf_Id() { return cwf_Master_Index_Empl_Racf_Id; }

    public DbsField getCwf_Master_Index_Last_Chnge_Dte_Tme() { return cwf_Master_Index_Last_Chnge_Dte_Tme; }

    public DbsField getCwf_Master_Index_Last_Chnge_Oprtr_Cde() { return cwf_Master_Index_Last_Chnge_Oprtr_Cde; }

    public DbsField getCwf_Master_Index_Last_Chnge_Unit_Cde() { return cwf_Master_Index_Last_Chnge_Unit_Cde; }

    public DbsField getCwf_Master_Index_Admin_Unit_Cde() { return cwf_Master_Index_Admin_Unit_Cde; }

    public DbsField getCwf_Master_Index_Admin_Status_Cde() { return cwf_Master_Index_Admin_Status_Cde; }

    public DbsField getCwf_Master_Index_Admin_Status_Updte_Dte_Tme() { return cwf_Master_Index_Admin_Status_Updte_Dte_Tme; }

    public DbsField getCwf_Master_Index_Status_Cde() { return cwf_Master_Index_Status_Cde; }

    public DbsField getCwf_Master_Index_Last_Updte_Dte() { return cwf_Master_Index_Last_Updte_Dte; }

    public DbsField getCwf_Master_Index_Last_Updte_Dte_Tme() { return cwf_Master_Index_Last_Updte_Dte_Tme; }

    public DbsField getCwf_Master_Index_Tiaa_Rcvd_Dte() { return cwf_Master_Index_Tiaa_Rcvd_Dte; }

    public DbsField getCwf_Master_Index_Effctve_Dte() { return cwf_Master_Index_Effctve_Dte; }

    public DbsField getCwf_Master_Index_Trnsctn_Dte() { return cwf_Master_Index_Trnsctn_Dte; }

    public DbsField getCwf_Master_Index_Trans_Dte() { return cwf_Master_Index_Trans_Dte; }

    public DbsField getCwf_Master_Index_Physcl_Fldr_Id_Nbr() { return cwf_Master_Index_Physcl_Fldr_Id_Nbr; }

    public DbsField getCwf_Master_Index_Mj_Chrge_Oprtr_Cde() { return cwf_Master_Index_Mj_Chrge_Oprtr_Cde; }

    public DbsField getCwf_Master_Index_Mstr_Indx_Actn_Cde() { return cwf_Master_Index_Mstr_Indx_Actn_Cde; }

    public DbsField getCwf_Master_Index_Mj_Pull_Ind() { return cwf_Master_Index_Mj_Pull_Ind; }

    public DbsField getCwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme() { return cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme; }

    public DbsField getCwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde() { return cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde; }

    public DbsField getCwf_Master_Index_Print_Q_Ind() { return cwf_Master_Index_Print_Q_Ind; }

    public DbsField getCwf_Master_Index_Print_Dte_Tme() { return cwf_Master_Index_Print_Dte_Tme; }

    public DbsField getCwf_Master_Index_Print_Batch_Sqnce_Nbr() { return cwf_Master_Index_Print_Batch_Sqnce_Nbr; }

    public DbsField getCwf_Master_Index_Print_Batch_Ind() { return cwf_Master_Index_Print_Batch_Ind; }

    public DbsField getCwf_Master_Index_Printer_Id_Cde() { return cwf_Master_Index_Printer_Id_Cde; }

    public DbsField getCwf_Master_Index_Rescan_Ind() { return cwf_Master_Index_Rescan_Ind; }

    public DbsField getCwf_Master_Index_Dup_Ind() { return cwf_Master_Index_Dup_Ind; }

    public DbsField getCwf_Master_Index_Cmplnt_Ind() { return cwf_Master_Index_Cmplnt_Ind; }

    public DbsGroup getCwf_Master_Index_Cntrct_NbrMuGroup() { return cwf_Master_Index_Cntrct_NbrMuGroup; }

    public DbsField getCwf_Master_Index_Cntrct_Nbr() { return cwf_Master_Index_Cntrct_Nbr; }

    public DbsField getCwf_Master_Index_Extrnl_Pend_Rcv_Dte() { return cwf_Master_Index_Extrnl_Pend_Rcv_Dte; }

    public DbsField getCwf_Master_Index_Work_List_Ind() { return cwf_Master_Index_Work_List_Ind; }

    public DbsField getCwf_Master_Index_Status_Freeze_Ind() { return cwf_Master_Index_Status_Freeze_Ind; }

    public DbsField getCwf_Master_Index_Elctrnc_Fldr_Ind() { return cwf_Master_Index_Elctrnc_Fldr_Ind; }

    public DbsField getCwf_Master_Index_Log_Rqstr_Cde() { return cwf_Master_Index_Log_Rqstr_Cde; }

    public DbsField getCwf_Master_Index_Unit_Clock_Start_Dte_Tme() { return cwf_Master_Index_Unit_Clock_Start_Dte_Tme; }

    public DbsField getCwf_Master_Index_Unit_Clock_End_Dte_Tme() { return cwf_Master_Index_Unit_Clock_End_Dte_Tme; }

    public DbsField getCwf_Master_Index_Intrnl_Pnd_Days() { return cwf_Master_Index_Intrnl_Pnd_Days; }

    public DbsField getCwf_Master_Index_Crprte_Clock_End_Dte_Tme() { return cwf_Master_Index_Crprte_Clock_End_Dte_Tme; }

    public DbsField getPnd_Comp_Date() { return pnd_Comp_Date; }

    public DbsField getPnd_Env() { return pnd_Env; }

    public DbsField getPnd_Base_Date() { return pnd_Base_Date; }

    public DbsField getPnd_Rep_Break_Ind() { return pnd_Rep_Break_Ind; }

    public DbsField getPnd_Save_Action() { return pnd_Save_Action; }

    public DbsField getPnd_Wpid() { return pnd_Wpid; }

    public DbsField getPnd_Wpid_Action() { return pnd_Wpid_Action; }

    public DbsField getPnd_Wpid_Count() { return pnd_Wpid_Count; }

    public DbsField getPnd_Wpid_Sum() { return pnd_Wpid_Sum; }

    public DbsField getPnd_Wpid_Ave() { return pnd_Wpid_Ave; }

    public DbsField getPnd_Wpid_Sname() { return pnd_Wpid_Sname; }

    public DbsField getPnd_Wpid_Text() { return pnd_Wpid_Text; }

    public DbsField getPnd_Wpid_Ave_Text() { return pnd_Wpid_Ave_Text; }

    public DbsField getPnd_Sub_Count() { return pnd_Sub_Count; }

    public DbsField getPnd_Total_Count_3() { return pnd_Total_Count_3; }

    public DbsField getPnd_Total_Count_4() { return pnd_Total_Count_4; }

    public DbsField getPnd_Total_Count_5() { return pnd_Total_Count_5; }

    public DbsField getPnd_Total_Count_10() { return pnd_Total_Count_10; }

    public DbsField getPnd_Total_Count_Over() { return pnd_Total_Count_Over; }

    public DbsField getPnd_Total_Count() { return pnd_Total_Count; }

    public DbsField getPnd_Ctot_Count_3() { return pnd_Ctot_Count_3; }

    public DbsField getPnd_Ctot_Count_4() { return pnd_Ctot_Count_4; }

    public DbsField getPnd_Ctot_Count_5() { return pnd_Ctot_Count_5; }

    public DbsField getPnd_Ctot_Count_10() { return pnd_Ctot_Count_10; }

    public DbsField getPnd_Ctot_Count_Over() { return pnd_Ctot_Count_Over; }

    public DbsField getPnd_Ctot_Count() { return pnd_Ctot_Count; }

    public DbsField getPnd_Ptotal_Count_3() { return pnd_Ptotal_Count_3; }

    public DbsField getPnd_Ptotal_Count_4() { return pnd_Ptotal_Count_4; }

    public DbsField getPnd_Ptotal_Count_5() { return pnd_Ptotal_Count_5; }

    public DbsField getPnd_Ptotal_Count_10() { return pnd_Ptotal_Count_10; }

    public DbsField getPnd_Ptotal_Count_Over() { return pnd_Ptotal_Count_Over; }

    public DbsField getPnd_Rep_Ctr() { return pnd_Rep_Ctr; }

    public DbsGroup getPnd_Array_Ctrs() { return pnd_Array_Ctrs; }

    public DbsField getPnd_Array_Ctrs_Pnd_Booklet_Ctr() { return pnd_Array_Ctrs_Pnd_Booklet_Ctr; }

    public DbsField getPnd_Array_Ctrs_Pnd_Forms_Ctr() { return pnd_Array_Ctrs_Pnd_Forms_Ctr; }

    public DbsField getPnd_Array_Ctrs_Pnd_Inquire_Ctr() { return pnd_Array_Ctrs_Pnd_Inquire_Ctr; }

    public DbsField getPnd_Array_Ctrs_Pnd_Research_Ctr() { return pnd_Array_Ctrs_Pnd_Research_Ctr; }

    public DbsField getPnd_Array_Ctrs_Pnd_Trans_Ctr() { return pnd_Array_Ctrs_Pnd_Trans_Ctr; }

    public DbsField getPnd_Array_Ctrs_Pnd_Complaint_Ctr() { return pnd_Array_Ctrs_Pnd_Complaint_Ctr; }

    public DbsField getPnd_Array_Ctrs_Pnd_Others_Ctr() { return pnd_Array_Ctrs_Pnd_Others_Ctr; }

    public DbsGroup getPnd_Array_Cnts() { return pnd_Array_Cnts; }

    public DbsField getPnd_Array_Cnts_Pnd_Booklet_Cnt() { return pnd_Array_Cnts_Pnd_Booklet_Cnt; }

    public DbsField getPnd_Array_Cnts_Pnd_Forms_Cnt() { return pnd_Array_Cnts_Pnd_Forms_Cnt; }

    public DbsField getPnd_Array_Cnts_Pnd_Inquire_Cnt() { return pnd_Array_Cnts_Pnd_Inquire_Cnt; }

    public DbsField getPnd_Array_Cnts_Pnd_Research_Cnt() { return pnd_Array_Cnts_Pnd_Research_Cnt; }

    public DbsField getPnd_Array_Cnts_Pnd_Trans_Cnt() { return pnd_Array_Cnts_Pnd_Trans_Cnt; }

    public DbsField getPnd_Array_Cnts_Pnd_Complaint_Cnt() { return pnd_Array_Cnts_Pnd_Complaint_Cnt; }

    public DbsField getPnd_Array_Cnts_Pnd_Others_Cnt() { return pnd_Array_Cnts_Pnd_Others_Cnt; }

    public DbsGroup getPnd_Percent_Ctrs() { return pnd_Percent_Ctrs; }

    public DbsField getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr() { return pnd_Percent_Ctrs_Pnd_Pbooklet_Ctr; }

    public DbsField getPnd_Percent_Ctrs_Pnd_Pforms_Ctr() { return pnd_Percent_Ctrs_Pnd_Pforms_Ctr; }

    public DbsField getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr() { return pnd_Percent_Ctrs_Pnd_Pinquire_Ctr; }

    public DbsField getPnd_Percent_Ctrs_Pnd_Presearch_Ctr() { return pnd_Percent_Ctrs_Pnd_Presearch_Ctr; }

    public DbsField getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr() { return pnd_Percent_Ctrs_Pnd_Ptrans_Ctr; }

    public DbsField getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr() { return pnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr; }

    public DbsField getPnd_Percent_Ctrs_Pnd_Pothers_Ctr() { return pnd_Percent_Ctrs_Pnd_Pothers_Ctr; }

    public DbsGroup getPnd_Percent_Cnts() { return pnd_Percent_Cnts; }

    public DbsField getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt() { return pnd_Percent_Cnts_Pnd_Pbooklet_Cnt; }

    public DbsField getPnd_Percent_Cnts_Pnd_Pforms_Cnt() { return pnd_Percent_Cnts_Pnd_Pforms_Cnt; }

    public DbsField getPnd_Percent_Cnts_Pnd_Pinquire_Cnt() { return pnd_Percent_Cnts_Pnd_Pinquire_Cnt; }

    public DbsField getPnd_Percent_Cnts_Pnd_Presearch_Cnt() { return pnd_Percent_Cnts_Pnd_Presearch_Cnt; }

    public DbsField getPnd_Percent_Cnts_Pnd_Ptrans_Cnt() { return pnd_Percent_Cnts_Pnd_Ptrans_Cnt; }

    public DbsField getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt() { return pnd_Percent_Cnts_Pnd_Pcomplaint_Cnt; }

    public DbsField getPnd_Percent_Cnts_Pnd_Pothers_Cnt() { return pnd_Percent_Cnts_Pnd_Pothers_Cnt; }

    public DbsGroup getPnd_Total_Ctrs() { return pnd_Total_Ctrs; }

    public DbsField getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr() { return pnd_Total_Ctrs_Pnd_Tbooklet_Ctr; }

    public DbsField getPnd_Total_Ctrs_Pnd_Tforms_Ctr() { return pnd_Total_Ctrs_Pnd_Tforms_Ctr; }

    public DbsField getPnd_Total_Ctrs_Pnd_Tinquire_Ctr() { return pnd_Total_Ctrs_Pnd_Tinquire_Ctr; }

    public DbsField getPnd_Total_Ctrs_Pnd_Tresearch_Ctr() { return pnd_Total_Ctrs_Pnd_Tresearch_Ctr; }

    public DbsField getPnd_Total_Ctrs_Pnd_Ttrans_Ctr() { return pnd_Total_Ctrs_Pnd_Ttrans_Ctr; }

    public DbsField getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr() { return pnd_Total_Ctrs_Pnd_Tcomplaint_Ctr; }

    public DbsField getPnd_Total_Ctrs_Pnd_Tothers_Ctr() { return pnd_Total_Ctrs_Pnd_Tothers_Ctr; }

    public DbsField getPnd_Todays_Time() { return pnd_Todays_Time; }

    public DbsField getPnd_Status_Sname() { return pnd_Status_Sname; }

    public DbsField getPnd_Return_Doc_Txt() { return pnd_Return_Doc_Txt; }

    public DbsField getPnd_Return_Rcvd_Txt() { return pnd_Return_Rcvd_Txt; }

    public DbsField getPnd_Work_Date_D() { return pnd_Work_Date_D; }

    public DbsField getPnd_Rep_Unit_Cde() { return pnd_Rep_Unit_Cde; }

    public DbsGroup getPnd_Rep_Unit_CdeRedef8() { return pnd_Rep_Unit_CdeRedef8; }

    public DbsField getPnd_Rep_Unit_Cde_Pnd_Rep_Unit_Id() { return pnd_Rep_Unit_Cde_Pnd_Rep_Unit_Id; }

    public DbsField getPnd_Unit_Name() { return pnd_Unit_Name; }

    public DbsField getPnd_Parm_Unit() { return pnd_Parm_Unit; }

    public DbsField getPnd_Start_Date() { return pnd_Start_Date; }

    public DbsField getPnd_Start_Dte_Tme() { return pnd_Start_Dte_Tme; }

    public DbsField getPnd_End_Dte_Tme() { return pnd_End_Dte_Tme; }

    public DbsField getPnd_Work_St_Date() { return pnd_Work_St_Date; }

    public DbsField getPnd_Work_End_Date() { return pnd_Work_End_Date; }

    public DbsField getPnd_Work_Date_Time() { return pnd_Work_Date_Time; }

    public DbsGroup getPnd_Work_Date_TimeRedef9() { return pnd_Work_Date_TimeRedef9; }

    public DbsField getPnd_Work_Date_Time_Pnd_Work_Date() { return pnd_Work_Date_Time_Pnd_Work_Date; }

    public DbsField getPnd_Work_Date_Time_Pnd_Work_Time() { return pnd_Work_Date_Time_Pnd_Work_Time; }

    public DbsField getPnd_Old_Moc() { return pnd_Old_Moc; }

    public DbsField getPnd_Save_Moc() { return pnd_Save_Moc; }

    public DbsField getPnd_Save_Moc_1() { return pnd_Save_Moc_1; }

    public DbsField getPnd_Save_Unit() { return pnd_Save_Unit; }

    public DbsField getPnd_First_Unit() { return pnd_First_Unit; }

    public DbsField getPnd_New_Unit() { return pnd_New_Unit; }

    public DbsField getPnd_Page_Num() { return pnd_Page_Num; }

    public DbsField getPnd_Rep_Unit_Nme() { return pnd_Rep_Unit_Nme; }

    public DbsField getPnd_Start_Dte_Tme_A() { return pnd_Start_Dte_Tme_A; }

    public DbsGroup getPnd_Start_Dte_Tme_ARedef10() { return pnd_Start_Dte_Tme_ARedef10; }

    public DbsField getPnd_Start_Dte_Tme_A_Pnd_Start_Dte_Tme_N() { return pnd_Start_Dte_Tme_A_Pnd_Start_Dte_Tme_N; }

    public DbsField getPnd_End_Dte_Tme_A() { return pnd_End_Dte_Tme_A; }

    public DbsGroup getPnd_End_Dte_Tme_ARedef11() { return pnd_End_Dte_Tme_ARedef11; }

    public DbsField getPnd_End_Dte_Tme_A_Pnd_End_Dte_Tme_N() { return pnd_End_Dte_Tme_A_Pnd_End_Dte_Tme_N; }

    public DbsField getPnd_Sub() { return pnd_Sub; }

    public DbsField getPnd_Start_Key() { return pnd_Start_Key; }

    public DbsGroup getPnd_Start_KeyRedef12() { return pnd_Start_KeyRedef12; }

    public DbsField getPnd_Start_Key_Crprte_Status_Ind() { return pnd_Start_Key_Crprte_Status_Ind; }

    public DbsField getPnd_Start_Key_Last_Chnge_Invrt_Dte_Tme() { return pnd_Start_Key_Last_Chnge_Invrt_Dte_Tme; }

    public DbsField getPnd_End_Key() { return pnd_End_Key; }

    public DbsGroup getPnd_End_KeyRedef13() { return pnd_End_KeyRedef13; }

    public DbsField getPnd_End_Key_Crprte_Status_Ind() { return pnd_End_Key_Crprte_Status_Ind; }

    public DbsField getPnd_End_Key_Last_Chnge_Invrt_Dte_Tme() { return pnd_End_Key_Last_Chnge_Invrt_Dte_Tme; }

    public DbsField getPnd_Timx() { return pnd_Timx; }

    public DbsField getPnd_Participant_Closed_Date_Time() { return pnd_Participant_Closed_Date_Time; }

    public DbsField getPnd_Receive_Date() { return pnd_Receive_Date; }

    public DbsField getPnd_Report_Date() { return pnd_Report_Date; }

    public DbsField getPnd_Status_Updte_Dte() { return pnd_Status_Updte_Dte; }

    public DbsField getPnd_Grand_Title() { return pnd_Grand_Title; }

    public DbsField getPnd_Grand_Title2() { return pnd_Grand_Title2; }

    public DbsField getPnd_Generate_Combined_Totals() { return pnd_Generate_Combined_Totals; }

    public DbsField getPnd_Mid_Total() { return pnd_Mid_Total; }

    public DbsField getPnd_Compare_Unit() { return pnd_Compare_Unit; }

    public DbsField getPnd_Comp_Unit() { return pnd_Comp_Unit; }

    public DbsGroup getPnd_Gtot_Registers() { return pnd_Gtot_Registers; }

    public DbsField getPnd_Gtot_Registers_Pnd_Ave_Total() { return pnd_Gtot_Registers_Pnd_Ave_Total; }

    public DbsField getPnd_Gtot_Registers_Pnd_Ave_Total_Num() { return pnd_Gtot_Registers_Pnd_Ave_Total_Num; }

    public DbsField getPnd_Gtot_Registers_Pnd_Total_Moc() { return pnd_Gtot_Registers_Pnd_Total_Moc; }

    public DbsField getPnd_Gtot_Registers_Pnd_Total_Moc_3() { return pnd_Gtot_Registers_Pnd_Total_Moc_3; }

    public DbsField getPnd_Gtot_Registers_Pnd_Total_Moc_4() { return pnd_Gtot_Registers_Pnd_Total_Moc_4; }

    public DbsField getPnd_Gtot_Registers_Pnd_Total_Moc_5() { return pnd_Gtot_Registers_Pnd_Total_Moc_5; }

    public DbsField getPnd_Gtot_Registers_Pnd_Total_Moc_10() { return pnd_Gtot_Registers_Pnd_Total_Moc_10; }

    public DbsField getPnd_Gtot_Registers_Pnd_Total_Moc_Over() { return pnd_Gtot_Registers_Pnd_Total_Moc_Over; }

    public DbsGroup getPnd_Combined_Totals() { return pnd_Combined_Totals; }

    public DbsField getPnd_Combined_Totals_Pnd_Cave_Total() { return pnd_Combined_Totals_Pnd_Cave_Total; }

    public DbsField getPnd_Combined_Totals_Pnd_Cave_Total_Num() { return pnd_Combined_Totals_Pnd_Cave_Total_Num; }

    public DbsField getPnd_Combined_Totals_Pnd_Ctotal_Moc() { return pnd_Combined_Totals_Pnd_Ctotal_Moc; }

    public DbsField getPnd_Combined_Totals_Pnd_Ctotal_Moc_3() { return pnd_Combined_Totals_Pnd_Ctotal_Moc_3; }

    public DbsField getPnd_Combined_Totals_Pnd_Ctotal_Moc_4() { return pnd_Combined_Totals_Pnd_Ctotal_Moc_4; }

    public DbsField getPnd_Combined_Totals_Pnd_Ctotal_Moc_5() { return pnd_Combined_Totals_Pnd_Ctotal_Moc_5; }

    public DbsField getPnd_Combined_Totals_Pnd_Ctotal_Moc_10() { return pnd_Combined_Totals_Pnd_Ctotal_Moc_10; }

    public DbsField getPnd_Combined_Totals_Pnd_Ctotal_Moc_Over() { return pnd_Combined_Totals_Pnd_Ctotal_Moc_Over; }

    public DbsField getPnd_Holiday() { return pnd_Holiday; }

    public DbsField getPnd_Datx() { return pnd_Datx; }

    public DbsField getPnd_Combine_Unit() { return pnd_Combine_Unit; }

    public DbsField getPnd_Exclude_Wpids() { return pnd_Exclude_Wpids; }

    public DbsField getPnd_Valid_Units() { return pnd_Valid_Units; }

    public DbsField getPnd_Combined_Unit() { return pnd_Combined_Unit; }

    public DbsField getPnd_Combined_Unit_Hdr() { return pnd_Combined_Unit_Hdr; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Work_Record = newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Tbl_Admin_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Admin_Unit", "TBL-ADMIN-UNIT", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Moc = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Moc", "TBL-MOC", FieldType.STRING, 1);
        pnd_Work_Record_Tbl_Racf = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Racf", "TBL-RACF", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Tiaa_Bsnss_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 
            5);
        pnd_Work_Record_Tbl_Business_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.DECIMAL, 
            5,1);
        pnd_Work_Record_Tbl_Complete_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Complete_Days", "TBL-COMPLETE-DAYS", FieldType.DECIMAL, 
            19,1);
        pnd_Work_Record_Tbl_Wpid_Act = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 1);
        pnd_Work_Record_Tbl_Wpid = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);
        pnd_Work_Record_Tbl_WpidRedef1 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Tbl_WpidRedef1", "Redefines", pnd_Work_Record_Tbl_Wpid);
        pnd_Work_Record_Tbl_Wpid_Action = pnd_Work_Record_Tbl_WpidRedef1.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Work_Record_Tbl_Status_Key = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Key", "TBL-STATUS-KEY", FieldType.STRING, 13);
        pnd_Work_Record_Tbl_Status_KeyRedef2 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Tbl_Status_KeyRedef2", "Redefines", pnd_Work_Record_Tbl_Status_Key);
        pnd_Work_Record_Tbl_Last_Chnge_Unit = pnd_Work_Record_Tbl_Status_KeyRedef2.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chnge_Unit", "TBL-LAST-CHNGE-UNIT", 
            FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Status_Cde = pnd_Work_Record_Tbl_Status_KeyRedef2.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde", "TBL-STATUS-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record_Tbl_Close_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Close_Unit", "TBL-CLOSE-UNIT", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Close_UnitRedef3 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Tbl_Close_UnitRedef3", "Redefines", pnd_Work_Record_Tbl_Close_Unit);
        pnd_Work_Record_Tbl_Close_Unit_Id = pnd_Work_Record_Tbl_Close_UnitRedef3.newFieldInGroup("pnd_Work_Record_Tbl_Close_Unit_Id", "TBL-CLOSE-UNIT-ID", 
            FieldType.STRING, 5);
        pnd_Work_Record_Tbl_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Pin", "TBL-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Tbl_Contracts = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Contracts", "TBL-CONTRACTS", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Dte_Tme", "TBL-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_Record_Tbl_Tiaa_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Dte", "TBL-TIAA-DTE", FieldType.DATE);
        pnd_Work_Record_Tbl_Status_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Dte", "TBL-STATUS-DTE", FieldType.TIME);
        pnd_Work_Record_Return_Doc_Rec_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Return_Rcvd_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Tbl_Last_Chge_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chge_Dte", "TBL-LAST-CHGE-DTE", FieldType.NUMERIC, 
            15);
        pnd_Work_Record_Pnd_Partic_Sname = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);
        pnd_Work_Record_Pnd_Partic_SnameRedef4 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Pnd_Partic_SnameRedef4", "Redefines", pnd_Work_Record_Pnd_Partic_Sname);
        pnd_Work_Record_Pnd_Fldr_Prefix = pnd_Work_Record_Pnd_Partic_SnameRedef4.newFieldInGroup("pnd_Work_Record_Pnd_Fldr_Prefix", "#FLDR-PREFIX", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_Physcl_Fldr_Id_Nbr = pnd_Work_Record_Pnd_Partic_SnameRedef4.newFieldInGroup("pnd_Work_Record_Pnd_Physcl_Fldr_Id_Nbr", "#PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_Pnd_Rqst_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_Work_Record_Pnd_Last_Chnge_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Last_Chnge_Dte_Tme", "#LAST-CHNGE-DTE-TME", FieldType.STRING, 
            15);

        vw_cwf_Master_Index = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index", "CWF-MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Index_Pin_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Index_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_Rqst_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Index_Rqst_Orgn_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_Work_Prcss_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_Work_Prcss_IdRedef5 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index_Work_Prcss_IdRedef5", "Redefines", cwf_Master_Index_Work_Prcss_Id);
        cwf_Master_Index_Work_Actn_Rqstd_Cde = cwf_Master_Index_Work_Prcss_IdRedef5.newFieldInGroup("cwf_Master_Index_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Master_Index_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Master_Index_Unit_CdeRedef6 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index_Unit_CdeRedef6", "Redefines", cwf_Master_Index_Unit_Cde);
        cwf_Master_Index_Unit_Id_Cde = cwf_Master_Index_Unit_CdeRedef6.newFieldInGroup("cwf_Master_Index_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 
            5);
        cwf_Master_Index_Empl_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        cwf_Master_Index_Empl_Oprtr_CdeRedef7 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index_Empl_Oprtr_CdeRedef7", "Redefines", 
            cwf_Master_Index_Empl_Oprtr_Cde);
        cwf_Master_Index_Empl_Racf_Id = cwf_Master_Index_Empl_Oprtr_CdeRedef7.newFieldInGroup("cwf_Master_Index_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_Last_Chnge_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_Last_Chnge_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_Admin_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_Admin_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_Admin_Status_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_Last_Updte_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        cwf_Master_Index_Last_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Index_Tiaa_Rcvd_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_Effctve_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_Trnsctn_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Trnsctn_Dte", "TRNSCTN-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRNSCTN_DTE");
        cwf_Master_Index_Trnsctn_Dte.setDdmHeader("TRANSACTION/DATE");
        cwf_Master_Index_Trans_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Trans_Dte", "TRANS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cwf_Master_Index_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_Physcl_Fldr_Id_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Index_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Index_Mj_Chrge_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Chrge_Oprtr_Cde", "MJ-CHRGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_CHRGE_OPRTR_CDE");
        cwf_Master_Index_Mj_Chrge_Oprtr_Cde.setDdmHeader("CHARGEOUT/OPERATOR");
        cwf_Master_Index_Mstr_Indx_Actn_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mstr_Indx_Actn_Cde", "MSTR-INDX-ACTN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "MSTR_INDX_ACTN_CDE");
        cwf_Master_Index_Mstr_Indx_Actn_Cde.setDdmHeader("CHARGEOUT/ACTION CODE");
        cwf_Master_Index_Mj_Pull_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme", "MJ-EMRGNCY-RQST-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_DTE_TME");
        cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme.setDdmHeader("EMERGENCY REQ/DATE-TIME");
        cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde", "MJ-EMRGNCY-RQST-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_OPRTR_CDE");
        cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde.setDdmHeader("EMERGENCY/REQUESTOR");
        cwf_Master_Index_Print_Q_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Print_Q_Ind", "PRINT-Q-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PRINT_Q_IND");
        cwf_Master_Index_Print_Q_Ind.setDdmHeader("MJ PRINT/QUEUE");
        cwf_Master_Index_Print_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Print_Dte_Tme", "PRINT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "PRINT_DTE_TME");
        cwf_Master_Index_Print_Dte_Tme.setDdmHeader("MJ PRINT/DATE-TIME");
        cwf_Master_Index_Print_Batch_Sqnce_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Print_Batch_Sqnce_Nbr", "PRINT-BATCH-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRINT_BATCH_SQNCE_NBR");
        cwf_Master_Index_Print_Batch_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Print_Batch_Ind", "PRINT-BATCH-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRINT_BATCH_IND");
        cwf_Master_Index_Printer_Id_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Printer_Id_Cde", "PRINTER-ID-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "PRINTER_ID_CDE");
        cwf_Master_Index_Rescan_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RESCAN_IND");
        cwf_Master_Index_Rescan_Ind.setDdmHeader("RE-SCAN/INDICATOR");
        cwf_Master_Index_Dup_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Dup_Ind", "DUP-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DUP_IND");
        cwf_Master_Index_Dup_Ind.setDdmHeader("DUP/IND");
        cwf_Master_Index_Cmplnt_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CMPLNT_IND");
        cwf_Master_Index_Cntrct_NbrMuGroup = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index_Cntrct_NbrMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_Cntrct_Nbr = cwf_Master_Index_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 
            8, new DbsArrayController(1,1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_Work_List_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "WORK_LIST_IND");
        cwf_Master_Index_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        cwf_Master_Index_Status_Freeze_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Index_Elctrnc_Fldr_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_Index_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        cwf_Master_Index_Log_Rqstr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Log_Rqstr_Cde", "LOG-RQSTR-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "LOG_RQSTR_CDE");
        cwf_Master_Index_Log_Rqstr_Cde.setDdmHeader("REQUESTOR/TYPE");
        cwf_Master_Index_Unit_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Clock_Start_Dte_Tme", "UNIT-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Index_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Index_Unit_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Clock_End_Dte_Tme", "UNIT-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Index_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_Days = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_Days", "INTRNL-PND-DAYS", FieldType.PACKED_DECIMAL, 
            5, RepeatingFieldStrategy.None, "INTRNL_PND_DAYS");
        cwf_Master_Index_Intrnl_Pnd_Days.setDdmHeader("INTERNAL PEND DAYS");
        cwf_Master_Index_Crprte_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Index_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");

        pnd_Comp_Date = newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);

        pnd_Env = newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);

        pnd_Base_Date = newFieldInRecord("pnd_Base_Date", "#BASE-DATE", FieldType.DATE);

        pnd_Rep_Break_Ind = newFieldInRecord("pnd_Rep_Break_Ind", "#REP-BREAK-IND", FieldType.STRING, 1);

        pnd_Save_Action = newFieldInRecord("pnd_Save_Action", "#SAVE-ACTION", FieldType.STRING, 1);

        pnd_Wpid = newFieldInRecord("pnd_Wpid", "#WPID", FieldType.STRING, 6);

        pnd_Wpid_Action = newFieldInRecord("pnd_Wpid_Action", "#WPID-ACTION", FieldType.STRING, 1);

        pnd_Wpid_Count = newFieldInRecord("pnd_Wpid_Count", "#WPID-COUNT", FieldType.NUMERIC, 7);

        pnd_Wpid_Sum = newFieldInRecord("pnd_Wpid_Sum", "#WPID-SUM", FieldType.NUMERIC, 18);

        pnd_Wpid_Ave = newFieldInRecord("pnd_Wpid_Ave", "#WPID-AVE", FieldType.DECIMAL, 10,3);

        pnd_Wpid_Sname = newFieldInRecord("pnd_Wpid_Sname", "#WPID-SNAME", FieldType.STRING, 15);

        pnd_Wpid_Text = newFieldInRecord("pnd_Wpid_Text", "#WPID-TEXT", FieldType.STRING, 69);

        pnd_Wpid_Ave_Text = newFieldInRecord("pnd_Wpid_Ave_Text", "#WPID-AVE-TEXT", FieldType.STRING, 69);

        pnd_Sub_Count = newFieldInRecord("pnd_Sub_Count", "#SUB-COUNT", FieldType.NUMERIC, 7);

        pnd_Total_Count_3 = newFieldInRecord("pnd_Total_Count_3", "#TOTAL-COUNT-3", FieldType.NUMERIC, 7);

        pnd_Total_Count_4 = newFieldInRecord("pnd_Total_Count_4", "#TOTAL-COUNT-4", FieldType.NUMERIC, 7);

        pnd_Total_Count_5 = newFieldInRecord("pnd_Total_Count_5", "#TOTAL-COUNT-5", FieldType.NUMERIC, 7);

        pnd_Total_Count_10 = newFieldInRecord("pnd_Total_Count_10", "#TOTAL-COUNT-10", FieldType.NUMERIC, 7);

        pnd_Total_Count_Over = newFieldInRecord("pnd_Total_Count_Over", "#TOTAL-COUNT-OVER", FieldType.NUMERIC, 7);

        pnd_Total_Count = newFieldInRecord("pnd_Total_Count", "#TOTAL-COUNT", FieldType.NUMERIC, 7);

        pnd_Ctot_Count_3 = newFieldInRecord("pnd_Ctot_Count_3", "#CTOT-COUNT-3", FieldType.NUMERIC, 7);

        pnd_Ctot_Count_4 = newFieldInRecord("pnd_Ctot_Count_4", "#CTOT-COUNT-4", FieldType.NUMERIC, 7);

        pnd_Ctot_Count_5 = newFieldInRecord("pnd_Ctot_Count_5", "#CTOT-COUNT-5", FieldType.NUMERIC, 7);

        pnd_Ctot_Count_10 = newFieldInRecord("pnd_Ctot_Count_10", "#CTOT-COUNT-10", FieldType.NUMERIC, 7);

        pnd_Ctot_Count_Over = newFieldInRecord("pnd_Ctot_Count_Over", "#CTOT-COUNT-OVER", FieldType.NUMERIC, 7);

        pnd_Ctot_Count = newFieldInRecord("pnd_Ctot_Count", "#CTOT-COUNT", FieldType.NUMERIC, 7);

        pnd_Ptotal_Count_3 = newFieldInRecord("pnd_Ptotal_Count_3", "#PTOTAL-COUNT-3", FieldType.FLOAT, 8);

        pnd_Ptotal_Count_4 = newFieldInRecord("pnd_Ptotal_Count_4", "#PTOTAL-COUNT-4", FieldType.FLOAT, 8);

        pnd_Ptotal_Count_5 = newFieldInRecord("pnd_Ptotal_Count_5", "#PTOTAL-COUNT-5", FieldType.FLOAT, 8);

        pnd_Ptotal_Count_10 = newFieldInRecord("pnd_Ptotal_Count_10", "#PTOTAL-COUNT-10", FieldType.FLOAT, 8);

        pnd_Ptotal_Count_Over = newFieldInRecord("pnd_Ptotal_Count_Over", "#PTOTAL-COUNT-OVER", FieldType.FLOAT, 8);

        pnd_Rep_Ctr = newFieldInRecord("pnd_Rep_Ctr", "#REP-CTR", FieldType.NUMERIC, 7);

        pnd_Array_Ctrs = newGroupArrayInRecord("pnd_Array_Ctrs", "#ARRAY-CTRS", new DbsArrayController(1,11));
        pnd_Array_Ctrs_Pnd_Booklet_Ctr = pnd_Array_Ctrs.newFieldInGroup("pnd_Array_Ctrs_Pnd_Booklet_Ctr", "#BOOKLET-CTR", FieldType.NUMERIC, 7);
        pnd_Array_Ctrs_Pnd_Forms_Ctr = pnd_Array_Ctrs.newFieldInGroup("pnd_Array_Ctrs_Pnd_Forms_Ctr", "#FORMS-CTR", FieldType.NUMERIC, 7);
        pnd_Array_Ctrs_Pnd_Inquire_Ctr = pnd_Array_Ctrs.newFieldInGroup("pnd_Array_Ctrs_Pnd_Inquire_Ctr", "#INQUIRE-CTR", FieldType.NUMERIC, 7);
        pnd_Array_Ctrs_Pnd_Research_Ctr = pnd_Array_Ctrs.newFieldInGroup("pnd_Array_Ctrs_Pnd_Research_Ctr", "#RESEARCH-CTR", FieldType.NUMERIC, 7);
        pnd_Array_Ctrs_Pnd_Trans_Ctr = pnd_Array_Ctrs.newFieldInGroup("pnd_Array_Ctrs_Pnd_Trans_Ctr", "#TRANS-CTR", FieldType.NUMERIC, 7);
        pnd_Array_Ctrs_Pnd_Complaint_Ctr = pnd_Array_Ctrs.newFieldInGroup("pnd_Array_Ctrs_Pnd_Complaint_Ctr", "#COMPLAINT-CTR", FieldType.NUMERIC, 7);
        pnd_Array_Ctrs_Pnd_Others_Ctr = pnd_Array_Ctrs.newFieldInGroup("pnd_Array_Ctrs_Pnd_Others_Ctr", "#OTHERS-CTR", FieldType.NUMERIC, 7);

        pnd_Array_Cnts = newGroupArrayInRecord("pnd_Array_Cnts", "#ARRAY-CNTS", new DbsArrayController(1,11));
        pnd_Array_Cnts_Pnd_Booklet_Cnt = pnd_Array_Cnts.newFieldInGroup("pnd_Array_Cnts_Pnd_Booklet_Cnt", "#BOOKLET-CNT", FieldType.NUMERIC, 7);
        pnd_Array_Cnts_Pnd_Forms_Cnt = pnd_Array_Cnts.newFieldInGroup("pnd_Array_Cnts_Pnd_Forms_Cnt", "#FORMS-CNT", FieldType.NUMERIC, 7);
        pnd_Array_Cnts_Pnd_Inquire_Cnt = pnd_Array_Cnts.newFieldInGroup("pnd_Array_Cnts_Pnd_Inquire_Cnt", "#INQUIRE-CNT", FieldType.NUMERIC, 7);
        pnd_Array_Cnts_Pnd_Research_Cnt = pnd_Array_Cnts.newFieldInGroup("pnd_Array_Cnts_Pnd_Research_Cnt", "#RESEARCH-CNT", FieldType.NUMERIC, 7);
        pnd_Array_Cnts_Pnd_Trans_Cnt = pnd_Array_Cnts.newFieldInGroup("pnd_Array_Cnts_Pnd_Trans_Cnt", "#TRANS-CNT", FieldType.NUMERIC, 7);
        pnd_Array_Cnts_Pnd_Complaint_Cnt = pnd_Array_Cnts.newFieldInGroup("pnd_Array_Cnts_Pnd_Complaint_Cnt", "#COMPLAINT-CNT", FieldType.NUMERIC, 7);
        pnd_Array_Cnts_Pnd_Others_Cnt = pnd_Array_Cnts.newFieldInGroup("pnd_Array_Cnts_Pnd_Others_Cnt", "#OTHERS-CNT", FieldType.NUMERIC, 7);

        pnd_Percent_Ctrs = newGroupArrayInRecord("pnd_Percent_Ctrs", "#PERCENT-CTRS", new DbsArrayController(1,11));
        pnd_Percent_Ctrs_Pnd_Pbooklet_Ctr = pnd_Percent_Ctrs.newFieldInGroup("pnd_Percent_Ctrs_Pnd_Pbooklet_Ctr", "#PBOOKLET-CTR", FieldType.FLOAT, 8);
        pnd_Percent_Ctrs_Pnd_Pforms_Ctr = pnd_Percent_Ctrs.newFieldInGroup("pnd_Percent_Ctrs_Pnd_Pforms_Ctr", "#PFORMS-CTR", FieldType.FLOAT, 8);
        pnd_Percent_Ctrs_Pnd_Pinquire_Ctr = pnd_Percent_Ctrs.newFieldInGroup("pnd_Percent_Ctrs_Pnd_Pinquire_Ctr", "#PINQUIRE-CTR", FieldType.FLOAT, 8);
        pnd_Percent_Ctrs_Pnd_Presearch_Ctr = pnd_Percent_Ctrs.newFieldInGroup("pnd_Percent_Ctrs_Pnd_Presearch_Ctr", "#PRESEARCH-CTR", FieldType.FLOAT, 
            8);
        pnd_Percent_Ctrs_Pnd_Ptrans_Ctr = pnd_Percent_Ctrs.newFieldInGroup("pnd_Percent_Ctrs_Pnd_Ptrans_Ctr", "#PTRANS-CTR", FieldType.FLOAT, 8);
        pnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr = pnd_Percent_Ctrs.newFieldInGroup("pnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr", "#PCOMPLAINT-CTR", FieldType.FLOAT, 
            8);
        pnd_Percent_Ctrs_Pnd_Pothers_Ctr = pnd_Percent_Ctrs.newFieldInGroup("pnd_Percent_Ctrs_Pnd_Pothers_Ctr", "#POTHERS-CTR", FieldType.FLOAT, 8);

        pnd_Percent_Cnts = newGroupArrayInRecord("pnd_Percent_Cnts", "#PERCENT-CNTS", new DbsArrayController(1,11));
        pnd_Percent_Cnts_Pnd_Pbooklet_Cnt = pnd_Percent_Cnts.newFieldInGroup("pnd_Percent_Cnts_Pnd_Pbooklet_Cnt", "#PBOOKLET-CNT", FieldType.FLOAT, 8);
        pnd_Percent_Cnts_Pnd_Pforms_Cnt = pnd_Percent_Cnts.newFieldInGroup("pnd_Percent_Cnts_Pnd_Pforms_Cnt", "#PFORMS-CNT", FieldType.FLOAT, 8);
        pnd_Percent_Cnts_Pnd_Pinquire_Cnt = pnd_Percent_Cnts.newFieldInGroup("pnd_Percent_Cnts_Pnd_Pinquire_Cnt", "#PINQUIRE-CNT", FieldType.FLOAT, 8);
        pnd_Percent_Cnts_Pnd_Presearch_Cnt = pnd_Percent_Cnts.newFieldInGroup("pnd_Percent_Cnts_Pnd_Presearch_Cnt", "#PRESEARCH-CNT", FieldType.FLOAT, 
            8);
        pnd_Percent_Cnts_Pnd_Ptrans_Cnt = pnd_Percent_Cnts.newFieldInGroup("pnd_Percent_Cnts_Pnd_Ptrans_Cnt", "#PTRANS-CNT", FieldType.FLOAT, 8);
        pnd_Percent_Cnts_Pnd_Pcomplaint_Cnt = pnd_Percent_Cnts.newFieldInGroup("pnd_Percent_Cnts_Pnd_Pcomplaint_Cnt", "#PCOMPLAINT-CNT", FieldType.FLOAT, 
            8);
        pnd_Percent_Cnts_Pnd_Pothers_Cnt = pnd_Percent_Cnts.newFieldInGroup("pnd_Percent_Cnts_Pnd_Pothers_Cnt", "#POTHERS-CNT", FieldType.FLOAT, 8);

        pnd_Total_Ctrs = newGroupInRecord("pnd_Total_Ctrs", "#TOTAL-CTRS");
        pnd_Total_Ctrs_Pnd_Tbooklet_Ctr = pnd_Total_Ctrs.newFieldInGroup("pnd_Total_Ctrs_Pnd_Tbooklet_Ctr", "#TBOOKLET-CTR", FieldType.NUMERIC, 7);
        pnd_Total_Ctrs_Pnd_Tforms_Ctr = pnd_Total_Ctrs.newFieldInGroup("pnd_Total_Ctrs_Pnd_Tforms_Ctr", "#TFORMS-CTR", FieldType.NUMERIC, 7);
        pnd_Total_Ctrs_Pnd_Tinquire_Ctr = pnd_Total_Ctrs.newFieldInGroup("pnd_Total_Ctrs_Pnd_Tinquire_Ctr", "#TINQUIRE-CTR", FieldType.NUMERIC, 7);
        pnd_Total_Ctrs_Pnd_Tresearch_Ctr = pnd_Total_Ctrs.newFieldInGroup("pnd_Total_Ctrs_Pnd_Tresearch_Ctr", "#TRESEARCH-CTR", FieldType.NUMERIC, 7);
        pnd_Total_Ctrs_Pnd_Ttrans_Ctr = pnd_Total_Ctrs.newFieldInGroup("pnd_Total_Ctrs_Pnd_Ttrans_Ctr", "#TTRANS-CTR", FieldType.NUMERIC, 7);
        pnd_Total_Ctrs_Pnd_Tcomplaint_Ctr = pnd_Total_Ctrs.newFieldInGroup("pnd_Total_Ctrs_Pnd_Tcomplaint_Ctr", "#TCOMPLAINT-CTR", FieldType.NUMERIC, 
            7);
        pnd_Total_Ctrs_Pnd_Tothers_Ctr = pnd_Total_Ctrs.newFieldInGroup("pnd_Total_Ctrs_Pnd_Tothers_Ctr", "#TOTHERS-CTR", FieldType.NUMERIC, 7);

        pnd_Todays_Time = newFieldInRecord("pnd_Todays_Time", "#TODAYS-TIME", FieldType.TIME);

        pnd_Status_Sname = newFieldInRecord("pnd_Status_Sname", "#STATUS-SNAME", FieldType.STRING, 25);

        pnd_Return_Doc_Txt = newFieldInRecord("pnd_Return_Doc_Txt", "#RETURN-DOC-TXT", FieldType.STRING, 8);

        pnd_Return_Rcvd_Txt = newFieldInRecord("pnd_Return_Rcvd_Txt", "#RETURN-RCVD-TXT", FieldType.STRING, 8);

        pnd_Work_Date_D = newFieldInRecord("pnd_Work_Date_D", "#WORK-DATE-D", FieldType.DATE);

        pnd_Rep_Unit_Cde = newFieldInRecord("pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);
        pnd_Rep_Unit_CdeRedef8 = newGroupInRecord("pnd_Rep_Unit_CdeRedef8", "Redefines", pnd_Rep_Unit_Cde);
        pnd_Rep_Unit_Cde_Pnd_Rep_Unit_Id = pnd_Rep_Unit_CdeRedef8.newFieldInGroup("pnd_Rep_Unit_Cde_Pnd_Rep_Unit_Id", "#REP-UNIT-ID", FieldType.STRING, 
            5);

        pnd_Unit_Name = newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);

        pnd_Parm_Unit = newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);

        pnd_Start_Date = newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Start_Dte_Tme = newFieldInRecord("pnd_Start_Dte_Tme", "#START-DTE-TME", FieldType.TIME);

        pnd_End_Dte_Tme = newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.TIME);

        pnd_Work_St_Date = newFieldInRecord("pnd_Work_St_Date", "#WORK-ST-DATE", FieldType.STRING, 8);

        pnd_Work_End_Date = newFieldInRecord("pnd_Work_End_Date", "#WORK-END-DATE", FieldType.STRING, 8);

        pnd_Work_Date_Time = newFieldInRecord("pnd_Work_Date_Time", "#WORK-DATE-TIME", FieldType.NUMERIC, 15);
        pnd_Work_Date_TimeRedef9 = newGroupInRecord("pnd_Work_Date_TimeRedef9", "Redefines", pnd_Work_Date_Time);
        pnd_Work_Date_Time_Pnd_Work_Date = pnd_Work_Date_TimeRedef9.newFieldInGroup("pnd_Work_Date_Time_Pnd_Work_Date", "#WORK-DATE", FieldType.STRING, 
            8);
        pnd_Work_Date_Time_Pnd_Work_Time = pnd_Work_Date_TimeRedef9.newFieldInGroup("pnd_Work_Date_Time_Pnd_Work_Time", "#WORK-TIME", FieldType.STRING, 
            7);

        pnd_Old_Moc = newFieldInRecord("pnd_Old_Moc", "#OLD-MOC", FieldType.STRING, 1);

        pnd_Save_Moc = newFieldInRecord("pnd_Save_Moc", "#SAVE-MOC", FieldType.STRING, 1);

        pnd_Save_Moc_1 = newFieldInRecord("pnd_Save_Moc_1", "#SAVE-MOC-1", FieldType.STRING, 1);

        pnd_Save_Unit = newFieldInRecord("pnd_Save_Unit", "#SAVE-UNIT", FieldType.STRING, 8);

        pnd_First_Unit = newFieldInRecord("pnd_First_Unit", "#FIRST-UNIT", FieldType.BOOLEAN);

        pnd_New_Unit = newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN);

        pnd_Page_Num = newFieldInRecord("pnd_Page_Num", "#PAGE-NUM", FieldType.NUMERIC, 4);

        pnd_Rep_Unit_Nme = newFieldInRecord("pnd_Rep_Unit_Nme", "#REP-UNIT-NME", FieldType.STRING, 45);

        pnd_Start_Dte_Tme_A = newFieldInRecord("pnd_Start_Dte_Tme_A", "#START-DTE-TME-A", FieldType.STRING, 15);
        pnd_Start_Dte_Tme_ARedef10 = newGroupInRecord("pnd_Start_Dte_Tme_ARedef10", "Redefines", pnd_Start_Dte_Tme_A);
        pnd_Start_Dte_Tme_A_Pnd_Start_Dte_Tme_N = pnd_Start_Dte_Tme_ARedef10.newFieldInGroup("pnd_Start_Dte_Tme_A_Pnd_Start_Dte_Tme_N", "#START-DTE-TME-N", 
            FieldType.NUMERIC, 15);

        pnd_End_Dte_Tme_A = newFieldInRecord("pnd_End_Dte_Tme_A", "#END-DTE-TME-A", FieldType.STRING, 15);
        pnd_End_Dte_Tme_ARedef11 = newGroupInRecord("pnd_End_Dte_Tme_ARedef11", "Redefines", pnd_End_Dte_Tme_A);
        pnd_End_Dte_Tme_A_Pnd_End_Dte_Tme_N = pnd_End_Dte_Tme_ARedef11.newFieldInGroup("pnd_End_Dte_Tme_A_Pnd_End_Dte_Tme_N", "#END-DTE-TME-N", FieldType.NUMERIC, 
            15);

        pnd_Sub = newFieldInRecord("pnd_Sub", "#SUB", FieldType.PACKED_DECIMAL, 5);

        pnd_Start_Key = newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.STRING, 25);
        pnd_Start_KeyRedef12 = newGroupInRecord("pnd_Start_KeyRedef12", "Redefines", pnd_Start_Key);
        pnd_Start_Key_Crprte_Status_Ind = pnd_Start_KeyRedef12.newFieldInGroup("pnd_Start_Key_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1);
        pnd_Start_Key_Last_Chnge_Invrt_Dte_Tme = pnd_Start_KeyRedef12.newFieldInGroup("pnd_Start_Key_Last_Chnge_Invrt_Dte_Tme", "LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15);

        pnd_End_Key = newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.STRING, 25);
        pnd_End_KeyRedef13 = newGroupInRecord("pnd_End_KeyRedef13", "Redefines", pnd_End_Key);
        pnd_End_Key_Crprte_Status_Ind = pnd_End_KeyRedef13.newFieldInGroup("pnd_End_Key_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1);
        pnd_End_Key_Last_Chnge_Invrt_Dte_Tme = pnd_End_KeyRedef13.newFieldInGroup("pnd_End_Key_Last_Chnge_Invrt_Dte_Tme", "LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15);

        pnd_Timx = newFieldInRecord("pnd_Timx", "#TIMX", FieldType.TIME);

        pnd_Participant_Closed_Date_Time = newFieldInRecord("pnd_Participant_Closed_Date_Time", "#PARTICIPANT-CLOSED-DATE-TIME", FieldType.TIME);

        pnd_Receive_Date = newFieldInRecord("pnd_Receive_Date", "#RECEIVE-DATE", FieldType.DATE);

        pnd_Report_Date = newFieldInRecord("pnd_Report_Date", "#REPORT-DATE", FieldType.DATE);

        pnd_Status_Updte_Dte = newFieldInRecord("pnd_Status_Updte_Dte", "#STATUS-UPDTE-DTE", FieldType.DATE);

        pnd_Grand_Title = newFieldInRecord("pnd_Grand_Title", "#GRAND-TITLE", FieldType.STRING, 29);

        pnd_Grand_Title2 = newFieldInRecord("pnd_Grand_Title2", "#GRAND-TITLE2", FieldType.STRING, 10);

        pnd_Generate_Combined_Totals = newFieldInRecord("pnd_Generate_Combined_Totals", "#GENERATE-COMBINED-TOTALS", FieldType.BOOLEAN);

        pnd_Mid_Total = newFieldInRecord("pnd_Mid_Total", "#MID-TOTAL", FieldType.BOOLEAN);

        pnd_Compare_Unit = newFieldInRecord("pnd_Compare_Unit", "#COMPARE-UNIT", FieldType.STRING, 8);

        pnd_Comp_Unit = newFieldInRecord("pnd_Comp_Unit", "#COMP-UNIT", FieldType.STRING, 8);

        pnd_Gtot_Registers = newGroupInRecord("pnd_Gtot_Registers", "#GTOT-REGISTERS");
        pnd_Gtot_Registers_Pnd_Ave_Total = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Ave_Total", "#AVE-TOTAL", FieldType.DECIMAL, 10,
            3);
        pnd_Gtot_Registers_Pnd_Ave_Total_Num = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Ave_Total_Num", "#AVE-TOTAL-NUM", FieldType.DECIMAL, 
            10,3);
        pnd_Gtot_Registers_Pnd_Total_Moc = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Total_Moc", "#TOTAL-MOC", FieldType.NUMERIC, 7);
        pnd_Gtot_Registers_Pnd_Total_Moc_3 = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Total_Moc_3", "#TOTAL-MOC-3", FieldType.NUMERIC, 
            7);
        pnd_Gtot_Registers_Pnd_Total_Moc_4 = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Total_Moc_4", "#TOTAL-MOC-4", FieldType.NUMERIC, 
            7);
        pnd_Gtot_Registers_Pnd_Total_Moc_5 = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Total_Moc_5", "#TOTAL-MOC-5", FieldType.NUMERIC, 
            7);
        pnd_Gtot_Registers_Pnd_Total_Moc_10 = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Total_Moc_10", "#TOTAL-MOC-10", FieldType.NUMERIC, 
            7);
        pnd_Gtot_Registers_Pnd_Total_Moc_Over = pnd_Gtot_Registers.newFieldInGroup("pnd_Gtot_Registers_Pnd_Total_Moc_Over", "#TOTAL-MOC-OVER", FieldType.NUMERIC, 
            7);

        pnd_Combined_Totals = newGroupInRecord("pnd_Combined_Totals", "#COMBINED-TOTALS");
        pnd_Combined_Totals_Pnd_Cave_Total = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Cave_Total", "#CAVE-TOTAL", FieldType.DECIMAL, 
            10,3, new DbsArrayController(1,3));
        pnd_Combined_Totals_Pnd_Cave_Total_Num = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Cave_Total_Num", "#CAVE-TOTAL-NUM", 
            FieldType.DECIMAL, 10,3, new DbsArrayController(1,3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc", "#CTOTAL-MOC", FieldType.NUMERIC, 
            7, new DbsArrayController(1,3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_3 = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_3", "#CTOTAL-MOC-3", FieldType.NUMERIC, 
            7, new DbsArrayController(1,3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_4 = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_4", "#CTOTAL-MOC-4", FieldType.NUMERIC, 
            7, new DbsArrayController(1,3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_5 = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_5", "#CTOTAL-MOC-5", FieldType.NUMERIC, 
            7, new DbsArrayController(1,3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_10 = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_10", "#CTOTAL-MOC-10", FieldType.NUMERIC, 
            7, new DbsArrayController(1,3));
        pnd_Combined_Totals_Pnd_Ctotal_Moc_Over = pnd_Combined_Totals.newFieldArrayInGroup("pnd_Combined_Totals_Pnd_Ctotal_Moc_Over", "#CTOTAL-MOC-OVER", 
            FieldType.NUMERIC, 7, new DbsArrayController(1,3));

        pnd_Holiday = newFieldInRecord("pnd_Holiday", "#HOLIDAY", FieldType.NUMERIC, 18);

        pnd_Datx = newFieldInRecord("pnd_Datx", "#DATX", FieldType.DATE);

        pnd_Combine_Unit = newFieldInRecord("pnd_Combine_Unit", "#COMBINE-UNIT", FieldType.STRING, 8);

        pnd_Exclude_Wpids = newFieldArrayInRecord("pnd_Exclude_Wpids", "#EXCLUDE-WPIDS", FieldType.STRING, 6, new DbsArrayController(1,32));

        pnd_Valid_Units = newFieldArrayInRecord("pnd_Valid_Units", "#VALID-UNITS", FieldType.STRING, 5, new DbsArrayController(1,8));

        pnd_Combined_Unit = newFieldArrayInRecord("pnd_Combined_Unit", "#COMBINED-UNIT", FieldType.STRING, 8, new DbsArrayController(1,8));

        pnd_Combined_Unit_Hdr = newFieldInRecord("pnd_Combined_Unit_Hdr", "#COMBINED-UNIT-HDR", FieldType.STRING, 6);

        this.setRecordName("LdaCwfl3802");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cwf_Master_Index.reset();
        pnd_Exclude_Wpids.getValue(1).setInitialValue("RA PDW");
        pnd_Exclude_Wpids.getValue(2).setInitialValue("RA PEI");
        pnd_Exclude_Wpids.getValue(3).setInitialValue("RA PNA");
        pnd_Exclude_Wpids.getValue(4).setInitialValue("RA RA");
        pnd_Exclude_Wpids.getValue(5).setInitialValue("RA RQ");
        pnd_Exclude_Wpids.getValue(6).setInitialValue("TA PAA");
        pnd_Exclude_Wpids.getValue(7).setInitialValue("TA PAB");
        pnd_Exclude_Wpids.getValue(8).setInitialValue("TA PAC");
        pnd_Exclude_Wpids.getValue(9).setInitialValue("TA PAD");
        pnd_Exclude_Wpids.getValue(10).setInitialValue("TA PAE");
        pnd_Exclude_Wpids.getValue(11).setInitialValue("TA PAF");
        pnd_Exclude_Wpids.getValue(12).setInitialValue("TA PAG");
        pnd_Exclude_Wpids.getValue(13).setInitialValue("TA PAH");
        pnd_Exclude_Wpids.getValue(14).setInitialValue("TA PAI");
        pnd_Exclude_Wpids.getValue(15).setInitialValue("TA PAJ");
        pnd_Exclude_Wpids.getValue(16).setInitialValue("TA PAK");
        pnd_Exclude_Wpids.getValue(17).setInitialValue("TA PAL");
        pnd_Exclude_Wpids.getValue(18).setInitialValue("TA PAM");
        pnd_Exclude_Wpids.getValue(19).setInitialValue("TA PAO");
        pnd_Exclude_Wpids.getValue(20).setInitialValue("TA PAP");
        pnd_Exclude_Wpids.getValue(21).setInitialValue("TA PAR");
        pnd_Exclude_Wpids.getValue(22).setInitialValue("TA PAS");
        pnd_Exclude_Wpids.getValue(23).setInitialValue("TA PJA");
        pnd_Exclude_Wpids.getValue(24).setInitialValue("TA PJG");
        pnd_Exclude_Wpids.getValue(25).setInitialValue("TA PJH");
        pnd_Exclude_Wpids.getValue(26).setInitialValue("TA PJI");
        pnd_Exclude_Wpids.getValue(27).setInitialValue("TA PJJ");
        pnd_Exclude_Wpids.getValue(28).setInitialValue("TA PJK");
        pnd_Exclude_Wpids.getValue(29).setInitialValue("TA PJL");
        pnd_Exclude_Wpids.getValue(30).setInitialValue("TA PJM");
        pnd_Exclude_Wpids.getValue(31).setInitialValue("TA PJP");
        pnd_Exclude_Wpids.getValue(32).setInitialValue("TA PJU");
        pnd_Valid_Units.getValue(1).setInitialValue("ACADM");
        pnd_Combined_Unit.getValue(1).setInitialValue("APC  1B");
        pnd_Combined_Unit.getValue(2).setInitialValue("APC  2B");
        pnd_Combined_Unit.getValue(3).setInitialValue("AIIVC2B");
    }

    // Constructor
    public LdaCwfl3802() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
