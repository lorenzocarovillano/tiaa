/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:05:55 PM
**        * FROM NATURAL LDA     : IEFL9013
************************************************************
**        * FILE NAME            : LdaIefl9013.java
**        * CLASS NAME           : LdaIefl9013
**        * INSTANCE NAME        : LdaIefl9013
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIefl9013 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_icw_Efm_Audit;
    private DbsField icw_Efm_Audit_Log_Dte_Tme;
    private DbsField icw_Efm_Audit_System_Cde;
    private DbsField icw_Efm_Audit_Action_Cde;
    private DbsField icw_Efm_Audit_Empl_Oprtr_Cde;
    private DbsField icw_Efm_Audit_Rqst_Prcss_Tme;
    private DbsField icw_Efm_Audit_Recursive_Ind;
    private DbsField icw_Efm_Audit_Error_Cde;
    private DbsField icw_Efm_Audit_Mit_Added_Cnt;
    private DbsField icw_Efm_Audit_Mit_Updated_Cnt;
    private DbsField icw_Efm_Audit_Dcmnt_Added_Cnt;
    private DbsField icw_Efm_Audit_Dcmnt_Renamed_Cnt;
    private DbsField icw_Efm_Audit_Cabinet_Id;
    private DbsField icw_Efm_Audit_Count_Castrqst_Id;
    private DbsGroup icw_Efm_Audit_Rqst_IdMuGroup;
    private DbsField icw_Efm_Audit_Rqst_Id;

    public DataAccessProgramView getVw_icw_Efm_Audit() { return vw_icw_Efm_Audit; }

    public DbsField getIcw_Efm_Audit_Log_Dte_Tme() { return icw_Efm_Audit_Log_Dte_Tme; }

    public DbsField getIcw_Efm_Audit_System_Cde() { return icw_Efm_Audit_System_Cde; }

    public DbsField getIcw_Efm_Audit_Action_Cde() { return icw_Efm_Audit_Action_Cde; }

    public DbsField getIcw_Efm_Audit_Empl_Oprtr_Cde() { return icw_Efm_Audit_Empl_Oprtr_Cde; }

    public DbsField getIcw_Efm_Audit_Rqst_Prcss_Tme() { return icw_Efm_Audit_Rqst_Prcss_Tme; }

    public DbsField getIcw_Efm_Audit_Recursive_Ind() { return icw_Efm_Audit_Recursive_Ind; }

    public DbsField getIcw_Efm_Audit_Error_Cde() { return icw_Efm_Audit_Error_Cde; }

    public DbsField getIcw_Efm_Audit_Mit_Added_Cnt() { return icw_Efm_Audit_Mit_Added_Cnt; }

    public DbsField getIcw_Efm_Audit_Mit_Updated_Cnt() { return icw_Efm_Audit_Mit_Updated_Cnt; }

    public DbsField getIcw_Efm_Audit_Dcmnt_Added_Cnt() { return icw_Efm_Audit_Dcmnt_Added_Cnt; }

    public DbsField getIcw_Efm_Audit_Dcmnt_Renamed_Cnt() { return icw_Efm_Audit_Dcmnt_Renamed_Cnt; }

    public DbsField getIcw_Efm_Audit_Cabinet_Id() { return icw_Efm_Audit_Cabinet_Id; }

    public DbsField getIcw_Efm_Audit_Count_Castrqst_Id() { return icw_Efm_Audit_Count_Castrqst_Id; }

    public DbsGroup getIcw_Efm_Audit_Rqst_IdMuGroup() { return icw_Efm_Audit_Rqst_IdMuGroup; }

    public DbsField getIcw_Efm_Audit_Rqst_Id() { return icw_Efm_Audit_Rqst_Id; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_icw_Efm_Audit = new DataAccessProgramView(new NameInfo("vw_icw_Efm_Audit", "ICW-EFM-AUDIT"), "ICW_EFM_AUDIT", "ICW_EFM_AUDIT");
        icw_Efm_Audit_Log_Dte_Tme = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Log_Dte_Tme", "LOG-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LOG_DTE_TME");
        icw_Efm_Audit_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        icw_Efm_Audit_System_Cde = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_System_Cde", "SYSTEM-CDE", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTEM_CDE");
        icw_Efm_Audit_System_Cde.setDdmHeader("SYSTEM");
        icw_Efm_Audit_Action_Cde = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Action_Cde", "ACTION-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTION_CDE");
        icw_Efm_Audit_Action_Cde.setDdmHeader("ACTION");
        icw_Efm_Audit_Empl_Oprtr_Cde = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        icw_Efm_Audit_Empl_Oprtr_Cde.setDdmHeader("OPERATOR/CODE");
        icw_Efm_Audit_Rqst_Prcss_Tme = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Rqst_Prcss_Tme", "RQST-PRCSS-TME", FieldType.NUMERIC, 
            5, RepeatingFieldStrategy.None, "RQST_PRCSS_TME");
        icw_Efm_Audit_Rqst_Prcss_Tme.setDdmHeader("PROCESS/TIME");
        icw_Efm_Audit_Recursive_Ind = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Recursive_Ind", "RECURSIVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RECURSIVE_IND");
        icw_Efm_Audit_Recursive_Ind.setDdmHeader("RECUR/IND");
        icw_Efm_Audit_Error_Cde = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Error_Cde", "ERROR-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "ERROR_CDE");
        icw_Efm_Audit_Error_Cde.setDdmHeader("ERR/CDE");
        icw_Efm_Audit_Mit_Added_Cnt = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Mit_Added_Cnt", "MIT-ADDED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MIT_ADDED_CNT");
        icw_Efm_Audit_Mit_Added_Cnt.setDdmHeader("NBR OF MIT/RECS ADDED");
        icw_Efm_Audit_Mit_Updated_Cnt = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Mit_Updated_Cnt", "MIT-UPDATED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MIT_UPDATED_CNT");
        icw_Efm_Audit_Mit_Updated_Cnt.setDdmHeader("NBR OF MIT/RECS UPDATED");
        icw_Efm_Audit_Dcmnt_Added_Cnt = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Dcmnt_Added_Cnt", "DCMNT-ADDED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DCMNT_ADDED_CNT");
        icw_Efm_Audit_Dcmnt_Added_Cnt.setDdmHeader("NBR OF DCMT/ADDED");
        icw_Efm_Audit_Dcmnt_Renamed_Cnt = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Dcmnt_Renamed_Cnt", "DCMNT-RENAMED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DCMNT_RENAMED_CNT");
        icw_Efm_Audit_Dcmnt_Renamed_Cnt.setDdmHeader("NBR OF DCMT/RENAMED");
        icw_Efm_Audit_Cabinet_Id = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Cabinet_Id", "CABINET-ID", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        icw_Efm_Audit_Cabinet_Id.setDdmHeader("PIN");
        icw_Efm_Audit_Count_Castrqst_Id = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Count_Castrqst_Id", "C*RQST-ID", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.CAsteriskVariable, "ICW_EFM_AUDIT_RQST_ID");
        icw_Efm_Audit_Rqst_IdMuGroup = vw_icw_Efm_Audit.getRecord().newGroupInGroup("icw_Efm_Audit_Rqst_IdMuGroup", "RQST_IDMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ICW_EFM_AUDIT_RQST_ID");        icw_Efm_Audit_Rqst_Id = icw_Efm_Audit_Rqst_IdMuGroup.newFieldArrayInGroup("icw_Efm_Audit_Rqst_Id", "RQST-ID", 
            FieldType.STRING, 22, new DbsArrayController(1,10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RQST_ID");
        icw_Efm_Audit_Rqst_Id.setDdmHeader("RQST ID");

        this.setRecordName("LdaIefl9013");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_icw_Efm_Audit.reset();
    }

    // Constructor
    public LdaIefl9013() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
