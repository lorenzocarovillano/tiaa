/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:05:13 PM
**        * FROM NATURAL LDA     : ICWL3015
************************************************************
**        * FILE NAME            : LdaIcwl3015.java
**        * CLASS NAME           : LdaIcwl3015
**        * INSTANCE NAME        : LdaIcwl3015
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIcwl3015 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_icw_Activity_View;
    private DbsField icw_Activity_View_Rqst_Log_Dte_Tme;
    private DbsGroup icw_Activity_View_Rqst_Log_Dte_TmeRedef1;
    private DbsField icw_Activity_View_Rqst_Log_Index_Dte;
    private DbsField icw_Activity_View_Rqst_Log_Index_Tme;
    private DbsField icw_Activity_View_Actvty_Type;
    private DbsField icw_Activity_View_Strtng_Event_Dte_Tme;
    private DbsField icw_Activity_View_Endng_Event_Dte_Tme;
    private DbsField icw_Activity_View_Rt_Sqnce_Nbr;
    private DbsField icw_Activity_View_Unit_Cde;
    private DbsField icw_Activity_View_Step_Id;
    private DbsField icw_Activity_View_Empl_Racf_Id;
    private DbsField icw_Activity_View_Start_Status_Cde;
    private DbsField icw_Activity_View_End_Status_Cde;
    private DbsField icw_Activity_View_Actvty_Wait_Hours;
    private DbsField icw_Activity_View_Actvty_Prod_Hours;
    private DbsField icw_Activity_View_Unit_Turnaround_Start_Dte_Tme;
    private DbsField icw_Activity_View_Unit_Turnaround;
    private DbsField icw_Activity_View_System_Updte_Dte_Tme;
    private DbsField icw_Activity_View_Spcl_Prcss_Ind;
    private DbsField icw_Activity_View_Spcl_Prcss_Log_Dte_Tme;

    public DataAccessProgramView getVw_icw_Activity_View() { return vw_icw_Activity_View; }

    public DbsField getIcw_Activity_View_Rqst_Log_Dte_Tme() { return icw_Activity_View_Rqst_Log_Dte_Tme; }

    public DbsGroup getIcw_Activity_View_Rqst_Log_Dte_TmeRedef1() { return icw_Activity_View_Rqst_Log_Dte_TmeRedef1; }

    public DbsField getIcw_Activity_View_Rqst_Log_Index_Dte() { return icw_Activity_View_Rqst_Log_Index_Dte; }

    public DbsField getIcw_Activity_View_Rqst_Log_Index_Tme() { return icw_Activity_View_Rqst_Log_Index_Tme; }

    public DbsField getIcw_Activity_View_Actvty_Type() { return icw_Activity_View_Actvty_Type; }

    public DbsField getIcw_Activity_View_Strtng_Event_Dte_Tme() { return icw_Activity_View_Strtng_Event_Dte_Tme; }

    public DbsField getIcw_Activity_View_Endng_Event_Dte_Tme() { return icw_Activity_View_Endng_Event_Dte_Tme; }

    public DbsField getIcw_Activity_View_Rt_Sqnce_Nbr() { return icw_Activity_View_Rt_Sqnce_Nbr; }

    public DbsField getIcw_Activity_View_Unit_Cde() { return icw_Activity_View_Unit_Cde; }

    public DbsField getIcw_Activity_View_Step_Id() { return icw_Activity_View_Step_Id; }

    public DbsField getIcw_Activity_View_Empl_Racf_Id() { return icw_Activity_View_Empl_Racf_Id; }

    public DbsField getIcw_Activity_View_Start_Status_Cde() { return icw_Activity_View_Start_Status_Cde; }

    public DbsField getIcw_Activity_View_End_Status_Cde() { return icw_Activity_View_End_Status_Cde; }

    public DbsField getIcw_Activity_View_Actvty_Wait_Hours() { return icw_Activity_View_Actvty_Wait_Hours; }

    public DbsField getIcw_Activity_View_Actvty_Prod_Hours() { return icw_Activity_View_Actvty_Prod_Hours; }

    public DbsField getIcw_Activity_View_Unit_Turnaround_Start_Dte_Tme() { return icw_Activity_View_Unit_Turnaround_Start_Dte_Tme; }

    public DbsField getIcw_Activity_View_Unit_Turnaround() { return icw_Activity_View_Unit_Turnaround; }

    public DbsField getIcw_Activity_View_System_Updte_Dte_Tme() { return icw_Activity_View_System_Updte_Dte_Tme; }

    public DbsField getIcw_Activity_View_Spcl_Prcss_Ind() { return icw_Activity_View_Spcl_Prcss_Ind; }

    public DbsField getIcw_Activity_View_Spcl_Prcss_Log_Dte_Tme() { return icw_Activity_View_Spcl_Prcss_Log_Dte_Tme; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_icw_Activity_View = new DataAccessProgramView(new NameInfo("vw_icw_Activity_View", "ICW-ACTIVITY-VIEW"), "ICW_ACTIVITY", "ICW_ACTIVTY_FILE");
        icw_Activity_View_Rqst_Log_Dte_Tme = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        icw_Activity_View_Rqst_Log_Dte_TmeRedef1 = vw_icw_Activity_View.getRecord().newGroupInGroup("icw_Activity_View_Rqst_Log_Dte_TmeRedef1", "Redefines", 
            icw_Activity_View_Rqst_Log_Dte_Tme);
        icw_Activity_View_Rqst_Log_Index_Dte = icw_Activity_View_Rqst_Log_Dte_TmeRedef1.newFieldInGroup("icw_Activity_View_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        icw_Activity_View_Rqst_Log_Index_Tme = icw_Activity_View_Rqst_Log_Dte_TmeRedef1.newFieldInGroup("icw_Activity_View_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        icw_Activity_View_Actvty_Type = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Actvty_Type", "ACTVTY-TYPE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVTY_TYPE");
        icw_Activity_View_Actvty_Type.setDdmHeader("ACTIVITY TYPE");
        icw_Activity_View_Strtng_Event_Dte_Tme = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Strtng_Event_Dte_Tme", "STRTNG-EVENT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STRTNG_EVENT_DTE_TME");
        icw_Activity_View_Strtng_Event_Dte_Tme.setDdmHeader("STARTING/EVENT");
        icw_Activity_View_Endng_Event_Dte_Tme = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Endng_Event_Dte_Tme", "ENDNG-EVENT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ENDNG_EVENT_DTE_TME");
        icw_Activity_View_Endng_Event_Dte_Tme.setDdmHeader("ENDING/EVENT");
        icw_Activity_View_Rt_Sqnce_Nbr = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        icw_Activity_View_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        icw_Activity_View_Unit_Cde = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        icw_Activity_View_Unit_Cde.setDdmHeader("UNIT");
        icw_Activity_View_Step_Id = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        icw_Activity_View_Step_Id.setDdmHeader("STEP/ID");
        icw_Activity_View_Empl_Racf_Id = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        icw_Activity_View_Empl_Racf_Id.setDdmHeader("EMPLOYEE");
        icw_Activity_View_Start_Status_Cde = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Start_Status_Cde", "START-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "START_STATUS_CDE");
        icw_Activity_View_End_Status_Cde = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_End_Status_Cde", "END-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "END_STATUS_CDE");
        icw_Activity_View_Actvty_Wait_Hours = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Actvty_Wait_Hours", "ACTVTY-WAIT-HOURS", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACTVTY_WAIT_HOURS");
        icw_Activity_View_Actvty_Wait_Hours.setDdmHeader("ELAPSED/WAIT/TIME(HR)");
        icw_Activity_View_Actvty_Prod_Hours = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Actvty_Prod_Hours", "ACTVTY-PROD-HOURS", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ACTVTY_PROD_HOURS");
        icw_Activity_View_Actvty_Prod_Hours.setDdmHeader("ELAPSED/PRODUCTIVITY/TIME(HR)");
        icw_Activity_View_Unit_Turnaround_Start_Dte_Tme = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Unit_Turnaround_Start_Dte_Tme", 
            "UNIT-TURNAROUND-START-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_TURNAROUND_START_DTE_TME");
        icw_Activity_View_Unit_Turnaround = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Unit_Turnaround", "UNIT-TURNAROUND", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "UNIT_TURNAROUND");
        icw_Activity_View_System_Updte_Dte_Tme = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_System_Updte_Dte_Tme", "SYSTEM-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "SYSTEM_UPDTE_DTE_TME");
        icw_Activity_View_Spcl_Prcss_Ind = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Spcl_Prcss_Ind", "SPCL-PRCSS-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "SPCL_PRCSS_IND");
        icw_Activity_View_Spcl_Prcss_Log_Dte_Tme = vw_icw_Activity_View.getRecord().newFieldInGroup("icw_Activity_View_Spcl_Prcss_Log_Dte_Tme", "SPCL-PRCSS-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "SPCL_PRCSS_LOG_DTE_TME");

        this.setRecordName("LdaIcwl3015");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_icw_Activity_View.reset();
    }

    // Constructor
    public LdaIcwl3015() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
