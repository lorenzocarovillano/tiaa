/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:16:36 PM
**        * FROM NATURAL LDA     : WFOL4500
************************************************************
**        * FILE NAME            : LdaWfol4500.java
**        * CLASS NAME           : LdaWfol4500
**        * INSTANCE NAME        : LdaWfol4500
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaWfol4500 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cmcv;
    private DbsField cmcv_Rcrd_Stamp_Tme;
    private DbsField cmcv_Rcrd_Upld_Tme;
    private DbsField cmcv_Rcrd_Type_Cde;
    private DbsField cmcv_Systm_Ind;
    private DbsField cmcv_Contact_Id_Cde;
    private DbsField cmcv_Pin_Nbr;
    private DbsField cmcv_Call_Stamp_Tme;
    private DbsField cmcv_Btch_Nbr;
    private DbsField cmcv_Wpids_Nbr;
    private DbsField cmcv_Rqst_Entry_Op_Cde;
    private DbsField cmcv_Rqst_Origin_Unit_Cde;
    private DbsGroup cmcv_Request_Array_Grp;
    private DbsField cmcv_Action_Cde;
    private DbsField cmcv_Wpid_Cde;
    private DbsGroup cmcv_Cntrct_NbrMuGroup;
    private DbsField cmcv_Cntrct_Nbr;
    private DbsField cmcv_Rel_Log_Dte_Tme;
    private DbsField cmcv_Wpid_Validate_Ind;
    private DbsField cmcv_Dstntn_Unt_Cde;
    private DbsField cmcv_Corp_Status_Cde;
    private DbsField cmcv_Unit_Status_Cde;
    private DbsField cmcv_Work_Req_Prty_Cde;
    private DbsGroup cmcv_Request_Arry2_Grp;
    private DbsField cmcv_Prcssng_Tpe;
    private DbsField cmcv_Container_Id;
    private DbsField cmcv_Doc_Ctgry_Cde;
    private DbsField cmcv_Doc_Clss_Cde;
    private DbsField cmcv_Doc_Type_Spcfc_Cde;
    private DbsGroup cmcv_Doc_TxtMuGroup;
    private DbsField cmcv_Doc_Txt;
    private DbsField cmcv_Error_Msg_Txt;
    private DbsGroup cmcv_Wfo_All_Othr_Flds;
    private DbsField cmcv_Wfo_Othr_Flds;
    private DbsGroup cmcv_Wfo_Othr_FldsRedef1;
    private DbsField cmcv_Wf_Ind;
    private DbsField cmcv_Ind_Tracking_Id;
    private DbsField cmcv_Inst_Tracking_Id;
    private DbsField cmcv_Cabinet_Id;
    private DbsField cmcv_Cabinet_Level;
    private DbsField cmcv_Empl_Racf_Id;
    private DbsField cmcv_Step;
    private DbsField cmcv_Due_Dte;
    private DbsField cmcv_Client_App_Cde;
    private DbsField cmcv_Elctrnc_Fldr_Ind;
    private DbsField cmcv_Rqst_Orgn_Cde;
    private DbsField cmcv_Topic;
    private DbsGroup cmcv_Processed_Work_Rqsts_Grp;
    private DbsField cmcv_Processed_Work_Rqsts_Elmt;
    private DbsGroup cmcv_Processed_Work_Rqsts_ElmtRedef2;
    private DbsField cmcv_P_Rqst_Id;
    private DbsGroup cmcv_P_Rqst_IdRedef3;
    private DbsField cmcv_P_Tiaa_Rcvd_Date;
    private DbsField cmcv_P_Case_Id;
    private DbsField cmcv_P_Wpid;
    private DbsField cmcv_P_Sub_Rqst_Cde;
    private DbsField cmcv_P_Multi_Rqst_Ind;
    private DbsField cmcv_P_Rldt;
    private DbsField cmcv_P_Doc_Group;
    private DbsGroup cmcv_P_Doc_GroupRedef4;
    private DbsField cmcv_P_Dstntn_Unt_Cde;
    private DbsField cmcv_P_Fil1;
    private DbsField cmcv_P_Topic;
    private DbsField cmcv_P_Fil2;
    private DbsField cmcv_P_Wpid_Cde;
    private DbsField cmcv_P_Fil3;
    private DbsField cmcv_P_Corp_Status;
    private DbsField cmcv_P_Fil4;
    private DbsField cmcv_P_Wf_Ind;
    private DbsField cmcv_P_Fil5;
    private DbsField cmcv_P_Action_Cde;
    private DbsField cmcv_P_Cabinet;
    private DbsGroup cmcv_P_CabinetRedef5;
    private DbsField cmcv_P_Prefix;
    private DbsField cmcv_P_Pin;
    private DbsField cmcv_P_Cabinet_Level;
    private DbsField cmcv_Processed_Text_Ind;
    private DbsField cmcv_Part_Orig_Crte_Dte_Tme;
    private DbsField cmcv_Inst_Orig_Crte_Dte_Tme;
    private DbsField cmcv_Nonp_Orig_Crte_Dte_Tme;
    private DbsField cmcv_Part_Orig_Document_Name;
    private DbsGroup cmcv_Part_Orig_Document_NameRedef6;
    private DbsField cmcv_Part_Orig_Category;
    private DbsField cmcv_Part_Orig_Class;
    private DbsField cmcv_Part_Orig_Work_Prcss_Id;
    private DbsField cmcv_Part_Orig_Specific;
    private DbsField cmcv_Part_Orig_Creation_Cc;
    private DbsField cmcv_Part_Orig_Creation_Yymmdd;
    private DbsField cmcv_Part_Orig_Direction;
    private DbsField cmcv_Part_Orig_Secured_Ind;
    private DbsField cmcv_Part_Orig_Retention_Ind;
    private DbsField cmcv_Part_Orig_Format;
    private DbsField cmcv_Part_Orig_Tie_Breaker;
    private DbsField cmcv_Part_Orig_Copy_Ind;
    private DbsField cmcv_Part_Orig_Important_Ind;
    private DbsField cmcv_Part_Orig_Package_Nbr;
    private DbsField cmcv_Inst_Orig_Document_Name;
    private DbsGroup cmcv_Inst_Orig_Document_NameRedef7;
    private DbsField cmcv_Inst_Orig_Category;
    private DbsField cmcv_Inst_Orig_Class;
    private DbsField cmcv_Inst_Orig_Specific;
    private DbsField cmcv_Inst_Orig_Direction;
    private DbsField cmcv_Inst_Orig_Format;
    private DbsField cmcv_Nonp_Orig_Document_Name;
    private DbsGroup cmcv_Nonp_Orig_Document_NameRedef8;
    private DbsField cmcv_Nonp_Orig_Category;
    private DbsField cmcv_Nonp_Orig_Class;
    private DbsField cmcv_Nonp_Orig_Specific;
    private DbsField cmcv_Nonp_Orig_Direction;
    private DbsField cmcv_Nonp_Orig_Format;
    private DbsField cmcv_Cntrct;
    private DbsGroup cmcv_Processed2_Work_Rqsts_Grp;
    private DbsField cmcv_P_Tracking_Id;
    private DbsField cmcv_P_Ati_Ind;
    private DbsField cmcv_Lead_Class;
    private DbsField cmcv_Express_Ind;
    private DbsField cmcv_Mail_Cde;
    private DbsGroup cmcv_Wfo_Othr_FldsRedef9;
    private DbsField cmcv_Dcmnt_Obj_Id;
    private DbsField cmcv_Mail_Item_No;
    private DbsField cmcv_T_Client_App_Cde;
    private DbsField cmcv_Image_Addrss;
    private DbsField cmcv_Batch_Id;
    private DbsField cmcv_Source_Id;
    private DbsField cmcv_Dcmnt_Direction;
    private DbsField cmcv_T_Wf_Ind;
    private DbsField cmcv_T_Ind_Tracking_Id;
    private DbsField cmcv_T_Inst_Tracking_Id;
    private DbsField cmcv_T_Cabinet_Id;
    private DbsField cmcv_T_Cabinet_Level;
    private DbsField cmcv_Dcmnt_Text_2;
    private DbsField cmcv_Img_Start_Page_Nbr;
    private DbsField cmcv_Img_Total_Pages;
    private DbsField cmcv_T_Prcssd_Ind;
    private DbsField cmcv_T_P_Orig_Crte_Dte_Tme;
    private DbsField cmcv_T_I_Orig_Crte_Dte_Tme;
    private DbsField cmcv_T_N_Orig_Crte_Dte_Tme;
    private DbsField cmcv_T_P_Orig_Document_Name;
    private DbsGroup cmcv_T_P_Orig_Document_NameRedef10;
    private DbsField cmcv_T_P_Orig_Category;
    private DbsField cmcv_T_P_Orig_Class;
    private DbsField cmcv_T_P_Orig_Work_Prcss_Id;
    private DbsField cmcv_T_P_Orig_Specific;
    private DbsField cmcv_T_P_Orig_Creation_Cc;
    private DbsField cmcv_T_P_Orig_Creation_Yymmdd;
    private DbsField cmcv_T_P_Orig_Direction;
    private DbsField cmcv_T_P_Orig_Secured_Ind;
    private DbsField cmcv_T_P_Orig_Retention_Ind;
    private DbsField cmcv_T_P_Orig_Format;
    private DbsField cmcv_T_P_Orig_Tie_Breaker;
    private DbsField cmcv_T_P_Orig_Copy_Ind;
    private DbsField cmcv_T_P_Orig_Important_Ind;
    private DbsField cmcv_T_P_Orig_Package_Nbr;
    private DbsField cmcv_T_I_Orig_Document_Name;
    private DbsGroup cmcv_T_I_Orig_Document_NameRedef11;
    private DbsField cmcv_T_I_Orig_Category;
    private DbsField cmcv_T_I_Orig_Class;
    private DbsField cmcv_T_I_Orig_Specific;
    private DbsField cmcv_T_I_Orig_Direction;
    private DbsField cmcv_T_I_Orig_Format;
    private DbsField cmcv_T_N_Orig_Document_Name;
    private DbsGroup cmcv_T_N_Orig_Document_NameRedef12;
    private DbsField cmcv_T_N_Orig_Category;
    private DbsField cmcv_T_N_Orig_Class;
    private DbsField cmcv_T_N_Orig_Specific;
    private DbsField cmcv_T_N_Orig_Direction;
    private DbsField cmcv_T_N_Orig_Format;
    private DbsField cmcv_T_Total_Nbr_Image_Processed;
    private DbsField cmcv_T_Image_Mail_Item_Nbr;
    private DbsField cmcv_T_Image_Start_Page_Nbr;
    private DbsField cmcv_T_Image_End_Page_Nbr;
    private DbsGroup cmcv_Tracking_Rcrd_NbrMuGroup;
    private DbsField cmcv_Tracking_Rcrd_Nbr;
    private DbsField cmcv_Wfo_Ind;

    public DataAccessProgramView getVw_cmcv() { return vw_cmcv; }

    public DbsField getCmcv_Rcrd_Stamp_Tme() { return cmcv_Rcrd_Stamp_Tme; }

    public DbsField getCmcv_Rcrd_Upld_Tme() { return cmcv_Rcrd_Upld_Tme; }

    public DbsField getCmcv_Rcrd_Type_Cde() { return cmcv_Rcrd_Type_Cde; }

    public DbsField getCmcv_Systm_Ind() { return cmcv_Systm_Ind; }

    public DbsField getCmcv_Contact_Id_Cde() { return cmcv_Contact_Id_Cde; }

    public DbsField getCmcv_Pin_Nbr() { return cmcv_Pin_Nbr; }

    public DbsField getCmcv_Call_Stamp_Tme() { return cmcv_Call_Stamp_Tme; }

    public DbsField getCmcv_Btch_Nbr() { return cmcv_Btch_Nbr; }

    public DbsField getCmcv_Wpids_Nbr() { return cmcv_Wpids_Nbr; }

    public DbsField getCmcv_Rqst_Entry_Op_Cde() { return cmcv_Rqst_Entry_Op_Cde; }

    public DbsField getCmcv_Rqst_Origin_Unit_Cde() { return cmcv_Rqst_Origin_Unit_Cde; }

    public DbsGroup getCmcv_Request_Array_Grp() { return cmcv_Request_Array_Grp; }

    public DbsField getCmcv_Action_Cde() { return cmcv_Action_Cde; }

    public DbsField getCmcv_Wpid_Cde() { return cmcv_Wpid_Cde; }

    public DbsGroup getCmcv_Cntrct_NbrMuGroup() { return cmcv_Cntrct_NbrMuGroup; }

    public DbsField getCmcv_Cntrct_Nbr() { return cmcv_Cntrct_Nbr; }

    public DbsField getCmcv_Rel_Log_Dte_Tme() { return cmcv_Rel_Log_Dte_Tme; }

    public DbsField getCmcv_Wpid_Validate_Ind() { return cmcv_Wpid_Validate_Ind; }

    public DbsField getCmcv_Dstntn_Unt_Cde() { return cmcv_Dstntn_Unt_Cde; }

    public DbsField getCmcv_Corp_Status_Cde() { return cmcv_Corp_Status_Cde; }

    public DbsField getCmcv_Unit_Status_Cde() { return cmcv_Unit_Status_Cde; }

    public DbsField getCmcv_Work_Req_Prty_Cde() { return cmcv_Work_Req_Prty_Cde; }

    public DbsGroup getCmcv_Request_Arry2_Grp() { return cmcv_Request_Arry2_Grp; }

    public DbsField getCmcv_Prcssng_Tpe() { return cmcv_Prcssng_Tpe; }

    public DbsField getCmcv_Container_Id() { return cmcv_Container_Id; }

    public DbsField getCmcv_Doc_Ctgry_Cde() { return cmcv_Doc_Ctgry_Cde; }

    public DbsField getCmcv_Doc_Clss_Cde() { return cmcv_Doc_Clss_Cde; }

    public DbsField getCmcv_Doc_Type_Spcfc_Cde() { return cmcv_Doc_Type_Spcfc_Cde; }

    public DbsGroup getCmcv_Doc_TxtMuGroup() { return cmcv_Doc_TxtMuGroup; }

    public DbsField getCmcv_Doc_Txt() { return cmcv_Doc_Txt; }

    public DbsField getCmcv_Error_Msg_Txt() { return cmcv_Error_Msg_Txt; }

    public DbsGroup getCmcv_Wfo_All_Othr_Flds() { return cmcv_Wfo_All_Othr_Flds; }

    public DbsField getCmcv_Wfo_Othr_Flds() { return cmcv_Wfo_Othr_Flds; }

    public DbsGroup getCmcv_Wfo_Othr_FldsRedef1() { return cmcv_Wfo_Othr_FldsRedef1; }

    public DbsField getCmcv_Wf_Ind() { return cmcv_Wf_Ind; }

    public DbsField getCmcv_Ind_Tracking_Id() { return cmcv_Ind_Tracking_Id; }

    public DbsField getCmcv_Inst_Tracking_Id() { return cmcv_Inst_Tracking_Id; }

    public DbsField getCmcv_Cabinet_Id() { return cmcv_Cabinet_Id; }

    public DbsField getCmcv_Cabinet_Level() { return cmcv_Cabinet_Level; }

    public DbsField getCmcv_Empl_Racf_Id() { return cmcv_Empl_Racf_Id; }

    public DbsField getCmcv_Step() { return cmcv_Step; }

    public DbsField getCmcv_Due_Dte() { return cmcv_Due_Dte; }

    public DbsField getCmcv_Client_App_Cde() { return cmcv_Client_App_Cde; }

    public DbsField getCmcv_Elctrnc_Fldr_Ind() { return cmcv_Elctrnc_Fldr_Ind; }

    public DbsField getCmcv_Rqst_Orgn_Cde() { return cmcv_Rqst_Orgn_Cde; }

    public DbsField getCmcv_Topic() { return cmcv_Topic; }

    public DbsGroup getCmcv_Processed_Work_Rqsts_Grp() { return cmcv_Processed_Work_Rqsts_Grp; }

    public DbsField getCmcv_Processed_Work_Rqsts_Elmt() { return cmcv_Processed_Work_Rqsts_Elmt; }

    public DbsGroup getCmcv_Processed_Work_Rqsts_ElmtRedef2() { return cmcv_Processed_Work_Rqsts_ElmtRedef2; }

    public DbsField getCmcv_P_Rqst_Id() { return cmcv_P_Rqst_Id; }

    public DbsGroup getCmcv_P_Rqst_IdRedef3() { return cmcv_P_Rqst_IdRedef3; }

    public DbsField getCmcv_P_Tiaa_Rcvd_Date() { return cmcv_P_Tiaa_Rcvd_Date; }

    public DbsField getCmcv_P_Case_Id() { return cmcv_P_Case_Id; }

    public DbsField getCmcv_P_Wpid() { return cmcv_P_Wpid; }

    public DbsField getCmcv_P_Sub_Rqst_Cde() { return cmcv_P_Sub_Rqst_Cde; }

    public DbsField getCmcv_P_Multi_Rqst_Ind() { return cmcv_P_Multi_Rqst_Ind; }

    public DbsField getCmcv_P_Rldt() { return cmcv_P_Rldt; }

    public DbsField getCmcv_P_Doc_Group() { return cmcv_P_Doc_Group; }

    public DbsGroup getCmcv_P_Doc_GroupRedef4() { return cmcv_P_Doc_GroupRedef4; }

    public DbsField getCmcv_P_Dstntn_Unt_Cde() { return cmcv_P_Dstntn_Unt_Cde; }

    public DbsField getCmcv_P_Fil1() { return cmcv_P_Fil1; }

    public DbsField getCmcv_P_Topic() { return cmcv_P_Topic; }

    public DbsField getCmcv_P_Fil2() { return cmcv_P_Fil2; }

    public DbsField getCmcv_P_Wpid_Cde() { return cmcv_P_Wpid_Cde; }

    public DbsField getCmcv_P_Fil3() { return cmcv_P_Fil3; }

    public DbsField getCmcv_P_Corp_Status() { return cmcv_P_Corp_Status; }

    public DbsField getCmcv_P_Fil4() { return cmcv_P_Fil4; }

    public DbsField getCmcv_P_Wf_Ind() { return cmcv_P_Wf_Ind; }

    public DbsField getCmcv_P_Fil5() { return cmcv_P_Fil5; }

    public DbsField getCmcv_P_Action_Cde() { return cmcv_P_Action_Cde; }

    public DbsField getCmcv_P_Cabinet() { return cmcv_P_Cabinet; }

    public DbsGroup getCmcv_P_CabinetRedef5() { return cmcv_P_CabinetRedef5; }

    public DbsField getCmcv_P_Prefix() { return cmcv_P_Prefix; }

    public DbsField getCmcv_P_Pin() { return cmcv_P_Pin; }

    public DbsField getCmcv_P_Cabinet_Level() { return cmcv_P_Cabinet_Level; }

    public DbsField getCmcv_Processed_Text_Ind() { return cmcv_Processed_Text_Ind; }

    public DbsField getCmcv_Part_Orig_Crte_Dte_Tme() { return cmcv_Part_Orig_Crte_Dte_Tme; }

    public DbsField getCmcv_Inst_Orig_Crte_Dte_Tme() { return cmcv_Inst_Orig_Crte_Dte_Tme; }

    public DbsField getCmcv_Nonp_Orig_Crte_Dte_Tme() { return cmcv_Nonp_Orig_Crte_Dte_Tme; }

    public DbsField getCmcv_Part_Orig_Document_Name() { return cmcv_Part_Orig_Document_Name; }

    public DbsGroup getCmcv_Part_Orig_Document_NameRedef6() { return cmcv_Part_Orig_Document_NameRedef6; }

    public DbsField getCmcv_Part_Orig_Category() { return cmcv_Part_Orig_Category; }

    public DbsField getCmcv_Part_Orig_Class() { return cmcv_Part_Orig_Class; }

    public DbsField getCmcv_Part_Orig_Work_Prcss_Id() { return cmcv_Part_Orig_Work_Prcss_Id; }

    public DbsField getCmcv_Part_Orig_Specific() { return cmcv_Part_Orig_Specific; }

    public DbsField getCmcv_Part_Orig_Creation_Cc() { return cmcv_Part_Orig_Creation_Cc; }

    public DbsField getCmcv_Part_Orig_Creation_Yymmdd() { return cmcv_Part_Orig_Creation_Yymmdd; }

    public DbsField getCmcv_Part_Orig_Direction() { return cmcv_Part_Orig_Direction; }

    public DbsField getCmcv_Part_Orig_Secured_Ind() { return cmcv_Part_Orig_Secured_Ind; }

    public DbsField getCmcv_Part_Orig_Retention_Ind() { return cmcv_Part_Orig_Retention_Ind; }

    public DbsField getCmcv_Part_Orig_Format() { return cmcv_Part_Orig_Format; }

    public DbsField getCmcv_Part_Orig_Tie_Breaker() { return cmcv_Part_Orig_Tie_Breaker; }

    public DbsField getCmcv_Part_Orig_Copy_Ind() { return cmcv_Part_Orig_Copy_Ind; }

    public DbsField getCmcv_Part_Orig_Important_Ind() { return cmcv_Part_Orig_Important_Ind; }

    public DbsField getCmcv_Part_Orig_Package_Nbr() { return cmcv_Part_Orig_Package_Nbr; }

    public DbsField getCmcv_Inst_Orig_Document_Name() { return cmcv_Inst_Orig_Document_Name; }

    public DbsGroup getCmcv_Inst_Orig_Document_NameRedef7() { return cmcv_Inst_Orig_Document_NameRedef7; }

    public DbsField getCmcv_Inst_Orig_Category() { return cmcv_Inst_Orig_Category; }

    public DbsField getCmcv_Inst_Orig_Class() { return cmcv_Inst_Orig_Class; }

    public DbsField getCmcv_Inst_Orig_Specific() { return cmcv_Inst_Orig_Specific; }

    public DbsField getCmcv_Inst_Orig_Direction() { return cmcv_Inst_Orig_Direction; }

    public DbsField getCmcv_Inst_Orig_Format() { return cmcv_Inst_Orig_Format; }

    public DbsField getCmcv_Nonp_Orig_Document_Name() { return cmcv_Nonp_Orig_Document_Name; }

    public DbsGroup getCmcv_Nonp_Orig_Document_NameRedef8() { return cmcv_Nonp_Orig_Document_NameRedef8; }

    public DbsField getCmcv_Nonp_Orig_Category() { return cmcv_Nonp_Orig_Category; }

    public DbsField getCmcv_Nonp_Orig_Class() { return cmcv_Nonp_Orig_Class; }

    public DbsField getCmcv_Nonp_Orig_Specific() { return cmcv_Nonp_Orig_Specific; }

    public DbsField getCmcv_Nonp_Orig_Direction() { return cmcv_Nonp_Orig_Direction; }

    public DbsField getCmcv_Nonp_Orig_Format() { return cmcv_Nonp_Orig_Format; }

    public DbsField getCmcv_Cntrct() { return cmcv_Cntrct; }

    public DbsGroup getCmcv_Processed2_Work_Rqsts_Grp() { return cmcv_Processed2_Work_Rqsts_Grp; }

    public DbsField getCmcv_P_Tracking_Id() { return cmcv_P_Tracking_Id; }

    public DbsField getCmcv_P_Ati_Ind() { return cmcv_P_Ati_Ind; }

    public DbsField getCmcv_Lead_Class() { return cmcv_Lead_Class; }

    public DbsField getCmcv_Express_Ind() { return cmcv_Express_Ind; }

    public DbsField getCmcv_Mail_Cde() { return cmcv_Mail_Cde; }

    public DbsGroup getCmcv_Wfo_Othr_FldsRedef9() { return cmcv_Wfo_Othr_FldsRedef9; }

    public DbsField getCmcv_Dcmnt_Obj_Id() { return cmcv_Dcmnt_Obj_Id; }

    public DbsField getCmcv_Mail_Item_No() { return cmcv_Mail_Item_No; }

    public DbsField getCmcv_T_Client_App_Cde() { return cmcv_T_Client_App_Cde; }

    public DbsField getCmcv_Image_Addrss() { return cmcv_Image_Addrss; }

    public DbsField getCmcv_Batch_Id() { return cmcv_Batch_Id; }

    public DbsField getCmcv_Source_Id() { return cmcv_Source_Id; }

    public DbsField getCmcv_Dcmnt_Direction() { return cmcv_Dcmnt_Direction; }

    public DbsField getCmcv_T_Wf_Ind() { return cmcv_T_Wf_Ind; }

    public DbsField getCmcv_T_Ind_Tracking_Id() { return cmcv_T_Ind_Tracking_Id; }

    public DbsField getCmcv_T_Inst_Tracking_Id() { return cmcv_T_Inst_Tracking_Id; }

    public DbsField getCmcv_T_Cabinet_Id() { return cmcv_T_Cabinet_Id; }

    public DbsField getCmcv_T_Cabinet_Level() { return cmcv_T_Cabinet_Level; }

    public DbsField getCmcv_Dcmnt_Text_2() { return cmcv_Dcmnt_Text_2; }

    public DbsField getCmcv_Img_Start_Page_Nbr() { return cmcv_Img_Start_Page_Nbr; }

    public DbsField getCmcv_Img_Total_Pages() { return cmcv_Img_Total_Pages; }

    public DbsField getCmcv_T_Prcssd_Ind() { return cmcv_T_Prcssd_Ind; }

    public DbsField getCmcv_T_P_Orig_Crte_Dte_Tme() { return cmcv_T_P_Orig_Crte_Dte_Tme; }

    public DbsField getCmcv_T_I_Orig_Crte_Dte_Tme() { return cmcv_T_I_Orig_Crte_Dte_Tme; }

    public DbsField getCmcv_T_N_Orig_Crte_Dte_Tme() { return cmcv_T_N_Orig_Crte_Dte_Tme; }

    public DbsField getCmcv_T_P_Orig_Document_Name() { return cmcv_T_P_Orig_Document_Name; }

    public DbsGroup getCmcv_T_P_Orig_Document_NameRedef10() { return cmcv_T_P_Orig_Document_NameRedef10; }

    public DbsField getCmcv_T_P_Orig_Category() { return cmcv_T_P_Orig_Category; }

    public DbsField getCmcv_T_P_Orig_Class() { return cmcv_T_P_Orig_Class; }

    public DbsField getCmcv_T_P_Orig_Work_Prcss_Id() { return cmcv_T_P_Orig_Work_Prcss_Id; }

    public DbsField getCmcv_T_P_Orig_Specific() { return cmcv_T_P_Orig_Specific; }

    public DbsField getCmcv_T_P_Orig_Creation_Cc() { return cmcv_T_P_Orig_Creation_Cc; }

    public DbsField getCmcv_T_P_Orig_Creation_Yymmdd() { return cmcv_T_P_Orig_Creation_Yymmdd; }

    public DbsField getCmcv_T_P_Orig_Direction() { return cmcv_T_P_Orig_Direction; }

    public DbsField getCmcv_T_P_Orig_Secured_Ind() { return cmcv_T_P_Orig_Secured_Ind; }

    public DbsField getCmcv_T_P_Orig_Retention_Ind() { return cmcv_T_P_Orig_Retention_Ind; }

    public DbsField getCmcv_T_P_Orig_Format() { return cmcv_T_P_Orig_Format; }

    public DbsField getCmcv_T_P_Orig_Tie_Breaker() { return cmcv_T_P_Orig_Tie_Breaker; }

    public DbsField getCmcv_T_P_Orig_Copy_Ind() { return cmcv_T_P_Orig_Copy_Ind; }

    public DbsField getCmcv_T_P_Orig_Important_Ind() { return cmcv_T_P_Orig_Important_Ind; }

    public DbsField getCmcv_T_P_Orig_Package_Nbr() { return cmcv_T_P_Orig_Package_Nbr; }

    public DbsField getCmcv_T_I_Orig_Document_Name() { return cmcv_T_I_Orig_Document_Name; }

    public DbsGroup getCmcv_T_I_Orig_Document_NameRedef11() { return cmcv_T_I_Orig_Document_NameRedef11; }

    public DbsField getCmcv_T_I_Orig_Category() { return cmcv_T_I_Orig_Category; }

    public DbsField getCmcv_T_I_Orig_Class() { return cmcv_T_I_Orig_Class; }

    public DbsField getCmcv_T_I_Orig_Specific() { return cmcv_T_I_Orig_Specific; }

    public DbsField getCmcv_T_I_Orig_Direction() { return cmcv_T_I_Orig_Direction; }

    public DbsField getCmcv_T_I_Orig_Format() { return cmcv_T_I_Orig_Format; }

    public DbsField getCmcv_T_N_Orig_Document_Name() { return cmcv_T_N_Orig_Document_Name; }

    public DbsGroup getCmcv_T_N_Orig_Document_NameRedef12() { return cmcv_T_N_Orig_Document_NameRedef12; }

    public DbsField getCmcv_T_N_Orig_Category() { return cmcv_T_N_Orig_Category; }

    public DbsField getCmcv_T_N_Orig_Class() { return cmcv_T_N_Orig_Class; }

    public DbsField getCmcv_T_N_Orig_Specific() { return cmcv_T_N_Orig_Specific; }

    public DbsField getCmcv_T_N_Orig_Direction() { return cmcv_T_N_Orig_Direction; }

    public DbsField getCmcv_T_N_Orig_Format() { return cmcv_T_N_Orig_Format; }

    public DbsField getCmcv_T_Total_Nbr_Image_Processed() { return cmcv_T_Total_Nbr_Image_Processed; }

    public DbsField getCmcv_T_Image_Mail_Item_Nbr() { return cmcv_T_Image_Mail_Item_Nbr; }

    public DbsField getCmcv_T_Image_Start_Page_Nbr() { return cmcv_T_Image_Start_Page_Nbr; }

    public DbsField getCmcv_T_Image_End_Page_Nbr() { return cmcv_T_Image_End_Page_Nbr; }

    public DbsGroup getCmcv_Tracking_Rcrd_NbrMuGroup() { return cmcv_Tracking_Rcrd_NbrMuGroup; }

    public DbsField getCmcv_Tracking_Rcrd_Nbr() { return cmcv_Tracking_Rcrd_Nbr; }

    public DbsField getCmcv_Wfo_Ind() { return cmcv_Wfo_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cmcv = new DataAccessProgramView(new NameInfo("vw_cmcv", "CMCV"), "CWF_MCSS_CALLS", "CWF_MCSS_CALLS", DdmPeriodicGroups.getInstance().getGroups("CWF_MCSS_CALLS"));
        cmcv_Rcrd_Stamp_Tme = vw_cmcv.getRecord().newFieldInGroup("cmcv_Rcrd_Stamp_Tme", "RCRD-STAMP-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "RCRD_STAMP_TME");
        cmcv_Rcrd_Stamp_Tme.setDdmHeader("RECORD/TIME/STAMP");
        cmcv_Rcrd_Upld_Tme = vw_cmcv.getRecord().newFieldInGroup("cmcv_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME, RepeatingFieldStrategy.None, "RCRD_UPLD_TME");
        cmcv_Rcrd_Upld_Tme.setDdmHeader("RECORD/UPLOAD/TIME");
        cmcv_Rcrd_Type_Cde = vw_cmcv.getRecord().newFieldInGroup("cmcv_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE_CDE");
        cmcv_Rcrd_Type_Cde.setDdmHeader("RECORD/TYPE");
        cmcv_Systm_Ind = vw_cmcv.getRecord().newFieldInGroup("cmcv_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15, RepeatingFieldStrategy.None, "SYSTM_IND");
        cmcv_Systm_Ind.setDdmHeader("SYSTEM/OF/ORIGIN");
        cmcv_Contact_Id_Cde = vw_cmcv.getRecord().newFieldInGroup("cmcv_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CONTACT_ID_CDE");
        cmcv_Contact_Id_Cde.setDdmHeader("CONTACT/ID");
        cmcv_Pin_Nbr = vw_cmcv.getRecord().newFieldInGroup("cmcv_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        cmcv_Pin_Nbr.setDdmHeader("PIN");
        cmcv_Call_Stamp_Tme = vw_cmcv.getRecord().newFieldInGroup("cmcv_Call_Stamp_Tme", "CALL-STAMP-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CALL_STAMP_TME");
        cmcv_Call_Stamp_Tme.setDdmHeader("CALL/DATE &/TIME");
        cmcv_Btch_Nbr = vw_cmcv.getRecord().newFieldInGroup("cmcv_Btch_Nbr", "BTCH-NBR", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "BTCH_NBR");
        cmcv_Btch_Nbr.setDdmHeader("BATCH/NUM.");
        cmcv_Wpids_Nbr = vw_cmcv.getRecord().newFieldInGroup("cmcv_Wpids_Nbr", "WPIDS-NBR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "WPIDS_NBR");
        cmcv_Wpids_Nbr.setDdmHeader("WPIDS/SENT");
        cmcv_Rqst_Entry_Op_Cde = vw_cmcv.getRecord().newFieldInGroup("cmcv_Rqst_Entry_Op_Cde", "RQST-ENTRY-OP-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_ENTRY_OP_CDE");
        cmcv_Rqst_Entry_Op_Cde.setDdmHeader("ORIGIN/RACF/ID");
        cmcv_Rqst_Origin_Unit_Cde = vw_cmcv.getRecord().newFieldInGroup("cmcv_Rqst_Origin_Unit_Cde", "RQST-ORIGIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_ORIGIN_UNIT_CDE");
        cmcv_Rqst_Origin_Unit_Cde.setDdmHeader("ORIGIN/UNIT");
        cmcv_Request_Array_Grp = vw_cmcv.getRecord().newGroupInGroup("cmcv_Request_Array_Grp", "REQUEST-ARRAY-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cmcv_Action_Cde = cmcv_Request_Array_Grp.newFieldArrayInGroup("cmcv_Action_Cde", "ACTION-CDE", FieldType.STRING, 2, new DbsArrayController(1,10) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACTION_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cmcv_Wpid_Cde = cmcv_Request_Array_Grp.newFieldArrayInGroup("cmcv_Wpid_Cde", "WPID-CDE", FieldType.STRING, 6, new DbsArrayController(1,10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "WPID_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cmcv_Cntrct_NbrMuGroup = cmcv_Request_Array_Grp.newGroupInGroup("cmcv_Cntrct_NbrMuGroup", "CNTRCT_NBRMuGroup", RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, 
            "CWF_MCSS_CALLS_CNTRCT_NBR", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cmcv_Cntrct_Nbr = cmcv_Cntrct_NbrMuGroup.newFieldArrayInGroup("cmcv_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8, new DbsArrayController(1,10,1,10) 
            , RepeatingFieldStrategy.SubTableFieldArray, "CNTRCT_NBR", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cmcv_Rel_Log_Dte_Tme = cmcv_Request_Array_Grp.newFieldArrayInGroup("cmcv_Rel_Log_Dte_Tme", "REL-LOG-DTE-TME", FieldType.TIME, new DbsArrayController(1,10) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "REL_LOG_DTE_TME", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cmcv_Wpid_Validate_Ind = cmcv_Request_Array_Grp.newFieldArrayInGroup("cmcv_Wpid_Validate_Ind", "WPID-VALIDATE-IND", FieldType.STRING, 1, new DbsArrayController(1,10) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WPID_VALIDATE_IND", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cmcv_Dstntn_Unt_Cde = cmcv_Request_Array_Grp.newFieldArrayInGroup("cmcv_Dstntn_Unt_Cde", "DSTNTN-UNT-CDE", FieldType.STRING, 8, new DbsArrayController(1,10) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "DSTNTN_UNT_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cmcv_Corp_Status_Cde = cmcv_Request_Array_Grp.newFieldArrayInGroup("cmcv_Corp_Status_Cde", "CORP-STATUS-CDE", FieldType.STRING, 1, new DbsArrayController(1,10) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CORP_STATUS_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cmcv_Unit_Status_Cde = cmcv_Request_Array_Grp.newFieldArrayInGroup("cmcv_Unit_Status_Cde", "UNIT-STATUS-CDE", FieldType.STRING, 4, new DbsArrayController(1,10) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "UNIT_STATUS_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cmcv_Work_Req_Prty_Cde = cmcv_Request_Array_Grp.newFieldArrayInGroup("cmcv_Work_Req_Prty_Cde", "WORK-REQ-PRTY-CDE", FieldType.STRING, 1, new DbsArrayController(1,10) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WORK_REQ_PRTY_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cmcv_Request_Arry2_Grp = vw_cmcv.getRecord().newGroupInGroup("cmcv_Request_Arry2_Grp", "REQUEST-ARRY2-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CWF_MCSS_CALLS_REQUEST_ARRY2_GRP");
        cmcv_Prcssng_Tpe = cmcv_Request_Arry2_Grp.newFieldArrayInGroup("cmcv_Prcssng_Tpe", "PRCSSNG-TPE", FieldType.STRING, 1, new DbsArrayController(1,10) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PRCSSNG_TPE", "CWF_MCSS_CALLS_REQUEST_ARRY2_GRP");
        cmcv_Container_Id = vw_cmcv.getRecord().newFieldInGroup("cmcv_Container_Id", "CONTAINER-ID", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "CONTAINER_ID");
        cmcv_Doc_Ctgry_Cde = vw_cmcv.getRecord().newFieldInGroup("cmcv_Doc_Ctgry_Cde", "DOC-CTGRY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DOC_CTGRY_CDE");
        cmcv_Doc_Clss_Cde = vw_cmcv.getRecord().newFieldInGroup("cmcv_Doc_Clss_Cde", "DOC-CLSS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DOC_CLSS_CDE");
        cmcv_Doc_Type_Spcfc_Cde = vw_cmcv.getRecord().newFieldInGroup("cmcv_Doc_Type_Spcfc_Cde", "DOC-TYPE-SPCFC-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DOC_TYPE_SPCFC_CDE");
        cmcv_Doc_Txt = vw_cmcv.getRecord().newFieldArrayInGroup("cmcv_Doc_Txt", "DOC-TXT", FieldType.STRING, 80, new DbsArrayController(1,60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "DOC_TXT", "CWF_MCSS_CALLS_DOC_TEXT_AREA_GRP");
        cmcv_Error_Msg_Txt = vw_cmcv.getRecord().newFieldInGroup("cmcv_Error_Msg_Txt", "ERROR-MSG-TXT", FieldType.STRING, 60, RepeatingFieldStrategy.None, 
            "ERROR_MSG_TXT");
        cmcv_Error_Msg_Txt.setDdmHeader("ERROR/MSG.");
        cmcv_Wfo_All_Othr_Flds = vw_cmcv.getRecord().newGroupInGroup("cmcv_Wfo_All_Othr_Flds", "WFO-ALL-OTHR-FLDS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CWF_MCSS_CALLS_WFO_ALL_OTHR_FLDS");
        cmcv_Wfo_Othr_Flds = cmcv_Wfo_All_Othr_Flds.newFieldArrayInGroup("cmcv_Wfo_Othr_Flds", "WFO-OTHR-FLDS", FieldType.STRING, 100, new DbsArrayController(1,99) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WFO_OTHR_FLDS", "CWF_MCSS_CALLS_WFO_ALL_OTHR_FLDS");
        cmcv_Wfo_Othr_FldsRedef1 = vw_cmcv.getRecord().newGroupInGroup("cmcv_Wfo_Othr_FldsRedef1", "Redefines", cmcv_Wfo_Othr_Flds);
        cmcv_Wf_Ind = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Wf_Ind", "WF-IND", FieldType.STRING, 1, new DbsArrayController(1,10));
        cmcv_Ind_Tracking_Id = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Ind_Tracking_Id", "IND-TRACKING-ID", FieldType.STRING, 20, new DbsArrayController(1,
            10));
        cmcv_Inst_Tracking_Id = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Inst_Tracking_Id", "INST-TRACKING-ID", FieldType.STRING, 20, new DbsArrayController(1,
            10));
        cmcv_Cabinet_Id = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Cabinet_Id", "CABINET-ID", FieldType.STRING, 17, new DbsArrayController(1,
            10));
        cmcv_Cabinet_Level = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Cabinet_Level", "CABINET-LEVEL", FieldType.STRING, 1, new DbsArrayController(1,
            10));
        cmcv_Empl_Racf_Id = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8, new DbsArrayController(1,
            10));
        cmcv_Step = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Step", "STEP", FieldType.STRING, 6, new DbsArrayController(1,10));
        cmcv_Due_Dte = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Due_Dte", "DUE-DTE", FieldType.NUMERIC, 8, new DbsArrayController(1,10));
        cmcv_Client_App_Cde = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Client_App_Cde", "CLIENT-APP-CDE", FieldType.STRING, 8, new DbsArrayController(1,
            10));
        cmcv_Elctrnc_Fldr_Ind = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 1, new DbsArrayController(1,
            10));
        cmcv_Rqst_Orgn_Cde = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1, new DbsArrayController(1,
            10));
        cmcv_Topic = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Topic", "TOPIC", FieldType.STRING, 20, new DbsArrayController(1,10));
        cmcv_Processed_Work_Rqsts_Grp = cmcv_Wfo_Othr_FldsRedef1.newGroupArrayInGroup("cmcv_Processed_Work_Rqsts_Grp", "PROCESSED-WORK-RQSTS-GRP", new 
            DbsArrayController(1,11));
        cmcv_Wfo_Othr_Flds = cmcv_Wfo_All_Othr_Flds.newFieldArrayInGroup("cmcv_Wfo_Othr_Flds", "WFO-OTHR-FLDS", FieldType.STRING, 100, new DbsArrayController(1,99) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WFO_OTHR_FLDS", "CWF_MCSS_CALLS_WFO_ALL_OTHR_FLDS");
        cmcv_Processed_Work_Rqsts_ElmtRedef2 = vw_cmcv.getRecord().newGroupInGroup("cmcv_Processed_Work_Rqsts_ElmtRedef2", "Redefines", cmcv_Processed_Work_Rqsts_Elmt);
        cmcv_P_Rqst_Id = cmcv_Processed_Work_Rqsts_ElmtRedef2.newFieldInGroup("cmcv_P_Rqst_Id", "P-RQST-ID", FieldType.STRING, 17);
        cmcv_P_Rqst_IdRedef3 = vw_cmcv.getRecord().newGroupInGroup("cmcv_P_Rqst_IdRedef3", "Redefines", cmcv_P_Rqst_Id);
        cmcv_P_Tiaa_Rcvd_Date = cmcv_P_Rqst_IdRedef3.newFieldInGroup("cmcv_P_Tiaa_Rcvd_Date", "P-TIAA-RCVD-DATE", FieldType.NUMERIC, 8);
        cmcv_P_Case_Id = cmcv_P_Rqst_IdRedef3.newFieldInGroup("cmcv_P_Case_Id", "P-CASE-ID", FieldType.STRING, 1);
        cmcv_P_Wpid = cmcv_P_Rqst_IdRedef3.newFieldInGroup("cmcv_P_Wpid", "P-WPID", FieldType.STRING, 6);
        cmcv_P_Sub_Rqst_Cde = cmcv_P_Rqst_IdRedef3.newFieldInGroup("cmcv_P_Sub_Rqst_Cde", "P-SUB-RQST-CDE", FieldType.STRING, 1);
        cmcv_P_Multi_Rqst_Ind = cmcv_P_Rqst_IdRedef3.newFieldInGroup("cmcv_P_Multi_Rqst_Ind", "P-MULTI-RQST-IND", FieldType.STRING, 1);
        cmcv_P_Rldt = cmcv_Processed_Work_Rqsts_ElmtRedef2.newFieldInGroup("cmcv_P_Rldt", "P-RLDT", FieldType.STRING, 15);
        cmcv_P_Doc_Group = cmcv_Processed_Work_Rqsts_ElmtRedef2.newFieldInGroup("cmcv_P_Doc_Group", "P-DOC-GROUP", FieldType.STRING, 54);
        cmcv_P_Doc_GroupRedef4 = vw_cmcv.getRecord().newGroupInGroup("cmcv_P_Doc_GroupRedef4", "Redefines", cmcv_P_Doc_Group);
        cmcv_P_Dstntn_Unt_Cde = cmcv_P_Doc_GroupRedef4.newFieldInGroup("cmcv_P_Dstntn_Unt_Cde", "P-DSTNTN-UNT-CDE", FieldType.STRING, 8);
        cmcv_P_Fil1 = cmcv_P_Doc_GroupRedef4.newFieldInGroup("cmcv_P_Fil1", "P-FIL1", FieldType.STRING, 2);
        cmcv_P_Topic = cmcv_P_Doc_GroupRedef4.newFieldInGroup("cmcv_P_Topic", "P-TOPIC", FieldType.STRING, 20);
        cmcv_P_Fil2 = cmcv_P_Doc_GroupRedef4.newFieldInGroup("cmcv_P_Fil2", "P-FIL2", FieldType.STRING, 2);
        cmcv_P_Wpid_Cde = cmcv_P_Doc_GroupRedef4.newFieldInGroup("cmcv_P_Wpid_Cde", "P-WPID-CDE", FieldType.STRING, 6);
        cmcv_P_Fil3 = cmcv_P_Doc_GroupRedef4.newFieldInGroup("cmcv_P_Fil3", "P-FIL3", FieldType.STRING, 2);
        cmcv_P_Corp_Status = cmcv_P_Doc_GroupRedef4.newFieldInGroup("cmcv_P_Corp_Status", "P-CORP-STATUS", FieldType.STRING, 5);
        cmcv_P_Fil4 = cmcv_P_Doc_GroupRedef4.newFieldInGroup("cmcv_P_Fil4", "P-FIL4", FieldType.STRING, 2);
        cmcv_P_Wf_Ind = cmcv_P_Doc_GroupRedef4.newFieldInGroup("cmcv_P_Wf_Ind", "P-WF-IND", FieldType.STRING, 1);
        cmcv_P_Fil5 = cmcv_P_Doc_GroupRedef4.newFieldInGroup("cmcv_P_Fil5", "P-FIL5", FieldType.STRING, 3);
        cmcv_P_Action_Cde = cmcv_P_Doc_GroupRedef4.newFieldInGroup("cmcv_P_Action_Cde", "P-ACTION-CDE", FieldType.STRING, 3);
        cmcv_P_Cabinet = cmcv_Processed_Work_Rqsts_ElmtRedef2.newFieldInGroup("cmcv_P_Cabinet", "P-CABINET", FieldType.STRING, 17);
        cmcv_P_CabinetRedef5 = vw_cmcv.getRecord().newGroupInGroup("cmcv_P_CabinetRedef5", "Redefines", cmcv_P_Cabinet);
        cmcv_P_Prefix = cmcv_P_CabinetRedef5.newFieldInGroup("cmcv_P_Prefix", "P-PREFIX", FieldType.STRING, 1);
        cmcv_P_Pin = cmcv_P_CabinetRedef5.newFieldInGroup("cmcv_P_Pin", "P-PIN", FieldType.NUMERIC, 12);
        cmcv_P_Cabinet_Level = cmcv_Processed_Work_Rqsts_ElmtRedef2.newFieldInGroup("cmcv_P_Cabinet_Level", "P-CABINET-LEVEL", FieldType.STRING, 1);
        cmcv_Processed_Text_Ind = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Processed_Text_Ind", "PROCESSED-TEXT-IND", FieldType.STRING, 1, 
            new DbsArrayController(1,10));
        cmcv_Part_Orig_Crte_Dte_Tme = cmcv_Wfo_Othr_FldsRedef1.newFieldInGroup("cmcv_Part_Orig_Crte_Dte_Tme", "PART-ORIG-CRTE-DTE-TME", FieldType.STRING, 
            15);
        cmcv_Inst_Orig_Crte_Dte_Tme = cmcv_Wfo_Othr_FldsRedef1.newFieldInGroup("cmcv_Inst_Orig_Crte_Dte_Tme", "INST-ORIG-CRTE-DTE-TME", FieldType.STRING, 
            15);
        cmcv_Nonp_Orig_Crte_Dte_Tme = cmcv_Wfo_Othr_FldsRedef1.newFieldInGroup("cmcv_Nonp_Orig_Crte_Dte_Tme", "NONP-ORIG-CRTE-DTE-TME", FieldType.STRING, 
            15);
        cmcv_Part_Orig_Document_Name = cmcv_Wfo_Othr_FldsRedef1.newFieldInGroup("cmcv_Part_Orig_Document_Name", "PART-ORIG-DOCUMENT-NAME", FieldType.STRING, 
            32);
        cmcv_Part_Orig_Document_NameRedef6 = vw_cmcv.getRecord().newGroupInGroup("cmcv_Part_Orig_Document_NameRedef6", "Redefines", cmcv_Part_Orig_Document_Name);
        cmcv_Part_Orig_Category = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Category", "PART-ORIG-CATEGORY", FieldType.STRING, 
            1);
        cmcv_Part_Orig_Class = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Class", "PART-ORIG-CLASS", FieldType.STRING, 3);
        cmcv_Part_Orig_Work_Prcss_Id = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Work_Prcss_Id", "PART-ORIG-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        cmcv_Part_Orig_Specific = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Specific", "PART-ORIG-SPECIFIC", FieldType.STRING, 
            4);
        cmcv_Part_Orig_Creation_Cc = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Creation_Cc", "PART-ORIG-CREATION-CC", FieldType.STRING, 
            1);
        cmcv_Part_Orig_Creation_Yymmdd = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Creation_Yymmdd", "PART-ORIG-CREATION-YYMMDD", 
            FieldType.STRING, 6);
        cmcv_Part_Orig_Direction = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Direction", "PART-ORIG-DIRECTION", FieldType.STRING, 
            1);
        cmcv_Part_Orig_Secured_Ind = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Secured_Ind", "PART-ORIG-SECURED-IND", FieldType.STRING, 
            1);
        cmcv_Part_Orig_Retention_Ind = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Retention_Ind", "PART-ORIG-RETENTION-IND", FieldType.STRING, 
            1);
        cmcv_Part_Orig_Format = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Format", "PART-ORIG-FORMAT", FieldType.STRING, 1);
        cmcv_Part_Orig_Tie_Breaker = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Tie_Breaker", "PART-ORIG-TIE-BREAKER", FieldType.STRING, 
            1);
        cmcv_Part_Orig_Copy_Ind = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Copy_Ind", "PART-ORIG-COPY-IND", FieldType.STRING, 
            1);
        cmcv_Part_Orig_Important_Ind = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Important_Ind", "PART-ORIG-IMPORTANT-IND", FieldType.STRING, 
            1);
        cmcv_Part_Orig_Package_Nbr = cmcv_Part_Orig_Document_NameRedef6.newFieldInGroup("cmcv_Part_Orig_Package_Nbr", "PART-ORIG-PACKAGE-NBR", FieldType.NUMERIC, 
            4);
        cmcv_Inst_Orig_Document_Name = cmcv_Wfo_Othr_FldsRedef1.newFieldInGroup("cmcv_Inst_Orig_Document_Name", "INST-ORIG-DOCUMENT-NAME", FieldType.STRING, 
            10);
        cmcv_Inst_Orig_Document_NameRedef7 = vw_cmcv.getRecord().newGroupInGroup("cmcv_Inst_Orig_Document_NameRedef7", "Redefines", cmcv_Inst_Orig_Document_Name);
        cmcv_Inst_Orig_Category = cmcv_Inst_Orig_Document_NameRedef7.newFieldInGroup("cmcv_Inst_Orig_Category", "INST-ORIG-CATEGORY", FieldType.STRING, 
            1);
        cmcv_Inst_Orig_Class = cmcv_Inst_Orig_Document_NameRedef7.newFieldInGroup("cmcv_Inst_Orig_Class", "INST-ORIG-CLASS", FieldType.STRING, 3);
        cmcv_Inst_Orig_Specific = cmcv_Inst_Orig_Document_NameRedef7.newFieldInGroup("cmcv_Inst_Orig_Specific", "INST-ORIG-SPECIFIC", FieldType.STRING, 
            4);
        cmcv_Inst_Orig_Direction = cmcv_Inst_Orig_Document_NameRedef7.newFieldInGroup("cmcv_Inst_Orig_Direction", "INST-ORIG-DIRECTION", FieldType.STRING, 
            1);
        cmcv_Inst_Orig_Format = cmcv_Inst_Orig_Document_NameRedef7.newFieldInGroup("cmcv_Inst_Orig_Format", "INST-ORIG-FORMAT", FieldType.STRING, 1);
        cmcv_Nonp_Orig_Document_Name = cmcv_Wfo_Othr_FldsRedef1.newFieldInGroup("cmcv_Nonp_Orig_Document_Name", "NONP-ORIG-DOCUMENT-NAME", FieldType.STRING, 
            10);
        cmcv_Nonp_Orig_Document_NameRedef8 = vw_cmcv.getRecord().newGroupInGroup("cmcv_Nonp_Orig_Document_NameRedef8", "Redefines", cmcv_Nonp_Orig_Document_Name);
        cmcv_Nonp_Orig_Category = cmcv_Nonp_Orig_Document_NameRedef8.newFieldInGroup("cmcv_Nonp_Orig_Category", "NONP-ORIG-CATEGORY", FieldType.STRING, 
            1);
        cmcv_Nonp_Orig_Class = cmcv_Nonp_Orig_Document_NameRedef8.newFieldInGroup("cmcv_Nonp_Orig_Class", "NONP-ORIG-CLASS", FieldType.STRING, 3);
        cmcv_Nonp_Orig_Specific = cmcv_Nonp_Orig_Document_NameRedef8.newFieldInGroup("cmcv_Nonp_Orig_Specific", "NONP-ORIG-SPECIFIC", FieldType.STRING, 
            4);
        cmcv_Nonp_Orig_Direction = cmcv_Nonp_Orig_Document_NameRedef8.newFieldInGroup("cmcv_Nonp_Orig_Direction", "NONP-ORIG-DIRECTION", FieldType.STRING, 
            1);
        cmcv_Nonp_Orig_Format = cmcv_Nonp_Orig_Document_NameRedef8.newFieldInGroup("cmcv_Nonp_Orig_Format", "NONP-ORIG-FORMAT", FieldType.STRING, 1);
        cmcv_Cntrct = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Cntrct", "CNTRCT", FieldType.STRING, 10, new DbsArrayController(1,10,1,10));
        cmcv_Processed2_Work_Rqsts_Grp = cmcv_Wfo_Othr_FldsRedef1.newGroupArrayInGroup("cmcv_Processed2_Work_Rqsts_Grp", "PROCESSED2-WORK-RQSTS-GRP", 
            new DbsArrayController(1,10));
        cmcv_Wfo_Othr_Flds = cmcv_Wfo_All_Othr_Flds.newFieldArrayInGroup("cmcv_Wfo_Othr_Flds", "WFO-OTHR-FLDS", FieldType.STRING, 100, new DbsArrayController(1,99) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WFO_OTHR_FLDS", "CWF_MCSS_CALLS_WFO_ALL_OTHR_FLDS");
        cmcv_P_Ati_Ind = cmcv_Wfo_Othr_FldsRedef1.newFieldInGroup("cmcv_P_Ati_Ind", "P-ATI-IND", FieldType.STRING, 1);
        cmcv_Lead_Class = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Lead_Class", "LEAD-CLASS", FieldType.STRING, 4, new DbsArrayController(1,
            10));
        cmcv_Express_Ind = cmcv_Wfo_Othr_FldsRedef1.newFieldInGroup("cmcv_Express_Ind", "EXPRESS-IND", FieldType.STRING, 1);
        cmcv_Mail_Cde = cmcv_Wfo_Othr_FldsRedef1.newFieldArrayInGroup("cmcv_Mail_Cde", "MAIL-CDE", FieldType.STRING, 2, new DbsArrayController(1,10));
        cmcv_Wfo_Othr_FldsRedef9 = vw_cmcv.getRecord().newGroupInGroup("cmcv_Wfo_Othr_FldsRedef9", "Redefines", cmcv_Wfo_Othr_Flds);
        cmcv_Dcmnt_Obj_Id = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_Dcmnt_Obj_Id", "DCMNT-OBJ-ID", FieldType.STRING, 20);
        cmcv_Mail_Item_No = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_Mail_Item_No", "MAIL-ITEM-NO", FieldType.STRING, 11);
        cmcv_T_Client_App_Cde = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_T_Client_App_Cde", "T-CLIENT-APP-CDE", FieldType.STRING, 8);
        cmcv_Image_Addrss = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_Image_Addrss", "IMAGE-ADDRSS", FieldType.STRING, 22);
        cmcv_Batch_Id = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_Batch_Id", "BATCH-ID", FieldType.DECIMAL, 10,2);
        cmcv_Source_Id = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_Source_Id", "SOURCE-ID", FieldType.STRING, 6);
        cmcv_Dcmnt_Direction = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_Dcmnt_Direction", "DCMNT-DIRECTION", FieldType.STRING, 1);
        cmcv_T_Wf_Ind = cmcv_Wfo_Othr_FldsRedef9.newFieldArrayInGroup("cmcv_T_Wf_Ind", "T-WF-IND", FieldType.STRING, 1, new DbsArrayController(1,35));
        cmcv_T_Ind_Tracking_Id = cmcv_Wfo_Othr_FldsRedef9.newFieldArrayInGroup("cmcv_T_Ind_Tracking_Id", "T-IND-TRACKING-ID", FieldType.STRING, 20, new 
            DbsArrayController(1,35));
        cmcv_T_Inst_Tracking_Id = cmcv_Wfo_Othr_FldsRedef9.newFieldArrayInGroup("cmcv_T_Inst_Tracking_Id", "T-INST-TRACKING-ID", FieldType.STRING, 20, 
            new DbsArrayController(1,35));
        cmcv_T_Cabinet_Id = cmcv_Wfo_Othr_FldsRedef9.newFieldArrayInGroup("cmcv_T_Cabinet_Id", "T-CABINET-ID", FieldType.STRING, 17, new DbsArrayController(1,
            35));
        cmcv_T_Cabinet_Level = cmcv_Wfo_Othr_FldsRedef9.newFieldArrayInGroup("cmcv_T_Cabinet_Level", "T-CABINET-LEVEL", FieldType.STRING, 1, new DbsArrayController(1,
            35));
        cmcv_Dcmnt_Text_2 = cmcv_Wfo_Othr_FldsRedef9.newFieldArrayInGroup("cmcv_Dcmnt_Text_2", "DCMNT-TEXT-2", FieldType.STRING, 80, new DbsArrayController(1,
            60));
        cmcv_Img_Start_Page_Nbr = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_Img_Start_Page_Nbr", "IMG-START-PAGE-NBR", FieldType.NUMERIC, 3);
        cmcv_Img_Total_Pages = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_Img_Total_Pages", "IMG-TOTAL-PAGES", FieldType.NUMERIC, 3);
        cmcv_T_Prcssd_Ind = cmcv_Wfo_Othr_FldsRedef9.newFieldArrayInGroup("cmcv_T_Prcssd_Ind", "T-PRCSSD-IND", FieldType.STRING, 1, new DbsArrayController(1,
            35));
        cmcv_T_P_Orig_Crte_Dte_Tme = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_T_P_Orig_Crte_Dte_Tme", "T-P-ORIG-CRTE-DTE-TME", FieldType.STRING, 
            15);
        cmcv_T_I_Orig_Crte_Dte_Tme = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_T_I_Orig_Crte_Dte_Tme", "T-I-ORIG-CRTE-DTE-TME", FieldType.STRING, 
            15);
        cmcv_T_N_Orig_Crte_Dte_Tme = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_T_N_Orig_Crte_Dte_Tme", "T-N-ORIG-CRTE-DTE-TME", FieldType.STRING, 
            15);
        cmcv_T_P_Orig_Document_Name = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_T_P_Orig_Document_Name", "T-P-ORIG-DOCUMENT-NAME", FieldType.STRING, 
            32);
        cmcv_T_P_Orig_Document_NameRedef10 = vw_cmcv.getRecord().newGroupInGroup("cmcv_T_P_Orig_Document_NameRedef10", "Redefines", cmcv_T_P_Orig_Document_Name);
        cmcv_T_P_Orig_Category = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Category", "T-P-ORIG-CATEGORY", FieldType.STRING, 1);
        cmcv_T_P_Orig_Class = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Class", "T-P-ORIG-CLASS", FieldType.STRING, 3);
        cmcv_T_P_Orig_Work_Prcss_Id = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Work_Prcss_Id", "T-P-ORIG-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        cmcv_T_P_Orig_Specific = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Specific", "T-P-ORIG-SPECIFIC", FieldType.STRING, 4);
        cmcv_T_P_Orig_Creation_Cc = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Creation_Cc", "T-P-ORIG-CREATION-CC", FieldType.STRING, 
            1);
        cmcv_T_P_Orig_Creation_Yymmdd = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Creation_Yymmdd", "T-P-ORIG-CREATION-YYMMDD", 
            FieldType.STRING, 6);
        cmcv_T_P_Orig_Direction = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Direction", "T-P-ORIG-DIRECTION", FieldType.STRING, 
            1);
        cmcv_T_P_Orig_Secured_Ind = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Secured_Ind", "T-P-ORIG-SECURED-IND", FieldType.STRING, 
            1);
        cmcv_T_P_Orig_Retention_Ind = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Retention_Ind", "T-P-ORIG-RETENTION-IND", FieldType.STRING, 
            1);
        cmcv_T_P_Orig_Format = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Format", "T-P-ORIG-FORMAT", FieldType.STRING, 1);
        cmcv_T_P_Orig_Tie_Breaker = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Tie_Breaker", "T-P-ORIG-TIE-BREAKER", FieldType.STRING, 
            1);
        cmcv_T_P_Orig_Copy_Ind = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Copy_Ind", "T-P-ORIG-COPY-IND", FieldType.STRING, 1);
        cmcv_T_P_Orig_Important_Ind = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Important_Ind", "T-P-ORIG-IMPORTANT-IND", FieldType.STRING, 
            1);
        cmcv_T_P_Orig_Package_Nbr = cmcv_T_P_Orig_Document_NameRedef10.newFieldInGroup("cmcv_T_P_Orig_Package_Nbr", "T-P-ORIG-PACKAGE-NBR", FieldType.NUMERIC, 
            4);
        cmcv_T_I_Orig_Document_Name = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_T_I_Orig_Document_Name", "T-I-ORIG-DOCUMENT-NAME", FieldType.STRING, 
            10);
        cmcv_T_I_Orig_Document_NameRedef11 = vw_cmcv.getRecord().newGroupInGroup("cmcv_T_I_Orig_Document_NameRedef11", "Redefines", cmcv_T_I_Orig_Document_Name);
        cmcv_T_I_Orig_Category = cmcv_T_I_Orig_Document_NameRedef11.newFieldInGroup("cmcv_T_I_Orig_Category", "T-I-ORIG-CATEGORY", FieldType.STRING, 1);
        cmcv_T_I_Orig_Class = cmcv_T_I_Orig_Document_NameRedef11.newFieldInGroup("cmcv_T_I_Orig_Class", "T-I-ORIG-CLASS", FieldType.STRING, 3);
        cmcv_T_I_Orig_Specific = cmcv_T_I_Orig_Document_NameRedef11.newFieldInGroup("cmcv_T_I_Orig_Specific", "T-I-ORIG-SPECIFIC", FieldType.STRING, 4);
        cmcv_T_I_Orig_Direction = cmcv_T_I_Orig_Document_NameRedef11.newFieldInGroup("cmcv_T_I_Orig_Direction", "T-I-ORIG-DIRECTION", FieldType.STRING, 
            1);
        cmcv_T_I_Orig_Format = cmcv_T_I_Orig_Document_NameRedef11.newFieldInGroup("cmcv_T_I_Orig_Format", "T-I-ORIG-FORMAT", FieldType.STRING, 1);
        cmcv_T_N_Orig_Document_Name = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_T_N_Orig_Document_Name", "T-N-ORIG-DOCUMENT-NAME", FieldType.STRING, 
            10);
        cmcv_T_N_Orig_Document_NameRedef12 = vw_cmcv.getRecord().newGroupInGroup("cmcv_T_N_Orig_Document_NameRedef12", "Redefines", cmcv_T_N_Orig_Document_Name);
        cmcv_T_N_Orig_Category = cmcv_T_N_Orig_Document_NameRedef12.newFieldInGroup("cmcv_T_N_Orig_Category", "T-N-ORIG-CATEGORY", FieldType.STRING, 1);
        cmcv_T_N_Orig_Class = cmcv_T_N_Orig_Document_NameRedef12.newFieldInGroup("cmcv_T_N_Orig_Class", "T-N-ORIG-CLASS", FieldType.STRING, 3);
        cmcv_T_N_Orig_Specific = cmcv_T_N_Orig_Document_NameRedef12.newFieldInGroup("cmcv_T_N_Orig_Specific", "T-N-ORIG-SPECIFIC", FieldType.STRING, 4);
        cmcv_T_N_Orig_Direction = cmcv_T_N_Orig_Document_NameRedef12.newFieldInGroup("cmcv_T_N_Orig_Direction", "T-N-ORIG-DIRECTION", FieldType.STRING, 
            1);
        cmcv_T_N_Orig_Format = cmcv_T_N_Orig_Document_NameRedef12.newFieldInGroup("cmcv_T_N_Orig_Format", "T-N-ORIG-FORMAT", FieldType.STRING, 1);
        cmcv_T_Total_Nbr_Image_Processed = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_T_Total_Nbr_Image_Processed", "T-TOTAL-NBR-IMAGE-PROCESSED", 
            FieldType.NUMERIC, 2);
        cmcv_T_Image_Mail_Item_Nbr = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_T_Image_Mail_Item_Nbr", "T-IMAGE-MAIL-ITEM-NBR", FieldType.NUMERIC, 
            11);
        cmcv_T_Image_Start_Page_Nbr = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_T_Image_Start_Page_Nbr", "T-IMAGE-START-PAGE-NBR", FieldType.NUMERIC, 
            2);
        cmcv_T_Image_End_Page_Nbr = cmcv_Wfo_Othr_FldsRedef9.newFieldInGroup("cmcv_T_Image_End_Page_Nbr", "T-IMAGE-END-PAGE-NBR", FieldType.NUMERIC, 2);
        cmcv_Tracking_Rcrd_NbrMuGroup = vw_cmcv.getRecord().newGroupInGroup("cmcv_Tracking_Rcrd_NbrMuGroup", "TRACKING_RCRD_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CWF_MCSS_CALLS_TRACKING_RCRD_NBR");
        cmcv_Tracking_Rcrd_Nbr = cmcv_Tracking_Rcrd_NbrMuGroup.newFieldArrayInGroup("cmcv_Tracking_Rcrd_Nbr", "TRACKING-RCRD-NBR", FieldType.STRING, 20, 
            new DbsArrayController(1,10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TRACKING_RCRD_NBR");
        cmcv_Wfo_Ind = vw_cmcv.getRecord().newFieldInGroup("cmcv_Wfo_Ind", "WFO-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "WFO_IND");
        vw_cmcv.setUniquePeList();

        this.setRecordName("LdaWfol4500");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cmcv.reset();
    }

    // Constructor
    public LdaWfol4500() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
