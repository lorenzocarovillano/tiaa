/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:58:59 PM
**        * FROM NATURAL PROGRAM : Mihp0415
************************************************************
**        * FILE NAME            : Mihp0415.java
**        * CLASS NAME           : Mihp0415
**        * INSTANCE NAME        : Mihp0415
************************************************************
************************************************************************
* PROGRAM  : MIHP0415
* SYSTEM   : WORK HISTORY OBJECT CONVERSION
* TITLE    : UPDATE RUN CONTROL FOR THE MIT VS. WHO COMPARISON
* GENERATED: JUL 10,96 AT 13:41 PM
* FUNCTION : THIS PROGRAM UPDATES THE RUN CONTROL FOR THE MIT VS. WHO
*          : PROCESS TO COMPLETED AFTER SUCCESSFUL RUN OF THE REPORT.
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mihp0415 extends BLNatBase
{
    // Data Areas
    private LdaMihl0210 ldaMihl0210;
    private LdaMihl0181 ldaMihl0181;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Who_Support_Tbl_Histogram;
    private DbsField cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key;
    private DbsField pnd_Tbl_Prime_Key_From;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_1;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_2;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key_To;

    private DbsGroup pnd_Tbl_Prime_Key_To__R_Field_3;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_To__R_Field_4;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaMihl0210 = new LdaMihl0210();
        registerRecord(ldaMihl0210);
        registerRecord(ldaMihl0210.getVw_cwf_Who_Support_Tbl());
        ldaMihl0181 = new LdaMihl0181();
        registerRecord(ldaMihl0181);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Who_Support_Tbl_Histogram = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Support_Tbl_Histogram", "CWF-WHO-SUPPORT-TBL-HISTOGRAM"), 
            "CWF_WHO_SUPPORT_TBL", "CWF_WHO_SUPPORT");
        cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key = vw_cwf_Who_Support_Tbl_Histogram.getRecord().newFieldInGroup("cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key", 
            "TBL-PRIME-KEY", FieldType.STRING, 53, RepeatingFieldStrategy.None, "TBL_PRIME_KEY");
        cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key.setDdmHeader("RECORD KEY");
        cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Who_Support_Tbl_Histogram);

        pnd_Tbl_Prime_Key_From = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_From", "#TBL-PRIME-KEY-FROM", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_From__R_Field_1 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_From__R_Field_1", "REDEFINE", pnd_Tbl_Prime_Key_From);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_From__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_From__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_From__R_Field_2 = pnd_Tbl_Prime_Key_From__R_Field_1.newGroupInGroup("pnd_Tbl_Prime_Key_From__R_Field_2", "REDEFINE", pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status = pnd_Tbl_Prime_Key_From__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Run_Status", "#RUN-STATUS", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key_From__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 15);
        pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 14);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_From__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_To = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_To", "#TBL-PRIME-KEY-TO", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_To__R_Field_3 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_To__R_Field_3", "REDEFINE", pnd_Tbl_Prime_Key_To);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_To__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_To__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_To__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_To__R_Field_4 = pnd_Tbl_Prime_Key_To__R_Field_3.newGroupInGroup("pnd_Tbl_Prime_Key_To__R_Field_4", "REDEFINE", pnd_Tbl_Prime_Key_To_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status = pnd_Tbl_Prime_Key_To__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Run_Status", "#RUN-STATUS", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key_To__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 15);
        pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_To__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 14);
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_To__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Who_Support_Tbl_Histogram.reset();

        ldaMihl0210.initializeValues();
        ldaMihl0181.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Mihp0415() throws Exception
    {
        super("Mihp0415");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("MIHP0415", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 64;//Natural: FORMAT ( 1 ) LS = 132 PS = 64
        //*  CHECK TO SEE IF AN ACTIVE RECORD EXISTS
        //*  -----------------------------------------------
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind.setValue(ldaMihl0181.getMihl0181_Tbl_Scrty_Level_Ind());                                                           //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-SCRTY-LEVEL-IND := MIHL0181.TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme.setValue(ldaMihl0181.getMihl0181_Mit_Vs_Who_Next_Run());                                                                 //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-TABLE-NME := MIHL0181.MIT-VS-WHO-NEXT-RUN
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status.setValue(ldaMihl0181.getMihl0181_Active_Status());                                                                          //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#RUN-STATUS := MIHL0181.ACTIVE-STATUS
        pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement.setValue(0);                                                                                               //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#STARTING-DATE-9S-COMPLEMENT := 0
        pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field.setValue(" ");                                                                                                   //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#REST-OF-TBL-KEY-FIELD := ' '
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind.setValue(" ");                                                                                                           //Natural: ASSIGN #TBL-PRIME-KEY-FROM.#TBL-ACTVE-IND := ' '
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Scrty_Level_Ind.setValue(ldaMihl0181.getMihl0181_Tbl_Scrty_Level_Ind());                                                             //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-SCRTY-LEVEL-IND := MIHL0181.TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Table_Nme.setValue(ldaMihl0181.getMihl0181_Mit_Vs_Who_Next_Run());                                                                   //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-TABLE-NME := MIHL0181.MIT-VS-WHO-NEXT-RUN
        pnd_Tbl_Prime_Key_To_Pnd_Run_Status.setValue(ldaMihl0181.getMihl0181_Active_Status());                                                                            //Natural: ASSIGN #TBL-PRIME-KEY-TO.#RUN-STATUS := MIHL0181.ACTIVE-STATUS
        pnd_Tbl_Prime_Key_To_Pnd_Starting_Date_9s_Complement.setValue(999999999999999L);                                                                                  //Natural: ASSIGN #TBL-PRIME-KEY-TO.#STARTING-DATE-9S-COMPLEMENT := 999999999999999
        pnd_Tbl_Prime_Key_To_Pnd_Rest_Of_Tbl_Key_Field.setValue("99999999999999");                                                                                        //Natural: ASSIGN #TBL-PRIME-KEY-TO.#REST-OF-TBL-KEY-FIELD := '99999999999999'
        pnd_Tbl_Prime_Key_To_Pnd_Tbl_Actve_Ind.setValue("9");                                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY-TO.#TBL-ACTVE-IND := '9'
        vw_cwf_Who_Support_Tbl_Histogram.createHistogram                                                                                                                  //Natural: HISTOGRAM CWF-WHO-SUPPORT-TBL-HISTOGRAM FOR TBL-PRIME-KEY FROM #TBL-PRIME-KEY-FROM THRU #TBL-PRIME-KEY-TO
        (
        "ACTIVE_HISTOGRAM",
        "TBL_PRIME_KEY",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key_From, "And", WcType.WITH) ,
        new Wc("TBL_PRIME_KEY", "<=", pnd_Tbl_Prime_Key_To, WcType.WITH) }
        );
        ACTIVE_HISTOGRAM:
        while (condition(vw_cwf_Who_Support_Tbl_Histogram.readNextRow("ACTIVE_HISTOGRAM")))
        {
            if (condition(vw_cwf_Who_Support_Tbl_Histogram.getAstNUMBER().greater(1)))                                                                                    //Natural: IF *NUMBER ( ACTIVE-HISTOGRAM. ) GT 1
            {
                getReports().write(1, "Program",Global.getPROGRAM(),"- More than one active control records","exist");                                                    //Natural: WRITE ( 1 ) 'Program' *PROGRAM '- More than one active control records' 'exist'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(4);  if (true) return;                                                                                                                  //Natural: TERMINATE 4
                //* (0690)
            }                                                                                                                                                             //Natural: END-IF
            //*  IF AN ACTIVE RECORD IS FOUND, UPDATE THE RUN STATUS TO COMPLETE.
            //*  ---------------------------------------------------------------------
            pnd_Tbl_Prime_Key_From.setValue(cwf_Who_Support_Tbl_Histogram_Tbl_Prime_Key);                                                                                 //Natural: ASSIGN #TBL-PRIME-KEY-FROM := CWF-WHO-SUPPORT-TBL-HISTOGRAM.TBL-PRIME-KEY
            ldaMihl0210.getVw_cwf_Who_Support_Tbl().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) CWF-WHO-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY-FROM
            (
            "ACTIVE_FIND",
            new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key_From, WcType.WITH) },
            1
            );
            ACTIVE_FIND:
            while (condition(ldaMihl0210.getVw_cwf_Who_Support_Tbl().readNextRow("ACTIVE_FIND")))
            {
                ldaMihl0210.getVw_cwf_Who_Support_Tbl().setIfNotFoundControlFlag(false);
                ldaMihl0210.getCwf_Who_Support_Tbl_Run_Status().setValue(ldaMihl0181.getMihl0181_Completed_Status());                                                     //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.RUN-STATUS := MIHL0181.COMPLETED-STATUS
                ldaMihl0210.getCwf_Who_Support_Tbl_Tbl_Updte_Dte_Tme().setValue(Global.getTIMX());                                                                        //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.TBL-UPDTE-DTE-TME := *TIMX
                ldaMihl0210.getCwf_Who_Support_Tbl_Tbl_Updte_Oprtr_Cde().setValue("CWFB3020");                                                                            //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE := 'CWFB3020'
                ldaMihl0210.getVw_cwf_Who_Support_Tbl().updateDBRow("ACTIVE_FIND");                                                                                       //Natural: UPDATE ( ACTIVE-FIND. )
                //* (ACTIVE-FIND.)
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("ACTIVE_HISTOGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break ACTIVE_HISTOGRAM;                                                                                                                             //Natural: ESCAPE BOTTOM ( ACTIVE-HISTOGRAM. )
            //* (ACTIVE-HISTOGRAM.)
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        if (condition(vw_cwf_Who_Support_Tbl_Histogram.getAstCOUNTER().greater(1)))                                                                                       //Natural: IF *COUNTER ( ACTIVE-HISTOGRAM. ) GT 1
        {
            getReports().write(1, "Program",Global.getPROGRAM(),"- More than one active control record","exist");                                                         //Natural: WRITE ( 1 ) 'Program' *PROGRAM '- More than one active control record' 'exist'
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
            //* (0910)
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF THE PROGRAM HAS GONE THROUGH SUCCESSFULLY, COMMIT THE TRANSACTION.
        //*  ---------------------------------------------------------------------
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  ON NATURAL RUN TIME ERROR, RETURN WITH AN APPROPRIATE ERROR MESSAGE
        //*  ===================================================================
        //*                                                                                                                                                               //Natural: ON ERROR
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(1, "NATURAL ERROR",Global.getERROR_NR(),"IN",Global.getPROGRAM(),"....LINE",Global.getERROR_LINE());                                           //Natural: WRITE ( 1 ) 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
        //* (1040)
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=64");
        Global.format(1, "LS=132 PS=64");
    }
}
