/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:46:53 PM
**        * FROM NATURAL PROGRAM : Wfob4500
************************************************************
**        * FILE NAME            : Wfob4500.java
**        * CLASS NAME           : Wfob4500
**        * INSTANCE NAME        : Wfob4500
************************************************************
************************************************************************
* PROGRAM  : WFOB4500
* SYSTEM   : PROJCWF
* TITLE    : PRINT CWF-MCSS-CALLS RECORDS BY CLIENT APPLICATION CODE
* FUNCTION : THIS PROGRAM READS THE CWF-MCSS-CALLS FILE AND PRODUCES
*            THE REPORT
*
* HISTORY
* --------
* 12/13/00 EPM DESCRIPTION OF W,T AND K RECORDS WERE INCLUDED.
* 01/17/01 EPM RESTOWED DUE TO INCLUSION OF EXPRESS IND IN MAPS
*              WFOF4501, WFOF4504, WFOF4511, WFOF4521 & WFOF4531;
*              ALSO ADDED EXPRESS-IND IN SORT FIELD.
* 02/23/2017 - SARKAB - PIN EXPANSION - AUG 2017 <STOW ONLY>
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Wfob4500 extends BLNatBase
{
    // Data Areas
    private LdaWfol4500 ldaWfol4500;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Program;
    private DbsField pnd_Tot_Cnt;
    private DbsField pnd_Client_Tot_Cnt;
    private DbsField pnd_Rec_T_Cnt;
    private DbsField pnd_Rec_W_Cnt;
    private DbsField pnd_Rec_K_Cnt;
    private DbsField pnd_Wrk_Rqst_Idx;
    private DbsField pnd_Text_Idx;
    private DbsField pnd_Hold_Cont_Id;
    private DbsField pnd_Curr_Cont_Id;
    private DbsField pnd_Hold_Client_App_Cde;
    private DbsField pnd_Temp_Client_App_Cde;
    private DbsField pnd_Save_Client_App_Cde;
    private DbsField pnd_Succ_Cnt;
    private DbsField pnd_Succ_W;
    private DbsField pnd_Succ_T;
    private DbsField pnd_Succ_K;
    private DbsField pnd_Client_Succ_Cont_Cnt;
    private DbsField pnd_Client_Succ_Cnt;
    private DbsField pnd_Client_Succ_W;
    private DbsField pnd_Client_Succ_T;
    private DbsField pnd_Client_Succ_K;
    private DbsField pnd_Init_Cnt;
    private DbsField pnd_Init_W;
    private DbsField pnd_Init_T;
    private DbsField pnd_Init_K;
    private DbsField pnd_Client_Init_Cont_Cnt;
    private DbsField pnd_Client_Init_Cnt;
    private DbsField pnd_Client_Init_W;
    private DbsField pnd_Client_Init_T;
    private DbsField pnd_Client_Init_K;
    private DbsField pnd_Error_Cnt;
    private DbsField pnd_Error_W;
    private DbsField pnd_Error_T;
    private DbsField pnd_Error_K;
    private DbsField pnd_Client_Error_Cont_Cnt;
    private DbsField pnd_Client_Error_Cnt;
    private DbsField pnd_Client_Error_W;
    private DbsField pnd_Client_Error_T;
    private DbsField pnd_Client_Error_K;
    private DbsField pnd_Save_Isn;
    private DbsField pnd_Rec_Stamp_Dte_Tme;
    private DbsField pnd_Hold_Stamp_Tme;
    private DbsField pnd_Client_Succ_Page_Nbr;
    private DbsField pnd_Client_Init_Page_Nbr;
    private DbsField pnd_Client_Err_Page_Nbr;
    private DbsField pnd_Init_Error_Cont;
    private DbsField pnd_Succ_W_Head_Printed;
    private DbsField pnd_Init_W_Head_Printed;
    private DbsField pnd_Error_W_Head_Printed;
    private DbsField pnd_Succ_T_Head_Printed;
    private DbsField pnd_Init_T_Head_Printed;
    private DbsField pnd_Error_T_Head_Printed;
    private DbsField pnd_Succ_K_Head_Printed;
    private DbsField pnd_Init_K_Head_Printed;
    private DbsField pnd_Error_K_Head_Printed;
    private DbsField pnd_Cont_Succ_Head_Printed;
    private DbsField pnd_Cont_Init_Head_Printed;
    private DbsField pnd_Cont_Error_Head_Printed;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_1;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Lvl;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Name;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Fld;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actv_Ind;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Tbl_Rpt_Date_Tme;

    private DbsGroup cwf_Support_Tbl__R_Field_3;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_Dte;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tracking_Type_Key;

    private DbsGroup pnd_Tracking_Type_Key__R_Field_4;
    private DbsField pnd_Tracking_Type_Key_Pnd_T_Container_Id;
    private DbsField pnd_Tracking_Type_Key_Pnd_T_Rcrd_Type_Cde;
    private DbsField pnd_Tracking_Type_Key_Pnd_T_Wfo_Ind;
    private DbsField pnd_Tracking_Type_Key_Pnd_T_Tracking_Rcrd_Nbr;
    private DbsField pnd_Date_Time;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaWfol4500 = new LdaWfol4500();
        registerRecord(ldaWfol4500);
        registerRecord(ldaWfol4500.getVw_cmcv());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Tot_Cnt = localVariables.newFieldInRecord("pnd_Tot_Cnt", "#TOT-CNT", FieldType.NUMERIC, 7);
        pnd_Client_Tot_Cnt = localVariables.newFieldInRecord("pnd_Client_Tot_Cnt", "#CLIENT-TOT-CNT", FieldType.NUMERIC, 7);
        pnd_Rec_T_Cnt = localVariables.newFieldInRecord("pnd_Rec_T_Cnt", "#REC-T-CNT", FieldType.NUMERIC, 7);
        pnd_Rec_W_Cnt = localVariables.newFieldInRecord("pnd_Rec_W_Cnt", "#REC-W-CNT", FieldType.NUMERIC, 7);
        pnd_Rec_K_Cnt = localVariables.newFieldInRecord("pnd_Rec_K_Cnt", "#REC-K-CNT", FieldType.NUMERIC, 7);
        pnd_Wrk_Rqst_Idx = localVariables.newFieldInRecord("pnd_Wrk_Rqst_Idx", "#WRK-RQST-IDX", FieldType.NUMERIC, 7);
        pnd_Text_Idx = localVariables.newFieldInRecord("pnd_Text_Idx", "#TEXT-IDX", FieldType.NUMERIC, 7);
        pnd_Hold_Cont_Id = localVariables.newFieldInRecord("pnd_Hold_Cont_Id", "#HOLD-CONT-ID", FieldType.STRING, 20);
        pnd_Curr_Cont_Id = localVariables.newFieldInRecord("pnd_Curr_Cont_Id", "#CURR-CONT-ID", FieldType.STRING, 20);
        pnd_Hold_Client_App_Cde = localVariables.newFieldInRecord("pnd_Hold_Client_App_Cde", "#HOLD-CLIENT-APP-CDE", FieldType.STRING, 8);
        pnd_Temp_Client_App_Cde = localVariables.newFieldInRecord("pnd_Temp_Client_App_Cde", "#TEMP-CLIENT-APP-CDE", FieldType.STRING, 8);
        pnd_Save_Client_App_Cde = localVariables.newFieldInRecord("pnd_Save_Client_App_Cde", "#SAVE-CLIENT-APP-CDE", FieldType.STRING, 8);
        pnd_Succ_Cnt = localVariables.newFieldInRecord("pnd_Succ_Cnt", "#SUCC-CNT", FieldType.NUMERIC, 7);
        pnd_Succ_W = localVariables.newFieldInRecord("pnd_Succ_W", "#SUCC-W", FieldType.NUMERIC, 7);
        pnd_Succ_T = localVariables.newFieldInRecord("pnd_Succ_T", "#SUCC-T", FieldType.NUMERIC, 7);
        pnd_Succ_K = localVariables.newFieldInRecord("pnd_Succ_K", "#SUCC-K", FieldType.NUMERIC, 7);
        pnd_Client_Succ_Cont_Cnt = localVariables.newFieldInRecord("pnd_Client_Succ_Cont_Cnt", "#CLIENT-SUCC-CONT-CNT", FieldType.NUMERIC, 7);
        pnd_Client_Succ_Cnt = localVariables.newFieldInRecord("pnd_Client_Succ_Cnt", "#CLIENT-SUCC-CNT", FieldType.NUMERIC, 7);
        pnd_Client_Succ_W = localVariables.newFieldInRecord("pnd_Client_Succ_W", "#CLIENT-SUCC-W", FieldType.NUMERIC, 7);
        pnd_Client_Succ_T = localVariables.newFieldInRecord("pnd_Client_Succ_T", "#CLIENT-SUCC-T", FieldType.NUMERIC, 7);
        pnd_Client_Succ_K = localVariables.newFieldInRecord("pnd_Client_Succ_K", "#CLIENT-SUCC-K", FieldType.NUMERIC, 7);
        pnd_Init_Cnt = localVariables.newFieldInRecord("pnd_Init_Cnt", "#INIT-CNT", FieldType.NUMERIC, 7);
        pnd_Init_W = localVariables.newFieldInRecord("pnd_Init_W", "#INIT-W", FieldType.NUMERIC, 7);
        pnd_Init_T = localVariables.newFieldInRecord("pnd_Init_T", "#INIT-T", FieldType.NUMERIC, 7);
        pnd_Init_K = localVariables.newFieldInRecord("pnd_Init_K", "#INIT-K", FieldType.NUMERIC, 7);
        pnd_Client_Init_Cont_Cnt = localVariables.newFieldInRecord("pnd_Client_Init_Cont_Cnt", "#CLIENT-INIT-CONT-CNT", FieldType.NUMERIC, 7);
        pnd_Client_Init_Cnt = localVariables.newFieldInRecord("pnd_Client_Init_Cnt", "#CLIENT-INIT-CNT", FieldType.NUMERIC, 7);
        pnd_Client_Init_W = localVariables.newFieldInRecord("pnd_Client_Init_W", "#CLIENT-INIT-W", FieldType.NUMERIC, 7);
        pnd_Client_Init_T = localVariables.newFieldInRecord("pnd_Client_Init_T", "#CLIENT-INIT-T", FieldType.NUMERIC, 7);
        pnd_Client_Init_K = localVariables.newFieldInRecord("pnd_Client_Init_K", "#CLIENT-INIT-K", FieldType.NUMERIC, 7);
        pnd_Error_Cnt = localVariables.newFieldInRecord("pnd_Error_Cnt", "#ERROR-CNT", FieldType.NUMERIC, 7);
        pnd_Error_W = localVariables.newFieldInRecord("pnd_Error_W", "#ERROR-W", FieldType.NUMERIC, 7);
        pnd_Error_T = localVariables.newFieldInRecord("pnd_Error_T", "#ERROR-T", FieldType.NUMERIC, 7);
        pnd_Error_K = localVariables.newFieldInRecord("pnd_Error_K", "#ERROR-K", FieldType.NUMERIC, 7);
        pnd_Client_Error_Cont_Cnt = localVariables.newFieldInRecord("pnd_Client_Error_Cont_Cnt", "#CLIENT-ERROR-CONT-CNT", FieldType.NUMERIC, 7);
        pnd_Client_Error_Cnt = localVariables.newFieldInRecord("pnd_Client_Error_Cnt", "#CLIENT-ERROR-CNT", FieldType.NUMERIC, 7);
        pnd_Client_Error_W = localVariables.newFieldInRecord("pnd_Client_Error_W", "#CLIENT-ERROR-W", FieldType.NUMERIC, 7);
        pnd_Client_Error_T = localVariables.newFieldInRecord("pnd_Client_Error_T", "#CLIENT-ERROR-T", FieldType.NUMERIC, 7);
        pnd_Client_Error_K = localVariables.newFieldInRecord("pnd_Client_Error_K", "#CLIENT-ERROR-K", FieldType.NUMERIC, 7);
        pnd_Save_Isn = localVariables.newFieldInRecord("pnd_Save_Isn", "#SAVE-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Rec_Stamp_Dte_Tme = localVariables.newFieldInRecord("pnd_Rec_Stamp_Dte_Tme", "#REC-STAMP-DTE-TME", FieldType.STRING, 15);
        pnd_Hold_Stamp_Tme = localVariables.newFieldInRecord("pnd_Hold_Stamp_Tme", "#HOLD-STAMP-TME", FieldType.STRING, 15);
        pnd_Client_Succ_Page_Nbr = localVariables.newFieldInRecord("pnd_Client_Succ_Page_Nbr", "#CLIENT-SUCC-PAGE-NBR", FieldType.NUMERIC, 7);
        pnd_Client_Init_Page_Nbr = localVariables.newFieldInRecord("pnd_Client_Init_Page_Nbr", "#CLIENT-INIT-PAGE-NBR", FieldType.NUMERIC, 7);
        pnd_Client_Err_Page_Nbr = localVariables.newFieldInRecord("pnd_Client_Err_Page_Nbr", "#CLIENT-ERR-PAGE-NBR", FieldType.NUMERIC, 7);
        pnd_Init_Error_Cont = localVariables.newFieldInRecord("pnd_Init_Error_Cont", "#INIT-ERROR-CONT", FieldType.STRING, 1);
        pnd_Succ_W_Head_Printed = localVariables.newFieldInRecord("pnd_Succ_W_Head_Printed", "#SUCC-W-HEAD-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Init_W_Head_Printed = localVariables.newFieldInRecord("pnd_Init_W_Head_Printed", "#INIT-W-HEAD-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Error_W_Head_Printed = localVariables.newFieldInRecord("pnd_Error_W_Head_Printed", "#ERROR-W-HEAD-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Succ_T_Head_Printed = localVariables.newFieldInRecord("pnd_Succ_T_Head_Printed", "#SUCC-T-HEAD-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Init_T_Head_Printed = localVariables.newFieldInRecord("pnd_Init_T_Head_Printed", "#INIT-T-HEAD-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Error_T_Head_Printed = localVariables.newFieldInRecord("pnd_Error_T_Head_Printed", "#ERROR-T-HEAD-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Succ_K_Head_Printed = localVariables.newFieldInRecord("pnd_Succ_K_Head_Printed", "#SUCC-K-HEAD-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Init_K_Head_Printed = localVariables.newFieldInRecord("pnd_Init_K_Head_Printed", "#INIT-K-HEAD-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Error_K_Head_Printed = localVariables.newFieldInRecord("pnd_Error_K_Head_Printed", "#ERROR-K-HEAD-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Cont_Succ_Head_Printed = localVariables.newFieldInRecord("pnd_Cont_Succ_Head_Printed", "#CONT-SUCC-HEAD-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Cont_Init_Head_Printed = localVariables.newFieldInRecord("pnd_Cont_Init_Head_Printed", "#CONT-INIT-HEAD-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Cont_Error_Head_Printed = localVariables.newFieldInRecord("pnd_Cont_Error_Head_Printed", "#CONT-ERROR-HEAD-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_1", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Lvl = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Lvl", "#TBL-SCRTY-LVL", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Name = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Name", "#TBL-NAME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Fld = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Fld", "#TBL-KEY-FLD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actv_Ind = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actv_Ind", "#TBL-ACTV-IND", FieldType.STRING, 
            1);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_2 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Rpt_Date_Tme = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Tbl_Rpt_Date_Tme", "TBL-RPT-DATE-TME", FieldType.STRING, 
            15);

        cwf_Support_Tbl__R_Field_3 = cwf_Support_Tbl__R_Field_2.newGroupInGroup("cwf_Support_Tbl__R_Field_3", "REDEFINE", cwf_Support_Tbl_Tbl_Rpt_Date_Tme);
        cwf_Support_Tbl_Pnd_Tbl_Dte = cwf_Support_Tbl__R_Field_3.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_Dte", "#TBL-DTE", FieldType.STRING, 8);
        cwf_Support_Tbl_Pnd_Tbl_Tme = cwf_Support_Tbl__R_Field_3.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_Tme", "#TBL-TME", FieldType.STRING, 7);
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tracking_Type_Key = localVariables.newFieldInRecord("pnd_Tracking_Type_Key", "#TRACKING-TYPE-KEY", FieldType.STRING, 42);

        pnd_Tracking_Type_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Tracking_Type_Key__R_Field_4", "REDEFINE", pnd_Tracking_Type_Key);
        pnd_Tracking_Type_Key_Pnd_T_Container_Id = pnd_Tracking_Type_Key__R_Field_4.newFieldInGroup("pnd_Tracking_Type_Key_Pnd_T_Container_Id", "#T-CONTAINER-ID", 
            FieldType.STRING, 20);
        pnd_Tracking_Type_Key_Pnd_T_Rcrd_Type_Cde = pnd_Tracking_Type_Key__R_Field_4.newFieldInGroup("pnd_Tracking_Type_Key_Pnd_T_Rcrd_Type_Cde", "#T-RCRD-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Tracking_Type_Key_Pnd_T_Wfo_Ind = pnd_Tracking_Type_Key__R_Field_4.newFieldInGroup("pnd_Tracking_Type_Key_Pnd_T_Wfo_Ind", "#T-WFO-IND", FieldType.STRING, 
            1);
        pnd_Tracking_Type_Key_Pnd_T_Tracking_Rcrd_Nbr = pnd_Tracking_Type_Key__R_Field_4.newFieldInGroup("pnd_Tracking_Type_Key_Pnd_T_Tracking_Rcrd_Nbr", 
            "#T-TRACKING-RCRD-NBR", FieldType.STRING, 20);
        pnd_Date_Time = localVariables.newFieldInRecord("pnd_Date_Time", "#DATE-TIME", FieldType.TIME);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();

        ldaWfol4500.initializeValues();

        localVariables.reset();
        pnd_Client_Succ_Page_Nbr.setInitialValue(0);
        pnd_Client_Init_Page_Nbr.setInitialValue(0);
        pnd_Client_Err_Page_Nbr.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Wfob4500() throws Exception
    {
        super("Wfob4500");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("WFOB4500", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*                                                                                                                                                               //Natural: ON ERROR
        st = Global.getTIMN();                                                                                                                                            //Natural: SET TIME
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 60 ES = OFF;//Natural: FORMAT ( 2 ) LS = 132 PS = 60 ES = OFF;//Natural: FORMAT ( 3 ) LS = 132 PS = 60 ES = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: MOVE *PROGRAM TO #PROGRAM
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Lvl.setValue("A");                                                                                                                //Natural: MOVE 'A' TO #TBL-SCRTY-LVL
        pnd_Tbl_Prime_Key_Pnd_Tbl_Name.setValue("WFO-RECORDS-REPORT");                                                                                                    //Natural: MOVE 'WFO-RECORDS-REPORT' TO #TBL-NAME
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Fld.setValue("WFOB4500");                                                                                                           //Natural: MOVE 'WFOB4500' TO #TBL-KEY-FLD
        vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "FIND01",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_cwf_Support_Tbl.readNextRow("FIND01", true)))
        {
            vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Support_Tbl.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "WFOB4500 Cancelled - table record not found");                                                                                     //Natural: WRITE 'WFOB4500 Cancelled - table record not found'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(5);  if (true) return;                                                                                                                  //Natural: TERMINATE 05
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Save_Isn.setValue(vw_cwf_Support_Tbl.getAstISN("Find01"));                                                                                                //Natural: MOVE *ISN TO #SAVE-ISN
            pnd_Date_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Support_Tbl_Tbl_Rpt_Date_Tme);                                                         //Natural: MOVE EDITED CWF-SUPPORT-TBL.TBL-RPT-DATE-TME TO #DATE-TIME ( EM = YYYYMMDDHHIISST )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        ldaWfol4500.getVw_cmcv().startDatabaseRead                                                                                                                        //Natural: READ CMCV BY CONTAINER-RCRD-TYPE-KEY
        (
        "READ01",
        new Oc[] { new Oc("CONTAINER_RCRD_TYPE_KEY", "ASC") }
        );
        READ01:
        while (condition(ldaWfol4500.getVw_cmcv().readNextRow("READ01")))
        {
            CheckAtStartofData389();

            if (condition(!(ldaWfol4500.getCmcv_Rcrd_Type_Cde().equals("T") || ldaWfol4500.getCmcv_Rcrd_Type_Cde().equals("W") || ldaWfol4500.getCmcv_Rcrd_Type_Cde().equals("K")))) //Natural: AT START OF DATA;//Natural: ACCEPT IF CMCV.RCRD-TYPE-CDE = 'T' OR = 'W' OR = 'K'
            {
                continue;
            }
            pnd_Rec_Stamp_Dte_Tme.setValueEdited(ldaWfol4500.getCmcv_Call_Stamp_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                                             //Natural: MOVE EDITED CMCV.CALL-STAMP-TME ( EM = YYYYMMDDHHIISST ) TO #REC-STAMP-DTE-TME
            if (condition(!(pnd_Rec_Stamp_Dte_Tme.greaterOrEqual(cwf_Support_Tbl_Tbl_Rpt_Date_Tme))))                                                                     //Natural: ACCEPT #REC-STAMP-DTE-TME GE TBL-RPT-DATE-TME
            {
                continue;
            }
            if (condition(ldaWfol4500.getCmcv_Error_Msg_Txt().equals(" ")))                                                                                               //Natural: IF CMCV.ERROR-MSG-TXT = ' '
            {
                pnd_Hold_Stamp_Tme.setValueEdited(ldaWfol4500.getCmcv_Call_Stamp_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                                            //Natural: MOVE EDITED CMCV.CALL-STAMP-TME ( EM = YYYYMMDDHHIISST ) TO #HOLD-STAMP-TME
                //*  THIS IS THE DATE WHICH WE UPDATE THE RECORDS REPORT TABLE WITH
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Curr_Cont_Id.notEquals(ldaWfol4500.getCmcv_Container_Id())))                                                                                //Natural: IF #CURR-CONT-ID NE CMCV.CONTAINER-ID
            {
                pnd_Curr_Cont_Id.setValue(ldaWfol4500.getCmcv_Container_Id());                                                                                            //Natural: ASSIGN #CURR-CONT-ID := CMCV.CONTAINER-ID
                pnd_Init_Error_Cont.reset();                                                                                                                              //Natural: RESET #INIT-ERROR-CONT
                //*  CHECK IF CNTNR IS EITHER SCCSSFL
                                                                                                                                                                          //Natural: PERFORM CHECK-CONTAINER-STAT
                sub_Check_Container_Stat();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  (MEANING ERR-MSG-TXT = ' ') OR NOT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaWfol4500.getCmcv_Rcrd_Type_Cde().equals("W")))                                                                                               //Natural: IF CMCV.RCRD-TYPE-CDE = 'W'
            {
                pnd_Temp_Client_App_Cde.setValue(ldaWfol4500.getCmcv_Client_App_Cde().getValue(1));                                                                       //Natural: MOVE CMCV.CLIENT-APP-CDE ( 1 ) TO #TEMP-CLIENT-APP-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaWfol4500.getCmcv_Rcrd_Type_Cde().equals("T")))                                                                                           //Natural: IF CMCV.RCRD-TYPE-CDE = 'T'
                {
                    pnd_Temp_Client_App_Cde.setValue(ldaWfol4500.getCmcv_T_Client_App_Cde());                                                                             //Natural: MOVE CMCV.T-CLIENT-APP-CDE TO #TEMP-CLIENT-APP-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tracking_Type_Key_Pnd_T_Container_Id.setValue(ldaWfol4500.getCmcv_Container_Id());                                                                //Natural: MOVE CMCV.CONTAINER-ID TO #T-CONTAINER-ID
                    pnd_Tracking_Type_Key_Pnd_T_Rcrd_Type_Cde.setValue("W");                                                                                              //Natural: MOVE 'W' TO #T-RCRD-TYPE-CDE
                    pnd_Tracking_Type_Key_Pnd_T_Wfo_Ind.setValue(ldaWfol4500.getCmcv_Wfo_Ind());                                                                          //Natural: MOVE CMCV.WFO-IND TO #T-WFO-IND
                    pnd_Tracking_Type_Key_Pnd_T_Tracking_Rcrd_Nbr.setValue(ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(1));                                          //Natural: MOVE CMCV.TRACKING-RCRD-NBR ( 1 ) TO #T-TRACKING-RCRD-NBR
                    DbsUtil.callnat(Wfon4501.class , getCurrentProcessState(), pnd_Tracking_Type_Key, pnd_Temp_Client_App_Cde);                                           //Natural: CALLNAT 'WFON4501' USING #TRACKING-TYPE-KEY #TEMP-CLIENT-APP-CDE
                    if (condition(Global.isEscape())) return;
                    //*      FIND(1) CMCV2 WITH TRACKING-TYPE-KEY = #TRACKING-TYPE-KEY
                    //*        MOVE CMCV2.CLIENT-APP-CDE(1) TO #TEMP-CLIENT-APP-CDE
                    //*      END-FIND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(pnd_Temp_Client_App_Cde,"'BATCH'...")))                                                                                     //Natural: IF #TEMP-CLIENT-APP-CDE = MASK ( 'BATCH'... )
            {
                pnd_Hold_Client_App_Cde.setValue(pnd_Temp_Client_App_Cde.getSubstring(6));                                                                                //Natural: ASSIGN #HOLD-CLIENT-APP-CDE := SUBSTR ( #TEMP-CLIENT-APP-CDE,6 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hold_Client_App_Cde.setValue(pnd_Temp_Client_App_Cde);                                                                                                //Natural: ASSIGN #HOLD-CLIENT-APP-CDE := #TEMP-CLIENT-APP-CDE
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_Hold_Client_App_Cde), new ExamineTranslate(TranslateOption.Upper));                                                     //Natural: EXAMINE #HOLD-CLIENT-APP-CDE TRANSLATE INTO UPPER CASE
            getSort().writeSortInData(pnd_Hold_Client_App_Cde, ldaWfol4500.getCmcv_Container_Id(), ldaWfol4500.getCmcv_Rcrd_Type_Cde(), pnd_Init_Error_Cont,              //Natural: END-ALL
                ldaWfol4500.getCmcv_Call_Stamp_Tme(), ldaWfol4500.getCmcv_Error_Msg_Txt(), ldaWfol4500.getCmcv_Rqst_Entry_Op_Cde(), ldaWfol4500.getCmcv_Rqst_Origin_Unit_Cde(), 
                ldaWfol4500.getCmcv_Express_Ind(), ldaWfol4500.getCmcv_Cabinet_Id().getValue(1), ldaWfol4500.getCmcv_Cabinet_Id().getValue(2), ldaWfol4500.getCmcv_Cabinet_Id().getValue(3), 
                ldaWfol4500.getCmcv_Cabinet_Id().getValue(4), ldaWfol4500.getCmcv_Cabinet_Id().getValue(5), ldaWfol4500.getCmcv_Cabinet_Id().getValue(6), 
                ldaWfol4500.getCmcv_Cabinet_Id().getValue(7), ldaWfol4500.getCmcv_Cabinet_Id().getValue(8), ldaWfol4500.getCmcv_Cabinet_Id().getValue(9), 
                ldaWfol4500.getCmcv_Cabinet_Id().getValue(10), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(1), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(2), 
                ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(3), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(4), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(5), 
                ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(6), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(7), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(8), 
                ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(9), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(10), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(1), 
                ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(2), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(3), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(4), 
                ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(5), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(6), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(7), 
                ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(8), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(9), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(10), 
                ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(1), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(2), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(3), 
                ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(4), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(5), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(6), 
                ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(7), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(8), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(9), 
                ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(10), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(1), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(2), 
                ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(3), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(4), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(5), 
                ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(6), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(7), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(8), 
                ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(9), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(10), ldaWfol4500.getCmcv_Wpid_Cde().getValue(1), 
                ldaWfol4500.getCmcv_Wpid_Cde().getValue(2), ldaWfol4500.getCmcv_Wpid_Cde().getValue(3), ldaWfol4500.getCmcv_Wpid_Cde().getValue(4), ldaWfol4500.getCmcv_Wpid_Cde().getValue(5), 
                ldaWfol4500.getCmcv_Wpid_Cde().getValue(6), ldaWfol4500.getCmcv_Wpid_Cde().getValue(7), ldaWfol4500.getCmcv_Wpid_Cde().getValue(8), ldaWfol4500.getCmcv_Wpid_Cde().getValue(9), 
                ldaWfol4500.getCmcv_Wpid_Cde().getValue(10), ldaWfol4500.getCmcv_Wf_Ind().getValue(1), ldaWfol4500.getCmcv_Wf_Ind().getValue(2), ldaWfol4500.getCmcv_Wf_Ind().getValue(3), 
                ldaWfol4500.getCmcv_Wf_Ind().getValue(4), ldaWfol4500.getCmcv_Wf_Ind().getValue(5), ldaWfol4500.getCmcv_Wf_Ind().getValue(6), ldaWfol4500.getCmcv_Wf_Ind().getValue(7), 
                ldaWfol4500.getCmcv_Wf_Ind().getValue(8), ldaWfol4500.getCmcv_Wf_Ind().getValue(9), ldaWfol4500.getCmcv_Wf_Ind().getValue(10), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(1), 
                ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(2), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(3), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(4), 
                ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(5), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(6), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(7), 
                ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(8), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(9), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(10), 
                ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(1), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(2), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(3), 
                ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(4), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(5), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(6), 
                ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(7), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(8), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(9), 
                ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(10), ldaWfol4500.getCmcv_Due_Dte().getValue(1), ldaWfol4500.getCmcv_Due_Dte().getValue(2), 
                ldaWfol4500.getCmcv_Due_Dte().getValue(3), ldaWfol4500.getCmcv_Due_Dte().getValue(4), ldaWfol4500.getCmcv_Due_Dte().getValue(5), ldaWfol4500.getCmcv_Due_Dte().getValue(6), 
                ldaWfol4500.getCmcv_Due_Dte().getValue(7), ldaWfol4500.getCmcv_Due_Dte().getValue(8), ldaWfol4500.getCmcv_Due_Dte().getValue(9), ldaWfol4500.getCmcv_Due_Dte().getValue(10), 
                ldaWfol4500.getCmcv_Cabinet_Level().getValue(1), ldaWfol4500.getCmcv_Cabinet_Level().getValue(2), ldaWfol4500.getCmcv_Cabinet_Level().getValue(3), 
                ldaWfol4500.getCmcv_Cabinet_Level().getValue(4), ldaWfol4500.getCmcv_Cabinet_Level().getValue(5), ldaWfol4500.getCmcv_Cabinet_Level().getValue(6), 
                ldaWfol4500.getCmcv_Cabinet_Level().getValue(7), ldaWfol4500.getCmcv_Cabinet_Level().getValue(8), ldaWfol4500.getCmcv_Cabinet_Level().getValue(9), 
                ldaWfol4500.getCmcv_Cabinet_Level().getValue(10), ldaWfol4500.getCmcv_Action_Cde().getValue(1), ldaWfol4500.getCmcv_Action_Cde().getValue(2), 
                ldaWfol4500.getCmcv_Action_Cde().getValue(3), ldaWfol4500.getCmcv_Action_Cde().getValue(4), ldaWfol4500.getCmcv_Action_Cde().getValue(5), 
                ldaWfol4500.getCmcv_Action_Cde().getValue(6), ldaWfol4500.getCmcv_Action_Cde().getValue(7), ldaWfol4500.getCmcv_Action_Cde().getValue(8), 
                ldaWfol4500.getCmcv_Action_Cde().getValue(9), ldaWfol4500.getCmcv_Action_Cde().getValue(10), ldaWfol4500.getCmcv_Topic().getValue(1), ldaWfol4500.getCmcv_Topic().getValue(2), 
                ldaWfol4500.getCmcv_Topic().getValue(3), ldaWfol4500.getCmcv_Topic().getValue(4), ldaWfol4500.getCmcv_Topic().getValue(5), ldaWfol4500.getCmcv_Topic().getValue(6), 
                ldaWfol4500.getCmcv_Topic().getValue(7), ldaWfol4500.getCmcv_Topic().getValue(8), ldaWfol4500.getCmcv_Topic().getValue(9), ldaWfol4500.getCmcv_Topic().getValue(10), 
                ldaWfol4500.getCmcv_Step().getValue(1), ldaWfol4500.getCmcv_Step().getValue(2), ldaWfol4500.getCmcv_Step().getValue(3), ldaWfol4500.getCmcv_Step().getValue(4), 
                ldaWfol4500.getCmcv_Step().getValue(5), ldaWfol4500.getCmcv_Step().getValue(6), ldaWfol4500.getCmcv_Step().getValue(7), ldaWfol4500.getCmcv_Step().getValue(8), 
                ldaWfol4500.getCmcv_Step().getValue(9), ldaWfol4500.getCmcv_Step().getValue(10), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(1), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(2), 
                ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(3), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(4), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(5), 
                ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(6), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(7), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(8), 
                ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(9), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(10), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(1), 
                ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(2), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(3), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(4), 
                ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(5), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(6), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(7), 
                ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(8), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(9), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(10), 
                ldaWfol4500.getCmcv_Doc_Clss_Cde(), ldaWfol4500.getCmcv_Dcmnt_Obj_Id(), ldaWfol4500.getCmcv_Source_Id(), ldaWfol4500.getCmcv_Doc_Ctgry_Cde(), 
                ldaWfol4500.getCmcv_Dcmnt_Direction(), ldaWfol4500.getCmcv_Image_Addrss(), ldaWfol4500.getCmcv_T_Client_App_Cde(), ldaWfol4500.getCmcv_Img_Start_Page_Nbr(), 
                ldaWfol4500.getCmcv_Mail_Item_No(), ldaWfol4500.getCmcv_Doc_Type_Spcfc_Cde(), ldaWfol4500.getCmcv_Batch_Id(), ldaWfol4500.getCmcv_Img_Total_Pages(), 
                ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(1), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(2), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(3), 
                ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(4), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(5), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(6), 
                ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(7), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(8), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(9), 
                ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(10), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(11), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(12), 
                ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(13), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(14), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(15), 
                ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(16), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(17), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(18), 
                ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(19), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(20), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(21), 
                ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(22), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(23), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(24), 
                ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(25), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(26), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(27), 
                ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(28), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(29), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(30), 
                ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(31), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(32), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(33), 
                ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(34), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(35), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(1), 
                ldaWfol4500.getCmcv_T_Wf_Ind().getValue(2), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(3), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(4), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(5), 
                ldaWfol4500.getCmcv_T_Wf_Ind().getValue(6), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(7), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(8), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(9), 
                ldaWfol4500.getCmcv_T_Wf_Ind().getValue(10), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(11), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(12), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(13), 
                ldaWfol4500.getCmcv_T_Wf_Ind().getValue(14), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(15), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(16), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(17), 
                ldaWfol4500.getCmcv_T_Wf_Ind().getValue(18), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(19), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(20), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(21), 
                ldaWfol4500.getCmcv_T_Wf_Ind().getValue(22), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(23), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(24), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(25), 
                ldaWfol4500.getCmcv_T_Wf_Ind().getValue(26), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(27), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(28), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(29), 
                ldaWfol4500.getCmcv_T_Wf_Ind().getValue(30), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(31), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(32), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(33), 
                ldaWfol4500.getCmcv_T_Wf_Ind().getValue(34), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(35), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(1), 
                ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(2), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(3), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(4), 
                ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(5), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(6), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(7), 
                ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(8), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(9), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(10), 
                ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(11), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(12), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(13), 
                ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(14), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(15), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(16), 
                ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(17), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(18), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(19), 
                ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(20), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(21), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(22), 
                ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(23), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(24), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(25), 
                ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(26), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(27), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(28), 
                ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(29), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(30), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(31), 
                ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(32), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(33), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(34), 
                ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(35), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(1), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(2), 
                ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(3), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(4), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(5), 
                ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(6), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(7), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(8), 
                ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(9), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(10), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(11), 
                ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(12), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(13), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(14), 
                ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(15), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(16), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(17), 
                ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(18), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(19), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(20), 
                ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(21), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(22), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(23), 
                ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(24), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(25), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(26), 
                ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(27), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(28), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(29), 
                ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(30), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(31), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(32), 
                ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(33), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(34), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(35), 
                ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(1), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(2), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(3), 
                ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(4), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(5), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(6), 
                ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(7), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(8), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(9), 
                ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(10), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(11), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(12), 
                ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(13), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(14), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(15), 
                ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(16), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(17), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(18), 
                ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(19), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(20), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(21), 
                ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(22), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(23), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(24), 
                ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(25), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(26), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(27), 
                ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(28), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(29), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(30), 
                ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(31), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(32), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(33), 
                ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(34), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(35), ldaWfol4500.getCmcv_Wfo_Ind(), 
                ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(1), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(2), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(3), 
                ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(4), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(5), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(6), 
                ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(7), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(8), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(9), 
                ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(10), ldaWfol4500.getCmcv_Doc_Txt().getValue(1), ldaWfol4500.getCmcv_Doc_Txt().getValue(2), 
                ldaWfol4500.getCmcv_Doc_Txt().getValue(3), ldaWfol4500.getCmcv_Doc_Txt().getValue(4), ldaWfol4500.getCmcv_Doc_Txt().getValue(5));
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  EPM 01/18/01
        //*  UP TO 35
        //*  UP TO 35
        //*  UP TO 35
        //*  UP TO 35
        //*  UP TO 35
        getSort().sortData(pnd_Hold_Client_App_Cde, ldaWfol4500.getCmcv_Container_Id(), ldaWfol4500.getCmcv_Rcrd_Type_Cde());                                             //Natural: SORT BY #HOLD-CLIENT-APP-CDE CMCV.CONTAINER-ID CMCV.RCRD-TYPE-CDE USING #INIT-ERROR-CONT CMCV.CALL-STAMP-TME CMCV.ERROR-MSG-TXT CMCV.RQST-ENTRY-OP-CDE CMCV.RQST-ORIGIN-UNIT-CDE CMCV.EXPRESS-IND CMCV.CABINET-ID ( * ) CMCV.DSTNTN-UNT-CDE ( * ) CMCV.CORP-STATUS-CDE ( * ) CMCV.UNIT-STATUS-CDE ( * ) CMCV.IND-TRACKING-ID ( * ) CMCV.WPID-CDE ( * ) CMCV.WF-IND ( * ) CMCV.EMPL-RACF-ID ( * ) CMCV.INST-TRACKING-ID ( * ) CMCV.DUE-DTE ( * ) CMCV.CABINET-LEVEL ( * ) CMCV.ACTION-CDE ( * ) CMCV.TOPIC ( * ) CMCV.STEP ( * ) CMCV.ELCTRNC-FLDR-IND ( * ) CMCV.RQST-ORGN-CDE ( * ) CMCV.DOC-CLSS-CDE CMCV.DCMNT-OBJ-ID CMCV.SOURCE-ID CMCV.DOC-CTGRY-CDE CMCV.DCMNT-DIRECTION CMCV.IMAGE-ADDRSS CMCV.T-CLIENT-APP-CDE CMCV.IMG-START-PAGE-NBR CMCV.MAIL-ITEM-NO CMCV.DOC-TYPE-SPCFC-CDE CMCV.BATCH-ID CMCV.IMG-TOTAL-PAGES CMCV.T-CABINET-LEVEL ( * ) CMCV.T-WF-IND ( * ) CMCV.T-CABINET-ID ( * ) CMCV.T-INST-TRACKING-ID ( * ) CMCV.T-IND-TRACKING-ID ( * ) CMCV.WFO-IND CMCV.TRACKING-RCRD-NBR ( * ) CMCV.DOC-TXT ( 1 ) CMCV.DOC-TXT ( 2 ) CMCV.DOC-TXT ( 3 ) CMCV.DOC-TXT ( 4 ) CMCV.DOC-TXT ( 5 )
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Hold_Client_App_Cde, ldaWfol4500.getCmcv_Container_Id(), ldaWfol4500.getCmcv_Rcrd_Type_Cde(), pnd_Init_Error_Cont, 
            ldaWfol4500.getCmcv_Call_Stamp_Tme(), ldaWfol4500.getCmcv_Error_Msg_Txt(), ldaWfol4500.getCmcv_Rqst_Entry_Op_Cde(), ldaWfol4500.getCmcv_Rqst_Origin_Unit_Cde(), 
            ldaWfol4500.getCmcv_Express_Ind(), ldaWfol4500.getCmcv_Cabinet_Id().getValue(1), ldaWfol4500.getCmcv_Cabinet_Id().getValue(2), ldaWfol4500.getCmcv_Cabinet_Id().getValue(3), 
            ldaWfol4500.getCmcv_Cabinet_Id().getValue(4), ldaWfol4500.getCmcv_Cabinet_Id().getValue(5), ldaWfol4500.getCmcv_Cabinet_Id().getValue(6), ldaWfol4500.getCmcv_Cabinet_Id().getValue(7), 
            ldaWfol4500.getCmcv_Cabinet_Id().getValue(8), ldaWfol4500.getCmcv_Cabinet_Id().getValue(9), ldaWfol4500.getCmcv_Cabinet_Id().getValue(10), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(1), 
            ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(2), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(3), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(4), 
            ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(5), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(6), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(7), 
            ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(8), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(9), ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(10), 
            ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(1), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(2), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(3), 
            ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(4), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(5), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(6), 
            ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(7), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(8), ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(9), 
            ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(10), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(1), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(2), 
            ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(3), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(4), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(5), 
            ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(6), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(7), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(8), 
            ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(9), ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(10), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(1), 
            ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(2), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(3), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(4), 
            ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(5), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(6), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(7), 
            ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(8), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(9), ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(10), 
            ldaWfol4500.getCmcv_Wpid_Cde().getValue(1), ldaWfol4500.getCmcv_Wpid_Cde().getValue(2), ldaWfol4500.getCmcv_Wpid_Cde().getValue(3), ldaWfol4500.getCmcv_Wpid_Cde().getValue(4), 
            ldaWfol4500.getCmcv_Wpid_Cde().getValue(5), ldaWfol4500.getCmcv_Wpid_Cde().getValue(6), ldaWfol4500.getCmcv_Wpid_Cde().getValue(7), ldaWfol4500.getCmcv_Wpid_Cde().getValue(8), 
            ldaWfol4500.getCmcv_Wpid_Cde().getValue(9), ldaWfol4500.getCmcv_Wpid_Cde().getValue(10), ldaWfol4500.getCmcv_Wf_Ind().getValue(1), ldaWfol4500.getCmcv_Wf_Ind().getValue(2), 
            ldaWfol4500.getCmcv_Wf_Ind().getValue(3), ldaWfol4500.getCmcv_Wf_Ind().getValue(4), ldaWfol4500.getCmcv_Wf_Ind().getValue(5), ldaWfol4500.getCmcv_Wf_Ind().getValue(6), 
            ldaWfol4500.getCmcv_Wf_Ind().getValue(7), ldaWfol4500.getCmcv_Wf_Ind().getValue(8), ldaWfol4500.getCmcv_Wf_Ind().getValue(9), ldaWfol4500.getCmcv_Wf_Ind().getValue(10), 
            ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(1), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(2), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(3), 
            ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(4), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(5), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(6), 
            ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(7), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(8), ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(9), 
            ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(10), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(1), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(2), 
            ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(3), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(4), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(5), 
            ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(6), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(7), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(8), 
            ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(9), ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(10), ldaWfol4500.getCmcv_Due_Dte().getValue(1), 
            ldaWfol4500.getCmcv_Due_Dte().getValue(2), ldaWfol4500.getCmcv_Due_Dte().getValue(3), ldaWfol4500.getCmcv_Due_Dte().getValue(4), ldaWfol4500.getCmcv_Due_Dte().getValue(5), 
            ldaWfol4500.getCmcv_Due_Dte().getValue(6), ldaWfol4500.getCmcv_Due_Dte().getValue(7), ldaWfol4500.getCmcv_Due_Dte().getValue(8), ldaWfol4500.getCmcv_Due_Dte().getValue(9), 
            ldaWfol4500.getCmcv_Due_Dte().getValue(10), ldaWfol4500.getCmcv_Cabinet_Level().getValue(1), ldaWfol4500.getCmcv_Cabinet_Level().getValue(2), 
            ldaWfol4500.getCmcv_Cabinet_Level().getValue(3), ldaWfol4500.getCmcv_Cabinet_Level().getValue(4), ldaWfol4500.getCmcv_Cabinet_Level().getValue(5), 
            ldaWfol4500.getCmcv_Cabinet_Level().getValue(6), ldaWfol4500.getCmcv_Cabinet_Level().getValue(7), ldaWfol4500.getCmcv_Cabinet_Level().getValue(8), 
            ldaWfol4500.getCmcv_Cabinet_Level().getValue(9), ldaWfol4500.getCmcv_Cabinet_Level().getValue(10), ldaWfol4500.getCmcv_Action_Cde().getValue(1), 
            ldaWfol4500.getCmcv_Action_Cde().getValue(2), ldaWfol4500.getCmcv_Action_Cde().getValue(3), ldaWfol4500.getCmcv_Action_Cde().getValue(4), ldaWfol4500.getCmcv_Action_Cde().getValue(5), 
            ldaWfol4500.getCmcv_Action_Cde().getValue(6), ldaWfol4500.getCmcv_Action_Cde().getValue(7), ldaWfol4500.getCmcv_Action_Cde().getValue(8), ldaWfol4500.getCmcv_Action_Cde().getValue(9), 
            ldaWfol4500.getCmcv_Action_Cde().getValue(10), ldaWfol4500.getCmcv_Topic().getValue(1), ldaWfol4500.getCmcv_Topic().getValue(2), ldaWfol4500.getCmcv_Topic().getValue(3), 
            ldaWfol4500.getCmcv_Topic().getValue(4), ldaWfol4500.getCmcv_Topic().getValue(5), ldaWfol4500.getCmcv_Topic().getValue(6), ldaWfol4500.getCmcv_Topic().getValue(7), 
            ldaWfol4500.getCmcv_Topic().getValue(8), ldaWfol4500.getCmcv_Topic().getValue(9), ldaWfol4500.getCmcv_Topic().getValue(10), ldaWfol4500.getCmcv_Step().getValue(1), 
            ldaWfol4500.getCmcv_Step().getValue(2), ldaWfol4500.getCmcv_Step().getValue(3), ldaWfol4500.getCmcv_Step().getValue(4), ldaWfol4500.getCmcv_Step().getValue(5), 
            ldaWfol4500.getCmcv_Step().getValue(6), ldaWfol4500.getCmcv_Step().getValue(7), ldaWfol4500.getCmcv_Step().getValue(8), ldaWfol4500.getCmcv_Step().getValue(9), 
            ldaWfol4500.getCmcv_Step().getValue(10), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(1), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(2), 
            ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(3), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(4), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(5), 
            ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(6), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(7), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(8), 
            ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(9), ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(10), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(1), 
            ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(2), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(3), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(4), 
            ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(5), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(6), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(7), 
            ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(8), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(9), ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(10), 
            ldaWfol4500.getCmcv_Doc_Clss_Cde(), ldaWfol4500.getCmcv_Dcmnt_Obj_Id(), ldaWfol4500.getCmcv_Source_Id(), ldaWfol4500.getCmcv_Doc_Ctgry_Cde(), 
            ldaWfol4500.getCmcv_Dcmnt_Direction(), ldaWfol4500.getCmcv_Image_Addrss(), ldaWfol4500.getCmcv_T_Client_App_Cde(), ldaWfol4500.getCmcv_Img_Start_Page_Nbr(), 
            ldaWfol4500.getCmcv_Mail_Item_No(), ldaWfol4500.getCmcv_Doc_Type_Spcfc_Cde(), ldaWfol4500.getCmcv_Batch_Id(), ldaWfol4500.getCmcv_Img_Total_Pages(), 
            ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(1), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(2), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(3), 
            ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(4), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(5), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(6), 
            ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(7), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(8), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(9), 
            ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(10), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(11), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(12), 
            ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(13), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(14), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(15), 
            ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(16), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(17), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(18), 
            ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(19), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(20), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(21), 
            ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(22), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(23), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(24), 
            ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(25), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(26), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(27), 
            ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(28), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(29), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(30), 
            ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(31), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(32), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(33), 
            ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(34), ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(35), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(1), 
            ldaWfol4500.getCmcv_T_Wf_Ind().getValue(2), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(3), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(4), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(5), 
            ldaWfol4500.getCmcv_T_Wf_Ind().getValue(6), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(7), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(8), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(9), 
            ldaWfol4500.getCmcv_T_Wf_Ind().getValue(10), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(11), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(12), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(13), 
            ldaWfol4500.getCmcv_T_Wf_Ind().getValue(14), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(15), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(16), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(17), 
            ldaWfol4500.getCmcv_T_Wf_Ind().getValue(18), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(19), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(20), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(21), 
            ldaWfol4500.getCmcv_T_Wf_Ind().getValue(22), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(23), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(24), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(25), 
            ldaWfol4500.getCmcv_T_Wf_Ind().getValue(26), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(27), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(28), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(29), 
            ldaWfol4500.getCmcv_T_Wf_Ind().getValue(30), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(31), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(32), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(33), 
            ldaWfol4500.getCmcv_T_Wf_Ind().getValue(34), ldaWfol4500.getCmcv_T_Wf_Ind().getValue(35), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(1), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(2), 
            ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(3), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(4), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(5), 
            ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(6), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(7), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(8), 
            ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(9), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(10), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(11), 
            ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(12), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(13), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(14), 
            ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(15), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(16), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(17), 
            ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(18), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(19), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(20), 
            ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(21), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(22), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(23), 
            ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(24), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(25), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(26), 
            ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(27), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(28), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(29), 
            ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(30), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(31), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(32), 
            ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(33), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(34), ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(35), 
            ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(1), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(2), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(3), 
            ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(4), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(5), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(6), 
            ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(7), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(8), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(9), 
            ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(10), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(11), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(12), 
            ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(13), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(14), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(15), 
            ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(16), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(17), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(18), 
            ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(19), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(20), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(21), 
            ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(22), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(23), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(24), 
            ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(25), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(26), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(27), 
            ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(28), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(29), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(30), 
            ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(31), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(32), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(33), 
            ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(34), ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(35), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(1), 
            ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(2), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(3), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(4), 
            ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(5), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(6), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(7), 
            ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(8), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(9), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(10), 
            ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(11), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(12), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(13), 
            ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(14), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(15), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(16), 
            ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(17), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(18), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(19), 
            ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(20), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(21), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(22), 
            ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(23), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(24), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(25), 
            ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(26), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(27), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(28), 
            ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(29), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(30), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(31), 
            ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(32), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(33), ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(34), 
            ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(35), ldaWfol4500.getCmcv_Wfo_Ind(), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(1), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(2), 
            ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(3), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(4), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(5), 
            ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(6), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(7), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(8), 
            ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(9), ldaWfol4500.getCmcv_Tracking_Rcrd_Nbr().getValue(10), ldaWfol4500.getCmcv_Doc_Txt().getValue(1), 
            ldaWfol4500.getCmcv_Doc_Txt().getValue(2), ldaWfol4500.getCmcv_Doc_Txt().getValue(3), ldaWfol4500.getCmcv_Doc_Txt().getValue(4), ldaWfol4500.getCmcv_Doc_Txt().getValue(5))))
        {
            CheckAtStartofData446();

            //*  FOR WORK REQUEST
            //*  FOR TEXT
            //*    CMCV.ACTION-CDE(*)
            //*  FOR ATI
            //* ***************
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*  BREAK IN CLIENT APPL CODE
            if (condition(pnd_Hold_Client_App_Cde.notEquals(pnd_Save_Client_App_Cde)))                                                                                    //Natural: IF #HOLD-CLIENT-APP-CDE NE #SAVE-CLIENT-APP-CDE
            {
                                                                                                                                                                          //Natural: PERFORM CLIENT-FOOTINGS
                sub_Client_Footings();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(3));                                                                                                         //Natural: NEWPAGE ( 3 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Save_Client_App_Cde.setValue(pnd_Hold_Client_App_Cde);                                                                                                //Natural: MOVE #HOLD-CLIENT-APP-CDE TO #SAVE-CLIENT-APP-CDE
                                                                                                                                                                          //Natural: PERFORM RESET-CLIENT-CTRS
                sub_Reset_Client_Ctrs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Cont_Succ_Head_Printed.reset();                                                                                                                       //Natural: RESET #CONT-SUCC-HEAD-PRINTED #CONT-INIT-HEAD-PRINTED #CONT-ERROR-HEAD-PRINTED
                pnd_Cont_Init_Head_Printed.reset();
                pnd_Cont_Error_Head_Printed.reset();
            }                                                                                                                                                             //Natural: END-IF
            //*  BREAK IN CONTAINER ID
            if (condition(pnd_Hold_Cont_Id.notEquals(ldaWfol4500.getCmcv_Container_Id())))                                                                                //Natural: IF #HOLD-CONT-ID NE CONTAINER-ID
            {
                //*  COUNTERS SHOULD BE ADDED DEPENDING ON WHICH TYPE OF CONTAINER
                pnd_Hold_Cont_Id.setValue(ldaWfol4500.getCmcv_Container_Id());                                                                                            //Natural: MOVE CONTAINER-ID TO #HOLD-CONT-ID
                pnd_Cont_Succ_Head_Printed.reset();                                                                                                                       //Natural: RESET #CONT-SUCC-HEAD-PRINTED #CONT-INIT-HEAD-PRINTED #CONT-ERROR-HEAD-PRINTED
                pnd_Cont_Init_Head_Printed.reset();
                pnd_Cont_Error_Head_Printed.reset();
            }                                                                                                                                                             //Natural: END-IF
            //* ---PROCESS RECORDS HERE
                                                                                                                                                                          //Natural: PERFORM PROCESS-DETAIL-CONT
            sub_Process_Detail_Cont();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Tot_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #TOT-CNT
            pnd_Client_Tot_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #CLIENT-TOT-CNT
            pnd_Succ_W_Head_Printed.reset();                                                                                                                              //Natural: RESET #SUCC-W-HEAD-PRINTED #INIT-W-HEAD-PRINTED #ERROR-W-HEAD-PRINTED
            pnd_Init_W_Head_Printed.reset();
            pnd_Error_W_Head_Printed.reset();
            pnd_Succ_T_Head_Printed.reset();                                                                                                                              //Natural: RESET #SUCC-T-HEAD-PRINTED #INIT-T-HEAD-PRINTED #ERROR-T-HEAD-PRINTED
            pnd_Init_T_Head_Printed.reset();
            pnd_Error_T_Head_Printed.reset();
            pnd_Succ_K_Head_Printed.reset();                                                                                                                              //Natural: RESET #SUCC-K-HEAD-PRINTED #INIT-K-HEAD-PRINTED #ERROR-K-HEAD-PRINTED
            pnd_Init_K_Head_Printed.reset();
            pnd_Error_K_Head_Printed.reset();
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
                                                                                                                                                                          //Natural: PERFORM CLIENT-FOOTINGS
        sub_Client_Footings();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Succ_Cnt.greater(getZero())))                                                                                                                   //Natural: IF #SUCC-CNT GT 0
        {
            F1:                                                                                                                                                           //Natural: GET CWF-SUPPORT-TBL #SAVE-ISN
            vw_cwf_Support_Tbl.readByID(pnd_Save_Isn.getLong(), "F1");
            cwf_Support_Tbl_Tbl_Rpt_Date_Tme.setValue(pnd_Hold_Stamp_Tme);                                                                                                //Natural: MOVE #HOLD-STAMP-TME TO TBL-RPT-DATE-TME
            cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                 //Natural: MOVE *TIMX TO TBL-UPDTE-DTE-TME
            cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                          //Natural: MOVE *INIT-USER TO TBL-UPDTE-OPRTR-CDE
            vw_cwf_Support_Tbl.updateDBRow("F1");                                                                                                                         //Natural: UPDATE ( F1. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE(1) /// 'Total SUCCESSFUL Messages Cnt ' #SUCC-CNT
        //*  WRITE(2) /// 'Total INITIAL    Messages Cnt ' #INIT-CNT
        //*  WRITE(3) /// 'Total ERROR      Messages Cnt ' #ERROR-CNT
        pnd_Save_Client_App_Cde.setValue("ALL");                                                                                                                          //Natural: MOVE 'ALL' TO #SAVE-CLIENT-APP-CDE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"Total Count for WrkRqst Records",pnd_Rec_W_Cnt,NEWLINE,NEWLINE,"Total Count for Txt/Img Records", //Natural: WRITE ( 1 ) /// 'Total Count for WrkRqst Records' #REC-W-CNT // 'Total Count for Txt/Img Records' #REC-T-CNT // 'Total Count for ATI     Records' #REC-K-CNT // 'Total Count for WrkRqst,Txt/Img & ATI Records' #TOT-CNT //// 'Total SUCCESSFUL Messages Cnt  ' #SUCC-CNT / '              Total WrkRqst Records  ' #SUCC-W / '              Total Txt/Img Records  ' #SUCC-T / '              Total ATI     Records  ' #SUCC-K // 'Total INIT       Messages Cnt ' #INIT-CNT / '              Total WrkRqst Records  ' #INIT-W / '              Total Txt/Img Records  ' #INIT-T / '              Total ATI     Records  ' #INIT-K // 'Total ERROR      Messages Cnt ' #ERROR-CNT / '              Total WrkRqst Records  ' #ERROR-W / '              Total Txt/Img Records  ' #ERROR-T / '              Total ATI     Records  ' #ERROR-K /
            pnd_Rec_T_Cnt,NEWLINE,NEWLINE,"Total Count for ATI     Records",pnd_Rec_K_Cnt,NEWLINE,NEWLINE,"Total Count for WrkRqst,Txt/Img & ATI Records",
            pnd_Tot_Cnt,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"Total SUCCESSFUL Messages Cnt  ",pnd_Succ_Cnt,NEWLINE,"              Total WrkRqst Records  ",pnd_Succ_W,
            NEWLINE,"              Total Txt/Img Records  ",pnd_Succ_T,NEWLINE,"              Total ATI     Records  ",pnd_Succ_K,NEWLINE,NEWLINE,"Total INIT       Messages Cnt ",
            pnd_Init_Cnt,NEWLINE,"              Total WrkRqst Records  ",pnd_Init_W,NEWLINE,"              Total Txt/Img Records  ",pnd_Init_T,NEWLINE,"              Total ATI     Records  ",
            pnd_Init_K,NEWLINE,NEWLINE,"Total ERROR      Messages Cnt ",pnd_Error_Cnt,NEWLINE,"              Total WrkRqst Records  ",pnd_Error_W,NEWLINE,
            "              Total Txt/Img Records  ",pnd_Error_T,NEWLINE,"              Total ATI     Records  ",pnd_Error_K,NEWLINE);
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"Total Count for WrkRqst Records",pnd_Rec_W_Cnt,NEWLINE,NEWLINE,"Total Count for Txt/Img Records", //Natural: WRITE ( 2 ) /// 'Total Count for WrkRqst Records' #REC-W-CNT // 'Total Count for Txt/Img Records' #REC-T-CNT // 'Total Count for ATI     Records' #REC-K-CNT // 'Total Count for WrkRqst,Txt/Img & ATI Records' #TOT-CNT //// 'Total SUCCESSFUL Messages Cnt  ' #SUCC-CNT / '              Total WrkRqst Records  ' #SUCC-W / '              Total Txt/Img Records  ' #SUCC-T / '              Total ATI     Records  ' #SUCC-K // 'Total INIT       Messages Cnt  ' #INIT-CNT / '              Total WrkRqst Records  ' #INIT-W / '              Total Txt/Img Records  ' #INIT-T / '              Total ATI     Records  ' #INIT-K // 'Total ERROR      Messages Cnt  ' #ERROR-CNT / '              Total WrkRqst Records  ' #ERROR-W / '              Total Txt/Img Records  ' #ERROR-T / '              Total ATI     Records  ' #ERROR-K /
            pnd_Rec_T_Cnt,NEWLINE,NEWLINE,"Total Count for ATI     Records",pnd_Rec_K_Cnt,NEWLINE,NEWLINE,"Total Count for WrkRqst,Txt/Img & ATI Records",
            pnd_Tot_Cnt,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"Total SUCCESSFUL Messages Cnt  ",pnd_Succ_Cnt,NEWLINE,"              Total WrkRqst Records  ",pnd_Succ_W,
            NEWLINE,"              Total Txt/Img Records  ",pnd_Succ_T,NEWLINE,"              Total ATI     Records  ",pnd_Succ_K,NEWLINE,NEWLINE,"Total INIT       Messages Cnt  ",
            pnd_Init_Cnt,NEWLINE,"              Total WrkRqst Records  ",pnd_Init_W,NEWLINE,"              Total Txt/Img Records  ",pnd_Init_T,NEWLINE,"              Total ATI     Records  ",
            pnd_Init_K,NEWLINE,NEWLINE,"Total ERROR      Messages Cnt  ",pnd_Error_Cnt,NEWLINE,"              Total WrkRqst Records  ",pnd_Error_W,NEWLINE,
            "              Total Txt/Img Records  ",pnd_Error_T,NEWLINE,"              Total ATI     Records  ",pnd_Error_K,NEWLINE);
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"Total Count for WrkRqst Records",pnd_Rec_W_Cnt,NEWLINE,NEWLINE,"Total Count for Txt/Img Records", //Natural: WRITE ( 3 ) /// 'Total Count for WrkRqst Records' #REC-W-CNT // 'Total Count for Txt/Img Records' #REC-T-CNT // 'Total Count for ATI     Records' #REC-K-CNT // 'Total Count for WrkRqst,Txt/Img & ATI Records' #TOT-CNT //// 'Total SUCCESSFUL Messages Cnt  ' #SUCC-CNT / '              Total WrkRqst Records  ' #SUCC-W / '              Total Txt/Img Records  ' #SUCC-T // '              Total ATI     Records  ' #SUCC-K // 'Total INIT       Messages Cnt  ' #INIT-CNT / '              Total WrkRqst Records  ' #INIT-W / '              Total Txt/Img Records  ' #INIT-T // '              Total ATI     Records  ' #INIT-K // 'Total ERROR      Messages Cnt  ' #ERROR-CNT / '              Total WrkRqst Records  ' #ERROR-W / '              Total Txt/Img Records  ' #ERROR-T / '              Total ATI     Records  ' #ERROR-K /
            pnd_Rec_T_Cnt,NEWLINE,NEWLINE,"Total Count for ATI     Records",pnd_Rec_K_Cnt,NEWLINE,NEWLINE,"Total Count for WrkRqst,Txt/Img & ATI Records",
            pnd_Tot_Cnt,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"Total SUCCESSFUL Messages Cnt  ",pnd_Succ_Cnt,NEWLINE,"              Total WrkRqst Records  ",pnd_Succ_W,
            NEWLINE,"              Total Txt/Img Records  ",pnd_Succ_T,NEWLINE,NEWLINE,"              Total ATI     Records  ",pnd_Succ_K,NEWLINE,NEWLINE,
            "Total INIT       Messages Cnt  ",pnd_Init_Cnt,NEWLINE,"              Total WrkRqst Records  ",pnd_Init_W,NEWLINE,"              Total Txt/Img Records  ",
            pnd_Init_T,NEWLINE,NEWLINE,"              Total ATI     Records  ",pnd_Init_K,NEWLINE,NEWLINE,"Total ERROR      Messages Cnt  ",pnd_Error_Cnt,
            NEWLINE,"              Total WrkRqst Records  ",pnd_Error_W,NEWLINE,"              Total Txt/Img Records  ",pnd_Error_T,NEWLINE,"              Total ATI     Records  ",
            pnd_Error_K,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Elapsed Time: ",st, new ReportEditMask ("99':'99':'99'.'9"));                                                 //Natural: WRITE ( 1 ) / 'Elapsed Time: '*TIMD ( ST. ) ( EM = 99':'99':'99'.'9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"Elapsed Time: ",st, new ReportEditMask ("99':'99':'99'.'9"));                                                 //Natural: WRITE ( 2 ) / 'Elapsed Time: '*TIMD ( ST. ) ( EM = 99':'99':'99'.'9 )
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,"Elapsed Time: ",st, new ReportEditMask ("99':'99':'99'.'9"));                                                 //Natural: WRITE ( 3 ) / 'Elapsed Time: '*TIMD ( ST. ) ( EM = 99':'99':'99'.'9 )
        if (Global.isEscape()) return;
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DETAIL-CONT
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-W
        //*      WRITE(1) NOHDR USING FORM 'WFOF4501'
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-T
        //*      WRITE(1) NOHDR USING FORM 'WFOF4502'
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-K
        //*      WRITE(1) NOHDR USING FORM 'WFOF4503'
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SPACES
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-ARRAYS-FIELDS-TO-MAP
        //* ******************************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TEXT-FIELDS
        //* *********************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CLIENT-FOOTINGS
        //* ********************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-CLIENT-CTRS
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-CONTAINER-STAT
    }
    private void sub_Process_Detail_Cont() throws Exception                                                                                                               //Natural: PROCESS-DETAIL-CONT
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        short decideConditionsMet546 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CMCV.RCRD-TYPE-CDE;//Natural: VALUE 'W'
        if (condition((ldaWfol4500.getCmcv_Rcrd_Type_Cde().equals("W"))))
        {
            decideConditionsMet546++;
            pnd_Rec_W_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #REC-W-CNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-W
            sub_Process_Record_Type_W();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaWfol4500.getCmcv_Rcrd_Type_Cde().equals("T"))))
        {
            decideConditionsMet546++;
            pnd_Rec_T_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #REC-T-CNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-T
            sub_Process_Record_Type_T();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'K'
        else if (condition((ldaWfol4500.getCmcv_Rcrd_Type_Cde().equals("K"))))
        {
            decideConditionsMet546++;
            pnd_Rec_K_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #REC-K-CNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-K
            sub_Process_Record_Type_K();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  PROCESS-DETAIL-CONT
    }
    private void sub_Process_Record_Type_W() throws Exception                                                                                                             //Natural: PROCESS-RECORD-TYPE-W
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pnd_Wrk_Rqst_Idx.setValue(1);                                                                                                                                     //Natural: MOVE 1 TO #WRK-RQST-IDX
        //*  MEANING SUCCESSFUL
        if (condition(pnd_Init_Error_Cont.equals(" ")))                                                                                                                   //Natural: IF #INIT-ERROR-CONT = ' '
        {
            if (condition(! (pnd_Cont_Succ_Head_Printed.getBoolean())))                                                                                                   //Natural: IF NOT #CONT-SUCC-HEAD-PRINTED
            {
                //*  HEADER WITH SEQ,CONTAINER
                pnd_Client_Succ_Cont_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CLIENT-SUCC-CONT-CNT
                getReports().write(1, ReportOption.NOHDR, writeMapToStringOutput(Wfof4511.class));                                                                        //Natural: WRITE ( 1 ) NOHDR USING FORM 'WFOF4511'
                pnd_Cont_Succ_Head_Printed.setValue(true);                                                                                                                //Natural: ASSIGN #CONT-SUCC-HEAD-PRINTED := TRUE
                pnd_Succ_W_Head_Printed.setValue(true);                                                                                                                   //Natural: ASSIGN #SUCC-W-HEAD-PRINTED := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pnd_Succ_W_Head_Printed.getBoolean())))                                                                                                  //Natural: IF NOT #SUCC-W-HEAD-PRINTED
                {
                    getReports().write(1, ReportOption.NOHDR, writeMapToStringOutput(Wfof4504.class));                                                                    //Natural: WRITE ( 1 ) NOHDR USING FORM 'WFOF4504'
                    pnd_Succ_W_Head_Printed.setValue(true);                                                                                                               //Natural: ASSIGN #SUCC-W-HEAD-PRINTED := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOHDR, writeMapToStringOutput(Wfof4541.class));                                                                            //Natural: WRITE ( 1 ) NOHDR USING FORM 'WFOF4541'
            pnd_Client_Succ_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CLIENT-SUCC-CNT
            pnd_Client_Succ_W.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CLIENT-SUCC-W
            pnd_Succ_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #SUCC-CNT
            pnd_Succ_W.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #SUCC-W
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaWfol4500.getCmcv_Error_Msg_Txt().equals("INIT")))                                                                                            //Natural: IF CMCV.ERROR-MSG-TXT = 'INIT'
            {
                if (condition(! (pnd_Cont_Init_Head_Printed.getBoolean())))                                                                                               //Natural: IF NOT #CONT-INIT-HEAD-PRINTED
                {
                    //*  HEADER WITH SEQ,CONTAINER
                    pnd_Client_Init_Cont_Cnt.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CLIENT-INIT-CONT-CNT
                    getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Wfof4521.class));                                                                    //Natural: WRITE ( 2 ) NOHDR USING FORM 'WFOF4521'
                    pnd_Cont_Init_Head_Printed.setValue(true);                                                                                                            //Natural: ASSIGN #CONT-INIT-HEAD-PRINTED := TRUE
                    pnd_Init_W_Head_Printed.setValue(true);                                                                                                               //Natural: ASSIGN #INIT-W-HEAD-PRINTED := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(! (pnd_Init_W_Head_Printed.getBoolean())))                                                                                              //Natural: IF NOT #INIT-W-HEAD-PRINTED
                    {
                        getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Wfof4501.class));                                                                //Natural: WRITE ( 2 ) NOHDR USING FORM 'WFOF4501'
                        pnd_Init_W_Head_Printed.setValue(true);                                                                                                           //Natural: ASSIGN #INIT-W-HEAD-PRINTED := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Wfof4541.class));                                                                        //Natural: WRITE ( 2 ) NOHDR USING FORM 'WFOF4541'
                pnd_Client_Init_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CLIENT-INIT-CNT
                pnd_Client_Init_W.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CLIENT-INIT-W
                pnd_Init_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #INIT-CNT
                pnd_Init_W.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #INIT-W
                //*  ERROR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pnd_Cont_Error_Head_Printed.getBoolean())))                                                                                              //Natural: IF NOT #CONT-ERROR-HEAD-PRINTED
                {
                    //*  HEADER WITH SEQ,CONTAINER
                    pnd_Client_Error_Cont_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #CLIENT-ERROR-CONT-CNT
                    getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Wfof4531.class));                                                                    //Natural: WRITE ( 3 ) NOHDR USING FORM 'WFOF4531'
                    pnd_Cont_Error_Head_Printed.setValue(true);                                                                                                           //Natural: ASSIGN #CONT-ERROR-HEAD-PRINTED := TRUE
                    pnd_Error_W_Head_Printed.setValue(true);                                                                                                              //Natural: ASSIGN #ERROR-W-HEAD-PRINTED := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(! (pnd_Error_W_Head_Printed.getBoolean())))                                                                                             //Natural: IF NOT #ERROR-W-HEAD-PRINTED
                    {
                        getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Wfof4501.class));                                                                //Natural: WRITE ( 3 ) NOHDR USING FORM 'WFOF4501'
                        pnd_Error_W_Head_Printed.setValue(true);                                                                                                          //Natural: ASSIGN #ERROR-W-HEAD-PRINTED := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Wfof4541.class));                                                                        //Natural: WRITE ( 3 ) NOHDR USING FORM 'WFOF4541'
                pnd_Client_Error_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CLIENT-ERROR-CNT
                pnd_Client_Error_W.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CLIENT-ERROR-W
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
                pnd_Error_W.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #ERROR-W
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #WRK-RQST-IDX 2 TO 10
        for (pnd_Wrk_Rqst_Idx.setValue(2); condition(pnd_Wrk_Rqst_Idx.lessOrEqual(10)); pnd_Wrk_Rqst_Idx.nadd(1))
        {
            if (condition(ldaWfol4500.getCmcv_Action_Cde().getValue(pnd_Wrk_Rqst_Idx).equals(" ")))                                                                       //Natural: IF CMCV.ACTION-CDE ( #WRK-RQST-IDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM MOVE-ARRAYS-FIELDS-TO-MAP
            sub_Move_Arrays_Fields_To_Map();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  MEANING SUCCESSFUL
            if (condition(pnd_Init_Error_Cont.equals(" ")))                                                                                                               //Natural: IF #INIT-ERROR-CONT = ' '
            {
                getReports().write(1, ReportOption.NOHDR, writeMapToStringOutput(Wfof4541.class));                                                                        //Natural: WRITE ( 1 ) NOHDR USING FORM 'WFOF4541'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaWfol4500.getCmcv_Error_Msg_Txt().equals("INIT")))                                                                                        //Natural: IF CMCV.ERROR-MSG-TXT = 'INIT'
                {
                    getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Wfof4541.class));                                                                    //Natural: WRITE ( 2 ) NOHDR USING FORM 'WFOF4541'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Wfof4541.class));                                                                    //Natural: WRITE ( 3 ) NOHDR USING FORM 'WFOF4541'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-SPACES
        sub_Write_Spaces();
        if (condition(Global.isEscape())) {return;}
        //*  PROCESS-RECORD-TYPE-W
    }
    private void sub_Process_Record_Type_T() throws Exception                                                                                                             //Natural: PROCESS-RECORD-TYPE-T
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pnd_Text_Idx.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #TEXT-IDX
        //*  MEANING SUCCESSFUL
        if (condition(pnd_Init_Error_Cont.equals(" ")))                                                                                                                   //Natural: IF #INIT-ERROR-CONT = ' '
        {
            if (condition(! (pnd_Cont_Succ_Head_Printed.getBoolean())))                                                                                                   //Natural: IF NOT #CONT-SUCC-HEAD-PRINTED
            {
                //*  HEADER WITH SEQ,CONTAINER
                pnd_Client_Succ_Cont_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CLIENT-SUCC-CONT-CNT
                getReports().write(1, ReportOption.NOHDR, writeMapToStringOutput(Wfof4512.class));                                                                        //Natural: WRITE ( 1 ) NOHDR USING FORM 'WFOF4512'
                pnd_Cont_Succ_Head_Printed.setValue(true);                                                                                                                //Natural: ASSIGN #CONT-SUCC-HEAD-PRINTED := TRUE
                pnd_Succ_T_Head_Printed.setValue(true);                                                                                                                   //Natural: ASSIGN #SUCC-T-HEAD-PRINTED := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pnd_Succ_T_Head_Printed.getBoolean())))                                                                                                  //Natural: IF NOT #SUCC-T-HEAD-PRINTED
                {
                    getReports().write(1, ReportOption.NOHDR, writeMapToStringOutput(Wfof4505.class));                                                                    //Natural: WRITE ( 1 ) NOHDR USING FORM 'WFOF4505'
                    pnd_Succ_T_Head_Printed.setValue(false);                                                                                                              //Natural: ASSIGN #SUCC-T-HEAD-PRINTED := FALSE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOHDR, writeMapToStringOutput(Wfof4542.class));                                                                            //Natural: WRITE ( 1 ) NOHDR USING FORM 'WFOF4542'
            getReports().write(1, ReportOption.NOHDR, writeMapToStringOutput(Wfof4552.class));                                                                            //Natural: WRITE ( 1 ) NOHDR USING FORM 'WFOF4552'
            pnd_Client_Succ_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CLIENT-SUCC-CNT
            pnd_Client_Succ_T.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CLIENT-SUCC-T
            pnd_Succ_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #SUCC-CNT
            pnd_Succ_T.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #SUCC-T
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaWfol4500.getCmcv_Error_Msg_Txt().equals("INIT")))                                                                                            //Natural: IF CMCV.ERROR-MSG-TXT = 'INIT'
            {
                if (condition(! (pnd_Cont_Init_Head_Printed.getBoolean())))                                                                                               //Natural: IF NOT #CONT-INIT-HEAD-PRINTED
                {
                    //*  HEADER WITH SEQ,CONTAINER
                    pnd_Client_Init_Cont_Cnt.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CLIENT-INIT-CONT-CNT
                    getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Wfof4522.class));                                                                    //Natural: WRITE ( 2 ) NOHDR USING FORM 'WFOF4522'
                    pnd_Cont_Init_Head_Printed.setValue(true);                                                                                                            //Natural: ASSIGN #CONT-INIT-HEAD-PRINTED := TRUE
                    pnd_Init_T_Head_Printed.setValue(true);                                                                                                               //Natural: ASSIGN #INIT-T-HEAD-PRINTED := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(! (pnd_Init_T_Head_Printed.getBoolean())))                                                                                              //Natural: IF NOT #INIT-T-HEAD-PRINTED
                    {
                        getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Wfof4502.class));                                                                //Natural: WRITE ( 2 ) NOHDR USING FORM 'WFOF4502'
                        pnd_Init_T_Head_Printed.setValue(true);                                                                                                           //Natural: ASSIGN #INIT-T-HEAD-PRINTED := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Wfof4542.class));                                                                        //Natural: WRITE ( 2 ) NOHDR USING FORM 'WFOF4542'
                getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Wfof4552.class));                                                                        //Natural: WRITE ( 2 ) NOHDR USING FORM 'WFOF4552'
                pnd_Client_Init_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CLIENT-INIT-CNT
                pnd_Client_Init_T.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CLIENT-INIT-T
                pnd_Init_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #INIT-CNT
                pnd_Init_T.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #INIT-T
                //*  ERROR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pnd_Cont_Error_Head_Printed.getBoolean())))                                                                                              //Natural: IF NOT #CONT-ERROR-HEAD-PRINTED
                {
                    //*  HEADER WITH SEQ,CONTAINER
                    pnd_Client_Error_Cont_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #CLIENT-ERROR-CONT-CNT
                    getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Wfof4532.class));                                                                    //Natural: WRITE ( 3 ) NOHDR USING FORM 'WFOF4532'
                    pnd_Cont_Error_Head_Printed.setValue(true);                                                                                                           //Natural: ASSIGN #CONT-ERROR-HEAD-PRINTED := TRUE
                    pnd_Error_T_Head_Printed.setValue(true);                                                                                                              //Natural: ASSIGN #ERROR-T-HEAD-PRINTED := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(! (pnd_Error_T_Head_Printed.getBoolean())))                                                                                             //Natural: IF NOT #ERROR-T-HEAD-PRINTED
                    {
                        getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Wfof4502.class));                                                                //Natural: WRITE ( 3 ) NOHDR USING FORM 'WFOF4502'
                        pnd_Error_T_Head_Printed.setValue(true);                                                                                                          //Natural: ASSIGN #ERROR-T-HEAD-PRINTED := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Wfof4542.class));                                                                        //Natural: WRITE ( 3 ) NOHDR USING FORM 'WFOF4542'
                getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Wfof4552.class));                                                                        //Natural: WRITE ( 3 ) NOHDR USING FORM 'WFOF4552'
                pnd_Client_Error_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CLIENT-ERROR-CNT
                pnd_Client_Error_T.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CLIENT-ERROR-T
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
                pnd_Error_T.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #ERROR-T
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #TEXT-IDX 2 TO 35
        for (pnd_Text_Idx.setValue(2); condition(pnd_Text_Idx.lessOrEqual(35)); pnd_Text_Idx.nadd(1))
        {
            if (condition(ldaWfol4500.getCmcv_T_Wf_Ind().getValue(pnd_Text_Idx).equals(" ")))                                                                             //Natural: IF CMCV.T-WF-IND ( #TEXT-IDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM MOVE-TEXT-FIELDS
            sub_Move_Text_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  MEANING SUCCESSFUL
            if (condition(pnd_Init_Error_Cont.equals(" ")))                                                                                                               //Natural: IF #INIT-ERROR-CONT = ' '
            {
                getReports().write(1, ReportOption.NOHDR, writeMapToStringOutput(Wfof4552.class));                                                                        //Natural: WRITE ( 1 ) NOHDR USING FORM 'WFOF4552'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaWfol4500.getCmcv_Error_Msg_Txt().equals("INIT")))                                                                                        //Natural: IF CMCV.ERROR-MSG-TXT = 'INIT'
                {
                    getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Wfof4552.class));                                                                    //Natural: WRITE ( 2 ) NOHDR USING FORM 'WFOF4552'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Wfof4552.class));                                                                    //Natural: WRITE ( 3 ) NOHDR USING FORM 'WFOF4552'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-SPACES
        sub_Write_Spaces();
        if (condition(Global.isEscape())) {return;}
        //*  PROCESS-RECORD-TYPE-T
    }
    private void sub_Process_Record_Type_K() throws Exception                                                                                                             //Natural: PROCESS-RECORD-TYPE-K
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*  MEANING SUCCESSFUL
        if (condition(pnd_Init_Error_Cont.equals(" ")))                                                                                                                   //Natural: IF #INIT-ERROR-CONT = ' '
        {
            if (condition(! (pnd_Cont_Succ_Head_Printed.getBoolean())))                                                                                                   //Natural: IF NOT #CONT-SUCC-HEAD-PRINTED
            {
                //*  HEADER WITH SEQ,CONTAINER
                pnd_Client_Succ_Cont_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CLIENT-SUCC-CONT-CNT
                getReports().write(1, ReportOption.NOHDR, writeMapToStringOutput(Wfof4513.class));                                                                        //Natural: WRITE ( 1 ) NOHDR USING FORM 'WFOF4513'
                pnd_Cont_Succ_Head_Printed.setValue(true);                                                                                                                //Natural: ASSIGN #CONT-SUCC-HEAD-PRINTED := TRUE
                pnd_Succ_K_Head_Printed.setValue(true);                                                                                                                   //Natural: ASSIGN #SUCC-K-HEAD-PRINTED := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pnd_Succ_K_Head_Printed.getBoolean())))                                                                                                  //Natural: IF NOT #SUCC-K-HEAD-PRINTED
                {
                    getReports().write(1, ReportOption.NOHDR, writeMapToStringOutput(Wfof4506.class));                                                                    //Natural: WRITE ( 1 ) NOHDR USING FORM 'WFOF4506'
                    pnd_Succ_K_Head_Printed.setValue(true);                                                                                                               //Natural: ASSIGN #SUCC-K-HEAD-PRINTED := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOHDR, writeMapToStringOutput(Wfof4543.class));                                                                            //Natural: WRITE ( 1 ) NOHDR USING FORM 'WFOF4543'
            pnd_Client_Succ_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CLIENT-SUCC-CNT
            pnd_Client_Succ_K.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CLIENT-SUCC-K
            pnd_Succ_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #SUCC-CNT
            pnd_Succ_K.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #SUCC-K
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaWfol4500.getCmcv_Error_Msg_Txt().equals("INIT")))                                                                                            //Natural: IF CMCV.ERROR-MSG-TXT = 'INIT'
            {
                if (condition(! (pnd_Cont_Init_Head_Printed.getBoolean())))                                                                                               //Natural: IF NOT #CONT-INIT-HEAD-PRINTED
                {
                    //*  HEADER WITH SEQ,CONTAINER
                    pnd_Client_Init_Cont_Cnt.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CLIENT-INIT-CONT-CNT
                    getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Wfof4523.class));                                                                    //Natural: WRITE ( 2 ) NOHDR USING FORM 'WFOF4523'
                    pnd_Cont_Init_Head_Printed.setValue(true);                                                                                                            //Natural: ASSIGN #CONT-INIT-HEAD-PRINTED := TRUE
                    pnd_Init_K_Head_Printed.setValue(true);                                                                                                               //Natural: ASSIGN #INIT-K-HEAD-PRINTED := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(! (pnd_Init_K_Head_Printed.getBoolean())))                                                                                              //Natural: IF NOT #INIT-K-HEAD-PRINTED
                    {
                        getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Wfof4503.class));                                                                //Natural: WRITE ( 2 ) NOHDR USING FORM 'WFOF4503'
                        pnd_Init_K_Head_Printed.setValue(true);                                                                                                           //Natural: ASSIGN #INIT-K-HEAD-PRINTED := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Wfof4543.class));                                                                        //Natural: WRITE ( 2 ) NOHDR USING FORM 'WFOF4543'
                pnd_Client_Init_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CLIENT-INIT-CNT
                pnd_Client_Init_K.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CLIENT-INIT-K
                pnd_Init_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #INIT-CNT
                pnd_Init_K.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #INIT-K
                //*  ERROR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pnd_Cont_Error_Head_Printed.getBoolean())))                                                                                              //Natural: IF NOT #CONT-ERROR-HEAD-PRINTED
                {
                    //*  HEADER WITH SEQ,CONTAINER
                    pnd_Client_Error_Cont_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #CLIENT-ERROR-CONT-CNT
                    getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Wfof4533.class));                                                                    //Natural: WRITE ( 3 ) NOHDR USING FORM 'WFOF4533'
                    pnd_Cont_Error_Head_Printed.setValue(true);                                                                                                           //Natural: ASSIGN #CONT-ERROR-HEAD-PRINTED := TRUE
                    pnd_Error_K_Head_Printed.setValue(true);                                                                                                              //Natural: ASSIGN #ERROR-K-HEAD-PRINTED := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(! (pnd_Error_K_Head_Printed.getBoolean())))                                                                                             //Natural: IF NOT #ERROR-K-HEAD-PRINTED
                    {
                        getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Wfof4503.class));                                                                //Natural: WRITE ( 3 ) NOHDR USING FORM 'WFOF4503'
                        pnd_Error_K_Head_Printed.setValue(true);                                                                                                          //Natural: ASSIGN #ERROR-K-HEAD-PRINTED := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Wfof4543.class));                                                                        //Natural: WRITE ( 3 ) NOHDR USING FORM 'WFOF4543'
                pnd_Client_Error_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CLIENT-ERROR-CNT
                pnd_Client_Error_K.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CLIENT-ERROR-K
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
                pnd_Error_K.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #ERROR-K
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-SPACES
        sub_Write_Spaces();
        if (condition(Global.isEscape())) {return;}
        //*  PROCESS-RECORD-TYPE-K
    }
    private void sub_Write_Spaces() throws Exception                                                                                                                      //Natural: WRITE-SPACES
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        //*  SUCCESSFUL
        if (condition(pnd_Init_Error_Cont.equals(" ")))                                                                                                                   //Natural: IF #INIT-ERROR-CONT = ' '
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE);                                                                                                  //Natural: WRITE ( 1 ) //
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaWfol4500.getCmcv_Error_Msg_Txt().equals("INIT")))                                                                                            //Natural: IF CMCV.ERROR-MSG-TXT = 'INIT'
            {
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE);                                                                                              //Natural: WRITE ( 2 ) //
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE);                                                                                              //Natural: WRITE ( 3 ) //
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-SPACES
    }
    private void sub_Move_Arrays_Fields_To_Map() throws Exception                                                                                                         //Natural: MOVE-ARRAYS-FIELDS-TO-MAP
    {
        if (BLNatReinput.isReinput()) return;

        ldaWfol4500.getCmcv_Cabinet_Id().getValue(1).setValue(ldaWfol4500.getCmcv_Cabinet_Id().getValue(pnd_Wrk_Rqst_Idx));                                               //Natural: ASSIGN CMCV.CABINET-ID ( 1 ) := CMCV.CABINET-ID ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(1).setValue(ldaWfol4500.getCmcv_Dstntn_Unt_Cde().getValue(pnd_Wrk_Rqst_Idx));                                       //Natural: ASSIGN CMCV.DSTNTN-UNT-CDE ( 1 ) := CMCV.DSTNTN-UNT-CDE ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(1).setValue(ldaWfol4500.getCmcv_Corp_Status_Cde().getValue(pnd_Wrk_Rqst_Idx));                                     //Natural: ASSIGN CMCV.CORP-STATUS-CDE ( 1 ) := CMCV.CORP-STATUS-CDE ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(1).setValue(ldaWfol4500.getCmcv_Unit_Status_Cde().getValue(pnd_Wrk_Rqst_Idx));                                     //Natural: ASSIGN CMCV.UNIT-STATUS-CDE ( 1 ) := CMCV.UNIT-STATUS-CDE ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(1).setValue(ldaWfol4500.getCmcv_Ind_Tracking_Id().getValue(pnd_Wrk_Rqst_Idx));                                     //Natural: ASSIGN CMCV.IND-TRACKING-ID ( 1 ) := CMCV.IND-TRACKING-ID ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Wpid_Cde().getValue(1).setValue(ldaWfol4500.getCmcv_Wpid_Cde().getValue(pnd_Wrk_Rqst_Idx));                                                   //Natural: ASSIGN CMCV.WPID-CDE ( 1 ) := CMCV.WPID-CDE ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Wf_Ind().getValue(1).setValue(ldaWfol4500.getCmcv_Wf_Ind().getValue(pnd_Wrk_Rqst_Idx));                                                       //Natural: ASSIGN CMCV.WF-IND ( 1 ) := CMCV.WF-IND ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(1).setValue(ldaWfol4500.getCmcv_Empl_Racf_Id().getValue(pnd_Wrk_Rqst_Idx));                                           //Natural: ASSIGN CMCV.EMPL-RACF-ID ( 1 ) := CMCV.EMPL-RACF-ID ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(1).setValue(ldaWfol4500.getCmcv_Inst_Tracking_Id().getValue(pnd_Wrk_Rqst_Idx));                                   //Natural: ASSIGN CMCV.INST-TRACKING-ID ( 1 ) := CMCV.INST-TRACKING-ID ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Due_Dte().getValue(1).setValue(ldaWfol4500.getCmcv_Due_Dte().getValue(pnd_Wrk_Rqst_Idx));                                                     //Natural: ASSIGN CMCV.DUE-DTE ( 1 ) := CMCV.DUE-DTE ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Cabinet_Level().getValue(1).setValue(ldaWfol4500.getCmcv_Cabinet_Level().getValue(pnd_Wrk_Rqst_Idx));                                         //Natural: ASSIGN CMCV.CABINET-LEVEL ( 1 ) := CMCV.CABINET-LEVEL ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Client_App_Cde().getValue(1).setValue(ldaWfol4500.getCmcv_Client_App_Cde().getValue(pnd_Wrk_Rqst_Idx));                                       //Natural: ASSIGN CMCV.CLIENT-APP-CDE ( 1 ) := CMCV.CLIENT-APP-CDE ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Action_Cde().getValue(1).setValue(ldaWfol4500.getCmcv_Action_Cde().getValue(pnd_Wrk_Rqst_Idx));                                               //Natural: ASSIGN CMCV.ACTION-CDE ( 1 ) := CMCV.ACTION-CDE ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Topic().getValue(1).setValue(ldaWfol4500.getCmcv_Topic().getValue(pnd_Wrk_Rqst_Idx));                                                         //Natural: ASSIGN CMCV.TOPIC ( 1 ) := CMCV.TOPIC ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Step().getValue(1).setValue(ldaWfol4500.getCmcv_Step().getValue(pnd_Wrk_Rqst_Idx));                                                           //Natural: ASSIGN CMCV.STEP ( 1 ) := CMCV.STEP ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(1).setValue(ldaWfol4500.getCmcv_Elctrnc_Fldr_Ind().getValue(pnd_Wrk_Rqst_Idx));                                   //Natural: ASSIGN CMCV.ELCTRNC-FLDR-IND ( 1 ) := CMCV.ELCTRNC-FLDR-IND ( #WRK-RQST-IDX )
        ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(1).setValue(ldaWfol4500.getCmcv_Rqst_Orgn_Cde().getValue(pnd_Wrk_Rqst_Idx));                                         //Natural: ASSIGN CMCV.RQST-ORGN-CDE ( 1 ) := CMCV.RQST-ORGN-CDE ( #WRK-RQST-IDX )
        //*  MOVE-ARRAYS-FIELDS-TO-MAP
    }
    private void sub_Move_Text_Fields() throws Exception                                                                                                                  //Natural: MOVE-TEXT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(1).setValue(ldaWfol4500.getCmcv_T_Cabinet_Level().getValue(pnd_Text_Idx));                                         //Natural: ASSIGN CMCV.T-CABINET-LEVEL ( 1 ) := CMCV.T-CABINET-LEVEL ( #TEXT-IDX )
        ldaWfol4500.getCmcv_T_Wf_Ind().getValue(1).setValue(ldaWfol4500.getCmcv_T_Wf_Ind().getValue(pnd_Text_Idx));                                                       //Natural: ASSIGN CMCV.T-WF-IND ( 1 ) := CMCV.T-WF-IND ( #TEXT-IDX )
        ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(1).setValue(ldaWfol4500.getCmcv_T_Cabinet_Id().getValue(pnd_Text_Idx));                                               //Natural: ASSIGN CMCV.T-CABINET-ID ( 1 ) := CMCV.T-CABINET-ID ( #TEXT-IDX )
        ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(1).setValue(ldaWfol4500.getCmcv_T_Inst_Tracking_Id().getValue(pnd_Text_Idx));                                   //Natural: ASSIGN CMCV.T-INST-TRACKING-ID ( 1 ) := CMCV.T-INST-TRACKING-ID ( #TEXT-IDX )
        ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(1).setValue(ldaWfol4500.getCmcv_T_Ind_Tracking_Id().getValue(pnd_Text_Idx));                                     //Natural: ASSIGN CMCV.T-IND-TRACKING-ID ( 1 ) := CMCV.T-IND-TRACKING-ID ( #TEXT-IDX )
        //*  MOVE-TEXT-FIELDS
    }
    private void sub_Client_Footings() throws Exception                                                                                                                   //Natural: CLIENT-FOOTINGS
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,"**** Nothing Follows for Client App Code : ",pnd_Save_Client_App_Cde," ****");                                        //Natural: WRITE ( 1 ) '**** Nothing Follows for Client App Code : ' #SAVE-CLIENT-APP-CDE ' ****'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"**** Nothing Follows for Client App Code : ",pnd_Save_Client_App_Cde," ****");                                        //Natural: WRITE ( 2 ) '**** Nothing Follows for Client App Code : ' #SAVE-CLIENT-APP-CDE ' ****'
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"**** Nothing Follows for Client App Code : ",pnd_Save_Client_App_Cde," ****");                                        //Natural: WRITE ( 3 ) '**** Nothing Follows for Client App Code : ' #SAVE-CLIENT-APP-CDE ' ****'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Client Total SUCCESSFUL Container Cnt ",pnd_Client_Succ_Cont_Cnt,NEWLINE,NEWLINE,"Client Total SUCCESSFUL Messages  Cnt ", //Natural: WRITE ( 1 ) / 'Client Total SUCCESSFUL Container Cnt ' #CLIENT-SUCC-CONT-CNT // 'Client Total SUCCESSFUL Messages  Cnt ' #CLIENT-SUCC-CNT / '              Client Total WrkRqst Records  ' #CLIENT-SUCC-W / '              Client Total Txt/Img Records  ' #CLIENT-SUCC-T / '              Client Total ATI     Records  ' #CLIENT-SUCC-K //
            pnd_Client_Succ_Cnt,NEWLINE,"              Client Total WrkRqst Records  ",pnd_Client_Succ_W,NEWLINE,"              Client Total Txt/Img Records  ",
            pnd_Client_Succ_T,NEWLINE,"              Client Total ATI     Records  ",pnd_Client_Succ_K,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"Client Total INIT       Container Cnt ",pnd_Client_Init_Cont_Cnt,NEWLINE,NEWLINE,"Client Total INIT       Messages  Cnt ", //Natural: WRITE ( 2 ) / 'Client Total INIT       Container Cnt ' #CLIENT-INIT-CONT-CNT // 'Client Total INIT       Messages  Cnt ' #CLIENT-INIT-CNT / '              Client Total WrkRqst Records  ' #CLIENT-INIT-W / '              Client Total Txt/Img Records  ' #CLIENT-INIT-T / '              Client Total ATI     Records  ' #CLIENT-INIT-K //
            pnd_Client_Init_Cnt,NEWLINE,"              Client Total WrkRqst Records  ",pnd_Client_Init_W,NEWLINE,"              Client Total Txt/Img Records  ",
            pnd_Client_Init_T,NEWLINE,"              Client Total ATI     Records  ",pnd_Client_Init_K,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,"Client Total ERROR      Container Cnt ",pnd_Client_Error_Cont_Cnt,NEWLINE,NEWLINE,"Client Total ERROR      Messages  Cnt ", //Natural: WRITE ( 3 ) / 'Client Total ERROR      Container Cnt ' #CLIENT-ERROR-CONT-CNT // 'Client Total ERROR      Messages  Cnt ' #CLIENT-ERROR-CNT / '              Client Total WrkRqst Records  ' #CLIENT-ERROR-W / '              Client Total Txt/Img Records  ' #CLIENT-ERROR-T / '              Client Total ATI     Records  ' #CLIENT-ERROR-K /
            pnd_Client_Error_Cnt,NEWLINE,"              Client Total WrkRqst Records  ",pnd_Client_Error_W,NEWLINE,"              Client Total Txt/Img Records  ",
            pnd_Client_Error_T,NEWLINE,"              Client Total ATI     Records  ",pnd_Client_Error_K,NEWLINE);
        if (Global.isEscape()) return;
        //*  CLIENT-FOOTINGS
    }
    private void sub_Reset_Client_Ctrs() throws Exception                                                                                                                 //Natural: RESET-CLIENT-CTRS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        pnd_Client_Succ_Cont_Cnt.reset();                                                                                                                                 //Natural: RESET #CLIENT-SUCC-CONT-CNT #CLIENT-SUCC-CNT #CLIENT-SUCC-W #CLIENT-SUCC-T #CLIENT-SUCC-K #CLIENT-INIT-CONT-CNT #CLIENT-INIT-CNT #CLIENT-INIT-W #CLIENT-INIT-T #CLIENT-INIT-K #CLIENT-ERROR-CONT-CNT #CLIENT-ERROR-CNT #CLIENT-ERROR-W #CLIENT-ERROR-T #CLIENT-ERROR-K #CLIENT-TOT-CNT
        pnd_Client_Succ_Cnt.reset();
        pnd_Client_Succ_W.reset();
        pnd_Client_Succ_T.reset();
        pnd_Client_Succ_K.reset();
        pnd_Client_Init_Cont_Cnt.reset();
        pnd_Client_Init_Cnt.reset();
        pnd_Client_Init_W.reset();
        pnd_Client_Init_T.reset();
        pnd_Client_Init_K.reset();
        pnd_Client_Error_Cont_Cnt.reset();
        pnd_Client_Error_Cnt.reset();
        pnd_Client_Error_W.reset();
        pnd_Client_Error_T.reset();
        pnd_Client_Error_K.reset();
        pnd_Client_Tot_Cnt.reset();
        pnd_Client_Succ_Page_Nbr.setValue(0);                                                                                                                             //Natural: ASSIGN #CLIENT-SUCC-PAGE-NBR := 0
        pnd_Client_Init_Page_Nbr.setValue(0);                                                                                                                             //Natural: ASSIGN #CLIENT-INIT-PAGE-NBR := 0
        pnd_Client_Err_Page_Nbr.setValue(0);                                                                                                                              //Natural: ASSIGN #CLIENT-ERR-PAGE-NBR := 0
        //*  RESET-CLIENT-CTRS
    }
    private void sub_Check_Container_Stat() throws Exception                                                                                                              //Natural: CHECK-CONTAINER-STAT
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_Init_Error_Cont.reset();                                                                                                                                      //Natural: RESET #INIT-ERROR-CONT
        DbsUtil.callnat(Wfon4500.class , getCurrentProcessState(), pnd_Curr_Cont_Id, pnd_Init_Error_Cont);                                                                //Natural: CALLNAT 'WFON4500' USING #CURR-CONT-ID #INIT-ERROR-CONT
        if (condition(Global.isEscape())) return;
        //*  READ CMCV3 BY CONTAINER-RCRD-TYPE-KEY     = #CURR-CONT-ID
        //*   IF CMCV3.CONTAINER-ID NE #CURR-CONT-ID
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   IF CMCV3.ERROR-MSG-TXT NE ' '
        //*     MOVE 'Y' TO #INIT-ERROR-CONT
        //*   END-IF
        //*  END-READ
        //*  CHECK-CONTAINER-STAT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Client_Succ_Page_Nbr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CLIENT-SUCC-PAGE-NBR
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new ColumnSpacing(2),"++++++++++ WFO RECORDS UPLOADED WITH GSUCCESSFULG MESSAGE TEXT ++++++++++ FROM :",pnd_Date_Time,  //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 2X '++++++++++ WFO RECORDS UPLOADED WITH ''SUCCESSFUL'' MESSAGE TEXT ++++++++++ FROM :' #DATE-TIME ( EM = YYYY/MM/DD ) ' ' #DATE-TIME ( EM = HH:II:SS:T ) 2X 'PAGE :' #CLIENT-SUCC-PAGE-NBR
                        new ReportEditMask ("YYYY/MM/DD")," ",pnd_Date_Time, new ReportEditMask ("HH:II:SS:T"),new ColumnSpacing(2),"PAGE :",pnd_Client_Succ_Page_Nbr);
                    //*    CWF-SUPPORT-TBL.#TBL-DTE ' ' CWF-SUPPORT-TBL.#TBL-TME 5X
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(30),"CLIENT APPLICATION CODE: ",pnd_Save_Client_App_Cde);             //Natural: WRITE ( 1 ) NOTITLE NOHDR 30X 'CLIENT APPLICATION CODE: ' #SAVE-CLIENT-APP-CDE
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE);                                                                                                  //Natural: WRITE ( 1 ) /
                    getReports().write(1, ReportOption.NOTITLE,"Seq       Container ID");                                                                                 //Natural: WRITE ( 1 ) 'Seq       Container ID'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Client_Init_Page_Nbr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CLIENT-INIT-PAGE-NBR
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new ColumnSpacing(2),"++++++++++ WFO RECORDS UPLOADED WITH GINITG MESSAGE TEXT ++++++++++ FROM :",pnd_Date_Time,  //Natural: WRITE ( 2 ) NOTITLE NOHDR *PROGRAM 2X '++++++++++ WFO RECORDS UPLOADED WITH ''INIT'' MESSAGE TEXT ++++++++++ FROM :' #DATE-TIME ( EM = YYYY/MM/DD ) ' ' #DATE-TIME ( EM = HH:II:SS:T ) 2X 'PAGE :' #CLIENT-INIT-PAGE-NBR
                        new ReportEditMask ("YYYY/MM/DD")," ",pnd_Date_Time, new ReportEditMask ("HH:II:SS:T"),new ColumnSpacing(2),"PAGE :",pnd_Client_Init_Page_Nbr);
                    //*    CWF-SUPPORT-TBL.#TBL-DTE ' ' CWF-SUPPORT-TBL.#TBL-TME 5X
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(30),"CLIENT APPLICATION CODE: ",pnd_Save_Client_App_Cde);             //Natural: WRITE ( 2 ) NOTITLE NOHDR 30X 'CLIENT APPLICATION CODE: ' #SAVE-CLIENT-APP-CDE
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE);                                                                                                  //Natural: WRITE ( 2 ) /
                    getReports().write(2, ReportOption.NOTITLE,"Seq       Container ID");                                                                                 //Natural: WRITE ( 2 ) 'Seq       Container ID'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Client_Err_Page_Nbr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #CLIENT-ERR-PAGE-NBR
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new ColumnSpacing(2),"++++++++++ WFO RECORDS UPLOADED WITH GERRORG MESSAGE TEXT ++++++++++ FROM :",pnd_Date_Time,  //Natural: WRITE ( 3 ) NOTITLE NOHDR *PROGRAM 2X '++++++++++ WFO RECORDS UPLOADED WITH ''ERROR'' MESSAGE TEXT ++++++++++ FROM :' #DATE-TIME ( EM = YYYY/MM/DD ) ' ' #DATE-TIME ( EM = HH:II:SS:T ) 2X 'PAGE :' #CLIENT-ERR-PAGE-NBR
                        new ReportEditMask ("YYYY/MM/DD")," ",pnd_Date_Time, new ReportEditMask ("HH:II:SS:T"),new ColumnSpacing(2),"PAGE :",pnd_Client_Err_Page_Nbr);
                    //*    CWF-SUPPORT-TBL.#TBL-DTE ' ' CWF-SUPPORT-TBL.#TBL-TME 5X
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(30),"CLIENT APPLICATION CODE: ",pnd_Save_Client_App_Cde);             //Natural: WRITE ( 3 ) NOTITLE NOHDR 30X 'CLIENT APPLICATION CODE: ' #SAVE-CLIENT-APP-CDE
                    getReports().write(3, ReportOption.NOTITLE,NEWLINE);                                                                                                  //Natural: WRITE ( 3 ) /
                    getReports().write(3, ReportOption.NOTITLE,"Seq       Container ID");                                                                                 //Natural: WRITE ( 3 ) 'Seq       Container ID'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "NATURAL ERROR",Global.getERROR_NR(),"ON LINE",Global.getERROR_LINE(),"OF",Global.getPROGRAM());                                            //Natural: WRITE 'NATURAL ERROR' *ERROR-NR 'ON LINE' *ERROR-LINE 'OF' *PROGRAM
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60 ES=OFF");
        Global.format(2, "LS=132 PS=60 ES=OFF");
        Global.format(3, "LS=132 PS=60 ES=OFF");
    }
    private void CheckAtStartofData389() throws Exception
    {
        if (condition(ldaWfol4500.getVw_cmcv().getAtStartOfData()))
        {
            pnd_Curr_Cont_Id.reset();                                                                                                                                     //Natural: RESET #CURR-CONT-ID
            pnd_Init_Error_Cont.reset();                                                                                                                                  //Natural: RESET #INIT-ERROR-CONT
        }                                                                                                                                                                 //Natural: END-START
    }
    private void CheckAtStartofData446() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
            pnd_Save_Client_App_Cde.setValue(pnd_Hold_Client_App_Cde);                                                                                                    //Natural: MOVE #HOLD-CLIENT-APP-CDE TO #SAVE-CLIENT-APP-CDE
            pnd_Succ_W_Head_Printed.reset();                                                                                                                              //Natural: RESET #SUCC-W-HEAD-PRINTED #INIT-W-HEAD-PRINTED #ERROR-W-HEAD-PRINTED
            pnd_Init_W_Head_Printed.reset();
            pnd_Error_W_Head_Printed.reset();
            pnd_Succ_T_Head_Printed.reset();                                                                                                                              //Natural: RESET #SUCC-T-HEAD-PRINTED #INIT-T-HEAD-PRINTED #ERROR-T-HEAD-PRINTED
            pnd_Init_T_Head_Printed.reset();
            pnd_Error_T_Head_Printed.reset();
            pnd_Succ_K_Head_Printed.reset();                                                                                                                              //Natural: RESET #SUCC-K-HEAD-PRINTED #INIT-K-HEAD-PRINTED #ERROR-K-HEAD-PRINTED
            pnd_Init_K_Head_Printed.reset();
            pnd_Error_K_Head_Printed.reset();
            pnd_Cont_Succ_Head_Printed.reset();                                                                                                                           //Natural: RESET #CONT-SUCC-HEAD-PRINTED #CONT-INIT-HEAD-PRINTED #CONT-ERROR-HEAD-PRINTED
            pnd_Cont_Init_Head_Printed.reset();
            pnd_Cont_Error_Head_Printed.reset();
        }                                                                                                                                                                 //Natural: END-START
    }
}
