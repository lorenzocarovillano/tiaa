/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:08:05 PM
**        * FROM NATURAL PROGRAM : Efsb6120
************************************************************
**        * FILE NAME            : Efsb6120.java
**        * CLASS NAME           : Efsb6120
**        * INSTANCE NAME        : Efsb6120
************************************************************
************************************************************************
* PROGRAM  : EFSB6120
* SYSTEM   : CRPCWF
* TITLE    : IMAGE-XREF AND DOCUMENT FILES RECONCILATION REPORTS
* WRITTEN  : JANUARY, 21
* FUNCTION : THIS PROGRAM READ WORK FILE, RECORD TYPE = "X","D","I","N"
*          : FOR RECORD TYPE "X" (XREF WITHOUT DOCUMENT) -
*          : CALL SUBPROGRAM EFSN5130 TO CREATE MISSING DOCUMENT.
*          : THE FOLLOWING REPORTS WILL BE PRODUCED:
*          :  1. MAIL ITEMS ON XREF BUT NOT ON DOCUMENT W/CREATION MARK
*          :  2. MAIL ITEMS ON DOCUMENTS BUT NOT ON XREF
*          :  3. ERROR MESSAGE REPORT
* HISTORY  :
* MM/DD/YY : 02/06/03 NEWPAGE LOGIC CHANGED TOPREVENT SKIPPING (L.E.)
* MM/DD/YY : 02/06/03 FORM NAMES CHANGED TO REFLECT REPORT     (L.E.)
* MM/DD/YY : 02/06/03 BACKOUT TRANSACTION ADDED TO PREVENT UPDATE (L.E.)
* MM/DD/YY : 02/11/03 FORM TO PRINT XREF REPORT CHANGED(EFSF8710) (L.E.)
* MM/DD/YY : 02/24/03 FORM TO PRINT XREF REPORT CHANGED(EFSF8711) (L.E.)
* MM/DD/YY : 02/26/03 #SOURCE-ID-MIN-KEY HAS BEEN REDEFINED       (L.E.)
* MM/DD/YY : 02/26/03 NEW COUNTER #ZERO-PIN-ALL-CNT #XRE-ALL-COUNT(L.E.)
* MM/DD/YY : 02/26/03 NEW COUNTER #CWF-TODAY                      (L.E.)
* MM/DD/YY : 02/26/03 POPULATE SOURCE & MIN AT THE BEGINNING      (L.E.)
* MM/DD/YY : 02/26/03 NEWPAGE COMMENTED (4470, 4500, 4530)        (L.E.)
* MM/DD/YY : 02/26/03 LINE CNTR FOR ANY REPORT HAS BEEN ELIMINATED(L.E.)
* MM/DD/YY : 02/26/03 NEW ROUTINE PRINT-SOURCE-TOTAL ADDED        (L.E.)
* MM/DD/YY : 03/19/03 CWF-UPLOAD-AUDIT FILE HAS BEEN ADDED        (L.E.)
* MM/DD/YY : 03/19/03 DO NOT DISPLAY "CALLIGO" RECORDS            (L.E.)
* MM/DD/YY : 03/19/03 DISPLAY ALL RECORDS ON REPORT               (L.E.)
* MM/DD/YY : 03/19/03 CREATE DOCS. ONLY FOR "POST" & "PCPOST"     (L.E.)
* MM/DD/YY : 03/19/03 SOURCE-TOTAL ROUTINES FOR XRE & DOC SEPARET.(L.E.)
* MM/DD/YY : 04/02/03 COMPARISON BETWEEN RECORDS DONE FOR 18 CHAR.(L.E.)
* MM/DD/YY : 04/02/03 TWO NEW MAP/FORMS EFSF8712 AND EFSF8713     (L.E.)
* MM/DD/YY : 05/06/03 RESET #PIN-PPG #INIT-UNIT #CURR-UNIT        (L.E.)
* MM/DD/YY : 06/10/03 "CALLIGO" SCANED THRUE THE ENTIRE FIELD     (L.E.)
* MM/DD/YY : 06/10/03 DON't create Docum. if #PCKGE-CODE = 'POST' (L.E.)
* 02/23/2017 - SARKAB - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsb6120 extends BLNatBase
{
    // Data Areas
    private PdaEfsa6110 pdaEfsa6110;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Image_Xref;
    private DbsField cwf_Image_Xref_Batch_Id;
    private DbsField cwf_Image_Xref_Mail_Item_Nbr;
    private DbsField cwf_Image_Xref_Image_Document_Address_Cde;
    private DbsField cwf_Image_Xref_Ph_Unique_Id_Nbr;
    private DbsField cwf_Image_Xref_Image_Quality_Ind;
    private DbsField cwf_Image_Xref_Upld_Date_Time;
    private DbsField cwf_Image_Xref_Number_Of_Pages;
    private DbsField cwf_Image_Xref_Source_Id;
    private DbsField cwf_Image_Xref_Image_Delete_Ind;
    private DbsField cwf_Image_Xref_Cabinet_Id;
    private DbsField cwf_Image_Xref_Archive_Dte_Tme;
    private DbsField cwf_Image_Xref_Identify_Dte_Tme;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Actve_Ind;
    private DbsField cwf_Master_Index_View_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Id;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;

    private DataAccessProgramView vw_cwf_Efm_Document;
    private DbsField cwf_Efm_Document_Cabinet_Id;

    private DbsGroup cwf_Efm_Document__R_Field_1;
    private DbsField cwf_Efm_Document_Pin_Prefix;
    private DbsField cwf_Efm_Document_Pin_Nbr;

    private DbsGroup cwf_Efm_Document_Folder_Id;
    private DbsField cwf_Efm_Document_Tiaa_Rcvd_Dte;
    private DbsField cwf_Efm_Document_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Efm_Document__R_Field_2;
    private DbsField cwf_Efm_Document_Case_Ind;
    private DbsField cwf_Efm_Document_Originating_Work_Prcss_Id;
    private DbsField cwf_Efm_Document_Multi_Rqst_Ind;
    private DbsField cwf_Efm_Document_Sub_Rqst_Ind;
    private DbsField cwf_Efm_Document_Entry_Dte_Tme;
    private DbsField cwf_Efm_Document_Image_Min;
    private DbsField cwf_Efm_Document_Document_Retention_Ind;
    private DbsField cwf_Efm_Document_Image_Source_Id;

    private DataAccessProgramView vw_ncw_Efm_Document;
    private DbsField ncw_Efm_Document_Np_Pin;
    private DbsField ncw_Efm_Document_Rqst_Log_Dte_Tme;
    private DbsField ncw_Efm_Document_Crte_Dte_Tme;
    private DbsField ncw_Efm_Document_Crte_Sqnce_Nbr;
    private DbsField ncw_Efm_Document_Crte_Empl_Racf_Id;
    private DbsField ncw_Efm_Document_Crte_Unit;
    private DbsField ncw_Efm_Document_Crte_System;
    private DbsField ncw_Efm_Document_Crte_Jobname;

    private DbsGroup ncw_Efm_Document_Dcmnt_Type;
    private DbsField ncw_Efm_Document_Dcmnt_Ctgry;
    private DbsField ncw_Efm_Document_Dcmnt_Sub_Ctgry;
    private DbsField ncw_Efm_Document_Dcmnt_Dtl;
    private DbsField ncw_Efm_Document_Dcmnt_Dscrptn;
    private DbsField ncw_Efm_Document_Dcmnt_Drctn;
    private DbsField ncw_Efm_Document_Dcmnt_Scrty_Ind;
    private DbsField ncw_Efm_Document_Dcmnt_Rtntn_Ind;
    private DbsField ncw_Efm_Document_Dcmt_Frmt;
    private DbsField ncw_Efm_Document_Dcmnt_Anntn_Ind;
    private DbsField ncw_Efm_Document_Dcmnt_Copy_Ind;
    private DbsField ncw_Efm_Document_Invrt_Crte_Dte_Tme;

    private DbsGroup ncw_Efm_Document_Audit_Data;
    private DbsField ncw_Efm_Document_Audit_Action;
    private DbsField ncw_Efm_Document_Audit_Empl_Racf_Id;
    private DbsField ncw_Efm_Document_Audit_System;
    private DbsField ncw_Efm_Document_Audit_Unit;
    private DbsField ncw_Efm_Document_Audit_Jobname;
    private DbsField ncw_Efm_Document_Audit_Dte_Tme;
    private DbsField ncw_Efm_Document_Text_Dte_Tme;
    private DbsField ncw_Efm_Document_Image_Source_Id;
    private DbsField ncw_Efm_Document_Image_Min;
    private DbsField ncw_Efm_Document_Image_End_Page;
    private DbsField ncw_Efm_Document_Image_Ornttn;
    private DbsField ncw_Efm_Document_Kdo_Crte_Dte_Tme;
    private DbsField ncw_Efm_Document_Orgnl_Np_Pin;
    private DbsField ncw_Efm_Document_Prvte_Dcmnt_Ind;

    private DataAccessProgramView vw_cwf_Mit_History;
    private DbsField cwf_Mit_History_Admin_Unit_Cde;

    private DataAccessProgramView vw_icw_Master_Index;
    private DbsField icw_Master_Index_Actve_Ind;
    private DbsField icw_Master_Index_Ppg_Cde;
    private DbsField icw_Master_Index_Rqst_Log_Dte_Tme;
    private DbsField icw_Master_Index_Rqst_Id;

    private DbsGroup icw_Master_Index__R_Field_3;
    private DbsField icw_Master_Index_Rqst_Work_Prcss_Id;
    private DbsField icw_Master_Index_Rqst_Tiaa_Rcvd_Dte;
    private DbsField icw_Master_Index_Rqst_Case_Id_Cde;

    private DbsGroup icw_Master_Index__R_Field_4;
    private DbsField icw_Master_Index_Pnd_Case_Id_Cde;
    private DbsField icw_Master_Index_Pnd_Subrqst_Id_Cde;
    private DbsField icw_Master_Index_Work_Prcss_Id;
    private DbsField icw_Master_Index_Tiaa_Rcvd_Dte_Tme;
    private DbsField icw_Master_Index_Admin_Unit_Cde;

    private DataAccessProgramView vw_ncw_Master_Index;
    private DbsField ncw_Master_Index_Np_Pin;
    private DbsField ncw_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup ncw_Master_Index__R_Field_5;
    private DbsField ncw_Master_Index_Rqst_Log_Index_Dte;
    private DbsField ncw_Master_Index_Rqst_Log_Index_Tme;
    private DbsField ncw_Master_Index_Rqst_Log_Unit_Cde;
    private DbsField ncw_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField ncw_Master_Index_Rqst_Orgn_Cde;
    private DbsField ncw_Master_Index_Rqst_Log_Invrt_Dte_Tme;
    private DbsField ncw_Master_Index_Modify_Unit_Cde;
    private DbsField ncw_Master_Index_Modify_Oprtr_Cde;
    private DbsField ncw_Master_Index_Modify_Applctn_Cde;
    private DbsField ncw_Master_Index_System_Updte_Dte_Tme;
    private DbsField ncw_Master_Index_Work_Prcss_Id;

    private DbsGroup ncw_Master_Index__R_Field_6;
    private DbsField ncw_Master_Index_Work_Actn_Rqstd_Cde;
    private DbsField ncw_Master_Index_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField ncw_Master_Index_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField ncw_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField ncw_Master_Index_Work_Rqst_Prty_Cde;
    private DbsField ncw_Master_Index_Rt_Sqnce_Nbr;
    private DbsField ncw_Master_Index_Tiaa_Rcvd_Dte_Tme;
    private DbsField ncw_Master_Index_Case_Id_Cde;
    private DbsField ncw_Master_Index_Rqst_Id;

    private DbsGroup ncw_Master_Index__R_Field_7;
    private DbsField ncw_Master_Index_Rqst_Work_Prcss_Id;
    private DbsField ncw_Master_Index_Rqst_Tiaa_Rcvd_Dte;
    private DbsField ncw_Master_Index_Rqst_Case_Id_Cde;

    private DbsGroup ncw_Master_Index__R_Field_8;
    private DbsField ncw_Master_Index_Pnd_Case_Id_Cde;
    private DbsField ncw_Master_Index_Pnd_Subrqst_Id_Cde;
    private DbsField ncw_Master_Index_Rqst_Np_Pin;
    private DbsField ncw_Master_Index_Unit_Cde;

    private DbsGroup ncw_Master_Index__R_Field_9;
    private DbsField ncw_Master_Index_Unit_Id_Cde;
    private DbsField ncw_Master_Index_Unit_Rgn_Cde;
    private DbsField ncw_Master_Index_Unit_Spcl_Dsgntn_Cde;
    private DbsField ncw_Master_Index_Unit_Brnch_Group_Cde;
    private DbsField ncw_Master_Index_Unit_Updte_Dte_Tme;
    private DbsField ncw_Master_Index_Admin_Unit_Cde;
    private DbsField ncw_Master_Index_Admin_Status_Cde;
    private DbsField ncw_Master_Index_Admin_Status_Updte_Dte_Tme;
    private DbsField ncw_Master_Index_Admin_Status_Updte_Oprtr_Cde;
    private DbsField ncw_Master_Index_Empl_Racf_Id;
    private DbsField ncw_Master_Index_Status_Cde;
    private DbsField ncw_Master_Index_Status_Updte_Dte_Tme;
    private DbsField ncw_Master_Index_Status_Updte_Oprtr_Cde;
    private DbsField ncw_Master_Index_Status_Freeze_Ind;
    private DbsField ncw_Master_Index_Step_Id;
    private DbsField ncw_Master_Index_Step_Sqnce_Nbr;
    private DbsField ncw_Master_Index_Step_Updte_Dte_Tme;
    private DbsField ncw_Master_Index_Cmplnt_Ind;
    private DbsField ncw_Master_Index_Sub_Rqst_Ind;
    private DbsField ncw_Master_Index_Work_List_Ind;
    private DbsField ncw_Master_Index_Wpid_Vldte_Ind;
    private DbsField ncw_Master_Index_Crprte_Status_Ind;
    private DbsField ncw_Master_Index_Crprte_Due_Dte_Tme;
    private DbsField ncw_Master_Index_Crprte_Close_Dte_Tme;
    private DbsField ncw_Master_Index_Parent_Log_Dte_Tme;
    private DbsField ncw_Master_Index_Spcl_Hndlng_Txt;
    private DbsField ncw_Master_Index_Strtng_Event_Dte_Tme;
    private DbsField ncw_Master_Index_Endng_Event_Dte_Tme;
    private DbsField ncw_Master_Index_Actvty_Elpsd_Clndr_Hours;
    private DbsField ncw_Master_Index_Actvty_Elpsd_Bsnss_Hours;
    private DbsField ncw_Master_Index_Invrt_Strtng_Event;
    private DbsField ncw_Master_Index_Actve_Ind;
    private DbsField ncw_Master_Index_Multi_Rqst_Ind;
    private DbsField ncw_Master_Index_Cntct_Orgn_Type_Cde;
    private DbsField ncw_Master_Index_Cntct_Dte_Tme;
    private DbsField ncw_Master_Index_Cntct_Oprtr_Id;
    private DbsField ncw_Master_Index_Dup_Ind;
    private DbsField ncw_Master_Index_Elctrn_Fld_Ind;
    private DbsField ncw_Master_Index_Crprte_On_Tme_Ind;
    private DbsField ncw_Master_Index_Prcssng_Type_Ind;
    private DbsField ncw_Master_Index_Invrt_Tiaa_Rcvd_Dte_Tme;
    private DbsField ncw_Master_Index_Crrnt_Due_Dte_Prty_Tme;

    private DbsGroup ncw_Master_Index__R_Field_10;
    private DbsField ncw_Master_Index_Crrnt_Due_Dte;
    private DbsField ncw_Master_Index_Crrnt_Prty_Cde;
    private DbsField ncw_Master_Index_Crrnt_Due_Tme;
    private DbsField ncw_Master_Index_Cntct_Sheet_Print_Cde;
    private DbsField ncw_Master_Index_Cntct_Sheet_Print_Dte_Tme;
    private DbsField ncw_Master_Index_Cntct_Sheet_Printer_Id_Cde;
    private DbsField ncw_Master_Index_Cntct_Invrt_Dte_Tme;
    private DbsField ncw_Master_Index_Cnnctd_Rqst_Ind;
    private DbsField ncw_Master_Index_Crrnt_Cmt_Ind;
    private DbsField ncw_Master_Index_Prvte_Wrk_Rqst_Ind;
    private DbsField ncw_Master_Index_Instn_Rqst_Log_Dte_Tme;
    private DbsField ncw_Master_Index_Log_Sqnce_Nbr;
    private DbsField ncw_Master_Index_Rescan_Ind;

    private DataAccessProgramView vw_icw_Mit_History;
    private DbsField icw_Mit_History_Admin_Unit_Cde;

    private DataAccessProgramView vw_ncw_Mit_History;
    private DbsField ncw_Mit_History_Admin_Unit_Cde;

    private DataAccessProgramView vw_icw_Efm_Document;
    private DbsField icw_Efm_Document_Cabinet_Id;

    private DbsGroup icw_Efm_Document__R_Field_11;
    private DbsField icw_Efm_Document_Iis_Hrchy_Level_Cde;
    private DbsField icw_Efm_Document_Ppg_Cde;
    private DbsField icw_Efm_Document_Rqst_Log_Dte_Tme;
    private DbsField icw_Efm_Document_Crte_Dte_Tme;
    private DbsField icw_Efm_Document_Crte_Sqnce_Nbr;
    private DbsField icw_Efm_Document_Crte_Empl_Racf_Id;
    private DbsField icw_Efm_Document_Crte_Unit;
    private DbsField icw_Efm_Document_Crte_System;
    private DbsField icw_Efm_Document_Crte_Jobname;
    private DbsField icw_Efm_Document_Dcmnt_Rtntn;
    private DbsField icw_Efm_Document_Image_Source_Id;
    private DbsField icw_Efm_Document_Image_Min;

    private DbsGroup icw_Efm_Document__R_Field_12;
    private DbsField icw_Efm_Document_Image_Min_11;
    private DbsField icw_Efm_Document_Image_End_Page;
    private DbsField icw_Efm_Document_Image_Ornttn;
    private DbsField icw_Efm_Document_Orgnl_Cabinet;

    private DataAccessProgramView vw_pst_Outgoing_Mail_Item;
    private DbsField pst_Outgoing_Mail_Item_Image_Srce_Id_Id;
    private DbsField pst_Outgoing_Mail_Item_Pckge_Cde;

    private DataAccessProgramView vw_cwf_Upload_Audit;
    private DbsField cwf_Upload_Audit_Batch_Nbr;
    private DbsField cwf_Upload_Audit_Batch_Ext_Nbr;
    private DbsField cwf_Upload_Audit_Batch_Label_Txt;
    private DbsField pnd_Source_Id_Batch_Nbr_Key;

    private DbsGroup pnd_Source_Id_Batch_Nbr_Key__R_Field_13;
    private DbsField pnd_Source_Id_Batch_Nbr_Key_Pnd_Key_Source_Id;
    private DbsField pnd_Source_Id_Batch_Nbr_Key_Pnd_Key_All_Batch;

    private DbsGroup pnd_Source_Id_Batch_Nbr_Key__R_Field_14;
    private DbsField pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Nbr;
    private DbsField pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Ext_Nbr;
    private DbsField pnd_Batch_Id_Conversion_A;

    private DbsGroup pnd_Batch_Id_Conversion_A__R_Field_15;
    private DbsField pnd_Batch_Id_Conversion_A_Pnd_Batch_Id_Conversion_N;
    private DbsField pnd_Pckge_Code;
    private DbsField pnd_Program;
    private DbsField pnd_Xre_Count;
    private DbsField pnd_Doc_Count;
    private DbsField pnd_Work_Record;

    private DbsGroup pnd_Work_Record__R_Field_16;
    private DbsField pnd_Work_Record_Pnd_Source_Id;
    private DbsField pnd_Work_Record_Pnd_Mail_Item_No;
    private DbsField pnd_Work_Record_Pnd_Record_Type;

    private DbsGroup pnd_Work_Record__R_Field_17;
    private DbsField pnd_Work_Record_Pnd_Min_Key;
    private DbsField pnd_Work_Record_Pnd_Filler;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_18;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Source_Id_Min_Key_18;

    private DbsGroup pnd_Source_Id_Min_Key_18__R_Field_19;
    private DbsField pnd_Source_Id_Min_Key_18_Pnd_Source_Id_Min_Key_17;

    private DbsGroup pnd_Source_Id_Min_Key_18__R_Field_20;
    private DbsField pnd_Source_Id_Min_Key_18_Pnd_Source;
    private DbsField pnd_Source_Id_Min_Key_18_Pnd_Min_Save_A;

    private DbsGroup pnd_Source_Id_Min_Key_18__R_Field_21;
    private DbsField pnd_Source_Id_Min_Key_18_Pnd_Min_Save;
    private DbsField pnd_Source_Id_Min_Key_18_Pnd_Rec_Type;
    private DbsField pnd_Doc_Created;
    private DbsField pnd_Isn_Control;
    private DbsField pnd_Curr_Unit;
    private DbsField pnd_Init_Unit;
    private DbsField pnd_Rqst_Routing_Key;

    private DbsGroup pnd_Rqst_Routing_Key__R_Field_22;
    private DbsField pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Icw_Rqst_Routing_Key;

    private DbsGroup pnd_Icw_Rqst_Routing_Key__R_Field_23;
    private DbsField pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Rqst_Log_Dte_Tme;
    private DbsField pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Strt_Evnt_Dte_Tme;
    private DbsField pnd_Rqst_Id_Key;

    private DbsGroup pnd_Rqst_Id_Key__R_Field_24;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Id;

    private DbsGroup pnd_Rqst_Id_Key__R_Field_25;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Work_Prcss_Id;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Tiaa_Rcvd_Dte;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Case_Id;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Sub_Rqst_Cde;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Pin_Nbr;
    private DbsField pnd_Rqst_Id_Key_Pnd_Multi_Rqst_Ind;
    private DbsField pnd_Rqst_Id_Key_Pnd_Actve_Ind;
    private DbsField pnd_Document_Key_33;

    private DbsGroup pnd_Document_Key_33__R_Field_26;
    private DbsField pnd_Document_Key_33_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Document_Key_33_Pnd_Crte_Dte_Tme;
    private DbsField pnd_Document_Key_33_Pnd_Crte_Sqnce_Nbr;

    private DbsGroup pnd_Document_Key_33__R_Field_27;
    private DbsField pnd_Document_Key_33_Pnd_Cab_Id;
    private DbsField pnd_Document_Key_33_Pnd_Ti_Rcvd_Dte;
    private DbsField pnd_Document_Key_33_Pnd_Case_Multi_Subrqst;
    private DbsField pnd_Document_Key_33_Pnd_Ent_Dte_Tme;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_28;
    private DbsField cwf_Support_Tbl_Run_Indicator;
    private DbsField cwf_Support_Tbl_Filler_01;
    private DbsField cwf_Support_Tbl_Normal_Start_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_02;
    private DbsField cwf_Support_Tbl_Normal_End_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_03;
    private DbsField cwf_Support_Tbl_Normal_Record_Count;
    private DbsField cwf_Support_Tbl_Filler_04;
    private DbsField cwf_Support_Tbl_Et_On_Off_Flag;
    private DbsField cwf_Support_Tbl_Filler_05;
    private DbsField cwf_Support_Tbl_Special_Start_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_06;
    private DbsField cwf_Support_Tbl_Special_End_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_07;
    private DbsField cwf_Support_Tbl_Special_Record_Count;
    private DbsField cwf_Support_Tbl_Filler_08;
    private DbsField cwf_Support_Tbl_Calc_Start_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_09;
    private DbsField cwf_Support_Tbl_Calc_End_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Tbl_Table_Access_Level;
    private DbsField pnd_Record;
    private DbsField pnd_Env;
    private DbsField pnd_Report_Name;
    private DbsField pnd_Date_Desc;
    private DbsField pnd_Run_Date;
    private DbsField pnd_Deliver_To;
    private DbsField pnd_Unit;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Zero_Pin;
    private DbsField pnd_Zero_Pin_Cnt;
    private DbsField pnd_Zero_Pin_All_Cnt;
    private DbsField pnd_Xre_All_Count;
    private DbsField pnd_Err_Cntr;
    private DbsField pnd_Xref_Deleted;
    private DbsField pnd_Xref_Today;
    private DbsField pnd_Xref_Cirs;
    private DbsField pnd_Cwf_Deleted;
    private DbsField pnd_Cwf_Today;
    private DbsField pnd_Icw_Deleted;
    private DbsField pnd_Icw_Today;
    private DbsField pnd_Ncw_Deleted;
    private DbsField pnd_Ncw_Today;
    private DbsField pnd_Call_Cntr;
    private DbsField pnd_Prev_Source;
    private DbsField pnd_Prev_Doc_Source;
    private DbsField pnd_Doc_Entry_Dte_Tme;
    private DbsField pnd_Doc_Entry_Dte;

    private DbsGroup pnd_Doc_Entry_Dte__R_Field_29;
    private DbsField pnd_Doc_Entry_Dte_Pnd_Doc_Entry_Dte_N;
    private DbsField pnd_Upld_Entry_Dte;

    private DbsGroup pnd_Upld_Entry_Dte__R_Field_30;
    private DbsField pnd_Upld_Entry_Dte_Pnd_Upld_Entry_Dte_N;
    private DbsField pnd_Image_Source_Id;
    private DbsField pnd_Cabinet_Id;
    private DbsField pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Case_Workprcss_Multi_Subrqst;

    private DbsGroup pnd_Case_Workprcss_Multi_Subrqst__R_Field_31;
    private DbsField pnd_Case_Workprcss_Multi_Subrqst_Pnd_Case_Id;
    private DbsField pnd_Case_Workprcss_Multi_Subrqst_Pnd_Work_Prcss_Id;
    private DbsField pnd_Case_Workprcss_Multi_Subrqst_Pnd_Multi_Ind;
    private DbsField pnd_Case_Workprcss_Multi_Subrqst_Pnd_Subrqst_Ind;
    private DbsField pnd_Pin_Ppg;
    private DbsField pnd_Entry_Dte_Tme;
    private DbsField pnd_Identify_Dte_Tme;
    private DbsField pnd_Wf_Cntr;
    private DbsField pnd_Xre_Doc_Created;
    private DbsField pnd_Xre_Doc_Cwf;
    private DbsField pnd_Xre_Doc_Icw;
    private DbsField pnd_Xre_Doc_Ncw;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Call_Nat;
    private DbsField pnd_Entry_Date_Time;

    private DbsGroup pnd_Entry_Date_Time__R_Field_32;
    private DbsField pnd_Entry_Date_Time_Pnd_Entry_Date;
    private DbsField pnd_Entry_Date_Time_Pnd_Entry_Time;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaEfsa6110 = new PdaEfsa6110(localVariables);

        // Local Variables

        vw_cwf_Image_Xref = new DataAccessProgramView(new NameInfo("vw_cwf_Image_Xref", "CWF-IMAGE-XREF"), "CWF_IMAGE_XREF", "CWF_IMAGE_XREF");
        cwf_Image_Xref_Batch_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Batch_Id", "BATCH-ID", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "BATCH_ID");
        cwf_Image_Xref_Mail_Item_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Mail_Item_Nbr", "MAIL-ITEM-NBR", FieldType.NUMERIC, 
            11, RepeatingFieldStrategy.None, "MAIL_ITEM_NBR");
        cwf_Image_Xref_Mail_Item_Nbr.setDdmHeader("MAIL ITEM/NUMBER");
        cwf_Image_Xref_Image_Document_Address_Cde = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Document_Address_Cde", "IMAGE-DOCUMENT-ADDRESS-CDE", 
            FieldType.STRING, 22, RepeatingFieldStrategy.None, "IMAGE_DOCUMENT_ADDRESS_CDE");
        cwf_Image_Xref_Image_Document_Address_Cde.setDdmHeader("IMNET DOC/ADDRESS");
        cwf_Image_Xref_Ph_Unique_Id_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PH_UNIQUE_ID_NBR");
        cwf_Image_Xref_Ph_Unique_Id_Nbr.setDdmHeader("PIN");
        cwf_Image_Xref_Image_Quality_Ind = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Quality_Ind", "IMAGE-QUALITY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_QUALITY_IND");
        cwf_Image_Xref_Image_Quality_Ind.setDdmHeader("POOR/QLTY");
        cwf_Image_Xref_Upld_Date_Time = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Upld_Date_Time", "UPLD-DATE-TIME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPLD_DATE_TIME");
        cwf_Image_Xref_Upld_Date_Time.setDdmHeader("UPLOAD/DATE TIME");
        cwf_Image_Xref_Number_Of_Pages = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Number_Of_Pages", "NUMBER-OF-PAGES", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "NUMBER_OF_PAGES");
        cwf_Image_Xref_Number_Of_Pages.setDdmHeader("NBR OF/PAGES");
        cwf_Image_Xref_Source_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Source_Id", "SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SOURCE_ID");
        cwf_Image_Xref_Image_Delete_Ind = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Delete_Ind", "IMAGE-DELETE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_DELETE_IND");
        cwf_Image_Xref_Cabinet_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Cabinet_Id", "CABINET-ID", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Image_Xref_Archive_Dte_Tme = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Archive_Dte_Tme", "ARCHIVE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ARCHIVE_DTE_TME");
        cwf_Image_Xref_Identify_Dte_Tme = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Identify_Dte_Tme", "IDENTIFY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "IDENTIFY_DTE_TME");
        registerRecord(vw_cwf_Image_Xref);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Actve_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");
        cwf_Master_Index_View_Pin_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Master_Index_View_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Index_View_Rqst_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Id", "RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Master_Index_View_Rqst_Id.setDdmHeader("REQUEST ID");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        registerRecord(vw_cwf_Master_Index_View);

        vw_cwf_Efm_Document = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Document", "CWF-EFM-DOCUMENT"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Efm_Document_Cabinet_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Document_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Document__R_Field_1 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_1", "REDEFINE", cwf_Efm_Document_Cabinet_Id);
        cwf_Efm_Document_Pin_Prefix = cwf_Efm_Document__R_Field_1.newFieldInGroup("cwf_Efm_Document_Pin_Prefix", "PIN-PREFIX", FieldType.STRING, 1);
        cwf_Efm_Document_Pin_Nbr = cwf_Efm_Document__R_Field_1.newFieldInGroup("cwf_Efm_Document_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);

        cwf_Efm_Document_Folder_Id = vw_cwf_Efm_Document.getRecord().newGroupInGroup("CWF_EFM_DOCUMENT_FOLDER_ID", "FOLDER-ID");
        cwf_Efm_Document_Folder_Id.setDdmHeader("FOLDER/ID");
        cwf_Efm_Document_Tiaa_Rcvd_Dte = cwf_Efm_Document_Folder_Id.newFieldInGroup("cwf_Efm_Document_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Efm_Document_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Efm_Document_Case_Workprcss_Multi_Subrqst = cwf_Efm_Document_Folder_Id.newFieldInGroup("cwf_Efm_Document_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Efm_Document_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Efm_Document__R_Field_2 = cwf_Efm_Document_Folder_Id.newGroupInGroup("cwf_Efm_Document__R_Field_2", "REDEFINE", cwf_Efm_Document_Case_Workprcss_Multi_Subrqst);
        cwf_Efm_Document_Case_Ind = cwf_Efm_Document__R_Field_2.newFieldInGroup("cwf_Efm_Document_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        cwf_Efm_Document_Originating_Work_Prcss_Id = cwf_Efm_Document__R_Field_2.newFieldInGroup("cwf_Efm_Document_Originating_Work_Prcss_Id", "ORIGINATING-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Efm_Document_Multi_Rqst_Ind = cwf_Efm_Document__R_Field_2.newFieldInGroup("cwf_Efm_Document_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 
            1);
        cwf_Efm_Document_Sub_Rqst_Ind = cwf_Efm_Document__R_Field_2.newFieldInGroup("cwf_Efm_Document_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 
            1);
        cwf_Efm_Document_Entry_Dte_Tme = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Efm_Document_Entry_Dte_Tme.setDdmHeader("ENTRY/DATE AND TIME");
        cwf_Efm_Document_Image_Min = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        cwf_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");
        cwf_Efm_Document_Document_Retention_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Document_Retention_Ind", "DOCUMENT-RETENTION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_RETENTION_IND");
        cwf_Efm_Document_Document_Retention_Ind.setDdmHeader("DOC/RETNTN");
        cwf_Efm_Document_Image_Source_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        cwf_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        registerRecord(vw_cwf_Efm_Document);

        vw_ncw_Efm_Document = new DataAccessProgramView(new NameInfo("vw_ncw_Efm_Document", "NCW-EFM-DOCUMENT"), "NCW_EFM_DOCUMENT", "NCW_EFM_DOCUMENT");
        ncw_Efm_Document_Np_Pin = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Efm_Document_Rqst_Log_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        ncw_Efm_Document_Rqst_Log_Dte_Tme.setDdmHeader("MIT RQST/DATE AND TIME");
        ncw_Efm_Document_Crte_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Dte_Tme", "CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRTE_DTE_TME");
        ncw_Efm_Document_Crte_Dte_Tme.setDdmHeader("CREATE/DATE AND TIME");
        ncw_Efm_Document_Crte_Sqnce_Nbr = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Sqnce_Nbr", "CRTE-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CRTE_SQNCE_NBR");
        ncw_Efm_Document_Crte_Empl_Racf_Id = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Empl_Racf_Id", "CRTE-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CRTE_EMPL_RACF_ID");
        ncw_Efm_Document_Crte_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        ncw_Efm_Document_Crte_Unit = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Unit", "CRTE-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CRTE_UNIT");
        ncw_Efm_Document_Crte_Unit.setDdmHeader("CREATED BY/UNIT");
        ncw_Efm_Document_Crte_System = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_System", "CRTE-SYSTEM", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_SYSTEM");
        ncw_Efm_Document_Crte_System.setDdmHeader("CREATED BY/SYSTEM");
        ncw_Efm_Document_Crte_Jobname = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Jobname", "CRTE-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_JOBNAME");
        ncw_Efm_Document_Crte_Jobname.setDdmHeader("CREATED BY/JOBNAME");

        ncw_Efm_Document_Dcmnt_Type = vw_ncw_Efm_Document.getRecord().newGroupInGroup("NCW_EFM_DOCUMENT_DCMNT_TYPE", "DCMNT-TYPE");
        ncw_Efm_Document_Dcmnt_Type.setDdmHeader("DOCUMENT/TYPE");
        ncw_Efm_Document_Dcmnt_Ctgry = ncw_Efm_Document_Dcmnt_Type.newFieldInGroup("ncw_Efm_Document_Dcmnt_Ctgry", "DCMNT-CTGRY", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "DCMNT_CTGRY");
        ncw_Efm_Document_Dcmnt_Ctgry.setDdmHeader("DOC/CAT");
        ncw_Efm_Document_Dcmnt_Sub_Ctgry = ncw_Efm_Document_Dcmnt_Type.newFieldInGroup("ncw_Efm_Document_Dcmnt_Sub_Ctgry", "DCMNT-SUB-CTGRY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DCMNT_SUB_CTGRY");
        ncw_Efm_Document_Dcmnt_Sub_Ctgry.setDdmHeader("DOC SUB-/CATEGORY");
        ncw_Efm_Document_Dcmnt_Dtl = ncw_Efm_Document_Dcmnt_Type.newFieldInGroup("ncw_Efm_Document_Dcmnt_Dtl", "DCMNT-DTL", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DCMNT_DTL");
        ncw_Efm_Document_Dcmnt_Dtl.setDdmHeader("DOCUMENT/DETAIL");
        ncw_Efm_Document_Dcmnt_Dscrptn = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Dscrptn", "DCMNT-DSCRPTN", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "DCMNT_DSCRPTN");
        ncw_Efm_Document_Dcmnt_Drctn = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Drctn", "DCMNT-DRCTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_DRCTN");
        ncw_Efm_Document_Dcmnt_Drctn.setDdmHeader("DOC/DIR");
        ncw_Efm_Document_Dcmnt_Scrty_Ind = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Scrty_Ind", "DCMNT-SCRTY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_SCRTY_IND");
        ncw_Efm_Document_Dcmnt_Scrty_Ind.setDdmHeader("SECRD/DOC");
        ncw_Efm_Document_Dcmnt_Rtntn_Ind = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Rtntn_Ind", "DCMNT-RTNTN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_RTNTN_IND");
        ncw_Efm_Document_Dcmnt_Rtntn_Ind.setDdmHeader("DOC/RETNTN");
        ncw_Efm_Document_Dcmt_Frmt = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmt_Frmt", "DCMT-FRMT", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DCMT_FRMT");
        ncw_Efm_Document_Dcmnt_Anntn_Ind = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Anntn_Ind", "DCMNT-ANNTN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_ANNTN_IND");
        ncw_Efm_Document_Dcmnt_Anntn_Ind.setDdmHeader("ANOTATION/IND");
        ncw_Efm_Document_Dcmnt_Copy_Ind = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Copy_Ind", "DCMNT-COPY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_COPY_IND");
        ncw_Efm_Document_Dcmnt_Copy_Ind.setDdmHeader("COPY/IND");
        ncw_Efm_Document_Invrt_Crte_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Invrt_Crte_Dte_Tme", "INVRT-CRTE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "INVRT_CRTE_DTE_TME");
        ncw_Efm_Document_Invrt_Crte_Dte_Tme.setDdmHeader("INVERTED CREATE/DATE AND TIME");

        ncw_Efm_Document_Audit_Data = vw_ncw_Efm_Document.getRecord().newGroupInGroup("NCW_EFM_DOCUMENT_AUDIT_DATA", "AUDIT-DATA");
        ncw_Efm_Document_Audit_Action = ncw_Efm_Document_Audit_Data.newFieldInGroup("ncw_Efm_Document_Audit_Action", "AUDIT-ACTION", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AUDIT_ACTION");
        ncw_Efm_Document_Audit_Action.setDdmHeader("ACTION");
        ncw_Efm_Document_Audit_Empl_Racf_Id = ncw_Efm_Document_Audit_Data.newFieldInGroup("ncw_Efm_Document_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AUDIT_EMPL_RACF_ID");
        ncw_Efm_Document_Audit_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF ID");
        ncw_Efm_Document_Audit_System = ncw_Efm_Document_Audit_Data.newFieldInGroup("ncw_Efm_Document_Audit_System", "AUDIT-SYSTEM", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_SYSTEM");
        ncw_Efm_Document_Audit_System.setDdmHeader("AUDIT/SYSTEM");
        ncw_Efm_Document_Audit_Unit = ncw_Efm_Document_Audit_Data.newFieldInGroup("ncw_Efm_Document_Audit_Unit", "AUDIT-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AUDIT_UNIT");
        ncw_Efm_Document_Audit_Unit.setDdmHeader("AUDIT/UNIT");
        ncw_Efm_Document_Audit_Jobname = ncw_Efm_Document_Audit_Data.newFieldInGroup("ncw_Efm_Document_Audit_Jobname", "AUDIT-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_JOBNAME");
        ncw_Efm_Document_Audit_Jobname.setDdmHeader("JOBNAME");
        ncw_Efm_Document_Audit_Dte_Tme = ncw_Efm_Document_Audit_Data.newFieldInGroup("ncw_Efm_Document_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "AUDIT_DTE_TME");
        ncw_Efm_Document_Audit_Dte_Tme.setDdmHeader("DATE/& TIME");
        ncw_Efm_Document_Text_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Text_Dte_Tme", "TEXT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TEXT_DTE_TME");
        ncw_Efm_Document_Text_Dte_Tme.setDdmHeader("TEXT/DATE AND TIME");
        ncw_Efm_Document_Image_Source_Id = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        ncw_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        ncw_Efm_Document_Image_Min = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        ncw_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");
        ncw_Efm_Document_Image_End_Page = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_End_Page", "IMAGE-END-PAGE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "IMAGE_END_PAGE");
        ncw_Efm_Document_Image_End_Page.setDdmHeader("IMAGE/END PAGE");
        ncw_Efm_Document_Image_Ornttn = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_Ornttn", "IMAGE-ORNTTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_ORNTTN");
        ncw_Efm_Document_Image_Ornttn.setDdmHeader("DOC/ORIENT");
        ncw_Efm_Document_Kdo_Crte_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Kdo_Crte_Dte_Tme", "KDO-CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "KDO_CRTE_DTE_TME");
        ncw_Efm_Document_Kdo_Crte_Dte_Tme.setDdmHeader("K D O/DATE AND TIME");
        ncw_Efm_Document_Orgnl_Np_Pin = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Orgnl_Np_Pin", "ORGNL-NP-PIN", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "ORGNL_NP_PIN");
        ncw_Efm_Document_Prvte_Dcmnt_Ind = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Prvte_Dcmnt_Ind", "PRVTE-DCMNT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRVTE_DCMNT_IND");
        registerRecord(vw_ncw_Efm_Document);

        vw_cwf_Mit_History = new DataAccessProgramView(new NameInfo("vw_cwf_Mit_History", "CWF-MIT-HISTORY"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Mit_History_Admin_Unit_Cde = vw_cwf_Mit_History.getRecord().newFieldInGroup("cwf_Mit_History_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        registerRecord(vw_cwf_Mit_History);

        vw_icw_Master_Index = new DataAccessProgramView(new NameInfo("vw_icw_Master_Index", "ICW-MASTER-INDEX"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        icw_Master_Index_Actve_Ind = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        icw_Master_Index_Ppg_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PPG_CDE");
        icw_Master_Index_Rqst_Log_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        icw_Master_Index_Rqst_Id = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Id", "RQST-ID", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "RQST_ID");
        icw_Master_Index_Rqst_Id.setDdmHeader("REQUEST ID");

        icw_Master_Index__R_Field_3 = vw_icw_Master_Index.getRecord().newGroupInGroup("icw_Master_Index__R_Field_3", "REDEFINE", icw_Master_Index_Rqst_Id);
        icw_Master_Index_Rqst_Work_Prcss_Id = icw_Master_Index__R_Field_3.newFieldInGroup("icw_Master_Index_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        icw_Master_Index_Rqst_Tiaa_Rcvd_Dte = icw_Master_Index__R_Field_3.newFieldInGroup("icw_Master_Index_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        icw_Master_Index_Rqst_Case_Id_Cde = icw_Master_Index__R_Field_3.newFieldInGroup("icw_Master_Index_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", FieldType.STRING, 
            2);

        icw_Master_Index__R_Field_4 = icw_Master_Index__R_Field_3.newGroupInGroup("icw_Master_Index__R_Field_4", "REDEFINE", icw_Master_Index_Rqst_Case_Id_Cde);
        icw_Master_Index_Pnd_Case_Id_Cde = icw_Master_Index__R_Field_4.newFieldInGroup("icw_Master_Index_Pnd_Case_Id_Cde", "#CASE-ID-CDE", FieldType.STRING, 
            1);
        icw_Master_Index_Pnd_Subrqst_Id_Cde = icw_Master_Index__R_Field_4.newFieldInGroup("icw_Master_Index_Pnd_Subrqst_Id_Cde", "#SUBRQST-ID-CDE", FieldType.STRING, 
            1);
        icw_Master_Index_Work_Prcss_Id = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        icw_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        icw_Master_Index_Tiaa_Rcvd_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        icw_Master_Index_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        icw_Master_Index_Admin_Unit_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        registerRecord(vw_icw_Master_Index);

        vw_ncw_Master_Index = new DataAccessProgramView(new NameInfo("vw_ncw_Master_Index", "NCW-MASTER-INDEX"), "NCW_MASTER_INDEX", "NCW_MASTER_INDEX");
        ncw_Master_Index_Np_Pin = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Master_Index_Rqst_Log_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");

        ncw_Master_Index__R_Field_5 = vw_ncw_Master_Index.getRecord().newGroupInGroup("ncw_Master_Index__R_Field_5", "REDEFINE", ncw_Master_Index_Rqst_Log_Dte_Tme);
        ncw_Master_Index_Rqst_Log_Index_Dte = ncw_Master_Index__R_Field_5.newFieldInGroup("ncw_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        ncw_Master_Index_Rqst_Log_Index_Tme = ncw_Master_Index__R_Field_5.newFieldInGroup("ncw_Master_Index_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        ncw_Master_Index_Rqst_Log_Unit_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rqst_Log_Unit_Cde", "RQST-LOG-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_UNIT_CDE");
        ncw_Master_Index_Rqst_Log_Unit_Cde.setDdmHeader("LOG/UNIT");
        ncw_Master_Index_Rqst_Log_Oprtr_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        ncw_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        ncw_Master_Index_Rqst_Orgn_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        ncw_Master_Index_Rqst_Log_Invrt_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rqst_Log_Invrt_Dte_Tme", "RQST-LOG-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        ncw_Master_Index_Modify_Unit_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Modify_Unit_Cde", "MODIFY-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "MODIFY_UNIT_CDE");
        ncw_Master_Index_Modify_Oprtr_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Modify_Oprtr_Cde", "MODIFY-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "MODIFY_OPRTR_CDE");
        ncw_Master_Index_Modify_Applctn_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Modify_Applctn_Cde", "MODIFY-APPLCTN-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MODIFY_APPLCTN_CDE");
        ncw_Master_Index_System_Updte_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_System_Updte_Dte_Tme", "SYSTEM-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "SYSTEM_UPDTE_DTE_TME");
        ncw_Master_Index_Work_Prcss_Id = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        ncw_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");

        ncw_Master_Index__R_Field_6 = vw_ncw_Master_Index.getRecord().newGroupInGroup("ncw_Master_Index__R_Field_6", "REDEFINE", ncw_Master_Index_Work_Prcss_Id);
        ncw_Master_Index_Work_Actn_Rqstd_Cde = ncw_Master_Index__R_Field_6.newFieldInGroup("ncw_Master_Index_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        ncw_Master_Index_Work_Lob_Cmpny_Prdct_Cde = ncw_Master_Index__R_Field_6.newFieldInGroup("ncw_Master_Index_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        ncw_Master_Index_Work_Mjr_Bsnss_Prcss_Cde = ncw_Master_Index__R_Field_6.newFieldInGroup("ncw_Master_Index_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        ncw_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde = ncw_Master_Index__R_Field_6.newFieldInGroup("ncw_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        ncw_Master_Index_Work_Rqst_Prty_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Work_Rqst_Prty_Cde", "WORK-RQST-PRTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WORK_RQST_PRTY_CDE");
        ncw_Master_Index_Work_Rqst_Prty_Cde.setDdmHeader("PRIO");
        ncw_Master_Index_Rt_Sqnce_Nbr = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        ncw_Master_Index_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        ncw_Master_Index_Tiaa_Rcvd_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        ncw_Master_Index_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        ncw_Master_Index_Case_Id_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CASE_ID_CDE");
        ncw_Master_Index_Case_Id_Cde.setDdmHeader("CASE/ID");
        ncw_Master_Index_Rqst_Id = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rqst_Id", "RQST-ID", FieldType.STRING, 23, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ncw_Master_Index_Rqst_Id.setDdmHeader("REQUEST ID");

        ncw_Master_Index__R_Field_7 = vw_ncw_Master_Index.getRecord().newGroupInGroup("ncw_Master_Index__R_Field_7", "REDEFINE", ncw_Master_Index_Rqst_Id);
        ncw_Master_Index_Rqst_Work_Prcss_Id = ncw_Master_Index__R_Field_7.newFieldInGroup("ncw_Master_Index_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        ncw_Master_Index_Rqst_Tiaa_Rcvd_Dte = ncw_Master_Index__R_Field_7.newFieldInGroup("ncw_Master_Index_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        ncw_Master_Index_Rqst_Case_Id_Cde = ncw_Master_Index__R_Field_7.newFieldInGroup("ncw_Master_Index_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", FieldType.STRING, 
            2);

        ncw_Master_Index__R_Field_8 = ncw_Master_Index__R_Field_7.newGroupInGroup("ncw_Master_Index__R_Field_8", "REDEFINE", ncw_Master_Index_Rqst_Case_Id_Cde);
        ncw_Master_Index_Pnd_Case_Id_Cde = ncw_Master_Index__R_Field_8.newFieldInGroup("ncw_Master_Index_Pnd_Case_Id_Cde", "#CASE-ID-CDE", FieldType.STRING, 
            1);
        ncw_Master_Index_Pnd_Subrqst_Id_Cde = ncw_Master_Index__R_Field_8.newFieldInGroup("ncw_Master_Index_Pnd_Subrqst_Id_Cde", "#SUBRQST-ID-CDE", FieldType.STRING, 
            1);
        ncw_Master_Index_Rqst_Np_Pin = ncw_Master_Index__R_Field_7.newFieldInGroup("ncw_Master_Index_Rqst_Np_Pin", "RQST-NP-PIN", FieldType.STRING, 7);
        ncw_Master_Index_Unit_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        ncw_Master_Index_Unit_Cde.setDdmHeader("UNIT/CODE");

        ncw_Master_Index__R_Field_9 = vw_ncw_Master_Index.getRecord().newGroupInGroup("ncw_Master_Index__R_Field_9", "REDEFINE", ncw_Master_Index_Unit_Cde);
        ncw_Master_Index_Unit_Id_Cde = ncw_Master_Index__R_Field_9.newFieldInGroup("ncw_Master_Index_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        ncw_Master_Index_Unit_Rgn_Cde = ncw_Master_Index__R_Field_9.newFieldInGroup("ncw_Master_Index_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 
            1);
        ncw_Master_Index_Unit_Spcl_Dsgntn_Cde = ncw_Master_Index__R_Field_9.newFieldInGroup("ncw_Master_Index_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        ncw_Master_Index_Unit_Brnch_Group_Cde = ncw_Master_Index__R_Field_9.newFieldInGroup("ncw_Master_Index_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        ncw_Master_Index_Unit_Updte_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        ncw_Master_Index_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        ncw_Master_Index_Admin_Unit_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        ncw_Master_Index_Admin_Status_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        ncw_Master_Index_Admin_Status_Updte_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        ncw_Master_Index_Admin_Status_Updte_Oprtr_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        ncw_Master_Index_Empl_Racf_Id = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        ncw_Master_Index_Status_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "STATUS_CDE");
        ncw_Master_Index_Status_Updte_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        ncw_Master_Index_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        ncw_Master_Index_Status_Updte_Oprtr_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        ncw_Master_Index_Status_Freeze_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        ncw_Master_Index_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        ncw_Master_Index_Step_Id = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        ncw_Master_Index_Step_Id.setDdmHeader("STEP/ID");
        ncw_Master_Index_Step_Sqnce_Nbr = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Step_Sqnce_Nbr", "STEP-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "STEP_SQNCE_NBR");
        ncw_Master_Index_Step_Updte_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Step_Updte_Dte_Tme", "STEP-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STEP_UPDTE_DTE_TME");
        ncw_Master_Index_Step_Updte_Dte_Tme.setDdmHeader("STEP UPDATE/DATE-TIME");
        ncw_Master_Index_Cmplnt_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CMPLNT_IND");
        ncw_Master_Index_Sub_Rqst_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SUB_RQST_IND");
        ncw_Master_Index_Sub_Rqst_Ind.setDdmHeader("SUB-REQUEST COUNT");
        ncw_Master_Index_Work_List_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "WORK_LIST_IND");
        ncw_Master_Index_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        ncw_Master_Index_Wpid_Vldte_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Wpid_Vldte_Ind", "WPID-VLDTE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "WPID_VLDTE_IND");
        ncw_Master_Index_Crprte_Status_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        ncw_Master_Index_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        ncw_Master_Index_Crprte_Due_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CRPRTE_DUE_DTE_TME");
        ncw_Master_Index_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        ncw_Master_Index_Crprte_Close_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Crprte_Close_Dte_Tme", "CRPRTE-CLOSE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CRPRTE_CLOSE_DTE_TME");
        ncw_Master_Index_Parent_Log_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Parent_Log_Dte_Tme", "PARENT-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "PARENT_LOG_DTE_TME");
        ncw_Master_Index_Parent_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        ncw_Master_Index_Spcl_Hndlng_Txt = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", FieldType.STRING, 
            72, RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        ncw_Master_Index_Spcl_Hndlng_Txt.setDdmHeader("MESSAGES");
        ncw_Master_Index_Strtng_Event_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Strtng_Event_Dte_Tme", "STRTNG-EVENT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STRTNG_EVENT_DTE_TME");
        ncw_Master_Index_Endng_Event_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Endng_Event_Dte_Tme", "ENDNG-EVENT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ENDNG_EVENT_DTE_TME");
        ncw_Master_Index_Actvty_Elpsd_Clndr_Hours = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Actvty_Elpsd_Clndr_Hours", "ACTVTY-ELPSD-CLNDR-HOURS", 
            FieldType.NUMERIC, 6, 2, RepeatingFieldStrategy.None, "ACTVTY_ELPSD_CLNDR_HOURS");
        ncw_Master_Index_Actvty_Elpsd_Bsnss_Hours = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Actvty_Elpsd_Bsnss_Hours", "ACTVTY-ELPSD-BSNSS-HOURS", 
            FieldType.NUMERIC, 6, 2, RepeatingFieldStrategy.None, "ACTVTY_ELPSD_BSNSS_HOURS");
        ncw_Master_Index_Invrt_Strtng_Event = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Invrt_Strtng_Event", "INVRT-STRTNG-EVENT", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "INVRT_STRTNG_EVENT");
        ncw_Master_Index_Actve_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        ncw_Master_Index_Multi_Rqst_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        ncw_Master_Index_Cntct_Orgn_Type_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Cntct_Orgn_Type_Cde", "CNTCT-ORGN-TYPE-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTCT_ORGN_TYPE_CDE");
        ncw_Master_Index_Cntct_Orgn_Type_Cde.setDdmHeader("CONTACT/MOC");
        ncw_Master_Index_Cntct_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Cntct_Dte_Tme", "CNTCT-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "CNTCT_DTE_TME");
        ncw_Master_Index_Cntct_Dte_Tme.setDdmHeader("CONTACT/DATE/TIME");
        ncw_Master_Index_Cntct_Oprtr_Id = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Cntct_Oprtr_Id", "CNTCT-OPRTR-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNTCT_OPRTR_ID");
        ncw_Master_Index_Cntct_Oprtr_Id.setDdmHeader("CONTACT/OPRTR ID");
        ncw_Master_Index_Dup_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Dup_Ind", "DUP-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DUP_IND");
        ncw_Master_Index_Dup_Ind.setDdmHeader("DUPL/IND");
        ncw_Master_Index_Elctrn_Fld_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Elctrn_Fld_Ind", "ELCTRN-FLD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ELCTRN_FLD_IND");
        ncw_Master_Index_Elctrn_Fld_Ind.setDdmHeader("ELECTRNC/FLDR IND");
        ncw_Master_Index_Crprte_On_Tme_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_ON_TME_IND");
        ncw_Master_Index_Crprte_On_Tme_Ind.setDdmHeader("CRPRTE/ON TIME");
        ncw_Master_Index_Prcssng_Type_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Prcssng_Type_Ind", "PRCSSNG-TYPE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRCSSNG_TYPE_IND");
        ncw_Master_Index_Prcssng_Type_Ind.setDdmHeader("PROC/TYPE");
        ncw_Master_Index_Invrt_Tiaa_Rcvd_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Invrt_Tiaa_Rcvd_Dte_Tme", "INVRT-TIAA-RCVD-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "INVRT_TIAA_RCVD_DTE_TME");
        ncw_Master_Index_Invrt_Tiaa_Rcvd_Dte_Tme.setDdmHeader("INVERT TIAA/RECVD DATE");
        ncw_Master_Index_Crrnt_Due_Dte_Prty_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Crrnt_Due_Dte_Prty_Tme", "CRRNT-DUE-DTE-PRTY-TME", 
            FieldType.STRING, 16, RepeatingFieldStrategy.None, "CRRNT_DUE_DTE_PRTY_TME");

        ncw_Master_Index__R_Field_10 = vw_ncw_Master_Index.getRecord().newGroupInGroup("ncw_Master_Index__R_Field_10", "REDEFINE", ncw_Master_Index_Crrnt_Due_Dte_Prty_Tme);
        ncw_Master_Index_Crrnt_Due_Dte = ncw_Master_Index__R_Field_10.newFieldInGroup("ncw_Master_Index_Crrnt_Due_Dte", "CRRNT-DUE-DTE", FieldType.STRING, 
            8);
        ncw_Master_Index_Crrnt_Prty_Cde = ncw_Master_Index__R_Field_10.newFieldInGroup("ncw_Master_Index_Crrnt_Prty_Cde", "CRRNT-PRTY-CDE", FieldType.STRING, 
            1);
        ncw_Master_Index_Crrnt_Due_Tme = ncw_Master_Index__R_Field_10.newFieldInGroup("ncw_Master_Index_Crrnt_Due_Tme", "CRRNT-DUE-TME", FieldType.STRING, 
            7);
        ncw_Master_Index_Cntct_Sheet_Print_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Cntct_Sheet_Print_Cde", "CNTCT-SHEET-PRINT-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_CDE");
        ncw_Master_Index_Cntct_Sheet_Print_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Cntct_Sheet_Print_Dte_Tme", "CNTCT-SHEET-PRINT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_DTE_TME");
        ncw_Master_Index_Cntct_Sheet_Printer_Id_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Cntct_Sheet_Printer_Id_Cde", "CNTCT-SHEET-PRINTER-ID-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINTER_ID_CDE");
        ncw_Master_Index_Cntct_Invrt_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Cntct_Invrt_Dte_Tme", "CNTCT-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "CNTCT_INVRT_DTE_TME");
        ncw_Master_Index_Cnnctd_Rqst_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Cnnctd_Rqst_Ind", "CNNCTD-RQST-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNNCTD_RQST_IND");
        ncw_Master_Index_Cnnctd_Rqst_Ind.setDdmHeader("CONNECTED REQUEST INDICATOR");
        ncw_Master_Index_Crrnt_Cmt_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Crrnt_Cmt_Ind", "CRRNT-CMT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRRNT_CMT_IND");
        ncw_Master_Index_Crrnt_Cmt_Ind.setDdmHeader("COMMITMENT INDICATOR");
        ncw_Master_Index_Prvte_Wrk_Rqst_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Prvte_Wrk_Rqst_Ind", "PRVTE-WRK-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRVTE_WRK_RQST_IND");
        ncw_Master_Index_Prvte_Wrk_Rqst_Ind.setDdmHeader("PRIVATE WORK REQUEST INDICATOR");
        ncw_Master_Index_Instn_Rqst_Log_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Instn_Rqst_Log_Dte_Tme", "INSTN-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "INSTN_RQST_LOG_DTE_TME");
        ncw_Master_Index_Instn_Rqst_Log_Dte_Tme.setDdmHeader("GUARDIAN INST RQST LOG DTE/TME");
        ncw_Master_Index_Log_Sqnce_Nbr = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Log_Sqnce_Nbr", "LOG-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "LOG_SQNCE_NBR");
        ncw_Master_Index_Log_Sqnce_Nbr.setDdmHeader("LOG SEQUENCE NUMBER");
        ncw_Master_Index_Rescan_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RESCAN_IND");
        registerRecord(vw_ncw_Master_Index);

        vw_icw_Mit_History = new DataAccessProgramView(new NameInfo("vw_icw_Mit_History", "ICW-MIT-HISTORY"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        icw_Mit_History_Admin_Unit_Cde = vw_icw_Mit_History.getRecord().newFieldInGroup("icw_Mit_History_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        registerRecord(vw_icw_Mit_History);

        vw_ncw_Mit_History = new DataAccessProgramView(new NameInfo("vw_ncw_Mit_History", "NCW-MIT-HISTORY"), "NCW_MASTER_INDEX", "NCW_MASTER_INDEX");
        ncw_Mit_History_Admin_Unit_Cde = vw_ncw_Mit_History.getRecord().newFieldInGroup("ncw_Mit_History_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        registerRecord(vw_ncw_Mit_History);

        vw_icw_Efm_Document = new DataAccessProgramView(new NameInfo("vw_icw_Efm_Document", "ICW-EFM-DOCUMENT"), "ICW_EFM_DOCUMENT", "ICW_EFM_DOCUMENT");
        icw_Efm_Document_Cabinet_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Cabinet_Id", "CABINET-ID", FieldType.STRING, 12, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        icw_Efm_Document_Cabinet_Id.setDdmHeader("CABINET/ID");

        icw_Efm_Document__R_Field_11 = vw_icw_Efm_Document.getRecord().newGroupInGroup("icw_Efm_Document__R_Field_11", "REDEFINE", icw_Efm_Document_Cabinet_Id);
        icw_Efm_Document_Iis_Hrchy_Level_Cde = icw_Efm_Document__R_Field_11.newFieldInGroup("icw_Efm_Document_Iis_Hrchy_Level_Cde", "IIS-HRCHY-LEVEL-CDE", 
            FieldType.NUMERIC, 6);
        icw_Efm_Document_Ppg_Cde = icw_Efm_Document__R_Field_11.newFieldInGroup("icw_Efm_Document_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6);
        icw_Efm_Document_Rqst_Log_Dte_Tme = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        icw_Efm_Document_Rqst_Log_Dte_Tme.setDdmHeader("MIT RQST/DATE AND TIME");
        icw_Efm_Document_Crte_Dte_Tme = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Dte_Tme", "CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRTE_DTE_TME");
        icw_Efm_Document_Crte_Dte_Tme.setDdmHeader("CREATE/DATE AND TIME");
        icw_Efm_Document_Crte_Sqnce_Nbr = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Sqnce_Nbr", "CRTE-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CRTE_SQNCE_NBR");
        icw_Efm_Document_Crte_Empl_Racf_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Empl_Racf_Id", "CRTE-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CRTE_EMPL_RACF_ID");
        icw_Efm_Document_Crte_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        icw_Efm_Document_Crte_Unit = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Unit", "CRTE-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CRTE_UNIT");
        icw_Efm_Document_Crte_Unit.setDdmHeader("CREATED BY/UNIT");
        icw_Efm_Document_Crte_System = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_System", "CRTE-SYSTEM", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_SYSTEM");
        icw_Efm_Document_Crte_System.setDdmHeader("CREATED BY/SYSTEM");
        icw_Efm_Document_Crte_Jobname = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Jobname", "CRTE-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_JOBNAME");
        icw_Efm_Document_Crte_Jobname.setDdmHeader("CREATED BY/JOBNAME");
        icw_Efm_Document_Dcmnt_Rtntn = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Dcmnt_Rtntn", "DCMNT-RTNTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_RTNTN");
        icw_Efm_Document_Dcmnt_Rtntn.setDdmHeader("DOC/RETNTN");
        icw_Efm_Document_Image_Source_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        icw_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        icw_Efm_Document_Image_Min = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        icw_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");

        icw_Efm_Document__R_Field_12 = vw_icw_Efm_Document.getRecord().newGroupInGroup("icw_Efm_Document__R_Field_12", "REDEFINE", icw_Efm_Document_Image_Min);
        icw_Efm_Document_Image_Min_11 = icw_Efm_Document__R_Field_12.newFieldInGroup("icw_Efm_Document_Image_Min_11", "IMAGE-MIN-11", FieldType.STRING, 
            11);
        icw_Efm_Document_Image_End_Page = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_End_Page", "IMAGE-END-PAGE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "IMAGE_END_PAGE");
        icw_Efm_Document_Image_End_Page.setDdmHeader("IMAGE/END PAGE");
        icw_Efm_Document_Image_Ornttn = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Ornttn", "IMAGE-ORNTTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_ORNTTN");
        icw_Efm_Document_Image_Ornttn.setDdmHeader("DOC/ORIENT");
        icw_Efm_Document_Orgnl_Cabinet = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Orgnl_Cabinet", "ORGNL-CABINET", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "ORGNL_CABINET");
        registerRecord(vw_icw_Efm_Document);

        vw_pst_Outgoing_Mail_Item = new DataAccessProgramView(new NameInfo("vw_pst_Outgoing_Mail_Item", "PST-OUTGOING-MAIL-ITEM"), "PST_OUTGOING_MAIL_ITEM", 
            "PST_REQUEST_DATA");
        pst_Outgoing_Mail_Item_Image_Srce_Id_Id = vw_pst_Outgoing_Mail_Item.getRecord().newFieldInGroup("pst_Outgoing_Mail_Item_Image_Srce_Id_Id", "IMAGE-SRCE-ID-ID", 
            FieldType.STRING, 17, RepeatingFieldStrategy.None, "IMAGE_SRCE_ID_ID");
        pst_Outgoing_Mail_Item_Image_Srce_Id_Id.setSuperDescriptor(true);
        pst_Outgoing_Mail_Item_Pckge_Cde = vw_pst_Outgoing_Mail_Item.getRecord().newFieldInGroup("pst_Outgoing_Mail_Item_Pckge_Cde", "PCKGE-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "PCKGE_CDE");
        pst_Outgoing_Mail_Item_Pckge_Cde.setDdmHeader("PACKAGE/ID");
        registerRecord(vw_pst_Outgoing_Mail_Item);

        vw_cwf_Upload_Audit = new DataAccessProgramView(new NameInfo("vw_cwf_Upload_Audit", "CWF-UPLOAD-AUDIT"), "CWF_UPLOAD_AUDIT", "CWF_UPLOAD_AUDIT");
        cwf_Upload_Audit_Batch_Nbr = vw_cwf_Upload_Audit.getRecord().newFieldInGroup("cwf_Upload_Audit_Batch_Nbr", "BATCH-NBR", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "BATCH_NBR");
        cwf_Upload_Audit_Batch_Nbr.setDdmHeader("BATCH/NUMBER");
        cwf_Upload_Audit_Batch_Ext_Nbr = vw_cwf_Upload_Audit.getRecord().newFieldInGroup("cwf_Upload_Audit_Batch_Ext_Nbr", "BATCH-EXT-NBR", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "BATCH_EXT_NBR");
        cwf_Upload_Audit_Batch_Ext_Nbr.setDdmHeader("BATCH/EXT");
        cwf_Upload_Audit_Batch_Label_Txt = vw_cwf_Upload_Audit.getRecord().newFieldInGroup("cwf_Upload_Audit_Batch_Label_Txt", "BATCH-LABEL-TXT", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "BATCH_LABEL_TXT");
        cwf_Upload_Audit_Batch_Label_Txt.setDdmHeader("BATCH/LABEL");
        registerRecord(vw_cwf_Upload_Audit);

        pnd_Source_Id_Batch_Nbr_Key = localVariables.newFieldInRecord("pnd_Source_Id_Batch_Nbr_Key", "#SOURCE-ID-BATCH-NBR-KEY", FieldType.STRING, 16);

        pnd_Source_Id_Batch_Nbr_Key__R_Field_13 = localVariables.newGroupInRecord("pnd_Source_Id_Batch_Nbr_Key__R_Field_13", "REDEFINE", pnd_Source_Id_Batch_Nbr_Key);
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Key_Source_Id = pnd_Source_Id_Batch_Nbr_Key__R_Field_13.newFieldInGroup("pnd_Source_Id_Batch_Nbr_Key_Pnd_Key_Source_Id", 
            "#KEY-SOURCE-ID", FieldType.STRING, 6);
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Key_All_Batch = pnd_Source_Id_Batch_Nbr_Key__R_Field_13.newFieldInGroup("pnd_Source_Id_Batch_Nbr_Key_Pnd_Key_All_Batch", 
            "#KEY-ALL-BATCH", FieldType.NUMERIC, 10);

        pnd_Source_Id_Batch_Nbr_Key__R_Field_14 = pnd_Source_Id_Batch_Nbr_Key__R_Field_13.newGroupInGroup("pnd_Source_Id_Batch_Nbr_Key__R_Field_14", "REDEFINE", 
            pnd_Source_Id_Batch_Nbr_Key_Pnd_Key_All_Batch);
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Nbr = pnd_Source_Id_Batch_Nbr_Key__R_Field_14.newFieldInGroup("pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Nbr", 
            "#BATCH-NBR", FieldType.NUMERIC, 8);
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Ext_Nbr = pnd_Source_Id_Batch_Nbr_Key__R_Field_14.newFieldInGroup("pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Ext_Nbr", 
            "#BATCH-EXT-NBR", FieldType.NUMERIC, 2);
        pnd_Batch_Id_Conversion_A = localVariables.newFieldInRecord("pnd_Batch_Id_Conversion_A", "#BATCH-ID-CONVERSION-A", FieldType.STRING, 10);

        pnd_Batch_Id_Conversion_A__R_Field_15 = localVariables.newGroupInRecord("pnd_Batch_Id_Conversion_A__R_Field_15", "REDEFINE", pnd_Batch_Id_Conversion_A);
        pnd_Batch_Id_Conversion_A_Pnd_Batch_Id_Conversion_N = pnd_Batch_Id_Conversion_A__R_Field_15.newFieldInGroup("pnd_Batch_Id_Conversion_A_Pnd_Batch_Id_Conversion_N", 
            "#BATCH-ID-CONVERSION-N", FieldType.NUMERIC, 10);
        pnd_Pckge_Code = localVariables.newFieldInRecord("pnd_Pckge_Code", "#PCKGE-CODE", FieldType.STRING, 8);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Xre_Count = localVariables.newFieldInRecord("pnd_Xre_Count", "#XRE-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Doc_Count = localVariables.newFieldInRecord("pnd_Doc_Count", "#DOC-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Work_Record = localVariables.newFieldInRecord("pnd_Work_Record", "#WORK-RECORD", FieldType.STRING, 18);

        pnd_Work_Record__R_Field_16 = localVariables.newGroupInRecord("pnd_Work_Record__R_Field_16", "REDEFINE", pnd_Work_Record);
        pnd_Work_Record_Pnd_Source_Id = pnd_Work_Record__R_Field_16.newFieldInGroup("pnd_Work_Record_Pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 6);
        pnd_Work_Record_Pnd_Mail_Item_No = pnd_Work_Record__R_Field_16.newFieldInGroup("pnd_Work_Record_Pnd_Mail_Item_No", "#MAIL-ITEM-NO", FieldType.NUMERIC, 
            11);
        pnd_Work_Record_Pnd_Record_Type = pnd_Work_Record__R_Field_16.newFieldInGroup("pnd_Work_Record_Pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 
            1);

        pnd_Work_Record__R_Field_17 = localVariables.newGroupInRecord("pnd_Work_Record__R_Field_17", "REDEFINE", pnd_Work_Record);
        pnd_Work_Record_Pnd_Min_Key = pnd_Work_Record__R_Field_17.newFieldInGroup("pnd_Work_Record_Pnd_Min_Key", "#MIN-KEY", FieldType.STRING, 17);
        pnd_Work_Record_Pnd_Filler = pnd_Work_Record__R_Field_17.newFieldInGroup("pnd_Work_Record_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_18 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_18", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_18.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_18.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_18.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_18.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Source_Id_Min_Key_18 = localVariables.newFieldInRecord("pnd_Source_Id_Min_Key_18", "#SOURCE-ID-MIN-KEY-18", FieldType.STRING, 18);

        pnd_Source_Id_Min_Key_18__R_Field_19 = localVariables.newGroupInRecord("pnd_Source_Id_Min_Key_18__R_Field_19", "REDEFINE", pnd_Source_Id_Min_Key_18);
        pnd_Source_Id_Min_Key_18_Pnd_Source_Id_Min_Key_17 = pnd_Source_Id_Min_Key_18__R_Field_19.newFieldInGroup("pnd_Source_Id_Min_Key_18_Pnd_Source_Id_Min_Key_17", 
            "#SOURCE-ID-MIN-KEY-17", FieldType.STRING, 17);

        pnd_Source_Id_Min_Key_18__R_Field_20 = pnd_Source_Id_Min_Key_18__R_Field_19.newGroupInGroup("pnd_Source_Id_Min_Key_18__R_Field_20", "REDEFINE", 
            pnd_Source_Id_Min_Key_18_Pnd_Source_Id_Min_Key_17);
        pnd_Source_Id_Min_Key_18_Pnd_Source = pnd_Source_Id_Min_Key_18__R_Field_20.newFieldInGroup("pnd_Source_Id_Min_Key_18_Pnd_Source", "#SOURCE", FieldType.STRING, 
            6);
        pnd_Source_Id_Min_Key_18_Pnd_Min_Save_A = pnd_Source_Id_Min_Key_18__R_Field_20.newFieldInGroup("pnd_Source_Id_Min_Key_18_Pnd_Min_Save_A", "#MIN-SAVE-A", 
            FieldType.STRING, 11);

        pnd_Source_Id_Min_Key_18__R_Field_21 = pnd_Source_Id_Min_Key_18__R_Field_20.newGroupInGroup("pnd_Source_Id_Min_Key_18__R_Field_21", "REDEFINE", 
            pnd_Source_Id_Min_Key_18_Pnd_Min_Save_A);
        pnd_Source_Id_Min_Key_18_Pnd_Min_Save = pnd_Source_Id_Min_Key_18__R_Field_21.newFieldInGroup("pnd_Source_Id_Min_Key_18_Pnd_Min_Save", "#MIN-SAVE", 
            FieldType.NUMERIC, 11);
        pnd_Source_Id_Min_Key_18_Pnd_Rec_Type = pnd_Source_Id_Min_Key_18__R_Field_19.newFieldInGroup("pnd_Source_Id_Min_Key_18_Pnd_Rec_Type", "#REC-TYPE", 
            FieldType.STRING, 1);
        pnd_Doc_Created = localVariables.newFieldInRecord("pnd_Doc_Created", "#DOC-CREATED", FieldType.STRING, 1);
        pnd_Isn_Control = localVariables.newFieldInRecord("pnd_Isn_Control", "#ISN-CONTROL", FieldType.PACKED_DECIMAL, 11);
        pnd_Curr_Unit = localVariables.newFieldInRecord("pnd_Curr_Unit", "#CURR-UNIT", FieldType.STRING, 8);
        pnd_Init_Unit = localVariables.newFieldInRecord("pnd_Init_Unit", "#INIT-UNIT", FieldType.STRING, 8);
        pnd_Rqst_Routing_Key = localVariables.newFieldInRecord("pnd_Rqst_Routing_Key", "#RQST-ROUTING-KEY", FieldType.STRING, 30);

        pnd_Rqst_Routing_Key__R_Field_22 = localVariables.newGroupInRecord("pnd_Rqst_Routing_Key__R_Field_22", "REDEFINE", pnd_Rqst_Routing_Key);
        pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Rqst_Routing_Key__R_Field_22.newFieldInGroup("pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Icw_Rqst_Routing_Key = localVariables.newFieldInRecord("pnd_Icw_Rqst_Routing_Key", "#ICW-RQST-ROUTING-KEY", FieldType.STRING, 22);

        pnd_Icw_Rqst_Routing_Key__R_Field_23 = localVariables.newGroupInRecord("pnd_Icw_Rqst_Routing_Key__R_Field_23", "REDEFINE", pnd_Icw_Rqst_Routing_Key);
        pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Rqst_Log_Dte_Tme = pnd_Icw_Rqst_Routing_Key__R_Field_23.newFieldInGroup("pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Rqst_Log_Dte_Tme", 
            "#ICW-RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Strt_Evnt_Dte_Tme = pnd_Icw_Rqst_Routing_Key__R_Field_23.newFieldInGroup("pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Strt_Evnt_Dte_Tme", 
            "#ICW-STRT-EVNT-DTE-TME", FieldType.TIME);
        pnd_Rqst_Id_Key = localVariables.newFieldInRecord("pnd_Rqst_Id_Key", "#RQST-ID-KEY", FieldType.STRING, 30);

        pnd_Rqst_Id_Key__R_Field_24 = localVariables.newGroupInRecord("pnd_Rqst_Id_Key__R_Field_24", "REDEFINE", pnd_Rqst_Id_Key);
        pnd_Rqst_Id_Key_Pnd_Rqst_Id = pnd_Rqst_Id_Key__R_Field_24.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 28);

        pnd_Rqst_Id_Key__R_Field_25 = pnd_Rqst_Id_Key__R_Field_24.newGroupInGroup("pnd_Rqst_Id_Key__R_Field_25", "REDEFINE", pnd_Rqst_Id_Key_Pnd_Rqst_Id);
        pnd_Rqst_Id_Key_Pnd_Rqst_Work_Prcss_Id = pnd_Rqst_Id_Key__R_Field_25.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Work_Prcss_Id", "#RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        pnd_Rqst_Id_Key_Pnd_Rqst_Tiaa_Rcvd_Dte = pnd_Rqst_Id_Key__R_Field_25.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Tiaa_Rcvd_Dte", "#RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        pnd_Rqst_Id_Key_Pnd_Rqst_Case_Id = pnd_Rqst_Id_Key__R_Field_25.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Case_Id", "#RQST-CASE-ID", FieldType.STRING, 
            1);
        pnd_Rqst_Id_Key_Pnd_Rqst_Sub_Rqst_Cde = pnd_Rqst_Id_Key__R_Field_25.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Sub_Rqst_Cde", "#RQST-SUB-RQST-CDE", 
            FieldType.STRING, 1);
        pnd_Rqst_Id_Key_Pnd_Rqst_Pin_Nbr = pnd_Rqst_Id_Key__R_Field_25.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Pin_Nbr", "#RQST-PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Rqst_Id_Key_Pnd_Multi_Rqst_Ind = pnd_Rqst_Id_Key__R_Field_24.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Multi_Rqst_Ind", "#MULTI-RQST-IND", FieldType.STRING, 
            1);
        pnd_Rqst_Id_Key_Pnd_Actve_Ind = pnd_Rqst_Id_Key__R_Field_24.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 1);
        pnd_Document_Key_33 = localVariables.newFieldInRecord("pnd_Document_Key_33", "#DOCUMENT-KEY-33", FieldType.STRING, 38);

        pnd_Document_Key_33__R_Field_26 = localVariables.newGroupInRecord("pnd_Document_Key_33__R_Field_26", "REDEFINE", pnd_Document_Key_33);
        pnd_Document_Key_33_Pnd_Rqst_Log_Dte_Tme = pnd_Document_Key_33__R_Field_26.newFieldInGroup("pnd_Document_Key_33_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Document_Key_33_Pnd_Crte_Dte_Tme = pnd_Document_Key_33__R_Field_26.newFieldInGroup("pnd_Document_Key_33_Pnd_Crte_Dte_Tme", "#CRTE-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Document_Key_33_Pnd_Crte_Sqnce_Nbr = pnd_Document_Key_33__R_Field_26.newFieldInGroup("pnd_Document_Key_33_Pnd_Crte_Sqnce_Nbr", "#CRTE-SQNCE-NBR", 
            FieldType.NUMERIC, 3);

        pnd_Document_Key_33__R_Field_27 = localVariables.newGroupInRecord("pnd_Document_Key_33__R_Field_27", "REDEFINE", pnd_Document_Key_33);
        pnd_Document_Key_33_Pnd_Cab_Id = pnd_Document_Key_33__R_Field_27.newFieldInGroup("pnd_Document_Key_33_Pnd_Cab_Id", "#CAB-ID", FieldType.STRING, 
            14);
        pnd_Document_Key_33_Pnd_Ti_Rcvd_Dte = pnd_Document_Key_33__R_Field_27.newFieldInGroup("pnd_Document_Key_33_Pnd_Ti_Rcvd_Dte", "#TI-RCVD-DTE", FieldType.STRING, 
            8);
        pnd_Document_Key_33_Pnd_Case_Multi_Subrqst = pnd_Document_Key_33__R_Field_27.newFieldInGroup("pnd_Document_Key_33_Pnd_Case_Multi_Subrqst", "#CASE-MULTI-SUBRQST", 
            FieldType.STRING, 9);
        pnd_Document_Key_33_Pnd_Ent_Dte_Tme = pnd_Document_Key_33__R_Field_27.newFieldInGroup("pnd_Document_Key_33_Pnd_Ent_Dte_Tme", "#ENT-DTE-TME", FieldType.STRING, 
            7);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_28 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_28", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Run_Indicator = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Run_Indicator", "RUN-INDICATOR", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Filler_01 = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Filler_01", "FILLER-01", FieldType.STRING, 8);
        cwf_Support_Tbl_Normal_Start_Dte_Tme = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Normal_Start_Dte_Tme", "NORMAL-START-DTE-TME", 
            FieldType.NUMERIC, 15);
        cwf_Support_Tbl_Filler_02 = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Filler_02", "FILLER-02", FieldType.STRING, 3);
        cwf_Support_Tbl_Normal_End_Dte_Tme = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Normal_End_Dte_Tme", "NORMAL-END-DTE-TME", FieldType.NUMERIC, 
            15);
        cwf_Support_Tbl_Filler_03 = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Filler_03", "FILLER-03", FieldType.STRING, 2);
        cwf_Support_Tbl_Normal_Record_Count = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Normal_Record_Count", "NORMAL-RECORD-COUNT", 
            FieldType.NUMERIC, 11);
        cwf_Support_Tbl_Filler_04 = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Filler_04", "FILLER-04", FieldType.STRING, 4);
        cwf_Support_Tbl_Et_On_Off_Flag = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Et_On_Off_Flag", "ET-ON-OFF-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Filler_05 = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Filler_05", "FILLER-05", FieldType.STRING, 13);
        cwf_Support_Tbl_Special_Start_Dte_Tme = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Special_Start_Dte_Tme", "SPECIAL-START-DTE-TME", 
            FieldType.NUMERIC, 15);
        cwf_Support_Tbl_Filler_06 = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Filler_06", "FILLER-06", FieldType.STRING, 3);
        cwf_Support_Tbl_Special_End_Dte_Tme = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Special_End_Dte_Tme", "SPECIAL-END-DTE-TME", 
            FieldType.NUMERIC, 15);
        cwf_Support_Tbl_Filler_07 = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Filler_07", "FILLER-07", FieldType.STRING, 2);
        cwf_Support_Tbl_Special_Record_Count = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Special_Record_Count", "SPECIAL-RECORD-COUNT", 
            FieldType.NUMERIC, 11);
        cwf_Support_Tbl_Filler_08 = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Filler_08", "FILLER-08", FieldType.STRING, 16);
        cwf_Support_Tbl_Calc_Start_Dte_Tme = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Calc_Start_Dte_Tme", "CALC-START-DTE-TME", FieldType.NUMERIC, 
            15);
        cwf_Support_Tbl_Filler_09 = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Filler_09", "FILLER-09", FieldType.STRING, 3);
        cwf_Support_Tbl_Calc_End_Dte_Tme = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Calc_End_Dte_Tme", "CALC-END-DTE-TME", FieldType.NUMERIC, 
            15);
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Report_Name = localVariables.newFieldInRecord("pnd_Report_Name", "#REPORT-NAME", FieldType.STRING, 45);
        pnd_Date_Desc = localVariables.newFieldInRecord("pnd_Date_Desc", "#DATE-DESC", FieldType.STRING, 20);
        pnd_Run_Date = localVariables.newFieldInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.DATE);
        pnd_Deliver_To = localVariables.newFieldInRecord("pnd_Deliver_To", "#DELIVER-TO", FieldType.STRING, 45);
        pnd_Unit = localVariables.newFieldInRecord("pnd_Unit", "#UNIT", FieldType.STRING, 45);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Zero_Pin = localVariables.newFieldInRecord("pnd_Zero_Pin", "#ZERO-PIN", FieldType.BOOLEAN, 1);
        pnd_Zero_Pin_Cnt = localVariables.newFieldInRecord("pnd_Zero_Pin_Cnt", "#ZERO-PIN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Zero_Pin_All_Cnt = localVariables.newFieldInRecord("pnd_Zero_Pin_All_Cnt", "#ZERO-PIN-ALL-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Xre_All_Count = localVariables.newFieldInRecord("pnd_Xre_All_Count", "#XRE-ALL-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Err_Cntr = localVariables.newFieldInRecord("pnd_Err_Cntr", "#ERR-CNTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Xref_Deleted = localVariables.newFieldInRecord("pnd_Xref_Deleted", "#XREF-DELETED", FieldType.PACKED_DECIMAL, 7);
        pnd_Xref_Today = localVariables.newFieldInRecord("pnd_Xref_Today", "#XREF-TODAY", FieldType.PACKED_DECIMAL, 7);
        pnd_Xref_Cirs = localVariables.newFieldInRecord("pnd_Xref_Cirs", "#XREF-CIRS", FieldType.PACKED_DECIMAL, 7);
        pnd_Cwf_Deleted = localVariables.newFieldInRecord("pnd_Cwf_Deleted", "#CWF-DELETED", FieldType.PACKED_DECIMAL, 7);
        pnd_Cwf_Today = localVariables.newFieldInRecord("pnd_Cwf_Today", "#CWF-TODAY", FieldType.PACKED_DECIMAL, 7);
        pnd_Icw_Deleted = localVariables.newFieldInRecord("pnd_Icw_Deleted", "#ICW-DELETED", FieldType.PACKED_DECIMAL, 7);
        pnd_Icw_Today = localVariables.newFieldInRecord("pnd_Icw_Today", "#ICW-TODAY", FieldType.PACKED_DECIMAL, 7);
        pnd_Ncw_Deleted = localVariables.newFieldInRecord("pnd_Ncw_Deleted", "#NCW-DELETED", FieldType.PACKED_DECIMAL, 7);
        pnd_Ncw_Today = localVariables.newFieldInRecord("pnd_Ncw_Today", "#NCW-TODAY", FieldType.PACKED_DECIMAL, 7);
        pnd_Call_Cntr = localVariables.newFieldInRecord("pnd_Call_Cntr", "#CALL-CNTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Prev_Source = localVariables.newFieldInRecord("pnd_Prev_Source", "#PREV-SOURCE", FieldType.STRING, 6);
        pnd_Prev_Doc_Source = localVariables.newFieldInRecord("pnd_Prev_Doc_Source", "#PREV-DOC-SOURCE", FieldType.STRING, 6);
        pnd_Doc_Entry_Dte_Tme = localVariables.newFieldInRecord("pnd_Doc_Entry_Dte_Tme", "#DOC-ENTRY-DTE-TME", FieldType.TIME);
        pnd_Doc_Entry_Dte = localVariables.newFieldInRecord("pnd_Doc_Entry_Dte", "#DOC-ENTRY-DTE", FieldType.STRING, 8);

        pnd_Doc_Entry_Dte__R_Field_29 = localVariables.newGroupInRecord("pnd_Doc_Entry_Dte__R_Field_29", "REDEFINE", pnd_Doc_Entry_Dte);
        pnd_Doc_Entry_Dte_Pnd_Doc_Entry_Dte_N = pnd_Doc_Entry_Dte__R_Field_29.newFieldInGroup("pnd_Doc_Entry_Dte_Pnd_Doc_Entry_Dte_N", "#DOC-ENTRY-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_Upld_Entry_Dte = localVariables.newFieldInRecord("pnd_Upld_Entry_Dte", "#UPLD-ENTRY-DTE", FieldType.STRING, 8);

        pnd_Upld_Entry_Dte__R_Field_30 = localVariables.newGroupInRecord("pnd_Upld_Entry_Dte__R_Field_30", "REDEFINE", pnd_Upld_Entry_Dte);
        pnd_Upld_Entry_Dte_Pnd_Upld_Entry_Dte_N = pnd_Upld_Entry_Dte__R_Field_30.newFieldInGroup("pnd_Upld_Entry_Dte_Pnd_Upld_Entry_Dte_N", "#UPLD-ENTRY-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_Image_Source_Id = localVariables.newFieldInRecord("pnd_Image_Source_Id", "#IMAGE-SOURCE-ID", FieldType.STRING, 6);
        pnd_Cabinet_Id = localVariables.newFieldInRecord("pnd_Cabinet_Id", "#CABINET-ID", FieldType.STRING, 14);
        pnd_Tiaa_Rcvd_Dte = localVariables.newFieldInRecord("pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Case_Workprcss_Multi_Subrqst = localVariables.newFieldInRecord("pnd_Case_Workprcss_Multi_Subrqst", "#CASE-WORKPRCSS-MULTI-SUBRQST", FieldType.STRING, 
            9);

        pnd_Case_Workprcss_Multi_Subrqst__R_Field_31 = localVariables.newGroupInRecord("pnd_Case_Workprcss_Multi_Subrqst__R_Field_31", "REDEFINE", pnd_Case_Workprcss_Multi_Subrqst);
        pnd_Case_Workprcss_Multi_Subrqst_Pnd_Case_Id = pnd_Case_Workprcss_Multi_Subrqst__R_Field_31.newFieldInGroup("pnd_Case_Workprcss_Multi_Subrqst_Pnd_Case_Id", 
            "#CASE-ID", FieldType.STRING, 1);
        pnd_Case_Workprcss_Multi_Subrqst_Pnd_Work_Prcss_Id = pnd_Case_Workprcss_Multi_Subrqst__R_Field_31.newFieldInGroup("pnd_Case_Workprcss_Multi_Subrqst_Pnd_Work_Prcss_Id", 
            "#WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Case_Workprcss_Multi_Subrqst_Pnd_Multi_Ind = pnd_Case_Workprcss_Multi_Subrqst__R_Field_31.newFieldInGroup("pnd_Case_Workprcss_Multi_Subrqst_Pnd_Multi_Ind", 
            "#MULTI-IND", FieldType.STRING, 1);
        pnd_Case_Workprcss_Multi_Subrqst_Pnd_Subrqst_Ind = pnd_Case_Workprcss_Multi_Subrqst__R_Field_31.newFieldInGroup("pnd_Case_Workprcss_Multi_Subrqst_Pnd_Subrqst_Ind", 
            "#SUBRQST-IND", FieldType.STRING, 1);
        pnd_Pin_Ppg = localVariables.newFieldInRecord("pnd_Pin_Ppg", "#PIN-PPG", FieldType.STRING, 12);
        pnd_Entry_Dte_Tme = localVariables.newFieldInRecord("pnd_Entry_Dte_Tme", "#ENTRY-DTE-TME", FieldType.TIME);
        pnd_Identify_Dte_Tme = localVariables.newFieldInRecord("pnd_Identify_Dte_Tme", "#IDENTIFY-DTE-TME", FieldType.STRING, 19);
        pnd_Wf_Cntr = localVariables.newFieldInRecord("pnd_Wf_Cntr", "#WF-CNTR", FieldType.NUMERIC, 7);
        pnd_Xre_Doc_Created = localVariables.newFieldInRecord("pnd_Xre_Doc_Created", "#XRE-DOC-CREATED", FieldType.NUMERIC, 7);
        pnd_Xre_Doc_Cwf = localVariables.newFieldInRecord("pnd_Xre_Doc_Cwf", "#XRE-DOC-CWF", FieldType.NUMERIC, 7);
        pnd_Xre_Doc_Icw = localVariables.newFieldInRecord("pnd_Xre_Doc_Icw", "#XRE-DOC-ICW", FieldType.NUMERIC, 7);
        pnd_Xre_Doc_Ncw = localVariables.newFieldInRecord("pnd_Xre_Doc_Ncw", "#XRE-DOC-NCW", FieldType.NUMERIC, 7);
        pnd_Page_Number = localVariables.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.NUMERIC, 5);
        pnd_Call_Nat = localVariables.newFieldInRecord("pnd_Call_Nat", "#CALL-NAT", FieldType.NUMERIC, 5);
        pnd_Entry_Date_Time = localVariables.newFieldInRecord("pnd_Entry_Date_Time", "#ENTRY-DATE-TIME", FieldType.STRING, 15);

        pnd_Entry_Date_Time__R_Field_32 = localVariables.newGroupInRecord("pnd_Entry_Date_Time__R_Field_32", "REDEFINE", pnd_Entry_Date_Time);
        pnd_Entry_Date_Time_Pnd_Entry_Date = pnd_Entry_Date_Time__R_Field_32.newFieldInGroup("pnd_Entry_Date_Time_Pnd_Entry_Date", "#ENTRY-DATE", FieldType.STRING, 
            8);
        pnd_Entry_Date_Time_Pnd_Entry_Time = pnd_Entry_Date_Time__R_Field_32.newFieldInGroup("pnd_Entry_Date_Time_Pnd_Entry_Time", "#ENTRY-TIME", FieldType.STRING, 
            7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Image_Xref.reset();
        vw_cwf_Master_Index_View.reset();
        vw_cwf_Efm_Document.reset();
        vw_ncw_Efm_Document.reset();
        vw_cwf_Mit_History.reset();
        vw_icw_Master_Index.reset();
        vw_ncw_Master_Index.reset();
        vw_icw_Mit_History.reset();
        vw_ncw_Mit_History.reset();
        vw_icw_Efm_Document.reset();
        vw_pst_Outgoing_Mail_Item.reset();
        vw_cwf_Upload_Audit.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Report_Name.setInitialValue("IMAGE XREF / DOC RECONCILIATION");
        pnd_Date_Desc.setInitialValue("RUN DATE -->");
        pnd_Deliver_To.setInitialValue("STAN MARICLE");
        pnd_Unit.setInitialValue("CRC");
        pnd_Floor.setInitialValue(12);
        pnd_Bldg.setInitialValue("485");
        pnd_Zero_Pin.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Efsb6120() throws Exception
    {
        super("Efsb6120");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*  MINS ON XREF BUT NOT ON DOC
        getReports().definePrinter(2, "XRE");                                                                                                                             //Natural: DEFINE PRINTER ( XRE = 1 )
        //*  MINS ON DOC  BUT NOT ON XREF
        getReports().definePrinter(3, "DOC");                                                                                                                             //Natural: DEFINE PRINTER ( DOC = 2 )
        //*  ERROR MESSAGES
        getReports().definePrinter(4, "MSG");                                                                                                                             //Natural: DEFINE PRINTER ( MSG = 3 )
        //*                                                                                                                                                               //Natural: FORMAT ( XRE ) LS = 133 PS = 60 ZP = OFF;//Natural: FORMAT ( DOC ) LS = 133 PS = 60 ZP = OFF;//Natural: FORMAT ( MSG ) LS = 133 PS = 60 ZP = ON
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM := *PROGRAM
        //*  ASSIGN BANNER PAGE VARIABLES
        pnd_Run_Date.setValue(Global.getDATX());                                                                                                                          //Natural: ASSIGN #RUN-DATE := *DATX
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                     //Natural: ASSIGN #ENV := *LIBRARY-ID
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE DISTRIBUTION BANNER PAGE
        //*  BANNER PAGE
        getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3910.class));                                                          //Natural: WRITE ( XRE ) NOTITLE NOHDR USING FORM 'CWFF3910'
        //*  BANNER PAGE (DUPLEX)
        getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3910.class));                                                          //Natural: WRITE ( XRE ) NOTITLE NOHDR USING FORM 'CWFF3910'
        //*  WRITE (XRE) NOTITLE NOHDR USING FORM 'EFSF8711'
        pnd_Page_Number.setValue(3);                                                                                                                                      //Natural: MOVE 3 TO #PAGE-NUMBER
        //*  BANNER PAGE
        getReports().write(3, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3910.class));                                                          //Natural: WRITE ( DOC ) NOTITLE NOHDR USING FORM 'CWFF3910'
        //*  BANNER PAGE (DUPLEX)
        getReports().write(3, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3910.class));                                                          //Natural: WRITE ( DOC ) NOTITLE NOHDR USING FORM 'CWFF3910'
        //*  WRITE (DOC) NOTITLE NOHDR USING FORM 'EFSF8702'
        //*  BANNER PAGE
        getReports().write(4, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3910.class));                                                          //Natural: WRITE ( MSG ) NOTITLE NOHDR USING FORM 'CWFF3910'
        //*  BANNER PAGE (DUPLEX)
        getReports().write(4, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3910.class));                                                          //Natural: WRITE ( MSG ) NOTITLE NOHDR USING FORM 'CWFF3910'
        //*  WRITE (MSG) NOTITLE NOHDR USING FORM 'EFSF8709'
                                                                                                                                                                          //Natural: PERFORM READ-RUN-CONTROL-RECORD
        sub_Read_Run_Control_Record();
        if (condition(Global.isEscape())) {return;}
        //*  READ THE WORK FILE WHICH WAS CREATED BY EFSB6110
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, pnd_Work_Record)))
        {
            if (condition(pnd_Work_Record.equals(pnd_Source_Id_Min_Key_18)))                                                                                              //Natural: IF #WORK-RECORD = #SOURCE-ID-MIN-KEY-18
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Source_Id_Min_Key_18.setValue(pnd_Work_Record);                                                                                                       //Natural: MOVE #WORK-RECORD TO #SOURCE-ID-MIN-KEY-18
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT END OF DATA
            pnd_Wf_Cntr.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #WF-CNTR
            if (condition(pnd_Wf_Cntr.equals(1)))                                                                                                                         //Natural: IF #WF-CNTR = 1
            {
                pnd_Prev_Source.setValue(pnd_Work_Record_Pnd_Source_Id);                                                                                                  //Natural: ASSIGN #PREV-SOURCE := #SOURCE := #WORK-RECORD.#SOURCE-ID
                pnd_Source_Id_Min_Key_18_Pnd_Source.setValue(pnd_Work_Record_Pnd_Source_Id);
                pnd_Prev_Doc_Source.setValue(pnd_Work_Record_Pnd_Source_Id);                                                                                              //Natural: ASSIGN #PREV-DOC-SOURCE := #SOURCE := #WORK-RECORD.#SOURCE-ID
                pnd_Source_Id_Min_Key_18_Pnd_Source.setValue(pnd_Work_Record_Pnd_Source_Id);
                pnd_Source_Id_Min_Key_18_Pnd_Min_Save.setValue(pnd_Work_Record_Pnd_Mail_Item_No);                                                                         //Natural: ASSIGN #MIN-SAVE := #WORK-RECORD.#MAIL-ITEM-NO
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pin_Ppg.reset();                                                                                                                                          //Natural: RESET #PIN-PPG #INIT-UNIT #CURR-UNIT
            pnd_Init_Unit.reset();
            pnd_Curr_Unit.reset();
            //*  USE CWF-EFM-DOCUMENT FILE
            short decideConditionsMet591 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RECORD-TYPE = 'D'
            if (condition(pnd_Work_Record_Pnd_Record_Type.equals("D")))
            {
                decideConditionsMet591++;
                if (condition(pnd_Work_Record_Pnd_Source_Id.notEquals(pnd_Prev_Doc_Source)))                                                                              //Natural: IF #SOURCE-ID NE #PREV-DOC-SOURCE
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-DOC-SOURCE-TOTAL
                    sub_Print_Doc_Source_Total();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Prev_Doc_Source.setValue(pnd_Work_Record_Pnd_Source_Id);                                                                                          //Natural: ASSIGN #PREV-DOC-SOURCE := #SOURCE := #WORK-RECORD.#SOURCE-ID
                    pnd_Source_Id_Min_Key_18_Pnd_Source.setValue(pnd_Work_Record_Pnd_Source_Id);
                }                                                                                                                                                         //Natural: END-IF
                //*  USE ICW-EFM-DOCUMENT FILE
                                                                                                                                                                          //Natural: PERFORM REPORT-CWF-DOC-MISMATCH
                sub_Report_Cwf_Doc_Mismatch();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #RECORD-TYPE = 'I'
            else if (condition(pnd_Work_Record_Pnd_Record_Type.equals("I")))
            {
                decideConditionsMet591++;
                if (condition(pnd_Work_Record_Pnd_Source_Id.notEquals(pnd_Prev_Doc_Source)))                                                                              //Natural: IF #SOURCE-ID NE #PREV-DOC-SOURCE
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-DOC-SOURCE-TOTAL
                    sub_Print_Doc_Source_Total();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Prev_Doc_Source.setValue(pnd_Work_Record_Pnd_Source_Id);                                                                                          //Natural: ASSIGN #PREV-DOC-SOURCE := #SOURCE := #WORK-RECORD.#SOURCE-ID
                    pnd_Source_Id_Min_Key_18_Pnd_Source.setValue(pnd_Work_Record_Pnd_Source_Id);
                }                                                                                                                                                         //Natural: END-IF
                //*  USE NCW-EFM-DOCUMENT FILE
                                                                                                                                                                          //Natural: PERFORM REPORT-ICW-DOC-MISMATCH
                sub_Report_Icw_Doc_Mismatch();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #RECORD-TYPE = 'N'
            else if (condition(pnd_Work_Record_Pnd_Record_Type.equals("N")))
            {
                decideConditionsMet591++;
                if (condition(pnd_Work_Record_Pnd_Source_Id.notEquals(pnd_Prev_Doc_Source)))                                                                              //Natural: IF #SOURCE-ID NE #PREV-DOC-SOURCE
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-DOC-SOURCE-TOTAL
                    sub_Print_Doc_Source_Total();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Prev_Doc_Source.setValue(pnd_Work_Record_Pnd_Source_Id);                                                                                          //Natural: ASSIGN #PREV-DOC-SOURCE := #SOURCE := #WORK-RECORD.#SOURCE-ID
                    pnd_Source_Id_Min_Key_18_Pnd_Source.setValue(pnd_Work_Record_Pnd_Source_Id);
                }                                                                                                                                                         //Natural: END-IF
                //*  USE CWF-IMAGE-XREF FILE
                                                                                                                                                                          //Natural: PERFORM REPORT-NCW-DOC-MISMATCH
                sub_Report_Ncw_Doc_Mismatch();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #RECORD-TYPE = 'X'
            else if (condition(pnd_Work_Record_Pnd_Record_Type.equals("X")))
            {
                decideConditionsMet591++;
                if (condition(pnd_Work_Record_Pnd_Source_Id.notEquals(pnd_Prev_Source)))                                                                                  //Natural: IF #SOURCE-ID NE #PREV-SOURCE
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-XRE-SOURCE-TOTAL
                    sub_Print_Xre_Source_Total();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Prev_Source.setValue(pnd_Work_Record_Pnd_Source_Id);                                                                                              //Natural: ASSIGN #PREV-SOURCE := #SOURCE := #WORK-RECORD.#SOURCE-ID
                    pnd_Source_Id_Min_Key_18_Pnd_Source.setValue(pnd_Work_Record_Pnd_Source_Id);
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM REPORT-CWF-XREF-MISMATCH
                sub_Report_Cwf_Xref_Mismatch();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-XRE-SOURCE-TOTAL
            sub_Print_Xre_Source_Total();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DOC-SOURCE-TOTAL
            sub_Print_Doc_Source_Total();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
            sub_Print_Totals();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-RECORD
            sub_Update_Control_Record();
            if (condition(Global.isEscape())) {return;}
            //*  WRITE END BANNER PAGE
            //*    NEWPAGE (DOC)
            //*  END BANNER PGE
            getReports().write(3, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3911.class));                                                      //Natural: WRITE ( DOC ) NOTITLE NOHDR USING FORM 'CWFF3911'
            //*  END BANNER PGE(DUP)
            getReports().write(3, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3911.class));                                                      //Natural: WRITE ( DOC ) NOTITLE NOHDR USING FORM 'CWFF3911'
            //*    NEWPAGE (XRE)
            //*  END BANNER PGE
            getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3911.class));                                                      //Natural: WRITE ( XRE ) NOTITLE NOHDR USING FORM 'CWFF3911'
            //*  END BANNER PGE(DUP)
            getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3911.class));                                                      //Natural: WRITE ( XRE ) NOTITLE NOHDR USING FORM 'CWFF3911'
            //*    NEWPAGE (MSG)
            //*  END BANNER PGE
            getReports().write(4, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3911.class));                                                      //Natural: WRITE ( MSG ) NOTITLE NOHDR USING FORM 'CWFF3911'
            //*  END BANNER PGE(DUP)
            getReports().write(4, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff3911.class));                                                      //Natural: WRITE ( MSG ) NOTITLE NOHDR USING FORM 'CWFF3911'
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  ----------------- POPULATE DOCUMENT KEY -----------------------
        //*  ----------------------------------------------------------------
        //* ***********************************************************************
        //*  #MULTI-IND     := ICW-MASTER-INDEX.MULTI-RQST-IND
        //*  DETERMINE INITIAL DESTINATION UNIT
        //*  END-FIND
        //*  ----------------- POPULATE DOCUMENT KEY -----------------------
        //*  ----------------------------------------------------------------
        //* ***********************************************************************
        //*  #MULTI-IND     := NCW-MASTER-INDEX.MULTI-RQST-IND
        //*  DETERMINE INITIAL DESTINATION UNIT
        //*  ----------------- POPULATE DOCUMENT KEY -----------------------
        //*  ----------------------------------------------------------------
        //* ***********************************************************************
        //*  ------ FIND AUDIT LOG RECORD --------
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //*  =======================================
        //*  =====================================
        //*  =====================================
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( XRE )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( DOC )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( MSG )
    }
    private void sub_Report_Cwf_Doc_Mismatch() throws Exception                                                                                                           //Natural: REPORT-CWF-DOC-MISMATCH
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_cwf_Efm_Document.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) CWF-EFM-DOCUMENT WITH IMAGE-POINTER-KEY = #SOURCE-ID-MIN-KEY-17
        (
        "FIND01",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Source_Id_Min_Key_18_Pnd_Source_Id_Min_Key_17, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_cwf_Efm_Document.readNextRow("FIND01", true)))
        {
            vw_cwf_Efm_Document.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Efm_Document.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  DELETED
        if (condition(cwf_Efm_Document_Document_Retention_Ind.equals("D")))                                                                                               //Natural: IF CWF-EFM-DOCUMENT.DOCUMENT-RETENTION-IND = 'D'
        {
            pnd_Cwf_Deleted.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CWF-DELETED
            //*  DO NOT REPORT ON DELETED MINS.
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  12/1/98
        if (condition(cwf_Efm_Document_Image_Source_Id.equals("POST") || cwf_Efm_Document_Image_Source_Id.equals("PCPOST")))                                              //Natural: IF CWF-EFM-DOCUMENT.IMAGE-SOURCE-ID = 'POST' OR = 'PCPOST'
        {
            pnd_Doc_Entry_Dte.setValueEdited(cwf_Efm_Document_Entry_Dte_Tme,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED CWF-EFM-DOCUMENT.ENTRY-DTE-TME ( EM = YYYYMMDD ) TO #DOC-ENTRY-DTE
            if (condition(pnd_Doc_Entry_Dte_Pnd_Doc_Entry_Dte_N.equals(Global.getDATN())))                                                                                //Natural: IF #DOC-ENTRY-DTE-N = *DATN
            {
                pnd_Cwf_Today.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CWF-TODAY
                //*  DO NOT REPORT ON POST MINS FOR CURRENT DATE.
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Image_Source_Id.setValue(cwf_Efm_Document_Image_Source_Id);                                                                                                   //Natural: ASSIGN #IMAGE-SOURCE-ID := CWF-EFM-DOCUMENT.IMAGE-SOURCE-ID
        pnd_Cabinet_Id.setValue(cwf_Efm_Document_Cabinet_Id);                                                                                                             //Natural: ASSIGN #CABINET-ID := CWF-EFM-DOCUMENT.CABINET-ID
        pnd_Tiaa_Rcvd_Dte.setValue(cwf_Efm_Document_Tiaa_Rcvd_Dte);                                                                                                       //Natural: ASSIGN #TIAA-RCVD-DTE := CWF-EFM-DOCUMENT.TIAA-RCVD-DTE
        pnd_Case_Workprcss_Multi_Subrqst.setValue(cwf_Efm_Document_Case_Workprcss_Multi_Subrqst);                                                                         //Natural: ASSIGN #CASE-WORKPRCSS-MULTI-SUBRQST := CWF-EFM-DOCUMENT.CASE-WORKPRCSS-MULTI-SUBRQST
        pnd_Entry_Dte_Tme.setValue(cwf_Efm_Document_Entry_Dte_Tme);                                                                                                       //Natural: ASSIGN #ENTRY-DTE-TME := CWF-EFM-DOCUMENT.ENTRY-DTE-TME
        pnd_Rqst_Id_Key_Pnd_Rqst_Work_Prcss_Id.setValue(cwf_Efm_Document_Originating_Work_Prcss_Id);                                                                      //Natural: ASSIGN #RQST-WORK-PRCSS-ID := CWF-EFM-DOCUMENT.ORIGINATING-WORK-PRCSS-ID
        pnd_Rqst_Id_Key_Pnd_Rqst_Tiaa_Rcvd_Dte.setValueEdited(cwf_Efm_Document_Tiaa_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED CWF-EFM-DOCUMENT.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO #RQST-TIAA-RCVD-DTE
        pnd_Rqst_Id_Key_Pnd_Rqst_Case_Id.setValue(cwf_Efm_Document_Case_Ind);                                                                                             //Natural: ASSIGN #RQST-CASE-ID := CWF-EFM-DOCUMENT.CASE-IND
        pnd_Rqst_Id_Key_Pnd_Rqst_Sub_Rqst_Cde.setValue(cwf_Efm_Document_Sub_Rqst_Ind);                                                                                    //Natural: ASSIGN #RQST-SUB-RQST-CDE := CWF-EFM-DOCUMENT.SUB-RQST-IND
        pnd_Rqst_Id_Key_Pnd_Rqst_Pin_Nbr.setValue(cwf_Efm_Document_Pin_Nbr);                                                                                              //Natural: ASSIGN #RQST-PIN-NBR := CWF-EFM-DOCUMENT.PIN-NBR
        pnd_Rqst_Id_Key_Pnd_Multi_Rqst_Ind.setValue(cwf_Efm_Document_Multi_Rqst_Ind);                                                                                     //Natural: ASSIGN #MULTI-RQST-IND := CWF-EFM-DOCUMENT.MULTI-RQST-IND
        vw_cwf_Master_Index_View.startDatabaseFind                                                                                                                        //Natural: FIND ( 1 ) CWF-MASTER-INDEX-VIEW WITH RQST-ID-KEY = #RQST-ID-KEY
        (
        "FIND02",
        new Wc[] { new Wc("RQST_ID_KEY", "=", pnd_Rqst_Id_Key, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(vw_cwf_Master_Index_View.readNextRow("FIND02", true)))
        {
            vw_cwf_Master_Index_View.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Master_Index_View.getAstCOUNTER().equals(0)))                                                                                            //Natural: IF NO RECORD FOUND
            {
                vw_cwf_Master_Index_View.reset();                                                                                                                         //Natural: RESET CWF-MASTER-INDEX-VIEW
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Curr_Unit.setValue(cwf_Master_Index_View_Admin_Unit_Cde);                                                                                                 //Natural: ASSIGN #CURR-UNIT := CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(cwf_Master_Index_View_Rqst_Log_Dte_Tme.greater(" ")))                                                                                               //Natural: IF CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME GT ' '
        {
            //*  DETERMINE INITIAL DESTINATION UNIT
            pnd_Rqst_Routing_Key.reset();                                                                                                                                 //Natural: RESET #RQST-ROUTING-KEY
            pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme.setValue(cwf_Master_Index_View_Rqst_Log_Dte_Tme);                                                                   //Natural: ASSIGN #RQST-ROUTING-KEY.#RQST-LOG-DTE-TME := CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
            vw_cwf_Mit_History.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CWF-MIT-HISTORY BY RQST-ROUTING-KEY = #RQST-ROUTING-KEY
            (
            "READ02",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Rqst_Routing_Key, WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
            1
            );
            READ02:
            while (condition(vw_cwf_Mit_History.readNextRow("READ02")))
            {
                pnd_Init_Unit.setValue(cwf_Mit_History_Admin_Unit_Cde);                                                                                                   //Natural: ASSIGN #INIT-UNIT := CWF-MIT-HISTORY.ADMIN-UNIT-CDE
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Document_Key_33_Pnd_Cab_Id.setValue(cwf_Efm_Document_Cabinet_Id);                                                                                             //Natural: ASSIGN #DOCUMENT-KEY-33.#CAB-ID := CWF-EFM-DOCUMENT.CABINET-ID
        pnd_Document_Key_33_Pnd_Ti_Rcvd_Dte.setValueEdited(cwf_Efm_Document_Tiaa_Rcvd_Dte,new ReportEditMask("MM/DD/YY"));                                                //Natural: MOVE EDITED CWF-EFM-DOCUMENT.TIAA-RCVD-DTE ( EM = MM/DD/YY ) TO #DOCUMENT-KEY-33.#TI-RCVD-DTE
        pnd_Document_Key_33_Pnd_Case_Multi_Subrqst.setValue(cwf_Efm_Document_Case_Workprcss_Multi_Subrqst);                                                               //Natural: MOVE CWF-EFM-DOCUMENT.CASE-WORKPRCSS-MULTI-SUBRQST TO #DOCUMENT-KEY-33.#CASE-MULTI-SUBRQST
        pnd_Entry_Date_Time.setValueEdited(cwf_Efm_Document_Entry_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                         //Natural: MOVE EDITED CWF-EFM-DOCUMENT.ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #ENTRY-DATE-TIME
        pnd_Document_Key_33_Pnd_Ent_Dte_Tme.setValue(pnd_Entry_Date_Time_Pnd_Entry_Date);                                                                                 //Natural: MOVE #ENTRY-DATE TO #DOCUMENT-KEY-33.#ENT-DTE-TME
        getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Efsf8713.class));                                                                                //Natural: WRITE ( DOC ) NOHDR USING FORM 'EFSF8713'
        pnd_Doc_Count.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #DOC-COUNT
    }
    private void sub_Report_Icw_Doc_Mismatch() throws Exception                                                                                                           //Natural: REPORT-ICW-DOC-MISMATCH
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  FIND (1) ICW-EFM-DOCUMENT WITH IMAGE-POINTER-KEY =
        //*                            #SOURCE-ID-MIN-KEY-17
        //*  IF NO RECORD FOUND
        //*    ESCAPE ROUTINE
        //*  END-NOREC
        vw_icw_Efm_Document.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) ICW-EFM-DOCUMENT WITH IMAGE-POINTER-KEY = #SOURCE-ID-MIN-KEY-17
        (
        "READ03",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", ">=", pnd_Source_Id_Min_Key_18_Pnd_Source_Id_Min_Key_17, WcType.BY) },
        new Oc[] { new Oc("IMAGE_POINTER_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(vw_icw_Efm_Document.readNextRow("READ03")))
        {
            if (condition(icw_Efm_Document_Image_Source_Id.notEquals(pnd_Source_Id_Min_Key_18_Pnd_Source) || icw_Efm_Document_Image_Min_11.notEquals(pnd_Source_Id_Min_Key_18_Pnd_Min_Save_A))) //Natural: IF IMAGE-SOURCE-ID NOT = #SOURCE-ID-MIN-KEY-18.#SOURCE OR IMAGE-MIN-11 NOT = #SOURCE-ID-MIN-KEY-18.#MIN-SAVE-A
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  DELETED
            if (condition(icw_Efm_Document_Dcmnt_Rtntn.equals("D")))                                                                                                      //Natural: IF ICW-EFM-DOCUMENT.DCMNT-RTNTN = 'D'
            {
                pnd_Icw_Deleted.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #ICW-DELETED
                //*  DO NOT REPORT ON DELETED MINS.
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  12/1/98
            if (condition(icw_Efm_Document_Image_Source_Id.equals("POST") || icw_Efm_Document_Image_Source_Id.equals("PCPOST")))                                          //Natural: IF ICW-EFM-DOCUMENT.IMAGE-SOURCE-ID = 'POST' OR = 'PCPOST'
            {
                pnd_Doc_Entry_Dte.setValueEdited(icw_Efm_Document_Crte_Dte_Tme,new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED ICW-EFM-DOCUMENT.CRTE-DTE-TME ( EM = YYYYMMDD ) TO #DOC-ENTRY-DTE
                if (condition(pnd_Doc_Entry_Dte_Pnd_Doc_Entry_Dte_N.equals(Global.getDATN())))                                                                            //Natural: IF #DOC-ENTRY-DTE-N = *DATN
                {
                    pnd_Icw_Today.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ICW-TODAY
                    //*  DO NOT REPORT ON POST MINS FOR CURRENT DATE.
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                //*  INST & PPG-CDE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Image_Source_Id.setValue(icw_Efm_Document_Image_Source_Id);                                                                                               //Natural: ASSIGN #IMAGE-SOURCE-ID := ICW-EFM-DOCUMENT.IMAGE-SOURCE-ID
            pnd_Cabinet_Id.setValue(icw_Efm_Document_Cabinet_Id);                                                                                                         //Natural: ASSIGN #CABINET-ID := ICW-EFM-DOCUMENT.CABINET-ID
            pnd_Entry_Dte_Tme.setValue(icw_Efm_Document_Crte_Dte_Tme);                                                                                                    //Natural: ASSIGN #ENTRY-DTE-TME := ICW-EFM-DOCUMENT.CRTE-DTE-TME
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  END-FIND
        vw_icw_Master_Index.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) ICW-MASTER-INDEX WITH ACTV-UNQUE-KEY = ICW-EFM-DOCUMENT.RQST-LOG-DTE-TME
        (
        "FIND03",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", "=", icw_Efm_Document_Rqst_Log_Dte_Tme, WcType.WITH) },
        1
        );
        FIND03:
        while (condition(vw_icw_Master_Index.readNextRow("FIND03")))
        {
            vw_icw_Master_Index.setIfNotFoundControlFlag(false);
            pnd_Tiaa_Rcvd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),icw_Master_Index_Rqst_Tiaa_Rcvd_Dte);                                                         //Natural: MOVE EDITED ICW-MASTER-INDEX.RQST-TIAA-RCVD-DTE TO #TIAA-RCVD-DTE ( EM = YYYYMMDD )
            pnd_Case_Workprcss_Multi_Subrqst_Pnd_Case_Id.setValue(icw_Master_Index_Pnd_Case_Id_Cde);                                                                      //Natural: ASSIGN #CASE-ID := ICW-MASTER-INDEX.#CASE-ID-CDE
            pnd_Case_Workprcss_Multi_Subrqst_Pnd_Work_Prcss_Id.setValue(icw_Master_Index_Rqst_Work_Prcss_Id);                                                             //Natural: ASSIGN #WORK-PRCSS-ID := ICW-MASTER-INDEX.RQST-WORK-PRCSS-ID
            pnd_Case_Workprcss_Multi_Subrqst_Pnd_Subrqst_Ind.setValue(icw_Master_Index_Pnd_Subrqst_Id_Cde);                                                               //Natural: ASSIGN #SUBRQST-IND := ICW-MASTER-INDEX.#SUBRQST-ID-CDE
            pnd_Curr_Unit.setValue(icw_Master_Index_Admin_Unit_Cde);                                                                                                      //Natural: ASSIGN #CURR-UNIT := ICW-MASTER-INDEX.ADMIN-UNIT-CDE
            pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Rqst_Log_Dte_Tme.setValue(icw_Master_Index_Rqst_Log_Dte_Tme);                                                                //Natural: ASSIGN #ICW-RQST-LOG-DTE-TME := ICW-MASTER-INDEX.RQST-LOG-DTE-TME
            pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Strt_Evnt_Dte_Tme.reset();                                                                                                   //Natural: RESET #ICW-STRT-EVNT-DTE-TME
            vw_icw_Mit_History.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) ICW-MIT-HISTORY BY RQST-ROUTING-KEY = #ICW-RQST-ROUTING-KEY
            (
            "READ04",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Icw_Rqst_Routing_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
            1
            );
            READ04:
            while (condition(vw_icw_Mit_History.readNextRow("READ04")))
            {
                pnd_Init_Unit.setValue(icw_Mit_History_Admin_Unit_Cde);                                                                                                   //Natural: ASSIGN #INIT-UNIT := ICW-MIT-HISTORY.ADMIN-UNIT-CDE
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Document_Key_33_Pnd_Rqst_Log_Dte_Tme.setValue(icw_Efm_Document_Rqst_Log_Dte_Tme);                                                                         //Natural: ASSIGN #DOCUMENT-KEY-33.#RQST-LOG-DTE-TME := ICW-EFM-DOCUMENT.RQST-LOG-DTE-TME
            pnd_Document_Key_33_Pnd_Crte_Dte_Tme.setValueEdited(icw_Efm_Document_Crte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                     //Natural: MOVE EDITED ICW-EFM-DOCUMENT.CRTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #DOCUMENT-KEY-33.#CRTE-DTE-TME
            pnd_Document_Key_33_Pnd_Crte_Sqnce_Nbr.setValue(icw_Efm_Document_Crte_Sqnce_Nbr);                                                                             //Natural: ASSIGN #DOCUMENT-KEY-33.#CRTE-SQNCE-NBR := ICW-EFM-DOCUMENT.CRTE-SQNCE-NBR
            getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Efsf8713.class));                                                                            //Natural: WRITE ( DOC ) NOHDR USING FORM 'EFSF8713'
            pnd_Doc_Count.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #DOC-COUNT
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Report_Ncw_Doc_Mismatch() throws Exception                                                                                                           //Natural: REPORT-NCW-DOC-MISMATCH
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_ncw_Efm_Document.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) NCW-EFM-DOCUMENT WITH IMAGE-POINTER-KEY = #SOURCE-ID-MIN-KEY-17
        (
        "FIND04",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Source_Id_Min_Key_18_Pnd_Source_Id_Min_Key_17, WcType.WITH) },
        1
        );
        FIND04:
        while (condition(vw_ncw_Efm_Document.readNextRow("FIND04", true)))
        {
            vw_ncw_Efm_Document.setIfNotFoundControlFlag(false);
            if (condition(vw_ncw_Efm_Document.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  DELETED
        if (condition(ncw_Efm_Document_Dcmnt_Rtntn_Ind.equals("D")))                                                                                                      //Natural: IF NCW-EFM-DOCUMENT.DCMNT-RTNTN-IND = 'D'
        {
            pnd_Ncw_Deleted.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #NCW-DELETED
            //*  DO NOT REPORT ON DELETED MINS.
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  12/1/98
        if (condition(ncw_Efm_Document_Image_Source_Id.equals("POST") || ncw_Efm_Document_Image_Source_Id.equals("PCPOST")))                                              //Natural: IF NCW-EFM-DOCUMENT.IMAGE-SOURCE-ID = 'POST' OR = 'PCPOST'
        {
            pnd_Doc_Entry_Dte.setValueEdited(ncw_Efm_Document_Crte_Dte_Tme,new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED NCW-EFM-DOCUMENT.CRTE-DTE-TME ( EM = YYYYMMDD ) TO #DOC-ENTRY-DTE
            if (condition(pnd_Doc_Entry_Dte_Pnd_Doc_Entry_Dte_N.equals(Global.getDATN())))                                                                                //Natural: IF #DOC-ENTRY-DTE-N = *DATN
            {
                pnd_Ncw_Today.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #NCW-TODAY
                //*  DO NOT REPORT ON POST MINS FOR CURRENT DATE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Image_Source_Id.setValue(ncw_Efm_Document_Image_Source_Id);                                                                                                   //Natural: ASSIGN #IMAGE-SOURCE-ID := NCW-EFM-DOCUMENT.IMAGE-SOURCE-ID
        pnd_Cabinet_Id.setValue(ncw_Efm_Document_Np_Pin);                                                                                                                 //Natural: ASSIGN #CABINET-ID := NCW-EFM-DOCUMENT.NP-PIN
        pnd_Entry_Dte_Tme.setValue(ncw_Efm_Document_Crte_Dte_Tme);                                                                                                        //Natural: ASSIGN #ENTRY-DTE-TME := NCW-EFM-DOCUMENT.CRTE-DTE-TME
        vw_ncw_Master_Index.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) NCW-MASTER-INDEX WITH ACTV-UNQUE-KEY = NCW-EFM-DOCUMENT.RQST-LOG-DTE-TME
        (
        "FIND05",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", "=", ncw_Efm_Document_Rqst_Log_Dte_Tme, WcType.WITH) },
        1
        );
        FIND05:
        while (condition(vw_ncw_Master_Index.readNextRow("FIND05")))
        {
            vw_ncw_Master_Index.setIfNotFoundControlFlag(false);
            pnd_Tiaa_Rcvd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ncw_Master_Index_Rqst_Tiaa_Rcvd_Dte);                                                         //Natural: MOVE EDITED NCW-MASTER-INDEX.RQST-TIAA-RCVD-DTE TO #TIAA-RCVD-DTE ( EM = YYYYMMDD )
            pnd_Case_Workprcss_Multi_Subrqst_Pnd_Case_Id.setValue(ncw_Master_Index_Pnd_Case_Id_Cde);                                                                      //Natural: ASSIGN #CASE-ID := NCW-MASTER-INDEX.#CASE-ID-CDE
            pnd_Case_Workprcss_Multi_Subrqst_Pnd_Work_Prcss_Id.setValue(ncw_Master_Index_Rqst_Work_Prcss_Id);                                                             //Natural: ASSIGN #WORK-PRCSS-ID := NCW-MASTER-INDEX.RQST-WORK-PRCSS-ID
            pnd_Case_Workprcss_Multi_Subrqst_Pnd_Subrqst_Ind.setValue(ncw_Master_Index_Pnd_Subrqst_Id_Cde);                                                               //Natural: ASSIGN #SUBRQST-IND := NCW-MASTER-INDEX.#SUBRQST-ID-CDE
            pnd_Curr_Unit.setValue(ncw_Master_Index_Admin_Unit_Cde);                                                                                                      //Natural: ASSIGN #CURR-UNIT := NCW-MASTER-INDEX.ADMIN-UNIT-CDE
            pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Rqst_Log_Dte_Tme.setValue(ncw_Master_Index_Rqst_Log_Dte_Tme);                                                                //Natural: ASSIGN #ICW-RQST-LOG-DTE-TME := NCW-MASTER-INDEX.RQST-LOG-DTE-TME
            pnd_Icw_Rqst_Routing_Key_Pnd_Icw_Strt_Evnt_Dte_Tme.reset();                                                                                                   //Natural: RESET #ICW-STRT-EVNT-DTE-TME
            vw_ncw_Mit_History.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) NCW-MIT-HISTORY BY RQST-ROUTING-KEY = #ICW-RQST-ROUTING-KEY
            (
            "READ05",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Icw_Rqst_Routing_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
            1
            );
            READ05:
            while (condition(vw_ncw_Mit_History.readNextRow("READ05")))
            {
                pnd_Init_Unit.setValue(ncw_Mit_History_Admin_Unit_Cde);                                                                                                   //Natural: ASSIGN #INIT-UNIT := NCW-MIT-HISTORY.ADMIN-UNIT-CDE
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Document_Key_33_Pnd_Rqst_Log_Dte_Tme.setValue(ncw_Efm_Document_Rqst_Log_Dte_Tme);                                                                             //Natural: ASSIGN #DOCUMENT-KEY-33.#RQST-LOG-DTE-TME := NCW-EFM-DOCUMENT.RQST-LOG-DTE-TME
        pnd_Document_Key_33_Pnd_Crte_Dte_Tme.setValueEdited(ncw_Efm_Document_Crte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                         //Natural: MOVE EDITED NCW-EFM-DOCUMENT.CRTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #DOCUMENT-KEY-33.#CRTE-DTE-TME
        pnd_Document_Key_33_Pnd_Crte_Sqnce_Nbr.setValue(ncw_Efm_Document_Crte_Sqnce_Nbr);                                                                                 //Natural: ASSIGN #DOCUMENT-KEY-33.#CRTE-SQNCE-NBR := NCW-EFM-DOCUMENT.CRTE-SQNCE-NBR
        getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Efsf8713.class));                                                                                //Natural: WRITE ( DOC ) NOHDR USING FORM 'EFSF8713'
        pnd_Doc_Count.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #DOC-COUNT
    }
    private void sub_Report_Cwf_Xref_Mismatch() throws Exception                                                                                                          //Natural: REPORT-CWF-XREF-MISMATCH
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Zero_Pin.resetInitial();                                                                                                                                      //Natural: RESET INITIAL #ZERO-PIN EFSA6110
        pdaEfsa6110.getEfsa6110().resetInitial();
        vw_cwf_Image_Xref.startDatabaseFind                                                                                                                               //Natural: FIND ( 1 ) CWF-IMAGE-XREF WITH SOURCE-ID-MIN-KEY = #SOURCE-ID-MIN-KEY-17
        (
        "FIND06",
        new Wc[] { new Wc("SOURCE_ID_MIN_KEY", "=", pnd_Source_Id_Min_Key_18_Pnd_Source_Id_Min_Key_17, WcType.WITH) },
        1
        );
        FIND06:
        while (condition(vw_cwf_Image_Xref.readNextRow("FIND06", true)))
        {
            vw_cwf_Image_Xref.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Image_Xref.getAstCOUNTER().equals(0)))                                                                                                   //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(cwf_Image_Xref_Ph_Unique_Id_Nbr.greater(getZero())))                                                                                            //Natural: IF CWF-IMAGE-XREF.PH-UNIQUE-ID-NBR GT 0
            {
                pnd_Zero_Pin.setValue(false);                                                                                                                             //Natural: ASSIGN #ZERO-PIN := FALSE
                //*    MOVE EDITED CWF-IMAGE-XREF.PH-UNIQUE-ID-NBR (EM=9999999) TO #PIN-PPG
                //*                                              /*  PIN-EXP>>
                //*   PIN-EXP<<
                pnd_Pin_Ppg.setValueEdited(cwf_Image_Xref_Ph_Unique_Id_Nbr,new ReportEditMask("999999999999"));                                                           //Natural: MOVE EDITED CWF-IMAGE-XREF.PH-UNIQUE-ID-NBR ( EM = 999999999999 ) TO #PIN-PPG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cwf_Image_Xref_Cabinet_Id.greater(" ") && pnd_Prev_Source.notEquals("CIRS")))                                                               //Natural: IF CWF-IMAGE-XREF.CABINET-ID GT ' ' AND #PREV-SOURCE NE 'CIRS'
                {
                    pnd_Zero_Pin.setValue(false);                                                                                                                         //Natural: ASSIGN #ZERO-PIN := FALSE
                    pnd_Pin_Ppg.setValue(cwf_Image_Xref_Cabinet_Id);                                                                                                      //Natural: ASSIGN #PIN-PPG := CWF-IMAGE-XREF.CABINET-ID
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  CONTROL RECORD
        //*  MIN DELETED
        //*  DOCS ARCHIVED
        if (condition(cwf_Image_Xref_Source_Id.equals("CNTRL") || cwf_Image_Xref_Image_Delete_Ind.equals("Y") || cwf_Image_Xref_Archive_Dte_Tme.greater(getZero())))      //Natural: IF CWF-IMAGE-XREF.SOURCE-ID = 'CNTRL' OR CWF-IMAGE-XREF.IMAGE-DELETE-IND = 'Y' OR CWF-IMAGE-XREF.ARCHIVE-DTE-TME GT 0
        {
            pnd_Xref_Deleted.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #XREF-DELETED
            //*  DO NOT REPORT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Pckge_Code.setValue(cwf_Image_Xref_Source_Id);                                                                                                                //Natural: ASSIGN #PCKGE-CODE := CWF-IMAGE-XREF.SOURCE-ID
        if (condition(cwf_Image_Xref_Source_Id.equals("POST") || cwf_Image_Xref_Source_Id.equals("PCPOST")))                                                              //Natural: IF CWF-IMAGE-XREF.SOURCE-ID = 'POST' OR = 'PCPOST'
        {
            pnd_Upld_Entry_Dte.setValueEdited(cwf_Image_Xref_Upld_Date_Time,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED CWF-IMAGE-XREF.UPLD-DATE-TIME ( EM = YYYYMMDD ) TO #UPLD-ENTRY-DTE
            if (condition(pnd_Upld_Entry_Dte_Pnd_Upld_Entry_Dte_N.equals(Global.getDATN())))                                                                              //Natural: IF #UPLD-ENTRY-DTE-N = *DATN
            {
                pnd_Xref_Today.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #XREF-TODAY
                //*  DON'T RPT POST MINS FOR CURRENT DATE.
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  1/15/99
                                                                                                                                                                          //Natural: PERFORM GET-POST-APPLICATION
            sub_Get_Post_Application();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Zero_Pin.getBoolean()))                                                                                                                         //Natural: IF #ZERO-PIN
        {
            pnd_Zero_Pin_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #ZERO-PIN-CNT
            //*  DON't display MIN just report total.
            if (condition(pnd_Prev_Source.equals("CIRS")))                                                                                                                //Natural: IF #PREV-SOURCE = 'CIRS'
            {
                pnd_Xref_Cirs.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #XREF-CIRS
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------ BUILD KEY FOR AUDIT FILE -----    /* 03/19/03  L.E.
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Key_Source_Id.setValue(cwf_Image_Xref_Source_Id);                                                                                 //Natural: MOVE CWF-IMAGE-XREF.SOURCE-ID TO #KEY-SOURCE-ID
        pnd_Batch_Id_Conversion_A.setValue(cwf_Image_Xref_Batch_Id);                                                                                                      //Natural: MOVE CWF-IMAGE-XREF.BATCH-ID TO #BATCH-ID-CONVERSION-A
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Key_All_Batch.setValue(pnd_Batch_Id_Conversion_A_Pnd_Batch_Id_Conversion_N);                                                      //Natural: MOVE #BATCH-ID-CONVERSION-N TO #KEY-ALL-BATCH
        vw_cwf_Upload_Audit.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) CWF-UPLOAD-AUDIT WITH SOURCE-ID-BATCH-NBR-KEY = #SOURCE-ID-BATCH-NBR-KEY
        (
        "F_AUD",
        new Wc[] { new Wc("SOURCE_ID_BATCH_NBR_KEY", "=", pnd_Source_Id_Batch_Nbr_Key, WcType.WITH) },
        1
        );
        F_AUD:
        while (condition(vw_cwf_Upload_Audit.readNextRow("F_AUD", true)))
        {
            vw_cwf_Upload_Audit.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Upload_Audit.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //*         IF BATCH-LABEL-TXT = 'CALLIGO'  /* DO NOT REPORT THESE RECS.
            //*  06/10/03
            if (condition(cwf_Upload_Audit_Batch_Label_Txt.contains ("CALLIGO")))                                                                                         //Natural: IF BATCH-LABEL-TXT = SCAN 'CALLIGO'
            {
                pnd_Call_Cntr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CALL-CNTR
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  F-AUD.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  -------------------------------------
        pnd_Xre_Count.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #XRE-COUNT
        pnd_Identify_Dte_Tme.reset();                                                                                                                                     //Natural: RESET #IDENTIFY-DTE-TME
        if (condition(cwf_Image_Xref_Identify_Dte_Tme.greater(getZero())))                                                                                                //Natural: IF CWF-IMAGE-XREF.IDENTIFY-DTE-TME GT 0
        {
            pnd_Identify_Dte_Tme.setValueEdited(cwf_Image_Xref_Identify_Dte_Tme,new ReportEditMask("MM'/'DD'/'YYYY' 'HH':'II':'SS"));                                     //Natural: MOVE EDITED CWF-IMAGE-XREF.IDENTIFY-DTE-TME ( EM = MM'/'DD'/'YYYY' 'HH':'II':'SS ) TO #IDENTIFY-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        //*  03/19/03
        if (condition(cwf_Image_Xref_Source_Id.equals("POST") || cwf_Image_Xref_Source_Id.equals("PCPOST")))                                                              //Natural: IF CWF-IMAGE-XREF.SOURCE-ID = 'POST' OR = 'PCPOST'
        {
            pdaEfsa6110.getEfsa6110_Parm_Source_Id().setValue(cwf_Image_Xref_Source_Id);                                                                                  //Natural: MOVE CWF-IMAGE-XREF.SOURCE-ID TO EFSA6110.PARM-SOURCE-ID
            pdaEfsa6110.getEfsa6110_Parm_Min().setValue(cwf_Image_Xref_Mail_Item_Nbr);                                                                                    //Natural: MOVE CWF-IMAGE-XREF.MAIL-ITEM-NBR TO EFSA6110.PARM-MIN
            pdaEfsa6110.getEfsa6110_Parm_Cabinet_Id().setValue(cwf_Image_Xref_Cabinet_Id);                                                                                //Natural: MOVE CWF-IMAGE-XREF.CABINET-ID TO EFSA6110.PARM-CABINET-ID
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Doc_Created.setValue(" ");                                                                                                                                //Natural: MOVE ' ' TO #DOC-CREATED
            getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Efsf8710.class));                                                                            //Natural: WRITE ( XRE ) NOHDR USING FORM 'EFSF8710'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  DO NOT CREATE DOCUMENT
        if (condition(pnd_Pckge_Code.equals("POST")))                                                                                                                     //Natural: IF #PCKGE-CODE = 'POST'
        {
            pnd_Doc_Created.setValue(" ");                                                                                                                                //Natural: MOVE ' ' TO #DOC-CREATED
            getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Efsf8710.class));                                                                            //Natural: WRITE ( XRE ) NOHDR USING FORM 'EFSF8710'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Call_Nat.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CALL-NAT
        //*  ----------- CALL SUB-PROGRAM EFSN6130 TO CREATE DOCUMENT ------
        DbsUtil.callnat(Efsn6130.class , getCurrentProcessState(), pdaEfsa6110.getEfsa6110());                                                                            //Natural: CALLNAT 'EFSN6130' EFSA6110
        if (condition(Global.isEscape())) return;
        //*  NO DOCUMENT CREATED
        if (condition(pdaEfsa6110.getEfsa6110_Parm_Return_Code().equals("3")))                                                                                            //Natural: IF EFSA6110.PARM-RETURN-CODE = '3'
        {
            DbsUtil.examine(new ExamineSource(pdaEfsa6110.getEfsa6110_Parm_Min_A()), new ExamineSearch(" "), new ExamineReplace("0"));                                    //Natural: EXAMINE EFSA6110.PARM-MIN-A ' ' REPLACE WITH '0'
            getReports().write(4, ReportOption.NOTITLE,pdaEfsa6110.getEfsa6110_Parm_Err_Msg(),pdaEfsa6110.getEfsa6110_Parm_Source_Id(),new ColumnSpacing(2),              //Natural: WRITE ( MSG ) NOTITLE EFSA6110.PARM-ERR-MSG EFSA6110.PARM-SOURCE-ID 2X EFSA6110.PARM-MIN-A EFSA6110.PARM-SYSTEM EFSA6110.PARM-CABINET-ID
                pdaEfsa6110.getEfsa6110_Parm_Min_A(),pdaEfsa6110.getEfsa6110_Parm_System(),pdaEfsa6110.getEfsa6110_Parm_Cabinet_Id());
            if (Global.isEscape()) return;
            pnd_Err_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #ERR-CNTR
            //*  RECORD HAS ERROR
            pnd_Doc_Created.setValue("E");                                                                                                                                //Natural: MOVE 'E' TO #DOC-CREATED
            getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Efsf8710.class));                                                                            //Natural: WRITE ( XRE ) NOHDR USING FORM 'EFSF8710'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ERROR OCCURED
        if (condition(pdaEfsa6110.getEfsa6110_Parm_Return_Code().equals("2")))                                                                                            //Natural: IF EFSA6110.PARM-RETURN-CODE = '2'
        {
            DbsUtil.examine(new ExamineSource(pdaEfsa6110.getEfsa6110_Parm_Min_A()), new ExamineSearch(" "), new ExamineReplace("0"));                                    //Natural: EXAMINE EFSA6110.PARM-MIN-A ' ' REPLACE WITH '0'
            getReports().write(4, ReportOption.NOTITLE,pdaEfsa6110.getEfsa6110_Parm_Err_Msg(),pdaEfsa6110.getEfsa6110_Parm_Source_Id(),new ColumnSpacing(2),              //Natural: WRITE ( MSG ) NOTITLE EFSA6110.PARM-ERR-MSG EFSA6110.PARM-SOURCE-ID 2X EFSA6110.PARM-MIN-A EFSA6110.PARM-SYSTEM EFSA6110.PARM-CABINET-ID
                pdaEfsa6110.getEfsa6110_Parm_Min_A(),pdaEfsa6110.getEfsa6110_Parm_System(),pdaEfsa6110.getEfsa6110_Parm_Cabinet_Id());
            if (Global.isEscape()) return;
            pnd_Err_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #ERR-CNTR
            //*  RECORD HAS ERROR
            pnd_Doc_Created.setValue("E");                                                                                                                                //Natural: MOVE 'E' TO #DOC-CREATED
            getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Efsf8710.class));                                                                            //Natural: WRITE ( XRE ) NOHDR USING FORM 'EFSF8710'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  SUCCESFULL CREATION
        if (condition(pdaEfsa6110.getEfsa6110_Parm_Return_Code().equals("1")))                                                                                            //Natural: IF EFSA6110.PARM-RETURN-CODE = '1'
        {
            pnd_Xre_Doc_Created.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #XRE-DOC-CREATED
            //*  PHYSICAL UPDATE
            if (condition(cwf_Support_Tbl_Et_On_Off_Flag.equals("Y")))                                                                                                    //Natural: IF CWF-SUPPORT-TBL.ET-ON-OFF-FLAG = 'Y'
            {
                //*  DOCUMENT CREATED
                pnd_Doc_Created.setValue("C");                                                                                                                            //Natural: MOVE 'C' TO #DOC-CREATED
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  DOCUMENT AKNOWELEDGE
                pnd_Doc_Created.setValue("M");                                                                                                                            //Natural: MOVE 'M' TO #DOC-CREATED
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaEfsa6110.getEfsa6110_Parm_System().equals("CWF")))                                                                                           //Natural: IF EFSA6110.PARM-SYSTEM = 'CWF'
            {
                pnd_Xre_Doc_Cwf.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #XRE-DOC-CWF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaEfsa6110.getEfsa6110_Parm_System().equals("ICW")))                                                                                           //Natural: IF EFSA6110.PARM-SYSTEM = 'ICW'
            {
                pnd_Xre_Doc_Icw.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #XRE-DOC-ICW
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaEfsa6110.getEfsa6110_Parm_System().equals("NCW")))                                                                                           //Natural: IF EFSA6110.PARM-SYSTEM = 'NCW'
            {
                pnd_Xre_Doc_Ncw.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #XRE-DOC-NCW
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Efsf8710.class));                                                                            //Natural: WRITE ( XRE ) NOHDR USING FORM 'EFSF8710'
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  NEW - ADDED 1/15/99
    private void sub_Get_Post_Application() throws Exception                                                                                                              //Natural: GET-POST-APPLICATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_pst_Outgoing_Mail_Item.startDatabaseFind                                                                                                                       //Natural: FIND ( 1 ) PST-OUTGOING-MAIL-ITEM WITH IMAGE-SRCE-ID-ID = #SOURCE-ID-MIN-KEY-17
        (
        "FIND07",
        new Wc[] { new Wc("IMAGE_SRCE_ID_ID", "=", pnd_Source_Id_Min_Key_18_Pnd_Source_Id_Min_Key_17, WcType.WITH) },
        1
        );
        FIND07:
        while (condition(vw_pst_Outgoing_Mail_Item.readNextRow("FIND07", true)))
        {
            vw_pst_Outgoing_Mail_Item.setIfNotFoundControlFlag(false);
            if (condition(vw_pst_Outgoing_Mail_Item.getAstCOUNTER().equals(0)))                                                                                           //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE IMMEDIATE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Pckge_Code.setValue(pst_Outgoing_Mail_Item_Pckge_Cde);                                                                                                    //Natural: ASSIGN #PCKGE-CODE := PST-OUTGOING-MAIL-ITEM.PCKGE-CDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Print_Xre_Source_Total() throws Exception                                                                                                            //Natural: PRINT-XRE-SOURCE-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Xre_Count.greater(getZero()) || pnd_Zero_Pin_Cnt.greater(getZero())))                                                                           //Natural: IF #XRE-COUNT GT 0 OR #ZERO-PIN-CNT GT 0
        {
            getReports().skip(0, 2);                                                                                                                                      //Natural: SKIP ( XRE ) 2 LINES
            getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Efsf8705.class));                                                                            //Natural: WRITE ( XRE ) NOHDR USING FORM 'EFSF8705'
            if (condition(pnd_Prev_Source.equals("CIRS")))                                                                                                                //Natural: IF #PREV-SOURCE = 'CIRS'
            {
                getReports().skip(0, 2);                                                                                                                                  //Natural: SKIP ( XRE ) 2 LINES
                getReports().write(2, ReportOption.NOTITLE,"*** MINs for Unidentifieds not printed for CIRS ***");                                                        //Natural: WRITE ( XRE ) NOTITLE '*** MINs for Unidentifieds not printed for CIRS ***'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Zero_Pin_All_Cnt.nadd(pnd_Zero_Pin_Cnt);                                                                                                                  //Natural: ADD #ZERO-PIN-CNT TO #ZERO-PIN-ALL-CNT
            pnd_Xre_All_Count.nadd(pnd_Xre_Count);                                                                                                                        //Natural: ADD #XRE-COUNT TO #XRE-ALL-COUNT
            getReports().getPageNumberDbs(2).reset();                                                                                                                     //Natural: RESET *PAGE-NUMBER ( XRE ) #XRE-COUNT #ZERO-PIN-CNT
            pnd_Xre_Count.reset();
            pnd_Zero_Pin_Cnt.reset();
            //*  WRITE (XRE) /'Before NEW PAGE...POST01..'
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( XRE )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Doc_Source_Total() throws Exception                                                                                                            //Natural: PRINT-DOC-SOURCE-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Doc_Count.greater(getZero())))                                                                                                                  //Natural: IF #DOC-COUNT GT 0
        {
            getReports().skip(0, 2);                                                                                                                                      //Natural: SKIP ( DOC ) 2 LINES
            getReports().write(3, ReportOption.NOHDR, writeMapToStringOutput(Efsf8706.class));                                                                            //Natural: WRITE ( DOC ) NOHDR USING FORM 'EFSF8706'
            getReports().getPageNumberDbs(3).reset();                                                                                                                     //Natural: RESET *PAGE-NUMBER ( DOC ) #DOC-COUNT
            pnd_Doc_Count.reset();
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( DOC )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Totals() throws Exception                                                                                                                      //Natural: PRINT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  AT THE END OF THE REPORT, PRINT SUMMARY BOX FOR EACH REPORT
        //*  WRITE (XRE) /'before new page...post02..'
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( XRE )
        if (condition(Global.isEscape())){return;}
        if (condition(pnd_Xre_All_Count.greater(getZero())))                                                                                                              //Natural: IF #XRE-ALL-COUNT GT 0
        {
            getReports().skip(0, 2);                                                                                                                                      //Natural: SKIP ( XRE ) 2 LINES
            getReports().write(2, NEWLINE,"Number of XREF with Zero PINs            = ",pnd_Zero_Pin_All_Cnt,NEWLINE,"Number of XREF Records Without Documents = ",       //Natural: WRITE ( XRE ) /'Number of XREF with Zero PINs            = ' #ZERO-PIN-ALL-CNT /'Number of XREF Records Without Documents = ' #XRE-ALL-COUNT /'Number of XREF Records Marked for Delete = ' #XREF-DELETED /'Number of XREF Records Uploaded Today    = ' #XREF-TODAY /'Number of XREF-CIRS Records Skipped      = ' #XREF-CIRS /'Number of Documents Created for (CWF)    = ' #XRE-DOC-CWF /'Number of CWF Records Marked for Delete  = ' #CWF-DELETED /'Number of CWF  Records Uploaded Today    = ' #CWF-TODAY /'Number of Documents Created for (ICW)    = ' #XRE-DOC-ICW /'Number of ICW Records Marked for Delete  = ' #ICW-DELETED /'Number of ICW  Records Uploaded Today    = ' #ICW-TODAY /'Number of Documents Created for (NCW)    = ' #XRE-DOC-NCW /'Number of NCW Records Marked for Delete  = ' #NCW-DELETED /'Number of NCW  Records Uploaded Today    = ' #NCW-TODAY /'Number of "CALLIGO" records rejected     = ' #CALL-CNTR /'****************************************** ' /'Total  of Documents Created              = ' #XRE-DOC-CREATED
                pnd_Xre_All_Count,NEWLINE,"Number of XREF Records Marked for Delete = ",pnd_Xref_Deleted,NEWLINE,"Number of XREF Records Uploaded Today    = ",
                pnd_Xref_Today,NEWLINE,"Number of XREF-CIRS Records Skipped      = ",pnd_Xref_Cirs,NEWLINE,"Number of Documents Created for (CWF)    = ",
                pnd_Xre_Doc_Cwf,NEWLINE,"Number of CWF Records Marked for Delete  = ",pnd_Cwf_Deleted,NEWLINE,"Number of CWF  Records Uploaded Today    = ",
                pnd_Cwf_Today,NEWLINE,"Number of Documents Created for (ICW)    = ",pnd_Xre_Doc_Icw,NEWLINE,"Number of ICW Records Marked for Delete  = ",
                pnd_Icw_Deleted,NEWLINE,"Number of ICW  Records Uploaded Today    = ",pnd_Icw_Today,NEWLINE,"Number of Documents Created for (NCW)    = ",
                pnd_Xre_Doc_Ncw,NEWLINE,"Number of NCW Records Marked for Delete  = ",pnd_Ncw_Deleted,NEWLINE,"Number of NCW  Records Uploaded Today    = ",
                pnd_Ncw_Today,NEWLINE,"Number of 'CALLIGO' records rejected     = ",pnd_Call_Cntr,NEWLINE,"****************************************** ",
                NEWLINE,"Total  of Documents Created              = ",pnd_Xre_Doc_Created);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Err_Cntr.greater(getZero())))                                                                                                                   //Natural: IF #ERR-CNTR GT 0
        {
            getReports().skip(0, 2);                                                                                                                                      //Natural: SKIP ( MSG ) 2 LINES
            getReports().write(4, NEWLINE,"NUMBER OF ERRORS                 = ",pnd_Err_Cntr);                                                                            //Natural: WRITE ( MSG ) /'NUMBER OF ERRORS                 = ' #ERR-CNTR
            if (Global.isEscape()) return;
            getReports().getPageNumberDbs(4).reset();                                                                                                                     //Natural: RESET *PAGE-NUMBER ( MSG )
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( MSG )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Read_Run_Control_Record() throws Exception                                                                                                           //Natural: READ-RUN-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  =======================================
        //*  --------- SET THE KEY -----------------
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: MOVE 'A' TO #TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("XREF-DOC-RECONCIL");                                                                                                //Natural: MOVE 'XREF-DOC-RECONCIL' TO #TBL-TABLE-NME
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("RECONCILIATION EXTRACT");                                                                                           //Natural: MOVE 'RECONCILIATION EXTRACT' TO #TBL-KEY-FIELD
        //*  ------------------------------------------
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ06",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ06:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ06")))
        {
            pnd_Isn_Control.setValue(vw_cwf_Support_Tbl.getAstISN("Read06"));                                                                                             //Natural: ASSIGN #ISN-CONTROL := *ISN
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Update_Control_Record() throws Exception                                                                                                             //Natural: UPDATE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        PND_PND_L5105:                                                                                                                                                    //Natural: GET CWF-SUPPORT-TBL #ISN-CONTROL
        vw_cwf_Support_Tbl.readByID(pnd_Isn_Control.getLong(), "PND_PND_L5105");
        if (condition(cwf_Support_Tbl_Run_Indicator.equals("N")))                                                                                                         //Natural: IF RUN-INDICATOR = 'N'
        {
            cwf_Support_Tbl_Normal_Start_Dte_Tme.setValue(cwf_Support_Tbl_Calc_Start_Dte_Tme);                                                                            //Natural: MOVE CWF-SUPPORT-TBL.CALC-START-DTE-TME TO CWF-SUPPORT-TBL.NORMAL-START-DTE-TME
            cwf_Support_Tbl_Normal_End_Dte_Tme.setValue(cwf_Support_Tbl_Calc_End_Dte_Tme);                                                                                //Natural: MOVE CWF-SUPPORT-TBL.CALC-END-DTE-TME TO CWF-SUPPORT-TBL.NORMAL-END-DTE-TME
            cwf_Support_Tbl_Normal_Record_Count.setValue(pnd_Xre_Doc_Created);                                                                                            //Natural: MOVE #XRE-DOC-CREATED TO NORMAL-RECORD-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Support_Tbl_Run_Indicator.equals("S")))                                                                                                         //Natural: IF RUN-INDICATOR = 'S'
        {
            cwf_Support_Tbl_Special_Record_Count.setValue(pnd_Xre_Doc_Created);                                                                                           //Natural: MOVE #XRE-DOC-CREATED TO SPECIAL-RECORD-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        vw_cwf_Support_Tbl.updateDBRow("PND_PND_L5105");                                                                                                                  //Natural: UPDATE ( ##L5105. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf8711.class));                                              //Natural: WRITE ( XRE ) NOTITLE NOHDR USING FORM 'EFSF8711'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf8712.class));                                              //Natural: WRITE ( DOC ) NOTITLE NOHDR USING FORM 'EFSF8712'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf8709.class));                                              //Natural: WRITE ( MSG ) NOTITLE NOHDR USING FORM 'EFSF8709'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=OFF");
        Global.format(3, "LS=133 PS=60 ZP=OFF");
        Global.format(4, "LS=133 PS=60 ZP=ON");
    }
}
