/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:12:15 PM
**        * FROM NATURAL PROGRAM : Efsp0600
************************************************************
**        * FILE NAME            : Efsp0600.java
**        * CLASS NAME           : Efsp0600
**        * INSTANCE NAME        : Efsp0600
************************************************************
************************************************************************
* PROGRAM  : EFSP0600
* SYSTEM   : CRPCWF
* TITLE    : BATCH DRIVER PROGRAM FOR IMAGING REPORTS
* WRITTEN  : 11/27/1995
* FUNCTION : THIS IS A BATCH DRIVER PROGRAM FOR IMAGING REPORTS.
*            THIS PROGRAM SHOULD RUN 5 REPORT PROGRAMS NAMELY:
*
*              1. EFSP0060 - IMAGE UPLOAD DAILY REPORT (BY BATCH NO.)
*              2. EFSP0061 - IMAGE UPLOAD DAILY REPORT (BY UPLD DTE/TME)
*              3. EFSP0062 - IMAGE UPLOAD EXCEPTION REPORT
*              4. EFSP0063 - IMAGE UPLOAD SUMMARY REPORT (MTD)
*              5. EFSP0064 - UNIDENTIFIED MINS/ERROR REPORT
*              6. EFSP0065 - DOCUMENT AND XREF CONFLICT REPORT
*              8. EFSP7060 - DOCUMENT AND XREF CONFLICT REPORT
*              9. EFSP7061 - DOCUMENT AND XREF CONFLICT REPORT
*             10. EFSP7063 - DOCUMENT AND XREF CONFLICT REPORT
*
* HISTORY
* 02/08/96  BE  CHANGED PROGRAM TO USE PREVIOUS DATE PARM IF INPUT DATE
*               IS 'GE' TO CURRENT DATE.
* 09/24/96  BE  NEW REPORT FOR DOCUMENT AND XREF CONFLICT REPORT.
* 10/10/00  MAK NEW REPORT FOR 'PIM
* 01/22/01  MAK NEW REPORT FOR ALL SOURCES
*
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsp0600 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Parms;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Time_Start;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Time_End;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Date;
    private DbsField cwf_Support_Tbl_Pnd_Override_Time_Parms;

    private DbsGroup cwf_Support_Tbl__R_Field_3;
    private DbsField cwf_Support_Tbl_Pnd_Override_Time_Start;
    private DbsField cwf_Support_Tbl_Pnd_Override_Time_End;
    private DbsField cwf_Support_Tbl_Pnd_Override_Flag;
    private DbsField cwf_Support_Tbl_Pnd_Prior_Date_Parm;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_4;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Input_Data;

    private DbsGroup pnd_Input_Data__R_Field_5;
    private DbsField pnd_Input_Data_Pnd_Input_Time_Start;
    private DbsField pnd_Input_Data_Pnd_Input_Time_End;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Source_Id;
    private DbsField pnd_Input_Date_D;
    private DbsField pnd_Last_Processed_Date_A;
    private DbsField pnd_Last_Processed_Date;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Orig_Date;

    private DbsGroup pnd_Orig_Date__R_Field_6;
    private DbsField pnd_Orig_Date_Pnd_Orig_Date_Yyyy;
    private DbsField pnd_Orig_Date_Pnd_Orig_Date_Mm;
    private DbsField pnd_Orig_Date_Pnd_Orig_Date_Dd;
    private DbsField pnd_Last_Processed_Date_Mm;
    private DbsField pnd_New_Mm;
    private DbsField pnd_Report_Ran;
    private DbsField pnd_Successful;
    private DbsField pnd_Prior_Date_Parm_Used;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Regular_Run_Parms = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Parms", "#REGULAR-RUN-PARMS", 
            FieldType.STRING, 20);

        cwf_Support_Tbl__R_Field_2 = cwf_Support_Tbl__R_Field_1.newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Pnd_Regular_Run_Parms);
        cwf_Support_Tbl_Pnd_Regular_Time_Start = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Time_Start", "#REGULAR-TIME-START", 
            FieldType.STRING, 6);
        cwf_Support_Tbl_Pnd_Regular_Time_End = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Time_End", "#REGULAR-TIME-END", 
            FieldType.STRING, 6);
        cwf_Support_Tbl_Pnd_Regular_Run_Date = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Date", "#REGULAR-RUN-DATE", 
            FieldType.STRING, 8);
        cwf_Support_Tbl_Pnd_Override_Time_Parms = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Time_Parms", "#OVERRIDE-TIME-PARMS", 
            FieldType.STRING, 12);

        cwf_Support_Tbl__R_Field_3 = cwf_Support_Tbl__R_Field_1.newGroupInGroup("cwf_Support_Tbl__R_Field_3", "REDEFINE", cwf_Support_Tbl_Pnd_Override_Time_Parms);
        cwf_Support_Tbl_Pnd_Override_Time_Start = cwf_Support_Tbl__R_Field_3.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Time_Start", "#OVERRIDE-TIME-START", 
            FieldType.STRING, 6);
        cwf_Support_Tbl_Pnd_Override_Time_End = cwf_Support_Tbl__R_Field_3.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Time_End", "#OVERRIDE-TIME-END", 
            FieldType.STRING, 6);
        cwf_Support_Tbl_Pnd_Override_Flag = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Flag", "#OVERRIDE-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Pnd_Prior_Date_Parm = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Prior_Date_Parm", "#PRIOR-DATE-PARM", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_4", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Input_Data = localVariables.newFieldInRecord("pnd_Input_Data", "#INPUT-DATA", FieldType.STRING, 60);

        pnd_Input_Data__R_Field_5 = localVariables.newGroupInRecord("pnd_Input_Data__R_Field_5", "REDEFINE", pnd_Input_Data);
        pnd_Input_Data_Pnd_Input_Time_Start = pnd_Input_Data__R_Field_5.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_Start", "#INPUT-TIME-START", FieldType.STRING, 
            6);
        pnd_Input_Data_Pnd_Input_Time_End = pnd_Input_Data__R_Field_5.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_End", "#INPUT-TIME-END", FieldType.STRING, 
            6);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data__R_Field_5.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Source_Id = localVariables.newFieldInRecord("pnd_Input_Source_Id", "#INPUT-SOURCE-ID", FieldType.STRING, 6);
        pnd_Input_Date_D = localVariables.newFieldInRecord("pnd_Input_Date_D", "#INPUT-DATE-D", FieldType.DATE);
        pnd_Last_Processed_Date_A = localVariables.newFieldInRecord("pnd_Last_Processed_Date_A", "#LAST-PROCESSED-DATE-A", FieldType.STRING, 8);
        pnd_Last_Processed_Date = localVariables.newFieldInRecord("pnd_Last_Processed_Date", "#LAST-PROCESSED-DATE", FieldType.DATE);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 10);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Orig_Date = localVariables.newFieldInRecord("pnd_Orig_Date", "#ORIG-DATE", FieldType.STRING, 8);

        pnd_Orig_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Orig_Date__R_Field_6", "REDEFINE", pnd_Orig_Date);
        pnd_Orig_Date_Pnd_Orig_Date_Yyyy = pnd_Orig_Date__R_Field_6.newFieldInGroup("pnd_Orig_Date_Pnd_Orig_Date_Yyyy", "#ORIG-DATE-YYYY", FieldType.STRING, 
            4);
        pnd_Orig_Date_Pnd_Orig_Date_Mm = pnd_Orig_Date__R_Field_6.newFieldInGroup("pnd_Orig_Date_Pnd_Orig_Date_Mm", "#ORIG-DATE-MM", FieldType.STRING, 
            2);
        pnd_Orig_Date_Pnd_Orig_Date_Dd = pnd_Orig_Date__R_Field_6.newFieldInGroup("pnd_Orig_Date_Pnd_Orig_Date_Dd", "#ORIG-DATE-DD", FieldType.STRING, 
            2);
        pnd_Last_Processed_Date_Mm = localVariables.newFieldInRecord("pnd_Last_Processed_Date_Mm", "#LAST-PROCESSED-DATE-MM", FieldType.STRING, 2);
        pnd_New_Mm = localVariables.newFieldInRecord("pnd_New_Mm", "#NEW-MM", FieldType.STRING, 2);
        pnd_Report_Ran = localVariables.newFieldInRecord("pnd_Report_Ran", "#REPORT-RAN", FieldType.BOOLEAN, 1);
        pnd_Successful = localVariables.newFieldInRecord("pnd_Successful", "#SUCCESSFUL", FieldType.BOOLEAN, 1);
        pnd_Prior_Date_Parm_Used = localVariables.newFieldInRecord("pnd_Prior_Date_Parm_Used", "#PRIOR-DATE-PARM-USED", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Report_Ran.setInitialValue(false);
        pnd_Successful.setInitialValue(false);
        pnd_Prior_Date_Parm_Used.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Efsp0600() throws Exception
    {
        super("Efsp0600");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Efsp0600|Main");
        setupReports();
        while(true)
        {
            try
            {
                getReports().definePrinter(2, "AUDIT");                                                                                                                   //Natural: DEFINE PRINTER ( AUDIT = 1 )
                //*                                                                                                                                                       //Natural: FORMAT ( AUDIT ) LS = 133 PS = 60 ZP = OFF
                //*  INPUT THE SOURCE ID (PARAMETER FROM THE JCL)
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Source_Id);                                                                                  //Natural: INPUT #INPUT-SOURCE-ID
                //*  FOR IMAGE UPLOAD THE DAY's work is from the time on the UPLD-PROCESS-
                //*  TIME TABLE.
                pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                  //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND := 'A'
                pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("IMG-PROCESS-DTE-TME");                                                                                      //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME := 'IMG-PROCESS-DTE-TME'
                pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue(pnd_Input_Source_Id);                                                                                        //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-KEY-FIELD := #INPUT-SOURCE-ID
                vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                      //Natural: FIND ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
                (
                "FIND_TBL",
                new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
                1
                );
                FIND_TBL:
                while (condition(vw_cwf_Support_Tbl.readNextRow("FIND_TBL")))
                {
                    vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
                    pnd_Input_Data.setValue(cwf_Support_Tbl_Pnd_Regular_Run_Parms);                                                                                       //Natural: ASSIGN #INPUT-DATA := CWF-SUPPORT-TBL.#REGULAR-RUN-PARMS
                    pnd_Isn_Tbl.setValue(vw_cwf_Support_Tbl.getAstISN("FIND_TBL"));                                                                                       //Natural: ASSIGN #ISN-TBL := *ISN ( FIND-TBL. )
                    if (condition(cwf_Support_Tbl_Pnd_Override_Flag.equals("Y") && cwf_Support_Tbl_Pnd_Override_Time_Parms.greater(" ")))                                 //Natural: IF #OVERRIDE-FLAG = 'Y' AND #OVERRIDE-TIME-PARMS GT ' '
                    {
                        cwf_Support_Tbl_Pnd_Regular_Time_Start.setValue(cwf_Support_Tbl_Pnd_Override_Time_Start);                                                         //Natural: ASSIGN #REGULAR-TIME-START := #INPUT-TIME-START := #OVERRIDE-TIME-START
                        pnd_Input_Data_Pnd_Input_Time_Start.setValue(cwf_Support_Tbl_Pnd_Override_Time_Start);
                        cwf_Support_Tbl_Pnd_Regular_Time_End.setValue(cwf_Support_Tbl_Pnd_Override_Time_End);                                                             //Natural: ASSIGN #REGULAR-TIME-END := #INPUT-TIME-END := #OVERRIDE-TIME-END
                        pnd_Input_Data_Pnd_Input_Time_End.setValue(cwf_Support_Tbl_Pnd_Override_Time_End);
                        cwf_Support_Tbl_Pnd_Override_Flag.reset();                                                                                                        //Natural: RESET #OVERRIDE-FLAG #OVERRIDE-TIME-PARMS
                        cwf_Support_Tbl_Pnd_Override_Time_Parms.reset();
                        vw_cwf_Support_Tbl.updateDBRow("FIND_TBL");                                                                                                       //Natural: UPDATE ( FIND-TBL. )
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
                short decideConditionsMet111 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #INPUT-DATA = ' '
                if (condition(pnd_Input_Data.equals(" ")))
                {
                    decideConditionsMet111++;
                    getReports().write(2, "*** ERROR IN TRYING TO FIND PROCESSING TIME ***");                                                                             //Natural: WRITE ( AUDIT ) '*** ERROR IN TRYING TO FIND PROCESSING TIME ***'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN #INPUT-DATE NE MASK ( 19-20YYMMDD )
                else if (condition(! ((DbsUtil.maskMatches(pnd_Input_Data_Pnd_Input_Date,"19-20YYMMDD")))))
                {
                    decideConditionsMet111++;
                    getReports().write(2, NEWLINE,NEWLINE,new TabSetting(30),"*",new RepeatItem(70),NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new     //Natural: WRITE ( AUDIT ) // 30T '*' ( 70 ) / 30T '*' 99T '*' / 30T '* INVALID INPUT DATE (YYYYMMDD) =' #INPUT-DATE 99T '*' / 30T '*' 99T '*' / 30T '*' ( 70 )
                        TabSetting(30),"* INVALID INPUT DATE (YYYYMMDD) =",pnd_Input_Data_Pnd_Input_Date,new TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new 
                        TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new RepeatItem(70));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN #INPUT-TIME-START NE MASK ( HH00:59 ) OR #INPUT-TIME-START NE MASK ( ....00:59 )
                else if (condition(! ((DbsUtil.maskMatches(pnd_Input_Data_Pnd_Input_Time_Start,"HH00:59")) || ! (DbsUtil.maskMatches(pnd_Input_Data_Pnd_Input_Time_Start,
                    "....00:59")))))
                {
                    decideConditionsMet111++;
                    getReports().write(1, NEWLINE,NEWLINE,new TabSetting(30),"*",new RepeatItem(70),NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new     //Natural: WRITE ( 1 ) // 30T '*' ( 70 ) / 30T '*' 99T '*' / 30T '* INVALID START TIME (HHIISS) =' #INPUT-TIME-START 99T '*' / 30T '*' 99T '*' / 30T '*' ( 70 )
                        TabSetting(30),"* INVALID START TIME (HHIISS) =",pnd_Input_Data_Pnd_Input_Time_Start,new TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new 
                        TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new RepeatItem(70));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN #INPUT-TIME-END NE MASK ( HH00:59 ) OR #INPUT-TIME-END NE MASK ( ....00:59 )
                else if (condition(! ((DbsUtil.maskMatches(pnd_Input_Data_Pnd_Input_Time_End,"HH00:59")) || ! (DbsUtil.maskMatches(pnd_Input_Data_Pnd_Input_Time_End,
                    "....00:59")))))
                {
                    decideConditionsMet111++;
                    getReports().write(2, NEWLINE,NEWLINE,new TabSetting(30),"*",new RepeatItem(70),NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new     //Natural: WRITE ( AUDIT ) // 30T '*' ( 70 ) / 30T '*' 99T '*' / 30T '* INVALID END TIME (HHIISS) =' #INPUT-TIME-END 99T '*' / 30T '*' 99T '*' / 30T '*' ( 70 )
                        TabSetting(30),"* INVALID END TIME (HHIISS) =",pnd_Input_Data_Pnd_Input_Time_End,new TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new 
                        TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new RepeatItem(70));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet111 > 0))
                {
                    DbsUtil.stop();  if (true) return;                                                                                                                    //Natural: STOP
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  IF #INPUT-DATE GE CURRENT DATE IT MEANS THE JOB IS BEING RERUN.
                //*  PROGRAM WILL THEN HAVE TO USE THE PRIOR DATE PARM.
                pnd_Input_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                            //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DATE-D ( EM = YYYYMMDD )
                if (condition(pnd_Input_Date_D.greaterOrEqual(Global.getDATX()) && cwf_Support_Tbl_Pnd_Prior_Date_Parm.greater(" ")))                                     //Natural: IF #INPUT-DATE-D GE *DATX AND #PRIOR-DATE-PARM GT ' '
                {
                    pnd_Input_Data_Pnd_Input_Date.setValue(cwf_Support_Tbl_Pnd_Prior_Date_Parm);                                                                          //Natural: ASSIGN #INPUT-DATE := #PRIOR-DATE-PARM
                    pnd_Prior_Date_Parm_Used.setValue(true);                                                                                                              //Natural: ASSIGN #PRIOR-DATE-PARM-USED := TRUE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Orig_Date.setValue(pnd_Input_Data_Pnd_Input_Date);                                                                                                    //Natural: ASSIGN #ORIG-DATE := #INPUT-DATE
                REPEAT01:                                                                                                                                                 //Natural: REPEAT
                while (condition(whileTrue))
                {
                    pnd_Input_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                        //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DATE-D ( EM = YYYYMMDD )
                    if (condition(pnd_Input_Date_D.greaterOrEqual(Global.getDATX())))                                                                                     //Natural: IF #INPUT-DATE-D GE *DATX
                    {
                        if (condition(pnd_Report_Ran.getBoolean()))                                                                                                       //Natural: IF #REPORT-RAN
                        {
                            pnd_Successful.setValue(true);                                                                                                                //Natural: ASSIGN #SUCCESSFUL := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Source_Id, pnd_Input_Data);                                                                     //Natural: FETCH RETURN 'EFSP0060' #INPUT-SOURCE-ID #INPUT-DATA
                    DbsUtil.invokeMain(DbsUtil.getBlType("EFSP0060"), getCurrentProcessState());
                    if (condition(Global.isEscape())) return;
                    //*  PREVENTS TIME OUT (NAT3009)
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Source_Id, pnd_Input_Data);                                                                     //Natural: FETCH RETURN 'EFSP0061' #INPUT-SOURCE-ID #INPUT-DATA
                    DbsUtil.invokeMain(DbsUtil.getBlType("EFSP0061"), getCurrentProcessState());
                    if (condition(Global.isEscape())) return;
                    //*  PREVENTS TIME OUT (NAT3009)
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Source_Id, pnd_Input_Data);                                                                     //Natural: FETCH RETURN 'EFSP0062' #INPUT-SOURCE-ID #INPUT-DATA
                    DbsUtil.invokeMain(DbsUtil.getBlType("EFSP0062"), getCurrentProcessState());
                    if (condition(Global.isEscape())) return;
                    //*  PREVENTS TIME OUT (NAT3009)
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Source_Id, pnd_Input_Data);                                                                     //Natural: FETCH RETURN 'EFSP0063' #INPUT-SOURCE-ID #INPUT-DATA
                    DbsUtil.invokeMain(DbsUtil.getBlType("EFSP0063"), getCurrentProcessState());
                    if (condition(Global.isEscape())) return;
                    //*  PREVENTS TIME OUT (NAT3009)
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Source_Id, pnd_Input_Data);                                                                     //Natural: FETCH RETURN 'EFSP0064' #INPUT-SOURCE-ID #INPUT-DATA
                    DbsUtil.invokeMain(DbsUtil.getBlType("EFSP0064"), getCurrentProcessState());
                    if (condition(Global.isEscape())) return;
                    //*  PREVENTS TIME OUT (NAT3009)
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Source_Id, pnd_Input_Data);                                                                     //Natural: FETCH RETURN 'EFSP0065' #INPUT-SOURCE-ID #INPUT-DATA
                    DbsUtil.invokeMain(DbsUtil.getBlType("EFSP0065"), getCurrentProcessState());
                    if (condition(Global.isEscape())) return;
                    //*  PREVENTS TIME OUT (NAT3009)
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Source_Id, pnd_Input_Data);                                                                     //Natural: FETCH RETURN 'EFSP7060' #INPUT-SOURCE-ID #INPUT-DATA
                    DbsUtil.invokeMain(DbsUtil.getBlType("EFSP7060"), getCurrentProcessState());
                    if (condition(Global.isEscape())) return;
                    //*  PREVENTS TIME OUT (NAT3009)
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Source_Id, pnd_Input_Data);                                                                     //Natural: FETCH RETURN 'EFSP7061' #INPUT-SOURCE-ID #INPUT-DATA
                    DbsUtil.invokeMain(DbsUtil.getBlType("EFSP7061"), getCurrentProcessState());
                    if (condition(Global.isEscape())) return;
                    //*  PREVENTS TIME OUT (NAT3009)
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Source_Id, pnd_Input_Data);                                                                     //Natural: FETCH RETURN 'EFSP7063' #INPUT-SOURCE-ID #INPUT-DATA
                    DbsUtil.invokeMain(DbsUtil.getBlType("EFSP7063"), getCurrentProcessState());
                    if (condition(Global.isEscape())) return;
                    //*  PREVENTS TIME OUT (NAT3009)
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    pnd_Report_Ran.setValue(true);                                                                                                                        //Natural: ASSIGN #REPORT-RAN := TRUE
                    pnd_Last_Processed_Date_A.setValue(pnd_Input_Data_Pnd_Input_Date);                                                                                    //Natural: ASSIGN #LAST-PROCESSED-DATE-A := #INPUT-DATE
                    pnd_Input_Date_D.nadd(1);                                                                                                                             //Natural: ADD 1 TO #INPUT-DATE-D
                    pnd_Input_Data_Pnd_Input_Date.setValueEdited(pnd_Input_Date_D,new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED #INPUT-DATE-D ( EM = YYYYMMDD ) TO #INPUT-DATE
                }                                                                                                                                                         //Natural: END-REPEAT
                if (Global.isEscape()) return;
                //*  REPORT PROGRAMS WERE EXECUTED.
                if (condition(pnd_Successful.getBoolean()))                                                                                                               //Natural: IF #SUCCESSFUL
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-NEXT-PROCESSING-DATE
                    sub_Update_Next_Processing_Date();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-NEXT-PROCESSING-DATE
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Update_Next_Processing_Date() throws Exception                                                                                                       //Natural: UPDATE-NEXT-PROCESSING-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  UPDATE TABLE FILE WITH THE NEXT PROCESSING DATE; ALSO IF FIRST
        //*  WORKING DAY OF THE MONTH, UPDATE THE REGULAR TIME PARAMETERS
        //*  WITH AN OVERRIDE TIME PARAMETERS IF IT IS ENTERED.
        if (condition(pnd_Prior_Date_Parm_Used.getBoolean()))                                                                                                             //Natural: IF #PRIOR-DATE-PARM-USED
        {
            //*  DON't update the table
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Last_Processed_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Last_Processed_Date_A);                                                                 //Natural: MOVE EDITED #LAST-PROCESSED-DATE-A TO #LAST-PROCESSED-DATE ( EM = YYYYMMDD )
        pnd_Last_Processed_Date_Mm.setValueEdited(pnd_Last_Processed_Date,new ReportEditMask("MM"));                                                                      //Natural: MOVE EDITED #LAST-PROCESSED-DATE ( EM = MM ) TO #LAST-PROCESSED-DATE-MM
        L_GET:                                                                                                                                                            //Natural: GET CWF-SUPPORT-TBL #ISN-TBL
        vw_cwf_Support_Tbl.readByID(pnd_Isn_Tbl.getLong(), "L_GET");
        DbsUtil.callnat(Efsn6000.class , getCurrentProcessState(), pnd_Last_Processed_Date, pnd_Next_Processing_Date);                                                    //Natural: CALLNAT 'EFSN6000' #LAST-PROCESSED-DATE #NEXT-PROCESSING-DATE
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Next_Processing_Date.greater(getZero())))                                                                                                       //Natural: IF #NEXT-PROCESSING-DATE GT 0
        {
            pnd_New_Mm.setValueEdited(pnd_Next_Processing_Date,new ReportEditMask("MM"));                                                                                 //Natural: MOVE EDITED #NEXT-PROCESSING-DATE ( EM = MM ) TO #NEW-MM
            //*  MEANS 1ST WORKING DAY OF THE MONTH.
            if (condition(pnd_Orig_Date_Pnd_Orig_Date_Mm.notEquals(pnd_New_Mm)))                                                                                          //Natural: IF #ORIG-DATE-MM NE #NEW-MM
            {
                cwf_Support_Tbl_Pnd_Override_Flag.setValue("Y");                                                                                                          //Natural: ASSIGN #OVERRIDE-FLAG := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            cwf_Support_Tbl_Pnd_Prior_Date_Parm.setValue(cwf_Support_Tbl_Pnd_Regular_Run_Date);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.#PRIOR-DATE-PARM := CWF-SUPPORT-TBL.#REGULAR-RUN-DATE
            cwf_Support_Tbl_Pnd_Regular_Run_Date.setValueEdited(pnd_Next_Processing_Date,new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED #NEXT-PROCESSING-DATE ( EM = YYYYMMDD ) TO CWF-SUPPORT-TBL.#REGULAR-RUN-DATE
        }                                                                                                                                                                 //Natural: END-IF
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setValue("BATCH");                                                                                                            //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE := 'BATCH'
        cwf_Support_Tbl_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                         //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-UPDTE-DTE := *DATX
        vw_cwf_Support_Tbl.updateDBRow("L_GET");                                                                                                                          //Natural: UPDATE ( L-GET. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=OFF");
    }
}
