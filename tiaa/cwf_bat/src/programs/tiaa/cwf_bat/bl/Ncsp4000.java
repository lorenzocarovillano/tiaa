/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:02:27 PM
**        * FROM NATURAL PROGRAM : Ncsp4000
************************************************************
**        * FILE NAME            : Ncsp4000.java
**        * CLASS NAME           : Ncsp4000
**        * INSTANCE NAME        : Ncsp4000
************************************************************
************************************************************************
* PROGRAM  : NCSP4000
* SYSTEM   : PROJCWF
* TITLE    : DELETE UPLOAD AUDIT RECORDS NON-PARTICIPANTS
* GENERATED: MARCH 10, 1994
* FUNCTION : THIS PROGRAM READS THE AUDIT STATS RECORDS AND DELETES
*            ALL RECORDS PREVIOUS TO THE ONE WEEK AGO.
* HISTORY
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ncsp4000 extends BLNatBase
{
    // Data Areas
    private GdaMcsg000 gdaMcsg000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ncw_Mcss_Calls;
    private DbsField ncw_Mcss_Calls_Rcrd_Stamp_Tme;
    private DbsField ncw_Mcss_Calls_Rcrd_Upld_Tme;
    private DbsField ncw_Mcss_Calls_Rcrd_Type_Cde;
    private DbsField ncw_Mcss_Calls_Rcrd_Contn_Ind;
    private DbsField ncw_Mcss_Calls_Systm_Ind;
    private DbsField ncw_Mcss_Calls_Np_Pin;
    private DbsField ncw_Mcss_Calls_Contact_Id_Cde;
    private DbsField ncw_Mcss_Calls_Wpids_Nbr;
    private DbsField ncw_Mcss_Calls_Lines_Total_Nbr;
    private DbsField ncw_Mcss_Calls_Wpids_Total_Nbr;
    private DbsField ncw_Mcss_Calls_Cntct_Total_Nbr;
    private DbsField ncw_Mcss_Calls_Cntct_Error_Nbr;
    private DbsField ncw_Mcss_Calls_Doc_Ctgry_Cde;
    private DbsField ncw_Mcss_Calls_Doc_Clss_Cde;
    private DbsField ncw_Mcss_Calls_Spcl_Hndlng_Txt;
    private DbsField ncw_Mcss_Calls_Error_Msg_Txt;

    private DbsGroup ncw_Mcss_Calls_Request_Array_Grp;
    private DbsField ncw_Mcss_Calls_Action_Cde;
    private DbsField ncw_Mcss_Calls_Wpid_Cde;

    private DbsGroup ncw_Mcss_Calls_Doc_Text_Area_Grp;
    private DbsField ncw_Mcss_Calls_Doc_Txt;
    private DbsField pnd_I;
    private DbsField pnd_Program;
    private DbsField pnd_Input_Time;
    private DbsField pnd_First_Rec;
    private DbsField pnd_Date_Time;

    private DbsGroup pnd_Date_Time__R_Field_1;
    private DbsField pnd_Date_Time_Pnd_Date;
    private DbsField pnd_Date_Time_Pnd_Time;
    private DbsField pnd_Chk_Dtetme;

    private DbsGroup pnd_Chk_Dtetme__R_Field_2;
    private DbsField pnd_Chk_Dtetme_Pnd_Chk_Date;

    private DbsGroup pnd_Chk_Dtetme__R_Field_3;
    private DbsField pnd_Chk_Dtetme_Pnd_Chk_Yyyy;
    private DbsField pnd_Chk_Dtetme_Pnd_Chk_Mm;
    private DbsField pnd_Chk_Dtetme_Pnd_Chk_Time;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_4;
    private DbsField pnd_Date_A_Pnd_Date_Num;
    private DbsField pnd_Tot_Delete;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Upld_Date;
    private DbsField pnd_Upld_Time;

    private DbsGroup pnd_Upld_Time__R_Field_5;
    private DbsField pnd_Upld_Time_Pnd_Upld_Timen;
    private DbsField pnd_Input_Sys;
    private DbsField pnd_Input_From;

    private DbsGroup pnd_Input_From__R_Field_6;
    private DbsField pnd_Input_From_Yyyy;
    private DbsField pnd_Input_From_Mm;
    private DbsField pnd_Input_From_Dd;
    private DbsField pnd_Input_From_D;
    private DbsField pnd_Input_To;

    private DbsGroup pnd_Input_To__R_Field_7;
    private DbsField pnd_Input_To_Yyyy;
    private DbsField pnd_Input_To_Mm;
    private DbsField pnd_Input_To_Dd;
    private DbsField pnd_Input_To_D;
    private DbsField pnd_Input_To_T;
    private DbsField pnd_Input;

    private DbsGroup pnd_Input__R_Field_8;
    private DbsField pnd_Input_Yyyy;
    private DbsField pnd_Input_Mm;
    private DbsField pnd_Input_Dd;
    private DbsField pnd_Cwf_Mcss_Key;

    private DbsGroup pnd_Cwf_Mcss_Key__R_Field_9;
    private DbsField pnd_Cwf_Mcss_Key_Systm_Ind;
    private DbsField pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme;
    private DbsField pnd_Cwf_Mcss_Key_Rcrd_Type_Cde;
    private DbsField pnd_Cwf_Mcss_Key_Btch_Nbr;
    private DbsField pnd_Cwf_Mcss_Key_Contact_Id_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMcsg000 = GdaMcsg000.getInstance(getCallnatLevel());
        registerRecord(gdaMcsg000);
        if (gdaOnly) return;

        // Local Variables
        localVariables = new DbsRecord();

        vw_ncw_Mcss_Calls = new DataAccessProgramView(new NameInfo("vw_ncw_Mcss_Calls", "NCW-MCSS-CALLS"), "NCW_MCSS_CALLS", "NCW_MCSS_CALLS", DdmPeriodicGroups.getInstance().getGroups("NCW_MCSS_CALLS"));
        ncw_Mcss_Calls_Rcrd_Stamp_Tme = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Rcrd_Stamp_Tme", "RCRD-STAMP-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_STAMP_TME");
        ncw_Mcss_Calls_Rcrd_Stamp_Tme.setDdmHeader("RECORD/TIME/STAMP");
        ncw_Mcss_Calls_Rcrd_Upld_Tme = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_UPLD_TME");
        ncw_Mcss_Calls_Rcrd_Upld_Tme.setDdmHeader("RECORD/UPLOAD/TIME");
        ncw_Mcss_Calls_Rcrd_Type_Cde = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        ncw_Mcss_Calls_Rcrd_Type_Cde.setDdmHeader("RECORD/TYPE");
        ncw_Mcss_Calls_Rcrd_Contn_Ind = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Rcrd_Contn_Ind", "RCRD-CONTN-IND", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "RCRD_CONTN_IND");
        ncw_Mcss_Calls_Systm_Ind = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTM_IND");
        ncw_Mcss_Calls_Systm_Ind.setDdmHeader("SYSTEM/OF/ORIGIN");
        ncw_Mcss_Calls_Np_Pin = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Mcss_Calls_Np_Pin.setDdmHeader("PIN");
        ncw_Mcss_Calls_Contact_Id_Cde = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CONTACT_ID_CDE");
        ncw_Mcss_Calls_Contact_Id_Cde.setDdmHeader("CONTACT/ID");
        ncw_Mcss_Calls_Wpids_Nbr = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Wpids_Nbr", "WPIDS-NBR", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "WPIDS_NBR");
        ncw_Mcss_Calls_Lines_Total_Nbr = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Lines_Total_Nbr", "LINES-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LINES_TOTAL_NBR");
        ncw_Mcss_Calls_Wpids_Total_Nbr = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Wpids_Total_Nbr", "WPIDS-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "WPIDS_TOTAL_NBR");
        ncw_Mcss_Calls_Cntct_Total_Nbr = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Cntct_Total_Nbr", "CNTCT-TOTAL-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTCT_TOTAL_NBR");
        ncw_Mcss_Calls_Cntct_Error_Nbr = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Cntct_Error_Nbr", "CNTCT-ERROR-NBR", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "CNTCT_ERROR_NBR");
        ncw_Mcss_Calls_Doc_Ctgry_Cde = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Doc_Ctgry_Cde", "DOC-CTGRY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOC_CTGRY_CDE");
        ncw_Mcss_Calls_Doc_Clss_Cde = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Doc_Clss_Cde", "DOC-CLSS-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "DOC_CLSS_CDE");
        ncw_Mcss_Calls_Spcl_Hndlng_Txt = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        ncw_Mcss_Calls_Spcl_Hndlng_Txt.setDdmHeader("SPECIAL/HANDLING");
        ncw_Mcss_Calls_Error_Msg_Txt = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Error_Msg_Txt", "ERROR-MSG-TXT", FieldType.STRING, 
            80, RepeatingFieldStrategy.None, "ERROR_MSG_TXT");
        ncw_Mcss_Calls_Error_Msg_Txt.setDdmHeader("ERROR/MSG.");

        ncw_Mcss_Calls_Request_Array_Grp = vw_ncw_Mcss_Calls.getRecord().newGroupInGroup("ncw_Mcss_Calls_Request_Array_Grp", "REQUEST-ARRAY-GRP", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");
        ncw_Mcss_Calls_Action_Cde = ncw_Mcss_Calls_Request_Array_Grp.newFieldArrayInGroup("ncw_Mcss_Calls_Action_Cde", "ACTION-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACTION_CDE", "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");
        ncw_Mcss_Calls_Wpid_Cde = ncw_Mcss_Calls_Request_Array_Grp.newFieldArrayInGroup("ncw_Mcss_Calls_Wpid_Cde", "WPID-CDE", FieldType.STRING, 6, new 
            DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WPID_CDE", "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");

        ncw_Mcss_Calls_Doc_Text_Area_Grp = vw_ncw_Mcss_Calls.getRecord().newGroupArrayInGroup("ncw_Mcss_Calls_Doc_Text_Area_Grp", "DOC-TEXT-AREA-GRP", 
            new DbsArrayController(1, 60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NCW_MCSS_CALLS_DOC_TEXT_AREA_GRP");
        ncw_Mcss_Calls_Doc_Txt = ncw_Mcss_Calls_Doc_Text_Area_Grp.newFieldInGroup("ncw_Mcss_Calls_Doc_Txt", "DOC-TXT", FieldType.STRING, 80, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "DOC_TXT", "NCW_MCSS_CALLS_DOC_TEXT_AREA_GRP");
        registerRecord(vw_ncw_Mcss_Calls);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Input_Time = localVariables.newFieldInRecord("pnd_Input_Time", "#INPUT-TIME", FieldType.PACKED_DECIMAL, 6);
        pnd_First_Rec = localVariables.newFieldInRecord("pnd_First_Rec", "#FIRST-REC", FieldType.STRING, 1);
        pnd_Date_Time = localVariables.newFieldInRecord("pnd_Date_Time", "#DATE-TIME", FieldType.STRING, 14);

        pnd_Date_Time__R_Field_1 = localVariables.newGroupInRecord("pnd_Date_Time__R_Field_1", "REDEFINE", pnd_Date_Time);
        pnd_Date_Time_Pnd_Date = pnd_Date_Time__R_Field_1.newFieldInGroup("pnd_Date_Time_Pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_Date_Time_Pnd_Time = pnd_Date_Time__R_Field_1.newFieldInGroup("pnd_Date_Time_Pnd_Time", "#TIME", FieldType.STRING, 6);
        pnd_Chk_Dtetme = localVariables.newFieldInRecord("pnd_Chk_Dtetme", "#CHK-DTETME", FieldType.STRING, 14);

        pnd_Chk_Dtetme__R_Field_2 = localVariables.newGroupInRecord("pnd_Chk_Dtetme__R_Field_2", "REDEFINE", pnd_Chk_Dtetme);
        pnd_Chk_Dtetme_Pnd_Chk_Date = pnd_Chk_Dtetme__R_Field_2.newFieldInGroup("pnd_Chk_Dtetme_Pnd_Chk_Date", "#CHK-DATE", FieldType.STRING, 8);

        pnd_Chk_Dtetme__R_Field_3 = pnd_Chk_Dtetme__R_Field_2.newGroupInGroup("pnd_Chk_Dtetme__R_Field_3", "REDEFINE", pnd_Chk_Dtetme_Pnd_Chk_Date);
        pnd_Chk_Dtetme_Pnd_Chk_Yyyy = pnd_Chk_Dtetme__R_Field_3.newFieldInGroup("pnd_Chk_Dtetme_Pnd_Chk_Yyyy", "#CHK-YYYY", FieldType.NUMERIC, 4);
        pnd_Chk_Dtetme_Pnd_Chk_Mm = pnd_Chk_Dtetme__R_Field_3.newFieldInGroup("pnd_Chk_Dtetme_Pnd_Chk_Mm", "#CHK-MM", FieldType.NUMERIC, 2);
        pnd_Chk_Dtetme_Pnd_Chk_Time = pnd_Chk_Dtetme__R_Field_2.newFieldInGroup("pnd_Chk_Dtetme_Pnd_Chk_Time", "#CHK-TIME", FieldType.STRING, 6);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 14);

        pnd_Date_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_4", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_Num = pnd_Date_A__R_Field_4.newFieldInGroup("pnd_Date_A_Pnd_Date_Num", "#DATE-NUM", FieldType.NUMERIC, 14);
        pnd_Tot_Delete = localVariables.newFieldInRecord("pnd_Tot_Delete", "#TOT-DELETE", FieldType.NUMERIC, 8);
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.NUMERIC, 4);
        pnd_Upld_Date = localVariables.newFieldInRecord("pnd_Upld_Date", "#UPLD-DATE", FieldType.DATE);
        pnd_Upld_Time = localVariables.newFieldInRecord("pnd_Upld_Time", "#UPLD-TIME", FieldType.STRING, 4);

        pnd_Upld_Time__R_Field_5 = localVariables.newGroupInRecord("pnd_Upld_Time__R_Field_5", "REDEFINE", pnd_Upld_Time);
        pnd_Upld_Time_Pnd_Upld_Timen = pnd_Upld_Time__R_Field_5.newFieldInGroup("pnd_Upld_Time_Pnd_Upld_Timen", "#UPLD-TIMEN", FieldType.NUMERIC, 4);
        pnd_Input_Sys = localVariables.newFieldInRecord("pnd_Input_Sys", "#INPUT-SYS", FieldType.STRING, 15);
        pnd_Input_From = localVariables.newFieldInRecord("pnd_Input_From", "#INPUT-FROM", FieldType.STRING, 8);

        pnd_Input_From__R_Field_6 = localVariables.newGroupInRecord("pnd_Input_From__R_Field_6", "REDEFINE", pnd_Input_From);
        pnd_Input_From_Yyyy = pnd_Input_From__R_Field_6.newFieldInGroup("pnd_Input_From_Yyyy", "YYYY", FieldType.NUMERIC, 4);
        pnd_Input_From_Mm = pnd_Input_From__R_Field_6.newFieldInGroup("pnd_Input_From_Mm", "MM", FieldType.NUMERIC, 2);
        pnd_Input_From_Dd = pnd_Input_From__R_Field_6.newFieldInGroup("pnd_Input_From_Dd", "DD", FieldType.NUMERIC, 2);
        pnd_Input_From_D = localVariables.newFieldInRecord("pnd_Input_From_D", "#INPUT-FROM-D", FieldType.DATE);
        pnd_Input_To = localVariables.newFieldInRecord("pnd_Input_To", "#INPUT-TO", FieldType.STRING, 8);

        pnd_Input_To__R_Field_7 = localVariables.newGroupInRecord("pnd_Input_To__R_Field_7", "REDEFINE", pnd_Input_To);
        pnd_Input_To_Yyyy = pnd_Input_To__R_Field_7.newFieldInGroup("pnd_Input_To_Yyyy", "YYYY", FieldType.NUMERIC, 4);
        pnd_Input_To_Mm = pnd_Input_To__R_Field_7.newFieldInGroup("pnd_Input_To_Mm", "MM", FieldType.NUMERIC, 2);
        pnd_Input_To_Dd = pnd_Input_To__R_Field_7.newFieldInGroup("pnd_Input_To_Dd", "DD", FieldType.NUMERIC, 2);
        pnd_Input_To_D = localVariables.newFieldInRecord("pnd_Input_To_D", "#INPUT-TO-D", FieldType.DATE);
        pnd_Input_To_T = localVariables.newFieldInRecord("pnd_Input_To_T", "#INPUT-TO-T", FieldType.TIME);
        pnd_Input = localVariables.newFieldInRecord("pnd_Input", "#INPUT", FieldType.STRING, 8);

        pnd_Input__R_Field_8 = localVariables.newGroupInRecord("pnd_Input__R_Field_8", "REDEFINE", pnd_Input);
        pnd_Input_Yyyy = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Yyyy", "YYYY", FieldType.NUMERIC, 4);
        pnd_Input_Mm = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Mm", "MM", FieldType.NUMERIC, 2);
        pnd_Input_Dd = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Dd", "DD", FieldType.NUMERIC, 2);
        pnd_Cwf_Mcss_Key = localVariables.newFieldInRecord("pnd_Cwf_Mcss_Key", "#CWF-MCSS-KEY", FieldType.STRING, 41);

        pnd_Cwf_Mcss_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Cwf_Mcss_Key__R_Field_9", "REDEFINE", pnd_Cwf_Mcss_Key);
        pnd_Cwf_Mcss_Key_Systm_Ind = pnd_Cwf_Mcss_Key__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Key_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15);
        pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme = pnd_Cwf_Mcss_Key__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME);
        pnd_Cwf_Mcss_Key_Rcrd_Type_Cde = pnd_Cwf_Mcss_Key__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Key_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Cwf_Mcss_Key_Btch_Nbr = pnd_Cwf_Mcss_Key__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Key_Btch_Nbr", "BTCH-NBR", FieldType.NUMERIC, 8);
        pnd_Cwf_Mcss_Key_Contact_Id_Cde = pnd_Cwf_Mcss_Key__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Key_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ncw_Mcss_Calls.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ncsp4000() throws Exception
    {
        super("Ncsp4000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
        pnd_Input.setValue(Global.getDATN());                                                                                                                             //Natural: ASSIGN #INPUT = *DATN
        pnd_Input_From.setValue(19900101);                                                                                                                                //Natural: ASSIGN #INPUT-FROM = 19900101
        pnd_Input_To.setValue(pnd_Input);                                                                                                                                 //Natural: ASSIGN #INPUT-TO = #INPUT
        pnd_Input_Sys.setValue("aaa");                                                                                                                                    //Natural: ASSIGN #INPUT-SYS = 'aaa'
        pnd_Cwf_Mcss_Key_Systm_Ind.setValue(pnd_Input_Sys);                                                                                                               //Natural: ASSIGN #CWF-MCSS-KEY.SYSTM-IND = #INPUT-SYS
        pnd_Cwf_Mcss_Key_Rcrd_Type_Cde.setValue("A");                                                                                                                     //Natural: ASSIGN #CWF-MCSS-KEY.RCRD-TYPE-CDE = 'A'
        pnd_Date_Time_Pnd_Date.setValue(pnd_Input_From);                                                                                                                  //Natural: ASSIGN #DATE = #INPUT-FROM
        pnd_Date_Time_Pnd_Time.setValue("000001");                                                                                                                        //Natural: ASSIGN #TIME = '000001'
        pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Date_Time);                                                                //Natural: MOVE EDITED #DATE-TIME TO #CWF-MCSS-KEY.RCRD-UPLD-TME ( EM = YYYYMMDDHHIISS )
        pnd_Input_From_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_From);                                                                                   //Natural: MOVE EDITED #INPUT-FROM TO #INPUT-FROM-D ( EM = YYYYMMDD )
        pnd_Input_To_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_To);                                                                                       //Natural: MOVE EDITED #INPUT-TO TO #INPUT-TO-D ( EM = YYYYMMDD )
        pnd_Input_To_D.nsubtract(30);                                                                                                                                     //Natural: COMPUTE #INPUT-TO-D = #INPUT-TO-D - 30
        pnd_Input_To_T.setValue(pnd_Input_To_D);                                                                                                                          //Natural: MOVE #INPUT-TO-D TO #INPUT-TO-T
        //*  READ THE AUDIT RECORDS
        vw_ncw_Mcss_Calls.startDatabaseRead                                                                                                                               //Natural: READ NCW-MCSS-CALLS WITH UNQUE-KEY = #CWF-MCSS-KEY
        (
        "READ_AUDIT",
        new Wc[] { new Wc("UNQUE_KEY", ">=", pnd_Cwf_Mcss_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("UNQUE_KEY", "ASC") }
        );
        READ_AUDIT:
        while (condition(vw_ncw_Mcss_Calls.readNextRow("READ_AUDIT")))
        {
            //*  COUNT READS - NOT DELETES BECAUSE NATURAL READS WITH HOLD
            pnd_Rec_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-CNT
            if (condition(ncw_Mcss_Calls_Rcrd_Type_Cde.equals("A") || ncw_Mcss_Calls_Rcrd_Type_Cde.equals("S") || ncw_Mcss_Calls_Rcrd_Type_Cde.equals("W")                //Natural: IF NCW-MCSS-CALLS.RCRD-TYPE-CDE = 'A' OR NCW-MCSS-CALLS.RCRD-TYPE-CDE = 'S' OR NCW-MCSS-CALLS.RCRD-TYPE-CDE = 'W' OR NCW-MCSS-CALLS.RCRD-TYPE-CDE = 'T'
                || ncw_Mcss_Calls_Rcrd_Type_Cde.equals("T")))
            {
                if (condition(ncw_Mcss_Calls_Rcrd_Upld_Tme.lessOrEqual(pnd_Input_To_T)))                                                                                  //Natural: IF NCW-MCSS-CALLS.RCRD-UPLD-TME LE #INPUT-TO-T
                {
                    pnd_Tot_Delete.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-DELETE
                    vw_ncw_Mcss_Calls.deleteDBRow("READ_AUDIT");                                                                                                          //Natural: DELETE ( READ-AUDIT. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Delete.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TOT-DELETE
                vw_ncw_Mcss_Calls.deleteDBRow("READ_AUDIT");                                                                                                              //Natural: DELETE ( READ-AUDIT. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Rec_Cnt.greater(100)))                                                                                                                      //Natural: IF #REC-CNT GT 100
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Rec_Cnt.setValue(0);                                                                                                                                  //Natural: ASSIGN #REC-CNT = 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(1, NEWLINE,NEWLINE,"    NUMBER OF RECORDS DELETED : ",pnd_Tot_Delete);                                                                         //Natural: WRITE ( 1 ) // '    NUMBER OF RECORDS DELETED : ' #TOT-DELETE
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE," ** UPLOAD CONTROL RECORD DELETE SUCCESSFUL **");                                                                          //Natural: WRITE ( 1 ) // ' ** UPLOAD CONTROL RECORD DELETE SUCCESSFUL **'
        if (Global.isEscape()) return;
        //*  AT TOP OF PAGE(1)
        //*   WRITE(1) NOTITLE NOHDR USING FORM 'MCSF1600'
        //*  END-TOPPAGE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=OFF");
    }
}
