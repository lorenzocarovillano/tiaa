/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:30:24 PM
**        * FROM NATURAL PROGRAM : Cwfb3804
************************************************************
**        * FILE NAME            : Cwfb3804.java
**        * CLASS NAME           : Cwfb3804
**        * INSTANCE NAME        : Cwfb3804
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: MONTHLY TURNAROUND REPT
**SAG SYSTEM: CRF
**SAG REPORT-HEADING(1): CORPORATE WORK FLOW
**SAG PRINT-FILE(1): 1
**SAG REPORT-HEADING(2): MONTHLY APPLICATION PROCESSING TURNAROUND
**SAG PRINT-FILE(2): 1
**SAG REPORT-HEADING(3): CORPORATE WORK FLOW
**SAG PRINT-FILE(3): 2
**SAG REPORT-HEADING(4): MONTHLY APPLICATION PROCESSING TURNAROUN ERRORS
**SAG PRINT-FILE(4): 2
**SAG HEADING-LINE: 12120000
**SAG DESCS(1): THIS PROGRAM READS THE MIT FILE FOR ACITVIE RECORDS THA
**SAG DESCS(2): T HAVE BEEN PARTICIPANT CLOSED THIS MONTH FOR THE WPIDS
**SAG DESCS(3): IN THE TABLE APP-PROCESSING-WPIDS.  CREATES A REPORT
**SAG DESCS(4): SHOWING TURNAROUND TIMES AND PERCENTAGES.
**SAG PRIMARY-FILE: CWF-MASTER-INDEX-VIEW
**SAG PRIMARY-KEY: PRTCPNT-CLOSED-KEY
**SAG MODEL-1: X
**SAG USER-FIELD-DEFINITION(1): #START-DATE               A080TF
************************************************************************
* PROGRAM  : CWFB3804
* SYSTEM   : CRF
* TITLE    : MONTHLY TURNAROUND REPT
* GENERATED: MAR 06,96 AT 03:30 PM
* FUNCTION : THIS PROGRAM READS THE MIT FILE FOR ACITVIE RECORDS THA
*            T HAVE BEEN PARTICIPANT CLOSED THIS MONTH FOR THE WPIDS
*            IN THE TABLE APP-PROCESSING-WPIDS.  CREATES A REPORT
*            SHOWING TURNAROUND TIMES AND PERCENTAGES.
* HISTORY
**SAG DEFINE EXIT CHANGE-HISTORY
* CHANGED ON FEB 29,96 BY HARGRAV FOR RELEASE ____
* CHANGED ON NOV 16,98 BY PAREDES TO ALLOW FOR PROCESSING IN YEAR 2000.
* >
**SAG END-EXIT
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3804 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private PdaCwfpda_M pdaCwfpda_M;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Start_Date;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_1;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Rgn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Brnch_Cde;
    private DbsField cwf_Master_Index_View_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Case_Id_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_2;
    private DbsField cwf_Master_Index_View_Case_Ind;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind;
    private DbsField cwf_Master_Index_View_Multi_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_3;
    private DbsField cwf_Master_Index_View_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Wpid_Vldte_Ind;
    private DbsField cwf_Master_Index_View_Unit_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_4;
    private DbsField cwf_Master_Index_View_Unit_Id_Cde;
    private DbsField cwf_Master_Index_View_Unit_Rgn_Cde;
    private DbsField cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_View_Unit_Brnch_Group_Cde;
    private DbsField cwf_Master_Index_View_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Old_Route_Cde;
    private DbsField cwf_Master_Index_View_Work_Rqst_Prty_Cde;
    private DbsField cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Assgn_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_5;
    private DbsField cwf_Master_Index_View_Empl_Racf_Id;
    private DbsField cwf_Master_Index_View_Empl_Sffx_Cde;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_6;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_Tme_8_15;
    private DbsField cwf_Master_Index_View_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_View_Step_Id;
    private DbsField cwf_Master_Index_View_Rt_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Step_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Step_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Status_Cde;
    private DbsField cwf_Master_Index_View_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Updte_Dte;
    private DbsField cwf_Master_Index_View_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Cntct_Orgn_Type_Cde;
    private DbsField cwf_Master_Index_View_Cntct_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Oprtr_Id;
    private DbsField cwf_Master_Index_View_Actve_Ind;

    private DbsGroup cwf_Master_Index_View__R_Field_7;
    private DbsField cwf_Master_Index_View_Fill_1;
    private DbsField cwf_Master_Index_View_Actve_Ind_2_2;
    private DbsField cwf_Master_Index_View_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_View_Cnflct_Ind;
    private DbsField cwf_Master_Index_View_Spcl_Hndlng_Txt;
    private DbsField cwf_Master_Index_View_Instn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Instn_Cde;
    private DbsGroup cwf_Master_Index_View_Spcl_Policy_Srce_CdeMuGroup;
    private DbsField cwf_Master_Index_View_Spcl_Policy_Srce_Cde;
    private DbsField cwf_Master_Index_View_Attntn_Txt;
    private DbsField cwf_Master_Index_View_Check_Ind;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_View_Effctve_Dte;
    private DbsField cwf_Master_Index_View_Trnsctn_Dte;
    private DbsField cwf_Master_Index_View_Trans_Dte;
    private DbsField cwf_Master_Index_View_Physcl_Fldr_Id_Nbr;
    private DbsField cwf_Master_Index_View_Mj_Chrge_Dte_Tme;
    private DbsField cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Mstr_Indx_Actn_Cde;
    private DbsField cwf_Master_Index_View_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_View_Final_Close_Out_Dte_Tme;
    private DbsField cwf_Master_Index_View_Final_Close_Out_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Mj_Emrgncy_Rqst_Dte_Tme;
    private DbsField cwf_Master_Index_View_Mj_Emrgncy_Rqst_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Print_Q_Ind;
    private DbsField cwf_Master_Index_View_Print_Dte_Tme;
    private DbsField cwf_Master_Index_View_Print_Batch_Id_Nbr;

    private DbsGroup cwf_Master_Index_View__R_Field_8;
    private DbsField cwf_Master_Index_View_Print_Prefix_Ind;
    private DbsField cwf_Master_Index_View_Print_Batch_Nbr;
    private DbsField cwf_Master_Index_View_Print_Batch_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Print_Batch_Ind;
    private DbsField cwf_Master_Index_View_Printer_Id_Cde;
    private DbsField cwf_Master_Index_View_Rescan_Ind;
    private DbsField cwf_Master_Index_View_Dup_Ind;
    private DbsField cwf_Master_Index_View_Cmplnt_Ind;
    private DbsGroup cwf_Master_Index_View_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Cntrct_Nbr;
    private DbsGroup cwf_Master_Index_View_Mail_Item_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Mail_Item_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_9;
    private DbsField cwf_Master_Index_View_Rqst_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Case_Id_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rlte_Rqst_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_10;
    private DbsField cwf_Master_Index_View_Rlte_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Rlte_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rlte_Case_Id_Cde;
    private DbsField cwf_Master_Index_View_Rlte_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_View_Work_List_Ind;

    private DbsGroup cwf_Master_Index_View__R_Field_11;
    private DbsField cwf_Master_Index_View_Work_List_Ind_1_1;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_12;
    private DbsField cwf_Master_Index_View_Due_Dte;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Ind;
    private DbsField cwf_Master_Index_View_Due_Dte_Prty;
    private DbsField cwf_Master_Index_View_Filler_1406_9818;
    private DbsField cwf_Master_Index_View_Sbsqnt_Cntct_Actn_Rqrd;
    private DbsField cwf_Master_Index_View_Sbsqnt_Cntct_Ind;
    private DbsField cwf_Master_Index_View_Prcssng_Type;
    private DbsField cwf_Master_Index_View_Bsnss_Reply_Ind;
    private DbsField cwf_Master_Index_View_Status_Freeze_Ind;
    private DbsField cwf_Master_Index_View_Elctrnc_Fldr_Ind;
    private DbsField cwf_Master_Index_View_Log_Insttn_Srce_Cde;
    private DbsField cwf_Master_Index_View_Log_Rqstr_Cde;
    private DbsField cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Days;
    private DbsField cwf_Master_Index_View_Step_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Step_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_13;
    private DbsField cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme_1_8;
    private DbsField cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme;
    private DbsField cwf_Master_Index_View_Acknwldgmnt_Cde;
    private DbsField cwf_Master_Index_View_Acknwldgmnt_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Acknwldgmnt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Sheet_Print_Cde;
    private DbsField cwf_Master_Index_View_Cntct_Sheet_Print_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Sheet_Printer_Id_Cde;
    private DbsField cwf_Master_Index_View_Shphrd_Id;
    private DbsField cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_14;
    private DbsField cwf_Master_Index_View_Crrnt_Due_Dte;
    private DbsField cwf_Master_Index_View_Crrnt_Cmt_Ind;
    private DbsField cwf_Master_Index_View_Crrnt_Prrty_Cde;
    private DbsField cwf_Master_Index_View_Crrnt_Due_Tme;
    private DbsField cwf_Master_Index_View_Crprte_Due_Dte_Tme;
    private DbsField cwf_Master_Index_View_Archvd_Dte;
    private DbsField cwf_Master_Index_View_Rstr_To_Crrnt_Dte;
    private DbsField cwf_Master_Index_View_Off_Rtng_Ind;
    private DbsField cwf_Master_Index_View_Unit_On_Tme_Ind;
    private DbsField cwf_Master_Index_View_Crprte_On_Tme_Ind;
    private DbsField cwf_Master_Index_View_Owner_Unit_Cde;
    private DbsField cwf_Master_Index_View_Physcl_Fldr_Owner_Id;
    private DbsField cwf_Master_Index_View_Physcl_Fldr_Owner_Unit_Cde;
    private DbsField cwf_Master_Index_View_Step_Re_Do_Ind;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_View_Trade_Dte_Tme;
    private DbsField cwf_Master_Index_View_Future_Pymnt_Ind;
    private DbsField cwf_Master_Index_View_Status_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_15;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_16;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_17;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_18;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_19;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_20;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_21;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_22;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_23;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_24;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Minutes;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;
    private DbsField pnd_Header2_1;
    private DbsField pnd_Header2_2;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_25;
    private DbsField cwf_Support_Tbl_Tbl_Wpid;

    private DbsGroup cwf_Support_Tbl__R_Field_26;
    private DbsField cwf_Support_Tbl_Tbl_Unit;

    private DbsGroup cwf_Support_Tbl__R_Field_27;
    private DbsField cwf_Support_Tbl_Date_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Tbl_Table_Access_Level;
    private DbsField pnd_Prtcpnt_Closed_Key;

    private DbsGroup pnd_Prtcpnt_Closed_Key__R_Field_28;
    private DbsField pnd_Prtcpnt_Closed_Key_Pnd_Crprte_Clock_End_Dte;
    private DbsField pnd_Prtcpnt_Closed_Key_Pnd_Status_Freeze_Ind;
    private DbsField pnd_Wpid_Prime_Key;

    private DbsGroup pnd_Wpid_Prime_Key__R_Field_29;
    private DbsField pnd_Wpid_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Wpid_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Wpid_Prime_Key_Pnd_Tbl_Wpid;
    private DbsField pnd_Unit_Prime_Key;

    private DbsGroup pnd_Unit_Prime_Key__R_Field_30;
    private DbsField pnd_Unit_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Unit_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Unit_Prime_Key_Pnd_Tbl_Unit;
    private DbsField pnd_Date_Prime_Key;

    private DbsGroup pnd_Date_Prime_Key__R_Field_31;
    private DbsField pnd_Date_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Date_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Date_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Valid_Wpid_Tbl;
    private DbsField pnd_Valid_Unit_Tbl;

    private DbsGroup pnd_Totals;
    private DbsField pnd_Totals_Pnd_Total_Work;
    private DbsField pnd_Totals_Pnd_0_3_Total;
    private DbsField pnd_Totals_Pnd_4_7_Total;
    private DbsField pnd_Totals_Pnd_0_7_Total;
    private DbsField pnd_Totals_Pnd_8_10_Total;
    private DbsField pnd_Totals_Pnd_Gt_10_Total;

    private DbsGroup pnd_Gtotals;
    private DbsField pnd_Gtotals_Pnd_Total_Work;
    private DbsField pnd_Gtotals_Pnd_0_3_Total;
    private DbsField pnd_Gtotals_Pnd_4_7_Total;
    private DbsField pnd_Gtotals_Pnd_0_7_Total;
    private DbsField pnd_Gtotals_Pnd_8_10_Total;
    private DbsField pnd_Gtotals_Pnd_Gt_10_Total;

    private DbsGroup pnd_Pcts;
    private DbsField pnd_Pcts_Pnd_Total_Pct;
    private DbsField pnd_Pcts_Pnd_0_3_Pct;
    private DbsField pnd_Pcts_Pnd_4_7_Pct;
    private DbsField pnd_Pcts_Pnd_0_7_Pct;
    private DbsField pnd_Pcts_Pnd_8_10_Pct;
    private DbsField pnd_Pcts_Pnd_Gt_10_Pct;

    private DbsGroup pnd_Tot_Pcts;
    private DbsField pnd_Tot_Pcts_Pnd_Total_Pct;
    private DbsField pnd_Tot_Pcts_Pnd_0_3_Pct;
    private DbsField pnd_Tot_Pcts_Pnd_4_7_Pct;
    private DbsField pnd_Tot_Pcts_Pnd_0_7_Pct;
    private DbsField pnd_Tot_Pcts_Pnd_8_10_Pct;
    private DbsField pnd_Tot_Pcts_Pnd_Gt_10_Pct;

    private DbsGroup pnd_Disp_Tots;
    private DbsField pnd_Disp_Tots_Pnd_D_Total;
    private DbsField pnd_Disp_Tots_Pnd_D_0_3;
    private DbsField pnd_Disp_Tots_Pnd_D_4_7;
    private DbsField pnd_Disp_Tots_Pnd_D_0_7;
    private DbsField pnd_Disp_Tots_Pnd_D_8_10;
    private DbsField pnd_Disp_Tots_Pnd_D_Gt_10;

    private DbsGroup pnd_Disp_Pcts;
    private DbsField pnd_Disp_Pcts_Pnd_D_Total_Pct;
    private DbsField pnd_Disp_Pcts_Pnd_D_0_3_Pct;
    private DbsField pnd_Disp_Pcts_Pnd_D_4_7_Pct;
    private DbsField pnd_Disp_Pcts_Pnd_D_0_7_Pct;
    private DbsField pnd_Disp_Pcts_Pnd_D_8_10_Pct;
    private DbsField pnd_Disp_Pcts_Pnd_D_Gt_10_Pct;
    private DbsField pnd_Run_Date;
    private DbsField pnd_Fix_Date;

    private DbsGroup pnd_Fix_Date__R_Field_32;
    private DbsField pnd_Fix_Date_Pnd_Fix_Cc;
    private DbsField pnd_Fix_Date_Pnd_Fix_Yy;
    private DbsField pnd_Fix_Date_Pnd_Fix_Mm;
    private DbsField pnd_Fix_Date_Pnd_Fix_Dd;
    private DbsField pnd_Convert_Date;
    private DbsField pnd_Start_Date_A;

    private DbsGroup pnd_Start_Date_A__R_Field_33;
    private DbsField pnd_Start_Date_A_Pnd_Start_Yyyy;
    private DbsField pnd_Start_Date_A_Pnd_Start_Mm;
    private DbsField pnd_Start_Date_A_Pnd_Start_Dd;
    private DbsField pnd_End_Date_A;

    private DbsGroup pnd_End_Date_A__R_Field_34;
    private DbsField pnd_End_Date_A_Pnd_End_Yyyy;
    private DbsField pnd_End_Date_A_Pnd_End_Mm;
    private DbsField pnd_End_Date_A_Pnd_End_Dd;
    private DbsField pnd_Disp_Unit;
    private DbsField pnd_Disp_Date;
    private DbsField pnd_Check_Clock_End_Dte_Tme;

    private DbsGroup pnd_Check_Clock_End_Dte_Tme__R_Field_35;
    private DbsField pnd_Check_Clock_End_Dte_Tme_Pnd_Check_Clock_End_Dte;
    private DbsField pnd_No_Records_Found;
    private DbsField pnd_Max_Valid_Wpids;
    private DbsField pnd_Max_Valid_Units;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Keep_It;
    private DbsField pnd_End_Of_Report;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Errors;
    private DbsField pnd_Prev_Unit;
    private DbsField pnd_Unit_Cde;
    private DbsField pnd_Base_Dte_Tme;
    private DbsField pnd_End_Dte_Tme;
    private DbsField pnd_Elapsed_Days;
    private DbsField pnd_Elapsed_Hrs;
    private DbsField pnd_Elapsed_Mins;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        localVariables = new DbsRecord();
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);

        // Local Variables
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Pin_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Master_Index_View_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index_View__R_Field_1 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_1", "REDEFINE", cwf_Master_Index_View_Rqst_Log_Dte_Tme);
        cwf_Master_Index_View_Rqst_Log_Index_Dte = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Log_Index_Tme = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme", 
            "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_View_Rqst_Orgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Orgn_Cde", "RQST-ORGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_View_Rqst_Rgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Rgn_Cde", "RQST-RGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_RGN_CDE");
        cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde", 
            "RQST-SPCL-DSGNTN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_SPCL_DSGNTN_CDE");
        cwf_Master_Index_View_Rqst_Brnch_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Brnch_Cde", "RQST-BRNCH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_BRNCH_CDE");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_View_Sub_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Ind", "SUB-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SUB_RQST_IND");
        cwf_Master_Index_View_Sub_Rqst_Ind.setDdmHeader("SUB-REQUEST COUNT");
        cwf_Master_Index_View_Case_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CASE_ID_CDE");
        cwf_Master_Index_View_Case_Id_Cde.setDdmHeader("CASE/ID");

        cwf_Master_Index_View__R_Field_2 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_2", "REDEFINE", cwf_Master_Index_View_Case_Id_Cde);
        cwf_Master_Index_View_Case_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Case_Ind", "CASE-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind", "SUB-RQST-SQNCE-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Multi_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Multi_Rqst_Ind", "MULTI-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        cwf_Master_Index_View_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Master_Index_View_Orgnl_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_View_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");

        cwf_Master_Index_View__R_Field_3 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_3", "REDEFINE", cwf_Master_Index_View_Work_Prcss_Id);
        cwf_Master_Index_View_Work_Actn_Rqstd_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde", 
            "WORK-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde", 
            "WORK-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Wpid_Vldte_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Wpid_Vldte_Ind", "WPID-VLDTE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WPID_VLDTE_IND");
        cwf_Master_Index_View_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Cde", "UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_CDE");
        cwf_Master_Index_View_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Master_Index_View__R_Field_4 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_4", "REDEFINE", cwf_Master_Index_View_Unit_Cde);
        cwf_Master_Index_View_Unit_Id_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 
            5);
        cwf_Master_Index_View_Unit_Rgn_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Unit_Brnch_Group_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Unit_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Index_View_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Index_View_Old_Route_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Old_Route_Cde", "OLD-ROUTE-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "OLD_ROUTE_CDE");
        cwf_Master_Index_View_Old_Route_Cde.setDdmHeader("ACTIVITY-END/STATUS");
        cwf_Master_Index_View_Work_Rqst_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Rqst_Prty_Cde", "WORK-RQST-PRTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WORK_RQST_PRTY_CDE");
        cwf_Master_Index_View_Work_Rqst_Prty_Cde.setDdmHeader("PRIO");
        cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde", 
            "ASSGN-SPRVSR-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ASSGN_SPRVSR_OPRTR_CDE");
        cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde.setDdmHeader("SUPER/VISOR");
        cwf_Master_Index_View_Assgn_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Assgn_Dte_Tme", "ASSGN-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ASSGN_DTE_TME");
        cwf_Master_Index_View_Assgn_Dte_Tme.setDdmHeader("ASSIGNMENT/DATE-TIME");
        cwf_Master_Index_View_Empl_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_View_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");

        cwf_Master_Index_View__R_Field_5 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_5", "REDEFINE", cwf_Master_Index_View_Empl_Oprtr_Cde);
        cwf_Master_Index_View_Empl_Racf_Id = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Empl_Sffx_Cde = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Empl_Sffx_Cde", "EMPL-SFFX-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Last_Chnge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        cwf_Master_Index_View__R_Field_6 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_6", "REDEFINE", cwf_Master_Index_View_Last_Chnge_Dte_Tme);
        cwf_Master_Index_View_Last_Chnge_Dte_Tme_8_15 = cwf_Master_Index_View__R_Field_6.newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_Tme_8_15", 
            "LAST-CHNGE-DTE-TME-8-15", FieldType.NUMERIC, 8);
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Oprtr_Cde", 
            "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme", 
            "LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Unit_Cde", 
            "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_View_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_View_Step_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Id", "STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "STEP_ID");
        cwf_Master_Index_View_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Index_View_Rt_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        cwf_Master_Index_View_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        cwf_Master_Index_View_Step_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Sqnce_Nbr", "STEP-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "STEP_SQNCE_NBR");
        cwf_Master_Index_View_Step_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Updte_Dte_Tme", "STEP-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STEP_UPDTE_DTE_TME");
        cwf_Master_Index_View_Step_Updte_Dte_Tme.setDdmHeader("STEP UPDATE/DATE-TIME");
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_View_Admin_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme", 
            "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_View_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Dte_Tme", 
            "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Index_View_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Oprtr_Cde", 
            "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Updte_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Dte", "LAST-UPDTE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        cwf_Master_Index_View_Last_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Index_View_Last_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Oprtr_Cde", 
            "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Cntct_Orgn_Type_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Orgn_Type_Cde", 
            "CNTCT-ORGN-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTCT_ORGN_TYPE_CDE");
        cwf_Master_Index_View_Cntct_Orgn_Type_Cde.setDdmHeader("CONTACT/CODE");
        cwf_Master_Index_View_Cntct_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Dte_Tme", "CNTCT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "CNTCT_DTE_TME");
        cwf_Master_Index_View_Cntct_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Invrt_Dte_Tme", 
            "CNTCT-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "CNTCT_INVRT_DTE_TME");
        cwf_Master_Index_View_Cntct_Oprtr_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Oprtr_Id", "CNTCT-OPRTR-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTCT_OPRTR_ID");
        cwf_Master_Index_View_Actve_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");

        cwf_Master_Index_View__R_Field_7 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_7", "REDEFINE", cwf_Master_Index_View_Actve_Ind);
        cwf_Master_Index_View_Fill_1 = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Master_Index_View_Actve_Ind_2_2 = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Actve_Ind_2_2", "ACTVE-IND-2-2", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Crprte_Status_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_View_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_View_Cnflct_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cnflct_Ind", "CNFLCT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNFLCT_IND");
        cwf_Master_Index_View_Cnflct_Ind.setDdmHeader("CONFLICT/REQUEST");
        cwf_Master_Index_View_Spcl_Hndlng_Txt = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", 
            FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        cwf_Master_Index_View_Spcl_Hndlng_Txt.setDdmHeader("MESSAGES");
        cwf_Master_Index_View_Instn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Instn_Cde", "INSTN-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "INSTN_CDE");
        cwf_Master_Index_View_Rqst_Instn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Instn_Cde", "RQST-INSTN-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "RQST_INSTN_CDE");
        cwf_Master_Index_View_Spcl_Policy_Srce_CdeMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_SPCL_POLICY_SRCE_CDEMuGroup", 
            "SPCL_POLICY_SRCE_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_SPCL_POLICY_SRCE_CDE");
        cwf_Master_Index_View_Spcl_Policy_Srce_Cde = cwf_Master_Index_View_Spcl_Policy_Srce_CdeMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Spcl_Policy_Srce_Cde", 
            "SPCL-POLICY-SRCE-CDE", FieldType.STRING, 5, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "SPCL_POLICY_SRCE_CDE");
        cwf_Master_Index_View_Attntn_Txt = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Attntn_Txt", "ATTNTN-TXT", FieldType.STRING, 
            25, RepeatingFieldStrategy.None, "ATTNTN_TXT");
        cwf_Master_Index_View_Attntn_Txt.setDdmHeader("ATTENTION");
        cwf_Master_Index_View_Check_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Check_Ind", "CHECK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CHECK_IND");
        cwf_Master_Index_View_Check_Ind.setDdmHeader("CHK");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme", 
            "RQST-INVRT-RCVD-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_INVRT_RCVD_DTE_TME");
        cwf_Master_Index_View_Effctve_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_View_Trnsctn_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trnsctn_Dte", "TRNSCTN-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRNSCTN_DTE");
        cwf_Master_Index_View_Trnsctn_Dte.setDdmHeader("TRANSACTION/DATE");
        cwf_Master_Index_View_Trans_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trans_Dte", "TRANS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        cwf_Master_Index_View_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_View_Physcl_Fldr_Id_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Index_View_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Index_View_Mj_Chrge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Chrge_Dte_Tme", "MJ-CHRGE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "MJ_CHRGE_DTE_TME");
        cwf_Master_Index_View_Mj_Chrge_Dte_Tme.setDdmHeader("CHARGEOUT/DATE-TIME");
        cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde", "MJ-CHRGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_CHRGE_OPRTR_CDE");
        cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde.setDdmHeader("CHARGEOUT/OPERATOR");
        cwf_Master_Index_View_Mstr_Indx_Actn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mstr_Indx_Actn_Cde", "MSTR-INDX-ACTN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "MSTR_INDX_ACTN_CDE");
        cwf_Master_Index_View_Mstr_Indx_Actn_Cde.setDdmHeader("CHARGEOUT/ACTION CODE");
        cwf_Master_Index_View_Mj_Pull_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_View_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_View_Final_Close_Out_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Final_Close_Out_Dte_Tme", 
            "FINAL-CLOSE-OUT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_DTE_TME");
        cwf_Master_Index_View_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        cwf_Master_Index_View_Final_Close_Out_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Final_Close_Out_Oprtr_Cde", 
            "FINAL-CLOSE-OUT-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_OPRTR_CDE");
        cwf_Master_Index_View_Mj_Emrgncy_Rqst_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Emrgncy_Rqst_Dte_Tme", 
            "MJ-EMRGNCY-RQST-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_DTE_TME");
        cwf_Master_Index_View_Mj_Emrgncy_Rqst_Dte_Tme.setDdmHeader("EMERGENCY REQ/DATE-TIME");
        cwf_Master_Index_View_Mj_Emrgncy_Rqst_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Emrgncy_Rqst_Oprtr_Cde", 
            "MJ-EMRGNCY-RQST-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_OPRTR_CDE");
        cwf_Master_Index_View_Mj_Emrgncy_Rqst_Oprtr_Cde.setDdmHeader("EMERGENCY/REQUESTOR");
        cwf_Master_Index_View_Print_Q_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Q_Ind", "PRINT-Q-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PRINT_Q_IND");
        cwf_Master_Index_View_Print_Q_Ind.setDdmHeader("MJ PRINT/QUEUE");
        cwf_Master_Index_View_Print_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Dte_Tme", "PRINT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "PRINT_DTE_TME");
        cwf_Master_Index_View_Print_Dte_Tme.setDdmHeader("MJ PRINT/DATE-TIME");
        cwf_Master_Index_View_Print_Batch_Id_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Batch_Id_Nbr", "PRINT-BATCH-ID-NBR", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "PRINT_BATCH_ID_NBR");

        cwf_Master_Index_View__R_Field_8 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_8", "REDEFINE", cwf_Master_Index_View_Print_Batch_Id_Nbr);
        cwf_Master_Index_View_Print_Prefix_Ind = cwf_Master_Index_View__R_Field_8.newFieldInGroup("cwf_Master_Index_View_Print_Prefix_Ind", "PRINT-PREFIX-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Print_Batch_Nbr = cwf_Master_Index_View__R_Field_8.newFieldInGroup("cwf_Master_Index_View_Print_Batch_Nbr", "PRINT-BATCH-NBR", 
            FieldType.NUMERIC, 8);
        cwf_Master_Index_View_Print_Batch_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Batch_Sqnce_Nbr", 
            "PRINT-BATCH-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRINT_BATCH_SQNCE_NBR");
        cwf_Master_Index_View_Print_Batch_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Batch_Ind", "PRINT-BATCH-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRINT_BATCH_IND");
        cwf_Master_Index_View_Printer_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Printer_Id_Cde", "PRINTER-ID-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "PRINTER_ID_CDE");
        cwf_Master_Index_View_Rescan_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RESCAN_IND");
        cwf_Master_Index_View_Rescan_Ind.setDdmHeader("RE-SCAN/INDICATOR");
        cwf_Master_Index_View_Dup_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Dup_Ind", "DUP-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DUP_IND");
        cwf_Master_Index_View_Dup_Ind.setDdmHeader("DUP/IND");
        cwf_Master_Index_View_Cmplnt_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CMPLNT_IND");
        cwf_Master_Index_View_Cntrct_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_View_Cntrct_Nbr = cwf_Master_Index_View_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Cntrct_Nbr", "CNTRCT-NBR", 
            FieldType.STRING, 8, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_View_Mail_Item_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_MAIL_ITEM_NBRMuGroup", 
            "MAIL_ITEM_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_MAIL_ITEM_NBR");
        cwf_Master_Index_View_Mail_Item_Nbr = cwf_Master_Index_View_Mail_Item_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Mail_Item_Nbr", "MAIL-ITEM-NBR", 
            FieldType.STRING, 11, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "MAIL_ITEM_NBR");
        cwf_Master_Index_View_Mail_Item_Nbr.setDdmHeader("MIN");
        cwf_Master_Index_View_Rqst_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Id", "RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Master_Index_View_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Master_Index_View__R_Field_9 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_9", "REDEFINE", cwf_Master_Index_View_Rqst_Id);
        cwf_Master_Index_View_Rqst_Work_Prcss_Id = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Case_Id_Cde = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Rqst_Pin_Nbr = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Rqst_Pin_Nbr", "RQST-PIN-NBR", FieldType.NUMERIC, 
            12);
        cwf_Master_Index_View_Rlte_Rqst_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rlte_Rqst_Id", "RLTE-RQST-ID", 
            FieldType.STRING, 28, RepeatingFieldStrategy.None, "RLTE_RQST_ID");

        cwf_Master_Index_View__R_Field_10 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_10", "REDEFINE", cwf_Master_Index_View_Rlte_Rqst_Id);
        cwf_Master_Index_View_Rlte_Work_Prcss_Id = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rlte_Work_Prcss_Id", "RLTE-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Master_Index_View_Rlte_Tiaa_Rcvd_Dte = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rlte_Tiaa_Rcvd_Dte", "RLTE-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rlte_Case_Id_Cde = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rlte_Case_Id_Cde", "RLTE-CASE-ID-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Rlte_Pin_Nbr = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rlte_Pin_Nbr", "RLTE-PIN-NBR", FieldType.NUMERIC, 
            12);
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte", 
            "EXTRNL-PEND-RCV-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_View_Work_List_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_List_Ind", "WORK-LIST-IND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "WORK_LIST_IND");
        cwf_Master_Index_View_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");

        cwf_Master_Index_View__R_Field_11 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_11", "REDEFINE", cwf_Master_Index_View_Work_List_Ind);
        cwf_Master_Index_View_Work_List_Ind_1_1 = cwf_Master_Index_View__R_Field_11.newFieldInGroup("cwf_Master_Index_View_Work_List_Ind_1_1", "WORK-LIST-IND-1-1", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde", 
            "DUE-DTE-CHG-PRTY-CDE", FieldType.STRING, 16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        cwf_Master_Index_View__R_Field_12 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_12", "REDEFINE", cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Index_View_Due_Dte = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Due_Dte", "DUE-DTE", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Due_Dte_Chg_Ind = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Prty = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Prty", "DUE-DTE-PRTY", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Filler_1406_9818 = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Filler_1406_9818", "FILLER-1406-9818", 
            FieldType.STRING, 3);
        cwf_Master_Index_View_Sbsqnt_Cntct_Actn_Rqrd = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Sbsqnt_Cntct_Actn_Rqrd", 
            "SBSQNT-CNTCT-ACTN-RQRD", FieldType.STRING, 1);
        cwf_Master_Index_View_Sbsqnt_Cntct_Ind = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Sbsqnt_Cntct_Ind", "SBSQNT-CNTCT-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Prcssng_Type = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Prcssng_Type", "PRCSSNG-TYPE", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Bsnss_Reply_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BSNSS_REPLY_IND");
        cwf_Master_Index_View_Bsnss_Reply_Ind.setDdmHeader("BUSINESS/REPLY");
        cwf_Master_Index_View_Status_Freeze_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_View_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Index_View_Elctrnc_Fldr_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_Index_View_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        cwf_Master_Index_View_Log_Insttn_Srce_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Log_Insttn_Srce_Cde", 
            "LOG-INSTTN-SRCE-CDE", FieldType.STRING, 5, RepeatingFieldStrategy.None, "LOG_INSTTN_SRCE_CDE");
        cwf_Master_Index_View_Log_Insttn_Srce_Cde.setDdmHeader("REQUESTOR/INSTITUTION");
        cwf_Master_Index_View_Log_Rqstr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Log_Rqstr_Cde", "LOG-RQSTR-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "LOG_RQSTR_CDE");
        cwf_Master_Index_View_Log_Rqstr_Cde.setDdmHeader("REQUESTOR/TYPE");
        cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme", 
            "UNIT-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Index_View_Unit_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Clock_End_Dte_Tme", 
            "UNIT-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme", 
            "EMPL-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        cwf_Master_Index_View_Empl_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Clock_End_Dte_Tme", 
            "EMPL-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme", 
            "INTRNL-PND-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_START_DTE_TME");
        cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme.setDdmHeader("ACKNOWLEDGEMENT DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme", 
            "INTRNL-PND-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_END_DTE_TME");
        cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme.setDdmHeader("INTERNAL PEND END DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_Days = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Days", "INTRNL-PND-DAYS", 
            FieldType.PACKED_DECIMAL, 5, RepeatingFieldStrategy.None, "INTRNL_PND_DAYS");
        cwf_Master_Index_View_Intrnl_Pnd_Days.setDdmHeader("INTERNAL PEND DAYS");
        cwf_Master_Index_View_Step_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Clock_Start_Dte_Tme", 
            "STEP-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        cwf_Master_Index_View_Step_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Clock_End_Dte_Tme", 
            "STEP-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme", 
            "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");

        cwf_Master_Index_View__R_Field_13 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_13", "REDEFINE", cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme);
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme_1_8 = cwf_Master_Index_View__R_Field_13.newFieldInGroup("cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme_1_8", 
            "CRPRTE-CLOCK-END-DTE-TME-1-8", FieldType.STRING, 8);
        cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme", 
            "UNIT-EN-RTE-TO-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_EN_RTE_TO_DTE_TME");
        cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme.setDdmHeader("UNIT EN ROUTE TO DATE TIME");
        cwf_Master_Index_View_Acknwldgmnt_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Acknwldgmnt_Cde", "ACKNWLDGMNT-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ACKNWLDGMNT_CDE");
        cwf_Master_Index_View_Acknwldgmnt_Cde.setDdmHeader("ACKNOWLEDGEMENT/CODE");
        cwf_Master_Index_View_Acknwldgmnt_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Acknwldgmnt_Oprtr_Cde", 
            "ACKNWLDGMNT-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ACKNWLDGMNT_OPRTR_CDE");
        cwf_Master_Index_View_Acknwldgmnt_Oprtr_Cde.setDdmHeader("ACKNOWLEDGED/BY");
        cwf_Master_Index_View_Acknwldgmnt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Acknwldgmnt_Dte_Tme", 
            "ACKNWLDGMNT-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "ACKNWLDGMNT_DTE_TME");
        cwf_Master_Index_View_Acknwldgmnt_Dte_Tme.setDdmHeader("ACKNOWLEDGED/ON");
        cwf_Master_Index_View_Cntct_Sheet_Print_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Sheet_Print_Cde", 
            "CNTCT-SHEET-PRINT-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_CDE");
        cwf_Master_Index_View_Cntct_Sheet_Print_Cde.setDdmHeader("CONTACT SHEET/PRINT CODE");
        cwf_Master_Index_View_Cntct_Sheet_Print_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Sheet_Print_Dte_Tme", 
            "CNTCT-SHEET-PRINT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_DTE_TME");
        cwf_Master_Index_View_Cntct_Sheet_Printer_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Sheet_Printer_Id_Cde", 
            "CNTCT-SHEET-PRINTER-ID-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINTER_ID_CDE");
        cwf_Master_Index_View_Shphrd_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Shphrd_Id", "SHPHRD-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "SHPHRD_ID");
        cwf_Master_Index_View_Shphrd_Id.setDdmHeader("SHEPHERD");
        cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme", 
            "CRRNT-DUE-DTE-CMT-PRTY-TME", FieldType.STRING, 17, RepeatingFieldStrategy.None, "CRRNT_DUE_DTE_CMT_PRTY_TME");
        cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme.setDdmHeader("CURR-DUE/TME-FIELD");

        cwf_Master_Index_View__R_Field_14 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_14", "REDEFINE", cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme);
        cwf_Master_Index_View_Crrnt_Due_Dte = cwf_Master_Index_View__R_Field_14.newFieldInGroup("cwf_Master_Index_View_Crrnt_Due_Dte", "CRRNT-DUE-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Crrnt_Cmt_Ind = cwf_Master_Index_View__R_Field_14.newFieldInGroup("cwf_Master_Index_View_Crrnt_Cmt_Ind", "CRRNT-CMT-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Crrnt_Prrty_Cde = cwf_Master_Index_View__R_Field_14.newFieldInGroup("cwf_Master_Index_View_Crrnt_Prrty_Cde", "CRRNT-PRRTY-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Crrnt_Due_Tme = cwf_Master_Index_View__R_Field_14.newFieldInGroup("cwf_Master_Index_View_Crrnt_Due_Tme", "CRRNT-DUE-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_View_Crprte_Due_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CRPRTE_DUE_DTE_TME");
        cwf_Master_Index_View_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Master_Index_View_Archvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Archvd_Dte", "ARCHVD-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ARCHVD_DTE");
        cwf_Master_Index_View_Archvd_Dte.setDdmHeader("ARCHIVED/DATE");
        cwf_Master_Index_View_Rstr_To_Crrnt_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rstr_To_Crrnt_Dte", "RSTR-TO-CRRNT-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RSTR_TO_CRRNT_DTE");
        cwf_Master_Index_View_Rstr_To_Crrnt_Dte.setDdmHeader("RESTORE/DATE");
        cwf_Master_Index_View_Off_Rtng_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Off_Rtng_Ind", "OFF-RTNG-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "OFF_RTNG_IND");
        cwf_Master_Index_View_Off_Rtng_Ind.setDdmHeader("OFF-ROUTING");
        cwf_Master_Index_View_Unit_On_Tme_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_On_Tme_Ind", "UNIT-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "UNIT_ON_TME_IND");
        cwf_Master_Index_View_Crprte_On_Tme_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_ON_TME_IND");
        cwf_Master_Index_View_Crprte_On_Tme_Ind.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Master_Index_View_Owner_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Owner_Unit_Cde", "OWNER-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "OWNER_UNIT_CDE");
        cwf_Master_Index_View_Owner_Unit_Cde.setDdmHeader("OWNERUNIT");
        cwf_Master_Index_View_Physcl_Fldr_Owner_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Physcl_Fldr_Owner_Id", 
            "PHYSCL-FLDR-OWNER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "PHYSCL_FLDR_OWNER_ID");
        cwf_Master_Index_View_Physcl_Fldr_Owner_Id.setDdmHeader("PHYSICAL/FOLDER-OWNER");
        cwf_Master_Index_View_Physcl_Fldr_Owner_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Physcl_Fldr_Owner_Unit_Cde", 
            "PHYSCL-FLDR-OWNER-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "PHYSCL_FLDR_OWNER_UNIT_CDE");
        cwf_Master_Index_View_Physcl_Fldr_Owner_Unit_Cde.setDdmHeader("PHYSICAL/FOLDER-UNIT");
        cwf_Master_Index_View_Step_Re_Do_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Re_Do_Ind", "STEP-RE-DO-IND", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "STEP_RE_DO_IND");
        cwf_Master_Index_View_Step_Re_Do_Ind.setDdmHeader("REDO/COUNT");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        cwf_Master_Index_View_Trade_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trade_Dte_Tme", "TRADE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TRADE_DTE_TME");
        cwf_Master_Index_View_Trade_Dte_Tme.setDdmHeader("TRADE/DTE-TME");
        cwf_Master_Index_View_Future_Pymnt_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Future_Pymnt_Ind", "FUTURE-PYMNT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "FUTURE_PYMNT_IND");
        cwf_Master_Index_View_Future_Pymnt_Ind.setDdmHeader("ACKNOWLEDGEMENT/PAYMENT-DTE");
        cwf_Master_Index_View_Status_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Clock_Start_Dte_Tme", 
            "STATUS-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STATUS_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Status_Clock_Start_Dte_Tme.setDdmHeader("STATUS/START CLOCK");
        cwf_Master_Index_View_Status_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Clock_End_Dte_Tme", 
            "STATUS-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STATUS_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Status_Clock_End_Dte_Tme.setDdmHeader("STATUS/CLOCK END");
        cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme", 
            "STATUS-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "STATUS_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme.setDdmHeader("STATUS ACTIVITY/TURNAROUND");

        cwf_Master_Index_View__R_Field_15 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_15", "REDEFINE", cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme);
        cwf_Master_Index_View_Status_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_15.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Clndr_Days", 
            "STATUS-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Status_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_15.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Clndr_Hours", 
            "STATUS-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Status_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_15.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Clndr_Minutes", 
            "STATUS-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme", 
            "STATUS-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "STATUS_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme.setDdmHeader("STATUS/(BUSINESS DAYS)");

        cwf_Master_Index_View__R_Field_16 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_16", "REDEFINE", cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_16.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Bsnss_Days", 
            "STATUS-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_16.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Bsnss_Hours", 
            "STATUS-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_16.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Bsnss_Minutes", 
            "STATUS-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme", 
            "STEP-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "STEP_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme.setDdmHeader("STEP WAIT TIME");

        cwf_Master_Index_View__R_Field_17 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_17", "REDEFINE", cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme);
        cwf_Master_Index_View_Step_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_17.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Clndr_Days", 
            "STEP-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Step_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_17.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Clndr_Hours", 
            "STEP-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Step_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_17.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Clndr_Minutes", 
            "STEP-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme", 
            "STEP-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "STEP_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme.setDdmHeader("STEP PROCESS TIME");

        cwf_Master_Index_View__R_Field_18 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_18", "REDEFINE", cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_18.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Bsnss_Days", 
            "STEP-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_18.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Bsnss_Hours", 
            "STEP-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_18.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Bsnss_Minutes", 
            "STEP-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme", 
            "EMPL-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "EMPL_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme.setDdmHeader("ASSOCIATE WAIT TIME");

        cwf_Master_Index_View__R_Field_19 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_19", "REDEFINE", cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme);
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_19.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Clndr_Days", 
            "EMPL-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_19.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Clndr_Hours", 
            "EMPL-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_19.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Clndr_Minutes", 
            "EMPL-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme", 
            "EMPL-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "EMPL_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme.setDdmHeader("ASSOCIATE PROCESS TIME");

        cwf_Master_Index_View__R_Field_20 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_20", "REDEFINE", cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_20.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days", 
            "EMPL-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_20.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Bsnss_Hours", 
            "EMPL-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_20.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Bsnss_Minutes", 
            "EMPL-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme", 
            "UNIT-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "UNIT_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme.setDdmHeader("UNIT WAIT TIME");

        cwf_Master_Index_View__R_Field_21 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_21", "REDEFINE", cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme);
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_21.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Clndr_Days", 
            "UNIT-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_21.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Clndr_Hours", 
            "UNIT-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_21.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Clndr_Minutes", 
            "UNIT-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme", 
            "UNIT-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "UNIT_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme.setDdmHeader("UNIT PROCESS TIME");

        cwf_Master_Index_View__R_Field_22 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_22", "REDEFINE", cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_22.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days", 
            "UNIT-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_22.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Bsnss_Hours", 
            "UNIT-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_22.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Bsnss_Minutes", 
            "UNIT-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme", 
            "INTRNL-PND-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "INTRNL_PND_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme.setDdmHeader("INTERNAL-PEND/(CALENDAR DAYS)");

        cwf_Master_Index_View__R_Field_23 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_23", "REDEFINE", cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_23.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Days", 
            "INTRNL-PND-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_23.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Hours", 
            "INTRNL-PND-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_23.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Minutes", 
            "INTRNL-PND-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme", 
            "INTRNL-PND-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "INTRNL_PND_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme.setDdmHeader("INTERNAL PEND/(BUSSINESS DAYS)");

        cwf_Master_Index_View__R_Field_24 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_24", "REDEFINE", cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_24.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Days", 
            "INTRNL-PND-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_24.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Hours", 
            "INTRNL-PND-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_24.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Minutes", 
            "INTRNL-PND-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        registerRecord(vw_cwf_Master_Index_View);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);
        pnd_Header2_1 = localVariables.newFieldInRecord("pnd_Header2_1", "#HEADER2-1", FieldType.STRING, 50);
        pnd_Header2_2 = localVariables.newFieldInRecord("pnd_Header2_2", "#HEADER2-2", FieldType.STRING, 50);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_25 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_25", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_Wpid = cwf_Support_Tbl__R_Field_25.newFieldInGroup("cwf_Support_Tbl_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        cwf_Support_Tbl__R_Field_26 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_26", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_Unit = cwf_Support_Tbl__R_Field_26.newFieldInGroup("cwf_Support_Tbl_Tbl_Unit", "TBL-UNIT", FieldType.STRING, 8);

        cwf_Support_Tbl__R_Field_27 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_27", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Date_Ind = cwf_Support_Tbl__R_Field_27.newFieldInGroup("cwf_Support_Tbl_Date_Ind", "DATE-IND", FieldType.STRING, 1);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Prtcpnt_Closed_Key = localVariables.newFieldInRecord("pnd_Prtcpnt_Closed_Key", "#PRTCPNT-CLOSED-KEY", FieldType.STRING, 10);

        pnd_Prtcpnt_Closed_Key__R_Field_28 = localVariables.newGroupInRecord("pnd_Prtcpnt_Closed_Key__R_Field_28", "REDEFINE", pnd_Prtcpnt_Closed_Key);
        pnd_Prtcpnt_Closed_Key_Pnd_Crprte_Clock_End_Dte = pnd_Prtcpnt_Closed_Key__R_Field_28.newFieldInGroup("pnd_Prtcpnt_Closed_Key_Pnd_Crprte_Clock_End_Dte", 
            "#CRPRTE-CLOCK-END-DTE", FieldType.STRING, 8);
        pnd_Prtcpnt_Closed_Key_Pnd_Status_Freeze_Ind = pnd_Prtcpnt_Closed_Key__R_Field_28.newFieldInGroup("pnd_Prtcpnt_Closed_Key_Pnd_Status_Freeze_Ind", 
            "#STATUS-FREEZE-IND", FieldType.STRING, 1);
        pnd_Wpid_Prime_Key = localVariables.newFieldInRecord("pnd_Wpid_Prime_Key", "#WPID-PRIME-KEY", FieldType.STRING, 28);

        pnd_Wpid_Prime_Key__R_Field_29 = localVariables.newGroupInRecord("pnd_Wpid_Prime_Key__R_Field_29", "REDEFINE", pnd_Wpid_Prime_Key);
        pnd_Wpid_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Wpid_Prime_Key__R_Field_29.newFieldInGroup("pnd_Wpid_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Wpid_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Wpid_Prime_Key__R_Field_29.newFieldInGroup("pnd_Wpid_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Wpid_Prime_Key_Pnd_Tbl_Wpid = pnd_Wpid_Prime_Key__R_Field_29.newFieldInGroup("pnd_Wpid_Prime_Key_Pnd_Tbl_Wpid", "#TBL-WPID", FieldType.STRING, 
            6);
        pnd_Unit_Prime_Key = localVariables.newFieldInRecord("pnd_Unit_Prime_Key", "#UNIT-PRIME-KEY", FieldType.STRING, 28);

        pnd_Unit_Prime_Key__R_Field_30 = localVariables.newGroupInRecord("pnd_Unit_Prime_Key__R_Field_30", "REDEFINE", pnd_Unit_Prime_Key);
        pnd_Unit_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Unit_Prime_Key__R_Field_30.newFieldInGroup("pnd_Unit_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Unit_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Unit_Prime_Key__R_Field_30.newFieldInGroup("pnd_Unit_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Unit_Prime_Key_Pnd_Tbl_Unit = pnd_Unit_Prime_Key__R_Field_30.newFieldInGroup("pnd_Unit_Prime_Key_Pnd_Tbl_Unit", "#TBL-UNIT", FieldType.STRING, 
            6);
        pnd_Date_Prime_Key = localVariables.newFieldInRecord("pnd_Date_Prime_Key", "#DATE-PRIME-KEY", FieldType.STRING, 28);

        pnd_Date_Prime_Key__R_Field_31 = localVariables.newGroupInRecord("pnd_Date_Prime_Key__R_Field_31", "REDEFINE", pnd_Date_Prime_Key);
        pnd_Date_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Date_Prime_Key__R_Field_31.newFieldInGroup("pnd_Date_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Date_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Date_Prime_Key__R_Field_31.newFieldInGroup("pnd_Date_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Date_Prime_Key_Pnd_Tbl_Key_Field = pnd_Date_Prime_Key__R_Field_31.newFieldInGroup("pnd_Date_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 1);
        pnd_Valid_Wpid_Tbl = localVariables.newFieldArrayInRecord("pnd_Valid_Wpid_Tbl", "#VALID-WPID-TBL", FieldType.STRING, 6, new DbsArrayController(1, 
            50));
        pnd_Valid_Unit_Tbl = localVariables.newFieldArrayInRecord("pnd_Valid_Unit_Tbl", "#VALID-UNIT-TBL", FieldType.STRING, 8, new DbsArrayController(1, 
            100));

        pnd_Totals = localVariables.newGroupInRecord("pnd_Totals", "#TOTALS");
        pnd_Totals_Pnd_Total_Work = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Work", "#TOTAL-WORK", FieldType.PACKED_DECIMAL, 7);
        pnd_Totals_Pnd_0_3_Total = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_0_3_Total", "#0-3-TOTAL", FieldType.PACKED_DECIMAL, 7);
        pnd_Totals_Pnd_4_7_Total = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_4_7_Total", "#4-7-TOTAL", FieldType.PACKED_DECIMAL, 7);
        pnd_Totals_Pnd_0_7_Total = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_0_7_Total", "#0-7-TOTAL", FieldType.PACKED_DECIMAL, 7);
        pnd_Totals_Pnd_8_10_Total = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_8_10_Total", "#8-10-TOTAL", FieldType.PACKED_DECIMAL, 7);
        pnd_Totals_Pnd_Gt_10_Total = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Gt_10_Total", "#GT-10-TOTAL", FieldType.PACKED_DECIMAL, 7);

        pnd_Gtotals = localVariables.newGroupInRecord("pnd_Gtotals", "#GTOTALS");
        pnd_Gtotals_Pnd_Total_Work = pnd_Gtotals.newFieldInGroup("pnd_Gtotals_Pnd_Total_Work", "#TOTAL-WORK", FieldType.PACKED_DECIMAL, 7);
        pnd_Gtotals_Pnd_0_3_Total = pnd_Gtotals.newFieldInGroup("pnd_Gtotals_Pnd_0_3_Total", "#0-3-TOTAL", FieldType.PACKED_DECIMAL, 7);
        pnd_Gtotals_Pnd_4_7_Total = pnd_Gtotals.newFieldInGroup("pnd_Gtotals_Pnd_4_7_Total", "#4-7-TOTAL", FieldType.PACKED_DECIMAL, 7);
        pnd_Gtotals_Pnd_0_7_Total = pnd_Gtotals.newFieldInGroup("pnd_Gtotals_Pnd_0_7_Total", "#0-7-TOTAL", FieldType.PACKED_DECIMAL, 7);
        pnd_Gtotals_Pnd_8_10_Total = pnd_Gtotals.newFieldInGroup("pnd_Gtotals_Pnd_8_10_Total", "#8-10-TOTAL", FieldType.PACKED_DECIMAL, 7);
        pnd_Gtotals_Pnd_Gt_10_Total = pnd_Gtotals.newFieldInGroup("pnd_Gtotals_Pnd_Gt_10_Total", "#GT-10-TOTAL", FieldType.PACKED_DECIMAL, 7);

        pnd_Pcts = localVariables.newGroupInRecord("pnd_Pcts", "#PCTS");
        pnd_Pcts_Pnd_Total_Pct = pnd_Pcts.newFieldInGroup("pnd_Pcts_Pnd_Total_Pct", "#TOTAL-PCT", FieldType.NUMERIC, 9, 6);
        pnd_Pcts_Pnd_0_3_Pct = pnd_Pcts.newFieldInGroup("pnd_Pcts_Pnd_0_3_Pct", "#0-3-PCT", FieldType.NUMERIC, 9, 6);
        pnd_Pcts_Pnd_4_7_Pct = pnd_Pcts.newFieldInGroup("pnd_Pcts_Pnd_4_7_Pct", "#4-7-PCT", FieldType.NUMERIC, 9, 6);
        pnd_Pcts_Pnd_0_7_Pct = pnd_Pcts.newFieldInGroup("pnd_Pcts_Pnd_0_7_Pct", "#0-7-PCT", FieldType.NUMERIC, 9, 6);
        pnd_Pcts_Pnd_8_10_Pct = pnd_Pcts.newFieldInGroup("pnd_Pcts_Pnd_8_10_Pct", "#8-10-PCT", FieldType.NUMERIC, 9, 6);
        pnd_Pcts_Pnd_Gt_10_Pct = pnd_Pcts.newFieldInGroup("pnd_Pcts_Pnd_Gt_10_Pct", "#GT-10-PCT", FieldType.NUMERIC, 9, 6);

        pnd_Tot_Pcts = localVariables.newGroupInRecord("pnd_Tot_Pcts", "#TOT-PCTS");
        pnd_Tot_Pcts_Pnd_Total_Pct = pnd_Tot_Pcts.newFieldInGroup("pnd_Tot_Pcts_Pnd_Total_Pct", "#TOTAL-PCT", FieldType.NUMERIC, 9, 6);
        pnd_Tot_Pcts_Pnd_0_3_Pct = pnd_Tot_Pcts.newFieldInGroup("pnd_Tot_Pcts_Pnd_0_3_Pct", "#0-3-PCT", FieldType.NUMERIC, 9, 6);
        pnd_Tot_Pcts_Pnd_4_7_Pct = pnd_Tot_Pcts.newFieldInGroup("pnd_Tot_Pcts_Pnd_4_7_Pct", "#4-7-PCT", FieldType.NUMERIC, 9, 6);
        pnd_Tot_Pcts_Pnd_0_7_Pct = pnd_Tot_Pcts.newFieldInGroup("pnd_Tot_Pcts_Pnd_0_7_Pct", "#0-7-PCT", FieldType.NUMERIC, 9, 6);
        pnd_Tot_Pcts_Pnd_8_10_Pct = pnd_Tot_Pcts.newFieldInGroup("pnd_Tot_Pcts_Pnd_8_10_Pct", "#8-10-PCT", FieldType.NUMERIC, 9, 6);
        pnd_Tot_Pcts_Pnd_Gt_10_Pct = pnd_Tot_Pcts.newFieldInGroup("pnd_Tot_Pcts_Pnd_Gt_10_Pct", "#GT-10-PCT", FieldType.NUMERIC, 9, 6);

        pnd_Disp_Tots = localVariables.newGroupInRecord("pnd_Disp_Tots", "#DISP-TOTS");
        pnd_Disp_Tots_Pnd_D_Total = pnd_Disp_Tots.newFieldInGroup("pnd_Disp_Tots_Pnd_D_Total", "#D-TOTAL", FieldType.PACKED_DECIMAL, 7);
        pnd_Disp_Tots_Pnd_D_0_3 = pnd_Disp_Tots.newFieldInGroup("pnd_Disp_Tots_Pnd_D_0_3", "#D-0-3", FieldType.PACKED_DECIMAL, 7);
        pnd_Disp_Tots_Pnd_D_4_7 = pnd_Disp_Tots.newFieldInGroup("pnd_Disp_Tots_Pnd_D_4_7", "#D-4-7", FieldType.PACKED_DECIMAL, 7);
        pnd_Disp_Tots_Pnd_D_0_7 = pnd_Disp_Tots.newFieldInGroup("pnd_Disp_Tots_Pnd_D_0_7", "#D-0-7", FieldType.PACKED_DECIMAL, 7);
        pnd_Disp_Tots_Pnd_D_8_10 = pnd_Disp_Tots.newFieldInGroup("pnd_Disp_Tots_Pnd_D_8_10", "#D-8-10", FieldType.PACKED_DECIMAL, 7);
        pnd_Disp_Tots_Pnd_D_Gt_10 = pnd_Disp_Tots.newFieldInGroup("pnd_Disp_Tots_Pnd_D_Gt_10", "#D-GT-10", FieldType.PACKED_DECIMAL, 7);

        pnd_Disp_Pcts = localVariables.newGroupInRecord("pnd_Disp_Pcts", "#DISP-PCTS");
        pnd_Disp_Pcts_Pnd_D_Total_Pct = pnd_Disp_Pcts.newFieldInGroup("pnd_Disp_Pcts_Pnd_D_Total_Pct", "#D-TOTAL-PCT", FieldType.NUMERIC, 5, 2);
        pnd_Disp_Pcts_Pnd_D_0_3_Pct = pnd_Disp_Pcts.newFieldInGroup("pnd_Disp_Pcts_Pnd_D_0_3_Pct", "#D-0-3-PCT", FieldType.NUMERIC, 5, 2);
        pnd_Disp_Pcts_Pnd_D_4_7_Pct = pnd_Disp_Pcts.newFieldInGroup("pnd_Disp_Pcts_Pnd_D_4_7_Pct", "#D-4-7-PCT", FieldType.NUMERIC, 5, 2);
        pnd_Disp_Pcts_Pnd_D_0_7_Pct = pnd_Disp_Pcts.newFieldInGroup("pnd_Disp_Pcts_Pnd_D_0_7_Pct", "#D-0-7-PCT", FieldType.NUMERIC, 5, 2);
        pnd_Disp_Pcts_Pnd_D_8_10_Pct = pnd_Disp_Pcts.newFieldInGroup("pnd_Disp_Pcts_Pnd_D_8_10_Pct", "#D-8-10-PCT", FieldType.NUMERIC, 5, 2);
        pnd_Disp_Pcts_Pnd_D_Gt_10_Pct = pnd_Disp_Pcts.newFieldInGroup("pnd_Disp_Pcts_Pnd_D_Gt_10_Pct", "#D-GT-10-PCT", FieldType.NUMERIC, 5, 2);
        pnd_Run_Date = localVariables.newFieldInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.STRING, 8);
        pnd_Fix_Date = localVariables.newFieldInRecord("pnd_Fix_Date", "#FIX-DATE", FieldType.STRING, 8);

        pnd_Fix_Date__R_Field_32 = localVariables.newGroupInRecord("pnd_Fix_Date__R_Field_32", "REDEFINE", pnd_Fix_Date);
        pnd_Fix_Date_Pnd_Fix_Cc = pnd_Fix_Date__R_Field_32.newFieldInGroup("pnd_Fix_Date_Pnd_Fix_Cc", "#FIX-CC", FieldType.NUMERIC, 2);
        pnd_Fix_Date_Pnd_Fix_Yy = pnd_Fix_Date__R_Field_32.newFieldInGroup("pnd_Fix_Date_Pnd_Fix_Yy", "#FIX-YY", FieldType.NUMERIC, 2);
        pnd_Fix_Date_Pnd_Fix_Mm = pnd_Fix_Date__R_Field_32.newFieldInGroup("pnd_Fix_Date_Pnd_Fix_Mm", "#FIX-MM", FieldType.NUMERIC, 2);
        pnd_Fix_Date_Pnd_Fix_Dd = pnd_Fix_Date__R_Field_32.newFieldInGroup("pnd_Fix_Date_Pnd_Fix_Dd", "#FIX-DD", FieldType.NUMERIC, 2);
        pnd_Convert_Date = localVariables.newFieldInRecord("pnd_Convert_Date", "#CONVERT-DATE", FieldType.DATE);
        pnd_Start_Date_A = localVariables.newFieldInRecord("pnd_Start_Date_A", "#START-DATE-A", FieldType.STRING, 8);

        pnd_Start_Date_A__R_Field_33 = localVariables.newGroupInRecord("pnd_Start_Date_A__R_Field_33", "REDEFINE", pnd_Start_Date_A);
        pnd_Start_Date_A_Pnd_Start_Yyyy = pnd_Start_Date_A__R_Field_33.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Yyyy", "#START-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Start_Date_A_Pnd_Start_Mm = pnd_Start_Date_A__R_Field_33.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Mm", "#START-MM", FieldType.NUMERIC, 
            2);
        pnd_Start_Date_A_Pnd_Start_Dd = pnd_Start_Date_A__R_Field_33.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Dd", "#START-DD", FieldType.NUMERIC, 
            2);
        pnd_End_Date_A = localVariables.newFieldInRecord("pnd_End_Date_A", "#END-DATE-A", FieldType.STRING, 8);

        pnd_End_Date_A__R_Field_34 = localVariables.newGroupInRecord("pnd_End_Date_A__R_Field_34", "REDEFINE", pnd_End_Date_A);
        pnd_End_Date_A_Pnd_End_Yyyy = pnd_End_Date_A__R_Field_34.newFieldInGroup("pnd_End_Date_A_Pnd_End_Yyyy", "#END-YYYY", FieldType.NUMERIC, 4);
        pnd_End_Date_A_Pnd_End_Mm = pnd_End_Date_A__R_Field_34.newFieldInGroup("pnd_End_Date_A_Pnd_End_Mm", "#END-MM", FieldType.NUMERIC, 2);
        pnd_End_Date_A_Pnd_End_Dd = pnd_End_Date_A__R_Field_34.newFieldInGroup("pnd_End_Date_A_Pnd_End_Dd", "#END-DD", FieldType.NUMERIC, 2);
        pnd_Disp_Unit = localVariables.newFieldInRecord("pnd_Disp_Unit", "#DISP-UNIT", FieldType.STRING, 9);
        pnd_Disp_Date = localVariables.newFieldInRecord("pnd_Disp_Date", "#DISP-DATE", FieldType.STRING, 10);
        pnd_Check_Clock_End_Dte_Tme = localVariables.newFieldInRecord("pnd_Check_Clock_End_Dte_Tme", "#CHECK-CLOCK-END-DTE-TME", FieldType.STRING, 15);

        pnd_Check_Clock_End_Dte_Tme__R_Field_35 = localVariables.newGroupInRecord("pnd_Check_Clock_End_Dte_Tme__R_Field_35", "REDEFINE", pnd_Check_Clock_End_Dte_Tme);
        pnd_Check_Clock_End_Dte_Tme_Pnd_Check_Clock_End_Dte = pnd_Check_Clock_End_Dte_Tme__R_Field_35.newFieldInGroup("pnd_Check_Clock_End_Dte_Tme_Pnd_Check_Clock_End_Dte", 
            "#CHECK-CLOCK-END-DTE", FieldType.STRING, 8);
        pnd_No_Records_Found = localVariables.newFieldInRecord("pnd_No_Records_Found", "#NO-RECORDS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Max_Valid_Wpids = localVariables.newFieldInRecord("pnd_Max_Valid_Wpids", "#MAX-VALID-WPIDS", FieldType.NUMERIC, 2);
        pnd_Max_Valid_Units = localVariables.newFieldInRecord("pnd_Max_Valid_Units", "#MAX-VALID-UNITS", FieldType.NUMERIC, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 2);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.STRING, 1);
        pnd_Keep_It = localVariables.newFieldInRecord("pnd_Keep_It", "#KEEP-IT", FieldType.BOOLEAN, 1);
        pnd_End_Of_Report = localVariables.newFieldInRecord("pnd_End_Of_Report", "#END-OF-REPORT", FieldType.BOOLEAN, 1);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 132);
        pnd_Errors = localVariables.newFieldInRecord("pnd_Errors", "#ERRORS", FieldType.PACKED_DECIMAL, 5);
        pnd_Prev_Unit = localVariables.newFieldInRecord("pnd_Prev_Unit", "#PREV-UNIT", FieldType.STRING, 8);
        pnd_Unit_Cde = localVariables.newFieldInRecord("pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 8);
        pnd_Base_Dte_Tme = localVariables.newFieldInRecord("pnd_Base_Dte_Tme", "#BASE-DTE-TME", FieldType.TIME);
        pnd_End_Dte_Tme = localVariables.newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.TIME);
        pnd_Elapsed_Days = localVariables.newFieldInRecord("pnd_Elapsed_Days", "#ELAPSED-DAYS", FieldType.NUMERIC, 3);
        pnd_Elapsed_Hrs = localVariables.newFieldInRecord("pnd_Elapsed_Hrs", "#ELAPSED-HRS", FieldType.NUMERIC, 2);
        pnd_Elapsed_Mins = localVariables.newFieldInRecord("pnd_Elapsed_Mins", "#ELAPSED-MINS", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index_View.reset();
        vw_cwf_Support_Tbl.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("               Corporate Work Flow");
        pnd_Header1_2.setInitialValue("    Monthly Application Processing Turnaround");
        pnd_Header2_1.setInitialValue("               Corporate Work Flow");
        pnd_Header2_2.setInitialValue(" Monthly Application Processing Turnaroun Errors");
        pnd_Wpid_Prime_Key.setInitialValue("A APP-PROCESSING-WPIDS");
        pnd_Unit_Prime_Key.setInitialValue("A APP-PROCESSING-UNITS");
        pnd_Date_Prime_Key.setInitialValue("A CWF-RPRT-RUN-TBL    D");
        pnd_Max_Valid_Wpids.setInitialValue(50);
        pnd_Max_Valid_Units.setInitialValue(100);
        pnd_Y.setInitialValue("Y");
        pnd_Keep_It.setInitialValue(false);
        pnd_End_Of_Report.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3804() throws Exception
    {
        super("Cwfb3804");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Cwfb3804|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        while(true)
        {
            try
            {
                //*  DEFINE PRINTERS AND FORMATS
                getReports().definePrinter(2, "NOT DEFINED");                                                                                                             //Natural: DEFINE PRINTER ( 1 )
                getReports().definePrinter(3, "NOT DEFINED");                                                                                                             //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: DEFINE PRINTER ( 2 )
                //*                                                                                                                                                       //Natural: FORMAT ( 2 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN #PROGRAM = *PROGRAM
                //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
                pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                              //Natural: ASSIGN #CUR-LANG = *LANGUAGE
                pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                       //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
                //* *SAG DEFINE EXIT START-OF-PROGRAM
                //* *SAG END-EXIT
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 )
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH' THEN
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA = 'INFP9000'
                //* ***********************
                //*   MAIN PROGRAM LOGIC  *
                //* ***********************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, new FieldAttributes ("AD=MITL'_'"), new SignPosition (true), new InputPrompting(false),                //Natural: INPUT ( AD = MITL'_' SG = ON IP = OFF ZP = OFF ) 'Start Date:' #START-DATE
                    new ReportZeroPrint (false),"Start Date:",pnd_Start_Date);
                //* *SAG DEFINE EXIT BEFORE-READ
                short decideConditionsMet568 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #START-DATE NE ' '
                if (condition(pnd_Start_Date.notEquals(" ")))
                {
                    decideConditionsMet568++;
                    if (condition(! (DbsUtil.maskMatches(pnd_Start_Date,"YYYYMMDD"))))                                                                                    //Natural: IF #START-DATE NE MASK ( YYYYMMDD )
                    {
                        getReports().write(0, "**************** INVALID START DATE ******************");                                                                  //Natural: WRITE '**************** INVALID START DATE ******************'
                        if (Global.isEscape()) return;
                        getReports().write(2, ReportOption.NOTITLE,"************* INVALID START DATE ****************");                                                  //Natural: WRITE ( 2 ) '************* INVALID START DATE ****************'
                        if (Global.isEscape()) return;
                        DbsUtil.terminate(33);  if (true) return;                                                                                                         //Natural: TERMINATE 33
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Start_Date_A.setValue(pnd_Start_Date);                                                                                                        //Natural: ASSIGN #START-DATE-A := #START-DATE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #START-DATE EQ ' '
                else if (condition(pnd_Start_Date.equals(" ")))
                {
                    decideConditionsMet568++;
                                                                                                                                                                          //Natural: PERFORM GET-START-DATE
                    sub_Get_Start_Date();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Start_Date_A_Pnd_Start_Dd.setValue(1);                                                                                                            //Natural: ASSIGN #START-DATE-A.#START-DD := 1
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM GET-END-DATE
                sub_Get_End_Date();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(0, "Start Date is:",pnd_Start_Date_A,NEWLINE,"End date is  :",pnd_End_Date_A);                                                         //Natural: WRITE 'Start Date is:' #START-DATE-A / 'End date is  :' #END-DATE-A
                if (Global.isEscape()) return;
                //*      GET DATE INTO MM/DD/YY FORMAT FOR REPORT HEADER
                //*  GO GET VALID WPIDS
                                                                                                                                                                          //Natural: PERFORM LOAD-WPID-TABLE
                sub_Load_Wpid_Table();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  VALUE = 'Y'
                                                                                                                                                                          //Natural: PERFORM LOAD-UNIT-TABLE
                sub_Load_Unit_Table();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Prtcpnt_Closed_Key_Pnd_Crprte_Clock_End_Dte.setValue(pnd_Start_Date_A);                                                                               //Natural: ASSIGN #PRTCPNT-CLOSED-KEY.#CRPRTE-CLOCK-END-DTE := #START-DATE-A
                pnd_Prtcpnt_Closed_Key_Pnd_Status_Freeze_Ind.setValue(pnd_Y);                                                                                             //Natural: ASSIGN #PRTCPNT-CLOSED-KEY.#STATUS-FREEZE-IND := #Y
                //* *SAG END-EXIT
                //*  PRIMARY FILE
                vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                //Natural: READ CWF-MASTER-INDEX-VIEW BY PRTCPNT-CLOSED-KEY STARTING FROM #PRTCPNT-CLOSED-KEY
                (
                "READ_PRIME",
                new Wc[] { new Wc("PRTCPNT_CLOSED_KEY", ">=", pnd_Prtcpnt_Closed_Key, WcType.BY) },
                new Oc[] { new Oc("PRTCPNT_CLOSED_KEY", "ASC") }
                );
                READ_PRIME:
                while (condition(vw_cwf_Master_Index_View.readNextRow("READ_PRIME")))
                {
                    //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
                    pnd_Check_Clock_End_Dte_Tme.setValue(cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme);                                                                 //Natural: ASSIGN #CHECK-CLOCK-END-DTE-TME := CWF-MASTER-INDEX-VIEW.CRPRTE-CLOCK-END-DTE-TME
                    //*  ARE WE PAST THIS MONTH's
                    if (condition(pnd_Check_Clock_End_Dte_Tme_Pnd_Check_Clock_End_Dte.greater(pnd_End_Date_A)))                                                           //Natural: IF #CHECK-CLOCK-END-DTE GT #END-DATE-A
                    {
                        //*  PARTICIPANT CLOSED RECORDS?
                        if (true) break READ_PRIME;                                                                                                                       //Natural: ESCAPE BOTTOM ( READ-PRIME. )
                        //*  IF SO - GET OUT WE're done
                    }                                                                                                                                                     //Natural: END-IF
                    //*  CHECK AGAINST THE TABLE OF WPIDS WE're interested in
                    pnd_Keep_It.resetInitial();                                                                                                                           //Natural: RESET INITIAL #KEEP-IT
                    short decideConditionsMet618 = 0;                                                                                                                     //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CWF-MASTER-INDEX-VIEW.STATUS-FREEZE-IND EQ #Y
                    if (condition(cwf_Master_Index_View_Status_Freeze_Ind.equals(pnd_Y)))
                    {
                        decideConditionsMet618++;
                        ignore();
                    }                                                                                                                                                     //Natural: WHEN CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID EQ #VALID-WPID-TBL ( * )
                    if (condition(cwf_Master_Index_View_Work_Prcss_Id.equals(pnd_Valid_Wpid_Tbl.getValue("*"))))
                    {
                        decideConditionsMet618++;
                        ignore();
                    }                                                                                                                                                     //Natural: WHEN CWF-MASTER-INDEX-VIEW.UNIT-CDE EQ #VALID-UNIT-TBL ( * )
                    if (condition(cwf_Master_Index_View_Unit_Cde.equals(pnd_Valid_Unit_Tbl.getValue("*"))))
                    {
                        decideConditionsMet618++;
                        ignore();
                    }                                                                                                                                                     //Natural: WHEN ALL
                    if (condition(decideConditionsMet618 == 3))
                    {
                        pnd_Keep_It.setValue(true);                                                                                                                       //Natural: ASSIGN #KEEP-IT := TRUE
                    }                                                                                                                                                     //Natural: WHEN NONE
                    if (condition(decideConditionsMet618 == 0))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(! (pnd_Keep_It.getBoolean())))                                                                                                          //Natural: REJECT IF NOT #KEEP-IT
                    {
                        continue;
                    }
                    //*       GO FIGURE OUT TURNAROUND TIME
                    short decideConditionsMet636 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-MASTER-INDEX-VIEW.EXTRNL-PEND-RCV-DTE GT 0
                    if (condition(cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte.greater(getZero())))
                    {
                        decideConditionsMet636++;
                        pnd_Base_Dte_Tme.setValue(cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte);                                                                             //Natural: ASSIGN #BASE-DTE-TME := CWF-MASTER-INDEX-VIEW.EXTRNL-PEND-RCV-DTE
                    }                                                                                                                                                     //Natural: WHEN CWF-MASTER-INDEX-VIEW.TIAA-RCVD-DTE-TME GT 0
                    else if (condition(cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme.greater(getZero())))
                    {
                        decideConditionsMet636++;
                        pnd_Base_Dte_Tme.setValue(cwf_Master_Index_View_Trade_Dte_Tme);                                                                                   //Natural: ASSIGN #BASE-DTE-TME := CWF-MASTER-INDEX-VIEW.TRADE-DTE-TME
                    }                                                                                                                                                     //Natural: WHEN ANY
                    if (condition(decideConditionsMet636 > 0))
                    {
                        pnd_End_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme);                             //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.CRPRTE-CLOCK-END-DTE-TME TO #END-DTE-TME ( EM = YYYYMMDDHHIISST )
                        DbsUtil.callnat(Cwfn9370.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pnd_Unit_Cde, pnd_Base_Dte_Tme, pnd_End_Dte_Tme,        //Natural: CALLNAT 'CWFN9370' MSG-INFO-SUB #UNIT-CDE #BASE-DTE-TME #END-DTE-TME #ELAPSED-DAYS #ELAPSED-HRS #ELAPSED-MINS
                            pnd_Elapsed_Days, pnd_Elapsed_Hrs, pnd_Elapsed_Mins);
                        if (condition(Global.isEscape())) return;
                        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
                        {
                            pnd_Error_Msg.setValue(DbsUtil.compress("Error getting turnaround time: Msg is:", pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg()));                //Natural: COMPRESS 'Error getting turnaround time: Msg is:' MSG-INFO-SUB.##MSG INTO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR
                            sub_Write_Error();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        pnd_Error_Msg.setValue(DbsUtil.compress("No External Pend date or Trade Date for:", "Pin:", cwf_Master_Index_View_Pin_Nbr, "WPID",                //Natural: COMPRESS 'No External Pend date or Trade Date for:' 'Pin:' CWF-MASTER-INDEX-VIEW.PIN-NBR 'WPID' CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID INTO #ERROR-MSG
                            cwf_Master_Index_View_Work_Prcss_Id));
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR
                        sub_Write_Error();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-DECIDE
                    getSort().writeSortInData(cwf_Master_Index_View_Unit_Cde, cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte, cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme,  //Natural: END-ALL
                        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme, pnd_Elapsed_Days);
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //* *SAG END-EXIT
                //* *SAG DEFINE EXIT SORT-FIELDS
                getSort().sortData(cwf_Master_Index_View_Unit_Cde);                                                                                                       //Natural: SORT BY CWF-MASTER-INDEX-VIEW.UNIT-CDE USING CWF-MASTER-INDEX-VIEW.EXTRNL-PEND-RCV-DTE CWF-MASTER-INDEX-VIEW.CRPRTE-CLOCK-END-DTE-TME CWF-MASTER-INDEX-VIEW.TIAA-RCVD-DTE-TME #ELAPSED-DAYS
                SORT01:
                while (condition(getSort().readSortOutData(cwf_Master_Index_View_Unit_Cde, cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte, cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme, 
                    cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme, pnd_Elapsed_Days)))
                {
                    //*        ADD TO APPROPRIATE BUCKET - PRINT REPORT BY UNIT
                    pnd_Gtotals_Pnd_Total_Work.nadd(1);                                                                                                                   //Natural: ADD 1 TO #GTOTALS.#TOTAL-WORK
                    short decideConditionsMet663 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PREV-UNIT = ' '
                    if (condition(pnd_Prev_Unit.equals(" ")))
                    {
                        decideConditionsMet663++;
                        pnd_Prev_Unit.setValue(cwf_Master_Index_View_Unit_Cde);                                                                                           //Natural: ASSIGN #PREV-UNIT := CWF-MASTER-INDEX-VIEW.UNIT-CDE
                    }                                                                                                                                                     //Natural: WHEN #PREV-UNIT NE CWF-MASTER-INDEX-VIEW.UNIT-CDE
                    else if (condition(pnd_Prev_Unit.notEquals(cwf_Master_Index_View_Unit_Cde)))
                    {
                        decideConditionsMet663++;
                                                                                                                                                                          //Natural: PERFORM CALCULATE-PERCENTS
                        sub_Calculate_Percents();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-REPORT
                        sub_Format_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
                        sub_Write_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-BUCKETS
                        sub_Reset_Buckets();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Prev_Unit.setValue(cwf_Master_Index_View_Unit_Cde);                                                                                           //Natural: ASSIGN #PREV-UNIT := CWF-MASTER-INDEX-VIEW.UNIT-CDE
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM ADD-TO-TOTALS
                    sub_Add_To_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //* *SAG END-EXIT
                }                                                                                                                                                         //Natural: END-SORT
                endSort();
                //* *SAG DEFINE EXIT END-OF-PROGRAM
                //*    WRITE OUT LAST UNIT
                                                                                                                                                                          //Natural: PERFORM CALCULATE-PERCENTS
                sub_Calculate_Percents();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-REPORT
                sub_Format_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
                sub_Write_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*    WRITE OUT TOTAL PAGE
                pnd_End_Of_Report.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #END-OF-REPORT
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TOTAL-PERCENTS
                sub_Calculate_Total_Percents();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-REPORT
                sub_Format_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
                sub_Write_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(56),"**** End of Report ****");                                         //Natural: WRITE ( 1 ) /// 56T '**** End of Report ****'
                if (Global.isEscape()) return;
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total Errors:",pnd_Errors);                                                                   //Natural: WRITE ( 2 ) // 'Total Errors:' #ERRORS
                if (Global.isEscape()) return;
                //* *SAG END-EXIT
                //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-WPID-TABLE
                //* *************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-UNIT-TABLE
                //* *************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-START-DATE
                //* *************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-END-DATE
                //* ***********************************************************************
                //* ***********************************************************************
                //* *************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR
                //* ***********************************************************************
                //* *************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-TO-TOTALS
                //* *************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-PERCENTS
                //* *************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TOTAL-PERCENTS
                //* *************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
                //* ***********************************************************************
                //* *************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-REPORT
                //* *************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-BUCKETS
                //* *************
                //* *SAG END-EXIT
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Load_Wpid_Table() throws Exception                                                                                                                   //Natural: LOAD-WPID-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  READ CWF-SUPPORT-TBL
        pnd_X.reset();                                                                                                                                                    //Natural: RESET #X
        pnd_No_Records_Found.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NO-RECORDS-FOUND
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #WPID-PRIME-KEY
        (
        "LOAD_WPID",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Wpid_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        LOAD_WPID:
        while (condition(vw_cwf_Support_Tbl.readNextRow("LOAD_WPID")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pnd_Wpid_Prime_Key_Pnd_Tbl_Table_Nme)))                                                                 //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME NE #WPID-PRIME-KEY.#TBL-TABLE-NME
            {
                if (true) break LOAD_WPID;                                                                                                                                //Natural: ESCAPE BOTTOM ( LOAD-WPID. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_No_Records_Found.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #NO-RECORDS-FOUND
            pnd_X.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #X
            if (condition(pnd_X.greater(pnd_Max_Valid_Wpids)))                                                                                                            //Natural: IF #X GT #MAX-VALID-WPIDS
            {
                getReports().write(0, "**************************************************");                                                                              //Natural: WRITE '**************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Maximum number (50) of WPID's Has Been Reached *");                                                                              //Natural: WRITE '* Maximum number (50) of WPID"s Has Been Reached *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Program CWFB3804 must be modified -- call MIS  *");                                                                              //Natural: WRITE '* Program CWFB3804 must be modified -- call MIS  *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "**************************************************");                                                                              //Natural: WRITE '**************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Valid_Wpid_Tbl.getValue(pnd_X).setValue(cwf_Support_Tbl_Tbl_Wpid);                                                                                        //Natural: ASSIGN #VALID-WPID-TBL ( #X ) := CWF-SUPPORT-TBL.TBL-WPID
            //*  LOAD-WPID.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_No_Records_Found.getBoolean()))                                                                                                                 //Natural: IF #NO-RECORDS-FOUND
        {
            pnd_Error_Msg.setValue("No Records on WPID Table:");                                                                                                          //Natural: MOVE 'No Records on WPID Table:' TO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR
            sub_Write_Error();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  LOAD-WPID-TABLE
    }
    private void sub_Load_Unit_Table() throws Exception                                                                                                                   //Natural: LOAD-UNIT-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  READ CWF-SUPPORT-TBL
        pnd_No_Records_Found.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NO-RECORDS-FOUND
        pnd_X.reset();                                                                                                                                                    //Natural: RESET #X
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #UNIT-PRIME-KEY
        (
        "LOAD_UNIT",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Unit_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        LOAD_UNIT:
        while (condition(vw_cwf_Support_Tbl.readNextRow("LOAD_UNIT")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pnd_Unit_Prime_Key_Pnd_Tbl_Table_Nme)))                                                                 //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME NE #UNIT-PRIME-KEY.#TBL-TABLE-NME
            {
                if (true) break LOAD_UNIT;                                                                                                                                //Natural: ESCAPE BOTTOM ( LOAD-UNIT. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_No_Records_Found.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #NO-RECORDS-FOUND
            pnd_X.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #X
            if (condition(pnd_X.greater(pnd_Max_Valid_Units)))                                                                                                            //Natural: IF #X GT #MAX-VALID-UNITS
            {
                getReports().write(0, "***************************************************");                                                                             //Natural: WRITE '***************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Maximum Number (100) of Unit's Has Been Reached *");                                                                             //Natural: WRITE '* Maximum Number (100) of Unit"s Has Been Reached *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Program CWFB3804 must be modified -- call MIS   *");                                                                             //Natural: WRITE '* Program CWFB3804 must be modified -- call MIS   *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "***************************************************");                                                                             //Natural: WRITE '***************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(35);  if (true) return;                                                                                                                 //Natural: TERMINATE 35
            }                                                                                                                                                             //Natural: END-IF
            pnd_Valid_Unit_Tbl.getValue(pnd_X).setValue(cwf_Support_Tbl_Tbl_Unit);                                                                                        //Natural: ASSIGN #VALID-UNIT-TBL ( #X ) := CWF-SUPPORT-TBL.TBL-UNIT
            //*  LOAD-WPID.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_No_Records_Found.getBoolean()))                                                                                                                 //Natural: IF #NO-RECORDS-FOUND
        {
            pnd_Error_Msg.setValue("No Records on Unit Table:");                                                                                                          //Natural: MOVE 'No Records on Unit Table:' TO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR
            sub_Write_Error();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  LOAD-UNIT-TABLE
    }
    private void sub_Get_Start_Date() throws Exception                                                                                                                    //Natural: GET-START-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  READ CWF-SUPPORT-TBL
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #DATE-PRIME-KEY
        (
        "GET_DATES",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Date_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        GET_DATES:
        while (condition(vw_cwf_Support_Tbl.readNextRow("GET_DATES")))
        {
            if (condition(cwf_Support_Tbl_Date_Ind.notEquals(pnd_Date_Prime_Key_Pnd_Tbl_Key_Field)))                                                                      //Natural: IF CWF-SUPPORT-TBL.DATE-IND NE #DATE-PRIME-KEY.#TBL-KEY-FIELD
            {
                if (true) break GET_DATES;                                                                                                                                //Natural: ESCAPE BOTTOM ( GET-DATES. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Run_Date.setValue(cwf_Support_Tbl_Tbl_Data_Field);                                                                                                        //Natural: ASSIGN #RUN-DATE := CWF-SUPPORT-TBL.TBL-DATA-FIELD
            //*  GET-DATES.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  IF NO DATE RECORD - FORCE ABEND WITH RC 33
        //*  IF NO INPUT START DATE  USE DAILY DATE RECORD ON CWF-RPRT-RUN-TBLE
        //*  TO GET MONTH AND YEAR; USE '01' FOR START-DAY AND '31' FOR END DAY
        if (condition(pnd_Run_Date.equals(" ")))                                                                                                                          //Natural: IF #RUN-DATE EQ ' '
        {
            getReports().write(0, "****** NO DAILY RUN DATE RECORD ON CWF-RPRT-RUN-TBL *****");                                                                           //Natural: WRITE '****** NO DAILY RUN DATE RECORD ON CWF-RPRT-RUN-TBL *****'
            if (Global.isEscape()) return;
            DbsUtil.terminate(33);  if (true) return;                                                                                                                     //Natural: TERMINATE 33
            //*  START DATE = TODAY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Start_Date_A.setValue(pnd_Run_Date);                                                                                                                      //Natural: ASSIGN #START-DATE-A := #RUN-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  GET-START-DATE
    }
    private void sub_Get_End_Date() throws Exception                                                                                                                      //Natural: GET-END-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fix_Date.setValue(pnd_Start_Date_A);                                                                                                                          //Natural: ASSIGN #FIX-DATE := #START-DATE-A
        short decideConditionsMet845 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #FIX-DATE.#FIX-MM;//Natural: VALUE 12
        if (condition((pnd_Fix_Date_Pnd_Fix_Mm.equals(12))))
        {
            decideConditionsMet845++;
            pnd_Fix_Date_Pnd_Fix_Mm.setValue(1);                                                                                                                          //Natural: ASSIGN #FIX-DATE.#FIX-MM := 01
            pnd_Fix_Date_Pnd_Fix_Dd.setValue(1);                                                                                                                          //Natural: ASSIGN #FIX-DATE.#FIX-DD := 01
            if (condition(pnd_Fix_Date_Pnd_Fix_Yy.equals(99)))                                                                                                            //Natural: IF #FIX-DATE.#FIX-YY = 99
            {
                pnd_Fix_Date_Pnd_Fix_Cc.setValue(20);                                                                                                                     //Natural: ASSIGN #FIX-DATE.#FIX-CC := 20
                pnd_Fix_Date_Pnd_Fix_Yy.setValue(0);                                                                                                                      //Natural: ASSIGN #FIX-DATE.#FIX-YY := 00
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Fix_Date_Pnd_Fix_Yy.compute(new ComputeParameters(false, pnd_Fix_Date_Pnd_Fix_Yy), pnd_Fix_Date_Pnd_Fix_Yy.add(1));                                   //Natural: ASSIGN #FIX-DATE.#FIX-YY := #FIX-YY +1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Fix_Date_Pnd_Fix_Mm.compute(new ComputeParameters(false, pnd_Fix_Date_Pnd_Fix_Mm), pnd_Fix_Date_Pnd_Fix_Mm.add(1));                                       //Natural: ASSIGN #FIX-DATE.#FIX-MM := #FIX-MM + 1
            pnd_Fix_Date_Pnd_Fix_Dd.setValue(1);                                                                                                                          //Natural: ASSIGN #FIX-DATE.#FIX-DD := 01
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Convert_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Fix_Date);                                                                                     //Natural: MOVE EDITED #FIX-DATE TO #CONVERT-DATE ( EM = YYYYMMDD )
        pnd_Convert_Date.nsubtract(1);                                                                                                                                    //Natural: ASSIGN #CONVERT-DATE := #CONVERT-DATE - 1
        pnd_End_Date_A.setValueEdited(pnd_Convert_Date,new ReportEditMask("YYYYMMDD"));                                                                                   //Natural: MOVE EDITED #CONVERT-DATE ( EM = YYYYMMDD ) TO #END-DATE-A
        pnd_Disp_Date.setValueEdited(pnd_Convert_Date,new ReportEditMask("MM/DD/YYYY"));                                                                                  //Natural: MOVE EDITED #CONVERT-DATE ( EM = MM/DD/YYYY ) TO #DISP-DATE
        //* *************
        //*  GET-END-DATE
    }
    private void sub_Write_Error() throws Exception                                                                                                                       //Natural: WRITE-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(2, pnd_Error_Msg);                                                                                                                           //Natural: DISPLAY ( 2 ) #ERROR-MSG
        if (Global.isEscape()) return;
        pnd_Errors.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ERRORS
        //* *************
        //*  WRITE-ERROR
    }
    private void sub_Add_To_Totals() throws Exception                                                                                                                     //Natural: ADD-TO-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet878 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ELAPSED-DAYS LE 4
        if (condition(pnd_Elapsed_Days.lessOrEqual(4)))
        {
            decideConditionsMet878++;
            pnd_Totals_Pnd_0_3_Total.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOTALS.#0-3-TOTAL
            pnd_Gtotals_Pnd_0_3_Total.nadd(1);                                                                                                                            //Natural: ADD 1 TO #GTOTALS.#0-3-TOTAL
        }                                                                                                                                                                 //Natural: WHEN #ELAPSED-DAYS LE 8
        else if (condition(pnd_Elapsed_Days.lessOrEqual(8)))
        {
            decideConditionsMet878++;
            pnd_Totals_Pnd_4_7_Total.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOTALS.#4-7-TOTAL
            pnd_Gtotals_Pnd_4_7_Total.nadd(1);                                                                                                                            //Natural: ADD 1 TO #GTOTALS.#4-7-TOTAL
        }                                                                                                                                                                 //Natural: WHEN #ELAPSED-DAYS LE 11
        else if (condition(pnd_Elapsed_Days.lessOrEqual(11)))
        {
            decideConditionsMet878++;
            pnd_Totals_Pnd_8_10_Total.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTALS.#8-10-TOTAL
            pnd_Gtotals_Pnd_8_10_Total.nadd(1);                                                                                                                           //Natural: ADD 1 TO #GTOTALS.#8-10-TOTAL
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Totals_Pnd_Gt_10_Total.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOTALS.#GT-10-TOTAL
            pnd_Gtotals_Pnd_Gt_10_Total.nadd(1);                                                                                                                          //Natural: ADD 1 TO #GTOTALS.#GT-10-TOTAL
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Elapsed_Days.lessOrEqual(8)))                                                                                                                   //Natural: IF #ELAPSED-DAYS LE 8
        {
            pnd_Totals_Pnd_0_7_Total.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOTALS.#0-7-TOTAL
            pnd_Gtotals_Pnd_0_7_Total.nadd(1);                                                                                                                            //Natural: ADD 1 TO #GTOTALS.#0-7-TOTAL
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Totals_Pnd_Total_Work.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTALS.#TOTAL-WORK
        //* *************
        //*  ADD-TO-TOTALS
    }
    private void sub_Calculate_Percents() throws Exception                                                                                                                //Natural: CALCULATE-PERCENTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Totals_Pnd_Total_Work.greater(getZero())))                                                                                                      //Natural: IF #TOTALS.#TOTAL-WORK GT 0
        {
            pnd_Pcts_Pnd_0_3_Pct.compute(new ComputeParameters(true, pnd_Pcts_Pnd_0_3_Pct), (pnd_Totals_Pnd_0_3_Total.divide(pnd_Totals_Pnd_Total_Work)).multiply(100));  //Natural: COMPUTE ROUNDED #PCTS.#0-3-PCT = ( #TOTALS.#0-3-TOTAL / #TOTALS.#TOTAL-WORK ) * 100
            pnd_Pcts_Pnd_4_7_Pct.compute(new ComputeParameters(true, pnd_Pcts_Pnd_4_7_Pct), (pnd_Totals_Pnd_4_7_Total.divide(pnd_Totals_Pnd_Total_Work)).multiply(100));  //Natural: COMPUTE ROUNDED #PCTS.#4-7-PCT = ( #TOTALS.#4-7-TOTAL / #TOTALS.#TOTAL-WORK ) * 100
            pnd_Pcts_Pnd_0_7_Pct.compute(new ComputeParameters(true, pnd_Pcts_Pnd_0_7_Pct), (pnd_Totals_Pnd_0_7_Total.divide(pnd_Totals_Pnd_Total_Work)).multiply(100));  //Natural: COMPUTE ROUNDED #PCTS.#0-7-PCT = ( #TOTALS.#0-7-TOTAL / #TOTALS.#TOTAL-WORK ) * 100
            pnd_Pcts_Pnd_8_10_Pct.compute(new ComputeParameters(true, pnd_Pcts_Pnd_8_10_Pct), (pnd_Totals_Pnd_8_10_Total.divide(pnd_Totals_Pnd_Total_Work)).multiply(100)); //Natural: COMPUTE ROUNDED #PCTS.#8-10-PCT = ( #TOTALS.#8-10-TOTAL / #TOTALS.#TOTAL-WORK ) * 100
            pnd_Pcts_Pnd_Gt_10_Pct.compute(new ComputeParameters(true, pnd_Pcts_Pnd_Gt_10_Pct), (pnd_Totals_Pnd_Gt_10_Total.divide(pnd_Totals_Pnd_Total_Work)).multiply(100)); //Natural: COMPUTE ROUNDED #PCTS.#GT-10-PCT = ( #TOTALS.#GT-10-TOTAL / #TOTALS.#TOTAL-WORK ) * 100
            pnd_Pcts_Pnd_Total_Pct.compute(new ComputeParameters(true, pnd_Pcts_Pnd_Total_Pct), pnd_Pcts_Pnd_0_3_Pct.add(pnd_Pcts_Pnd_4_7_Pct).add(pnd_Pcts_Pnd_8_10_Pct).add(pnd_Pcts_Pnd_Gt_10_Pct)); //Natural: COMPUTE ROUNDED #PCTS.#TOTAL-PCT = #PCTS.#0-3-PCT + #PCTS.#4-7-PCT + #PCTS.#8-10-PCT + #PCTS.#GT-10-PCT
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  CALCULATE-PERCENTS
    }
    private void sub_Calculate_Total_Percents() throws Exception                                                                                                          //Natural: CALCULATE-TOTAL-PERCENTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  #GTOTALS.#TOTAL-WORK       := #GTOTALS.#0-3-TOTAL
        //*                             +  #GTOTALS.#4-7-TOTAL
        //*                             +  #GTOTALS.#8-10-TOTAL
        //*                             +  #GTOTALS.#GT-10-TOTAL
        if (condition(pnd_Gtotals_Pnd_Total_Work.greater(getZero())))                                                                                                     //Natural: IF #GTOTALS.#TOTAL-WORK GT 0
        {
            pnd_Tot_Pcts_Pnd_0_3_Pct.compute(new ComputeParameters(true, pnd_Tot_Pcts_Pnd_0_3_Pct), (pnd_Gtotals_Pnd_0_3_Total.divide(pnd_Gtotals_Pnd_Total_Work)).multiply(100)); //Natural: COMPUTE ROUNDED #TOT-PCTS.#0-3-PCT = ( #GTOTALS.#0-3-TOTAL / #GTOTALS.#TOTAL-WORK ) * 100
            pnd_Tot_Pcts_Pnd_4_7_Pct.compute(new ComputeParameters(true, pnd_Tot_Pcts_Pnd_4_7_Pct), (pnd_Gtotals_Pnd_4_7_Total.divide(pnd_Gtotals_Pnd_Total_Work)).multiply(100)); //Natural: COMPUTE ROUNDED #TOT-PCTS.#4-7-PCT = ( #GTOTALS.#4-7-TOTAL / #GTOTALS.#TOTAL-WORK ) * 100
            pnd_Tot_Pcts_Pnd_0_7_Pct.compute(new ComputeParameters(true, pnd_Tot_Pcts_Pnd_0_7_Pct), (pnd_Gtotals_Pnd_0_7_Total.divide(pnd_Gtotals_Pnd_Total_Work)).multiply(100)); //Natural: COMPUTE ROUNDED #TOT-PCTS.#0-7-PCT = ( #GTOTALS.#0-7-TOTAL / #GTOTALS.#TOTAL-WORK ) * 100
            pnd_Tot_Pcts_Pnd_8_10_Pct.compute(new ComputeParameters(true, pnd_Tot_Pcts_Pnd_8_10_Pct), (pnd_Gtotals_Pnd_8_10_Total.divide(pnd_Gtotals_Pnd_Total_Work)).multiply(100)); //Natural: COMPUTE ROUNDED #TOT-PCTS.#8-10-PCT = ( #GTOTALS.#8-10-TOTAL / #GTOTALS.#TOTAL-WORK ) * 100
            pnd_Tot_Pcts_Pnd_Gt_10_Pct.compute(new ComputeParameters(true, pnd_Tot_Pcts_Pnd_Gt_10_Pct), (pnd_Gtotals_Pnd_Gt_10_Total.divide(pnd_Gtotals_Pnd_Total_Work)).multiply(100)); //Natural: COMPUTE ROUNDED #TOT-PCTS.#GT-10-PCT = ( #GTOTALS.#GT-10-TOTAL / #GTOTALS.#TOTAL-WORK ) * 100
            pnd_Tot_Pcts_Pnd_Total_Pct.compute(new ComputeParameters(true, pnd_Tot_Pcts_Pnd_Total_Pct), pnd_Tot_Pcts_Pnd_0_3_Pct.add(pnd_Tot_Pcts_Pnd_4_7_Pct).add(pnd_Tot_Pcts_Pnd_8_10_Pct).add(pnd_Tot_Pcts_Pnd_Gt_10_Pct)); //Natural: COMPUTE ROUNDED #TOT-PCTS.#TOTAL-PCT = #TOT-PCTS.#0-3-PCT + #TOT-PCTS.#4-7-PCT + #TOT-PCTS.#8-10-PCT + #TOT-PCTS.#GT-10-PCT
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  CALCULATE-TOTAL-PERCENTS
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total Transactions Completed in 4 Days or Less   :",pnd_Disp_Tots_Pnd_D_0_3,new ColumnSpacing(10),pnd_Disp_Pcts_Pnd_D_0_3_Pct,  //Natural: WRITE ( 1 ) / 'Total Transactions Completed in 4 Days or Less   :' #D-0-3 10X #D-0-3-PCT ( EM = ZZ9.99 ) '%' // 'Total Transactions Completed in 5 to 8 Days      :' #D-4-7 10X #D-4-7-PCT ( EM = ZZ9.99 ) '%' / '               Sub Total for 0 to 8 Days         :' #D-0-7 10X #D-0-7-PCT ( EM = ZZ9.99 ) '%' // 'Total Transactions Completed in 9 to 11 Days     :' #D-8-10 10X #D-8-10-PCT ( EM = ZZ9.99 ) '%' // 'Total Transactions Completed in more than 11 Days:' #D-GT-10 10X #D-GT-10-PCT ( EM = ZZ9.99 ) '%' // 'Total Transactions Completed                     :' #D-TOTAL 10X #D-TOTAL-PCT ( EM = ZZ9.99 ) '%'
            new ReportEditMask ("ZZ9.99"),"%",NEWLINE,NEWLINE,"Total Transactions Completed in 5 to 8 Days      :",pnd_Disp_Tots_Pnd_D_4_7,new ColumnSpacing(10),pnd_Disp_Pcts_Pnd_D_4_7_Pct, 
            new ReportEditMask ("ZZ9.99"),"%",NEWLINE,"               Sub Total for 0 to 8 Days         :",pnd_Disp_Tots_Pnd_D_0_7,new ColumnSpacing(10),pnd_Disp_Pcts_Pnd_D_0_7_Pct, 
            new ReportEditMask ("ZZ9.99"),"%",NEWLINE,NEWLINE,"Total Transactions Completed in 9 to 11 Days     :",pnd_Disp_Tots_Pnd_D_8_10,new ColumnSpacing(10),pnd_Disp_Pcts_Pnd_D_8_10_Pct, 
            new ReportEditMask ("ZZ9.99"),"%",NEWLINE,NEWLINE,"Total Transactions Completed in more than 11 Days:",pnd_Disp_Tots_Pnd_D_Gt_10,new ColumnSpacing(10),pnd_Disp_Pcts_Pnd_D_Gt_10_Pct, 
            new ReportEditMask ("ZZ9.99"),"%",NEWLINE,NEWLINE,"Total Transactions Completed                     :",pnd_Disp_Tots_Pnd_D_Total,new ColumnSpacing(10),pnd_Disp_Pcts_Pnd_D_Total_Pct, 
            new ReportEditMask ("ZZ9.99"),"%");
        if (Global.isEscape()) return;
        //* *************
        //*  WRITE-REPORT
    }
    private void sub_Format_Report() throws Exception                                                                                                                     //Natural: FORMAT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_End_Of_Report.getBoolean()))                                                                                                                    //Natural: IF #END-OF-REPORT
        {
            pnd_Disp_Tots_Pnd_D_0_3.setValue(pnd_Gtotals_Pnd_0_3_Total);                                                                                                  //Natural: MOVE #GTOTALS.#0-3-TOTAL TO #D-0-3
            pnd_Disp_Tots_Pnd_D_4_7.setValue(pnd_Gtotals_Pnd_4_7_Total);                                                                                                  //Natural: MOVE #GTOTALS.#4-7-TOTAL TO #D-4-7
            pnd_Disp_Tots_Pnd_D_0_7.setValue(pnd_Gtotals_Pnd_0_7_Total);                                                                                                  //Natural: MOVE #GTOTALS.#0-7-TOTAL TO #D-0-7
            pnd_Disp_Tots_Pnd_D_8_10.setValue(pnd_Gtotals_Pnd_8_10_Total);                                                                                                //Natural: MOVE #GTOTALS.#8-10-TOTAL TO #D-8-10
            pnd_Disp_Tots_Pnd_D_Gt_10.setValue(pnd_Gtotals_Pnd_Gt_10_Total);                                                                                              //Natural: MOVE #GTOTALS.#GT-10-TOTAL TO #D-GT-10
            pnd_Disp_Tots_Pnd_D_Total.setValue(pnd_Gtotals_Pnd_Total_Work);                                                                                               //Natural: MOVE #GTOTALS.#TOTAL-WORK TO #D-TOTAL
            pnd_Disp_Pcts_Pnd_D_0_3_Pct.setValue(pnd_Tot_Pcts_Pnd_0_3_Pct);                                                                                               //Natural: MOVE ROUNDED #TOT-PCTS.#0-3-PCT TO #D-0-3-PCT
            pnd_Disp_Pcts_Pnd_D_4_7_Pct.setValue(pnd_Tot_Pcts_Pnd_4_7_Pct);                                                                                               //Natural: MOVE ROUNDED #TOT-PCTS.#4-7-PCT TO #D-4-7-PCT
            pnd_Disp_Pcts_Pnd_D_0_7_Pct.setValue(pnd_Tot_Pcts_Pnd_0_7_Pct);                                                                                               //Natural: MOVE ROUNDED #TOT-PCTS.#0-7-PCT TO #D-0-7-PCT
            pnd_Disp_Pcts_Pnd_D_8_10_Pct.setValue(pnd_Tot_Pcts_Pnd_8_10_Pct);                                                                                             //Natural: MOVE ROUNDED #TOT-PCTS.#8-10-PCT TO #D-8-10-PCT
            pnd_Disp_Pcts_Pnd_D_Gt_10_Pct.setValue(pnd_Tot_Pcts_Pnd_Gt_10_Pct);                                                                                           //Natural: MOVE ROUNDED #TOT-PCTS.#GT-10-PCT TO #D-GT-10-PCT
            pnd_Disp_Pcts_Pnd_D_Total_Pct.setValue(pnd_Tot_Pcts_Pnd_Total_Pct);                                                                                           //Natural: MOVE ROUNDED #TOT-PCTS.#TOTAL-PCT TO #D-TOTAL-PCT
            pnd_Disp_Unit.setValue("ALL UNITS");                                                                                                                          //Natural: ASSIGN #DISP-UNIT := 'ALL UNITS'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Disp_Tots_Pnd_D_0_3.setValue(pnd_Totals_Pnd_0_3_Total);                                                                                                   //Natural: MOVE #TOTALS.#0-3-TOTAL TO #D-0-3
            pnd_Disp_Tots_Pnd_D_4_7.setValue(pnd_Totals_Pnd_4_7_Total);                                                                                                   //Natural: MOVE #TOTALS.#4-7-TOTAL TO #D-4-7
            pnd_Disp_Tots_Pnd_D_0_7.setValue(pnd_Totals_Pnd_0_7_Total);                                                                                                   //Natural: MOVE #TOTALS.#0-7-TOTAL TO #D-0-7
            pnd_Disp_Tots_Pnd_D_8_10.setValue(pnd_Totals_Pnd_8_10_Total);                                                                                                 //Natural: MOVE #TOTALS.#8-10-TOTAL TO #D-8-10
            pnd_Disp_Tots_Pnd_D_Gt_10.setValue(pnd_Totals_Pnd_Gt_10_Total);                                                                                               //Natural: MOVE #TOTALS.#GT-10-TOTAL TO #D-GT-10
            pnd_Disp_Tots_Pnd_D_Total.setValue(pnd_Totals_Pnd_Total_Work);                                                                                                //Natural: MOVE #TOTALS.#TOTAL-WORK TO #D-TOTAL
            pnd_Disp_Pcts_Pnd_D_0_3_Pct.setValue(pnd_Pcts_Pnd_0_3_Pct);                                                                                                   //Natural: MOVE ROUNDED #PCTS.#0-3-PCT TO #D-0-3-PCT
            pnd_Disp_Pcts_Pnd_D_4_7_Pct.setValue(pnd_Pcts_Pnd_4_7_Pct);                                                                                                   //Natural: MOVE ROUNDED #PCTS.#4-7-PCT TO #D-4-7-PCT
            pnd_Disp_Pcts_Pnd_D_0_7_Pct.setValue(pnd_Pcts_Pnd_0_7_Pct);                                                                                                   //Natural: MOVE ROUNDED #PCTS.#0-7-PCT TO #D-0-7-PCT
            pnd_Disp_Pcts_Pnd_D_8_10_Pct.setValue(pnd_Pcts_Pnd_8_10_Pct);                                                                                                 //Natural: MOVE ROUNDED #PCTS.#8-10-PCT TO #D-8-10-PCT
            pnd_Disp_Pcts_Pnd_D_Gt_10_Pct.setValue(pnd_Pcts_Pnd_Gt_10_Pct);                                                                                               //Natural: MOVE ROUNDED #PCTS.#GT-10-PCT TO #D-GT-10-PCT
            pnd_Disp_Pcts_Pnd_D_Total_Pct.setValue(pnd_Pcts_Pnd_Total_Pct);                                                                                               //Natural: MOVE ROUNDED #PCTS.#TOTAL-PCT TO #D-TOTAL-PCT
            pnd_Disp_Unit.setValue(pnd_Prev_Unit);                                                                                                                        //Natural: ASSIGN #DISP-UNIT := #PREV-UNIT
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  FORMAT-REPORT
    }
    private void sub_Reset_Buckets() throws Exception                                                                                                                     //Natural: RESET-BUCKETS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Totals.reset();                                                                                                                                               //Natural: RESET #TOTALS #PCTS
        pnd_Pcts.reset();
        //* *************
        //*  FORMAT-REPORT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP ) / 54T 'Report For Unit' #DISP-UNIT / 53T 'For Month Ending' #DISP-DATE
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header1_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(54),"Report For Unit",pnd_Disp_Unit,NEWLINE,new 
                        TabSetting(53),"For Month Ending",pnd_Disp_Date);
                    //* *SAG DEFINE EXIT REPORT1-AT-TOP-OF-PAGE
                    //* *SAG END-EXIT
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header2_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(2),  //Natural: WRITE ( 2 ) NOTITLE *PROGRAM 41T #HEADER2-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 2 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER2-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header2_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(2, pnd_Error_Msg);
    }
}
