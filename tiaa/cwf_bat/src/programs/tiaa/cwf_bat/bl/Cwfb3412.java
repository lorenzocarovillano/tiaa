/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:24 PM
**        * FROM NATURAL PROGRAM : Cwfb3412
************************************************************
**        * FILE NAME            : Cwfb3412.java
**        * CLASS NAME           : Cwfb3412
**        * INSTANCE NAME        : Cwfb3412
************************************************************
************************************************************************
* PROGRAM  : CWFB3412
* SYSTEM   : CRPCWF
* TITLE    : EMPLOYEE CASE LOAD
* GENERATED: OCT 26,94 AT 09:20 AM
* FUNCTION : THIS PROGRAM GENERATES A REPORT OF EMPLOYEE CASE LOAD
*
*
*
************************************************************************
*                    M O D I F I C A T I O N S                         *
************************************************************************
* NAME         DATE    DESCRIPTION                                     *
* ----------- -------- ------------------------------------------------*
* J. HARGRAVE 08/21/95 CHANGE TO READ CORE FILE INSTEAD OF PIF FILE    *
* J. HARGRAVE 10/21/95 CHANGE TO USE EMPL-WORK-LIST-TME-KEY (OLD KEY   *
*                      WAS DELETED)                                    *
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3412 extends BLNatBase
{
    // Data Areas
    private PdaCwfa5372 pdaCwfa5372;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_1;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_View_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Case_Id_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_2;
    private DbsField cwf_Master_Index_View_Case_Ind;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind;
    private DbsField cwf_Master_Index_View_Multi_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_3;
    private DbsField cwf_Master_Index_View_Empl_Racf_Id;
    private DbsField cwf_Master_Index_View_Empl_Sffx_Cde;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_View_Step_Id;
    private DbsField cwf_Master_Index_View_Rt_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Step_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Step_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Status_Cde;
    private DbsField cwf_Master_Index_View_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_View_Cnflct_Ind;
    private DbsField cwf_Master_Index_View_Check_Ind;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Effctve_Dte;
    private DbsField cwf_Master_Index_View_Trnsctn_Dte;
    private DbsField cwf_Master_Index_View_Trans_Dte;
    private DbsField cwf_Master_Index_View_Physcl_Fldr_Id_Nbr;
    private DbsField cwf_Master_Index_View_Mj_Chrge_Dte_Tme;
    private DbsField cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Mstr_Indx_Actn_Cde;
    private DbsField cwf_Master_Index_View_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_View_Rescan_Ind;
    private DbsField cwf_Master_Index_View_Dup_Ind;
    private DbsField cwf_Master_Index_View_Cmplnt_Ind;
    private DbsGroup cwf_Master_Index_View_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Cntrct_Nbr;
    private DbsField cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_View_Work_List_Ind;

    private DbsGroup cwf_Master_Index_View__R_Field_4;
    private DbsField cwf_Master_Index_View_Work_List_Ind_1_1;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_5;
    private DbsField cwf_Master_Index_View_Due_Dte;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Ind;
    private DbsField cwf_Master_Index_View_Due_Dte_Prty_Cde;
    private DbsField cwf_Master_Index_View_Due_Dte_Filler;
    private DbsField cwf_Master_Index_View_Bsnss_Reply_Ind;
    private DbsField cwf_Master_Index_View_Status_Freeze_Ind;
    private DbsField cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Days;
    private DbsField cwf_Master_Index_View_Step_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Step_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;
    private DbsField pnd_Partic_Sname;

    private DbsGroup pnd_Partic_Sname__R_Field_6;
    private DbsField pnd_Partic_Sname_Pnd_Fldr_Prefix;
    private DbsField pnd_Partic_Sname_Pnd_Fldr_No;
    private DbsField pnd_Record;
    private DbsField pnd_Booklet_Ctr;
    private DbsField pnd_Forms_Ctr;
    private DbsField pnd_Inquire_Ctr;
    private DbsField pnd_Research_Ctr;
    private DbsField pnd_Transaction_Ctr;
    private DbsField pnd_Complaint_Ctr;
    private DbsField pnd_Empl_Total_Ctr;
    private DbsField pnd_Program;
    private DbsField pnd_Empl_Name;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Old_Route_Cde;
    private DbsField pnd_Wpid_Lname;
    private DbsField pnd_Wpid_Sname;
    private DbsField pnd_Parm_Status_Key;

    private DbsGroup pnd_Parm_Status_Key__R_Field_7;
    private DbsField pnd_Parm_Status_Key_Pnd_Parm_Status_Unit;
    private DbsField pnd_Parm_Status_Key_Pnd_Parm_Status_Code;
    private DbsField pnd_Parm_Status_Name;
    private DbsField pnd_Parm_Status_Wpid;
    private DbsField pnd_Source;
    private DbsField pnd_Due_Date;
    private DbsField pnd_Commit_Ind;
    private DbsField pnd_Doc_Received_Date;
    private DbsField pnd_Received_In_Unit;
    private DbsField pnd_Received_In_Unit_A;
    private DbsField pnd_Non_Working_Days;
    private DbsField pnd_Business_Days;
    private DbsField pnd_Parm_Report_No;
    private DbsField pnd_Parm_Empl_Racf_Id;
    private DbsField pnd_Parm_Unit_Cde;
    private DbsField pnd_Parm_Floor;
    private DbsField pnd_Parm_Bldg;
    private DbsField pnd_Parm_Drop_Off;
    private DbsField pnd_Parm_Type;
    private DbsField pnd_First_Input;
    private DbsField pnd_Env;

    private DbsGroup pnd_Rep_Parm;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date;

    private DbsGroup pnd_Rep_Parm__R_Field_8;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date_N;
    private DbsField pnd_Rep_Parm_Pnd_End_Date;

    private DbsGroup pnd_Rep_Parm__R_Field_9;
    private DbsField pnd_Rep_Parm_Pnd_End_Date_N;
    private DbsField pnd_Rep_Parm_Pnd_Work_Start_Date_A;

    private DbsGroup pnd_Rep_Parm__R_Field_10;
    private DbsField pnd_Rep_Parm_Pnd_Work_Mm;
    private DbsField pnd_Rep_Parm_Pnd_Filler1;
    private DbsField pnd_Rep_Parm_Pnd_Work_Dd;
    private DbsField pnd_Rep_Parm_Pnd_Filler2;
    private DbsField pnd_Rep_Parm_Pnd_Work_Yy;
    private DbsField pnd_Rep_Parm_Pnd_Work_End_Date_A;

    private DbsGroup pnd_Rep_Parm__R_Field_11;
    private DbsField pnd_Rep_Parm_Pnd_Work_Mmm;
    private DbsField pnd_Rep_Parm_Pnd_Fillera;
    private DbsField pnd_Rep_Parm_Pnd_Work_Ddd;
    private DbsField pnd_Rep_Parm_Pnd_Fillerb;
    private DbsField pnd_Rep_Parm_Pnd_Work_Yyy;
    private DbsField pnd_Start_Date_D;
    private DbsField pnd_End_Date_D;
    private DbsField pnd_Work_Date;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Work_Start_Date;
    private DbsField pnd_Work_Comp_Date;
    private DbsField pnd_Day_Of_Week;
    private DbsField pnd_Days_To_Subtract;
    private DbsField pnd_Yyyymmdd;

    private DbsGroup pnd_Yyyymmdd__R_Field_12;
    private DbsField pnd_Yyyymmdd_Pnd_Century;
    private DbsField pnd_Yyyymmdd_Pnd_Yy;
    private DbsField pnd_Yyyymmdd_Pnd_Mm;
    private DbsField pnd_Yyyymmdd_Pnd_Dd;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa5372 = new PdaCwfa5372(localVariables);

        // Local Variables

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Pin_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Master_Index_View_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index_View__R_Field_1 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_1", "REDEFINE", cwf_Master_Index_View_Rqst_Log_Dte_Tme);
        cwf_Master_Index_View_Rqst_Log_Index_Dte = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Log_Index_Tme = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_View_Rqst_Orgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Orgn_Cde", "RQST-ORGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_View_Sub_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Ind", "SUB-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SUB_RQST_IND");
        cwf_Master_Index_View_Sub_Rqst_Ind.setDdmHeader("SUB-REQUEST COUNT");
        cwf_Master_Index_View_Case_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CASE_ID_CDE");
        cwf_Master_Index_View_Case_Id_Cde.setDdmHeader("CASE/ID");

        cwf_Master_Index_View__R_Field_2 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_2", "REDEFINE", cwf_Master_Index_View_Case_Id_Cde);
        cwf_Master_Index_View_Case_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Case_Ind", "CASE-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind", "SUB-RQST-SQNCE-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Multi_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Multi_Rqst_Ind", "MULTI-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        cwf_Master_Index_View_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Master_Index_View_Orgnl_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_View_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_View_Empl_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_View_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");

        cwf_Master_Index_View__R_Field_3 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_3", "REDEFINE", cwf_Master_Index_View_Empl_Oprtr_Cde);
        cwf_Master_Index_View_Empl_Racf_Id = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Empl_Sffx_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Empl_Sffx_Cde", "EMPL-SFFX-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Last_Chnge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Oprtr_Cde", 
            "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme", 
            "LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Unit_Cde", 
            "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_View_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_View_Step_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Id", "STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "STEP_ID");
        cwf_Master_Index_View_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Index_View_Rt_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        cwf_Master_Index_View_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        cwf_Master_Index_View_Step_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Sqnce_Nbr", "STEP-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "STEP_SQNCE_NBR");
        cwf_Master_Index_View_Step_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Updte_Dte_Tme", "STEP-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STEP_UPDTE_DTE_TME");
        cwf_Master_Index_View_Step_Updte_Dte_Tme.setDdmHeader("STEP UPDATE/DATE-TIME");
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_View_Admin_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme", 
            "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_View_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Dte_Tme", 
            "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Index_View_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Oprtr_Cde", 
            "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Crprte_Status_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_View_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_View_Cnflct_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cnflct_Ind", "CNFLCT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNFLCT_IND");
        cwf_Master_Index_View_Cnflct_Ind.setDdmHeader("CONFLICT/REQUEST");
        cwf_Master_Index_View_Check_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Check_Ind", "CHECK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CHECK_IND");
        cwf_Master_Index_View_Check_Ind.setDdmHeader("CHK");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Effctve_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_View_Trnsctn_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trnsctn_Dte", "TRNSCTN-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRNSCTN_DTE");
        cwf_Master_Index_View_Trnsctn_Dte.setDdmHeader("TRANSACTION/DATE");
        cwf_Master_Index_View_Trans_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trans_Dte", "TRANS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        cwf_Master_Index_View_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_View_Physcl_Fldr_Id_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Index_View_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Index_View_Mj_Chrge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Chrge_Dte_Tme", "MJ-CHRGE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "MJ_CHRGE_DTE_TME");
        cwf_Master_Index_View_Mj_Chrge_Dte_Tme.setDdmHeader("CHARGEOUT/DATE-TIME");
        cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde", "MJ-CHRGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_CHRGE_OPRTR_CDE");
        cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde.setDdmHeader("CHARGEOUT/OPERATOR");
        cwf_Master_Index_View_Mstr_Indx_Actn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mstr_Indx_Actn_Cde", "MSTR-INDX-ACTN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "MSTR_INDX_ACTN_CDE");
        cwf_Master_Index_View_Mstr_Indx_Actn_Cde.setDdmHeader("CHARGEOUT/ACTION CODE");
        cwf_Master_Index_View_Mj_Pull_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_View_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_View_Rescan_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RESCAN_IND");
        cwf_Master_Index_View_Rescan_Ind.setDdmHeader("RE-SCAN/INDICATOR");
        cwf_Master_Index_View_Dup_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Dup_Ind", "DUP-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DUP_IND");
        cwf_Master_Index_View_Dup_Ind.setDdmHeader("DUP/IND");
        cwf_Master_Index_View_Cmplnt_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CMPLNT_IND");
        cwf_Master_Index_View_Cntrct_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_View_Cntrct_Nbr = cwf_Master_Index_View_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Cntrct_Nbr", "CNTRCT-NBR", 
            FieldType.STRING, 8, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte", 
            "EXTRNL-PEND-RCV-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_View_Work_List_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_List_Ind", "WORK-LIST-IND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "WORK_LIST_IND");
        cwf_Master_Index_View_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");

        cwf_Master_Index_View__R_Field_4 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_4", "REDEFINE", cwf_Master_Index_View_Work_List_Ind);
        cwf_Master_Index_View_Work_List_Ind_1_1 = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Work_List_Ind_1_1", "WORK-LIST-IND-1-1", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde", 
            "DUE-DTE-CHG-PRTY-CDE", FieldType.STRING, 16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        cwf_Master_Index_View__R_Field_5 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_5", "REDEFINE", cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Index_View_Due_Dte = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Due_Dte", "DUE-DTE", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Due_Dte_Chg_Ind = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Prty_Cde = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Prty_Cde", "DUE-DTE-PRTY-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Filler = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Filler", "DUE-DTE-FILLER", 
            FieldType.STRING, 6);
        cwf_Master_Index_View_Bsnss_Reply_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BSNSS_REPLY_IND");
        cwf_Master_Index_View_Bsnss_Reply_Ind.setDdmHeader("BUSINESS/REPLY");
        cwf_Master_Index_View_Status_Freeze_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_View_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme", 
            "UNIT-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Index_View_Unit_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Clock_End_Dte_Tme", 
            "UNIT-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme", 
            "EMPL-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        cwf_Master_Index_View_Empl_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Clock_End_Dte_Tme", 
            "EMPL-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme", 
            "INTRNL-PND-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_START_DTE_TME");
        cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme.setDdmHeader("ACKNOWLEDGEMENT DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme", 
            "INTRNL-PND-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_END_DTE_TME");
        cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme.setDdmHeader("INTERNAL PEND END DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_Days = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Days", "INTRNL-PND-DAYS", 
            FieldType.PACKED_DECIMAL, 5, RepeatingFieldStrategy.None, "INTRNL_PND_DAYS");
        cwf_Master_Index_View_Intrnl_Pnd_Days.setDdmHeader("INTERNAL PEND DAYS");
        cwf_Master_Index_View_Step_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Clock_Start_Dte_Tme", 
            "STEP-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        cwf_Master_Index_View_Step_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Clock_End_Dte_Tme", 
            "STEP-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme", 
            "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme", 
            "UNIT-EN-RTE-TO-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_EN_RTE_TO_DTE_TME");
        cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme.setDdmHeader("UNIT EN ROUTE TO DATE TIME");
        registerRecord(vw_cwf_Master_Index_View);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);
        pnd_Partic_Sname = localVariables.newFieldInRecord("pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);

        pnd_Partic_Sname__R_Field_6 = localVariables.newGroupInRecord("pnd_Partic_Sname__R_Field_6", "REDEFINE", pnd_Partic_Sname);
        pnd_Partic_Sname_Pnd_Fldr_Prefix = pnd_Partic_Sname__R_Field_6.newFieldInGroup("pnd_Partic_Sname_Pnd_Fldr_Prefix", "#FLDR-PREFIX", FieldType.STRING, 
            1);
        pnd_Partic_Sname_Pnd_Fldr_No = pnd_Partic_Sname__R_Field_6.newFieldInGroup("pnd_Partic_Sname_Pnd_Fldr_No", "#FLDR-NO", FieldType.NUMERIC, 6);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        pnd_Booklet_Ctr = localVariables.newFieldInRecord("pnd_Booklet_Ctr", "#BOOKLET-CTR", FieldType.NUMERIC, 18);
        pnd_Forms_Ctr = localVariables.newFieldInRecord("pnd_Forms_Ctr", "#FORMS-CTR", FieldType.NUMERIC, 18);
        pnd_Inquire_Ctr = localVariables.newFieldInRecord("pnd_Inquire_Ctr", "#INQUIRE-CTR", FieldType.NUMERIC, 18);
        pnd_Research_Ctr = localVariables.newFieldInRecord("pnd_Research_Ctr", "#RESEARCH-CTR", FieldType.NUMERIC, 18);
        pnd_Transaction_Ctr = localVariables.newFieldInRecord("pnd_Transaction_Ctr", "#TRANSACTION-CTR", FieldType.NUMERIC, 18);
        pnd_Complaint_Ctr = localVariables.newFieldInRecord("pnd_Complaint_Ctr", "#COMPLAINT-CTR", FieldType.NUMERIC, 18);
        pnd_Empl_Total_Ctr = localVariables.newFieldInRecord("pnd_Empl_Total_Ctr", "#EMPL-TOTAL-CTR", FieldType.NUMERIC, 18);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Empl_Name = localVariables.newFieldInRecord("pnd_Empl_Name", "#EMPL-NAME", FieldType.STRING, 30);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Page_Number = localVariables.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.NUMERIC, 4);
        pnd_Old_Route_Cde = localVariables.newFieldInRecord("pnd_Old_Route_Cde", "#OLD-ROUTE-CDE", FieldType.STRING, 4);
        pnd_Wpid_Lname = localVariables.newFieldInRecord("pnd_Wpid_Lname", "#WPID-LNAME", FieldType.STRING, 45);
        pnd_Wpid_Sname = localVariables.newFieldInRecord("pnd_Wpid_Sname", "#WPID-SNAME", FieldType.STRING, 15);
        pnd_Parm_Status_Key = localVariables.newFieldInRecord("pnd_Parm_Status_Key", "#PARM-STATUS-KEY", FieldType.STRING, 13);

        pnd_Parm_Status_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Parm_Status_Key__R_Field_7", "REDEFINE", pnd_Parm_Status_Key);
        pnd_Parm_Status_Key_Pnd_Parm_Status_Unit = pnd_Parm_Status_Key__R_Field_7.newFieldInGroup("pnd_Parm_Status_Key_Pnd_Parm_Status_Unit", "#PARM-STATUS-UNIT", 
            FieldType.STRING, 8);
        pnd_Parm_Status_Key_Pnd_Parm_Status_Code = pnd_Parm_Status_Key__R_Field_7.newFieldInGroup("pnd_Parm_Status_Key_Pnd_Parm_Status_Code", "#PARM-STATUS-CODE", 
            FieldType.STRING, 4);
        pnd_Parm_Status_Name = localVariables.newFieldInRecord("pnd_Parm_Status_Name", "#PARM-STATUS-NAME", FieldType.STRING, 25);
        pnd_Parm_Status_Wpid = localVariables.newFieldInRecord("pnd_Parm_Status_Wpid", "#PARM-STATUS-WPID", FieldType.STRING, 6);
        pnd_Source = localVariables.newFieldInRecord("pnd_Source", "#SOURCE", FieldType.STRING, 7);
        pnd_Due_Date = localVariables.newFieldInRecord("pnd_Due_Date", "#DUE-DATE", FieldType.DATE);
        pnd_Commit_Ind = localVariables.newFieldInRecord("pnd_Commit_Ind", "#COMMIT-IND", FieldType.STRING, 1);
        pnd_Doc_Received_Date = localVariables.newFieldInRecord("pnd_Doc_Received_Date", "#DOC-RECEIVED-DATE", FieldType.STRING, 8);
        pnd_Received_In_Unit = localVariables.newFieldInRecord("pnd_Received_In_Unit", "#RECEIVED-IN-UNIT", FieldType.TIME);
        pnd_Received_In_Unit_A = localVariables.newFieldInRecord("pnd_Received_In_Unit_A", "#RECEIVED-IN-UNIT-A", FieldType.STRING, 8);
        pnd_Non_Working_Days = localVariables.newFieldInRecord("pnd_Non_Working_Days", "#NON-WORKING-DAYS", FieldType.NUMERIC, 18);
        pnd_Business_Days = localVariables.newFieldInRecord("pnd_Business_Days", "#BUSINESS-DAYS", FieldType.NUMERIC, 19, 1);
        pnd_Parm_Report_No = localVariables.newFieldInRecord("pnd_Parm_Report_No", "#PARM-REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Parm_Empl_Racf_Id = localVariables.newFieldInRecord("pnd_Parm_Empl_Racf_Id", "#PARM-EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Parm_Unit_Cde = localVariables.newFieldInRecord("pnd_Parm_Unit_Cde", "#PARM-UNIT-CDE", FieldType.STRING, 8);
        pnd_Parm_Floor = localVariables.newFieldInRecord("pnd_Parm_Floor", "#PARM-FLOOR", FieldType.NUMERIC, 2);
        pnd_Parm_Bldg = localVariables.newFieldInRecord("pnd_Parm_Bldg", "#PARM-BLDG", FieldType.STRING, 3);
        pnd_Parm_Drop_Off = localVariables.newFieldInRecord("pnd_Parm_Drop_Off", "#PARM-DROP-OFF", FieldType.STRING, 2);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        pnd_First_Input = localVariables.newFieldInRecord("pnd_First_Input", "#FIRST-INPUT", FieldType.BOOLEAN, 1);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);

        pnd_Rep_Parm = localVariables.newGroupInRecord("pnd_Rep_Parm", "#REP-PARM");
        pnd_Rep_Parm_Pnd_Start_Date = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_8 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_8", "REDEFINE", pnd_Rep_Parm_Pnd_Start_Date);
        pnd_Rep_Parm_Pnd_Start_Date_N = pnd_Rep_Parm__R_Field_8.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 8);
        pnd_Rep_Parm_Pnd_End_Date = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_9 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_9", "REDEFINE", pnd_Rep_Parm_Pnd_End_Date);
        pnd_Rep_Parm_Pnd_End_Date_N = pnd_Rep_Parm__R_Field_9.newFieldInGroup("pnd_Rep_Parm_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 8);
        pnd_Rep_Parm_Pnd_Work_Start_Date_A = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Start_Date_A", "#WORK-START-DATE-A", FieldType.STRING, 
            8);

        pnd_Rep_Parm__R_Field_10 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_10", "REDEFINE", pnd_Rep_Parm_Pnd_Work_Start_Date_A);
        pnd_Rep_Parm_Pnd_Work_Mm = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Mm", "#WORK-MM", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Filler1 = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Dd = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Filler2 = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Yy = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Yy", "#WORK-YY", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Work_End_Date_A = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_End_Date_A", "#WORK-END-DATE-A", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_11 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_11", "REDEFINE", pnd_Rep_Parm_Pnd_Work_End_Date_A);
        pnd_Rep_Parm_Pnd_Work_Mmm = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Mmm", "#WORK-MMM", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Fillera = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Fillera", "#FILLERA", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Ddd = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Ddd", "#WORK-DDD", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Fillerb = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Fillerb", "#FILLERB", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Yyy = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Yyy", "#WORK-YYY", FieldType.STRING, 2);
        pnd_Start_Date_D = localVariables.newFieldInRecord("pnd_Start_Date_D", "#START-DATE-D", FieldType.DATE);
        pnd_End_Date_D = localVariables.newFieldInRecord("pnd_End_Date_D", "#END-DATE-D", FieldType.DATE);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Work_Start_Date = localVariables.newFieldInRecord("pnd_Work_Start_Date", "#WORK-START-DATE", FieldType.DATE);
        pnd_Work_Comp_Date = localVariables.newFieldInRecord("pnd_Work_Comp_Date", "#WORK-COMP-DATE", FieldType.DATE);
        pnd_Day_Of_Week = localVariables.newFieldInRecord("pnd_Day_Of_Week", "#DAY-OF-WEEK", FieldType.STRING, 3);
        pnd_Days_To_Subtract = localVariables.newFieldInRecord("pnd_Days_To_Subtract", "#DAYS-TO-SUBTRACT", FieldType.NUMERIC, 2);
        pnd_Yyyymmdd = localVariables.newFieldInRecord("pnd_Yyyymmdd", "#YYYYMMDD", FieldType.STRING, 8);

        pnd_Yyyymmdd__R_Field_12 = localVariables.newGroupInRecord("pnd_Yyyymmdd__R_Field_12", "REDEFINE", pnd_Yyyymmdd);
        pnd_Yyyymmdd_Pnd_Century = pnd_Yyyymmdd__R_Field_12.newFieldInGroup("pnd_Yyyymmdd_Pnd_Century", "#CENTURY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Yy = pnd_Yyyymmdd__R_Field_12.newFieldInGroup("pnd_Yyyymmdd_Pnd_Yy", "#YY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Mm = pnd_Yyyymmdd__R_Field_12.newFieldInGroup("pnd_Yyyymmdd_Pnd_Mm", "#MM", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Dd = pnd_Yyyymmdd__R_Field_12.newFieldInGroup("pnd_Yyyymmdd_Pnd_Dd", "#DD", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index_View.reset();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("          Corporate Workflow Facilities");
        pnd_Header1_2.setInitialValue("               Employee Case Report");
        pnd_Record.setInitialValue("�E�&l1o2a5.6c72p3e66F�&a2L�(9905X�&l0L");
        pnd_Parm_Report_No.setInitialValue(15);
        pnd_Parm_Type.setInitialValue("D");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3412() throws Exception
    {
        super("Cwfb3412");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 150 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF")))                                                                                                                         //Natural: IF #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("TEST ");                                                                                                                                    //Natural: MOVE 'TEST ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        //* ********************
        //*                    *
        //*  REPORT SECTION    *
        //* ********************
        //*  GET THE RUN DATE
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        pnd_Rep_Parm_Pnd_Start_Date.setValue(pnd_Comp_Date);                                                                                                              //Natural: MOVE #COMP-DATE TO #START-DATE #END-DATE
        pnd_Rep_Parm_Pnd_End_Date.setValue(pnd_Comp_Date);
        pnd_End_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Comp_Date);                                                                                      //Natural: MOVE EDITED #COMP-DATE TO #END-DATE-D ( EM = YYYYMMDD )
        pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_Start_Date);                                                                                                               //Natural: MOVE #START-DATE TO #YYYYMMDD
        pnd_Rep_Parm_Pnd_Work_Mm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                                           //Natural: MOVE #MM TO #WORK-MM
        pnd_Rep_Parm_Pnd_Work_Dd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                                           //Natural: MOVE #DD TO #WORK-DD
        pnd_Rep_Parm_Pnd_Work_Yy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                                           //Natural: MOVE #YY TO #WORK-YY
        pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                                                 //Natural: MOVE #END-DATE TO #YYYYMMDD
        pnd_Rep_Parm_Pnd_Work_Mmm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                                          //Natural: MOVE #MM TO #WORK-MMM
        pnd_Rep_Parm_Pnd_Work_Ddd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                                          //Natural: MOVE #DD TO #WORK-DDD
        pnd_Rep_Parm_Pnd_Work_Yyy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                                          //Natural: MOVE #YY TO #WORK-YYY
        //*  START DATE AND END DATE ESTABLISHED
        pnd_Page_Number.setValue(0);                                                                                                                                      //Natural: MOVE 0 TO #PAGE-NUMBER
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  PRIMARY FILE
        vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                        //Natural: READ CWF-MASTER-INDEX-VIEW BY EMPL-WORK-LIST-TME-KEY
        (
        "READ_PRIME",
        new Oc[] { new Oc("EMPL_WORK_LIST_TME_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Master_Index_View.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT SORT-FIELDS
            //*  THIS USER EXIT ALLOWS FIELDS OF SELECTED PRIME SEC TER FILES TO
            //*  BE SORTED
            getSort().writeSortInData(cwf_Master_Index_View_Admin_Unit_Cde, cwf_Master_Index_View_Empl_Oprtr_Cde, cwf_Master_Index_View_Due_Dte, cwf_Master_Index_View_Due_Dte_Chg_Ind,  //Natural: END-ALL
                cwf_Master_Index_View_Work_Prcss_Id, cwf_Master_Index_View_Tiaa_Rcvd_Dte, cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte, cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme, 
                cwf_Master_Index_View_Step_Id, cwf_Master_Index_View_Admin_Status_Cde, cwf_Master_Index_View_Last_Chnge_Unit_Cde, cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme, 
                cwf_Master_Index_View_Pin_Nbr, cwf_Master_Index_View_Cntrct_Nbr.getValue(1), cwf_Master_Index_View_Mj_Pull_Ind);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(cwf_Master_Index_View_Admin_Unit_Cde, cwf_Master_Index_View_Empl_Oprtr_Cde, cwf_Master_Index_View_Due_Dte, cwf_Master_Index_View_Due_Dte_Chg_Ind,  //Natural: SORT CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE CWF-MASTER-INDEX-VIEW.EMPL-OPRTR-CDE CWF-MASTER-INDEX-VIEW.DUE-DTE CWF-MASTER-INDEX-VIEW.DUE-DTE-CHG-IND CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID USING CWF-MASTER-INDEX-VIEW.TIAA-RCVD-DTE CWF-MASTER-INDEX-VIEW.EXTRNL-PEND-RCV-DTE CWF-MASTER-INDEX-VIEW.UNIT-CLOCK-START-DTE-TME CWF-MASTER-INDEX-VIEW.STEP-ID CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE CWF-MASTER-INDEX-VIEW.LAST-CHNGE-UNIT-CDE CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-UPDTE-DTE-TME CWF-MASTER-INDEX-VIEW.PIN-NBR CWF-MASTER-INDEX-VIEW.CNTRCT-NBR ( 1 ) CWF-MASTER-INDEX-VIEW.MJ-PULL-IND
            cwf_Master_Index_View_Work_Prcss_Id);
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Master_Index_View_Admin_Unit_Cde, cwf_Master_Index_View_Empl_Oprtr_Cde, cwf_Master_Index_View_Due_Dte, 
            cwf_Master_Index_View_Due_Dte_Chg_Ind, cwf_Master_Index_View_Work_Prcss_Id, cwf_Master_Index_View_Tiaa_Rcvd_Dte, cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte, 
            cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme, cwf_Master_Index_View_Step_Id, cwf_Master_Index_View_Admin_Status_Cde, cwf_Master_Index_View_Last_Chnge_Unit_Cde, 
            cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme, cwf_Master_Index_View_Pin_Nbr, cwf_Master_Index_View_Cntrct_Nbr.getValue(1), cwf_Master_Index_View_Mj_Pull_Ind)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //* *SAG END-EXIT
            //* *SAG DEFINE EXIT SORT-WRITE-FIELDS
            DbsUtil.callnat(Cwfn0013.class , getCurrentProcessState(), cwf_Master_Index_View_Work_Prcss_Id, pnd_Old_Route_Cde, pnd_Wpid_Lname, pnd_Wpid_Sname);           //Natural: AT TOP OF PAGE ( 1 );//Natural: AT BREAK OF CWF-MASTER-INDEX-VIEW.EMPL-OPRTR-CDE;//Natural: AT BREAK OF CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE;//Natural: CALLNAT 'CWFN0013' CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID #OLD-ROUTE-CDE #WPID-LNAME #WPID-SNAME
            if (condition(Global.isEscape())) return;
            pnd_Parm_Status_Key_Pnd_Parm_Status_Unit.setValue(cwf_Master_Index_View_Last_Chnge_Unit_Cde);                                                                 //Natural: MOVE CWF-MASTER-INDEX-VIEW.LAST-CHNGE-UNIT-CDE TO #PARM-STATUS-UNIT
            pnd_Parm_Status_Key_Pnd_Parm_Status_Code.setValue(cwf_Master_Index_View_Admin_Status_Cde);                                                                    //Natural: MOVE CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE TO #PARM-STATUS-CODE
            DbsUtil.callnat(Cwfn1118.class , getCurrentProcessState(), pnd_Parm_Status_Key, pnd_Parm_Status_Name, pnd_Parm_Status_Wpid, pnd_Source);                      //Natural: CALLNAT 'CWFN1118' #PARM-STATUS-KEY #PARM-STATUS-NAME #PARM-STATUS-WPID #SOURCE
            if (condition(Global.isEscape())) return;
            pnd_Due_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Master_Index_View_Due_Dte);                                                                    //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.DUE-DTE TO #DUE-DATE ( EM = YYYYMMDD )
            if (condition(cwf_Master_Index_View_Due_Dte_Chg_Ind.equals("9")))                                                                                             //Natural: IF CWF-MASTER-INDEX-VIEW.DUE-DTE-CHG-IND = '9'
            {
                pnd_Commit_Ind.setValue(" ");                                                                                                                             //Natural: MOVE ' ' TO #COMMIT-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Commit_Ind.setValue("Y");                                                                                                                             //Natural: MOVE 'Y' TO #COMMIT-IND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte.greater(getZero())))                                                                                  //Natural: IF CWF-MASTER-INDEX-VIEW.EXTRNL-PEND-RCV-DTE GT 0
            {
                pnd_Doc_Received_Date.setValueEdited(cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte,new ReportEditMask("MM'/'DD'/'YY"));                                       //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.EXTRNL-PEND-RCV-DTE ( EM = MM'/'DD'/'YY ) TO #DOC-RECEIVED-DATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Doc_Received_Date.setValue(" ");                                                                                                                      //Natural: MOVE ' ' TO #DOC-RECEIVED-DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme,"YYYYMMDD.......") && DbsUtil.maskMatches(cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme,"........00:23.....")  //Natural: IF CWF-MASTER-INDEX-VIEW.UNIT-CLOCK-START-DTE-TME = MASK ( YYYYMMDD....... ) AND CWF-MASTER-INDEX-VIEW.UNIT-CLOCK-START-DTE-TME = MASK ( ........00:23..... ) AND CWF-MASTER-INDEX-VIEW.UNIT-CLOCK-START-DTE-TME = MASK ( ..........00:59... ) AND CWF-MASTER-INDEX-VIEW.UNIT-CLOCK-START-DTE-TME = MASK ( ............00:59. ) AND CWF-MASTER-INDEX-VIEW.UNIT-CLOCK-START-DTE-TME = MASK ( ..............0:9 )
                && DbsUtil.maskMatches(cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme,"..........00:59...") && DbsUtil.maskMatches(cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme,"............00:59.") 
                && DbsUtil.maskMatches(cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme,"..............0:9")))
            {
                pnd_Received_In_Unit.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme);                                //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.UNIT-CLOCK-START-DTE-TME TO #RECEIVED-IN-UNIT ( EM = YYYYMMDDHHIISST )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Received_In_Unit.reset();                                                                                                                             //Natural: RESET #RECEIVED-IN-UNIT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Received_In_Unit.greater(getZero())))                                                                                                       //Natural: IF #RECEIVED-IN-UNIT GT 0
            {
                pnd_Received_In_Unit_A.setValueEdited(pnd_Received_In_Unit,new ReportEditMask("MM'/'DD'/'YY"));                                                           //Natural: MOVE EDITED #RECEIVED-IN-UNIT ( EM = MM'/'DD'/'YY ) TO #RECEIVED-IN-UNIT-A
            }                                                                                                                                                             //Natural: END-IF
            pnd_Partic_Sname.reset();                                                                                                                                     //Natural: RESET #PARTIC-SNAME
            pdaCwfa5372.getCwfa5372_Pnd_Pin_Key().setValue(cwf_Master_Index_View_Pin_Nbr);                                                                                //Natural: ASSIGN CWFA5372.#PIN-KEY := CWF-MASTER-INDEX-VIEW.PIN-NBR
            //*  FETCH RETURN 'MDMP0011'  /* MQ OPEN
            //*  CALLNAT 'CWFN5372'
            //*           CWFA5372.#PIN-KEY
            //*           CWFA5372.#PASS-NAME
            //*           CWFA5372.#PASS-SSN
            //*           CWFA5372.#PASS-TLC
            //*           CWFA5372.#PASS-DOB
            //*           CWFA5372.#PASS-FOUND
            //*  FETCH RETURN 'MDMP0012'  /* MQ CLOSE
            if (condition(pdaCwfa5372.getCwfa5372_Pnd_Pass_Found().getBoolean()))                                                                                         //Natural: IF #PASS-FOUND
            {
                pnd_Partic_Sname.setValue(pdaCwfa5372.getCwfa5372_Pnd_Pass_Name());                                                                                       //Natural: MOVE CWFA5372.#PASS-NAME TO #PARTIC-SNAME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cwf_Master_Index_View_Rqst_Orgn_Cde.equals("J")))                                                                                           //Natural: IF CWF-MASTER-INDEX-VIEW.RQST-ORGN-CDE = 'J'
                {
                    pnd_Partic_Sname_Pnd_Fldr_Prefix.setValue("R");                                                                                                       //Natural: MOVE 'R' TO #FLDR-PREFIX
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(cwf_Master_Index_View_Mj_Pull_Ind.equals("Y") || cwf_Master_Index_View_Mj_Pull_Ind.equals("R")))                                        //Natural: IF CWF-MASTER-INDEX-VIEW.MJ-PULL-IND = 'Y' OR = 'R'
                    {
                        pnd_Partic_Sname_Pnd_Fldr_Prefix.setValue("M");                                                                                                   //Natural: MOVE 'M' TO #FLDR-PREFIX
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Partic_Sname_Pnd_Fldr_Prefix.setValue("F");                                                                                                   //Natural: MOVE 'F' TO #FLDR-PREFIX
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Partic_Sname_Pnd_Fldr_No.setValue(cwf_Master_Index_View_Physcl_Fldr_Id_Nbr);                                                                          //Natural: MOVE CWF-MASTER-INDEX-VIEW.PHYSCL-FLDR-ID-NBR TO #FLDR-NO
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Received_In_Unit.greater(getZero())))                                                                                                       //Natural: IF #RECEIVED-IN-UNIT GT 0
            {
                pnd_Start_Date_D.setValue(pnd_Received_In_Unit);                                                                                                          //Natural: MOVE #RECEIVED-IN-UNIT TO #START-DATE-D
                DbsUtil.callnat(Cwfn3901.class , getCurrentProcessState(), pnd_Start_Date_D, pnd_End_Date_D, pnd_Non_Working_Days);                                       //Natural: CALLNAT 'CWFN3901' #START-DATE-D #END-DATE-D #NON-WORKING-DAYS
                if (condition(Global.isEscape())) return;
                pnd_Business_Days.compute(new ComputeParameters(false, pnd_Business_Days), pnd_End_Date_D.subtract(pnd_Start_Date_D));                                    //Natural: COMPUTE #BUSINESS-DAYS = #END-DATE-D - #START-DATE-D
                pnd_Business_Days.nsubtract(pnd_Non_Working_Days);                                                                                                        //Natural: COMPUTE #BUSINESS-DAYS = #BUSINESS-DAYS - #NON-WORKING-DAYS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Business_Days.setValue(0);                                                                                                                            //Natural: MOVE 0 TO #BUSINESS-DAYS
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet373 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID = MASK ( 'B'..... )
            if (condition(DbsUtil.maskMatches(cwf_Master_Index_View_Work_Prcss_Id,"'B'.....")))
            {
                decideConditionsMet373++;
                pnd_Booklet_Ctr.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #BOOKLET-CTR
            }                                                                                                                                                             //Natural: WHEN CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID = MASK ( 'F'..... )
            else if (condition(DbsUtil.maskMatches(cwf_Master_Index_View_Work_Prcss_Id,"'F'.....")))
            {
                decideConditionsMet373++;
                pnd_Forms_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #FORMS-CTR
            }                                                                                                                                                             //Natural: WHEN CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID = MASK ( 'I'..... )
            else if (condition(DbsUtil.maskMatches(cwf_Master_Index_View_Work_Prcss_Id,"'I'.....")))
            {
                decideConditionsMet373++;
                pnd_Inquire_Ctr.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #INQUIRE-CTR
            }                                                                                                                                                             //Natural: WHEN CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID = MASK ( 'R'..... )
            else if (condition(DbsUtil.maskMatches(cwf_Master_Index_View_Work_Prcss_Id,"'R'.....")))
            {
                decideConditionsMet373++;
                pnd_Research_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #RESEARCH-CTR
            }                                                                                                                                                             //Natural: WHEN CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID = MASK ( 'T'..... )
            else if (condition(DbsUtil.maskMatches(cwf_Master_Index_View_Work_Prcss_Id,"'T'.....")))
            {
                decideConditionsMet373++;
                pnd_Transaction_Ctr.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TRANSACTION-CTR
            }                                                                                                                                                             //Natural: WHEN CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID = MASK ( 'X'..... )
            else if (condition(DbsUtil.maskMatches(cwf_Master_Index_View_Work_Prcss_Id,"'X'.....")))
            {
                decideConditionsMet373++;
                pnd_Complaint_Ctr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #COMPLAINT-CTR
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
                //*  PIN-EXP
            }                                                                                                                                                             //Natural: END-DECIDE
            getReports().display(1, "/Due/Date", new FieldAttributes ("AD=I"),                                                                                            //Natural: DISPLAY ( 1 ) '/Due/Date' ( I ) #DUE-DATE ( EM = MM'/'DD'/'YY ) '/Commit/Ind' ( I ) #COMMIT-IND ( LC = �� ) '/Received/At TIAA' CWF-MASTER-INDEX-VIEW.TIAA-RCVD-DTE ( EM = MM'/'DD'/'YY ) '/Document/Received' #DOC-RECEIVED-DATE 'Received/In/Unit' #RECEIVED-IN-UNIT-A 'Business/Days/In Unit' #BUSINESS-DAYS ( NL = 4.1 LC = �� ) '/Work/Process' #WPID-SNAME '/Work/Step' CWF-MASTER-INDEX-VIEW.STEP-ID '//Status' #PARM-STATUS-NAME '/Status/Date' CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-UPDTE-DTE-TME ( EM = MM'/'DD'/'YY ) '/Partic/Name' #PARTIC-SNAME '//PIN' CWF-MASTER-INDEX-VIEW.PIN-NBR ( EM = 999999999999 ) '/Contract/Number' CWF-MASTER-INDEX-VIEW.CNTRCT-NBR ( 1 )
            		pnd_Due_Date, new ReportEditMask ("MM'/'DD'/'YY"),"/Commit/Ind", new FieldAttributes ("AD=I"),
            		pnd_Commit_Ind, new FieldAttributes("LC=��"),"/Received/At TIAA",
            		cwf_Master_Index_View_Tiaa_Rcvd_Dte, new ReportEditMask ("MM'/'DD'/'YY"),"/Document/Received",
            		pnd_Doc_Received_Date,"Received/In/Unit",
            		pnd_Received_In_Unit_A,"Business/Days/In Unit",
            		pnd_Business_Days, new NumericLength (4,1), new FieldAttributes("LC=��"),"/Work/Process",
            		pnd_Wpid_Sname,"/Work/Step",
            		cwf_Master_Index_View_Step_Id,"//Status",
            		pnd_Parm_Status_Name,"/Status/Date",
            		cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme, new ReportEditMask ("MM'/'DD'/'YY"),"/Partic/Name",
            		pnd_Partic_Sname,"//PIN",
            		cwf_Master_Index_View_Pin_Nbr, new ReportEditMask ("999999999999"),"/Contract/Number",
            		cwf_Master_Index_View_Cntrct_Nbr.getValue(1));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *SAG END-EXIT
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        //* *SAG END-EXIT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), cwf_Master_Index_View_Admin_Unit_Cde, pnd_Unit_Name);                                      //Natural: CALLNAT 'CWFN1103' CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE #UNIT-NAME
                    if (condition(Global.isEscape())) return;
                    pnd_Empl_Name.setValue(cwf_Master_Index_View_Admin_Unit_Cde);                                                                                         //Natural: MOVE CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE TO #EMPL-NAME #PARM-UNIT-CDE
                    pnd_Parm_Unit_Cde.setValue(cwf_Master_Index_View_Admin_Unit_Cde);
                    DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), cwf_Master_Index_View_Empl_Racf_Id, pnd_Empl_Name);                                        //Natural: CALLNAT 'CWFN1107' CWF-MASTER-INDEX-VIEW.EMPL-RACF-ID #EMPL-NAME
                    if (condition(Global.isEscape())) return;
                    pnd_Page_Number.nadd(1);                                                                                                                              //Natural: ADD 1 TO #PAGE-NUMBER
                    if (condition(pnd_Page_Number.equals(1)))                                                                                                             //Natural: IF #PAGE-NUMBER = 1
                    {
                        pnd_Parm_Bldg.setValue("D");                                                                                                                      //Natural: MOVE 'D' TO #PARM-BLDG
                        DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Parm_Report_No, pnd_Parm_Empl_Racf_Id, pnd_Parm_Unit_Cde, pnd_Parm_Floor,          //Natural: CALLNAT 'CWFN3910' #PARM-REPORT-NO #PARM-EMPL-RACF-ID #PARM-UNIT-CDE #PARM-FLOOR #PARM-BLDG #PARM-DROP-OFF #COMP-DATE
                            pnd_Parm_Bldg, pnd_Parm_Drop_Off, pnd_Comp_Date);
                        if (condition(Global.isEscape())) return;
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),"PAGE",pnd_Page_Number,           //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T 'PAGE' #PAGE-NUMBER ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP ) / ' ' / 'Unit    :' CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE '-' #UNIT-NAME / 'Employee:' CWF-MASTER-INDEX-VIEW.EMPL-RACF-ID '-' #EMPL-NAME '-' ( 131 )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header1_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE," ",NEWLINE,"Unit    :",cwf_Master_Index_View_Admin_Unit_Cde,"-",pnd_Unit_Name,NEWLINE,"Employee:",cwf_Master_Index_View_Empl_Racf_Id,"-",pnd_Empl_Name,"-",new 
                        RepeatItem(131));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Master_Index_View_Empl_Oprtr_CdeIsBreak = cwf_Master_Index_View_Empl_Oprtr_Cde.isBreak(endOfData);
        boolean cwf_Master_Index_View_Admin_Unit_CdeIsBreak = cwf_Master_Index_View_Admin_Unit_Cde.isBreak(endOfData);
        if (condition(cwf_Master_Index_View_Empl_Oprtr_CdeIsBreak || cwf_Master_Index_View_Admin_Unit_CdeIsBreak))
        {
            pnd_Empl_Total_Ctr.compute(new ComputeParameters(false, pnd_Empl_Total_Ctr), pnd_Booklet_Ctr.add(pnd_Forms_Ctr).add(pnd_Inquire_Ctr).add(pnd_Research_Ctr).add(pnd_Transaction_Ctr).add(pnd_Complaint_Ctr)); //Natural: COMPUTE #EMPL-TOTAL-CTR = #BOOKLET-CTR + #FORMS-CTR + #INQUIRE-CTR + #RESEARCH-CTR + #TRANSACTION-CTR + #COMPLAINT-CTR
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE," TOTAL OPEN REQUESTS FOR BOOKLETS........",pnd_Booklet_Ctr,NEWLINE," TOTAL OPEN REQUESTS FOR FORMS...........", //Natural: WRITE ( 1 ) / / ' TOTAL OPEN REQUESTS FOR BOOKLETS........' #BOOKLET-CTR / ' TOTAL OPEN REQUESTS FOR FORMS...........' #FORMS-CTR / ' TOTAL OPEN INQUIRIES....................' #INQUIRE-CTR / ' TOTAL OPEN RESEARCH.....................' #RESEARCH-CTR / ' TOTAL OPEN TRANSACTIONS.................' #TRANSACTION-CTR / ' TOTAL OPEN COMPLAINTS...................' #COMPLAINT-CTR // ' TOTAL FOR' #EMPL-NAME #EMPL-TOTAL-CTR
                pnd_Forms_Ctr,NEWLINE," TOTAL OPEN INQUIRIES....................",pnd_Inquire_Ctr,NEWLINE," TOTAL OPEN RESEARCH.....................",pnd_Research_Ctr,
                NEWLINE," TOTAL OPEN TRANSACTIONS.................",pnd_Transaction_Ctr,NEWLINE," TOTAL OPEN COMPLAINTS...................",pnd_Complaint_Ctr,
                NEWLINE,NEWLINE," TOTAL FOR",pnd_Empl_Name,pnd_Empl_Total_Ctr);
            if (condition(Global.isEscape())) return;
            pnd_Empl_Total_Ctr.reset();                                                                                                                                   //Natural: RESET #EMPL-TOTAL-CTR #BOOKLET-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANSACTION-CTR #COMPLAINT-CTR
            pnd_Booklet_Ctr.reset();
            pnd_Forms_Ctr.reset();
            pnd_Inquire_Ctr.reset();
            pnd_Research_Ctr.reset();
            pnd_Transaction_Ctr.reset();
            pnd_Complaint_Ctr.reset();
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(cwf_Master_Index_View_Admin_Unit_CdeIsBreak))
        {
            pnd_Page_Number.setValue(0);                                                                                                                                  //Natural: MOVE 0 TO #PAGE-NUMBER
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=150 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "/Due/Date", new FieldAttributes ("AD=I"),
        		pnd_Due_Date, new ReportEditMask ("MM'/'DD'/'YY"),"/Commit/Ind", new FieldAttributes ("AD=I"),
        		pnd_Commit_Ind, new FieldAttributes("LC=��"),"/Received/At TIAA",
        		cwf_Master_Index_View_Tiaa_Rcvd_Dte, new ReportEditMask ("MM'/'DD'/'YY"),"/Document/Received",
        		pnd_Doc_Received_Date,"Received/In/Unit",
        		pnd_Received_In_Unit_A,"Business/Days/In Unit",
        		pnd_Business_Days, new NumericLength (4,1), new FieldAttributes("LC=��"),"/Work/Process",
        		pnd_Wpid_Sname,"/Work/Step",
        		cwf_Master_Index_View_Step_Id,"//Status",
        		pnd_Parm_Status_Name,"/Status/Date",
        		cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme, new ReportEditMask ("MM'/'DD'/'YY"),"/Partic/Name",
        		pnd_Partic_Sname,"//PIN",
        		cwf_Master_Index_View_Pin_Nbr, new ReportEditMask ("999999999999"),"/Contract/Number",
        		cwf_Master_Index_View_Cntrct_Nbr);
    }
}
