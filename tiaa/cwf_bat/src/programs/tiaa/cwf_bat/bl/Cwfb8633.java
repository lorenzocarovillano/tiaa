/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:46:21 PM
**        * FROM NATURAL PROGRAM : Cwfb8633
************************************************************
**        * FILE NAME            : Cwfb8633.java
**        * CLASS NAME           : Cwfb8633
**        * INSTANCE NAME        : Cwfb8633
************************************************************
************************************************************************
*
* SYSTEM   : CORPORATE WORKFLOW
* PROGRAM  : CWFB8633
* TITLE    : PAYMENT OPERATIONS STAFF ACTIVITY REPORT - UNIT WISE
* FUNCTION : READ SORTED WORKFILE CREATED FROM CWF-MASTER-INDEX-VIEW
*          : (045/181) TO CREATE MONTHLY ASSOCIATE ACTIVITY REPORT
* HISTORY  : CLONED FROM CWFB8631. CREATED ON AUG 2012 - VINODH KUMAR R
*
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8633 extends BLNatBase
{
    // Data Areas
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_File;

    private DbsGroup pnd_Work_File__R_Field_1;
    private DbsField pnd_Work_File_Pnd_Wf_Unit;
    private DbsField pnd_Work_File_Pnd_Wf_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_Wpid;
    private DbsField pnd_Work_File_Pnd_Wf_Pin;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_2;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Tme;
    private DbsField pnd_Work_File__Filler1;

    private DbsGroup pnd_Work_File__R_Field_3;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Dte_A;
    private DbsField pnd_Work_File_Pnd_Wf_Due_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_4;
    private DbsField pnd_Work_File_Pnd_Wf_Due_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Due_Tme;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_5;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Tme;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Stts;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_6;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Tme;

    private DbsGroup pnd_Work_File__R_Field_7;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Dte_A;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_8;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Tme;

    private DbsGroup pnd_Work_File__R_Field_9;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_A;

    private DbsGroup pnd_Work_File__R_Field_10;
    private DbsField pnd_Work_File__Filler2;
    private DbsField pnd_Work_File_Pnd_Wf_Run;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Dte;

    private DbsGroup pnd_Work_File__R_Field_11;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Dte_A;
    private DbsField pnd_Work_File_Pnd_Wf_End_Dte;

    private DataAccessProgramView vw_cwf_Wpid;
    private DbsField cwf_Wpid_Work_Prcss_Id;
    private DbsField cwf_Wpid_Work_Prcss_Long_Nme;
    private DbsField cwf_Wpid_Work_Prcss_Short_Nme;
    private DbsField cwf_Wpid_Dlte_Oprtr_Cde;
    private DbsField cwf_Wpid_Crprte_Srvce_Time_Stndrd_Grp;
    private DbsField pnd_Hdg_Weekly;

    private DbsGroup pnd_Hdg_Weekly__R_Field_12;
    private DbsField pnd_Hdg_Weekly_Pnd_Lit1;
    private DbsField pnd_Hdg_Weekly_Pnd_Lit2;
    private DbsField pnd_Hdg_Weekly_Pnd_H_Start_Dte;
    private DbsField pnd_Hdg_Weekly_Pnd_Lit3;
    private DbsField pnd_Hdg_Weekly_Pnd_H_End_Dte;
    private DbsField pnd_Hdg_Weekly_Pnd_Lit4;
    private DbsField pnd_Hdg_Monthly;

    private DbsGroup pnd_Hdg_Monthly__R_Field_13;
    private DbsField pnd_Hdg_Monthly_Pnd_Lit5;
    private DbsField pnd_Hdg_Monthly_Pnd_H_Month;
    private DbsField pnd_Hdg_Line2;
    private DbsField pnd_Datx;
    private DbsField pnd_Newpage;
    private DbsField pnd_T_End_Dte;
    private DbsField pnd_P_Unit;
    private DbsField pnd_P_Oprtr;
    private DbsField pnd_P_Wpid;
    private DbsField pnd_P_Wpid_Desc;
    private DbsField pnd_P_Srvce_Std;

    private DbsGroup pnd_P_Srvce_Std__R_Field_14;
    private DbsField pnd_P_Srvce_Std_Pnd_P_Srvce_Std_Days;
    private DbsField pnd_Wk_Recvd_Dte_A;

    private DbsGroup pnd_Wk_Recvd_Dte_A__R_Field_15;
    private DbsField pnd_Wk_Recvd_Dte_A_Pnd_Wk_Recvd_Dte_N;
    private DbsField pnd_Wk_Recvd_Dte_D;
    private DbsField pnd_Wk_Compltd_Dte_D;
    private DbsField pnd_P_Prcss_Days;
    private DbsField pnd_P_Srvce_Std_Met;
    private DbsField pnd_P_Variance;
    private DbsField pnd_Wpid_Count;
    private DbsField pnd_Wpid_Days;
    private DbsField pnd_Wpid_Avg_Days;
    private DbsField pnd_Wpid_In_Std;
    private DbsField pnd_Wpid_In_Std_Pct;
    private DbsField pnd_Wpid_Out_Std;
    private DbsField pnd_Wpid_Out_Std_Pct;
    private DbsField pnd_Unit_Count;
    private DbsField pnd_Unit_Days;
    private DbsField pnd_Unit_Avg_Days;
    private DbsField pnd_Unit_In_Std;
    private DbsField pnd_Unit_In_Std_Pct;
    private DbsField pnd_Unit_Out_Std;
    private DbsField pnd_Unit_Out_Std_Pct;
    private DbsField pnd_Total_Count;
    private DbsField pnd_Total_Days;
    private DbsField pnd_Total_Avg_Days;
    private DbsField pnd_Total_In_Std;
    private DbsField pnd_Total_In_Std_Pct;
    private DbsField pnd_Total_Out_Std;
    private DbsField pnd_Total_Out_Std_Pct;
    private DbsField pnd_Write_Count;
    private DbsField pnd_Max_Unit;
    private DbsField pnd_Max_Wpid;
    private DbsField pnd_Max_Count;
    private DbsField pnd_I1_Unit;
    private DbsField pnd_I1_Wpid;
    private DbsField pnd_I1_Count;

    private DbsGroup pnd_Table_1;

    private DbsGroup pnd_Table_1_Pnd_T1_Oooo;
    private DbsField pnd_Table_1_Pnd_T1_Unit;

    private DbsGroup pnd_Table_1_Pnd_T1_Wwww;
    private DbsField pnd_Table_1_Pnd_T1_Wpid;

    private DbsGroup pnd_Table_1_Pnd_T1_Cccc;
    private DbsField pnd_Table_1_Pnd_T1_Count;
    private DbsField pnd_I2_Wpid;
    private DbsField pnd_I2_Count;

    private DbsGroup pnd_Table_2;

    private DbsGroup pnd_Table_2_Pnd_T2_Wwww;
    private DbsField pnd_Table_2_Pnd_T2_Wpid;

    private DbsGroup pnd_Table_2_Pnd_T2_Cccc;
    private DbsField pnd_Table_2_Pnd_T2_Count;
    private DbsField pnd_I3_Wpid;
    private DbsField pnd_I3_Count;

    private DbsGroup pnd_Table_3;

    private DbsGroup pnd_Table_3_Pnd_T3_Wwww;
    private DbsField pnd_Table_3_Pnd_T3_Wpid;

    private DbsGroup pnd_Table_3_Pnd_T3_Cccc;
    private DbsField pnd_Table_3_Pnd_T3_Count;
    private DbsField pnd_I4_Wpid;

    private DbsGroup pnd_T4_Wwww;
    private DbsField pnd_T4_Wwww_Pnd_T4_Wpid;
    private DbsField pnd_T4_Wwww_Pnd_T4_Wpid_Desc;
    private DbsField pnd_Wk_Gt_Days;
    private DbsField pnd_Wk_T_Days;
    private DbsField pnd_Wk_T_Pct;
    private DbsField pnd_Wk_Gt_Days_U;
    private DbsField pnd_Wk_T_Days_U;
    private DbsField pnd_Wk_T_Pct_U;
    private DbsField pnd_End_Of_Loop;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Wf_WpidOld;
    private DbsField readWork01Pnd_Wf_UnitOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // Local Variables
        pnd_Work_File = localVariables.newFieldInRecord("pnd_Work_File", "#WORK-FILE", FieldType.STRING, 258);

        pnd_Work_File__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_1", "REDEFINE", pnd_Work_File);
        pnd_Work_File_Pnd_Wf_Unit = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit", "#WF-UNIT", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Oprtr = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Oprtr", "#WF-OPRTR", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Wpid = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Wpid", "#WF-WPID", FieldType.STRING, 6);
        pnd_Work_File_Pnd_Wf_Pin = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Pin", "#WF-PIN", FieldType.NUMERIC, 12);
        pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme", "#WF-RECVD-DTE-TME", FieldType.STRING, 
            15);

        pnd_Work_File__R_Field_2 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_2", "REDEFINE", pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Recvd_Dte = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Dte", "#WF-RECVD-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Recvd_Tme = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Tme", "#WF-RECVD-TME", FieldType.NUMERIC, 
            4);
        pnd_Work_File__Filler1 = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File__Filler1", "_FILLER1", FieldType.STRING, 3);

        pnd_Work_File__R_Field_3 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_3", "REDEFINE", pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Recvd_Dte_A = pnd_Work_File__R_Field_3.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Dte_A", "#WF-RECVD-DTE-A", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_Due_Dte_Tme = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Due_Dte_Tme", "#WF-DUE-DTE-TME", FieldType.STRING, 
            15);

        pnd_Work_File__R_Field_4 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_4", "REDEFINE", pnd_Work_File_Pnd_Wf_Due_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Due_Dte = pnd_Work_File__R_Field_4.newFieldInGroup("pnd_Work_File_Pnd_Wf_Due_Dte", "#WF-DUE-DTE", FieldType.NUMERIC, 8);
        pnd_Work_File_Pnd_Wf_Due_Tme = pnd_Work_File__R_Field_4.newFieldInGroup("pnd_Work_File_Pnd_Wf_Due_Tme", "#WF-DUE-TME", FieldType.NUMERIC, 7);
        pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme", "#WF-COMPLTD-DTE-TME", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_5 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_5", "REDEFINE", pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Compltd_Dte = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Dte", "#WF-COMPLTD-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Compltd_Tme = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Tme", "#WF-COMPLTD-TME", FieldType.NUMERIC, 
            7);
        pnd_Work_File_Pnd_Wf_Compltd_Oprtr = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Oprtr", "#WF-COMPLTD-OPRTR", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_Unit_Open_Stts = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Stts", "#WF-UNIT-OPEN-STTS", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme", "#WF-UNIT-OPEN-DTE-TME", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_6 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_6", "REDEFINE", pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Unit_Open_Dte = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Dte", "#WF-UNIT-OPEN-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Unit_Open_Tme = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Tme", "#WF-UNIT-OPEN-TME", FieldType.NUMERIC, 
            7);

        pnd_Work_File__R_Field_7 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_7", "REDEFINE", pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Unit_Open_Dte_A = pnd_Work_File__R_Field_7.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Dte_A", "#WF-UNIT-OPEN-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts", "#WF-UNIT-CLSD-STTS", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme", "#WF-UNIT-CLSD-DTE-TME", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_8 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_8", "REDEFINE", pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte = pnd_Work_File__R_Field_8.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte", "#WF-UNIT-CLSD-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Tme = pnd_Work_File__R_Field_8.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Tme", "#WF-UNIT-CLSD-TME", FieldType.NUMERIC, 
            7);

        pnd_Work_File__R_Field_9 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_9", "REDEFINE", pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_A = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_A", "#WF-UNIT-CLSD-DTE-A", 
            FieldType.STRING, 8);

        pnd_Work_File__R_Field_10 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_10", "REDEFINE", pnd_Work_File);
        pnd_Work_File__Filler2 = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File__Filler2", "_FILLER2", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Run = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File_Pnd_Wf_Run", "#WF-RUN", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Start_Dte = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Dte", "#WF-START-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File__R_Field_11 = pnd_Work_File__R_Field_10.newGroupInGroup("pnd_Work_File__R_Field_11", "REDEFINE", pnd_Work_File_Pnd_Wf_Start_Dte);
        pnd_Work_File_Pnd_Wf_Start_Dte_A = pnd_Work_File__R_Field_11.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Dte_A", "#WF-START-DTE-A", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_End_Dte = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File_Pnd_Wf_End_Dte", "#WF-END-DTE", FieldType.NUMERIC, 8);

        vw_cwf_Wpid = new DataAccessProgramView(new NameInfo("vw_cwf_Wpid", "CWF-WPID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wpid_Work_Prcss_Id = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Wpid_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wpid_Work_Prcss_Long_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        cwf_Wpid_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        cwf_Wpid_Work_Prcss_Short_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wpid_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        cwf_Wpid_Dlte_Oprtr_Cde = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DLTE_OPRTR_CDE");
        cwf_Wpid_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        cwf_Wpid_Crprte_Srvce_Time_Stndrd_Grp = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Crprte_Srvce_Time_Stndrd_Grp", "CRPRTE-SRVCE-TIME-STNDRD-GRP", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "CRPRTE_SRVCE_TIME_STNDRD_GRP");
        cwf_Wpid_Crprte_Srvce_Time_Stndrd_Grp.setDdmHeader("CORPORATE SERVICE/TIME STANDARD");
        registerRecord(vw_cwf_Wpid);

        pnd_Hdg_Weekly = localVariables.newFieldInRecord("pnd_Hdg_Weekly", "#HDG-WEEKLY", FieldType.STRING, 28);

        pnd_Hdg_Weekly__R_Field_12 = localVariables.newGroupInRecord("pnd_Hdg_Weekly__R_Field_12", "REDEFINE", pnd_Hdg_Weekly);
        pnd_Hdg_Weekly_Pnd_Lit1 = pnd_Hdg_Weekly__R_Field_12.newFieldInGroup("pnd_Hdg_Weekly_Pnd_Lit1", "#LIT1", FieldType.STRING, 2);
        pnd_Hdg_Weekly_Pnd_Lit2 = pnd_Hdg_Weekly__R_Field_12.newFieldInGroup("pnd_Hdg_Weekly_Pnd_Lit2", "#LIT2", FieldType.STRING, 5);
        pnd_Hdg_Weekly_Pnd_H_Start_Dte = pnd_Hdg_Weekly__R_Field_12.newFieldInGroup("pnd_Hdg_Weekly_Pnd_H_Start_Dte", "#H-START-DTE", FieldType.NUMERIC, 
            8);
        pnd_Hdg_Weekly_Pnd_Lit3 = pnd_Hdg_Weekly__R_Field_12.newFieldInGroup("pnd_Hdg_Weekly_Pnd_Lit3", "#LIT3", FieldType.STRING, 4);
        pnd_Hdg_Weekly_Pnd_H_End_Dte = pnd_Hdg_Weekly__R_Field_12.newFieldInGroup("pnd_Hdg_Weekly_Pnd_H_End_Dte", "#H-END-DTE", FieldType.NUMERIC, 8);
        pnd_Hdg_Weekly_Pnd_Lit4 = pnd_Hdg_Weekly__R_Field_12.newFieldInGroup("pnd_Hdg_Weekly_Pnd_Lit4", "#LIT4", FieldType.STRING, 1);
        pnd_Hdg_Monthly = localVariables.newFieldInRecord("pnd_Hdg_Monthly", "#HDG-MONTHLY", FieldType.STRING, 28);

        pnd_Hdg_Monthly__R_Field_13 = localVariables.newGroupInRecord("pnd_Hdg_Monthly__R_Field_13", "REDEFINE", pnd_Hdg_Monthly);
        pnd_Hdg_Monthly_Pnd_Lit5 = pnd_Hdg_Monthly__R_Field_13.newFieldInGroup("pnd_Hdg_Monthly_Pnd_Lit5", "#LIT5", FieldType.STRING, 13);
        pnd_Hdg_Monthly_Pnd_H_Month = pnd_Hdg_Monthly__R_Field_13.newFieldInGroup("pnd_Hdg_Monthly_Pnd_H_Month", "#H-MONTH", FieldType.STRING, 15);
        pnd_Hdg_Line2 = localVariables.newFieldInRecord("pnd_Hdg_Line2", "#HDG-LINE2", FieldType.STRING, 28);
        pnd_Datx = localVariables.newFieldInRecord("pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Newpage = localVariables.newFieldInRecord("pnd_Newpage", "#NEWPAGE", FieldType.BOOLEAN, 1);
        pnd_T_End_Dte = localVariables.newFieldInRecord("pnd_T_End_Dte", "#T-END-DTE", FieldType.NUMERIC, 8);
        pnd_P_Unit = localVariables.newFieldInRecord("pnd_P_Unit", "#P-UNIT", FieldType.STRING, 8);
        pnd_P_Oprtr = localVariables.newFieldInRecord("pnd_P_Oprtr", "#P-OPRTR", FieldType.STRING, 8);
        pnd_P_Wpid = localVariables.newFieldInRecord("pnd_P_Wpid", "#P-WPID", FieldType.STRING, 6);
        pnd_P_Wpid_Desc = localVariables.newFieldInRecord("pnd_P_Wpid_Desc", "#P-WPID-DESC", FieldType.STRING, 15);
        pnd_P_Srvce_Std = localVariables.newFieldInRecord("pnd_P_Srvce_Std", "#P-SRVCE-STD", FieldType.NUMERIC, 7);

        pnd_P_Srvce_Std__R_Field_14 = localVariables.newGroupInRecord("pnd_P_Srvce_Std__R_Field_14", "REDEFINE", pnd_P_Srvce_Std);
        pnd_P_Srvce_Std_Pnd_P_Srvce_Std_Days = pnd_P_Srvce_Std__R_Field_14.newFieldInGroup("pnd_P_Srvce_Std_Pnd_P_Srvce_Std_Days", "#P-SRVCE-STD-DAYS", 
            FieldType.NUMERIC, 3);
        pnd_Wk_Recvd_Dte_A = localVariables.newFieldInRecord("pnd_Wk_Recvd_Dte_A", "#WK-RECVD-DTE-A", FieldType.STRING, 8);

        pnd_Wk_Recvd_Dte_A__R_Field_15 = localVariables.newGroupInRecord("pnd_Wk_Recvd_Dte_A__R_Field_15", "REDEFINE", pnd_Wk_Recvd_Dte_A);
        pnd_Wk_Recvd_Dte_A_Pnd_Wk_Recvd_Dte_N = pnd_Wk_Recvd_Dte_A__R_Field_15.newFieldInGroup("pnd_Wk_Recvd_Dte_A_Pnd_Wk_Recvd_Dte_N", "#WK-RECVD-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_Wk_Recvd_Dte_D = localVariables.newFieldInRecord("pnd_Wk_Recvd_Dte_D", "#WK-RECVD-DTE-D", FieldType.DATE);
        pnd_Wk_Compltd_Dte_D = localVariables.newFieldInRecord("pnd_Wk_Compltd_Dte_D", "#WK-COMPLTD-DTE-D", FieldType.DATE);
        pnd_P_Prcss_Days = localVariables.newFieldInRecord("pnd_P_Prcss_Days", "#P-PRCSS-DAYS", FieldType.NUMERIC, 4);
        pnd_P_Srvce_Std_Met = localVariables.newFieldInRecord("pnd_P_Srvce_Std_Met", "#P-SRVCE-STD-MET", FieldType.STRING, 1);
        pnd_P_Variance = localVariables.newFieldInRecord("pnd_P_Variance", "#P-VARIANCE", FieldType.NUMERIC, 4);
        pnd_Wpid_Count = localVariables.newFieldInRecord("pnd_Wpid_Count", "#WPID-COUNT", FieldType.NUMERIC, 9);
        pnd_Wpid_Days = localVariables.newFieldInRecord("pnd_Wpid_Days", "#WPID-DAYS", FieldType.NUMERIC, 9);
        pnd_Wpid_Avg_Days = localVariables.newFieldInRecord("pnd_Wpid_Avg_Days", "#WPID-AVG-DAYS", FieldType.NUMERIC, 6, 2);
        pnd_Wpid_In_Std = localVariables.newFieldInRecord("pnd_Wpid_In_Std", "#WPID-IN-STD", FieldType.NUMERIC, 5);
        pnd_Wpid_In_Std_Pct = localVariables.newFieldInRecord("pnd_Wpid_In_Std_Pct", "#WPID-IN-STD-PCT", FieldType.NUMERIC, 3);
        pnd_Wpid_Out_Std = localVariables.newFieldInRecord("pnd_Wpid_Out_Std", "#WPID-OUT-STD", FieldType.NUMERIC, 5);
        pnd_Wpid_Out_Std_Pct = localVariables.newFieldInRecord("pnd_Wpid_Out_Std_Pct", "#WPID-OUT-STD-PCT", FieldType.NUMERIC, 3);
        pnd_Unit_Count = localVariables.newFieldInRecord("pnd_Unit_Count", "#UNIT-COUNT", FieldType.NUMERIC, 9);
        pnd_Unit_Days = localVariables.newFieldInRecord("pnd_Unit_Days", "#UNIT-DAYS", FieldType.NUMERIC, 9);
        pnd_Unit_Avg_Days = localVariables.newFieldInRecord("pnd_Unit_Avg_Days", "#UNIT-AVG-DAYS", FieldType.NUMERIC, 6, 2);
        pnd_Unit_In_Std = localVariables.newFieldInRecord("pnd_Unit_In_Std", "#UNIT-IN-STD", FieldType.NUMERIC, 5);
        pnd_Unit_In_Std_Pct = localVariables.newFieldInRecord("pnd_Unit_In_Std_Pct", "#UNIT-IN-STD-PCT", FieldType.NUMERIC, 3);
        pnd_Unit_Out_Std = localVariables.newFieldInRecord("pnd_Unit_Out_Std", "#UNIT-OUT-STD", FieldType.NUMERIC, 5);
        pnd_Unit_Out_Std_Pct = localVariables.newFieldInRecord("pnd_Unit_Out_Std_Pct", "#UNIT-OUT-STD-PCT", FieldType.NUMERIC, 3);
        pnd_Total_Count = localVariables.newFieldInRecord("pnd_Total_Count", "#TOTAL-COUNT", FieldType.NUMERIC, 9);
        pnd_Total_Days = localVariables.newFieldInRecord("pnd_Total_Days", "#TOTAL-DAYS", FieldType.NUMERIC, 9);
        pnd_Total_Avg_Days = localVariables.newFieldInRecord("pnd_Total_Avg_Days", "#TOTAL-AVG-DAYS", FieldType.NUMERIC, 6, 2);
        pnd_Total_In_Std = localVariables.newFieldInRecord("pnd_Total_In_Std", "#TOTAL-IN-STD", FieldType.NUMERIC, 5);
        pnd_Total_In_Std_Pct = localVariables.newFieldInRecord("pnd_Total_In_Std_Pct", "#TOTAL-IN-STD-PCT", FieldType.NUMERIC, 3);
        pnd_Total_Out_Std = localVariables.newFieldInRecord("pnd_Total_Out_Std", "#TOTAL-OUT-STD", FieldType.NUMERIC, 5);
        pnd_Total_Out_Std_Pct = localVariables.newFieldInRecord("pnd_Total_Out_Std_Pct", "#TOTAL-OUT-STD-PCT", FieldType.NUMERIC, 3);
        pnd_Write_Count = localVariables.newFieldInRecord("pnd_Write_Count", "#WRITE-COUNT", FieldType.NUMERIC, 9);
        pnd_Max_Unit = localVariables.newFieldInRecord("pnd_Max_Unit", "#MAX-UNIT", FieldType.NUMERIC, 2);
        pnd_Max_Wpid = localVariables.newFieldInRecord("pnd_Max_Wpid", "#MAX-WPID", FieldType.NUMERIC, 3);
        pnd_Max_Count = localVariables.newFieldInRecord("pnd_Max_Count", "#MAX-COUNT", FieldType.NUMERIC, 1);
        pnd_I1_Unit = localVariables.newFieldInRecord("pnd_I1_Unit", "#I1-UNIT", FieldType.NUMERIC, 2);
        pnd_I1_Wpid = localVariables.newFieldInRecord("pnd_I1_Wpid", "#I1-WPID", FieldType.NUMERIC, 2);
        pnd_I1_Count = localVariables.newFieldInRecord("pnd_I1_Count", "#I1-COUNT", FieldType.NUMERIC, 2);

        pnd_Table_1 = localVariables.newGroupInRecord("pnd_Table_1", "#TABLE-1");

        pnd_Table_1_Pnd_T1_Oooo = pnd_Table_1.newGroupArrayInGroup("pnd_Table_1_Pnd_T1_Oooo", "#T1-OOOO", new DbsArrayController(1, 99));
        pnd_Table_1_Pnd_T1_Unit = pnd_Table_1_Pnd_T1_Oooo.newFieldInGroup("pnd_Table_1_Pnd_T1_Unit", "#T1-UNIT", FieldType.STRING, 8);

        pnd_Table_1_Pnd_T1_Wwww = pnd_Table_1_Pnd_T1_Oooo.newGroupArrayInGroup("pnd_Table_1_Pnd_T1_Wwww", "#T1-WWWW", new DbsArrayController(1, 999));
        pnd_Table_1_Pnd_T1_Wpid = pnd_Table_1_Pnd_T1_Wwww.newFieldInGroup("pnd_Table_1_Pnd_T1_Wpid", "#T1-WPID", FieldType.STRING, 6);

        pnd_Table_1_Pnd_T1_Cccc = pnd_Table_1_Pnd_T1_Wwww.newGroupArrayInGroup("pnd_Table_1_Pnd_T1_Cccc", "#T1-CCCC", new DbsArrayController(1, 7));
        pnd_Table_1_Pnd_T1_Count = pnd_Table_1_Pnd_T1_Cccc.newFieldInGroup("pnd_Table_1_Pnd_T1_Count", "#T1-COUNT", FieldType.NUMERIC, 4);
        pnd_I2_Wpid = localVariables.newFieldInRecord("pnd_I2_Wpid", "#I2-WPID", FieldType.NUMERIC, 2);
        pnd_I2_Count = localVariables.newFieldInRecord("pnd_I2_Count", "#I2-COUNT", FieldType.NUMERIC, 2);

        pnd_Table_2 = localVariables.newGroupInRecord("pnd_Table_2", "#TABLE-2");

        pnd_Table_2_Pnd_T2_Wwww = pnd_Table_2.newGroupArrayInGroup("pnd_Table_2_Pnd_T2_Wwww", "#T2-WWWW", new DbsArrayController(1, 999));
        pnd_Table_2_Pnd_T2_Wpid = pnd_Table_2_Pnd_T2_Wwww.newFieldInGroup("pnd_Table_2_Pnd_T2_Wpid", "#T2-WPID", FieldType.STRING, 6);

        pnd_Table_2_Pnd_T2_Cccc = pnd_Table_2_Pnd_T2_Wwww.newGroupArrayInGroup("pnd_Table_2_Pnd_T2_Cccc", "#T2-CCCC", new DbsArrayController(1, 7));
        pnd_Table_2_Pnd_T2_Count = pnd_Table_2_Pnd_T2_Cccc.newFieldInGroup("pnd_Table_2_Pnd_T2_Count", "#T2-COUNT", FieldType.NUMERIC, 5);
        pnd_I3_Wpid = localVariables.newFieldInRecord("pnd_I3_Wpid", "#I3-WPID", FieldType.NUMERIC, 2);
        pnd_I3_Count = localVariables.newFieldInRecord("pnd_I3_Count", "#I3-COUNT", FieldType.NUMERIC, 2);

        pnd_Table_3 = localVariables.newGroupInRecord("pnd_Table_3", "#TABLE-3");

        pnd_Table_3_Pnd_T3_Wwww = pnd_Table_3.newGroupArrayInGroup("pnd_Table_3_Pnd_T3_Wwww", "#T3-WWWW", new DbsArrayController(1, 24));
        pnd_Table_3_Pnd_T3_Wpid = pnd_Table_3_Pnd_T3_Wwww.newFieldInGroup("pnd_Table_3_Pnd_T3_Wpid", "#T3-WPID", FieldType.STRING, 6);

        pnd_Table_3_Pnd_T3_Cccc = pnd_Table_3_Pnd_T3_Wwww.newGroupArrayInGroup("pnd_Table_3_Pnd_T3_Cccc", "#T3-CCCC", new DbsArrayController(1, 7));
        pnd_Table_3_Pnd_T3_Count = pnd_Table_3_Pnd_T3_Cccc.newFieldInGroup("pnd_Table_3_Pnd_T3_Count", "#T3-COUNT", FieldType.NUMERIC, 9);
        pnd_I4_Wpid = localVariables.newFieldInRecord("pnd_I4_Wpid", "#I4-WPID", FieldType.NUMERIC, 2);

        pnd_T4_Wwww = localVariables.newGroupArrayInRecord("pnd_T4_Wwww", "#T4-WWWW", new DbsArrayController(1, 999));
        pnd_T4_Wwww_Pnd_T4_Wpid = pnd_T4_Wwww.newFieldInGroup("pnd_T4_Wwww_Pnd_T4_Wpid", "#T4-WPID", FieldType.STRING, 6);
        pnd_T4_Wwww_Pnd_T4_Wpid_Desc = pnd_T4_Wwww.newFieldInGroup("pnd_T4_Wwww_Pnd_T4_Wpid_Desc", "#T4-WPID-DESC", FieldType.STRING, 15);
        pnd_Wk_Gt_Days = localVariables.newFieldInRecord("pnd_Wk_Gt_Days", "#WK-GT-DAYS", FieldType.NUMERIC, 9);
        pnd_Wk_T_Days = localVariables.newFieldInRecord("pnd_Wk_T_Days", "#WK-T-DAYS", FieldType.NUMERIC, 5);
        pnd_Wk_T_Pct = localVariables.newFieldArrayInRecord("pnd_Wk_T_Pct", "#WK-T-PCT", FieldType.NUMERIC, 5, new DbsArrayController(1, 7));
        pnd_Wk_Gt_Days_U = localVariables.newFieldInRecord("pnd_Wk_Gt_Days_U", "#WK-GT-DAYS-U", FieldType.NUMERIC, 9);
        pnd_Wk_T_Days_U = localVariables.newFieldInRecord("pnd_Wk_T_Days_U", "#WK-T-DAYS-U", FieldType.NUMERIC, 5);
        pnd_Wk_T_Pct_U = localVariables.newFieldArrayInRecord("pnd_Wk_T_Pct_U", "#WK-T-PCT-U", FieldType.NUMERIC, 5, new DbsArrayController(1, 7));
        pnd_End_Of_Loop = localVariables.newFieldInRecord("pnd_End_Of_Loop", "#END-OF-LOOP", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Wf_WpidOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Wf_Wpid_OLD", "Pnd_Wf_Wpid_OLD", FieldType.STRING, 6);
        readWork01Pnd_Wf_UnitOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Wf_Unit_OLD", "Pnd_Wf_Unit_OLD", FieldType.STRING, 8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Wpid.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Max_Unit.setInitialValue(99);
        pnd_Max_Wpid.setInitialValue(999);
        pnd_Max_Count.setInitialValue(7);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8633() throws Exception
    {
        super("Cwfb8633");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 55 LS = 133;//Natural: FORMAT ( 01 ) PS = 55 LS = 133
        getWorkFiles().read(1, pnd_Work_File);                                                                                                                            //Natural: READ WORK 1 ONCE #WORK-FILE
        if (condition(pnd_Work_File_Pnd_Wf_Run.equals("WEEKLY  ")))                                                                                                       //Natural: IF #WF-RUN = 'WEEKLY  '
        {
            pnd_Hdg_Weekly.reset();                                                                                                                                       //Natural: RESET #HDG-WEEKLY
            pnd_Hdg_Weekly_Pnd_Lit2.setValue("From ");                                                                                                                    //Natural: ASSIGN #LIT2 := 'From '
            pnd_Hdg_Weekly_Pnd_H_Start_Dte.setValue(pnd_Work_File_Pnd_Wf_Start_Dte);                                                                                      //Natural: ASSIGN #H-START-DTE := #WF-START-DTE
            pnd_Hdg_Weekly_Pnd_Lit3.setValue(" To ");                                                                                                                     //Natural: ASSIGN #LIT3 := ' To '
            pnd_Hdg_Weekly_Pnd_H_End_Dte.setValue(pnd_Work_File_Pnd_Wf_End_Dte);                                                                                          //Natural: ASSIGN #H-END-DTE := #WF-END-DTE
            pnd_Hdg_Line2.setValue(pnd_Hdg_Weekly);                                                                                                                       //Natural: MOVE #HDG-WEEKLY TO #HDG-LINE2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Hdg_Monthly.reset();                                                                                                                                      //Natural: RESET #HDG-MONTHLY
            pnd_Hdg_Monthly_Pnd_Lit5.setValue("For Month of ");                                                                                                           //Natural: ASSIGN #LIT5 := 'For Month of '
            pnd_Datx.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_File_Pnd_Wf_Start_Dte_A);                                                                     //Natural: MOVE EDITED #WF-START-DTE-A TO #DATX ( EM = YYYYMMDD )
            pnd_Hdg_Monthly_Pnd_H_Month.setValueEdited(pnd_Datx,new ReportEditMask("LLLLLLLLL' 'YYYY"));                                                                  //Natural: MOVE EDITED #DATX ( EM = LLLLLLLLL' 'YYYY ) TO #H-MONTH
            pnd_Hdg_Line2.setValue(pnd_Hdg_Monthly);                                                                                                                      //Natural: MOVE #HDG-MONTHLY TO #HDG-LINE2
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 30X 'PAYMENT OPERATIONS - WORK MANAGEMENT - CWF REPORT 1' / *DATX ( EM = LLL' 'DD', 'YYYY ) 33X #HDG-LINE2 / 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZZ9 ) 14X
        pnd_Table_3_Pnd_T3_Wpid.getValue(1).setValue("IA SAP");                                                                                                           //Natural: ASSIGN #T3-WPID ( 1 ) := 'IA SAP'
        pnd_Table_3_Pnd_T3_Wpid.getValue(2).setValue("IA SR");                                                                                                            //Natural: ASSIGN #T3-WPID ( 2 ) := 'IA SR'
        pnd_Table_3_Pnd_T3_Wpid.getValue(3).setValue("IAID");                                                                                                             //Natural: ASSIGN #T3-WPID ( 3 ) := 'IAID'
        pnd_Table_3_Pnd_T3_Wpid.getValue(4).setValue("IAIMD");                                                                                                            //Natural: ASSIGN #T3-WPID ( 4 ) := 'IAIMD'
        pnd_Table_3_Pnd_T3_Wpid.getValue(5).setValue("IAIOD");                                                                                                            //Natural: ASSIGN #T3-WPID ( 5 ) := 'IAIOD'
        pnd_Table_3_Pnd_T3_Wpid.getValue(6).setValue("IAIOEE");                                                                                                           //Natural: ASSIGN #T3-WPID ( 6 ) := 'IAIOEE'
        pnd_Table_3_Pnd_T3_Wpid.getValue(7).setValue("RA OI");                                                                                                            //Natural: ASSIGN #T3-WPID ( 7 ) := 'RA OI'
        pnd_Table_3_Pnd_T3_Wpid.getValue(8).setValue("RAIDAR");                                                                                                           //Natural: ASSIGN #T3-WPID ( 8 ) := 'RAIDAR'
        pnd_Table_3_Pnd_T3_Wpid.getValue(9).setValue("RAIDRC");                                                                                                           //Natural: ASSIGN #T3-WPID ( 9 ) := 'RAIDRC'
        pnd_Table_3_Pnd_T3_Wpid.getValue(10).setValue("RAIDRO");                                                                                                          //Natural: ASSIGN #T3-WPID ( 10 ) := 'RAIDRO'
        pnd_Table_3_Pnd_T3_Wpid.getValue(11).setValue("RAIHH");                                                                                                           //Natural: ASSIGN #T3-WPID ( 11 ) := 'RAIHH'
        pnd_Table_3_Pnd_T3_Wpid.getValue(12).setValue("RAIOE");                                                                                                           //Natural: ASSIGN #T3-WPID ( 12 ) := 'RAIOE'
        pnd_Table_3_Pnd_T3_Wpid.getValue(13).setValue("RAIOF");                                                                                                           //Natural: ASSIGN #T3-WPID ( 13 ) := 'RAIOF'
        pnd_Table_3_Pnd_T3_Wpid.getValue(14).setValue("TA HRT");                                                                                                          //Natural: ASSIGN #T3-WPID ( 14 ) := 'TA HRT'
        pnd_Table_3_Pnd_T3_Wpid.getValue(15).setValue("TA HV");                                                                                                           //Natural: ASSIGN #T3-WPID ( 15 ) := 'TA HV'
        pnd_Table_3_Pnd_T3_Wpid.getValue(16).setValue("TA SRM");                                                                                                          //Natural: ASSIGN #T3-WPID ( 16 ) := 'TA SRM'
        pnd_Table_3_Pnd_T3_Wpid.getValue(17).setValue("TAIDAM");                                                                                                          //Natural: ASSIGN #T3-WPID ( 17 ) := 'TAIDAM'
        pnd_Table_3_Pnd_T3_Wpid.getValue(18).setValue("TAIDLS");                                                                                                          //Natural: ASSIGN #T3-WPID ( 18 ) := 'TAIDLS'
        pnd_Table_3_Pnd_T3_Wpid.getValue(19).setValue("TAIEC");                                                                                                           //Natural: ASSIGN #T3-WPID ( 19 ) := 'TAIEC'
        pnd_Table_3_Pnd_T3_Wpid.getValue(20).setValue("TAIEL");                                                                                                           //Natural: ASSIGN #T3-WPID ( 20 ) := 'TAIEL'
        pnd_Table_3_Pnd_T3_Wpid.getValue(21).setValue("TAIHD");                                                                                                           //Natural: ASSIGN #T3-WPID ( 21 ) := 'TAIHD'
        pnd_Table_3_Pnd_T3_Wpid.getValue(22).setValue("TIPSA");                                                                                                           //Natural: ASSIGN #T3-WPID ( 22 ) := 'TIPSA'
        pnd_Table_3_Pnd_T3_Wpid.getValue(23).setValue("TZ MAU");                                                                                                          //Natural: ASSIGN #T3-WPID ( 23 ) := 'TZ MAU'
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK 1 #WORK-FILE
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Work_File)))
        {
            CheckAtStartofData1206();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*                                                                                                                                                           //Natural: AT BREAK #WF-WPID
            //*                                                                                                                                                           //Natural: AT BREAK #WF-UNIT
                                                                                                                                                                          //Natural: PERFORM CALCULATE-PRCSS-DAYS
            sub_Calculate_Prcss_Days();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_P_Variance.compute(new ComputeParameters(false, pnd_P_Variance), pnd_P_Prcss_Days.subtract(pnd_P_Srvce_Std_Pnd_P_Srvce_Std_Days));                        //Natural: COMPUTE #P-VARIANCE = #P-PRCSS-DAYS - #P-SRVCE-STD-DAYS
            if (condition(pnd_P_Variance.greater(getZero())))                                                                                                             //Natural: IF #P-VARIANCE GT 0
            {
                pnd_P_Srvce_Std_Met.setValue("N");                                                                                                                        //Natural: ASSIGN #P-SRVCE-STD-MET := 'N'
                pnd_Wpid_Out_Std.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #WPID-OUT-STD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_P_Srvce_Std_Met.setValue("Y");                                                                                                                        //Natural: ASSIGN #P-SRVCE-STD-MET := 'Y'
                pnd_Wpid_In_Std.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WPID-IN-STD
            }                                                                                                                                                             //Natural: END-IF
            //*   PIN-EXP
            getReports().display(1, "/UNIT   ",                                                                                                                           //Natural: DISPLAY ( 1 ) '/UNIT   ' #WF-UNIT ( IS = ON ) '/USER ID' #WF-OPRTR '/WPID' #WF-WPID '/PIN' #WF-PIN ( EM = 9 ( 12 ) ) 'DATE/RECVD' #WF-RECVD-DTE ( EM = 9999/99/99 ) 'TIME/RECVD' #WF-RECVD-TME ( EM = 99:99 ) 'DUE/DATE' #WF-DUE-DTE ( EM = 9999/99/99 ) 'DATE/COMPLTD' #WF-COMPLTD-DTE ( EM = 9999/99/99 ) 'UNIT/RECVD' #WF-UNIT-OPEN-DTE ( EM = 9999/99/99 ) 'UNIT/COMPLTD' #WF-UNIT-CLSD-DTE ( EM = 9999/99/99 ) 'PRCSS/DAYS' #P-PRCSS-DAYS 'SVC/STD' #P-SRVCE-STD-DAYS 'STD/MET?' #P-SRVCE-STD-MET 'LAST/OPRTR' #WF-COMPLTD-OPRTR
            		pnd_Work_File_Pnd_Wf_Unit, new IdenticalSuppress(true),"/USER ID",
            		pnd_Work_File_Pnd_Wf_Oprtr,"/WPID",
            		pnd_Work_File_Pnd_Wf_Wpid,"/PIN",
            		pnd_Work_File_Pnd_Wf_Pin, new ReportEditMask ("999999999999"),"DATE/RECVD",
            		pnd_Work_File_Pnd_Wf_Recvd_Dte, new ReportEditMask ("9999/99/99"),"TIME/RECVD",
            		pnd_Work_File_Pnd_Wf_Recvd_Tme, new ReportEditMask ("99:99"),"DUE/DATE",
            		pnd_Work_File_Pnd_Wf_Due_Dte, new ReportEditMask ("9999/99/99"),"DATE/COMPLTD",
            		pnd_Work_File_Pnd_Wf_Compltd_Dte, new ReportEditMask ("9999/99/99"),"UNIT/RECVD",
            		pnd_Work_File_Pnd_Wf_Unit_Open_Dte, new ReportEditMask ("9999/99/99"),"UNIT/COMPLTD",
            		pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte, new ReportEditMask ("9999/99/99"),"PRCSS/DAYS",
            		pnd_P_Prcss_Days,"SVC/STD",
            		pnd_P_Srvce_Std_Pnd_P_Srvce_Std_Days,"STD/MET?",
            		pnd_P_Srvce_Std_Met,"LAST/OPRTR",
            		pnd_Work_File_Pnd_Wf_Compltd_Oprtr);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    '/DESCRIPTION' #P-WPID-DESC
            //*    '/PIN'         #WF-PIN        (EM=9(7))
            pnd_Wpid_Count.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WPID-COUNT
            pnd_Wpid_Days.nadd(pnd_P_Prcss_Days);                                                                                                                         //Natural: ADD #P-PRCSS-DAYS TO #WPID-DAYS
            pnd_Write_Count.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #WRITE-COUNT
                                                                                                                                                                          //Natural: PERFORM SUMMARY-CALCULATION
            sub_Summary_Calculation();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Pnd_Wf_WpidOld.setValue(pnd_Work_File_Pnd_Wf_Wpid);                                                                                                 //Natural: END-WORK
            readWork01Pnd_Wf_UnitOld.setValue(pnd_Work_File_Pnd_Wf_Unit);
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-WPID-DETAILS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-PRCSS-DAYS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHANGE-OF-WPID
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHANGE-OF-UNIT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-CALCULATION
        //*  SUMMARY BY WPID
        //* ***********************************************************************
        if (condition(pnd_Total_Days.greater(getZero()) && pnd_Total_Count.greater(getZero())))                                                                           //Natural: IF #TOTAL-DAYS > 0 AND #TOTAL-COUNT > 0
        {
            pnd_Total_Avg_Days.compute(new ComputeParameters(true, pnd_Total_Avg_Days), pnd_Total_Days.divide(pnd_Total_Count));                                          //Natural: COMPUTE ROUNDED #TOTAL-AVG-DAYS = #TOTAL-DAYS / #TOTAL-COUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Total_Avg_Days.setValue(0);                                                                                                                               //Natural: ASSIGN #TOTAL-AVG-DAYS := 0
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Total_In_Std.greater(getZero()) && pnd_Total_Count.greater(getZero())))                                                                         //Natural: IF #TOTAL-IN-STD > 0 AND #TOTAL-COUNT > 0
        {
            pnd_Total_In_Std_Pct.compute(new ComputeParameters(true, pnd_Total_In_Std_Pct), (pnd_Total_In_Std.multiply(100)).divide(pnd_Total_Count));                    //Natural: COMPUTE ROUNDED #TOTAL-IN-STD-PCT = ( #TOTAL-IN-STD * 100 ) / #TOTAL-COUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Total_In_Std_Pct.setValue(0);                                                                                                                             //Natural: ASSIGN #TOTAL-IN-STD-PCT := 0
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Total_Out_Std_Pct.compute(new ComputeParameters(true, pnd_Total_Out_Std_Pct), DbsField.subtract(100,pnd_Total_In_Std_Pct));                                   //Natural: COMPUTE ROUNDED #TOTAL-OUT-STD-PCT = 100 - #TOTAL-IN-STD-PCT
        getReports().write(1, NEWLINE,"***GRAND TOTAL***",new ColumnSpacing(25),pnd_Total_Count, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(91),pnd_Total_Days,      //Natural: WRITE ( 1 ) / '***GRAND TOTAL***' 25X #TOTAL-COUNT ( EM = Z,ZZZ,ZZ9 ) 91T #TOTAL-DAYS ( EM = Z,ZZZ,ZZ9 ) 111T '    Avg. Days' #TOTAL-AVG-DAYS / 111T ' Total In-Std ' #TOTAL-IN-STD / 111T 'Total Out-Std ' #TOTAL-OUT-STD / 111T '     % In-Std ' #TOTAL-IN-STD-PCT '%' / 111T '    % Out-Std ' #TOTAL-OUT-STD-PCT '%' / '=' ( 132 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(111),"    Avg. Days",pnd_Total_Avg_Days,NEWLINE,new TabSetting(111)," Total In-Std ",pnd_Total_In_Std,NEWLINE,new 
            TabSetting(111),"Total Out-Std ",pnd_Total_Out_Std,NEWLINE,new TabSetting(111),"     % In-Std ",pnd_Total_In_Std_Pct,"%",NEWLINE,new TabSetting(111),"    % Out-Std ",pnd_Total_Out_Std_Pct,"%",NEWLINE,"=",new 
            RepeatItem(132));
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  SUMMARY PAGE (TOTALS: 1- BY USER ID; 2- BY WPID; 3- BY TRANSACTION)
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        pnd_Newpage.setValue(true);                                                                                                                                       //Natural: ASSIGN #NEWPAGE := TRUE
        FOR01:                                                                                                                                                            //Natural: FOR #I1-UNIT 1 #MAX-UNIT
        for (pnd_I1_Unit.setValue(1); condition(pnd_I1_Unit.lessOrEqual(pnd_Max_Unit)); pnd_I1_Unit.nadd(1))
        {
            if (condition(pnd_Table_1_Pnd_T1_Unit.getValue(pnd_I1_Unit).equals(" ")))                                                                                     //Natural: IF #T1-UNIT ( #I1-UNIT ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_End_Of_Loop.setValue(false);                                                                                                                              //Natural: ASSIGN #END-OF-LOOP := FALSE
            FOR02:                                                                                                                                                        //Natural: FOR #I1-WPID 1 #MAX-WPID
            for (pnd_I1_Wpid.setValue(1); condition(pnd_I1_Wpid.lessOrEqual(pnd_Max_Wpid)); pnd_I1_Wpid.nadd(1))
            {
                if (condition(pnd_Table_1_Pnd_T1_Wpid.getValue(pnd_I1_Unit,pnd_I1_Wpid).equals(" ")))                                                                     //Natural: IF #T1-WPID ( #I1-UNIT, #I1-WPID ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_T4_Wwww_Pnd_T4_Wpid.getValue("*")), new ExamineSearch(pnd_Table_1_Pnd_T1_Wpid.getValue(pnd_I1_Unit,pnd_I1_Wpid)),   //Natural: EXAMINE #T4-WPID ( * ) FOR #T1-WPID ( #I1-UNIT, #I1-WPID ) GIVING INDEX #I4-WPID
                    new ExamineGivingIndex(pnd_I4_Wpid));
                pnd_P_Wpid_Desc.setValue(pnd_T4_Wwww_Pnd_T4_Wpid_Desc.getValue(pnd_I4_Wpid));                                                                             //Natural: ASSIGN #P-WPID-DESC := #T4-WPID-DESC ( #I4-WPID )
                pnd_Wk_T_Days.compute(new ComputeParameters(false, pnd_Wk_T_Days), pnd_Table_1_Pnd_T1_Count.getValue(pnd_I1_Unit,pnd_I1_Wpid,"*").add(getZero()));        //Natural: COMPUTE #WK-T-DAYS = #T1-COUNT ( #I1-UNIT, #I1-WPID, * ) + 0
                if (condition(pnd_Wk_T_Days.lessOrEqual(getZero())))                                                                                                      //Natural: IF #WK-T-DAYS LE 0
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                FOR03:                                                                                                                                                    //Natural: FOR #I1-COUNT 2 7
                for (pnd_I1_Count.setValue(2); condition(pnd_I1_Count.lessOrEqual(7)); pnd_I1_Count.nadd(1))
                {
                    if (condition(pnd_Table_1_Pnd_T1_Count.getValue(pnd_I1_Unit,pnd_I1_Wpid,pnd_I1_Count).equals(getZero())))                                             //Natural: IF #T1-COUNT ( #I1-UNIT, #I1-WPID, #I1-COUNT ) = 0
                    {
                        pnd_Wk_T_Pct.getValue(pnd_I1_Count).setValue(0);                                                                                                  //Natural: ASSIGN #WK-T-PCT ( #I1-COUNT ) := 0
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Wk_T_Pct.getValue(pnd_I1_Count).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(pnd_I1_Count)), (pnd_Table_1_Pnd_T1_Count.getValue(pnd_I1_Unit, //Natural: COMPUTE #WK-T-PCT ( #I1-COUNT ) = ( #T1-COUNT ( #I1-UNIT, #I1-WPID, #I1-COUNT ) * 100 ) / #WK-T-DAYS
                            pnd_I1_Wpid,pnd_I1_Count).multiply(100)).divide(pnd_Wk_T_Days));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Wk_T_Pct.getValue(1).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(1)), DbsField.subtract(100,pnd_Wk_T_Pct.getValue(2,                   //Natural: COMPUTE #WK-T-PCT ( 1 ) = 100 - #WK-T-PCT ( 2:7 )
                    ":",7)));
                if (condition(pnd_Newpage.getBoolean() || getReports().getAstLineCount(1).greater(50)))                                                                   //Natural: IF #NEWPAGE OR *LINE-COUNT ( 1 ) > 50
                {
                    getReports().eject(1, true);                                                                                                                          //Natural: EJECT ( 1 )
                    getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(30),"PAYMENT OPERATIONS - WORK MANAGEMENT - CWF REPORT 1",NEWLINE,Global.getDATX(),       //Natural: WRITE ( 1 ) *PROGRAM 30X 'PAYMENT OPERATIONS - WORK MANAGEMENT - CWF REPORT 1' / *DATX ( EM = LLL' 'DD', 'YYYY ) 31X '***** SUMMARY BY UNIT *****' / 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZZ9 ) 35X #HDG-LINE2 // '           UNIT                              TOTAL     ' ' %T+0     %T+1     %T+2     %T+3     %T+4     %T+5     %T>5'
                        new ReportEditMask ("LLL' 'DD', 'YYYY"),new ColumnSpacing(31),"***** SUMMARY BY UNIT *****",NEWLINE,"Page:",getReports().getPageNumberDbs(1), 
                        new ReportEditMask ("ZZZ9"),new ColumnSpacing(35),pnd_Hdg_Line2,NEWLINE,NEWLINE,"           UNIT                              TOTAL     ",
                        " %T+0     %T+1     %T+2     %T+3     %T+4     %T+5     %T>5");
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Newpage.setValue(false);                                                                                                                          //Natural: ASSIGN #NEWPAGE := FALSE
                }                                                                                                                                                         //Natural: END-IF
                pnd_End_Of_Loop.setValue(true);                                                                                                                           //Natural: ASSIGN #END-OF-LOOP := TRUE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_End_Of_Loop.getBoolean()))                                                                                                                  //Natural: IF #END-OF-LOOP
            {
                pnd_Wk_Gt_Days_U.compute(new ComputeParameters(false, pnd_Wk_Gt_Days_U), pnd_Table_1_Pnd_T1_Count.getValue(pnd_I1_Unit,"*","*").add(getZero()));          //Natural: COMPUTE #WK-GT-DAYS-U = #T1-COUNT ( #I1-UNIT, *, * ) + 0
                FOR04:                                                                                                                                                    //Natural: FOR #I1-COUNT 2 7
                for (pnd_I1_Count.setValue(2); condition(pnd_I1_Count.lessOrEqual(7)); pnd_I1_Count.nadd(1))
                {
                    pnd_Wk_T_Days_U.compute(new ComputeParameters(false, pnd_Wk_T_Days_U), pnd_Table_1_Pnd_T1_Count.getValue(pnd_I1_Unit,"*",pnd_I1_Count).add(getZero())); //Natural: COMPUTE #WK-T-DAYS-U = #T1-COUNT ( #I1-UNIT, *, #I1-COUNT ) + 0
                    if (condition(pnd_Wk_T_Days_U.greater(getZero()) && pnd_Wk_Gt_Days_U.greater(getZero())))                                                             //Natural: IF #WK-T-DAYS-U > 0 AND #WK-GT-DAYS-U > 0
                    {
                        pnd_Wk_T_Pct_U.getValue(pnd_I1_Count).compute(new ComputeParameters(false, pnd_Wk_T_Pct_U.getValue(pnd_I1_Count)), (pnd_Wk_T_Days_U.multiply(100)).divide(pnd_Wk_Gt_Days_U)); //Natural: ASSIGN #WK-T-PCT-U ( #I1-COUNT ) := ( #WK-T-DAYS-U * 100 ) / #WK-GT-DAYS-U
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Wk_T_Pct_U.getValue(pnd_I1_Count).setValue(0);                                                                                                //Natural: ASSIGN #WK-T-PCT-U ( #I1-COUNT ) := 0
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Wk_T_Pct_U.getValue(1).compute(new ComputeParameters(false, pnd_Wk_T_Pct_U.getValue(1)), DbsField.subtract(100,pnd_Wk_T_Pct_U.getValue(2,             //Natural: COMPUTE #WK-T-PCT-U ( 1 ) = 100 - #WK-T-PCT-U ( 2:7 )
                    ":",7)));
                getReports().write(1, NEWLINE,new ColumnSpacing(11),pnd_Table_1_Pnd_T1_Unit.getValue(pnd_I1_Unit),new ColumnSpacing(21),pnd_Wk_Gt_Days_U,new              //Natural: WRITE ( 1 ) / 11X #T1-UNIT ( #I1-UNIT ) 21X #WK-GT-DAYS-U 5X #WK-T-PCT-U ( 1 ) 3X #WK-T-PCT-U ( 2 ) 3X #WK-T-PCT-U ( 3 ) 3X #WK-T-PCT-U ( 4 ) 3X #WK-T-PCT-U ( 5 ) 3X #WK-T-PCT-U ( 6 ) 3X #WK-T-PCT-U ( 7 )
                    ColumnSpacing(5),pnd_Wk_T_Pct_U.getValue(1),new ColumnSpacing(3),pnd_Wk_T_Pct_U.getValue(2),new ColumnSpacing(3),pnd_Wk_T_Pct_U.getValue(3),new 
                    ColumnSpacing(3),pnd_Wk_T_Pct_U.getValue(4),new ColumnSpacing(3),pnd_Wk_T_Pct_U.getValue(5),new ColumnSpacing(3),pnd_Wk_T_Pct_U.getValue(6),new 
                    ColumnSpacing(3),pnd_Wk_T_Pct_U.getValue(7));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  GRAND TOTAL
        pnd_Wk_Gt_Days.compute(new ComputeParameters(false, pnd_Wk_Gt_Days), pnd_Table_1_Pnd_T1_Count.getValue("*","*","*").add(getZero()));                              //Natural: COMPUTE #WK-GT-DAYS = #T1-COUNT ( *, *, * ) + 0
        FOR05:                                                                                                                                                            //Natural: FOR #I1-COUNT 2 7
        for (pnd_I1_Count.setValue(2); condition(pnd_I1_Count.lessOrEqual(7)); pnd_I1_Count.nadd(1))
        {
            //*  TOTAL FOR T+
            pnd_Wk_T_Days.compute(new ComputeParameters(false, pnd_Wk_T_Days), pnd_Table_1_Pnd_T1_Count.getValue("*","*",pnd_I1_Count).add(getZero()));                   //Natural: COMPUTE #WK-T-DAYS = #T1-COUNT ( *, *, #I1-COUNT ) + 0
            if (condition(pnd_Wk_T_Days.greater(getZero()) && pnd_Wk_Gt_Days.greater(getZero())))                                                                         //Natural: IF #WK-T-DAYS > 0 AND #WK-GT-DAYS> 0
            {
                pnd_Wk_T_Pct.getValue(pnd_I1_Count).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(pnd_I1_Count)), (pnd_Wk_T_Days.multiply(100)).divide(pnd_Wk_Gt_Days)); //Natural: COMPUTE #WK-T-PCT ( #I1-COUNT ) = ( #WK-T-DAYS * 100 ) / #WK-GT-DAYS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Wk_T_Pct.getValue(pnd_I1_Count).setValue(0);                                                                                                          //Natural: ASSIGN #WK-T-PCT ( #I1-COUNT ) := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Wk_T_Pct.getValue(1).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(1)), DbsField.subtract(100,pnd_Wk_T_Pct.getValue(2,":",7)));                  //Natural: COMPUTE #WK-T-PCT ( 1 ) = 100 - #WK-T-PCT ( 2:7 )
        getReports().write(1, NEWLINE,NEWLINE,"***** GRAND TOTAL BY UNIT *****        ",pnd_Wk_Gt_Days,new ColumnSpacing(5),pnd_Wk_T_Pct.getValue(1),new                  //Natural: WRITE ( 1 ) // '***** GRAND TOTAL BY UNIT *****        ' #WK-GT-DAYS 5X #WK-T-PCT ( 1 ) 3X #WK-T-PCT ( 2 ) 3X #WK-T-PCT ( 3 ) 3X #WK-T-PCT ( 4 ) 3X #WK-T-PCT ( 5 ) 3X #WK-T-PCT ( 6 ) 3X #WK-T-PCT ( 7 ) / '=' ( 132 )
            ColumnSpacing(3),pnd_Wk_T_Pct.getValue(2),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(3),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(4),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(5),new 
            ColumnSpacing(3),pnd_Wk_T_Pct.getValue(6),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(7),NEWLINE,"=",new RepeatItem(132));
        if (Global.isEscape()) return;
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        pnd_Newpage.setValue(true);                                                                                                                                       //Natural: ASSIGN #NEWPAGE := TRUE
        pnd_Wk_T_Pct.getValue("*").reset();                                                                                                                               //Natural: RESET #WK-T-PCT ( * )
        FOR06:                                                                                                                                                            //Natural: FOR #I2-WPID 1 #MAX-WPID
        for (pnd_I2_Wpid.setValue(1); condition(pnd_I2_Wpid.lessOrEqual(pnd_Max_Wpid)); pnd_I2_Wpid.nadd(1))
        {
            if (condition(pnd_Table_2_Pnd_T2_Wpid.getValue(pnd_I2_Wpid).equals(" ")))                                                                                     //Natural: IF #T2-WPID ( #I2-WPID ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_T4_Wwww_Pnd_T4_Wpid.getValue("*")), new ExamineSearch(pnd_Table_2_Pnd_T2_Wpid.getValue(pnd_I2_Wpid)),                   //Natural: EXAMINE #T4-WPID ( * ) FOR #T2-WPID ( #I2-WPID ) GIVING INDEX #I4-WPID
                new ExamineGivingIndex(pnd_I4_Wpid));
            pnd_P_Wpid_Desc.setValue(pnd_T4_Wwww_Pnd_T4_Wpid_Desc.getValue(pnd_I4_Wpid));                                                                                 //Natural: ASSIGN #P-WPID-DESC := #T4-WPID-DESC ( #I4-WPID )
            pnd_Wk_T_Days.compute(new ComputeParameters(false, pnd_Wk_T_Days), pnd_Table_2_Pnd_T2_Count.getValue(pnd_I2_Wpid,"*").add(getZero()));                        //Natural: COMPUTE #WK-T-DAYS = #T2-COUNT ( #I2-WPID, * ) + 0
            FOR07:                                                                                                                                                        //Natural: FOR #I2-COUNT 2 7
            for (pnd_I2_Count.setValue(2); condition(pnd_I2_Count.lessOrEqual(7)); pnd_I2_Count.nadd(1))
            {
                if (condition(pnd_Table_2_Pnd_T2_Count.getValue(pnd_I2_Wpid,pnd_I2_Count).greater(getZero()) && pnd_Wk_T_Days.greater(getZero())))                        //Natural: IF #T2-COUNT ( #I2-WPID, #I2-COUNT ) > 0 AND #WK-T-DAYS > 0
                {
                    pnd_Wk_T_Pct.getValue(pnd_I2_Count).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(pnd_I2_Count)), (pnd_Table_2_Pnd_T2_Count.getValue(pnd_I2_Wpid, //Natural: COMPUTE #WK-T-PCT ( #I2-COUNT ) = ( #T2-COUNT ( #I2-WPID, #I2-COUNT ) * 100 ) / #WK-T-DAYS
                        pnd_I2_Count).multiply(100)).divide(pnd_Wk_T_Days));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Wk_T_Pct.getValue(pnd_I2_Count).setValue(0);                                                                                                      //Natural: ASSIGN #WK-T-PCT ( #I2-COUNT ) := 0
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Wk_T_Pct.getValue(1).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(1)), DbsField.subtract(100,pnd_Wk_T_Pct.getValue(2,":",                   //Natural: COMPUTE #WK-T-PCT ( 1 ) = 100 - #WK-T-PCT ( 2:7 )
                7)));
            if (condition(pnd_Newpage.getBoolean() || getReports().getAstLineCount(1).greater(50)))                                                                       //Natural: IF #NEWPAGE OR *LINE-COUNT ( 1 ) > 50
            {
                getReports().eject(1, true);                                                                                                                              //Natural: EJECT ( 1 )
                getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(30),"PAYMENT OPERATIONS - WORK MANAGEMENT - CWF REPORT 1",NEWLINE,Global.getDATX(),           //Natural: WRITE ( 1 ) *PROGRAM 30X 'PAYMENT OPERATIONS - WORK MANAGEMENT - CWF REPORT 1' / *DATX ( EM = LLL' 'DD', 'YYYY ) 31X '***** SUMMARY BY WPID *****' / 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZZ9 ) 35X #HDG-LINE2 // '           WPID         WPID DESCRIPTION     TOTAL    ' '  %T+0     %T+1     %T+2     %T+3     %T+4     %T+5     %T>5'
                    new ReportEditMask ("LLL' 'DD', 'YYYY"),new ColumnSpacing(31),"***** SUMMARY BY WPID *****",NEWLINE,"Page:",getReports().getPageNumberDbs(1), 
                    new ReportEditMask ("ZZZ9"),new ColumnSpacing(35),pnd_Hdg_Line2,NEWLINE,NEWLINE,"           WPID         WPID DESCRIPTION     TOTAL    ",
                    "  %T+0     %T+1     %T+2     %T+3     %T+4     %T+5     %T>5");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Newpage.setValue(false);                                                                                                                              //Natural: ASSIGN #NEWPAGE := FALSE
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, new ColumnSpacing(11),pnd_Table_2_Pnd_T2_Wpid.getValue(pnd_I2_Wpid),new ColumnSpacing(7),pnd_P_Wpid_Desc,new ColumnSpacing(5),pnd_Wk_T_Days,new  //Natural: WRITE ( 1 ) 11X #T2-WPID ( #I2-WPID ) 7X #P-WPID-DESC 5X #WK-T-DAYS 5X #WK-T-PCT ( 1 ) 3X #WK-T-PCT ( 2 ) 3X #WK-T-PCT ( 3 ) 3X #WK-T-PCT ( 4 ) 3X #WK-T-PCT ( 5 ) 3X #WK-T-PCT ( 6 ) 3X #WK-T-PCT ( 7 )
                ColumnSpacing(5),pnd_Wk_T_Pct.getValue(1),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(2),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(3),new 
                ColumnSpacing(3),pnd_Wk_T_Pct.getValue(4),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(5),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(6),new 
                ColumnSpacing(3),pnd_Wk_T_Pct.getValue(7));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  GRAND TOTAL
        pnd_Wk_Gt_Days.compute(new ComputeParameters(false, pnd_Wk_Gt_Days), pnd_Table_2_Pnd_T2_Count.getValue("*","*").add(getZero()));                                  //Natural: COMPUTE #WK-GT-DAYS = #T2-COUNT ( *, * ) + 0
        FOR08:                                                                                                                                                            //Natural: FOR #I2-COUNT 2 7
        for (pnd_I2_Count.setValue(2); condition(pnd_I2_Count.lessOrEqual(7)); pnd_I2_Count.nadd(1))
        {
            //*  TOTAL FOR T+
            pnd_Wk_T_Days.compute(new ComputeParameters(false, pnd_Wk_T_Days), pnd_Table_2_Pnd_T2_Count.getValue("*",pnd_I2_Count).add(getZero()));                       //Natural: COMPUTE #WK-T-DAYS = #T2-COUNT ( *, #I2-COUNT ) + 0
            if (condition(pnd_Wk_T_Days.greater(getZero()) && pnd_Wk_Gt_Days.greater(getZero())))                                                                         //Natural: IF #WK-T-DAYS > 0 AND #WK-GT-DAYS > 0
            {
                pnd_Wk_T_Pct.getValue(pnd_I2_Count).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(pnd_I2_Count)), (pnd_Wk_T_Days.multiply(100)).divide(pnd_Wk_Gt_Days)); //Natural: COMPUTE #WK-T-PCT ( #I2-COUNT ) = ( #WK-T-DAYS * 100 ) / #WK-GT-DAYS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Wk_T_Pct.getValue(pnd_I2_Count).setValue(0);                                                                                                          //Natural: ASSIGN #WK-T-PCT ( #I2-COUNT ) := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Wk_T_Pct.getValue(1).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(1)), DbsField.subtract(100,pnd_Wk_T_Pct.getValue(2,":",7)));                  //Natural: COMPUTE #WK-T-PCT ( 1 ) = 100 - #WK-T-PCT ( 2:7 )
        getReports().write(1, NEWLINE,NEWLINE,"***** GRAND TOTAL BY WPID *****   ",new ColumnSpacing(6),pnd_Wk_Gt_Days,new ColumnSpacing(5),pnd_Wk_T_Pct.getValue(1),new  //Natural: WRITE ( 1 ) // '***** GRAND TOTAL BY WPID *****   ' 6X #WK-GT-DAYS 5X #WK-T-PCT ( 1 ) 3X #WK-T-PCT ( 2 ) 3X #WK-T-PCT ( 3 ) 3X #WK-T-PCT ( 4 ) 3X #WK-T-PCT ( 5 ) 3X #WK-T-PCT ( 6 ) 3X #WK-T-PCT ( 7 ) / '=' ( 132 )
            ColumnSpacing(3),pnd_Wk_T_Pct.getValue(2),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(3),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(4),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(5),new 
            ColumnSpacing(3),pnd_Wk_T_Pct.getValue(6),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(7),NEWLINE,"=",new RepeatItem(132));
        if (Global.isEscape()) return;
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(30),"PAYMENT OPERATIONS - WORK MANAGEMENT - CWF REPORT 1",NEWLINE,Global.getDATX(),                   //Natural: WRITE ( 1 ) *PROGRAM 30X 'PAYMENT OPERATIONS - WORK MANAGEMENT - CWF REPORT 1' / *DATX ( EM = LLL' 'DD', 'YYYY ) 31X '***** SUMMARY BY TRANSACTIONS *****' / 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZZ9 ) 35X #HDG-LINE2 // 'Financial Transactions' / '           WPID         WPID DESCRIPTION     TOTAL    ' '  %T+0     %T+1     %T+2     %T+3     %T+4     %T+5     %T>5'
            new ReportEditMask ("LLL' 'DD', 'YYYY"),new ColumnSpacing(31),"***** SUMMARY BY TRANSACTIONS *****",NEWLINE,"Page:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZZ9"),new ColumnSpacing(35),pnd_Hdg_Line2,NEWLINE,NEWLINE,"Financial Transactions",NEWLINE,"           WPID         WPID DESCRIPTION     TOTAL    ",
            "  %T+0     %T+1     %T+2     %T+3     %T+4     %T+5     %T>5");
        if (Global.isEscape()) return;
        //*  FINANCIAL
        FOR09:                                                                                                                                                            //Natural: FOR #I3-WPID 1 23
        for (pnd_I3_Wpid.setValue(1); condition(pnd_I3_Wpid.lessOrEqual(23)); pnd_I3_Wpid.nadd(1))
        {
            DbsUtil.examine(new ExamineSource(pnd_T4_Wwww_Pnd_T4_Wpid.getValue("*")), new ExamineSearch(pnd_Table_3_Pnd_T3_Wpid.getValue(pnd_I3_Wpid)),                   //Natural: EXAMINE #T4-WPID ( * ) FOR #T3-WPID ( #I3-WPID ) GIVING INDEX #I4-WPID
                new ExamineGivingIndex(pnd_I4_Wpid));
            //*  NO REQUESTS FOR WPID
            if (condition(pnd_I4_Wpid.equals(getZero())))                                                                                                                 //Natural: IF #I4-WPID = 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_P_Wpid_Desc.setValue(pnd_T4_Wwww_Pnd_T4_Wpid_Desc.getValue(pnd_I4_Wpid));                                                                                 //Natural: ASSIGN #P-WPID-DESC := #T4-WPID-DESC ( #I4-WPID )
            pnd_Wk_T_Days.compute(new ComputeParameters(false, pnd_Wk_T_Days), pnd_Table_3_Pnd_T3_Count.getValue(pnd_I3_Wpid,"*").add(getZero()));                        //Natural: COMPUTE #WK-T-DAYS = #T3-COUNT ( #I3-WPID, * ) + 0
            FOR10:                                                                                                                                                        //Natural: FOR #I3-COUNT 2 7
            for (pnd_I3_Count.setValue(2); condition(pnd_I3_Count.lessOrEqual(7)); pnd_I3_Count.nadd(1))
            {
                if (condition(pnd_Table_3_Pnd_T3_Count.getValue(pnd_I3_Wpid,pnd_I3_Count).greater(getZero()) && pnd_Wk_T_Days.greater(getZero())))                        //Natural: IF #T3-COUNT ( #I3-WPID, #I3-COUNT ) > 0 AND #WK-T-DAYS > 0
                {
                    pnd_Wk_T_Pct.getValue(pnd_I3_Count).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(pnd_I3_Count)), (pnd_Table_3_Pnd_T3_Count.getValue(pnd_I3_Wpid, //Natural: COMPUTE #WK-T-PCT ( #I3-COUNT ) = ( #T3-COUNT ( #I3-WPID, #I3-COUNT ) * 100 ) / #WK-T-DAYS
                        pnd_I3_Count).multiply(100)).divide(pnd_Wk_T_Days));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Wk_T_Pct.getValue(pnd_I3_Count).setValue(0);                                                                                                      //Natural: ASSIGN #WK-T-PCT ( #I3-COUNT ) := 0
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Wk_T_Pct.getValue(1).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(1)), DbsField.subtract(100,pnd_Wk_T_Pct.getValue(2,":",                   //Natural: COMPUTE #WK-T-PCT ( 1 ) = 100 - #WK-T-PCT ( 2:7 )
                7)));
            getReports().write(1, new ColumnSpacing(11),pnd_Table_3_Pnd_T3_Wpid.getValue(pnd_I3_Wpid),new ColumnSpacing(7),pnd_P_Wpid_Desc,new ColumnSpacing(5),pnd_Wk_T_Days,new  //Natural: WRITE ( 1 ) 11X #T3-WPID ( #I3-WPID ) 7X #P-WPID-DESC 5X #WK-T-DAYS 5X #WK-T-PCT ( 1 ) 3X #WK-T-PCT ( 2 ) 3X #WK-T-PCT ( 3 ) 3X #WK-T-PCT ( 4 ) 3X #WK-T-PCT ( 5 ) 3X #WK-T-PCT ( 6 ) 3X #WK-T-PCT ( 7 )
                ColumnSpacing(5),pnd_Wk_T_Pct.getValue(1),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(2),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(3),new 
                ColumnSpacing(3),pnd_Wk_T_Pct.getValue(4),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(5),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(6),new 
                ColumnSpacing(3),pnd_Wk_T_Pct.getValue(7));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Wk_Gt_Days.compute(new ComputeParameters(false, pnd_Wk_Gt_Days), pnd_Table_3_Pnd_T3_Count.getValue(1,":",23,"*").add(getZero()));                             //Natural: COMPUTE #WK-GT-DAYS = #T3-COUNT ( 1:23, * ) + 0
        FOR11:                                                                                                                                                            //Natural: FOR #I3-COUNT 2 7
        for (pnd_I3_Count.setValue(2); condition(pnd_I3_Count.lessOrEqual(7)); pnd_I3_Count.nadd(1))
        {
            pnd_Wk_T_Days.compute(new ComputeParameters(false, pnd_Wk_T_Days), pnd_Table_3_Pnd_T3_Count.getValue(1,":",23,pnd_I3_Count).add(getZero()));                  //Natural: COMPUTE #WK-T-DAYS = #T3-COUNT ( 1:23, #I3-COUNT ) + 0
            if (condition(pnd_Wk_T_Days.greater(getZero()) && pnd_Wk_Gt_Days.greater(getZero())))                                                                         //Natural: IF #WK-T-DAYS > 0 AND #WK-GT-DAYS > 0
            {
                pnd_Wk_T_Pct.getValue(pnd_I3_Count).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(pnd_I3_Count)), (pnd_Wk_T_Days.multiply(100)).divide(pnd_Wk_Gt_Days)); //Natural: COMPUTE #WK-T-PCT ( #I3-COUNT ) = ( #WK-T-DAYS * 100 ) / #WK-GT-DAYS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Wk_T_Pct.getValue(pnd_I3_Count).setValue(0);                                                                                                          //Natural: ASSIGN #WK-T-PCT ( #I3-COUNT ) := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Wk_T_Pct.getValue(1).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(1)), DbsField.subtract(100,pnd_Wk_T_Pct.getValue(2,":",7)));                  //Natural: COMPUTE #WK-T-PCT ( 1 ) = 100 - #WK-T-PCT ( 2:7 )
        getReports().write(1, NEWLINE,NEWLINE,"*** TOTAL FINANCIAL *** ",new ColumnSpacing(16),pnd_Wk_Gt_Days,new ColumnSpacing(5),pnd_Wk_T_Pct.getValue(1),new           //Natural: WRITE ( 1 ) // '*** TOTAL FINANCIAL *** ' 16X #WK-GT-DAYS 5X #WK-T-PCT ( 1 ) 3X #WK-T-PCT ( 2 ) 3X #WK-T-PCT ( 3 ) 3X #WK-T-PCT ( 4 ) 3X #WK-T-PCT ( 5 ) 3X #WK-T-PCT ( 6 ) 3X #WK-T-PCT ( 7 )
            ColumnSpacing(3),pnd_Wk_T_Pct.getValue(2),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(3),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(4),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(5),new 
            ColumnSpacing(3),pnd_Wk_T_Pct.getValue(6),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(7));
        if (Global.isEscape()) return;
        //*  NON-FINANCIAL
        pnd_Wk_T_Days.compute(new ComputeParameters(false, pnd_Wk_T_Days), pnd_Table_3_Pnd_T3_Count.getValue(24,"*").add(getZero()));                                     //Natural: COMPUTE #WK-T-DAYS = #T3-COUNT ( 24, * ) + 0
        FOR12:                                                                                                                                                            //Natural: FOR #I3-COUNT 2 7
        for (pnd_I3_Count.setValue(2); condition(pnd_I3_Count.lessOrEqual(7)); pnd_I3_Count.nadd(1))
        {
            if (condition(pnd_Table_3_Pnd_T3_Count.getValue(6,pnd_I3_Count).greater(getZero()) && pnd_Wk_T_Days.greater(getZero())))                                      //Natural: IF #T3-COUNT ( 6, #I3-COUNT ) > 0 AND #WK-T-DAYS > 0
            {
                pnd_Wk_T_Pct.getValue(pnd_I3_Count).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(pnd_I3_Count)), (pnd_Table_3_Pnd_T3_Count.getValue(24,     //Natural: COMPUTE #WK-T-PCT ( #I3-COUNT ) = ( #T3-COUNT ( 24, #I3-COUNT ) * 100 ) / #WK-T-DAYS
                    pnd_I3_Count).multiply(100)).divide(pnd_Wk_T_Days));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Wk_T_Pct.getValue(pnd_I3_Count).setValue(0);                                                                                                          //Natural: ASSIGN #WK-T-PCT ( #I3-COUNT ) := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Wk_T_Pct.getValue(1).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(1)), DbsField.subtract(100,pnd_Wk_T_Pct.getValue(2,":",7)));                  //Natural: COMPUTE #WK-T-PCT ( 1 ) = 100 - #WK-T-PCT ( 2:7 )
        getReports().write(1, NEWLINE,NEWLINE,"Non-Financial Transactions",NEWLINE,NEWLINE,"WPIDS OTHER THAN THE ABOVE  ",new ColumnSpacing(16),pnd_Wk_T_Days,new         //Natural: WRITE ( 1 ) // 'Non-Financial Transactions' // 'WPIDS OTHER THAN THE ABOVE  ' 16X #WK-T-DAYS 5X #WK-T-PCT ( 1 ) 3X #WK-T-PCT ( 2 ) 3X #WK-T-PCT ( 3 ) 3X #WK-T-PCT ( 4 ) 3X #WK-T-PCT ( 5 ) 3X #WK-T-PCT ( 6 ) 3X #WK-T-PCT ( 7 )
            ColumnSpacing(5),pnd_Wk_T_Pct.getValue(1),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(2),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(3),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(4),new 
            ColumnSpacing(3),pnd_Wk_T_Pct.getValue(5),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(6),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(7));
        if (Global.isEscape()) return;
        //*  GRAND TOTAL
        pnd_Wk_Gt_Days.compute(new ComputeParameters(false, pnd_Wk_Gt_Days), pnd_Table_3_Pnd_T3_Count.getValue("*","*").add(getZero()));                                  //Natural: COMPUTE #WK-GT-DAYS = #T3-COUNT ( *, * ) + 0
        FOR13:                                                                                                                                                            //Natural: FOR #I3-COUNT 2 7
        for (pnd_I3_Count.setValue(2); condition(pnd_I3_Count.lessOrEqual(7)); pnd_I3_Count.nadd(1))
        {
            //*  TOTAL FOR T+
            pnd_Wk_T_Days.compute(new ComputeParameters(false, pnd_Wk_T_Days), pnd_Table_3_Pnd_T3_Count.getValue("*",pnd_I3_Count).add(getZero()));                       //Natural: COMPUTE #WK-T-DAYS = #T3-COUNT ( *, #I3-COUNT ) + 0
            if (condition(pnd_Wk_T_Days.greater(getZero()) && pnd_Wk_Gt_Days.greater(getZero())))                                                                         //Natural: IF #WK-T-DAYS > 0 AND #WK-GT-DAYS > 0
            {
                pnd_Wk_T_Pct.getValue(pnd_I3_Count).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(pnd_I3_Count)), (pnd_Wk_T_Days.multiply(100)).divide(pnd_Wk_Gt_Days)); //Natural: COMPUTE #WK-T-PCT ( #I3-COUNT ) = ( #WK-T-DAYS * 100 ) / #WK-GT-DAYS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Wk_T_Pct.getValue(pnd_I3_Count).setValue(0);                                                                                                          //Natural: ASSIGN #WK-T-PCT ( #I3-COUNT ) := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Wk_T_Pct.getValue(1).compute(new ComputeParameters(false, pnd_Wk_T_Pct.getValue(1)), DbsField.subtract(100,pnd_Wk_T_Pct.getValue(2,":",7)));                  //Natural: COMPUTE #WK-T-PCT ( 1 ) = 100 - #WK-T-PCT ( 2:7 )
        getReports().write(1, NEWLINE,NEWLINE,"***** TOTAL ORDER ***** ",new ColumnSpacing(16),pnd_Wk_Gt_Days,new ColumnSpacing(5),pnd_Wk_T_Pct.getValue(1),new           //Natural: WRITE ( 1 ) // '***** TOTAL ORDER ***** ' 16X #WK-GT-DAYS 5X #WK-T-PCT ( 1 ) 3X #WK-T-PCT ( 2 ) 3X #WK-T-PCT ( 3 ) 3X #WK-T-PCT ( 4 ) 3X #WK-T-PCT ( 5 ) 3X #WK-T-PCT ( 6 ) 3X #WK-T-PCT ( 7 ) / '=' ( 132 )
            ColumnSpacing(3),pnd_Wk_T_Pct.getValue(2),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(3),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(4),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(5),new 
            ColumnSpacing(3),pnd_Wk_T_Pct.getValue(6),new ColumnSpacing(3),pnd_Wk_T_Pct.getValue(7),NEWLINE,"=",new RepeatItem(132));
        if (Global.isEscape()) return;
        //* ***********************************************************************
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,pnd_Write_Count, new ReportEditMask ("ZZZZ,ZZ9"),"Total Count");                                                    //Natural: WRITE /// #WRITE-COUNT ( EM = ZZZZ,ZZ9 ) 'Total Count'
        if (Global.isEscape()) return;
    }
    private void sub_Get_Wpid_Details() throws Exception                                                                                                                  //Natural: GET-WPID-DETAILS
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Wpid.startDatabaseRead                                                                                                                                     //Natural: READ ( 1 ) CWF-WPID BY WPID-UNIQ-KEY FROM #WF-WPID
        (
        "READ02",
        new Wc[] { new Wc("WPID_UNIQ_KEY", ">=", pnd_Work_File_Pnd_Wf_Wpid, WcType.BY) },
        new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_cwf_Wpid.readNextRow("READ02")))
        {
            if (condition(cwf_Wpid_Work_Prcss_Id.equals(pnd_Work_File_Pnd_Wf_Wpid)))                                                                                      //Natural: IF CWF-WPID.WORK-PRCSS-ID = #WF-WPID
            {
                pnd_P_Wpid_Desc.setValue(cwf_Wpid_Work_Prcss_Short_Nme);                                                                                                  //Natural: ASSIGN #P-WPID-DESC := CWF-WPID.WORK-PRCSS-SHORT-NME
                pnd_P_Srvce_Std.setValue(cwf_Wpid_Crprte_Srvce_Time_Stndrd_Grp);                                                                                          //Natural: ASSIGN #P-SRVCE-STD := CWF-WPID.CRPRTE-SRVCE-TIME-STNDRD-GRP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_P_Wpid_Desc.setValue("No WPID description found");                                                                                                    //Natural: ASSIGN #P-WPID-DESC := 'No WPID description found'
                pnd_P_Srvce_Std.setValue(0);                                                                                                                              //Natural: ASSIGN #P-SRVCE-STD := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Work_File_Pnd_Wf_Wpid.equals(pnd_T4_Wwww_Pnd_T4_Wpid.getValue("*"))))                                                                           //Natural: IF #WF-WPID = #T4-WPID ( * )
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR14:                                                                                                                                                            //Natural: FOR #I4-WPID 1 #MAX-WPID
        for (pnd_I4_Wpid.setValue(1); condition(pnd_I4_Wpid.lessOrEqual(pnd_Max_Wpid)); pnd_I4_Wpid.nadd(1))
        {
            if (condition(pnd_T4_Wwww_Pnd_T4_Wpid.getValue(pnd_I4_Wpid).notEquals(" ")))                                                                                  //Natural: IF #T4-WPID ( #I4-WPID ) NE ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_I4_Wpid.greater(pnd_Max_Wpid)))                                                                                                             //Natural: IF #I4-WPID > #MAX-WPID
            {
                getReports().write(0, pnd_Work_File_Pnd_Wf_Wpid,pnd_P_Wpid_Desc,"not included in summary ");                                                              //Natural: WRITE #WF-WPID #P-WPID-DESC 'not included in summary '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_T4_Wwww_Pnd_T4_Wpid.getValue(pnd_I4_Wpid).setValue(pnd_Work_File_Pnd_Wf_Wpid);                                                                            //Natural: ASSIGN #T4-WPID ( #I4-WPID ) := #WF-WPID
            pnd_T4_Wwww_Pnd_T4_Wpid_Desc.getValue(pnd_I4_Wpid).setValue(pnd_P_Wpid_Desc);                                                                                 //Natural: ASSIGN #T4-WPID-DESC ( #I4-WPID ) := #P-WPID-DESC
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  GET-WPID-DETAILS
    }
    private void sub_Calculate_Prcss_Days() throws Exception                                                                                                              //Natural: CALCULATE-PRCSS-DAYS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_P_Prcss_Days.setValue(0);                                                                                                                                     //Natural: ASSIGN #P-PRCSS-DAYS := 0
        //*  RS0208 CALULATE USING THE TIAA-RCVD-DATE INSTEAD OF THE UNIT-RCVD-DTE
        //*  UNIT BASED
        if (condition(pnd_Work_File_Pnd_Wf_Unit_Open_Dte_A.equals(pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_A)))                                                                 //Natural: IF #WF-UNIT-OPEN-DTE-A = #WF-UNIT-CLSD-DTE-A
        {
            //* * IF #WF-RECVD-DTE-A = #WF-UNIT-CLSD-DTE-A
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wk_Recvd_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_File_Pnd_Wf_Unit_Open_Dte_A);                                                           //Natural: MOVE EDITED #WF-UNIT-OPEN-DTE-A TO #WK-RECVD-DTE-D ( EM = YYYYMMDD )
        //* * MOVE EDITED #WF-RECVD-DTE-A     TO #WK-RECVD-DTE-D   (EM=YYYYMMDD)
        pnd_Wk_Compltd_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_A);                                                         //Natural: MOVE EDITED #WF-UNIT-CLSD-DTE-A TO #WK-COMPLTD-DTE-D ( EM = YYYYMMDD )
        pnd_Wk_Recvd_Dte_D.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WK-RECVD-DTE-D
        //* *
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Wk_Recvd_Dte_D.greater(pnd_Wk_Compltd_Dte_D))) {break;}                                                                                     //Natural: UNTIL #WK-RECVD-DTE-D GT #WK-COMPLTD-DTE-D
            pnd_Wk_Recvd_Dte_A.setValueEdited(pnd_Wk_Recvd_Dte_D,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED #WK-RECVD-DTE-D ( EM = YYYYMMDD ) TO #WK-RECVD-DTE-A
            pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                          //Natural: ASSIGN NECA4000.REQUEST-IND := ' '
            pdaNeca4000.getNeca4000_Function_Cde().setValue("CAL");                                                                                                       //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'CAL'
            pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("  ");                                                                                                 //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '  '
            pdaNeca4000.getNeca4000_Cal_Key_For_Dte().setValue(pnd_Wk_Recvd_Dte_A_Pnd_Wk_Recvd_Dte_N);                                                                    //Natural: ASSIGN NECA4000.CAL-KEY-FOR-DTE := #WK-RECVD-DTE-N
            DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                        //Natural: CALLNAT 'NECN4000' NECA4000
            if (condition(Global.isEscape())) return;
            if (condition(pdaNeca4000.getNeca4000_Return_Cde().notEquals("00")))                                                                                          //Natural: IF NECA4000.RETURN-CDE NE '00'
            {
                getReports().write(0, " BAD RETURN FROM CALENDAR CALL",pdaNeca4000.getNeca4000_Return_Cde());                                                             //Natural: WRITE ' BAD RETURN FROM CALENDAR CALL'NECA4000.RETURN-CDE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(8);  if (true) return;                                                                                                                  //Natural: TERMINATE 8
            }                                                                                                                                                             //Natural: END-IF
            pnd_Wk_Recvd_Dte_D.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WK-RECVD-DTE-D
            if (condition(pdaNeca4000.getNeca4000_Cal_Business_Day_Ind().getValue(1).equals("Y")))                                                                        //Natural: IF NECA4000.CAL-BUSINESS-DAY-IND ( 1 ) = 'Y'
            {
                pnd_P_Prcss_Days.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #P-PRCSS-DAYS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* **
        //*  REQUEST RECEIVED AFTER 4PM IS COUNTED AS NEXT DAY'S WORK
        if (condition(pnd_Work_File_Pnd_Wf_Recvd_Tme.greater(1600)))                                                                                                      //Natural: IF #WF-RECVD-TME > 1600
        {
            pnd_P_Prcss_Days.nsubtract(1);                                                                                                                                //Natural: SUBTRACT 1 FROM #P-PRCSS-DAYS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_P_Prcss_Days.less(getZero())))                                                                                                                  //Natural: IF #P-PRCSS-DAYS LT 0
        {
            pnd_P_Prcss_Days.setValue(0);                                                                                                                                 //Natural: ASSIGN #P-PRCSS-DAYS := 0
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALCULATE-PRCSS-DAYS
    }
    private void sub_Change_Of_Wpid() throws Exception                                                                                                                    //Natural: CHANGE-OF-WPID
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Wpid_Days.greater(getZero()) && pnd_Wpid_Count.greater(getZero())))                                                                             //Natural: IF #WPID-DAYS > 0 AND #WPID-COUNT > 0
        {
            pnd_Wpid_Avg_Days.compute(new ComputeParameters(true, pnd_Wpid_Avg_Days), pnd_Wpid_Days.divide(pnd_Wpid_Count));                                              //Natural: COMPUTE ROUNDED #WPID-AVG-DAYS = #WPID-DAYS / #WPID-COUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Wpid_Avg_Days.setValue(0);                                                                                                                                //Natural: ASSIGN #WPID-AVG-DAYS := 0
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Wpid_In_Std.greater(getZero()) && pnd_Wpid_Count.greater(getZero())))                                                                           //Natural: IF #WPID-IN-STD > 0 AND #WPID-COUNT > 0
        {
            pnd_Wpid_In_Std_Pct.compute(new ComputeParameters(true, pnd_Wpid_In_Std_Pct), (pnd_Wpid_In_Std.multiply(100)).divide(pnd_Wpid_Count));                        //Natural: COMPUTE ROUNDED #WPID-IN-STD-PCT = ( #WPID-IN-STD * 100 ) / #WPID-COUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Wpid_In_Std_Pct.setValue(0);                                                                                                                              //Natural: ASSIGN #WPID-IN-STD-PCT := 0
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wpid_Out_Std_Pct.compute(new ComputeParameters(true, pnd_Wpid_Out_Std_Pct), DbsField.subtract(100,pnd_Wpid_In_Std_Pct));                                      //Natural: COMPUTE ROUNDED #WPID-OUT-STD-PCT = 100 - #WPID-IN-STD-PCT
        getReports().write(1, NEWLINE,new ColumnSpacing(18),pnd_P_Wpid,pnd_P_Wpid_Desc,new ColumnSpacing(2),pnd_Wpid_Count, new ReportEditMask ("Z,ZZZ,ZZ9"),new          //Natural: WRITE ( 1 ) / 18X #P-WPID #P-WPID-DESC 2X #WPID-COUNT ( EM = Z,ZZZ,ZZ9 ) 91T #WPID-DAYS ( EM = Z,ZZZ,ZZ9 ) 111T '    Avg. Days' #WPID-AVG-DAYS / 111T ' Total In-Std ' #WPID-IN-STD / 111T 'Total Out-Std ' #WPID-OUT-STD / 111T '     % In-Std ' #WPID-IN-STD-PCT '%' / 111T '    % Out-Std ' #WPID-OUT-STD-PCT '%' / 9X '_' ( 132 )
            TabSetting(91),pnd_Wpid_Days, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(111),"    Avg. Days",pnd_Wpid_Avg_Days,NEWLINE,new TabSetting(111)," Total In-Std ",pnd_Wpid_In_Std,NEWLINE,new 
            TabSetting(111),"Total Out-Std ",pnd_Wpid_Out_Std,NEWLINE,new TabSetting(111),"     % In-Std ",pnd_Wpid_In_Std_Pct,"%",NEWLINE,new TabSetting(111),"    % Out-Std ",pnd_Wpid_Out_Std_Pct,"%",NEWLINE,new 
            ColumnSpacing(9),"_",new RepeatItem(132));
        if (Global.isEscape()) return;
        pnd_Unit_Count.nadd(pnd_Wpid_Count);                                                                                                                              //Natural: ADD #WPID-COUNT TO #UNIT-COUNT
        pnd_Unit_Days.nadd(pnd_Wpid_Days);                                                                                                                                //Natural: ADD #WPID-DAYS TO #UNIT-DAYS
        pnd_Unit_In_Std.nadd(pnd_Wpid_In_Std);                                                                                                                            //Natural: ADD #WPID-IN-STD TO #UNIT-IN-STD
        pnd_Unit_Out_Std.nadd(pnd_Wpid_Out_Std);                                                                                                                          //Natural: ADD #WPID-OUT-STD TO #UNIT-OUT-STD
        pnd_Wpid_Count.reset();                                                                                                                                           //Natural: RESET #WPID-COUNT #WPID-DAYS #WPID-IN-STD #WPID-OUT-STD
        pnd_Wpid_Days.reset();
        pnd_Wpid_In_Std.reset();
        pnd_Wpid_Out_Std.reset();
        //*  CHANGE-OF-WPID
    }
    private void sub_Change_Of_Unit() throws Exception                                                                                                                    //Natural: CHANGE-OF-UNIT
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Unit_Days.greater(getZero()) && pnd_Unit_Count.greater(getZero())))                                                                             //Natural: IF #UNIT-DAYS > 0 AND #UNIT-COUNT > 0
        {
            pnd_Unit_Avg_Days.compute(new ComputeParameters(true, pnd_Unit_Avg_Days), pnd_Unit_Days.divide(pnd_Unit_Count));                                              //Natural: COMPUTE ROUNDED #UNIT-AVG-DAYS = #UNIT-DAYS / #UNIT-COUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Unit_Avg_Days.setValue(0);                                                                                                                                //Natural: ASSIGN #UNIT-AVG-DAYS := 0
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Unit_In_Std.greater(getZero()) && pnd_Unit_Count.greater(getZero())))                                                                           //Natural: IF #UNIT-IN-STD > 0 AND #UNIT-COUNT > 0
        {
            pnd_Unit_In_Std_Pct.compute(new ComputeParameters(true, pnd_Unit_In_Std_Pct), (pnd_Unit_In_Std.multiply(100)).divide(pnd_Unit_Count));                        //Natural: COMPUTE ROUNDED #UNIT-IN-STD-PCT = ( #UNIT-IN-STD * 100 ) / #UNIT-COUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Unit_In_Std_Pct.setValue(0);                                                                                                                              //Natural: ASSIGN #UNIT-IN-STD-PCT := 0
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Unit_Out_Std_Pct.compute(new ComputeParameters(true, pnd_Unit_Out_Std_Pct), DbsField.subtract(100,pnd_Unit_In_Std_Pct));                                      //Natural: COMPUTE ROUNDED #UNIT-OUT-STD-PCT = 100 - #UNIT-IN-STD-PCT
        getReports().write(1, NEWLINE,pnd_P_Unit,new ColumnSpacing(10),"TOTAL",new ColumnSpacing(19),pnd_Unit_Count, new ReportEditMask ("Z,ZZZ,ZZ9"),new                 //Natural: WRITE ( 1 ) / #P-UNIT 10X 'TOTAL' 19X #UNIT-COUNT ( EM = Z,ZZZ,ZZ9 ) 91T #UNIT-DAYS ( EM = Z,ZZZ,ZZ9 ) 111T '    AVG. DAYS' #UNIT-AVG-DAYS / 111T ' TOTAL IN-STD ' #UNIT-IN-STD / 111T 'TOTAL OUT-STD ' #UNIT-OUT-STD / 111T '     % IN-STD ' #UNIT-IN-STD-PCT '%' / 111T '    % OUT-STD ' #UNIT-OUT-STD-PCT '%' / '_' ( 132 )
            TabSetting(91),pnd_Unit_Days, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(111),"    AVG. DAYS",pnd_Unit_Avg_Days,NEWLINE,new TabSetting(111)," TOTAL IN-STD ",pnd_Unit_In_Std,NEWLINE,new 
            TabSetting(111),"TOTAL OUT-STD ",pnd_Unit_Out_Std,NEWLINE,new TabSetting(111),"     % IN-STD ",pnd_Unit_In_Std_Pct,"%",NEWLINE,new TabSetting(111),"    % OUT-STD ",pnd_Unit_Out_Std_Pct,"%",NEWLINE,"_",new 
            RepeatItem(132));
        if (Global.isEscape()) return;
        pnd_Total_Count.nadd(pnd_Unit_Count);                                                                                                                             //Natural: ADD #UNIT-COUNT TO #TOTAL-COUNT
        pnd_Total_Days.nadd(pnd_Unit_Days);                                                                                                                               //Natural: ADD #UNIT-DAYS TO #TOTAL-DAYS
        pnd_Total_In_Std.nadd(pnd_Unit_In_Std);                                                                                                                           //Natural: ADD #UNIT-IN-STD TO #TOTAL-IN-STD
        pnd_Total_Out_Std.nadd(pnd_Unit_Out_Std);                                                                                                                         //Natural: ADD #UNIT-OUT-STD TO #TOTAL-OUT-STD
        pnd_Unit_Count.reset();                                                                                                                                           //Natural: RESET #UNIT-COUNT #UNIT-DAYS #UNIT-IN-STD #UNIT-OUT-STD
        pnd_Unit_Days.reset();
        pnd_Unit_In_Std.reset();
        pnd_Unit_Out_Std.reset();
        //*  CHANGE-OF-UNIT
    }
    private void sub_Summary_Calculation() throws Exception                                                                                                               //Natural: SUMMARY-CALCULATION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Wk_T_Days.compute(new ComputeParameters(false, pnd_Wk_T_Days), pnd_P_Prcss_Days.subtract(pnd_P_Srvce_Std_Pnd_P_Srvce_Std_Days));                              //Natural: COMPUTE #WK-T-DAYS = #P-PRCSS-DAYS - #P-SRVCE-STD-DAYS
        if (condition(pnd_Wk_T_Days.less(getZero())))                                                                                                                     //Natural: IF #WK-T-DAYS < 0
        {
            pnd_Wk_T_Days.setValue(0);                                                                                                                                    //Natural: ASSIGN #WK-T-DAYS := 0
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1586 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #WK-T-DAYS;//Natural: VALUE 0
        if (condition((pnd_Wk_T_Days.equals(0))))
        {
            decideConditionsMet1586++;
            pnd_I1_Count.setValue(1);                                                                                                                                     //Natural: ASSIGN #I1-COUNT := 1
            pnd_I2_Count.setValue(1);                                                                                                                                     //Natural: ASSIGN #I2-COUNT := 1
            pnd_I3_Count.setValue(1);                                                                                                                                     //Natural: ASSIGN #I3-COUNT := 1
        }                                                                                                                                                                 //Natural: VALUE 1
        else if (condition((pnd_Wk_T_Days.equals(1))))
        {
            decideConditionsMet1586++;
            pnd_I1_Count.setValue(2);                                                                                                                                     //Natural: ASSIGN #I1-COUNT := 2
            pnd_I2_Count.setValue(2);                                                                                                                                     //Natural: ASSIGN #I2-COUNT := 2
            pnd_I3_Count.setValue(2);                                                                                                                                     //Natural: ASSIGN #I3-COUNT := 2
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Wk_T_Days.equals(2))))
        {
            decideConditionsMet1586++;
            pnd_I1_Count.setValue(3);                                                                                                                                     //Natural: ASSIGN #I1-COUNT := 3
            pnd_I2_Count.setValue(3);                                                                                                                                     //Natural: ASSIGN #I2-COUNT := 3
            pnd_I3_Count.setValue(3);                                                                                                                                     //Natural: ASSIGN #I3-COUNT := 3
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Wk_T_Days.equals(3))))
        {
            decideConditionsMet1586++;
            pnd_I1_Count.setValue(4);                                                                                                                                     //Natural: ASSIGN #I1-COUNT := 4
            pnd_I2_Count.setValue(4);                                                                                                                                     //Natural: ASSIGN #I2-COUNT := 4
            pnd_I3_Count.setValue(4);                                                                                                                                     //Natural: ASSIGN #I3-COUNT := 4
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Wk_T_Days.equals(4))))
        {
            decideConditionsMet1586++;
            pnd_I1_Count.setValue(5);                                                                                                                                     //Natural: ASSIGN #I1-COUNT := 5
            pnd_I2_Count.setValue(5);                                                                                                                                     //Natural: ASSIGN #I2-COUNT := 5
            pnd_I3_Count.setValue(5);                                                                                                                                     //Natural: ASSIGN #I3-COUNT := 5
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((pnd_Wk_T_Days.equals(5))))
        {
            decideConditionsMet1586++;
            pnd_I1_Count.setValue(6);                                                                                                                                     //Natural: ASSIGN #I1-COUNT := 6
            pnd_I2_Count.setValue(6);                                                                                                                                     //Natural: ASSIGN #I2-COUNT := 6
            pnd_I3_Count.setValue(6);                                                                                                                                     //Natural: ASSIGN #I3-COUNT := 6
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_I1_Count.setValue(7);                                                                                                                                     //Natural: ASSIGN #I1-COUNT := 7
            pnd_I2_Count.setValue(7);                                                                                                                                     //Natural: ASSIGN #I2-COUNT := 7
            pnd_I3_Count.setValue(7);                                                                                                                                     //Natural: ASSIGN #I3-COUNT := 7
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  SUMMARY BY OPERATOR
        pnd_Table_1_Pnd_T1_Count.getValue(pnd_I1_Unit,pnd_I1_Wpid,pnd_I1_Count).nadd(1);                                                                                  //Natural: ADD 1 TO #T1-COUNT ( #I1-UNIT, #I1-WPID, #I1-COUNT )
        FOR15:                                                                                                                                                            //Natural: FOR #I2-WPID 1 #MAX-WPID
        for (pnd_I2_Wpid.setValue(1); condition(pnd_I2_Wpid.lessOrEqual(pnd_Max_Wpid)); pnd_I2_Wpid.nadd(1))
        {
            if (condition(pnd_Table_2_Pnd_T2_Wpid.getValue(pnd_I2_Wpid).equals(" ")))                                                                                     //Natural: IF #T2-WPID ( #I2-WPID ) = ' '
            {
                pnd_Table_2_Pnd_T2_Wpid.getValue(pnd_I2_Wpid).setValue(pnd_Work_File_Pnd_Wf_Wpid);                                                                        //Natural: ASSIGN #T2-WPID ( #I2-WPID ) := #WF-WPID
                pnd_Table_2_Pnd_T2_Count.getValue(pnd_I2_Wpid,pnd_I2_Count).nadd(1);                                                                                      //Natural: ADD 1 TO #T2-COUNT ( #I2-WPID, #I2-COUNT )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Table_2_Pnd_T2_Wpid.getValue(pnd_I2_Wpid).equals(pnd_Work_File_Pnd_Wf_Wpid)))                                                               //Natural: IF #T2-WPID ( #I2-WPID ) = #WF-WPID
            {
                pnd_Table_2_Pnd_T2_Count.getValue(pnd_I2_Wpid,pnd_I2_Count).nadd(1);                                                                                      //Natural: ADD 1 TO #T2-COUNT ( #I2-WPID, #I2-COUNT )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_I2_Wpid.greaterOrEqual(pnd_Max_Wpid)))                                                                                                          //Natural: IF #I2-WPID GE #MAX-WPID
        {
            getReports().write(0, "****** wpid ct",pnd_I2_Wpid,"=",pnd_Work_File_Pnd_Wf_Wpid);                                                                            //Natural: WRITE '****** wpid ct' #I2-WPID '=' #WF-WPID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  SUMMARY
        short decideConditionsMet1635 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #WF-WPID;//Natural: VALUE 'IA SAP'
        if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("IA SAP"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(1,pnd_I3_Count).nadd(1);                                                                                                    //Natural: ADD 1 TO #T3-COUNT ( 1, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'IA SR'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("IA SR"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(2,pnd_I3_Count).nadd(1);                                                                                                    //Natural: ADD 1 TO #T3-COUNT ( 2, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'IAID'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("IAID"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(3,pnd_I3_Count).nadd(1);                                                                                                    //Natural: ADD 1 TO #T3-COUNT ( 3, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'IAIMD'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("IAIMD"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(4,pnd_I3_Count).nadd(1);                                                                                                    //Natural: ADD 1 TO #T3-COUNT ( 4, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'IAIOD'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("IAIOD"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(5,pnd_I3_Count).nadd(1);                                                                                                    //Natural: ADD 1 TO #T3-COUNT ( 5, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'IAIOEE'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("IAIOEE"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(6,pnd_I3_Count).nadd(1);                                                                                                    //Natural: ADD 1 TO #T3-COUNT ( 6, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'RA OI'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("RA OI"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(7,pnd_I3_Count).nadd(1);                                                                                                    //Natural: ADD 1 TO #T3-COUNT ( 7, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'RAIDAR'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("RAIDAR"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(8,pnd_I3_Count).nadd(1);                                                                                                    //Natural: ADD 1 TO #T3-COUNT ( 8, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'RAIDRC'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("RAIDRC"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(9,pnd_I3_Count).nadd(1);                                                                                                    //Natural: ADD 1 TO #T3-COUNT ( 9, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'RAIDRO'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("RAIDRO"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(10,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 10, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'RAIHH'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("RAIHH"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(11,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 11, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'RAIOE'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("RAIOE"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(12,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 12, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'RAIOF'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("RAIOF"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(13,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 13, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'TA HRT'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("TA HRT"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(14,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 14, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'TA HV'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("TA HV"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(15,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 15, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'TA SRM'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("TA SRM"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(16,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 16, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'TAIDAM'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("TAIDAM"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(17,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 17, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'TAIDLS'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("TAIDLS"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(18,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 18, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'TAIEC'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("TAIEC"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(19,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 19, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'TAIEL'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("TAIEL"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(20,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 20, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'TAIHD'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("TAIHD"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(21,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 21, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'TIPSA'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("TIPSA"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(22,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 22, #I3-COUNT )
        }                                                                                                                                                                 //Natural: VALUE 'TZ MAU'
        else if (condition((pnd_Work_File_Pnd_Wf_Wpid.equals("TZ MAU"))))
        {
            decideConditionsMet1635++;
            pnd_Table_3_Pnd_T3_Count.getValue(23,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 23, #I3-COUNT )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Table_3_Pnd_T3_Count.getValue(24,pnd_I3_Count).nadd(1);                                                                                                   //Natural: ADD 1 TO #T3-COUNT ( 24, #I3-COUNT )
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  SUMMARY-CALCULATION
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Work_File_Pnd_Wf_WpidIsBreak = pnd_Work_File_Pnd_Wf_Wpid.isBreak(endOfData);
        boolean pnd_Work_File_Pnd_Wf_UnitIsBreak = pnd_Work_File_Pnd_Wf_Unit.isBreak(endOfData);
        if (condition(pnd_Work_File_Pnd_Wf_WpidIsBreak || pnd_Work_File_Pnd_Wf_UnitIsBreak))
        {
            pnd_P_Wpid.setValue(readWork01Pnd_Wf_WpidOld);                                                                                                                //Natural: ASSIGN #P-WPID := OLD ( #WF-WPID )
                                                                                                                                                                          //Natural: PERFORM CHANGE-OF-WPID
            sub_Change_Of_Wpid();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-WPID-DETAILS
            sub_Get_Wpid_Details();
            if (condition(Global.isEscape())) {return;}
            pnd_I1_Wpid.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #I1-WPID
            pnd_Table_1_Pnd_T1_Wpid.getValue(pnd_I1_Unit,pnd_I1_Wpid).setValue(pnd_Work_File_Pnd_Wf_Wpid);                                                                //Natural: ASSIGN #T1-WPID ( #I1-UNIT,#I1-WPID ) := #WF-WPID
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Work_File_Pnd_Wf_UnitIsBreak))
        {
            pnd_P_Unit.setValue(readWork01Pnd_Wf_UnitOld);                                                                                                                //Natural: ASSIGN #P-UNIT := OLD ( #WF-UNIT )
                                                                                                                                                                          //Natural: PERFORM CHANGE-OF-UNIT
            sub_Change_Of_Unit();
            if (condition(Global.isEscape())) {return;}
            pnd_I1_Unit.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #I1-UNIT
            //*  REACHED END OF FILE
            if (condition(pnd_I1_Unit.greater(pnd_Max_Unit)))                                                                                                             //Natural: IF #I1-UNIT > #MAX-UNIT
            {
                getReports().write(0, "****** reached end of processing *******",NEWLINE,"=",pnd_Work_File_Pnd_Wf_Unit,"=",pnd_Work_File_Pnd_Wf_Oprtr,                    //Natural: WRITE '****** reached end of processing *******' / '=' #WF-UNIT '=' #WF-OPRTR '=' #WF-WPID
                    "=",pnd_Work_File_Pnd_Wf_Wpid);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_I1_Wpid.setValue(1);                                                                                                                                  //Natural: ASSIGN #I1-WPID := 1
                pnd_Table_1_Pnd_T1_Unit.getValue(pnd_I1_Unit).setValue(pnd_Work_File_Pnd_Wf_Unit);                                                                        //Natural: ASSIGN #T1-UNIT ( #I1-UNIT ) := #WF-UNIT
                pnd_Table_1_Pnd_T1_Wpid.getValue(pnd_I1_Unit,pnd_I1_Wpid).setValue(pnd_Work_File_Pnd_Wf_Wpid);                                                            //Natural: ASSIGN #T1-WPID ( #I1-UNIT,#I1-WPID ) := #WF-WPID
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=55 LS=133");
        Global.format(1, "PS=55 LS=133");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(30),"PAYMENT OPERATIONS - WORK MANAGEMENT - CWF REPORT 1",NEWLINE,Global.getDATX(), 
            new ReportEditMask ("LLL' 'DD', 'YYYY"),new ColumnSpacing(33),pnd_Hdg_Line2,NEWLINE,"Page:",getReports().getPageNumberDbs(1), new ReportEditMask 
            ("ZZZ9"),new ColumnSpacing(14));

        getReports().setDisplayColumns(1, "/UNIT   ",
        		pnd_Work_File_Pnd_Wf_Unit, new IdenticalSuppress(true),"/USER ID",
        		pnd_Work_File_Pnd_Wf_Oprtr,"/WPID",
        		pnd_Work_File_Pnd_Wf_Wpid,"/PIN",
        		pnd_Work_File_Pnd_Wf_Pin, new ReportEditMask ("999999999999"),"DATE/RECVD",
        		pnd_Work_File_Pnd_Wf_Recvd_Dte, new ReportEditMask ("9999/99/99"),"TIME/RECVD",
        		pnd_Work_File_Pnd_Wf_Recvd_Tme, new ReportEditMask ("99:99"),"DUE/DATE",
        		pnd_Work_File_Pnd_Wf_Due_Dte, new ReportEditMask ("9999/99/99"),"DATE/COMPLTD",
        		pnd_Work_File_Pnd_Wf_Compltd_Dte, new ReportEditMask ("9999/99/99"),"UNIT/RECVD",
        		pnd_Work_File_Pnd_Wf_Unit_Open_Dte, new ReportEditMask ("9999/99/99"),"UNIT/COMPLTD",
        		pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte, new ReportEditMask ("9999/99/99"),"PRCSS/DAYS",
        		pnd_P_Prcss_Days,"SVC/STD",
        		pnd_P_Srvce_Std_Pnd_P_Srvce_Std_Days,"STD/MET?",
        		pnd_P_Srvce_Std_Met,"LAST/OPRTR",
        		pnd_Work_File_Pnd_Wf_Compltd_Oprtr);
    }
    private void CheckAtStartofData1206() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM GET-WPID-DETAILS
            sub_Get_Wpid_Details();
            if (condition(Global.isEscape())) {return;}
            pnd_I1_Wpid.setValue(1);                                                                                                                                      //Natural: ASSIGN #I1-WPID := 1
            pnd_I1_Unit.setValue(1);                                                                                                                                      //Natural: ASSIGN #I1-UNIT := 1
            pnd_Table_1_Pnd_T1_Unit.getValue(pnd_I1_Unit).setValue(pnd_Work_File_Pnd_Wf_Unit);                                                                            //Natural: ASSIGN #T1-UNIT ( #I1-UNIT ) := #WF-UNIT
            pnd_Table_1_Pnd_T1_Wpid.getValue(pnd_I1_Unit,pnd_I1_Wpid).setValue(pnd_Work_File_Pnd_Wf_Wpid);                                                                //Natural: ASSIGN #T1-WPID ( #I1-UNIT, #I1-WPID ) := #WF-WPID
            pnd_I2_Wpid.setValue(1);                                                                                                                                      //Natural: ASSIGN #I2-WPID := 1
            pnd_Table_2_Pnd_T2_Wpid.getValue(pnd_I2_Wpid).setValue(pnd_P_Wpid);                                                                                           //Natural: ASSIGN #T2-WPID ( #I2-WPID ) := #P-WPID
        }                                                                                                                                                                 //Natural: END-START
    }
}
