/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:53:55 PM
**        * FROM NATURAL PROGRAM : Mcsp1800
************************************************************
**        * FILE NAME            : Mcsp1800.java
**        * CLASS NAME           : Mcsp1800
**        * INSTANCE NAME        : Mcsp1800
************************************************************
************************************************************************
* PROGRAM  : MCSP1800
* SYSTEM   : PROJCWF
* TITLE    : PRINT UPLOAD AUDIT RECORDS
* GENERATED: MARCH 10, 1994
* FUNCTION : THIS PROGRAM READS THE UPLOAD AUDIT FILE AND PRODUCES
*            THE FOLLOWING REPORTS :
*
*          1.  CMPRT01 - CONTACT SHEET SUCCESSFUL UPLOADS REPORT
*          2.  CMPRT02 - CONTACT SHEET ERROR UPLOADS REPORT
*          3.  CMPRT03 - DUPLICATE CONTACT SHEET UPLOADS REPORT
*          4.  CMPRT04 - UPDATE BATCH SUMMARY REPORT
*
* HISTORY
* 03/07/95  B. ELLO  - CHANGED PRG TO PRODUCE REPORTS FOR ALL SYSTEMS
*                      WHICH ARE IN THE SUPPORT TABLE. TABLE NAME IS
*                      'CWF-CNTCT-SHT-SYSTMS'.
*                    - ADDED A NEW AUDIT REPORT (CMPRT04)
*                    - ADDED A NEW FIELD 'CONTACT-PLS' TO REPORTS 1-3.
* 02/23/2017 - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsp1800 extends BLNatBase
{
    // Data Areas
    private GdaMcsg000 gdaMcsg000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Mcss_Calls;
    private DbsField cwf_Mcss_Calls_Rcrd_Stamp_Tme;
    private DbsField cwf_Mcss_Calls_Rcrd_Upld_Tme;
    private DbsField cwf_Mcss_Calls_Rcrd_Type_Cde;
    private DbsField cwf_Mcss_Calls_Rcrd_Contn_Ind;
    private DbsField cwf_Mcss_Calls_Systm_Ind;
    private DbsField cwf_Mcss_Calls_Pin_Nbr;
    private DbsField cwf_Mcss_Calls_Contact_Id_Cde;
    private DbsField cwf_Mcss_Calls_Contact_Pls;
    private DbsField cwf_Mcss_Calls_Call_Stamp_Tme;
    private DbsField cwf_Mcss_Calls_Call_Rcvd_Dte;
    private DbsField cwf_Mcss_Calls_Btch_Nbr;
    private DbsField cwf_Mcss_Calls_Lines_Nbr;
    private DbsField cwf_Mcss_Calls_Wpids_Nbr;
    private DbsField cwf_Mcss_Calls_Rqst_Entry_Op_Cde;
    private DbsField cwf_Mcss_Calls_Rqst_Origin_Unit_Cde;
    private DbsField cwf_Mcss_Calls_Error_Msg_Txt;
    private DbsField cwf_Mcss_Calls_Ph_Ssn_Nbr;
    private DbsField cwf_Mcss_Calls_Cntct_Total_Nbr;
    private DbsField cwf_Mcss_Calls_Cntct_Error_Nbr;
    private DbsField cwf_Mcss_Calls_Wpids_Total_Nbr;
    private DbsField cwf_Mcss_Calls_Lines_Total_Nbr;
    private DbsField cwf_Mcss_Calls_Attn_Txt;

    private DataAccessProgramView vw_cwf_Mcss_Smry;
    private DbsField cwf_Mcss_Smry_Rcrd_Stamp_Tme;
    private DbsField cwf_Mcss_Smry_Rcrd_Upld_Tme;
    private DbsField cwf_Mcss_Smry_Rcrd_Type_Cde;
    private DbsField cwf_Mcss_Smry_Rcrd_Contn_Ind;
    private DbsField cwf_Mcss_Smry_Systm_Ind;
    private DbsField cwf_Mcss_Smry_Pin_Nbr;
    private DbsField cwf_Mcss_Smry_Contact_Id_Cde;
    private DbsField cwf_Mcss_Smry_Btch_Nbr;
    private DbsField cwf_Mcss_Smry_Lines_Nbr;
    private DbsField cwf_Mcss_Smry_Wpids_Nbr;
    private DbsField cwf_Mcss_Smry_Lines_Total_Nbr;
    private DbsField cwf_Mcss_Smry_Wpids_Total_Nbr;
    private DbsField cwf_Mcss_Smry_Cntct_Total_Nbr;
    private DbsField cwf_Mcss_Smry_Cntct_Error_Nbr;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Parms;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Date;
    private DbsField cwf_Support_Tbl_Pnd_Prior_Date_Parm;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField pnd_I;
    private DbsField pnd_Program;
    private DbsField pnd_Input_Time;
    private DbsField pnd_First_Rec;
    private DbsField pnd_Prev_Btch;
    private DbsField pnd_No_Error;
    private DbsField pnd_Error_Cnt;
    private DbsField pnd_Accept_Reg;
    private DbsField pnd_No_Sign;
    private DbsField pnd_Date_Time;

    private DbsGroup pnd_Date_Time__R_Field_3;
    private DbsField pnd_Date_Time_Pnd_Date;
    private DbsField pnd_Date_Time_Pnd_Time;
    private DbsField pnd_Chk_Dtetme;

    private DbsGroup pnd_Chk_Dtetme__R_Field_4;
    private DbsField pnd_Chk_Dtetme_Pnd_Chk_Date;
    private DbsField pnd_Chk_Dtetme_Pnd_Chk_Time;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_5;
    private DbsField pnd_Date_A_Pnd_Date_Num;

    private DbsGroup pnd_Line;
    private DbsField pnd_Line_Pnd_Tot_Upld;
    private DbsField pnd_Line_Pnd_Tot_Error;
    private DbsField pnd_Line_Pnd_Tot_Success;
    private DbsField pnd_Line_Pnd_Tot_Wpid;
    private DbsField pnd_Line_Pnd_Tot_Lines;
    private DbsField pnd_Line_Pnd_Btch_Success;
    private DbsField pnd_Upld_Date;
    private DbsField pnd_Upld_Time;

    private DbsGroup pnd_Upld_Time__R_Field_6;
    private DbsField pnd_Upld_Time_Pnd_Upld_Timen;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Input_Sys;
    private DbsField pnd_Input_Pnd_Input_Batch;
    private DbsField pnd_Input_From_D;
    private DbsField pnd_Cwf_Mcss_Key;

    private DbsGroup pnd_Cwf_Mcss_Key__R_Field_7;
    private DbsField pnd_Cwf_Mcss_Key_Systm_Ind;
    private DbsField pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme;
    private DbsField pnd_Cwf_Mcss_Key_Rcrd_Type_Cde;
    private DbsField pnd_Cwf_Mcss_Key_Btch_Nbr;
    private DbsField pnd_Cwf_Mcss_Key_Contact_Id_Cde;
    private DbsField pnd_Contact_Key;

    private DbsGroup pnd_Contact_Key__R_Field_8;
    private DbsField pnd_Contact_Key_Systm_Ind;
    private DbsField pnd_Contact_Key_Rcrd_Type_Cde;
    private DbsField pnd_Contact_Key_Contact_Id_Cde;
    private DbsField pnd_Contact_Key_Rcrd_Upld_Tme;
    private DbsField pnd_Cwf_Mcss_Skey;

    private DbsGroup pnd_Cwf_Mcss_Skey__R_Field_9;
    private DbsField pnd_Cwf_Mcss_Skey_Systm_Ind;
    private DbsField pnd_Cwf_Mcss_Skey_Rcrd_Upld_Tme;
    private DbsField pnd_Cwf_Mcss_Skey_Rcrd_Type_Cde;
    private DbsField pnd_Cwf_Mcss_Skey_Btch_Nbr;
    private DbsField pnd_Cwf_Mcss_Skey_Contact_Id_Cde;
    private DbsField pnd_Gen_Tbl_Key;

    private DbsGroup pnd_Gen_Tbl_Key__R_Field_10;
    private DbsField pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Authrty;
    private DbsField pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Name;
    private DbsField pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Code;
    private DbsField pnd_Gen_Tbl_Desc;
    private DbsField pnd_Accepted;
    private DbsField pnd_Prev_Id;
    private DbsField pnd_Printed_Id;
    private DbsField pnd_Prev_Sys;
    private DbsField pnd_Prev_Tme;
    private DbsField pnd_Prev_Error;
    private DbsField pnd_Blank;
    private DbsField pnd_Cnt;
    private DbsField pnd_Contact_Sheet_Systems;
    private DbsField pnd_Tot_Diff;
    private DbsField pnd_Count;
    private DbsField pnd_Rcount;
    private DbsField pnd_Success;
    private DbsField pnd_Date_D;
    private DbsField pnd_Time_S;

    private DbsGroup pnd_Time_S__R_Field_11;
    private DbsField pnd_Time_S_Pnd_Time_N;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_12;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Report_Ran;
    private DbsField pnd_Successful;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Last_Processed_Date_A;
    private DbsField pnd_Last_Processed_Date;
    private DbsField pnd_Last_Processed_Date_Mm;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Input_Fields;

    private DbsGroup pnd_Input_Fields__R_Field_13;
    private DbsField pnd_Input_Fields_Pnd_Input_From;
    private DbsField pnd_Input_Fields_Pnd_Isn_Number;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Systm_IndOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMcsg000 = GdaMcsg000.getInstance(getCallnatLevel());
        registerRecord(gdaMcsg000);
        if (gdaOnly) return;

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Mcss_Calls = new DataAccessProgramView(new NameInfo("vw_cwf_Mcss_Calls", "CWF-MCSS-CALLS"), "CWF_MCSS_CALLS", "CWF_MCSS_CALLS");
        cwf_Mcss_Calls_Rcrd_Stamp_Tme = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rcrd_Stamp_Tme", "RCRD-STAMP-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_STAMP_TME");
        cwf_Mcss_Calls_Rcrd_Stamp_Tme.setDdmHeader("RECORD/TIME/STAMP");
        cwf_Mcss_Calls_Rcrd_Upld_Tme = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_UPLD_TME");
        cwf_Mcss_Calls_Rcrd_Upld_Tme.setDdmHeader("RECORD/UPLOAD/TIME");
        cwf_Mcss_Calls_Rcrd_Type_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        cwf_Mcss_Calls_Rcrd_Type_Cde.setDdmHeader("RECORD/TYPE");
        cwf_Mcss_Calls_Rcrd_Contn_Ind = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rcrd_Contn_Ind", "RCRD-CONTN-IND", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "RCRD_CONTN_IND");
        cwf_Mcss_Calls_Rcrd_Contn_Ind.setDdmHeader("RECORD/CONT./NUMBER");
        cwf_Mcss_Calls_Systm_Ind = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTM_IND");
        cwf_Mcss_Calls_Systm_Ind.setDdmHeader("SYSTEM/OF/ORIGIN");
        cwf_Mcss_Calls_Pin_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Mcss_Calls_Pin_Nbr.setDdmHeader("PIN");
        cwf_Mcss_Calls_Contact_Id_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CONTACT_ID_CDE");
        cwf_Mcss_Calls_Contact_Id_Cde.setDdmHeader("CONTACT/ID");
        cwf_Mcss_Calls_Contact_Pls = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Contact_Pls", "CONTACT-PLS", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "CONTACT_PLS");
        cwf_Mcss_Calls_Call_Stamp_Tme = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Call_Stamp_Tme", "CALL-STAMP-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CALL_STAMP_TME");
        cwf_Mcss_Calls_Call_Stamp_Tme.setDdmHeader("CALL/DATE &/TIME");
        cwf_Mcss_Calls_Call_Rcvd_Dte = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Call_Rcvd_Dte", "CALL-RCVD-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CALL_RCVD_DTE");
        cwf_Mcss_Calls_Call_Rcvd_Dte.setDdmHeader("CALL/DATE");
        cwf_Mcss_Calls_Btch_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Btch_Nbr", "BTCH-NBR", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "BTCH_NBR");
        cwf_Mcss_Calls_Btch_Nbr.setDdmHeader("BATCH/NUM.");
        cwf_Mcss_Calls_Lines_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Lines_Nbr", "LINES-NBR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "LINES_NBR");
        cwf_Mcss_Calls_Lines_Nbr.setDdmHeader("TEXT/LINES/SENT");
        cwf_Mcss_Calls_Wpids_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Wpids_Nbr", "WPIDS-NBR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "WPIDS_NBR");
        cwf_Mcss_Calls_Wpids_Nbr.setDdmHeader("WPIDS/SENT");
        cwf_Mcss_Calls_Rqst_Entry_Op_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rqst_Entry_Op_Cde", "RQST-ENTRY-OP-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_ENTRY_OP_CDE");
        cwf_Mcss_Calls_Rqst_Entry_Op_Cde.setDdmHeader("ORIGIN/RACF/ID");
        cwf_Mcss_Calls_Rqst_Origin_Unit_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rqst_Origin_Unit_Cde", "RQST-ORIGIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_ORIGIN_UNIT_CDE");
        cwf_Mcss_Calls_Rqst_Origin_Unit_Cde.setDdmHeader("ORIGIN/UNIT");
        cwf_Mcss_Calls_Error_Msg_Txt = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Error_Msg_Txt", "ERROR-MSG-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ERROR_MSG_TXT");
        cwf_Mcss_Calls_Error_Msg_Txt.setDdmHeader("ERROR/MSG.");
        cwf_Mcss_Calls_Ph_Ssn_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Ph_Ssn_Nbr", "PH-SSN-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PH_SSN_NBR");
        cwf_Mcss_Calls_Cntct_Total_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Cntct_Total_Nbr", "CNTCT-TOTAL-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTCT_TOTAL_NBR");
        cwf_Mcss_Calls_Cntct_Total_Nbr.setDdmHeader("TOTAL/CONTACT/SHEETS");
        cwf_Mcss_Calls_Cntct_Error_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Cntct_Error_Nbr", "CNTCT-ERROR-NBR", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "CNTCT_ERROR_NBR");
        cwf_Mcss_Calls_Cntct_Error_Nbr.setDdmHeader("TOTAL/ERRORS");
        cwf_Mcss_Calls_Wpids_Total_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Wpids_Total_Nbr", "WPIDS-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "WPIDS_TOTAL_NBR");
        cwf_Mcss_Calls_Wpids_Total_Nbr.setDdmHeader("TOTAL/WPIDS");
        cwf_Mcss_Calls_Lines_Total_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Lines_Total_Nbr", "LINES-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LINES_TOTAL_NBR");
        cwf_Mcss_Calls_Lines_Total_Nbr.setDdmHeader("TOTAL/LINES");
        cwf_Mcss_Calls_Attn_Txt = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Attn_Txt", "ATTN-TXT", FieldType.STRING, 25, RepeatingFieldStrategy.None, 
            "ATTN_TXT");
        cwf_Mcss_Calls_Attn_Txt.setDdmHeader("ATTN./TEXT");
        registerRecord(vw_cwf_Mcss_Calls);

        vw_cwf_Mcss_Smry = new DataAccessProgramView(new NameInfo("vw_cwf_Mcss_Smry", "CWF-MCSS-SMRY"), "CWF_MCSS_CALLS", "CWF_MCSS_CALLS");
        cwf_Mcss_Smry_Rcrd_Stamp_Tme = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Rcrd_Stamp_Tme", "RCRD-STAMP-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_STAMP_TME");
        cwf_Mcss_Smry_Rcrd_Stamp_Tme.setDdmHeader("RECORD/TIME/STAMP");
        cwf_Mcss_Smry_Rcrd_Upld_Tme = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "RCRD_UPLD_TME");
        cwf_Mcss_Smry_Rcrd_Upld_Tme.setDdmHeader("RECORD/UPLOAD/TIME");
        cwf_Mcss_Smry_Rcrd_Type_Cde = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        cwf_Mcss_Smry_Rcrd_Type_Cde.setDdmHeader("RECORD/TYPE");
        cwf_Mcss_Smry_Rcrd_Contn_Ind = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Rcrd_Contn_Ind", "RCRD-CONTN-IND", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "RCRD_CONTN_IND");
        cwf_Mcss_Smry_Rcrd_Contn_Ind.setDdmHeader("RECORD/CONT./NUMBER");
        cwf_Mcss_Smry_Systm_Ind = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTM_IND");
        cwf_Mcss_Smry_Systm_Ind.setDdmHeader("SYSTEM/OF/ORIGIN");
        cwf_Mcss_Smry_Pin_Nbr = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Mcss_Smry_Pin_Nbr.setDdmHeader("PIN");
        cwf_Mcss_Smry_Contact_Id_Cde = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CONTACT_ID_CDE");
        cwf_Mcss_Smry_Contact_Id_Cde.setDdmHeader("CONTACT/ID");
        cwf_Mcss_Smry_Btch_Nbr = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Btch_Nbr", "BTCH-NBR", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "BTCH_NBR");
        cwf_Mcss_Smry_Btch_Nbr.setDdmHeader("BATCH/NUM.");
        cwf_Mcss_Smry_Lines_Nbr = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Lines_Nbr", "LINES-NBR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "LINES_NBR");
        cwf_Mcss_Smry_Lines_Nbr.setDdmHeader("TEXT/LINES/SENT");
        cwf_Mcss_Smry_Wpids_Nbr = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Wpids_Nbr", "WPIDS-NBR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "WPIDS_NBR");
        cwf_Mcss_Smry_Wpids_Nbr.setDdmHeader("WPIDS/SENT");
        cwf_Mcss_Smry_Lines_Total_Nbr = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Lines_Total_Nbr", "LINES-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LINES_TOTAL_NBR");
        cwf_Mcss_Smry_Lines_Total_Nbr.setDdmHeader("TOTAL/LINES");
        cwf_Mcss_Smry_Wpids_Total_Nbr = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Wpids_Total_Nbr", "WPIDS-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "WPIDS_TOTAL_NBR");
        cwf_Mcss_Smry_Wpids_Total_Nbr.setDdmHeader("TOTAL/WPIDS");
        cwf_Mcss_Smry_Cntct_Total_Nbr = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Cntct_Total_Nbr", "CNTCT-TOTAL-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTCT_TOTAL_NBR");
        cwf_Mcss_Smry_Cntct_Total_Nbr.setDdmHeader("TOTAL/CONTACT/SHEETS");
        cwf_Mcss_Smry_Cntct_Error_Nbr = vw_cwf_Mcss_Smry.getRecord().newFieldInGroup("cwf_Mcss_Smry_Cntct_Error_Nbr", "CNTCT-ERROR-NBR", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "CNTCT_ERROR_NBR");
        cwf_Mcss_Smry_Cntct_Error_Nbr.setDdmHeader("TOTAL/ERRORS");
        registerRecord(vw_cwf_Mcss_Smry);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Regular_Run_Parms = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Parms", "#REGULAR-RUN-PARMS", 
            FieldType.STRING, 30);

        cwf_Support_Tbl__R_Field_2 = cwf_Support_Tbl__R_Field_1.newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Pnd_Regular_Run_Parms);
        cwf_Support_Tbl_Pnd_Regular_Run_Date = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Date", "#REGULAR-RUN-DATE", 
            FieldType.STRING, 8);
        cwf_Support_Tbl_Pnd_Prior_Date_Parm = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Prior_Date_Parm", "#PRIOR-DATE-PARM", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Input_Time = localVariables.newFieldInRecord("pnd_Input_Time", "#INPUT-TIME", FieldType.PACKED_DECIMAL, 6);
        pnd_First_Rec = localVariables.newFieldInRecord("pnd_First_Rec", "#FIRST-REC", FieldType.STRING, 1);
        pnd_Prev_Btch = localVariables.newFieldInRecord("pnd_Prev_Btch", "#PREV-BTCH", FieldType.NUMERIC, 8);
        pnd_No_Error = localVariables.newFieldInRecord("pnd_No_Error", "#NO-ERROR", FieldType.NUMERIC, 4);
        pnd_Error_Cnt = localVariables.newFieldInRecord("pnd_Error_Cnt", "#ERROR-CNT", FieldType.NUMERIC, 4);
        pnd_Accept_Reg = localVariables.newFieldInRecord("pnd_Accept_Reg", "#ACCEPT-REG", FieldType.STRING, 1);
        pnd_No_Sign = localVariables.newFieldInRecord("pnd_No_Sign", "#NO-SIGN", FieldType.STRING, 1);
        pnd_Date_Time = localVariables.newFieldInRecord("pnd_Date_Time", "#DATE-TIME", FieldType.STRING, 14);

        pnd_Date_Time__R_Field_3 = localVariables.newGroupInRecord("pnd_Date_Time__R_Field_3", "REDEFINE", pnd_Date_Time);
        pnd_Date_Time_Pnd_Date = pnd_Date_Time__R_Field_3.newFieldInGroup("pnd_Date_Time_Pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_Date_Time_Pnd_Time = pnd_Date_Time__R_Field_3.newFieldInGroup("pnd_Date_Time_Pnd_Time", "#TIME", FieldType.STRING, 6);
        pnd_Chk_Dtetme = localVariables.newFieldInRecord("pnd_Chk_Dtetme", "#CHK-DTETME", FieldType.STRING, 14);

        pnd_Chk_Dtetme__R_Field_4 = localVariables.newGroupInRecord("pnd_Chk_Dtetme__R_Field_4", "REDEFINE", pnd_Chk_Dtetme);
        pnd_Chk_Dtetme_Pnd_Chk_Date = pnd_Chk_Dtetme__R_Field_4.newFieldInGroup("pnd_Chk_Dtetme_Pnd_Chk_Date", "#CHK-DATE", FieldType.STRING, 8);
        pnd_Chk_Dtetme_Pnd_Chk_Time = pnd_Chk_Dtetme__R_Field_4.newFieldInGroup("pnd_Chk_Dtetme_Pnd_Chk_Time", "#CHK-TIME", FieldType.STRING, 6);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 14);

        pnd_Date_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_5", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_Num = pnd_Date_A__R_Field_5.newFieldInGroup("pnd_Date_A_Pnd_Date_Num", "#DATE-NUM", FieldType.NUMERIC, 14);

        pnd_Line = localVariables.newGroupInRecord("pnd_Line", "#LINE");
        pnd_Line_Pnd_Tot_Upld = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Tot_Upld", "#TOT-UPLD", FieldType.NUMERIC, 10);
        pnd_Line_Pnd_Tot_Error = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Tot_Error", "#TOT-ERROR", FieldType.NUMERIC, 10);
        pnd_Line_Pnd_Tot_Success = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Tot_Success", "#TOT-SUCCESS", FieldType.NUMERIC, 10);
        pnd_Line_Pnd_Tot_Wpid = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Tot_Wpid", "#TOT-WPID", FieldType.NUMERIC, 10);
        pnd_Line_Pnd_Tot_Lines = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Tot_Lines", "#TOT-LINES", FieldType.NUMERIC, 10);
        pnd_Line_Pnd_Btch_Success = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Btch_Success", "#BTCH-SUCCESS", FieldType.NUMERIC, 8);
        pnd_Upld_Date = localVariables.newFieldInRecord("pnd_Upld_Date", "#UPLD-DATE", FieldType.DATE);
        pnd_Upld_Time = localVariables.newFieldInRecord("pnd_Upld_Time", "#UPLD-TIME", FieldType.STRING, 4);

        pnd_Upld_Time__R_Field_6 = localVariables.newGroupInRecord("pnd_Upld_Time__R_Field_6", "REDEFINE", pnd_Upld_Time);
        pnd_Upld_Time_Pnd_Upld_Timen = pnd_Upld_Time__R_Field_6.newFieldInGroup("pnd_Upld_Time_Pnd_Upld_Timen", "#UPLD-TIMEN", FieldType.NUMERIC, 4);

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Input_Sys = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Input_Sys", "#INPUT-SYS", FieldType.STRING, 15);
        pnd_Input_Pnd_Input_Batch = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Input_Batch", "#INPUT-BATCH", FieldType.NUMERIC, 8);
        pnd_Input_From_D = localVariables.newFieldInRecord("pnd_Input_From_D", "#INPUT-FROM-D", FieldType.DATE);
        pnd_Cwf_Mcss_Key = localVariables.newFieldInRecord("pnd_Cwf_Mcss_Key", "#CWF-MCSS-KEY", FieldType.STRING, 41);

        pnd_Cwf_Mcss_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Cwf_Mcss_Key__R_Field_7", "REDEFINE", pnd_Cwf_Mcss_Key);
        pnd_Cwf_Mcss_Key_Systm_Ind = pnd_Cwf_Mcss_Key__R_Field_7.newFieldInGroup("pnd_Cwf_Mcss_Key_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15);
        pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme = pnd_Cwf_Mcss_Key__R_Field_7.newFieldInGroup("pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME);
        pnd_Cwf_Mcss_Key_Rcrd_Type_Cde = pnd_Cwf_Mcss_Key__R_Field_7.newFieldInGroup("pnd_Cwf_Mcss_Key_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Cwf_Mcss_Key_Btch_Nbr = pnd_Cwf_Mcss_Key__R_Field_7.newFieldInGroup("pnd_Cwf_Mcss_Key_Btch_Nbr", "BTCH-NBR", FieldType.NUMERIC, 8);
        pnd_Cwf_Mcss_Key_Contact_Id_Cde = pnd_Cwf_Mcss_Key__R_Field_7.newFieldInGroup("pnd_Cwf_Mcss_Key_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10);
        pnd_Contact_Key = localVariables.newFieldInRecord("pnd_Contact_Key", "#CONTACT-KEY", FieldType.STRING, 33);

        pnd_Contact_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Contact_Key__R_Field_8", "REDEFINE", pnd_Contact_Key);
        pnd_Contact_Key_Systm_Ind = pnd_Contact_Key__R_Field_8.newFieldInGroup("pnd_Contact_Key_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15);
        pnd_Contact_Key_Rcrd_Type_Cde = pnd_Contact_Key__R_Field_8.newFieldInGroup("pnd_Contact_Key_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Contact_Key_Contact_Id_Cde = pnd_Contact_Key__R_Field_8.newFieldInGroup("pnd_Contact_Key_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10);
        pnd_Contact_Key_Rcrd_Upld_Tme = pnd_Contact_Key__R_Field_8.newFieldInGroup("pnd_Contact_Key_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME);
        pnd_Cwf_Mcss_Skey = localVariables.newFieldInRecord("pnd_Cwf_Mcss_Skey", "#CWF-MCSS-SKEY", FieldType.STRING, 41);

        pnd_Cwf_Mcss_Skey__R_Field_9 = localVariables.newGroupInRecord("pnd_Cwf_Mcss_Skey__R_Field_9", "REDEFINE", pnd_Cwf_Mcss_Skey);
        pnd_Cwf_Mcss_Skey_Systm_Ind = pnd_Cwf_Mcss_Skey__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Skey_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15);
        pnd_Cwf_Mcss_Skey_Rcrd_Upld_Tme = pnd_Cwf_Mcss_Skey__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Skey_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME);
        pnd_Cwf_Mcss_Skey_Rcrd_Type_Cde = pnd_Cwf_Mcss_Skey__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Skey_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Cwf_Mcss_Skey_Btch_Nbr = pnd_Cwf_Mcss_Skey__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Skey_Btch_Nbr", "BTCH-NBR", FieldType.NUMERIC, 8);
        pnd_Cwf_Mcss_Skey_Contact_Id_Cde = pnd_Cwf_Mcss_Skey__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Skey_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10);
        pnd_Gen_Tbl_Key = localVariables.newFieldInRecord("pnd_Gen_Tbl_Key", "#GEN-TBL-KEY", FieldType.STRING, 53);

        pnd_Gen_Tbl_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Gen_Tbl_Key__R_Field_10", "REDEFINE", pnd_Gen_Tbl_Key);
        pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Authrty = pnd_Gen_Tbl_Key__R_Field_10.newFieldInGroup("pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Authrty", "#GEN-TBL-AUTHRTY", FieldType.STRING, 
            2);
        pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Name = pnd_Gen_Tbl_Key__R_Field_10.newFieldInGroup("pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Name", "#GEN-TBL-NAME", FieldType.STRING, 
            20);
        pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Code = pnd_Gen_Tbl_Key__R_Field_10.newFieldInGroup("pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Code", "#GEN-TBL-CODE", FieldType.STRING, 
            30);
        pnd_Gen_Tbl_Desc = localVariables.newFieldInRecord("pnd_Gen_Tbl_Desc", "#GEN-TBL-DESC", FieldType.STRING, 30);
        pnd_Accepted = localVariables.newFieldInRecord("pnd_Accepted", "#ACCEPTED", FieldType.STRING, 1);
        pnd_Prev_Id = localVariables.newFieldInRecord("pnd_Prev_Id", "#PREV-ID", FieldType.STRING, 10);
        pnd_Printed_Id = localVariables.newFieldInRecord("pnd_Printed_Id", "#PRINTED-ID", FieldType.STRING, 10);
        pnd_Prev_Sys = localVariables.newFieldInRecord("pnd_Prev_Sys", "#PREV-SYS", FieldType.STRING, 15);
        pnd_Prev_Tme = localVariables.newFieldInRecord("pnd_Prev_Tme", "#PREV-TME", FieldType.TIME);
        pnd_Prev_Error = localVariables.newFieldInRecord("pnd_Prev_Error", "#PREV-ERROR", FieldType.STRING, 60);
        pnd_Blank = localVariables.newFieldInRecord("pnd_Blank", "#BLANK", FieldType.STRING, 10);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 3);
        pnd_Contact_Sheet_Systems = localVariables.newFieldArrayInRecord("pnd_Contact_Sheet_Systems", "#CONTACT-SHEET-SYSTEMS", FieldType.STRING, 8, new 
            DbsArrayController(1, 100));
        pnd_Tot_Diff = localVariables.newFieldInRecord("pnd_Tot_Diff", "#TOT-DIFF", FieldType.TIME);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 4);
        pnd_Rcount = localVariables.newFieldInRecord("pnd_Rcount", "#RCOUNT", FieldType.NUMERIC, 4);
        pnd_Success = localVariables.newFieldInRecord("pnd_Success", "#SUCCESS", FieldType.NUMERIC, 6);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Time_S = localVariables.newFieldInRecord("pnd_Time_S", "#TIME-S", FieldType.STRING, 6);

        pnd_Time_S__R_Field_11 = localVariables.newGroupInRecord("pnd_Time_S__R_Field_11", "REDEFINE", pnd_Time_S);
        pnd_Time_S_Pnd_Time_N = pnd_Time_S__R_Field_11.newFieldInGroup("pnd_Time_S_Pnd_Time_N", "#TIME-N", FieldType.NUMERIC, 6);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_12", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_12.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_12.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_12.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_12.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Report_Ran = localVariables.newFieldInRecord("pnd_Report_Ran", "#REPORT-RAN", FieldType.BOOLEAN, 1);
        pnd_Successful = localVariables.newFieldInRecord("pnd_Successful", "#SUCCESSFUL", FieldType.BOOLEAN, 1);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 8);
        pnd_Last_Processed_Date_A = localVariables.newFieldInRecord("pnd_Last_Processed_Date_A", "#LAST-PROCESSED-DATE-A", FieldType.STRING, 8);
        pnd_Last_Processed_Date = localVariables.newFieldInRecord("pnd_Last_Processed_Date", "#LAST-PROCESSED-DATE", FieldType.DATE);
        pnd_Last_Processed_Date_Mm = localVariables.newFieldInRecord("pnd_Last_Processed_Date_Mm", "#LAST-PROCESSED-DATE-MM", FieldType.STRING, 2);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Input_Fields = localVariables.newFieldInRecord("pnd_Input_Fields", "#INPUT-FIELDS", FieldType.STRING, 20);

        pnd_Input_Fields__R_Field_13 = localVariables.newGroupInRecord("pnd_Input_Fields__R_Field_13", "REDEFINE", pnd_Input_Fields);
        pnd_Input_Fields_Pnd_Input_From = pnd_Input_Fields__R_Field_13.newFieldInGroup("pnd_Input_Fields_Pnd_Input_From", "#INPUT-FROM", FieldType.STRING, 
            8);
        pnd_Input_Fields_Pnd_Isn_Number = pnd_Input_Fields__R_Field_13.newFieldInGroup("pnd_Input_Fields_Pnd_Isn_Number", "#ISN-NUMBER", FieldType.NUMERIC, 
            12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Systm_IndOld = internalLoopRecord.newFieldInRecord("Sort01_Systm_Ind_OLD", "Systm_Ind_OLD", FieldType.STRING, 15);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Mcss_Calls.reset();
        vw_cwf_Mcss_Smry.reset();
        vw_cwf_Support_Tbl.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_First_Rec.setInitialValue("Y");
        pnd_Report_Ran.setInitialValue(false);
        pnd_Successful.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Mcsp1800() throws Exception
    {
        super("Mcsp1800");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Mcsp1800|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = OFF;//Natural: FORMAT ( 2 ) LS = 133 PS = 60 ZP = OFF;//Natural: FORMAT ( 3 ) LS = 133 PS = 60 ZP = OFF;//Natural: FORMAT ( 4 ) LS = 133 PS = 60 ZP = OFF
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN #PROGRAM := *PROGRAM
                if (condition(Global.getSTACK().getDatacount().greater(getZero()), INPUT_1))                                                                              //Natural: IF *DATA > 0
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Fields);                                                                                 //Natural: INPUT #INPUT-FIELDS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Input_Fields_Pnd_Input_From.reset();                                                                                                              //Natural: RESET #INPUT-FROM #ISN-NUMBER
                    pnd_Input_Fields_Pnd_Isn_Number.reset();
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Fields_Pnd_Input_From.equals(" ")))                                                                                               //Natural: IF #INPUT-FROM = ' '
                {
                    pnd_Input_Pnd_Input_Batch.reset();                                                                                                                    //Natural: RESET #INPUT-BATCH
                    pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                              //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND := 'A'
                    pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("CONTACT-SYS-PRCSS-DT");                                                                                 //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME := 'CONTACT-SYS-PRCSS-DT'
                    pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("PARTICIPANT-CONTACT");                                                                                  //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-KEY-FIELD := 'PARTICIPANT-CONTACT'
                    vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
                    (
                    "PND_PND_L2070",
                    new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
                    1
                    );
                    PND_PND_L2070:
                    while (condition(vw_cwf_Support_Tbl.readNextRow("PND_PND_L2070")))
                    {
                        vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
                        pnd_Input_Fields_Pnd_Input_From.setValue(cwf_Support_Tbl_Pnd_Regular_Run_Parms);                                                                  //Natural: ASSIGN #INPUT-FROM := CWF-SUPPORT-TBL.#REGULAR-RUN-PARMS
                        pnd_Isn_Tbl.setValue(vw_cwf_Support_Tbl.getAstISN("PND_PND_L2070"));                                                                              //Natural: ASSIGN #ISN-TBL := *ISN ( ##L2070. )
                        pnd_Input_Fields_Pnd_Isn_Number.setValue(pnd_Isn_Tbl);                                                                                            //Natural: ASSIGN #ISN-NUMBER := #ISN-TBL
                    }                                                                                                                                                     //Natural: END-FIND
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //* ******************      R E P O R T S   1 & 2   **********************
                pnd_Input_From_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Fields_Pnd_Input_From);                                                          //Natural: MOVE EDITED #INPUT-FROM TO #INPUT-FROM-D ( EM = YYYYMMDD )
                if (condition(pnd_Input_From_D.greater(Global.getDATX())))                                                                                                //Natural: IF #INPUT-FROM-D GT *DATX
                {
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Input_From_D.equals(Global.getDATX())))                                                                                             //Natural: IF #INPUT-FROM-D = *DATX
                    {
                        if (condition(Global.getTIME().greaterOrEqual("00:00:00.0") && Global.getTIME().lessOrEqual("06:59:59.9")))                                       //Natural: IF *TIME = '00:00:00.0' THRU '06:59:59.9'
                        {
                            if (condition(true)) return;                                                                                                                  //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-INPUT-PARAMETERS
                sub_Check_Input_Parameters();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-ALL-CONTACT-SHEET-SYSTEMS
                sub_Get_All_Contact_Sheet_Systems();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                FOR01:                                                                                                                                                    //Natural: FOR #I = 1 TO #CNT
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Cnt)); pnd_I.nadd(1))
                {
                    pnd_Cwf_Mcss_Key_Systm_Ind.setValue(pnd_Contact_Sheet_Systems.getValue(pnd_I));                                                                       //Natural: ASSIGN #CWF-MCSS-KEY.SYSTM-IND := #INPUT-SYS := #CONTACT-SHEET-SYSTEMS ( #I )
                    pnd_Input_Pnd_Input_Sys.setValue(pnd_Contact_Sheet_Systems.getValue(pnd_I));
                    pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Fields_Pnd_Input_From);                                        //Natural: MOVE EDITED #INPUT-FROM TO #CWF-MCSS-KEY.RCRD-UPLD-TME ( EM = YYYYMMDD )
                    pnd_Cwf_Mcss_Key_Rcrd_Type_Cde.setValue("A");                                                                                                         //Natural: ASSIGN #CWF-MCSS-KEY.RCRD-TYPE-CDE := 'A'
                    pnd_Cwf_Mcss_Key_Btch_Nbr.setValue(pnd_Input_Pnd_Input_Batch);                                                                                        //Natural: ASSIGN #CWF-MCSS-KEY.BTCH-NBR := #INPUT-BATCH
                    pnd_Date_Time_Pnd_Date.setValue(pnd_Input_Fields_Pnd_Input_From);                                                                                     //Natural: ASSIGN #DATE := #INPUT-FROM
                    pnd_Date_Time_Pnd_Time.setValue("000001");                                                                                                            //Natural: ASSIGN #TIME := '000001'
                    pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Date_Time);                                                    //Natural: MOVE EDITED #DATE-TIME TO #CWF-MCSS-KEY.RCRD-UPLD-TME ( EM = YYYYMMDDHHIISS )
                    vw_cwf_Mcss_Calls.startDatabaseRead                                                                                                                   //Natural: READ CWF-MCSS-CALLS WITH BATCH-KEY EQ #CWF-MCSS-KEY
                    (
                    "READ_AUDIT",
                    new Wc[] { new Wc("BATCH_KEY", ">=", pnd_Cwf_Mcss_Key.getBinary(), WcType.BY) },
                    new Oc[] { new Oc("BATCH_KEY", "ASC") }
                    );
                    READ_AUDIT:
                    while (condition(vw_cwf_Mcss_Calls.readNextRow("READ_AUDIT")))
                    {
                        if (condition(cwf_Mcss_Calls_Systm_Ind.notEquals(pnd_Input_Pnd_Input_Sys)))                                                                       //Natural: IF CWF-MCSS-CALLS.SYSTM-IND NE #INPUT-SYS
                        {
                            if (true) break READ_AUDIT;                                                                                                                   //Natural: ESCAPE BOTTOM ( READ-AUDIT. ) IMMEDIATE
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Chk_Dtetme.setValueEdited(cwf_Mcss_Calls_Rcrd_Upld_Tme,new ReportEditMask("YYYYMMDDHHIISS"));                                                 //Natural: MOVE EDITED CWF-MCSS-CALLS.RCRD-UPLD-TME ( EM = YYYYMMDDHHIISS ) TO #CHK-DTETME
                        if (condition(pnd_Chk_Dtetme_Pnd_Chk_Date.notEquals(pnd_Input_Fields_Pnd_Input_From)))                                                            //Natural: IF #CHK-DATE NE #INPUT-FROM
                        {
                            if (true) break READ_AUDIT;                                                                                                                   //Natural: ESCAPE BOTTOM ( READ-AUDIT. ) IMMEDIATE
                        }                                                                                                                                                 //Natural: END-IF
                        getSort().writeSortInData(cwf_Mcss_Calls_Systm_Ind, cwf_Mcss_Calls_Rcrd_Type_Cde, cwf_Mcss_Calls_Btch_Nbr, cwf_Mcss_Calls_Contact_Id_Cde,         //Natural: END-ALL
                            cwf_Mcss_Calls_Rcrd_Stamp_Tme, cwf_Mcss_Calls_Rcrd_Upld_Tme, cwf_Mcss_Calls_Rcrd_Contn_Ind, cwf_Mcss_Calls_Pin_Nbr, cwf_Mcss_Calls_Call_Stamp_Tme, 
                            cwf_Mcss_Calls_Call_Rcvd_Dte, cwf_Mcss_Calls_Lines_Nbr, cwf_Mcss_Calls_Wpids_Nbr, cwf_Mcss_Calls_Ph_Ssn_Nbr, cwf_Mcss_Calls_Error_Msg_Txt, 
                            cwf_Mcss_Calls_Rqst_Entry_Op_Cde, cwf_Mcss_Calls_Rqst_Origin_Unit_Cde, cwf_Mcss_Calls_Contact_Pls, cwf_Mcss_Calls_Attn_Txt);
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                getSort().sortData(cwf_Mcss_Calls_Systm_Ind, cwf_Mcss_Calls_Rcrd_Type_Cde, cwf_Mcss_Calls_Btch_Nbr, cwf_Mcss_Calls_Contact_Id_Cde);                       //Natural: SORT RECORDS BY CWF-MCSS-CALLS.SYSTM-IND CWF-MCSS-CALLS.RCRD-TYPE-CDE CWF-MCSS-CALLS.BTCH-NBR CWF-MCSS-CALLS.CONTACT-ID-CDE USING CWF-MCSS-CALLS.RCRD-STAMP-TME CWF-MCSS-CALLS.RCRD-UPLD-TME CWF-MCSS-CALLS.RCRD-CONTN-IND CWF-MCSS-CALLS.PIN-NBR CWF-MCSS-CALLS.CALL-STAMP-TME CWF-MCSS-CALLS.CALL-RCVD-DTE CWF-MCSS-CALLS.LINES-NBR CWF-MCSS-CALLS.WPIDS-NBR CWF-MCSS-CALLS.PH-SSN-NBR CWF-MCSS-CALLS.ERROR-MSG-TXT CWF-MCSS-CALLS.RQST-ENTRY-OP-CDE CWF-MCSS-CALLS.RQST-ORIGIN-UNIT-CDE CWF-MCSS-CALLS.CONTACT-PLS CWF-MCSS-CALLS.ATTN-TXT
                boolean endOfDataSort01 = true;
                boolean firstSort01 = true;
                SORT01:
                while (condition(getSort().readSortOutData(cwf_Mcss_Calls_Systm_Ind, cwf_Mcss_Calls_Rcrd_Type_Cde, cwf_Mcss_Calls_Btch_Nbr, cwf_Mcss_Calls_Contact_Id_Cde, 
                    cwf_Mcss_Calls_Rcrd_Stamp_Tme, cwf_Mcss_Calls_Rcrd_Upld_Tme, cwf_Mcss_Calls_Rcrd_Contn_Ind, cwf_Mcss_Calls_Pin_Nbr, cwf_Mcss_Calls_Call_Stamp_Tme, 
                    cwf_Mcss_Calls_Call_Rcvd_Dte, cwf_Mcss_Calls_Lines_Nbr, cwf_Mcss_Calls_Wpids_Nbr, cwf_Mcss_Calls_Ph_Ssn_Nbr, cwf_Mcss_Calls_Error_Msg_Txt, 
                    cwf_Mcss_Calls_Rqst_Entry_Op_Cde, cwf_Mcss_Calls_Rqst_Origin_Unit_Cde, cwf_Mcss_Calls_Contact_Pls, cwf_Mcss_Calls_Attn_Txt)))
                {
                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventSort01(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataSort01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    pnd_Input_Pnd_Input_Sys.setValue(cwf_Mcss_Calls_Systm_Ind);                                                                                           //Natural: ASSIGN #INPUT-SYS := CWF-MCSS-CALLS.SYSTM-IND
                    //*                                                                                                                                                   //Natural: AT BREAK OF CWF-MCSS-CALLS.SYSTM-IND
                    if (condition(pnd_Input_Pnd_Input_Batch.notEquals(getZero())))                                                                                        //Natural: IF #INPUT-BATCH NE 0
                    {
                        if (condition(cwf_Mcss_Calls_Btch_Nbr.equals(pnd_Input_Pnd_Input_Batch)))                                                                         //Natural: IF CWF-MCSS-CALLS.BTCH-NBR = #INPUT-BATCH
                        {
                            pnd_Accepted.setValue("Y");                                                                                                                   //Natural: ASSIGN #ACCEPTED := 'Y'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Accepted.setValue("N");                                                                                                                   //Natural: ASSIGN #ACCEPTED := 'N'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Accepted.setValue("Y");                                                                                                                       //Natural: ASSIGN #ACCEPTED := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cwf_Mcss_Calls_Rcrd_Type_Cde.equals("A")))                                                                                              //Natural: IF CWF-MCSS-CALLS.RCRD-TYPE-CDE = 'A'
                    {
                        if (condition(cwf_Mcss_Calls_Btch_Nbr.notEquals(pnd_Prev_Btch) && pnd_Accepted.equals("Y") && pnd_First_Rec.notEquals("Y")))                      //Natural: IF CWF-MCSS-CALLS.BTCH-NBR NE #PREV-BTCH AND #ACCEPTED = 'Y' AND #FIRST-REC NE 'Y'
                        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-BATCH-SUMMARY
                            sub_Process_Batch_Summary();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Accepted.equals("Y")))                                                                                                          //Natural: IF #ACCEPTED = 'Y'
                        {
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-LINE
                            sub_Write_Detail_Line();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_First_Rec.setValue("N");                                                                                                                  //Natural: ASSIGN #FIRST-REC := 'N'
                            pnd_Prev_Btch.setValue(cwf_Mcss_Calls_Btch_Nbr);                                                                                              //Natural: ASSIGN #PREV-BTCH := CWF-MCSS-CALLS.BTCH-NBR
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    sort01Systm_IndOld.setValue(cwf_Mcss_Calls_Systm_Ind);                                                                                                //Natural: END-SORT
                }
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    atBreakEventSort01(endOfDataSort01);
                }
                endSort();
                //* ********************       R E P O R T   3      ***********************
                FOR02:                                                                                                                                                    //Natural: FOR #I = 1 TO #CNT
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Cnt)); pnd_I.nadd(1))
                {
                    pnd_Contact_Key_Systm_Ind.setValue(pnd_Contact_Sheet_Systems.getValue(pnd_I));                                                                        //Natural: ASSIGN #CONTACT-KEY.SYSTM-IND := #INPUT-SYS := #CONTACT-SHEET-SYSTEMS ( #I )
                    pnd_Input_Pnd_Input_Sys.setValue(pnd_Contact_Sheet_Systems.getValue(pnd_I));
                    pnd_Contact_Key_Rcrd_Type_Cde.setValue("A");                                                                                                          //Natural: ASSIGN #CONTACT-KEY.RCRD-TYPE-CDE := 'A'
                    vw_cwf_Mcss_Calls.startDatabaseRead                                                                                                                   //Natural: READ CWF-MCSS-CALLS WITH CONTACT-KEY EQ #CONTACT-KEY
                    (
                    "READ_CONTACT",
                    new Wc[] { new Wc("CONTACT_KEY", ">=", pnd_Contact_Key.getBinary(), WcType.BY) },
                    new Oc[] { new Oc("CONTACT_KEY", "ASC") }
                    );
                    READ_CONTACT:
                    while (condition(vw_cwf_Mcss_Calls.readNextRow("READ_CONTACT")))
                    {
                        if (condition(cwf_Mcss_Calls_Systm_Ind.notEquals(pnd_Input_Pnd_Input_Sys) || cwf_Mcss_Calls_Rcrd_Type_Cde.notEquals("A")))                        //Natural: IF CWF-MCSS-CALLS.SYSTM-IND NE #INPUT-SYS OR CWF-MCSS-CALLS.RCRD-TYPE-CDE NE 'A'
                        {
                            if (true) break READ_CONTACT;                                                                                                                 //Natural: ESCAPE BOTTOM ( READ-CONTACT. )
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Accept_Reg.setValue("N");                                                                                                                     //Natural: ASSIGN #ACCEPT-REG := 'N'
                        pnd_Chk_Dtetme.setValueEdited(cwf_Mcss_Calls_Rcrd_Upld_Tme,new ReportEditMask("YYYYMMDDHHIISS"));                                                 //Natural: MOVE EDITED CWF-MCSS-CALLS.RCRD-UPLD-TME ( EM = YYYYMMDDHHIISS ) TO #CHK-DTETME
                        if (condition(pnd_Chk_Dtetme_Pnd_Chk_Date.equals(pnd_Input_Fields_Pnd_Input_From)))                                                               //Natural: IF #CHK-DATE EQ #INPUT-FROM
                        {
                            pnd_Accept_Reg.setValue("Y");                                                                                                                 //Natural: ASSIGN #ACCEPT-REG := 'Y'
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(cwf_Mcss_Calls_Contact_Id_Cde.notEquals(pnd_Prev_Id) && pnd_Printed_Id.equals(pnd_Prev_Id)))                                        //Natural: IF CWF-MCSS-CALLS.CONTACT-ID-CDE NE #PREV-ID AND #PRINTED-ID EQ #PREV-ID
                        {
                            pnd_Printed_Id.setValue(" ");                                                                                                                 //Natural: ASSIGN #PRINTED-ID := ' '
                            getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),pnd_Blank);                                                                      //Natural: WRITE ( 3 ) NOTITLE 1T #BLANK
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_CONTACT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_CONTACT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(cwf_Mcss_Calls_Error_Msg_Txt.notEquals(" ") || cwf_Mcss_Calls_Contact_Id_Cde.equals(pnd_Prev_Id)))                                  //Natural: IF CWF-MCSS-CALLS.ERROR-MSG-TXT NE ' ' OR CWF-MCSS-CALLS.CONTACT-ID-CDE = #PREV-ID
                        {
                            pnd_Printed_Id.setValue(cwf_Mcss_Calls_Contact_Id_Cde);                                                                                       //Natural: ASSIGN #PRINTED-ID := CWF-MCSS-CALLS.CONTACT-ID-CDE
                            //*  PIN-EXP
                            getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),cwf_Mcss_Calls_Contact_Id_Cde,new TabSetting(12),cwf_Mcss_Calls_Contact_Pls,new  //Natural: WRITE ( 3 ) NOTITLE 1T CWF-MCSS-CALLS.CONTACT-ID-CDE 12T CWF-MCSS-CALLS.CONTACT-PLS 19T CWF-MCSS-CALLS.BTCH-NBR ( AD = L ) 29T CWF-MCSS-CALLS.PIN-NBR ( AD = L ) 43T CWF-MCSS-CALLS.RCRD-UPLD-TME ( EM = MM/DD/YY' 'HH:II:SS ) 62T CWF-MCSS-CALLS.ERROR-MSG-TXT ( AL = 75 )
                                TabSetting(19),cwf_Mcss_Calls_Btch_Nbr, new FieldAttributes ("AD=L"),new TabSetting(29),cwf_Mcss_Calls_Pin_Nbr, new FieldAttributes 
                                ("AD=L"),new TabSetting(43),cwf_Mcss_Calls_Rcrd_Upld_Tme, new ReportEditMask ("MM/DD/YY' 'HH:II:SS"),new TabSetting(62),cwf_Mcss_Calls_Error_Msg_Txt, 
                                new AlphanumericLength (75));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_CONTACT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_CONTACT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*        38T CWF-MCSS-CALLS.RCRD-UPLD-TME (EM=MM/DD/YY' 'HH:II:SS)
                            //*        57T CWF-MCSS-CALLS.ERROR-MSG-TXT (AL=75)
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Prev_Id.setValue(cwf_Mcss_Calls_Contact_Id_Cde);                                                                                              //Natural: ASSIGN #PREV-ID := CWF-MCSS-CALLS.CONTACT-ID-CDE
                        //*  (READ-CONTACT)
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_I.less(pnd_Cnt)))                                                                                                                   //Natural: IF #I LT #CNT
                    {
                        getReports().newPage(new ReportSpecification(3));                                                                                                 //Natural: NEWPAGE ( 3 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().getPageNumberDbs(3).reset();                                                                                                         //Natural: RESET *PAGE-NUMBER ( 3 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //* ********************       R E P O R T   4      ***********************
                FOR03:                                                                                                                                                    //Natural: FOR #I = 1 TO #CNT
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Cnt)); pnd_I.nadd(1))
                {
                    pnd_Cwf_Mcss_Key_Systm_Ind.setValue(pnd_Contact_Sheet_Systems.getValue(pnd_I));                                                                       //Natural: ASSIGN #CWF-MCSS-KEY.SYSTM-IND := #INPUT-SYS := #CONTACT-SHEET-SYSTEMS ( #I )
                    pnd_Input_Pnd_Input_Sys.setValue(pnd_Contact_Sheet_Systems.getValue(pnd_I));
                    pnd_Cwf_Mcss_Key_Btch_Nbr.setValue(pnd_Input_Pnd_Input_Batch);                                                                                        //Natural: ASSIGN #CWF-MCSS-KEY.BTCH-NBR := #INPUT-BATCH
                    pnd_Cwf_Mcss_Key_Rcrd_Type_Cde.setValue("S");                                                                                                         //Natural: ASSIGN #CWF-MCSS-KEY.RCRD-TYPE-CDE := 'S'
                    pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Fields_Pnd_Input_From);                                        //Natural: MOVE EDITED #INPUT-FROM TO #CWF-MCSS-KEY.RCRD-UPLD-TME ( EM = YYYYMMDD )
                    pnd_Date_Time_Pnd_Date.setValue(pnd_Input_Fields_Pnd_Input_From);                                                                                     //Natural: ASSIGN #DATE := #INPUT-FROM
                    pnd_Date_Time_Pnd_Time.setValue("000000");                                                                                                            //Natural: ASSIGN #TIME := '000000'
                    pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Date_Time);                                                    //Natural: MOVE EDITED #DATE-TIME TO #CWF-MCSS-KEY.RCRD-UPLD-TME ( EM = YYYYMMDDHHIISS )
                    pnd_Count.reset();                                                                                                                                    //Natural: RESET #COUNT #RCOUNT #TOT-DIFF
                    pnd_Rcount.reset();
                    pnd_Tot_Diff.reset();
                    vw_cwf_Mcss_Calls.startDatabaseRead                                                                                                                   //Natural: READ CWF-MCSS-CALLS WITH BATCH-KEY EQ #CWF-MCSS-KEY
                    (
                    "READ_AUDIT2",
                    new Wc[] { new Wc("BATCH_KEY", ">=", pnd_Cwf_Mcss_Key.getBinary(), WcType.BY) },
                    new Oc[] { new Oc("BATCH_KEY", "ASC") }
                    );
                    READ_AUDIT2:
                    while (condition(vw_cwf_Mcss_Calls.readNextRow("READ_AUDIT2")))
                    {
                        if (condition(cwf_Mcss_Calls_Systm_Ind.notEquals(pnd_Input_Pnd_Input_Sys)))                                                                       //Natural: IF CWF-MCSS-CALLS.SYSTM-IND NE #INPUT-SYS
                        {
                            if (true) break READ_AUDIT2;                                                                                                                  //Natural: ESCAPE BOTTOM ( READ-AUDIT2. ) IMMEDIATE
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Chk_Dtetme.setValueEdited(cwf_Mcss_Calls_Rcrd_Upld_Tme,new ReportEditMask("YYYYMMDDHHIISS"));                                                 //Natural: MOVE EDITED CWF-MCSS-CALLS.RCRD-UPLD-TME ( EM = YYYYMMDDHHIISS ) TO #CHK-DTETME
                        if (condition(pnd_Chk_Dtetme_Pnd_Chk_Date.notEquals(pnd_Input_Fields_Pnd_Input_From)))                                                            //Natural: IF #CHK-DATE NE #INPUT-FROM
                        {
                            if (true) break READ_AUDIT2;                                                                                                                  //Natural: ESCAPE BOTTOM ( READ-AUDIT2. ) IMMEDIATE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Input_Pnd_Input_Batch.notEquals(getZero())))                                                                                    //Natural: IF #INPUT-BATCH NE 0
                        {
                            if (condition(cwf_Mcss_Calls_Btch_Nbr.equals(pnd_Input_Pnd_Input_Batch)))                                                                     //Natural: IF CWF-MCSS-CALLS.BTCH-NBR = #INPUT-BATCH
                            {
                                pnd_Accepted.setValue("Y");                                                                                                               //Natural: ASSIGN #ACCEPTED := 'Y'
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Accepted.setValue("N");                                                                                                               //Natural: ASSIGN #ACCEPTED := 'N'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Accepted.setValue("Y");                                                                                                                   //Natural: ASSIGN #ACCEPTED := 'Y'
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(cwf_Mcss_Calls_Rcrd_Type_Cde.equals("S")))                                                                                          //Natural: IF CWF-MCSS-CALLS.RCRD-TYPE-CDE EQ 'S'
                        {
                            if (condition(pnd_Accepted.equals("Y")))                                                                                                      //Natural: IF #ACCEPTED = 'Y'
                            {
                                pnd_Count.nadd(1);                                                                                                                        //Natural: ADD 1 TO #COUNT
                                if (condition(cwf_Mcss_Calls_Cntct_Total_Nbr.equals(1)))                                                                                  //Natural: IF CWF-MCSS-CALLS.CNTCT-TOTAL-NBR = 1
                                {
                                    pnd_Rcount.nadd(1);                                                                                                                   //Natural: ADD 1 TO #RCOUNT
                                }                                                                                                                                         //Natural: END-IF
                                pnd_Success.compute(new ComputeParameters(false, pnd_Success), cwf_Mcss_Calls_Cntct_Total_Nbr.subtract(cwf_Mcss_Calls_Cntct_Error_Nbr));  //Natural: ASSIGN #SUCCESS := CWF-MCSS-CALLS.CNTCT-TOTAL-NBR - CWF-MCSS-CALLS.CNTCT-ERROR-NBR
                                getReports().write(4, ReportOption.NOTITLE,new TabSetting(2),cwf_Mcss_Calls_Btch_Nbr, new FieldAttributes ("AD=L"),new                    //Natural: WRITE ( 4 ) NOTITLE 002T CWF-MCSS-CALLS.BTCH-NBR ( AD = L ) 015T CWF-MCSS-CALLS.CNTCT-TOTAL-NBR 026T #SUCCESS 038T CWF-MCSS-CALLS.CNTCT-ERROR-NBR 048T CWF-MCSS-CALLS.WPIDS-TOTAL-NBR 061T CWF-MCSS-CALLS.LINES-TOTAL-NBR 072T CWF-MCSS-CALLS.RCRD-STAMP-TME ( EM = MM/DD/YYYY' 'HH:II:SS:T ) 097T CWF-MCSS-CALLS.CALL-STAMP-TME ( EM = MM/DD/YYYY' 'HH:II:SS:T )
                                    TabSetting(15),cwf_Mcss_Calls_Cntct_Total_Nbr,new TabSetting(26),pnd_Success,new TabSetting(38),cwf_Mcss_Calls_Cntct_Error_Nbr,new 
                                    TabSetting(48),cwf_Mcss_Calls_Wpids_Total_Nbr,new TabSetting(61),cwf_Mcss_Calls_Lines_Total_Nbr,new TabSetting(72),cwf_Mcss_Calls_Rcrd_Stamp_Tme, 
                                    new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS:T"),new TabSetting(97),cwf_Mcss_Calls_Call_Stamp_Tme, new ReportEditMask 
                                    ("MM/DD/YYYY' 'HH:II:SS:T"));
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("READ_AUDIT2"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("READ_AUDIT2"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(cwf_Mcss_Calls_Error_Msg_Txt.notEquals(" ")))                                                                               //Natural: IF CWF-MCSS-CALLS.ERROR-MSG-TXT NE ' '
                                {
                                    getReports().write(4, ReportOption.NOTITLE,cwf_Mcss_Calls_Error_Msg_Txt, new AlphanumericLength (70));                                //Natural: WRITE ( 4 ) NOTITLE CWF-MCSS-CALLS.ERROR-MSG-TXT ( AL = 70 )
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom("READ_AUDIT2"))) break;
                                        else if (condition(Global.isEscapeBottomImmediate("READ_AUDIT2"))) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().skip(4, 2);                                                                                                                              //Natural: SKIP ( 4 ) 2
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(2),"NUMBER OF ENTRIES:",pnd_Count,new TabSetting(28),"RUSH :",pnd_Rcount);                  //Natural: WRITE ( 4 ) NOTITLE 2T 'NUMBER OF ENTRIES:' #COUNT 28T 'RUSH :' #RCOUNT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_I.less(pnd_Cnt)))                                                                                                                   //Natural: IF #I LT #CNT
                    {
                        getReports().newPage(new ReportSpecification(4));                                                                                                 //Natural: NEWPAGE ( 4 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().getPageNumberDbs(4).reset();                                                                                                         //Natural: RESET *PAGE-NUMBER ( 4 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 2 )
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 3 )
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 4 )
                //*  IF #INPUT-FROM-D = *DATX OR
                //*     #INPUT-FROM-D > *DATX
                pnd_Last_Processed_Date_A.setValue(pnd_Input_Fields_Pnd_Input_From);                                                                                      //Natural: ASSIGN #LAST-PROCESSED-DATE-A := #INPUT-FROM
                                                                                                                                                                          //Natural: PERFORM UPDATE-NEXT-PROCESSING-DATE
                sub_Update_Next_Processing_Date();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  ELSE
                pnd_Input_From_D.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #INPUT-FROM-D
                pnd_Input_Fields_Pnd_Input_From.setValueEdited(pnd_Input_From_D,new ReportEditMask("YYYYMMDD"));                                                          //Natural: MOVE EDITED #INPUT-FROM-D ( EM = YYYYMMDD ) TO #INPUT-FROM
                Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Fields);                                                                                            //Natural: FETCH 'MCSP1800' #INPUT-FIELDS
                Global.setFetchProgram(DbsUtil.getBlType("MCSP1800"));
                if (condition(Global.isEscape())) return;
                //*  END-IF
                //* **********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-NEXT-PROCESSING-DATE
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ALL-CONTACT-SHEET-SYSTEMS
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-INPUT-PARAMETERS
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL-LINE
                //*    52T CWF-MCSS-CALLS.CALL-STAMP-TME (EM=MM/DD/YYYY' 'HH:II:SS)
                //*    73T CWF-MCSS-CALLS.RCRD-UPLD-TME  (EM=MM/DD/YYYY' 'HH:II:SS)
                //*    94T CWF-MCSS-CALLS.RQST-ENTRY-OP-CDE
                //*    104T #NO-SIGN
                //*    108T CWF-MCSS-CALLS.RQST-ORIGIN-UNIT-CDE
                //*    118T CWF-MCSS-CALLS.WPIDS-NBR (EM=ZZZ9)
                //*    127T CWF-MCSS-CALLS.LINES-NBR (EM=ZZZ9)
                //*    52T CWF-MCSS-CALLS.CALL-STAMP-TME (EM=MM/DD/YYYY' 'HH:II:SS)
                //*    73T CWF-MCSS-CALLS.RCRD-UPLD-TME  (EM=MM/DD/YYYY' 'HH:II:SS)
                //*    94T CWF-MCSS-CALLS.RQST-ENTRY-OP-CDE
                //*    104T #NO-SIGN
                //*    108T CWF-MCSS-CALLS.RQST-ORIGIN-UNIT-CDE
                //*    118T CWF-MCSS-CALLS.WPIDS-NBR (EM=ZZZ9)
                //*    127T CWF-MCSS-CALLS.LINES-NBR (EM=ZZZ9)
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-BATCH-SUMMARY
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTAL-TOTALS
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Update_Next_Processing_Date() throws Exception                                                                                                       //Natural: UPDATE-NEXT-PROCESSING-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //*  UPDATE TABLE FILE WITH THE NEXT PROCESSING DATE; ALSO IF FIRST
        //*  WORKING DAY OF THE MONTH, UPDATE THE REGULAR TIME PARAMETERS
        //*  WITH AN OVERRIDE TIME PARAMETERS IF IT IS ENTERED.
        pnd_Last_Processed_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Last_Processed_Date_A);                                                                 //Natural: MOVE EDITED #LAST-PROCESSED-DATE-A TO #LAST-PROCESSED-DATE ( EM = YYYYMMDD )
        pnd_Last_Processed_Date_Mm.setValueEdited(pnd_Last_Processed_Date,new ReportEditMask("MM"));                                                                      //Natural: MOVE EDITED #LAST-PROCESSED-DATE ( EM = MM ) TO #LAST-PROCESSED-DATE-MM
        //*  #ORIG-DATE := #INPUT-DTE-D
        pnd_Isn_Tbl.setValue(pnd_Input_Fields_Pnd_Isn_Number);                                                                                                            //Natural: MOVE #ISN-NUMBER TO #ISN-TBL
        PND_PND_L5040:                                                                                                                                                    //Natural: GET CWF-SUPPORT-TBL #ISN-TBL
        vw_cwf_Support_Tbl.readByID(pnd_Isn_Tbl.getLong(), "PND_PND_L5040");
        DbsUtil.callnat(Mcsn3810.class , getCurrentProcessState(), pnd_Last_Processed_Date, pnd_Next_Processing_Date);                                                    //Natural: CALLNAT 'MCSN3810' #LAST-PROCESSED-DATE #NEXT-PROCESSING-DATE
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Next_Processing_Date.greater(getZero())))                                                                                                       //Natural: IF #NEXT-PROCESSING-DATE GT 0
        {
            cwf_Support_Tbl_Pnd_Regular_Run_Date.setValueEdited(pnd_Next_Processing_Date,new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED #NEXT-PROCESSING-DATE ( EM = YYYYMMDD ) TO CWF-SUPPORT-TBL.#REGULAR-RUN-DATE
            cwf_Support_Tbl_Pnd_Prior_Date_Parm.setValueEdited(pnd_Last_Processed_Date,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED #LAST-PROCESSED-DATE ( EM = YYYYMMDD ) TO CWF-SUPPORT-TBL.#PRIOR-DATE-PARM
        }                                                                                                                                                                 //Natural: END-IF
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setValue("BATCH");                                                                                                            //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE := 'BATCH'
        cwf_Support_Tbl_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                         //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-UPDTE-DTE := *DATX
        //*  UPDATE (L-GET.)
        vw_cwf_Support_Tbl.updateDBRow("PND_PND_L5040");                                                                                                                  //Natural: UPDATE ( ##L5040. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Get_All_Contact_Sheet_Systems() throws Exception                                                                                                     //Natural: GET-ALL-CONTACT-SHEET-SYSTEMS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Authrty.setValue("S");                                                                                                                //Natural: ASSIGN #GEN-TBL-AUTHRTY := 'S'
        pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Name.setValue("CWF-CNTCT-SHT-SYSTMS");                                                                                                //Natural: ASSIGN #GEN-TBL-NAME := 'CWF-CNTCT-SHT-SYSTMS'
        pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Code.reset();                                                                                                                         //Natural: RESET #GEN-TBL-CODE #CNT
        pnd_Cnt.reset();
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY FROM #GEN-TBL-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Gen_Tbl_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ01")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Scrty_Level_Ind.notEquals("S") || cwf_Support_Tbl_Tbl_Table_Nme.notEquals("CWF-CNTCT-SHT-SYSTMS")))                         //Natural: IF TBL-SCRTY-LEVEL-IND NE 'S' OR TBL-TABLE-NME NE 'CWF-CNTCT-SHT-SYSTMS'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CNT
            pnd_Contact_Sheet_Systems.getValue(pnd_Cnt).setValue(cwf_Support_Tbl_Tbl_Key_Field);                                                                          //Natural: ASSIGN #CONTACT-SHEET-SYSTEMS ( #CNT ) := TBL-KEY-FIELD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Cnt.equals(getZero())))                                                                                                                         //Natural: IF #CNT = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"*",new RepeatItem(70),NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new  //Natural: WRITE ( 1 ) NOTITLE // 30T '*' ( 70 ) / 30T '*' 99T '*' / 30T '* NO SYSTEM IS SETUP IN MIT SUPPORT TABLE.' 99T '*' / 30T '*' 99T '*' / 30T '*' ( 70 )
                TabSetting(30),"* NO SYSTEM IS SETUP IN MIT SUPPORT TABLE.",new TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new 
                TabSetting(30),"*",new RepeatItem(70));
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Input_Parameters() throws Exception                                                                                                            //Natural: CHECK-INPUT-PARAMETERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(! (DbsUtil.maskMatches(pnd_Input_Fields_Pnd_Input_From,"19-20YYMMDD"))))                                                                            //Natural: IF #INPUT-FROM NE MASK ( 19-20YYMMDD )
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"*",new RepeatItem(70),NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new  //Natural: WRITE ( 1 ) // 30T '*' ( 70 ) / 30T '*' 99T '*' / 30T '* INVALID INPUT DATE (YYYYMMDD) =' #INPUT-FROM 99T '*' / 30T '*' 99T '*' / 30T '*' ( 70 )
                TabSetting(30),"* INVALID INPUT DATE (YYYYMMDD) =",pnd_Input_Fields_Pnd_Input_From,new TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new 
                TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new RepeatItem(70));
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Detail_Line() throws Exception                                                                                                                 //Natural: WRITE-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(cwf_Mcss_Calls_Error_Msg_Txt.equals(" ")))                                                                                                          //Natural: IF CWF-MCSS-CALLS.ERROR-MSG-TXT EQ ' '
        {
            pnd_No_Error.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #NO-ERROR
            pnd_No_Sign.reset();                                                                                                                                          //Natural: RESET #NO-SIGN
            if (condition(cwf_Mcss_Calls_Attn_Txt.equals("Y")))                                                                                                           //Natural: IF CWF-MCSS-CALLS.ATTN-TXT = 'Y'
            {
                pnd_No_Sign.setValue("*");                                                                                                                                //Natural: ASSIGN #NO-SIGN := '*'
                //* PIN-EX
                //* PIN-EX
                //* PIN-EXP
                //* PIN-EXP
                //* PIN-EXP
                //* PIN-EXP
                //* PIN-EXP
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),cwf_Mcss_Calls_Btch_Nbr, new FieldAttributes ("AD=L"),new TabSetting(12),cwf_Mcss_Calls_Contact_Id_Cde,new  //Natural: WRITE ( 1 ) NOTITLE / 2T CWF-MCSS-CALLS.BTCH-NBR ( AD = L ) 12T CWF-MCSS-CALLS.CONTACT-ID-CDE 23T CWF-MCSS-CALLS.CONTACT-PLS 30T CWF-MCSS-CALLS.PH-SSN-NBR ( EM = 999-99-9999 ) 43T CWF-MCSS-CALLS.PIN-NBR ( AD = L ) 57T CWF-MCSS-CALLS.CALL-STAMP-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 78T CWF-MCSS-CALLS.RCRD-UPLD-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 99T CWF-MCSS-CALLS.RQST-ENTRY-OP-CDE 109T #NO-SIGN 113T CWF-MCSS-CALLS.RQST-ORIGIN-UNIT-CDE 123T CWF-MCSS-CALLS.WPIDS-NBR ( EM = ZZZ9 ) 132T CWF-MCSS-CALLS.LINES-NBR ( EM = ZZZ9 )
                TabSetting(23),cwf_Mcss_Calls_Contact_Pls,new TabSetting(30),cwf_Mcss_Calls_Ph_Ssn_Nbr, new ReportEditMask ("999-99-9999"),new TabSetting(43),cwf_Mcss_Calls_Pin_Nbr, 
                new FieldAttributes ("AD=L"),new TabSetting(57),cwf_Mcss_Calls_Call_Stamp_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(78),cwf_Mcss_Calls_Rcrd_Upld_Tme, 
                new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(99),cwf_Mcss_Calls_Rqst_Entry_Op_Cde,new TabSetting(109),pnd_No_Sign,new TabSetting(113),cwf_Mcss_Calls_Rqst_Origin_Unit_Cde,new 
                TabSetting(123),cwf_Mcss_Calls_Wpids_Nbr, new ReportEditMask ("ZZZ9"),new TabSetting(132),cwf_Mcss_Calls_Lines_Nbr, new ReportEditMask ("ZZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* PIN-EX
            //* PIN-EX
            //* PIN-EXP
            //* PIN-EXP
            //* PIN-EXP
            //* PIN-EXP
            //* PIN-EXP
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),cwf_Mcss_Calls_Btch_Nbr, new FieldAttributes ("AD=L"),new TabSetting(12),cwf_Mcss_Calls_Contact_Id_Cde,new  //Natural: WRITE ( 2 ) NOTITLE / 2T CWF-MCSS-CALLS.BTCH-NBR ( AD = L ) 12T CWF-MCSS-CALLS.CONTACT-ID-CDE 23T CWF-MCSS-CALLS.CONTACT-PLS 30T CWF-MCSS-CALLS.PH-SSN-NBR ( EM = 999-99-9999 ) 43T CWF-MCSS-CALLS.PIN-NBR ( AD = L ) 57T CWF-MCSS-CALLS.CALL-STAMP-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 78T CWF-MCSS-CALLS.RCRD-UPLD-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 99T CWF-MCSS-CALLS.RQST-ENTRY-OP-CDE 109T #NO-SIGN 113T CWF-MCSS-CALLS.RQST-ORIGIN-UNIT-CDE 123T CWF-MCSS-CALLS.WPIDS-NBR ( EM = ZZZ9 ) 132T CWF-MCSS-CALLS.LINES-NBR ( EM = ZZZ9 )
                TabSetting(23),cwf_Mcss_Calls_Contact_Pls,new TabSetting(30),cwf_Mcss_Calls_Ph_Ssn_Nbr, new ReportEditMask ("999-99-9999"),new TabSetting(43),cwf_Mcss_Calls_Pin_Nbr, 
                new FieldAttributes ("AD=L"),new TabSetting(57),cwf_Mcss_Calls_Call_Stamp_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(78),cwf_Mcss_Calls_Rcrd_Upld_Tme, 
                new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new TabSetting(99),cwf_Mcss_Calls_Rqst_Entry_Op_Cde,new TabSetting(109),pnd_No_Sign,new TabSetting(113),cwf_Mcss_Calls_Rqst_Origin_Unit_Cde,new 
                TabSetting(123),cwf_Mcss_Calls_Wpids_Nbr, new ReportEditMask ("ZZZ9"),new TabSetting(132),cwf_Mcss_Calls_Lines_Nbr, new ReportEditMask ("ZZZ9"));
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(11),cwf_Mcss_Calls_Error_Msg_Txt);                                                                  //Natural: WRITE ( 2 ) NOTITLE 11T CWF-MCSS-CALLS.ERROR-MSG-TXT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Batch_Summary() throws Exception                                                                                                             //Natural: PROCESS-BATCH-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cwf_Mcss_Skey_Rcrd_Type_Cde.setValue("S");                                                                                                                    //Natural: ASSIGN #CWF-MCSS-SKEY.RCRD-TYPE-CDE := 'S'
        pnd_Cwf_Mcss_Skey_Btch_Nbr.setValue(pnd_Prev_Btch);                                                                                                               //Natural: ASSIGN #CWF-MCSS-SKEY.BTCH-NBR := #PREV-BTCH
        pnd_Cwf_Mcss_Skey_Contact_Id_Cde.setValue("9999999999");                                                                                                          //Natural: ASSIGN #CWF-MCSS-SKEY.CONTACT-ID-CDE := '9999999999'
        pnd_Cwf_Mcss_Skey_Systm_Ind.setValue(pnd_Input_Pnd_Input_Sys);                                                                                                    //Natural: ASSIGN #CWF-MCSS-SKEY.SYSTM-IND := #INPUT-SYS
        pnd_Cwf_Mcss_Skey_Rcrd_Upld_Tme.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Fields_Pnd_Input_From);                                                   //Natural: MOVE EDITED #INPUT-FROM TO #CWF-MCSS-SKEY.RCRD-UPLD-TME ( EM = YYYYMMDD )
        vw_cwf_Mcss_Smry.startDatabaseFind                                                                                                                                //Natural: FIND CWF-MCSS-SMRY WITH BATCH-KEY = #CWF-MCSS-SKEY
        (
        "FIND01",
        new Wc[] { new Wc("BATCH_KEY", "=", pnd_Cwf_Mcss_Skey.getBinary(), WcType.WITH) }
        );
        FIND01:
        while (condition(vw_cwf_Mcss_Smry.readNextRow("FIND01")))
        {
            vw_cwf_Mcss_Smry.setIfNotFoundControlFlag(false);
            pnd_Line_Pnd_Tot_Upld.nadd(cwf_Mcss_Smry_Cntct_Total_Nbr);                                                                                                    //Natural: ASSIGN #TOT-UPLD := #TOT-UPLD + CWF-MCSS-SMRY.CNTCT-TOTAL-NBR
            pnd_Line_Pnd_Tot_Error.nadd(cwf_Mcss_Smry_Cntct_Error_Nbr);                                                                                                   //Natural: ASSIGN #TOT-ERROR := #TOT-ERROR + CWF-MCSS-SMRY.CNTCT-ERROR-NBR
            pnd_Line_Pnd_Tot_Lines.nadd(cwf_Mcss_Smry_Lines_Total_Nbr);                                                                                                   //Natural: ASSIGN #TOT-LINES := #TOT-LINES + CWF-MCSS-SMRY.LINES-TOTAL-NBR
            pnd_Line_Pnd_Tot_Wpid.nadd(cwf_Mcss_Smry_Wpids_Total_Nbr);                                                                                                    //Natural: ASSIGN #TOT-WPID := #TOT-WPID + CWF-MCSS-SMRY.WPIDS-TOTAL-NBR
            pnd_Line_Pnd_Btch_Success.compute(new ComputeParameters(false, pnd_Line_Pnd_Btch_Success), cwf_Mcss_Smry_Cntct_Total_Nbr.subtract(cwf_Mcss_Smry_Cntct_Error_Nbr)); //Natural: COMPUTE #BTCH-SUCCESS = CWF-MCSS-SMRY.CNTCT-TOTAL-NBR - CWF-MCSS-SMRY.CNTCT-ERROR-NBR
            if (condition(pnd_No_Error.greater(getZero())))                                                                                                               //Natural: IF #NO-ERROR GT 0
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(1),"BATCH SUMMARY :",new TabSetting(29),pnd_Prev_Btch);                         //Natural: WRITE ( 1 ) NOTITLE // 1T 'BATCH SUMMARY :' 29T #PREV-BTCH
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"SUCCESSFUL UPLOADS  :",new TabSetting(35),pnd_Line_Pnd_Btch_Success);              //Natural: WRITE ( 1 ) NOTITLE / 10T 'SUCCESSFUL UPLOADS  :' 35T #BTCH-SUCCESS
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"TOTAL UPLOADS       :",new TabSetting(37),cwf_Mcss_Smry_Cntct_Total_Nbr);          //Natural: WRITE ( 1 ) NOTITLE / 10T 'TOTAL UPLOADS       :' 37T CWF-MCSS-SMRY.CNTCT-TOTAL-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"WPID'S UPLOADED     :",new TabSetting(35),cwf_Mcss_Smry_Wpids_Total_Nbr);          //Natural: WRITE ( 1 ) NOTITLE / 10T 'WPID"S UPLOADED     :' 35T CWF-MCSS-SMRY.WPIDS-TOTAL-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"TEXT LINES UPLOADED :",new TabSetting(35),cwf_Mcss_Smry_Lines_Total_Nbr);          //Natural: WRITE ( 1 ) NOTITLE / 10T 'TEXT LINES UPLOADED :' 35T CWF-MCSS-SMRY.LINES-TOTAL-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Error_Cnt.greater(getZero())))                                                                                                              //Natural: IF #ERROR-CNT GT 0
            {
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(1),"BATCH SUMMARY :",new TabSetting(29),pnd_Prev_Btch);                         //Natural: WRITE ( 2 ) NOTITLE // 1T 'BATCH SUMMARY :' 29T #PREV-BTCH
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"ERROR UPLOADS       :",new TabSetting(37),cwf_Mcss_Smry_Cntct_Error_Nbr);          //Natural: WRITE ( 2 ) NOTITLE / 10T 'ERROR UPLOADS       :' 37T CWF-MCSS-SMRY.CNTCT-ERROR-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"TOTAL UPLOADS       :",new TabSetting(35),cwf_Mcss_Smry_Cntct_Total_Nbr);          //Natural: WRITE ( 2 ) NOTITLE / 10T 'TOTAL UPLOADS       :' 35T CWF-MCSS-SMRY.CNTCT-TOTAL-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_No_Error.setValue(0);                                                                                                                                     //Natural: ASSIGN #NO-ERROR := 0
            pnd_Error_Cnt.setValue(0);                                                                                                                                    //Natural: ASSIGN #ERROR-CNT := 0
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Write_Total_Totals() throws Exception                                                                                                                //Natural: WRITE-TOTAL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Line_Pnd_Tot_Success.compute(new ComputeParameters(false, pnd_Line_Pnd_Tot_Success), pnd_Line_Pnd_Tot_Upld.subtract(pnd_Line_Pnd_Tot_Error));                 //Natural: COMPUTE #TOT-SUCCESS = #TOT-UPLD - #TOT-ERROR
        if (condition(pnd_Input_Pnd_Input_Batch.equals(getZero())))                                                                                                       //Natural: IF #INPUT-BATCH EQ 0
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"SUMMARY OF CONTACT SHEET UPLOAD CONTROL FOR",new TabSetting(76),pnd_Input_From_D,  //Natural: WRITE ( 2 ) NOTITLE // 30T 'SUMMARY OF CONTACT SHEET UPLOAD CONTROL FOR' 76T #INPUT-FROM-D ( EM = MM/DD/YYYY )
                new ReportEditMask ("MM/DD/YYYY"));
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"TOTAL SUCCESSFUL    :",new TabSetting(35),pnd_Line_Pnd_Tot_Success);                   //Natural: WRITE ( 2 ) NOTITLE / 10T 'TOTAL SUCCESSFUL    :' 35T #TOT-SUCCESS
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"TOTAL ERRORS        :",new TabSetting(35),pnd_Line_Pnd_Tot_Error);                     //Natural: WRITE ( 2 ) NOTITLE / 10T 'TOTAL ERRORS        :' 35T #TOT-ERROR
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(35),"------------");                                                                        //Natural: WRITE ( 2 ) NOTITLE / 35T '------------'
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(10),"TOTAL UPLOADS       :",new TabSetting(35),pnd_Line_Pnd_Tot_Upld);                              //Natural: WRITE ( 2 ) NOTITLE 10T 'TOTAL UPLOADS       :' 35T #TOT-UPLD
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(35),"------------");                                                                                //Natural: WRITE ( 2 ) NOTITLE 35T '------------'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"SUMMARY OF CONTACT SHEET UPLOAD CONTROL FOR",new TabSetting(76),pnd_Input_From_D,  //Natural: WRITE ( 1 ) NOTITLE // 30T 'SUMMARY OF CONTACT SHEET UPLOAD CONTROL FOR' 76T #INPUT-FROM-D ( EM = MM/DD/YYYY )
                new ReportEditMask ("MM/DD/YYYY"));
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"TOTAL SUCCESSFUL    :",new TabSetting(35),pnd_Line_Pnd_Tot_Success);                   //Natural: WRITE ( 1 ) NOTITLE / 10T 'TOTAL SUCCESSFUL    :' 35T #TOT-SUCCESS
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"TOTAL ERRORS        :",new TabSetting(35),pnd_Line_Pnd_Tot_Error);                     //Natural: WRITE ( 1 ) NOTITLE / 10T 'TOTAL ERRORS        :' 35T #TOT-ERROR
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(35),"------------");                                                                        //Natural: WRITE ( 1 ) NOTITLE / 35T '------------'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(10),"TOTAL UPLOADS       :",new TabSetting(35),pnd_Line_Pnd_Tot_Upld);                              //Natural: WRITE ( 1 ) NOTITLE 10T 'TOTAL UPLOADS       :' 35T #TOT-UPLD
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(35),"------------");                                                                                //Natural: WRITE ( 1 ) NOTITLE 35T '------------'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(10),"WPID'S UPLOADED     :",new TabSetting(35),pnd_Line_Pnd_Tot_Wpid);              //Natural: WRITE ( 1 ) NOTITLE // 10T 'WPID"S UPLOADED     :' 35T #TOT-WPID
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"TEXT LINES UPLOADED :",new TabSetting(35),pnd_Line_Pnd_Tot_Lines);                     //Natural: WRITE ( 1 ) NOTITLE / 10T 'TEXT LINES UPLOADED :' 35T #TOT-LINES
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"END OF BATCH REPORT");                                                                            //Natural: WRITE ( 1 ) NOTITLE // 'END OF BATCH REPORT'
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"END OF BATCH REPORT");                                                                            //Natural: WRITE ( 2 ) NOTITLE // 'END OF BATCH REPORT'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Line_Pnd_Tot_Success.reset();                                                                                                                                 //Natural: RESET #TOT-SUCCESS #TOT-ERROR #TOT-UPLD #TOT-WPID #TOT-LINES
        pnd_Line_Pnd_Tot_Error.reset();
        pnd_Line_Pnd_Tot_Upld.reset();
        pnd_Line_Pnd_Tot_Wpid.reset();
        pnd_Line_Pnd_Tot_Lines.reset();
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Mcsf1500.class));                                              //Natural: WRITE ( 1 ) NOTITLE NOHDR USING FORM 'MCSF1500'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Mcsf1501.class));                                              //Natural: WRITE ( 2 ) NOTITLE NOHDR USING FORM 'MCSF1501'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Mcsf1502.class));                                              //Natural: WRITE ( 3 ) NOTITLE NOHDR USING FORM 'MCSF1502'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),pnd_Program,new TabSetting(50),"CORPORATE WORKFLOW SYSTEM",new TabSetting(101),Global.getDATU(),new  //Natural: WRITE ( 4 ) NOTITLE 001T #PROGRAM 050T 'CORPORATE WORKFLOW SYSTEM' 101T *DATU 110T *TIME ( AL = 005 ) 121T 'PAGE:' 127T *PAGE-NUMBER ( 4 ) ( AD = L ) / 041T 'CWF UPDATE BATCH SUMMARY REPORT FOR:' 078T #INPUT-FROM-D ( AD = L EM = MM/DD/YYYY ) / 050T 'SYSTEM :' 059T #INPUT-SYS / 001T '-' ( 131 ) / 049T 'NO. OF' 062T 'NO. OF' / 004T 'BATCH' 015T 'NUMBER' 025T 'SUCCESS' 037T 'ERRORS' 049T 'ITEMS' 062T 'LINES' 077T 'BATCH START' 102T 'BATCH END' / 001T '-' ( 131 ) /
                        TabSetting(110),Global.getTIME(), new AlphanumericLength (5),new TabSetting(121),"PAGE:",new TabSetting(127),getReports().getPageNumberDbs(4), 
                        new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(41),"CWF UPDATE BATCH SUMMARY REPORT FOR:",new TabSetting(78),pnd_Input_From_D, 
                        new FieldAttributes ("AD=L"), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(50),"SYSTEM :",new TabSetting(59),pnd_Input_Pnd_Input_Sys,NEWLINE,new 
                        TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(49),"NO. OF",new TabSetting(62),"NO. OF",NEWLINE,new TabSetting(4),"BATCH",new 
                        TabSetting(15),"NUMBER",new TabSetting(25),"SUCCESS",new TabSetting(37),"ERRORS",new TabSetting(49),"ITEMS",new TabSetting(62),"LINES",new 
                        TabSetting(77),"BATCH START",new TabSetting(102),"BATCH END",NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Mcss_Calls_Systm_IndIsBreak = cwf_Mcss_Calls_Systm_Ind.isBreak(endOfData);
        if (condition(cwf_Mcss_Calls_Systm_IndIsBreak))
        {
            pnd_Input_Pnd_Input_Sys.setValue(sort01Systm_IndOld);                                                                                                         //Natural: ASSIGN #INPUT-SYS := OLD ( CWF-MCSS-CALLS.SYSTM-IND )
                                                                                                                                                                          //Natural: PERFORM PROCESS-BATCH-SUMMARY
            sub_Process_Batch_Summary();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTAL-TOTALS
            sub_Write_Total_Totals();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Input_Pnd_Input_Sys.setValue(cwf_Mcss_Calls_Systm_Ind);                                                                                                   //Natural: ASSIGN #INPUT-SYS := CWF-MCSS-CALLS.SYSTM-IND
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().getPageNumberDbs(1).reset();                                                                                                                     //Natural: RESET *PAGE-NUMBER ( 1 )
            getReports().getPageNumberDbs(2).reset();                                                                                                                     //Natural: RESET *PAGE-NUMBER ( 2 )
            pnd_First_Rec.resetInitial();                                                                                                                                 //Natural: RESET INITIAL #FIRST-REC
            pnd_No_Error.reset();                                                                                                                                         //Natural: RESET #NO-ERROR #ERROR-CNT
            pnd_Error_Cnt.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=OFF");
        Global.format(2, "LS=133 PS=60 ZP=OFF");
        Global.format(3, "LS=133 PS=60 ZP=OFF");
        Global.format(4, "LS=133 PS=60 ZP=OFF");
    }
}
