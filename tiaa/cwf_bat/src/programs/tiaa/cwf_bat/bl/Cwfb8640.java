/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:46:34 PM
**        * FROM NATURAL PROGRAM : Cwfb8640
************************************************************
**        * FILE NAME            : Cwfb8640.java
**        * CLASS NAME           : Cwfb8640
**        * INSTANCE NAME        : Cwfb8640
************************************************************
************************************************************************
* SYSTEM   : CORPORATE WORKFLOW (CWF)
* PROGRAM  : CWFB8640
* TITLE    : PENDING/AGING ATA & LIFE INSURANCE DISBURSEMENT REPORT
* FUNCTION : READ CWF-WHO-WORK-REQUEST (50/60) TO CREATE
*          : PENDING CWF WORK REQUEST DETAILS EXTRACT
* HISTORY  : CREATED ON JUL 1, 2015 - VINODHKUMAR RAMACHANDRAN
* 02/23/2017 - MONDALP - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8640 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private PdaErla1000 pdaErla1000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_master;
    private DbsField master_Rqst_Log_Dte_Tme;

    private DbsGroup master__R_Field_1;
    private DbsField master_Pnd_Rqst_Log_Dte;
    private DbsField master_Pnd_Rqst_Log_Tme;
    private DbsField master_Last_Chnge_Oprtr_Cde;
    private DbsField master_Admin_Status_Cde;
    private DbsGroup master_Cntrct_NbrMuGroup;
    private DbsField master_Cntrct_Nbr;
    private DbsField master_Work_Prcss_Id;
    private DbsField master_Pin_Nbr;
    private DbsField master_Status_Cde;
    private DbsField master_Tiaa_Rcvd_Dte;
    private DbsField master_Crprte_Status_Ind;
    private DbsField master_Empl_Oprtr_Cde;
    private DbsField master_Last_Chnge_Invrt_Dte_Tme;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key;

    private DbsGroup pnd_Crprte_Status_Chnge_Dte_Key__R_Field_2;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_Pnd_Crprte_Status_Ind;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_Pnd_Last_Chnge_Invrt_Dte_Tme;
    private DbsField pnd_Last_Chnge_Dte_Tme;

    private DbsGroup pnd_Last_Chnge_Dte_Tme__R_Field_3;
    private DbsField pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte;
    private DbsField pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Tme;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_4;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_Data_Field1;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_5;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;

    private DataAccessProgramView vw_cwf_Wp_Work_Prcss_Id;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Id;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme;
    private DbsField pnd_Wpid_Uniq_Key;

    private DbsGroup pnd_Wpid_Uniq_Key__R_Field_6;
    private DbsField pnd_Wpid_Uniq_Key_Pnd_Wpid;

    private DataAccessProgramView vw_empl_Tbl;
    private DbsField empl_Tbl_Empl_Racf_Id;
    private DbsField empl_Tbl_Empl_Nme;

    private DbsGroup empl_Tbl__R_Field_7;
    private DbsField empl_Tbl_Empl_First_Nme;
    private DbsField empl_Tbl_Empl_Last_Nme;
    private DbsField empl_Tbl_Dlte_Dte_Tme;
    private DbsField empl_Tbl_Dlte_Oprtr_Cde;
    private DbsField pnd_Output_Workfile;

    private DbsGroup pnd_Output_Workfile__R_Field_8;
    private DbsField pnd_Output_Workfile_Pnd_O_Empl_Name;
    private DbsField pnd_Output_Workfile_Pnd_O_Filler1;
    private DbsField pnd_Output_Workfile_Pnd_O_Cntrct_Nbr;
    private DbsField pnd_Output_Workfile_Pnd_O_Filler2;
    private DbsField pnd_Output_Workfile_Pnd_O_Pin_Nbr;
    private DbsField pnd_Output_Workfile_Pnd_O_Filler3;
    private DbsField pnd_Output_Workfile_Pnd_O_Tiaa_Rcvd_Dte;
    private DbsField pnd_Output_Workfile_Pnd_O_Filler5;
    private DbsField pnd_Output_Workfile_Pnd_O_Work_Prcss_Id;
    private DbsField pnd_Output_Workfile_Pnd_O_Filler4;
    private DbsField pnd_Output_Workfile_Pnd_O_Transaction_Type;
    private DbsField pnd_Output_Workfile_Pnd_O_Filler6;
    private DbsField pnd_Output_Workfile_Pnd_O_Clndr_Days_Aging;
    private DbsField pnd_Start_Date;
    private DbsField pnd_End_Date;
    private DbsField pnd_I;
    private DbsField pnd_Rec_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaErla1000 = new PdaErla1000(localVariables);

        // Local Variables

        vw_master = new DataAccessProgramView(new NameInfo("vw_master", "MASTER"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        master_Rqst_Log_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        master_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        master__R_Field_1 = vw_master.getRecord().newGroupInGroup("master__R_Field_1", "REDEFINE", master_Rqst_Log_Dte_Tme);
        master_Pnd_Rqst_Log_Dte = master__R_Field_1.newFieldInGroup("master_Pnd_Rqst_Log_Dte", "#RQST-LOG-DTE", FieldType.NUMERIC, 8);
        master_Pnd_Rqst_Log_Tme = master__R_Field_1.newFieldInGroup("master_Pnd_Rqst_Log_Tme", "#RQST-LOG-TME", FieldType.NUMERIC, 7);
        master_Last_Chnge_Oprtr_Cde = vw_master.getRecord().newFieldInGroup("master_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        master_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        master_Admin_Status_Cde = vw_master.getRecord().newFieldInGroup("master_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "ADMIN_STATUS_CDE");
        master_Cntrct_NbrMuGroup = vw_master.getRecord().newGroupInGroup("MASTER_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CWF_MASTER_INDEX_CNTRCT_NBR");
        master_Cntrct_Nbr = master_Cntrct_NbrMuGroup.newFieldArrayInGroup("master_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8, new DbsArrayController(1, 
            10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        master_Work_Prcss_Id = vw_master.getRecord().newFieldInGroup("master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        master_Pin_Nbr = vw_master.getRecord().newFieldInGroup("master_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        master_Pin_Nbr.setDdmHeader("PIN");
        master_Status_Cde = vw_master.getRecord().newFieldInGroup("master_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        master_Tiaa_Rcvd_Dte = vw_master.getRecord().newFieldInGroup("master_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        master_Crprte_Status_Ind = vw_master.getRecord().newFieldInGroup("master_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_STATUS_IND");
        master_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        master_Empl_Oprtr_Cde = vw_master.getRecord().newFieldInGroup("master_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "EMPL_OPRTR_CDE");
        master_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        master_Last_Chnge_Invrt_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Last_Chnge_Invrt_Dte_Tme", "LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        registerRecord(vw_master);

        pnd_Crprte_Status_Chnge_Dte_Key = localVariables.newFieldInRecord("pnd_Crprte_Status_Chnge_Dte_Key", "#CRPRTE-STATUS-CHNGE-DTE-KEY", FieldType.STRING, 
            25);

        pnd_Crprte_Status_Chnge_Dte_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Crprte_Status_Chnge_Dte_Key__R_Field_2", "REDEFINE", pnd_Crprte_Status_Chnge_Dte_Key);
        pnd_Crprte_Status_Chnge_Dte_Key_Pnd_Crprte_Status_Ind = pnd_Crprte_Status_Chnge_Dte_Key__R_Field_2.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_Pnd_Crprte_Status_Ind", 
            "#CRPRTE-STATUS-IND", FieldType.STRING, 1);
        pnd_Crprte_Status_Chnge_Dte_Key_Pnd_Last_Chnge_Invrt_Dte_Tme = pnd_Crprte_Status_Chnge_Dte_Key__R_Field_2.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_Pnd_Last_Chnge_Invrt_Dte_Tme", 
            "#LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Last_Chnge_Dte_Tme = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_Tme", "#LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);

        pnd_Last_Chnge_Dte_Tme__R_Field_3 = localVariables.newGroupInRecord("pnd_Last_Chnge_Dte_Tme__R_Field_3", "REDEFINE", pnd_Last_Chnge_Dte_Tme);
        pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte = pnd_Last_Chnge_Dte_Tme__R_Field_3.newFieldInGroup("pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte", "#LAST-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Tme = pnd_Last_Chnge_Dte_Tme__R_Field_3.newFieldInGroup("pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Tme", "#LAST-CHNGE-TME", 
            FieldType.NUMERIC, 7);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_4 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_4", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Tbl_Data_Field1 = cwf_Support_Tbl__R_Field_4.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_Data_Field1", "#TBL-DATA-FIELD1", FieldType.NUMERIC, 
            8);
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_5", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);

        vw_cwf_Wp_Work_Prcss_Id = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Work_Prcss_Id", "CWF-WP-WORK-PRCSS-ID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Id = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", 
            FieldType.STRING, 45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        registerRecord(vw_cwf_Wp_Work_Prcss_Id);

        pnd_Wpid_Uniq_Key = localVariables.newFieldInRecord("pnd_Wpid_Uniq_Key", "#WPID-UNIQ-KEY", FieldType.STRING, 7);

        pnd_Wpid_Uniq_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Wpid_Uniq_Key__R_Field_6", "REDEFINE", pnd_Wpid_Uniq_Key);
        pnd_Wpid_Uniq_Key_Pnd_Wpid = pnd_Wpid_Uniq_Key__R_Field_6.newFieldInGroup("pnd_Wpid_Uniq_Key_Pnd_Wpid", "#WPID", FieldType.STRING, 6);

        vw_empl_Tbl = new DataAccessProgramView(new NameInfo("vw_empl_Tbl", "EMPL-TBL"), "CWF_ORG_EMPL_TBL", "CWF_ASSIGN_RULE");
        empl_Tbl_Empl_Racf_Id = vw_empl_Tbl.getRecord().newFieldInGroup("empl_Tbl_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "EMPL_RACF_ID");
        empl_Tbl_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF  ID");
        empl_Tbl_Empl_Nme = vw_empl_Tbl.getRecord().newFieldInGroup("empl_Tbl_Empl_Nme", "EMPL-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "EMPL_NME");
        empl_Tbl_Empl_Nme.setDdmHeader("EMPLOYEE NAME");

        empl_Tbl__R_Field_7 = vw_empl_Tbl.getRecord().newGroupInGroup("empl_Tbl__R_Field_7", "REDEFINE", empl_Tbl_Empl_Nme);
        empl_Tbl_Empl_First_Nme = empl_Tbl__R_Field_7.newFieldInGroup("empl_Tbl_Empl_First_Nme", "EMPL-FIRST-NME", FieldType.STRING, 20);
        empl_Tbl_Empl_Last_Nme = empl_Tbl__R_Field_7.newFieldInGroup("empl_Tbl_Empl_Last_Nme", "EMPL-LAST-NME", FieldType.STRING, 20);
        empl_Tbl_Dlte_Dte_Tme = vw_empl_Tbl.getRecord().newFieldInGroup("empl_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "DLTE_DTE_TME");
        empl_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        empl_Tbl_Dlte_Oprtr_Cde = vw_empl_Tbl.getRecord().newFieldInGroup("empl_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DLTE_OPRTR_CDE");
        empl_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPERATOR");
        registerRecord(vw_empl_Tbl);

        pnd_Output_Workfile = localVariables.newFieldInRecord("pnd_Output_Workfile", "#OUTPUT-WORKFILE", FieldType.STRING, 181);

        pnd_Output_Workfile__R_Field_8 = localVariables.newGroupInRecord("pnd_Output_Workfile__R_Field_8", "REDEFINE", pnd_Output_Workfile);
        pnd_Output_Workfile_Pnd_O_Empl_Name = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Empl_Name", "#O-EMPL-NAME", FieldType.STRING, 
            40);
        pnd_Output_Workfile_Pnd_O_Filler1 = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Filler1", "#O-FILLER1", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_O_Cntrct_Nbr = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Cntrct_Nbr", "#O-CNTRCT-NBR", 
            FieldType.STRING, 90);
        pnd_Output_Workfile_Pnd_O_Filler2 = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Filler2", "#O-FILLER2", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_O_Pin_Nbr = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Pin_Nbr", "#O-PIN-NBR", FieldType.STRING, 
            12);
        pnd_Output_Workfile_Pnd_O_Filler3 = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Filler3", "#O-FILLER3", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_O_Tiaa_Rcvd_Dte = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Tiaa_Rcvd_Dte", "#O-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        pnd_Output_Workfile_Pnd_O_Filler5 = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Filler5", "#O-FILLER5", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_O_Work_Prcss_Id = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Work_Prcss_Id", "#O-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        pnd_Output_Workfile_Pnd_O_Filler4 = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Filler4", "#O-FILLER4", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_O_Transaction_Type = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Transaction_Type", "#O-TRANSACTION-TYPE", 
            FieldType.STRING, 15);
        pnd_Output_Workfile_Pnd_O_Filler6 = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Filler6", "#O-FILLER6", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_O_Clndr_Days_Aging = pnd_Output_Workfile__R_Field_8.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Clndr_Days_Aging", "#O-CLNDR-DAYS-AGING", 
            FieldType.NUMERIC, 4);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.NUMERIC, 8);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.NUMERIC, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Rec_Count = localVariables.newFieldInRecord("pnd_Rec_Count", "#REC-COUNT", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_master.reset();
        vw_cwf_Support_Tbl.reset();
        vw_cwf_Wp_Work_Prcss_Id.reset();
        vw_empl_Tbl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8640() throws Exception
    {
        super("Cwfb8640");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* *----------------------------------------------------------------------
        pnd_Rec_Count.reset();                                                                                                                                            //Natural: RESET #REC-COUNT #TBL-PRIME-KEY
        pnd_Tbl_Prime_Key.reset();
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A ");                                                                                                         //Natural: ASSIGN #TBL-SCRTY-LEVEL-IND := 'A '
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("PCW4500D-RUN-DATE");                                                                                                //Natural: ASSIGN #TBL-TABLE-NME := 'PCW4500D-RUN-DATE'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("PCW4500D-RUN-DATE-KEY");                                                                                            //Natural: ASSIGN #TBL-KEY-FIELD := 'PCW4500D-RUN-DATE-KEY'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind.setValue(" ");                                                                                                                //Natural: ASSIGN #TBL-ACTVE-IND := ' '
        vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                              //Natural: FIND CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "F1",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) }
        );
        F1:
        while (condition(vw_cwf_Support_Tbl.readNextRow("F1", true)))
        {
            vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Support_Tbl.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS FOUND
            {
                pnd_Start_Date.setValue(20140101);                                                                                                                        //Natural: ASSIGN #START-DATE := 20140101
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Start_Date.setValue(cwf_Support_Tbl_Pnd_Tbl_Data_Field1);                                                                                                 //Natural: MOVE CWF-SUPPORT-TBL.#TBL-DATA-FIELD1 TO #START-DATE
            if (condition(pnd_Start_Date.equals(getZero())))                                                                                                              //Natural: IF #START-DATE EQ 0
            {
                pnd_Start_Date.setValue(20140101);                                                                                                                        //Natural: ASSIGN #START-DATE := 20140101
            }                                                                                                                                                             //Natural: END-IF
            //*  (F1.)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_End_Date.setValue(Global.getDATN());                                                                                                                          //Natural: MOVE *DATN TO #END-DATE
        getReports().print(0, "=",pnd_Start_Date,NEWLINE,"=",pnd_End_Date);                                                                                               //Natural: PRINT '=' #START-DATE / '=' #END-DATE
        pnd_Crprte_Status_Chnge_Dte_Key.reset();                                                                                                                          //Natural: RESET #CRPRTE-STATUS-CHNGE-DTE-KEY
        pnd_Crprte_Status_Chnge_Dte_Key_Pnd_Crprte_Status_Ind.setValue("0");                                                                                              //Natural: ASSIGN #CRPRTE-STATUS-IND := '0'
        vw_master.startDatabaseRead                                                                                                                                       //Natural: READ MASTER BY CRPRTE-STATUS-CHNGE-DTE-KEY = #CRPRTE-STATUS-CHNGE-DTE-KEY
        (
        "R1",
        new Wc[] { new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", ">=", pnd_Crprte_Status_Chnge_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
        );
        R1:
        while (condition(vw_master.readNextRow("R1")))
        {
            if (condition(master_Crprte_Status_Ind.greater("0")))                                                                                                         //Natural: IF MASTER.CRPRTE-STATUS-IND GT '0'
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Last_Chnge_Dte_Tme.compute(new ComputeParameters(false, pnd_Last_Chnge_Dte_Tme), new DbsDecimal("999999999999999").subtract(master_Last_Chnge_Invrt_Dte_Tme)); //Natural: ASSIGN #LAST-CHNGE-DTE-TME := 999999999999999 - MASTER.LAST-CHNGE-INVRT-DTE-TME
            if (condition(pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte.less(pnd_Start_Date)))                                                                                //Natural: IF #LAST-CHNGE-DTE LT #START-DATE
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(master_Work_Prcss_Id.equals("TIPCPW") || master_Work_Prcss_Id.equals("TIPCS") || master_Work_Prcss_Id.equals("TIPOW") || master_Work_Prcss_Id.equals("TIPTXO")  //Natural: ACCEPT IF MASTER.WORK-PRCSS-ID = 'TIPCPW' OR = 'TIPCS' OR = 'TIPOW' OR = 'TIPTXO' OR = 'TIPTPO' OR = 'TIPIEX' OR = 'TIPSA'
                || master_Work_Prcss_Id.equals("TIPTPO") || master_Work_Prcss_Id.equals("TIPIEX") || master_Work_Prcss_Id.equals("TIPSA"))))
            {
                continue;
            }
            //*    OR = 'TIPDM'  OR = 'TIPDP'   /* REMOVING THESE 2 WPIDS FROM THE
            //*                                 /* REPORT AS THEY ARE PROCESSED BY
            //*                                 /* A DIFFERENT TEAM (CLAIMS TXNS)
            //*                                 /* PER KAREN BURKART
            pnd_Output_Workfile.reset();                                                                                                                                  //Natural: RESET #OUTPUT-WORKFILE
            //*  ** EMPLOYEE NAME LOOKUP **
            vw_empl_Tbl.startDatabaseRead                                                                                                                                 //Natural: READ EMPL-TBL BY EMPL-RACF-ID-UNIT-KEY STARTING FROM MASTER.EMPL-OPRTR-CDE
            (
            "R2",
            new Wc[] { new Wc("EMPL_RACF_ID_UNIT_KEY", ">=", master_Empl_Oprtr_Cde, WcType.BY) },
            new Oc[] { new Oc("EMPL_RACF_ID_UNIT_KEY", "ASC") }
            );
            R2:
            while (condition(vw_empl_Tbl.readNextRow("R2")))
            {
                if (condition(empl_Tbl_Empl_Racf_Id.notEquals(master_Empl_Oprtr_Cde)))                                                                                    //Natural: IF EMPL-TBL.EMPL-RACF-ID NE MASTER.EMPL-OPRTR-CDE
                {
                    pnd_Output_Workfile_Pnd_O_Empl_Name.setValue(master_Empl_Oprtr_Cde);                                                                                  //Natural: MOVE MASTER.EMPL-OPRTR-CDE TO #O-EMPL-NAME
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(empl_Tbl_Dlte_Dte_Tme.equals(getZero()) && empl_Tbl_Dlte_Oprtr_Cde.equals(" ")))                                                            //Natural: IF EMPL-TBL.DLTE-DTE-TME EQ 0 AND EMPL-TBL.DLTE-OPRTR-CDE EQ ' '
                {
                    pnd_Output_Workfile_Pnd_O_Empl_Name.setValue(DbsUtil.compress(empl_Tbl_Empl_First_Nme, empl_Tbl_Empl_Last_Nme));                                      //Natural: COMPRESS EMPL-TBL.EMPL-FIRST-NME EMPL-TBL.EMPL-LAST-NME INTO #O-EMPL-NAME
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  (R2.)
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  (R2.)
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO *OCCURRENCE ( MASTER.CNTRCT-NBR ( * ) )
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(master_Cntrct_Nbr.getValue("*").astOCCURRENCE())); pnd_I.nadd(1))
            {
                if (condition(master_Cntrct_Nbr.getValue(pnd_I).notEquals(" ")))                                                                                          //Natural: IF MASTER.CNTRCT-NBR ( #I ) NE ' '
                {
                    if (condition(pnd_Output_Workfile_Pnd_O_Cntrct_Nbr.equals(" ")))                                                                                      //Natural: IF #O-CNTRCT-NBR EQ ' '
                    {
                        pnd_Output_Workfile_Pnd_O_Cntrct_Nbr.setValue(master_Cntrct_Nbr.getValue(pnd_I));                                                                 //Natural: MOVE MASTER.CNTRCT-NBR ( #I ) TO #O-CNTRCT-NBR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Output_Workfile_Pnd_O_Cntrct_Nbr.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_Workfile_Pnd_O_Cntrct_Nbr,               //Natural: COMPRESS #O-CNTRCT-NBR '/' MASTER.CNTRCT-NBR ( #I ) INTO #O-CNTRCT-NBR LEAVING NO
                            "/", master_Cntrct_Nbr.getValue(pnd_I)));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Output_Workfile_Pnd_O_Pin_Nbr.moveAll(master_Pin_Nbr);                                                                                                    //Natural: MOVE ALL MASTER.PIN-NBR TO #O-PIN-NBR
            pnd_Output_Workfile_Pnd_O_Tiaa_Rcvd_Dte.setValueEdited(master_Tiaa_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED MASTER.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO #O-TIAA-RCVD-DTE
            //*    ** WPID LOOKUP TO GET WPID DESCRIPTION **
            pnd_Wpid_Uniq_Key.reset();                                                                                                                                    //Natural: RESET #WPID-UNIQ-KEY
            pnd_Output_Workfile_Pnd_O_Work_Prcss_Id.setValue(master_Work_Prcss_Id);                                                                                       //Natural: MOVE MASTER.WORK-PRCSS-ID TO #O-WORK-PRCSS-ID #WPID
            pnd_Wpid_Uniq_Key_Pnd_Wpid.setValue(master_Work_Prcss_Id);
            vw_cwf_Wp_Work_Prcss_Id.startDatabaseFind                                                                                                                     //Natural: FIND CWF-WP-WORK-PRCSS-ID WITH WPID-UNIQ-KEY = #WPID-UNIQ-KEY
            (
            "F2",
            new Wc[] { new Wc("WPID_UNIQ_KEY", "=", pnd_Wpid_Uniq_Key, WcType.WITH) }
            );
            F2:
            while (condition(vw_cwf_Wp_Work_Prcss_Id.readNextRow("F2", true)))
            {
                vw_cwf_Wp_Work_Prcss_Id.setIfNotFoundControlFlag(false);
                if (condition(vw_cwf_Wp_Work_Prcss_Id.getAstCOUNTER().equals(0)))                                                                                         //Natural: IF NO RECORDS FOUND
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Output_Workfile_Pnd_O_Transaction_Type.setValue(cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme);                                                           //Natural: MOVE CWF-WP-WORK-PRCSS-ID.WORK-PRCSS-SHORT-NME TO #O-TRANSACTION-TYPE
                //*  (F2.)
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Output_Workfile_Pnd_O_Clndr_Days_Aging.compute(new ComputeParameters(false, pnd_Output_Workfile_Pnd_O_Clndr_Days_Aging), Global.getDATX().subtract(master_Tiaa_Rcvd_Dte)); //Natural: COMPUTE #O-CLNDR-DAYS-AGING = *DATX - MASTER.TIAA-RCVD-DTE
            pnd_Output_Workfile_Pnd_O_Filler1.setValue(",");                                                                                                              //Natural: MOVE ',' TO #O-FILLER1 #O-FILLER2 #O-FILLER3 #O-FILLER4 #O-FILLER5 #O-FILLER6
            pnd_Output_Workfile_Pnd_O_Filler2.setValue(",");
            pnd_Output_Workfile_Pnd_O_Filler3.setValue(",");
            pnd_Output_Workfile_Pnd_O_Filler4.setValue(",");
            pnd_Output_Workfile_Pnd_O_Filler5.setValue(",");
            pnd_Output_Workfile_Pnd_O_Filler6.setValue(",");
            getWorkFiles().write(1, false, pnd_Output_Workfile);                                                                                                          //Natural: WRITE WORK FILE 1 #OUTPUT-WORKFILE
            pnd_Rec_Count.nadd(1);                                                                                                                                        //Natural: ASSIGN #REC-COUNT := #REC-COUNT + 1
            //*  (R1.)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().print(0, "Number of Records written to the Workfile:",pnd_Rec_Count, new ReportEditMask ("Z,ZZZ,ZZ9"));                                              //Natural: PRINT 'Number of Records written to the Workfile:' #REC-COUNT ( EM = Z,ZZZ,ZZ9 )
    }

    //
}
