/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:30:01 PM
**        * FROM NATURAL PROGRAM : Cwfb3495
************************************************************
**        * FILE NAME            : Cwfb3495.java
**        * CLASS NAME           : Cwfb3495
**        * INSTANCE NAME        : Cwfb3495
************************************************************
************************************************************************
* PROGRAM  : CWFB3495
* SYSTEM   : CWF
* TITLE    :
* GENERATED: OCT 05,2000
* FUNCTION : EXTRACTS THE STATUS CATEGORY ENTRIES FROM THE SUPPORT
*          :   TABLE FOR LOADING TO ORACLE (FOR SMART)
*          :
*          :
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------   -------- --------------------------------------------------
*
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3495 extends BLNatBase
{
    // Data Areas
    private PdaCwfl5312 pdaCwfl5312;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Group_Name;
    private DbsField pnd_Work_File_Tbl_Key_Field;
    private DbsField pnd_Work_File_Tbl_Data_Field;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfl5312 = new PdaCwfl5312(localVariables);

        // Local Variables

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Group_Name = pnd_Work_File.newFieldInGroup("pnd_Work_File_Group_Name", "GROUP-NAME", FieldType.STRING, 30);
        pnd_Work_File_Tbl_Key_Field = pnd_Work_File.newFieldInGroup("pnd_Work_File_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Work_File_Tbl_Data_Field = pnd_Work_File.newFieldInGroup("pnd_Work_File_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Work_File_Group_Name.setInitialValue("STATUS-CATEGORY-TBL");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3495() throws Exception
    {
        super("Cwfb3495");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Authrty().setValue("A");                                                                                                   //Natural: MOVE 'A' TO #GEN-TBL-AUTHRTY
        pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Name().setValue("SMART-STATUS-CAT");                                                                                       //Natural: MOVE 'SMART-STATUS-CAT' TO #GEN-TBL-NAME
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY FROM #GEN-TBL-KEY
        (
        "RD1",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Key(), WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        RD1:
        while (condition(vw_cwf_Support_Tbl.readNextRow("RD1")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Scrty_Level_Ind.notEquals(pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Authrty()) || cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Name()))) //Natural: IF CWF-SUPPORT-TBL.TBL-SCRTY-LEVEL-IND NE #GEN-TBL-AUTHRTY OR CWF-SUPPORT-TBL.TBL-TABLE-NME NE #GEN-TBL-NAME
            {
                if (true) break RD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_File.setValuesByName(vw_cwf_Support_Tbl);                                                                                                            //Natural: MOVE BY NAME CWF-SUPPORT-TBL TO #WORK-FILE
            getWorkFiles().write(1, false, pnd_Work_File);                                                                                                                //Natural: WRITE WORK FILE 1 #WORK-FILE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
