/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:30:10 PM
**        * FROM NATURAL PROGRAM : Cwfb3801
************************************************************
**        * FILE NAME            : Cwfb3801.java
**        * CLASS NAME           : Cwfb3801
**        * INSTANCE NAME        : Cwfb3801
************************************************************
************************************************************************
* PROGRAM  : CWFB3801
* SYSTEM   : CRPCWF
* TITLE    : PREMIUM DIVISION DAILY OPEN CASES REPORT
* GENERATED: OCT 05,93 AT 08:41 AM
* FUNCTION : EXTRACT ALL PREMIUM OPEN MASTER INDEX RECORDS
*
************************************************************************
*                    M O D I F I C A T I O N S                         *
************************************************************************
* NAME         DATE    DESCRIPTION                                     *
* ----------- -------- ------------------------------------------------*
* J. HARGRAVE 08/23/95 CHANGE TO READ CORE FILE INSTEAD OF PIF FILE    *
************************************************************************
*
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3801 extends BLNatBase
{
    // Data Areas
    private PdaCwfa5372 pdaCwfa5372;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Master_Index;
    private DbsField cwf_Master_Index_Pin_Nbr;
    private DbsField cwf_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index__R_Field_1;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_Work_Prcss_Id;
    private DbsField cwf_Master_Index_Unit_Cde;
    private DbsField cwf_Master_Index_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index__R_Field_2;
    private DbsField cwf_Master_Index_Empl_Racf_Id;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_Step_Id;
    private DbsField cwf_Master_Index_Admin_Unit_Cde;

    private DbsGroup cwf_Master_Index__R_Field_3;
    private DbsField cwf_Master_Index_Admin_Unit_Id_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Status_Cde;
    private DbsField cwf_Master_Index_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Updte_Dte;
    private DbsField cwf_Master_Index_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Actve_Ind;
    private DbsField cwf_Master_Index_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_Effctve_Dte;
    private DbsField cwf_Master_Index_Trans_Dte;
    private DbsField cwf_Master_Index_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_Physcl_Fldr_Id_Nbr;
    private DbsGroup cwf_Master_Index_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_Cntrct_Nbr;
    private DbsField cwf_Master_Index_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_Work_List_Ind;
    private DbsField cwf_Master_Index_Due_Dte_Chg_Prty_Cde;

    private DbsGroup cwf_Master_Index__R_Field_4;
    private DbsField cwf_Master_Index_Due_Dte;
    private DbsField cwf_Master_Index_Due_Dte_Chg_Ind;
    private DbsField cwf_Master_Index_Due_Dte_Prty_Cde;
    private DbsField cwf_Master_Index_Due_Dte_Filler;
    private DbsField cwf_Master_Index_Bsnss_Reply_Ind;
    private DbsField cwf_Master_Index_Status_Freeze_Ind;
    private DbsField cwf_Master_Index_Elctrnc_Fldr_Ind;
    private DbsField cwf_Master_Index_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_Days;
    private DbsField cwf_Master_Index_Step_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Step_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Crprte_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Unit_En_Rte_To_Dte_Tme;

    private DbsGroup extract_Record;
    private DbsField extract_Record_Pin_Nbr;
    private DbsField extract_Record_Rqst_Log_Dte_Tme;

    private DbsGroup extract_Record__R_Field_5;
    private DbsField extract_Record_Rqst_Log_Index_Dte;
    private DbsField extract_Record_Rqst_Log_Index_Tme;
    private DbsField extract_Record_Rqst_Log_Oprtr_Cde;
    private DbsField extract_Record_Rqst_Orgn_Cde;
    private DbsField extract_Record_Orgnl_Log_Dte_Tme;
    private DbsField extract_Record_Orgnl_Unit_Cde;
    private DbsField extract_Record_Work_Prcss_Id;

    private DbsGroup extract_Record__R_Field_6;
    private DbsField extract_Record_Work_Actn_Rqstd_Cde;
    private DbsField extract_Record_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField extract_Record_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField extract_Record_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField extract_Record_Unit_Cde;

    private DbsGroup extract_Record__R_Field_7;
    private DbsField extract_Record_Unit_Id_Cde;
    private DbsField extract_Record_Unit_Rgn_Cde;
    private DbsField extract_Record_Unit_Spcl_Dsgntn_Cde;
    private DbsField extract_Record_Unit_Brnch_Group_Cde;
    private DbsField extract_Record_Unit_Updte_Dte_Tme;
    private DbsField extract_Record_Empl_Oprtr_Cde;

    private DbsGroup extract_Record__R_Field_8;
    private DbsField extract_Record_Empl_Racf_Id;
    private DbsField extract_Record_Empl_Sffx_Cde;
    private DbsField extract_Record_Last_Chnge_Dte_Tme;
    private DbsField extract_Record_Last_Chnge_Oprtr_Cde;
    private DbsField extract_Record_Last_Chnge_Unit_Cde;
    private DbsField extract_Record_Step_Id;
    private DbsField extract_Record_Admin_Unit_Cde;
    private DbsField extract_Record_Admin_Status_Cde;
    private DbsField extract_Record_Admin_Status_Updte_Dte_Tme;
    private DbsField extract_Record_Admin_Status_Updte_Oprtr_Cde;
    private DbsField extract_Record_Status_Cde;
    private DbsField extract_Record_Status_Updte_Dte_Tme;
    private DbsField extract_Record_Status_Updte_Oprtr_Cde;
    private DbsField extract_Record_Last_Updte_Dte;
    private DbsField extract_Record_Last_Updte_Dte_Tme;
    private DbsField extract_Record_Last_Updte_Oprtr_Cde;
    private DbsField extract_Record_Crprte_Status_Ind;
    private DbsField extract_Record_Tiaa_Rcvd_Dte;
    private DbsField extract_Record_Effctve_Dte;
    private DbsField extract_Record_Trans_Dte;
    private DbsField extract_Record_Cntrct_Nbr;
    private DbsField extract_Record_Pnd_Calendar_Days_In_Tiaa;
    private DbsField extract_Record_Pnd_Business_Days_In_Tiaa;
    private DbsField extract_Record_Extrnl_Pend_Rcv_Dte;
    private DbsField extract_Record_Pnd_Received_In_Unit;
    private DbsField extract_Record_Pnd_Bsnss_Days_In_Unit;
    private DbsField extract_Record_Pnd_Intrnl_Pend_Days;
    private DbsField extract_Record_Pnd_Extrnl_Pend_Days;
    private DbsField extract_Record_Pnd_Extrnl_Pend_Bsnss_Days;
    private DbsField extract_Record_Pnd_Partic_Sname;

    private DbsGroup extract_Record__R_Field_9;
    private DbsField extract_Record_Pnd_Folder_Prefix;
    private DbsField extract_Record_Pnd_Folder_Nbr;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Tbl_Racf;
    private DbsField pnd_Work_Record_Tbl_Moc;
    private DbsField pnd_Work_Record_Tbl_Calendar_Days;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Bsnss_Days;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Bsnssdays;
    private DbsField pnd_Work_Record_Tbl_Business_Days;
    private DbsField pnd_Work_Record_Tbl_Wpid_Act;
    private DbsField pnd_Work_Record_Tbl_Wpid;

    private DbsGroup pnd_Work_Record__R_Field_10;
    private DbsField pnd_Work_Record_Tbl_Wpid_Action;
    private DbsField pnd_Work_Record_Tbl_Status_Key;

    private DbsGroup pnd_Work_Record__R_Field_11;
    private DbsField pnd_Work_Record_Tbl_Last_Chnge_Unit_Cde;
    private DbsField pnd_Work_Record_Tbl_Status_Cde;
    private DbsField pnd_Work_Record_Tbl_Log_Unit_Cde;
    private DbsField pnd_Work_Record_Tbl_Pin;
    private DbsField pnd_Work_Record_Tbl_Step_Id;
    private DbsField pnd_Work_Record_Tbl_Sort_Step_Id;
    private DbsField pnd_Work_Record_Tbl_Contracts;
    private DbsField pnd_Work_Record_Tbl_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Dte;
    private DbsField pnd_Work_Record_Tbl_Status_Dte;
    private DbsField pnd_Work_Record_Return_Doc_Rec_Dte_Tme;
    private DbsField pnd_Work_Record_Return_Rcvd_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Last_Chge_Dte;
    private DbsField pnd_Work_Record_Partic_Sname;
    private DbsField pnd_Env;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Conv_Ctr;
    private DbsField pnd_Nconv_Ctr;
    private DbsField pnd_Rject_Ctr;
    private DbsField pnd_Total_Ctr;
    private DbsField pnd_No_Extract;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Tbl_Run_Date;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Start_Date_A;
    private DbsField pnd_Base_Date;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_12;

    private DbsGroup pnd_Tbl_Key_Data_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Key_Field;
    private DbsField pnd_Parm_Report_No;
    private DbsField pnd_Parm_Empl_Racf_Id;
    private DbsField pnd_Parm_Unit_Cde;
    private DbsField pnd_Parm_Floor;
    private DbsField pnd_Parm_Bldg;
    private DbsField pnd_Parm_Drop_Off;
    private DbsField pnd_Parm_Run_Date;

    private DbsGroup pnd_End_Var;
    private DbsField pnd_End_Var_Pnd_End_Tbl_Key;

    private DbsGroup pnd_End_Var__R_Field_13;
    private DbsField pnd_End_Var_Pnd_End_Tbl_Authrty;
    private DbsField pnd_End_Var_Pnd_End_Tbl_Name;
    private DbsField pnd_End_Var_Pnd_End_Tbl_Code;

    private DbsGroup pnd_Gen_Var;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Key;

    private DbsGroup pnd_Gen_Var__R_Field_14;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Authrty;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Name;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Code;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Desc;
    private DbsField pnd_Parm_Type;
    private DbsField pnd_Sub;
    private DbsField pnd_Timx;
    private DbsField pnd_Receive_Date;
    private DbsField pnd_Report_Date;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Report_Date_Time;
    private DbsField pnd_Intrnl_Pend_Date;
    private DbsField pnd_Unit_Start_Date_Time;
    private DbsField pnd_Intrnl_Pend_Daysx;
    private DbsField pnd_Extrnl_Pend_Daysx;
    private DbsField pnd_Ext_Pend_Business_Days;
    private DbsField pnd_Bsnss_Days_In_Unitx;
    private DbsField pnd_No_Work_Days;
    private DbsField pnd_Valid_Units;

    private DbsGroup pnd_Report_Data;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Cde;

    private DbsGroup pnd_Report_Data__R_Field_15;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Id_Cde;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid;

    private DbsGroup pnd_Report_Data__R_Field_16;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Lob;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Mbp;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Sbp;
    private DbsField pnd_Report_Data_Pnd_Rep_Racf_Id;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Empl_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Subtotal_Ind;

    private DbsGroup pnd_Misc_Parm;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Cde;

    private DbsGroup pnd_Misc_Parm__R_Field_17;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix;
    private DbsField pnd_Misc_Parm_Pnd_Save_Action;
    private DbsField pnd_Misc_Parm_Pnd_Wpid;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Action;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Code;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Count;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Desc;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Text;
    private DbsField pnd_Misc_Parm_Pnd_Rep_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count;
    private DbsField pnd_Misc_Parm_Pnd_Sub_Count;
    private DbsField pnd_Misc_Parm_Pnd_Booklet_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Forms_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Inquire_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Research_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Trans_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Complaint_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Other_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Read_Count;
    private DbsField pnd_Misc_Parm_Pnd_Actve_Unque_Key;
    private DbsField pnd_Misc_Parm_Pnd_Todays_Time;
    private DbsField pnd_Misc_Parm_Pnd_Return_Code;
    private DbsField pnd_Misc_Parm_Pnd_Return_Msg;
    private DbsField pnd_Misc_Parm_Pnd_Status_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Return_Doc_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Return_Rcvd_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Confirmed;
    private DbsField pnd_Page;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Int_Moc;
    private DbsField pnd_Ext_Moc;
    private DbsField pnd_Work_Moc;
    private DbsField pnd_Holiday;
    private DbsField pnd_Report_No;
    private DbsField pnd_Save_Unit;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Run_Date;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Tbl_WpidOld;
    private DbsField sort01Tbl_Wpid_ActOld;
    private DbsField sort01Tbl_WpidCount613;
    private DbsField sort01Tbl_WpidCount;
    private DbsField sort01Tbl_MocOld;
    private DbsField sort01Tbl_MocCount661;
    private DbsField sort01Tbl_MocCount;
    private DbsField sort01Tbl_Log_Unit_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa5372 = new PdaCwfa5372(localVariables);

        // Local Variables

        vw_cwf_Master_Index = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index", "CWF-MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Index_Pin_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Index_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_Rqst_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index__R_Field_1 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_1", "REDEFINE", cwf_Master_Index_Rqst_Log_Dte_Tme);
        cwf_Master_Index_Rqst_Log_Index_Dte = cwf_Master_Index__R_Field_1.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_Rqst_Log_Index_Tme = cwf_Master_Index__R_Field_1.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_Rqst_Orgn_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_Orgnl_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_Work_Prcss_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Master_Index_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Master_Index_Unit_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Index_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Index_Empl_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");

        cwf_Master_Index__R_Field_2 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_2", "REDEFINE", cwf_Master_Index_Empl_Oprtr_Cde);
        cwf_Master_Index_Empl_Racf_Id = cwf_Master_Index__R_Field_2.newFieldInGroup("cwf_Master_Index_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_Last_Chnge_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_Last_Chnge_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_Step_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        cwf_Master_Index_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Index_Admin_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");

        cwf_Master_Index__R_Field_3 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_3", "REDEFINE", cwf_Master_Index_Admin_Unit_Cde);
        cwf_Master_Index_Admin_Unit_Id_Cde = cwf_Master_Index__R_Field_3.newFieldInGroup("cwf_Master_Index_Admin_Unit_Id_Cde", "ADMIN-UNIT-ID-CDE", FieldType.STRING, 
            5);
        cwf_Master_Index_Admin_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_Admin_Status_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_Status_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Index_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_Last_Updte_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        cwf_Master_Index_Last_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Index_Last_Updte_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Index_Actve_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Master_Index_Crprte_Status_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_Tiaa_Rcvd_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_Effctve_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_Trans_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Trans_Dte", "TRANS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cwf_Master_Index_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_Mj_Pull_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_Physcl_Fldr_Id_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Index_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Index_Cntrct_NbrMuGroup = vw_cwf_Master_Index.getRecord().newGroupInGroup("CWF_MASTER_INDEX_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_Cntrct_Nbr = cwf_Master_Index_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 
            8, new DbsArrayController(1, 1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_Work_List_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "WORK_LIST_IND");
        cwf_Master_Index_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        cwf_Master_Index_Due_Dte_Chg_Prty_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Due_Dte_Chg_Prty_Cde", "DUE-DTE-CHG-PRTY-CDE", 
            FieldType.STRING, 16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Index_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        cwf_Master_Index__R_Field_4 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_4", "REDEFINE", cwf_Master_Index_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Index_Due_Dte = cwf_Master_Index__R_Field_4.newFieldInGroup("cwf_Master_Index_Due_Dte", "DUE-DTE", FieldType.STRING, 8);
        cwf_Master_Index_Due_Dte_Chg_Ind = cwf_Master_Index__R_Field_4.newFieldInGroup("cwf_Master_Index_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_Due_Dte_Prty_Cde = cwf_Master_Index__R_Field_4.newFieldInGroup("cwf_Master_Index_Due_Dte_Prty_Cde", "DUE-DTE-PRTY-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_Due_Dte_Filler = cwf_Master_Index__R_Field_4.newFieldInGroup("cwf_Master_Index_Due_Dte_Filler", "DUE-DTE-FILLER", FieldType.STRING, 
            6);
        cwf_Master_Index_Bsnss_Reply_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BSNSS_REPLY_IND");
        cwf_Master_Index_Bsnss_Reply_Ind.setDdmHeader("BUSINESS/REPLY");
        cwf_Master_Index_Status_Freeze_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Index_Elctrnc_Fldr_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_Index_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        cwf_Master_Index_Unit_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Clock_Start_Dte_Tme", "UNIT-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Index_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Index_Unit_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Clock_End_Dte_Tme", "UNIT-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Index_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Index_Empl_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Clock_Start_Dte_Tme", "EMPL-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        cwf_Master_Index_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        cwf_Master_Index_Empl_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Clock_End_Dte_Tme", "EMPL-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        cwf_Master_Index_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme", "INTRNL-PND-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_START_DTE_TME");
        cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme.setDdmHeader("ACKNOWLEDGEMENT DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme", "INTRNL-PND-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_END_DTE_TME");
        cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme.setDdmHeader("INTERNAL PEND END DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_Days = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_Days", "INTRNL-PND-DAYS", FieldType.PACKED_DECIMAL, 
            5, RepeatingFieldStrategy.None, "INTRNL_PND_DAYS");
        cwf_Master_Index_Intrnl_Pnd_Days.setDdmHeader("INTERNAL PEND DAYS");
        cwf_Master_Index_Step_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Clock_Start_Dte_Tme", "STEP-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        cwf_Master_Index_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        cwf_Master_Index_Step_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Clock_End_Dte_Tme", "STEP-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        cwf_Master_Index_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        cwf_Master_Index_Crprte_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Index_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        cwf_Master_Index_Unit_En_Rte_To_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_En_Rte_To_Dte_Tme", "UNIT-EN-RTE-TO-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_EN_RTE_TO_DTE_TME");
        cwf_Master_Index_Unit_En_Rte_To_Dte_Tme.setDdmHeader("UNIT EN ROUTE TO DATE TIME");
        registerRecord(vw_cwf_Master_Index);

        extract_Record = localVariables.newGroupInRecord("extract_Record", "EXTRACT-RECORD");
        extract_Record_Pin_Nbr = extract_Record.newFieldInGroup("extract_Record_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        extract_Record_Rqst_Log_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15);

        extract_Record__R_Field_5 = extract_Record.newGroupInGroup("extract_Record__R_Field_5", "REDEFINE", extract_Record_Rqst_Log_Dte_Tme);
        extract_Record_Rqst_Log_Index_Dte = extract_Record__R_Field_5.newFieldInGroup("extract_Record_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 
            8);
        extract_Record_Rqst_Log_Index_Tme = extract_Record__R_Field_5.newFieldInGroup("extract_Record_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", FieldType.STRING, 
            7);
        extract_Record_Rqst_Log_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Rqst_Orgn_Cde = extract_Record.newFieldInGroup("extract_Record_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1);
        extract_Record_Orgnl_Log_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", FieldType.STRING, 15);
        extract_Record_Orgnl_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        extract_Record_Work_Prcss_Id = extract_Record.newFieldInGroup("extract_Record_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);

        extract_Record__R_Field_6 = extract_Record.newGroupInGroup("extract_Record__R_Field_6", "REDEFINE", extract_Record_Work_Prcss_Id);
        extract_Record_Work_Actn_Rqstd_Cde = extract_Record__R_Field_6.newFieldInGroup("extract_Record_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", FieldType.STRING, 
            1);
        extract_Record_Work_Lob_Cmpny_Prdct_Cde = extract_Record__R_Field_6.newFieldInGroup("extract_Record_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        extract_Record_Work_Mjr_Bsnss_Prcss_Cde = extract_Record__R_Field_6.newFieldInGroup("extract_Record_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        extract_Record_Work_Spcfc_Bsnss_Prcss_Cde = extract_Record__R_Field_6.newFieldInGroup("extract_Record_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        extract_Record_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);

        extract_Record__R_Field_7 = extract_Record.newGroupInGroup("extract_Record__R_Field_7", "REDEFINE", extract_Record_Unit_Cde);
        extract_Record_Unit_Id_Cde = extract_Record__R_Field_7.newFieldInGroup("extract_Record_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        extract_Record_Unit_Rgn_Cde = extract_Record__R_Field_7.newFieldInGroup("extract_Record_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 1);
        extract_Record_Unit_Spcl_Dsgntn_Cde = extract_Record__R_Field_7.newFieldInGroup("extract_Record_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        extract_Record_Unit_Brnch_Group_Cde = extract_Record__R_Field_7.newFieldInGroup("extract_Record_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        extract_Record_Unit_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", FieldType.TIME);
        extract_Record_Empl_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10);

        extract_Record__R_Field_8 = extract_Record.newGroupInGroup("extract_Record__R_Field_8", "REDEFINE", extract_Record_Empl_Oprtr_Cde);
        extract_Record_Empl_Racf_Id = extract_Record__R_Field_8.newFieldInGroup("extract_Record_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        extract_Record_Empl_Sffx_Cde = extract_Record__R_Field_8.newFieldInGroup("extract_Record_Empl_Sffx_Cde", "EMPL-SFFX-CDE", FieldType.STRING, 2);
        extract_Record_Last_Chnge_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15);
        extract_Record_Last_Chnge_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Last_Chnge_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8);
        extract_Record_Step_Id = extract_Record.newFieldInGroup("extract_Record_Step_Id", "STEP-ID", FieldType.STRING, 6);
        extract_Record_Admin_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        extract_Record_Admin_Status_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4);
        extract_Record_Admin_Status_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME);
        extract_Record_Admin_Status_Updte_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8);
        extract_Record_Status_Cde = extract_Record.newFieldInGroup("extract_Record_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        extract_Record_Status_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME);
        extract_Record_Status_Updte_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Last_Updte_Dte = extract_Record.newFieldInGroup("extract_Record_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 8);
        extract_Record_Last_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME);
        extract_Record_Last_Updte_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Crprte_Status_Ind = extract_Record.newFieldInGroup("extract_Record_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1);
        extract_Record_Tiaa_Rcvd_Dte = extract_Record.newFieldInGroup("extract_Record_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE);
        extract_Record_Effctve_Dte = extract_Record.newFieldInGroup("extract_Record_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE);
        extract_Record_Trans_Dte = extract_Record.newFieldInGroup("extract_Record_Trans_Dte", "TRANS-DTE", FieldType.DATE);
        extract_Record_Cntrct_Nbr = extract_Record.newFieldInGroup("extract_Record_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        extract_Record_Pnd_Calendar_Days_In_Tiaa = extract_Record.newFieldInGroup("extract_Record_Pnd_Calendar_Days_In_Tiaa", "#CALENDAR-DAYS-IN-TIAA", 
            FieldType.NUMERIC, 5);
        extract_Record_Pnd_Business_Days_In_Tiaa = extract_Record.newFieldInGroup("extract_Record_Pnd_Business_Days_In_Tiaa", "#BUSINESS-DAYS-IN-TIAA", 
            FieldType.NUMERIC, 5);
        extract_Record_Extrnl_Pend_Rcv_Dte = extract_Record.newFieldInGroup("extract_Record_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", FieldType.TIME);
        extract_Record_Pnd_Received_In_Unit = extract_Record.newFieldInGroup("extract_Record_Pnd_Received_In_Unit", "#RECEIVED-IN-UNIT", FieldType.TIME);
        extract_Record_Pnd_Bsnss_Days_In_Unit = extract_Record.newFieldInGroup("extract_Record_Pnd_Bsnss_Days_In_Unit", "#BSNSS-DAYS-IN-UNIT", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Intrnl_Pend_Days = extract_Record.newFieldInGroup("extract_Record_Pnd_Intrnl_Pend_Days", "#INTRNL-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Extrnl_Pend_Days = extract_Record.newFieldInGroup("extract_Record_Pnd_Extrnl_Pend_Days", "#EXTRNL-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Extrnl_Pend_Bsnss_Days = extract_Record.newFieldInGroup("extract_Record_Pnd_Extrnl_Pend_Bsnss_Days", "#EXTRNL-PEND-BSNSS-DAYS", 
            FieldType.NUMERIC, 5, 1);
        extract_Record_Pnd_Partic_Sname = extract_Record.newFieldInGroup("extract_Record_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);

        extract_Record__R_Field_9 = extract_Record.newGroupInGroup("extract_Record__R_Field_9", "REDEFINE", extract_Record_Pnd_Partic_Sname);
        extract_Record_Pnd_Folder_Prefix = extract_Record__R_Field_9.newFieldInGroup("extract_Record_Pnd_Folder_Prefix", "#FOLDER-PREFIX", FieldType.STRING, 
            1);
        extract_Record_Pnd_Folder_Nbr = extract_Record__R_Field_9.newFieldInGroup("extract_Record_Pnd_Folder_Nbr", "#FOLDER-NBR", FieldType.NUMERIC, 6);

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Tbl_Racf = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Racf", "TBL-RACF", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Moc = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Moc", "TBL-MOC", FieldType.STRING, 1);
        pnd_Work_Record_Tbl_Calendar_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 
            5);
        pnd_Work_Record_Tbl_Tiaa_Bsnss_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 
            5);
        pnd_Work_Record_Tbl_Tiaa_Bsnssdays = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Bsnssdays", "TBL-TIAA-BSNSSDAYS", FieldType.NUMERIC, 
            5);
        pnd_Work_Record_Tbl_Business_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.NUMERIC, 
            5, 1);
        pnd_Work_Record_Tbl_Wpid_Act = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 1);
        pnd_Work_Record_Tbl_Wpid = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        pnd_Work_Record__R_Field_10 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_10", "REDEFINE", pnd_Work_Record_Tbl_Wpid);
        pnd_Work_Record_Tbl_Wpid_Action = pnd_Work_Record__R_Field_10.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Work_Record_Tbl_Status_Key = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Key", "TBL-STATUS-KEY", FieldType.STRING, 13);

        pnd_Work_Record__R_Field_11 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_11", "REDEFINE", pnd_Work_Record_Tbl_Status_Key);
        pnd_Work_Record_Tbl_Last_Chnge_Unit_Cde = pnd_Work_Record__R_Field_11.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chnge_Unit_Cde", "TBL-LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Status_Cde = pnd_Work_Record__R_Field_11.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde", "TBL-STATUS-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record_Tbl_Log_Unit_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Unit_Cde", "TBL-LOG-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Pin", "TBL-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Tbl_Step_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Step_Id", "TBL-STEP-ID", FieldType.STRING, 6);
        pnd_Work_Record_Tbl_Sort_Step_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Sort_Step_Id", "TBL-SORT-STEP-ID", FieldType.STRING, 6);
        pnd_Work_Record_Tbl_Contracts = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Contracts", "TBL-CONTRACTS", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Dte_Tme", "TBL-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_Record_Tbl_Tiaa_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Dte", "TBL-TIAA-DTE", FieldType.DATE);
        pnd_Work_Record_Tbl_Status_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Dte", "TBL-STATUS-DTE", FieldType.TIME);
        pnd_Work_Record_Return_Doc_Rec_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Return_Rcvd_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Tbl_Last_Chge_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chge_Dte", "TBL-LAST-CHGE-DTE", FieldType.NUMERIC, 
            15);
        pnd_Work_Record_Partic_Sname = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Partic_Sname", "PARTIC-SNAME", FieldType.STRING, 7);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Conv_Ctr = localVariables.newFieldInRecord("pnd_Conv_Ctr", "#CONV-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Nconv_Ctr = localVariables.newFieldInRecord("pnd_Nconv_Ctr", "#NCONV-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Rject_Ctr = localVariables.newFieldInRecord("pnd_Rject_Ctr", "#RJECT-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Total_Ctr = localVariables.newFieldInRecord("pnd_Total_Ctr", "#TOTAL-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_No_Extract = localVariables.newFieldInRecord("pnd_No_Extract", "#NO-EXTRACT", FieldType.NUMERIC, 2);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Tbl_Run_Date = localVariables.newFieldInRecord("pnd_Tbl_Run_Date", "#TBL-RUN-DATE", FieldType.STRING, 8);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Start_Date_A = localVariables.newFieldInRecord("pnd_Start_Date_A", "#START-DATE-A", FieldType.STRING, 8);
        pnd_Base_Date = localVariables.newFieldInRecord("pnd_Base_Date", "#BASE-DATE", FieldType.DATE);
        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 53);

        pnd_Tbl_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_12", "REDEFINE", pnd_Tbl_Key);

        pnd_Tbl_Key_Data_Nme = pnd_Tbl_Key__R_Field_12.newGroupInGroup("pnd_Tbl_Key_Data_Nme", "DATA-NME");
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Tbl_Table_Nme = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Tbl_Key_Field = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);

        pnd_Tbl_Prime_Key = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY");
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Key_Tbl_Table_Nme = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_Tbl_Key_Field = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Parm_Report_No = localVariables.newFieldInRecord("pnd_Parm_Report_No", "#PARM-REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Parm_Empl_Racf_Id = localVariables.newFieldInRecord("pnd_Parm_Empl_Racf_Id", "#PARM-EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Parm_Unit_Cde = localVariables.newFieldInRecord("pnd_Parm_Unit_Cde", "#PARM-UNIT-CDE", FieldType.STRING, 8);
        pnd_Parm_Floor = localVariables.newFieldInRecord("pnd_Parm_Floor", "#PARM-FLOOR", FieldType.NUMERIC, 2);
        pnd_Parm_Bldg = localVariables.newFieldInRecord("pnd_Parm_Bldg", "#PARM-BLDG", FieldType.STRING, 3);
        pnd_Parm_Drop_Off = localVariables.newFieldInRecord("pnd_Parm_Drop_Off", "#PARM-DROP-OFF", FieldType.STRING, 2);
        pnd_Parm_Run_Date = localVariables.newFieldInRecord("pnd_Parm_Run_Date", "#PARM-RUN-DATE", FieldType.STRING, 8);

        pnd_End_Var = localVariables.newGroupInRecord("pnd_End_Var", "#END-VAR");
        pnd_End_Var_Pnd_End_Tbl_Key = pnd_End_Var.newFieldInGroup("pnd_End_Var_Pnd_End_Tbl_Key", "#END-TBL-KEY", FieldType.STRING, 53);

        pnd_End_Var__R_Field_13 = pnd_End_Var.newGroupInGroup("pnd_End_Var__R_Field_13", "REDEFINE", pnd_End_Var_Pnd_End_Tbl_Key);
        pnd_End_Var_Pnd_End_Tbl_Authrty = pnd_End_Var__R_Field_13.newFieldInGroup("pnd_End_Var_Pnd_End_Tbl_Authrty", "#END-TBL-AUTHRTY", FieldType.STRING, 
            2);
        pnd_End_Var_Pnd_End_Tbl_Name = pnd_End_Var__R_Field_13.newFieldInGroup("pnd_End_Var_Pnd_End_Tbl_Name", "#END-TBL-NAME", FieldType.STRING, 20);
        pnd_End_Var_Pnd_End_Tbl_Code = pnd_End_Var__R_Field_13.newFieldInGroup("pnd_End_Var_Pnd_End_Tbl_Code", "#END-TBL-CODE", FieldType.STRING, 30);

        pnd_Gen_Var = localVariables.newGroupInRecord("pnd_Gen_Var", "#GEN-VAR");
        pnd_Gen_Var_Pnd_Gen_Tbl_Key = pnd_Gen_Var.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Key", "#GEN-TBL-KEY", FieldType.STRING, 53);

        pnd_Gen_Var__R_Field_14 = pnd_Gen_Var.newGroupInGroup("pnd_Gen_Var__R_Field_14", "REDEFINE", pnd_Gen_Var_Pnd_Gen_Tbl_Key);
        pnd_Gen_Var_Pnd_Gen_Tbl_Authrty = pnd_Gen_Var__R_Field_14.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Authrty", "#GEN-TBL-AUTHRTY", FieldType.STRING, 
            2);
        pnd_Gen_Var_Pnd_Gen_Tbl_Name = pnd_Gen_Var__R_Field_14.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Name", "#GEN-TBL-NAME", FieldType.STRING, 20);
        pnd_Gen_Var_Pnd_Gen_Tbl_Code = pnd_Gen_Var__R_Field_14.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Code", "#GEN-TBL-CODE", FieldType.STRING, 30);
        pnd_Gen_Var_Pnd_Gen_Tbl_Desc = pnd_Gen_Var.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Desc", "#GEN-TBL-DESC", FieldType.STRING, 30);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.PACKED_DECIMAL, 4);
        pnd_Timx = localVariables.newFieldInRecord("pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Receive_Date = localVariables.newFieldInRecord("pnd_Receive_Date", "#RECEIVE-DATE", FieldType.DATE);
        pnd_Report_Date = localVariables.newFieldInRecord("pnd_Report_Date", "#REPORT-DATE", FieldType.DATE);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.DATE);
        pnd_Report_Date_Time = localVariables.newFieldInRecord("pnd_Report_Date_Time", "#REPORT-DATE-TIME", FieldType.TIME);
        pnd_Intrnl_Pend_Date = localVariables.newFieldInRecord("pnd_Intrnl_Pend_Date", "#INTRNL-PEND-DATE", FieldType.TIME);
        pnd_Unit_Start_Date_Time = localVariables.newFieldInRecord("pnd_Unit_Start_Date_Time", "#UNIT-START-DATE-TIME", FieldType.TIME);
        pnd_Intrnl_Pend_Daysx = localVariables.newFieldInRecord("pnd_Intrnl_Pend_Daysx", "#INTRNL-PEND-DAYSX", FieldType.NUMERIC, 20, 2);
        pnd_Extrnl_Pend_Daysx = localVariables.newFieldInRecord("pnd_Extrnl_Pend_Daysx", "#EXTRNL-PEND-DAYSX", FieldType.PACKED_DECIMAL, 19, 1);
        pnd_Ext_Pend_Business_Days = localVariables.newFieldInRecord("pnd_Ext_Pend_Business_Days", "#EXT-PEND-BUSINESS-DAYS", FieldType.NUMERIC, 20, 2);
        pnd_Bsnss_Days_In_Unitx = localVariables.newFieldInRecord("pnd_Bsnss_Days_In_Unitx", "#BSNSS-DAYS-IN-UNITX", FieldType.PACKED_DECIMAL, 19, 1);
        pnd_No_Work_Days = localVariables.newFieldInRecord("pnd_No_Work_Days", "#NO-WORK-DAYS", FieldType.NUMERIC, 18);
        pnd_Valid_Units = localVariables.newFieldArrayInRecord("pnd_Valid_Units", "#VALID-UNITS", FieldType.STRING, 5, new DbsArrayController(1, 8));

        pnd_Report_Data = localVariables.newGroupInRecord("pnd_Report_Data", "#REPORT-DATA");
        pnd_Report_Data_Pnd_Rep_Unit_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Name", "#REP-UNIT-NAME", FieldType.STRING, 45);
        pnd_Report_Data_Pnd_Rep_Unit_Cde = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);

        pnd_Report_Data__R_Field_15 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_15", "REDEFINE", pnd_Report_Data_Pnd_Rep_Unit_Cde);
        pnd_Report_Data_Pnd_Rep_Unit_Id_Cde = pnd_Report_Data__R_Field_15.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Id_Cde", "#REP-UNIT-ID-CDE", FieldType.STRING, 
            5);
        pnd_Report_Data_Pnd_Rep_Wpid = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid", "#REP-WPID", FieldType.STRING, 6);

        pnd_Report_Data__R_Field_16 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_16", "REDEFINE", pnd_Report_Data_Pnd_Rep_Wpid);
        pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde = pnd_Report_Data__R_Field_16.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde", "#REP-WPID-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        pnd_Report_Data_Pnd_Rep_Wpid_Lob = pnd_Report_Data__R_Field_16.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Lob", "#REP-WPID-LOB", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Wpid_Mbp = pnd_Report_Data__R_Field_16.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Mbp", "#REP-WPID-MBP", FieldType.STRING, 
            1);
        pnd_Report_Data_Pnd_Rep_Wpid_Sbp = pnd_Report_Data__R_Field_16.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Sbp", "#REP-WPID-SBP", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Racf_Id = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Racf_Id", "#REP-RACF-ID", FieldType.STRING, 8);
        pnd_Report_Data_Pnd_Rep_Wpid_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Name", "#REP-WPID-NAME", FieldType.STRING, 45);
        pnd_Report_Data_Pnd_Rep_Empl_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Empl_Name", "#REP-EMPL-NAME", FieldType.STRING, 30);
        pnd_Report_Data_Pnd_Rep_Subtotal_Ind = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Subtotal_Ind", "#REP-SUBTOTAL-IND", FieldType.STRING, 
            1);

        pnd_Misc_Parm = localVariables.newGroupInRecord("pnd_Misc_Parm", "#MISC-PARM");
        pnd_Misc_Parm_Pnd_Wrk_Unit_Cde = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Cde", "#WRK-UNIT-CDE", FieldType.STRING, 8);

        pnd_Misc_Parm__R_Field_17 = pnd_Misc_Parm.newGroupInGroup("pnd_Misc_Parm__R_Field_17", "REDEFINE", pnd_Misc_Parm_Pnd_Wrk_Unit_Cde);
        pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde = pnd_Misc_Parm__R_Field_17.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde", "#WRK-UNIT-ID-CDE", FieldType.STRING, 
            5);
        pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix = pnd_Misc_Parm__R_Field_17.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix", "#WRK-UNIT-SUFFIX", FieldType.STRING, 
            3);
        pnd_Misc_Parm_Pnd_Save_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Save_Action", "#SAVE-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Wpid = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Misc_Parm_Pnd_Wpid_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Action", "#WPID-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Wpid_Code = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Code", "#WPID-CODE", FieldType.STRING, 6);
        pnd_Misc_Parm_Pnd_Wpid_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Count", "#WPID-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Wpid_Desc = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Desc", "#WPID-DESC", FieldType.STRING, 45);
        pnd_Misc_Parm_Pnd_Wpid_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Sname", "#WPID-SNAME", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Wpid_Text = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Text", "#WPID-TEXT", FieldType.STRING, 56);
        pnd_Misc_Parm_Pnd_Rep_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Rep_Ctr", "#REP-CTR", FieldType.PACKED_DECIMAL, 2);
        pnd_Misc_Parm_Pnd_Total_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count", "#TOTAL-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Sub_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Sub_Count", "#SUB-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Booklet_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Booklet_Ctr", "#BOOKLET-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Forms_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Forms_Ctr", "#FORMS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Inquire_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Inquire_Ctr", "#INQUIRE-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Research_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Research_Ctr", "#RESEARCH-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Trans_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Trans_Ctr", "#TRANS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Complaint_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Complaint_Ctr", "#COMPLAINT-CTR", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Misc_Parm_Pnd_Other_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Other_Ctr", "#OTHER-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Read_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Read_Count", "#READ-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Actve_Unque_Key = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Actve_Unque_Key", "#ACTVE-UNQUE-KEY", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Todays_Time = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Todays_Time", "#TODAYS-TIME", FieldType.TIME);
        pnd_Misc_Parm_Pnd_Return_Code = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 1);
        pnd_Misc_Parm_Pnd_Return_Msg = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 20);
        pnd_Misc_Parm_Pnd_Status_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Status_Sname", "#STATUS-SNAME", FieldType.STRING, 25);
        pnd_Misc_Parm_Pnd_Return_Doc_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Doc_Txt", "#RETURN-DOC-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Return_Rcvd_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Rcvd_Txt", "#RETURN-RCVD-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Confirmed = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Confirmed", "#CONFIRMED", FieldType.BOOLEAN, 1);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 5);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Int_Moc = localVariables.newFieldInRecord("pnd_Int_Moc", "#INT-MOC", FieldType.PACKED_DECIMAL, 9);
        pnd_Ext_Moc = localVariables.newFieldInRecord("pnd_Ext_Moc", "#EXT-MOC", FieldType.PACKED_DECIMAL, 9);
        pnd_Work_Moc = localVariables.newFieldInRecord("pnd_Work_Moc", "#WORK-MOC", FieldType.STRING, 1);
        pnd_Holiday = localVariables.newFieldInRecord("pnd_Holiday", "#HOLIDAY", FieldType.NUMERIC, 18);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Save_Unit = localVariables.newFieldInRecord("pnd_Save_Unit", "#SAVE-UNIT", FieldType.STRING, 8);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Run_Date = localVariables.newFieldInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Tbl_WpidOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_OLD", "Tbl_Wpid_OLD", FieldType.STRING, 6);
        sort01Tbl_Wpid_ActOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_Act_OLD", "Tbl_Wpid_Act_OLD", FieldType.STRING, 1);
        sort01Tbl_WpidCount613 = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT_613", "Tbl_Wpid_COUNT_613", FieldType.NUMERIC, 9);
        sort01Tbl_WpidCount = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT", "Tbl_Wpid_COUNT", FieldType.NUMERIC, 9);
        sort01Tbl_MocOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Moc_OLD", "Tbl_Moc_OLD", FieldType.STRING, 1);
        sort01Tbl_MocCount661 = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Moc_COUNT_661", "Tbl_Moc_COUNT_661", FieldType.NUMERIC, 9);
        sort01Tbl_MocCount = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Moc_COUNT", "Tbl_Moc_COUNT", FieldType.NUMERIC, 9);
        sort01Tbl_Log_Unit_CdeOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Log_Unit_Cde_OLD", "Tbl_Log_Unit_Cde_OLD", FieldType.STRING, 8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind.setInitialValue("A");
        pnd_Tbl_Prime_Key_Tbl_Table_Nme.setInitialValue("CWF-RPRT-RUN-TBL");
        pnd_Tbl_Prime_Key_Tbl_Key_Field.setInitialValue("D");
        pnd_Parm_Report_No.setInitialValue(21);
        pnd_Parm_Unit_Cde.setInitialValue("CWF");
        pnd_Parm_Type.setInitialValue("D");
        pnd_Valid_Units.getValue(1).setInitialValue("ACADM");
        pnd_Valid_Units.getValue(2).setInitialValue("AIIVC");
        pnd_Valid_Units.getValue(3).setInitialValue("ALOAN");
        pnd_Valid_Units.getValue(4).setInitialValue("APC  ");
        pnd_Valid_Units.getValue(5).setInitialValue("PRADJ");
        pnd_Valid_Units.getValue(6).setInitialValue("PRREC");
        pnd_Valid_Units.getValue(7).setInitialValue("STATS");
        pnd_Valid_Units.getValue(8).setInitialValue("TRSFR");
        pnd_Report_No.setInitialValue(6);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3801() throws Exception
    {
        super("Cwfb3801");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB3801", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_Timx.setValue(Global.getTIMX());                                                                                                                              //Natural: MOVE *TIMX TO #TIMX
        pnd_Report_Date.setValue(pnd_Timx);                                                                                                                               //Natural: MOVE #TIMX TO #REPORT-DATE
        pnd_Report_Date_Time.setValue(pnd_Timx);                                                                                                                          //Natural: MOVE #TIMX TO #REPORT-DATE-TIME
        pnd_Start_Date_A.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                 //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #START-DATE-A
        pnd_Comp_Date.setValue(pnd_Start_Date_A);                                                                                                                         //Natural: MOVE #START-DATE-A TO #COMP-DATE #PARM-RUN-DATE
        pnd_Parm_Run_Date.setValue(pnd_Start_Date_A);
        pnd_Base_Date.compute(new ComputeParameters(false, pnd_Base_Date), Global.getDATX().subtract(9999));                                                              //Natural: COMPUTE #BASE-DATE = *DATX - 9999
        pnd_Report_Data_Pnd_Rep_Subtotal_Ind.setValue("N");                                                                                                               //Natural: MOVE 'N' TO #REP-SUBTOTAL-IND
        pnd_Run_Date.setValue(pnd_Comp_Date);                                                                                                                             //Natural: MOVE #COMP-DATE TO #RUN-DATE
        pnd_New_Unit.setValue(true);                                                                                                                                      //Natural: MOVE TRUE TO #NEW-UNIT
        pnd_Page.setValue(0);                                                                                                                                             //Natural: MOVE 0 TO #PAGE
        //* *                                                                                                                                                             //Natural: FORMAT ( 1 ) LS = 140 PS = 60
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        //*  MAIN PROGRAM LOOP
        vw_cwf_Master_Index.startDatabaseRead                                                                                                                             //Natural: READ CWF-MASTER-INDEX BY CRPRTE-STATUS-CHNGE-DTE-KEY
        (
        "PROG",
        new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
        );
        PROG:
        while (condition(vw_cwf_Master_Index.readNextRow("PROG")))
        {
            if (condition(cwf_Master_Index_Crprte_Status_Ind.greater("0")))                                                                                               //Natural: IF CWF-MASTER-INDEX.CRPRTE-STATUS-IND GT '0'
            {
                if (true) break PROG;                                                                                                                                     //Natural: ESCAPE BOTTOM ( PROG. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!((cwf_Master_Index_Admin_Unit_Id_Cde.equals(pnd_Valid_Units.getValue("*")) && (((cwf_Master_Index_Admin_Status_Cde.equals("LINK")              //Natural: ACCEPT IF CWF-MASTER-INDEX.ADMIN-UNIT-ID-CDE = #VALID-UNITS ( * ) AND ( CWF-MASTER-INDEX.ADMIN-STATUS-CDE = 'LINK' OR CWF-MASTER-INDEX.STATUS-CDE = '1000' THRU '3999' OR CWF-MASTER-INDEX.STATUS-CDE = '4998' THRU '6999' OR CWF-MASTER-INDEX.STATUS-CDE = '7500' THRU '7999' )
                || (cwf_Master_Index_Status_Cde.greaterOrEqual("1000") && cwf_Master_Index_Status_Cde.lessOrEqual("3999"))) || (cwf_Master_Index_Status_Cde.greaterOrEqual("4998") 
                && cwf_Master_Index_Status_Cde.lessOrEqual("6999"))) || (cwf_Master_Index_Status_Cde.greaterOrEqual("7500") && cwf_Master_Index_Status_Cde.lessOrEqual("7999")))))))
            {
                continue;
            }
            if (condition(cwf_Master_Index_Tiaa_Rcvd_Dte.lessOrEqual(pnd_Base_Date)))                                                                                     //Natural: IF CWF-MASTER-INDEX.TIAA-RCVD-DTE LE #BASE-DATE
            {
                getReports().write(0, "TIAA DATE",cwf_Master_Index_Tiaa_Rcvd_Dte,"MAY BE IN ERROR FOR PIN",cwf_Master_Index_Pin_Nbr,"(",cwf_Master_Index_Rqst_Log_Dte_Tme, //Natural: WRITE 'TIAA DATE' CWF-MASTER-INDEX.TIAA-RCVD-DTE 'MAY BE IN ERROR FOR PIN' CWF-MASTER-INDEX.PIN-NBR '(' CWF-MASTER-INDEX.RQST-LOG-DTE-TME ') WPID = ' CWF-MASTER-INDEX.WORK-PRCSS-ID
                    ") WPID = ",cwf_Master_Index_Work_Prcss_Id);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Index_Rqst_Orgn_Cde.notEquals("J")))                                                                                                 //Natural: IF CWF-MASTER-INDEX.RQST-ORGN-CDE NE 'J'
            {
                //*  ---------------------
                //*  CALENDAR DAYS IN TIAA
                //*  ---------------------
                if (condition(cwf_Master_Index_Extrnl_Pend_Rcv_Dte.greater(getZero())))                                                                                   //Natural: IF CWF-MASTER-INDEX.EXTRNL-PEND-RCV-DTE GT 0
                {
                    pnd_Receive_Date.setValue(cwf_Master_Index_Extrnl_Pend_Rcv_Dte);                                                                                      //Natural: MOVE CWF-MASTER-INDEX.EXTRNL-PEND-RCV-DTE TO #RECEIVE-DATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Receive_Date.setValue(cwf_Master_Index_Tiaa_Rcvd_Dte);                                                                                            //Natural: MOVE CWF-MASTER-INDEX.TIAA-RCVD-DTE TO #RECEIVE-DATE
                }                                                                                                                                                         //Natural: END-IF
                extract_Record_Pnd_Calendar_Days_In_Tiaa.compute(new ComputeParameters(false, extract_Record_Pnd_Calendar_Days_In_Tiaa), pnd_Report_Date.subtract(pnd_Receive_Date)); //Natural: COMPUTE #CALENDAR-DAYS-IN-TIAA = #REPORT-DATE - #RECEIVE-DATE
                DbsUtil.callnat(Cwfn3901.class , getCurrentProcessState(), pnd_Receive_Date, pnd_Report_Date, pnd_Holiday);                                               //Natural: CALLNAT 'CWFN3901' #RECEIVE-DATE #REPORT-DATE #HOLIDAY
                if (condition(Global.isEscape())) return;
                extract_Record_Pnd_Business_Days_In_Tiaa.compute(new ComputeParameters(false, extract_Record_Pnd_Business_Days_In_Tiaa), extract_Record_Pnd_Calendar_Days_In_Tiaa.subtract(pnd_Holiday)); //Natural: COMPUTE #BUSINESS-DAYS-IN-TIAA = #CALENDAR-DAYS-IN-TIAA - #HOLIDAY
                //*  ---------------------
                //*  RECEIVED IN UNIT DATE
                //*  ---------------------
                if (condition(DbsUtil.maskMatches(cwf_Master_Index_Unit_Clock_Start_Dte_Tme,"YYYYMMDD")))                                                                 //Natural: IF CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME = MASK ( YYYYMMDD )
                {
                    extract_Record_Pnd_Received_In_Unit.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_Unit_Clock_Start_Dte_Tme);                  //Natural: MOVE EDITED CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME TO #RECEIVED-IN-UNIT ( EM = YYYYMMDDHHIISST )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Received_In_Unit.reset();                                                                                                          //Natural: RESET #RECEIVED-IN-UNIT
                }                                                                                                                                                         //Natural: END-IF
                //*  ------------------
                //*  EXTERNAL-PEND-DAYS
                //*  ------------------
                if (condition(cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("4500") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("4997")))                         //Natural: IF CWF-MASTER-INDEX.ADMIN-STATUS-CDE = '4500' THRU '4997'
                {
                    pnd_Extrnl_Pend_Daysx.compute(new ComputeParameters(false, pnd_Extrnl_Pend_Daysx), pnd_Report_Date_Time.subtract(cwf_Master_Index_Admin_Status_Updte_Dte_Tme)); //Natural: COMPUTE #EXTRNL-PEND-DAYSX = #REPORT-DATE-TIME - CWF-MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME
                    extract_Record_Pnd_Extrnl_Pend_Days.compute(new ComputeParameters(false, extract_Record_Pnd_Extrnl_Pend_Days), pnd_Extrnl_Pend_Daysx.divide(864000)); //Natural: COMPUTE #EXTRNL-PEND-DAYS = #EXTRNL-PEND-DAYSX / 864000
                    DbsUtil.callnat(Cwfn3900.class , getCurrentProcessState(), cwf_Master_Index_Admin_Status_Updte_Dte_Tme, pnd_Report_Date_Time, pnd_Ext_Pend_Business_Days); //Natural: CALLNAT 'CWFN3900' CWF-MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME #REPORT-DATE-TIME #EXT-PEND-BUSINESS-DAYS
                    if (condition(Global.isEscape())) return;
                    extract_Record_Pnd_Extrnl_Pend_Bsnss_Days.nadd(pnd_Ext_Pend_Business_Days);                                                                           //Natural: COMPUTE #EXTRNL-PEND-BSNSS-DAYS = #EXTRNL-PEND-BSNSS-DAYS + #EXT-PEND-BUSINESS-DAYS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Extrnl_Pend_Days.reset();                                                                                                          //Natural: RESET #EXTRNL-PEND-DAYS
                }                                                                                                                                                         //Natural: END-IF
                //*  ------------------
                //*  INTERNAL-PEND-DAYS
                //*  ------------------
                if (condition(cwf_Master_Index_Work_List_Ind.equals("2IP") || cwf_Master_Index_Work_List_Ind.equals("1RA")))                                              //Natural: IF CWF-MASTER-INDEX.WORK-LIST-IND = '2IP' OR = '1RA'
                {
                    if (condition(cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme.greater(" ")))                                                                                //Natural: IF CWF-MASTER-INDEX.INTRNL-PND-START-DTE-TME GT ' '
                    {
                        pnd_Intrnl_Pend_Date.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme);                             //Natural: MOVE EDITED CWF-MASTER-INDEX.INTRNL-PND-START-DTE-TME TO #INTRNL-PEND-DATE ( EM = YYYYMMDDHHIISST )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Intrnl_Pend_Date.setValue(cwf_Master_Index_Admin_Status_Updte_Dte_Tme);                                                                       //Natural: MOVE CWF-MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME TO #INTRNL-PEND-DATE
                    }                                                                                                                                                     //Natural: END-IF
                    DbsUtil.callnat(Cwfn3900.class , getCurrentProcessState(), pnd_Intrnl_Pend_Date, pnd_Report_Date_Time, pnd_Intrnl_Pend_Daysx);                        //Natural: CALLNAT 'CWFN3900' #INTRNL-PEND-DATE #REPORT-DATE-TIME #INTRNL-PEND-DAYSX
                    if (condition(Global.isEscape())) return;
                    extract_Record_Pnd_Intrnl_Pend_Days.compute(new ComputeParameters(false, extract_Record_Pnd_Intrnl_Pend_Days), pnd_Intrnl_Pend_Daysx.add((cwf_Master_Index_Intrnl_Pnd_Days.divide(10)))); //Natural: COMPUTE #INTRNL-PEND-DAYS = #INTRNL-PEND-DAYSX + ( CWF-MASTER-INDEX.INTRNL-PND-DAYS / 10 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Intrnl_Pend_Days.reset();                                                                                                          //Natural: RESET #INTRNL-PEND-DAYS
                }                                                                                                                                                         //Natural: END-IF
                //*  -------------------------------------------------------------
                //*  BUSINESS DAYS IN UNIT (LESS HOLIDAYS/WEEKENDS/INTERNAL PENDS)
                //*  -------------------------------------------------------------
                //*        COMPUTE HOLIDAYS/WEEKENDS BETWEEN DAYS
                if (condition(cwf_Master_Index_Unit_Clock_Start_Dte_Tme.greater(" ")))                                                                                    //Natural: IF CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME GT ' '
                {
                    pnd_Start_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Master_Index_Unit_Clock_Start_Dte_Tme);                                              //Natural: MOVE EDITED CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME TO #START-DATE ( EM = YYYYMMDD )
                    pnd_Unit_Start_Date_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_Unit_Clock_Start_Dte_Tme);                             //Natural: MOVE EDITED CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME TO #UNIT-START-DATE-TIME ( EM = YYYYMMDDHHIISST )
                    DbsUtil.callnat(Cwfn3901.class , getCurrentProcessState(), pnd_Start_Date, pnd_Report_Date, pnd_No_Work_Days);                                        //Natural: CALLNAT 'CWFN3901' #START-DATE #REPORT-DATE #NO-WORK-DAYS
                    if (condition(Global.isEscape())) return;
                    pnd_Bsnss_Days_In_Unitx.compute(new ComputeParameters(false, pnd_Bsnss_Days_In_Unitx), pnd_Report_Date_Time.subtract(pnd_Unit_Start_Date_Time));      //Natural: COMPUTE #BSNSS-DAYS-IN-UNITX = #REPORT-DATE-TIME - #UNIT-START-DATE-TIME
                    extract_Record_Pnd_Bsnss_Days_In_Unit.compute(new ComputeParameters(false, extract_Record_Pnd_Bsnss_Days_In_Unit), (pnd_Bsnss_Days_In_Unitx.divide(864000)).subtract(extract_Record_Pnd_Intrnl_Pend_Days).subtract(pnd_No_Work_Days)); //Natural: COMPUTE #BSNSS-DAYS-IN-UNIT = ( #BSNSS-DAYS-IN-UNITX / 864000 ) - #INTRNL-PEND-DAYS - #NO-WORK-DAYS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Bsnss_Days_In_Unit.setValue(0);                                                                                                    //Natural: MOVE 0 TO #BSNSS-DAYS-IN-UNIT
                }                                                                                                                                                         //Natural: END-IF
                //* *****************************************************************
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Index_Admin_Status_Cde.lessOrEqual("0899")))                                                                                         //Natural: IF CWF-MASTER-INDEX.ADMIN-STATUS-CDE LE '0899'
            {
                extract_Record_Pnd_Bsnss_Days_In_Unit.reset();                                                                                                            //Natural: RESET #BSNSS-DAYS-IN-UNIT #INTRNL-PEND-DAYS #RECEIVED-IN-UNIT
                extract_Record_Pnd_Intrnl_Pend_Days.reset();
                extract_Record_Pnd_Received_In_Unit.reset();
            }                                                                                                                                                             //Natural: END-IF
            //* *
            if (condition(cwf_Master_Index_Mj_Pull_Ind.equals("Y") || cwf_Master_Index_Mj_Pull_Ind.equals("R")))                                                          //Natural: IF CWF-MASTER-INDEX.MJ-PULL-IND = 'Y' OR = 'R'
            {
                extract_Record_Pnd_Folder_Prefix.setValue("M");                                                                                                           //Natural: MOVE 'M' TO #FOLDER-PREFIX
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                extract_Record_Pnd_Folder_Prefix.setValue("F");                                                                                                           //Natural: MOVE 'F' TO #FOLDER-PREFIX
            }                                                                                                                                                             //Natural: END-IF
            extract_Record_Pnd_Folder_Nbr.setValue(cwf_Master_Index_Physcl_Fldr_Id_Nbr);                                                                                  //Natural: MOVE CWF-MASTER-INDEX.PHYSCL-FLDR-ID-NBR TO #FOLDER-NBR
            //* *
            extract_Record.setValuesByName(vw_cwf_Master_Index);                                                                                                          //Natural: MOVE BY NAME CWF-MASTER-INDEX TO EXTRACT-RECORD
            //* *
            pnd_Work_Record_Tbl_Pin.setValue(extract_Record_Pin_Nbr);                                                                                                     //Natural: MOVE EXTRACT-RECORD.PIN-NBR TO #WORK-RECORD.TBL-PIN
            //*  INTERNAL
            if (condition(extract_Record_Rqst_Orgn_Cde.equals("I")))                                                                                                      //Natural: IF EXTRACT-RECORD.RQST-ORGN-CDE = 'I'
            {
                //*  FIRST IN SORT ORDER
                pnd_Work_Record_Tbl_Moc.setValue("1");                                                                                                                    //Natural: MOVE '1' TO #WORK-RECORD.TBL-MOC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  2ND IN SORT ORDER
                pnd_Work_Record_Tbl_Moc.setValue("2");                                                                                                                    //Natural: MOVE '2' TO #WORK-RECORD.TBL-MOC
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Record_Tbl_Wpid.setValue(extract_Record_Work_Prcss_Id);                                                                                              //Natural: MOVE EXTRACT-RECORD.WORK-PRCSS-ID TO #WORK-RECORD.TBL-WPID
            if (condition(DbsUtil.maskMatches(extract_Record_Work_Prcss_Id,"NN....")))                                                                                    //Natural: IF EXTRACT-RECORD.WORK-PRCSS-ID = MASK ( NN.... )
            {
                pnd_Work_Record_Tbl_Wpid_Act.setValue("Z");                                                                                                               //Natural: MOVE 'Z' TO #WORK-RECORD.TBL-WPID-ACT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Record_Tbl_Wpid_Act.setValue(extract_Record_Work_Prcss_Id);                                                                                      //Natural: MOVE EXTRACT-RECORD.WORK-PRCSS-ID TO #WORK-RECORD.TBL-WPID-ACT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Record_Tbl_Pin.setValue(extract_Record_Pin_Nbr);                                                                                                     //Natural: MOVE EXTRACT-RECORD.PIN-NBR TO #WORK-RECORD.TBL-PIN
            pnd_Work_Record_Tbl_Last_Chnge_Unit_Cde.setValue(extract_Record_Last_Chnge_Unit_Cde);                                                                         //Natural: MOVE EXTRACT-RECORD.LAST-CHNGE-UNIT-CDE TO #WORK-RECORD.TBL-LAST-CHNGE-UNIT-CDE
            pnd_Work_Record_Tbl_Log_Unit_Cde.setValue(extract_Record_Admin_Unit_Cde);                                                                                     //Natural: MOVE EXTRACT-RECORD.ADMIN-UNIT-CDE TO #WORK-RECORD.TBL-LOG-UNIT-CDE
            pnd_Work_Record_Tbl_Racf.setValue(extract_Record_Empl_Racf_Id);                                                                                               //Natural: MOVE EXTRACT-RECORD.EMPL-RACF-ID TO #WORK-RECORD.TBL-RACF
            pnd_Work_Record_Tbl_Last_Chge_Dte.setValue(extract_Record_Last_Chnge_Dte_Tme);                                                                                //Natural: MOVE EXTRACT-RECORD.LAST-CHNGE-DTE-TME TO #WORK-RECORD.TBL-LAST-CHGE-DTE
            pnd_Work_Record_Tbl_Log_Dte_Tme.setValue(extract_Record_Rqst_Log_Dte_Tme);                                                                                    //Natural: MOVE EXTRACT-RECORD.RQST-LOG-DTE-TME TO #WORK-RECORD.TBL-LOG-DTE-TME
            pnd_Work_Record_Tbl_Tiaa_Dte.setValue(extract_Record_Tiaa_Rcvd_Dte);                                                                                          //Natural: MOVE EXTRACT-RECORD.TIAA-RCVD-DTE TO #WORK-RECORD.TBL-TIAA-DTE
            pnd_Work_Record_Tbl_Step_Id.setValue(extract_Record_Step_Id);                                                                                                 //Natural: MOVE EXTRACT-RECORD.STEP-ID TO #WORK-RECORD.TBL-STEP-ID
            if (condition(pnd_Report_Data_Pnd_Rep_Subtotal_Ind.greater(" ")))                                                                                             //Natural: IF #REP-SUBTOTAL-IND GT ' '
            {
                pnd_Work_Record_Tbl_Sort_Step_Id.setValue(extract_Record_Step_Id);                                                                                        //Natural: MOVE EXTRACT-RECORD.STEP-ID TO #WORK-RECORD.TBL-SORT-STEP-ID
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Record_Tbl_Status_Cde.setValue(extract_Record_Admin_Status_Cde);                                                                                     //Natural: MOVE EXTRACT-RECORD.ADMIN-STATUS-CDE TO #WORK-RECORD.TBL-STATUS-CDE
            pnd_Work_Record_Tbl_Status_Dte.setValue(extract_Record_Admin_Status_Updte_Dte_Tme);                                                                           //Natural: MOVE EXTRACT-RECORD.ADMIN-STATUS-UPDTE-DTE-TME TO #WORK-RECORD.TBL-STATUS-DTE
            pnd_Work_Record_Tbl_Contracts.setValue(extract_Record_Cntrct_Nbr);                                                                                            //Natural: MOVE EXTRACT-RECORD.CNTRCT-NBR TO #WORK-RECORD.TBL-CONTRACTS
            pnd_Work_Record_Tbl_Calendar_Days.setValue(extract_Record_Pnd_Calendar_Days_In_Tiaa);                                                                         //Natural: MOVE EXTRACT-RECORD.#CALENDAR-DAYS-IN-TIAA TO #WORK-RECORD.TBL-CALENDAR-DAYS
            pnd_Work_Record_Tbl_Tiaa_Bsnss_Days.setValue(extract_Record_Pnd_Business_Days_In_Tiaa);                                                                       //Natural: MOVE EXTRACT-RECORD.#BUSINESS-DAYS-IN-TIAA TO #WORK-RECORD.TBL-TIAA-BSNSS-DAYS
            pnd_Work_Record_Return_Doc_Rec_Dte_Tme.setValue(extract_Record_Extrnl_Pend_Rcv_Dte);                                                                          //Natural: MOVE EXTRACT-RECORD.EXTRNL-PEND-RCV-DTE TO #WORK-RECORD.RETURN-DOC-REC-DTE-TME
            pnd_Work_Record_Return_Rcvd_Dte_Tme.setValue(extract_Record_Pnd_Received_In_Unit);                                                                            //Natural: MOVE EXTRACT-RECORD.#RECEIVED-IN-UNIT TO #WORK-RECORD.RETURN-RCVD-DTE-TME
            pnd_Work_Record_Tbl_Business_Days.setValue(extract_Record_Pnd_Bsnss_Days_In_Unit);                                                                            //Natural: MOVE EXTRACT-RECORD.#BSNSS-DAYS-IN-UNIT TO #WORK-RECORD.TBL-BUSINESS-DAYS
            pnd_Work_Record_Partic_Sname.setValue(extract_Record_Pnd_Partic_Sname);                                                                                       //Natural: MOVE EXTRACT-RECORD.#PARTIC-SNAME TO #WORK-RECORD.PARTIC-SNAME
            //* ***********************************************************************
            getSort().writeSortInData(pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Work_Record_Tbl_Moc, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid,                  //Natural: END-ALL
                pnd_Work_Record_Return_Doc_Rec_Dte_Tme, pnd_Work_Record_Tbl_Tiaa_Dte, pnd_Work_Record_Tbl_Status_Cde, pnd_Work_Record_Tbl_Racf, pnd_Work_Record_Tbl_Calendar_Days, 
                pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, pnd_Work_Record_Tbl_Business_Days, pnd_Work_Record_Tbl_Pin, pnd_Work_Record_Tbl_Step_Id, pnd_Work_Record_Tbl_Status_Key, 
                pnd_Work_Record_Tbl_Contracts, pnd_Work_Record_Tbl_Status_Dte, pnd_Work_Record_Return_Rcvd_Dte_Tme, pnd_Work_Record_Partic_Sname);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Work_Record_Tbl_Moc, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Return_Doc_Rec_Dte_Tme,  //Natural: SORT BY #WORK-RECORD.TBL-LOG-UNIT-CDE #WORK-RECORD.TBL-MOC #WORK-RECORD.TBL-WPID-ACT #WORK-RECORD.TBL-WPID #WORK-RECORD.RETURN-DOC-REC-DTE-TME #WORK-RECORD.TBL-TIAA-DTE #WORK-RECORD.TBL-STATUS-CDE #WORK-RECORD.TBL-RACF USING #WORK-RECORD.TBL-CALENDAR-DAYS #WORK-RECORD.TBL-TIAA-BSNSS-DAYS #WORK-RECORD.TBL-BUSINESS-DAYS #WORK-RECORD.TBL-PIN #WORK-RECORD.TBL-STEP-ID #WORK-RECORD.TBL-STATUS-KEY #WORK-RECORD.TBL-CONTRACTS #WORK-RECORD.TBL-STATUS-DTE #WORK-RECORD.RETURN-RCVD-DTE-TME #WORK-RECORD.PARTIC-SNAME
            pnd_Work_Record_Tbl_Tiaa_Dte, pnd_Work_Record_Tbl_Status_Cde, pnd_Work_Record_Tbl_Racf);
        sort01Tbl_WpidCount613.setDec(new DbsDecimal(0));
        sort01Tbl_WpidCount.setDec(new DbsDecimal(0));
        sort01Tbl_MocCount661.setDec(new DbsDecimal(0));
        sort01Tbl_MocCount.setDec(new DbsDecimal(0));
        sort01Tbl_MocCount661.setDec(new DbsDecimal(0));
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Work_Record_Tbl_Moc, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, 
            pnd_Work_Record_Return_Doc_Rec_Dte_Tme, pnd_Work_Record_Tbl_Tiaa_Dte, pnd_Work_Record_Tbl_Status_Cde, pnd_Work_Record_Tbl_Racf, pnd_Work_Record_Tbl_Calendar_Days, 
            pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, pnd_Work_Record_Tbl_Business_Days, pnd_Work_Record_Tbl_Pin, pnd_Work_Record_Tbl_Step_Id, pnd_Work_Record_Tbl_Status_Key, 
            pnd_Work_Record_Tbl_Contracts, pnd_Work_Record_Tbl_Status_Dte, pnd_Work_Record_Return_Rcvd_Dte_Tme, pnd_Work_Record_Partic_Sname)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            sort01Tbl_WpidCount613.setInt(sort01Tbl_WpidCount613.getInt() + 1);
            sort01Tbl_WpidCount.setInt(sort01Tbl_WpidCount.getInt() + 1);
            sort01Tbl_MocCount661.setInt(sort01Tbl_MocCount661.getInt() + 1);
            sort01Tbl_MocCount.setInt(sort01Tbl_MocCount.getInt() + 1);
            sort01Tbl_MocCount661.setInt(sort01Tbl_MocCount661.getInt() + 1);
            pnd_Report_Data_Pnd_Rep_Unit_Cde.setValue(pnd_Work_Record_Tbl_Log_Unit_Cde);                                                                                  //Natural: MOVE TBL-LOG-UNIT-CDE TO #REP-UNIT-CDE
            if (condition(pnd_New_Unit.getBoolean()))                                                                                                                     //Natural: IF #NEW-UNIT
            {
                DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Report_Data_Pnd_Rep_Unit_Name);                          //Natural: CALLNAT 'CWFN1103' TBL-LOG-UNIT-CDE #REP-UNIT-NAME
                if (condition(Global.isEscape())) return;
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
                pnd_Save_Unit.setValue(pnd_Work_Record_Tbl_Log_Unit_Cde);                                                                                                 //Natural: MOVE #WORK-RECORD.TBL-LOG-UNIT-CDE TO #SAVE-UNIT
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet541 = 0;                                                                                                                             //Natural: AT TOP OF PAGE ( 1 );//Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACTION;//Natural: VALUE 'B'
            if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("B"))))
            {
                decideConditionsMet541++;
                pnd_Misc_Parm_Pnd_Booklet_Ctr.nadd(1);                                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("F"))))
            {
                decideConditionsMet541++;
                pnd_Misc_Parm_Pnd_Forms_Ctr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #FORMS-CTR
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("I"))))
            {
                decideConditionsMet541++;
                pnd_Misc_Parm_Pnd_Inquire_Ctr.nadd(1);                                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("R"))))
            {
                decideConditionsMet541++;
                pnd_Misc_Parm_Pnd_Research_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("T"))))
            {
                decideConditionsMet541++;
                pnd_Misc_Parm_Pnd_Trans_Ctr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TRANS-CTR
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("X"))))
            {
                decideConditionsMet541++;
                pnd_Misc_Parm_Pnd_Complaint_Ctr.nadd(1);                                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR
            }                                                                                                                                                             //Natural: VALUES '0':'9'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("0' : '9"))))
            {
                decideConditionsMet541++;
                pnd_Misc_Parm_Pnd_Other_Ctr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #OTHER-CTR
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet541 > 0))
            {
                pnd_Misc_Parm_Pnd_Sub_Count.nadd(1);                                                                                                                      //Natural: ADD 1 TO #SUB-COUNT
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            DbsUtil.callnat(Cwfn5390.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Wpid, pnd_Misc_Parm_Pnd_Wpid_Sname);                                           //Natural: CALLNAT 'CWFN5390' #WORK-RECORD.TBL-WPID #WPID-SNAME
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn1102.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Status_Key, pnd_Misc_Parm_Pnd_Status_Sname);                                   //Natural: CALLNAT 'CWFN1102' #WORK-RECORD.TBL-STATUS-KEY #STATUS-SNAME
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Work_Record_Return_Doc_Rec_Dte_Tme.greater(getZero())))                                                                                     //Natural: IF #WORK-RECORD.RETURN-DOC-REC-DTE-TME GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Doc_Txt.setValueEdited(pnd_Work_Record_Return_Doc_Rec_Dte_Tme,new ReportEditMask("MM/DD/YY"));                                   //Natural: MOVE EDITED #WORK-RECORD.RETURN-DOC-REC-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Doc_Txt.setValue(" ");                                                                                                           //Natural: MOVE ' ' TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Return_Rcvd_Dte_Tme.greater(getZero())))                                                                                        //Natural: IF #WORK-RECORD.RETURN-RCVD-DTE-TME GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValueEdited(pnd_Work_Record_Return_Rcvd_Dte_Tme,new ReportEditMask("MM/DD/YY"));                                     //Natural: MOVE EDITED #WORK-RECORD.RETURN-RCVD-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValue(" ");                                                                                                          //Natural: MOVE ' ' TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Misc_Parm_Pnd_Save_Action.notEquals(pnd_Work_Record_Tbl_Wpid_Act)))                                                                         //Natural: IF #SAVE-ACTION NE #WORK-RECORD.TBL-WPID-ACT
            {
                pnd_Misc_Parm_Pnd_Save_Action.setValue(pnd_Work_Record_Tbl_Wpid_Act);                                                                                     //Natural: MOVE #WORK-RECORD.TBL-WPID-ACT TO #SAVE-ACTION
                short decideConditionsMet577 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #SAVE-ACTION;//Natural: VALUE 'B'
                if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("B"))))
                {
                    decideConditionsMet577++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"BOOKLETS    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'BOOKLETS    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("F"))))
                {
                    decideConditionsMet577++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"FORMS       :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'FORMS       :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("I"))))
                {
                    decideConditionsMet577++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"INQUIRY     :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'INQUIRY     :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("R"))))
                {
                    decideConditionsMet577++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"RESEARCH    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'RESEARCH    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("T"))))
                {
                    decideConditionsMet577++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TRANSACTIONS:",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'TRANSACTIONS:' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("X"))))
                {
                    decideConditionsMet577++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"COMPLAINTS  :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'COMPLAINTS  :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("Z"))))
                {
                    decideConditionsMet577++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"OTHERS      :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'OTHERS      :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            extract_Record_Pnd_Partic_Sname.reset();                                                                                                                      //Natural: RESET #PARTIC-SNAME
            pdaCwfa5372.getCwfa5372_Pnd_Pin_Key().setValue(pnd_Work_Record_Tbl_Pin);                                                                                      //Natural: ASSIGN CWFA5372.#PIN-KEY := #WORK-RECORD.TBL-PIN
            //*  MQ OPEN
            DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                  //Natural: FETCH RETURN 'MDMP0011'
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn5372.class , getCurrentProcessState(), pdaCwfa5372.getCwfa5372_Pnd_Pin_Key(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Name(),                    //Natural: CALLNAT 'CWFN5372' CWFA5372.#PIN-KEY CWFA5372.#PASS-NAME CWFA5372.#PASS-SSN CWFA5372.#PASS-TLC CWFA5372.#PASS-DOB CWFA5372.#PASS-FOUND
                pdaCwfa5372.getCwfa5372_Pnd_Pass_Ssn(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Tlc(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Dob(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Found());
            if (condition(Global.isEscape())) return;
            //*  MQ CLOSE
            DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                  //Natural: FETCH RETURN 'MDMP0012'
            if (condition(Global.isEscape())) return;
            if (condition(pdaCwfa5372.getCwfa5372_Pnd_Pass_Found().getBoolean()))                                                                                         //Natural: IF #PASS-FOUND
            {
                pnd_Work_Record_Partic_Sname.setValue(pdaCwfa5372.getCwfa5372_Pnd_Pass_Name());                                                                           //Natural: MOVE CWFA5372.#PASS-NAME TO #WORK-RECORD.PARTIC-SNAME
                //*  PIN-EXP
            }                                                                                                                                                             //Natural: END-IF
            //* *
            getReports().display(1, "//RECEIVED/AT TIAA",                                                                                                                 //Natural: DISPLAY ( 1 ) '//RECEIVED/AT TIAA' #WORK-RECORD.TBL-TIAA-DTE ( EM = MM/DD/YY ) '//DOC/REC"D' #RETURN-DOC-TXT '//REC"D IN/UNIT' #RETURN-RCVD-TXT 'BSNSS/DAYS/IN/UNIT' #WORK-RECORD.TBL-BUSINESS-DAYS ( EM = ZZZ9.9 ) '//WORK/PROCESS' #WPID-SNAME ( IS = ON ) '//WORK/STEP' #WORK-RECORD.TBL-STEP-ID '///STATUS' #STATUS-SNAME ( AL = 15 ) '//STATUS/DATE' #WORK-RECORD.TBL-STATUS-DTE ( EM = MM/DD/YY ) 'CLNDR/DAYS/IN/TIAA' #WORK-RECORD.TBL-CALENDAR-DAYS ( EM = ZZZ9 ) 'BSNSS/DAYS/IN/TIAA' #WORK-RECORD.TBL-TIAA-BSNSS-DAYS ( EM = ZZZ9 ) '//PARTIC/NAME' #WORK-RECORD.PARTIC-SNAME '///PIN' #WORK-RECORD.TBL-PIN ( EM = 999999999999 ) '//CONTRACT/NUMBER' #WORK-RECORD.TBL-CONTRACTS '///EMPLOYEE' #WORK-RECORD.TBL-RACF
            		pnd_Work_Record_Tbl_Tiaa_Dte, new ReportEditMask ("MM/DD/YY"),"//DOC/REC'D",
            		pnd_Misc_Parm_Pnd_Return_Doc_Txt,"//REC'D IN/UNIT",
            		pnd_Misc_Parm_Pnd_Return_Rcvd_Txt,"BSNSS/DAYS/IN/UNIT",
            		pnd_Work_Record_Tbl_Business_Days, new ReportEditMask ("ZZZ9.9"),"//WORK/PROCESS",
            		pnd_Misc_Parm_Pnd_Wpid_Sname, new IdenticalSuppress(true),"//WORK/STEP",
            		pnd_Work_Record_Tbl_Step_Id,"///STATUS",
            		pnd_Misc_Parm_Pnd_Status_Sname, new AlphanumericLength (15),"//STATUS/DATE",
            		pnd_Work_Record_Tbl_Status_Dte, new ReportEditMask ("MM/DD/YY"),"CLNDR/DAYS/IN/TIAA",
            		pnd_Work_Record_Tbl_Calendar_Days, new ReportEditMask ("ZZZ9"),"BSNSS/DAYS/IN/TIAA",
            		pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, new ReportEditMask ("ZZZ9"),"//PARTIC/NAME",
            		pnd_Work_Record_Partic_Sname,"///PIN",
            		pnd_Work_Record_Tbl_Pin, new ReportEditMask ("999999999999"),"//CONTRACT/NUMBER",
            		pnd_Work_Record_Tbl_Contracts,"///EMPLOYEE",
            		pnd_Work_Record_Tbl_Racf);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    '///PIN' #WORK-RECORD.TBL-PIN (EM=9999999)
            //*                                                                                                                                                           //Natural: AT BREAK OF #WORK-RECORD.TBL-WPID;//Natural: AT BREAK OF #WORK-RECORD.TBL-WPID-ACT;//Natural: AT BREAK OF TBL-MOC;//Natural: AT BREAK OF TBL-LOG-UNIT-CDE
            //*  READ-2.
            sort01Tbl_WpidOld.setValue(pnd_Work_Record_Tbl_Wpid);                                                                                                         //Natural: END-SORT
            sort01Tbl_Wpid_ActOld.setValue(pnd_Work_Record_Tbl_Wpid_Act);
            sort01Tbl_MocOld.setValue(pnd_Work_Record_Tbl_Moc);
            sort01Tbl_Log_Unit_CdeOld.setValue(pnd_Work_Record_Tbl_Log_Unit_Cde);
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            sort01Tbl_WpidCount613.resetBreak();
            sort01Tbl_WpidCount.resetBreak();
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //* ***********************************************************************
    }                                                                                                                                                                     //Natural: ON ERROR

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page.nadd(1);                                                                                                                                     //Natural: COMPUTE #PAGE = #PAGE + 1
                    getReports().write(1, ReportOption.NOTITLE,"CWFB3801",pnd_Env,new TabSetting(52),"CORPORATE WORKFLOW FACILITIES",new TabSetting(124),"PAGE",pnd_Page, //Natural: WRITE ( 1 ) NOTITLE 'CWFB3801' #ENV 52T 'CORPORATE WORKFLOW FACILITIES' 124T 'PAGE' #PAGE ( NL = 5 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 48T 'PREMIUM DIVISION  REPORT OF OPEN CASES' 124T *TIMX ( EM = HH':'II' 'AP ) / 61T '(NOT PENDED)'
                        new NumericLength (5), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
                        TabSetting(48),"PREMIUM DIVISION  REPORT OF OPEN CASES",new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new 
                        TabSetting(61),"(NOT PENDED)");
                    if (condition(pnd_Work_Record_Tbl_Moc.equals("1")))                                                                                                   //Natural: IF #WORK-RECORD.TBL-MOC = '1'
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(57),"INTERNALLY GENERATED");                                                            //Natural: WRITE ( 1 ) 57T 'INTERNALLY GENERATED'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(57),"EXTERNALLY GENERATED");                                                            //Natural: WRITE ( 1 ) 57T 'EXTERNALLY GENERATED'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,"UNIT      :",pnd_Report_Data_Pnd_Rep_Unit_Cde,pnd_Report_Data_Pnd_Rep_Unit_Name);                         //Natural: WRITE ( 1 ) 'UNIT      :' #REP-UNIT-CDE #REP-UNIT-NAME
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,"*************** START OF ERROR LOG *****************",NEWLINE,"ERROR NO----->",Global.getERROR_NR(),NEWLINE,"LINE--------->",      //Natural: WRITE / '*************** START OF ERROR LOG *****************' / 'ERROR NO----->' *ERROR-NR / 'LINE--------->' *ERROR-LINE / 'PROGRAM------>' *PROGRAM / 'PARM PIN----->' CWF-MASTER-INDEX.PIN-NBR / 'PARM WPID---->' CWF-MASTER-INDEX.WORK-PRCSS-ID / 'PARM RQST NO->' CWF-MASTER-INDEX.RQST-LOG-DTE-TME / '**************** END OF ERROR LOG ******************'
            Global.getERROR_LINE(),NEWLINE,"PROGRAM------>",Global.getPROGRAM(),NEWLINE,"PARM PIN----->",cwf_Master_Index_Pin_Nbr,NEWLINE,"PARM WPID---->",
            cwf_Master_Index_Work_Prcss_Id,NEWLINE,"PARM RQST NO->",cwf_Master_Index_Rqst_Log_Dte_Tme,NEWLINE,"**************** END OF ERROR LOG ******************");
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Work_Record_Tbl_WpidIsBreak = pnd_Work_Record_Tbl_Wpid.isBreak(endOfData);
        boolean pnd_Work_Record_Tbl_Wpid_ActIsBreak = pnd_Work_Record_Tbl_Wpid_Act.isBreak(endOfData);
        boolean pnd_Work_Record_Tbl_MocIsBreak = pnd_Work_Record_Tbl_Moc.isBreak(endOfData);
        boolean pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak = pnd_Work_Record_Tbl_Log_Unit_Cde.isBreak(endOfData);
        if (condition(pnd_Work_Record_Tbl_WpidIsBreak || pnd_Work_Record_Tbl_Wpid_ActIsBreak || pnd_Work_Record_Tbl_MocIsBreak || pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak))
        {
            pnd_Misc_Parm_Pnd_Wpid.setValue(sort01Tbl_WpidOld);                                                                                                           //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID ) TO #WPID
            pnd_Misc_Parm_Pnd_Wpid_Action.setValue(sort01Tbl_Wpid_ActOld);                                                                                                //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID-ACT ) TO #WPID-ACTION
            pnd_Misc_Parm_Pnd_Wpid_Count.setValue(sort01Tbl_WpidCount613);                                                                                                //Natural: MOVE COUNT ( #WORK-RECORD.TBL-WPID ) TO #WPID-COUNT
            short decideConditionsMet617 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
            if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("B"))))
            {
                decideConditionsMet617++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR BOOKLETS FOR WPID....:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR BOOKLETS FOR WPID....:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("F"))))
            {
                decideConditionsMet617++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR FORMS FOR WPID.......:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR FORMS FOR WPID.......:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("I"))))
            {
                decideConditionsMet617++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR INQUIRY FOR WPID.....:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR INQUIRY FOR WPID.....:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("R"))))
            {
                decideConditionsMet617++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR RESEARCH FOR WPID....:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR RESEARCH FOR WPID....:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("T"))))
            {
                decideConditionsMet617++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR TRANSACTIONS FOR WPID:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR TRANSACTIONS FOR WPID:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("X"))))
            {
                decideConditionsMet617++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR COMPLAINTS FOR WPID..:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR COMPLAINTS FOR WPID..:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUES 'Z'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("Z"))))
            {
                decideConditionsMet617++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OTHER OPEN REQUESTS FOR WPID...........:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OTHER OPEN REQUESTS FOR WPID...........:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet617 > 0))
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),pnd_Misc_Parm_Pnd_Wpid_Text," ",pnd_Misc_Parm_Pnd_Wpid_Count, new                    //Natural: WRITE ( 1 ) / 3T #WPID-TEXT ' ' #WPID-COUNT ( AD = OI ) /
                    FieldAttributes ("AD=OI"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            sort01Tbl_WpidCount613.setDec(new DbsDecimal(0));                                                                                                             //Natural: END-BREAK
        }
        if (condition(pnd_Work_Record_Tbl_Wpid_ActIsBreak || pnd_Work_Record_Tbl_MocIsBreak || pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak))
        {
            pnd_Misc_Parm_Pnd_Total_Count.nadd(pnd_Misc_Parm_Pnd_Sub_Count);                                                                                              //Natural: COMPUTE #TOTAL-COUNT = #TOTAL-COUNT + #SUB-COUNT
            pnd_Misc_Parm_Pnd_Wpid_Action.setValue(sort01Tbl_Wpid_ActOld);                                                                                                //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID-ACT ) TO #WPID-ACTION
            short decideConditionsMet641 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
            if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("B"))))
            {
                decideConditionsMet641++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR BOOKLETS (ALL WPIDS).....:  ",pnd_Misc_Parm_Pnd_Booklet_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR BOOKLETS (ALL WPIDS).....:  ' #BOOKLET-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("F"))))
            {
                decideConditionsMet641++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR FORMS (ALL WPIDS)........:  ",pnd_Misc_Parm_Pnd_Forms_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR FORMS (ALL WPIDS)........:  ' #FORMS-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("I"))))
            {
                decideConditionsMet641++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR INQUIRY (ALL WPIDS)......:  ",pnd_Misc_Parm_Pnd_Inquire_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR INQUIRY (ALL WPIDS)......:  ' #INQUIRE-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("R"))))
            {
                decideConditionsMet641++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR RESEARCH (ALL WPIDS).....:  ",pnd_Misc_Parm_Pnd_Research_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR RESEARCH (ALL WPIDS).....:  ' #RESEARCH-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("T"))))
            {
                decideConditionsMet641++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR TRANSACTIONS (ALL WPIDS).:  ",pnd_Misc_Parm_Pnd_Trans_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR TRANSACTIONS (ALL WPIDS).:  ' #TRANS-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("X"))))
            {
                decideConditionsMet641++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR COMPLAINTS (ALL WPIDS)...:  ",pnd_Misc_Parm_Pnd_Complaint_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR COMPLAINTS (ALL WPIDS)...:  ' #COMPLAINT-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'Z'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("Z"))))
            {
                decideConditionsMet641++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OTHER OPEN REQUESTS (ALL WPIDS)............:  ",pnd_Misc_Parm_Pnd_Other_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OTHER OPEN REQUESTS (ALL WPIDS)............:  ' #OTHER-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Misc_Parm_Pnd_Booklet_Ctr.reset();                                                                                                                        //Natural: RESET #BOOKLET-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANS-CTR #COMPLAINT-CTR #OTHER-CTR #SUB-COUNT
            pnd_Misc_Parm_Pnd_Forms_Ctr.reset();
            pnd_Misc_Parm_Pnd_Inquire_Ctr.reset();
            pnd_Misc_Parm_Pnd_Research_Ctr.reset();
            pnd_Misc_Parm_Pnd_Trans_Ctr.reset();
            pnd_Misc_Parm_Pnd_Complaint_Ctr.reset();
            pnd_Misc_Parm_Pnd_Other_Ctr.reset();
            pnd_Misc_Parm_Pnd_Sub_Count.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Work_Record_Tbl_MocIsBreak || pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak))
        {
            pnd_Work_Moc.setValue(sort01Tbl_MocOld);                                                                                                                      //Natural: MOVE OLD ( TBL-MOC ) TO #WORK-MOC
            if (condition(pnd_Work_Moc.equals("1")))                                                                                                                      //Natural: IF #WORK-MOC = '1'
            {
                pnd_Int_Moc.setValue(sort01Tbl_MocCount661);                                                                                                              //Natural: MOVE COUNT ( #WORK-RECORD.TBL-MOC ) TO #INT-MOC
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"    TOTAL FOR INTERNALLY GENERATED REQUESTS .......:  ",pnd_Int_Moc,                //Natural: WRITE ( 1 ) / 3T '    TOTAL FOR INTERNALLY GENERATED REQUESTS .......:  ' #INT-MOC /
                    NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ext_Moc.setValue(sort01Tbl_MocCount661);                                                                                                              //Natural: MOVE COUNT ( #WORK-RECORD.TBL-MOC ) TO #EXT-MOC
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"    TOTAL FOR EXTERNALLY GENERATED REQUESTS .......:  ",pnd_Ext_Moc,                //Natural: WRITE ( 1 ) / 3T '    TOTAL FOR EXTERNALLY GENERATED REQUESTS .......:  ' #EXT-MOC /
                    NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Save_Unit.equals(pnd_Work_Record_Tbl_Log_Unit_Cde) && pnd_Work_Moc.equals("1")))                                                            //Natural: IF #SAVE-UNIT = #WORK-RECORD.TBL-LOG-UNIT-CDE AND #WORK-MOC = '1'
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            sort01Tbl_MocCount661.setDec(new DbsDecimal(0));                                                                                                              //Natural: END-BREAK
        }
        if (condition(pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(3),"    GRAND TOTAL OF ALL OPEN REQUESTS               :  ",pnd_Misc_Parm_Pnd_Total_Count,  //Natural: WRITE ( 1 ) // 3T '    GRAND TOTAL OF ALL OPEN REQUESTS               :  ' #TOTAL-COUNT ( AD = OU ) / 3T '    -------------------------------------------------'
                new FieldAttributes ("AD=OU"),NEWLINE,new TabSetting(3),"    -------------------------------------------------");
            if (condition(Global.isEscape())) return;
            pnd_Misc_Parm_Pnd_Booklet_Ctr.reset();                                                                                                                        //Natural: RESET #BOOKLET-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANS-CTR #COMPLAINT-CTR #OTHER-CTR #SUB-COUNT #TOTAL-COUNT #INT-MOC #EXT-MOC
            pnd_Misc_Parm_Pnd_Forms_Ctr.reset();
            pnd_Misc_Parm_Pnd_Inquire_Ctr.reset();
            pnd_Misc_Parm_Pnd_Research_Ctr.reset();
            pnd_Misc_Parm_Pnd_Trans_Ctr.reset();
            pnd_Misc_Parm_Pnd_Complaint_Ctr.reset();
            pnd_Misc_Parm_Pnd_Other_Ctr.reset();
            pnd_Misc_Parm_Pnd_Sub_Count.reset();
            pnd_Misc_Parm_Pnd_Total_Count.reset();
            pnd_Int_Moc.reset();
            pnd_Ext_Moc.reset();
            pnd_Report_Data_Pnd_Rep_Unit_Cde.setValue(sort01Tbl_Log_Unit_CdeOld);                                                                                         //Natural: MOVE OLD ( TBL-LOG-UNIT-CDE ) TO #REP-UNIT-CDE
            pnd_New_Unit.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-UNIT
            pnd_Page.reset();                                                                                                                                             //Natural: RESET #PAGE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=140 PS=60");

        getReports().setDisplayColumns(1, "//RECEIVED/AT TIAA",
        		pnd_Work_Record_Tbl_Tiaa_Dte, new ReportEditMask ("MM/DD/YY"),"//DOC/REC'D",
        		pnd_Misc_Parm_Pnd_Return_Doc_Txt,"//REC'D IN/UNIT",
        		pnd_Misc_Parm_Pnd_Return_Rcvd_Txt,"BSNSS/DAYS/IN/UNIT",
        		pnd_Work_Record_Tbl_Business_Days, new ReportEditMask ("ZZZ9.9"),"//WORK/PROCESS",
        		pnd_Misc_Parm_Pnd_Wpid_Sname, new IdenticalSuppress(true),"//WORK/STEP",
        		pnd_Work_Record_Tbl_Step_Id,"///STATUS",
        		pnd_Misc_Parm_Pnd_Status_Sname, new AlphanumericLength (15),"//STATUS/DATE",
        		pnd_Work_Record_Tbl_Status_Dte, new ReportEditMask ("MM/DD/YY"),"CLNDR/DAYS/IN/TIAA",
        		pnd_Work_Record_Tbl_Calendar_Days, new ReportEditMask ("ZZZ9"),"BSNSS/DAYS/IN/TIAA",
        		pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, new ReportEditMask ("ZZZ9"),"//PARTIC/NAME",
        		pnd_Work_Record_Partic_Sname,"///PIN",
        		pnd_Work_Record_Tbl_Pin, new ReportEditMask ("999999999999"),"//CONTRACT/NUMBER",
        		pnd_Work_Record_Tbl_Contracts,"///EMPLOYEE",
        		pnd_Work_Record_Tbl_Racf);
    }
}
