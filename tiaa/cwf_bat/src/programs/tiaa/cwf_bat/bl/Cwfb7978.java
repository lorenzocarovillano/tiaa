/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:44:53 PM
**        * FROM NATURAL PROGRAM : Cwfb7978
************************************************************
**        * FILE NAME            : Cwfb7978.java
**        * CLASS NAME           : Cwfb7978
**        * INSTANCE NAME        : Cwfb7978
************************************************************
************************************************************************
* PROGRAM  : CWFB7978
* SYSTEM   : CRPCWF
* WRITTEN  : 09/09/96  B.ELLO
* TITLE    : PRINT CABINET RECORDS AWAITING DIGITIZING FOR 1 DAY OR MORE
* FUNCTION : THIS PROGRAM WILL PRINT CABINET RECORDS WITH MEDIA IND = 5
*            FOR 1 DAY OR MORE.
* HISTORY
* 09/11/96 AS ADDED PAGE BREAK
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb7978 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Efm_Cabinet;
    private DbsField cwf_Efm_Cabinet_Cabinet_Id;

    private DbsGroup cwf_Efm_Cabinet__R_Field_1;
    private DbsField cwf_Efm_Cabinet_Cabinet_Prex;
    private DbsField cwf_Efm_Cabinet_Cabinet_Pin;
    private DbsField cwf_Efm_Cabinet_Media_Ind;
    private DbsField cwf_Efm_Cabinet_Media_Updte_Dte_Tme;
    private DbsField cwf_Efm_Cabinet_Mit_Rqst_Log_Dte_Tme;
    private DbsField cwf_Efm_Cabinet_Dgtze_Powerfile_Ind;
    private DbsField cwf_Efm_Cabinet_Dgtze_Srce_Cd;
    private DbsField cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme;
    private DbsField cwf_Efm_Cabinet_Updte_Srce_Cde;
    private DbsField cwf_Efm_Cabinet_Audit_Action;
    private DbsField cwf_Efm_Cabinet_Audit_Empl_Racf_Id;
    private DbsField cwf_Efm_Cabinet_Audit_System_Or_Unit;
    private DbsField cwf_Efm_Cabinet_Audit_Jobname;
    private DbsField cwf_Efm_Cabinet_Audit_Dte_Tme;
    private DbsField pnd_Powerfile_Count;
    private DbsField pnd_Total_Count;
    private DbsField pnd_Program;
    private DbsField pnd_Prev_Powerfile;
    private DbsField pnd_Powerfile;
    private DbsField pnd_Source_Cd;
    private DbsField pnd_Date_Time_Diff;
    private DbsField pnd_Page_Break;
    private DbsField pnd_Start_Key;

    private DbsGroup pnd_Start_Key__R_Field_2;
    private DbsField pnd_Start_Key_Pnd_Media_Ind;
    private DbsField pnd_Start_Key_Pnd_Dgtze_Powerfile_Ind;
    private DbsField pnd_Start_Key_Pnd_Dgtze_Due_Dte_Cmt_Prty_Tme;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Dgtze_Powerfile_IndOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Efm_Cabinet = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Cabinet", "CWF-EFM-CABINET"), "CWF_EFM_CABINET", "CWF_EFM_CABINET");
        cwf_Efm_Cabinet_Cabinet_Id = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Cabinet_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Cabinet__R_Field_1 = vw_cwf_Efm_Cabinet.getRecord().newGroupInGroup("cwf_Efm_Cabinet__R_Field_1", "REDEFINE", cwf_Efm_Cabinet_Cabinet_Id);
        cwf_Efm_Cabinet_Cabinet_Prex = cwf_Efm_Cabinet__R_Field_1.newFieldInGroup("cwf_Efm_Cabinet_Cabinet_Prex", "CABINET-PREX", FieldType.STRING, 1);
        cwf_Efm_Cabinet_Cabinet_Pin = cwf_Efm_Cabinet__R_Field_1.newFieldInGroup("cwf_Efm_Cabinet_Cabinet_Pin", "CABINET-PIN", FieldType.NUMERIC, 12);
        cwf_Efm_Cabinet_Media_Ind = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Media_Ind", "MEDIA-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MEDIA_IND");
        cwf_Efm_Cabinet_Media_Ind.setDdmHeader("MEDIA IND");
        cwf_Efm_Cabinet_Media_Updte_Dte_Tme = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Media_Updte_Dte_Tme", "MEDIA-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "MEDIA_UPDTE_DTE_TME");
        cwf_Efm_Cabinet_Media_Updte_Dte_Tme.setDdmHeader("MEDIA UPDATE");
        cwf_Efm_Cabinet_Mit_Rqst_Log_Dte_Tme = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Mit_Rqst_Log_Dte_Tme", "MIT-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "MIT_RQST_LOG_DTE_TME");
        cwf_Efm_Cabinet_Mit_Rqst_Log_Dte_Tme.setDdmHeader("MIT-RLDT");
        cwf_Efm_Cabinet_Dgtze_Powerfile_Ind = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Dgtze_Powerfile_Ind", "DGTZE-POWERFILE-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "DGTZE_POWERFILE_IND");
        cwf_Efm_Cabinet_Dgtze_Powerfile_Ind.setDdmHeader("DGTZE POWERFILE/INDICATOR");
        cwf_Efm_Cabinet_Dgtze_Srce_Cd = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Dgtze_Srce_Cd", "DGTZE-SRCE-CD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DGTZE_SRCE_CD");
        cwf_Efm_Cabinet_Dgtze_Srce_Cd.setDdmHeader("DGTZE SOURCE/CODE");
        cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme", "DGTZE-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "DGTZE_RQST_LOG_DTE_TME");
        cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme.setDdmHeader("DGTZE RQST LOG/DTE TME");
        cwf_Efm_Cabinet_Updte_Srce_Cde = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Updte_Srce_Cde", "UPDTE-SRCE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "UPDTE_SRCE_CDE");
        cwf_Efm_Cabinet_Updte_Srce_Cde.setDdmHeader("UPDTE SRCE");
        cwf_Efm_Cabinet_Audit_Action = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Audit_Action", "AUDIT-ACTION", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AUDIT_ACTION");
        cwf_Efm_Cabinet_Audit_Action.setDdmHeader("ACTION");
        cwf_Efm_Cabinet_Audit_Empl_Racf_Id = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AUDIT_EMPL_RACF_ID");
        cwf_Efm_Cabinet_Audit_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF ID");
        cwf_Efm_Cabinet_Audit_System_Or_Unit = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Audit_System_Or_Unit", "AUDIT-SYSTEM-OR-UNIT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AUDIT_SYSTEM_OR_UNIT");
        cwf_Efm_Cabinet_Audit_System_Or_Unit.setDdmHeader("SYSTEM/OR UNIT");
        cwf_Efm_Cabinet_Audit_Jobname = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Audit_Jobname", "AUDIT-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_JOBNAME");
        cwf_Efm_Cabinet_Audit_Jobname.setDdmHeader("JOBNAME");
        cwf_Efm_Cabinet_Audit_Dte_Tme = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "AUDIT_DTE_TME");
        cwf_Efm_Cabinet_Audit_Dte_Tme.setDdmHeader("DATE/& TIME");
        registerRecord(vw_cwf_Efm_Cabinet);

        pnd_Powerfile_Count = localVariables.newFieldInRecord("pnd_Powerfile_Count", "#POWERFILE-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Count = localVariables.newFieldInRecord("pnd_Total_Count", "#TOTAL-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Prev_Powerfile = localVariables.newFieldInRecord("pnd_Prev_Powerfile", "#PREV-POWERFILE", FieldType.STRING, 2);
        pnd_Powerfile = localVariables.newFieldInRecord("pnd_Powerfile", "#POWERFILE", FieldType.STRING, 2);
        pnd_Source_Cd = localVariables.newFieldInRecord("pnd_Source_Cd", "#SOURCE-CD", FieldType.STRING, 12);
        pnd_Date_Time_Diff = localVariables.newFieldInRecord("pnd_Date_Time_Diff", "#DATE-TIME-DIFF", FieldType.PACKED_DECIMAL, 10);
        pnd_Page_Break = localVariables.newFieldInRecord("pnd_Page_Break", "#PAGE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Start_Key = localVariables.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.STRING, 20);

        pnd_Start_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Start_Key__R_Field_2", "REDEFINE", pnd_Start_Key);
        pnd_Start_Key_Pnd_Media_Ind = pnd_Start_Key__R_Field_2.newFieldInGroup("pnd_Start_Key_Pnd_Media_Ind", "#MEDIA-IND", FieldType.STRING, 1);
        pnd_Start_Key_Pnd_Dgtze_Powerfile_Ind = pnd_Start_Key__R_Field_2.newFieldInGroup("pnd_Start_Key_Pnd_Dgtze_Powerfile_Ind", "#DGTZE-POWERFILE-IND", 
            FieldType.STRING, 2);
        pnd_Start_Key_Pnd_Dgtze_Due_Dte_Cmt_Prty_Tme = pnd_Start_Key__R_Field_2.newFieldInGroup("pnd_Start_Key_Pnd_Dgtze_Due_Dte_Cmt_Prty_Tme", "#DGTZE-DUE-DTE-CMT-PRTY-TME", 
            FieldType.STRING, 17);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Dgtze_Powerfile_IndOld = internalLoopRecord.newFieldInRecord("Sort01_Dgtze_Powerfile_Ind_OLD", "Dgtze_Powerfile_Ind_OLD", FieldType.STRING, 
            2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Efm_Cabinet.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Page_Break.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb7978() throws Exception
    {
        super("Cwfb7978");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        getReports().definePrinter(2, "RPT1");                                                                                                                            //Natural: DEFINE PRINTER ( RPT1 = 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( RPT1 ) LS = 133 PS = 60
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM := *PROGRAM
        pnd_Start_Key_Pnd_Media_Ind.setValue(5);                                                                                                                          //Natural: ASSIGN #MEDIA-IND := 5
        vw_cwf_Efm_Cabinet.startDatabaseRead                                                                                                                              //Natural: READ CWF-EFM-CABINET BY DGTZE-WORK-LIST-KEY-2 STARTING FROM #START-KEY
        (
        "READ01",
        new Wc[] { new Wc("DGTZE_WORK_LIST_KEY_2", ">=", pnd_Start_Key, WcType.BY) },
        new Oc[] { new Oc("DGTZE_WORK_LIST_KEY_2", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Efm_Cabinet.readNextRow("READ01")))
        {
            if (condition(cwf_Efm_Cabinet_Media_Ind.notEquals("5")))                                                                                                      //Natural: IF CWF-EFM-CABINET.MEDIA-IND NE '5'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  ONLY ACCEPT CABINETS AWAITING DIGITIZING FOR A DAY OR MORE.
            pnd_Date_Time_Diff.compute(new ComputeParameters(false, pnd_Date_Time_Diff), Global.getTIMX().subtract(cwf_Efm_Cabinet_Media_Updte_Dte_Tme));                 //Natural: COMPUTE #DATE-TIME-DIFF = *TIMX - CWF-EFM-CABINET.MEDIA-UPDTE-DTE-TME
            //*  (864000 = 24 HOURS)
            if (condition(!(pnd_Date_Time_Diff.greaterOrEqual(864000))))                                                                                                  //Natural: ACCEPT IF #DATE-TIME-DIFF GE 864000
            {
                continue;
            }
            getSort().writeSortInData(cwf_Efm_Cabinet_Dgtze_Powerfile_Ind, cwf_Efm_Cabinet_Media_Updte_Dte_Tme, cwf_Efm_Cabinet_Cabinet_Id, cwf_Efm_Cabinet_Media_Ind,    //Natural: END-ALL
                cwf_Efm_Cabinet_Dgtze_Srce_Cd, cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(cwf_Efm_Cabinet_Dgtze_Powerfile_Ind, cwf_Efm_Cabinet_Media_Updte_Dte_Tme, cwf_Efm_Cabinet_Cabinet_Id);                                         //Natural: SORT BY CWF-EFM-CABINET.DGTZE-POWERFILE-IND CWF-EFM-CABINET.MEDIA-UPDTE-DTE-TME CWF-EFM-CABINET.CABINET-ID USING CWF-EFM-CABINET.MEDIA-IND CWF-EFM-CABINET.DGTZE-SRCE-CD CWF-EFM-CABINET.DGTZE-RQST-LOG-DTE-TME
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Efm_Cabinet_Dgtze_Powerfile_Ind, cwf_Efm_Cabinet_Media_Updte_Dte_Tme, cwf_Efm_Cabinet_Cabinet_Id, 
            cwf_Efm_Cabinet_Media_Ind, cwf_Efm_Cabinet_Dgtze_Srce_Cd, cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF CWF-EFM-CABINET.DGTZE-POWERFILE-IND
            pnd_Powerfile_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #POWERFILE-COUNT
            pnd_Powerfile.setValue(cwf_Efm_Cabinet_Dgtze_Powerfile_Ind);                                                                                                  //Natural: ASSIGN #POWERFILE := CWF-EFM-CABINET.DGTZE-POWERFILE-IND
            if (condition(pnd_Prev_Powerfile.equals(cwf_Efm_Cabinet_Dgtze_Powerfile_Ind)))                                                                                //Natural: IF #PREV-POWERFILE = CWF-EFM-CABINET.DGTZE-POWERFILE-IND
            {
                pnd_Powerfile.reset();                                                                                                                                    //Natural: RESET #POWERFILE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Prev_Powerfile.setValue(cwf_Efm_Cabinet_Dgtze_Powerfile_Ind);                                                                                             //Natural: ASSIGN #PREV-POWERFILE := CWF-EFM-CABINET.DGTZE-POWERFILE-IND
            pnd_Source_Cd.reset();                                                                                                                                        //Natural: RESET #SOURCE-CD
            short decideConditionsMet95 = 0;                                                                                                                              //Natural: DECIDE ON FIRST VALUE OF CWF-EFM-CABINET.DGTZE-SRCE-CD;//Natural: VALUE 'L'
            if (condition((cwf_Efm_Cabinet_Dgtze_Srce_Cd.equals("L"))))
            {
                decideConditionsMet95++;
                pnd_Source_Cd.setValue("LOGGING");                                                                                                                        //Natural: ASSIGN #SOURCE-CD := 'LOGGING'
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((cwf_Efm_Cabinet_Dgtze_Srce_Cd.equals("S"))))
            {
                decideConditionsMet95++;
                pnd_Source_Cd.setValue("SELECT");                                                                                                                         //Natural: ASSIGN #SOURCE-CD := 'SELECT'
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((cwf_Efm_Cabinet_Dgtze_Srce_Cd.equals("T"))))
            {
                decideConditionsMet95++;
                pnd_Source_Cd.setValue("T FUNCTION");                                                                                                                     //Natural: ASSIGN #SOURCE-CD := 'T FUNCTION'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_Page_Break.getBoolean()))                                                                                                                   //Natural: IF #PAGE-BREAK
            {
                pnd_Page_Break.resetInitial();                                                                                                                            //Natural: RESET INITIAL #PAGE-BREAK
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( RPT1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Cwff7979.class));                                                                          //Natural: WRITE ( RPT1 ) NOTITLE USING FORM 'CWFF7979'
            sort01Dgtze_Powerfile_IndOld.setValue(cwf_Efm_Cabinet_Dgtze_Powerfile_Ind);                                                                                   //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        if (condition(getReports().getAstLinesLeft(0).less(4)))                                                                                                           //Natural: NEWPAGE ( RPT1 ) WHEN LESS THAN 4 LINES LEFT
        {
            getReports().newPage(0);
            if (condition(Global.isEscape())){return;}
        }
        getReports().skip(0, 3);                                                                                                                                          //Natural: SKIP ( RPT1 ) 3
        getReports().write(2, "Total for All Powerfiles :",pnd_Total_Count);                                                                                              //Natural: WRITE ( RPT1 ) 'Total for All Powerfiles :' #TOTAL-COUNT
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( RPT1 )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Cwff7978.class));                                              //Natural: WRITE ( RPT1 ) NOTITLE NOHDR USING FORM 'CWFF7978'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Efm_Cabinet_Dgtze_Powerfile_IndIsBreak = cwf_Efm_Cabinet_Dgtze_Powerfile_Ind.isBreak(endOfData);
        if (condition(cwf_Efm_Cabinet_Dgtze_Powerfile_IndIsBreak))
        {
            pnd_Total_Count.nadd(pnd_Powerfile_Count);                                                                                                                    //Natural: ADD #POWERFILE-COUNT TO #TOTAL-COUNT
            getReports().skip(0, 1);                                                                                                                                      //Natural: SKIP ( RPT1 ) 1
            getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,"     Total for Powerfile",sort01Dgtze_Powerfile_IndOld,":",pnd_Powerfile_Count,                //Natural: WRITE ( RPT1 ) NOTITLE NOHDR '     Total for Powerfile' OLD ( DGTZE-POWERFILE-IND ) ':' #POWERFILE-COUNT //
                NEWLINE,NEWLINE);
            if (condition(Global.isEscape())) return;
            pnd_Powerfile_Count.reset();                                                                                                                                  //Natural: RESET #POWERFILE-COUNT
            pnd_Page_Break.setValue(true);                                                                                                                                //Natural: ASSIGN #PAGE-BREAK = TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60");
    }
}
