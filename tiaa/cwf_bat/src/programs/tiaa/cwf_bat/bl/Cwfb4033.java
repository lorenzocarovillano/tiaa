/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:32:42 PM
**        * FROM NATURAL PROGRAM : Cwfb4033
************************************************************
**        * FILE NAME            : Cwfb4033.java
**        * CLASS NAME           : Cwfb4033
**        * INSTANCE NAME        : Cwfb4033
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: DIVISION TABLE
**SAG SYSTEM: CRPCWF
**SAG REPORT-HEADING(1): CWF DIVISION CODES EXTRACT REPORT
**SAG PRINT-FILE(1): 1
**SAG HEADING-LINE: 10000000
**SAG DESCS(1): THIS PROGRAM READS THE CWF-DIV-TBL TABLE AND WRITES A
**SAG DESCS(2): WORK FILE TO BE DOWNLOADED TO THE SQL SERVER.
**SAG PRIMARY-FILE: CWF-DIV-TBL
**SAG PRIMARY-KEY: TBL-PRIME-KEY
************************************************************************
* PROGRAM  : CWFB4033
* SYSTEM   : CRPCWF
* TITLE    : DIVISION TABLE
* GENERATED: DEC 18,95 AT 11:11 AM
* FUNCTION : THIS PROGRAM READS THE CWF-DIV-TBL TABLE AND WRITES A
*            WORK FILE TO BE DOWNLOADED TO THE SQL SERVER.
*
*
* HISTORY
**SAG DEFINE EXIT CHANGE-HISTORY
* CHANGED ON DEC 18,95 BY HARGRAV FOR RELEASE ____
* >
* >
* >
**SAG END-EXIT
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4033 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Div_Tbl;
    private DbsField cwf_Div_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Div_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Div_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Div_Tbl__R_Field_1;
    private DbsField cwf_Div_Tbl_Division;
    private DbsField cwf_Div_Tbl_Div_Fill;
    private DbsField cwf_Div_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Div_Tbl__R_Field_2;
    private DbsField cwf_Div_Tbl_Div_Name;
    private DbsField cwf_Div_Tbl_Mang_Lname;
    private DbsField cwf_Div_Tbl_Mang_Fname;
    private DbsField cwf_Div_Tbl_Mang_Title;
    private DbsField cwf_Div_Tbl_Sup_Lname;
    private DbsField cwf_Div_Tbl_Sup_Fname;
    private DbsField cwf_Div_Tbl_Tbl_Fill;

    private DbsGroup cwf_Div_Tbl__R_Field_3;
    private DbsField cwf_Div_Tbl_Unit_Cde;
    private DbsField cwf_Div_Tbl_Unit_Fill;
    private DbsField cwf_Div_Tbl_Tbl_Actve_Ind;

    private DbsGroup cwf_Div_Tbl__R_Field_4;
    private DbsField cwf_Div_Tbl_Fill_Acb770;
    private DbsField cwf_Div_Tbl_Tbl_Actve_Ind_2_2;
    private DbsField cwf_Div_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Div_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Div_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Div_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Div_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Div_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Div_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Div_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Div_Tbl_Tbl_Table_Access_Level;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;

    private DbsGroup work_Record;
    private DbsField work_Record_Division;
    private DbsField work_Record_Pnd_C1;
    private DbsField work_Record_Unit;
    private DbsField work_Record_Pnd_C2;
    private DbsField work_Record_Div_Name;
    private DbsField work_Record_Pnd_C3;
    private DbsField work_Record_Mang_Lname;
    private DbsField work_Record_Pnd_C4;
    private DbsField work_Record_Mang_Fname;
    private DbsField work_Record_Pnd_C5;
    private DbsField work_Record_Mang_Title;
    private DbsField work_Record_Pnd_C6;
    private DbsField work_Record_Sup_Lname;
    private DbsField work_Record_Pnd_C7;
    private DbsField work_Record_Sup_Fname;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Rex_Read;
    private DbsField pnd_Counters_Pnd_Rex_Written;

    private DbsGroup pnd_Report_Fields;
    private DbsField pnd_Report_Fields_Pnd_Error_Msg;
    private DbsField pnd_Report_Fields_Pnd_Mang_Name;
    private DbsField pnd_Report_Fields_Pnd_Sup_Name;

    private DbsGroup pnd_Contstants;
    private DbsField pnd_Contstants_Pnd_Delimiter;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_5;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Prev_Division;
    private DbsField pnd_I;
    private DbsField pnd_Record_Written;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Div_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Div_Tbl", "CWF-DIV-TBL"), "CWF_DIV_TBL", "CWF_DCMNT_TABLE");
        cwf_Div_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Div_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Div_Tbl_Tbl_Table_Nme = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TBL_TABLE_NME");
        cwf_Div_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Div_Tbl_Tbl_Key_Field = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "TBL_KEY_FIELD");

        cwf_Div_Tbl__R_Field_1 = vw_cwf_Div_Tbl.getRecord().newGroupInGroup("cwf_Div_Tbl__R_Field_1", "REDEFINE", cwf_Div_Tbl_Tbl_Key_Field);
        cwf_Div_Tbl_Division = cwf_Div_Tbl__R_Field_1.newFieldInGroup("cwf_Div_Tbl_Division", "DIVISION", FieldType.STRING, 6);
        cwf_Div_Tbl_Div_Fill = cwf_Div_Tbl__R_Field_1.newFieldInGroup("cwf_Div_Tbl_Div_Fill", "DIV-FILL", FieldType.STRING, 24);
        cwf_Div_Tbl_Tbl_Data_Field = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, 
            RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Div_Tbl__R_Field_2 = vw_cwf_Div_Tbl.getRecord().newGroupInGroup("cwf_Div_Tbl__R_Field_2", "REDEFINE", cwf_Div_Tbl_Tbl_Data_Field);
        cwf_Div_Tbl_Div_Name = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Div_Name", "DIV-NAME", FieldType.STRING, 30);
        cwf_Div_Tbl_Mang_Lname = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Mang_Lname", "MANG-LNAME", FieldType.STRING, 20);
        cwf_Div_Tbl_Mang_Fname = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Mang_Fname", "MANG-FNAME", FieldType.STRING, 20);
        cwf_Div_Tbl_Mang_Title = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Mang_Title", "MANG-TITLE", FieldType.STRING, 30);
        cwf_Div_Tbl_Sup_Lname = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Sup_Lname", "SUP-LNAME", FieldType.STRING, 20);
        cwf_Div_Tbl_Sup_Fname = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Sup_Fname", "SUP-FNAME", FieldType.STRING, 20);
        cwf_Div_Tbl_Tbl_Fill = cwf_Div_Tbl__R_Field_2.newFieldInGroup("cwf_Div_Tbl_Tbl_Fill", "TBL-FILL", FieldType.STRING, 113);

        cwf_Div_Tbl__R_Field_3 = vw_cwf_Div_Tbl.getRecord().newGroupInGroup("cwf_Div_Tbl__R_Field_3", "REDEFINE", cwf_Div_Tbl_Tbl_Data_Field);
        cwf_Div_Tbl_Unit_Cde = cwf_Div_Tbl__R_Field_3.newFieldArrayInGroup("cwf_Div_Tbl_Unit_Cde", "UNIT-CDE", FieldType.STRING, 7, new DbsArrayController(1, 
            36));
        cwf_Div_Tbl_Unit_Fill = cwf_Div_Tbl__R_Field_3.newFieldInGroup("cwf_Div_Tbl_Unit_Fill", "UNIT-FILL", FieldType.STRING, 1);
        cwf_Div_Tbl_Tbl_Actve_Ind = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TBL_ACTVE_IND");

        cwf_Div_Tbl__R_Field_4 = vw_cwf_Div_Tbl.getRecord().newGroupInGroup("cwf_Div_Tbl__R_Field_4", "REDEFINE", cwf_Div_Tbl_Tbl_Actve_Ind);
        cwf_Div_Tbl_Fill_Acb770 = cwf_Div_Tbl__R_Field_4.newFieldInGroup("cwf_Div_Tbl_Fill_Acb770", "FILL-ACB770", FieldType.STRING, 1);
        cwf_Div_Tbl_Tbl_Actve_Ind_2_2 = cwf_Div_Tbl__R_Field_4.newFieldInGroup("cwf_Div_Tbl_Tbl_Actve_Ind_2_2", "TBL-ACTVE-IND-2-2", FieldType.STRING, 
            1);
        cwf_Div_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Div_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Div_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Div_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Div_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Div_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Div_Tbl_Tbl_Updte_Dte = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Div_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Div_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Div_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Div_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Div_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Div_Tbl_Tbl_Table_Rectype = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Div_Tbl_Tbl_Table_Access_Level = vw_cwf_Div_Tbl.getRecord().newFieldInGroup("cwf_Div_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Div_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");
        registerRecord(vw_cwf_Div_Tbl);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);

        work_Record = localVariables.newGroupInRecord("work_Record", "WORK-RECORD");
        work_Record_Division = work_Record.newFieldInGroup("work_Record_Division", "DIVISION", FieldType.STRING, 6);
        work_Record_Pnd_C1 = work_Record.newFieldInGroup("work_Record_Pnd_C1", "#C1", FieldType.STRING, 1);
        work_Record_Unit = work_Record.newFieldInGroup("work_Record_Unit", "UNIT", FieldType.STRING, 7);
        work_Record_Pnd_C2 = work_Record.newFieldInGroup("work_Record_Pnd_C2", "#C2", FieldType.STRING, 1);
        work_Record_Div_Name = work_Record.newFieldInGroup("work_Record_Div_Name", "DIV-NAME", FieldType.STRING, 30);
        work_Record_Pnd_C3 = work_Record.newFieldInGroup("work_Record_Pnd_C3", "#C3", FieldType.STRING, 1);
        work_Record_Mang_Lname = work_Record.newFieldInGroup("work_Record_Mang_Lname", "MANG-LNAME", FieldType.STRING, 20);
        work_Record_Pnd_C4 = work_Record.newFieldInGroup("work_Record_Pnd_C4", "#C4", FieldType.STRING, 1);
        work_Record_Mang_Fname = work_Record.newFieldInGroup("work_Record_Mang_Fname", "MANG-FNAME", FieldType.STRING, 20);
        work_Record_Pnd_C5 = work_Record.newFieldInGroup("work_Record_Pnd_C5", "#C5", FieldType.STRING, 1);
        work_Record_Mang_Title = work_Record.newFieldInGroup("work_Record_Mang_Title", "MANG-TITLE", FieldType.STRING, 30);
        work_Record_Pnd_C6 = work_Record.newFieldInGroup("work_Record_Pnd_C6", "#C6", FieldType.STRING, 1);
        work_Record_Sup_Lname = work_Record.newFieldInGroup("work_Record_Sup_Lname", "SUP-LNAME", FieldType.STRING, 20);
        work_Record_Pnd_C7 = work_Record.newFieldInGroup("work_Record_Pnd_C7", "#C7", FieldType.STRING, 1);
        work_Record_Sup_Fname = work_Record.newFieldInGroup("work_Record_Sup_Fname", "SUP-FNAME", FieldType.STRING, 20);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Rex_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Read", "#REX-READ", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_Rex_Written = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Written", "#REX-WRITTEN", FieldType.PACKED_DECIMAL, 5);

        pnd_Report_Fields = localVariables.newGroupInRecord("pnd_Report_Fields", "#REPORT-FIELDS");
        pnd_Report_Fields_Pnd_Error_Msg = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 60);
        pnd_Report_Fields_Pnd_Mang_Name = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Mang_Name", "#MANG-NAME", FieldType.STRING, 30);
        pnd_Report_Fields_Pnd_Sup_Name = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Sup_Name", "#SUP-NAME", FieldType.STRING, 30);

        pnd_Contstants = localVariables.newGroupInRecord("pnd_Contstants", "#CONTSTANTS");
        pnd_Contstants_Pnd_Delimiter = pnd_Contstants.newFieldInGroup("pnd_Contstants_Pnd_Delimiter", "#DELIMITER", FieldType.STRING, 1);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 22);

        pnd_Tbl_Prime_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_5", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Prev_Division = localVariables.newFieldInRecord("pnd_Prev_Division", "#PREV-DIVISION", FieldType.STRING, 6);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Record_Written = localVariables.newFieldInRecord("pnd_Record_Written", "#RECORD-WRITTEN", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Div_Tbl.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("        CWF Division Codes Extract Report");
        pnd_Header1_2.setInitialValue("                        �");
        pnd_Contstants_Pnd_Delimiter.setInitialValue("@");
        pnd_Tbl_Prime_Key.setInitialValue("A DIVISION-TABLE");
        pnd_Record_Written.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4033() throws Exception
    {
        super("Cwfb4033");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        vw_cwf_Div_Tbl.startDatabaseRead                                                                                                                                  //Natural: READ CWF-DIV-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ_PRIME",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Div_Tbl.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            if (condition(cwf_Div_Tbl_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)))                                                                      //Natural: IF CWF-DIV-TBL.TBL-TABLE-NME NE #TBL-PRIME-KEY.#TBL-TABLE-NME
            {
                if (true) break READ_PRIME;                                                                                                                               //Natural: ESCAPE BOTTOM ( READ-PRIME. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Counters_Pnd_Rex_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #REX-READ
            //* ***********************************************************************
            //*   AS LONG AS DIVISION TABLE HAS UNIT ON IT WE NEED TO WRITE A RECORD  *
            //*   FOR EACH DIVISION/UNIT COMBINATION.  ONCE WE PUT DIVISION ON THE    *
            //*   UNIT TABLE THE FOLLOWING DECIDE STATEMENT CAN BE REMOVED. AT THAT   *
            //*   POINT WE WILL WRITE ONE RECORD FOR EACH DIVISION RECORD READ.       *
            //*                                                                       *
            //*                                                                       *
            //* ***********************************************************************
            short decideConditionsMet195 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-DIV-TBL.DIVISION NE #PREV-DIVISION
            if (condition(cwf_Div_Tbl_Division.notEquals(pnd_Prev_Division)))
            {
                decideConditionsMet195++;
                work_Record.reset();                                                                                                                                      //Natural: RESET WORK-RECORD
                pnd_Record_Written.resetInitial();                                                                                                                        //Natural: RESET INITIAL #RECORD-WRITTEN
                work_Record.setValuesByName(vw_cwf_Div_Tbl);                                                                                                              //Natural: MOVE BY NAME CWF-DIV-TBL TO WORK-RECORD
                pnd_Report_Fields_Pnd_Mang_Name.setValue(DbsUtil.compress(work_Record_Mang_Fname, cwf_Div_Tbl_Mang_Lname));                                               //Natural: COMPRESS WORK-RECORD.MANG-FNAME CWF-DIV-TBL.MANG-LNAME INTO #MANG-NAME
                pnd_Report_Fields_Pnd_Sup_Name.setValue(DbsUtil.compress(cwf_Div_Tbl_Sup_Fname, cwf_Div_Tbl_Sup_Lname));                                                  //Natural: COMPRESS CWF-DIV-TBL.SUP-FNAME CWF-DIV-TBL.SUP-LNAME INTO #SUP-NAME
                pnd_Prev_Division.setValue(cwf_Div_Tbl_Division);                                                                                                         //Natural: MOVE CWF-DIV-TBL.DIVISION TO #PREV-DIVISION
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                FOR01:                                                                                                                                                    //Natural: FOR #I 1 TO 36
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(36)); pnd_I.nadd(1))
                {
                    if (condition(cwf_Div_Tbl_Unit_Cde.getValue(pnd_I).notEquals(" ")))                                                                                   //Natural: IF CWF-DIV-TBL.UNIT-CDE ( #I ) NE ' '
                    {
                        work_Record_Unit.setValue(cwf_Div_Tbl_Unit_Cde.getValue(pnd_I));                                                                                  //Natural: MOVE CWF-DIV-TBL.UNIT-CDE ( #I ) TO WORK-RECORD.UNIT
                                                                                                                                                                          //Natural: PERFORM FORMAT-RECORD
                        sub_Format_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
                        sub_Write_Work_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Record_Written.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #RECORD-WRITTEN
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(! (pnd_Record_Written.getBoolean())))                                                                                                       //Natural: IF NOT #RECORD-WRITTEN
                {
                                                                                                                                                                          //Natural: PERFORM FORMAT-RECORD
                    sub_Format_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
                    sub_Write_Work_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *SAG END-EXIT
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        if (condition(getReports().getAstLinesLeft(1).less(6)))                                                                                                           //Natural: NEWPAGE ( 1 ) IF LESS THAN 6 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total Number of Division Table Records Read:",pnd_Counters_Pnd_Rex_Read,NEWLINE,"Total Number of Work File Records Written  :",pnd_Counters_Pnd_Rex_Written,NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) // 'Total Number of Division Table Records Read:' #REX-READ / 'Total Number of Work File Records Written  :' #REX-WRITTEN //56T 'End of Report'
            TabSetting(56),"End of Report");
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-RECORD
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *****************************
        //* *************
        //* *SAG END-EXIT
    }
    private void sub_Format_Record() throws Exception                                                                                                                     //Natural: FORMAT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        work_Record_Pnd_C1.setValue(pnd_Contstants_Pnd_Delimiter);                                                                                                        //Natural: MOVE #DELIMITER TO #C1 #C2 #C3 #C4 #C5 #C6 #C7
        work_Record_Pnd_C2.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C3.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C4.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C5.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C6.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C7.setValue(pnd_Contstants_Pnd_Delimiter);
        //* *************
        //*  FORMAT-RECORD
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(1, false, work_Record);                                                                                                                      //Natural: WRITE WORK FILE 1 WORK-RECORD
        pnd_Counters_Pnd_Rex_Written.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REX-WRITTEN
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        //* ********************************
        //*  WRITE-WORK-FILE
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "/Div",                                                                                                                                   //Natural: DISPLAY ( 1 ) '/Div' WORK-RECORD.DIVISION 'Div/Name' WORK-RECORD.DIV-NAME '/Unit' WORK-RECORD.UNIT 'Manager/Name' #MANG-NAME 'Supervisor/Name' #SUP-NAME 'Manager/Title' WORK-RECORD.MANG-TITLE ( AL = 10 )
        		work_Record_Division,"Div/Name",
        		work_Record_Div_Name,"/Unit",
        		work_Record_Unit,"Manager/Name",
        		pnd_Report_Fields_Pnd_Mang_Name,"Supervisor/Name",
        		pnd_Report_Fields_Pnd_Sup_Name,"Manager/Title",
        		work_Record_Mang_Title, new AlphanumericLength (10));
        if (Global.isEscape()) return;
        //* *************
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),pnd_Header1_2,new 
                        TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "/Div",
        		work_Record_Division,"Div/Name",
        		work_Record_Div_Name,"/Unit",
        		work_Record_Unit,"Manager/Name",
        		pnd_Report_Fields_Pnd_Mang_Name,"Supervisor/Name",
        		pnd_Report_Fields_Pnd_Sup_Name,"Manager/Title",
        		work_Record_Mang_Title, new AlphanumericLength (10));
    }
}
