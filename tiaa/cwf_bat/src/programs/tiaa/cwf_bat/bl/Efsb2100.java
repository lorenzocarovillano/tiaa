/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:06:09 PM
**        * FROM NATURAL PROGRAM : Efsb2100
************************************************************
**        * FILE NAME            : Efsb2100.java
**        * CLASS NAME           : Efsb2100
**        * INSTANCE NAME        : Efsb2100
************************************************************
**********************************************************************
* PROGRAM NAME: EFSB2100
* WRITTEN BY  : J HOFSTETTER
* DESCRIPTION :
* 1. READ CWF-SUPPORT-TBL FILE CWF-REPORT12-INPUT BY KEY EFSB2100
*    TO GET REPORT FROM/TO DATES
* 2. INTERROGATE CWF-MASTER-INDEX FOR PARTICIPANT-CLOSED WORK REQUESTS
*    FOR GIVEN PERIOD USING PRTCPNT-CLOSED-KEY (CRPRTE-CLOCK-END-DTE)
* 3. READ 1ST CWF-MASTER-INDEX RECORD BY RQST-ROUTING-KEY TO GET
*    EARLIEST ADMIN-UNIT-CDE
* 4. READ EFM-DOCUMENT FILE WITH DOCUMENT-KEY FOR ALL DOCUMENTS
*    AND TO GET ENTRY-SYSTEM-OR-UNIT FOR SORT
* 5. WRITE WORK FILE TO BE SORTED
* 6. UPDATE CWF-SUPPORT-TBL FILE CWF-REPORT12-INPUT BY KEY EFSB2100,
*    INCREMENTING FROM/TO DATES BY 7 DAYS.
*
* DATE WRITTEN: JUL 26, 1995
* MODIFIED    :
* -----------------------------------------------------
* 10/02/95 JHH - EXPAND CWF-SUPPORT-TBL UPDATE (DATE, TIME, USER)
*              - DELETE WRITES TO SYSLOG
* 11/14/95 JHH - ADD GET BEFORE UPDATE TO STOP TIMEOUTS
* 11/27/95 JHH - USE OTHER FIELDS ON MASTER-INDEX IF RQST-ID IS NULL
* 09/27/99 JOHNSZI - INCREASED THE LENGTH OF FIELD #DEL-CNT TO N4
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
**********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsb2100 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Parm;

    private DbsGroup pnd_Parm__R_Field_1;
    private DbsField pnd_Parm_Pnd_Next;
    private DbsField pnd_Parm_Pnd_Next_Dates;

    private DbsGroup pnd_Parm__R_Field_2;
    private DbsField pnd_Parm_Pnd_Next_From_Dte;
    private DbsField pnd_Parm_Pnd_Next_To;
    private DbsField pnd_Parm_Pnd_Next_To_Dte;
    private DbsField pnd_Parm_Pnd_Prev;
    private DbsField pnd_Parm_Pnd_Prev_Dates;

    private DbsGroup pnd_Parm__R_Field_3;
    private DbsField pnd_Parm_Pnd_Prev_From_Dte;
    private DbsField pnd_Parm_Pnd_Prev_To;
    private DbsField pnd_Parm_Pnd_Prev_To_Dte;
    private DbsField pnd_Parm_Pnd_Run;
    private DbsField pnd_Parm_Pnd_Run_Dte;
    private DbsField pnd_Dte_From;
    private DbsField pnd_Dte_To;

    private DataAccessProgramView vw_cwf_Mit1;
    private DbsField cwf_Mit1_Pin_Nbr;
    private DbsField cwf_Mit1_Rqst_Log_Dte_Tme;
    private DbsField cwf_Mit1_Case_Id_Cde;

    private DbsGroup cwf_Mit1__R_Field_4;
    private DbsField cwf_Mit1_Case_Cde;
    private DbsField cwf_Mit1_Sub_Rqst_Seq;
    private DbsField cwf_Mit1_Multi_Rqst_Ind;
    private DbsField cwf_Mit1_Work_Prcss_Id;
    private DbsField cwf_Mit1_Rqst_Id;

    private DbsGroup cwf_Mit1__R_Field_5;
    private DbsField cwf_Mit1_R_Wpid;
    private DbsField cwf_Mit1_R_Rcvd_Dte;
    private DbsField cwf_Mit1_R_Case;
    private DbsField cwf_Mit1_R_Sub_Rqst;
    private DbsField cwf_Mit1_R_Pin;
    private DbsField cwf_Mit1_Tiaa_Rcvd_Dte;
    private DbsField cwf_Mit1_Crprte_Clock_End_Dte_Tme;

    private DbsGroup cwf_Mit1__R_Field_6;
    private DbsField cwf_Mit1_Crprte_Clock_End_Dte;

    private DataAccessProgramView vw_cwf_Mit2;
    private DbsField cwf_Mit2_Rqst_Log_Dte_Tme;
    private DbsField cwf_Mit2_Admin_Unit_Cde;

    private DataAccessProgramView vw_cwf_Dcmt;
    private DbsField cwf_Dcmt_Entry_System_Or_Unit;

    private DbsGroup cwf_Dcmt_Document_Type;
    private DbsField cwf_Dcmt_Document_Category;
    private DbsField cwf_Dcmt_Document_Sub_Category;
    private DbsField cwf_Dcmt_Document_Detail;
    private DbsField cwf_Dcmt_Document_Key;

    private DbsGroup cwf_Dcmt__R_Field_7;
    private DbsField cwf_Dcmt_Folder_Key;
    private DbsField cwf_Dcmt_Document_Direction_Ind;
    private DbsField cwf_Dcmt_Document_Retention_Ind;
    private DbsField cwf_Dcmt_Document_Subtype;

    private DbsGroup cwf_Dcmt__R_Field_8;
    private DbsField cwf_Dcmt_Doc_Text;
    private DbsField cwf_Dcmt_Doc_Image;
    private DbsField cwf_Dcmt_Doc_Kdo;
    private DbsField cwf_Dcmt_Entry_System;
    private DbsField pnd_Document_Key;

    private DbsGroup pnd_Document_Key__R_Field_9;
    private DbsField pnd_Document_Key_Pnd_Folder_Key;
    private DbsField pnd_Document_Key_Pnd_Entry_Dte_Tme;

    private DataAccessProgramView vw_cwf_Tbl;
    private DbsField cwf_Tbl_Tbl_Data_Field;
    private DbsField cwf_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Prime_Key;
    private DbsField pnd_Work_Record;

    private DbsGroup pnd_Work_Record__R_Field_10;
    private DbsField pnd_Work_Record_Pnd_Work_Type;
    private DbsField pnd_Work_Record_Pnd_Work_Folder;

    private DbsGroup pnd_Work_Record__R_Field_11;
    private DbsField pnd_Work_Record_Pnd_Cabinet_Id;

    private DbsGroup pnd_Work_Record__R_Field_12;
    private DbsField pnd_Work_Record_Pnd_Cab_Pfx;
    private DbsField pnd_Work_Record_Pnd_Pin_Nbr;
    private DbsField pnd_Work_Record_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Work_Record_Pnd_Case_Id;
    private DbsField pnd_Work_Record_Pnd_Wpid;
    private DbsField pnd_Work_Record_Pnd_Multi_Rqst_Ind;
    private DbsField pnd_Work_Record_Pnd_Sub_Rqst;

    private DbsGroup pnd_Work_Record__R_Field_13;
    private DbsField pnd_Work_Record_Pnd_Work_From_Dte;
    private DbsField pnd_Work_Record_Pnd_Work_To_Dte;
    private DbsField pnd_Work_Record_Pnd_Work_Unit_1st;

    private DbsGroup pnd_Work_Record_Pnd_Work_Doc_Type;
    private DbsField pnd_Work_Record_Document_Category;
    private DbsField pnd_Work_Record_Document_Sub_Category;
    private DbsField pnd_Work_Record_Document_Detail;
    private DbsField pnd_Work_Record_Pnd_Work_Doc_Sub_Type;
    private DbsField pnd_Work_Record_Pnd_Work_System;
    private DbsField pnd_Work_Record_Pnd_Work_Drctn;
    private DbsField pnd_Work_Record_Pnd_Work_Unit_Entry;
    private DbsField pnd_Mit1_Cnt;
    private DbsField pnd_Mit2_Cnt;
    private DbsField pnd_Dcmt_Cnt;
    private DbsField pnd_Del_Cnt;
    private DbsField first_Dcmt;
    private DbsField pnd_Isn;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Parm = localVariables.newFieldInRecord("pnd_Parm", "#PARM", FieldType.STRING, 63);

        pnd_Parm__R_Field_1 = localVariables.newGroupInRecord("pnd_Parm__R_Field_1", "REDEFINE", pnd_Parm);
        pnd_Parm_Pnd_Next = pnd_Parm__R_Field_1.newFieldInGroup("pnd_Parm_Pnd_Next", "#NEXT", FieldType.STRING, 5);
        pnd_Parm_Pnd_Next_Dates = pnd_Parm__R_Field_1.newFieldInGroup("pnd_Parm_Pnd_Next_Dates", "#NEXT-DATES", FieldType.STRING, 20);

        pnd_Parm__R_Field_2 = pnd_Parm__R_Field_1.newGroupInGroup("pnd_Parm__R_Field_2", "REDEFINE", pnd_Parm_Pnd_Next_Dates);
        pnd_Parm_Pnd_Next_From_Dte = pnd_Parm__R_Field_2.newFieldInGroup("pnd_Parm_Pnd_Next_From_Dte", "#NEXT-FROM-DTE", FieldType.STRING, 8);
        pnd_Parm_Pnd_Next_To = pnd_Parm__R_Field_2.newFieldInGroup("pnd_Parm_Pnd_Next_To", "#NEXT-TO", FieldType.STRING, 4);
        pnd_Parm_Pnd_Next_To_Dte = pnd_Parm__R_Field_2.newFieldInGroup("pnd_Parm_Pnd_Next_To_Dte", "#NEXT-TO-DTE", FieldType.STRING, 8);
        pnd_Parm_Pnd_Prev = pnd_Parm__R_Field_1.newFieldInGroup("pnd_Parm_Pnd_Prev", "#PREV", FieldType.STRING, 7);
        pnd_Parm_Pnd_Prev_Dates = pnd_Parm__R_Field_1.newFieldInGroup("pnd_Parm_Pnd_Prev_Dates", "#PREV-DATES", FieldType.STRING, 20);

        pnd_Parm__R_Field_3 = pnd_Parm__R_Field_1.newGroupInGroup("pnd_Parm__R_Field_3", "REDEFINE", pnd_Parm_Pnd_Prev_Dates);
        pnd_Parm_Pnd_Prev_From_Dte = pnd_Parm__R_Field_3.newFieldInGroup("pnd_Parm_Pnd_Prev_From_Dte", "#PREV-FROM-DTE", FieldType.STRING, 8);
        pnd_Parm_Pnd_Prev_To = pnd_Parm__R_Field_3.newFieldInGroup("pnd_Parm_Pnd_Prev_To", "#PREV-TO", FieldType.STRING, 4);
        pnd_Parm_Pnd_Prev_To_Dte = pnd_Parm__R_Field_3.newFieldInGroup("pnd_Parm_Pnd_Prev_To_Dte", "#PREV-TO-DTE", FieldType.STRING, 8);
        pnd_Parm_Pnd_Run = pnd_Parm__R_Field_1.newFieldInGroup("pnd_Parm_Pnd_Run", "#RUN", FieldType.STRING, 3);
        pnd_Parm_Pnd_Run_Dte = pnd_Parm__R_Field_1.newFieldInGroup("pnd_Parm_Pnd_Run_Dte", "#RUN-DTE", FieldType.STRING, 8);
        pnd_Dte_From = localVariables.newFieldInRecord("pnd_Dte_From", "#DTE-FROM", FieldType.DATE);
        pnd_Dte_To = localVariables.newFieldInRecord("pnd_Dte_To", "#DTE-TO", FieldType.DATE);

        vw_cwf_Mit1 = new DataAccessProgramView(new NameInfo("vw_cwf_Mit1", "CWF-MIT1"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Mit1_Pin_Nbr = vw_cwf_Mit1.getRecord().newFieldInGroup("cwf_Mit1_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Mit1_Pin_Nbr.setDdmHeader("PIN");
        cwf_Mit1_Rqst_Log_Dte_Tme = vw_cwf_Mit1.getRecord().newFieldInGroup("cwf_Mit1_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        cwf_Mit1_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Mit1_Case_Id_Cde = vw_cwf_Mit1.getRecord().newFieldInGroup("cwf_Mit1_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CASE_ID_CDE");
        cwf_Mit1_Case_Id_Cde.setDdmHeader("CASE/ID");

        cwf_Mit1__R_Field_4 = vw_cwf_Mit1.getRecord().newGroupInGroup("cwf_Mit1__R_Field_4", "REDEFINE", cwf_Mit1_Case_Id_Cde);
        cwf_Mit1_Case_Cde = cwf_Mit1__R_Field_4.newFieldInGroup("cwf_Mit1_Case_Cde", "CASE-CDE", FieldType.STRING, 1);
        cwf_Mit1_Sub_Rqst_Seq = cwf_Mit1__R_Field_4.newFieldInGroup("cwf_Mit1_Sub_Rqst_Seq", "SUB-RQST-SEQ", FieldType.STRING, 1);
        cwf_Mit1_Multi_Rqst_Ind = vw_cwf_Mit1.getRecord().newFieldInGroup("cwf_Mit1_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MULTI_RQST_IND");
        cwf_Mit1_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Mit1_Work_Prcss_Id = vw_cwf_Mit1.getRecord().newFieldInGroup("cwf_Mit1_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Mit1_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Mit1_Rqst_Id = vw_cwf_Mit1.getRecord().newFieldInGroup("cwf_Mit1_Rqst_Id", "RQST-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Mit1_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Mit1__R_Field_5 = vw_cwf_Mit1.getRecord().newGroupInGroup("cwf_Mit1__R_Field_5", "REDEFINE", cwf_Mit1_Rqst_Id);
        cwf_Mit1_R_Wpid = cwf_Mit1__R_Field_5.newFieldInGroup("cwf_Mit1_R_Wpid", "R-WPID", FieldType.STRING, 6);
        cwf_Mit1_R_Rcvd_Dte = cwf_Mit1__R_Field_5.newFieldInGroup("cwf_Mit1_R_Rcvd_Dte", "R-RCVD-DTE", FieldType.STRING, 8);
        cwf_Mit1_R_Case = cwf_Mit1__R_Field_5.newFieldInGroup("cwf_Mit1_R_Case", "R-CASE", FieldType.STRING, 1);
        cwf_Mit1_R_Sub_Rqst = cwf_Mit1__R_Field_5.newFieldInGroup("cwf_Mit1_R_Sub_Rqst", "R-SUB-RQST", FieldType.STRING, 1);
        cwf_Mit1_R_Pin = cwf_Mit1__R_Field_5.newFieldInGroup("cwf_Mit1_R_Pin", "R-PIN", FieldType.NUMERIC, 12);
        cwf_Mit1_Tiaa_Rcvd_Dte = vw_cwf_Mit1.getRecord().newFieldInGroup("cwf_Mit1_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Mit1_Crprte_Clock_End_Dte_Tme = vw_cwf_Mit1.getRecord().newFieldInGroup("cwf_Mit1_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Mit1_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");

        cwf_Mit1__R_Field_6 = vw_cwf_Mit1.getRecord().newGroupInGroup("cwf_Mit1__R_Field_6", "REDEFINE", cwf_Mit1_Crprte_Clock_End_Dte_Tme);
        cwf_Mit1_Crprte_Clock_End_Dte = cwf_Mit1__R_Field_6.newFieldInGroup("cwf_Mit1_Crprte_Clock_End_Dte", "CRPRTE-CLOCK-END-DTE", FieldType.STRING, 
            8);
        registerRecord(vw_cwf_Mit1);

        vw_cwf_Mit2 = new DataAccessProgramView(new NameInfo("vw_cwf_Mit2", "CWF-MIT2"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Mit2_Rqst_Log_Dte_Tme = vw_cwf_Mit2.getRecord().newFieldInGroup("cwf_Mit2_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        cwf_Mit2_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Mit2_Admin_Unit_Cde = vw_cwf_Mit2.getRecord().newFieldInGroup("cwf_Mit2_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        registerRecord(vw_cwf_Mit2);

        vw_cwf_Dcmt = new DataAccessProgramView(new NameInfo("vw_cwf_Dcmt", "CWF-DCMT"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Dcmt_Entry_System_Or_Unit = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Entry_System_Or_Unit", "ENTRY-SYSTEM-OR-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_SYSTEM_OR_UNIT");
        cwf_Dcmt_Entry_System_Or_Unit.setDdmHeader("CREATED BY/SYSTEM OR UNIT");

        cwf_Dcmt_Document_Type = vw_cwf_Dcmt.getRecord().newGroupInGroup("CWF_DCMT_DOCUMENT_TYPE", "DOCUMENT-TYPE");
        cwf_Dcmt_Document_Type.setDdmHeader("DOCUMENT/TYPE");
        cwf_Dcmt_Document_Category = cwf_Dcmt_Document_Type.newFieldInGroup("cwf_Dcmt_Document_Category", "DOCUMENT-CATEGORY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DOCUMENT_CATEGORY");
        cwf_Dcmt_Document_Category.setDdmHeader("DOC/CAT");
        cwf_Dcmt_Document_Sub_Category = cwf_Dcmt_Document_Type.newFieldInGroup("cwf_Dcmt_Document_Sub_Category", "DOCUMENT-SUB-CATEGORY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DOCUMENT_SUB_CATEGORY");
        cwf_Dcmt_Document_Sub_Category.setDdmHeader("DOC SUB-/CATEGORY");
        cwf_Dcmt_Document_Detail = cwf_Dcmt_Document_Type.newFieldInGroup("cwf_Dcmt_Document_Detail", "DOCUMENT-DETAIL", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DOCUMENT_DETAIL");
        cwf_Dcmt_Document_Detail.setDdmHeader("DOCUMENT/DETAIL");
        cwf_Dcmt_Document_Key = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Document_Key", "DOCUMENT-KEY", FieldType.BINARY, 34, RepeatingFieldStrategy.None, 
            "DOCUMENT_KEY");
        cwf_Dcmt_Document_Key.setDdmHeader("DOCUMENT/KEY");
        cwf_Dcmt_Document_Key.setSuperDescriptor(true);

        cwf_Dcmt__R_Field_7 = vw_cwf_Dcmt.getRecord().newGroupInGroup("cwf_Dcmt__R_Field_7", "REDEFINE", cwf_Dcmt_Document_Key);
        cwf_Dcmt_Folder_Key = cwf_Dcmt__R_Field_7.newFieldInGroup("cwf_Dcmt_Folder_Key", "FOLDER-KEY", FieldType.STRING, 27);
        cwf_Dcmt_Document_Direction_Ind = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Document_Direction_Ind", "DOCUMENT-DIRECTION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOCUMENT_DIRECTION_IND");
        cwf_Dcmt_Document_Direction_Ind.setDdmHeader("DOC/DIR");
        cwf_Dcmt_Document_Retention_Ind = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Document_Retention_Ind", "DOCUMENT-RETENTION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOCUMENT_RETENTION_IND");
        cwf_Dcmt_Document_Retention_Ind.setDdmHeader("DOC/RETNTN");
        cwf_Dcmt_Document_Subtype = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Document_Subtype", "DOCUMENT-SUBTYPE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DOCUMENT_SUBTYPE");
        cwf_Dcmt_Document_Subtype.setDdmHeader("DOC/SUBTP");

        cwf_Dcmt__R_Field_8 = vw_cwf_Dcmt.getRecord().newGroupInGroup("cwf_Dcmt__R_Field_8", "REDEFINE", cwf_Dcmt_Document_Subtype);
        cwf_Dcmt_Doc_Text = cwf_Dcmt__R_Field_8.newFieldInGroup("cwf_Dcmt_Doc_Text", "DOC-TEXT", FieldType.STRING, 1);
        cwf_Dcmt_Doc_Image = cwf_Dcmt__R_Field_8.newFieldInGroup("cwf_Dcmt_Doc_Image", "DOC-IMAGE", FieldType.STRING, 1);
        cwf_Dcmt_Doc_Kdo = cwf_Dcmt__R_Field_8.newFieldInGroup("cwf_Dcmt_Doc_Kdo", "DOC-KDO", FieldType.STRING, 1);
        cwf_Dcmt_Entry_System = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Entry_System", "ENTRY-SYSTEM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ENTRY_SYSTEM");
        cwf_Dcmt_Entry_System.setDdmHeader("ENTRY/SYSTEM");
        registerRecord(vw_cwf_Dcmt);

        pnd_Document_Key = localVariables.newFieldInRecord("pnd_Document_Key", "#DOCUMENT-KEY", FieldType.STRING, 34);

        pnd_Document_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Document_Key__R_Field_9", "REDEFINE", pnd_Document_Key);
        pnd_Document_Key_Pnd_Folder_Key = pnd_Document_Key__R_Field_9.newFieldInGroup("pnd_Document_Key_Pnd_Folder_Key", "#FOLDER-KEY", FieldType.STRING, 
            27);
        pnd_Document_Key_Pnd_Entry_Dte_Tme = pnd_Document_Key__R_Field_9.newFieldInGroup("pnd_Document_Key_Pnd_Entry_Dte_Tme", "#ENTRY-DTE-TME", FieldType.TIME);

        vw_cwf_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Tbl", "CWF-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Tbl_Tbl_Data_Field = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");
        cwf_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE_TME");
        cwf_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Tbl_Tbl_Updte_Dte = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);
        pnd_Work_Record = localVariables.newFieldInRecord("pnd_Work_Record", "#WORK-RECORD", FieldType.STRING, 65);

        pnd_Work_Record__R_Field_10 = localVariables.newGroupInRecord("pnd_Work_Record__R_Field_10", "REDEFINE", pnd_Work_Record);
        pnd_Work_Record_Pnd_Work_Type = pnd_Work_Record__R_Field_10.newFieldInGroup("pnd_Work_Record_Pnd_Work_Type", "#WORK-TYPE", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_Work_Folder = pnd_Work_Record__R_Field_10.newFieldInGroup("pnd_Work_Record_Pnd_Work_Folder", "#WORK-FOLDER", FieldType.STRING, 
            27);

        pnd_Work_Record__R_Field_11 = pnd_Work_Record__R_Field_10.newGroupInGroup("pnd_Work_Record__R_Field_11", "REDEFINE", pnd_Work_Record_Pnd_Work_Folder);
        pnd_Work_Record_Pnd_Cabinet_Id = pnd_Work_Record__R_Field_11.newFieldInGroup("pnd_Work_Record_Pnd_Cabinet_Id", "#CABINET-ID", FieldType.STRING, 
            14);

        pnd_Work_Record__R_Field_12 = pnd_Work_Record__R_Field_11.newGroupInGroup("pnd_Work_Record__R_Field_12", "REDEFINE", pnd_Work_Record_Pnd_Cabinet_Id);
        pnd_Work_Record_Pnd_Cab_Pfx = pnd_Work_Record__R_Field_12.newFieldInGroup("pnd_Work_Record_Pnd_Cab_Pfx", "#CAB-PFX", FieldType.STRING, 1);
        pnd_Work_Record_Pnd_Pin_Nbr = pnd_Work_Record__R_Field_12.newFieldInGroup("pnd_Work_Record_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Work_Record_Pnd_Tiaa_Rcvd_Dte = pnd_Work_Record__R_Field_11.newFieldInGroup("pnd_Work_Record_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Work_Record_Pnd_Case_Id = pnd_Work_Record__R_Field_11.newFieldInGroup("pnd_Work_Record_Pnd_Case_Id", "#CASE-ID", FieldType.STRING, 1);
        pnd_Work_Record_Pnd_Wpid = pnd_Work_Record__R_Field_11.newFieldInGroup("pnd_Work_Record_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Work_Record_Pnd_Multi_Rqst_Ind = pnd_Work_Record__R_Field_11.newFieldInGroup("pnd_Work_Record_Pnd_Multi_Rqst_Ind", "#MULTI-RQST-IND", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_Sub_Rqst = pnd_Work_Record__R_Field_11.newFieldInGroup("pnd_Work_Record_Pnd_Sub_Rqst", "#SUB-RQST", FieldType.STRING, 1);

        pnd_Work_Record__R_Field_13 = pnd_Work_Record__R_Field_10.newGroupInGroup("pnd_Work_Record__R_Field_13", "REDEFINE", pnd_Work_Record_Pnd_Work_Folder);
        pnd_Work_Record_Pnd_Work_From_Dte = pnd_Work_Record__R_Field_13.newFieldInGroup("pnd_Work_Record_Pnd_Work_From_Dte", "#WORK-FROM-DTE", FieldType.STRING, 
            8);
        pnd_Work_Record_Pnd_Work_To_Dte = pnd_Work_Record__R_Field_13.newFieldInGroup("pnd_Work_Record_Pnd_Work_To_Dte", "#WORK-TO-DTE", FieldType.STRING, 
            8);
        pnd_Work_Record_Pnd_Work_Unit_1st = pnd_Work_Record__R_Field_10.newFieldInGroup("pnd_Work_Record_Pnd_Work_Unit_1st", "#WORK-UNIT-1ST", FieldType.STRING, 
            8);

        pnd_Work_Record_Pnd_Work_Doc_Type = pnd_Work_Record__R_Field_10.newGroupInGroup("pnd_Work_Record_Pnd_Work_Doc_Type", "#WORK-DOC-TYPE");
        pnd_Work_Record_Document_Category = pnd_Work_Record_Pnd_Work_Doc_Type.newFieldInGroup("pnd_Work_Record_Document_Category", "DOCUMENT-CATEGORY", 
            FieldType.STRING, 1);
        pnd_Work_Record_Document_Sub_Category = pnd_Work_Record_Pnd_Work_Doc_Type.newFieldInGroup("pnd_Work_Record_Document_Sub_Category", "DOCUMENT-SUB-CATEGORY", 
            FieldType.STRING, 3);
        pnd_Work_Record_Document_Detail = pnd_Work_Record_Pnd_Work_Doc_Type.newFieldInGroup("pnd_Work_Record_Document_Detail", "DOCUMENT-DETAIL", FieldType.STRING, 
            4);
        pnd_Work_Record_Pnd_Work_Doc_Sub_Type = pnd_Work_Record__R_Field_10.newFieldInGroup("pnd_Work_Record_Pnd_Work_Doc_Sub_Type", "#WORK-DOC-SUB-TYPE", 
            FieldType.STRING, 1);
        pnd_Work_Record_Pnd_Work_System = pnd_Work_Record__R_Field_10.newFieldInGroup("pnd_Work_Record_Pnd_Work_System", "#WORK-SYSTEM", FieldType.STRING, 
            8);
        pnd_Work_Record_Pnd_Work_Drctn = pnd_Work_Record__R_Field_10.newFieldInGroup("pnd_Work_Record_Pnd_Work_Drctn", "#WORK-DRCTN", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_Work_Unit_Entry = pnd_Work_Record__R_Field_10.newFieldInGroup("pnd_Work_Record_Pnd_Work_Unit_Entry", "#WORK-UNIT-ENTRY", FieldType.STRING, 
            8);
        pnd_Mit1_Cnt = localVariables.newFieldInRecord("pnd_Mit1_Cnt", "#MIT1-CNT", FieldType.NUMERIC, 7);
        pnd_Mit2_Cnt = localVariables.newFieldInRecord("pnd_Mit2_Cnt", "#MIT2-CNT", FieldType.NUMERIC, 7);
        pnd_Dcmt_Cnt = localVariables.newFieldInRecord("pnd_Dcmt_Cnt", "#DCMT-CNT", FieldType.NUMERIC, 7);
        pnd_Del_Cnt = localVariables.newFieldInRecord("pnd_Del_Cnt", "#DEL-CNT", FieldType.NUMERIC, 4);
        first_Dcmt = localVariables.newFieldInRecord("first_Dcmt", "FIRST-DCMT", FieldType.BOOLEAN, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Mit1.reset();
        vw_cwf_Mit2.reset();
        vw_cwf_Dcmt.reset();
        vw_cwf_Tbl.reset();

        localVariables.reset();
        pnd_Tbl_Prime_Key.setInitialValue("A CWF-REPORT12-INPUT  EFSB2100");
        pnd_Work_Record.setInitialValue("20P0000000 ");
        first_Dcmt.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Efsb2100() throws Exception
    {
        super("Efsb2100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("EFSB2100", onError);
        setupReports();
        //*  ===================================================================
        //*  ----------------------------------------                                                                                                                     //Natural: ON ERROR
        vw_cwf_Tbl.startDatabaseFind                                                                                                                                      //Natural: FIND ( 1 ) CWF-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "FIND01",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_cwf_Tbl.readNextRow("FIND01", true)))
        {
            vw_cwf_Tbl.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Tbl.getAstCOUNTER().equals(0)))                                                                                                          //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "Support Table record",pnd_Tbl_Prime_Key,"not found");                                                                              //Natural: WRITE 'Support Table record' #TBL-PRIME-KEY 'not found'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE IMMEDIATE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Isn.setValue(vw_cwf_Tbl.getAstISN("Find01"));                                                                                                             //Natural: MOVE *ISN TO #ISN
            pnd_Parm.setValue(cwf_Tbl_Tbl_Data_Field.getSubstring(1,63));                                                                                                 //Natural: MOVE SUBSTRING ( TBL-DATA-FIELD,1,63 ) TO #PARM
            pnd_Dte_From.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Parm_Pnd_Next_To_Dte);                                                                         //Natural: MOVE EDITED #PARM.#NEXT-TO-DTE TO #DTE-FROM ( EM = YYYYMMDD )
            pnd_Dte_From.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #DTE-FROM
            pnd_Dte_To.compute(new ComputeParameters(false, pnd_Dte_To), pnd_Dte_From.add(6));                                                                            //Natural: COMPUTE #DTE-TO = #DTE-FROM + 6
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  PIN-EXP
        getReports().display(0, "#PARM.#NEXT-FROM-DTE",                                                                                                                   //Natural: DISPLAY '#PARM.#NEXT-FROM-DTE' #PARM.#NEXT-FROM-DTE
        		pnd_Parm_Pnd_Next_From_Dte);
        if (Global.isEscape()) return;
        //*  ----------------------------------------
        vw_cwf_Mit1.startDatabaseRead                                                                                                                                     //Natural: READ CWF-MIT1 BY PRTCPNT-CLOSED-KEY FROM #PARM.#NEXT-FROM-DTE
        (
        "READ01",
        new Wc[] { new Wc("PRTCPNT_CLOSED_KEY", ">=", pnd_Parm_Pnd_Next_From_Dte, WcType.BY) },
        new Oc[] { new Oc("PRTCPNT_CLOSED_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Mit1.readNextRow("READ01")))
        {
            //*  PIN-EXP
            getReports().display(0, " ENTERED");                                                                                                                          //Natural: DISPLAY ' ENTERED'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(cwf_Mit1_Crprte_Clock_End_Dte.greater(pnd_Parm_Pnd_Next_To_Dte)))                                                                               //Natural: IF CRPRTE-CLOCK-END-DTE > #PARM.#NEXT-TO-DTE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Mit1_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #MIT1-CNT
            pnd_Work_Record_Pnd_Multi_Rqst_Ind.setValue(cwf_Mit1_Multi_Rqst_Ind);                                                                                         //Natural: MOVE MULTI-RQST-IND TO #MULTI-RQST-IND
            if (condition(cwf_Mit1_R_Wpid.notEquals("      ")))                                                                                                           //Natural: IF R-WPID NOT = '      '
            {
                pnd_Work_Record_Pnd_Pin_Nbr.setValue(cwf_Mit1_R_Pin);                                                                                                     //Natural: MOVE R-PIN TO #PIN-NBR
                pnd_Work_Record_Pnd_Tiaa_Rcvd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Mit1_R_Rcvd_Dte);                                                     //Natural: MOVE EDITED R-RCVD-DTE TO #TIAA-RCVD-DTE ( EM = YYYYMMDD )
                pnd_Work_Record_Pnd_Case_Id.setValue(cwf_Mit1_R_Case);                                                                                                    //Natural: MOVE R-CASE TO #CASE-ID
                pnd_Work_Record_Pnd_Wpid.setValue(cwf_Mit1_R_Wpid);                                                                                                       //Natural: MOVE R-WPID TO #WPID
                pnd_Work_Record_Pnd_Sub_Rqst.setValue(cwf_Mit1_R_Sub_Rqst);                                                                                               //Natural: MOVE R-SUB-RQST TO #SUB-RQST
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  NEW 11/27/95
                pnd_Work_Record_Pnd_Pin_Nbr.setValue(cwf_Mit1_Pin_Nbr);                                                                                                   //Natural: MOVE PIN-NBR TO #PIN-NBR
                pnd_Work_Record_Pnd_Tiaa_Rcvd_Dte.setValue(cwf_Mit1_Tiaa_Rcvd_Dte);                                                                                       //Natural: MOVE TIAA-RCVD-DTE TO #TIAA-RCVD-DTE
                pnd_Work_Record_Pnd_Case_Id.setValue(cwf_Mit1_Case_Cde);                                                                                                  //Natural: MOVE CASE-CDE TO #CASE-ID
                pnd_Work_Record_Pnd_Wpid.setValue(cwf_Mit1_Work_Prcss_Id);                                                                                                //Natural: MOVE WORK-PRCSS-ID TO #WPID
                pnd_Work_Record_Pnd_Sub_Rqst.setValue(cwf_Mit1_Sub_Rqst_Seq);                                                                                             //Natural: MOVE SUB-RQST-SEQ TO #SUB-RQST
            }                                                                                                                                                             //Natural: END-IF
            //*  ----------------------------
            vw_cwf_Mit2.startDatabaseRead                                                                                                                                 //Natural: READ ( 1 ) CWF-MIT2 BY RQST-ROUTING-KEY FROM CWF-MIT1.RQST-LOG-DTE-TME
            (
            "READ02",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", cwf_Mit1_Rqst_Log_Dte_Tme, WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
            1
            );
            READ02:
            while (condition(vw_cwf_Mit2.readNextRow("READ02")))
            {
                if (condition(cwf_Mit1_Rqst_Log_Dte_Tme.notEquals(cwf_Mit2_Rqst_Log_Dte_Tme)))                                                                            //Natural: IF CWF-MIT1.RQST-LOG-DTE-TME NOT = CWF-MIT2.RQST-LOG-DTE-TME
                {
                    getReports().write(0, "Missing MIT=",pnd_Work_Record);                                                                                                //Natural: WRITE 'Missing MIT=' #WORK-RECORD
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Mit2_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #MIT2-CNT
                pnd_Work_Record_Pnd_Work_Unit_1st.setValue(cwf_Mit2_Admin_Unit_Cde);                                                                                      //Natural: MOVE ADMIN-UNIT-CDE TO #WORK-UNIT-1ST
                pnd_Document_Key_Pnd_Folder_Key.setValue(pnd_Work_Record_Pnd_Work_Folder);                                                                                //Natural: MOVE #WORK-FOLDER TO #FOLDER-KEY
                pnd_Document_Key_Pnd_Entry_Dte_Tme.setValue(0);                                                                                                           //Natural: MOVE 0 TO #ENTRY-DTE-TME
                //*  ----------------
                first_Dcmt.setValue(true);                                                                                                                                //Natural: ASSIGN FIRST-DCMT = TRUE
                vw_cwf_Dcmt.startDatabaseRead                                                                                                                             //Natural: READ CWF-DCMT BY DOCUMENT-KEY FROM #DOCUMENT-KEY
                (
                "READ03",
                new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Document_Key.getBinary(), WcType.BY) },
                new Oc[] { new Oc("DOCUMENT_KEY", "ASC") }
                );
                READ03:
                while (condition(vw_cwf_Dcmt.readNextRow("READ03")))
                {
                    if (condition(cwf_Dcmt_Folder_Key.notEquals(pnd_Document_Key_Pnd_Folder_Key)))                                                                        //Natural: IF FOLDER-KEY NOT = #FOLDER-KEY
                    {
                        //* *      IF FIRST-DCMT = TRUE                        /* DELETED 10/3/95
                        //* *        WRITE #WORK-RECORD 'has no Documents'
                        //* *      END-IF
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    first_Dcmt.setValue(false);                                                                                                                           //Natural: ASSIGN FIRST-DCMT = FALSE
                    pnd_Work_Record_Pnd_Work_Doc_Type.setValuesByName(cwf_Dcmt_Document_Type);                                                                            //Natural: MOVE BY NAME DOCUMENT-TYPE TO #WORK-DOC-TYPE
                    pnd_Work_Record_Pnd_Work_Unit_Entry.setValue(cwf_Dcmt_Entry_System_Or_Unit);                                                                          //Natural: MOVE ENTRY-SYSTEM-OR-UNIT TO #WORK-UNIT-ENTRY
                    pnd_Work_Record_Pnd_Work_Drctn.setValue(cwf_Dcmt_Document_Direction_Ind);                                                                             //Natural: MOVE DOCUMENT-DIRECTION-IND TO #WORK-DRCTN
                    pnd_Work_Record_Pnd_Work_System.setValue(cwf_Dcmt_Entry_System);                                                                                      //Natural: MOVE ENTRY-SYSTEM TO #WORK-SYSTEM
                    if (condition(cwf_Dcmt_Doc_Image.equals("I")))                                                                                                        //Natural: IF CWF-DCMT.DOC-IMAGE = 'I'
                    {
                        //*  IMAGE
                        pnd_Work_Record_Pnd_Work_Doc_Sub_Type.setValue("I");                                                                                              //Natural: ASSIGN #WORK-DOC-SUB-TYPE = 'I'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cwf_Dcmt_Doc_Text.equals("T")))                                                                                                     //Natural: IF CWF-DCMT.DOC-TEXT = 'T'
                        {
                            //*  TEXT
                            pnd_Work_Record_Pnd_Work_Doc_Sub_Type.setValue("T");                                                                                          //Natural: ASSIGN #WORK-DOC-SUB-TYPE = 'T'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  NEITHER TEXT NOR IMAGE
                            pnd_Work_Record_Pnd_Work_Doc_Sub_Type.setValue("O");                                                                                          //Natural: ASSIGN #WORK-DOC-SUB-TYPE = 'O'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  DELETED
                    if (condition(cwf_Dcmt_Document_Retention_Ind.equals("D")))                                                                                           //Natural: IF DOCUMENT-RETENTION-IND = 'D'
                    {
                        //* *      WRITE #WORK-RECORD 'is a deleted Document'  /* DELETED 10/3/95
                        pnd_Del_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #DEL-CNT
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Dcmt_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #DCMT-CNT
                    //* *    WRITE         #WORK-RECORD            /* FOR TEST ONLY !!!!
                    getWorkFiles().write(1, false, pnd_Work_Record);                                                                                                      //Natural: WRITE WORK FILE 1 #WORK-RECORD
                    //*  DOCUMENT
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ----------------
                //*  MIT2
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ----------------------------
            //*  MIT1
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ---------------------------------------- PUT CTL INFO ON WORK & TABLE
        pnd_Work_Record_Pnd_Work_Type.setValue("10");                                                                                                                     //Natural: ASSIGN #WORK-TYPE = '10'
        pnd_Work_Record_Pnd_Work_From_Dte.setValue(pnd_Parm_Pnd_Next_From_Dte);                                                                                           //Natural: ASSIGN #WORK-FROM-DTE = #PARM.#NEXT-FROM-DTE
        pnd_Work_Record_Pnd_Work_To_Dte.setValue(pnd_Parm_Pnd_Next_To_Dte);                                                                                               //Natural: ASSIGN #WORK-TO-DTE = #PARM.#NEXT-TO-DTE
        pnd_Work_Record_Pnd_Work_Unit_1st.reset();                                                                                                                        //Natural: RESET #WORK-UNIT-1ST #WORK-UNIT-ENTRY
        pnd_Work_Record_Pnd_Work_Unit_Entry.reset();
        getWorkFiles().write(1, false, pnd_Work_Record);                                                                                                                  //Natural: WRITE WORK FILE 1 #WORK-RECORD
        //*  ----------------------
        GET_TBL:                                                                                                                                                          //Natural: GET CWF-TBL #ISN
        vw_cwf_Tbl.readByID(pnd_Isn.getLong(), "GET_TBL");
        pnd_Parm_Pnd_Prev_Dates.setValue(pnd_Parm_Pnd_Next_Dates);                                                                                                        //Natural: MOVE #PARM.#NEXT-DATES TO #PARM.#PREV-DATES
        pnd_Parm_Pnd_Next_From_Dte.setValueEdited(pnd_Dte_From,new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED #DTE-FROM ( EM = YYYYMMDD ) TO #PARM.#NEXT-FROM-DTE
        pnd_Parm_Pnd_Next_To_Dte.setValueEdited(pnd_Dte_To,new ReportEditMask("YYYYMMDD"));                                                                               //Natural: MOVE EDITED #DTE-TO ( EM = YYYYMMDD ) TO #PARM.#NEXT-TO-DTE
        pnd_Parm_Pnd_Run_Dte.setValue(Global.getDATU());                                                                                                                  //Natural: MOVE *DATU TO #PARM.#RUN-DTE
        cwf_Tbl_Tbl_Data_Field.setValue(pnd_Parm);                                                                                                                        //Natural: MOVE #PARM TO TBL-DATA-FIELD
        cwf_Tbl_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                             //Natural: MOVE *TIMX TO TBL-UPDTE-DTE-TME
        cwf_Tbl_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                                 //Natural: MOVE *DATX TO TBL-UPDTE-DTE
        cwf_Tbl_Tbl_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                                      //Natural: MOVE *INIT-USER TO TBL-UPDTE-OPRTR-CDE
        vw_cwf_Tbl.updateDBRow("GET_TBL");                                                                                                                                //Natural: UPDATE ( GET-TBL. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  ----------------------------------------
        getReports().write(0, pnd_Mit1_Cnt,"MI Items Participant closed");                                                                                                //Natural: WRITE #MIT1-CNT 'MI Items Participant closed'
        if (Global.isEscape()) return;
        getReports().write(0, pnd_Dcmt_Cnt,"Documents selected");                                                                                                         //Natural: WRITE #DCMT-CNT 'Documents selected'
        if (Global.isEscape()) return;
        //*  ==============================================
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "NATURAL error",Global.getERROR_NR(),"in",Global.getPROGRAM(),"at line",Global.getERROR_LINE());                                            //Natural: WRITE 'NATURAL error' *ERROR-NR 'in' *PROGRAM 'at line' *ERROR-LINE
        DbsUtil.terminate(5);  if (true) return;                                                                                                                          //Natural: TERMINATE 05
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(0, "#PARM.#NEXT-FROM-DTE",
        		pnd_Parm_Pnd_Next_From_Dte);
    }
}
