/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:32:39 PM
**        * FROM NATURAL PROGRAM : Cwfb4031
************************************************************
**        * FILE NAME            : Cwfb4031.java
**        * CLASS NAME           : Cwfb4031
**        * INSTANCE NAME        : Cwfb4031
************************************************************
************************************************************************
* PROGRAM  : CWFB4031
* SYSTEM   : CRPCWF
* TITLE    : EMPLOYEE TABLE EXTRACT
* GENERATED: SEP 15,95 AT 12:01 PM
* FUNCTION : THIS PROGRAM READS THE CWF-ORG-EMPL-TBL-POST AND WRITES
*            A WORK FILE TO BE DOWNLOADED TO THE SQL SERVER
* CHANGES  : L.E. OUTPUT FILE HAS BEEN EXPANDED FOR 100 BYTES(EMPL-INFO)
*
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4031 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Org_Empl_Tbl_Post;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Unit_Cde;

    private DbsGroup cwf_Org_Empl_Tbl_Post__R_Field_1;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Unit_Id_Cde;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Unit_Rgn_Cde;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Unit_Brnch_Group_Cde;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Racf_Id;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Nme;

    private DbsGroup cwf_Org_Empl_Tbl_Post__R_Field_2;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_First_Nme;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Last_Nme;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Title_Nme;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Extnsn_Nbr;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Table_Updte_Authrty_Cde;

    private DbsGroup cwf_Org_Empl_Tbl_Post_Empl_Scrty_Group;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Systm_Cde;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Scrty_Lvl_Ind;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Signatory;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Unit_Work_Nme;
    private DbsField cwf_Org_Empl_Tbl_Post_Actve_Ind;

    private DbsGroup cwf_Org_Empl_Tbl_Post__R_Field_3;
    private DbsField cwf_Org_Empl_Tbl_Post_Fill_1;
    private DbsField cwf_Org_Empl_Tbl_Post_Actve_Ind_2_2;
    private DbsField cwf_Org_Empl_Tbl_Post_Dlte_Dte_Tme;
    private DbsField cwf_Org_Empl_Tbl_Post_Dlte_Oprtr_Cde;
    private DbsField cwf_Org_Empl_Tbl_Post_Entry_Dte_Tme;
    private DbsField cwf_Org_Empl_Tbl_Post_Entry_Oprtr_Cde;
    private DbsField cwf_Org_Empl_Tbl_Post_Updte_Actn_Cde;
    private DbsField cwf_Org_Empl_Tbl_Post_Updte_Dte;
    private DbsField cwf_Org_Empl_Tbl_Post_Updte_Dte_Tme;
    private DbsField cwf_Org_Empl_Tbl_Post_Updte_Oprtr_Cde;
    private DbsField cwf_Org_Empl_Tbl_Post_Blltn_Board_Read_Dte;
    private DbsField cwf_Org_Empl_Tbl_Post_Empl_Info;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;

    private DbsGroup work_Record;
    private DbsField work_Record_Empl_Unit_Cde;
    private DbsField work_Record_Pnd_C1;
    private DbsField work_Record_Empl_Nme;
    private DbsField work_Record_Pnd_C2;
    private DbsField work_Record_Empl_Title_Nme;
    private DbsField work_Record_Pnd_C3;
    private DbsField work_Record_Empl_Racf_Id;
    private DbsField work_Record_Pnd_C4;
    private DbsField work_Record_Empl_Table_Updte_Authrty_Cde;
    private DbsField work_Record_Pnd_C5;
    private DbsField work_Record_Empl_Extnsn_Nbr;
    private DbsField work_Record_Pnd_C6;
    private DbsField work_Record_Empl_Signatory;
    private DbsField work_Record_Pnd_C7;
    private DbsField work_Record_Empl_Unit_Work_Nme;
    private DbsField work_Record_Pnd_C8;
    private DbsField work_Record_Empl_Info;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Rex_Read;
    private DbsField pnd_Counters_Pnd_Rex_Written;

    private DbsGroup pnd_Report_Fields;
    private DbsField pnd_Report_Fields_Pnd_Error_Msg;

    private DbsGroup pnd_Contstants;
    private DbsField pnd_Contstants_Pnd_Delimiter;
    private DbsField pnd_Contact_Name;

    private DbsGroup pnd_Contact_Name__R_Field_4;
    private DbsField pnd_Contact_Name_Pnd_Contact_Name_8;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Org_Empl_Tbl_Post = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Empl_Tbl_Post", "CWF-ORG-EMPL-TBL-POST"), "CWF_ORG_EMPL_TBL_POST", 
            "CWF_ASSIGN_RULE", DdmPeriodicGroups.getInstance().getGroups("CWF_ORG_EMPL_TBL_POST"));
        cwf_Org_Empl_Tbl_Post_Empl_Unit_Cde = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Unit_Cde", "EMPL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "EMPL_UNIT_CDE");
        cwf_Org_Empl_Tbl_Post_Empl_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Org_Empl_Tbl_Post__R_Field_1 = vw_cwf_Org_Empl_Tbl_Post.getRecord().newGroupInGroup("cwf_Org_Empl_Tbl_Post__R_Field_1", "REDEFINE", cwf_Org_Empl_Tbl_Post_Empl_Unit_Cde);
        cwf_Org_Empl_Tbl_Post_Empl_Unit_Id_Cde = cwf_Org_Empl_Tbl_Post__R_Field_1.newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Unit_Id_Cde", "EMPL-UNIT-ID-CDE", 
            FieldType.STRING, 5);
        cwf_Org_Empl_Tbl_Post_Empl_Unit_Rgn_Cde = cwf_Org_Empl_Tbl_Post__R_Field_1.newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Unit_Rgn_Cde", "EMPL-UNIT-RGN-CDE", 
            FieldType.STRING, 1);
        cwf_Org_Empl_Tbl_Post_Empl_Unit_Spcl_Dsgntn_Cde = cwf_Org_Empl_Tbl_Post__R_Field_1.newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Unit_Spcl_Dsgntn_Cde", 
            "EMPL-UNIT-SPCL-DSGNTN-CDE", FieldType.STRING, 1);
        cwf_Org_Empl_Tbl_Post_Empl_Unit_Brnch_Group_Cde = cwf_Org_Empl_Tbl_Post__R_Field_1.newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Unit_Brnch_Group_Cde", 
            "EMPL-UNIT-BRNCH-GROUP-CDE", FieldType.STRING, 1);
        cwf_Org_Empl_Tbl_Post_Empl_Racf_Id = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Racf_Id", "EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        cwf_Org_Empl_Tbl_Post_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF  ID");
        cwf_Org_Empl_Tbl_Post_Empl_Nme = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Nme", "EMPL-NME", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "EMPL_NME");
        cwf_Org_Empl_Tbl_Post_Empl_Nme.setDdmHeader("EMPLOYEE NAME");

        cwf_Org_Empl_Tbl_Post__R_Field_2 = vw_cwf_Org_Empl_Tbl_Post.getRecord().newGroupInGroup("cwf_Org_Empl_Tbl_Post__R_Field_2", "REDEFINE", cwf_Org_Empl_Tbl_Post_Empl_Nme);
        cwf_Org_Empl_Tbl_Post_Empl_First_Nme = cwf_Org_Empl_Tbl_Post__R_Field_2.newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_First_Nme", "EMPL-FIRST-NME", 
            FieldType.STRING, 20);
        cwf_Org_Empl_Tbl_Post_Empl_Last_Nme = cwf_Org_Empl_Tbl_Post__R_Field_2.newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Last_Nme", "EMPL-LAST-NME", 
            FieldType.STRING, 20);
        cwf_Org_Empl_Tbl_Post_Empl_Title_Nme = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Title_Nme", "EMPL-TITLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "EMPL_TITLE_NME");
        cwf_Org_Empl_Tbl_Post_Empl_Title_Nme.setDdmHeader("TITLE");
        cwf_Org_Empl_Tbl_Post_Empl_Extnsn_Nbr = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Extnsn_Nbr", "EMPL-EXTNSN-NBR", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "EMPL_EXTNSN_NBR");
        cwf_Org_Empl_Tbl_Post_Empl_Extnsn_Nbr.setDdmHeader("EXT");
        cwf_Org_Empl_Tbl_Post_Empl_Table_Updte_Authrty_Cde = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Table_Updte_Authrty_Cde", 
            "EMPL-TABLE-UPDTE-AUTHRTY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "EMPL_TABLE_UPDTE_AUTHRTY_CDE");
        cwf_Org_Empl_Tbl_Post_Empl_Table_Updte_Authrty_Cde.setDdmHeader("TABLE UPDATE/AUTHORITY");

        cwf_Org_Empl_Tbl_Post_Empl_Scrty_Group = vw_cwf_Org_Empl_Tbl_Post.getRecord().newGroupArrayInGroup("cwf_Org_Empl_Tbl_Post_Empl_Scrty_Group", "EMPL-SCRTY-GROUP", 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_ASSIGN_RULE_EMPL_SCRTY_GROUP");
        cwf_Org_Empl_Tbl_Post_Empl_Systm_Cde = cwf_Org_Empl_Tbl_Post_Empl_Scrty_Group.newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Systm_Cde", "EMPL-SYSTM-CDE", 
            FieldType.STRING, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "EMPL_SYSTM_CDE", "CWF_ASSIGN_RULE_EMPL_SCRTY_GROUP");
        cwf_Org_Empl_Tbl_Post_Empl_Systm_Cde.setDdmHeader("SYSTEM/APPLICATION");
        cwf_Org_Empl_Tbl_Post_Empl_Scrty_Lvl_Ind = cwf_Org_Empl_Tbl_Post_Empl_Scrty_Group.newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Scrty_Lvl_Ind", 
            "EMPL-SCRTY-LVL-IND", FieldType.STRING, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "EMPL_SCRTY_LVL_IND", "CWF_ASSIGN_RULE_EMPL_SCRTY_GROUP");
        cwf_Org_Empl_Tbl_Post_Empl_Scrty_Lvl_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Org_Empl_Tbl_Post_Empl_Signatory = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Signatory", "EMPL-SIGNATORY", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "EMPL_SIGNATORY");
        cwf_Org_Empl_Tbl_Post_Empl_Signatory.setDdmHeader("SIGNATORY");
        cwf_Org_Empl_Tbl_Post_Empl_Unit_Work_Nme = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Unit_Work_Nme", "EMPL-UNIT-WORK-NME", 
            FieldType.STRING, 45, RepeatingFieldStrategy.None, "EMPL_UNIT_WORK_NME");
        cwf_Org_Empl_Tbl_Post_Empl_Unit_Work_Nme.setDdmHeader("EXTERNAL UNIT/NAME");
        cwf_Org_Empl_Tbl_Post_Actve_Ind = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");

        cwf_Org_Empl_Tbl_Post__R_Field_3 = vw_cwf_Org_Empl_Tbl_Post.getRecord().newGroupInGroup("cwf_Org_Empl_Tbl_Post__R_Field_3", "REDEFINE", cwf_Org_Empl_Tbl_Post_Actve_Ind);
        cwf_Org_Empl_Tbl_Post_Fill_1 = cwf_Org_Empl_Tbl_Post__R_Field_3.newFieldInGroup("cwf_Org_Empl_Tbl_Post_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Org_Empl_Tbl_Post_Actve_Ind_2_2 = cwf_Org_Empl_Tbl_Post__R_Field_3.newFieldInGroup("cwf_Org_Empl_Tbl_Post_Actve_Ind_2_2", "ACTVE-IND-2-2", 
            FieldType.STRING, 1);
        cwf_Org_Empl_Tbl_Post_Dlte_Dte_Tme = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Dlte_Dte_Tme", "DLTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Org_Empl_Tbl_Post_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        cwf_Org_Empl_Tbl_Post_Dlte_Oprtr_Cde = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Org_Empl_Tbl_Post_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPERATOR");
        cwf_Org_Empl_Tbl_Post_Entry_Dte_Tme = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Entry_Dte_Tme", "ENTRY-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Org_Empl_Tbl_Post_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        cwf_Org_Empl_Tbl_Post_Entry_Oprtr_Cde = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");
        cwf_Org_Empl_Tbl_Post_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Org_Empl_Tbl_Post_Updte_Actn_Cde = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Updte_Actn_Cde", "UPDTE-ACTN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "UPDTE_ACTN_CDE");
        cwf_Org_Empl_Tbl_Post_Updte_Actn_Cde.setDdmHeader("UPDT/ACTN");
        cwf_Org_Empl_Tbl_Post_Updte_Dte = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Updte_Dte", "UPDTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "UPDTE_DTE");
        cwf_Org_Empl_Tbl_Post_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Org_Empl_Tbl_Post_Updte_Dte_Tme = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Updte_Dte_Tme", "UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UPDTE_DTE_TME");
        cwf_Org_Empl_Tbl_Post_Updte_Oprtr_Cde = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        cwf_Org_Empl_Tbl_Post_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Org_Empl_Tbl_Post_Blltn_Board_Read_Dte = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Blltn_Board_Read_Dte", 
            "BLLTN-BOARD-READ-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "BLLTN_BOARD_READ_DTE");
        cwf_Org_Empl_Tbl_Post_Blltn_Board_Read_Dte.setDdmHeader("BULLETIN/READ-DTE");
        cwf_Org_Empl_Tbl_Post_Empl_Info = vw_cwf_Org_Empl_Tbl_Post.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Post_Empl_Info", "EMPL-INFO", FieldType.STRING, 
            100, RepeatingFieldStrategy.None, "EMPL_INFO");
        registerRecord(vw_cwf_Org_Empl_Tbl_Post);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);

        work_Record = localVariables.newGroupInRecord("work_Record", "WORK-RECORD");
        work_Record_Empl_Unit_Cde = work_Record.newFieldInGroup("work_Record_Empl_Unit_Cde", "EMPL-UNIT-CDE", FieldType.STRING, 8);
        work_Record_Pnd_C1 = work_Record.newFieldInGroup("work_Record_Pnd_C1", "#C1", FieldType.STRING, 1);
        work_Record_Empl_Nme = work_Record.newFieldInGroup("work_Record_Empl_Nme", "EMPL-NME", FieldType.STRING, 40);
        work_Record_Pnd_C2 = work_Record.newFieldInGroup("work_Record_Pnd_C2", "#C2", FieldType.STRING, 1);
        work_Record_Empl_Title_Nme = work_Record.newFieldInGroup("work_Record_Empl_Title_Nme", "EMPL-TITLE-NME", FieldType.STRING, 30);
        work_Record_Pnd_C3 = work_Record.newFieldInGroup("work_Record_Pnd_C3", "#C3", FieldType.STRING, 1);
        work_Record_Empl_Racf_Id = work_Record.newFieldInGroup("work_Record_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        work_Record_Pnd_C4 = work_Record.newFieldInGroup("work_Record_Pnd_C4", "#C4", FieldType.STRING, 1);
        work_Record_Empl_Table_Updte_Authrty_Cde = work_Record.newFieldInGroup("work_Record_Empl_Table_Updte_Authrty_Cde", "EMPL-TABLE-UPDTE-AUTHRTY-CDE", 
            FieldType.STRING, 1);
        work_Record_Pnd_C5 = work_Record.newFieldInGroup("work_Record_Pnd_C5", "#C5", FieldType.STRING, 1);
        work_Record_Empl_Extnsn_Nbr = work_Record.newFieldInGroup("work_Record_Empl_Extnsn_Nbr", "EMPL-EXTNSN-NBR", FieldType.NUMERIC, 4);
        work_Record_Pnd_C6 = work_Record.newFieldInGroup("work_Record_Pnd_C6", "#C6", FieldType.STRING, 1);
        work_Record_Empl_Signatory = work_Record.newFieldInGroup("work_Record_Empl_Signatory", "EMPL-SIGNATORY", FieldType.STRING, 35);
        work_Record_Pnd_C7 = work_Record.newFieldInGroup("work_Record_Pnd_C7", "#C7", FieldType.STRING, 1);
        work_Record_Empl_Unit_Work_Nme = work_Record.newFieldInGroup("work_Record_Empl_Unit_Work_Nme", "EMPL-UNIT-WORK-NME", FieldType.STRING, 45);
        work_Record_Pnd_C8 = work_Record.newFieldInGroup("work_Record_Pnd_C8", "#C8", FieldType.STRING, 1);
        work_Record_Empl_Info = work_Record.newFieldInGroup("work_Record_Empl_Info", "EMPL-INFO", FieldType.STRING, 100);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Rex_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Read", "#REX-READ", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_Rex_Written = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Written", "#REX-WRITTEN", FieldType.PACKED_DECIMAL, 5);

        pnd_Report_Fields = localVariables.newGroupInRecord("pnd_Report_Fields", "#REPORT-FIELDS");
        pnd_Report_Fields_Pnd_Error_Msg = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 60);

        pnd_Contstants = localVariables.newGroupInRecord("pnd_Contstants", "#CONTSTANTS");
        pnd_Contstants_Pnd_Delimiter = pnd_Contstants.newFieldInGroup("pnd_Contstants_Pnd_Delimiter", "#DELIMITER", FieldType.STRING, 1);
        pnd_Contact_Name = localVariables.newFieldInRecord("pnd_Contact_Name", "#CONTACT-NAME", FieldType.STRING, 40);

        pnd_Contact_Name__R_Field_4 = localVariables.newGroupInRecord("pnd_Contact_Name__R_Field_4", "REDEFINE", pnd_Contact_Name);
        pnd_Contact_Name_Pnd_Contact_Name_8 = pnd_Contact_Name__R_Field_4.newFieldInGroup("pnd_Contact_Name_Pnd_Contact_Name_8", "#CONTACT-NAME-8", FieldType.STRING, 
            8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Org_Empl_Tbl_Post.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("     CWF Employee Post Table Download Report");
        pnd_Header1_2.setInitialValue("                        �");
        pnd_Contstants_Pnd_Delimiter.setInitialValue("@");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4031() throws Exception
    {
        super("Cwfb4031");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        vw_cwf_Org_Empl_Tbl_Post.startDatabaseRead                                                                                                                        //Natural: READ CWF-ORG-EMPL-TBL-POST BY EMPL-RACF-ID-KEY
        (
        "READ_PRIME",
        new Oc[] { new Oc("EMPL_RACF_ID_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Org_Empl_Tbl_Post.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            if (condition(cwf_Org_Empl_Tbl_Post_Dlte_Oprtr_Cde.greater(" ")))                                                                                             //Natural: REJECT IF DLTE-OPRTR-CDE GT ' '
            {
                continue;
            }
            pnd_Counters_Pnd_Rex_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #REX-READ
                                                                                                                                                                          //Natural: PERFORM FORMAT-RECORD
            sub_Format_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *SAG END-EXIT
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        getReports().write(1, ReportOption.NOTITLE,"Total Number of Employee Table Records Read  :",pnd_Counters_Pnd_Rex_Read,NEWLINE,"Total Number of Work File Records Written:",pnd_Counters_Pnd_Rex_Written,NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) 'Total Number of Employee Table Records Read  :' #REX-READ / 'Total Number of Work File Records Written:' #REX-WRITTEN //56T 'End of Report'
            TabSetting(56),"End of Report");
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-RECORD
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *****************************
        //* *SAG END-EXIT
    }
    private void sub_Format_Record() throws Exception                                                                                                                     //Natural: FORMAT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        work_Record.reset();                                                                                                                                              //Natural: RESET WORK-RECORD
        work_Record.setValuesByName(vw_cwf_Org_Empl_Tbl_Post);                                                                                                            //Natural: MOVE BY NAME CWF-ORG-EMPL-TBL-POST TO WORK-RECORD
        work_Record_Pnd_C1.setValue(pnd_Contstants_Pnd_Delimiter);                                                                                                        //Natural: MOVE #DELIMITER TO #C1 #C2 #C3 #C4 #C5 #C6 #C7 #C8
        work_Record_Pnd_C2.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C3.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C4.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C5.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C6.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C7.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C8.setValue(pnd_Contstants_Pnd_Delimiter);
        //* *************
        //*  FORMAT-RECORD
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(1, false, work_Record);                                                                                                                      //Natural: WRITE WORK FILE 1 WORK-RECORD
        pnd_Counters_Pnd_Rex_Written.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REX-WRITTEN
        //*  PERFORM WRITE-REPORT
        //* ********************************
        //*  WRITE-WORK-FILE
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().display(1, "Unit/Cde",                                                                                                                               //Natural: DISPLAY ( 1 ) 'Unit/Cde' WORK-RECORD.EMPL-UNIT-CDE 'Emp/Name' WORK-RECORD.EMPL-NME ( AL = 30 ) '/Title' WORK-RECORD.EMPL-TITLE-NME 'RACF/ID' WORK-RECORD.EMPL-RACF-ID 'Authrty/Cde' WORK-RECORD.EMPL-TABLE-UPDTE-AUTHRTY-CDE 'Empl/Ext.' WORK-RECORD.EMPL-EXTNSN-NBR 'Empl/Signature' WORK-RECORD.EMPL-SIGNATORY ( AL = 20 ) 'Unit/Nme' WORK-RECORD.EMPL-UNIT-WORK-NME ( AL = 10 )
        		work_Record_Empl_Unit_Cde,"Emp/Name",
        		work_Record_Empl_Nme, new AlphanumericLength (30),"/Title",
        		work_Record_Empl_Title_Nme,"RACF/ID",
        		work_Record_Empl_Racf_Id,"Authrty/Cde",
        		work_Record_Empl_Table_Updte_Authrty_Cde,"Empl/Ext.",
        		work_Record_Empl_Extnsn_Nbr,"Empl/Signature",
        		work_Record_Empl_Signatory, new AlphanumericLength (20),"Unit/Nme",
        		work_Record_Empl_Unit_Work_Nme, new AlphanumericLength (10));
        if (Global.isEscape()) return;
        //* *****************************
        //* WRITE-REPORT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),pnd_Header1_2,new 
                        TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "Unit/Cde",
        		work_Record_Empl_Unit_Cde,"Emp/Name",
        		work_Record_Empl_Nme, new AlphanumericLength (30),"/Title",
        		work_Record_Empl_Title_Nme,"RACF/ID",
        		work_Record_Empl_Racf_Id,"Authrty/Cde",
        		work_Record_Empl_Table_Updte_Authrty_Cde,"Empl/Ext.",
        		work_Record_Empl_Extnsn_Nbr,"Empl/Signature",
        		work_Record_Empl_Signatory, new AlphanumericLength (20),"Unit/Nme",
        		work_Record_Empl_Unit_Work_Nme, new AlphanumericLength (10));
    }
}
