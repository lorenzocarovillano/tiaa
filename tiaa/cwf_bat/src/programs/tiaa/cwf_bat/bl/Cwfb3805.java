/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:30:36 PM
**        * FROM NATURAL PROGRAM : Cwfb3805
************************************************************
**        * FILE NAME            : Cwfb3805.java
**        * CLASS NAME           : Cwfb3805
**        * INSTANCE NAME        : Cwfb3805
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: MONTHLY TURNAROUND REPT
**SAG SYSTEM: CRF
**SAG REPORT-HEADING(1): CORPORATE WORK FLOW
**SAG PRINT-FILE(1): 1
**SAG REPORT-HEADING(2): WEEKLY BPS FORMS MAILING REPORT
**SAG PRINT-FILE(2): 1
**SAG REPORT-HEADING(3): CORPORATE WORK FLOW
**SAG PRINT-FILE(3): 2
**SAG REPORT-HEADING(4): WEEKLY BPS FORMS MAILING ERROR REPORT
**SAG PRINT-FILE(4): 2
**SAG HEADING-LINE: 12120000
**SAG DESCS(1): THIS PROGRAM READS THE MIT FILE FOR ACITVE RECORDS THAT
**SAG DESCS(2): HAVE BEEN PARTICIPANT CLOSED IN THE PAST WEEK FOR WPID/
**SAG DESCS(3): UNIT IN TBLE BPS-PROC-WPIDS/BPS-PROC-UNITS. CREATES
**SAG DESCS(4): REPORT SHOWING TURNAROUND FOR UNIT/WPID.
**SAG PRIMARY-FILE: CWF-MASTER-INDEX-VIEW
**SAG PRIMARY-KEY: PRTCPNT-CLOSED-KEY
************************************************************************
* PROGRAM  : CWFB3805
* SYSTEM   : CRF
* TITLE    : MONTHLY TURNAROUND REPT
* GENERATED: AUG 18,97 AT 02:45 PM
* FUNCTION : THIS PROGRAM READS THE MIT FILE FOR ACITVE RECORDS THAT
*            HAVE BEEN PARTICIPANT CLOSED IN THE PAST WEEK FOR WPID/
*            UNIT IN TBLE BPS-PROC-WPIDS/BPS-PROC-UNITS. CREATES
*            REPORT SHOWING TURNAROUND FOR UNIT/WPID.
* HISTORY
**SAG DEFINE EXIT CHANGE-HISTORY
* CHANGED ON FEB 29,96 BY HARGRAV FOR RELEASE ____
* CHANGED ON JAN 11,99 BY PAREDES TO EXPAND THE FOLLOWING FIELDS
*       FROM N5.2 TO N7.2 FORMAT:
*       #TOTAL-WPID-TAT, #AVG-WPID-TAT, #TOTAL-UNIT-TAT, #AVG-UNIT-TAT
* >
**SAG END-EXIT
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017 <STOW ONLY>
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3805 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private PdaCwfpda_M pdaCwfpda_M;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;
    private DbsField pnd_Header2_1;
    private DbsField pnd_Header2_2;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_1;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_2;
    private DbsField cwf_Master_Index_View_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Unit_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_3;
    private DbsField cwf_Master_Index_View_Unit_Id_Cde;
    private DbsField cwf_Master_Index_View_Unit_Rgn_Cde;
    private DbsField cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_View_Unit_Brnch_Group_Cde;
    private DbsField cwf_Master_Index_View_Step_Id;
    private DbsField cwf_Master_Index_View_Rt_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_View_Status_Cde;
    private DbsField cwf_Master_Index_View_Cntct_Orgn_Type_Cde;
    private DbsField cwf_Master_Index_View_Actve_Ind;

    private DbsGroup cwf_Master_Index_View__R_Field_4;
    private DbsField cwf_Master_Index_View_Fill_1;
    private DbsField cwf_Master_Index_View_Actve_Ind_2_2;
    private DbsField cwf_Master_Index_View_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Effctve_Dte;
    private DbsField cwf_Master_Index_View_Trnsctn_Dte;
    private DbsField cwf_Master_Index_View_Trans_Dte;
    private DbsField cwf_Master_Index_View_Mstr_Indx_Actn_Cde;
    private DbsField cwf_Master_Index_View_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_View_Final_Close_Out_Dte_Tme;
    private DbsField cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_View_Status_Freeze_Ind;
    private DbsField cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_5;
    private DbsField cwf_Master_Index_View_Crprte_Clock_End_Dte;
    private DbsField cwf_Master_Index_View_Owner_Unit_Cde;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_View_Trade_Dte_Tme;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_6;
    private DbsField cwf_Support_Tbl_Tbl_Wpid;

    private DbsGroup cwf_Support_Tbl__R_Field_7;
    private DbsField cwf_Support_Tbl_Tbl_Unit;

    private DbsGroup cwf_Support_Tbl__R_Field_8;
    private DbsField cwf_Support_Tbl_Date_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_9;
    private DbsField cwf_Support_Tbl_Pnd_Wpid;
    private DbsField cwf_Support_Tbl_Pnd_Wpid_Rest;

    private DbsGroup cwf_Support_Tbl__R_Field_10;
    private DbsField cwf_Support_Tbl_Pnd_Unit;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Tbl_Table_Access_Level;

    private DataAccessProgramView vw_cwf_Wp_Work_Prcss_Id;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Id;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp;

    private DbsGroup cwf_Wp_Work_Prcss_Id__R_Field_11;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Days_Nbr;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Hours_Nbr;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Mins_Nbr;
    private DbsField cwf_Wp_Work_Prcss_Id_Actve_Ind;

    private DbsGroup cwf_Wp_Work_Prcss_Id__R_Field_12;
    private DbsField cwf_Wp_Work_Prcss_Id_Fill_1;
    private DbsField cwf_Wp_Work_Prcss_Id_Actve_Ind_2_2;
    private DbsField cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde;

    private DataAccessProgramView vw_cwf_Org_Unit_Tbl;
    private DbsField cwf_Org_Unit_Tbl_Unit_Cde;

    private DbsGroup cwf_Org_Unit_Tbl__R_Field_13;
    private DbsField cwf_Org_Unit_Tbl_Unit_Id_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Rgn_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Brnch_Group_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Short_Nme;
    private DbsField cwf_Org_Unit_Tbl_Unit_Long_Nme;
    private DbsField cwf_Org_Unit_Tbl_Actve_Ind;
    private DbsField pnd_Prtcpnt_Closed_Key;

    private DbsGroup pnd_Prtcpnt_Closed_Key__R_Field_14;
    private DbsField pnd_Prtcpnt_Closed_Key_Pnd_Crprte_Clock_End_Dte;
    private DbsField pnd_Prtcpnt_Closed_Key_Pnd_Status_Freeze_Ind;
    private DbsField pnd_Wpid_Prime_Key;

    private DbsGroup pnd_Wpid_Prime_Key__R_Field_15;
    private DbsField pnd_Wpid_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Wpid_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Wpid_Prime_Key_Pnd_Tbl_Wpid;
    private DbsField pnd_Unit_Prime_Key;

    private DbsGroup pnd_Unit_Prime_Key__R_Field_16;
    private DbsField pnd_Unit_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Unit_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Unit_Prime_Key_Pnd_Tbl_Unit;
    private DbsField pnd_Date_Prime_Key;

    private DbsGroup pnd_Date_Prime_Key__R_Field_17;
    private DbsField pnd_Date_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Date_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Date_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Valid_Wpid_Tbl;
    private DbsField pnd_Valid_Unit_Tbl;

    private DbsGroup pnd_Valid_Unit_Tbl__R_Field_18;

    private DbsGroup pnd_Valid_Unit_Tbl_Pnd_Unit_Info;
    private DbsField pnd_Valid_Unit_Tbl_Pnd_Valid_Unit;
    private DbsField pnd_Valid_Unit_Tbl_Pnd_Valid_Tat;

    private DbsGroup pnd_Wpid_Totals;
    private DbsField pnd_Wpid_Totals_Pnd_Total_Wpid_Work;
    private DbsField pnd_Wpid_Totals_Pnd_Wpid_On_Time;
    private DbsField pnd_Wpid_Totals_Pnd_Wpid_Not_On;
    private DbsField pnd_Wpid_Totals_Pnd_Wpid_On_Time_Pct;
    private DbsField pnd_Wpid_Totals_Pnd_Wpid_Not_On_Pct;
    private DbsField pnd_Wpid_Totals_Pnd_Total_Wpid_Tat;
    private DbsField pnd_Wpid_Totals_Pnd_Avg_Wpid_Tat;

    private DbsGroup pnd_Unit_Totals;
    private DbsField pnd_Unit_Totals_Pnd_Total_Unit_Work;
    private DbsField pnd_Unit_Totals_Pnd_Unit_On_Time;
    private DbsField pnd_Unit_Totals_Pnd_Unit_Not_On;
    private DbsField pnd_Unit_Totals_Pnd_Unit_On_Time_Pct;
    private DbsField pnd_Unit_Totals_Pnd_Unit_Not_On_Pct;
    private DbsField pnd_Unit_Totals_Pnd_Total_Unit_Tat;
    private DbsField pnd_Unit_Totals_Pnd_Avg_Unit_Tat;

    private DbsGroup pnd_Gtotals;
    private DbsField pnd_Gtotals_Pnd_Gtotal_Work;
    private DbsField pnd_Gtotals_Pnd_Gtotal_On_Time;
    private DbsField pnd_Gtotals_Pnd_Gtotal_Not_On;
    private DbsField pnd_Gtotals_Pnd_Gtotal_On_Time_Pct;
    private DbsField pnd_Gtotals_Pnd_Gtotal_Not_On_Pct;

    private DbsGroup pnd_Report_Fields;
    private DbsField pnd_Report_Fields_Pnd_Rpt_Wpid;
    private DbsField pnd_Report_Fields_Pnd_Rpt_Unit;
    private DbsField pnd_Report_Fields_Work_Prcss_Long_Nme;
    private DbsField pnd_Report_Fields_Work_Prcss_Short_Nme;
    private DbsField pnd_Report_Fields_Unit_Nme_Short;
    private DbsField pnd_Report_Fields_Unit_Nme_Long;
    private DbsField pnd_Report_Fields_Pnd_Unit_Cde_Rpt;
    private DbsField pnd_Report_Fields_Pnd_Corp_Stnd;
    private DbsField pnd_Run_Date;
    private DbsField pnd_Convert_Date;
    private DbsField pnd_Start_Date_A;

    private DbsGroup pnd_Start_Date_A__R_Field_19;
    private DbsField pnd_Start_Date_A_Pnd_Start_Yyyy;
    private DbsField pnd_Start_Date_A_Pnd_Start_Mm;
    private DbsField pnd_Start_Date_A_Pnd_Start_Dd;
    private DbsField pnd_End_Date_A;

    private DbsGroup pnd_End_Date_A__R_Field_20;
    private DbsField pnd_End_Date_A_Pnd_End_Yyyy;
    private DbsField pnd_End_Date_A_Pnd_End_Mm;
    private DbsField pnd_End_Date_A_Pnd_End_Dd;
    private DbsField pnd_Disp_Unit;
    private DbsField pnd_Disp_Date;
    private DbsField pnd_Check_Clock_End_Dte_Tme;

    private DbsGroup pnd_Check_Clock_End_Dte_Tme__R_Field_21;
    private DbsField pnd_Check_Clock_End_Dte_Tme_Pnd_Check_Clock_End_Dte;
    private DbsField pnd_No_Records_Found;
    private DbsField pnd_Max_Valid_Wpids;
    private DbsField pnd_Max_Valid_Units;
    private DbsField pnd_Index;
    private DbsField pnd_X;
    private DbsField pnd_I;
    private DbsField pnd_Y;
    private DbsField pnd_Keep_It;
    private DbsField pnd_End_Of_Report;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Errors;
    private DbsField pnd_Line;
    private DbsField pnd_Line2;
    private DbsField pnd_Hold_Tat;
    private DbsField pnd_Prev_Unit;
    private DbsField pnd_Prev_Wpid;
    private DbsField pnd_Unit_Cde;
    private DbsField pnd_Base_Dte_Tme;
    private DbsField pnd_End_Dte_Tme;
    private DbsField pnd_Elapsed_Days;
    private DbsField pnd_Elapsed_Hrs;
    private DbsField pnd_Elapsed_Mins;
    private DbsField pnd_No_Wpid;
    private DbsField pnd_Elapsed_Hrs_Dec;
    private DbsField pnd_Tat;
    private DbsField pnd_Rex_Read;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Owner_Unit_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        localVariables = new DbsRecord();
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);

        // Local Variables
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);
        pnd_Header2_1 = localVariables.newFieldInRecord("pnd_Header2_1", "#HEADER2-1", FieldType.STRING, 50);
        pnd_Header2_2 = localVariables.newFieldInRecord("pnd_Header2_2", "#HEADER2-2", FieldType.STRING, 50);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Pin_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Master_Index_View_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index_View__R_Field_1 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_1", "REDEFINE", cwf_Master_Index_View_Rqst_Log_Dte_Tme);
        cwf_Master_Index_View_Rqst_Log_Index_Dte = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Log_Index_Tme = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");

        cwf_Master_Index_View__R_Field_2 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_2", "REDEFINE", cwf_Master_Index_View_Work_Prcss_Id);
        cwf_Master_Index_View_Work_Actn_Rqstd_Cde = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde", 
            "WORK-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde", 
            "WORK-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Cde", "UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_CDE");
        cwf_Master_Index_View_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Master_Index_View__R_Field_3 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_3", "REDEFINE", cwf_Master_Index_View_Unit_Cde);
        cwf_Master_Index_View_Unit_Id_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 
            5);
        cwf_Master_Index_View_Unit_Rgn_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Unit_Brnch_Group_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Step_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Id", "STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "STEP_ID");
        cwf_Master_Index_View_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Index_View_Rt_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        cwf_Master_Index_View_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_View_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_View_Cntct_Orgn_Type_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Orgn_Type_Cde", 
            "CNTCT-ORGN-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTCT_ORGN_TYPE_CDE");
        cwf_Master_Index_View_Cntct_Orgn_Type_Cde.setDdmHeader("CONTACT/CODE");
        cwf_Master_Index_View_Actve_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");

        cwf_Master_Index_View__R_Field_4 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_4", "REDEFINE", cwf_Master_Index_View_Actve_Ind);
        cwf_Master_Index_View_Fill_1 = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Master_Index_View_Actve_Ind_2_2 = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Actve_Ind_2_2", "ACTVE-IND-2-2", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Crprte_Status_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_View_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Effctve_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_View_Trnsctn_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trnsctn_Dte", "TRNSCTN-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRNSCTN_DTE");
        cwf_Master_Index_View_Trnsctn_Dte.setDdmHeader("TRANSACTION/DATE");
        cwf_Master_Index_View_Trans_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trans_Dte", "TRANS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        cwf_Master_Index_View_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_View_Mstr_Indx_Actn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mstr_Indx_Actn_Cde", "MSTR-INDX-ACTN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "MSTR_INDX_ACTN_CDE");
        cwf_Master_Index_View_Mstr_Indx_Actn_Cde.setDdmHeader("CHARGEOUT/ACTION CODE");
        cwf_Master_Index_View_Mj_Pull_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_View_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_View_Final_Close_Out_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Final_Close_Out_Dte_Tme", 
            "FINAL-CLOSE-OUT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_DTE_TME");
        cwf_Master_Index_View_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte", 
            "EXTRNL-PEND-RCV-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_View_Status_Freeze_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_View_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme", 
            "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");

        cwf_Master_Index_View__R_Field_5 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_5", "REDEFINE", cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme);
        cwf_Master_Index_View_Crprte_Clock_End_Dte = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Crprte_Clock_End_Dte", "CRPRTE-CLOCK-END-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Owner_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Owner_Unit_Cde", "OWNER-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "OWNER_UNIT_CDE");
        cwf_Master_Index_View_Owner_Unit_Cde.setDdmHeader("OWNERUNIT");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        cwf_Master_Index_View_Trade_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trade_Dte_Tme", "TRADE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TRADE_DTE_TME");
        cwf_Master_Index_View_Trade_Dte_Tme.setDdmHeader("TRADE/DTE-TME");
        registerRecord(vw_cwf_Master_Index_View);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_6 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_6", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_Wpid = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        cwf_Support_Tbl__R_Field_7 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_7", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_Unit = cwf_Support_Tbl__R_Field_7.newFieldInGroup("cwf_Support_Tbl_Tbl_Unit", "TBL-UNIT", FieldType.STRING, 8);

        cwf_Support_Tbl__R_Field_8 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_8", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Date_Ind = cwf_Support_Tbl__R_Field_8.newFieldInGroup("cwf_Support_Tbl_Date_Ind", "DATE-IND", FieldType.STRING, 1);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_9 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_9", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Wpid = cwf_Support_Tbl__R_Field_9.newFieldArrayInGroup("cwf_Support_Tbl_Pnd_Wpid", "#WPID", FieldType.STRING, 6, new DbsArrayController(1, 
            42));
        cwf_Support_Tbl_Pnd_Wpid_Rest = cwf_Support_Tbl__R_Field_9.newFieldInGroup("cwf_Support_Tbl_Pnd_Wpid_Rest", "#WPID-REST", FieldType.STRING, 1);

        cwf_Support_Tbl__R_Field_10 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_10", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Unit = cwf_Support_Tbl__R_Field_10.newFieldArrayInGroup("cwf_Support_Tbl_Pnd_Unit", "#UNIT", FieldType.STRING, 9, new DbsArrayController(1, 
            20));
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");
        registerRecord(vw_cwf_Support_Tbl);

        vw_cwf_Wp_Work_Prcss_Id = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Work_Prcss_Id", "CWF-WP-WORK-PRCSS-ID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Id = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", 
            FieldType.STRING, 45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp", 
            "CRPRTE-SRVCE-TIME-STNDRD-GRP", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "CRPRTE_SRVCE_TIME_STNDRD_GRP");
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp.setDdmHeader("CORPORATE SERVICE/TIME STANDARD");

        cwf_Wp_Work_Prcss_Id__R_Field_11 = vw_cwf_Wp_Work_Prcss_Id.getRecord().newGroupInGroup("cwf_Wp_Work_Prcss_Id__R_Field_11", "REDEFINE", cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp);
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Days_Nbr = cwf_Wp_Work_Prcss_Id__R_Field_11.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Days_Nbr", 
            "CRPRTE-SRVCE-STNDRD-DAYS-NBR", FieldType.NUMERIC, 3);
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Hours_Nbr = cwf_Wp_Work_Prcss_Id__R_Field_11.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Hours_Nbr", 
            "CRPRTE-SRVCE-STNDRD-HOURS-NBR", FieldType.NUMERIC, 2);
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Mins_Nbr = cwf_Wp_Work_Prcss_Id__R_Field_11.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Mins_Nbr", 
            "CRPRTE-SRVCE-STNDRD-MINS-NBR", FieldType.NUMERIC, 2);
        cwf_Wp_Work_Prcss_Id_Actve_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");

        cwf_Wp_Work_Prcss_Id__R_Field_12 = vw_cwf_Wp_Work_Prcss_Id.getRecord().newGroupInGroup("cwf_Wp_Work_Prcss_Id__R_Field_12", "REDEFINE", cwf_Wp_Work_Prcss_Id_Actve_Ind);
        cwf_Wp_Work_Prcss_Id_Fill_1 = cwf_Wp_Work_Prcss_Id__R_Field_12.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Wp_Work_Prcss_Id_Actve_Ind_2_2 = cwf_Wp_Work_Prcss_Id__R_Field_12.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Actve_Ind_2_2", "ACTVE-IND-2-2", FieldType.STRING, 
            1);
        cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        registerRecord(vw_cwf_Wp_Work_Prcss_Id);

        vw_cwf_Org_Unit_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Unit_Tbl", "CWF-ORG-UNIT-TBL"), "CWF_ORG_UNIT_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Unit_Tbl_Unit_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Org_Unit_Tbl_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Org_Unit_Tbl__R_Field_13 = vw_cwf_Org_Unit_Tbl.getRecord().newGroupInGroup("cwf_Org_Unit_Tbl__R_Field_13", "REDEFINE", cwf_Org_Unit_Tbl_Unit_Cde);
        cwf_Org_Unit_Tbl_Unit_Id_Cde = cwf_Org_Unit_Tbl__R_Field_13.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        cwf_Org_Unit_Tbl_Unit_Rgn_Cde = cwf_Org_Unit_Tbl__R_Field_13.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 
            1);
        cwf_Org_Unit_Tbl_Unit_Spcl_Dsgntn_Cde = cwf_Org_Unit_Tbl__R_Field_13.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Org_Unit_Tbl_Unit_Brnch_Group_Cde = cwf_Org_Unit_Tbl__R_Field_13.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Org_Unit_Tbl_Unit_Short_Nme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Short_Nme", "UNIT-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_SHORT_NME");
        cwf_Org_Unit_Tbl_Unit_Short_Nme.setDdmHeader("UNIT SHORT NAME");
        cwf_Org_Unit_Tbl_Unit_Long_Nme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Long_Nme", "UNIT-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "UNIT_LONG_NME");
        cwf_Org_Unit_Tbl_Unit_Long_Nme.setDdmHeader("UNIT LONG NAME");
        cwf_Org_Unit_Tbl_Actve_Ind = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        registerRecord(vw_cwf_Org_Unit_Tbl);

        pnd_Prtcpnt_Closed_Key = localVariables.newFieldInRecord("pnd_Prtcpnt_Closed_Key", "#PRTCPNT-CLOSED-KEY", FieldType.STRING, 10);

        pnd_Prtcpnt_Closed_Key__R_Field_14 = localVariables.newGroupInRecord("pnd_Prtcpnt_Closed_Key__R_Field_14", "REDEFINE", pnd_Prtcpnt_Closed_Key);
        pnd_Prtcpnt_Closed_Key_Pnd_Crprte_Clock_End_Dte = pnd_Prtcpnt_Closed_Key__R_Field_14.newFieldInGroup("pnd_Prtcpnt_Closed_Key_Pnd_Crprte_Clock_End_Dte", 
            "#CRPRTE-CLOCK-END-DTE", FieldType.STRING, 8);
        pnd_Prtcpnt_Closed_Key_Pnd_Status_Freeze_Ind = pnd_Prtcpnt_Closed_Key__R_Field_14.newFieldInGroup("pnd_Prtcpnt_Closed_Key_Pnd_Status_Freeze_Ind", 
            "#STATUS-FREEZE-IND", FieldType.STRING, 1);
        pnd_Wpid_Prime_Key = localVariables.newFieldInRecord("pnd_Wpid_Prime_Key", "#WPID-PRIME-KEY", FieldType.STRING, 28);

        pnd_Wpid_Prime_Key__R_Field_15 = localVariables.newGroupInRecord("pnd_Wpid_Prime_Key__R_Field_15", "REDEFINE", pnd_Wpid_Prime_Key);
        pnd_Wpid_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Wpid_Prime_Key__R_Field_15.newFieldInGroup("pnd_Wpid_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Wpid_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Wpid_Prime_Key__R_Field_15.newFieldInGroup("pnd_Wpid_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Wpid_Prime_Key_Pnd_Tbl_Wpid = pnd_Wpid_Prime_Key__R_Field_15.newFieldInGroup("pnd_Wpid_Prime_Key_Pnd_Tbl_Wpid", "#TBL-WPID", FieldType.STRING, 
            6);
        pnd_Unit_Prime_Key = localVariables.newFieldInRecord("pnd_Unit_Prime_Key", "#UNIT-PRIME-KEY", FieldType.STRING, 28);

        pnd_Unit_Prime_Key__R_Field_16 = localVariables.newGroupInRecord("pnd_Unit_Prime_Key__R_Field_16", "REDEFINE", pnd_Unit_Prime_Key);
        pnd_Unit_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Unit_Prime_Key__R_Field_16.newFieldInGroup("pnd_Unit_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Unit_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Unit_Prime_Key__R_Field_16.newFieldInGroup("pnd_Unit_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Unit_Prime_Key_Pnd_Tbl_Unit = pnd_Unit_Prime_Key__R_Field_16.newFieldInGroup("pnd_Unit_Prime_Key_Pnd_Tbl_Unit", "#TBL-UNIT", FieldType.STRING, 
            6);
        pnd_Date_Prime_Key = localVariables.newFieldInRecord("pnd_Date_Prime_Key", "#DATE-PRIME-KEY", FieldType.STRING, 28);

        pnd_Date_Prime_Key__R_Field_17 = localVariables.newGroupInRecord("pnd_Date_Prime_Key__R_Field_17", "REDEFINE", pnd_Date_Prime_Key);
        pnd_Date_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Date_Prime_Key__R_Field_17.newFieldInGroup("pnd_Date_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Date_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Date_Prime_Key__R_Field_17.newFieldInGroup("pnd_Date_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Date_Prime_Key_Pnd_Tbl_Key_Field = pnd_Date_Prime_Key__R_Field_17.newFieldInGroup("pnd_Date_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 1);
        pnd_Valid_Wpid_Tbl = localVariables.newFieldArrayInRecord("pnd_Valid_Wpid_Tbl", "#VALID-WPID-TBL", FieldType.STRING, 6, new DbsArrayController(1, 
            100));
        pnd_Valid_Unit_Tbl = localVariables.newFieldArrayInRecord("pnd_Valid_Unit_Tbl", "#VALID-UNIT-TBL", FieldType.STRING, 9, new DbsArrayController(1, 
            50));

        pnd_Valid_Unit_Tbl__R_Field_18 = localVariables.newGroupInRecord("pnd_Valid_Unit_Tbl__R_Field_18", "REDEFINE", pnd_Valid_Unit_Tbl);

        pnd_Valid_Unit_Tbl_Pnd_Unit_Info = pnd_Valid_Unit_Tbl__R_Field_18.newGroupArrayInGroup("pnd_Valid_Unit_Tbl_Pnd_Unit_Info", "#UNIT-INFO", new DbsArrayController(1, 
            50));
        pnd_Valid_Unit_Tbl_Pnd_Valid_Unit = pnd_Valid_Unit_Tbl_Pnd_Unit_Info.newFieldInGroup("pnd_Valid_Unit_Tbl_Pnd_Valid_Unit", "#VALID-UNIT", FieldType.STRING, 
            8);
        pnd_Valid_Unit_Tbl_Pnd_Valid_Tat = pnd_Valid_Unit_Tbl_Pnd_Unit_Info.newFieldInGroup("pnd_Valid_Unit_Tbl_Pnd_Valid_Tat", "#VALID-TAT", FieldType.NUMERIC, 
            1);

        pnd_Wpid_Totals = localVariables.newGroupInRecord("pnd_Wpid_Totals", "#WPID-TOTALS");
        pnd_Wpid_Totals_Pnd_Total_Wpid_Work = pnd_Wpid_Totals.newFieldInGroup("pnd_Wpid_Totals_Pnd_Total_Wpid_Work", "#TOTAL-WPID-WORK", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Wpid_Totals_Pnd_Wpid_On_Time = pnd_Wpid_Totals.newFieldInGroup("pnd_Wpid_Totals_Pnd_Wpid_On_Time", "#WPID-ON-TIME", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Wpid_Totals_Pnd_Wpid_Not_On = pnd_Wpid_Totals.newFieldInGroup("pnd_Wpid_Totals_Pnd_Wpid_Not_On", "#WPID-NOT-ON", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Wpid_Totals_Pnd_Wpid_On_Time_Pct = pnd_Wpid_Totals.newFieldInGroup("pnd_Wpid_Totals_Pnd_Wpid_On_Time_Pct", "#WPID-ON-TIME-PCT", FieldType.NUMERIC, 
            5, 2);
        pnd_Wpid_Totals_Pnd_Wpid_Not_On_Pct = pnd_Wpid_Totals.newFieldInGroup("pnd_Wpid_Totals_Pnd_Wpid_Not_On_Pct", "#WPID-NOT-ON-PCT", FieldType.NUMERIC, 
            5, 2);
        pnd_Wpid_Totals_Pnd_Total_Wpid_Tat = pnd_Wpid_Totals.newFieldInGroup("pnd_Wpid_Totals_Pnd_Total_Wpid_Tat", "#TOTAL-WPID-TAT", FieldType.NUMERIC, 
            9, 2);
        pnd_Wpid_Totals_Pnd_Avg_Wpid_Tat = pnd_Wpid_Totals.newFieldInGroup("pnd_Wpid_Totals_Pnd_Avg_Wpid_Tat", "#AVG-WPID-TAT", FieldType.NUMERIC, 9, 
            2);

        pnd_Unit_Totals = localVariables.newGroupInRecord("pnd_Unit_Totals", "#UNIT-TOTALS");
        pnd_Unit_Totals_Pnd_Total_Unit_Work = pnd_Unit_Totals.newFieldInGroup("pnd_Unit_Totals_Pnd_Total_Unit_Work", "#TOTAL-UNIT-WORK", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Unit_Totals_Pnd_Unit_On_Time = pnd_Unit_Totals.newFieldInGroup("pnd_Unit_Totals_Pnd_Unit_On_Time", "#UNIT-ON-TIME", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Unit_Totals_Pnd_Unit_Not_On = pnd_Unit_Totals.newFieldInGroup("pnd_Unit_Totals_Pnd_Unit_Not_On", "#UNIT-NOT-ON", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Unit_Totals_Pnd_Unit_On_Time_Pct = pnd_Unit_Totals.newFieldInGroup("pnd_Unit_Totals_Pnd_Unit_On_Time_Pct", "#UNIT-ON-TIME-PCT", FieldType.NUMERIC, 
            5, 2);
        pnd_Unit_Totals_Pnd_Unit_Not_On_Pct = pnd_Unit_Totals.newFieldInGroup("pnd_Unit_Totals_Pnd_Unit_Not_On_Pct", "#UNIT-NOT-ON-PCT", FieldType.NUMERIC, 
            5, 2);
        pnd_Unit_Totals_Pnd_Total_Unit_Tat = pnd_Unit_Totals.newFieldInGroup("pnd_Unit_Totals_Pnd_Total_Unit_Tat", "#TOTAL-UNIT-TAT", FieldType.NUMERIC, 
            9, 2);
        pnd_Unit_Totals_Pnd_Avg_Unit_Tat = pnd_Unit_Totals.newFieldInGroup("pnd_Unit_Totals_Pnd_Avg_Unit_Tat", "#AVG-UNIT-TAT", FieldType.NUMERIC, 9, 
            2);

        pnd_Gtotals = localVariables.newGroupInRecord("pnd_Gtotals", "#GTOTALS");
        pnd_Gtotals_Pnd_Gtotal_Work = pnd_Gtotals.newFieldInGroup("pnd_Gtotals_Pnd_Gtotal_Work", "#GTOTAL-WORK", FieldType.PACKED_DECIMAL, 9);
        pnd_Gtotals_Pnd_Gtotal_On_Time = pnd_Gtotals.newFieldInGroup("pnd_Gtotals_Pnd_Gtotal_On_Time", "#GTOTAL-ON-TIME", FieldType.PACKED_DECIMAL, 9);
        pnd_Gtotals_Pnd_Gtotal_Not_On = pnd_Gtotals.newFieldInGroup("pnd_Gtotals_Pnd_Gtotal_Not_On", "#GTOTAL-NOT-ON", FieldType.PACKED_DECIMAL, 9);
        pnd_Gtotals_Pnd_Gtotal_On_Time_Pct = pnd_Gtotals.newFieldInGroup("pnd_Gtotals_Pnd_Gtotal_On_Time_Pct", "#GTOTAL-ON-TIME-PCT", FieldType.NUMERIC, 
            5, 2);
        pnd_Gtotals_Pnd_Gtotal_Not_On_Pct = pnd_Gtotals.newFieldInGroup("pnd_Gtotals_Pnd_Gtotal_Not_On_Pct", "#GTOTAL-NOT-ON-PCT", FieldType.NUMERIC, 
            5, 2);

        pnd_Report_Fields = localVariables.newGroupInRecord("pnd_Report_Fields", "#REPORT-FIELDS");
        pnd_Report_Fields_Pnd_Rpt_Wpid = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Rpt_Wpid", "#RPT-WPID", FieldType.STRING, 6);
        pnd_Report_Fields_Pnd_Rpt_Unit = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Rpt_Unit", "#RPT-UNIT", FieldType.STRING, 8);
        pnd_Report_Fields_Work_Prcss_Long_Nme = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 
            45);
        pnd_Report_Fields_Work_Prcss_Short_Nme = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15);
        pnd_Report_Fields_Unit_Nme_Short = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Unit_Nme_Short", "UNIT-NME-SHORT", FieldType.STRING, 15);
        pnd_Report_Fields_Unit_Nme_Long = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Unit_Nme_Long", "UNIT-NME-LONG", FieldType.STRING, 45);
        pnd_Report_Fields_Pnd_Unit_Cde_Rpt = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Unit_Cde_Rpt", "#UNIT-CDE-RPT", FieldType.STRING, 
            10);
        pnd_Report_Fields_Pnd_Corp_Stnd = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Corp_Stnd", "#CORP-STND", FieldType.NUMERIC, 1);
        pnd_Run_Date = localVariables.newFieldInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.STRING, 8);
        pnd_Convert_Date = localVariables.newFieldInRecord("pnd_Convert_Date", "#CONVERT-DATE", FieldType.DATE);
        pnd_Start_Date_A = localVariables.newFieldInRecord("pnd_Start_Date_A", "#START-DATE-A", FieldType.STRING, 8);

        pnd_Start_Date_A__R_Field_19 = localVariables.newGroupInRecord("pnd_Start_Date_A__R_Field_19", "REDEFINE", pnd_Start_Date_A);
        pnd_Start_Date_A_Pnd_Start_Yyyy = pnd_Start_Date_A__R_Field_19.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Yyyy", "#START-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Start_Date_A_Pnd_Start_Mm = pnd_Start_Date_A__R_Field_19.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Mm", "#START-MM", FieldType.NUMERIC, 
            2);
        pnd_Start_Date_A_Pnd_Start_Dd = pnd_Start_Date_A__R_Field_19.newFieldInGroup("pnd_Start_Date_A_Pnd_Start_Dd", "#START-DD", FieldType.NUMERIC, 
            2);
        pnd_End_Date_A = localVariables.newFieldInRecord("pnd_End_Date_A", "#END-DATE-A", FieldType.STRING, 8);

        pnd_End_Date_A__R_Field_20 = localVariables.newGroupInRecord("pnd_End_Date_A__R_Field_20", "REDEFINE", pnd_End_Date_A);
        pnd_End_Date_A_Pnd_End_Yyyy = pnd_End_Date_A__R_Field_20.newFieldInGroup("pnd_End_Date_A_Pnd_End_Yyyy", "#END-YYYY", FieldType.NUMERIC, 4);
        pnd_End_Date_A_Pnd_End_Mm = pnd_End_Date_A__R_Field_20.newFieldInGroup("pnd_End_Date_A_Pnd_End_Mm", "#END-MM", FieldType.NUMERIC, 2);
        pnd_End_Date_A_Pnd_End_Dd = pnd_End_Date_A__R_Field_20.newFieldInGroup("pnd_End_Date_A_Pnd_End_Dd", "#END-DD", FieldType.NUMERIC, 2);
        pnd_Disp_Unit = localVariables.newFieldInRecord("pnd_Disp_Unit", "#DISP-UNIT", FieldType.STRING, 9);
        pnd_Disp_Date = localVariables.newFieldInRecord("pnd_Disp_Date", "#DISP-DATE", FieldType.STRING, 8);
        pnd_Check_Clock_End_Dte_Tme = localVariables.newFieldInRecord("pnd_Check_Clock_End_Dte_Tme", "#CHECK-CLOCK-END-DTE-TME", FieldType.STRING, 15);

        pnd_Check_Clock_End_Dte_Tme__R_Field_21 = localVariables.newGroupInRecord("pnd_Check_Clock_End_Dte_Tme__R_Field_21", "REDEFINE", pnd_Check_Clock_End_Dte_Tme);
        pnd_Check_Clock_End_Dte_Tme_Pnd_Check_Clock_End_Dte = pnd_Check_Clock_End_Dte_Tme__R_Field_21.newFieldInGroup("pnd_Check_Clock_End_Dte_Tme_Pnd_Check_Clock_End_Dte", 
            "#CHECK-CLOCK-END-DTE", FieldType.STRING, 8);
        pnd_No_Records_Found = localVariables.newFieldInRecord("pnd_No_Records_Found", "#NO-RECORDS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Max_Valid_Wpids = localVariables.newFieldInRecord("pnd_Max_Valid_Wpids", "#MAX-VALID-WPIDS", FieldType.NUMERIC, 3);
        pnd_Max_Valid_Units = localVariables.newFieldInRecord("pnd_Max_Valid_Units", "#MAX-VALID-UNITS", FieldType.NUMERIC, 2);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.INTEGER, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.STRING, 1);
        pnd_Keep_It = localVariables.newFieldInRecord("pnd_Keep_It", "#KEEP-IT", FieldType.BOOLEAN, 1);
        pnd_End_Of_Report = localVariables.newFieldInRecord("pnd_End_Of_Report", "#END-OF-REPORT", FieldType.BOOLEAN, 1);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 132);
        pnd_Errors = localVariables.newFieldInRecord("pnd_Errors", "#ERRORS", FieldType.PACKED_DECIMAL, 5);
        pnd_Line = localVariables.newFieldInRecord("pnd_Line", "#LINE", FieldType.STRING, 1);
        pnd_Line2 = localVariables.newFieldInRecord("pnd_Line2", "#LINE2", FieldType.STRING, 1);
        pnd_Hold_Tat = localVariables.newFieldInRecord("pnd_Hold_Tat", "#HOLD-TAT", FieldType.NUMERIC, 1);
        pnd_Prev_Unit = localVariables.newFieldInRecord("pnd_Prev_Unit", "#PREV-UNIT", FieldType.STRING, 8);
        pnd_Prev_Wpid = localVariables.newFieldInRecord("pnd_Prev_Wpid", "#PREV-WPID", FieldType.STRING, 6);
        pnd_Unit_Cde = localVariables.newFieldInRecord("pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 8);
        pnd_Base_Dte_Tme = localVariables.newFieldInRecord("pnd_Base_Dte_Tme", "#BASE-DTE-TME", FieldType.TIME);
        pnd_End_Dte_Tme = localVariables.newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.TIME);
        pnd_Elapsed_Days = localVariables.newFieldInRecord("pnd_Elapsed_Days", "#ELAPSED-DAYS", FieldType.NUMERIC, 3);
        pnd_Elapsed_Hrs = localVariables.newFieldInRecord("pnd_Elapsed_Hrs", "#ELAPSED-HRS", FieldType.NUMERIC, 2);
        pnd_Elapsed_Mins = localVariables.newFieldInRecord("pnd_Elapsed_Mins", "#ELAPSED-MINS", FieldType.NUMERIC, 2);
        pnd_No_Wpid = localVariables.newFieldInRecord("pnd_No_Wpid", "#NO-WPID", FieldType.PACKED_DECIMAL, 7);
        pnd_Elapsed_Hrs_Dec = localVariables.newFieldInRecord("pnd_Elapsed_Hrs_Dec", "#ELAPSED-HRS-DEC", FieldType.NUMERIC, 3, 2);
        pnd_Tat = localVariables.newFieldInRecord("pnd_Tat", "#TAT", FieldType.NUMERIC, 5, 2);
        pnd_Rex_Read = localVariables.newFieldInRecord("pnd_Rex_Read", "#REX-READ", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Owner_Unit_CdeOld = internalLoopRecord.newFieldInRecord("Sort01_Owner_Unit_Cde_OLD", "Owner_Unit_Cde_OLD", FieldType.STRING, 8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index_View.reset();
        vw_cwf_Support_Tbl.reset();
        vw_cwf_Wp_Work_Prcss_Id.reset();
        vw_cwf_Org_Unit_Tbl.reset();
        internalLoopRecord.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("               Corporate Work Flow");
        pnd_Header1_2.setInitialValue("         Weekly BPS Forms Mailing Report");
        pnd_Header2_1.setInitialValue("               Corporate Work Flow");
        pnd_Header2_2.setInitialValue("      Weekly BPS Forms Mailing Error Report");
        pnd_Wpid_Prime_Key.setInitialValue("A BPS-PROC-WPIDS");
        pnd_Unit_Prime_Key.setInitialValue("A BPS-PROC-UNITS");
        pnd_Date_Prime_Key.setInitialValue("A CWF-RPRT-RUN-TBL    W");
        pnd_Max_Valid_Wpids.setInitialValue(100);
        pnd_Max_Valid_Units.setInitialValue(20);
        pnd_Y.setInitialValue("Y");
        pnd_Keep_It.setInitialValue(false);
        pnd_End_Of_Report.setInitialValue(false);
        pnd_Line.setInitialValue("�");
        pnd_Line2.setInitialValue("�");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3805() throws Exception
    {
        super("Cwfb3805");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: DEFINE PRINTER ( 1 )
        getReports().definePrinter(3, "NOT DEFINED");                                                                                                                     //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: DEFINE PRINTER ( 2 )
        getReports().definePrinter(4, "NOT DEFINED");                                                                                                                     //Natural: FORMAT ( 2 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: DEFINE PRINTER ( 3 )
        //*                                                                                                                                                               //Natural: FORMAT ( 3 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
                                                                                                                                                                          //Natural: PERFORM GET-DATE-RANGE
        sub_Get_Date_Range();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "Start Date is:",pnd_Start_Date_A,NEWLINE,"End Date is:  ",pnd_End_Date_A);                                                                 //Natural: WRITE 'Start Date is:' #START-DATE-A / 'End Date is:  ' #END-DATE-A
        if (Global.isEscape()) return;
        //*  GO GET VALID WPIDS
                                                                                                                                                                          //Natural: PERFORM LOAD-WPID-TABLE
        sub_Load_Wpid_Table();
        if (condition(Global.isEscape())) {return;}
        //*  VALUE = 'Y'
                                                                                                                                                                          //Natural: PERFORM LOAD-UNIT-TABLE
        sub_Load_Unit_Table();
        if (condition(Global.isEscape())) {return;}
        pnd_Prtcpnt_Closed_Key_Pnd_Crprte_Clock_End_Dte.setValue(pnd_Start_Date_A);                                                                                       //Natural: ASSIGN #PRTCPNT-CLOSED-KEY.#CRPRTE-CLOCK-END-DTE := #START-DATE-A
        pnd_Prtcpnt_Closed_Key_Pnd_Status_Freeze_Ind.setValue(pnd_Y);                                                                                                     //Natural: ASSIGN #PRTCPNT-CLOSED-KEY.#STATUS-FREEZE-IND := #Y
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                        //Natural: READ CWF-MASTER-INDEX-VIEW BY PRTCPNT-CLOSED-KEY STARTING FROM #PRTCPNT-CLOSED-KEY
        (
        "READ_PRIME",
        new Wc[] { new Wc("PRTCPNT_CLOSED_KEY", ">=", pnd_Prtcpnt_Closed_Key, WcType.BY) },
        new Oc[] { new Oc("PRTCPNT_CLOSED_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Master_Index_View.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            if (condition(cwf_Master_Index_View_Crprte_Clock_End_Dte.greater(pnd_End_Date_A)))                                                                            //Natural: IF CWF-MASTER-INDEX-VIEW.CRPRTE-CLOCK-END-DTE GT #END-DATE-A
            {
                //*  PARTICIPANT CLOSED RECORDS?
                if (true) break READ_PRIME;                                                                                                                               //Natural: ESCAPE BOTTOM ( READ-PRIME. )
                //*  IF SO - GET OUT WE're done
            }                                                                                                                                                             //Natural: END-IF
            //*  CHECK AGAINST THE TABLE OF WPIDS WE're interested in
            pnd_Keep_It.resetInitial();                                                                                                                                   //Natural: RESET INITIAL #KEEP-IT
            short decideConditionsMet433 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CWF-MASTER-INDEX-VIEW.STATUS-FREEZE-IND EQ #Y
            if (condition(cwf_Master_Index_View_Status_Freeze_Ind.equals(pnd_Y)))
            {
                decideConditionsMet433++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID EQ #VALID-WPID-TBL ( * )
            if (condition(cwf_Master_Index_View_Work_Prcss_Id.equals(pnd_Valid_Wpid_Tbl.getValue("*"))))
            {
                decideConditionsMet433++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN CWF-MASTER-INDEX-VIEW.OWNER-UNIT-CDE EQ #VALID-UNIT ( * )
            if (condition(cwf_Master_Index_View_Owner_Unit_Cde.equals(pnd_Valid_Unit_Tbl_Pnd_Valid_Unit.getValue("*"))))
            {
                decideConditionsMet433++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN ALL
            if (condition(decideConditionsMet433 == 3))
            {
                pnd_Keep_It.setValue(true);                                                                                                                               //Natural: ASSIGN #KEEP-IT := TRUE
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet433 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(!(pnd_Keep_It.getBoolean())))                                                                                                                   //Natural: ACCEPT IF #KEEP-IT
            {
                continue;
            }
            if (condition(cwf_Master_Index_View_Owner_Unit_Cde.notEquals(" ")))                                                                                           //Natural: IF CWF-MASTER-INDEX-VIEW.OWNER-UNIT-CDE NE ' '
            {
                DbsUtil.examine(new ExamineSource(pnd_Valid_Unit_Tbl_Pnd_Valid_Unit.getValue("*"),true), new ExamineSearch(cwf_Master_Index_View_Owner_Unit_Cde),         //Natural: EXAMINE FULL #VALID-UNIT ( * ) FOR CWF-MASTER-INDEX-VIEW.OWNER-UNIT-CDE GIVING INDEX IN #INDEX
                    new ExamineGivingIndex(pnd_Index));
                if (condition(pnd_Index.greater(getZero())))                                                                                                              //Natural: IF #INDEX GT 0
                {
                    pnd_Hold_Tat.setValue(pnd_Valid_Unit_Tbl_Pnd_Valid_Tat.getValue(pnd_Index));                                                                          //Natural: MOVE #VALID-TAT ( #INDEX ) TO #HOLD-TAT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hold_Tat.reset();                                                                                                                                     //Natural: RESET #HOLD-TAT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rex_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REX-READ
            //*       GO FIGURE OUT TURNAROUND TIME
            short decideConditionsMet460 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-MASTER-INDEX-VIEW.EXTRNL-PEND-RCV-DTE GT 0
            if (condition(cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte.greater(getZero())))
            {
                decideConditionsMet460++;
                pnd_Base_Dte_Tme.setValue(cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte);                                                                                     //Natural: ASSIGN #BASE-DTE-TME := CWF-MASTER-INDEX-VIEW.EXTRNL-PEND-RCV-DTE
            }                                                                                                                                                             //Natural: WHEN CWF-MASTER-INDEX-VIEW.TIAA-RCVD-DTE-TME GT 0
            else if (condition(cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme.greater(getZero())))
            {
                decideConditionsMet460++;
                pnd_Base_Dte_Tme.setValue(cwf_Master_Index_View_Trade_Dte_Tme);                                                                                           //Natural: ASSIGN #BASE-DTE-TME := CWF-MASTER-INDEX-VIEW.TRADE-DTE-TME
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet460 > 0))
            {
                pnd_End_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme);                                     //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.CRPRTE-CLOCK-END-DTE-TME TO #END-DTE-TME ( EM = YYYYMMDDHHIISST )
                DbsUtil.callnat(Cwfn9370.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pnd_Unit_Cde, pnd_Base_Dte_Tme, pnd_End_Dte_Tme,                //Natural: CALLNAT 'CWFN9370' MSG-INFO-SUB #UNIT-CDE #BASE-DTE-TME #END-DTE-TME #ELAPSED-DAYS #ELAPSED-HRS #ELAPSED-MINS
                    pnd_Elapsed_Days, pnd_Elapsed_Hrs, pnd_Elapsed_Mins);
                if (condition(Global.isEscape())) return;
                if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                          //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
                {
                    pnd_Error_Msg.setValue(DbsUtil.compress("Error getting turnaround time: Msg is:", pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg()));                        //Natural: COMPRESS 'Error getting turnaround time: Msg is:' MSG-INFO-SUB.##MSG INTO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR
                    sub_Write_Error();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tat.setValue(pnd_Elapsed_Days);                                                                                                                   //Natural: ASSIGN #TAT := #ELAPSED-DAYS
                    if (condition(pnd_Elapsed_Hrs.greater(getZero())))                                                                                                    //Natural: IF #ELAPSED-HRS GT 0
                    {
                        pnd_Elapsed_Hrs_Dec.compute(new ComputeParameters(false, pnd_Elapsed_Hrs_Dec), pnd_Elapsed_Hrs.divide(7));                                        //Natural: ASSIGN #ELAPSED-HRS-DEC := #ELAPSED-HRS / 7
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Tat.nadd(pnd_Elapsed_Hrs_Dec);                                                                                                                    //Natural: ASSIGN #TAT := #TAT + #ELAPSED-HRS-DEC
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Error_Msg.setValue(DbsUtil.compress("No External Pend date or Trade Date for:", "Pin:", cwf_Master_Index_View_Pin_Nbr, "WPID", cwf_Master_Index_View_Work_Prcss_Id)); //Natural: COMPRESS 'No External Pend date or Trade Date for:' 'Pin:' CWF-MASTER-INDEX-VIEW.PIN-NBR 'WPID' CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID INTO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR
                sub_Write_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            getSort().writeSortInData(cwf_Master_Index_View_Owner_Unit_Cde, cwf_Master_Index_View_Work_Prcss_Id, cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte,               //Natural: END-ALL
                cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme, cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme, pnd_Tat, pnd_Hold_Tat, cwf_Master_Index_View_Pin_Nbr, 
                cwf_Master_Index_View_Rqst_Log_Dte_Tme, cwf_Master_Index_View_Actve_Ind, cwf_Master_Index_View_Trade_Dte_Tme);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT SORT-FIELDS
        getSort().sortData(cwf_Master_Index_View_Owner_Unit_Cde, cwf_Master_Index_View_Work_Prcss_Id);                                                                    //Natural: SORT BY CWF-MASTER-INDEX-VIEW.OWNER-UNIT-CDE CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID USING CWF-MASTER-INDEX-VIEW.EXTRNL-PEND-RCV-DTE CWF-MASTER-INDEX-VIEW.CRPRTE-CLOCK-END-DTE-TME CWF-MASTER-INDEX-VIEW.TIAA-RCVD-DTE-TME #TAT #HOLD-TAT CWF-MASTER-INDEX-VIEW.PIN-NBR CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME CWF-MASTER-INDEX-VIEW.ACTVE-IND CWF-MASTER-INDEX-VIEW.TRADE-DTE-TME
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Master_Index_View_Owner_Unit_Cde, cwf_Master_Index_View_Work_Prcss_Id, cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte, 
            cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme, cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme, pnd_Tat, pnd_Hold_Tat, cwf_Master_Index_View_Pin_Nbr, 
            cwf_Master_Index_View_Rqst_Log_Dte_Tme, cwf_Master_Index_View_Actve_Ind, cwf_Master_Index_View_Trade_Dte_Tme)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL
            sub_Write_Detail();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            short decideConditionsMet491 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CWF-MASTER-INDEX-VIEW.OWNER-UNIT-CDE NE #PREV-UNIT OR CWF-MASTER-INDEX-VIEW.OWNER-UNIT-CDE EQ ' '
            if (condition(cwf_Master_Index_View_Owner_Unit_Cde.notEquals(pnd_Prev_Unit) || cwf_Master_Index_View_Owner_Unit_Cde.equals(" ")))
            {
                decideConditionsMet491++;
                pnd_Prev_Unit.setValue(cwf_Master_Index_View_Owner_Unit_Cde);                                                                                             //Natural: MOVE CWF-MASTER-INDEX-VIEW.OWNER-UNIT-CDE TO #PREV-UNIT #REPORT-FIELDS.#RPT-UNIT
                pnd_Report_Fields_Pnd_Rpt_Unit.setValue(cwf_Master_Index_View_Owner_Unit_Cde);
                pnd_Report_Fields_Pnd_Corp_Stnd.setValue(pnd_Hold_Tat);                                                                                                   //Natural: ASSIGN #REPORT-FIELDS.#CORP-STND := #HOLD-TAT
                                                                                                                                                                          //Natural: PERFORM GET-UNIT-DESC
                sub_Get_Unit_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID NE #PREV-WPID OR CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID EQ ' '
            if (condition(cwf_Master_Index_View_Work_Prcss_Id.notEquals(pnd_Prev_Wpid) || cwf_Master_Index_View_Work_Prcss_Id.equals(" ")))
            {
                decideConditionsMet491++;
                pnd_Report_Fields_Pnd_Rpt_Wpid.setValue(cwf_Master_Index_View_Work_Prcss_Id);                                                                             //Natural: MOVE CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID TO #REPORT-FIELDS.#RPT-WPID #PREV-WPID
                pnd_Prev_Wpid.setValue(cwf_Master_Index_View_Work_Prcss_Id);
                                                                                                                                                                          //Natural: PERFORM GET-WPID-DESC
                sub_Get_Wpid_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet491 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*                                                                                                                                                           //Natural: AT BREAK OF CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID
            //*                                                                                                                                                           //Natural: AT BREAK OF CWF-MASTER-INDEX-VIEW.OWNER-UNIT-CDE
            //*        ADD TO APPROPRIATE BUCKET - PRINT REPORT BY UNIT
                                                                                                                                                                          //Natural: PERFORM ADD-TO-TOTALS
            sub_Add_To_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *SAG END-EXIT
            sort01Owner_Unit_CdeOld.setValue(cwf_Master_Index_View_Owner_Unit_Cde);                                                                                       //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //* *SAG DEFINE EXIT END-OF-PROGRAM
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TOTAL-PCTS
        sub_Calculate_Total_Pcts();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTAL-LINE
        sub_Write_Total_Line();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,"Total MIT Records Read:",pnd_Rex_Read);                                                                               //Natural: WRITE ( 1 ) 'Total MIT Records Read:' #REX-READ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(56),"**** End of Report ****");                                                 //Natural: WRITE ( 1 ) /// 56T '**** End of Report ****'
        if (Global.isEscape()) return;
        getReports().write(3, NEWLINE,NEWLINE,"Total Errors:",pnd_Errors);                                                                                                //Natural: WRITE ( 3 ) // 'Total Errors:' #ERRORS
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-TO-TOTALS
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TOTAL-PCTS
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-UNIT-PCTS
        //*   COMPUTE ROUNDED
        //*   #UNIT-TOTALS.#UNIT-NOT-ON-PCT  = (#UNIT-TOTALS.#UNIT-NOT-ON  /
        //*                                     #UNIT-TOTALS.#TOTAL-UNIT-WORK)
        //*                                     * 100
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-WPID-PCTS
        //*   COMPUTE ROUNDED
        //*   #WPID-TOTALS.#WPID-NOT-ON-PCT = (#WPID-TOTALS.#WPID-NOT-ON  /
        //*                                    #WPID-TOTALS.#TOTAL-WPID-WORK)
        //*                                    * 100
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-WPID-TABLE
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-UNIT-TABLE
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DATE-RANGE
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR
        //* *************
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *********************************************************************
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-UNIT-LINE
        //* *********************************************************************
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTAL-LINE
        //* *********************************************************************
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-WPID-DESC
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-UNIT-DESC
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL
        //* ***********************************************************************
        //*            'EFFCT/DTE'         EFFCTVE-DTE
        //* *************
        //* *SAG END-EXIT
    }
    private void sub_Add_To_Totals() throws Exception                                                                                                                     //Natural: ADD-TO-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Wpid_Totals_Pnd_Total_Wpid_Work.nadd(1);                                                                                                                      //Natural: ADD 1 TO #WPID-TOTALS.#TOTAL-WPID-WORK
        pnd_Unit_Totals_Pnd_Total_Unit_Work.nadd(1);                                                                                                                      //Natural: ADD 1 TO #UNIT-TOTALS.#TOTAL-UNIT-WORK
        pnd_Gtotals_Pnd_Gtotal_Work.nadd(1);                                                                                                                              //Natural: ADD 1 TO #GTOTALS.#GTOTAL-WORK
        pnd_Wpid_Totals_Pnd_Total_Wpid_Tat.nadd(pnd_Tat);                                                                                                                 //Natural: ASSIGN #TOTAL-WPID-TAT := #TOTAL-WPID-TAT + #TAT
        pnd_Unit_Totals_Pnd_Total_Unit_Tat.nadd(pnd_Tat);                                                                                                                 //Natural: ASSIGN #TOTAL-UNIT-TAT := #TOTAL-UNIT-TAT + #TAT
        if (condition(pnd_Tat.lessOrEqual(pnd_Hold_Tat)))                                                                                                                 //Natural: IF #TAT LE #HOLD-TAT
        {
            pnd_Wpid_Totals_Pnd_Wpid_On_Time.nadd(1);                                                                                                                     //Natural: ADD 1 TO #WPID-TOTALS.#WPID-ON-TIME
            pnd_Unit_Totals_Pnd_Unit_On_Time.nadd(1);                                                                                                                     //Natural: ADD 1 TO #UNIT-TOTALS.#UNIT-ON-TIME
            pnd_Gtotals_Pnd_Gtotal_On_Time.nadd(1);                                                                                                                       //Natural: ADD 1 TO #GTOTALS.#GTOTAL-ON-TIME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Wpid_Totals_Pnd_Wpid_Not_On.nadd(1);                                                                                                                      //Natural: ADD 1 TO #WPID-TOTALS.#WPID-NOT-ON
            pnd_Unit_Totals_Pnd_Unit_Not_On.nadd(1);                                                                                                                      //Natural: ADD 1 TO #UNIT-TOTALS.#UNIT-NOT-ON
            pnd_Gtotals_Pnd_Gtotal_Not_On.nadd(1);                                                                                                                        //Natural: ADD 1 TO #GTOTALS.#GTOTAL-NOT-ON
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  ADD-TO-TOTALS
    }
    private void sub_Calculate_Total_Pcts() throws Exception                                                                                                              //Natural: CALCULATE-TOTAL-PCTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Gtotals_Pnd_Gtotal_Work.greater(getZero())))                                                                                                    //Natural: IF #GTOTALS.#GTOTAL-WORK GT 0
        {
            pnd_Gtotals_Pnd_Gtotal_On_Time_Pct.compute(new ComputeParameters(true, pnd_Gtotals_Pnd_Gtotal_On_Time_Pct), (pnd_Gtotals_Pnd_Gtotal_On_Time.divide(pnd_Gtotals_Pnd_Gtotal_Work)).multiply(100)); //Natural: COMPUTE ROUNDED #GTOTALS.#GTOTAL-ON-TIME-PCT = ( #GTOTALS.#GTOTAL-ON-TIME / #GTOTALS.#GTOTAL-WORK ) * 100
            pnd_Gtotals_Pnd_Gtotal_Not_On_Pct.compute(new ComputeParameters(false, pnd_Gtotals_Pnd_Gtotal_Not_On_Pct), new DbsDecimal("100.00").subtract(pnd_Gtotals_Pnd_Gtotal_On_Time_Pct)); //Natural: ASSIGN #GTOTALS.#GTOTAL-NOT-ON-PCT := 100.00 - #GTOTALS.#GTOTAL-ON-TIME-PCT
            //*     COMPUTE ROUNDED
            //*     #GTOTALS.#GTOTAL-NOT-ON-PCT = (#GTOTALS.#GTOTAL-NOT-ON /
            //*                                    #GTOTALS.#GTOTAL-WORK) * 100
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  CALCULATE-UNIT-PCTS
    }
    private void sub_Calculate_Unit_Pcts() throws Exception                                                                                                               //Natural: CALCULATE-UNIT-PCTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Unit_Totals_Pnd_Total_Unit_Work.greater(getZero())))                                                                                            //Natural: IF #UNIT-TOTALS.#TOTAL-UNIT-WORK GT 0
        {
            pnd_Unit_Totals_Pnd_Unit_On_Time_Pct.compute(new ComputeParameters(true, pnd_Unit_Totals_Pnd_Unit_On_Time_Pct), (pnd_Unit_Totals_Pnd_Unit_On_Time.divide(pnd_Unit_Totals_Pnd_Total_Unit_Work)).multiply(100)); //Natural: COMPUTE ROUNDED #UNIT-TOTALS.#UNIT-ON-TIME-PCT = ( #UNIT-TOTALS.#UNIT-ON-TIME / #UNIT-TOTALS.#TOTAL-UNIT-WORK ) * 100
            pnd_Unit_Totals_Pnd_Unit_Not_On_Pct.compute(new ComputeParameters(false, pnd_Unit_Totals_Pnd_Unit_Not_On_Pct), new DbsDecimal("100.00").subtract(pnd_Unit_Totals_Pnd_Unit_On_Time_Pct)); //Natural: ASSIGN #UNIT-TOTALS.#UNIT-NOT-ON-PCT := 100.00 - #UNIT-TOTALS.#UNIT-ON-TIME-PCT
            pnd_Unit_Totals_Pnd_Avg_Unit_Tat.compute(new ComputeParameters(false, pnd_Unit_Totals_Pnd_Avg_Unit_Tat), pnd_Unit_Totals_Pnd_Total_Unit_Tat.divide(pnd_Unit_Totals_Pnd_Total_Unit_Work)); //Natural: ASSIGN #AVG-UNIT-TAT := #TOTAL-UNIT-TAT / #UNIT-TOTALS.#TOTAL-UNIT-WORK
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  CALCULATE-UNIT-PCTS
    }
    private void sub_Calculate_Wpid_Pcts() throws Exception                                                                                                               //Natural: CALCULATE-WPID-PCTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Wpid_Totals_Pnd_Total_Wpid_Work.greater(getZero())))                                                                                            //Natural: IF #WPID-TOTALS.#TOTAL-WPID-WORK GT 0
        {
            pnd_Wpid_Totals_Pnd_Wpid_On_Time_Pct.compute(new ComputeParameters(true, pnd_Wpid_Totals_Pnd_Wpid_On_Time_Pct), (pnd_Wpid_Totals_Pnd_Wpid_On_Time.divide(pnd_Wpid_Totals_Pnd_Total_Wpid_Work)).multiply(100)); //Natural: COMPUTE ROUNDED #WPID-TOTALS.#WPID-ON-TIME-PCT = ( #WPID-TOTALS.#WPID-ON-TIME / #WPID-TOTALS.#TOTAL-WPID-WORK ) * 100
            pnd_Wpid_Totals_Pnd_Wpid_Not_On_Pct.compute(new ComputeParameters(false, pnd_Wpid_Totals_Pnd_Wpid_Not_On_Pct), new DbsDecimal("100.00").subtract(pnd_Wpid_Totals_Pnd_Wpid_On_Time_Pct)); //Natural: ASSIGN #WPID-TOTALS.#WPID-NOT-ON-PCT := 100.00 - #WPID-TOTALS.#WPID-ON-TIME-PCT
            pnd_Wpid_Totals_Pnd_Avg_Wpid_Tat.compute(new ComputeParameters(false, pnd_Wpid_Totals_Pnd_Avg_Wpid_Tat), pnd_Wpid_Totals_Pnd_Total_Wpid_Tat.divide(pnd_Wpid_Totals_Pnd_Total_Wpid_Work)); //Natural: ASSIGN #AVG-WPID-TAT := #TOTAL-WPID-TAT / #WPID-TOTALS.#TOTAL-WPID-WORK
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  CALCULATE-WPID-PCTS
    }
    private void sub_Load_Wpid_Table() throws Exception                                                                                                                   //Natural: LOAD-WPID-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  READ CWF-SUPPORT-TBL
        pnd_X.reset();                                                                                                                                                    //Natural: RESET #X
        pnd_No_Records_Found.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NO-RECORDS-FOUND
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #WPID-PRIME-KEY
        (
        "LOAD_WPID",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Wpid_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        LOAD_WPID:
        while (condition(vw_cwf_Support_Tbl.readNextRow("LOAD_WPID")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pnd_Wpid_Prime_Key_Pnd_Tbl_Table_Nme)))                                                                 //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME NE #WPID-PRIME-KEY.#TBL-TABLE-NME
            {
                if (true) break LOAD_WPID;                                                                                                                                //Natural: ESCAPE BOTTOM ( LOAD-WPID. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_No_Records_Found.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #NO-RECORDS-FOUND
            LOAD_WPID_RECORD:                                                                                                                                             //Natural: FOR #I 1 TO 42
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(42)); pnd_I.nadd(1))
            {
                if (condition(cwf_Support_Tbl_Pnd_Wpid.getValue(pnd_I).equals(" ")))                                                                                      //Natural: IF CWF-SUPPORT-TBL.#WPID ( #I ) = ' '
                {
                    if (true) break LOAD_WPID;                                                                                                                            //Natural: ESCAPE BOTTOM ( LOAD-WPID. )
                }                                                                                                                                                         //Natural: END-IF
                pnd_X.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #X
                pnd_Valid_Wpid_Tbl.getValue(pnd_X).setValue(cwf_Support_Tbl_Pnd_Wpid.getValue(pnd_I));                                                                    //Natural: ASSIGN #VALID-WPID-TBL ( #X ) := CWF-SUPPORT-TBL.#WPID ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("LOAD_WPID"))) break;
                else if (condition(Global.isEscapeBottomImmediate("LOAD_WPID"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_X.greater(pnd_Max_Valid_Wpids)))                                                                                                            //Natural: IF #X GT #MAX-VALID-WPIDS
            {
                getReports().write(0, "***************************************************");                                                                             //Natural: WRITE '***************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Maximum number (100) of WPID's Has Been Reached *");                                                                             //Natural: WRITE '* Maximum number (100) of WPID"s Has Been Reached *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Program CWFB3805 must be modified -- call MIS   *");                                                                             //Natural: WRITE '* Program CWFB3805 must be modified -- call MIS   *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "***************************************************");                                                                             //Natural: WRITE '***************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_WPID"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(35);  if (true) return;                                                                                                                 //Natural: TERMINATE 35
            }                                                                                                                                                             //Natural: END-IF
            //*  LOAD-WPID.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO #X
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_X)); pnd_I.nadd(1))
        {
            getReports().write(0, pnd_Valid_Wpid_Tbl.getValue(pnd_I),pnd_I);                                                                                              //Natural: WRITE #VALID-WPID-TBL ( #I ) #I
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "number of entries in wpid table is:",pnd_X);                                                                                               //Natural: WRITE 'number of entries in wpid table is:' #X
        if (Global.isEscape()) return;
        if (condition(pnd_No_Records_Found.getBoolean()))                                                                                                                 //Natural: IF #NO-RECORDS-FOUND
        {
            pnd_Error_Msg.setValue("No Records on WPID Table:");                                                                                                          //Natural: MOVE 'No Records on WPID Table:' TO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR
            sub_Write_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  LOAD-WPID-TABLE
    }
    private void sub_Load_Unit_Table() throws Exception                                                                                                                   //Natural: LOAD-UNIT-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  READ CWF-SUPPORT-TBL
        pnd_No_Records_Found.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NO-RECORDS-FOUND
        pnd_X.reset();                                                                                                                                                    //Natural: RESET #X
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #UNIT-PRIME-KEY
        (
        "LOAD_UNIT",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Unit_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        LOAD_UNIT:
        while (condition(vw_cwf_Support_Tbl.readNextRow("LOAD_UNIT")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pnd_Unit_Prime_Key_Pnd_Tbl_Table_Nme)))                                                                 //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME NE #UNIT-PRIME-KEY.#TBL-TABLE-NME
            {
                if (true) break LOAD_UNIT;                                                                                                                                //Natural: ESCAPE BOTTOM ( LOAD-UNIT. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_No_Records_Found.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #NO-RECORDS-FOUND
            LOAD_UNIT_RECORD:                                                                                                                                             //Natural: FOR #I 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(cwf_Support_Tbl_Pnd_Unit.getValue(pnd_I).equals(" ")))                                                                                      //Natural: IF CWF-SUPPORT-TBL.#UNIT ( #I ) = ' '
                {
                    if (true) break LOAD_UNIT;                                                                                                                            //Natural: ESCAPE BOTTOM ( LOAD-UNIT. )
                }                                                                                                                                                         //Natural: END-IF
                pnd_X.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #X
                pnd_Valid_Unit_Tbl.getValue(pnd_X).setValue(cwf_Support_Tbl_Pnd_Unit.getValue(pnd_I));                                                                    //Natural: ASSIGN #VALID-UNIT-TBL ( #X ) := CWF-SUPPORT-TBL.#UNIT ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("LOAD_UNIT"))) break;
                else if (condition(Global.isEscapeBottomImmediate("LOAD_UNIT"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_X.greater(pnd_Max_Valid_Units)))                                                                                                            //Natural: IF #X GT #MAX-VALID-UNITS
            {
                getReports().write(0, "**************************************************");                                                                              //Natural: WRITE '**************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Maximum Number (20) of Unit's Has Been Reached *");                                                                              //Natural: WRITE '* Maximum Number (20) of Unit"s Has Been Reached *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Program CWFB3805 must be modified -- call MIS  *");                                                                              //Natural: WRITE '* Program CWFB3805 must be modified -- call MIS  *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "**************************************************");                                                                              //Natural: WRITE '**************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOAD_UNIT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(35);  if (true) return;                                                                                                                 //Natural: TERMINATE 35
            }                                                                                                                                                             //Natural: END-IF
            //*  LOAD-WPID.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO #X
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_X)); pnd_I.nadd(1))
        {
            getReports().write(0, pnd_Valid_Unit_Tbl.getValue(pnd_I),pnd_I,pnd_Valid_Unit_Tbl_Pnd_Valid_Unit.getValue(pnd_I),pnd_Valid_Unit_Tbl_Pnd_Valid_Tat.getValue(pnd_I)); //Natural: WRITE #VALID-UNIT-TBL ( #I ) #I #VALID-UNIT ( #I ) #VALID-TAT ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "number or entries in unit table is:",pnd_X);                                                                                               //Natural: WRITE 'number or entries in unit table is:' #X
        if (Global.isEscape()) return;
        if (condition(pnd_No_Records_Found.getBoolean()))                                                                                                                 //Natural: IF #NO-RECORDS-FOUND
        {
            pnd_Error_Msg.setValue("No Records on Unit Table:");                                                                                                          //Natural: MOVE 'No Records on Unit Table:' TO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR
            sub_Write_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  LOAD-UNIT-TABLE
    }
    private void sub_Get_Date_Range() throws Exception                                                                                                                    //Natural: GET-DATE-RANGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  READ CWF-SUPPORT-TBL
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #DATE-PRIME-KEY
        (
        "GET_DATES",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Date_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        GET_DATES:
        while (condition(vw_cwf_Support_Tbl.readNextRow("GET_DATES")))
        {
            if (condition(cwf_Support_Tbl_Date_Ind.notEquals(pnd_Date_Prime_Key_Pnd_Tbl_Key_Field)))                                                                      //Natural: IF CWF-SUPPORT-TBL.DATE-IND NE #DATE-PRIME-KEY.#TBL-KEY-FIELD
            {
                if (true) break GET_DATES;                                                                                                                                //Natural: ESCAPE BOTTOM ( GET-DATES. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Run_Date.setValue(cwf_Support_Tbl_Tbl_Data_Field);                                                                                                        //Natural: ASSIGN #RUN-DATE := CWF-SUPPORT-TBL.TBL-DATA-FIELD
            //*  GET-DATES.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  IF NO DATE RECORD - FORCE ABEND WITH RC 33
        //*  IF NO INPUT START DATE  USE DAILY DATE RECORD ON CWF-RPRT-RUN-TBLE
        //*  TO GET MONTH AND YEAR; USE '01' FOR START-DAY AND '31' FOR END DAY
        if (condition(pnd_Run_Date.equals(" ")))                                                                                                                          //Natural: IF #RUN-DATE EQ ' '
        {
            getReports().write(0, "****** NO DAILY RUN DATE RECORD ON CWF-RPRT-RUN-TBL *****");                                                                           //Natural: WRITE '****** NO DAILY RUN DATE RECORD ON CWF-RPRT-RUN-TBL *****'
            if (Global.isEscape()) return;
            DbsUtil.terminate(33);  if (true) return;                                                                                                                     //Natural: TERMINATE 33
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Convert_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Run_Date);                                                                                 //Natural: MOVE EDITED #RUN-DATE TO #CONVERT-DATE ( EM = YYYYMMDD )
            //*  START-DATE = RUN-DATE - 6
            pnd_Disp_Date.setValueEdited(pnd_Convert_Date,new ReportEditMask("MM/DD/YY"));                                                                                //Natural: MOVE EDITED #CONVERT-DATE ( EM = MM/DD/YY ) TO #DISP-DATE
            pnd_Convert_Date.nsubtract(6);                                                                                                                                //Natural: ASSIGN #CONVERT-DATE := #CONVERT-DATE - 6
            //*  END-DATE   = FRIDAY
            pnd_Start_Date_A.setValueEdited(pnd_Convert_Date,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED #CONVERT-DATE ( EM = YYYYMMDD ) TO #START-DATE-A
            pnd_End_Date_A.setValue(pnd_Run_Date);                                                                                                                        //Natural: ASSIGN #END-DATE-A := #RUN-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  GET-DATE-RANGE
    }
    private void sub_Write_Error() throws Exception                                                                                                                       //Natural: WRITE-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, pnd_Error_Msg);                                                                                                                             //Natural: WRITE #ERROR-MSG
        if (Global.isEscape()) return;
        pnd_Errors.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ERRORS
        //* *************
        //*  WRITE-ERROR
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "/WPID",                                                                                                                                  //Natural: DISPLAY ( 1 ) '/WPID' #REPORT-FIELDS.#RPT-WPID 5X '/Description' #REPORT-FIELDS.WORK-PRCSS-LONG-NME 'Total/On Time' #WPID-TOTALS.#WPID-ON-TIME 4X 'Percent/On Time' #WPID-TOTALS.#WPID-ON-TIME-PCT '�/�' #LINE 'Total Not/On Time' #WPID-TOTALS.#WPID-NOT-ON 4X 'Percent Not/On Time' #WPID-TOTALS.#WPID-NOT-ON-PCT '�/�' #LINE2 3X 'Total/Completed' #WPID-TOTALS.#TOTAL-WPID-WORK 'Avg/Turnaround' #WPID-TOTALS.#AVG-WPID-TAT
        		pnd_Report_Fields_Pnd_Rpt_Wpid,new ColumnSpacing(5),"/Description",
        		pnd_Report_Fields_Work_Prcss_Long_Nme,"Total/On Time",
        		pnd_Wpid_Totals_Pnd_Wpid_On_Time,new ColumnSpacing(4),"Percent/On Time",
        		pnd_Wpid_Totals_Pnd_Wpid_On_Time_Pct,"�/�",
        		pnd_Line,"Total Not/On Time",
        		pnd_Wpid_Totals_Pnd_Wpid_Not_On,new ColumnSpacing(4),"Percent Not/On Time",
        		pnd_Wpid_Totals_Pnd_Wpid_Not_On_Pct,"�/�",
        		pnd_Line2,new ColumnSpacing(3),"Total/Completed",
        		pnd_Wpid_Totals_Pnd_Total_Wpid_Work,"Avg/Turnaround",
        		pnd_Wpid_Totals_Pnd_Avg_Wpid_Tat);
        if (Global.isEscape()) return;
        //* *************
        //*  WRITE-REPORT
    }
    private void sub_Write_Unit_Line() throws Exception                                                                                                                   //Natural: WRITE-UNIT-LINE
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"-",new RepeatItem(132),"Total for:",pnd_Report_Fields_Unit_Nme_Short,new ReportTAsterisk(pnd_Wpid_Totals_Pnd_Wpid_On_Time),pnd_Unit_Totals_Pnd_Unit_On_Time,new  //Natural: WRITE ( 1 ) // '-' ( 132 ) 'Total for:' #REPORT-FIELDS.UNIT-NME-SHORT T*#WPID-TOTALS.#WPID-ON-TIME #UNIT-TOTALS.#UNIT-ON-TIME T*#WPID-TOTALS.#WPID-ON-TIME-PCT #UNIT-TOTALS.#UNIT-ON-TIME-PCT T*#LINE #LINE T*#WPID-TOTALS.#WPID-NOT-ON #UNIT-TOTALS.#UNIT-NOT-ON T*#WPID-TOTALS.#WPID-NOT-ON-PCT #UNIT-TOTALS.#UNIT-NOT-ON-PCT T*#LINE2 #LINE2 T*#WPID-TOTALS.#TOTAL-WPID-WORK #UNIT-TOTALS.#TOTAL-UNIT-WORK T*#WPID-TOTALS.#AVG-WPID-TAT #UNIT-TOTALS.#AVG-UNIT-TAT
            ReportTAsterisk(pnd_Wpid_Totals_Pnd_Wpid_On_Time_Pct),pnd_Unit_Totals_Pnd_Unit_On_Time_Pct,new ReportTAsterisk(pnd_Line),pnd_Line,new ReportTAsterisk(pnd_Wpid_Totals_Pnd_Wpid_Not_On),pnd_Unit_Totals_Pnd_Unit_Not_On,new 
            ReportTAsterisk(pnd_Wpid_Totals_Pnd_Wpid_Not_On_Pct),pnd_Unit_Totals_Pnd_Unit_Not_On_Pct,new ReportTAsterisk(pnd_Line2),pnd_Line2,new ReportTAsterisk(pnd_Wpid_Totals_Pnd_Total_Wpid_Work),pnd_Unit_Totals_Pnd_Total_Unit_Work,new 
            ReportTAsterisk(pnd_Wpid_Totals_Pnd_Avg_Wpid_Tat),pnd_Unit_Totals_Pnd_Avg_Unit_Tat);
        if (Global.isEscape()) return;
        //* *************
        //*  WRITE-REPORT
    }
    private void sub_Write_Total_Line() throws Exception                                                                                                                  //Natural: WRITE-TOTAL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"-",new RepeatItem(132),"Totals:",new ReportTAsterisk(pnd_Wpid_Totals_Pnd_Wpid_On_Time),pnd_Gtotals_Pnd_Gtotal_On_Time,new  //Natural: WRITE ( 1 ) // '-' ( 132 ) 'Totals:' T*#WPID-TOTALS.#WPID-ON-TIME #GTOTALS.#GTOTAL-ON-TIME T*#WPID-TOTALS.#WPID-ON-TIME-PCT #GTOTALS.#GTOTAL-ON-TIME-PCT T*#LINE #LINE T*#WPID-TOTALS.#WPID-NOT-ON #GTOTALS.#GTOTAL-NOT-ON T*#WPID-TOTALS.#WPID-NOT-ON-PCT #GTOTALS.#GTOTAL-NOT-ON-PCT T*#LINE2 #LINE2 T*#WPID-TOTALS.#TOTAL-WPID-WORK #GTOTALS.#GTOTAL-WORK
            ReportTAsterisk(pnd_Wpid_Totals_Pnd_Wpid_On_Time_Pct),pnd_Gtotals_Pnd_Gtotal_On_Time_Pct,new ReportTAsterisk(pnd_Line),pnd_Line,new ReportTAsterisk(pnd_Wpid_Totals_Pnd_Wpid_Not_On),pnd_Gtotals_Pnd_Gtotal_Not_On,new 
            ReportTAsterisk(pnd_Wpid_Totals_Pnd_Wpid_Not_On_Pct),pnd_Gtotals_Pnd_Gtotal_Not_On_Pct,new ReportTAsterisk(pnd_Line2),pnd_Line2,new ReportTAsterisk(pnd_Wpid_Totals_Pnd_Total_Wpid_Work),
            pnd_Gtotals_Pnd_Gtotal_Work);
        if (Global.isEscape()) return;
        //* *************
        //*  WRITE-REPORT
    }
    private void sub_Get_Wpid_Desc() throws Exception                                                                                                                     //Natural: GET-WPID-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_No_Records_Found.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NO-RECORDS-FOUND
        vw_cwf_Wp_Work_Prcss_Id.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) CWF-WP-WORK-PRCSS-ID BY WPID-UNIQ-KEY STARTING FROM #REPORT-FIELDS.#RPT-WPID
        (
        "READ_WPID",
        new Wc[] { new Wc("WPID_UNIQ_KEY", ">=", pnd_Report_Fields_Pnd_Rpt_Wpid, WcType.BY) },
        new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") },
        1
        );
        READ_WPID:
        while (condition(vw_cwf_Wp_Work_Prcss_Id.readNextRow("READ_WPID")))
        {
            if (condition(cwf_Wp_Work_Prcss_Id_Work_Prcss_Id.notEquals(pnd_Report_Fields_Pnd_Rpt_Wpid)))                                                                  //Natural: IF CWF-WP-WORK-PRCSS-ID.WORK-PRCSS-ID NE #REPORT-FIELDS.#RPT-WPID
            {
                if (true) break READ_WPID;                                                                                                                                //Natural: ESCAPE BOTTOM ( READ-WPID. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Report_Fields.setValuesByName(vw_cwf_Wp_Work_Prcss_Id);                                                                                                   //Natural: MOVE BY NAME CWF-WP-WORK-PRCSS-ID TO #REPORT-FIELDS
            pnd_No_Records_Found.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #NO-RECORDS-FOUND
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_No_Records_Found.getBoolean() || pnd_Report_Fields_Work_Prcss_Long_Nme.equals(" ")))                                                            //Natural: IF #NO-RECORDS-FOUND OR #REPORT-FIELDS.WORK-PRCSS-LONG-NME = ' '
        {
            pnd_Error_Msg.setValue(DbsUtil.compress("****** No WPID Record Found on WPID table for WPID:", pnd_Report_Fields_Pnd_Rpt_Wpid, "******"));                    //Natural: COMPRESS '****** No WPID Record Found on WPID table for WPID:' #REPORT-FIELDS.#RPT-WPID '******' INTO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR
            sub_Write_Error();
            if (condition(Global.isEscape())) {return;}
            pnd_Report_Fields_Work_Prcss_Long_Nme.setValue(pnd_Error_Msg);                                                                                                //Natural: MOVE #ERROR-MSG TO #REPORT-FIELDS.WORK-PRCSS-LONG-NME
            pnd_Report_Fields_Work_Prcss_Short_Nme.reset();                                                                                                               //Natural: RESET #REPORT-FIELDS.WORK-PRCSS-SHORT-NME
            pnd_No_Wpid.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #NO-WPID
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  GET-WPID-INFO
    }
    private void sub_Get_Unit_Desc() throws Exception                                                                                                                     //Natural: GET-UNIT-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_No_Records_Found.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NO-RECORDS-FOUND
        //*  READ CWF-ORG-UNIT-TBL
        vw_cwf_Org_Unit_Tbl.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) CWF-ORG-UNIT-TBL BY UNIQ-UNIT-CDE STARTING FROM #REPORT-FIELDS.#RPT-UNIT
        (
        "READ_UNIT",
        new Wc[] { new Wc("UNIQ_UNIT_CDE", ">=", pnd_Report_Fields_Pnd_Rpt_Unit, WcType.BY) },
        new Oc[] { new Oc("UNIQ_UNIT_CDE", "ASC") },
        1
        );
        READ_UNIT:
        while (condition(vw_cwf_Org_Unit_Tbl.readNextRow("READ_UNIT")))
        {
            if (condition(cwf_Org_Unit_Tbl_Unit_Cde.notEquals(pnd_Report_Fields_Pnd_Rpt_Unit)))                                                                           //Natural: IF CWF-ORG-UNIT-TBL.UNIT-CDE NE #REPORT-FIELDS.#RPT-UNIT
            {
                if (true) break READ_UNIT;                                                                                                                                //Natural: ESCAPE BOTTOM ( READ-UNIT. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_No_Records_Found.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #NO-RECORDS-FOUND
            pnd_Report_Fields_Unit_Nme_Short.setValue(cwf_Org_Unit_Tbl_Unit_Short_Nme);                                                                                   //Natural: MOVE CWF-ORG-UNIT-TBL.UNIT-SHORT-NME TO #REPORT-FIELDS.UNIT-NME-SHORT
            pnd_Report_Fields_Unit_Nme_Long.setValue(cwf_Org_Unit_Tbl_Unit_Long_Nme);                                                                                     //Natural: MOVE CWF-ORG-UNIT-TBL.UNIT-LONG-NME TO #REPORT-FIELDS.UNIT-NME-LONG
            //*  READ-UNIT.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_No_Records_Found.getBoolean() || pnd_Report_Fields_Unit_Nme_Long.equals(" ")))                                                                  //Natural: IF #NO-RECORDS-FOUND OR #REPORT-FIELDS.UNIT-NME-LONG = ' '
        {
            pnd_Error_Msg.setValue(DbsUtil.compress("****** No Unit Record found for Unit:", pnd_Report_Fields_Pnd_Rpt_Unit, "******"));                                  //Natural: COMPRESS '****** No Unit Record found for Unit:' #REPORT-FIELDS.#RPT-UNIT '******' INTO #ERROR-MSG
            pnd_Report_Fields_Unit_Nme_Long.setValue(pnd_Error_Msg);                                                                                                      //Natural: MOVE #ERROR-MSG TO #REPORT-FIELDS.UNIT-NME-LONG
            pnd_Report_Fields_Unit_Nme_Short.reset();                                                                                                                     //Natural: RESET #REPORT-FIELDS.UNIT-NME-SHORT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR
            sub_Write_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  GET-UNIT-NME
    }
    private void sub_Write_Detail() throws Exception                                                                                                                      //Natural: WRITE-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(2, "PIN/NBR",                                                                                                                                //Natural: DISPLAY ( 2 ) 'PIN/NBR' PIN-NBR 'RQLDT' RQST-LOG-DTE-TME '/WPID' CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID 'UNIT/CDE' CWF-MASTER-INDEX-VIEW.OWNER-UNIT-CDE 'ACT/IND' CWF-MASTER-INDEX-VIEW.ACTVE-IND 'TIAA/RCVD' TIAA-RCVD-DTE ( EM = YYYYMMDD ) 'EXT/PND DTE' EXTRNL-PEND-RCV-DTE ( EM = YYYYMMDD ) 'PART/CLOS DTE' CRPRTE-CLOCK-END-DTE-TME 'OWNER/UNIT' OWNER-UNIT-CDE 'TRADE/DTE' TRADE-DTE-TME ( EM = YYYYMMDDHHIISST )
        		cwf_Master_Index_View_Pin_Nbr,"RQLDT",
        		cwf_Master_Index_View_Rqst_Log_Dte_Tme,"/WPID",
        		cwf_Master_Index_View_Work_Prcss_Id,"UNIT/CDE",
        		cwf_Master_Index_View_Owner_Unit_Cde,"ACT/IND",
        		cwf_Master_Index_View_Actve_Ind,"TIAA/RCVD",
        		cwf_Master_Index_View_Tiaa_Rcvd_Dte, new ReportEditMask ("YYYYMMDD"),"EXT/PND DTE",
        		cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte, new ReportEditMask ("YYYYMMDD"),"PART/CLOS DTE",
        		cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme,"OWNER/UNIT",
        		cwf_Master_Index_View_Owner_Unit_Cde,"TRADE/DTE",
        		cwf_Master_Index_View_Trade_Dte_Tme, new ReportEditMask ("YYYYMMDDHHIISST"));
        if (Global.isEscape()) return;
        //* *************
        //*  WRITE-DETAIL
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Report_Fields_Pnd_Unit_Cde_Rpt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Prev_Unit, ")"));                                //Natural: COMPRESS '(' #PREV-UNIT ')' INTO #UNIT-CDE-RPT LEAVING NO SPACE
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP ) / 20T 'Unit:'#PREV-UNIT #REPORT-FIELDS.UNIT-NME-LONG '(On time Standard = ' #CORP-STND 'Days)' / 53T 'For Week Ending' #DISP-DATE
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header1_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(20),"Unit:",pnd_Prev_Unit,pnd_Report_Fields_Unit_Nme_Long,"(On time Standard = ",pnd_Report_Fields_Pnd_Corp_Stnd,"Days)",NEWLINE,new 
                        TabSetting(53),"For Week Ending",pnd_Disp_Date);
                    //* *SAG DEFINE EXIT REPORT1-AT-TOP-OF-PAGE
                    //* *SAG END-EXIT
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header2_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(2),  //Natural: WRITE ( 2 ) NOTITLE *PROGRAM 41T #HEADER2-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 2 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER2-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header2_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Master_Index_View_Work_Prcss_IdIsBreak = cwf_Master_Index_View_Work_Prcss_Id.isBreak(endOfData);
        boolean cwf_Master_Index_View_Owner_Unit_CdeIsBreak = cwf_Master_Index_View_Owner_Unit_Cde.isBreak(endOfData);
        if (condition(cwf_Master_Index_View_Work_Prcss_IdIsBreak || cwf_Master_Index_View_Owner_Unit_CdeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-WPID-PCTS
            sub_Calculate_Wpid_Pcts();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Wpid_Totals.reset();                                                                                                                                      //Natural: RESET #WPID-TOTALS
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(cwf_Master_Index_View_Owner_Unit_CdeIsBreak))
        {
            pnd_Report_Fields_Pnd_Rpt_Unit.setValue(sort01Owner_Unit_CdeOld);                                                                                             //Natural: ASSIGN #REPORT-FIELDS.#RPT-UNIT := OLD ( CWF-MASTER-INDEX-VIEW.OWNER-UNIT-CDE )
                                                                                                                                                                          //Natural: PERFORM CALCULATE-UNIT-PCTS
            sub_Calculate_Unit_Pcts();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-UNIT-LINE
            sub_Write_Unit_Line();
            if (condition(Global.isEscape())) {return;}
            pnd_Unit_Totals.reset();                                                                                                                                      //Natural: RESET #UNIT-TOTALS
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(3, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "/WPID",
        		pnd_Report_Fields_Pnd_Rpt_Wpid,new ColumnSpacing(5),"/Description",
        		pnd_Report_Fields_Work_Prcss_Long_Nme,"Total/On Time",
        		pnd_Wpid_Totals_Pnd_Wpid_On_Time,new ColumnSpacing(4),"Percent/On Time",
        		pnd_Wpid_Totals_Pnd_Wpid_On_Time_Pct,"�/�",
        		pnd_Line,"Total Not/On Time",
        		pnd_Wpid_Totals_Pnd_Wpid_Not_On,new ColumnSpacing(4),"Percent Not/On Time",
        		pnd_Wpid_Totals_Pnd_Wpid_Not_On_Pct,"�/�",
        		pnd_Line2,new ColumnSpacing(3),"Total/Completed",
        		pnd_Wpid_Totals_Pnd_Total_Wpid_Work,"Avg/Turnaround",
        		pnd_Wpid_Totals_Pnd_Avg_Wpid_Tat);
        getReports().setDisplayColumns(2, "PIN/NBR",
        		cwf_Master_Index_View_Pin_Nbr,"RQLDT",
        		cwf_Master_Index_View_Rqst_Log_Dte_Tme,"/WPID",
        		cwf_Master_Index_View_Work_Prcss_Id,"UNIT/CDE",
        		cwf_Master_Index_View_Owner_Unit_Cde,"ACT/IND",
        		cwf_Master_Index_View_Actve_Ind,"TIAA/RCVD",
        		cwf_Master_Index_View_Tiaa_Rcvd_Dte, new ReportEditMask ("YYYYMMDD"),"EXT/PND DTE",
        		cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte, new ReportEditMask ("YYYYMMDD"),"PART/CLOS DTE",
        		cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme,"OWNER/UNIT",
        		cwf_Master_Index_View_Owner_Unit_Cde,"TRADE/DTE",
        		cwf_Master_Index_View_Trade_Dte_Tme, new ReportEditMask ("YYYYMMDDHHIISST"));
    }
}
