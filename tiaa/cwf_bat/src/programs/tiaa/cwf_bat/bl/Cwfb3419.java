/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:43 PM
**        * FROM NATURAL PROGRAM : Cwfb3419
************************************************************
**        * FILE NAME            : Cwfb3419.java
**        * CLASS NAME           : Cwfb3419
**        * INSTANCE NAME        : Cwfb3419
************************************************************
************************************************************************
* PROGRAM  : CWFB3419
* SYSTEM   : CRPCWF
* TITLE    : BATCH UPDATE/REPORT DRIVER/ P2200CWD
* FUNCTION : GETS THE NAME OF THE BATCH PROGRAMS FROM THE
*          :  SUPPORT TABLE AND FETCHES IT WHEN INDICATORS SAYS SO.
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3419 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_gen_Tbl;
    private DbsField gen_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField gen_Tbl_Tbl_Table_Nme;
    private DbsField gen_Tbl_Tbl_Key_Field;
    private DbsField gen_Tbl_Tbl_Data_Field;

    private DbsGroup gen_Tbl__R_Field_1;
    private DbsField gen_Tbl_Tbl_Data_Program;
    private DbsField gen_Tbl_Tbl_Data_F1;
    private DbsField gen_Tbl_Tbl_Data_Parm;
    private DbsField gen_Tbl_Tbl_Data_F2;
    private DbsField gen_Tbl_Tbl_Data_Run_Ind;
    private DbsField gen_Tbl_Tbl_Data_F3;
    private DbsField gen_Tbl_Tbl_Data_Run_Time;
    private DbsField gen_Tbl_Tbl_Updte_Dte;
    private DbsField gen_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField gen_Tbl_Tbl_Updte_Oprtr_Cde;

    private DbsGroup pnd_Gen_Var;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Key;

    private DbsGroup pnd_Gen_Var__R_Field_2;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Authrty;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Name;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Code;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Desc;
    private DbsField pnd_Batch_Step;
    private DbsField pnd_Batch_Prog;
    private DbsField pnd_Batch_Parm;
    private DbsField pnd_Batch_Ind;

    private DbsGroup pnd_Vars;
    private DbsField pnd_Vars_Pnd_Ctr;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Yes_Run;
    private DbsField pnd_Const_Pnd_Permanent;
    private DbsField pnd_Const_Pnd_Success_Ran;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_gen_Tbl = new DataAccessProgramView(new NameInfo("vw_gen_Tbl", "GEN-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        gen_Tbl_Tbl_Scrty_Level_Ind = vw_gen_Tbl.getRecord().newFieldInGroup("gen_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        gen_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        gen_Tbl_Tbl_Table_Nme = vw_gen_Tbl.getRecord().newFieldInGroup("gen_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TBL_TABLE_NME");
        gen_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        gen_Tbl_Tbl_Key_Field = vw_gen_Tbl.getRecord().newFieldInGroup("gen_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "TBL_KEY_FIELD");
        gen_Tbl_Tbl_Data_Field = vw_gen_Tbl.getRecord().newFieldInGroup("gen_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");

        gen_Tbl__R_Field_1 = vw_gen_Tbl.getRecord().newGroupInGroup("gen_Tbl__R_Field_1", "REDEFINE", gen_Tbl_Tbl_Data_Field);
        gen_Tbl_Tbl_Data_Program = gen_Tbl__R_Field_1.newFieldInGroup("gen_Tbl_Tbl_Data_Program", "TBL-DATA-PROGRAM", FieldType.STRING, 8);
        gen_Tbl_Tbl_Data_F1 = gen_Tbl__R_Field_1.newFieldInGroup("gen_Tbl_Tbl_Data_F1", "TBL-DATA-F1", FieldType.STRING, 1);
        gen_Tbl_Tbl_Data_Parm = gen_Tbl__R_Field_1.newFieldInGroup("gen_Tbl_Tbl_Data_Parm", "TBL-DATA-PARM", FieldType.STRING, 8);
        gen_Tbl_Tbl_Data_F2 = gen_Tbl__R_Field_1.newFieldInGroup("gen_Tbl_Tbl_Data_F2", "TBL-DATA-F2", FieldType.STRING, 1);
        gen_Tbl_Tbl_Data_Run_Ind = gen_Tbl__R_Field_1.newFieldInGroup("gen_Tbl_Tbl_Data_Run_Ind", "TBL-DATA-RUN-IND", FieldType.STRING, 1);
        gen_Tbl_Tbl_Data_F3 = gen_Tbl__R_Field_1.newFieldInGroup("gen_Tbl_Tbl_Data_F3", "TBL-DATA-F3", FieldType.STRING, 1);
        gen_Tbl_Tbl_Data_Run_Time = gen_Tbl__R_Field_1.newFieldInGroup("gen_Tbl_Tbl_Data_Run_Time", "TBL-DATA-RUN-TIME", FieldType.STRING, 15);
        gen_Tbl_Tbl_Updte_Dte = vw_gen_Tbl.getRecord().newFieldInGroup("gen_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        gen_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        gen_Tbl_Tbl_Updte_Dte_Tme = vw_gen_Tbl.getRecord().newFieldInGroup("gen_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE_TME");
        gen_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        gen_Tbl_Tbl_Updte_Oprtr_Cde = vw_gen_Tbl.getRecord().newFieldInGroup("gen_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        gen_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_gen_Tbl);

        pnd_Gen_Var = localVariables.newGroupInRecord("pnd_Gen_Var", "#GEN-VAR");
        pnd_Gen_Var_Pnd_Gen_Tbl_Key = pnd_Gen_Var.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Key", "#GEN-TBL-KEY", FieldType.STRING, 53);

        pnd_Gen_Var__R_Field_2 = pnd_Gen_Var.newGroupInGroup("pnd_Gen_Var__R_Field_2", "REDEFINE", pnd_Gen_Var_Pnd_Gen_Tbl_Key);
        pnd_Gen_Var_Pnd_Gen_Tbl_Authrty = pnd_Gen_Var__R_Field_2.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Authrty", "#GEN-TBL-AUTHRTY", FieldType.STRING, 
            2);
        pnd_Gen_Var_Pnd_Gen_Tbl_Name = pnd_Gen_Var__R_Field_2.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Name", "#GEN-TBL-NAME", FieldType.STRING, 20);
        pnd_Gen_Var_Pnd_Gen_Tbl_Code = pnd_Gen_Var__R_Field_2.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Code", "#GEN-TBL-CODE", FieldType.STRING, 30);
        pnd_Gen_Var_Pnd_Gen_Tbl_Desc = pnd_Gen_Var.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Desc", "#GEN-TBL-DESC", FieldType.STRING, 30);
        pnd_Batch_Step = localVariables.newFieldArrayInRecord("pnd_Batch_Step", "#BATCH-STEP", FieldType.STRING, 8, new DbsArrayController(1, 15));
        pnd_Batch_Prog = localVariables.newFieldArrayInRecord("pnd_Batch_Prog", "#BATCH-PROG", FieldType.STRING, 8, new DbsArrayController(1, 15));
        pnd_Batch_Parm = localVariables.newFieldArrayInRecord("pnd_Batch_Parm", "#BATCH-PARM", FieldType.STRING, 8, new DbsArrayController(1, 15));
        pnd_Batch_Ind = localVariables.newFieldArrayInRecord("pnd_Batch_Ind", "#BATCH-IND", FieldType.STRING, 1, new DbsArrayController(1, 15));

        pnd_Vars = localVariables.newGroupInRecord("pnd_Vars", "#VARS");
        pnd_Vars_Pnd_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 3);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Yes_Run = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Yes_Run", "#YES-RUN", FieldType.STRING, 1);
        pnd_Const_Pnd_Permanent = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Permanent", "#PERMANENT", FieldType.STRING, 1);
        pnd_Const_Pnd_Success_Ran = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Success_Ran", "#SUCCESS-RAN", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_gen_Tbl.reset();

        localVariables.reset();
        pnd_Vars_Pnd_Ctr.setInitialValue(0);
        pnd_Const_Pnd_Yes_Run.setInitialValue("Y");
        pnd_Const_Pnd_Permanent.setInitialValue("P");
        pnd_Const_Pnd_Success_Ran.setInitialValue("S");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3419() throws Exception
    {
        super("Cwfb3419");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 140 PS = 60
        pnd_Gen_Var_Pnd_Gen_Tbl_Authrty.setValue("A");                                                                                                                    //Natural: MOVE 'A' TO #GEN-TBL-AUTHRTY
        pnd_Gen_Var_Pnd_Gen_Tbl_Name.setValue("P2200CWD-UPDT900");                                                                                                        //Natural: MOVE 'P2200CWD-UPDT900' TO #GEN-TBL-NAME
        pnd_Batch_Step.getValue("*").setValue(" ");                                                                                                                       //Natural: MOVE ' ' TO #BATCH-STEP ( * ) #BATCH-PROG ( * ) #BATCH-PARM ( * ) #BATCH-IND ( * )
        pnd_Batch_Prog.getValue("*").setValue(" ");
        pnd_Batch_Parm.getValue("*").setValue(" ");
        pnd_Batch_Ind.getValue("*").setValue(" ");
        vw_gen_Tbl.startDatabaseRead                                                                                                                                      //Natural: READ GEN-TBL BY TBL-PRIME-KEY FROM #GEN-TBL-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Gen_Var_Pnd_Gen_Tbl_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_gen_Tbl.readNextRow("READ01")))
        {
            if (condition(gen_Tbl_Tbl_Scrty_Level_Ind.notEquals(pnd_Gen_Var_Pnd_Gen_Tbl_Authrty) || gen_Tbl_Tbl_Table_Nme.notEquals(pnd_Gen_Var_Pnd_Gen_Tbl_Name)))       //Natural: IF TBL-SCRTY-LEVEL-IND NE #GEN-TBL-AUTHRTY OR TBL-TABLE-NME NE #GEN-TBL-NAME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(gen_Tbl_Tbl_Data_Run_Ind.equals(pnd_Const_Pnd_Yes_Run) || gen_Tbl_Tbl_Data_Run_Ind.equals(pnd_Const_Pnd_Permanent)))                            //Natural: IF TBL-DATA-RUN-IND EQ #YES-RUN OR EQ #PERMANENT
            {
                pnd_Vars_Pnd_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CTR
                pnd_Batch_Step.getValue(pnd_Vars_Pnd_Ctr).setValue(gen_Tbl_Tbl_Key_Field);                                                                                //Natural: MOVE TBL-KEY-FIELD TO #BATCH-STEP ( #CTR )
                pnd_Batch_Prog.getValue(pnd_Vars_Pnd_Ctr).setValue(gen_Tbl_Tbl_Data_Program);                                                                             //Natural: MOVE TBL-DATA-PROGRAM TO #BATCH-PROG ( #CTR )
                pnd_Batch_Parm.getValue(pnd_Vars_Pnd_Ctr).setValue(gen_Tbl_Tbl_Data_Parm);                                                                                //Natural: MOVE TBL-DATA-PARM TO #BATCH-PARM ( #CTR )
                pnd_Batch_Ind.getValue(pnd_Vars_Pnd_Ctr).setValue(gen_Tbl_Tbl_Data_Run_Ind);                                                                              //Natural: MOVE TBL-DATA-RUN-IND TO #BATCH-IND ( #CTR )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #CTR 1 15
        for (pnd_Vars_Pnd_Ctr.setValue(1); condition(pnd_Vars_Pnd_Ctr.lessOrEqual(15)); pnd_Vars_Pnd_Ctr.nadd(1))
        {
            if (condition(pnd_Batch_Prog.getValue(pnd_Vars_Pnd_Ctr).equals(" ")))                                                                                         //Natural: IF #BATCH-PROG ( #CTR ) EQ ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Batch_Ind.getValue(pnd_Vars_Pnd_Ctr).equals(pnd_Const_Pnd_Yes_Run) || pnd_Batch_Ind.getValue(pnd_Vars_Pnd_Ctr).equals(pnd_Const_Pnd_Permanent))) //Natural: IF #BATCH-IND ( #CTR ) EQ #YES-RUN OR EQ #PERMANENT
            {
                if (condition(pnd_Batch_Parm.getValue(pnd_Vars_Pnd_Ctr).equals(" ") || pnd_Batch_Parm.getValue(pnd_Vars_Pnd_Ctr).equals("00000000")))                     //Natural: IF #BATCH-PARM ( #CTR ) EQ ' ' OR EQ '00000000'
                {
                    DbsUtil.invokeMain(DbsUtil.getBlType(pnd_Batch_Prog.getValue(pnd_Vars_Pnd_Ctr)), getCurrentProcessState());                                           //Natural: FETCH RETURN #BATCH-PROG ( #CTR )
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Batch_Parm.getValue(pnd_Vars_Pnd_Ctr));                                                               //Natural: FETCH RETURN #BATCH-PROG ( #CTR ) #BATCH-PARM ( #CTR )
                    DbsUtil.invokeMain(DbsUtil.getBlType(pnd_Batch_Prog.getValue(pnd_Vars_Pnd_Ctr)), getCurrentProcessState());
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                pnd_Gen_Var_Pnd_Gen_Tbl_Authrty.setValue("A");                                                                                                            //Natural: MOVE 'A' TO #GEN-TBL-AUTHRTY
                pnd_Gen_Var_Pnd_Gen_Tbl_Name.setValue("P2200CWD-UPDT900");                                                                                                //Natural: MOVE 'P2200CWD-UPDT900' TO #GEN-TBL-NAME
                pnd_Gen_Var_Pnd_Gen_Tbl_Code.setValue(pnd_Batch_Step.getValue(pnd_Vars_Pnd_Ctr));                                                                         //Natural: MOVE #BATCH-STEP ( #CTR ) TO #GEN-TBL-CODE
                vw_gen_Tbl.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) GEN-TBL BY TBL-PRIME-KEY FROM #GEN-TBL-KEY
                (
                "READ02",
                new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Gen_Var_Pnd_Gen_Tbl_Key, WcType.BY) },
                new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
                1
                );
                READ02:
                while (condition(vw_gen_Tbl.readNextRow("READ02")))
                {
                    if (condition(gen_Tbl_Tbl_Scrty_Level_Ind.notEquals(pnd_Gen_Var_Pnd_Gen_Tbl_Authrty) || gen_Tbl_Tbl_Table_Nme.notEquals(pnd_Gen_Var_Pnd_Gen_Tbl_Name))) //Natural: IF TBL-SCRTY-LEVEL-IND NE #GEN-TBL-AUTHRTY OR TBL-TABLE-NME NE #GEN-TBL-NAME
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Batch_Ind.getValue(pnd_Vars_Pnd_Ctr).equals(pnd_Const_Pnd_Yes_Run)))                                                                //Natural: IF #BATCH-IND ( #CTR ) = #YES-RUN
                    {
                        gen_Tbl_Tbl_Data_Run_Ind.setValue(pnd_Const_Pnd_Success_Ran);                                                                                     //Natural: MOVE #SUCCESS-RAN TO TBL-DATA-RUN-IND
                    }                                                                                                                                                     //Natural: END-IF
                    gen_Tbl_Tbl_Data_Run_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                                     //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO TBL-DATA-RUN-TIME
                    gen_Tbl_Tbl_Updte_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),gen_Tbl_Tbl_Data_Run_Time);                                            //Natural: MOVE EDITED TBL-DATA-RUN-TIME TO TBL-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST )
                    gen_Tbl_Tbl_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                          //Natural: MOVE *INIT-USER TO TBL-UPDTE-OPRTR-CDE
                    gen_Tbl_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                     //Natural: MOVE *DATX TO TBL-UPDTE-DTE
                    vw_gen_Tbl.updateDBRow("READ02");                                                                                                                     //Natural: UPDATE
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=140 PS=60");
    }
}
