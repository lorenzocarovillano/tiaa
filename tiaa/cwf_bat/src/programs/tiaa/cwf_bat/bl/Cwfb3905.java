/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:30:52 PM
**        * FROM NATURAL PROGRAM : Cwfb3905
************************************************************
**        * FILE NAME            : Cwfb3905.java
**        * CLASS NAME           : Cwfb3905
**        * INSTANCE NAME        : Cwfb3905
************************************************************
************************************************************************
* PROGRAM  : CWFB3905
* SYSTEM   : CRPCWF
* TITLE    : EXTRACT PIN LOOKUP
* FUNCTION : FILL PARTICIPANT NAME BASED ON PIN-NBR
*
************************************************************************
*                    M O D I F I C A T I O N S                         *
************************************************************************
* NAME         DATE    DESCRIPTION                                     *
* ----------- -------- ------------------------------------------------*
* J. HARGRAVE 08/21/95 CHANGE TO READ CORE FILE INSTEAD OF PIF FILE    *
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3905 extends BLNatBase
{
    // Data Areas
    private PdaCwfa5372 pdaCwfa5372;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup extract_Record;
    private DbsField extract_Record_Pin_Nbr;
    private DbsField extract_Record_Rqst_Log_Dte_Tme;

    private DbsGroup extract_Record__R_Field_1;
    private DbsField extract_Record_Rqst_Log_Index_Dte;
    private DbsField extract_Record_Rqst_Log_Index_Tme;
    private DbsField extract_Record_Rqst_Log_Oprtr_Cde;
    private DbsField extract_Record_Orgnl_Log_Dte_Tme;
    private DbsField extract_Record_Orgnl_Unit_Cde;
    private DbsField extract_Record_Work_Prcss_Id;

    private DbsGroup extract_Record__R_Field_2;
    private DbsField extract_Record_Work_Actn_Rqstd_Cde;
    private DbsField extract_Record_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField extract_Record_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField extract_Record_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField extract_Record_Unit_Cde;

    private DbsGroup extract_Record__R_Field_3;
    private DbsField extract_Record_Unit_Id_Cde;
    private DbsField extract_Record_Unit_Rgn_Cde;
    private DbsField extract_Record_Unit_Spcl_Dsgntn_Cde;
    private DbsField extract_Record_Unit_Brnch_Group_Cde;
    private DbsField extract_Record_Unit_Updte_Dte_Tme;
    private DbsField extract_Record_Empl_Oprtr_Cde;

    private DbsGroup extract_Record__R_Field_4;
    private DbsField extract_Record_Empl_Racf_Id;
    private DbsField extract_Record_Empl_Sffx_Cde;
    private DbsField extract_Record_Last_Chnge_Dte_Tme;
    private DbsField extract_Record_Last_Chnge_Oprtr_Cde;
    private DbsField extract_Record_Last_Chnge_Unit_Cde;
    private DbsField extract_Record_Step_Id;
    private DbsField extract_Record_Admin_Unit_Cde;
    private DbsField extract_Record_Admin_Status_Cde;
    private DbsField extract_Record_Admin_Status_Updte_Dte_Tme;
    private DbsField extract_Record_Admin_Status_Updte_Oprtr_Cde;
    private DbsField extract_Record_Status_Cde;
    private DbsField extract_Record_Status_Updte_Dte_Tme;
    private DbsField extract_Record_Status_Updte_Oprtr_Cde;
    private DbsField extract_Record_Last_Updte_Dte;
    private DbsField extract_Record_Last_Updte_Dte_Tme;
    private DbsField extract_Record_Last_Updte_Oprtr_Cde;
    private DbsField extract_Record_Crprte_Status_Ind;
    private DbsField extract_Record_Tiaa_Rcvd_Dte;
    private DbsField extract_Record_Effctve_Dte;
    private DbsField extract_Record_Trans_Dte;
    private DbsField extract_Record_Cntrct_Nbr;
    private DbsField extract_Record_Tbl_Calendar_Days;
    private DbsField extract_Record_Tbl_Tiaa_Bsnss_Days;
    private DbsField extract_Record_Return_Doc_Rec_Dte_Tme;
    private DbsField extract_Record_Return_Rcvd_Dte_Tme;
    private DbsField extract_Record_Tbl_Business_Days;
    private DbsField extract_Record_Tbl_Pend_Days;
    private DbsField extract_Record_Tbl_Ext_Pend_Days;
    private DbsField extract_Record_Tbl_Ext_Pend_Bsnss_Days;
    private DbsField extract_Record_Pnd_Partic_Sname;

    private DbsGroup extract_Record__R_Field_5;
    private DbsField extract_Record_Pnd_Folder_Prefix;
    private DbsField extract_Record_Pnd_Folder_Nbr;
    private DbsField extract_Record_Work_List_Ind;

    private DbsGroup extr_Record;
    private DbsField extr_Record_Pin_Nbr;
    private DbsField extr_Record_Rqst_Log_Dte_Tme;

    private DbsGroup extr_Record__R_Field_6;
    private DbsField extr_Record_Rqst_Log_Index_Dte;
    private DbsField extr_Record_Rqst_Log_Index_Tme;
    private DbsField extr_Record_Rqst_Log_Oprtr_Cde;
    private DbsField extr_Record_Orgnl_Log_Dte_Tme;
    private DbsField extr_Record_Orgnl_Unit_Cde;
    private DbsField extr_Record_Work_Prcss_Id;

    private DbsGroup extr_Record__R_Field_7;
    private DbsField extr_Record_Work_Actn_Rqstd_Cde;
    private DbsField extr_Record_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField extr_Record_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField extr_Record_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField extr_Record_Unit_Cde;

    private DbsGroup extr_Record__R_Field_8;
    private DbsField extr_Record_Unit_Id_Cde;
    private DbsField extr_Record_Unit_Rgn_Cde;
    private DbsField extr_Record_Unit_Spcl_Dsgntn_Cde;
    private DbsField extr_Record_Unit_Brnch_Group_Cde;
    private DbsField extr_Record_Unit_Updte_Dte_Tme;
    private DbsField extr_Record_Empl_Oprtr_Cde;

    private DbsGroup extr_Record__R_Field_9;
    private DbsField extr_Record_Empl_Racf_Id;
    private DbsField extr_Record_Empl_Sffx_Cde;
    private DbsField extr_Record_Last_Chnge_Dte_Tme;
    private DbsField extr_Record_Last_Chnge_Oprtr_Cde;
    private DbsField extr_Record_Last_Chnge_Unit_Cde;
    private DbsField extr_Record_Step_Id;
    private DbsField extr_Record_Admin_Unit_Cde;
    private DbsField extr_Record_Admin_Status_Cde;
    private DbsField extr_Record_Admin_Status_Updte_Dte_Tme;
    private DbsField extr_Record_Admin_Status_Updte_Oprtr_Cde;
    private DbsField extr_Record_Status_Cde;
    private DbsField extr_Record_Status_Updte_Dte_Tme;
    private DbsField extr_Record_Status_Updte_Oprtr_Cde;
    private DbsField extr_Record_Last_Updte_Dte;
    private DbsField extr_Record_Last_Updte_Dte_Tme;
    private DbsField extr_Record_Last_Updte_Oprtr_Cde;
    private DbsField extr_Record_Crprte_Status_Ind;
    private DbsField extr_Record_Tiaa_Rcvd_Dte;
    private DbsField extr_Record_Effctve_Dte;
    private DbsField extr_Record_Trans_Dte;
    private DbsField extr_Record_Cntrct_Nbr;
    private DbsField extr_Record_Tbl_Calendar_Days;
    private DbsField extr_Record_Tbl_Tiaa_Bsnss_Days;
    private DbsField extr_Record_Return_Doc_Rec_Dte_Tme;
    private DbsField extr_Record_Return_Rcvd_Dte_Tme;
    private DbsField extr_Record_Tbl_Business_Days;
    private DbsField extr_Record_Tbl_Pend_Days;
    private DbsField extr_Record_Tbl_Ext_Pend_Days;
    private DbsField extr_Record_Tbl_Ext_Pend_Bsnss_Days;
    private DbsField extr_Record_Pnd_Partic_Sname;

    private DbsGroup extr_Record__R_Field_10;
    private DbsField extr_Record_Pnd_Folder_Prefix;
    private DbsField extr_Record_Pnd_Folder_Nbr;
    private DbsField extr_Record_Work_List_Ind;
    private DbsField pnd_Save_Pin;
    private DbsField pnd_Save_Partic_Sname;

    private DbsGroup pnd_Save_Partic_Sname__R_Field_11;
    private DbsField pnd_Save_Partic_Sname_Pnd_Save_Folder_Prefix;
    private DbsField pnd_Save_Partic_Sname_Pnd_Save_Folder_Nbr;
    private DbsField pnd_Ctr;
    private DbsField pnd_Extr_Ctr;
    private DbsField pnd_My_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa5372 = new PdaCwfa5372(localVariables);

        // Local Variables

        extract_Record = localVariables.newGroupInRecord("extract_Record", "EXTRACT-RECORD");
        extract_Record_Pin_Nbr = extract_Record.newFieldInGroup("extract_Record_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        extract_Record_Rqst_Log_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15);

        extract_Record__R_Field_1 = extract_Record.newGroupInGroup("extract_Record__R_Field_1", "REDEFINE", extract_Record_Rqst_Log_Dte_Tme);
        extract_Record_Rqst_Log_Index_Dte = extract_Record__R_Field_1.newFieldInGroup("extract_Record_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 
            8);
        extract_Record_Rqst_Log_Index_Tme = extract_Record__R_Field_1.newFieldInGroup("extract_Record_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", FieldType.STRING, 
            7);
        extract_Record_Rqst_Log_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Orgnl_Log_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", FieldType.STRING, 15);
        extract_Record_Orgnl_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        extract_Record_Work_Prcss_Id = extract_Record.newFieldInGroup("extract_Record_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);

        extract_Record__R_Field_2 = extract_Record.newGroupInGroup("extract_Record__R_Field_2", "REDEFINE", extract_Record_Work_Prcss_Id);
        extract_Record_Work_Actn_Rqstd_Cde = extract_Record__R_Field_2.newFieldInGroup("extract_Record_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", FieldType.STRING, 
            1);
        extract_Record_Work_Lob_Cmpny_Prdct_Cde = extract_Record__R_Field_2.newFieldInGroup("extract_Record_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        extract_Record_Work_Mjr_Bsnss_Prcss_Cde = extract_Record__R_Field_2.newFieldInGroup("extract_Record_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        extract_Record_Work_Spcfc_Bsnss_Prcss_Cde = extract_Record__R_Field_2.newFieldInGroup("extract_Record_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        extract_Record_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);

        extract_Record__R_Field_3 = extract_Record.newGroupInGroup("extract_Record__R_Field_3", "REDEFINE", extract_Record_Unit_Cde);
        extract_Record_Unit_Id_Cde = extract_Record__R_Field_3.newFieldInGroup("extract_Record_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        extract_Record_Unit_Rgn_Cde = extract_Record__R_Field_3.newFieldInGroup("extract_Record_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 1);
        extract_Record_Unit_Spcl_Dsgntn_Cde = extract_Record__R_Field_3.newFieldInGroup("extract_Record_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        extract_Record_Unit_Brnch_Group_Cde = extract_Record__R_Field_3.newFieldInGroup("extract_Record_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        extract_Record_Unit_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", FieldType.TIME);
        extract_Record_Empl_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10);

        extract_Record__R_Field_4 = extract_Record.newGroupInGroup("extract_Record__R_Field_4", "REDEFINE", extract_Record_Empl_Oprtr_Cde);
        extract_Record_Empl_Racf_Id = extract_Record__R_Field_4.newFieldInGroup("extract_Record_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        extract_Record_Empl_Sffx_Cde = extract_Record__R_Field_4.newFieldInGroup("extract_Record_Empl_Sffx_Cde", "EMPL-SFFX-CDE", FieldType.STRING, 2);
        extract_Record_Last_Chnge_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15);
        extract_Record_Last_Chnge_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Last_Chnge_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8);
        extract_Record_Step_Id = extract_Record.newFieldInGroup("extract_Record_Step_Id", "STEP-ID", FieldType.STRING, 6);
        extract_Record_Admin_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        extract_Record_Admin_Status_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4);
        extract_Record_Admin_Status_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME);
        extract_Record_Admin_Status_Updte_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8);
        extract_Record_Status_Cde = extract_Record.newFieldInGroup("extract_Record_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        extract_Record_Status_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME);
        extract_Record_Status_Updte_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Last_Updte_Dte = extract_Record.newFieldInGroup("extract_Record_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 8);
        extract_Record_Last_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME);
        extract_Record_Last_Updte_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Crprte_Status_Ind = extract_Record.newFieldInGroup("extract_Record_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1);
        extract_Record_Tiaa_Rcvd_Dte = extract_Record.newFieldInGroup("extract_Record_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE);
        extract_Record_Effctve_Dte = extract_Record.newFieldInGroup("extract_Record_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE);
        extract_Record_Trans_Dte = extract_Record.newFieldInGroup("extract_Record_Trans_Dte", "TRANS-DTE", FieldType.DATE);
        extract_Record_Cntrct_Nbr = extract_Record.newFieldInGroup("extract_Record_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        extract_Record_Tbl_Calendar_Days = extract_Record.newFieldInGroup("extract_Record_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 
            5);
        extract_Record_Tbl_Tiaa_Bsnss_Days = extract_Record.newFieldInGroup("extract_Record_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 
            5);
        extract_Record_Return_Doc_Rec_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        extract_Record_Return_Rcvd_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        extract_Record_Tbl_Business_Days = extract_Record.newFieldInGroup("extract_Record_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Tbl_Pend_Days = extract_Record.newFieldInGroup("extract_Record_Tbl_Pend_Days", "TBL-PEND-DAYS", FieldType.NUMERIC, 5, 1);
        extract_Record_Tbl_Ext_Pend_Days = extract_Record.newFieldInGroup("extract_Record_Tbl_Ext_Pend_Days", "TBL-EXT-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Tbl_Ext_Pend_Bsnss_Days = extract_Record.newFieldInGroup("extract_Record_Tbl_Ext_Pend_Bsnss_Days", "TBL-EXT-PEND-BSNSS-DAYS", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Partic_Sname = extract_Record.newFieldInGroup("extract_Record_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);

        extract_Record__R_Field_5 = extract_Record.newGroupInGroup("extract_Record__R_Field_5", "REDEFINE", extract_Record_Pnd_Partic_Sname);
        extract_Record_Pnd_Folder_Prefix = extract_Record__R_Field_5.newFieldInGroup("extract_Record_Pnd_Folder_Prefix", "#FOLDER-PREFIX", FieldType.STRING, 
            1);
        extract_Record_Pnd_Folder_Nbr = extract_Record__R_Field_5.newFieldInGroup("extract_Record_Pnd_Folder_Nbr", "#FOLDER-NBR", FieldType.NUMERIC, 6);
        extract_Record_Work_List_Ind = extract_Record.newFieldInGroup("extract_Record_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 3);

        extr_Record = localVariables.newGroupInRecord("extr_Record", "EXTR-RECORD");
        extr_Record_Pin_Nbr = extr_Record.newFieldInGroup("extr_Record_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        extr_Record_Rqst_Log_Dte_Tme = extr_Record.newFieldInGroup("extr_Record_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15);

        extr_Record__R_Field_6 = extr_Record.newGroupInGroup("extr_Record__R_Field_6", "REDEFINE", extr_Record_Rqst_Log_Dte_Tme);
        extr_Record_Rqst_Log_Index_Dte = extr_Record__R_Field_6.newFieldInGroup("extr_Record_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 
            8);
        extr_Record_Rqst_Log_Index_Tme = extr_Record__R_Field_6.newFieldInGroup("extr_Record_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", FieldType.STRING, 
            7);
        extr_Record_Rqst_Log_Oprtr_Cde = extr_Record.newFieldInGroup("extr_Record_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 8);
        extr_Record_Orgnl_Log_Dte_Tme = extr_Record.newFieldInGroup("extr_Record_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", FieldType.STRING, 15);
        extr_Record_Orgnl_Unit_Cde = extr_Record.newFieldInGroup("extr_Record_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        extr_Record_Work_Prcss_Id = extr_Record.newFieldInGroup("extr_Record_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);

        extr_Record__R_Field_7 = extr_Record.newGroupInGroup("extr_Record__R_Field_7", "REDEFINE", extr_Record_Work_Prcss_Id);
        extr_Record_Work_Actn_Rqstd_Cde = extr_Record__R_Field_7.newFieldInGroup("extr_Record_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", FieldType.STRING, 
            1);
        extr_Record_Work_Lob_Cmpny_Prdct_Cde = extr_Record__R_Field_7.newFieldInGroup("extr_Record_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        extr_Record_Work_Mjr_Bsnss_Prcss_Cde = extr_Record__R_Field_7.newFieldInGroup("extr_Record_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        extr_Record_Work_Spcfc_Bsnss_Prcss_Cde = extr_Record__R_Field_7.newFieldInGroup("extr_Record_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        extr_Record_Unit_Cde = extr_Record.newFieldInGroup("extr_Record_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);

        extr_Record__R_Field_8 = extr_Record.newGroupInGroup("extr_Record__R_Field_8", "REDEFINE", extr_Record_Unit_Cde);
        extr_Record_Unit_Id_Cde = extr_Record__R_Field_8.newFieldInGroup("extr_Record_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        extr_Record_Unit_Rgn_Cde = extr_Record__R_Field_8.newFieldInGroup("extr_Record_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 1);
        extr_Record_Unit_Spcl_Dsgntn_Cde = extr_Record__R_Field_8.newFieldInGroup("extr_Record_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", FieldType.STRING, 
            1);
        extr_Record_Unit_Brnch_Group_Cde = extr_Record__R_Field_8.newFieldInGroup("extr_Record_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", FieldType.STRING, 
            1);
        extr_Record_Unit_Updte_Dte_Tme = extr_Record.newFieldInGroup("extr_Record_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", FieldType.TIME);
        extr_Record_Empl_Oprtr_Cde = extr_Record.newFieldInGroup("extr_Record_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10);

        extr_Record__R_Field_9 = extr_Record.newGroupInGroup("extr_Record__R_Field_9", "REDEFINE", extr_Record_Empl_Oprtr_Cde);
        extr_Record_Empl_Racf_Id = extr_Record__R_Field_9.newFieldInGroup("extr_Record_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        extr_Record_Empl_Sffx_Cde = extr_Record__R_Field_9.newFieldInGroup("extr_Record_Empl_Sffx_Cde", "EMPL-SFFX-CDE", FieldType.STRING, 2);
        extr_Record_Last_Chnge_Dte_Tme = extr_Record.newFieldInGroup("extr_Record_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);
        extr_Record_Last_Chnge_Oprtr_Cde = extr_Record.newFieldInGroup("extr_Record_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8);
        extr_Record_Last_Chnge_Unit_Cde = extr_Record.newFieldInGroup("extr_Record_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8);
        extr_Record_Step_Id = extr_Record.newFieldInGroup("extr_Record_Step_Id", "STEP-ID", FieldType.STRING, 6);
        extr_Record_Admin_Unit_Cde = extr_Record.newFieldInGroup("extr_Record_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        extr_Record_Admin_Status_Cde = extr_Record.newFieldInGroup("extr_Record_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4);
        extr_Record_Admin_Status_Updte_Dte_Tme = extr_Record.newFieldInGroup("extr_Record_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME);
        extr_Record_Admin_Status_Updte_Oprtr_Cde = extr_Record.newFieldInGroup("extr_Record_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8);
        extr_Record_Status_Cde = extr_Record.newFieldInGroup("extr_Record_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        extr_Record_Status_Updte_Dte_Tme = extr_Record.newFieldInGroup("extr_Record_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME);
        extr_Record_Status_Updte_Oprtr_Cde = extr_Record.newFieldInGroup("extr_Record_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        extr_Record_Last_Updte_Dte = extr_Record.newFieldInGroup("extr_Record_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 8);
        extr_Record_Last_Updte_Dte_Tme = extr_Record.newFieldInGroup("extr_Record_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME);
        extr_Record_Last_Updte_Oprtr_Cde = extr_Record.newFieldInGroup("extr_Record_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 8);
        extr_Record_Crprte_Status_Ind = extr_Record.newFieldInGroup("extr_Record_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1);
        extr_Record_Tiaa_Rcvd_Dte = extr_Record.newFieldInGroup("extr_Record_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE);
        extr_Record_Effctve_Dte = extr_Record.newFieldInGroup("extr_Record_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE);
        extr_Record_Trans_Dte = extr_Record.newFieldInGroup("extr_Record_Trans_Dte", "TRANS-DTE", FieldType.DATE);
        extr_Record_Cntrct_Nbr = extr_Record.newFieldInGroup("extr_Record_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        extr_Record_Tbl_Calendar_Days = extr_Record.newFieldInGroup("extr_Record_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 5);
        extr_Record_Tbl_Tiaa_Bsnss_Days = extr_Record.newFieldInGroup("extr_Record_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 5);
        extr_Record_Return_Doc_Rec_Dte_Tme = extr_Record.newFieldInGroup("extr_Record_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        extr_Record_Return_Rcvd_Dte_Tme = extr_Record.newFieldInGroup("extr_Record_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        extr_Record_Tbl_Business_Days = extr_Record.newFieldInGroup("extr_Record_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.NUMERIC, 5, 1);
        extr_Record_Tbl_Pend_Days = extr_Record.newFieldInGroup("extr_Record_Tbl_Pend_Days", "TBL-PEND-DAYS", FieldType.NUMERIC, 5, 1);
        extr_Record_Tbl_Ext_Pend_Days = extr_Record.newFieldInGroup("extr_Record_Tbl_Ext_Pend_Days", "TBL-EXT-PEND-DAYS", FieldType.NUMERIC, 5, 1);
        extr_Record_Tbl_Ext_Pend_Bsnss_Days = extr_Record.newFieldInGroup("extr_Record_Tbl_Ext_Pend_Bsnss_Days", "TBL-EXT-PEND-BSNSS-DAYS", FieldType.NUMERIC, 
            5, 1);
        extr_Record_Pnd_Partic_Sname = extr_Record.newFieldInGroup("extr_Record_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);

        extr_Record__R_Field_10 = extr_Record.newGroupInGroup("extr_Record__R_Field_10", "REDEFINE", extr_Record_Pnd_Partic_Sname);
        extr_Record_Pnd_Folder_Prefix = extr_Record__R_Field_10.newFieldInGroup("extr_Record_Pnd_Folder_Prefix", "#FOLDER-PREFIX", FieldType.STRING, 1);
        extr_Record_Pnd_Folder_Nbr = extr_Record__R_Field_10.newFieldInGroup("extr_Record_Pnd_Folder_Nbr", "#FOLDER-NBR", FieldType.NUMERIC, 6);
        extr_Record_Work_List_Ind = extr_Record.newFieldInGroup("extr_Record_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 3);
        pnd_Save_Pin = localVariables.newFieldInRecord("pnd_Save_Pin", "#SAVE-PIN", FieldType.NUMERIC, 12);
        pnd_Save_Partic_Sname = localVariables.newFieldInRecord("pnd_Save_Partic_Sname", "#SAVE-PARTIC-SNAME", FieldType.STRING, 7);

        pnd_Save_Partic_Sname__R_Field_11 = localVariables.newGroupInRecord("pnd_Save_Partic_Sname__R_Field_11", "REDEFINE", pnd_Save_Partic_Sname);
        pnd_Save_Partic_Sname_Pnd_Save_Folder_Prefix = pnd_Save_Partic_Sname__R_Field_11.newFieldInGroup("pnd_Save_Partic_Sname_Pnd_Save_Folder_Prefix", 
            "#SAVE-FOLDER-PREFIX", FieldType.STRING, 1);
        pnd_Save_Partic_Sname_Pnd_Save_Folder_Nbr = pnd_Save_Partic_Sname__R_Field_11.newFieldInGroup("pnd_Save_Partic_Sname_Pnd_Save_Folder_Nbr", "#SAVE-FOLDER-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Extr_Ctr = localVariables.newFieldInRecord("pnd_Extr_Ctr", "#EXTR-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_My_Count = localVariables.newFieldInRecord("pnd_My_Count", "#MY-COUNT", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3905() throws Exception
    {
        super("Cwfb3905");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* ***********************************************************************
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 EXTRACT-RECORD
        while (condition(getWorkFiles().read(1, extract_Record)))
        {
            if (condition(extract_Record_Pin_Nbr.notEquals(pnd_Save_Pin)))                                                                                                //Natural: IF EXTRACT-RECORD.PIN-NBR NE #SAVE-PIN
            {
                pnd_Save_Pin.setValue(extract_Record_Pin_Nbr);                                                                                                            //Natural: MOVE EXTRACT-RECORD.PIN-NBR TO #SAVE-PIN
                short decideConditionsMet173 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #SAVE-PIN;//Natural: VALUE 0
                if (condition((pnd_Save_Pin.equals(0))))
                {
                    decideConditionsMet173++;
                    ignore();
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Save_Partic_Sname.reset();                                                                                                                        //Natural: RESET #SAVE-PARTIC-SNAME
                    pdaCwfa5372.getCwfa5372_Pnd_Pin_Key().setValue(extract_Record_Pin_Nbr);                                                                               //Natural: ASSIGN CWFA5372.#PIN-KEY := EXTRACT-RECORD.PIN-NBR
                    //*        FETCH RETURN 'MDMP0011'  /* MQ OPEN
                    //*        CALLNAT 'CWFN5372'
                    //*                 CWFA5372.#PIN-KEY
                    //*                 CWFA5372.#PASS-NAME
                    //*                 CWFA5372.#PASS-SSN
                    //*                 CWFA5372.#PASS-TLC
                    //*                 CWFA5372.#PASS-DOB
                    //*                 CWFA5372.#PASS-FOUND
                    //*        FETCH RETURN 'MDMP0012'  /* MQ CLOSE
                    if (condition(pdaCwfa5372.getCwfa5372_Pnd_Pass_Found().getBoolean()))                                                                                 //Natural: IF #PASS-FOUND
                    {
                        pnd_Save_Partic_Sname.setValue(pdaCwfa5372.getCwfa5372_Pnd_Pass_Name());                                                                          //Natural: MOVE CWFA5372.#PASS-NAME TO #SAVE-PARTIC-SNAME
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CTR
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Save_Partic_Sname.greater(" ")))                                                                                                            //Natural: IF #SAVE-PARTIC-SNAME GT ' '
            {
                extract_Record_Pnd_Partic_Sname.setValue(pnd_Save_Partic_Sname);                                                                                          //Natural: MOVE #SAVE-PARTIC-SNAME TO EXTRACT-RECORD.#PARTIC-SNAME
            }                                                                                                                                                             //Natural: END-IF
            extr_Record.setValuesByName(extract_Record);                                                                                                                  //Natural: MOVE BY NAME EXTRACT-RECORD TO EXTR-RECORD
            getWorkFiles().write(5, false, extr_Record);                                                                                                                  //Natural: WRITE WORK FILE 5 EXTR-RECORD
            pnd_Extr_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #EXTR-CTR
            extr_Record.reset();                                                                                                                                          //Natural: RESET EXTR-RECORD
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }

    //
}
