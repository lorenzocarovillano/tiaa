/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:55:57 PM
**        * FROM NATURAL PROGRAM : Mcsp3800
************************************************************
**        * FILE NAME            : Mcsp3800.java
**        * CLASS NAME           : Mcsp3800
**        * INSTANCE NAME        : Mcsp3800
************************************************************
************************************************************************
* PROGRAM  : MCSP3800
* TITLE    : ACCESS FULFILLMENT RECORDS FROM THE CWF-ATI-FULFILLMENT
* FUNCTION : DISPLAY ATI-FULFILLMENT RECORDS ON 192 IN WPID ORDER
* KB 0698  : MODIFIED TO PASS DATA TO SUBPROGRAMS - THE PURPOSE
*            WAS TO GIVE TOTALS FOR ALL ATI'S FOR EACH DAY.
*            THE NEW SUMMARY PROGRAM IS MCSP3899
*
* NH 07/06/99 : ADDITION OF CDMS PROCESSING.
* JG 09/22/99 : ADDITION OF VOAD PROCESSING.
* JG 03/15/00 : ADDITION OF QSUM PROCESSING (GENERIC)
* JG 01/09/01 : CHANGED     ABR  PROCESSING TO GENERIC.
* GH 06/05/01 : ADDITION OF IPI  PROCESSING (GENERIC)
*             : ALSO INCREASE THE ARRAY ELEMENTS TO 30.
* GH 12/17/01 : ADDITION OF TAX  PROCESSING (GENERIC)
* GH 05/14/02 : ADDITION OF PLI  PROCESSING (GENERIC)
* GH 04/14/04 : ADDITION OF ADV  PROCESSING (GENERIC)
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsp3800 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Parms;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Date;
    private DbsField cwf_Support_Tbl_Pnd_Prior_Date_Parm;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_3;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_B;
    private DbsField pnd_Cc;
    private DbsField pnd_Field;
    private DbsField pnd_Title_Literal;
    private DbsField pnd_Input_Dte_Tme;
    private DbsField pnd_Orig_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Last_Page_Flag;
    private DbsField pnd_Input_Source_Id;
    private DbsField pnd_Input_Date_D;
    private DbsField pnd_Last_Processed_Date_A;
    private DbsField pnd_Last_Processed_Date;
    private DbsField pnd_Last_Processed_Date_Mm;
    private DbsField pnd_New_Mm;
    private DbsField pnd_Report_Ran;
    private DbsField pnd_Successful;
    private DbsField pnd_Prior_Date_Parm_Used;

    private DbsGroup pnd_Input_Data;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Data_Pnd_W_Inx;

    private DbsGroup pnd_Input_Data_Pnd_Ati_Total_Table;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Program_Name;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Open;
    private DbsField pnd_Input_Data_Pnd_W_Ati_In_Progress;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Push_Successful;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Manual;
    private DbsField pnd_Input_Data_Pnd_W_Ati_Successful;

    private DbsGroup pnd_Ati_Grand_Totals;
    private DbsField pnd_Ati_Grand_Totals_Pnd_T_Ati_Open;
    private DbsField pnd_Ati_Grand_Totals_Pnd_T_Ati_In_Progress;
    private DbsField pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Unsuccessful;
    private DbsField pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Successful;
    private DbsField pnd_Ati_Grand_Totals_Pnd_T_Ati_Manual;
    private DbsField pnd_Ati_Grand_Totals_Pnd_T_Ati_Successful;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Regular_Run_Parms = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Parms", "#REGULAR-RUN-PARMS", 
            FieldType.STRING, 30);

        cwf_Support_Tbl__R_Field_2 = cwf_Support_Tbl__R_Field_1.newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Pnd_Regular_Run_Parms);
        cwf_Support_Tbl_Pnd_Regular_Run_Date = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Date", "#REGULAR-RUN-DATE", 
            FieldType.STRING, 8);
        cwf_Support_Tbl_Pnd_Prior_Date_Parm = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Prior_Date_Parm", "#PRIOR-DATE-PARM", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_3", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 3);
        pnd_Cc = localVariables.newFieldInRecord("pnd_Cc", "#CC", FieldType.NUMERIC, 3);
        pnd_Field = localVariables.newFieldInRecord("pnd_Field", "#FIELD", FieldType.STRING, 30);
        pnd_Title_Literal = localVariables.newFieldInRecord("pnd_Title_Literal", "#TITLE-LITERAL", FieldType.STRING, 54);
        pnd_Input_Dte_Tme = localVariables.newFieldInRecord("pnd_Input_Dte_Tme", "#INPUT-DTE-TME", FieldType.TIME);
        pnd_Orig_Date = localVariables.newFieldInRecord("pnd_Orig_Date", "#ORIG-DATE", FieldType.DATE);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 8);
        pnd_Last_Page_Flag = localVariables.newFieldInRecord("pnd_Last_Page_Flag", "#LAST-PAGE-FLAG", FieldType.STRING, 1);
        pnd_Input_Source_Id = localVariables.newFieldInRecord("pnd_Input_Source_Id", "#INPUT-SOURCE-ID", FieldType.STRING, 6);
        pnd_Input_Date_D = localVariables.newFieldInRecord("pnd_Input_Date_D", "#INPUT-DATE-D", FieldType.DATE);
        pnd_Last_Processed_Date_A = localVariables.newFieldInRecord("pnd_Last_Processed_Date_A", "#LAST-PROCESSED-DATE-A", FieldType.STRING, 8);
        pnd_Last_Processed_Date = localVariables.newFieldInRecord("pnd_Last_Processed_Date", "#LAST-PROCESSED-DATE", FieldType.DATE);
        pnd_Last_Processed_Date_Mm = localVariables.newFieldInRecord("pnd_Last_Processed_Date_Mm", "#LAST-PROCESSED-DATE-MM", FieldType.STRING, 2);
        pnd_New_Mm = localVariables.newFieldInRecord("pnd_New_Mm", "#NEW-MM", FieldType.STRING, 2);
        pnd_Report_Ran = localVariables.newFieldInRecord("pnd_Report_Ran", "#REPORT-RAN", FieldType.BOOLEAN, 1);
        pnd_Successful = localVariables.newFieldInRecord("pnd_Successful", "#SUCCESSFUL", FieldType.BOOLEAN, 1);
        pnd_Prior_Date_Parm_Used = localVariables.newFieldInRecord("pnd_Prior_Date_Parm_Used", "#PRIOR-DATE-PARM-USED", FieldType.BOOLEAN, 1);

        pnd_Input_Data = localVariables.newGroupInRecord("pnd_Input_Data", "#INPUT-DATA");
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Inx = pnd_Input_Data.newFieldInGroup("pnd_Input_Data_Pnd_W_Inx", "#W-INX", FieldType.NUMERIC, 2);

        pnd_Input_Data_Pnd_Ati_Total_Table = pnd_Input_Data.newGroupArrayInGroup("pnd_Input_Data_Pnd_Ati_Total_Table", "#ATI-TOTAL-TABLE", new DbsArrayController(1, 
            30));
        pnd_Input_Data_Pnd_W_Ati_Program_Name = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Program_Name", "#W-ATI-PROGRAM-NAME", 
            FieldType.STRING, 8);
        pnd_Input_Data_Pnd_W_Ati_Open = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Open", "#W-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_In_Progress = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_In_Progress", "#W-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful", 
            "#W-ATI-PUSH-UNSUCCESSFUL", FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Push_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Push_Successful", "#W-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Input_Data_Pnd_W_Ati_Manual = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Manual", "#W-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Input_Data_Pnd_W_Ati_Successful = pnd_Input_Data_Pnd_Ati_Total_Table.newFieldInGroup("pnd_Input_Data_Pnd_W_Ati_Successful", "#W-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);

        pnd_Ati_Grand_Totals = localVariables.newGroupInRecord("pnd_Ati_Grand_Totals", "#ATI-GRAND-TOTALS");
        pnd_Ati_Grand_Totals_Pnd_T_Ati_Open = pnd_Ati_Grand_Totals.newFieldInGroup("pnd_Ati_Grand_Totals_Pnd_T_Ati_Open", "#T-ATI-OPEN", FieldType.NUMERIC, 
            7);
        pnd_Ati_Grand_Totals_Pnd_T_Ati_In_Progress = pnd_Ati_Grand_Totals.newFieldInGroup("pnd_Ati_Grand_Totals_Pnd_T_Ati_In_Progress", "#T-ATI-IN-PROGRESS", 
            FieldType.NUMERIC, 7);
        pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Unsuccessful = pnd_Ati_Grand_Totals.newFieldInGroup("pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Unsuccessful", "#T-ATI-PUSH-UNSUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Successful = pnd_Ati_Grand_Totals.newFieldInGroup("pnd_Ati_Grand_Totals_Pnd_T_Ati_Push_Successful", "#T-ATI-PUSH-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        pnd_Ati_Grand_Totals_Pnd_T_Ati_Manual = pnd_Ati_Grand_Totals.newFieldInGroup("pnd_Ati_Grand_Totals_Pnd_T_Ati_Manual", "#T-ATI-MANUAL", FieldType.NUMERIC, 
            7);
        pnd_Ati_Grand_Totals_Pnd_T_Ati_Successful = pnd_Ati_Grand_Totals.newFieldInGroup("pnd_Ati_Grand_Totals_Pnd_T_Ati_Successful", "#T-ATI-SUCCESSFUL", 
            FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Report_Ran.setInitialValue(false);
        pnd_Successful.setInitialValue(false);
        pnd_Prior_Date_Parm_Used.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Mcsp3800() throws Exception
    {
        super("Mcsp3800");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  ----------------------------------------------------------------------
        //*   MAIN PROGRAM
        //*  ----------------------------------------------------------------------
        //*  07/06                                                                                                                                                        //Natural: FORMAT PS = 60 LS = 133 ZP = OFF;//Natural: FORMAT ( 1 ) PS = 60 LS = 133 ZP = OFF
        //*  JG990922
        //*  GALINA03152000
        //*  06/25/01 GH
        //*  12/17/01 GH
        //*  05/14/02 GH
        //*  04/14/04 GH
        //*  ---                                                                                                                                                          //Natural: FORMAT ( 15 ) PS = 60 LS = 133 ZP = OFF
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(1).setValue("SSSF    ");                                                                                           //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 01 ) := 'SSSF    '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(2).setValue("FFT     ");                                                                                           //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 02 ) := 'FFT     '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(3).setValue("ILLU    ");                                                                                           //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 03 ) := 'ILLU    '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(4).setValue("ADRS    ");                                                                                           //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 04 ) := 'ADRS    '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(5).setValue("ABRS    ");                                                                                           //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 05 ) := 'ABRS    '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(6).setValue("AWDP    ");                                                                                           //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 06 ) := 'AWDP    '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(7).setValue("IOP     ");                                                                                           //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 07 ) := 'IOP     '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(8).setValue("POST    ");                                                                                           //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 08 ) := 'POST    '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(9).setValue("VOAD    ");                                                                                           //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 09 ) := 'VOAD    '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(10).setValue("QSUM    ");                                                                                          //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 10 ) := 'QSUM    '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(11).setValue("IPI     ");                                                                                          //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 11 ) := 'IPI     '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(12).setValue("TAX     ");                                                                                          //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 12 ) := 'TAX     '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(13).setValue("PLI     ");                                                                                          //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 13 ) := 'PLI     '
        pnd_Input_Data_Pnd_W_Ati_Program_Name.getValue(14).setValue("ADV     ");                                                                                          //Natural: ASSIGN #W-ATI-PROGRAM-NAME ( 14 ) := 'ADV     '
        //*  ---
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("ATI-PROCESSING-DTE");                                                                                               //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME := 'ATI-PROCESSING-DTE'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("DAILY");                                                                                                            //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-KEY-FIELD := 'DAILY'
        //*  ---
        vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "FIND_TBL",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        FIND_TBL:
        while (condition(vw_cwf_Support_Tbl.readNextRow("FIND_TBL")))
        {
            vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
            //*  #INPUT-DATA := CWF-SUPPORT-TBL.#REGULAR-RUN-PARMS
            pnd_Input_Data_Pnd_Input_Date.setValue(cwf_Support_Tbl_Pnd_Regular_Run_Date);                                                                                 //Natural: ASSIGN #INPUT-DATE := CWF-SUPPORT-TBL.#REGULAR-RUN-DATE
            pnd_Isn_Tbl.setValue(vw_cwf_Support_Tbl.getAstISN("FIND_TBL"));                                                                                               //Natural: ASSIGN #ISN-TBL := *ISN ( FIND-TBL. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Input_Data_Pnd_Input_Date.equals(" ")))                                                                                                         //Natural: IF #INPUT-DATE = ' '
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,"*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***");                                                          //Natural: WRITE ( 1 ) /// '*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***'
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Input_Data_Pnd_W_Ati_Open.getValue("*").reset();                                                                                                          //Natural: RESET #W-ATI-OPEN ( * ) #W-ATI-IN-PROGRESS ( * ) #W-ATI-PUSH-UNSUCCESSFUL ( * ) #W-ATI-PUSH-SUCCESSFUL ( * ) #W-ATI-MANUAL ( * ) #W-ATI-SUCCESSFUL ( * )
            pnd_Input_Data_Pnd_W_Ati_In_Progress.getValue("*").reset();
            pnd_Input_Data_Pnd_W_Ati_Push_Unsuccessful.getValue("*").reset();
            pnd_Input_Data_Pnd_W_Ati_Push_Successful.getValue("*").reset();
            pnd_Input_Data_Pnd_W_Ati_Manual.getValue("*").reset();
            pnd_Input_Data_Pnd_W_Ati_Successful.getValue("*").reset();
            pnd_Input_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DATE-D ( EM = YYYYMMDD )
            pnd_Input_Dte_Tme.setValue(pnd_Input_Date_D);                                                                                                                 //Natural: MOVE #INPUT-DATE-D TO #INPUT-DTE-TME
            if (condition(pnd_Input_Date_D.greater(Global.getDATX())))                                                                                                    //Natural: IF #INPUT-DATE-D GT *DATX
            {
                if (condition(pnd_Report_Ran.getBoolean()))                                                                                                               //Natural: IF #REPORT-RAN
                {
                    pnd_Successful.setValue(true);                                                                                                                        //Natural: ASSIGN #SUCCESSFUL := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Input_Date_D.equals(Global.getDATX())))                                                                                                 //Natural: IF #INPUT-DATE-D = *DATX
                {
                    if (condition(Global.getTIME().greaterOrEqual("00:00:00.0") && Global.getTIME().lessOrEqual("06:59:59.9")))                                           //Natural: IF *TIME = '00:00:00.0' THRU '06:59:59.9'
                    {
                        if (condition(pnd_Report_Ran.getBoolean()))                                                                                                       //Natural: IF #REPORT-RAN
                        {
                            pnd_Successful.setValue(true);                                                                                                                //Natural: ASSIGN #SUCCESSFUL := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  ----------------------------------------------------------------------
            //*   FETCH PROGRAMS TO PRINT VARIOUS ATI'S
            //*  ----------------------------------------------------------------------
            pnd_Input_Data_Pnd_W_Inx.setValue(1);                                                                                                                         //Natural: ASSIGN #W-INX := 1
            //*  SSSS - OPEN
            DbsUtil.callnat(Mcsn3801.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3801' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  SSSS - PROCESSED
            DbsUtil.callnat(Mcsn3802.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3802' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Input_Data_Pnd_W_Inx.setValue(2);                                                                                                                         //Natural: ASSIGN #W-INX := 2
            //*  FFT  - OPEN
            DbsUtil.callnat(Mcsn3803.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3803' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  FFT  - PROCESSED
            DbsUtil.callnat(Mcsn3804.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3804' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Input_Data_Pnd_W_Inx.setValue(3);                                                                                                                         //Natural: ASSIGN #W-INX := 3
            //*  ILLU - OPEN
            DbsUtil.callnat(Mcsn3806.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3806' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  ILLU - PROCESSED
            DbsUtil.callnat(Mcsn3807.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3807' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Input_Data_Pnd_W_Inx.setValue(4);                                                                                                                         //Natural: ASSIGN #W-INX := 4
            //*  ADRS - OPEN
            DbsUtil.callnat(Mcsn3808.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3808' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  ADRS - PROCESSED - SUCCESSFUL
            DbsUtil.callnat(Mcsn3809.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3809' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  ADRS - PROCESSED - FAILED
            DbsUtil.callnat(Mcsn3812.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3812' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  ADRS - ERROR SUMMARY
            DbsUtil.callnat(Mcsn3811.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3811' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Input_Data_Pnd_W_Inx.setValue(5);                                                                                                                         //Natural: ASSIGN #W-INX := 5
            //*  ABR  - OPEN       (GENERIC)
            DbsUtil.callnat(Mcsn3832.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3832' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  ABR  - PROCESSED  (GENERIC)
            DbsUtil.callnat(Mcsn3833.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3833' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  CALLNAT 'MCSN3821' #INPUT-DATA
            //*  CALLNAT 'MCSN3820' #INPUT-DATA
            //*  CALLNAT 'MCSN3822' #INPUT-DATA
            pnd_Input_Data_Pnd_W_Inx.setValue(6);                                                                                                                         //Natural: ASSIGN #W-INX := 6
            //*  AWDP - OPEN
            DbsUtil.callnat(Mcsn3814.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3814' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  AWDP - PROCESSED - SUCCESSFUL
            DbsUtil.callnat(Mcsn3815.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3815' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  AWDP - PROCESSED - FAILED
            DbsUtil.callnat(Mcsn3816.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3816' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  AWDP - PROCESSED
            DbsUtil.callnat(Mcsn3818.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3818' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Input_Data_Pnd_W_Inx.setValue(7);                                                                                                                         //Natural: ASSIGN #W-INX := 7
            //*  IOP  - OPEN
            DbsUtil.callnat(Mcsn3824.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3824' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  IOP  - PROCESSED - SUCCESSFUL
            DbsUtil.callnat(Mcsn3823.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3823' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  IOP  - PROCESSED - FAILED
            DbsUtil.callnat(Mcsn3825.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3825' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Input_Data_Pnd_W_Inx.setValue(8);                                                                                                                         //Natural: ASSIGN #W-INX := 8
            //*  POST - OPEN
            DbsUtil.callnat(Mcsn3826.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3826' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  POST - PROCESSED
            DbsUtil.callnat(Mcsn3827.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3827' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Input_Data_Pnd_W_Inx.setValue(9);                                                                                                                         //Natural: ASSIGN #W-INX := 9
            //*  VOAD - OPEN
            DbsUtil.callnat(Mcsn3828.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3828' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  VOAD - PROCESSED
            DbsUtil.callnat(Mcsn3829.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3829' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Input_Data_Pnd_W_Inx.setValue(10);                                                                                                                        //Natural: ASSIGN #W-INX := 10
            //*  QSUM - OPEN
            DbsUtil.callnat(Mcsn3830.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3830' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  QSUM - PROCESSED
            DbsUtil.callnat(Mcsn3831.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3831' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Input_Data_Pnd_W_Inx.setValue(11);                                                                                                                        //Natural: ASSIGN #W-INX := 11
            //*  IPI  - OPEN
            DbsUtil.callnat(Mcsn3834.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3834' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  IPI  - PROCESSED
            DbsUtil.callnat(Mcsn3835.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3835' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Input_Data_Pnd_W_Inx.setValue(12);                                                                                                                        //Natural: ASSIGN #W-INX := 12
            //*  TAX  - OPEN
            DbsUtil.callnat(Mcsn3836.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3836' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  TAX  - PROCESSED
            DbsUtil.callnat(Mcsn3837.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3837' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Input_Data_Pnd_W_Inx.setValue(13);                                                                                                                        //Natural: ASSIGN #W-INX := 13
            //*  PLI  - OPEN
            DbsUtil.callnat(Mcsn3838.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3838' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  PLI  - PROCESSED
            DbsUtil.callnat(Mcsn3839.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3839' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Input_Data_Pnd_W_Inx.setValue(14);                                                                                                                        //Natural: ASSIGN #W-INX := 14
            //*  ADV  - OPEN
            DbsUtil.callnat(Mcsn3840.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3840' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  ADV  - PROCESSED
            DbsUtil.callnat(Mcsn3841.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3841' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  ----------------------------------------------------------------------
            //*  UPDATE TABLE FILE WITH THE NEXT PROCESSING DATE.
            //*  ----------------------------------------------------------------------
            //*  LOOKUP NEXT BUSINESS DAY
            DbsUtil.callnat(Mcsn3899.class , getCurrentProcessState(), pnd_Input_Data);                                                                                   //Natural: CALLNAT 'MCSN3899' #INPUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Report_Ran.setValue(true);                                                                                                                                //Natural: ASSIGN #REPORT-RAN := TRUE
            pnd_Last_Processed_Date_A.setValue(pnd_Input_Data_Pnd_Input_Date);                                                                                            //Natural: ASSIGN #LAST-PROCESSED-DATE-A := #INPUT-DATE
            pnd_Input_Date_D.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #INPUT-DATE-D
            pnd_Input_Data_Pnd_Input_Date.setValueEdited(pnd_Input_Date_D,new ReportEditMask("YYYYMMDD"));                                                                //Natural: MOVE EDITED #INPUT-DATE-D ( EM = YYYYMMDD ) TO #INPUT-DATE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  ---
        //*  REPORT PROGRAMS WERE EXECUTED
        if (condition(pnd_Successful.getBoolean()))                                                                                                                       //Natural: IF #SUCCESSFUL
        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-NEXT-PROCESSING-DATE
            sub_Update_Next_Processing_Date();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-NEXT-PROCESSING-DATE
    }
    private void sub_Update_Next_Processing_Date() throws Exception                                                                                                       //Natural: UPDATE-NEXT-PROCESSING-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  UPDATE TABLE FILE WITH THE NEXT PROCESSING DATE; ALSO IF FIRST
        //*  WORKING DAY OF THE MONTH, UPDATE THE REGULAR TIME PARAMETERS
        //*  WITH AN OVERRIDE TIME PARAMETERS IF IT IS ENTERED.
        pnd_Last_Processed_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Last_Processed_Date_A);                                                                 //Natural: MOVE EDITED #LAST-PROCESSED-DATE-A TO #LAST-PROCESSED-DATE ( EM = YYYYMMDD )
        pnd_Last_Processed_Date_Mm.setValueEdited(pnd_Last_Processed_Date,new ReportEditMask("MM"));                                                                      //Natural: MOVE EDITED #LAST-PROCESSED-DATE ( EM = MM ) TO #LAST-PROCESSED-DATE-MM
        L_GET:                                                                                                                                                            //Natural: GET CWF-SUPPORT-TBL #ISN-TBL
        vw_cwf_Support_Tbl.readByID(pnd_Isn_Tbl.getLong(), "L_GET");
        DbsUtil.callnat(Mcsn3810.class , getCurrentProcessState(), pnd_Last_Processed_Date, pnd_Next_Processing_Date);                                                    //Natural: CALLNAT 'MCSN3810' #LAST-PROCESSED-DATE #NEXT-PROCESSING-DATE
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Next_Processing_Date.greater(getZero())))                                                                                                       //Natural: IF #NEXT-PROCESSING-DATE GT 0
        {
            cwf_Support_Tbl_Pnd_Regular_Run_Date.setValueEdited(pnd_Next_Processing_Date,new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED #NEXT-PROCESSING-DATE ( EM = YYYYMMDD ) TO CWF-SUPPORT-TBL.#REGULAR-RUN-DATE
            cwf_Support_Tbl_Pnd_Prior_Date_Parm.setValueEdited(pnd_Last_Processed_Date,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED #LAST-PROCESSED-DATE ( EM = YYYYMMDD ) TO CWF-SUPPORT-TBL.#PRIOR-DATE-PARM
        }                                                                                                                                                                 //Natural: END-IF
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setValue("BATCH");                                                                                                            //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE := 'BATCH'
        cwf_Support_Tbl_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                         //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-UPDTE-DTE := *DATX
        vw_cwf_Support_Tbl.updateDBRow("L_GET");                                                                                                                          //Natural: UPDATE ( L-GET. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=OFF");
        Global.format(1, "PS=60 LS=133 ZP=OFF");
        Global.format(15, "PS=60 LS=133 ZP=OFF");
    }
}
