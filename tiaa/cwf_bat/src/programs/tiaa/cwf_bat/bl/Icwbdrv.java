/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:41:01 PM
**        * FROM NATURAL PROGRAM : Icwbdrv
************************************************************
**        * FILE NAME            : Icwbdrv.java
**        * CLASS NAME           : Icwbdrv
**        * INSTANCE NAME        : Icwbdrv
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: INSTITUTIONAL REPORT DRIV
**SAG SYSTEM:
************************************************************************
* PROGRAM  : ICWBDRV
* SYSTEM   :
* TITLE    : INSTITUTIONAL REPORT DRIV
* GENERATED: JUL 09,97 AT 03:39 PM
* FUNCTION : THIS IS THE INSTITUTIONAL REPORTING DRIVER PROGRAM.
*          | IT CREATES RECORDS ON THE SUPPORT TABLE TO INDICATE
*          | THE FREQUENCY TYPE OF REPORT THAT SHOULD BE RUN TODAY.
*          | (I.E. DAILY,WEEKLY,MONTHLY,QUARTERLY).  IT WRITES RECOR
*          | DS TO THE SUPPORT TABLE FOR EACH REPORT TYPE TO BE RUN
*          | AS WELL DATE RANGES TO USE.
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwbdrv extends BLNatBase
{
    // Data Areas
    private LdaZdkeylda ldaZdkeylda;
    private PdaCdenvira pdaCdenvira;
    private LdaCddialda ldaCddialda;
    private PdaIcwpda_M pdaIcwpda_M;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Daily_Date;
    private DbsField cwf_Support_Tbl_Data_Rest;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Tbl_Table_Access_Level;

    private DataAccessProgramView vw_icw_Rept_Driver_Tbl;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Table_Nme;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Key_Field;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Data_Field;

    private DbsGroup icw_Rept_Driver_Tbl__R_Field_2;
    private DbsField icw_Rept_Driver_Tbl_Rpt_Start_Date;
    private DbsField icw_Rept_Driver_Tbl_Rpt_End_Date;
    private DbsField icw_Rept_Driver_Tbl_Run_Flag;
    private DbsField icw_Rept_Driver_Tbl_Override_Flag;
    private DbsField icw_Rept_Driver_Tbl_Last_Run_Date;
    private DbsField icw_Rept_Driver_Tbl_Rpt_Misc;
    private DbsField icw_Rept_Driver_Tbl_Data_Rest;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Actve_Ind;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Updte_Dte;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField icw_Rept_Driver_Tbl_Tbl_Table_Rectype;

    private DataAccessProgramView vw_icw_Rept_Driver_Updt_Tbl;
    private DbsField icw_Rept_Driver_Updt_Tbl_Tbl_Data_Field;

    private DbsGroup icw_Rept_Driver_Updt_Tbl__R_Field_3;
    private DbsField icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date;

    private DbsGroup icw_Rept_Driver_Updt_Tbl__R_Field_4;
    private DbsField icw_Rept_Driver_Updt_Tbl_Start_Date_Yyyy;
    private DbsField icw_Rept_Driver_Updt_Tbl_Start_Date_Mm;

    private DbsGroup icw_Rept_Driver_Updt_Tbl__R_Field_5;
    private DbsField icw_Rept_Driver_Updt_Tbl_Start_Date_Mm_Nn;
    private DbsField icw_Rept_Driver_Updt_Tbl_Start_Date_Dd;
    private DbsField icw_Rept_Driver_Updt_Tbl_Rpt_End_Date;
    private DbsField icw_Rept_Driver_Updt_Tbl_Run_Flag;
    private DbsField icw_Rept_Driver_Updt_Tbl_Override_Flag;
    private DbsField icw_Rept_Driver_Updt_Tbl_Last_Run_Date;
    private DbsField icw_Rept_Driver_Updt_Tbl_Rpt_Misc;
    private DbsField icw_Rept_Driver_Updt_Tbl_Data_Rest;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_6;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Sec_Level;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Act_Ind;
    private DbsField pnd_Tbl_Prime_Key_2;

    private DbsGroup pnd_Tbl_Prime_Key_2__R_Field_7;
    private DbsField pnd_Tbl_Prime_Key_2_Pnd_Tbl_Sec_Level;
    private DbsField pnd_Tbl_Prime_Key_2_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_2_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_2_Pnd_Act_Ind;
    private DbsField pnd_Message;
    private DbsField pnd_Override_Start_Dte;
    private DbsField pnd_Override_End_Dte;
    private DbsField pnd_Override_Tbl;
    private DbsField pnd_Frequency_Tbl;

    private DbsGroup pnd_Frequency_Tbl__R_Field_8;
    private DbsField pnd_Frequency_Tbl_Pnd_Freq;
    private DbsField pnd_Report_Type;

    private DbsGroup pnd_Report_Type__R_Field_9;
    private DbsField pnd_Report_Type_Pnd_Report_Array;
    private DbsField pnd_Terminate;
    private DbsField pnd_No_Records_Found;
    private DbsField pnd_Daily;
    private DbsField pnd_Weekly;
    private DbsField pnd_Monthly;
    private DbsField pnd_Qtrly;
    private DbsField pnd_Yearly;
    private DbsField pnd_Override_Update;
    private DbsField pnd_Rpt_Cntr;
    private DbsField pnd_Check_Date;
    private DbsField pnd_Check_Date_Name;
    private DbsField pnd_I;
    private DbsField pnd_Hold_Freq;
    private DbsField pnd_Remarks;
    private DbsField pnd_Start_Dte;
    private DbsField pnd_Fix_Dte_Tme;

    private DbsGroup pnd_Fix_Dte_Tme__R_Field_10;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Fix_Dte;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Fix_Tme;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Isn;
    private DbsField pnd_Check_Dte_Tme;

    private DbsGroup pnd_Check_Dte_Tme__R_Field_11;
    private DbsField pnd_Check_Dte_Tme_Pnd_Check_Dte;
    private DbsField pnd_Check_Dte_Tme_Pnd_Check_Tme;
    private DbsField pnd_Start_Dte_Tme;
    private DbsField pnd_End_Dte_Tme;
    private DbsField pnd_Calendar_Days;
    private DbsField pnd_Business_Days;
    private DbsField pnd_Holiday_And_Weekend;
    private DbsField pnd_Sdte_Holiday;
    private DbsField pnd_Sdte_Dayofweek;
    private DbsField pnd_Bsnss_D;
    private DbsField pnd_Week_End_D;
    private DbsField pnd_Month_End_D;
    private DbsField pnd_Qtr_End_D;
    private DbsField pnd_Week_Start_D;
    private DbsField pnd_Month_Start_D;
    private DbsField pnd_Qtr_Start_D;
    private DbsField pnd_Disp_Daily_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaZdkeylda = new LdaZdkeylda();
        registerRecord(ldaZdkeylda);
        localVariables = new DbsRecord();
        pdaCdenvira = new PdaCdenvira(localVariables);
        ldaCddialda = new LdaCddialda();
        registerRecord(ldaCddialda);
        pdaIcwpda_M = new PdaIcwpda_M(localVariables);

        // Local Variables

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Daily_Date = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Daily_Date", "DAILY-DATE", FieldType.STRING, 8);
        cwf_Support_Tbl_Data_Rest = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Data_Rest", "DATA-REST", FieldType.STRING, 245);
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");
        registerRecord(vw_cwf_Support_Tbl);

        vw_icw_Rept_Driver_Tbl = new DataAccessProgramView(new NameInfo("vw_icw_Rept_Driver_Tbl", "ICW-REPT-DRIVER-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        icw_Rept_Driver_Tbl_Tbl_Scrty_Level_Ind = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        icw_Rept_Driver_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        icw_Rept_Driver_Tbl_Tbl_Table_Nme = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        icw_Rept_Driver_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        icw_Rept_Driver_Tbl_Tbl_Key_Field = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        icw_Rept_Driver_Tbl_Tbl_Data_Field = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", 
            FieldType.STRING, 253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        icw_Rept_Driver_Tbl__R_Field_2 = vw_icw_Rept_Driver_Tbl.getRecord().newGroupInGroup("icw_Rept_Driver_Tbl__R_Field_2", "REDEFINE", icw_Rept_Driver_Tbl_Tbl_Data_Field);
        icw_Rept_Driver_Tbl_Rpt_Start_Date = icw_Rept_Driver_Tbl__R_Field_2.newFieldInGroup("icw_Rept_Driver_Tbl_Rpt_Start_Date", "RPT-START-DATE", FieldType.STRING, 
            8);
        icw_Rept_Driver_Tbl_Rpt_End_Date = icw_Rept_Driver_Tbl__R_Field_2.newFieldInGroup("icw_Rept_Driver_Tbl_Rpt_End_Date", "RPT-END-DATE", FieldType.STRING, 
            8);
        icw_Rept_Driver_Tbl_Run_Flag = icw_Rept_Driver_Tbl__R_Field_2.newFieldInGroup("icw_Rept_Driver_Tbl_Run_Flag", "RUN-FLAG", FieldType.STRING, 1);
        icw_Rept_Driver_Tbl_Override_Flag = icw_Rept_Driver_Tbl__R_Field_2.newFieldInGroup("icw_Rept_Driver_Tbl_Override_Flag", "OVERRIDE-FLAG", FieldType.STRING, 
            1);
        icw_Rept_Driver_Tbl_Last_Run_Date = icw_Rept_Driver_Tbl__R_Field_2.newFieldInGroup("icw_Rept_Driver_Tbl_Last_Run_Date", "LAST-RUN-DATE", FieldType.STRING, 
            15);
        icw_Rept_Driver_Tbl_Rpt_Misc = icw_Rept_Driver_Tbl__R_Field_2.newFieldInGroup("icw_Rept_Driver_Tbl_Rpt_Misc", "RPT-MISC", FieldType.STRING, 30);
        icw_Rept_Driver_Tbl_Data_Rest = icw_Rept_Driver_Tbl__R_Field_2.newFieldInGroup("icw_Rept_Driver_Tbl_Data_Rest", "DATA-REST", FieldType.STRING, 
            190);
        icw_Rept_Driver_Tbl_Tbl_Actve_Ind = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        icw_Rept_Driver_Tbl_Tbl_Entry_Dte_Tme = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        icw_Rept_Driver_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        icw_Rept_Driver_Tbl_Tbl_Entry_Oprtr_Cde = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        icw_Rept_Driver_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        icw_Rept_Driver_Tbl_Tbl_Updte_Dte_Tme = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        icw_Rept_Driver_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        icw_Rept_Driver_Tbl_Tbl_Updte_Dte = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        icw_Rept_Driver_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        icw_Rept_Driver_Tbl_Tbl_Updte_Oprtr_Cde = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        icw_Rept_Driver_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        icw_Rept_Driver_Tbl_Tbl_Dlte_Dte_Tme = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        icw_Rept_Driver_Tbl_Tbl_Dlte_Oprtr_Cde = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        icw_Rept_Driver_Tbl_Tbl_Table_Rectype = vw_icw_Rept_Driver_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        registerRecord(vw_icw_Rept_Driver_Tbl);

        vw_icw_Rept_Driver_Updt_Tbl = new DataAccessProgramView(new NameInfo("vw_icw_Rept_Driver_Updt_Tbl", "ICW-REPT-DRIVER-UPDT-TBL"), "CWF_SUPPORT_TBL", 
            "CWF_DCMNT_TABLE");
        icw_Rept_Driver_Updt_Tbl_Tbl_Data_Field = vw_icw_Rept_Driver_Updt_Tbl.getRecord().newFieldInGroup("icw_Rept_Driver_Updt_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", 
            FieldType.STRING, 253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        icw_Rept_Driver_Updt_Tbl__R_Field_3 = vw_icw_Rept_Driver_Updt_Tbl.getRecord().newGroupInGroup("icw_Rept_Driver_Updt_Tbl__R_Field_3", "REDEFINE", 
            icw_Rept_Driver_Updt_Tbl_Tbl_Data_Field);
        icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date = icw_Rept_Driver_Updt_Tbl__R_Field_3.newFieldInGroup("icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date", "RPT-START-DATE", 
            FieldType.STRING, 8);

        icw_Rept_Driver_Updt_Tbl__R_Field_4 = icw_Rept_Driver_Updt_Tbl__R_Field_3.newGroupInGroup("icw_Rept_Driver_Updt_Tbl__R_Field_4", "REDEFINE", icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date);
        icw_Rept_Driver_Updt_Tbl_Start_Date_Yyyy = icw_Rept_Driver_Updt_Tbl__R_Field_4.newFieldInGroup("icw_Rept_Driver_Updt_Tbl_Start_Date_Yyyy", "START-DATE-YYYY", 
            FieldType.STRING, 4);
        icw_Rept_Driver_Updt_Tbl_Start_Date_Mm = icw_Rept_Driver_Updt_Tbl__R_Field_4.newFieldInGroup("icw_Rept_Driver_Updt_Tbl_Start_Date_Mm", "START-DATE-MM", 
            FieldType.STRING, 2);

        icw_Rept_Driver_Updt_Tbl__R_Field_5 = icw_Rept_Driver_Updt_Tbl__R_Field_4.newGroupInGroup("icw_Rept_Driver_Updt_Tbl__R_Field_5", "REDEFINE", icw_Rept_Driver_Updt_Tbl_Start_Date_Mm);
        icw_Rept_Driver_Updt_Tbl_Start_Date_Mm_Nn = icw_Rept_Driver_Updt_Tbl__R_Field_5.newFieldInGroup("icw_Rept_Driver_Updt_Tbl_Start_Date_Mm_Nn", "START-DATE-MM-NN", 
            FieldType.NUMERIC, 2);
        icw_Rept_Driver_Updt_Tbl_Start_Date_Dd = icw_Rept_Driver_Updt_Tbl__R_Field_4.newFieldInGroup("icw_Rept_Driver_Updt_Tbl_Start_Date_Dd", "START-DATE-DD", 
            FieldType.STRING, 2);
        icw_Rept_Driver_Updt_Tbl_Rpt_End_Date = icw_Rept_Driver_Updt_Tbl__R_Field_3.newFieldInGroup("icw_Rept_Driver_Updt_Tbl_Rpt_End_Date", "RPT-END-DATE", 
            FieldType.STRING, 8);
        icw_Rept_Driver_Updt_Tbl_Run_Flag = icw_Rept_Driver_Updt_Tbl__R_Field_3.newFieldInGroup("icw_Rept_Driver_Updt_Tbl_Run_Flag", "RUN-FLAG", FieldType.STRING, 
            1);
        icw_Rept_Driver_Updt_Tbl_Override_Flag = icw_Rept_Driver_Updt_Tbl__R_Field_3.newFieldInGroup("icw_Rept_Driver_Updt_Tbl_Override_Flag", "OVERRIDE-FLAG", 
            FieldType.STRING, 1);
        icw_Rept_Driver_Updt_Tbl_Last_Run_Date = icw_Rept_Driver_Updt_Tbl__R_Field_3.newFieldInGroup("icw_Rept_Driver_Updt_Tbl_Last_Run_Date", "LAST-RUN-DATE", 
            FieldType.STRING, 15);
        icw_Rept_Driver_Updt_Tbl_Rpt_Misc = icw_Rept_Driver_Updt_Tbl__R_Field_3.newFieldInGroup("icw_Rept_Driver_Updt_Tbl_Rpt_Misc", "RPT-MISC", FieldType.STRING, 
            30);
        icw_Rept_Driver_Updt_Tbl_Data_Rest = icw_Rept_Driver_Updt_Tbl__R_Field_3.newFieldInGroup("icw_Rept_Driver_Updt_Tbl_Data_Rest", "DATA-REST", FieldType.STRING, 
            190);
        registerRecord(vw_icw_Rept_Driver_Updt_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_6", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Sec_Level = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Sec_Level", "#TBL-SEC-LEVEL", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Act_Ind = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Act_Ind", "#ACT-IND", FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_2 = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_2", "#TBL-PRIME-KEY-2", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_2__R_Field_7 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_2__R_Field_7", "REDEFINE", pnd_Tbl_Prime_Key_2);
        pnd_Tbl_Prime_Key_2_Pnd_Tbl_Sec_Level = pnd_Tbl_Prime_Key_2__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_2_Pnd_Tbl_Sec_Level", "#TBL-SEC-LEVEL", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_2_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_2__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_2_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_2_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_2__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_2_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);
        pnd_Tbl_Prime_Key_2_Pnd_Act_Ind = pnd_Tbl_Prime_Key_2__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_2_Pnd_Act_Ind", "#ACT-IND", FieldType.STRING, 
            1);
        pnd_Message = localVariables.newFieldInRecord("pnd_Message", "#MESSAGE", FieldType.STRING, 132);
        pnd_Override_Start_Dte = localVariables.newFieldArrayInRecord("pnd_Override_Start_Dte", "#OVERRIDE-START-DTE", FieldType.STRING, 8, new DbsArrayController(1, 
            4));
        pnd_Override_End_Dte = localVariables.newFieldArrayInRecord("pnd_Override_End_Dte", "#OVERRIDE-END-DTE", FieldType.STRING, 8, new DbsArrayController(1, 
            4));
        pnd_Override_Tbl = localVariables.newFieldArrayInRecord("pnd_Override_Tbl", "#OVERRIDE-TBL", FieldType.BOOLEAN, 1, new DbsArrayController(1, 4));
        pnd_Frequency_Tbl = localVariables.newFieldInRecord("pnd_Frequency_Tbl", "#FREQUENCY-TBL", FieldType.STRING, 4);

        pnd_Frequency_Tbl__R_Field_8 = localVariables.newGroupInRecord("pnd_Frequency_Tbl__R_Field_8", "REDEFINE", pnd_Frequency_Tbl);
        pnd_Frequency_Tbl_Pnd_Freq = pnd_Frequency_Tbl__R_Field_8.newFieldArrayInGroup("pnd_Frequency_Tbl_Pnd_Freq", "#FREQ", FieldType.STRING, 1, new 
            DbsArrayController(1, 4));
        pnd_Report_Type = localVariables.newFieldInRecord("pnd_Report_Type", "#REPORT-TYPE", FieldType.STRING, 36);

        pnd_Report_Type__R_Field_9 = localVariables.newGroupInRecord("pnd_Report_Type__R_Field_9", "REDEFINE", pnd_Report_Type);
        pnd_Report_Type_Pnd_Report_Array = pnd_Report_Type__R_Field_9.newFieldArrayInGroup("pnd_Report_Type_Pnd_Report_Array", "#REPORT-ARRAY", FieldType.STRING, 
            9, new DbsArrayController(1, 4));
        pnd_Terminate = localVariables.newFieldInRecord("pnd_Terminate", "#TERMINATE", FieldType.BOOLEAN, 1);
        pnd_No_Records_Found = localVariables.newFieldInRecord("pnd_No_Records_Found", "#NO-RECORDS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Daily = localVariables.newFieldInRecord("pnd_Daily", "#DAILY", FieldType.BOOLEAN, 1);
        pnd_Weekly = localVariables.newFieldInRecord("pnd_Weekly", "#WEEKLY", FieldType.BOOLEAN, 1);
        pnd_Monthly = localVariables.newFieldInRecord("pnd_Monthly", "#MONTHLY", FieldType.BOOLEAN, 1);
        pnd_Qtrly = localVariables.newFieldInRecord("pnd_Qtrly", "#QTRLY", FieldType.BOOLEAN, 1);
        pnd_Yearly = localVariables.newFieldInRecord("pnd_Yearly", "#YEARLY", FieldType.BOOLEAN, 1);
        pnd_Override_Update = localVariables.newFieldInRecord("pnd_Override_Update", "#OVERRIDE-UPDATE", FieldType.BOOLEAN, 1);
        pnd_Rpt_Cntr = localVariables.newFieldInRecord("pnd_Rpt_Cntr", "#RPT-CNTR", FieldType.NUMERIC, 2);
        pnd_Check_Date = localVariables.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.DATE);
        pnd_Check_Date_Name = localVariables.newFieldInRecord("pnd_Check_Date_Name", "#CHECK-DATE-NAME", FieldType.STRING, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Hold_Freq = localVariables.newFieldInRecord("pnd_Hold_Freq", "#HOLD-FREQ", FieldType.STRING, 1);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 45);
        pnd_Start_Dte = localVariables.newFieldInRecord("pnd_Start_Dte", "#START-DTE", FieldType.DATE);
        pnd_Fix_Dte_Tme = localVariables.newFieldInRecord("pnd_Fix_Dte_Tme", "#FIX-DTE-TME", FieldType.STRING, 15);

        pnd_Fix_Dte_Tme__R_Field_10 = localVariables.newGroupInRecord("pnd_Fix_Dte_Tme__R_Field_10", "REDEFINE", pnd_Fix_Dte_Tme);
        pnd_Fix_Dte_Tme_Pnd_Fix_Dte = pnd_Fix_Dte_Tme__R_Field_10.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Fix_Dte", "#FIX-DTE", FieldType.STRING, 8);
        pnd_Fix_Dte_Tme_Pnd_Fix_Tme = pnd_Fix_Dte_Tme__R_Field_10.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Fix_Tme", "#FIX-TME", FieldType.NUMERIC, 7);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 100);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Check_Dte_Tme = localVariables.newFieldInRecord("pnd_Check_Dte_Tme", "#CHECK-DTE-TME", FieldType.STRING, 15);

        pnd_Check_Dte_Tme__R_Field_11 = localVariables.newGroupInRecord("pnd_Check_Dte_Tme__R_Field_11", "REDEFINE", pnd_Check_Dte_Tme);
        pnd_Check_Dte_Tme_Pnd_Check_Dte = pnd_Check_Dte_Tme__R_Field_11.newFieldInGroup("pnd_Check_Dte_Tme_Pnd_Check_Dte", "#CHECK-DTE", FieldType.STRING, 
            8);
        pnd_Check_Dte_Tme_Pnd_Check_Tme = pnd_Check_Dte_Tme__R_Field_11.newFieldInGroup("pnd_Check_Dte_Tme_Pnd_Check_Tme", "#CHECK-TME", FieldType.NUMERIC, 
            7);
        pnd_Start_Dte_Tme = localVariables.newFieldInRecord("pnd_Start_Dte_Tme", "#START-DTE-TME", FieldType.TIME);
        pnd_End_Dte_Tme = localVariables.newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.TIME);
        pnd_Calendar_Days = localVariables.newFieldInRecord("pnd_Calendar_Days", "#CALENDAR-DAYS", FieldType.NUMERIC, 18);
        pnd_Business_Days = localVariables.newFieldInRecord("pnd_Business_Days", "#BUSINESS-DAYS", FieldType.NUMERIC, 18);
        pnd_Holiday_And_Weekend = localVariables.newFieldInRecord("pnd_Holiday_And_Weekend", "#HOLIDAY-AND-WEEKEND", FieldType.NUMERIC, 18);
        pnd_Sdte_Holiday = localVariables.newFieldInRecord("pnd_Sdte_Holiday", "#SDTE-HOLIDAY", FieldType.STRING, 20);
        pnd_Sdte_Dayofweek = localVariables.newFieldInRecord("pnd_Sdte_Dayofweek", "#SDTE-DAYOFWEEK", FieldType.STRING, 9);
        pnd_Bsnss_D = localVariables.newFieldInRecord("pnd_Bsnss_D", "#BSNSS-D", FieldType.DATE);
        pnd_Week_End_D = localVariables.newFieldInRecord("pnd_Week_End_D", "#WEEK-END-D", FieldType.DATE);
        pnd_Month_End_D = localVariables.newFieldInRecord("pnd_Month_End_D", "#MONTH-END-D", FieldType.DATE);
        pnd_Qtr_End_D = localVariables.newFieldInRecord("pnd_Qtr_End_D", "#QTR-END-D", FieldType.DATE);
        pnd_Week_Start_D = localVariables.newFieldInRecord("pnd_Week_Start_D", "#WEEK-START-D", FieldType.DATE);
        pnd_Month_Start_D = localVariables.newFieldInRecord("pnd_Month_Start_D", "#MONTH-START-D", FieldType.DATE);
        pnd_Qtr_Start_D = localVariables.newFieldInRecord("pnd_Qtr_Start_D", "#QTR-START-D", FieldType.DATE);
        pnd_Disp_Daily_Dte = localVariables.newFieldInRecord("pnd_Disp_Daily_Dte", "#DISP-DAILY-DTE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();
        vw_icw_Rept_Driver_Tbl.reset();
        vw_icw_Rept_Driver_Updt_Tbl.reset();

        ldaZdkeylda.initializeValues();
        ldaCddialda.initializeValues();

        localVariables.reset();
        pnd_Tbl_Prime_Key.setInitialValue("A CWF-RPRT-RUN-TBL    D");
        pnd_Tbl_Prime_Key_2.setInitialValue("A ICW-REPT-DRIVER-TBL  ");
        pnd_Frequency_Tbl.setInitialValue("DMQW");
        pnd_Report_Type.setInitialValue("Daily    Monthly  QuarterlyWeekly   ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Icwbdrv() throws Exception
    {
        super("Icwbdrv");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 53 LS = 133;//Natural: FORMAT ( 2 ) PS = 53 LS = 133;//Natural: FORMAT ( 3 ) PS = 53 LS = 133
                                                                                                                                                                          //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 );//Natural: AT TOP OF PAGE ( 3 );//Natural: PERFORM INITIALIZATIONS
        sub_Initializations();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-STATS-REPORT
        sub_Get_Stats_Report();
        if (condition(Global.isEscape())) {return;}
        //* *
                                                                                                                                                                          //Natural: PERFORM GET-RUN-DATE
        sub_Get_Run_Date();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-OVERRIDE
        sub_Check_For_Override();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-DATES
        sub_Get_Dates();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-DATES-RETURNED
        sub_Write_Dates_Returned();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATIONS
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RUN-DATE
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DATES
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATES-RETURNED
        //*    'BUSINESS/DATE'           #BSNSS-D
        //* *************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-FREQUENCY
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-OVERRIDE
        //* ***********************************************************************
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-DRIVER-TBL
        //*  MOVE EDITED *TIMX(EM=YYYYMMDDHHIISST) TO LAST-RUN-DATE
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-WEEK-BEGIN-DATE
        //* ***********************************************************************
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MONTH-BEGIN-DATE
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-QTR-BEGIN-DATE
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATS-REPORT
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR
        //* ***********************************************************************
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-STATS-REPORT
        //* ***********************************************************************
        //* *************
    }
    private void sub_Initializations() throws Exception                                                                                                                   //Natural: INITIALIZATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Remarks.reset();                                                                                                                                              //Natural: RESET #REMARKS #CALENDAR-DAYS #BUSINESS-DAYS #HOLIDAY-AND-WEEKEND #SDTE-HOLIDAY #SDTE-DAYOFWEEK #WEEK-END-D #MONTH-END-D #QTR-END-D MSG-INFO-SUB
        pnd_Calendar_Days.reset();
        pnd_Business_Days.reset();
        pnd_Holiday_And_Weekend.reset();
        pnd_Sdte_Holiday.reset();
        pnd_Sdte_Dayofweek.reset();
        pnd_Week_End_D.reset();
        pnd_Month_End_D.reset();
        pnd_Qtr_End_D.reset();
        pdaIcwpda_M.getMsg_Info_Sub().reset();
        pnd_Daily.setValue(false);                                                                                                                                        //Natural: MOVE FALSE TO #DAILY #WEEKLY #MONTHLY #QTRLY #OVERRIDE-TBL ( * )
        pnd_Weekly.setValue(false);
        pnd_Monthly.setValue(false);
        pnd_Qtrly.setValue(false);
        pnd_Override_Tbl.getValue("*").setValue(false);
        //* *************
        //*  INITIALIZATIONS
    }
    private void sub_Get_Run_Date() throws Exception                                                                                                                      //Natural: GET-RUN-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Fix_Dte_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                                                           //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO #FIX-DTE-TME
        //*  MOVE  '199707211301111' TO #FIX-DTE-TME
        pnd_Start_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Fix_Dte_Tme_Pnd_Fix_Dte);                                                                         //Natural: MOVE EDITED #FIX-DTE TO #START-DTE ( EM = YYYYMMDD )
        if (condition(pnd_Fix_Dte_Tme_Pnd_Fix_Tme.lessOrEqual(800000)))                                                                                                   //Natural: IF #FIX-TME LE 0800000
        {
            pnd_Start_Dte.nsubtract(1);                                                                                                                                   //Natural: ASSIGN #START-DTE := #START-DTE - 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Start_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Fix_Dte_Tme_Pnd_Fix_Dte);                                                                     //Natural: MOVE EDITED #FIX-DTE TO #START-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Fix_Dte_Tme_Pnd_Fix_Dte.setValueEdited(pnd_Start_Dte,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED #START-DTE ( EM = YYYYMMDD ) TO #FIX-DTE
        pnd_Fix_Dte_Tme.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Fix_Dte_Tme_Pnd_Fix_Dte, "0000000"));                                                //Natural: COMPRESS #FIX-DTE '0000000' INTO #FIX-DTE-TME LEAVING NO SPACE
        pnd_Start_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Fix_Dte_Tme);                                                                          //Natural: MOVE EDITED #FIX-DTE-TME TO #START-DTE-TME ( EM = YYYYMMDDHHIISST )
        //* *************
        //*  GET-RUN-DATE
    }
    private void sub_Get_Dates() throws Exception                                                                                                                         //Natural: GET-DATES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Icwn3900.class , getCurrentProcessState(), pnd_Start_Dte_Tme, pnd_End_Dte_Tme, pnd_Calendar_Days, pnd_Business_Days, pnd_Holiday_And_Weekend,     //Natural: CALLNAT 'ICWN3900' #START-DTE-TME #END-DTE-TME #CALENDAR-DAYS #BUSINESS-DAYS #HOLIDAY-AND-WEEKEND #SDTE-HOLIDAY #SDTE-DAYOFWEEK #BSNSS-D #WEEK-END-D #MONTH-END-D #QTR-END-D MSG-INFO-SUB
            pnd_Sdte_Holiday, pnd_Sdte_Dayofweek, pnd_Bsnss_D, pnd_Week_End_D, pnd_Month_End_D, pnd_Qtr_End_D, pdaIcwpda_M.getMsg_Info_Sub());
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM ASSIGN-FREQUENCY
        sub_Assign_Frequency();
        if (condition(Global.isEscape())) {return;}
        //* *************
        //*  GET-FREQ-DATES
    }
    private void sub_Write_Dates_Returned() throws Exception                                                                                                              //Natural: WRITE-DATES-RETURNED
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().display(1, "DAILY/DATE",                                                                                                                             //Natural: DISPLAY ( 1 ) 'DAILY/DATE' #DISP-DAILY-DTE ( EM = YYYYMMDD ) 'START DATE/& TIME:' #START-DTE-TME ( EM = MM/DD/YY ) 'WEEKEND/PROC DATE' #WEEK-END-D ( EM = YYYYMMDD ) 'MONTHEND/PROC DATE' #MONTH-END-D ( EM = YYYYMMDD ) 'QUARTEREND/PROC DATE' #QTR-END-D ( EM = YYYYMMDD ) 'DAY OF/WEEK/NAME ' #SDTE-DAYOFWEEK ( AL = 3 ) 'REPORTS TO RUN' #REMARKS ( AL = 25 ) 'HOLIDAY/NAME' #SDTE-HOLIDAY 'OVERRIDE/START DATES' #OVERRIDE-START-DTE ( * ) 'OVERRIDE/ENE DATES' #OVERRIDE-END-DTE ( * )
        		pnd_Disp_Daily_Dte, new ReportEditMask ("YYYYMMDD"),"START DATE/& TIME:",
        		pnd_Start_Dte_Tme, new ReportEditMask ("MM/DD/YY"),"WEEKEND/PROC DATE",
        		pnd_Week_End_D, new ReportEditMask ("YYYYMMDD"),"MONTHEND/PROC DATE",
        		pnd_Month_End_D, new ReportEditMask ("YYYYMMDD"),"QUARTEREND/PROC DATE",
        		pnd_Qtr_End_D, new ReportEditMask ("YYYYMMDD"),"DAY OF/WEEK/NAME ",
        		pnd_Sdte_Dayofweek, new AlphanumericLength (3),"REPORTS TO RUN",
        		pnd_Remarks, new AlphanumericLength (25),"HOLIDAY/NAME",
        		pnd_Sdte_Holiday,"OVERRIDE/START DATES",
        		pnd_Override_Start_Dte.getValue("*"),"OVERRIDE/ENE DATES",
        		pnd_Override_End_Dte.getValue("*"));
        if (Global.isEscape()) return;
        //*  DISPLAY (1) #START-DTE-TME (EM=YYYYMMDD)
        //*             #END-DTE-TME  (EM=YYYYMMDD)
        //*            #CALENDAR-DAYS
        //*            #BUSINESS-DAYS
        //*            #HOLIDAY-AND-WEEKEND
        //*            #SDTE-HOLIDAY
        //*            #SDTE-DAYOFWEEK
        //*             #BSNSS-D (EM=YYYYMMDD)
        //*             #WEEK-END-D     (EM=YYYYMMDD)
        //*             #MONTH-END-D    (EM=YYYYMMDD)
        //*             #QTR-END-D      (EM=YYYYMMDD)
        //*            MSG-INFO-SUB
        //* *************
        //*  WRITE-DATES-RETURNED
    }
    private void sub_Assign_Frequency() throws Exception                                                                                                                  //Natural: ASSIGN-FREQUENCY
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(DbsUtil.maskMatches(pnd_Sdte_Dayofweek,"'SAT'") || DbsUtil.maskMatches(pnd_Sdte_Dayofweek,"'SUN'")))                                                //Natural: IF #SDTE-DAYOFWEEK = MASK ( 'SAT' ) OR = MASK ( 'SUN' )
        {
            pnd_Daily.setValue(false);                                                                                                                                    //Natural: MOVE FALSE TO #DAILY #MONTHLY #QTRLY
            pnd_Monthly.setValue(false);
            pnd_Qtrly.setValue(false);
            pnd_Weekly.setValue(true);                                                                                                                                    //Natural: MOVE TRUE TO #WEEKLY
            //*  USED TO BE ELSE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Sdte_Holiday.equals(" ")))                                                                                                                  //Natural: IF #SDTE-HOLIDAY = ' '
            {
                pnd_Daily.setValue(true);                                                                                                                                 //Natural: MOVE TRUE TO #DAILY
                short decideConditionsMet512 = 0;                                                                                                                         //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #START-DTE = #WEEK-END-D
                if (condition(pnd_Start_Dte.equals(pnd_Week_End_D)))
                {
                    decideConditionsMet512++;
                    //*  DO WE RUN WEEKLY ON FRIDAY?
                    pnd_Weekly.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #WEEKLY #DAILY
                    pnd_Daily.setValue(true);
                }                                                                                                                                                         //Natural: WHEN #START-DTE = #MONTH-END-D
                if (condition(pnd_Start_Dte.equals(pnd_Month_End_D)))
                {
                    decideConditionsMet512++;
                    pnd_Monthly.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO #MONTHLY #DAILY
                    pnd_Daily.setValue(true);
                }                                                                                                                                                         //Natural: WHEN #START-DTE = #QTR-END-D
                if (condition(pnd_Start_Dte.equals(pnd_Qtr_End_D)))
                {
                    decideConditionsMet512++;
                    pnd_Qtrly.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #QTRLY #DAILY
                    pnd_Daily.setValue(true);
                }                                                                                                                                                         //Natural: WHEN NONE
                if (condition(decideConditionsMet512 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        icw_Rept_Driver_Updt_Tbl_Run_Flag.reset();                                                                                                                        //Natural: RESET ICW-REPT-DRIVER-UPDT-TBL.RUN-FLAG ICW-REPT-DRIVER-UPDT-TBL.OVERRIDE-FLAG
        icw_Rept_Driver_Updt_Tbl_Override_Flag.reset();
        short decideConditionsMet526 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #DAILY AND #OVERRIDE-TBL ( 1 ) EQ FALSE
        if (condition(pnd_Daily.getBoolean() && pnd_Override_Tbl.getValue(1).equals(false)))
        {
            decideConditionsMet526++;
            pnd_Remarks.setValue("DAILY");                                                                                                                                //Natural: MOVE 'DAILY' TO #REMARKS
            icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date.setValueEdited(pnd_Start_Dte,new ReportEditMask("YYYYMMDD"));                                                         //Natural: MOVE EDITED #START-DTE ( EM = YYYYMMDD ) TO ICW-REPT-DRIVER-UPDT-TBL.RPT-START-DATE
            icw_Rept_Driver_Updt_Tbl_Rpt_End_Date.setValue(icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date);                                                                      //Natural: MOVE ICW-REPT-DRIVER-UPDT-TBL.RPT-START-DATE TO ICW-REPT-DRIVER-UPDT-TBL.RPT-END-DATE
            pnd_Disp_Daily_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date);                                                    //Natural: MOVE EDITED ICW-REPT-DRIVER-UPDT-TBL.RPT-START-DATE TO #DISP-DAILY-DTE ( EM = YYYYMMDD )
            pnd_Tbl_Prime_Key_2_Pnd_Tbl_Key_Field.setValue("D");                                                                                                          //Natural: MOVE 'D' TO #TBL-PRIME-KEY-2.#TBL-KEY-FIELD
                                                                                                                                                                          //Natural: PERFORM UPDATE-DRIVER-TBL
            sub_Update_Driver_Tbl();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #WEEKLY AND #OVERRIDE-TBL ( 4 ) EQ FALSE
        if (condition(pnd_Weekly.getBoolean() && pnd_Override_Tbl.getValue(4).equals(false)))
        {
            decideConditionsMet526++;
            icw_Rept_Driver_Updt_Tbl_Rpt_End_Date.setValueEdited(pnd_Week_End_D,new ReportEditMask("YYYYMMDD"));                                                          //Natural: MOVE EDITED #WEEK-END-D ( EM = YYYYMMDD ) TO ICW-REPT-DRIVER-UPDT-TBL.RPT-END-DATE
                                                                                                                                                                          //Natural: PERFORM GET-WEEK-BEGIN-DATE
            sub_Get_Week_Begin_Date();
            if (condition(Global.isEscape())) {return;}
            pnd_Tbl_Prime_Key_2_Pnd_Tbl_Key_Field.setValue("W");                                                                                                          //Natural: MOVE 'W' TO #TBL-PRIME-KEY-2.#TBL-KEY-FIELD
                                                                                                                                                                          //Natural: PERFORM UPDATE-DRIVER-TBL
            sub_Update_Driver_Tbl();
            if (condition(Global.isEscape())) {return;}
            pnd_Remarks.setValue(DbsUtil.compress(pnd_Remarks, "WEEKLY"));                                                                                                //Natural: COMPRESS #REMARKS 'WEEKLY' INTO #REMARKS
        }                                                                                                                                                                 //Natural: WHEN #MONTHLY AND #OVERRIDE-TBL ( 2 ) EQ FALSE
        if (condition(pnd_Monthly.getBoolean() && pnd_Override_Tbl.getValue(2).equals(false)))
        {
            decideConditionsMet526++;
            pnd_Remarks.setValue(DbsUtil.compress(pnd_Remarks, ", MONTHLY"));                                                                                             //Natural: COMPRESS #REMARKS ', MONTHLY' INTO #REMARKS
            icw_Rept_Driver_Updt_Tbl_Rpt_End_Date.setValueEdited(pnd_Month_End_D,new ReportEditMask("YYYYMMDD"));                                                         //Natural: MOVE EDITED #MONTH-END-D ( EM = YYYYMMDD ) TO ICW-REPT-DRIVER-UPDT-TBL.RPT-END-DATE
                                                                                                                                                                          //Natural: PERFORM GET-MONTH-BEGIN-DATE
            sub_Get_Month_Begin_Date();
            if (condition(Global.isEscape())) {return;}
            pnd_Tbl_Prime_Key_2_Pnd_Tbl_Key_Field.setValue("M");                                                                                                          //Natural: MOVE 'M' TO #TBL-PRIME-KEY-2.#TBL-KEY-FIELD
                                                                                                                                                                          //Natural: PERFORM UPDATE-DRIVER-TBL
            sub_Update_Driver_Tbl();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #QTRLY AND #OVERRIDE-TBL ( 3 ) EQ FALSE
        if (condition(pnd_Qtrly.getBoolean() && pnd_Override_Tbl.getValue(3).equals(false)))
        {
            decideConditionsMet526++;
            pnd_Remarks.setValue(DbsUtil.compress(pnd_Remarks, ", QUARTERLY"));                                                                                           //Natural: COMPRESS #REMARKS ', QUARTERLY' INTO #REMARKS
            icw_Rept_Driver_Updt_Tbl_Rpt_End_Date.setValueEdited(pnd_Qtr_End_D,new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED #QTR-END-D ( EM = YYYYMMDD ) TO ICW-REPT-DRIVER-UPDT-TBL.RPT-END-DATE
                                                                                                                                                                          //Natural: PERFORM GET-QTR-BEGIN-DATE
            sub_Get_Qtr_Begin_Date();
            if (condition(Global.isEscape())) {return;}
            pnd_Tbl_Prime_Key_2_Pnd_Tbl_Key_Field.setValue("Q");                                                                                                          //Natural: MOVE 'Q' TO #TBL-PRIME-KEY-2.#TBL-KEY-FIELD
                                                                                                                                                                          //Natural: PERFORM UPDATE-DRIVER-TBL
            sub_Update_Driver_Tbl();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #SDTE-HOLIDAY GT ' '
        if (condition(pnd_Sdte_Holiday.greater(" ")))
        {
            decideConditionsMet526++;
            pnd_Remarks.setValue("NO REPORTS TODAY");                                                                                                                     //Natural: MOVE 'NO REPORTS TODAY' TO #REMARKS
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet526 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *************
        //*  ASSIGN-FREQUENCY
    }
    private void sub_Check_For_Override() throws Exception                                                                                                                //Natural: CHECK-FOR-OVERRIDE
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 4
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
        {
            pnd_Override_Update.setValue(false);                                                                                                                          //Natural: MOVE FALSE TO #OVERRIDE-UPDATE
            if (condition(pnd_Override_Tbl.getValue(pnd_I).equals(true)))                                                                                                 //Natural: IF #OVERRIDE-TBL ( #I ) = TRUE
            {
                pnd_Tbl_Prime_Key_2_Pnd_Tbl_Key_Field.setValue(pnd_Frequency_Tbl_Pnd_Freq.getValue(pnd_I));                                                               //Natural: MOVE #FREQ ( #I ) TO #TBL-PRIME-KEY-2.#TBL-KEY-FIELD
                pnd_Override_Update.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #OVERRIDE-UPDATE
                                                                                                                                                                          //Natural: PERFORM UPDATE-DRIVER-TBL
                sub_Update_Driver_Tbl();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *************
        //*  CHECK-FOR-OVERRIDE
    }
    private void sub_Update_Driver_Tbl() throws Exception                                                                                                                 //Natural: UPDATE-DRIVER-TBL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_No_Records_Found.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NO-RECORDS-FOUND
        //*  READ CWF-SUPPORT-TBL
        vw_icw_Rept_Driver_Tbl.startDatabaseRead                                                                                                                          //Natural: READ ICW-REPT-DRIVER-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY-2
        (
        "UPDATE_DATES",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key_2, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        UPDATE_DATES:
        while (condition(vw_icw_Rept_Driver_Tbl.readNextRow("UPDATE_DATES")))
        {
            if (condition(icw_Rept_Driver_Tbl_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_2_Pnd_Tbl_Table_Nme) || icw_Rept_Driver_Tbl_Tbl_Key_Field.notEquals(pnd_Tbl_Prime_Key_2_Pnd_Tbl_Key_Field))) //Natural: IF ICW-REPT-DRIVER-TBL.TBL-TABLE-NME NE #TBL-PRIME-KEY-2.#TBL-TABLE-NME OR ICW-REPT-DRIVER-TBL.TBL-KEY-FIELD NE #TBL-PRIME-KEY-2.#TBL-KEY-FIELD
            {
                if (true) break UPDATE_DATES;                                                                                                                             //Natural: ESCAPE BOTTOM ( UPDATE-DATES. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Override_Update.getBoolean()))                                                                                                              //Natural: IF #OVERRIDE-UPDATE
            {
                vw_icw_Rept_Driver_Updt_Tbl.setValuesByName(vw_icw_Rept_Driver_Tbl);                                                                                      //Natural: MOVE BY NAME ICW-REPT-DRIVER-TBL TO ICW-REPT-DRIVER-UPDT-TBL
                icw_Rept_Driver_Updt_Tbl_Run_Flag.reset();                                                                                                                //Natural: RESET ICW-REPT-DRIVER-UPDT-TBL.RUN-FLAG ICW-REPT-DRIVER-UPDT-TBL.OVERRIDE-FLAG
                icw_Rept_Driver_Updt_Tbl_Override_Flag.reset();
            }                                                                                                                                                             //Natural: END-IF
            pnd_Isn.setValue(vw_icw_Rept_Driver_Tbl.getAstISN("UPDATE_DATES"));                                                                                           //Natural: MOVE *ISN ( UPDATE-DATES. ) TO #ISN
            pnd_No_Records_Found.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #NO-RECORDS-FOUND
            GET_DATE_RECORD:                                                                                                                                              //Natural: GET ICW-REPT-DRIVER-TBL #ISN
            vw_icw_Rept_Driver_Tbl.readByID(pnd_Isn.getLong(), "GET_DATE_RECORD");
            vw_icw_Rept_Driver_Tbl.setValuesByName(vw_icw_Rept_Driver_Updt_Tbl);                                                                                          //Natural: MOVE BY NAME ICW-REPT-DRIVER-UPDT-TBL TO ICW-REPT-DRIVER-TBL
            vw_icw_Rept_Driver_Tbl.updateDBRow("GET_DATE_RECORD");                                                                                                        //Natural: UPDATE ( GET-DATE-RECORD. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_No_Records_Found.getBoolean()))                                                                                                                 //Natural: IF #NO-RECORDS-FOUND
        {
            pnd_Error_Msg.setValue(DbsUtil.compress("No Record for ", pnd_Tbl_Prime_Key_2_Pnd_Tbl_Key_Field, "record on ICW-REPT-DRIVER-TBL - check table"));             //Natural: COMPRESS 'No Record for ' #TBL-PRIME-KEY-2.#TBL-KEY-FIELD 'record on ICW-REPT-DRIVER-TBL - check table' INTO #ERROR-MSG
            pnd_Terminate.setValue(true);                                                                                                                                 //Natural: MOVE TRUE TO #TERMINATE
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR
            sub_Write_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  UPDATE-DRIVER-TB;
    }
    private void sub_Get_Week_Begin_Date() throws Exception                                                                                                               //Natural: GET-WEEK-BEGIN-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Check_Date.setValue(pnd_Week_End_D);                                                                                                                          //Natural: ASSIGN #CHECK-DATE := #WEEK-END-D
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 7
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(7)); pnd_I.nadd(1))
        {
            pnd_Check_Date.nsubtract(1);                                                                                                                                  //Natural: ASSIGN #CHECK-DATE := #CHECK-DATE - 1
            getReports().write(0, "check date:",pnd_Check_Date);                                                                                                          //Natural: WRITE 'check date:' #CHECK-DATE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Check_Date_Name.setValueEdited(pnd_Check_Date,new ReportEditMask("NNN"));                                                                                 //Natural: MOVE EDITED #CHECK-DATE ( EM = NNN ) TO #CHECK-DATE-NAME
            if (condition(pnd_Check_Date_Name.equals("Sat")))                                                                                                             //Natural: IF #CHECK-DATE-NAME = 'Sat'
            {
                icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date.setValueEdited(pnd_Check_Date,new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED #CHECK-DATE ( EM = YYYYMMDD ) TO ICW-REPT-DRIVER-UPDT-TBL.RPT-START-DATE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date);                                                                                                   //Natural: WRITE ICW-REPT-DRIVER-UPDT-TBL.RPT-START-DATE
        if (Global.isEscape()) return;
        //* *************
        //*  GET-WEEK-BEGIN-DATE
    }
    private void sub_Get_Month_Begin_Date() throws Exception                                                                                                              //Natural: GET-MONTH-BEGIN-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *
        icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date.setValue(icw_Rept_Driver_Updt_Tbl_Rpt_End_Date);                                                                          //Natural: MOVE ICW-REPT-DRIVER-UPDT-TBL.RPT-END-DATE TO ICW-REPT-DRIVER-UPDT-TBL.RPT-START-DATE
        icw_Rept_Driver_Updt_Tbl_Start_Date_Dd.setValue("01");                                                                                                            //Natural: MOVE '01' TO ICW-REPT-DRIVER-UPDT-TBL.START-DATE-DD
        getReports().write(0, icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date,icw_Rept_Driver_Updt_Tbl_Rpt_End_Date);                                                             //Natural: WRITE ICW-REPT-DRIVER-UPDT-TBL.RPT-START-DATE ICW-REPT-DRIVER-UPDT-TBL.RPT-END-DATE
        if (Global.isEscape()) return;
        //* *************
        //*  GET-MONTH-BEGIN-DATE
    }
    private void sub_Get_Qtr_Begin_Date() throws Exception                                                                                                                //Natural: GET-QTR-BEGIN-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *
        icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date.setValue(icw_Rept_Driver_Updt_Tbl_Rpt_End_Date);                                                                          //Natural: MOVE ICW-REPT-DRIVER-UPDT-TBL.RPT-END-DATE TO ICW-REPT-DRIVER-UPDT-TBL.RPT-START-DATE
        icw_Rept_Driver_Updt_Tbl_Start_Date_Mm_Nn.nsubtract(2);                                                                                                           //Natural: ASSIGN ICW-REPT-DRIVER-UPDT-TBL.START-DATE-MM-NN := ICW-REPT-DRIVER-UPDT-TBL.START-DATE-MM-NN - 2
        icw_Rept_Driver_Updt_Tbl_Start_Date_Dd.setValue("01");                                                                                                            //Natural: MOVE '01' TO START-DATE-DD
        getReports().write(0, icw_Rept_Driver_Updt_Tbl_Rpt_Start_Date,icw_Rept_Driver_Updt_Tbl_Rpt_End_Date);                                                             //Natural: WRITE ICW-REPT-DRIVER-UPDT-TBL.RPT-START-DATE ICW-REPT-DRIVER-UPDT-TBL.RPT-END-DATE
        if (Global.isEscape()) return;
        //* *************
        //*  GET-QTR-BEGIN-DATE
    }
    private void sub_Get_Stats_Report() throws Exception                                                                                                                  //Natural: GET-STATS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Tbl_Prime_Key_2_Pnd_Tbl_Key_Field.setValue(" ");                                                                                                              //Natural: MOVE ' ' TO #TBL-PRIME-KEY-2.#TBL-KEY-FIELD
        pnd_No_Records_Found.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NO-RECORDS-FOUND
        vw_icw_Rept_Driver_Tbl.startDatabaseRead                                                                                                                          //Natural: READ ICW-REPT-DRIVER-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY-2
        (
        "READ_FOR_STATS",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key_2, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ_FOR_STATS:
        while (condition(vw_icw_Rept_Driver_Tbl.readNextRow("READ_FOR_STATS")))
        {
            if (condition(icw_Rept_Driver_Tbl_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_2_Pnd_Tbl_Table_Nme)))                                                            //Natural: IF ICW-REPT-DRIVER-TBL.TBL-TABLE-NME NE #TBL-PRIME-KEY-2.#TBL-TABLE-NME
            {
                if (true) break READ_FOR_STATS;                                                                                                                           //Natural: ESCAPE BOTTOM ( READ-FOR-STATS. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rpt_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #RPT-CNTR
            if (condition(icw_Rept_Driver_Tbl_Run_Flag.equals(" ")))                                                                                                      //Natural: IF ICW-REPT-DRIVER-TBL.RUN-FLAG EQ ' '
            {
                pnd_Message.setValue(DbsUtil.compress("Previous", pnd_Report_Type_Pnd_Report_Array.getValue(pnd_Rpt_Cntr), "report run did not complete successfully"));  //Natural: COMPRESS 'Previous' #REPORT-ARRAY ( #RPT-CNTR ) 'report run did not complete successfully' INTO #MESSAGE
                                                                                                                                                                          //Natural: PERFORM WRITE-STATS-REPORT
                sub_Write_Stats_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FOR_STATS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FOR_STATS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  SET SWITCH SO WE KNOW WHAT DATES ARE BEING OVERRIDDEN
            if (condition(icw_Rept_Driver_Tbl_Override_Flag.notEquals(" ")))                                                                                              //Natural: IF ICW-REPT-DRIVER-TBL.OVERRIDE-FLAG NE ' '
            {
                pnd_Override_Tbl.getValue(pnd_Rpt_Cntr).setValue(true);                                                                                                   //Natural: MOVE TRUE TO #OVERRIDE-TBL ( #RPT-CNTR )
                pnd_Override_Start_Dte.getValue(pnd_Rpt_Cntr).setValue(icw_Rept_Driver_Tbl_Rpt_Start_Date);                                                               //Natural: MOVE ICW-REPT-DRIVER-TBL.RPT-START-DATE TO #OVERRIDE-START-DTE ( #RPT-CNTR )
                pnd_Override_End_Dte.getValue(pnd_Rpt_Cntr).setValue(icw_Rept_Driver_Tbl_Rpt_End_Date);                                                                   //Natural: MOVE ICW-REPT-DRIVER-TBL.RPT-END-DATE TO #OVERRIDE-END-DTE ( #RPT-CNTR )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *************
        //*  GET-STATS
    }
    private void sub_Write_Error() throws Exception                                                                                                                       //Natural: WRITE-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(2, "Error Message",                                                                                                                          //Natural: DISPLAY ( 2 ) 'Error Message' #MESSAGE
        		pnd_Message);
        if (Global.isEscape()) return;
        if (condition(pnd_Terminate.getBoolean()))                                                                                                                        //Natural: IF #TERMINATE
        {
            DbsUtil.terminate(35);  if (true) return;                                                                                                                     //Natural: TERMINATE 35
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  WRITE-STATS-REPORT
    }
    private void sub_Write_Stats_Report() throws Exception                                                                                                                //Natural: WRITE-STATS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(3, "Message",                                                                                                                                //Natural: DISPLAY ( 3 ) 'Message' #MESSAGE
        		pnd_Message);
        if (Global.isEscape()) return;
        //* *************
        //*  WRITE-STATS-REPORT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(51),"Institutional Corporate Reporting",new TabSetting(125),"Page",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 51T 'Institutional Corporate Reporting' 125T 'Page' *PAGE-NUMBER ( 1 ) ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 52T 'Report Dates Being Used' 125T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(52),"Report Dates Being Used",new TabSetting(125),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(50),"Institutional Corporate Reporting",new TabSetting(125),"Page",getReports().getPageNumberDbs(2),  //Natural: WRITE ( 2 ) NOTITLE *PROGRAM 50T 'Institutional Corporate Reporting' 125T 'Page' *PAGE-NUMBER ( 2 ) ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 61T 'Error Report' 125T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(61),"Error Report",new TabSetting(125),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(50),"Institutional Corporate Reporting",new TabSetting(124),"Page",getReports().getPageNumberDbs(3),  //Natural: WRITE ( 3 ) NOTITLE *PROGRAM 50T 'Institutional Corporate Reporting' 124T 'Page' *PAGE-NUMBER ( 3 ) ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 54T 'Previous Report Run Report' 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(54),"Previous Report Run Report",new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(3, 1);                                                                                                                              //Natural: SKIP ( 3 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=53 LS=133");
        Global.format(2, "PS=53 LS=133");
        Global.format(3, "PS=53 LS=133");

        getReports().setDisplayColumns(1, "DAILY/DATE",
        		pnd_Disp_Daily_Dte, new ReportEditMask ("YYYYMMDD"),"START DATE/& TIME:",
        		pnd_Start_Dte_Tme, new ReportEditMask ("MM/DD/YY"),"WEEKEND/PROC DATE",
        		pnd_Week_End_D, new ReportEditMask ("YYYYMMDD"),"MONTHEND/PROC DATE",
        		pnd_Month_End_D, new ReportEditMask ("YYYYMMDD"),"QUARTEREND/PROC DATE",
        		pnd_Qtr_End_D, new ReportEditMask ("YYYYMMDD"),"DAY OF/WEEK/NAME ",
        		pnd_Sdte_Dayofweek, new AlphanumericLength (3),"REPORTS TO RUN",
        		pnd_Remarks, new AlphanumericLength (25),"HOLIDAY/NAME",
        		pnd_Sdte_Holiday,"OVERRIDE/START DATES",
        		pnd_Override_Start_Dte,"OVERRIDE/ENE DATES",
        		pnd_Override_End_Dte);
        getReports().setDisplayColumns(2, "Error Message",
        		pnd_Message);
        getReports().setDisplayColumns(3, "Message",
        		pnd_Message);
    }
}
