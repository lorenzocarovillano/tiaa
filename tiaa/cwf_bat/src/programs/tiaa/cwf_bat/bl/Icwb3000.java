/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:39:01 PM
**        * FROM NATURAL PROGRAM : Icwb3000
************************************************************
**        * FILE NAME            : Icwb3000.java
**        * CLASS NAME           : Icwb3000
**        * INSTANCE NAME        : Icwb3000
************************************************************
***********************************************************
* PROGRAM : ICWB3000
* AUTHOR  : LEON EPSTEIN
* DATE    : JUNE 24, 1997
* FUNCTION: EXTRACTS THE DATA FROM ICW-MASTER-INDEX,ICW-ACTIVITY,
*         | IIS-PREM-PYMNT-GRP AND IIS-ORGNZTN-HRCHY FILE
*         | FOR GQL SERVER REPORTING
*         |
*         � THE FOLLOWING FILES WILL BE CREATED:
*         � 1. UPDATED WORK REQUEST   432  BYTES LONG
*         � 2. ADDED   WORK REQUEST   432  BYTES LONG
*         � 3. UPDATED  ACTIVITY      194  BYTES LONG
*         � 4. ADDED ACTIVITY         194  BYTES LONG
*         � 5. SPECIAL ACTIVITY        89  BYTES LONG
*         � 6. PPG                     75  BYTES LONG
*         � 7. INSTITUTION             73  BYTES LONG
*         � 8. COUNT FILE              96  BYTES LONG
*         � 9. WR-REASON-CDE-UPDATE    36  BYTES LONG
*         �10. WR-REASON-CDE-ADD       36  BYTES LONG
*         |
*         |
* CHANGES : L.E. 04/03/98 WORK REQUEST FILES WHERE EXPANDED BY 4
*         | BYTES AT THE END.
* CHANGES : L.E. 12/08/98 COUNT FILE HAS BEEN EXPANDED BY 25 BYTES
* CHANGES : JVH. 02/22/99 ADD LOGIC TO FIX TIAA-RCVD-DTE AND TRADE DTE
*                (SCAN FOR JVH)
* CHANGES : JVH. 02/23/99 CHECK TRADE DATE AGAINST TRADE DATE WE're
*           DOWNLOADING - IF ORIGINAL IS GREATER THAN USE IT
* CHANGES : JVH. 02/25/99 -CHECK TRADE DATE AGAINST CLOSED DATE - IF
*           IT's less use original trade date from IMIT
* CHANGES : L.E. 06/20/99 -ADDED FIELDS TO A WORK REQUESTS
* CHANGES : L.E. 08/03/99 -ADDED PAS FIELDS TO A WORK REQUESTS
*         : CREATED 2(TWO) REASON FILES
* CHANGES : L.E. 09/99 THE MASK FOR DATES HAVE BEEN CHANGED FROM
*         :      (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
*    *********TO (EM='"'LLL�DD�YYYY�HH':'II':'SSAP'"') FOR ORACLE
*         :   TO (EM='"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"')
* CHANGES : L.E. 03/01 THE LIAISON-UNIT-CDE HAS BEEN ADDED TO PPG-TBL
*
*           JVH  08/01 - CHANGE READ OF INSTITUTION TABLE TO PHYSICAL
*                        TAKE THE WHOLE TABLE EVERY NIGHT
*           JVH  03/02 - ADD LAISON-UNIT-CDE TO VIEW IN LOCAL ICWL3014
*                        SO IT IS PICKED UP IN THE MOVE BY NAME
*********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwb3000 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaIcwa3000 pdaIcwa3000;
    private LdaIcwl3000 ldaIcwl3000;
    private LdaIcwl3010 ldaIcwl3010;
    private LdaIcwl3014 ldaIcwl3014;
    private LdaIcwl3015 ldaIcwl3015;
    private LdaIcwl3016 ldaIcwl3016;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Start_Key_From;

    private DbsGroup pnd_Start_Key_From__R_Field_1;
    private DbsField pnd_Start_Key_From_Pnd_Crprte_Status_Ind;
    private DbsField pnd_Start_Key_From_Pnd_Invrt_Strtng_Event;
    private DbsField pnd_Start_Key_From_Pnd_Actve_Ind;
    private DbsField pnd_Starting_A8;

    private DbsGroup pnd_Starting_A8__R_Field_2;
    private DbsField pnd_Starting_A8_Pnd_Starting_T;

    private DbsGroup pnd_Time_Window;
    private DbsField pnd_Time_Window_Pnd_Starting_Numeric;

    private DbsGroup pnd_Time_Window__R_Field_3;
    private DbsField pnd_Time_Window_Pnd_Starting_Alpha;
    private DbsField pnd_Time_Window_Pnd_Ending_Numeric;

    private DbsGroup pnd_Time_Window__R_Field_4;
    private DbsField pnd_Time_Window_Pnd_Ending_Alpha;
    private DbsField pnd_Ending_Event_Alpha;

    private DbsGroup pnd_Ending_Event_Alpha__R_Field_5;
    private DbsField pnd_Ending_Event_Alpha_Pnd_Ending_Event_Numeric;
    private DbsField pnd_Audit_Mdfy_Alpha;

    private DbsGroup pnd_Audit_Mdfy_Alpha__R_Field_6;
    private DbsField pnd_Audit_Mdfy_Alpha_Pnd_Audit_Mdfy_Numeric;
    private DbsField pnd_Atsign;

    private DataAccessProgramView vw_hst_View;
    private DbsField hst_View_Rqst_History_Key;

    private DbsGroup hst_View__R_Field_7;
    private DbsField hst_View_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Compare_Dte_Tme;

    private DbsGroup pnd_Compare_Dte_Tme__R_Field_8;
    private DbsField pnd_Compare_Dte_Tme_Pnd_Compare_Dte_Tme_N;
    private DbsField pnd_Nines;
    private DbsField pnd_Invrt_Start;
    private DbsField pnd_Invrt_End;
    private DbsField pnd_Time;
    private DbsField pnd_Date;
    private DbsField pnd_End_Time_T;
    private DbsField pnd_Add_Updt_Sw;
    private DbsField pnd_Ppg_Code;
    private DbsField pnd_Ppg_Code_No;
    private DbsField pnd_Ppg_Name;
    private DbsField pnd_Psi_Code;
    private DbsField pnd_Psi_Name;
    private DbsField pnd_Ny_Denver_Ind;
    private DbsField pnd_Pbs_Apps_Ind;
    private DbsField pnd_Prem_Grp_Cde;
    private DbsField pnd_Corp_Trnr_Clndr_Days;
    private DbsField pnd_Corp_Trnr_Clndr_Start;
    private DbsField pnd_Rqst_Log_Dte_Tme_T;
    private DbsField pnd_Check_Trade_Dte_Tme;
    private DbsField pnd_Isn_Save;
    private DbsField pnd_Skip_Status_Sw;

    private DbsGroup work_File_1_2;
    private DbsField work_File_1_2_Rqst_Log_Dte_Tme;
    private DbsField work_File_1_2_A01;
    private DbsField work_File_1_2_Work_Prcss_Id;
    private DbsField work_File_1_2_A02;
    private DbsField work_File_1_2_Owner_Unit;
    private DbsField work_File_1_2_A03;
    private DbsField work_File_1_2_Iis_Instn_Link_Cde;
    private DbsField work_File_1_2_A04;
    private DbsField work_File_1_2_Ppg_Cde;
    private DbsField work_File_1_2_A05;
    private DbsField work_File_1_2_Tiaa_Rcvd_Dte_Tme;
    private DbsField work_File_1_2_A06;
    private DbsField work_File_1_2_Rqst_Log_Unit_Cde;
    private DbsField work_File_1_2_A07;
    private DbsField work_File_1_2_Rqst_Log_Oprtr_Cde;
    private DbsField work_File_1_2_A08;
    private DbsField work_File_1_2_Rqst_Orgn_Cde;
    private DbsField work_File_1_2_A09;
    private DbsField work_File_1_2_Crprte_Status_Ind;
    private DbsField work_File_1_2_A10;
    private DbsField work_File_1_2_Sap_Ind;
    private DbsField work_File_1_2_A11;
    private DbsField work_File_1_2_List_Data_Rcvd_Dte;
    private DbsField work_File_1_2_A12;
    private DbsField work_File_1_2_Prtcpnt_Dte;
    private DbsField work_File_1_2_A13;
    private DbsField work_File_1_2_Schdle_Pay_Dte;
    private DbsField work_File_1_2_A14;
    private DbsField work_File_1_2_Multi_Rqst_Ind;
    private DbsField work_File_1_2_A15;
    private DbsField work_File_1_2_Elctrn_Fld_Ind;
    private DbsField work_File_1_2_A16;
    private DbsField work_File_1_2_Check_Ind;
    private DbsField work_File_1_2_A17;
    private DbsField work_File_1_2_Applctns_Rcvd_Ind;
    private DbsField work_File_1_2_A18;
    private DbsField work_File_1_2_Dcmnt_Anttn_Ind;
    private DbsField work_File_1_2_A19;
    private DbsField work_File_1_2_Img_Paper_Prcssng_Ind;
    private DbsField work_File_1_2_A20;
    private DbsField work_File_1_2_Prcssng_Type_Ind;
    private DbsField work_File_1_2_A21;
    private DbsField work_File_1_2_Paid_As_Billed_Ind;
    private DbsField work_File_1_2_A22;
    private DbsField work_File_1_2_Cmplnt_Ind;
    private DbsField work_File_1_2_A23;
    private DbsField work_File_1_2_List_Page_Cnt;
    private DbsField work_File_1_2_A24;
    private DbsField work_File_1_2_Iis_Hrchy_Level_Cde;
    private DbsField work_File_1_2_A25;
    private DbsField work_File_1_2_Iis_Rgn_Cde;
    private DbsField work_File_1_2_A26;
    private DbsField work_File_1_2_Iis_Spcl_Dsgntn_Ind;
    private DbsField work_File_1_2_A27;
    private DbsField work_File_1_2_Iis_Tiaa_Brnch_Cde;
    private DbsField work_File_1_2_A28;
    private DbsField work_File_1_2_Ny_Denver_Ind;
    private DbsField work_File_1_2_A29;
    private DbsField work_File_1_2_Pbs_Apps_Ind;
    private DbsField work_File_1_2_A30;
    private DbsField work_File_1_2_Trade_Dte_Tme;
    private DbsField work_File_1_2_A31;
    private DbsField work_File_1_2_Corp_Wait_Days;
    private DbsField work_File_1_2_A32;
    private DbsField work_File_1_2_Corp_Process_Days;
    private DbsField work_File_1_2_A33;
    private DbsField work_File_1_2_Crprte_Due_Dte_Tme;
    private DbsField work_File_1_2_A34;
    private DbsField work_File_1_2_Instn_Close_Dte_Tme;
    private DbsField work_File_1_2_A35;
    private DbsField work_File_1_2_Crprte_On_Tme_Ind;
    private DbsField work_File_1_2_A36;
    private DbsField work_File_1_2_Corp_Trnr_Buss_Days;
    private DbsField work_File_1_2_A37;
    private DbsField work_File_1_2_Corp_Trnr_Clndr_Days;
    private DbsField work_File_1_2_A38;
    private DbsField work_File_1_2_Crprte_Close_Dte_Tme;
    private DbsField work_File_1_2_A39;
    private DbsField work_File_1_2_Corp_Wait_Days_Overall;
    private DbsField work_File_1_2_A40;
    private DbsField work_File_1_2_Corp_Process_Days_Overall;
    private DbsField work_File_1_2_A41;
    private DbsField work_File_1_2_Step_Re_Do_Ind;
    private DbsField work_File_1_2_A42;
    private DbsField work_File_1_2_Rt_Sqnce_Nbr;
    private DbsField work_File_1_2_A43;
    private DbsField work_File_1_2_Unit_Cde;
    private DbsField work_File_1_2_A44;
    private DbsField work_File_1_2_Step_Id;
    private DbsField work_File_1_2_A45;
    private DbsField work_File_1_2_Racf_Id;
    private DbsField work_File_1_2_A46;
    private DbsField work_File_1_2_Status_Cde;
    private DbsField work_File_1_2_A47;
    private DbsField work_File_1_2_Dummy;
    private DbsField work_File_1_2_A48;
    private DbsField work_File_1_2_Cash_Match_Ind;
    private DbsField work_File_1_2_A49;
    private DbsField work_File_1_2_Iris_Ind;
    private DbsField work_File_1_2_A50;
    private DbsField work_File_1_2_Cnnctd_Rqst_Ind;
    private DbsField work_File_1_2_A51;
    private DbsField work_File_1_2_Init_Undrlyng_Rqst_Cnt;
    private DbsField work_File_1_2_A52;
    private DbsField work_File_1_2_Crrnt_Cmt_Ind;
    private DbsField work_File_1_2_A53;
    private DbsField work_File_1_2_Log_Sqnce_Nbr;
    private DbsField work_File_1_2_A54;
    private DbsField work_File_1_2_Alwys_Vrfy;
    private DbsField work_File_1_2_A55;
    private DbsField work_File_1_2_Crs_Memo;
    private DbsField work_File_1_2_A56;
    private DbsField work_File_1_2_Pas_Ind;
    private DbsField work_File_1_2_A57;
    private DbsField work_File_1_2_Pas_Ovrrde;
    private DbsField work_File_1_2_A58;
    private DbsField work_File_1_2_Work_Rqst_Prty_Cde;

    private DbsGroup work_File_3_4;
    private DbsField work_File_3_4_Rqst_Log_Dte_Tme;
    private DbsField work_File_3_4_B01;
    private DbsField work_File_3_4_Activity_Type;
    private DbsField work_File_3_4_B02;
    private DbsField work_File_3_4_Strtng_Event_Dte_Tme;
    private DbsField work_File_3_4_B03;
    private DbsField work_File_3_4_Ending_Event_Dte_Tme;
    private DbsField work_File_3_4_B04;
    private DbsField work_File_3_4_Rt_Sqnce_Nbr;
    private DbsField work_File_3_4_B05;
    private DbsField work_File_3_4_Unit_Cde;
    private DbsField work_File_3_4_B06;
    private DbsField work_File_3_4_Step_Id;
    private DbsField work_File_3_4_B07;
    private DbsField work_File_3_4_Racf_Id;
    private DbsField work_File_3_4_B08;
    private DbsField work_File_3_4_Strtng_Status_Cde;
    private DbsField work_File_3_4_B09;
    private DbsField work_File_3_4_Ending_Status_Cde;
    private DbsField work_File_3_4_B10;
    private DbsField work_File_3_4_Actvty_Wait_Days;
    private DbsField work_File_3_4_B11;
    private DbsField work_File_3_4_Actvty_Process_Days;
    private DbsField work_File_3_4_B12;
    private DbsField work_File_3_4_Unit_Turnaround_Start;
    private DbsField work_File_3_4_B13;
    private DbsField work_File_3_4_Unit_Turnaround_Days;
    private DbsField work_File_3_4_B14;
    private DbsField work_File_3_4_Unit_On_Tme_Ind;

    private DbsGroup work_File_5;
    private DbsField work_File_5_Rqst_Log_Dte_Tme;
    private DbsField work_File_5_C01;
    private DbsField work_File_5_Spcl_Prcss_Ind;
    private DbsField work_File_5_C02;
    private DbsField work_File_5_Ending_Event_Dte_Tme;
    private DbsField work_File_5_C03;
    private DbsField work_File_5_Spcl_Prcss_Log_Dte_Tme;

    private DbsGroup work_File_6;
    private DbsField work_File_6_Orgnztn_Hrchy_Link_Instn_Cde;
    private DbsField work_File_6_D01;
    private DbsField work_File_6_Ppg_Cde;
    private DbsField work_File_6_D02;
    private DbsField work_File_6_Ppg_Nme;
    private DbsField work_File_6_D03;
    private DbsField work_File_6_Rmttr_Type_Cde;
    private DbsField work_File_6_D04;
    private DbsField work_File_6_Remit_Locn_Cde;
    private DbsField work_File_6_D05;
    private DbsField work_File_6_Prem_Process_Cde;
    private DbsField work_File_6_D06;
    private DbsField work_File_6_Prem_Grp_Cde;
    private DbsField work_File_6_D07;
    private DbsField work_File_6_Liaison_Unit_Cde;

    private DbsGroup work_File_7;
    private DbsField work_File_7_Orgnztn_Hrchy_Lvl_Cde;
    private DbsField work_File_7_E01;
    private DbsField work_File_7_Orgnztn_Hrchy_Link_Public_Cde;
    private DbsField work_File_7_E02;
    private DbsField work_File_7_Orgnztn_Hrchy_Link_System_Cde;
    private DbsField work_File_7_E03;
    private DbsField work_File_7_Orgnztn_Hrchy_Key_Cde;
    private DbsField work_File_7_E04;
    private DbsField work_File_7_Orgnztn_Legal_Nme;
    private DbsField work_File_7_E05;
    private DbsField work_File_7_Orgnztn_Spcl_Needs_Ind;
    private DbsField work_File_7_E06;
    private DbsField work_File_7_Orgnztn_Prty_Ind;
    private DbsField work_File_7_E07;
    private DbsField work_File_7_Tiaa_Brnch_Cde;
    private DbsField work_File_7_E08;
    private DbsField work_File_7_State_Cde;

    private DbsGroup work_File_8;
    private DbsField work_File_8_Pnd_Tot_File_1;
    private DbsField work_File_8_F01;
    private DbsField work_File_8_Pnd_Tot_File_2;
    private DbsField work_File_8_F02;
    private DbsField work_File_8_Pnd_Tot_File_3;
    private DbsField work_File_8_F03;
    private DbsField work_File_8_Pnd_Tot_File_4;
    private DbsField work_File_8_F04;
    private DbsField work_File_8_Pnd_Tot_File_5;
    private DbsField work_File_8_F05;
    private DbsField work_File_8_Pnd_Tot_File_6;
    private DbsField work_File_8_F06;
    private DbsField work_File_8_Pnd_Tot_File_7;
    private DbsField work_File_8_F07;
    private DbsField work_File_8_Pnd_Tot_File_8;
    private DbsField work_File_8_F08;
    private DbsField work_File_8_Pnd_Tot_File_9;
    private DbsField work_File_8_F09;
    private DbsField work_File_8_Pnd_Run_Dte_Tme;

    private DbsGroup work_File_9_10;
    private DbsField work_File_9_10_Rqst_Log_Dte_Tme;
    private DbsField work_File_9_10_G01;
    private DbsField work_File_9_10_Reason_Cde;
    private DbsField work_File_9_10_G02;
    private DbsField work_File_9_10_Seqnce_Nbr;
    private DbsField pnd_Corp_Process_Days_Overall;
    private DbsField pnd_Calc_Days;
    private DbsField pnd_Activity_Type;
    private DbsField pnd_Unit_Actvity_Cntr;
    private DbsField pnd_Empl_Actvity_Cntr;
    private DbsField pnd_Step_Actvity_Cntr;
    private DbsField pnd_Status_Actvity_Cntr;
    private DbsField pnd_Up_Wr_Cntr;
    private DbsField pnd_Ad_Wr_Cntr;
    private DbsField pnd_Up_Ac_Cntr;
    private DbsField pnd_Op_Ac_Cntr;
    private DbsField pnd_Cl_Ac_Cntr;
    private DbsField pnd_Ad_Ac_Cntr;
    private DbsField pnd_Sp_Ac_Cntr;
    private DbsField pnd_Ppg_Cntr;
    private DbsField pnd_Ppg_Rejected;
    private DbsField pnd_Org_Cntr;
    private DbsField pnd_Skip_Cntr;
    private DbsField pnd_Up_Rsn_Temp_Cntr;
    private DbsField pnd_Up_Rsn_Cde_Cntr;
    private DbsField pnd_Ad_Rsn_Temp_Cntr;
    private DbsField pnd_Ad_Rsn_Cde_Cntr;
    private DbsField pnd_I;
    private DbsField pnd_Run_Id;
    private DbsField pnd_Page_No;
    private DbsField pnd_Max_Rec;
    private DbsField pnd_Rqst_History_Key;

    private DbsGroup pnd_Rqst_History_Key__R_Field_9;
    private DbsField pnd_Rqst_History_Key_Pnd_Ky_Rqst_Log_Dte_Tme;
    private DbsField pnd_Rqst_History_Key_Pnd_Ky_Invrt_Strtng_Event;
    private DbsField pnd_Tiaa_Rcvd_Dte_Tme_A;

    private DbsGroup pnd_Tiaa_Rcvd_Dte_Tme_A__R_Field_10;
    private DbsField pnd_Tiaa_Rcvd_Dte_Tme_A_Pnd_Tiaa_Rcvd_Dte_Dte;
    private DbsField pnd_Tiaa_Rcvd_Dte_Tme_A_Pnd_Tiaa_Rcvd_Dte_Tme;
    private DbsField pnd_Eight_Oclock;
    private DbsField pnd_Restart_Invrt_Start_A;

    private DbsGroup pnd_Restart_Invrt_Start_A__R_Field_11;
    private DbsField pnd_Restart_Invrt_Start_A_Pnd_Restart_Invrt_Start_N;

    private DataAccessProgramView vw_icw_Imit;
    private DbsField icw_Imit_Rqst_Log_Dte_Tme;

    private DbsGroup icw_Imit__R_Field_12;
    private DbsField icw_Imit_Rqst_Log_Index_Dte;
    private DbsField icw_Imit_Rqst_Log_Index_Tme;
    private DbsField icw_Imit_Crprte_Status_Ind;
    private DbsField icw_Imit_Tiaa_Rcvd_Dte_Tme;
    private DbsField icw_Imit_Instn_Close_Dte_Tme;
    private DbsField icw_Imit_Crprte_Close_Dte_Tme;
    private DbsField icw_Imit_Strtng_Event_Dte_Tme;
    private DbsField icw_Imit_Invrt_Strtng_Event;
    private DbsField icw_Imit_Endng_Event_Dte_Tme;
    private DbsField icw_Imit_Trade_Dte_Tme;
    private DbsField icw_Imit_Status_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaIcwa3000 = new PdaIcwa3000(localVariables);
        ldaIcwl3000 = new LdaIcwl3000();
        registerRecord(ldaIcwl3000);
        registerRecord(ldaIcwl3000.getVw_cwf_Support_Tbl());
        ldaIcwl3010 = new LdaIcwl3010();
        registerRecord(ldaIcwl3010);
        registerRecord(ldaIcwl3010.getVw_icw_Master_Index_View());
        ldaIcwl3014 = new LdaIcwl3014();
        registerRecord(ldaIcwl3014);
        registerRecord(ldaIcwl3014.getVw_iis_Prem_Pymnt_Grp_View());
        ldaIcwl3015 = new LdaIcwl3015();
        registerRecord(ldaIcwl3015);
        registerRecord(ldaIcwl3015.getVw_icw_Activity_View());
        ldaIcwl3016 = new LdaIcwl3016();
        registerRecord(ldaIcwl3016);
        registerRecord(ldaIcwl3016.getVw_iis_Orgnztn_Hrchy_View());

        // Local Variables
        pnd_Start_Key_From = localVariables.newFieldInRecord("pnd_Start_Key_From", "#START-KEY-FROM", FieldType.STRING, 17);

        pnd_Start_Key_From__R_Field_1 = localVariables.newGroupInRecord("pnd_Start_Key_From__R_Field_1", "REDEFINE", pnd_Start_Key_From);
        pnd_Start_Key_From_Pnd_Crprte_Status_Ind = pnd_Start_Key_From__R_Field_1.newFieldInGroup("pnd_Start_Key_From_Pnd_Crprte_Status_Ind", "#CRPRTE-STATUS-IND", 
            FieldType.STRING, 1);
        pnd_Start_Key_From_Pnd_Invrt_Strtng_Event = pnd_Start_Key_From__R_Field_1.newFieldInGroup("pnd_Start_Key_From_Pnd_Invrt_Strtng_Event", "#INVRT-STRTNG-EVENT", 
            FieldType.NUMERIC, 15);
        pnd_Start_Key_From_Pnd_Actve_Ind = pnd_Start_Key_From__R_Field_1.newFieldInGroup("pnd_Start_Key_From_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Starting_A8 = localVariables.newFieldInRecord("pnd_Starting_A8", "#STARTING-A8", FieldType.STRING, 8);

        pnd_Starting_A8__R_Field_2 = localVariables.newGroupInRecord("pnd_Starting_A8__R_Field_2", "REDEFINE", pnd_Starting_A8);
        pnd_Starting_A8_Pnd_Starting_T = pnd_Starting_A8__R_Field_2.newFieldInGroup("pnd_Starting_A8_Pnd_Starting_T", "#STARTING-T", FieldType.TIME);

        pnd_Time_Window = localVariables.newGroupInRecord("pnd_Time_Window", "#TIME-WINDOW");
        pnd_Time_Window_Pnd_Starting_Numeric = pnd_Time_Window.newFieldInGroup("pnd_Time_Window_Pnd_Starting_Numeric", "#STARTING-NUMERIC", FieldType.NUMERIC, 
            15);

        pnd_Time_Window__R_Field_3 = pnd_Time_Window.newGroupInGroup("pnd_Time_Window__R_Field_3", "REDEFINE", pnd_Time_Window_Pnd_Starting_Numeric);
        pnd_Time_Window_Pnd_Starting_Alpha = pnd_Time_Window__R_Field_3.newFieldInGroup("pnd_Time_Window_Pnd_Starting_Alpha", "#STARTING-ALPHA", FieldType.STRING, 
            15);
        pnd_Time_Window_Pnd_Ending_Numeric = pnd_Time_Window.newFieldInGroup("pnd_Time_Window_Pnd_Ending_Numeric", "#ENDING-NUMERIC", FieldType.NUMERIC, 
            15);

        pnd_Time_Window__R_Field_4 = pnd_Time_Window.newGroupInGroup("pnd_Time_Window__R_Field_4", "REDEFINE", pnd_Time_Window_Pnd_Ending_Numeric);
        pnd_Time_Window_Pnd_Ending_Alpha = pnd_Time_Window__R_Field_4.newFieldInGroup("pnd_Time_Window_Pnd_Ending_Alpha", "#ENDING-ALPHA", FieldType.STRING, 
            15);
        pnd_Ending_Event_Alpha = localVariables.newFieldInRecord("pnd_Ending_Event_Alpha", "#ENDING-EVENT-ALPHA", FieldType.STRING, 15);

        pnd_Ending_Event_Alpha__R_Field_5 = localVariables.newGroupInRecord("pnd_Ending_Event_Alpha__R_Field_5", "REDEFINE", pnd_Ending_Event_Alpha);
        pnd_Ending_Event_Alpha_Pnd_Ending_Event_Numeric = pnd_Ending_Event_Alpha__R_Field_5.newFieldInGroup("pnd_Ending_Event_Alpha_Pnd_Ending_Event_Numeric", 
            "#ENDING-EVENT-NUMERIC", FieldType.NUMERIC, 15);
        pnd_Audit_Mdfy_Alpha = localVariables.newFieldInRecord("pnd_Audit_Mdfy_Alpha", "#AUDIT-MDFY-ALPHA", FieldType.STRING, 15);

        pnd_Audit_Mdfy_Alpha__R_Field_6 = localVariables.newGroupInRecord("pnd_Audit_Mdfy_Alpha__R_Field_6", "REDEFINE", pnd_Audit_Mdfy_Alpha);
        pnd_Audit_Mdfy_Alpha_Pnd_Audit_Mdfy_Numeric = pnd_Audit_Mdfy_Alpha__R_Field_6.newFieldInGroup("pnd_Audit_Mdfy_Alpha_Pnd_Audit_Mdfy_Numeric", "#AUDIT-MDFY-NUMERIC", 
            FieldType.NUMERIC, 15);
        pnd_Atsign = localVariables.newFieldInRecord("pnd_Atsign", "#ATSIGN", FieldType.STRING, 1);

        vw_hst_View = new DataAccessProgramView(new NameInfo("vw_hst_View", "HST-VIEW"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        hst_View_Rqst_History_Key = vw_hst_View.getRecord().newFieldInGroup("hst_View_Rqst_History_Key", "RQST-HISTORY-KEY", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "RQST_HISTORY_KEY");
        hst_View_Rqst_History_Key.setSuperDescriptor(true);

        hst_View__R_Field_7 = vw_hst_View.getRecord().newGroupInGroup("hst_View__R_Field_7", "REDEFINE", hst_View_Rqst_History_Key);
        hst_View_Pnd_Rqst_Log_Dte_Tme = hst_View__R_Field_7.newFieldInGroup("hst_View_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        registerRecord(vw_hst_View);

        pnd_Compare_Dte_Tme = localVariables.newFieldInRecord("pnd_Compare_Dte_Tme", "#COMPARE-DTE-TME", FieldType.STRING, 15);

        pnd_Compare_Dte_Tme__R_Field_8 = localVariables.newGroupInRecord("pnd_Compare_Dte_Tme__R_Field_8", "REDEFINE", pnd_Compare_Dte_Tme);
        pnd_Compare_Dte_Tme_Pnd_Compare_Dte_Tme_N = pnd_Compare_Dte_Tme__R_Field_8.newFieldInGroup("pnd_Compare_Dte_Tme_Pnd_Compare_Dte_Tme_N", "#COMPARE-DTE-TME-N", 
            FieldType.NUMERIC, 15);
        pnd_Nines = localVariables.newFieldInRecord("pnd_Nines", "#NINES", FieldType.NUMERIC, 15);
        pnd_Invrt_Start = localVariables.newFieldInRecord("pnd_Invrt_Start", "#INVRT-START", FieldType.NUMERIC, 15);
        pnd_Invrt_End = localVariables.newFieldInRecord("pnd_Invrt_End", "#INVRT-END", FieldType.NUMERIC, 15);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_End_Time_T = localVariables.newFieldInRecord("pnd_End_Time_T", "#END-TIME-T", FieldType.TIME);
        pnd_Add_Updt_Sw = localVariables.newFieldInRecord("pnd_Add_Updt_Sw", "#ADD-UPDT-SW", FieldType.STRING, 1);
        pnd_Ppg_Code = localVariables.newFieldInRecord("pnd_Ppg_Code", "#PPG-CODE", FieldType.STRING, 6);
        pnd_Ppg_Code_No = localVariables.newFieldInRecord("pnd_Ppg_Code_No", "#PPG-CODE-NO", FieldType.STRING, 1);
        pnd_Ppg_Name = localVariables.newFieldInRecord("pnd_Ppg_Name", "#PPG-NAME", FieldType.STRING, 40);
        pnd_Psi_Code = localVariables.newFieldInRecord("pnd_Psi_Code", "#PSI-CODE", FieldType.NUMERIC, 6);
        pnd_Psi_Name = localVariables.newFieldInRecord("pnd_Psi_Name", "#PSI-NAME", FieldType.STRING, 50);
        pnd_Ny_Denver_Ind = localVariables.newFieldInRecord("pnd_Ny_Denver_Ind", "#NY-DENVER-IND", FieldType.STRING, 1);
        pnd_Pbs_Apps_Ind = localVariables.newFieldInRecord("pnd_Pbs_Apps_Ind", "#PBS-APPS-IND", FieldType.STRING, 1);
        pnd_Prem_Grp_Cde = localVariables.newFieldInRecord("pnd_Prem_Grp_Cde", "#PREM-GRP-CDE", FieldType.NUMERIC, 2);
        pnd_Corp_Trnr_Clndr_Days = localVariables.newFieldInRecord("pnd_Corp_Trnr_Clndr_Days", "#CORP-TRNR-CLNDR-DAYS", FieldType.NUMERIC, 9, 2);
        pnd_Corp_Trnr_Clndr_Start = localVariables.newFieldInRecord("pnd_Corp_Trnr_Clndr_Start", "#CORP-TRNR-CLNDR-START", FieldType.TIME);
        pnd_Rqst_Log_Dte_Tme_T = localVariables.newFieldInRecord("pnd_Rqst_Log_Dte_Tme_T", "#RQST-LOG-DTE-TME-T", FieldType.TIME);
        pnd_Check_Trade_Dte_Tme = localVariables.newFieldInRecord("pnd_Check_Trade_Dte_Tme", "#CHECK-TRADE-DTE-TME", FieldType.TIME);
        pnd_Isn_Save = localVariables.newFieldInRecord("pnd_Isn_Save", "#ISN-SAVE", FieldType.PACKED_DECIMAL, 12);
        pnd_Skip_Status_Sw = localVariables.newFieldInRecord("pnd_Skip_Status_Sw", "#SKIP-STATUS-SW", FieldType.BOOLEAN, 1);

        work_File_1_2 = localVariables.newGroupInRecord("work_File_1_2", "WORK-FILE-1-2");
        work_File_1_2_Rqst_Log_Dte_Tme = work_File_1_2.newFieldInGroup("work_File_1_2_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 28);
        work_File_1_2_A01 = work_File_1_2.newFieldInGroup("work_File_1_2_A01", "A01", FieldType.STRING, 1);
        work_File_1_2_Work_Prcss_Id = work_File_1_2.newFieldInGroup("work_File_1_2_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        work_File_1_2_A02 = work_File_1_2.newFieldInGroup("work_File_1_2_A02", "A02", FieldType.STRING, 1);
        work_File_1_2_Owner_Unit = work_File_1_2.newFieldInGroup("work_File_1_2_Owner_Unit", "OWNER-UNIT", FieldType.STRING, 8);
        work_File_1_2_A03 = work_File_1_2.newFieldInGroup("work_File_1_2_A03", "A03", FieldType.STRING, 1);
        work_File_1_2_Iis_Instn_Link_Cde = work_File_1_2.newFieldInGroup("work_File_1_2_Iis_Instn_Link_Cde", "IIS-INSTN-LINK-CDE", FieldType.NUMERIC, 
            6);
        work_File_1_2_A04 = work_File_1_2.newFieldInGroup("work_File_1_2_A04", "A04", FieldType.STRING, 1);
        work_File_1_2_Ppg_Cde = work_File_1_2.newFieldInGroup("work_File_1_2_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6);
        work_File_1_2_A05 = work_File_1_2.newFieldInGroup("work_File_1_2_A05", "A05", FieldType.STRING, 1);
        work_File_1_2_Tiaa_Rcvd_Dte_Tme = work_File_1_2.newFieldInGroup("work_File_1_2_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.STRING, 28);
        work_File_1_2_A06 = work_File_1_2.newFieldInGroup("work_File_1_2_A06", "A06", FieldType.STRING, 1);
        work_File_1_2_Rqst_Log_Unit_Cde = work_File_1_2.newFieldInGroup("work_File_1_2_Rqst_Log_Unit_Cde", "RQST-LOG-UNIT-CDE", FieldType.STRING, 8);
        work_File_1_2_A07 = work_File_1_2.newFieldInGroup("work_File_1_2_A07", "A07", FieldType.STRING, 1);
        work_File_1_2_Rqst_Log_Oprtr_Cde = work_File_1_2.newFieldInGroup("work_File_1_2_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 8);
        work_File_1_2_A08 = work_File_1_2.newFieldInGroup("work_File_1_2_A08", "A08", FieldType.STRING, 1);
        work_File_1_2_Rqst_Orgn_Cde = work_File_1_2.newFieldInGroup("work_File_1_2_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1);
        work_File_1_2_A09 = work_File_1_2.newFieldInGroup("work_File_1_2_A09", "A09", FieldType.STRING, 1);
        work_File_1_2_Crprte_Status_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1);
        work_File_1_2_A10 = work_File_1_2.newFieldInGroup("work_File_1_2_A10", "A10", FieldType.STRING, 1);
        work_File_1_2_Sap_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Sap_Ind", "SAP-IND", FieldType.STRING, 1);
        work_File_1_2_A11 = work_File_1_2.newFieldInGroup("work_File_1_2_A11", "A11", FieldType.STRING, 1);
        work_File_1_2_List_Data_Rcvd_Dte = work_File_1_2.newFieldInGroup("work_File_1_2_List_Data_Rcvd_Dte", "LIST-DATA-RCVD-DTE", FieldType.STRING, 12);
        work_File_1_2_A12 = work_File_1_2.newFieldInGroup("work_File_1_2_A12", "A12", FieldType.STRING, 1);
        work_File_1_2_Prtcpnt_Dte = work_File_1_2.newFieldInGroup("work_File_1_2_Prtcpnt_Dte", "PRTCPNT-DTE", FieldType.STRING, 12);
        work_File_1_2_A13 = work_File_1_2.newFieldInGroup("work_File_1_2_A13", "A13", FieldType.STRING, 1);
        work_File_1_2_Schdle_Pay_Dte = work_File_1_2.newFieldInGroup("work_File_1_2_Schdle_Pay_Dte", "SCHDLE-PAY-DTE", FieldType.STRING, 12);
        work_File_1_2_A14 = work_File_1_2.newFieldInGroup("work_File_1_2_A14", "A14", FieldType.STRING, 1);
        work_File_1_2_Multi_Rqst_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 1);
        work_File_1_2_A15 = work_File_1_2.newFieldInGroup("work_File_1_2_A15", "A15", FieldType.STRING, 1);
        work_File_1_2_Elctrn_Fld_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Elctrn_Fld_Ind", "ELCTRN-FLD-IND", FieldType.STRING, 1);
        work_File_1_2_A16 = work_File_1_2.newFieldInGroup("work_File_1_2_A16", "A16", FieldType.STRING, 1);
        work_File_1_2_Check_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Check_Ind", "CHECK-IND", FieldType.STRING, 1);
        work_File_1_2_A17 = work_File_1_2.newFieldInGroup("work_File_1_2_A17", "A17", FieldType.STRING, 1);
        work_File_1_2_Applctns_Rcvd_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Applctns_Rcvd_Ind", "APPLCTNS-RCVD-IND", FieldType.STRING, 1);
        work_File_1_2_A18 = work_File_1_2.newFieldInGroup("work_File_1_2_A18", "A18", FieldType.STRING, 1);
        work_File_1_2_Dcmnt_Anttn_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Dcmnt_Anttn_Ind", "DCMNT-ANTTN-IND", FieldType.STRING, 1);
        work_File_1_2_A19 = work_File_1_2.newFieldInGroup("work_File_1_2_A19", "A19", FieldType.STRING, 1);
        work_File_1_2_Img_Paper_Prcssng_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Img_Paper_Prcssng_Ind", "IMG-PAPER-PRCSSNG-IND", FieldType.STRING, 
            1);
        work_File_1_2_A20 = work_File_1_2.newFieldInGroup("work_File_1_2_A20", "A20", FieldType.STRING, 1);
        work_File_1_2_Prcssng_Type_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Prcssng_Type_Ind", "PRCSSNG-TYPE-IND", FieldType.STRING, 1);
        work_File_1_2_A21 = work_File_1_2.newFieldInGroup("work_File_1_2_A21", "A21", FieldType.STRING, 1);
        work_File_1_2_Paid_As_Billed_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Paid_As_Billed_Ind", "PAID-AS-BILLED-IND", FieldType.STRING, 1);
        work_File_1_2_A22 = work_File_1_2.newFieldInGroup("work_File_1_2_A22", "A22", FieldType.STRING, 1);
        work_File_1_2_Cmplnt_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 1);
        work_File_1_2_A23 = work_File_1_2.newFieldInGroup("work_File_1_2_A23", "A23", FieldType.STRING, 1);
        work_File_1_2_List_Page_Cnt = work_File_1_2.newFieldInGroup("work_File_1_2_List_Page_Cnt", "LIST-PAGE-CNT", FieldType.NUMERIC, 3);
        work_File_1_2_A24 = work_File_1_2.newFieldInGroup("work_File_1_2_A24", "A24", FieldType.STRING, 1);
        work_File_1_2_Iis_Hrchy_Level_Cde = work_File_1_2.newFieldInGroup("work_File_1_2_Iis_Hrchy_Level_Cde", "IIS-HRCHY-LEVEL-CDE", FieldType.STRING, 
            1);
        work_File_1_2_A25 = work_File_1_2.newFieldInGroup("work_File_1_2_A25", "A25", FieldType.STRING, 1);
        work_File_1_2_Iis_Rgn_Cde = work_File_1_2.newFieldInGroup("work_File_1_2_Iis_Rgn_Cde", "IIS-RGN-CDE", FieldType.NUMERIC, 2);
        work_File_1_2_A26 = work_File_1_2.newFieldInGroup("work_File_1_2_A26", "A26", FieldType.STRING, 1);
        work_File_1_2_Iis_Spcl_Dsgntn_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Iis_Spcl_Dsgntn_Ind", "IIS-SPCL-DSGNTN-IND", FieldType.STRING, 
            1);
        work_File_1_2_A27 = work_File_1_2.newFieldInGroup("work_File_1_2_A27", "A27", FieldType.STRING, 1);
        work_File_1_2_Iis_Tiaa_Brnch_Cde = work_File_1_2.newFieldInGroup("work_File_1_2_Iis_Tiaa_Brnch_Cde", "IIS-TIAA-BRNCH-CDE", FieldType.NUMERIC, 
            2);
        work_File_1_2_A28 = work_File_1_2.newFieldInGroup("work_File_1_2_A28", "A28", FieldType.STRING, 1);
        work_File_1_2_Ny_Denver_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Ny_Denver_Ind", "NY-DENVER-IND", FieldType.STRING, 1);
        work_File_1_2_A29 = work_File_1_2.newFieldInGroup("work_File_1_2_A29", "A29", FieldType.STRING, 1);
        work_File_1_2_Pbs_Apps_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Pbs_Apps_Ind", "PBS-APPS-IND", FieldType.STRING, 1);
        work_File_1_2_A30 = work_File_1_2.newFieldInGroup("work_File_1_2_A30", "A30", FieldType.STRING, 1);
        work_File_1_2_Trade_Dte_Tme = work_File_1_2.newFieldInGroup("work_File_1_2_Trade_Dte_Tme", "TRADE-DTE-TME", FieldType.STRING, 28);
        work_File_1_2_A31 = work_File_1_2.newFieldInGroup("work_File_1_2_A31", "A31", FieldType.STRING, 1);
        work_File_1_2_Corp_Wait_Days = work_File_1_2.newFieldInGroup("work_File_1_2_Corp_Wait_Days", "CORP-WAIT-DAYS", FieldType.STRING, 9);
        work_File_1_2_A32 = work_File_1_2.newFieldInGroup("work_File_1_2_A32", "A32", FieldType.STRING, 1);
        work_File_1_2_Corp_Process_Days = work_File_1_2.newFieldInGroup("work_File_1_2_Corp_Process_Days", "CORP-PROCESS-DAYS", FieldType.STRING, 9);
        work_File_1_2_A33 = work_File_1_2.newFieldInGroup("work_File_1_2_A33", "A33", FieldType.STRING, 1);
        work_File_1_2_Crprte_Due_Dte_Tme = work_File_1_2.newFieldInGroup("work_File_1_2_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", FieldType.STRING, 28);
        work_File_1_2_A34 = work_File_1_2.newFieldInGroup("work_File_1_2_A34", "A34", FieldType.STRING, 1);
        work_File_1_2_Instn_Close_Dte_Tme = work_File_1_2.newFieldInGroup("work_File_1_2_Instn_Close_Dte_Tme", "INSTN-CLOSE-DTE-TME", FieldType.STRING, 
            28);
        work_File_1_2_A35 = work_File_1_2.newFieldInGroup("work_File_1_2_A35", "A35", FieldType.STRING, 1);
        work_File_1_2_Crprte_On_Tme_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", FieldType.STRING, 1);
        work_File_1_2_A36 = work_File_1_2.newFieldInGroup("work_File_1_2_A36", "A36", FieldType.STRING, 1);
        work_File_1_2_Corp_Trnr_Buss_Days = work_File_1_2.newFieldInGroup("work_File_1_2_Corp_Trnr_Buss_Days", "CORP-TRNR-BUSS-DAYS", FieldType.STRING, 
            9);
        work_File_1_2_A37 = work_File_1_2.newFieldInGroup("work_File_1_2_A37", "A37", FieldType.STRING, 1);
        work_File_1_2_Corp_Trnr_Clndr_Days = work_File_1_2.newFieldInGroup("work_File_1_2_Corp_Trnr_Clndr_Days", "CORP-TRNR-CLNDR-DAYS", FieldType.STRING, 
            9);
        work_File_1_2_A38 = work_File_1_2.newFieldInGroup("work_File_1_2_A38", "A38", FieldType.STRING, 1);
        work_File_1_2_Crprte_Close_Dte_Tme = work_File_1_2.newFieldInGroup("work_File_1_2_Crprte_Close_Dte_Tme", "CRPRTE-CLOSE-DTE-TME", FieldType.STRING, 
            28);
        work_File_1_2_A39 = work_File_1_2.newFieldInGroup("work_File_1_2_A39", "A39", FieldType.STRING, 1);
        work_File_1_2_Corp_Wait_Days_Overall = work_File_1_2.newFieldInGroup("work_File_1_2_Corp_Wait_Days_Overall", "CORP-WAIT-DAYS-OVERALL", FieldType.STRING, 
            9);
        work_File_1_2_A40 = work_File_1_2.newFieldInGroup("work_File_1_2_A40", "A40", FieldType.STRING, 1);
        work_File_1_2_Corp_Process_Days_Overall = work_File_1_2.newFieldInGroup("work_File_1_2_Corp_Process_Days_Overall", "CORP-PROCESS-DAYS-OVERALL", 
            FieldType.STRING, 9);
        work_File_1_2_A41 = work_File_1_2.newFieldInGroup("work_File_1_2_A41", "A41", FieldType.STRING, 1);
        work_File_1_2_Step_Re_Do_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Step_Re_Do_Ind", "STEP-RE-DO-IND", FieldType.STRING, 2);
        work_File_1_2_A42 = work_File_1_2.newFieldInGroup("work_File_1_2_A42", "A42", FieldType.STRING, 1);
        work_File_1_2_Rt_Sqnce_Nbr = work_File_1_2.newFieldInGroup("work_File_1_2_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 2);
        work_File_1_2_A43 = work_File_1_2.newFieldInGroup("work_File_1_2_A43", "A43", FieldType.STRING, 1);
        work_File_1_2_Unit_Cde = work_File_1_2.newFieldInGroup("work_File_1_2_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);
        work_File_1_2_A44 = work_File_1_2.newFieldInGroup("work_File_1_2_A44", "A44", FieldType.STRING, 1);
        work_File_1_2_Step_Id = work_File_1_2.newFieldInGroup("work_File_1_2_Step_Id", "STEP-ID", FieldType.STRING, 6);
        work_File_1_2_A45 = work_File_1_2.newFieldInGroup("work_File_1_2_A45", "A45", FieldType.STRING, 1);
        work_File_1_2_Racf_Id = work_File_1_2.newFieldInGroup("work_File_1_2_Racf_Id", "RACF-ID", FieldType.STRING, 8);
        work_File_1_2_A46 = work_File_1_2.newFieldInGroup("work_File_1_2_A46", "A46", FieldType.STRING, 1);
        work_File_1_2_Status_Cde = work_File_1_2.newFieldInGroup("work_File_1_2_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        work_File_1_2_A47 = work_File_1_2.newFieldInGroup("work_File_1_2_A47", "A47", FieldType.STRING, 1);
        work_File_1_2_Dummy = work_File_1_2.newFieldInGroup("work_File_1_2_Dummy", "DUMMY", FieldType.STRING, 4);
        work_File_1_2_A48 = work_File_1_2.newFieldInGroup("work_File_1_2_A48", "A48", FieldType.STRING, 1);
        work_File_1_2_Cash_Match_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Cash_Match_Ind", "CASH-MATCH-IND", FieldType.STRING, 1);
        work_File_1_2_A49 = work_File_1_2.newFieldInGroup("work_File_1_2_A49", "A49", FieldType.STRING, 1);
        work_File_1_2_Iris_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Iris_Ind", "IRIS-IND", FieldType.STRING, 1);
        work_File_1_2_A50 = work_File_1_2.newFieldInGroup("work_File_1_2_A50", "A50", FieldType.STRING, 1);
        work_File_1_2_Cnnctd_Rqst_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Cnnctd_Rqst_Ind", "CNNCTD-RQST-IND", FieldType.STRING, 2);
        work_File_1_2_A51 = work_File_1_2.newFieldInGroup("work_File_1_2_A51", "A51", FieldType.STRING, 1);
        work_File_1_2_Init_Undrlyng_Rqst_Cnt = work_File_1_2.newFieldInGroup("work_File_1_2_Init_Undrlyng_Rqst_Cnt", "INIT-UNDRLYNG-RQST-CNT", FieldType.NUMERIC, 
            3);
        work_File_1_2_A52 = work_File_1_2.newFieldInGroup("work_File_1_2_A52", "A52", FieldType.STRING, 1);
        work_File_1_2_Crrnt_Cmt_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Crrnt_Cmt_Ind", "CRRNT-CMT-IND", FieldType.STRING, 1);
        work_File_1_2_A53 = work_File_1_2.newFieldInGroup("work_File_1_2_A53", "A53", FieldType.STRING, 1);
        work_File_1_2_Log_Sqnce_Nbr = work_File_1_2.newFieldInGroup("work_File_1_2_Log_Sqnce_Nbr", "LOG-SQNCE-NBR", FieldType.NUMERIC, 3);
        work_File_1_2_A54 = work_File_1_2.newFieldInGroup("work_File_1_2_A54", "A54", FieldType.STRING, 1);
        work_File_1_2_Alwys_Vrfy = work_File_1_2.newFieldInGroup("work_File_1_2_Alwys_Vrfy", "ALWYS-VRFY", FieldType.STRING, 1);
        work_File_1_2_A55 = work_File_1_2.newFieldInGroup("work_File_1_2_A55", "A55", FieldType.STRING, 1);
        work_File_1_2_Crs_Memo = work_File_1_2.newFieldInGroup("work_File_1_2_Crs_Memo", "CRS-MEMO", FieldType.STRING, 1);
        work_File_1_2_A56 = work_File_1_2.newFieldInGroup("work_File_1_2_A56", "A56", FieldType.STRING, 1);
        work_File_1_2_Pas_Ind = work_File_1_2.newFieldInGroup("work_File_1_2_Pas_Ind", "PAS-IND", FieldType.STRING, 1);
        work_File_1_2_A57 = work_File_1_2.newFieldInGroup("work_File_1_2_A57", "A57", FieldType.STRING, 1);
        work_File_1_2_Pas_Ovrrde = work_File_1_2.newFieldInGroup("work_File_1_2_Pas_Ovrrde", "PAS-OVRRDE", FieldType.STRING, 1);
        work_File_1_2_A58 = work_File_1_2.newFieldInGroup("work_File_1_2_A58", "A58", FieldType.STRING, 1);
        work_File_1_2_Work_Rqst_Prty_Cde = work_File_1_2.newFieldInGroup("work_File_1_2_Work_Rqst_Prty_Cde", "WORK-RQST-PRTY-CDE", FieldType.STRING, 1);

        work_File_3_4 = localVariables.newGroupInRecord("work_File_3_4", "WORK-FILE-3-4");
        work_File_3_4_Rqst_Log_Dte_Tme = work_File_3_4.newFieldInGroup("work_File_3_4_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 28);
        work_File_3_4_B01 = work_File_3_4.newFieldInGroup("work_File_3_4_B01", "B01", FieldType.STRING, 1);
        work_File_3_4_Activity_Type = work_File_3_4.newFieldInGroup("work_File_3_4_Activity_Type", "ACTIVITY-TYPE", FieldType.STRING, 8);
        work_File_3_4_B02 = work_File_3_4.newFieldInGroup("work_File_3_4_B02", "B02", FieldType.STRING, 1);
        work_File_3_4_Strtng_Event_Dte_Tme = work_File_3_4.newFieldInGroup("work_File_3_4_Strtng_Event_Dte_Tme", "STRTNG-EVENT-DTE-TME", FieldType.STRING, 
            28);
        work_File_3_4_B03 = work_File_3_4.newFieldInGroup("work_File_3_4_B03", "B03", FieldType.STRING, 1);
        work_File_3_4_Ending_Event_Dte_Tme = work_File_3_4.newFieldInGroup("work_File_3_4_Ending_Event_Dte_Tme", "ENDING-EVENT-DTE-TME", FieldType.STRING, 
            28);
        work_File_3_4_B04 = work_File_3_4.newFieldInGroup("work_File_3_4_B04", "B04", FieldType.STRING, 1);
        work_File_3_4_Rt_Sqnce_Nbr = work_File_3_4.newFieldInGroup("work_File_3_4_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 2);
        work_File_3_4_B05 = work_File_3_4.newFieldInGroup("work_File_3_4_B05", "B05", FieldType.STRING, 1);
        work_File_3_4_Unit_Cde = work_File_3_4.newFieldInGroup("work_File_3_4_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);
        work_File_3_4_B06 = work_File_3_4.newFieldInGroup("work_File_3_4_B06", "B06", FieldType.STRING, 1);
        work_File_3_4_Step_Id = work_File_3_4.newFieldInGroup("work_File_3_4_Step_Id", "STEP-ID", FieldType.STRING, 6);
        work_File_3_4_B07 = work_File_3_4.newFieldInGroup("work_File_3_4_B07", "B07", FieldType.STRING, 1);
        work_File_3_4_Racf_Id = work_File_3_4.newFieldInGroup("work_File_3_4_Racf_Id", "RACF-ID", FieldType.STRING, 8);
        work_File_3_4_B08 = work_File_3_4.newFieldInGroup("work_File_3_4_B08", "B08", FieldType.STRING, 1);
        work_File_3_4_Strtng_Status_Cde = work_File_3_4.newFieldInGroup("work_File_3_4_Strtng_Status_Cde", "STRTNG-STATUS-CDE", FieldType.STRING, 4);
        work_File_3_4_B09 = work_File_3_4.newFieldInGroup("work_File_3_4_B09", "B09", FieldType.STRING, 1);
        work_File_3_4_Ending_Status_Cde = work_File_3_4.newFieldInGroup("work_File_3_4_Ending_Status_Cde", "ENDING-STATUS-CDE", FieldType.STRING, 4);
        work_File_3_4_B10 = work_File_3_4.newFieldInGroup("work_File_3_4_B10", "B10", FieldType.STRING, 1);
        work_File_3_4_Actvty_Wait_Days = work_File_3_4.newFieldInGroup("work_File_3_4_Actvty_Wait_Days", "ACTVTY-WAIT-DAYS", FieldType.STRING, 9);
        work_File_3_4_B11 = work_File_3_4.newFieldInGroup("work_File_3_4_B11", "B11", FieldType.STRING, 1);
        work_File_3_4_Actvty_Process_Days = work_File_3_4.newFieldInGroup("work_File_3_4_Actvty_Process_Days", "ACTVTY-PROCESS-DAYS", FieldType.STRING, 
            9);
        work_File_3_4_B12 = work_File_3_4.newFieldInGroup("work_File_3_4_B12", "B12", FieldType.STRING, 1);
        work_File_3_4_Unit_Turnaround_Start = work_File_3_4.newFieldInGroup("work_File_3_4_Unit_Turnaround_Start", "UNIT-TURNAROUND-START", FieldType.STRING, 
            28);
        work_File_3_4_B13 = work_File_3_4.newFieldInGroup("work_File_3_4_B13", "B13", FieldType.STRING, 1);
        work_File_3_4_Unit_Turnaround_Days = work_File_3_4.newFieldInGroup("work_File_3_4_Unit_Turnaround_Days", "UNIT-TURNAROUND-DAYS", FieldType.STRING, 
            9);
        work_File_3_4_B14 = work_File_3_4.newFieldInGroup("work_File_3_4_B14", "B14", FieldType.STRING, 1);
        work_File_3_4_Unit_On_Tme_Ind = work_File_3_4.newFieldInGroup("work_File_3_4_Unit_On_Tme_Ind", "UNIT-ON-TME-IND", FieldType.STRING, 1);

        work_File_5 = localVariables.newGroupInRecord("work_File_5", "WORK-FILE-5");
        work_File_5_Rqst_Log_Dte_Tme = work_File_5.newFieldInGroup("work_File_5_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 28);
        work_File_5_C01 = work_File_5.newFieldInGroup("work_File_5_C01", "C01", FieldType.STRING, 1);
        work_File_5_Spcl_Prcss_Ind = work_File_5.newFieldInGroup("work_File_5_Spcl_Prcss_Ind", "SPCL-PRCSS-IND", FieldType.STRING, 2);
        work_File_5_C02 = work_File_5.newFieldInGroup("work_File_5_C02", "C02", FieldType.STRING, 1);
        work_File_5_Ending_Event_Dte_Tme = work_File_5.newFieldInGroup("work_File_5_Ending_Event_Dte_Tme", "ENDING-EVENT-DTE-TME", FieldType.STRING, 28);
        work_File_5_C03 = work_File_5.newFieldInGroup("work_File_5_C03", "C03", FieldType.STRING, 1);
        work_File_5_Spcl_Prcss_Log_Dte_Tme = work_File_5.newFieldInGroup("work_File_5_Spcl_Prcss_Log_Dte_Tme", "SPCL-PRCSS-LOG-DTE-TME", FieldType.STRING, 
            28);

        work_File_6 = localVariables.newGroupInRecord("work_File_6", "WORK-FILE-6");
        work_File_6_Orgnztn_Hrchy_Link_Instn_Cde = work_File_6.newFieldInGroup("work_File_6_Orgnztn_Hrchy_Link_Instn_Cde", "ORGNZTN-HRCHY-LINK-INSTN-CDE", 
            FieldType.NUMERIC, 6);
        work_File_6_D01 = work_File_6.newFieldInGroup("work_File_6_D01", "D01", FieldType.STRING, 1);
        work_File_6_Ppg_Cde = work_File_6.newFieldInGroup("work_File_6_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6);
        work_File_6_D02 = work_File_6.newFieldInGroup("work_File_6_D02", "D02", FieldType.STRING, 1);
        work_File_6_Ppg_Nme = work_File_6.newFieldInGroup("work_File_6_Ppg_Nme", "PPG-NME", FieldType.STRING, 40);
        work_File_6_D03 = work_File_6.newFieldInGroup("work_File_6_D03", "D03", FieldType.STRING, 1);
        work_File_6_Rmttr_Type_Cde = work_File_6.newFieldInGroup("work_File_6_Rmttr_Type_Cde", "RMTTR-TYPE-CDE", FieldType.NUMERIC, 2);
        work_File_6_D04 = work_File_6.newFieldInGroup("work_File_6_D04", "D04", FieldType.STRING, 1);
        work_File_6_Remit_Locn_Cde = work_File_6.newFieldInGroup("work_File_6_Remit_Locn_Cde", "REMIT-LOCN-CDE", FieldType.NUMERIC, 2);
        work_File_6_D05 = work_File_6.newFieldInGroup("work_File_6_D05", "D05", FieldType.STRING, 1);
        work_File_6_Prem_Process_Cde = work_File_6.newFieldInGroup("work_File_6_Prem_Process_Cde", "PREM-PROCESS-CDE", FieldType.NUMERIC, 2);
        work_File_6_D06 = work_File_6.newFieldInGroup("work_File_6_D06", "D06", FieldType.STRING, 1);
        work_File_6_Prem_Grp_Cde = work_File_6.newFieldInGroup("work_File_6_Prem_Grp_Cde", "PREM-GRP-CDE", FieldType.NUMERIC, 2);
        work_File_6_D07 = work_File_6.newFieldInGroup("work_File_6_D07", "D07", FieldType.STRING, 1);
        work_File_6_Liaison_Unit_Cde = work_File_6.newFieldInGroup("work_File_6_Liaison_Unit_Cde", "LIAISON-UNIT-CDE", FieldType.STRING, 8);

        work_File_7 = localVariables.newGroupInRecord("work_File_7", "WORK-FILE-7");
        work_File_7_Orgnztn_Hrchy_Lvl_Cde = work_File_7.newFieldInGroup("work_File_7_Orgnztn_Hrchy_Lvl_Cde", "ORGNZTN-HRCHY-LVL-CDE", FieldType.STRING, 
            1);
        work_File_7_E01 = work_File_7.newFieldInGroup("work_File_7_E01", "E01", FieldType.STRING, 1);
        work_File_7_Orgnztn_Hrchy_Link_Public_Cde = work_File_7.newFieldInGroup("work_File_7_Orgnztn_Hrchy_Link_Public_Cde", "ORGNZTN-HRCHY-LINK-PUBLIC-CDE", 
            FieldType.NUMERIC, 6);
        work_File_7_E02 = work_File_7.newFieldInGroup("work_File_7_E02", "E02", FieldType.STRING, 1);
        work_File_7_Orgnztn_Hrchy_Link_System_Cde = work_File_7.newFieldInGroup("work_File_7_Orgnztn_Hrchy_Link_System_Cde", "ORGNZTN-HRCHY-LINK-SYSTEM-CDE", 
            FieldType.NUMERIC, 6);
        work_File_7_E03 = work_File_7.newFieldInGroup("work_File_7_E03", "E03", FieldType.STRING, 1);
        work_File_7_Orgnztn_Hrchy_Key_Cde = work_File_7.newFieldInGroup("work_File_7_Orgnztn_Hrchy_Key_Cde", "ORGNZTN-HRCHY-KEY-CDE", FieldType.NUMERIC, 
            6);
        work_File_7_E04 = work_File_7.newFieldInGroup("work_File_7_E04", "E04", FieldType.STRING, 1);
        work_File_7_Orgnztn_Legal_Nme = work_File_7.newFieldInGroup("work_File_7_Orgnztn_Legal_Nme", "ORGNZTN-LEGAL-NME", FieldType.STRING, 40);
        work_File_7_E05 = work_File_7.newFieldInGroup("work_File_7_E05", "E05", FieldType.STRING, 1);
        work_File_7_Orgnztn_Spcl_Needs_Ind = work_File_7.newFieldInGroup("work_File_7_Orgnztn_Spcl_Needs_Ind", "ORGNZTN-SPCL-NEEDS-IND", FieldType.STRING, 
            1);
        work_File_7_E06 = work_File_7.newFieldInGroup("work_File_7_E06", "E06", FieldType.STRING, 1);
        work_File_7_Orgnztn_Prty_Ind = work_File_7.newFieldInGroup("work_File_7_Orgnztn_Prty_Ind", "ORGNZTN-PRTY-IND", FieldType.STRING, 1);
        work_File_7_E07 = work_File_7.newFieldInGroup("work_File_7_E07", "E07", FieldType.STRING, 1);
        work_File_7_Tiaa_Brnch_Cde = work_File_7.newFieldInGroup("work_File_7_Tiaa_Brnch_Cde", "TIAA-BRNCH-CDE", FieldType.NUMERIC, 2);
        work_File_7_E08 = work_File_7.newFieldInGroup("work_File_7_E08", "E08", FieldType.STRING, 1);
        work_File_7_State_Cde = work_File_7.newFieldInGroup("work_File_7_State_Cde", "STATE-CDE", FieldType.STRING, 2);

        work_File_8 = localVariables.newGroupInRecord("work_File_8", "WORK-FILE-8");
        work_File_8_Pnd_Tot_File_1 = work_File_8.newFieldInGroup("work_File_8_Pnd_Tot_File_1", "#TOT-FILE-1", FieldType.NUMERIC, 7);
        work_File_8_F01 = work_File_8.newFieldInGroup("work_File_8_F01", "F01", FieldType.STRING, 1);
        work_File_8_Pnd_Tot_File_2 = work_File_8.newFieldInGroup("work_File_8_Pnd_Tot_File_2", "#TOT-FILE-2", FieldType.NUMERIC, 7);
        work_File_8_F02 = work_File_8.newFieldInGroup("work_File_8_F02", "F02", FieldType.STRING, 1);
        work_File_8_Pnd_Tot_File_3 = work_File_8.newFieldInGroup("work_File_8_Pnd_Tot_File_3", "#TOT-FILE-3", FieldType.NUMERIC, 7);
        work_File_8_F03 = work_File_8.newFieldInGroup("work_File_8_F03", "F03", FieldType.STRING, 1);
        work_File_8_Pnd_Tot_File_4 = work_File_8.newFieldInGroup("work_File_8_Pnd_Tot_File_4", "#TOT-FILE-4", FieldType.NUMERIC, 7);
        work_File_8_F04 = work_File_8.newFieldInGroup("work_File_8_F04", "F04", FieldType.STRING, 1);
        work_File_8_Pnd_Tot_File_5 = work_File_8.newFieldInGroup("work_File_8_Pnd_Tot_File_5", "#TOT-FILE-5", FieldType.NUMERIC, 7);
        work_File_8_F05 = work_File_8.newFieldInGroup("work_File_8_F05", "F05", FieldType.STRING, 1);
        work_File_8_Pnd_Tot_File_6 = work_File_8.newFieldInGroup("work_File_8_Pnd_Tot_File_6", "#TOT-FILE-6", FieldType.NUMERIC, 7);
        work_File_8_F06 = work_File_8.newFieldInGroup("work_File_8_F06", "F06", FieldType.STRING, 1);
        work_File_8_Pnd_Tot_File_7 = work_File_8.newFieldInGroup("work_File_8_Pnd_Tot_File_7", "#TOT-FILE-7", FieldType.NUMERIC, 7);
        work_File_8_F07 = work_File_8.newFieldInGroup("work_File_8_F07", "F07", FieldType.STRING, 1);
        work_File_8_Pnd_Tot_File_8 = work_File_8.newFieldInGroup("work_File_8_Pnd_Tot_File_8", "#TOT-FILE-8", FieldType.NUMERIC, 7);
        work_File_8_F08 = work_File_8.newFieldInGroup("work_File_8_F08", "F08", FieldType.STRING, 1);
        work_File_8_Pnd_Tot_File_9 = work_File_8.newFieldInGroup("work_File_8_Pnd_Tot_File_9", "#TOT-FILE-9", FieldType.NUMERIC, 7);
        work_File_8_F09 = work_File_8.newFieldInGroup("work_File_8_F09", "F09", FieldType.STRING, 1);
        work_File_8_Pnd_Run_Dte_Tme = work_File_8.newFieldInGroup("work_File_8_Pnd_Run_Dte_Tme", "#RUN-DTE-TME", FieldType.STRING, 24);

        work_File_9_10 = localVariables.newGroupInRecord("work_File_9_10", "WORK-FILE-9-10");
        work_File_9_10_Rqst_Log_Dte_Tme = work_File_9_10.newFieldInGroup("work_File_9_10_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 28);
        work_File_9_10_G01 = work_File_9_10.newFieldInGroup("work_File_9_10_G01", "G01", FieldType.STRING, 1);
        work_File_9_10_Reason_Cde = work_File_9_10.newFieldInGroup("work_File_9_10_Reason_Cde", "REASON-CDE", FieldType.STRING, 4);
        work_File_9_10_G02 = work_File_9_10.newFieldInGroup("work_File_9_10_G02", "G02", FieldType.STRING, 1);
        work_File_9_10_Seqnce_Nbr = work_File_9_10.newFieldInGroup("work_File_9_10_Seqnce_Nbr", "SEQNCE-NBR", FieldType.NUMERIC, 2);
        pnd_Corp_Process_Days_Overall = localVariables.newFieldInRecord("pnd_Corp_Process_Days_Overall", "#CORP-PROCESS-DAYS-OVERALL", FieldType.NUMERIC, 
            8, 2);
        pnd_Calc_Days = localVariables.newFieldInRecord("pnd_Calc_Days", "#CALC-DAYS", FieldType.NUMERIC, 8, 2);
        pnd_Activity_Type = localVariables.newFieldInRecord("pnd_Activity_Type", "#ACTIVITY-TYPE", FieldType.STRING, 2);
        pnd_Unit_Actvity_Cntr = localVariables.newFieldInRecord("pnd_Unit_Actvity_Cntr", "#UNIT-ACTVITY-CNTR", FieldType.NUMERIC, 7);
        pnd_Empl_Actvity_Cntr = localVariables.newFieldInRecord("pnd_Empl_Actvity_Cntr", "#EMPL-ACTVITY-CNTR", FieldType.NUMERIC, 7);
        pnd_Step_Actvity_Cntr = localVariables.newFieldInRecord("pnd_Step_Actvity_Cntr", "#STEP-ACTVITY-CNTR", FieldType.NUMERIC, 7);
        pnd_Status_Actvity_Cntr = localVariables.newFieldInRecord("pnd_Status_Actvity_Cntr", "#STATUS-ACTVITY-CNTR", FieldType.NUMERIC, 7);
        pnd_Up_Wr_Cntr = localVariables.newFieldInRecord("pnd_Up_Wr_Cntr", "#UP-WR-CNTR", FieldType.NUMERIC, 7);
        pnd_Ad_Wr_Cntr = localVariables.newFieldInRecord("pnd_Ad_Wr_Cntr", "#AD-WR-CNTR", FieldType.NUMERIC, 7);
        pnd_Up_Ac_Cntr = localVariables.newFieldInRecord("pnd_Up_Ac_Cntr", "#UP-AC-CNTR", FieldType.NUMERIC, 7);
        pnd_Op_Ac_Cntr = localVariables.newFieldInRecord("pnd_Op_Ac_Cntr", "#OP-AC-CNTR", FieldType.NUMERIC, 7);
        pnd_Cl_Ac_Cntr = localVariables.newFieldInRecord("pnd_Cl_Ac_Cntr", "#CL-AC-CNTR", FieldType.NUMERIC, 7);
        pnd_Ad_Ac_Cntr = localVariables.newFieldInRecord("pnd_Ad_Ac_Cntr", "#AD-AC-CNTR", FieldType.NUMERIC, 7);
        pnd_Sp_Ac_Cntr = localVariables.newFieldInRecord("pnd_Sp_Ac_Cntr", "#SP-AC-CNTR", FieldType.NUMERIC, 7);
        pnd_Ppg_Cntr = localVariables.newFieldInRecord("pnd_Ppg_Cntr", "#PPG-CNTR", FieldType.NUMERIC, 7);
        pnd_Ppg_Rejected = localVariables.newFieldInRecord("pnd_Ppg_Rejected", "#PPG-REJECTED", FieldType.NUMERIC, 7);
        pnd_Org_Cntr = localVariables.newFieldInRecord("pnd_Org_Cntr", "#ORG-CNTR", FieldType.NUMERIC, 7);
        pnd_Skip_Cntr = localVariables.newFieldInRecord("pnd_Skip_Cntr", "#SKIP-CNTR", FieldType.NUMERIC, 7);
        pnd_Up_Rsn_Temp_Cntr = localVariables.newFieldInRecord("pnd_Up_Rsn_Temp_Cntr", "#UP-RSN-TEMP-CNTR", FieldType.NUMERIC, 7);
        pnd_Up_Rsn_Cde_Cntr = localVariables.newFieldInRecord("pnd_Up_Rsn_Cde_Cntr", "#UP-RSN-CDE-CNTR", FieldType.NUMERIC, 7);
        pnd_Ad_Rsn_Temp_Cntr = localVariables.newFieldInRecord("pnd_Ad_Rsn_Temp_Cntr", "#AD-RSN-TEMP-CNTR", FieldType.NUMERIC, 7);
        pnd_Ad_Rsn_Cde_Cntr = localVariables.newFieldInRecord("pnd_Ad_Rsn_Cde_Cntr", "#AD-RSN-CDE-CNTR", FieldType.NUMERIC, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Run_Id = localVariables.newFieldInRecord("pnd_Run_Id", "#RUN-ID", FieldType.PACKED_DECIMAL, 10);
        pnd_Page_No = localVariables.newFieldInRecord("pnd_Page_No", "#PAGE-NO", FieldType.NUMERIC, 4);
        pnd_Max_Rec = localVariables.newFieldInRecord("pnd_Max_Rec", "#MAX-REC", FieldType.NUMERIC, 4);
        pnd_Rqst_History_Key = localVariables.newFieldInRecord("pnd_Rqst_History_Key", "#RQST-HISTORY-KEY", FieldType.STRING, 30);

        pnd_Rqst_History_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Rqst_History_Key__R_Field_9", "REDEFINE", pnd_Rqst_History_Key);
        pnd_Rqst_History_Key_Pnd_Ky_Rqst_Log_Dte_Tme = pnd_Rqst_History_Key__R_Field_9.newFieldInGroup("pnd_Rqst_History_Key_Pnd_Ky_Rqst_Log_Dte_Tme", 
            "#KY-RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Rqst_History_Key_Pnd_Ky_Invrt_Strtng_Event = pnd_Rqst_History_Key__R_Field_9.newFieldInGroup("pnd_Rqst_History_Key_Pnd_Ky_Invrt_Strtng_Event", 
            "#KY-INVRT-STRTNG-EVENT", FieldType.NUMERIC, 15);
        pnd_Tiaa_Rcvd_Dte_Tme_A = localVariables.newFieldInRecord("pnd_Tiaa_Rcvd_Dte_Tme_A", "#TIAA-RCVD-DTE-TME-A", FieldType.STRING, 15);

        pnd_Tiaa_Rcvd_Dte_Tme_A__R_Field_10 = localVariables.newGroupInRecord("pnd_Tiaa_Rcvd_Dte_Tme_A__R_Field_10", "REDEFINE", pnd_Tiaa_Rcvd_Dte_Tme_A);
        pnd_Tiaa_Rcvd_Dte_Tme_A_Pnd_Tiaa_Rcvd_Dte_Dte = pnd_Tiaa_Rcvd_Dte_Tme_A__R_Field_10.newFieldInGroup("pnd_Tiaa_Rcvd_Dte_Tme_A_Pnd_Tiaa_Rcvd_Dte_Dte", 
            "#TIAA-RCVD-DTE-DTE", FieldType.STRING, 8);
        pnd_Tiaa_Rcvd_Dte_Tme_A_Pnd_Tiaa_Rcvd_Dte_Tme = pnd_Tiaa_Rcvd_Dte_Tme_A__R_Field_10.newFieldInGroup("pnd_Tiaa_Rcvd_Dte_Tme_A_Pnd_Tiaa_Rcvd_Dte_Tme", 
            "#TIAA-RCVD-DTE-TME", FieldType.STRING, 7);
        pnd_Eight_Oclock = localVariables.newFieldInRecord("pnd_Eight_Oclock", "#EIGHT-OCLOCK", FieldType.STRING, 7);
        pnd_Restart_Invrt_Start_A = localVariables.newFieldInRecord("pnd_Restart_Invrt_Start_A", "#RESTART-INVRT-START-A", FieldType.STRING, 15);

        pnd_Restart_Invrt_Start_A__R_Field_11 = localVariables.newGroupInRecord("pnd_Restart_Invrt_Start_A__R_Field_11", "REDEFINE", pnd_Restart_Invrt_Start_A);
        pnd_Restart_Invrt_Start_A_Pnd_Restart_Invrt_Start_N = pnd_Restart_Invrt_Start_A__R_Field_11.newFieldInGroup("pnd_Restart_Invrt_Start_A_Pnd_Restart_Invrt_Start_N", 
            "#RESTART-INVRT-START-N", FieldType.NUMERIC, 15);

        vw_icw_Imit = new DataAccessProgramView(new NameInfo("vw_icw_Imit", "ICW-IMIT"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        icw_Imit_Rqst_Log_Dte_Tme = vw_icw_Imit.getRecord().newFieldInGroup("icw_Imit_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");

        icw_Imit__R_Field_12 = vw_icw_Imit.getRecord().newGroupInGroup("icw_Imit__R_Field_12", "REDEFINE", icw_Imit_Rqst_Log_Dte_Tme);
        icw_Imit_Rqst_Log_Index_Dte = icw_Imit__R_Field_12.newFieldInGroup("icw_Imit_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 8);
        icw_Imit_Rqst_Log_Index_Tme = icw_Imit__R_Field_12.newFieldInGroup("icw_Imit_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", FieldType.STRING, 7);
        icw_Imit_Crprte_Status_Ind = vw_icw_Imit.getRecord().newFieldInGroup("icw_Imit_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_STATUS_IND");
        icw_Imit_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        icw_Imit_Tiaa_Rcvd_Dte_Tme = vw_icw_Imit.getRecord().newFieldInGroup("icw_Imit_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE_TME");
        icw_Imit_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        icw_Imit_Instn_Close_Dte_Tme = vw_icw_Imit.getRecord().newFieldInGroup("icw_Imit_Instn_Close_Dte_Tme", "INSTN-CLOSE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "INSTN_CLOSE_DTE_TME");
        icw_Imit_Crprte_Close_Dte_Tme = vw_icw_Imit.getRecord().newFieldInGroup("icw_Imit_Crprte_Close_Dte_Tme", "CRPRTE-CLOSE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRPRTE_CLOSE_DTE_TME");
        icw_Imit_Strtng_Event_Dte_Tme = vw_icw_Imit.getRecord().newFieldInGroup("icw_Imit_Strtng_Event_Dte_Tme", "STRTNG-EVENT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STRTNG_EVENT_DTE_TME");
        icw_Imit_Invrt_Strtng_Event = vw_icw_Imit.getRecord().newFieldInGroup("icw_Imit_Invrt_Strtng_Event", "INVRT-STRTNG-EVENT", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "INVRT_STRTNG_EVENT");
        icw_Imit_Endng_Event_Dte_Tme = vw_icw_Imit.getRecord().newFieldInGroup("icw_Imit_Endng_Event_Dte_Tme", "ENDNG-EVENT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENDNG_EVENT_DTE_TME");
        icw_Imit_Trade_Dte_Tme = vw_icw_Imit.getRecord().newFieldInGroup("icw_Imit_Trade_Dte_Tme", "TRADE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRADE_DTE_TME");
        icw_Imit_Status_Cde = vw_icw_Imit.getRecord().newFieldInGroup("icw_Imit_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_hst_View.reset();
        vw_icw_Imit.reset();

        ldaIcwl3000.initializeValues();
        ldaIcwl3010.initializeValues();
        ldaIcwl3014.initializeValues();
        ldaIcwl3015.initializeValues();
        ldaIcwl3016.initializeValues();

        localVariables.reset();
        pnd_Atsign.setInitialValue("@");
        pnd_Nines.setInitialValue(-2147483648);
        pnd_Eight_Oclock.setInitialValue("0800000");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Icwb3000() throws Exception
    {
        super("Icwb3000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("ICWB3000", onError);
        setupReports();
        //*  INITIALIZATION
        //*  --------------
        //* (TO ALLOW EASY ESCAPE FROM THE ROUTINE)
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60
        //*  MAIN PROCESSING
        //*  ---------------
        PROGRAM:                                                                                                                                                          //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM SET-UP-THE-RUN-CONTROL-RECORD
            sub_Set_Up_The_Run_Control_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM EXTRACT-MASTER-INDEX
            sub_Extract_Master_Index();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM EXTRACT-CLOSE-ACTIVITY
            sub_Extract_Close_Activity();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM EXTRACT-PPG-INST
            sub_Extract_Ppg_Inst();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM SET-THE-RUN-CONTROL-RECORD-TO-COMPLETE
            sub_Set_The_Run_Control_Record_To_Complete();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,"         ** Statistic Summary **",NEWLINE,"** for Institutional Extract job ICWB3000 **",                 //Natural: WRITE / '         ** Statistic Summary **' / '** for Institutional Extract job ICWB3000 **' / 'For Period: ' #TIME-WINDOW.#STARTING-ALPHA / '     Thru   ' #TIME-WINDOW.#ENDING-ALPHA / '========================================================' / 'Updated Work Requests Extracted  =  ' #UP-WR-CNTR / 'Added   Work Requests Extracted  =  ' #AD-WR-CNTR / 'Updated Activities Extracted     =  ' #UP-AC-CNTR / '++++++++++++++++++++++++++++++++++++' / 'Open Unit Activity created       =  ' #UNIT-ACTVITY-CNTR / 'Open Empl Activity created       =  ' #EMPL-ACTVITY-CNTR / 'Open Step Activity created       =  ' #STEP-ACTVITY-CNTR / 'Open Status Activity Created     =  ' #STATUS-ACTVITY-CNTR / '++++++++++++++++++++++++++++++++++++' / 'Open Activity Created            =  ' #OP-AC-CNTR / 'Close Activity Extracted         =  ' #CL-AC-CNTR / '++++++++++++++++++++++++++++++++++++' / 'Added Activities                 =  ' #AD-AC-CNTR / '************************************' / 'Reason-Code Update file created  =  ' #UP-RSN-CDE-CNTR / 'Reason-Code Added  file created  =  ' #AD-RSN-CDE-CNTR / '************************************' / 'Special Work RequestS Extracted  =  ' #SP-AC-CNTR / '************************************' / 'PPG  Records Created             =  ' #PPG-CNTR / 'PPG  Records Rejected            =  ' #PPG-REJECTED / 'Inst Records Created             =  ' #ORG-CNTR / 'Statuses Skipped (C099 & 8...)   =  ' #SKIP-CNTR
                NEWLINE,"For Period: ",pnd_Time_Window_Pnd_Starting_Alpha,NEWLINE,"     Thru   ",pnd_Time_Window_Pnd_Ending_Alpha,NEWLINE,"========================================================",
                NEWLINE,"Updated Work Requests Extracted  =  ",pnd_Up_Wr_Cntr,NEWLINE,"Added   Work Requests Extracted  =  ",pnd_Ad_Wr_Cntr,NEWLINE,"Updated Activities Extracted     =  ",
                pnd_Up_Ac_Cntr,NEWLINE,"++++++++++++++++++++++++++++++++++++",NEWLINE,"Open Unit Activity created       =  ",pnd_Unit_Actvity_Cntr,NEWLINE,
                "Open Empl Activity created       =  ",pnd_Empl_Actvity_Cntr,NEWLINE,"Open Step Activity created       =  ",pnd_Step_Actvity_Cntr,NEWLINE,
                "Open Status Activity Created     =  ",pnd_Status_Actvity_Cntr,NEWLINE,"++++++++++++++++++++++++++++++++++++",NEWLINE,"Open Activity Created            =  ",
                pnd_Op_Ac_Cntr,NEWLINE,"Close Activity Extracted         =  ",pnd_Cl_Ac_Cntr,NEWLINE,"++++++++++++++++++++++++++++++++++++",NEWLINE,"Added Activities                 =  ",
                pnd_Ad_Ac_Cntr,NEWLINE,"************************************",NEWLINE,"Reason-Code Update file created  =  ",pnd_Up_Rsn_Cde_Cntr,NEWLINE,
                "Reason-Code Added  file created  =  ",pnd_Ad_Rsn_Cde_Cntr,NEWLINE,"************************************",NEWLINE,"Special Work RequestS Extracted  =  ",
                pnd_Sp_Ac_Cntr,NEWLINE,"************************************",NEWLINE,"PPG  Records Created             =  ",pnd_Ppg_Cntr,NEWLINE,"PPG  Records Rejected            =  ",
                pnd_Ppg_Rejected,NEWLINE,"Inst Records Created             =  ",pnd_Org_Cntr,NEWLINE,"Statuses Skipped (C099 & 8...)   =  ",pnd_Skip_Cntr);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            work_File_8_Pnd_Tot_File_1.setValue(pnd_Up_Wr_Cntr);                                                                                                          //Natural: MOVE #UP-WR-CNTR TO #TOT-FILE-1
            work_File_8_Pnd_Tot_File_2.setValue(pnd_Ad_Wr_Cntr);                                                                                                          //Natural: MOVE #AD-WR-CNTR TO #TOT-FILE-2
            work_File_8_Pnd_Tot_File_3.setValue(pnd_Up_Ac_Cntr);                                                                                                          //Natural: MOVE #UP-AC-CNTR TO #TOT-FILE-3
            work_File_8_Pnd_Tot_File_4.setValue(pnd_Ad_Ac_Cntr);                                                                                                          //Natural: MOVE #AD-AC-CNTR TO #TOT-FILE-4
            work_File_8_Pnd_Tot_File_5.setValue(pnd_Sp_Ac_Cntr);                                                                                                          //Natural: MOVE #SP-AC-CNTR TO #TOT-FILE-5
            work_File_8_Pnd_Tot_File_6.setValue(pnd_Ppg_Cntr);                                                                                                            //Natural: MOVE #PPG-CNTR TO #TOT-FILE-6
            work_File_8_Pnd_Tot_File_7.setValue(pnd_Org_Cntr);                                                                                                            //Natural: MOVE #ORG-CNTR TO #TOT-FILE-7
            work_File_8_Pnd_Tot_File_8.setValue(pnd_Up_Rsn_Cde_Cntr);                                                                                                     //Natural: MOVE #UP-RSN-CDE-CNTR TO #TOT-FILE-8
            work_File_8_Pnd_Tot_File_9.setValue(pnd_Ad_Rsn_Cde_Cntr);                                                                                                     //Natural: MOVE #AD-RSN-CDE-CNTR TO #TOT-FILE-9
            pnd_End_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Time_Window_Pnd_Ending_Alpha);                                                        //Natural: MOVE EDITED #ENDING-ALPHA TO #END-TIME-T ( EM = YYYYMMDDHHIISST )
            work_File_8_Pnd_Run_Dte_Tme.setValueEdited(pnd_End_Time_T,new ReportEditMask("LLL' 'DD' 'YYYY' 'HH':'II':'SSAP"));                                            //Natural: MOVE EDITED #END-TIME-T ( EM = LLL' 'DD' 'YYYY' 'HH':'II':'SSAP ) TO #RUN-DTE-TME
                                                                                                                                                                          //Natural: PERFORM FILL-IN-F-ATSIGN
            sub_Fill_In_F_Atsign();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(8, false, work_File_8);                                                                                                                  //Natural: WRITE WORK FILE 8 WORK-FILE-8
            if (true) break PROGRAM;                                                                                                                                      //Natural: ESCAPE BOTTOM ( PROGRAM. )
            //*  SUBROUTINE JUST FOR ENDING THIS PROGRAM IMMEDIATELY
            //*  ---------------------------------------------------
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            //* *************
            //*                                                                                                                                                           //Natural: ON ERROR
            //* (PROGRAM.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  SUBROUTINES
        //*  ===========
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-UP-THE-RUN-CONTROL-RECORD
        //*  =============================================
        //*  USE ICWN3000 TO SET UP THE RUN CONTROL RECORD.  IT ALSO RETURNS THE
        //*  ISN OF THE RUN CONTROL RECORD AND INDICATES WHETHER OR NOT THIS IS
        //*  A RESTART.
        //*  -------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-THE-RUN-CONTROL-RECORD-TO-COMPLETE
        //*  ======================================================
        //* *
        //* *
        //*  ====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXTRACT-MASTER-INDEX
        //*  =======================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-WR
        //*  ========================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CLOSE-WR
        //*  ====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXTRACT-CLOSE-ACTIVITY
        //*  ===================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SPCL-ACTIVITY
        //*  (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*  =============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WR
        //*   (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*   (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*        (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*  (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*    (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*    (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*    (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*  ====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-REASON-CDE-UPD
        //*  ====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-REASON-CDE-ADD
        //*  ===================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-OPEN-ACTIVITY
        //*  ===================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-IN-OP-ACTIVITY
        //*  (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*    (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*    (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*    (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*    (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*  ====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CLOSE-ACTIVITY
        //*  ===================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-IN-CL-ACTIVITY
        //*  (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*  (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*    (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXTRACT-PPG-INST
        //*  ================================
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-IN-A-ATSIGN
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-IN-B-ATSIGN
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-IN-C-ATSIGN
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-IN-D-ATSIGN
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-IN-E-ATSIGN
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-IN-F-ATSIGN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-RESTART
        //* *************
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROGRAM. )
        Global.setEscapeCode(EscapeType.Bottom, "PROGRAM");
        if (true) return;
        //* (2120)
    }
    private void sub_Set_Up_The_Run_Control_Record() throws Exception                                                                                                     //Natural: SET-UP-THE-RUN-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaIcwa3000.getIcwa3000_Input_Job_Name().setValue(Global.getINIT_USER());                                                                                         //Natural: ASSIGN ICWA3000-INPUT.JOB-NAME := *INIT-USER
        pdaIcwa3000.getIcwa3000_Input_Program_Name().setValue(Global.getPROGRAM());                                                                                       //Natural: ASSIGN ICWA3000-INPUT.PROGRAM-NAME := *PROGRAM
        DbsUtil.callnat(Icwn3000.class , getCurrentProcessState(), pdaIcwa3000.getIcwa3000_Input(), pdaIcwa3000.getIcwa3000_Output(), pdaCdaobj.getCdaobj(),              //Natural: CALLNAT 'ICWN3000' ICWA3000-INPUT ICWA3000-OUTPUT CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        //*  ------------------------------------------
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("W")))                    //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E' OR MSG-INFO-SUB.##RETURN-CODE = 'W'
        {
            getReports().write(0, ReportOption.NOTITLE,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                     //Natural: WRITE 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
            {
                DbsUtil.terminate(10);  if (true) return;                                                                                                                 //Natural: TERMINATE 10
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(999)))                                                                                          //Natural: IF MSG-INFO-SUB.##MSG-NR = 999
        {
            getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                  //Natural: WRITE ( 0 ) NOTITLE NOHDR 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            DbsUtil.terminate(11);  if (true) return;                                                                                                                     //Natural: TERMINATE 11
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------------------------------------
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* (2570)
    }
    private void sub_Set_The_Run_Control_Record_To_Complete() throws Exception                                                                                            //Natural: SET-THE-RUN-CONTROL-RECORD-TO-COMPLETE
    {
        if (BLNatReinput.isReinput()) return;

        COMPLETE_RUN_CONTROL_GET:                                                                                                                                         //Natural: GET CWF-SUPPORT-TBL ICWA3000-OUTPUT.RUN-CONTROL-ISN
        ldaIcwl3000.getVw_cwf_Support_Tbl().readByID(pdaIcwa3000.getIcwa3000_Output_Run_Control_Isn().getLong(), "COMPLETE_RUN_CONTROL_GET");
        ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Upd_Wr().setValue(pnd_Up_Wr_Cntr);                                                                                     //Natural: ASSIGN CWF-SUPPORT-TBL.NBR-OF-RECS-UPD-WR := #UP-WR-CNTR
        ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Add_Wr().setValue(pnd_Ad_Wr_Cntr);                                                                                     //Natural: ASSIGN CWF-SUPPORT-TBL.NBR-OF-RECS-ADD-WR := #AD-WR-CNTR
        ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Opn_Ac().setValue(pnd_Up_Ac_Cntr);                                                                                     //Natural: ASSIGN CWF-SUPPORT-TBL.NBR-OF-RECS-OPN-AC := #UP-AC-CNTR
        ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Cls_Ac().setValue(pnd_Ad_Ac_Cntr);                                                                                     //Natural: ASSIGN CWF-SUPPORT-TBL.NBR-OF-RECS-CLS-AC := #AD-AC-CNTR
        ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Spl_Pr().setValue(pnd_Sp_Ac_Cntr);                                                                                     //Natural: ASSIGN CWF-SUPPORT-TBL.NBR-OF-RECS-SPL-PR := #SP-AC-CNTR
        ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Ppg().setValue(pnd_Ppg_Cntr);                                                                                          //Natural: ASSIGN CWF-SUPPORT-TBL.NBR-OF-RECS-PPG := #PPG-CNTR
        ldaIcwl3000.getCwf_Support_Tbl_Nbr_Of_Recs_Inst().setValue(pnd_Org_Cntr);                                                                                         //Natural: ASSIGN CWF-SUPPORT-TBL.NBR-OF-RECS-INST := #ORG-CNTR
        ldaIcwl3000.getCwf_Support_Tbl_Run_Status().setValue("C");                                                                                                        //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-STATUS := 'C'
        ldaIcwl3000.getCwf_Support_Tbl_Run_Date().setValue(Global.getDATN());                                                                                             //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-DATE := *DATN
        ldaIcwl3000.getCwf_Support_Tbl_Program_Name().setValue(Global.getPROGRAM());                                                                                      //Natural: ASSIGN CWF-SUPPORT-TBL.PROGRAM-NAME := *PROGRAM
        ldaIcwl3000.getCwf_Support_Tbl_Tbl_Updte_Dte_Tme().setValue(Global.getTIMX());                                                                                    //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-UPDTE-DTE-TME := *TIMX
        ldaIcwl3000.getCwf_Support_Tbl_Tbl_Updte_Oprtr_Cde().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE := *PROGRAM
        ldaIcwl3000.getVw_cwf_Support_Tbl().updateDBRow("COMPLETE_RUN_CONTROL_GET");                                                                                      //Natural: UPDATE ( COMPLETE-RUN-CONTROL-GET. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* (2505)
    }
    private void sub_Extract_Master_Index() throws Exception                                                                                                              //Natural: EXTRACT-MASTER-INDEX
    {
        if (BLNatReinput.isReinput()) return;

        //*  ====================================
        //*  GET THE RUN CONTROL RECORD TO DETERMINE THE PROCESSING TIME WINDOW
        //*  ------------------------------------------------------------------
        INITIAL_RUN_CONTROL_GET:                                                                                                                                          //Natural: GET CWF-SUPPORT-TBL ICWA3000-OUTPUT.RUN-CONTROL-ISN
        ldaIcwl3000.getVw_cwf_Support_Tbl().readByID(pdaIcwa3000.getIcwa3000_Output_Run_Control_Isn().getLong(), "INITIAL_RUN_CONTROL_GET");
        pnd_Run_Id.setValue(ldaIcwl3000.getCwf_Support_Tbl_Run_Id());                                                                                                     //Natural: ASSIGN #RUN-ID := CWF-SUPPORT-TBL.RUN-ID
        pnd_Time_Window_Pnd_Starting_Alpha.setValueEdited(ldaIcwl3000.getCwf_Support_Tbl_Starting_Date(),new ReportEditMask("YYYYMMDDHHIISST"));                          //Natural: MOVE EDITED CWF-SUPPORT-TBL.STARTING-DATE ( EM = YYYYMMDDHHIISST ) TO #TIME-WINDOW.#STARTING-ALPHA
        pnd_Time_Window_Pnd_Ending_Alpha.setValueEdited(ldaIcwl3000.getCwf_Support_Tbl_Ending_Date(),new ReportEditMask("YYYYMMDDHHIISST"));                              //Natural: MOVE EDITED CWF-SUPPORT-TBL.ENDING-DATE ( EM = YYYYMMDDHHIISST ) TO #TIME-WINDOW.#ENDING-ALPHA
        pnd_Starting_A8_Pnd_Starting_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Time_Window_Pnd_Starting_Alpha);                                          //Natural: MOVE EDITED #TIME-WINDOW.#STARTING-ALPHA TO #STARTING-T ( EM = YYYYMMDDHHIISST )
        //*  SET UP THE STARTING KEY
        //*  -----------------------
        pnd_Invrt_Start.compute(new ComputeParameters(false, pnd_Invrt_Start), (pnd_Nines.subtract(pnd_Time_Window_Pnd_Starting_Numeric)));                               //Natural: COMPUTE #INVRT-START = ( #NINES - #STARTING-NUMERIC )
        pnd_Invrt_End.compute(new ComputeParameters(false, pnd_Invrt_End), (pnd_Nines.subtract(pnd_Time_Window_Pnd_Ending_Numeric)));                                     //Natural: COMPUTE #INVRT-END = ( #NINES - #ENDING-NUMERIC )
        //*  SELECT ALL IMIT RECORDS IN THE TIME WINDOW
        //*  ------------------------------------------
        pnd_Start_Key_From.reset();                                                                                                                                       //Natural: RESET #START-KEY-FROM
        pnd_Start_Key_From_Pnd_Crprte_Status_Ind.setValue("0");                                                                                                           //Natural: ASSIGN #CRPRTE-STATUS-IND := '0'
        pnd_Start_Key_From_Pnd_Invrt_Strtng_Event.setValue(pnd_Invrt_End);                                                                                                //Natural: ASSIGN #INVRT-STRTNG-EVENT := #INVRT-END
                                                                                                                                                                          //Natural: PERFORM OPEN-WR
        sub_Open_Wr();
        if (condition(Global.isEscape())) {return;}
        pnd_Start_Key_From.reset();                                                                                                                                       //Natural: RESET #START-KEY-FROM
        pnd_Start_Key_From_Pnd_Crprte_Status_Ind.setValue("9");                                                                                                           //Natural: ASSIGN #CRPRTE-STATUS-IND := '9'
        pnd_Start_Key_From_Pnd_Invrt_Strtng_Event.setValue(pnd_Invrt_End);                                                                                                //Natural: ASSIGN #INVRT-STRTNG-EVENT := #INVRT-END
                                                                                                                                                                          //Natural: PERFORM CLOSE-WR
        sub_Close_Wr();
        if (condition(Global.isEscape())) {return;}
        //*  EXTRACT-MASTER-INDEX  (2425)
    }
    private void sub_Open_Wr() throws Exception                                                                                                                           //Natural: OPEN-WR
    {
        if (BLNatReinput.isReinput()) return;

        //*  =======================
        ldaIcwl3010.getVw_icw_Master_Index_View().startDatabaseRead                                                                                                       //Natural: READ ICW-MASTER-INDEX-VIEW BY CRPRTE-STATUS-CHNGE-DTE-KEY STARTING FROM #START-KEY-FROM
        (
        "R1",
        new Wc[] { new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", ">=", pnd_Start_Key_From, WcType.BY) },
        new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
        );
        R1:
        while (condition(ldaIcwl3010.getVw_icw_Master_Index_View().readNextRow("R1")))
        {
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Crprte_Status_Ind().equals("9")))                                                                          //Natural: IF ICW-MASTER-INDEX-VIEW.CRPRTE-STATUS-IND = '9'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Invrt_Strtng_Event().greater(pnd_Invrt_Start)))                                                            //Natural: IF ICW-MASTER-INDEX-VIEW.INVRT-STRTNG-EVENT GT #INVRT-START
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Isn_Save.setValue(ldaIcwl3010.getVw_icw_Master_Index_View().getAstISN("R1"));                                                                             //Natural: ASSIGN #ISN-SAVE := *ISN
            //* ************* CHECKING FOR ADDED OR UPDATED RECORDS *************
            //*  CREATED BEFORE WINDOW
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Dte_Tme().less(pnd_Time_Window_Pnd_Starting_Alpha)))                                              //Natural: IF ICW-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME LT #TIME-WINDOW.#STARTING-ALPHA
            {
                pnd_Add_Updt_Sw.setValue("U");                                                                                                                            //Natural: MOVE 'U' TO #ADD-UPDT-SW
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Add_Updt_Sw.setValue("A");                                                                                                                            //Natural: MOVE 'A' TO #ADD-UPDT-SW
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-WR
            sub_Write_Wr();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Activity_Type.reset();                                                                                                                                    //Natural: RESET #ACTIVITY-TYPE
            pnd_Skip_Status_Sw.setValue(false);                                                                                                                           //Natural: ASSIGN #SKIP-STATUS-SW := FALSE
            //*  08/25/98 L.E.
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Unit_Start_Dte_Tme().greater(getZero()) && ldaIcwl3010.getIcw_Master_Index_View_Unit_Cde().notEquals("RSSMP"))) //Natural: IF ICW-MASTER-INDEX-VIEW.UNIT-START-DTE-TME GT 0 AND ICW-MASTER-INDEX-VIEW.UNIT-CDE NE 'RSSMP'
            {
                pnd_Compare_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Unit_Start_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                      //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.UNIT-START-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #COMPARE-DTE-TME
                if (condition(pnd_Compare_Dte_Tme_Pnd_Compare_Dte_Tme_N.greaterOrEqual(pnd_Time_Window_Pnd_Starting_Numeric)))                                            //Natural: IF #COMPARE-DTE-TME-N GE #STARTING-NUMERIC
                {
                    pnd_Activity_Type.setValue("01");                                                                                                                     //Natural: MOVE '01' TO #ACTIVITY-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-OPEN-ACTIVITY
                    sub_Write_Open_Activity();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Unit_Actvity_Cntr.nadd(1);                                                                                                                        //Natural: ADD 1 TO #UNIT-ACTVITY-CNTR
                    pnd_Op_Ac_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #OP-AC-CNTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Empl_Start_Dte_Tme().greater(getZero())))                                                                  //Natural: IF ICW-MASTER-INDEX-VIEW.EMPL-START-DTE-TME GT 0
            {
                pnd_Compare_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Empl_Start_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                      //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.EMPL-START-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #COMPARE-DTE-TME
                if (condition(pnd_Compare_Dte_Tme_Pnd_Compare_Dte_Tme_N.greaterOrEqual(pnd_Time_Window_Pnd_Starting_Numeric)))                                            //Natural: IF #COMPARE-DTE-TME-N GE #STARTING-NUMERIC
                {
                    pnd_Activity_Type.setValue("02");                                                                                                                     //Natural: MOVE '02' TO #ACTIVITY-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-OPEN-ACTIVITY
                    sub_Write_Open_Activity();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Empl_Actvity_Cntr.nadd(1);                                                                                                                        //Natural: ADD 1 TO #EMPL-ACTVITY-CNTR
                    pnd_Op_Ac_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #OP-AC-CNTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  08/21/98 L.E.
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Step_Start_Dte_Tme().greater(getZero()) && ldaIcwl3010.getIcw_Master_Index_View_Step_Id().greater(" ")))   //Natural: IF ICW-MASTER-INDEX-VIEW.STEP-START-DTE-TME GT 0 AND ICW-MASTER-INDEX-VIEW.STEP-ID GT ' '
            {
                pnd_Compare_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Step_Start_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                      //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.STEP-START-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #COMPARE-DTE-TME
                if (condition(pnd_Compare_Dte_Tme_Pnd_Compare_Dte_Tme_N.greaterOrEqual(pnd_Time_Window_Pnd_Starting_Numeric)))                                            //Natural: IF #COMPARE-DTE-TME-N GE #STARTING-NUMERIC
                {
                    pnd_Activity_Type.setValue("03");                                                                                                                     //Natural: MOVE '03' TO #ACTIVITY-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-OPEN-ACTIVITY
                    sub_Write_Open_Activity();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Step_Actvity_Cntr.nadd(1);                                                                                                                        //Natural: ADD 1 TO #STEP-ACTVITY-CNTR
                    pnd_Op_Ac_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #OP-AC-CNTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Status_Updte_Dte_Tme().greater(getZero())))                                                                //Natural: IF ICW-MASTER-INDEX-VIEW.STATUS-UPDTE-DTE-TME GT 0
            {
                //*  LE 10/04/2000
                if (condition(ldaIcwl3010.getIcw_Master_Index_View_Admin_Status_Cde().equals("C099")))                                                                    //Natural: IF ICW-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE = 'C099'
                {
                    pnd_Skip_Cntr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #SKIP-CNTR
                    pnd_Skip_Status_Sw.setValue(true);                                                                                                                    //Natural: ASSIGN #SKIP-STATUS-SW := TRUE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Compare_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Status_Updte_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                    //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.STATUS-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #COMPARE-DTE-TME
                if (condition(pnd_Skip_Status_Sw.equals(false)))                                                                                                          //Natural: IF #SKIP-STATUS-SW = FALSE
                {
                    if (condition(pnd_Compare_Dte_Tme_Pnd_Compare_Dte_Tme_N.greaterOrEqual(pnd_Time_Window_Pnd_Starting_Numeric)))                                        //Natural: IF #COMPARE-DTE-TME-N GE #STARTING-NUMERIC
                    {
                        pnd_Activity_Type.setValue("04");                                                                                                                 //Natural: MOVE '04' TO #ACTIVITY-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-OPEN-ACTIVITY
                        sub_Write_Open_Activity();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Status_Actvity_Cntr.nadd(1);                                                                                                                  //Natural: ADD 1 TO #STATUS-ACTVITY-CNTR
                        pnd_Op_Ac_Cntr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #OP-AC-CNTR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  R1.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  EXTRACT-OPEN-WORK-REQUEST
    }
    private void sub_Close_Wr() throws Exception                                                                                                                          //Natural: CLOSE-WR
    {
        if (BLNatReinput.isReinput()) return;

        //*  ========================
        ldaIcwl3010.getVw_icw_Master_Index_View().startDatabaseRead                                                                                                       //Natural: READ ICW-MASTER-INDEX-VIEW BY CRPRTE-STATUS-CHNGE-DTE-KEY STARTING FROM #START-KEY-FROM
        (
        "READ01",
        new Wc[] { new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", ">=", pnd_Start_Key_From, WcType.BY) },
        new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
        );
        READ01:
        while (condition(ldaIcwl3010.getVw_icw_Master_Index_View().readNextRow("READ01")))
        {
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Invrt_Strtng_Event().greater(pnd_Invrt_Start)))                                                            //Natural: IF ICW-MASTER-INDEX-VIEW.INVRT-STRTNG-EVENT GT #INVRT-START
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Isn_Save.setValue(ldaIcwl3010.getVw_icw_Master_Index_View().getAstISN("Read01"));                                                                         //Natural: ASSIGN #ISN-SAVE := *ISN
            //* ************* CHECKING FOR ADDED OR UPDATED RECORDS *************
            //*  CREATED BEFORE WINDOW
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Dte_Tme().less(pnd_Time_Window_Pnd_Starting_Alpha)))                                              //Natural: IF ICW-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME LT #TIME-WINDOW.#STARTING-ALPHA
            {
                pnd_Add_Updt_Sw.setValue("U");                                                                                                                            //Natural: MOVE 'U' TO #ADD-UPDT-SW
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Add_Updt_Sw.setValue("A");                                                                                                                            //Natural: MOVE 'A' TO #ADD-UPDT-SW
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-WR
            sub_Write_Wr();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CLOSE-WR
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Extract_Close_Activity() throws Exception                                                                                                            //Natural: EXTRACT-CLOSE-ACTIVITY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ====================================
        ldaIcwl3015.getVw_icw_Activity_View().startDatabaseRead                                                                                                           //Natural: READ ICW-ACTIVITY-VIEW BY ENDNG-EVENT-DTE-TME STARTING FROM #STARTING-T
        (
        "ACT_READ",
        new Wc[] { new Wc("ENDNG_EVENT_DTE_TME", ">=", pnd_Starting_A8_Pnd_Starting_T, WcType.BY) },
        new Oc[] { new Oc("ENDNG_EVENT_DTE_TME", "ASC") }
        );
        ACT_READ:
        while (condition(ldaIcwl3015.getVw_icw_Activity_View().readNextRow("ACT_READ")))
        {
            pnd_Ending_Event_Alpha.setValueEdited(ldaIcwl3015.getIcw_Activity_View_Endng_Event_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                          //Natural: MOVE EDITED ENDNG-EVENT-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #ENDING-EVENT-ALPHA
            //*    IF #ENDING-EVENT-NUMERIC LT #STARTING-NUMERIC
            //*       ESCAPE TOP
            //*    END-IF
            if (condition(pnd_Ending_Event_Alpha_Pnd_Ending_Event_Numeric.greater(pnd_Time_Window_Pnd_Ending_Numeric)))                                                   //Natural: IF #ENDING-EVENT-NUMERIC GT #ENDING-NUMERIC
            {
                if (true) break ACT_READ;                                                                                                                                 //Natural: ESCAPE BOTTOM ( ACT-READ. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIcwl3015.getIcw_Activity_View_Actvty_Type().equals("05")))                                                                                   //Natural: IF ICW-ACTIVITY-VIEW.ACTVTY-TYPE = '05'
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-SPCL-ACTIVITY
                sub_Write_Spcl_Activity();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("ACT_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("ACT_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-CLOSE-ACTIVITY
                sub_Write_Close_Activity();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("ACT_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("ACT_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  ACT-READ.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  EXTRACT-CLOSE-ACTIVITY
    }
    private void sub_Write_Spcl_Activity() throws Exception                                                                                                               //Natural: WRITE-SPCL-ACTIVITY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===================================
        pnd_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaIcwl3015.getIcw_Activity_View_Rqst_Log_Dte_Tme());                                               //Natural: MOVE EDITED ICW-ACTIVITY-VIEW.RQST-LOG-DTE-TME TO #TIME ( EM = YYYYMMDDHHIISST )
        work_File_5_Rqst_Log_Dte_Tme.setValueEdited(pnd_Time,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                                                   //Natural: MOVE EDITED #TIME ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO WORK-FILE-5.RQST-LOG-DTE-TME
        if (condition(ldaIcwl3015.getIcw_Activity_View_Spcl_Prcss_Log_Dte_Tme().greater(" ")))                                                                            //Natural: IF ICW-ACTIVITY-VIEW.SPCL-PRCSS-LOG-DTE-TME > ' '
        {
            pnd_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaIcwl3015.getIcw_Activity_View_Spcl_Prcss_Log_Dte_Tme());                                     //Natural: MOVE EDITED ICW-ACTIVITY-VIEW.SPCL-PRCSS-LOG-DTE-TME TO #TIME ( EM = YYYYMMDDHHIISST )
            work_File_5_Spcl_Prcss_Log_Dte_Tme.setValueEdited(pnd_Time,new ReportEditMask("'''LLL' 'DD' 'YYYY' 'HH':'II':'SS':'0AP'''"));                                 //Natural: MOVE EDITED #TIME ( EM = '"'LLL' 'DD' 'YYYY' 'HH':'II':'SS':'T'00'AP'"' ) TO WORK-FILE-5.SPCL-PRCSS-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_File_5_Spcl_Prcss_Log_Dte_Tme.setValue(" ");                                                                                                             //Natural: MOVE ' ' TO WORK-FILE-5.SPCL-PRCSS-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        work_File_5_Spcl_Prcss_Ind.setValue(ldaIcwl3015.getIcw_Activity_View_Spcl_Prcss_Ind());                                                                           //Natural: MOVE ICW-ACTIVITY-VIEW.SPCL-PRCSS-IND TO WORK-FILE-5.SPCL-PRCSS-IND
        work_File_5_Ending_Event_Dte_Tme.setValueEdited(ldaIcwl3015.getIcw_Activity_View_Endng_Event_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));   //Natural: MOVE EDITED ICW-ACTIVITY-VIEW.ENDNG-EVENT-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-5.ENDING-EVENT-DTE-TME
                                                                                                                                                                          //Natural: PERFORM FILL-IN-C-ATSIGN
        sub_Fill_In_C_Atsign();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().write(5, false, work_File_5);                                                                                                                      //Natural: WRITE WORK FILE 5 WORK-FILE-5
        pnd_Sp_Ac_Cntr.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #SP-AC-CNTR
        work_File_5.reset();                                                                                                                                              //Natural: RESET WORK-FILE-5
        //*  WRITE-SPCL-AC
    }
    private void sub_Write_Wr() throws Exception                                                                                                                          //Natural: WRITE-WR
    {
        if (BLNatReinput.isReinput()) return;

        //*  =============================
        pnd_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Dte_Tme());                                           //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO #TIME ( EM = YYYYMMDDHHIISST )
        work_File_1_2_Rqst_Log_Dte_Tme.setValueEdited(pnd_Time,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                                                 //Natural: MOVE EDITED #TIME ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO WORK-FILE-1-2.RQST-LOG-DTE-TME
        work_File_1_2_Owner_Unit.setValue(ldaIcwl3010.getIcw_Master_Index_View_Owner_Unit());                                                                             //Natural: MOVE ICW-MASTER-INDEX-VIEW.OWNER-UNIT TO WORK-FILE-1-2.OWNER-UNIT
        work_File_1_2_Work_Prcss_Id.setValue(ldaIcwl3010.getIcw_Master_Index_View_Work_Prcss_Id());                                                                       //Natural: MOVE ICW-MASTER-INDEX-VIEW.WORK-PRCSS-ID TO WORK-FILE-1-2.WORK-PRCSS-ID
        work_File_1_2_Iis_Instn_Link_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Iis_Instn_Link_Cde());                                                             //Natural: MOVE ICW-MASTER-INDEX-VIEW.IIS-INSTN-LINK-CDE TO WORK-FILE-1-2.IIS-INSTN-LINK-CDE
        work_File_1_2_Ppg_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Ppg_Cde());                                                                                   //Natural: MOVE ICW-MASTER-INDEX-VIEW.PPG-CDE TO WORK-FILE-1-2.PPG-CDE
        pnd_Rqst_Log_Dte_Tme_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Dte_Tme());                             //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO #RQST-LOG-DTE-TME-T ( EM = YYYYMMDDHHIISST )
        //*   USE THE LESSER OF RQST-LOG-DTE-TME OR TIAA-RCVD-DTE-TME FOR TRADE-DT
        //*   (ACTUALLY WORK-START-DTE) JVH
        if (condition(pnd_Rqst_Log_Dte_Tme_T.lessOrEqual(ldaIcwl3010.getIcw_Master_Index_View_Tiaa_Rcvd_Dte_Tme())))                                                      //Natural: IF #RQST-LOG-DTE-TME-T LE ICW-MASTER-INDEX-VIEW.TIAA-RCVD-DTE-TME
        {
            work_File_1_2_Trade_Dte_Tme.setValueEdited(pnd_Rqst_Log_Dte_Tme_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                                    //Natural: MOVE EDITED #RQST-LOG-DTE-TME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-1-2.TRADE-DTE-TME
            pnd_Check_Trade_Dte_Tme.setValue(pnd_Rqst_Log_Dte_Tme_T);                                                                                                     //Natural: MOVE #RQST-LOG-DTE-TME-T TO #CHECK-TRADE-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_File_1_2_Trade_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Tiaa_Rcvd_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));  //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.TIAA-RCVD-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-1-2.TRADE-DTE-TME
            pnd_Check_Trade_Dte_Tme.setValue(ldaIcwl3010.getIcw_Master_Index_View_Tiaa_Rcvd_Dte_Tme());                                                                   //Natural: MOVE ICW-MASTER-INDEX-VIEW.TIAA-RCVD-DTE-TME TO #CHECK-TRADE-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        //*   NOW CHECK TO SEE IF THE TRADE DATE ON THE MAIN FRAME IS GREATER THAN
        //*   THE TRADE DATE WE PLAN ON USING.(I KNOW THIS SEEMS A LITTLE MISCOMBO
        //*   BULATED, BUT BELIEVE ME IT's the lesser of 2 evils)
        if (condition(ldaIcwl3010.getIcw_Master_Index_View_Trade_Dte_Tme().greater(pnd_Check_Trade_Dte_Tme)))                                                             //Natural: IF ICW-MASTER-INDEX-VIEW.TRADE-DTE-TME > #CHECK-TRADE-DTE-TME
        {
            short decideConditionsMet1563 = 0;                                                                                                                            //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN ICW-MASTER-INDEX-VIEW.INSTN-CLOSE-DTE-TME = 0
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Instn_Close_Dte_Tme().equals(getZero())))
            {
                decideConditionsMet1563++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN ICW-MASTER-INDEX-VIEW.TRADE-DTE-TME < ICW-MASTER-INDEX-VIEW.INSTN-CLOSE-DTE-TME
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Trade_Dte_Tme().less(ldaIcwl3010.getIcw_Master_Index_View_Instn_Close_Dte_Tme())))
            {
                decideConditionsMet1563++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1563 > 0))
            {
                work_File_1_2_Trade_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Trade_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));  //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.TRADE-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-1-2.TRADE-DTE-TME
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet1563 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*   NOW CHECK THE TIAA-RCVD-DTE; IF THE DATE IS GREATER THAN RQST-LOG-DT
        //*   SET THE TIME TO 08:00:00AM JVH
        pnd_Tiaa_Rcvd_Dte_Tme_A.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Tiaa_Rcvd_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                           //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.TIAA-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #TIAA-RCVD-DTE-TME-A
        if (condition(pnd_Tiaa_Rcvd_Dte_Tme_A_Pnd_Tiaa_Rcvd_Dte_Dte.greater(ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Index_Dte())))                                  //Natural: IF #TIAA-RCVD-DTE-DTE GT ICW-MASTER-INDEX-VIEW.RQST-LOG-INDEX-DTE
        {
            pnd_Tiaa_Rcvd_Dte_Tme_A_Pnd_Tiaa_Rcvd_Dte_Tme.setValue(pnd_Eight_Oclock);                                                                                     //Natural: MOVE #EIGHT-OCLOCK TO #TIAA-RCVD-DTE-TME
            pnd_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Tiaa_Rcvd_Dte_Tme_A);                                                                       //Natural: MOVE EDITED #TIAA-RCVD-DTE-TME-A TO #TIME ( EM = YYYYMMDDHHIISST )
            //*   MOVE EDITED #TIME (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
            work_File_1_2_Tiaa_Rcvd_Dte_Tme.setValueEdited(pnd_Time,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                                              //Natural: MOVE EDITED #TIME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-1-2.TIAA-RCVD-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_File_1_2_Tiaa_Rcvd_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Tiaa_Rcvd_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.TIAA-RCVD-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-1-2.TIAA-RCVD-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        work_File_1_2_Rqst_Log_Unit_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Unit_Cde());                                                               //Natural: MOVE ICW-MASTER-INDEX-VIEW.RQST-LOG-UNIT-CDE TO WORK-FILE-1-2.RQST-LOG-UNIT-CDE
        work_File_1_2_Rqst_Log_Oprtr_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Oprtr_Cde());                                                             //Natural: MOVE ICW-MASTER-INDEX-VIEW.RQST-LOG-OPRTR-CDE TO WORK-FILE-1-2.RQST-LOG-OPRTR-CDE
        work_File_1_2_Rqst_Orgn_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Rqst_Orgn_Cde());                                                                       //Natural: MOVE ICW-MASTER-INDEX-VIEW.RQST-ORGN-CDE TO WORK-FILE-1-2.RQST-ORGN-CDE
        work_File_1_2_Crprte_Status_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Crprte_Status_Ind());                                                               //Natural: MOVE ICW-MASTER-INDEX-VIEW.CRPRTE-STATUS-IND TO WORK-FILE-1-2.CRPRTE-STATUS-IND
        work_File_1_2_Sap_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Sap_Ind());                                                                                   //Natural: MOVE ICW-MASTER-INDEX-VIEW.SAP-IND TO WORK-FILE-1-2.SAP-IND
        if (condition(ldaIcwl3010.getIcw_Master_Index_View_List_Data_Rcvd_Dte().greater(getZero())))                                                                      //Natural: IF ICW-MASTER-INDEX-VIEW.LIST-DATA-RCVD-DTE GT 0
        {
            work_File_1_2_List_Data_Rcvd_Dte.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_List_Data_Rcvd_Dte(),new ReportEditMask("'''MM/DD/YYYY'''"));            //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.LIST-DATA-RCVD-DTE ( EM = '"'MM/DD/YYYY'"' ) TO WORK-FILE-1-2.LIST-DATA-RCVD-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_File_1_2_List_Data_Rcvd_Dte.setValue(" ");                                                                                                               //Natural: MOVE ' ' TO WORK-FILE-1-2.LIST-DATA-RCVD-DTE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIcwl3010.getIcw_Master_Index_View_Prtcpnt_Dte().greater(getZero())))                                                                             //Natural: IF ICW-MASTER-INDEX-VIEW.PRTCPNT-DTE GT 0
        {
            work_File_1_2_Prtcpnt_Dte.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Prtcpnt_Dte(),new ReportEditMask("'''MM/DD/YYYY'''"));                          //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.PRTCPNT-DTE ( EM = '"'MM/DD/YYYY'"' ) TO WORK-FILE-1-2.PRTCPNT-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_File_1_2_Prtcpnt_Dte.setValue(" ");                                                                                                                      //Natural: MOVE ' ' TO WORK-FILE-1-2.PRTCPNT-DTE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIcwl3010.getIcw_Master_Index_View_Schdle_Pay_Dte().greater(" ")))                                                                                //Natural: IF ICW-MASTER-INDEX-VIEW.SCHDLE-PAY-DTE GT ' '
        {
            if (condition(! (DbsUtil.maskMatches(ldaIcwl3010.getIcw_Master_Index_View_Schdle_Pay_Dte(),"YYYYMMDD"))))                                                     //Natural: IF ICW-MASTER-INDEX-VIEW.SCHDLE-PAY-DTE NE MASK ( YYYYMMDD )
            {
                getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"***** BAD PAY SCHEDULE DATE ***",NEWLINE,ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Dte_Tme(), //Natural: WRITE /// '***** BAD PAY SCHEDULE DATE ***' / ICW-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME / ICW-MASTER-INDEX-VIEW.PPG-CDE / ICW-MASTER-INDEX-VIEW.SCHDLE-PAY-DTE
                    NEWLINE,ldaIcwl3010.getIcw_Master_Index_View_Ppg_Cde(),NEWLINE,ldaIcwl3010.getIcw_Master_Index_View_Schdle_Pay_Dte());
                if (Global.isEscape()) return;
                work_File_1_2_Schdle_Pay_Dte.setValue(" ");                                                                                                               //Natural: MOVE ' ' TO WORK-FILE-1-2.SCHDLE-PAY-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaIcwl3010.getIcw_Master_Index_View_Schdle_Pay_Dte());                                            //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.SCHDLE-PAY-DTE TO #DATE ( EM = YYYYMMDD )
                work_File_1_2_Schdle_Pay_Dte.setValueEdited(pnd_Date,new ReportEditMask("'''MM/DD/YYYY'''"));                                                             //Natural: MOVE EDITED #DATE ( EM = '"'MM/DD/YYYY'"' ) TO WORK-FILE-1-2.SCHDLE-PAY-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_File_1_2_Schdle_Pay_Dte.setValue(" ");                                                                                                                   //Natural: MOVE ' ' TO WORK-FILE-1-2.SCHDLE-PAY-DTE
        }                                                                                                                                                                 //Natural: END-IF
        work_File_1_2_Multi_Rqst_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Multi_Rqst_Ind());                                                                     //Natural: MOVE ICW-MASTER-INDEX-VIEW.MULTI-RQST-IND TO WORK-FILE-1-2.MULTI-RQST-IND
        work_File_1_2_Elctrn_Fld_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Elctrn_Fld_Ind());                                                                     //Natural: MOVE ICW-MASTER-INDEX-VIEW.ELCTRN-FLD-IND TO WORK-FILE-1-2.ELCTRN-FLD-IND
        work_File_1_2_Check_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Check_Ind());                                                                               //Natural: MOVE ICW-MASTER-INDEX-VIEW.CHECK-IND TO WORK-FILE-1-2.CHECK-IND
        work_File_1_2_Applctns_Rcvd_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Applctns_Rcvd_Ind());                                                               //Natural: MOVE ICW-MASTER-INDEX-VIEW.APPLCTNS-RCVD-IND TO WORK-FILE-1-2.APPLCTNS-RCVD-IND
        work_File_1_2_Dcmnt_Anttn_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Dcmnt_Anttn_Ind());                                                                   //Natural: MOVE ICW-MASTER-INDEX-VIEW.DCMNT-ANTTN-IND TO WORK-FILE-1-2.DCMNT-ANTTN-IND
        work_File_1_2_Img_Paper_Prcssng_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Img_Paper_Prcssng_Ind());                                                       //Natural: MOVE ICW-MASTER-INDEX-VIEW.IMG-PAPER-PRCSSNG-IND TO WORK-FILE-1-2.IMG-PAPER-PRCSSNG-IND
        work_File_1_2_Prcssng_Type_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Prcssng_Type_Ind());                                                                 //Natural: MOVE ICW-MASTER-INDEX-VIEW.PRCSSNG-TYPE-IND TO WORK-FILE-1-2.PRCSSNG-TYPE-IND
        work_File_1_2_Paid_As_Billed_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Paid_As_Billed_Ind());                                                             //Natural: MOVE ICW-MASTER-INDEX-VIEW.PAID-AS-BILLED-IND TO WORK-FILE-1-2.PAID-AS-BILLED-IND
        work_File_1_2_Cmplnt_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Cmplnt_Ind());                                                                             //Natural: MOVE ICW-MASTER-INDEX-VIEW.CMPLNT-IND TO WORK-FILE-1-2.CMPLNT-IND
        work_File_1_2_List_Page_Cnt.setValue(ldaIcwl3010.getIcw_Master_Index_View_List_Page_Cnt());                                                                       //Natural: MOVE ICW-MASTER-INDEX-VIEW.LIST-PAGE-CNT TO WORK-FILE-1-2.LIST-PAGE-CNT
        work_File_1_2_Iis_Hrchy_Level_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Iis_Hrchy_Level_Cde());                                                           //Natural: MOVE ICW-MASTER-INDEX-VIEW.IIS-HRCHY-LEVEL-CDE TO WORK-FILE-1-2.IIS-HRCHY-LEVEL-CDE
        work_File_1_2_Iis_Rgn_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Iis_Rgn_Cde());                                                                           //Natural: MOVE ICW-MASTER-INDEX-VIEW.IIS-RGN-CDE TO WORK-FILE-1-2.IIS-RGN-CDE
        work_File_1_2_Iis_Spcl_Dsgntn_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Iis_Spcl_Dsgntn_Ind());                                                           //Natural: MOVE ICW-MASTER-INDEX-VIEW.IIS-SPCL-DSGNTN-IND TO WORK-FILE-1-2.IIS-SPCL-DSGNTN-IND
        work_File_1_2_Iis_Tiaa_Brnch_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Iis_Tiaa_Brnch_Cde());                                                             //Natural: MOVE ICW-MASTER-INDEX-VIEW.IIS-TIAA-BRNCH-CDE TO WORK-FILE-1-2.IIS-TIAA-BRNCH-CDE
        work_File_1_2_Ny_Denver_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Ny_Denver_Ind());                                                                       //Natural: MOVE ICW-MASTER-INDEX-VIEW.NY-DENVER-IND TO WORK-FILE-1-2.NY-DENVER-IND
        work_File_1_2_Pbs_Apps_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Pbs_Apps_Ind());                                                                         //Natural: MOVE ICW-MASTER-INDEX-VIEW.PBS-APPS-IND TO WORK-FILE-1-2.PBS-APPS-IND
        work_File_1_2_Crprte_On_Tme_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Crprte_On_Tme_Ind());                                                               //Natural: MOVE ICW-MASTER-INDEX-VIEW.CRPRTE-ON-TME-IND TO WORK-FILE-1-2.CRPRTE-ON-TME-IND
        work_File_1_2_Work_Rqst_Prty_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Work_Rqst_Prty_Cde());                                                             //Natural: MOVE ICW-MASTER-INDEX-VIEW.WORK-RQST-PRTY-CDE TO WORK-FILE-1-2.WORK-RQST-PRTY-CDE
        if (condition(ldaIcwl3010.getIcw_Master_Index_View_Crprte_Due_Dte_Tme().greater(getZero())))                                                                      //Natural: IF ICW-MASTER-INDEX-VIEW.CRPRTE-DUE-DTE-TME GT 0
        {
            work_File_1_2_Crprte_Due_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Crprte_Due_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.CRPRTE-DUE-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-1-2.CRPRTE-DUE-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_File_1_2_Crprte_Due_Dte_Tme.setValue(" ");                                                                                                               //Natural: MOVE ' ' TO WORK-FILE-1-2.CRPRTE-DUE-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIcwl3010.getIcw_Master_Index_View_Instn_Close_Dte_Tme().greater(getZero())))                                                                     //Natural: IF ICW-MASTER-INDEX-VIEW.INSTN-CLOSE-DTE-TME GT 0
        {
            work_File_1_2_Instn_Close_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Instn_Close_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.INSTN-CLOSE-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-1-2.INSTN-CLOSE-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_File_1_2_Instn_Close_Dte_Tme.setValue(" ");                                                                                                              //Natural: MOVE ' ' TO WORK-FILE-1-2.INSTN-CLOSE-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIcwl3010.getIcw_Master_Index_View_Crprte_Close_Dte_Tme().greater(getZero())))                                                                    //Natural: IF ICW-MASTER-INDEX-VIEW.CRPRTE-CLOSE-DTE-TME GT 0
        {
            work_File_1_2_Crprte_Close_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Crprte_Close_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.CRPRTE-CLOSE-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-1-2.CRPRTE-CLOSE-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_File_1_2_Crprte_Close_Dte_Tme.setValue(" ");                                                                                                             //Natural: MOVE ' ' TO WORK-FILE-1-2.CRPRTE-CLOSE-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Calc_Days.compute(new ComputeParameters(false, pnd_Calc_Days), ldaIcwl3010.getIcw_Master_Index_View_Accum_Corp_Wait_Hours_Overall().divide(24));              //Natural: ASSIGN #CALC-DAYS := ICW-MASTER-INDEX-VIEW.ACCUM-CORP-WAIT-HOURS-OVERALL / 24
        //*                ICW-MASTER-INDEX-VIEW.ACCUM-CORP-WAIT-HOURS-OVERALL / 24
        work_File_1_2_Corp_Wait_Days_Overall.setValueEdited(pnd_Calc_Days,new ReportEditMask("999999.99"));                                                               //Natural: MOVE EDITED #CALC-DAYS ( EM = 999999.99 ) TO WORK-FILE-1-2.CORP-WAIT-DAYS-OVERALL
        pnd_Calc_Days.compute(new ComputeParameters(false, pnd_Calc_Days), ldaIcwl3010.getIcw_Master_Index_View_Accum_Corp_Prod_Hours_Overall().divide(24));              //Natural: ASSIGN #CALC-DAYS := ICW-MASTER-INDEX-VIEW.ACCUM-CORP-PROD-HOURS-OVERALL / 24
        //*                ICW-MASTER-INDEX-VIEW.ACCUM-CORP-PROD-HOURS-OVERALL / 24
        work_File_1_2_Corp_Process_Days_Overall.setValueEdited(pnd_Calc_Days,new ReportEditMask("999999.99"));                                                            //Natural: MOVE EDITED #CALC-DAYS ( EM = 999999.99 ) TO WORK-FILE-1-2.CORP-PROCESS-DAYS-OVERALL
        pnd_Calc_Days.compute(new ComputeParameters(false, pnd_Calc_Days), ldaIcwl3010.getIcw_Master_Index_View_Accum_Corp_Wait_Hours().divide(24));                      //Natural: ASSIGN #CALC-DAYS := ICW-MASTER-INDEX-VIEW.ACCUM-CORP-WAIT-HOURS / 24
        //*                ICW-MASTER-INDEX-VIEW.ACCUM-CORP-WAIT-HOURS / 24
        work_File_1_2_Corp_Wait_Days.setValueEdited(pnd_Calc_Days,new ReportEditMask("999999.99"));                                                                       //Natural: MOVE EDITED #CALC-DAYS ( EM = 999999.99 ) TO WORK-FILE-1-2.CORP-WAIT-DAYS
        pnd_Calc_Days.compute(new ComputeParameters(false, pnd_Calc_Days), ldaIcwl3010.getIcw_Master_Index_View_Accum_Corp_Prod_Hours().divide(24));                      //Natural: ASSIGN #CALC-DAYS := ICW-MASTER-INDEX-VIEW.ACCUM-CORP-PROD-HOURS / 24
        //*                ICW-MASTER-INDEX-VIEW.ACCUM-CORP-PROD-HOURS / 24
        work_File_1_2_Corp_Process_Days.setValueEdited(pnd_Calc_Days,new ReportEditMask("999999.99"));                                                                    //Natural: MOVE EDITED #CALC-DAYS ( EM = 999999.99 ) TO WORK-FILE-1-2.CORP-PROCESS-DAYS
        work_File_1_2_Corp_Trnr_Buss_Days.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Corp_Turnaround(),new ReportEditMask("999999.99"));                         //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.CORP-TURNAROUND ( EM = 999999.99 ) TO WORK-FILE-1-2.CORP-TRNR-BUSS-DAYS
        pnd_Corp_Trnr_Clndr_Days.reset();                                                                                                                                 //Natural: RESET #CORP-TRNR-CLNDR-DAYS
        pnd_Rqst_Log_Dte_Tme_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Dte_Tme());                             //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO #RQST-LOG-DTE-TME-T ( EM = YYYYMMDDHHIISST )
        if (condition(ldaIcwl3010.getIcw_Master_Index_View_Instn_Close_Dte_Tme().greater(getZero())))                                                                     //Natural: IF ICW-MASTER-INDEX-VIEW.INSTN-CLOSE-DTE-TME > 0
        {
            //*  PERFORM CHECK-FOR-RESTART
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Tiaa_Rcvd_Dte_Tme().less(pnd_Rqst_Log_Dte_Tme_T)))                                                         //Natural: IF ICW-MASTER-INDEX-VIEW.TIAA-RCVD-DTE-TME < #RQST-LOG-DTE-TME-T
            {
                pnd_Corp_Trnr_Clndr_Start.setValue(ldaIcwl3010.getIcw_Master_Index_View_Tiaa_Rcvd_Dte_Tme());                                                             //Natural: MOVE ICW-MASTER-INDEX-VIEW.TIAA-RCVD-DTE-TME TO #CORP-TRNR-CLNDR-START
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Corp_Trnr_Clndr_Start.setValue(pnd_Rqst_Log_Dte_Tme_T);                                                                                               //Natural: MOVE #RQST-LOG-DTE-TME-T TO #CORP-TRNR-CLNDR-START
            }                                                                                                                                                             //Natural: END-IF
            //*  GET ELAPSED CALENDAR DAYS
            DbsUtil.callnat(Icwn9376.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pnd_Corp_Trnr_Clndr_Start, ldaIcwl3010.getIcw_Master_Index_View_Instn_Close_Dte_Tme(),  //Natural: CALLNAT 'ICWN9376' MSG-INFO-SUB #CORP-TRNR-CLNDR-START ICW-MASTER-INDEX-VIEW.INSTN-CLOSE-DTE-TME #CORP-TRNR-CLNDR-DAYS
                pnd_Corp_Trnr_Clndr_Days);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        work_File_1_2_Corp_Trnr_Clndr_Days.setValueEdited(pnd_Corp_Trnr_Clndr_Days,new ReportEditMask("999999.99"));                                                      //Natural: MOVE EDITED #CORP-TRNR-CLNDR-DAYS ( EM = 999999.99 ) TO WORK-FILE-1-2.CORP-TRNR-CLNDR-DAYS
        work_File_1_2_Step_Re_Do_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Step_Re_Do_Ind());                                                                     //Natural: MOVE ICW-MASTER-INDEX-VIEW.STEP-RE-DO-IND TO WORK-FILE-1-2.STEP-RE-DO-IND
        work_File_1_2_Rt_Sqnce_Nbr.setValue(ldaIcwl3010.getIcw_Master_Index_View_Rt_Sqnce_Nbr());                                                                         //Natural: MOVE ICW-MASTER-INDEX-VIEW.RT-SQNCE-NBR TO WORK-FILE-1-2.RT-SQNCE-NBR
        work_File_1_2_Unit_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Unit_Cde());                                                                                 //Natural: MOVE ICW-MASTER-INDEX-VIEW.UNIT-CDE TO WORK-FILE-1-2.UNIT-CDE
        work_File_1_2_Racf_Id.setValue(ldaIcwl3010.getIcw_Master_Index_View_Empl_Racf_Id());                                                                              //Natural: MOVE ICW-MASTER-INDEX-VIEW.EMPL-RACF-ID TO WORK-FILE-1-2.RACF-ID
        work_File_1_2_Step_Id.setValue(ldaIcwl3010.getIcw_Master_Index_View_Step_Id());                                                                                   //Natural: MOVE ICW-MASTER-INDEX-VIEW.STEP-ID TO WORK-FILE-1-2.STEP-ID
        work_File_1_2_Status_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Status_Cde());                                                                             //Natural: MOVE ICW-MASTER-INDEX-VIEW.STATUS-CDE TO WORK-FILE-1-2.STATUS-CDE
        work_File_1_2_Cash_Match_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Cash_Match_Ind());                                                                     //Natural: MOVE ICW-MASTER-INDEX-VIEW.CASH-MATCH-IND TO WORK-FILE-1-2.CASH-MATCH-IND
        work_File_1_2_Iris_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Iris_Ind());                                                                                 //Natural: MOVE ICW-MASTER-INDEX-VIEW.IRIS-IND TO WORK-FILE-1-2.IRIS-IND
        work_File_1_2_Cnnctd_Rqst_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Cnnctd_Rqst_Ind());                                                                   //Natural: MOVE ICW-MASTER-INDEX-VIEW.CNNCTD-RQST-IND TO WORK-FILE-1-2.CNNCTD-RQST-IND
        work_File_1_2_Init_Undrlyng_Rqst_Cnt.setValue(ldaIcwl3010.getIcw_Master_Index_View_Init_Undrlyng_Rqst_Cnt());                                                     //Natural: MOVE ICW-MASTER-INDEX-VIEW.INIT-UNDRLYNG-RQST-CNT TO WORK-FILE-1-2.INIT-UNDRLYNG-RQST-CNT
        work_File_1_2_Crrnt_Cmt_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Crrnt_Cmt_Ind());                                                                       //Natural: MOVE ICW-MASTER-INDEX-VIEW.CRRNT-CMT-IND TO WORK-FILE-1-2.CRRNT-CMT-IND
        work_File_1_2_Log_Sqnce_Nbr.setValue(ldaIcwl3010.getIcw_Master_Index_View_Log_Sqnce_Nbr());                                                                       //Natural: MOVE ICW-MASTER-INDEX-VIEW.LOG-SQNCE-NBR TO WORK-FILE-1-2.LOG-SQNCE-NBR
        work_File_1_2_Alwys_Vrfy.setValue(ldaIcwl3010.getIcw_Master_Index_View_Alwys_Vrfy());                                                                             //Natural: MOVE ICW-MASTER-INDEX-VIEW.ALWYS-VRFY TO WORK-FILE-1-2.ALWYS-VRFY
        work_File_1_2_Crs_Memo.setValue(ldaIcwl3010.getIcw_Master_Index_View_Crs_Memo());                                                                                 //Natural: MOVE ICW-MASTER-INDEX-VIEW.CRS-MEMO TO WORK-FILE-1-2.CRS-MEMO
        work_File_1_2_Pas_Ovrrde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Pas_Ovrrde());                                                                             //Natural: MOVE ICW-MASTER-INDEX-VIEW.PAS-OVRRDE TO WORK-FILE-1-2.PAS-OVRRDE
        work_File_1_2_Pas_Ind.setValue(ldaIcwl3010.getIcw_Master_Index_View_Pas_Ind());                                                                                   //Natural: MOVE ICW-MASTER-INDEX-VIEW.PAS-IND TO WORK-FILE-1-2.PAS-IND
                                                                                                                                                                          //Natural: PERFORM FILL-IN-A-ATSIGN
        sub_Fill_In_A_Atsign();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Add_Updt_Sw.equals("U")))                                                                                                                       //Natural: IF #ADD-UPDT-SW = 'U'
        {
            getWorkFiles().write(1, false, work_File_1_2);                                                                                                                //Natural: WRITE WORK FILE 1 WORK-FILE-1-2
            pnd_Up_Wr_Cntr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #UP-WR-CNTR
                                                                                                                                                                          //Natural: PERFORM CHECK-REASON-CDE-UPD
            sub_Check_Reason_Cde_Upd();
            if (condition(Global.isEscape())) {return;}
            pnd_Up_Rsn_Cde_Cntr.nadd(pnd_Up_Rsn_Temp_Cntr);                                                                                                               //Natural: ADD #UP-RSN-TEMP-CNTR TO #UP-RSN-CDE-CNTR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getWorkFiles().write(2, false, work_File_1_2);                                                                                                                //Natural: WRITE WORK FILE 2 WORK-FILE-1-2
            pnd_Ad_Wr_Cntr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #AD-WR-CNTR
                                                                                                                                                                          //Natural: PERFORM CHECK-REASON-CDE-ADD
            sub_Check_Reason_Cde_Add();
            if (condition(Global.isEscape())) {return;}
            pnd_Ad_Rsn_Cde_Cntr.nadd(pnd_Ad_Rsn_Temp_Cntr);                                                                                                               //Natural: ADD #AD-RSN-TEMP-CNTR TO #AD-RSN-CDE-CNTR
        }                                                                                                                                                                 //Natural: END-IF
        work_File_1_2.reset();                                                                                                                                            //Natural: RESET WORK-FILE-1-2
        //*  WRITE-WR
    }
    private void sub_Check_Reason_Cde_Upd() throws Exception                                                                                                              //Natural: CHECK-REASON-CDE-UPD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ====================================
        pnd_Up_Rsn_Temp_Cntr.reset();                                                                                                                                     //Natural: RESET #UP-RSN-TEMP-CNTR
        work_File_9_10.reset();                                                                                                                                           //Natural: RESET WORK-FILE-9-10
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Rsn_Cde().getValue(pnd_I).equals(" ")))                                                                    //Natural: IF ICW-MASTER-INDEX-VIEW.RSN-CDE ( #I ) EQ ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Dte_Tme());                                   //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO #TIME ( EM = YYYYMMDDHHIISST )
                work_File_9_10_Rqst_Log_Dte_Tme.setValueEdited(pnd_Time,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                                        //Natural: MOVE EDITED #TIME ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO WORK-FILE-9-10.RQST-LOG-DTE-TME
                work_File_9_10_Reason_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Rsn_Cde().getValue(pnd_I));                                                       //Natural: MOVE ICW-MASTER-INDEX-VIEW.RSN-CDE ( #I ) TO WORK-FILE-9-10.REASON-CDE
                work_File_9_10_Seqnce_Nbr.setValue(pnd_I);                                                                                                                //Natural: MOVE #I TO WORK-FILE-9-10.SEQNCE-NBR
                work_File_9_10_G01.setValue("@");                                                                                                                         //Natural: MOVE '@' TO G01 G02
                work_File_9_10_G02.setValue("@");
                getWorkFiles().write(9, false, work_File_9_10);                                                                                                           //Natural: WRITE WORK FILE 9 WORK-FILE-9-10
                pnd_Up_Rsn_Temp_Cntr.nadd(1);                                                                                                                             //Natural: ADD 1 TO #UP-RSN-TEMP-CNTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_Reason_Cde_Add() throws Exception                                                                                                              //Natural: CHECK-REASON-CDE-ADD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ====================================
        pnd_Ad_Rsn_Temp_Cntr.reset();                                                                                                                                     //Natural: RESET #AD-RSN-TEMP-CNTR
        work_File_9_10.reset();                                                                                                                                           //Natural: RESET WORK-FILE-9-10
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaIcwl3010.getIcw_Master_Index_View_Rsn_Cde().getValue(pnd_I).equals(" ")))                                                                    //Natural: IF ICW-MASTER-INDEX-VIEW.RSN-CDE ( #I ) EQ ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Dte_Tme());                                   //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO #TIME ( EM = YYYYMMDDHHIISST )
                work_File_9_10_Rqst_Log_Dte_Tme.setValueEdited(pnd_Time,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                                        //Natural: MOVE EDITED #TIME ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO WORK-FILE-9-10.RQST-LOG-DTE-TME
                work_File_9_10_Reason_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Rsn_Cde().getValue(pnd_I));                                                       //Natural: MOVE ICW-MASTER-INDEX-VIEW.RSN-CDE ( #I ) TO WORK-FILE-9-10.REASON-CDE
                work_File_9_10_Seqnce_Nbr.setValue(pnd_I);                                                                                                                //Natural: MOVE #I TO WORK-FILE-9-10.SEQNCE-NBR
                work_File_9_10_G01.setValue("@");                                                                                                                         //Natural: MOVE '@' TO G01 G02
                work_File_9_10_G02.setValue("@");
                getWorkFiles().write(10, false, work_File_9_10);                                                                                                          //Natural: WRITE WORK FILE 10 WORK-FILE-9-10
                pnd_Ad_Rsn_Temp_Cntr.nadd(1);                                                                                                                             //Natural: ADD 1 TO #AD-RSN-TEMP-CNTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Open_Activity() throws Exception                                                                                                               //Natural: WRITE-OPEN-ACTIVITY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===================================
        if (condition(ldaIcwl3010.getIcw_Master_Index_View_Unit_Actvty_Start_Status().equals(" ")))                                                                       //Natural: IF ICW-MASTER-INDEX-VIEW.UNIT-ACTVTY-START-STATUS = ' '
        {
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Unit-Actvty-Start-Status is empty ???",ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Dte_Tme());          //Natural: WRITE /'Unit-Actvty-Start-Status is empty ???' ICW-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
            if (Global.isEscape()) return;
            //*   ESCAPE ROUTINE IMMEDIATE
            //*  ELSE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FILL-IN-OP-ACTIVITY
        sub_Fill_In_Op_Activity();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FILL-IN-B-ATSIGN
        sub_Fill_In_B_Atsign();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().write(4, false, work_File_3_4);                                                                                                                    //Natural: WRITE WORK FILE 4 WORK-FILE-3-4
        pnd_Ad_Ac_Cntr.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #AD-AC-CNTR
        work_File_3_4.reset();                                                                                                                                            //Natural: RESET WORK-FILE-3-4
    }
    private void sub_Fill_In_Op_Activity() throws Exception                                                                                                               //Natural: FILL-IN-OP-ACTIVITY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===================================
        pnd_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Dte_Tme());                                           //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO #TIME ( EM = YYYYMMDDHHIISST )
        work_File_3_4_Rqst_Log_Dte_Tme.setValueEdited(pnd_Time,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                                                 //Natural: MOVE EDITED #TIME ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO WORK-FILE-3-4.RQST-LOG-DTE-TME
        work_File_3_4_Activity_Type.setValue(pnd_Activity_Type);                                                                                                          //Natural: MOVE #ACTIVITY-TYPE TO WORK-FILE-3-4.ACTIVITY-TYPE
        work_File_3_4_Strtng_Event_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Strtng_Event_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.STRTNG-EVENT-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-3-4.STRTNG-EVENT-DTE-TME
        work_File_3_4_Ending_Event_Dte_Tme.setValue(" ");                                                                                                                 //Natural: MOVE ' ' TO WORK-FILE-3-4.ENDING-EVENT-DTE-TME
        if (condition(pnd_Activity_Type.equals("01")))                                                                                                                    //Natural: IF #ACTIVITY-TYPE = '01'
        {
            work_File_3_4_Activity_Type.setValue("UNIT    ");                                                                                                             //Natural: MOVE 'UNIT    ' TO WORK-FILE-3-4.ACTIVITY-TYPE
            work_File_3_4_Strtng_Event_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Unit_Start_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.UNIT-START-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-3-4.STRTNG-EVENT-DTE-TME
            work_File_3_4_Strtng_Status_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Unit_Actvty_Start_Status());                                                    //Natural: MOVE ICW-MASTER-INDEX-VIEW.UNIT-ACTVTY-START-STATUS TO WORK-FILE-3-4.STRTNG-STATUS-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Activity_Type.equals("02")))                                                                                                                    //Natural: IF #ACTIVITY-TYPE = '02'
        {
            work_File_3_4_Activity_Type.setValue("EMPLOYEE");                                                                                                             //Natural: MOVE 'EMPLOYEE' TO WORK-FILE-3-4.ACTIVITY-TYPE
            work_File_3_4_Strtng_Event_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Empl_Start_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.EMPL-START-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-3-4.STRTNG-EVENT-DTE-TME
            work_File_3_4_Strtng_Status_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Empl_Actvty_Start_Status());                                                    //Natural: MOVE ICW-MASTER-INDEX-VIEW.EMPL-ACTVTY-START-STATUS TO WORK-FILE-3-4.STRTNG-STATUS-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Activity_Type.equals("03")))                                                                                                                    //Natural: IF #ACTIVITY-TYPE = '03'
        {
            work_File_3_4_Activity_Type.setValue("STEP    ");                                                                                                             //Natural: MOVE 'STEP    ' TO WORK-FILE-3-4.ACTIVITY-TYPE
            work_File_3_4_Strtng_Event_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Step_Start_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.STEP-START-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-3-4.STRTNG-EVENT-DTE-TME
            work_File_3_4_Strtng_Status_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Step_Actvty_Start_Status());                                                    //Natural: MOVE ICW-MASTER-INDEX-VIEW.STEP-ACTVTY-START-STATUS TO WORK-FILE-3-4.STRTNG-STATUS-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Activity_Type.equals("04")))                                                                                                                    //Natural: IF #ACTIVITY-TYPE = '04'
        {
            work_File_3_4_Activity_Type.setValue("STATUS  ");                                                                                                             //Natural: MOVE 'STATUS  ' TO WORK-FILE-3-4.ACTIVITY-TYPE
            work_File_3_4_Strtng_Event_Dte_Tme.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Status_Updte_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.STATUS-UPDTE-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-3-4.STRTNG-EVENT-DTE-TME
            work_File_3_4_Strtng_Status_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Status_Cde());                                                                  //Natural: MOVE ICW-MASTER-INDEX-VIEW.STATUS-CDE TO WORK-FILE-3-4.STRTNG-STATUS-CDE
        }                                                                                                                                                                 //Natural: END-IF
        work_File_3_4_Rt_Sqnce_Nbr.setValue(ldaIcwl3010.getIcw_Master_Index_View_Rt_Sqnce_Nbr());                                                                         //Natural: MOVE ICW-MASTER-INDEX-VIEW.RT-SQNCE-NBR TO WORK-FILE-3-4.RT-SQNCE-NBR
        work_File_3_4_Unit_Cde.setValue(ldaIcwl3010.getIcw_Master_Index_View_Unit_Cde());                                                                                 //Natural: MOVE ICW-MASTER-INDEX-VIEW.UNIT-CDE TO WORK-FILE-3-4.UNIT-CDE
        work_File_3_4_Racf_Id.setValue(ldaIcwl3010.getIcw_Master_Index_View_Empl_Racf_Id());                                                                              //Natural: MOVE ICW-MASTER-INDEX-VIEW.EMPL-RACF-ID TO WORK-FILE-3-4.RACF-ID
        work_File_3_4_Step_Id.setValue(ldaIcwl3010.getIcw_Master_Index_View_Step_Id());                                                                                   //Natural: MOVE ICW-MASTER-INDEX-VIEW.STEP-ID TO WORK-FILE-3-4.STEP-ID
        //*  VE ICW-MASTER-INDEX-VIEW.STATUS-CDE  TO WORK-FILE-3-4.STRTNG-STATUS-CDE
        work_File_3_4_Ending_Status_Cde.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO WORK-FILE-3-4.ENDING-STATUS-CDE
        work_File_3_4_Actvty_Wait_Days.setValue("000000.00");                                                                                                             //Natural: MOVE '000000.00' TO WORK-FILE-3-4.ACTVTY-WAIT-DAYS WORK-FILE-3-4.ACTVTY-PROCESS-DAYS WORK-FILE-3-4.UNIT-TURNAROUND-DAYS
        work_File_3_4_Actvty_Process_Days.setValue("000000.00");
        work_File_3_4_Unit_Turnaround_Days.setValue("000000.00");
        work_File_3_4_Unit_Turnaround_Start.setValue(" ");                                                                                                                //Natural: MOVE ' ' TO WORK-FILE-3-4.UNIT-TURNAROUND-START
        //*  FILL-IN-OP-ACTIVITY
    }
    private void sub_Write_Close_Activity() throws Exception                                                                                                              //Natural: WRITE-CLOSE-ACTIVITY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ====================================
        work_File_3_4.reset();                                                                                                                                            //Natural: RESET WORK-FILE-3-4
                                                                                                                                                                          //Natural: PERFORM FILL-IN-CL-ACTIVITY
        sub_Fill_In_Cl_Activity();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FILL-IN-B-ATSIGN
        sub_Fill_In_B_Atsign();
        if (condition(Global.isEscape())) {return;}
        pnd_Compare_Dte_Tme.setValueEdited(ldaIcwl3015.getIcw_Activity_View_Strtng_Event_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                                //Natural: MOVE EDITED ICW-ACTIVITY-VIEW.STRTNG-EVENT-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #COMPARE-DTE-TME
        if (condition(pnd_Compare_Dte_Tme_Pnd_Compare_Dte_Tme_N.greaterOrEqual(pnd_Time_Window_Pnd_Starting_Numeric)))                                                    //Natural: IF #COMPARE-DTE-TME-N GE #STARTING-NUMERIC
        {
            getWorkFiles().write(4, false, work_File_3_4);                                                                                                                //Natural: WRITE WORK FILE 4 WORK-FILE-3-4
            pnd_Cl_Ac_Cntr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CL-AC-CNTR
            pnd_Ad_Ac_Cntr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #AD-AC-CNTR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getWorkFiles().write(3, false, work_File_3_4);                                                                                                                //Natural: WRITE WORK FILE 3 WORK-FILE-3-4
            pnd_Up_Ac_Cntr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #UP-AC-CNTR
        }                                                                                                                                                                 //Natural: END-IF
        work_File_3_4.reset();                                                                                                                                            //Natural: RESET WORK-FILE-3-4
    }
    private void sub_Fill_In_Cl_Activity() throws Exception                                                                                                               //Natural: FILL-IN-CL-ACTIVITY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===================================
        pnd_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaIcwl3015.getIcw_Activity_View_Rqst_Log_Dte_Tme());                                               //Natural: MOVE EDITED ICW-ACTIVITY-VIEW.RQST-LOG-DTE-TME TO #TIME ( EM = YYYYMMDDHHIISST )
        work_File_3_4_Rqst_Log_Dte_Tme.setValueEdited(pnd_Time,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                                                 //Natural: MOVE EDITED #TIME ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO WORK-FILE-3-4.RQST-LOG-DTE-TME
        if (condition(ldaIcwl3015.getIcw_Activity_View_Actvty_Type().equals("01")))                                                                                       //Natural: IF ICW-ACTIVITY-VIEW.ACTVTY-TYPE = '01'
        {
            work_File_3_4_Activity_Type.setValue("UNIT    ");                                                                                                             //Natural: MOVE 'UNIT    ' TO WORK-FILE-3-4.ACTIVITY-TYPE
            work_File_3_4_Unit_On_Tme_Ind.setValue(ldaIcwl3015.getIcw_Activity_View_Spcl_Prcss_Ind());                                                                    //Natural: MOVE ICW-ACTIVITY-VIEW.SPCL-PRCSS-IND TO WORK-FILE-3-4.UNIT-ON-TME-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIcwl3015.getIcw_Activity_View_Actvty_Type().equals("02")))                                                                                   //Natural: IF ICW-ACTIVITY-VIEW.ACTVTY-TYPE = '02'
            {
                work_File_3_4_Activity_Type.setValue("EMPLOYEE");                                                                                                         //Natural: MOVE 'EMPLOYEE' TO WORK-FILE-3-4.ACTIVITY-TYPE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaIcwl3015.getIcw_Activity_View_Actvty_Type().equals("03")))                                                                               //Natural: IF ICW-ACTIVITY-VIEW.ACTVTY-TYPE = '03'
                {
                    work_File_3_4_Activity_Type.setValue("STEP    ");                                                                                                     //Natural: MOVE 'STEP    ' TO WORK-FILE-3-4.ACTIVITY-TYPE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaIcwl3015.getIcw_Activity_View_Actvty_Type().equals("04")))                                                                           //Natural: IF ICW-ACTIVITY-VIEW.ACTVTY-TYPE = '04'
                    {
                        work_File_3_4_Activity_Type.setValue("STATUS  ");                                                                                                 //Natural: MOVE 'STATUS  ' TO WORK-FILE-3-4.ACTIVITY-TYPE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        work_File_3_4_Activity_Type.setValue("        ");                                                                                                 //Natural: MOVE '        ' TO WORK-FILE-3-4.ACTIVITY-TYPE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        work_File_3_4_Strtng_Event_Dte_Tme.setValueEdited(ldaIcwl3015.getIcw_Activity_View_Strtng_Event_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED ICW-ACTIVITY-VIEW.STRTNG-EVENT-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-3-4.STRTNG-EVENT-DTE-TME
        work_File_3_4_Ending_Event_Dte_Tme.setValueEdited(ldaIcwl3015.getIcw_Activity_View_Endng_Event_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED ICW-ACTIVITY-VIEW.ENDNG-EVENT-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-3-4.ENDING-EVENT-DTE-TME
        work_File_3_4_Rt_Sqnce_Nbr.setValue(ldaIcwl3015.getIcw_Activity_View_Rt_Sqnce_Nbr());                                                                             //Natural: MOVE ICW-ACTIVITY-VIEW.RT-SQNCE-NBR TO WORK-FILE-3-4.RT-SQNCE-NBR
        work_File_3_4_Unit_Cde.setValue(ldaIcwl3015.getIcw_Activity_View_Unit_Cde());                                                                                     //Natural: MOVE ICW-ACTIVITY-VIEW.UNIT-CDE TO WORK-FILE-3-4.UNIT-CDE
        work_File_3_4_Racf_Id.setValue(ldaIcwl3015.getIcw_Activity_View_Empl_Racf_Id());                                                                                  //Natural: MOVE ICW-ACTIVITY-VIEW.EMPL-RACF-ID TO WORK-FILE-3-4.RACF-ID
        work_File_3_4_Step_Id.setValue(ldaIcwl3015.getIcw_Activity_View_Step_Id());                                                                                       //Natural: MOVE ICW-ACTIVITY-VIEW.STEP-ID TO WORK-FILE-3-4.STEP-ID
        work_File_3_4_Strtng_Status_Cde.setValue(ldaIcwl3015.getIcw_Activity_View_Start_Status_Cde());                                                                    //Natural: MOVE ICW-ACTIVITY-VIEW.START-STATUS-CDE TO WORK-FILE-3-4.STRTNG-STATUS-CDE
        work_File_3_4_Ending_Status_Cde.setValue(ldaIcwl3015.getIcw_Activity_View_End_Status_Cde());                                                                      //Natural: MOVE ICW-ACTIVITY-VIEW.END-STATUS-CDE TO WORK-FILE-3-4.ENDING-STATUS-CDE
        pnd_Calc_Days.compute(new ComputeParameters(false, pnd_Calc_Days), ldaIcwl3015.getIcw_Activity_View_Actvty_Wait_Hours().divide(24));                              //Natural: ASSIGN #CALC-DAYS := ICW-ACTIVITY-VIEW.ACTVTY-WAIT-HOURS / 24
        //*                ICW-ACTIVITY-VIEW.ACTVTY-WAIT-HOURS / 24
        work_File_3_4_Actvty_Wait_Days.setValueEdited(pnd_Calc_Days,new ReportEditMask("999999.99"));                                                                     //Natural: MOVE EDITED #CALC-DAYS ( EM = 999999.99 ) TO WORK-FILE-3-4.ACTVTY-WAIT-DAYS
        pnd_Calc_Days.compute(new ComputeParameters(false, pnd_Calc_Days), ldaIcwl3015.getIcw_Activity_View_Actvty_Prod_Hours().divide(24));                              //Natural: ASSIGN #CALC-DAYS := ICW-ACTIVITY-VIEW.ACTVTY-PROD-HOURS / 24
        //*                ICW-ACTIVITY-VIEW.ACTVTY-PROD-HOURS / 24
        work_File_3_4_Actvty_Process_Days.setValueEdited(pnd_Calc_Days,new ReportEditMask("999999.99"));                                                                  //Natural: MOVE EDITED #CALC-DAYS ( EM = 999999.99 ) TO WORK-FILE-3-4.ACTVTY-PROCESS-DAYS
        if (condition(ldaIcwl3015.getIcw_Activity_View_Unit_Turnaround_Start_Dte_Tme().greater(getZero())))                                                               //Natural: IF ICW-ACTIVITY-VIEW.UNIT-TURNAROUND-START-DTE-TME GT 0
        {
            work_File_3_4_Unit_Turnaround_Start.setValueEdited(ldaIcwl3015.getIcw_Activity_View_Unit_Turnaround_Start_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED ICW-ACTIVITY-VIEW.UNIT-TURNAROUND-START-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-3-4.UNIT-TURNAROUND-START
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_File_3_4_Unit_Turnaround_Start.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO WORK-FILE-3-4.UNIT-TURNAROUND-START
        }                                                                                                                                                                 //Natural: END-IF
        work_File_3_4_Unit_Turnaround_Days.setValueEdited(ldaIcwl3015.getIcw_Activity_View_Unit_Turnaround(),new ReportEditMask("999999.99"));                            //Natural: MOVE EDITED ICW-ACTIVITY-VIEW.UNIT-TURNAROUND ( EM = 999999.99 ) TO WORK-FILE-3-4.UNIT-TURNAROUND-DAYS
        //*  FILL-IN-CL-ACTIVITY
    }
    private void sub_Extract_Ppg_Inst() throws Exception                                                                                                                  //Natural: EXTRACT-PPG-INST
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        ldaIcwl3014.getVw_iis_Prem_Pymnt_Grp_View().startDatabaseRead                                                                                                     //Natural: READ IIS-PREM-PYMNT-GRP-VIEW PHYSICAL SEQUENCE
        (
        "READ02",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        READ02:
        while (condition(ldaIcwl3014.getVw_iis_Prem_Pymnt_Grp_View().readNextRow("READ02")))
        {
            work_File_6.setValuesByName(ldaIcwl3014.getVw_iis_Prem_Pymnt_Grp_View());                                                                                     //Natural: MOVE BY NAME IIS-PREM-PYMNT-GRP-VIEW TO WORK-FILE-6
            //*  ------------- CHECKING REQUIRED FIELDS ----------------------
            if (condition(work_File_6_Orgnztn_Hrchy_Link_Instn_Cde.equals(getZero()) || work_File_6_Ppg_Cde.equals(" ") || work_File_6_Ppg_Nme.equals(" ")))              //Natural: IF WORK-FILE-6.ORGNZTN-HRCHY-LINK-INSTN-CDE = 0 OR WORK-FILE-6.PPG-CDE = ' ' OR WORK-FILE-6.PPG-NME = ' '
            {
                //*      WRITE /'PPG Record was rejected....ISN = ' *ISN
                pnd_Ppg_Rejected.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #PPG-REJECTED
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------- CHECKING REQUIRED FIELDS ----------------------
            DbsUtil.examine(new ExamineSource(work_File_6_Ppg_Nme), new ExamineSearch("@"), new ExamineReplace(" "));                                                     //Natural: EXAMINE WORK-FILE-6.PPG-NME FOR '@' REPLACE WITH ' '
                                                                                                                                                                          //Natural: PERFORM FILL-IN-D-ATSIGN
            sub_Fill_In_D_Atsign();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(6, false, work_File_6);                                                                                                                  //Natural: WRITE WORK FILE 6 WORK-FILE-6
            pnd_Ppg_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #PPG-CNTR
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  READ IIS-ORGNZTN-HRCHY
        ldaIcwl3016.getVw_iis_Orgnztn_Hrchy_View().startDatabaseRead                                                                                                      //Natural: READ IIS-ORGNZTN-HRCHY-VIEW PHYSICAL
        (
        "READ03",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        READ03:
        while (condition(ldaIcwl3016.getVw_iis_Orgnztn_Hrchy_View().readNextRow("READ03")))
        {
            work_File_7.setValuesByName(ldaIcwl3016.getVw_iis_Orgnztn_Hrchy_View());                                                                                      //Natural: MOVE BY NAME IIS-ORGNZTN-HRCHY-VIEW TO WORK-FILE-7
            DbsUtil.examine(new ExamineSource(work_File_7_Orgnztn_Legal_Nme), new ExamineSearch("@"), new ExamineReplace(" "));                                           //Natural: EXAMINE WORK-FILE-7.ORGNZTN-LEGAL-NME FOR '@' REPLACE WITH ' '
                                                                                                                                                                          //Natural: PERFORM FILL-IN-E-ATSIGN
            sub_Fill_In_E_Atsign();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(7, false, work_File_7);                                                                                                                  //Natural: WRITE WORK FILE 7 WORK-FILE-7
            pnd_Org_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #ORG-CNTR
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ================================
    }
    private void sub_Fill_In_A_Atsign() throws Exception                                                                                                                  //Natural: FILL-IN-A-ATSIGN
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        work_File_1_2_A01.setValue(pnd_Atsign);                                                                                                                           //Natural: MOVE #ATSIGN TO A01 A02 A03 A04 A05 A06 A07 A08 A09 A10 A11 A12 A13 A14 A15 A16 A17 A18 A19 A20 A21 A22 A23 A24 A25 A26 A27 A28 A29 A30 A31 A32 A33 A34 A35 A36 A37 A38 A39 A40 A41 A42 A43 A44 A45 A46 A47 A48 A49 A50 A51 A52 A53 A54 A55 A56 A57 A58
        work_File_1_2_A02.setValue(pnd_Atsign);
        work_File_1_2_A03.setValue(pnd_Atsign);
        work_File_1_2_A04.setValue(pnd_Atsign);
        work_File_1_2_A05.setValue(pnd_Atsign);
        work_File_1_2_A06.setValue(pnd_Atsign);
        work_File_1_2_A07.setValue(pnd_Atsign);
        work_File_1_2_A08.setValue(pnd_Atsign);
        work_File_1_2_A09.setValue(pnd_Atsign);
        work_File_1_2_A10.setValue(pnd_Atsign);
        work_File_1_2_A11.setValue(pnd_Atsign);
        work_File_1_2_A12.setValue(pnd_Atsign);
        work_File_1_2_A13.setValue(pnd_Atsign);
        work_File_1_2_A14.setValue(pnd_Atsign);
        work_File_1_2_A15.setValue(pnd_Atsign);
        work_File_1_2_A16.setValue(pnd_Atsign);
        work_File_1_2_A17.setValue(pnd_Atsign);
        work_File_1_2_A18.setValue(pnd_Atsign);
        work_File_1_2_A19.setValue(pnd_Atsign);
        work_File_1_2_A20.setValue(pnd_Atsign);
        work_File_1_2_A21.setValue(pnd_Atsign);
        work_File_1_2_A22.setValue(pnd_Atsign);
        work_File_1_2_A23.setValue(pnd_Atsign);
        work_File_1_2_A24.setValue(pnd_Atsign);
        work_File_1_2_A25.setValue(pnd_Atsign);
        work_File_1_2_A26.setValue(pnd_Atsign);
        work_File_1_2_A27.setValue(pnd_Atsign);
        work_File_1_2_A28.setValue(pnd_Atsign);
        work_File_1_2_A29.setValue(pnd_Atsign);
        work_File_1_2_A30.setValue(pnd_Atsign);
        work_File_1_2_A31.setValue(pnd_Atsign);
        work_File_1_2_A32.setValue(pnd_Atsign);
        work_File_1_2_A33.setValue(pnd_Atsign);
        work_File_1_2_A34.setValue(pnd_Atsign);
        work_File_1_2_A35.setValue(pnd_Atsign);
        work_File_1_2_A36.setValue(pnd_Atsign);
        work_File_1_2_A37.setValue(pnd_Atsign);
        work_File_1_2_A38.setValue(pnd_Atsign);
        work_File_1_2_A39.setValue(pnd_Atsign);
        work_File_1_2_A40.setValue(pnd_Atsign);
        work_File_1_2_A41.setValue(pnd_Atsign);
        work_File_1_2_A42.setValue(pnd_Atsign);
        work_File_1_2_A43.setValue(pnd_Atsign);
        work_File_1_2_A44.setValue(pnd_Atsign);
        work_File_1_2_A45.setValue(pnd_Atsign);
        work_File_1_2_A46.setValue(pnd_Atsign);
        work_File_1_2_A47.setValue(pnd_Atsign);
        work_File_1_2_A48.setValue(pnd_Atsign);
        work_File_1_2_A49.setValue(pnd_Atsign);
        work_File_1_2_A50.setValue(pnd_Atsign);
        work_File_1_2_A51.setValue(pnd_Atsign);
        work_File_1_2_A52.setValue(pnd_Atsign);
        work_File_1_2_A53.setValue(pnd_Atsign);
        work_File_1_2_A54.setValue(pnd_Atsign);
        work_File_1_2_A55.setValue(pnd_Atsign);
        work_File_1_2_A56.setValue(pnd_Atsign);
        work_File_1_2_A57.setValue(pnd_Atsign);
        work_File_1_2_A58.setValue(pnd_Atsign);
        //*  FILL-IN-A-ATSIGN
    }
    private void sub_Fill_In_B_Atsign() throws Exception                                                                                                                  //Natural: FILL-IN-B-ATSIGN
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        work_File_3_4_B01.setValue(pnd_Atsign);                                                                                                                           //Natural: MOVE #ATSIGN TO B01 B02 B03 B04 B05 B06 B07 B08 B09 B10 B11 B12 B13 B14
        work_File_3_4_B02.setValue(pnd_Atsign);
        work_File_3_4_B03.setValue(pnd_Atsign);
        work_File_3_4_B04.setValue(pnd_Atsign);
        work_File_3_4_B05.setValue(pnd_Atsign);
        work_File_3_4_B06.setValue(pnd_Atsign);
        work_File_3_4_B07.setValue(pnd_Atsign);
        work_File_3_4_B08.setValue(pnd_Atsign);
        work_File_3_4_B09.setValue(pnd_Atsign);
        work_File_3_4_B10.setValue(pnd_Atsign);
        work_File_3_4_B11.setValue(pnd_Atsign);
        work_File_3_4_B12.setValue(pnd_Atsign);
        work_File_3_4_B13.setValue(pnd_Atsign);
        work_File_3_4_B14.setValue(pnd_Atsign);
        //*  FILL-IN-B-ATSIGN
    }
    private void sub_Fill_In_C_Atsign() throws Exception                                                                                                                  //Natural: FILL-IN-C-ATSIGN
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        work_File_5_C01.setValue(pnd_Atsign);                                                                                                                             //Natural: MOVE #ATSIGN TO C01 C02 C03
        work_File_5_C02.setValue(pnd_Atsign);
        work_File_5_C03.setValue(pnd_Atsign);
        //*  FILL-IN-C-ATSIGN
    }
    private void sub_Fill_In_D_Atsign() throws Exception                                                                                                                  //Natural: FILL-IN-D-ATSIGN
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        work_File_6_D01.setValue(pnd_Atsign);                                                                                                                             //Natural: MOVE #ATSIGN TO D01 D02 D03 D04 D05 D06 D07
        work_File_6_D02.setValue(pnd_Atsign);
        work_File_6_D03.setValue(pnd_Atsign);
        work_File_6_D04.setValue(pnd_Atsign);
        work_File_6_D05.setValue(pnd_Atsign);
        work_File_6_D06.setValue(pnd_Atsign);
        work_File_6_D07.setValue(pnd_Atsign);
        //*  FILL-IN-D-ATSIGN
    }
    private void sub_Fill_In_E_Atsign() throws Exception                                                                                                                  //Natural: FILL-IN-E-ATSIGN
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        work_File_7_E01.setValue(pnd_Atsign);                                                                                                                             //Natural: MOVE #ATSIGN TO E01 E02 E03 E04 E05 E06 E07 E08
        work_File_7_E02.setValue(pnd_Atsign);
        work_File_7_E03.setValue(pnd_Atsign);
        work_File_7_E04.setValue(pnd_Atsign);
        work_File_7_E05.setValue(pnd_Atsign);
        work_File_7_E06.setValue(pnd_Atsign);
        work_File_7_E07.setValue(pnd_Atsign);
        work_File_7_E08.setValue(pnd_Atsign);
        //*  FILL-IN-E-ATSIGN
    }
    private void sub_Fill_In_F_Atsign() throws Exception                                                                                                                  //Natural: FILL-IN-F-ATSIGN
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        work_File_8_F01.setValue(pnd_Atsign);                                                                                                                             //Natural: MOVE #ATSIGN TO F01 F02 F03 F04 F05 F06 F07 F08 F09
        work_File_8_F02.setValue(pnd_Atsign);
        work_File_8_F03.setValue(pnd_Atsign);
        work_File_8_F04.setValue(pnd_Atsign);
        work_File_8_F05.setValue(pnd_Atsign);
        work_File_8_F06.setValue(pnd_Atsign);
        work_File_8_F07.setValue(pnd_Atsign);
        work_File_8_F08.setValue(pnd_Atsign);
        work_File_8_F09.setValue(pnd_Atsign);
        //*  FILL-IN-F-ATSIGN
    }
    private void sub_Check_For_Restart() throws Exception                                                                                                                 //Natural: CHECK-FOR-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(ldaIcwl3010.getIcw_Master_Index_View_Tiaa_Rcvd_Dte_Tme().greaterOrEqual(ldaIcwl3010.getIcw_Master_Index_View_Trade_Dte_Tme())))                     //Natural: IF ICW-MASTER-INDEX-VIEW.TIAA-RCVD-DTE-TME GE ICW-MASTER-INDEX-VIEW.TRADE-DTE-TME
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET UP THE KEY USING RQST-LOG-DTE-TME AND THE INVERTED TRADE-DTE-TME
        //*  THIS SHOULD READ FROM MOST RECENT RECORD - GETTING US CLOSE TO THE PE
        //*  PEND/UNPEND ACTIVITY
        pnd_Restart_Invrt_Start_A.setValueEdited(ldaIcwl3010.getIcw_Master_Index_View_Trade_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                             //Natural: MOVE EDITED ICW-MASTER-INDEX-VIEW.TRADE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #RESTART-INVRT-START-A
        pnd_Rqst_History_Key_Pnd_Ky_Invrt_Strtng_Event.compute(new ComputeParameters(false, pnd_Rqst_History_Key_Pnd_Ky_Invrt_Strtng_Event), (pnd_Nines.subtract(pnd_Restart_Invrt_Start_A_Pnd_Restart_Invrt_Start_N))); //Natural: COMPUTE #KY-INVRT-STRTNG-EVENT = ( #NINES - #RESTART-INVRT-START-N )
        pnd_Rqst_History_Key_Pnd_Ky_Rqst_Log_Dte_Tme.setValue(ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Dte_Tme());                                                   //Natural: MOVE ICW-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO #KY-RQST-LOG-DTE-TME
        vw_icw_Imit.startDatabaseRead                                                                                                                                     //Natural: READ ICW-IMIT BY RQST-HISTORY-KEY STARTING FROM #RQST-HISTORY-KEY
        (
        "READ_ICW_IMIT",
        new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", pnd_Rqst_History_Key, WcType.BY) },
        new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
        );
        READ_ICW_IMIT:
        while (condition(vw_icw_Imit.readNextRow("READ_ICW_IMIT")))
        {
            short decideConditionsMet1988 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ICW-IMIT.RQST-LOG-DTE-TME NE #RQST-HISTORY-KEY.#KY-RQST-LOG-DTE-TME
            if (condition(icw_Imit_Rqst_Log_Dte_Tme.notEquals(pnd_Rqst_History_Key_Pnd_Ky_Rqst_Log_Dte_Tme)))
            {
                decideConditionsMet1988++;
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN ICW-IMIT.STATUS-CDE GE '4500' AND ICW-IMIT.STATUS-CDE LE '4997'
            else if (condition(icw_Imit_Status_Cde.greaterOrEqual("4500") && icw_Imit_Status_Cde.lessOrEqual("4997")))
            {
                decideConditionsMet1988++;
                work_File_1_2_Trade_Dte_Tme.setValueEdited(icw_Imit_Endng_Event_Dte_Tme,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                          //Natural: MOVE EDITED ICW-IMIT.ENDNG-EVENT-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-1-2.TRADE-DTE-TME
                if (true) break READ_ICW_IMIT;                                                                                                                            //Natural: ESCAPE BOTTOM ( READ-ICW-IMIT. )
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *************
        //*  CHECK-FOR-RESTART
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"=== FATAL ERROR  ===",NEWLINE,"Rqst-Log-Dte-Tme",ldaIcwl3010.getIcw_Master_Index_View_Rqst_Log_Dte_Tme(),     //Natural: WRITE / '=== FATAL ERROR  ===' / 'Rqst-Log-Dte-Tme' ICW-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME / 'Crprte-Stat-Ind ' ICW-MASTER-INDEX-VIEW.CRPRTE-STATUS-IND / 'Schedule-Pay-Dte' ICW-MASTER-INDEX-VIEW.SCHDLE-PAY-DTE / '*ISN            ' #ISN-SAVE
            NEWLINE,"Crprte-Stat-Ind ",ldaIcwl3010.getIcw_Master_Index_View_Crprte_Status_Ind(),NEWLINE,"Schedule-Pay-Dte",ldaIcwl3010.getIcw_Master_Index_View_Schdle_Pay_Dte(),
            NEWLINE,"*ISN            ",pnd_Isn_Save);
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
    }
}
