/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:32:32 PM
**        * FROM NATURAL PROGRAM : Cwfb4029
************************************************************
**        * FILE NAME            : Cwfb4029.java
**        * CLASS NAME           : Cwfb4029
**        * INSTANCE NAME        : Cwfb4029
************************************************************
************************************************************************
* PROGRAM  : CWFB4029
* SYSTEM   : CRPCWF
* TITLE    : WORK PROCESS ID EXTRACT
* GENERATED: SEP 15,95 AT 12:00 PM
* FUNCTION : THIS PROGRAM READS THE CWF-WP-WORK-PRCSS-ID TABLE AND
*            WRITES A WORK FILE TO BE DOWNLOADED TO THE SQL SERVER
*
* 11/10/97 JAMIE HARGRAVE - ADD ENTRY/UPDATE/DELETE OPERATOR/DATE
*                           FIELDS FOR STAN MARICLE
* 05/13/98 JAMIE HARGRAVE - ADD WPID-USAGE-DESC TO DOWNLOAD FILE
* 12/30/98 JAMIE HARGRAVE - ADD NEW FIELDS (UP TO TEAM-IND)
* 07/07/00 LEON EPSTEIN   - A NEW FILE(WPID-INDICATORS-TABLE) EXTRACTED
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4029 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private LdaCwftotal ldaCwftotal;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Wp_Work_Prcss_Id;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Id;

    private DbsGroup cwf_Wp_Work_Prcss_Id__R_Field_1;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme;
    private DbsField cwf_Wp_Work_Prcss_Id_Cntrct_Slctn_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Mj_Pull_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Prty_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Check_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Crc_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp;

    private DbsGroup cwf_Wp_Work_Prcss_Id__R_Field_2;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Days_Nbr;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Hours_Nbr;
    private DbsField cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Mins_Nbr;
    private DbsField cwf_Wp_Work_Prcss_Id_Actve_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme;
    private DbsField cwf_Wp_Work_Prcss_Id_Entry_Oprtr_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Updte_Dte;

    private DbsGroup cwf_Wp_Work_Prcss_Id__R_Field_3;
    private DbsField cwf_Wp_Work_Prcss_Id_Updte_Dte_A;
    private DbsField cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme;
    private DbsField cwf_Wp_Work_Prcss_Id_Updte_Oprtr_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme;
    private DbsField cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Sec_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Effctve_Dte_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Trnsctn_Dte_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Check_Mail_Dte_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Kdo_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Back_End_Scan_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Dgtze_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Dgtze_Region_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Dgtze_Branch_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Dgtze_Special_Needs_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Wpid_Usage_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Shrd_Srvce_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Da_Sra_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Insrnce_Prcss_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Function_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Wpid_Suppress_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Wpid_Intrnet_Nme;
    private DbsField cwf_Wp_Work_Prcss_Id_Team_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Wpid_Inds;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_4;
    private DbsField cwf_Support_Tbl_Tbl_Ind_Desc;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Tbl_Table_Access_Level;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_5;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;

    private DbsGroup total_Counters;
    private DbsField total_Counters_Work_Read_Cntr;
    private DbsField total_Counters_Wrqst_Add_Cntr;
    private DbsField total_Counters_Wrqst_Upd_Cntr;
    private DbsField total_Counters_Unita_Add_Cntr;
    private DbsField total_Counters_Unita_Upd_Cntr;
    private DbsField total_Counters_Empla_Add_Cntr;
    private DbsField total_Counters_Empla_Upd_Cntr;
    private DbsField total_Counters_Stepa_Add_Cntr;
    private DbsField total_Counters_Stepa_Upd_Cntr;
    private DbsField total_Counters_Extra_Add_Cntr;
    private DbsField total_Counters_Extra_Upd_Cntr;
    private DbsField total_Counters_Intra_Add_Cntr;
    private DbsField total_Counters_Intra_Upd_Cntr;
    private DbsField total_Counters_Enrta_Add_Cntr;
    private DbsField total_Counters_Enrta_Upd_Cntr;
    private DbsField total_Counters_Contr_Add_Cntr;
    private DbsField total_Counters_Contr_Upd_Cntr;
    private DbsField total_Counters_Cstat_Add_Cntr;
    private DbsField total_Counters_Merg_Delt_Cntr;
    private DbsField total_Counters_Adtnl_Upd_Cntr;
    private DbsField total_Counters_Adtnl_Add_Cntr;
    private DbsField total_Counters_Relat_Add_Cntr;
    private DbsField total_Counters_Efmcab_Add_Cntr;
    private DbsField total_Counters_Wpidind_Add_Cntr;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;

    private DbsGroup work_Record;
    private DbsField work_Record_Work_Prcss_Id;
    private DbsField work_Record_Pnd_C1;
    private DbsField work_Record_Work_Prcss_Short_Nme;
    private DbsField work_Record_Pnd_C2;
    private DbsField work_Record_Work_Prcss_Long_Nme;
    private DbsField work_Record_Pnd_C3;
    private DbsField work_Record_Crprte_Srvce_Stndrd_Days_Nbr;
    private DbsField work_Record_Pnd_C4;
    private DbsField work_Record_Crprte_Srvce_Stndrd_Hours_Nbr;
    private DbsField work_Record_Pnd_C5;
    private DbsField work_Record_Crprte_Srvce_Stndrd_Mins_Nbr;
    private DbsField work_Record_Pnd_C6;
    private DbsField work_Record_Owner_Unit_Cde;
    private DbsField work_Record_Pnd_C7;
    private DbsField work_Record_Kdo_Cde;
    private DbsField work_Record_Pnd_C8;
    private DbsField work_Record_Effctve_Dte_Ind;
    private DbsField work_Record_Pnd_C9;
    private DbsField work_Record_Check_Mail_Dte_Ind;
    private DbsField work_Record_Pnd_C10;
    private DbsField work_Record_Trnsctn_Dte_Ind;
    private DbsField work_Record_Pnd_C11;
    private DbsField work_Record_Cntrct_Slctn_Ind;
    private DbsField work_Record_Pnd_C12;
    private DbsField work_Record_Mj_Pull_Ind;
    private DbsField work_Record_Pnd_C13;
    private DbsField work_Record_Check_Ind;
    private DbsField work_Record_Pnd_C14;
    private DbsField work_Record_Sec_Ind;
    private DbsField work_Record_Pnd_C15;
    private DbsField work_Record_Dgtze_Ind;
    private DbsField work_Record_Pnd_C16;
    private DbsField work_Record_Dgtze_Region_Ind;
    private DbsField work_Record_Pnd_C17;
    private DbsField work_Record_Dgtze_Branch_Ind;
    private DbsField work_Record_Pnd_C18;
    private DbsField work_Record_Dgtze_Special_Needs_Ind;
    private DbsField work_Record_Pnd_C19;
    private DbsField work_Record_Back_End_Scan_Ind;
    private DbsField work_Record_Pnd_C20;
    private DbsField work_Record_Crprte_Prty_Ind;
    private DbsField work_Record_Pnd_C21;
    private DbsField work_Record_Crc_Ind;
    private DbsField work_Record_Pnd_C22;
    private DbsField work_Record_Entry_Dte_A;
    private DbsField work_Record_Pnd_C23;
    private DbsField work_Record_Entry_Oprtr_Cde;
    private DbsField work_Record_Pnd_C24;
    private DbsField work_Record_Updte_Dte_A12;
    private DbsField work_Record_Pnd_C25;
    private DbsField work_Record_Updte_Oprtr_Cde;
    private DbsField work_Record_Pnd_C26;
    private DbsField work_Record_Dlte_Dte_A;
    private DbsField work_Record_Pnd_C27;
    private DbsField work_Record_Dlte_Oprtr_Cde;
    private DbsField work_Record_Pnd_C28;
    private DbsField work_Record_Wpid_Usage_Desc;
    private DbsField work_Record_Pnd_C29;
    private DbsField work_Record_Shrd_Srvce_Ind;
    private DbsField work_Record_Pnd_C30;
    private DbsField work_Record_Da_Sra_Ind;
    private DbsField work_Record_Pnd_C31;
    private DbsField work_Record_Insrnce_Prcss_Ind;
    private DbsField work_Record_Pnd_C32;
    private DbsField work_Record_Function_Cde;
    private DbsField work_Record_Pnd_C33;
    private DbsField work_Record_Wpid_Suppress_Ind;
    private DbsField work_Record_Pnd_C34;
    private DbsField work_Record_Wpid_Intrnet_Nme;
    private DbsField work_Record_Pnd_C35;
    private DbsField work_Record_Team_Ind;

    private DbsGroup wpid_Ind_Record;
    private DbsField wpid_Ind_Record_Wpid;
    private DbsField wpid_Ind_Record_Pnd_D1;
    private DbsField wpid_Ind_Record_Wpid_Ind_Cde;
    private DbsField wpid_Ind_Record_Pnd_D2;
    private DbsField wpid_Ind_Record_Wpid_Ind_Cde_Desc;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Rex_Read;
    private DbsField pnd_Counters_Pnd_Rex_Written;
    private DbsField pnd_Counters_Pnd_Wpid_Ind_Cntr;

    private DbsGroup pnd_Report_Fields;
    private DbsField pnd_Report_Fields_Pnd_Error_Msg;

    private DbsGroup pnd_Contstants;
    private DbsField pnd_Contstants_Pnd_Delimiter;
    private DbsField pnd_Contact_Name;

    private DbsGroup pnd_Contact_Name__R_Field_6;
    private DbsField pnd_Contact_Name_Pnd_Contact_Name_8;
    private DbsField pnd_Wpid_Inds;

    private DbsGroup pnd_Wpid_Inds__R_Field_7;
    private DbsField pnd_Wpid_Inds_Pnd_Wpid_Inds_Occur;
    private DbsField pnd_I;
    private DbsField pnd_Date_D;
    private DbsField pnd_Time_T;
    private DbsField pnd_Date_A;
    private DbsField pnd_Time_A;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        ldaCwftotal = new LdaCwftotal();
        registerRecord(ldaCwftotal);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Wp_Work_Prcss_Id = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Work_Prcss_Id", "CWF-WP-WORK-PRCSS-ID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Id = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");

        cwf_Wp_Work_Prcss_Id__R_Field_1 = vw_cwf_Wp_Work_Prcss_Id.getRecord().newGroupInGroup("cwf_Wp_Work_Prcss_Id__R_Field_1", "REDEFINE", cwf_Wp_Work_Prcss_Id_Work_Prcss_Id);
        cwf_Wp_Work_Prcss_Id_Work_Actn_Rqstd_Cde = cwf_Wp_Work_Prcss_Id__R_Field_1.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Wp_Work_Prcss_Id_Work_Lob_Cmpny_Prdct_Cde = cwf_Wp_Work_Prcss_Id__R_Field_1.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Lob_Cmpny_Prdct_Cde", 
            "WORK-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        cwf_Wp_Work_Prcss_Id_Work_Mjr_Bsnss_Prcss_Cde = cwf_Wp_Work_Prcss_Id__R_Field_1.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Mjr_Bsnss_Prcss_Cde", 
            "WORK-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        cwf_Wp_Work_Prcss_Id_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Wp_Work_Prcss_Id__R_Field_1.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", 
            FieldType.STRING, 45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        cwf_Wp_Work_Prcss_Id_Cntrct_Slctn_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Cntrct_Slctn_Ind", "CNTRCT-SLCTN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SLCTN_IND");
        cwf_Wp_Work_Prcss_Id_Cntrct_Slctn_Ind.setDdmHeader("CONTRACT/SELECTION");
        cwf_Wp_Work_Prcss_Id_Mj_Pull_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Wp_Work_Prcss_Id_Mj_Pull_Ind.setDdmHeader("MJ/IND");
        cwf_Wp_Work_Prcss_Id_Crprte_Prty_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Prty_Ind", "CRPRTE-PRTY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_PRTY_IND");
        cwf_Wp_Work_Prcss_Id_Crprte_Prty_Ind.setDdmHeader("CORP/PRTY");
        cwf_Wp_Work_Prcss_Id_Check_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Check_Ind", "CHECK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CHECK_IND");
        cwf_Wp_Work_Prcss_Id_Check_Ind.setDdmHeader("CHECK/IND");
        cwf_Wp_Work_Prcss_Id_Crc_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crc_Ind", "CRC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRC_IND");
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp", 
            "CRPRTE-SRVCE-TIME-STNDRD-GRP", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "CRPRTE_SRVCE_TIME_STNDRD_GRP");
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp.setDdmHeader("CORPORATE SERVICE/TIME STANDARD");

        cwf_Wp_Work_Prcss_Id__R_Field_2 = vw_cwf_Wp_Work_Prcss_Id.getRecord().newGroupInGroup("cwf_Wp_Work_Prcss_Id__R_Field_2", "REDEFINE", cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Time_Stndrd_Grp);
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Days_Nbr = cwf_Wp_Work_Prcss_Id__R_Field_2.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Days_Nbr", 
            "CRPRTE-SRVCE-STNDRD-DAYS-NBR", FieldType.NUMERIC, 3);
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Hours_Nbr = cwf_Wp_Work_Prcss_Id__R_Field_2.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Hours_Nbr", 
            "CRPRTE-SRVCE-STNDRD-HOURS-NBR", FieldType.NUMERIC, 2);
        cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Mins_Nbr = cwf_Wp_Work_Prcss_Id__R_Field_2.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Crprte_Srvce_Stndrd_Mins_Nbr", 
            "CRPRTE-SRVCE-STNDRD-MINS-NBR", FieldType.NUMERIC, 2);
        cwf_Wp_Work_Prcss_Id_Actve_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");
        cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme", "ENTRY-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        cwf_Wp_Work_Prcss_Id_Entry_Oprtr_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");
        cwf_Wp_Work_Prcss_Id_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Wp_Work_Prcss_Id_Updte_Dte = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Updte_Dte", "UPDTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "UPDTE_DTE");
        cwf_Wp_Work_Prcss_Id_Updte_Dte.setDdmHeader("UPDATE/DATE");

        cwf_Wp_Work_Prcss_Id__R_Field_3 = vw_cwf_Wp_Work_Prcss_Id.getRecord().newGroupInGroup("cwf_Wp_Work_Prcss_Id__R_Field_3", "REDEFINE", cwf_Wp_Work_Prcss_Id_Updte_Dte);
        cwf_Wp_Work_Prcss_Id_Updte_Dte_A = cwf_Wp_Work_Prcss_Id__R_Field_3.newFieldInGroup("cwf_Wp_Work_Prcss_Id_Updte_Dte_A", "UPDTE-DTE-A", FieldType.STRING, 
            8);
        cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Updte_Dte_Tme", "UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UPDTE_DTE_TME");
        cwf_Wp_Work_Prcss_Id_Updte_Oprtr_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        cwf_Wp_Work_Prcss_Id_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPER");
        cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme.setDdmHeader("DELETE/DT-TM");
        cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde", "OWNER-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "OWNER_UNIT_CDE");
        cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde.setDdmHeader("OWNER/UNIT");
        cwf_Wp_Work_Prcss_Id_Sec_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Sec_Ind", "SEC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SEC_IND");
        cwf_Wp_Work_Prcss_Id_Sec_Ind.setDdmHeader("SEC/IND");
        cwf_Wp_Work_Prcss_Id_Effctve_Dte_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Effctve_Dte_Ind", "EFFCTVE-DTE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "EFFCTVE_DTE_IND");
        cwf_Wp_Work_Prcss_Id_Effctve_Dte_Ind.setDdmHeader("EFFCTVE/DTE-IND");
        cwf_Wp_Work_Prcss_Id_Trnsctn_Dte_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Trnsctn_Dte_Ind", "TRNSCTN-DTE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TRNSCTN_DTE_IND");
        cwf_Wp_Work_Prcss_Id_Trnsctn_Dte_Ind.setDdmHeader("TRNSCTN/DTE-IND");
        cwf_Wp_Work_Prcss_Id_Check_Mail_Dte_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Check_Mail_Dte_Ind", "CHECK-MAIL-DTE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CHECK_MAIL_DTE_IND");
        cwf_Wp_Work_Prcss_Id_Check_Mail_Dte_Ind.setDdmHeader("CHK-MAIL/DTE-IND");
        cwf_Wp_Work_Prcss_Id_Kdo_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Kdo_Cde", "KDO-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "KDO_CDE");
        cwf_Wp_Work_Prcss_Id_Kdo_Cde.setDdmHeader("KDO CODE");
        cwf_Wp_Work_Prcss_Id_Back_End_Scan_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Back_End_Scan_Ind", "BACK-END-SCAN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BACK_END_SCAN_IND");
        cwf_Wp_Work_Prcss_Id_Back_End_Scan_Ind.setDdmHeader("BACK-END/SCAN-IND");
        cwf_Wp_Work_Prcss_Id_Dgtze_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dgtze_Ind", "DGTZE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DGTZE_IND");
        cwf_Wp_Work_Prcss_Id_Dgtze_Ind.setDdmHeader("DIGITIZE/INDICATOR");
        cwf_Wp_Work_Prcss_Id_Dgtze_Region_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dgtze_Region_Ind", "DGTZE-REGION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DGTZE_REGION_IND");
        cwf_Wp_Work_Prcss_Id_Dgtze_Region_Ind.setDdmHeader("DIGITIZE REGION/INDICATOR");
        cwf_Wp_Work_Prcss_Id_Dgtze_Branch_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dgtze_Branch_Ind", "DGTZE-BRANCH-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DGTZE_BRANCH_IND");
        cwf_Wp_Work_Prcss_Id_Dgtze_Branch_Ind.setDdmHeader("DIGITIZE BRANCH/INDICATOR");
        cwf_Wp_Work_Prcss_Id_Dgtze_Special_Needs_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dgtze_Special_Needs_Ind", 
            "DGTZE-SPECIAL-NEEDS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "DGTZE_SPECIAL_NEEDS_IND");
        cwf_Wp_Work_Prcss_Id_Dgtze_Special_Needs_Ind.setDdmHeader("DIGITIZE SPECIAL/NEEDS INDICATOR");
        cwf_Wp_Work_Prcss_Id_Wpid_Usage_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Wpid_Usage_Ind", "WPID-USAGE-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "WPID_USAGE_IND");
        cwf_Wp_Work_Prcss_Id_Wpid_Usage_Ind.setDdmHeader("WPID/USAGE");
        cwf_Wp_Work_Prcss_Id_Shrd_Srvce_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Shrd_Srvce_Ind", "SHRD-SRVCE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SHRD_SRVCE_IND");
        cwf_Wp_Work_Prcss_Id_Shrd_Srvce_Ind.setDdmHeader("SHARED/SERVICE");
        cwf_Wp_Work_Prcss_Id_Da_Sra_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Da_Sra_Ind", "DA-SRA-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DA_SRA_IND");
        cwf_Wp_Work_Prcss_Id_Da_Sra_Ind.setDdmHeader("DA/SRA/IND");
        cwf_Wp_Work_Prcss_Id_Insrnce_Prcss_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Insrnce_Prcss_Ind", "INSRNCE-PRCSS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "INSRNCE_PRCSS_IND");
        cwf_Wp_Work_Prcss_Id_Insrnce_Prcss_Ind.setDdmHeader("INSURANCE/PROCESS");
        cwf_Wp_Work_Prcss_Id_Function_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Function_Cde", "FUNCTION-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "FUNCTION_CDE");
        cwf_Wp_Work_Prcss_Id_Function_Cde.setDdmHeader("FUNCTION/CODE");
        cwf_Wp_Work_Prcss_Id_Wpid_Suppress_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Wpid_Suppress_Ind", "WPID-SUPPRESS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WPID_SUPPRESS_IND");
        cwf_Wp_Work_Prcss_Id_Wpid_Suppress_Ind.setDdmHeader("SUPPRESS/IND");
        cwf_Wp_Work_Prcss_Id_Wpid_Intrnet_Nme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Wpid_Intrnet_Nme", "WPID-INTRNET-NME", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "WPID_INTRNET_NME");
        cwf_Wp_Work_Prcss_Id_Wpid_Intrnet_Nme.setDdmHeader("INTER/INTRANET WPID NAME");
        cwf_Wp_Work_Prcss_Id_Team_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Team_Ind", "TEAM-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TEAM_IND");
        cwf_Wp_Work_Prcss_Id_Wpid_Inds = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Wpid_Inds", "WPID-INDS", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "WPID_INDS");
        registerRecord(vw_cwf_Wp_Work_Prcss_Id);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_4 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_4", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Ind_Desc = cwf_Support_Tbl__R_Field_4.newFieldInGroup("cwf_Support_Tbl_Tbl_Ind_Desc", "TBL-IND-DESC", FieldType.STRING, 60);
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_5", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_5.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);

        total_Counters = localVariables.newGroupInRecord("total_Counters", "TOTAL-COUNTERS");
        total_Counters_Work_Read_Cntr = total_Counters.newFieldInGroup("total_Counters_Work_Read_Cntr", "WORK-READ-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Wrqst_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Wrqst_Add_Cntr", "WRQST-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Wrqst_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Wrqst_Upd_Cntr", "WRQST-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Unita_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Unita_Add_Cntr", "UNITA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Unita_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Unita_Upd_Cntr", "UNITA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Empla_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Empla_Add_Cntr", "EMPLA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Empla_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Empla_Upd_Cntr", "EMPLA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Stepa_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Stepa_Add_Cntr", "STEPA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Stepa_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Stepa_Upd_Cntr", "STEPA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Extra_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Extra_Add_Cntr", "EXTRA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Extra_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Extra_Upd_Cntr", "EXTRA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Intra_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Intra_Add_Cntr", "INTRA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Intra_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Intra_Upd_Cntr", "INTRA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Enrta_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Enrta_Add_Cntr", "ENRTA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Enrta_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Enrta_Upd_Cntr", "ENRTA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Contr_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Contr_Add_Cntr", "CONTR-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Contr_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Contr_Upd_Cntr", "CONTR-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Cstat_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Cstat_Add_Cntr", "CSTAT-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Merg_Delt_Cntr = total_Counters.newFieldInGroup("total_Counters_Merg_Delt_Cntr", "MERG-DELT-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Adtnl_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Adtnl_Upd_Cntr", "ADTNL-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Adtnl_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Adtnl_Add_Cntr", "ADTNL-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Relat_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Relat_Add_Cntr", "RELAT-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Efmcab_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Efmcab_Add_Cntr", "EFMCAB-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Wpidind_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Wpidind_Add_Cntr", "WPIDIND-ADD-CNTR", FieldType.NUMERIC, 9);
        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);

        work_Record = localVariables.newGroupInRecord("work_Record", "WORK-RECORD");
        work_Record_Work_Prcss_Id = work_Record.newFieldInGroup("work_Record_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        work_Record_Pnd_C1 = work_Record.newFieldInGroup("work_Record_Pnd_C1", "#C1", FieldType.STRING, 1);
        work_Record_Work_Prcss_Short_Nme = work_Record.newFieldInGroup("work_Record_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 15);
        work_Record_Pnd_C2 = work_Record.newFieldInGroup("work_Record_Pnd_C2", "#C2", FieldType.STRING, 1);
        work_Record_Work_Prcss_Long_Nme = work_Record.newFieldInGroup("work_Record_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 45);
        work_Record_Pnd_C3 = work_Record.newFieldInGroup("work_Record_Pnd_C3", "#C3", FieldType.STRING, 1);
        work_Record_Crprte_Srvce_Stndrd_Days_Nbr = work_Record.newFieldInGroup("work_Record_Crprte_Srvce_Stndrd_Days_Nbr", "CRPRTE-SRVCE-STNDRD-DAYS-NBR", 
            FieldType.NUMERIC, 3);
        work_Record_Pnd_C4 = work_Record.newFieldInGroup("work_Record_Pnd_C4", "#C4", FieldType.STRING, 1);
        work_Record_Crprte_Srvce_Stndrd_Hours_Nbr = work_Record.newFieldInGroup("work_Record_Crprte_Srvce_Stndrd_Hours_Nbr", "CRPRTE-SRVCE-STNDRD-HOURS-NBR", 
            FieldType.NUMERIC, 2);
        work_Record_Pnd_C5 = work_Record.newFieldInGroup("work_Record_Pnd_C5", "#C5", FieldType.STRING, 1);
        work_Record_Crprte_Srvce_Stndrd_Mins_Nbr = work_Record.newFieldInGroup("work_Record_Crprte_Srvce_Stndrd_Mins_Nbr", "CRPRTE-SRVCE-STNDRD-MINS-NBR", 
            FieldType.NUMERIC, 2);
        work_Record_Pnd_C6 = work_Record.newFieldInGroup("work_Record_Pnd_C6", "#C6", FieldType.STRING, 1);
        work_Record_Owner_Unit_Cde = work_Record.newFieldInGroup("work_Record_Owner_Unit_Cde", "OWNER-UNIT-CDE", FieldType.STRING, 8);
        work_Record_Pnd_C7 = work_Record.newFieldInGroup("work_Record_Pnd_C7", "#C7", FieldType.STRING, 1);
        work_Record_Kdo_Cde = work_Record.newFieldInGroup("work_Record_Kdo_Cde", "KDO-CDE", FieldType.STRING, 8);
        work_Record_Pnd_C8 = work_Record.newFieldInGroup("work_Record_Pnd_C8", "#C8", FieldType.STRING, 1);
        work_Record_Effctve_Dte_Ind = work_Record.newFieldInGroup("work_Record_Effctve_Dte_Ind", "EFFCTVE-DTE-IND", FieldType.STRING, 1);
        work_Record_Pnd_C9 = work_Record.newFieldInGroup("work_Record_Pnd_C9", "#C9", FieldType.STRING, 1);
        work_Record_Check_Mail_Dte_Ind = work_Record.newFieldInGroup("work_Record_Check_Mail_Dte_Ind", "CHECK-MAIL-DTE-IND", FieldType.STRING, 1);
        work_Record_Pnd_C10 = work_Record.newFieldInGroup("work_Record_Pnd_C10", "#C10", FieldType.STRING, 1);
        work_Record_Trnsctn_Dte_Ind = work_Record.newFieldInGroup("work_Record_Trnsctn_Dte_Ind", "TRNSCTN-DTE-IND", FieldType.STRING, 1);
        work_Record_Pnd_C11 = work_Record.newFieldInGroup("work_Record_Pnd_C11", "#C11", FieldType.STRING, 1);
        work_Record_Cntrct_Slctn_Ind = work_Record.newFieldInGroup("work_Record_Cntrct_Slctn_Ind", "CNTRCT-SLCTN-IND", FieldType.STRING, 1);
        work_Record_Pnd_C12 = work_Record.newFieldInGroup("work_Record_Pnd_C12", "#C12", FieldType.STRING, 1);
        work_Record_Mj_Pull_Ind = work_Record.newFieldInGroup("work_Record_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1);
        work_Record_Pnd_C13 = work_Record.newFieldInGroup("work_Record_Pnd_C13", "#C13", FieldType.STRING, 1);
        work_Record_Check_Ind = work_Record.newFieldInGroup("work_Record_Check_Ind", "CHECK-IND", FieldType.STRING, 1);
        work_Record_Pnd_C14 = work_Record.newFieldInGroup("work_Record_Pnd_C14", "#C14", FieldType.STRING, 1);
        work_Record_Sec_Ind = work_Record.newFieldInGroup("work_Record_Sec_Ind", "SEC-IND", FieldType.STRING, 1);
        work_Record_Pnd_C15 = work_Record.newFieldInGroup("work_Record_Pnd_C15", "#C15", FieldType.STRING, 1);
        work_Record_Dgtze_Ind = work_Record.newFieldInGroup("work_Record_Dgtze_Ind", "DGTZE-IND", FieldType.STRING, 1);
        work_Record_Pnd_C16 = work_Record.newFieldInGroup("work_Record_Pnd_C16", "#C16", FieldType.STRING, 1);
        work_Record_Dgtze_Region_Ind = work_Record.newFieldInGroup("work_Record_Dgtze_Region_Ind", "DGTZE-REGION-IND", FieldType.STRING, 1);
        work_Record_Pnd_C17 = work_Record.newFieldInGroup("work_Record_Pnd_C17", "#C17", FieldType.STRING, 1);
        work_Record_Dgtze_Branch_Ind = work_Record.newFieldInGroup("work_Record_Dgtze_Branch_Ind", "DGTZE-BRANCH-IND", FieldType.STRING, 1);
        work_Record_Pnd_C18 = work_Record.newFieldInGroup("work_Record_Pnd_C18", "#C18", FieldType.STRING, 1);
        work_Record_Dgtze_Special_Needs_Ind = work_Record.newFieldInGroup("work_Record_Dgtze_Special_Needs_Ind", "DGTZE-SPECIAL-NEEDS-IND", FieldType.STRING, 
            1);
        work_Record_Pnd_C19 = work_Record.newFieldInGroup("work_Record_Pnd_C19", "#C19", FieldType.STRING, 1);
        work_Record_Back_End_Scan_Ind = work_Record.newFieldInGroup("work_Record_Back_End_Scan_Ind", "BACK-END-SCAN-IND", FieldType.STRING, 1);
        work_Record_Pnd_C20 = work_Record.newFieldInGroup("work_Record_Pnd_C20", "#C20", FieldType.STRING, 1);
        work_Record_Crprte_Prty_Ind = work_Record.newFieldInGroup("work_Record_Crprte_Prty_Ind", "CRPRTE-PRTY-IND", FieldType.STRING, 1);
        work_Record_Pnd_C21 = work_Record.newFieldInGroup("work_Record_Pnd_C21", "#C21", FieldType.STRING, 1);
        work_Record_Crc_Ind = work_Record.newFieldInGroup("work_Record_Crc_Ind", "CRC-IND", FieldType.STRING, 1);
        work_Record_Pnd_C22 = work_Record.newFieldInGroup("work_Record_Pnd_C22", "#C22", FieldType.STRING, 1);
        work_Record_Entry_Dte_A = work_Record.newFieldInGroup("work_Record_Entry_Dte_A", "ENTRY-DTE-A", FieldType.STRING, 12);
        work_Record_Pnd_C23 = work_Record.newFieldInGroup("work_Record_Pnd_C23", "#C23", FieldType.STRING, 1);
        work_Record_Entry_Oprtr_Cde = work_Record.newFieldInGroup("work_Record_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 8);
        work_Record_Pnd_C24 = work_Record.newFieldInGroup("work_Record_Pnd_C24", "#C24", FieldType.STRING, 1);
        work_Record_Updte_Dte_A12 = work_Record.newFieldInGroup("work_Record_Updte_Dte_A12", "UPDTE-DTE-A12", FieldType.STRING, 12);
        work_Record_Pnd_C25 = work_Record.newFieldInGroup("work_Record_Pnd_C25", "#C25", FieldType.STRING, 1);
        work_Record_Updte_Oprtr_Cde = work_Record.newFieldInGroup("work_Record_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 8);
        work_Record_Pnd_C26 = work_Record.newFieldInGroup("work_Record_Pnd_C26", "#C26", FieldType.STRING, 1);
        work_Record_Dlte_Dte_A = work_Record.newFieldInGroup("work_Record_Dlte_Dte_A", "DLTE-DTE-A", FieldType.STRING, 12);
        work_Record_Pnd_C27 = work_Record.newFieldInGroup("work_Record_Pnd_C27", "#C27", FieldType.STRING, 1);
        work_Record_Dlte_Oprtr_Cde = work_Record.newFieldInGroup("work_Record_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8);
        work_Record_Pnd_C28 = work_Record.newFieldInGroup("work_Record_Pnd_C28", "#C28", FieldType.STRING, 1);
        work_Record_Wpid_Usage_Desc = work_Record.newFieldInGroup("work_Record_Wpid_Usage_Desc", "WPID-USAGE-DESC", FieldType.STRING, 4);
        work_Record_Pnd_C29 = work_Record.newFieldInGroup("work_Record_Pnd_C29", "#C29", FieldType.STRING, 1);
        work_Record_Shrd_Srvce_Ind = work_Record.newFieldInGroup("work_Record_Shrd_Srvce_Ind", "SHRD-SRVCE-IND", FieldType.STRING, 1);
        work_Record_Pnd_C30 = work_Record.newFieldInGroup("work_Record_Pnd_C30", "#C30", FieldType.STRING, 1);
        work_Record_Da_Sra_Ind = work_Record.newFieldInGroup("work_Record_Da_Sra_Ind", "DA-SRA-IND", FieldType.STRING, 1);
        work_Record_Pnd_C31 = work_Record.newFieldInGroup("work_Record_Pnd_C31", "#C31", FieldType.STRING, 1);
        work_Record_Insrnce_Prcss_Ind = work_Record.newFieldInGroup("work_Record_Insrnce_Prcss_Ind", "INSRNCE-PRCSS-IND", FieldType.STRING, 1);
        work_Record_Pnd_C32 = work_Record.newFieldInGroup("work_Record_Pnd_C32", "#C32", FieldType.STRING, 1);
        work_Record_Function_Cde = work_Record.newFieldInGroup("work_Record_Function_Cde", "FUNCTION-CDE", FieldType.STRING, 2);
        work_Record_Pnd_C33 = work_Record.newFieldInGroup("work_Record_Pnd_C33", "#C33", FieldType.STRING, 1);
        work_Record_Wpid_Suppress_Ind = work_Record.newFieldInGroup("work_Record_Wpid_Suppress_Ind", "WPID-SUPPRESS-IND", FieldType.STRING, 1);
        work_Record_Pnd_C34 = work_Record.newFieldInGroup("work_Record_Pnd_C34", "#C34", FieldType.STRING, 1);
        work_Record_Wpid_Intrnet_Nme = work_Record.newFieldInGroup("work_Record_Wpid_Intrnet_Nme", "WPID-INTRNET-NME", FieldType.STRING, 60);
        work_Record_Pnd_C35 = work_Record.newFieldInGroup("work_Record_Pnd_C35", "#C35", FieldType.STRING, 1);
        work_Record_Team_Ind = work_Record.newFieldInGroup("work_Record_Team_Ind", "TEAM-IND", FieldType.STRING, 1);

        wpid_Ind_Record = localVariables.newGroupInRecord("wpid_Ind_Record", "WPID-IND-RECORD");
        wpid_Ind_Record_Wpid = wpid_Ind_Record.newFieldInGroup("wpid_Ind_Record_Wpid", "WPID", FieldType.STRING, 6);
        wpid_Ind_Record_Pnd_D1 = wpid_Ind_Record.newFieldInGroup("wpid_Ind_Record_Pnd_D1", "#D1", FieldType.STRING, 1);
        wpid_Ind_Record_Wpid_Ind_Cde = wpid_Ind_Record.newFieldInGroup("wpid_Ind_Record_Wpid_Ind_Cde", "WPID-IND-CDE", FieldType.STRING, 4);
        wpid_Ind_Record_Pnd_D2 = wpid_Ind_Record.newFieldInGroup("wpid_Ind_Record_Pnd_D2", "#D2", FieldType.STRING, 1);
        wpid_Ind_Record_Wpid_Ind_Cde_Desc = wpid_Ind_Record.newFieldInGroup("wpid_Ind_Record_Wpid_Ind_Cde_Desc", "WPID-IND-CDE-DESC", FieldType.STRING, 
            60);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Rex_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Read", "#REX-READ", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_Rex_Written = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Written", "#REX-WRITTEN", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_Wpid_Ind_Cntr = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Wpid_Ind_Cntr", "#WPID-IND-CNTR", FieldType.PACKED_DECIMAL, 5);

        pnd_Report_Fields = localVariables.newGroupInRecord("pnd_Report_Fields", "#REPORT-FIELDS");
        pnd_Report_Fields_Pnd_Error_Msg = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 60);

        pnd_Contstants = localVariables.newGroupInRecord("pnd_Contstants", "#CONTSTANTS");
        pnd_Contstants_Pnd_Delimiter = pnd_Contstants.newFieldInGroup("pnd_Contstants_Pnd_Delimiter", "#DELIMITER", FieldType.STRING, 1);
        pnd_Contact_Name = localVariables.newFieldInRecord("pnd_Contact_Name", "#CONTACT-NAME", FieldType.STRING, 40);

        pnd_Contact_Name__R_Field_6 = localVariables.newGroupInRecord("pnd_Contact_Name__R_Field_6", "REDEFINE", pnd_Contact_Name);
        pnd_Contact_Name_Pnd_Contact_Name_8 = pnd_Contact_Name__R_Field_6.newFieldInGroup("pnd_Contact_Name_Pnd_Contact_Name_8", "#CONTACT-NAME-8", FieldType.STRING, 
            8);
        pnd_Wpid_Inds = localVariables.newFieldInRecord("pnd_Wpid_Inds", "#WPID-INDS", FieldType.STRING, 253);

        pnd_Wpid_Inds__R_Field_7 = localVariables.newGroupInRecord("pnd_Wpid_Inds__R_Field_7", "REDEFINE", pnd_Wpid_Inds);
        pnd_Wpid_Inds_Pnd_Wpid_Inds_Occur = pnd_Wpid_Inds__R_Field_7.newFieldArrayInGroup("pnd_Wpid_Inds_Pnd_Wpid_Inds_Occur", "#WPID-INDS-OCCUR", FieldType.STRING, 
            4, new DbsArrayController(1, 20));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Time_T = localVariables.newFieldInRecord("pnd_Time_T", "#TIME-T", FieldType.TIME);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);
        pnd_Time_A = localVariables.newFieldInRecord("pnd_Time_A", "#TIME-A", FieldType.STRING, 24);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Wp_Work_Prcss_Id.reset();
        vw_cwf_Support_Tbl.reset();

        ldaCdbatxa.initializeValues();
        ldaCwftotal.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("       CWF Work Process ID Download Report");
        pnd_Header1_2.setInitialValue("                         ");
        pnd_Contstants_Pnd_Delimiter.setInitialValue("@");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4029() throws Exception
    {
        super("Cwfb4029");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        getReports().write(0, "New Version as of 08/16/00");                                                                                                              //Natural: WRITE 'New Version as of 08/16/00'
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        vw_cwf_Wp_Work_Prcss_Id.startDatabaseRead                                                                                                                         //Natural: READ CWF-WP-WORK-PRCSS-ID BY WPID-UNIQ-KEY
        (
        "READ_PRIME",
        new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Wp_Work_Prcss_Id.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            //*     REJECT IF  CWF-WP-WORK-PRCSS-ID.DLTE-OPRTR-CDE GT ' '
            pnd_Counters_Pnd_Rex_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #REX-READ
                                                                                                                                                                          //Natural: PERFORM FORMAT-RECORD
            sub_Format_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *SAG END-EXIT
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM UPD-TOTAL-REC
        sub_Upd_Total_Rec();
        if (condition(Global.isEscape())) {return;}
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        getReports().write(1, ReportOption.NOTITLE,"Total Number of WPID Table Records Read  :",pnd_Counters_Pnd_Rex_Read,NEWLINE,"Total Number of Work File Records Written:",pnd_Counters_Pnd_Rex_Written,NEWLINE,"********* WPID Indicators Table *************",NEWLINE,"Number of WPID Indicators Table recs written:",pnd_Counters_Pnd_Wpid_Ind_Cntr,NEWLINE,"*********************************************",NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) 'Total Number of WPID Table Records Read  :' #REX-READ / 'Total Number of Work File Records Written:' #REX-WRITTEN / '********* WPID Indicators Table *************' / 'Number of WPID Indicators Table recs written:' #WPID-IND-CNTR / '*********************************************' //56T 'End of Report'
            TabSetting(56),"End of Report");
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-RECORD
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DATES
        //* **************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-WPID-USAGE-DESC
        //* *************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-WPID-INDS
        //* ******************************
        //*  ------------ SET THE KEY -----------------------------------
        //* *************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-WPID-IND-DESC
        //* **********************************
        //* *************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FILE-2
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPD-TOTAL-REC
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //*     'Long/Name'      WORK-RECORD.WORK-PRCSS-LONG-NME (AL=15)
        //*     '/Days'          WORK-RECORD.CRPRTE-SRVCE-STNDRD-DAYS-NBR
        //*      'Effct/Date Ind' WORK-RECORD.EFFCTVE-DTE-IND
        //*      'Chk/Mail INd'   WORK-RECORD.CHECK-MAIL-DTE-IND
        //*      'Txn/Date Ind'   WORK-RECORD.TRNSCTN-DTE-IND
        //*      'Cntrct/Ind'     WORK-RECORD.CNTRCT-SLCTN-IND
        //*      'MJ/Pull Ind'    WORK-RECORD.MJ-PULL-IND
        //*     'Check/Ind'      WORK-RECORD.CHECK-IND
        //*     'SEC/Ind'        WORK-RECORD.SEC-IND
        //*     'dgtn/ind'       WORK-RECORD.DGTZE-IND
        //*     'dgtz/rgn'       WORK-RECORD.DGTZE-REGION-IND
        //*     'dgtz/brnch'     WORK-RECORD.DGTZE-BRANCH-IND
        //*     'dgtz/spc nds'   WORK-RECORD.DGTZE-SPECIAL-NEEDS-IND
        //*     'bck/end'        WORK-RECORD.BACK-END-SCAN-IND
        //* *****************************
        //* *SAG END-EXIT
    }
    private void sub_Format_Record() throws Exception                                                                                                                     //Natural: FORMAT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        work_Record.reset();                                                                                                                                              //Natural: RESET WORK-RECORD
        work_Record.setValuesByName(vw_cwf_Wp_Work_Prcss_Id);                                                                                                             //Natural: MOVE BY NAME CWF-WP-WORK-PRCSS-ID TO WORK-RECORD
                                                                                                                                                                          //Natural: PERFORM FORMAT-DATES
        sub_Format_Dates();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-WPID-USAGE-DESC
        sub_Get_Wpid_Usage_Desc();
        if (condition(Global.isEscape())) {return;}
        //*  ---------- ADDED BY L.EPSTEIN ON 06/27/2000
        if (condition(cwf_Wp_Work_Prcss_Id_Wpid_Inds.notEquals(" ")))                                                                                                     //Natural: IF WPID-INDS NE ' '
        {
            pnd_Wpid_Inds.setValue(cwf_Wp_Work_Prcss_Id_Wpid_Inds);                                                                                                       //Natural: MOVE WPID-INDS TO #WPID-INDS
                                                                                                                                                                          //Natural: PERFORM GET-WPID-INDS
            sub_Get_Wpid_Inds();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        work_Record_Pnd_C1.setValue(pnd_Contstants_Pnd_Delimiter);                                                                                                        //Natural: MOVE #DELIMITER TO #C1 #C2 #C3 #C4 #C5 #C6 #C7 #C8 #C9 #C10 #C11 #C12 #C13 #C14 #C15 #C16 #C17 #C18 #C19 #C20 #C21 #C22 #C23 #C24 #C25 #C26 #C27 #C28 #C29 #C30 #C31 #C32 #C33 #C34 #C35
        work_Record_Pnd_C2.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C3.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C4.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C5.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C6.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C7.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C8.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C9.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C10.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C11.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C12.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C13.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C14.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C15.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C16.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C17.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C18.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C19.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C20.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C21.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C22.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C23.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C24.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C25.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C26.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C27.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C28.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C29.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C30.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C31.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C32.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C33.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C34.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C35.setValue(pnd_Contstants_Pnd_Delimiter);
        //* *************
        //*  FORMAT-RECORD
    }
    private void sub_Format_Dates() throws Exception                                                                                                                      //Natural: FORMAT-DATES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*   FORMAT UPDATE DATE
        if (condition(DbsUtil.maskMatches(cwf_Wp_Work_Prcss_Id_Updte_Dte,"YYYYMMDD")))                                                                                    //Natural: IF CWF-WP-WORK-PRCSS-ID.UPDTE-DTE EQ MASK ( YYYYMMDD )
        {
            pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Wp_Work_Prcss_Id_Updte_Dte_A);                                                                   //Natural: MOVE EDITED CWF-WP-WORK-PRCSS-ID.UPDTE-DTE-A TO #DATE-D ( EM = YYYYMMDD )
            work_Record_Updte_Dte_A12.setValueEdited(pnd_Date_D,new ReportEditMask("'''MM'/'DD'/'YYYY'''"));                                                              //Natural: MOVE EDITED #DATE-D ( EM = '"'MM'/'DD'/'YYYY'"' ) TO UPDTE-DTE-A12
        }                                                                                                                                                                 //Natural: END-IF
        //*   FORMAT ENTRY DATE
        if (condition(cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme.notEquals(getZero())))                                                                                           //Natural: IF ENTRY-DTE-TME NE 0
        {
            work_Record_Entry_Dte_A.setValueEdited(cwf_Wp_Work_Prcss_Id_Entry_Dte_Tme,new ReportEditMask("'''MM'/'DD'/'YYYY'''"));                                        //Natural: MOVE EDITED ENTRY-DTE-TME ( EM = '"'MM'/'DD'/'YYYY'"' ) TO ENTRY-DTE-A
        }                                                                                                                                                                 //Natural: END-IF
        //*   FORMAT DELETE DATE
        if (condition(cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme.notEquals(getZero())))                                                                                            //Natural: IF DLTE-DTE-TME NE 0
        {
            work_Record_Dlte_Dte_A.setValueEdited(cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme,new ReportEditMask("'''MM'/'DD'/'YYYY'''"));                                          //Natural: MOVE EDITED DLTE-DTE-TME ( EM = '"'MM'/'DD'/'YYYY'"' ) TO DLTE-DTE-A
        }                                                                                                                                                                 //Natural: END-IF
        //* **************
        //*  FORMAT-DATES
    }
    private void sub_Get_Wpid_Usage_Desc() throws Exception                                                                                                               //Natural: GET-WPID-USAGE-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet566 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE CWF-WP-WORK-PRCSS-ID.WPID-USAGE-IND;//Natural: VALUE ' I'
        if (condition((cwf_Wp_Work_Prcss_Id_Wpid_Usage_Ind.equals(" I"))))
        {
            decideConditionsMet566++;
            work_Record_Wpid_Usage_Desc.setValue("INST");                                                                                                                 //Natural: ASSIGN WORK-RECORD.WPID-USAGE-DESC := 'INST'
        }                                                                                                                                                                 //Natural: VALUE 'P '
        else if (condition((cwf_Wp_Work_Prcss_Id_Wpid_Usage_Ind.equals("P "))))
        {
            decideConditionsMet566++;
            work_Record_Wpid_Usage_Desc.setValue("PART");                                                                                                                 //Natural: ASSIGN WORK-RECORD.WPID-USAGE-DESC := 'PART'
        }                                                                                                                                                                 //Natural: VALUE 'PI'
        else if (condition((cwf_Wp_Work_Prcss_Id_Wpid_Usage_Ind.equals("PI"))))
        {
            decideConditionsMet566++;
            work_Record_Wpid_Usage_Desc.setValue("BOTH");                                                                                                                 //Natural: ASSIGN WORK-RECORD.WPID-USAGE-DESC := 'BOTH'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            work_Record_Wpid_Usage_Desc.reset();                                                                                                                          //Natural: RESET WORK-RECORD.WPID-USAGE-DESC
            //*  "P_"=USED FOR CWF ONLY
            //*   "_I"=USED FOR ICW ONLY
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *************
        //*  GET-WPID-USAGE-DESC
    }
    private void sub_Get_Wpid_Inds() throws Exception                                                                                                                     //Natural: GET-WPID-INDS
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pnd_Wpid_Inds_Pnd_Wpid_Inds_Occur.getValue(pnd_I).notEquals(" ")))                                                                              //Natural: IF #WPID-INDS-OCCUR ( #I ) NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM GET-WPID-IND-DESC
                sub_Get_Wpid_Ind_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                wpid_Ind_Record_Wpid.setValue(cwf_Wp_Work_Prcss_Id_Work_Prcss_Id);                                                                                        //Natural: MOVE CWF-WP-WORK-PRCSS-ID.WORK-PRCSS-ID TO WPID-IND-RECORD.WPID
                wpid_Ind_Record_Wpid_Ind_Cde.setValue(pnd_Wpid_Inds_Pnd_Wpid_Inds_Occur.getValue(pnd_I));                                                                 //Natural: MOVE #WPID-INDS-OCCUR ( #I ) TO WPID-IND-RECORD.WPID-IND-CDE
                pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A ");                                                                                                 //Natural: ASSIGN #TBL-SCRTY-LEVEL-IND := 'A '
                pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("WPID-INDICATORS");                                                                                          //Natural: ASSIGN #TBL-TABLE-NME := 'WPID-INDICATORS'
                pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue(pnd_Wpid_Inds_Pnd_Wpid_Inds_Occur.getValue(pnd_I));                                                          //Natural: ASSIGN #TBL-KEY-FIELD := #WPID-INDS-OCCUR ( #I )
                                                                                                                                                                          //Natural: PERFORM GET-WPID-IND-DESC
                sub_Get_Wpid_Ind_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ------------------------------------------------------------
                wpid_Ind_Record_Pnd_D1.setValue(pnd_Contstants_Pnd_Delimiter);                                                                                            //Natural: MOVE #DELIMITER TO #D1 #D2
                wpid_Ind_Record_Pnd_D2.setValue(pnd_Contstants_Pnd_Delimiter);
                                                                                                                                                                          //Natural: PERFORM WRITE-FILE-2
                sub_Write_File_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *************
        //*  GET-WPID-INDS
    }
    private void sub_Get_Wpid_Ind_Desc() throws Exception                                                                                                                 //Natural: GET-WPID-IND-DESC
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "R2",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        R2:
        while (condition(vw_cwf_Support_Tbl.readNextRow("R2")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme) || cwf_Support_Tbl_Tbl_Key_Field.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field))) //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME NE #TBL-PRIME-KEY.#TBL-TABLE-NME OR CWF-SUPPORT-TBL.TBL-KEY-FIELD NE #TBL-PRIME-KEY.#TBL-KEY-FIELD
            {
                if (true) break R2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R2. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            wpid_Ind_Record_Wpid_Ind_Cde_Desc.setValue(cwf_Support_Tbl_Tbl_Ind_Desc);                                                                                     //Natural: MOVE CWF-SUPPORT-TBL.TBL-IND-DESC TO WPID-IND-RECORD.WPID-IND-CDE-DESC
            //*  R2.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ************* /* R2.
        //*  GET-WPID-IND-DESC
    }
    private void sub_Write_File_2() throws Exception                                                                                                                      //Natural: WRITE-FILE-2
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getWorkFiles().write(2, false, wpid_Ind_Record);                                                                                                                  //Natural: WRITE WORK FILE 2 WPID-IND-RECORD
        pnd_Counters_Pnd_Wpid_Ind_Cntr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #WPID-IND-CNTR
        //* *************
        //*  GET-WPID-INDS
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(1, false, work_Record);                                                                                                                      //Natural: WRITE WORK FILE 1 WORK-RECORD
        pnd_Counters_Pnd_Rex_Written.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REX-WRITTEN
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        //* ********************************
        //*  WRITE-WORK-FILE
    }
    private void sub_Upd_Total_Rec() throws Exception                                                                                                                     //Natural: UPD-TOTAL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        total_Counters.reset();                                                                                                                                           //Natural: RESET TOTAL-COUNTERS
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 3 TOTAL
        while (condition(getWorkFiles().read(3, ldaCwftotal.getTotal())))
        {
            total_Counters.setValuesByName(ldaCwftotal.getTotal());                                                                                                       //Natural: MOVE BY NAME TOTAL TO TOTAL-COUNTERS
            ldaCwftotal.getTotal_Program_Name().setValue("CWFB4029");                                                                                                     //Natural: ASSIGN TOTAL.PROGRAM-NAME := 'CWFB4029'
            total_Counters_Wpidind_Add_Cntr.setValue(pnd_Counters_Pnd_Wpid_Ind_Cntr);                                                                                     //Natural: ASSIGN TOTAL-COUNTERS.WPIDIND-ADD-CNTR := #WPID-IND-CNTR
            ldaCwftotal.getTotal().setValuesByName(total_Counters);                                                                                                       //Natural: MOVE BY NAME TOTAL-COUNTERS TO TOTAL
            ldaCwftotal.getTotal_T01().setValue(pnd_Contstants_Pnd_Delimiter);                                                                                            //Natural: MOVE #DELIMITER TO T01 T02 T03 T04 T05 T06 T07 T08 T09 T10 T11 T12 T13 T14 T15 T16 T17 T18 T19 T20 T21 T22 T23 T24 T25 T26 T27
            ldaCwftotal.getTotal_T02().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T03().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T04().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T05().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T06().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T07().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T08().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T09().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T10().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T11().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T12().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T13().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T14().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T15().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T16().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T17().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T18().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T19().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T20().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T21().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T22().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T23().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T24().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T25().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T26().setValue(pnd_Contstants_Pnd_Delimiter);
            ldaCwftotal.getTotal_T27().setValue(pnd_Contstants_Pnd_Delimiter);
            getWorkFiles().write(4, false, ldaCwftotal.getTotal());                                                                                                       //Natural: WRITE WORK FILE 4 TOTAL
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().display(1, "/WPID",                                                                                                                                  //Natural: DISPLAY ( 1 ) '/WPID' WORK-RECORD.WORK-PRCSS-ID 'Short/Name' WORK-RECORD.WORK-PRCSS-SHORT-NME '/Hours' WORK-RECORD.CRPRTE-SRVCE-STNDRD-HOURS-NBR '/Mins' WORK-RECORD.CRPRTE-SRVCE-STNDRD-MINS-NBR 'Owner/Unit' WORK-RECORD.OWNER-UNIT-CDE 'KDO/Cde' WORK-RECORD.KDO-CDE 'Prty/Ind' WORK-RECORD.CRPRTE-PRTY-IND 'CRC/Ind' WORK-RECORD.CRC-IND 'Entry/Dte' WORK-RECORD.ENTRY-DTE-A 'Entry/Cde' WORK-RECORD.ENTRY-OPRTR-CDE 'Updte-Dte' WORK-RECORD.UPDTE-DTE-A12 'Updte/Cde' WORK-RECORD.UPDTE-OPRTR-CDE 'Dlte/Dte' WORK-RECORD.DLTE-DTE-A 'Dlte/Cde' WORK-RECORD.DLTE-OPRTR-CDE 'Usag/Desc' WORK-RECORD.WPID-USAGE-DESC
        		work_Record_Work_Prcss_Id,"Short/Name",
        		work_Record_Work_Prcss_Short_Nme,"/Hours",
        		work_Record_Crprte_Srvce_Stndrd_Hours_Nbr,"/Mins",
        		work_Record_Crprte_Srvce_Stndrd_Mins_Nbr,"Owner/Unit",
        		work_Record_Owner_Unit_Cde,"KDO/Cde",
        		work_Record_Kdo_Cde,"Prty/Ind",
        		work_Record_Crprte_Prty_Ind,"CRC/Ind",
        		work_Record_Crc_Ind,"Entry/Dte",
        		work_Record_Entry_Dte_A,"Entry/Cde",
        		work_Record_Entry_Oprtr_Cde,"Updte-Dte",
        		work_Record_Updte_Dte_A12,"Updte/Cde",
        		work_Record_Updte_Oprtr_Cde,"Dlte/Dte",
        		work_Record_Dlte_Dte_A,"Dlte/Cde",
        		work_Record_Dlte_Oprtr_Cde,"Usag/Desc",
        		work_Record_Wpid_Usage_Desc);
        if (Global.isEscape()) return;
        //* *****************************
        //* WRITE-REPORT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),pnd_Header1_2,new 
                        TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "/WPID",
        		work_Record_Work_Prcss_Id,"Short/Name",
        		work_Record_Work_Prcss_Short_Nme,"/Hours",
        		work_Record_Crprte_Srvce_Stndrd_Hours_Nbr,"/Mins",
        		work_Record_Crprte_Srvce_Stndrd_Mins_Nbr,"Owner/Unit",
        		work_Record_Owner_Unit_Cde,"KDO/Cde",
        		work_Record_Kdo_Cde,"Prty/Ind",
        		work_Record_Crprte_Prty_Ind,"CRC/Ind",
        		work_Record_Crc_Ind,"Entry/Dte",
        		work_Record_Entry_Dte_A,"Entry/Cde",
        		work_Record_Entry_Oprtr_Cde,"Updte-Dte",
        		work_Record_Updte_Dte_A12,"Updte/Cde",
        		work_Record_Updte_Oprtr_Cde,"Dlte/Dte",
        		work_Record_Dlte_Dte_A,"Dlte/Cde",
        		work_Record_Dlte_Oprtr_Cde,"Usag/Desc",
        		work_Record_Wpid_Usage_Desc);
    }
}
