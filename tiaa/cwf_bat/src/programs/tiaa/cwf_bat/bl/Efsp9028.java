/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:13:44 PM
**        * FROM NATURAL PROGRAM : Efsp9028
************************************************************
**        * FILE NAME            : Efsp9028.java
**        * CLASS NAME           : Efsp9028
**        * INSTANCE NAME        : Efsp9028
************************************************************
************************************************************************
* PROGRAM  : EFSP9028
* SYSTEM   : CRPCWF
* TITLE    : CWF-FOLDER-AUDIT (190) DELETE
* GENERATED: NOVEMBER 15, 1995 AT  10:00 AM
* FUNCTION : THIS PROGRAM DELETES RECORDS FROM THE CWF-FOLDER-AUDIT FILE
*            BY ISN.
* HISTORY
* ---------------------------------------------
* 12/18/95 JHH - REGISTER #X NOW 5 POSITIONS LONG
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsp9028 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Folder_Audit;

    private DbsGroup pnd_Rpt4_Work;
    private DbsField pnd_Rpt4_Work_Isn;
    private DbsField pnd_Isn_Cnt;
    private DbsField pnd_Et_Cnt;
    private DbsField pnd_X;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Folder_Audit = new DataAccessProgramView(new NameInfo("vw_cwf_Folder_Audit", "CWF-FOLDER-AUDIT"), "CWF_FOLDER_AUDIT", "CWF_FOLDER_AUDIT");
        registerRecord(vw_cwf_Folder_Audit);

        pnd_Rpt4_Work = localVariables.newGroupInRecord("pnd_Rpt4_Work", "#RPT4-WORK");
        pnd_Rpt4_Work_Isn = pnd_Rpt4_Work.newFieldArrayInGroup("pnd_Rpt4_Work_Isn", "ISN", FieldType.PACKED_DECIMAL, 8, new DbsArrayController(1, 1000));
        pnd_Isn_Cnt = localVariables.newFieldInRecord("pnd_Isn_Cnt", "#ISN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Et_Cnt = localVariables.newFieldInRecord("pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Folder_Audit.reset();

        localVariables.reset();
        pnd_Isn_Cnt.setInitialValue(0);
        pnd_Et_Cnt.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Efsp9028() throws Exception
    {
        super("Efsp9028");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("EFSP9028", onError);
        //*  -------------------------
        READ_WRK:                                                                                                                                                         //Natural: READ WORK 4 #RPT4-WORK
        while (condition(getWorkFiles().read(4, pnd_Rpt4_Work)))
        {
            FOR01:                                                                                                                                                        //Natural: FOR #X = 1 TO 1000
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(1000)); pnd_X.nadd(1))
            {
                if (condition(pnd_Rpt4_Work_Isn.getValue(pnd_X).equals(0)))                                                                                               //Natural: IF #RPT4-WORK.ISN ( #X ) = 00000000
                {
                    if (true) break READ_WRK;                                                                                                                             //Natural: ESCAPE BOTTOM ( READ-WRK. )
                }                                                                                                                                                         //Natural: END-IF
                //*  FILE 190
                GET_AUD:                                                                                                                                                  //Natural: GET CWF-FOLDER-AUDIT #RPT4-WORK.ISN ( #X )
                vw_cwf_Folder_Audit.readByID(pnd_Rpt4_Work_Isn.getValue(pnd_X).getLong(), "GET_AUD");
                vw_cwf_Folder_Audit.deleteDBRow("GET_AUD");                                                                                                               //Natural: DELETE ( GET-AUD. )
                pnd_Isn_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #ISN-CNT
                pnd_Et_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ET-CNT
                if (condition(pnd_Et_Cnt.greater(200)))                                                                                                                   //Natural: IF #ET-CNT > 200
                {
                    pnd_Et_Cnt.reset();                                                                                                                                   //Natural: RESET #ET-CNT
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_WRK"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_WRK"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READ_WRK_Exit:
        if (Global.isEscape()) return;
        //*  ---------------------------------------------
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  =============================================
        getReports().write(0, NEWLINE,pnd_Isn_Cnt,"Audit Records deleted");                                                                                               //Natural: WRITE / #ISN-CNT 'Audit Records deleted'
        if (Global.isEscape()) return;
        //*  -------------------------------
        //*  -------------------------------                                                                                                                              //Natural: ON ERROR
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "NATURAL error",Global.getERROR_NR(),"in",Global.getPROGRAM(),"at line",Global.getERROR_LINE(),"on record",pnd_Isn_Cnt);                    //Natural: WRITE 'NATURAL error' *ERROR-NR 'in' *PROGRAM 'at line' *ERROR-LINE 'on record' #ISN-CNT
        DbsUtil.terminate(5);  if (true) return;                                                                                                                          //Natural: TERMINATE 05
    };                                                                                                                                                                    //Natural: END-ERROR
}
