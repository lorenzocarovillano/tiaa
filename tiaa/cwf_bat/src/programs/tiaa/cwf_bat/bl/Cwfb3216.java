/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:05 PM
**        * FROM NATURAL PROGRAM : Cwfb3216
************************************************************
**        * FILE NAME            : Cwfb3216.java
**        * CLASS NAME           : Cwfb3216
**        * INSTANCE NAME        : Cwfb3216
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 10
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3216
* SYSTEM   : CRPCWF
* TITLE    : REPORT 9
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : FOLLOW-UP REPORT OF PENDED CASES
*          | SORTED BY UNIT
*          |
*          |
*          |
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3216 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Env;
    private DbsField pnd_Work_Date;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Pin_Nbr;
    private DbsField pnd_Work_Record_Work_Prcss_Id;
    private DbsField pnd_Work_Record_Empl_Oprtr_Cde;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Empl_Racf_Id;
    private DbsField pnd_Work_Record_Last_Chnge_Dte_Tme;
    private DbsField pnd_Work_Record_Last_Chnge_Oprtr_Cde;
    private DbsField pnd_Work_Record_Step_Id;
    private DbsField pnd_Work_Record_Admin_Unit_Cde;
    private DbsField pnd_Work_Record_Tbl_Status_Key1;

    private DbsGroup pnd_Work_Record__R_Field_2;
    private DbsField pnd_Work_Record_Unit_Cde;
    private DbsField pnd_Work_Record_Status_Cde;
    private DbsField pnd_Work_Record_Tbl_Status_Key2;

    private DbsGroup pnd_Work_Record__R_Field_3;
    private DbsField pnd_Work_Record_Last_Chnge_Unit_Cde;
    private DbsField pnd_Work_Record_Admin_Status_Cde;
    private DbsField pnd_Work_Record_Admin_Status_Updte_Dte_Tme;
    private DbsField pnd_Work_Record_Tiaa_Rcvd_Dte;
    private DbsField pnd_Work_Record_Cntrct_Nbr;
    private DbsField pnd_Work_Record_Pnd_Calendar_Days_In_Tiaa;
    private DbsField pnd_Work_Record_Pnd_Business_Days_In_Tiaa;
    private DbsField pnd_Work_Record_Extrnl_Pend_Rcv_Dte;
    private DbsField pnd_Work_Record_Pnd_Received_In_Unit;
    private DbsField pnd_Work_Record_Pnd_Bsnss_Days_In_Unit;
    private DbsField pnd_Work_Record_Pnd_Intrnl_Pend_Days;
    private DbsField pnd_Work_Record_Pnd_Extrnl_Pend_Days;
    private DbsField pnd_Work_Record_Partic_Sname;

    private DbsGroup pnd_Work_Record__R_Field_4;
    private DbsField pnd_Work_Record_Pnd_Folder_Prefix;
    private DbsField pnd_Work_Record_Pnd_Folder_Nbr;
    private DbsField pnd_Work_Record_Tbl_Class;

    private DbsGroup pnd_Report_Data;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Cde;

    private DbsGroup pnd_Report_Data__R_Field_5;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Id_Cde;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid;

    private DbsGroup pnd_Report_Data__R_Field_6;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Lob;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Mbp;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Sbp;
    private DbsField pnd_Report_Data_Pnd_Rep_Racf_Id;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Empl_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Subtotal_Ind;

    private DbsGroup pnd_Misc_Parm;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Action;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Sub_Count;

    private DbsGroup pnd_Misc_Parm_Pnd_Array_Ctrs;
    private DbsField pnd_Misc_Parm_Pnd_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Booklet_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Forms_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Inquire_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Research_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Trans_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Complaint_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Read_Count;
    private DbsField pnd_Misc_Parm_Pnd_Return_Code;
    private DbsField pnd_Misc_Parm_Pnd_Return_Msg;
    private DbsField pnd_Misc_Parm_Pnd_Status_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Return_Doc_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Return_Rcvd_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Confirmed;
    private DbsField pnd_Actv_Unque_Key;
    private DbsField pnd_Report_No;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Comp_Date;

    private DbsGroup pnd_Comp_Date__R_Field_7;
    private DbsField pnd_Comp_Date_Pnd_Comp_Yy;
    private DbsField pnd_Comp_Date_Pnd_Comp_Mm;
    private DbsField pnd_Comp_Date_Pnd_Comp_Dd;
    private DbsField pnd_Save_Unit;
    private DbsField pnd_First_Unit;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Page_Num;
    private DbsField pnd_Rep_Unit_Nme;
    private DbsField pnd_Parm_Report_No;
    private DbsField pnd_Parm_Empl_Racf_Id;
    private DbsField pnd_Parm_Unit_Cde;
    private DbsField pnd_Parm_Floor;
    private DbsField pnd_Parm_Bldg;
    private DbsField pnd_Parm_Drop_Off;
    private DbsField pnd_Parm_Type;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Tbl_ClassOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Pin_Nbr = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Work_Record_Work_Prcss_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Work_Record_Empl_Oprtr_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10);

        pnd_Work_Record__R_Field_1 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record_Empl_Oprtr_Cde);
        pnd_Work_Record_Empl_Racf_Id = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Work_Record_Last_Chnge_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15);
        pnd_Work_Record_Last_Chnge_Oprtr_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8);
        pnd_Work_Record_Step_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Step_Id", "STEP-ID", FieldType.STRING, 6);
        pnd_Work_Record_Admin_Unit_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Status_Key1 = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Key1", "TBL-STATUS-KEY1", FieldType.STRING, 13);

        pnd_Work_Record__R_Field_2 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_2", "REDEFINE", pnd_Work_Record_Tbl_Status_Key1);
        pnd_Work_Record_Unit_Cde = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_Record_Status_Cde = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        pnd_Work_Record_Tbl_Status_Key2 = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Key2", "TBL-STATUS-KEY2", FieldType.STRING, 13);

        pnd_Work_Record__R_Field_3 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_3", "REDEFINE", pnd_Work_Record_Tbl_Status_Key2);
        pnd_Work_Record_Last_Chnge_Unit_Cde = pnd_Work_Record__R_Field_3.newFieldInGroup("pnd_Work_Record_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8);
        pnd_Work_Record_Admin_Status_Cde = pnd_Work_Record__R_Field_3.newFieldInGroup("pnd_Work_Record_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record_Admin_Status_Updte_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME);
        pnd_Work_Record_Tiaa_Rcvd_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Work_Record_Cntrct_Nbr = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Work_Record_Pnd_Calendar_Days_In_Tiaa = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Calendar_Days_In_Tiaa", "#CALENDAR-DAYS-IN-TIAA", 
            FieldType.NUMERIC, 5);
        pnd_Work_Record_Pnd_Business_Days_In_Tiaa = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Business_Days_In_Tiaa", "#BUSINESS-DAYS-IN-TIAA", 
            FieldType.NUMERIC, 5);
        pnd_Work_Record_Extrnl_Pend_Rcv_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", FieldType.TIME);
        pnd_Work_Record_Pnd_Received_In_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Received_In_Unit", "#RECEIVED-IN-UNIT", FieldType.TIME);
        pnd_Work_Record_Pnd_Bsnss_Days_In_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Bsnss_Days_In_Unit", "#BSNSS-DAYS-IN-UNIT", FieldType.NUMERIC, 
            5, 1);
        pnd_Work_Record_Pnd_Intrnl_Pend_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Intrnl_Pend_Days", "#INTRNL-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        pnd_Work_Record_Pnd_Extrnl_Pend_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Extrnl_Pend_Days", "#EXTRNL-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        pnd_Work_Record_Partic_Sname = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Partic_Sname", "PARTIC-SNAME", FieldType.STRING, 7);

        pnd_Work_Record__R_Field_4 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_4", "REDEFINE", pnd_Work_Record_Partic_Sname);
        pnd_Work_Record_Pnd_Folder_Prefix = pnd_Work_Record__R_Field_4.newFieldInGroup("pnd_Work_Record_Pnd_Folder_Prefix", "#FOLDER-PREFIX", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_Folder_Nbr = pnd_Work_Record__R_Field_4.newFieldInGroup("pnd_Work_Record_Pnd_Folder_Nbr", "#FOLDER-NBR", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_Tbl_Class = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Class", "TBL-CLASS", FieldType.STRING, 1);

        pnd_Report_Data = localVariables.newGroupInRecord("pnd_Report_Data", "#REPORT-DATA");
        pnd_Report_Data_Pnd_Rep_Unit_Cde = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);

        pnd_Report_Data__R_Field_5 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_5", "REDEFINE", pnd_Report_Data_Pnd_Rep_Unit_Cde);
        pnd_Report_Data_Pnd_Rep_Unit_Id_Cde = pnd_Report_Data__R_Field_5.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Id_Cde", "#REP-UNIT-ID-CDE", FieldType.STRING, 
            5);
        pnd_Report_Data_Pnd_Rep_Wpid = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid", "#REP-WPID", FieldType.STRING, 6);

        pnd_Report_Data__R_Field_6 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_6", "REDEFINE", pnd_Report_Data_Pnd_Rep_Wpid);
        pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde = pnd_Report_Data__R_Field_6.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde", "#REP-WPID-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        pnd_Report_Data_Pnd_Rep_Wpid_Lob = pnd_Report_Data__R_Field_6.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Lob", "#REP-WPID-LOB", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Wpid_Mbp = pnd_Report_Data__R_Field_6.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Mbp", "#REP-WPID-MBP", FieldType.STRING, 
            1);
        pnd_Report_Data_Pnd_Rep_Wpid_Sbp = pnd_Report_Data__R_Field_6.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Sbp", "#REP-WPID-SBP", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Racf_Id = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Racf_Id", "#REP-RACF-ID", FieldType.STRING, 8);
        pnd_Report_Data_Pnd_Rep_Wpid_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Name", "#REP-WPID-NAME", FieldType.STRING, 45);
        pnd_Report_Data_Pnd_Rep_Empl_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Empl_Name", "#REP-EMPL-NAME", FieldType.STRING, 30);
        pnd_Report_Data_Pnd_Rep_Subtotal_Ind = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Subtotal_Ind", "#REP-SUBTOTAL-IND", FieldType.STRING, 
            1);

        pnd_Misc_Parm = localVariables.newGroupInRecord("pnd_Misc_Parm", "#MISC-PARM");
        pnd_Misc_Parm_Pnd_Wpid_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Action", "#WPID-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Wpid_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Sname", "#WPID-SNAME", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Sub_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Sub_Count", "#SUB-COUNT", FieldType.NUMERIC, 6);

        pnd_Misc_Parm_Pnd_Array_Ctrs = pnd_Misc_Parm.newGroupArrayInGroup("pnd_Misc_Parm_Pnd_Array_Ctrs", "#ARRAY-CTRS", new DbsArrayController(1, 14));
        pnd_Misc_Parm_Pnd_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Ctr", "#CTR", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Booklet_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Booklet_Ctr", "#BOOKLET-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Forms_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Forms_Ctr", "#FORMS-CTR", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Inquire_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Inquire_Ctr", "#INQUIRE-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Research_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Research_Ctr", "#RESEARCH-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Trans_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Trans_Ctr", "#TRANS-CTR", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Complaint_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Complaint_Ctr", "#COMPLAINT-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Read_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Read_Count", "#READ-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Return_Code = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 1);
        pnd_Misc_Parm_Pnd_Return_Msg = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 20);
        pnd_Misc_Parm_Pnd_Status_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Status_Sname", "#STATUS-SNAME", FieldType.STRING, 25);
        pnd_Misc_Parm_Pnd_Return_Doc_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Doc_Txt", "#RETURN-DOC-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Return_Rcvd_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Rcvd_Txt", "#RETURN-RCVD-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Confirmed = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Confirmed", "#CONFIRMED", FieldType.BOOLEAN, 1);
        pnd_Actv_Unque_Key = localVariables.newFieldInRecord("pnd_Actv_Unque_Key", "#ACTV-UNQUE-KEY", FieldType.STRING, 15);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);

        pnd_Comp_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Comp_Date__R_Field_7", "REDEFINE", pnd_Comp_Date);
        pnd_Comp_Date_Pnd_Comp_Yy = pnd_Comp_Date__R_Field_7.newFieldInGroup("pnd_Comp_Date_Pnd_Comp_Yy", "#COMP-YY", FieldType.NUMERIC, 4);
        pnd_Comp_Date_Pnd_Comp_Mm = pnd_Comp_Date__R_Field_7.newFieldInGroup("pnd_Comp_Date_Pnd_Comp_Mm", "#COMP-MM", FieldType.NUMERIC, 2);
        pnd_Comp_Date_Pnd_Comp_Dd = pnd_Comp_Date__R_Field_7.newFieldInGroup("pnd_Comp_Date_Pnd_Comp_Dd", "#COMP-DD", FieldType.NUMERIC, 2);
        pnd_Save_Unit = localVariables.newFieldInRecord("pnd_Save_Unit", "#SAVE-UNIT", FieldType.STRING, 8);
        pnd_First_Unit = localVariables.newFieldInRecord("pnd_First_Unit", "#FIRST-UNIT", FieldType.BOOLEAN, 1);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Page_Num = localVariables.newFieldInRecord("pnd_Page_Num", "#PAGE-NUM", FieldType.NUMERIC, 4);
        pnd_Rep_Unit_Nme = localVariables.newFieldInRecord("pnd_Rep_Unit_Nme", "#REP-UNIT-NME", FieldType.STRING, 45);
        pnd_Parm_Report_No = localVariables.newFieldInRecord("pnd_Parm_Report_No", "#PARM-REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Parm_Empl_Racf_Id = localVariables.newFieldInRecord("pnd_Parm_Empl_Racf_Id", "#PARM-EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Parm_Unit_Cde = localVariables.newFieldInRecord("pnd_Parm_Unit_Cde", "#PARM-UNIT-CDE", FieldType.STRING, 8);
        pnd_Parm_Floor = localVariables.newFieldInRecord("pnd_Parm_Floor", "#PARM-FLOOR", FieldType.NUMERIC, 2);
        pnd_Parm_Bldg = localVariables.newFieldInRecord("pnd_Parm_Bldg", "#PARM-BLDG", FieldType.STRING, 3);
        pnd_Parm_Drop_Off = localVariables.newFieldInRecord("pnd_Parm_Drop_Off", "#PARM-DROP-OFF", FieldType.STRING, 2);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Tbl_ClassOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Class_OLD", "Tbl_Class_OLD", FieldType.STRING, 1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        localVariables.reset();
        pnd_Report_No.setInitialValue(10);
        pnd_Report_Parm.setInitialValue("CWFB3010M*");
        pnd_Parm_Report_No.setInitialValue(10);
        pnd_Parm_Type.setInitialValue("M");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3216() throws Exception
    {
        super("Cwfb3216");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Date.setValue(Global.getDATX());                                                                                                                         //Natural: MOVE *DATX TO #WORK-DATE
        pnd_Comp_Date.setValueEdited(pnd_Work_Date,new ReportEditMask("YYYYMMDD"));                                                                                       //Natural: MOVE EDITED #WORK-DATE ( EM = YYYYMMDD ) TO #COMP-DATE
        //*  USE CURRENT MONTH
        if (condition(pnd_Comp_Date_Pnd_Comp_Dd.greaterOrEqual(25) && pnd_Comp_Date_Pnd_Comp_Dd.lessOrEqual(31)))                                                         //Natural: IF #COMP-DD = 25 THRU 31
        {
            pnd_Comp_Date_Pnd_Comp_Mm.nadd(1);                                                                                                                            //Natural: ADD 1 TO #COMP-MM
            if (condition(pnd_Comp_Date_Pnd_Comp_Mm.greater(12)))                                                                                                         //Natural: IF #COMP-MM GT 12
            {
                pnd_Comp_Date_Pnd_Comp_Yy.nadd(1);                                                                                                                        //Natural: ADD 1 TO #COMP-YY
                pnd_Comp_Date_Pnd_Comp_Mm.setValue(1);                                                                                                                    //Natural: MOVE 01 TO #COMP-MM
                pnd_Comp_Date_Pnd_Comp_Dd.setValue(1);                                                                                                                    //Natural: MOVE 01 TO #COMP-DD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Comp_Date_Pnd_Comp_Dd.setValue(1);                                                                                                                            //Natural: MOVE 01 TO #COMP-DD
        pnd_Work_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Comp_Date);                                                                                       //Natural: MOVE EDITED #COMP-DATE TO #WORK-DATE ( EM = YYYYMMDD )
        pnd_Work_Date.nsubtract(1);                                                                                                                                       //Natural: COMPUTE #WORK-DATE = #WORK-DATE - 1
        pnd_Comp_Date.setValueEdited(pnd_Work_Date,new ReportEditMask("YYYYMMDD"));                                                                                       //Natural: MOVE EDITED #WORK-DATE ( EM = YYYYMMDD ) TO #COMP-DATE
        READWORK01:                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 142 PS = 58;//Natural: READ WORK 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, pnd_Work_Record)))
        {
            getSort().writeSortInData(pnd_Work_Record_Admin_Unit_Cde, pnd_Work_Record_Tbl_Class, pnd_Work_Record_Pnd_Extrnl_Pend_Days, pnd_Work_Record_Work_Prcss_Id,     //Natural: END-ALL
                pnd_Work_Record_Extrnl_Pend_Rcv_Dte, pnd_Work_Record_Tiaa_Rcvd_Dte, pnd_Work_Record_Step_Id, pnd_Work_Record_Admin_Status_Cde, pnd_Work_Record_Empl_Racf_Id, 
                pnd_Work_Record_Pnd_Calendar_Days_In_Tiaa, pnd_Work_Record_Pnd_Business_Days_In_Tiaa, pnd_Work_Record_Pnd_Bsnss_Days_In_Unit, pnd_Work_Record_Pin_Nbr, 
                pnd_Work_Record_Cntrct_Nbr, pnd_Work_Record_Admin_Status_Updte_Dte_Tme, pnd_Work_Record_Unit_Cde, pnd_Work_Record_Status_Cde, pnd_Work_Record_Pnd_Received_In_Unit, 
                pnd_Work_Record_Last_Chnge_Unit_Cde, pnd_Work_Record_Last_Chnge_Dte_Tme, pnd_Work_Record_Pnd_Intrnl_Pend_Days, pnd_Work_Record_Partic_Sname);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Work_Record_Admin_Unit_Cde, pnd_Work_Record_Tbl_Class, pnd_Work_Record_Pnd_Extrnl_Pend_Days, pnd_Work_Record_Work_Prcss_Id,                //Natural: SORT BY #WORK-RECORD.ADMIN-UNIT-CDE #WORK-RECORD.TBL-CLASS #WORK-RECORD.#EXTRNL-PEND-DAYS #WORK-RECORD.WORK-PRCSS-ID #WORK-RECORD.EXTRNL-PEND-RCV-DTE #WORK-RECORD.TIAA-RCVD-DTE #WORK-RECORD.STEP-ID #WORK-RECORD.ADMIN-STATUS-CDE #WORK-RECORD.EMPL-RACF-ID USING #WORK-RECORD.#CALENDAR-DAYS-IN-TIAA #WORK-RECORD.#BUSINESS-DAYS-IN-TIAA #WORK-RECORD.#BSNSS-DAYS-IN-UNIT #WORK-RECORD.PIN-NBR #WORK-RECORD.CNTRCT-NBR #WORK-RECORD.ADMIN-STATUS-UPDTE-DTE-TME #WORK-RECORD.UNIT-CDE #WORK-RECORD.STATUS-CDE #WORK-RECORD.#RECEIVED-IN-UNIT #WORK-RECORD.LAST-CHNGE-UNIT-CDE #WORK-RECORD.LAST-CHNGE-DTE-TME #WORK-RECORD.#INTRNL-PEND-DAYS #WORK-RECORD.PARTIC-SNAME
            pnd_Work_Record_Extrnl_Pend_Rcv_Dte, pnd_Work_Record_Tiaa_Rcvd_Dte, pnd_Work_Record_Step_Id, pnd_Work_Record_Admin_Status_Cde, pnd_Work_Record_Empl_Racf_Id);
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Work_Record_Admin_Unit_Cde, pnd_Work_Record_Tbl_Class, pnd_Work_Record_Pnd_Extrnl_Pend_Days, pnd_Work_Record_Work_Prcss_Id, 
            pnd_Work_Record_Extrnl_Pend_Rcv_Dte, pnd_Work_Record_Tiaa_Rcvd_Dte, pnd_Work_Record_Step_Id, pnd_Work_Record_Admin_Status_Cde, pnd_Work_Record_Empl_Racf_Id, 
            pnd_Work_Record_Pnd_Calendar_Days_In_Tiaa, pnd_Work_Record_Pnd_Business_Days_In_Tiaa, pnd_Work_Record_Pnd_Bsnss_Days_In_Unit, pnd_Work_Record_Pin_Nbr, 
            pnd_Work_Record_Cntrct_Nbr, pnd_Work_Record_Admin_Status_Updte_Dte_Tme, pnd_Work_Record_Unit_Cde, pnd_Work_Record_Status_Cde, pnd_Work_Record_Pnd_Received_In_Unit, 
            pnd_Work_Record_Last_Chnge_Unit_Cde, pnd_Work_Record_Last_Chnge_Dte_Tme, pnd_Work_Record_Pnd_Intrnl_Pend_Days, pnd_Work_Record_Partic_Sname)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(pnd_Save_Unit.equals(" ")))                                                                                                                     //Natural: IF #SAVE-UNIT = ' '
            {
                pnd_First_Unit.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #FIRST-UNIT
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
                pnd_Save_Unit.setValue(pnd_Work_Record_Admin_Unit_Cde);                                                                                                   //Natural: MOVE #WORK-RECORD.ADMIN-UNIT-CDE TO #SAVE-UNIT
                DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Work_Record_Admin_Unit_Cde, pnd_Unit_Name);                                                //Natural: CALLNAT 'CWFN1103' #WORK-RECORD.ADMIN-UNIT-CDE #UNIT-NAME
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_New_Unit.getBoolean() || pnd_First_Unit.getBoolean()))                                                                                      //Natural: IF #NEW-UNIT OR #FIRST-UNIT
            {
                pnd_Parm_Bldg.setValue("M");                                                                                                                              //Natural: MOVE 'M' TO #PARM-BLDG
                DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Parm_Report_No, pnd_Parm_Empl_Racf_Id, pnd_Save_Unit, pnd_Parm_Floor, pnd_Parm_Bldg,       //Natural: CALLNAT 'CWFN3910' #PARM-REPORT-NO #PARM-EMPL-RACF-ID #SAVE-UNIT #PARM-FLOOR #PARM-BLDG #PARM-DROP-OFF #COMP-DATE
                    pnd_Parm_Drop_Off, pnd_Comp_Date);
                if (condition(Global.isEscape())) return;
                pnd_First_Unit.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #FIRST-UNIT
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet194 = 0;                                                                                                                             //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.#EXTRNL-PEND-DAYS;//Natural: VALUE 00:29.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(0) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(29)))
            {
                decideConditionsMet194++;
                pnd_Misc_Parm_Pnd_Ctr.getValue(1).nadd(1);                                                                                                                //Natural: ADD 1 TO #CTR ( 1 )
            }                                                                                                                                                             //Natural: VALUE 30:59.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(30) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(59)))
            {
                decideConditionsMet194++;
                pnd_Misc_Parm_Pnd_Ctr.getValue(2).nadd(1);                                                                                                                //Natural: ADD 1 TO #CTR ( 2 )
            }                                                                                                                                                             //Natural: VALUE 60:89.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(60) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(89)))
            {
                decideConditionsMet194++;
                pnd_Misc_Parm_Pnd_Ctr.getValue(3).nadd(1);                                                                                                                //Natural: ADD 1 TO #CTR ( 3 )
            }                                                                                                                                                             //Natural: VALUE 90:364.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(90) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(364)))
            {
                decideConditionsMet194++;
                //*  1 TO < 2 YEARS
                pnd_Misc_Parm_Pnd_Ctr.getValue(4).nadd(1);                                                                                                                //Natural: ADD 1 TO #CTR ( 4 )
            }                                                                                                                                                             //Natural: VALUE 365:729.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(365) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(729)))
            {
                decideConditionsMet194++;
                //*  2 TO < 3 YEARS
                pnd_Misc_Parm_Pnd_Ctr.getValue(5).nadd(1);                                                                                                                //Natural: ADD 1 TO #CTR ( 5 )
            }                                                                                                                                                             //Natural: VALUE 730:1094.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(730) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(1094)))
            {
                decideConditionsMet194++;
                //*  3 TO < 4 YEARS
                pnd_Misc_Parm_Pnd_Ctr.getValue(6).nadd(1);                                                                                                                //Natural: ADD 1 TO #CTR ( 6 )
            }                                                                                                                                                             //Natural: VALUE 1095:1459.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(1095) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(1459)))
            {
                decideConditionsMet194++;
                //*  4 TO < 5 YEARS
                pnd_Misc_Parm_Pnd_Ctr.getValue(7).nadd(1);                                                                                                                //Natural: ADD 1 TO #CTR ( 7 )
            }                                                                                                                                                             //Natural: VALUE 1460:1824.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(1460) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(1824)))
            {
                decideConditionsMet194++;
                //*  5 TO < 6 YEARS
                pnd_Misc_Parm_Pnd_Ctr.getValue(8).nadd(1);                                                                                                                //Natural: ADD 1 TO #CTR ( 8 )
            }                                                                                                                                                             //Natural: VALUE 1825:2189.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(1825) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(2189)))
            {
                decideConditionsMet194++;
                //*  6 TO < 7 YEARS
                pnd_Misc_Parm_Pnd_Ctr.getValue(9).nadd(1);                                                                                                                //Natural: ADD 1 TO #CTR ( 9 )
            }                                                                                                                                                             //Natural: VALUE 2190:2554.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(2190) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(2554)))
            {
                decideConditionsMet194++;
                //*  7 TO < 8 YEARS
                pnd_Misc_Parm_Pnd_Ctr.getValue(10).nadd(1);                                                                                                               //Natural: ADD 1 TO #CTR ( 10 )
            }                                                                                                                                                             //Natural: VALUE 2555:2919.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(2555) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(2919)))
            {
                decideConditionsMet194++;
                //*  8 TO < 9 YEARS
                pnd_Misc_Parm_Pnd_Ctr.getValue(11).nadd(1);                                                                                                               //Natural: ADD 1 TO #CTR ( 11 )
            }                                                                                                                                                             //Natural: VALUE 2920:3284.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(2920) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(3284)))
            {
                decideConditionsMet194++;
                //*  9 TO < 10 YEARS
                pnd_Misc_Parm_Pnd_Ctr.getValue(12).nadd(1);                                                                                                               //Natural: ADD 1 TO #CTR ( 12 )
            }                                                                                                                                                             //Natural: VALUE 3285:3649.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(3285) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(3649)))
            {
                decideConditionsMet194++;
                //*  10 YEARS OR MORE
                pnd_Misc_Parm_Pnd_Ctr.getValue(13).nadd(1);                                                                                                               //Natural: ADD 1 TO #CTR ( 13 )
            }                                                                                                                                                             //Natural: VALUE 3650:9999.9
            if (condition(pnd_Work_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(3650) && pnd_Work_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(9999)))
            {
                decideConditionsMet194++;
                pnd_Misc_Parm_Pnd_Ctr.getValue(14).nadd(1);                                                                                                               //Natural: ADD 1 TO #CTR ( 14 )
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet194 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            DbsUtil.callnat(Cwfn5390.class , getCurrentProcessState(), pnd_Work_Record_Work_Prcss_Id, pnd_Misc_Parm_Pnd_Wpid_Sname);                                      //Natural: AT TOP OF PAGE ( 1 );//Natural: CALLNAT 'CWFN5390' #WORK-RECORD.WORK-PRCSS-ID #WPID-SNAME
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Work_Record_Admin_Unit_Cde.greaterOrEqual("4500") && pnd_Work_Record_Admin_Unit_Cde.lessOrEqual("4997")))                                   //Natural: IF #WORK-RECORD.ADMIN-UNIT-CDE = '4500' THRU '4997'
            {
                DbsUtil.callnat(Cwfn1102.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Status_Key2, pnd_Misc_Parm_Pnd_Status_Sname);                              //Natural: CALLNAT 'CWFN1102' #WORK-RECORD.TBL-STATUS-KEY2 #STATUS-SNAME
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                DbsUtil.callnat(Cwfn1102.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Status_Key1, pnd_Misc_Parm_Pnd_Status_Sname);                              //Natural: CALLNAT 'CWFN1102' #WORK-RECORD.TBL-STATUS-KEY1 #STATUS-SNAME
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Extrnl_Pend_Rcv_Dte.greater(getZero())))                                                                                        //Natural: IF #WORK-RECORD.EXTRNL-PEND-RCV-DTE GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Doc_Txt.setValueEdited(pnd_Work_Record_Extrnl_Pend_Rcv_Dte,new ReportEditMask("MM/DD/YY"));                                      //Natural: MOVE EDITED #WORK-RECORD.EXTRNL-PEND-RCV-DTE ( EM = MM/DD/YY ) TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Doc_Txt.setValue(" ");                                                                                                           //Natural: MOVE ' ' TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Received_In_Unit.greater(getZero())))                                                                                       //Natural: IF #WORK-RECORD.#RECEIVED-IN-UNIT GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValueEdited(pnd_Work_Record_Pnd_Received_In_Unit,new ReportEditMask("MM/DD/YY"));                                    //Natural: MOVE EDITED #WORK-RECORD.#RECEIVED-IN-UNIT ( EM = MM/DD/YY ) TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValue(" ");                                                                                                          //Natural: MOVE ' ' TO #RETURN-RCVD-TXT
                //*  PIN-EXP
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "//Received/At TIAA",                                                                                                                 //Natural: DISPLAY ( 1 ) '//Received/At TIAA' #WORK-RECORD.TIAA-RCVD-DTE ( EM = MM/DD/YY ) '//Doc/Rec"d' #RETURN-DOC-TXT '//Rec"d In/Unit' #RETURN-RCVD-TXT 'Bsnss/Days/in/Unit' #WORK-RECORD.#BSNSS-DAYS-IN-UNIT ( EM = Z9.9 ) '//Work/Process' #WPID-SNAME '//Work/Step' #WORK-RECORD.STEP-ID '///Status' #STATUS-SNAME ( AL = 15 ) '//Status/Date' #WORK-RECORD.ADMIN-STATUS-UPDTE-DTE-TME ( EM = MM/DD/YY ) '/Days/Ext/Pend' #WORK-RECORD.#EXTRNL-PEND-DAYS ( EM = ZZZ9.9 ) 'Clndr/Days/in/TIAA' #WORK-RECORD.#CALENDAR-DAYS-IN-TIAA ( EM = ZZZ9 ) 'Bsnss/Days/in/TIAA' #WORK-RECORD.#BUSINESS-DAYS-IN-TIAA ( EM = ZZZ9 ) '//Partic/Name' PARTIC-SNAME '///PIN' #WORK-RECORD.PIN-NBR ( EM = 999999999999 ) '//Contract/Number' #WORK-RECORD.CNTRCT-NBR '///Employee' #WORK-RECORD.EMPL-RACF-ID
            		pnd_Work_Record_Tiaa_Rcvd_Dte, new ReportEditMask ("MM/DD/YY"),"//Doc/Rec'd",
            		pnd_Misc_Parm_Pnd_Return_Doc_Txt,"//Rec'd In/Unit",
            		pnd_Misc_Parm_Pnd_Return_Rcvd_Txt,"Bsnss/Days/in/Unit",
            		pnd_Work_Record_Pnd_Bsnss_Days_In_Unit, new ReportEditMask ("Z9.9"),"//Work/Process",
            		pnd_Misc_Parm_Pnd_Wpid_Sname,"//Work/Step",
            		pnd_Work_Record_Step_Id,"///Status",
            		pnd_Misc_Parm_Pnd_Status_Sname, new AlphanumericLength (15),"//Status/Date",
            		pnd_Work_Record_Admin_Status_Updte_Dte_Tme, new ReportEditMask ("MM/DD/YY"),"/Days/Ext/Pend",
            		pnd_Work_Record_Pnd_Extrnl_Pend_Days, new ReportEditMask ("ZZZ9.9"),"Clndr/Days/in/TIAA",
            		pnd_Work_Record_Pnd_Calendar_Days_In_Tiaa, new ReportEditMask ("ZZZ9"),"Bsnss/Days/in/TIAA",
            		pnd_Work_Record_Pnd_Business_Days_In_Tiaa, new ReportEditMask ("ZZZ9"),"//Partic/Name",
            		pnd_Work_Record_Partic_Sname,"///PIN",
            		pnd_Work_Record_Pin_Nbr, new ReportEditMask ("999999999999"),"//Contract/Number",
            		pnd_Work_Record_Cntrct_Nbr,"///Employee",
            		pnd_Work_Record_Empl_Racf_Id);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    '///PIN' #WORK-RECORD.PIN-NBR (EM=9999999)
            //*                                                                                                                                                           //Natural: AT BREAK OF #WORK-RECORD.TBL-CLASS
            //*                                                                                                                                                           //Natural: AT BREAK OF #WORK-RECORD.ADMIN-UNIT-CDE
            //*  READ-2.
            sort01Tbl_ClassOld.setValue(pnd_Work_Record_Tbl_Class);                                                                                                       //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Env,new TabSetting(52),"CORPORATE WORKFLOW FACILITIES",new TabSetting(124),"Page",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 52T 'CORPORATE WORKFLOW FACILITIES' 124T 'Page' *PAGE-NUMBER ( 1 ) ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 50T 'FOLLOW-UP REPORT OF PENDED CASES' 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(50),"FOLLOW-UP REPORT OF PENDED CASES",new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,"UNIT      :",pnd_Work_Record_Admin_Unit_Cde,pnd_Unit_Name);                                               //Natural: WRITE ( 1 ) 'UNIT      :' #WORK-RECORD.ADMIN-UNIT-CDE #UNIT-NAME
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Work_Record_Tbl_ClassIsBreak = pnd_Work_Record_Tbl_Class.isBreak(endOfData);
        boolean pnd_Work_Record_Admin_Unit_CdeIsBreak = pnd_Work_Record_Admin_Unit_Cde.isBreak(endOfData);
        if (condition(pnd_Work_Record_Tbl_ClassIsBreak || pnd_Work_Record_Admin_Unit_CdeIsBreak))
        {
            pnd_Misc_Parm_Pnd_Wpid_Action.setValue(sort01Tbl_ClassOld);                                                                                                   //Natural: MOVE OLD ( #WORK-RECORD.TBL-CLASS ) TO #WPID-ACTION
            short decideConditionsMet267 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'A'
            if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("A"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 0 - 29 Days ...................:",pnd_Misc_Parm_Pnd_Ctr.getValue(1),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 0 - 29 Days ...................:' #CTR ( 1 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("B"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 30 - 59 Days ..................:",pnd_Misc_Parm_Pnd_Ctr.getValue(2),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 30 - 59 Days ..................:' #CTR ( 2 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("C"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 60 - 89 Days ..................:",pnd_Misc_Parm_Pnd_Ctr.getValue(3),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 60 - 89 Days ..................:' #CTR ( 3 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("D"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 90 - 364 Days .................:",pnd_Misc_Parm_Pnd_Ctr.getValue(4),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 90 - 364 Days .................:' #CTR ( 4 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'E'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("E"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 365 Days - 2 YEARS (Less 1 Day):",pnd_Misc_Parm_Pnd_Ctr.getValue(5),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 365 Days - 2 YEARS (Less 1 Day):' #CTR ( 5 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("F"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 2 YEARS - 3 YEARS  (Less 1 Day):",pnd_Misc_Parm_Pnd_Ctr.getValue(6),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 2 YEARS - 3 YEARS  (Less 1 Day):' #CTR ( 6 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'G'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("G"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 3 YEARS - 4 YEARS  (Less 1 Day):",pnd_Misc_Parm_Pnd_Ctr.getValue(7),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 3 YEARS - 4 YEARS  (Less 1 Day):' #CTR ( 7 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'H'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("H"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 4 YEARS - 5 YEARS  (Less 1 Day):",pnd_Misc_Parm_Pnd_Ctr.getValue(8),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 4 YEARS - 5 YEARS  (Less 1 Day):' #CTR ( 8 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("I"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 5 YEARS - 6 YEARS  (Less 1 Day):",pnd_Misc_Parm_Pnd_Ctr.getValue(9),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 5 YEARS - 6 YEARS  (Less 1 Day):' #CTR ( 9 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'J'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("J"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 6 YEARS - 7 YEARS  (Less 1 Day):",pnd_Misc_Parm_Pnd_Ctr.getValue(10),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 6 YEARS - 7 YEARS  (Less 1 Day):' #CTR ( 10 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'K'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("K"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 7 YEARS - 8 YEARS  (Less 1 Day):",pnd_Misc_Parm_Pnd_Ctr.getValue(11),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 7 YEARS - 8 YEARS  (Less 1 Day):' #CTR ( 11 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("L"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 8 YEARS - 9 YEARS  (Less 1 Day):",pnd_Misc_Parm_Pnd_Ctr.getValue(12),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 8 YEARS - 9 YEARS  (Less 1 Day):' #CTR ( 12 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'M'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("M"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 9 YEARS - 10 YEARS (Less 1 Day):",pnd_Misc_Parm_Pnd_Ctr.getValue(13),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 9 YEARS - 10 YEARS (Less 1 Day):' #CTR ( 13 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("N"))))
            {
                decideConditionsMet267++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Work Requests Pended 10 YEARS OR MORE...............:",pnd_Misc_Parm_Pnd_Ctr.getValue(14),"(Calendar Days)",  //Natural: WRITE ( 1 ) / 10T 'Work Requests Pended 10 YEARS OR MORE...............:' #CTR ( 14 ) '(Calendar Days)' ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue("*").reset();                                                                                                          //Natural: RESET #BOOKLET-CTR ( * ) #FORMS-CTR ( * ) #INQUIRE-CTR ( * ) #RESEARCH-CTR ( * ) #SUB-COUNT
            pnd_Misc_Parm_Pnd_Forms_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Research_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Sub_Count.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Work_Record_Admin_Unit_CdeIsBreak))
        {
            getReports().write(0, " ");                                                                                                                                   //Natural: WRITE ' '
            if (condition(Global.isEscape())) return;
            pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue("*").reset();                                                                                                          //Natural: RESET #BOOKLET-CTR ( * ) #FORMS-CTR ( * ) #INQUIRE-CTR ( * ) #RESEARCH-CTR ( * ) #SUB-COUNT #CTR ( * )
            pnd_Misc_Parm_Pnd_Forms_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Research_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Sub_Count.reset();
            pnd_Misc_Parm_Pnd_Ctr.getValue("*").reset();
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())) return;
            pnd_New_Unit.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-UNIT
            pnd_Save_Unit.setValue(pnd_Work_Record_Admin_Unit_Cde);                                                                                                       //Natural: MOVE #WORK-RECORD.ADMIN-UNIT-CDE TO #SAVE-UNIT
            DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Work_Record_Admin_Unit_Cde, pnd_Unit_Name);                                                    //Natural: CALLNAT 'CWFN1103' #WORK-RECORD.ADMIN-UNIT-CDE #UNIT-NAME
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=142 PS=58");

        getReports().setDisplayColumns(1, "//Received/At TIAA",
        		pnd_Work_Record_Tiaa_Rcvd_Dte, new ReportEditMask ("MM/DD/YY"),"//Doc/Rec'd",
        		pnd_Misc_Parm_Pnd_Return_Doc_Txt,"//Rec'd In/Unit",
        		pnd_Misc_Parm_Pnd_Return_Rcvd_Txt,"Bsnss/Days/in/Unit",
        		pnd_Work_Record_Pnd_Bsnss_Days_In_Unit, new ReportEditMask ("Z9.9"),"//Work/Process",
        		pnd_Misc_Parm_Pnd_Wpid_Sname,"//Work/Step",
        		pnd_Work_Record_Step_Id,"///Status",
        		pnd_Misc_Parm_Pnd_Status_Sname, new AlphanumericLength (15),"//Status/Date",
        		pnd_Work_Record_Admin_Status_Updte_Dte_Tme, new ReportEditMask ("MM/DD/YY"),"/Days/Ext/Pend",
        		pnd_Work_Record_Pnd_Extrnl_Pend_Days, new ReportEditMask ("ZZZ9.9"),"Clndr/Days/in/TIAA",
        		pnd_Work_Record_Pnd_Calendar_Days_In_Tiaa, new ReportEditMask ("ZZZ9"),"Bsnss/Days/in/TIAA",
        		pnd_Work_Record_Pnd_Business_Days_In_Tiaa, new ReportEditMask ("ZZZ9"),"//Partic/Name",
        		pnd_Work_Record_Partic_Sname,"///PIN",
        		pnd_Work_Record_Pin_Nbr, new ReportEditMask ("999999999999"),"//Contract/Number",
        		pnd_Work_Record_Cntrct_Nbr,"///Employee",
        		pnd_Work_Record_Empl_Racf_Id);
    }
}
