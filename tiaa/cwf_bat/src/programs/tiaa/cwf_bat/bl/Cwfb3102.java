/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:28:09 PM
**        * FROM NATURAL PROGRAM : Cwfb3102
************************************************************
**        * FILE NAME            : Cwfb3102.java
**        * CLASS NAME           : Cwfb3102
**        * INSTANCE NAME        : Cwfb3102
************************************************************
***********************************************************************
** PROGRAM NAME........:CWFB3102                                     **
** PROGRAM DESCRIPTION.:THIS PROGRAM REPORTS ON UNKNOWN CASES WHERE  **
**                      THE WPID INCLUDES A 'U' ELEMENT.             **
** AUTHOR..............:J.S.PATINGO                                  **
** PROGRAM DATE CREATED:10/06/1993                                   **
** PROGRAM MODIFICATION:                                             **
**      CLONED FROM CWFB3002                                         **
***********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3102 extends BLNatBase
{
    // Data Areas
    private PdaCwfa2110 pdaCwfa2110;
    private PdaCwfa2101 pdaCwfa2101;
    private PdaCdaobj pdaCdaobj;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Generic_Tbl;
    private DbsField cwf_Generic_Tbl_Tbl_Data_Field;
    private DbsField cwf_Generic_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Generic_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Generic_Tbl_Tbl_Key_Field;

    private DataAccessProgramView vw_cwf_12_Master_Index_View;
    private DbsField cwf_12_Master_Index_View_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_12_Master_Index_View__R_Field_1;
    private DbsField cwf_12_Master_Index_View_Rqst_Log_Index_Dte;
    private DbsField cwf_12_Master_Index_View_Work_Prcss_Id;

    private DbsGroup cwf_12_Master_Index_View__R_Field_2;
    private DbsField cwf_12_Master_Index_View_Work_Prcss_Id_Num;

    private DbsGroup cwf_12_Master_Index_View__R_Field_3;
    private DbsField cwf_12_Master_Index_View_Wpid_1;
    private DbsField cwf_12_Master_Index_View_Wpid_2;
    private DbsField cwf_12_Master_Index_View_Wpid_4;
    private DbsField cwf_12_Master_Index_View_Wpid_5;
    private DbsField cwf_12_Master_Index_View_Orgnl_Unit_Cde;
    private DbsField cwf_12_Master_Index_View_Admin_Unit_Cde;
    private DbsField cwf_12_Master_Index_View_Last_Chnge_Unit_Cde;
    private DbsField cwf_12_Master_Index_View_Rqst_Log_Oprtr_Cde;

    private DataAccessProgramView vw_cwf_12_Master_Index_2;
    private DbsField cwf_12_Master_Index_2_Work_Prcss_Id;

    private DbsGroup cwf_12_Master_Index_2__R_Field_4;
    private DbsField cwf_12_Master_Index_2_Work_Prcss_Id_Num;

    private DbsGroup cwf_12_Master_Index_2__R_Field_5;
    private DbsField cwf_12_Master_Index_2_Wpid_1;
    private DbsField cwf_12_Master_Index_2_Wpid_2;
    private DbsField cwf_12_Master_Index_2_Wpid_4;
    private DbsField cwf_12_Master_Index_2_Wpid_5;
    private DbsField cwf_12_Master_Index_2_Last_Chnge_Dte_Tme;

    private DbsGroup cwf_12_Master_Index_2__R_Field_6;
    private DbsField cwf_12_Master_Index_2_Rqst_Yyyy;
    private DbsField cwf_12_Master_Index_2_Rqst_Mm;
    private DbsField cwf_12_Master_Index_2_Rqst_Dd;
    private DbsField cwf_12_Master_Index_2_Rqst_Log_Dte_Tme;
    private DbsField cwf_12_Master_Index_2_Rqst_Log_Oprtr_Cde;
    private DbsField pnd_Record_Count;
    private DbsField pnd_Record_Count2;
    private DbsField hold_Mjh_Req_No;
    private DbsField hold_Work_Prcss_Id;
    private DbsField hold_Work_Prcss_Id_Desc;
    private DbsField hold_Rs_Req_No;
    private DbsField hold_Date;
    private DbsField pnd_Work_Key;

    private DbsGroup pnd_Work_Key__R_Field_7;
    private DbsField pnd_Work_Key_Pnd_Work_Fill1;
    private DbsField pnd_Work_Key_Pnd_Work_Route;
    private DbsField pnd_Work_Key_Pnd_Work_Fill2;
    private DbsField pnd_Route_Desc;
    private DbsField pnd_Reroute_Desc;
    private DbsField pnd_Work_Procss_Desc;
    private DbsField pnd_Unit_Desc;
    private DbsField pnd_Hold_Unit;
    private DbsField pnd_Shrt_Nme;
    private DbsField pnd_Shrt_Nme2;
    private DbsField pnd_Pass_Desc;
    private DbsField pnd_Pass_Desc2;
    private DbsField pnd_Status_Desc;
    private DbsField pnd_Pda_Key;

    private DbsGroup pnd_Pda_Key__R_Field_8;
    private DbsField pnd_Pda_Key_Pnd_Pda_Unit;
    private DbsField pnd_Pda_Key_Pnd_Pda_Status;
    private DbsField pnd_Date;
    private DbsField pnd_U;
    private DbsField pnd_Nbr;
    private DbsField pnd_Rqst_Routing_Key;

    private DbsGroup pnd_Rqst_Routing_Key__R_Field_9;
    private DbsField pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Disp_Wpid;
    private DbsField pnd_Cnt;
    private DbsField pnd_Total_Cnt;
    private DbsField pnd_Work1;

    private DbsGroup pnd_Work1__R_Field_10;
    private DbsField pnd_Work1_Wk_Work_Prcss_Id;
    private DbsField pnd_Work1_Wk_Shrt_Nme;
    private DbsField pnd_Work1_Wk_Disp_Wpid;
    private DbsField pnd_Work1_Wk_Shrt_Nme2;
    private DbsField pnd_Work1_Wk_Orgnl_Unit_Cde;
    private DbsField pnd_Work1_Wk_Empl_Oprtr;
    private DbsField pnd_Log_Dte_Tme;

    private DbsGroup pnd_Log_Dte_Tme__R_Field_11;
    private DbsField pnd_Log_Dte_Tme_Pnd_Log_Dte;
    private DbsField pnd_Log_Dte_Tme_Pnd_Log_Tme;
    private DbsField pnd_Display_Date;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Env;
    private DbsField pnd_Start_Date;

    private DbsGroup pnd_Start_Date__R_Field_12;
    private DbsField pnd_Start_Date_Pnd_Input_St_Dte;

    private DbsGroup pnd_Start_Date__R_Field_13;
    private DbsField pnd_Start_Date_Pnd_Input_Yyyy;
    private DbsField pnd_Start_Date_Pnd_Input_Mm;
    private DbsField pnd_Start_Date_Pnd_Input_Dd;
    private DbsField pnd_End_Date;

    private DbsGroup pnd_End_Date__R_Field_14;
    private DbsField pnd_End_Date_Pnd_Input_End_Dte;

    private DbsGroup pnd_End_Date__R_Field_15;
    private DbsField pnd_End_Date_Pnd_Input_End_Yyyy;
    private DbsField pnd_End_Date_Pnd_Input_End_Mm;
    private DbsField pnd_End_Date_Pnd_Input_End_Dd;
    private DbsField pnd_Work_Start_Date;
    private DbsField pnd_Work_Comp_Date;
    private DbsField pnd_Date_Diff;
    private DbsField pnd_Page_No;
    private DbsField pnd_Report_No;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Parm_Type;
    private DbsField pnd_Rep_Unit_Cde;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Wk_Work_Prcss_IdOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa2110 = new PdaCwfa2110(localVariables);
        pdaCwfa2101 = new PdaCwfa2101(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);

        // Local Variables

        vw_cwf_Generic_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Generic_Tbl", "CWF-GENERIC-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Generic_Tbl_Tbl_Data_Field = vw_cwf_Generic_Tbl.getRecord().newFieldInGroup("cwf_Generic_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        cwf_Generic_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Generic_Tbl.getRecord().newFieldInGroup("cwf_Generic_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Generic_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Generic_Tbl_Tbl_Table_Nme = vw_cwf_Generic_Tbl.getRecord().newFieldInGroup("cwf_Generic_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Generic_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Generic_Tbl_Tbl_Key_Field = vw_cwf_Generic_Tbl.getRecord().newFieldInGroup("cwf_Generic_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        registerRecord(vw_cwf_Generic_Tbl);

        vw_cwf_12_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_12_Master_Index_View", "CWF-12-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_12_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_12_Master_Index_View.getRecord().newFieldInGroup("cwf_12_Master_Index_View_Rqst_Log_Dte_Tme", 
            "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_12_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_12_Master_Index_View__R_Field_1 = vw_cwf_12_Master_Index_View.getRecord().newGroupInGroup("cwf_12_Master_Index_View__R_Field_1", "REDEFINE", 
            cwf_12_Master_Index_View_Rqst_Log_Dte_Tme);
        cwf_12_Master_Index_View_Rqst_Log_Index_Dte = cwf_12_Master_Index_View__R_Field_1.newFieldInGroup("cwf_12_Master_Index_View_Rqst_Log_Index_Dte", 
            "RQST-LOG-INDEX-DTE", FieldType.STRING, 8);
        cwf_12_Master_Index_View_Work_Prcss_Id = vw_cwf_12_Master_Index_View.getRecord().newFieldInGroup("cwf_12_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_12_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");

        cwf_12_Master_Index_View__R_Field_2 = vw_cwf_12_Master_Index_View.getRecord().newGroupInGroup("cwf_12_Master_Index_View__R_Field_2", "REDEFINE", 
            cwf_12_Master_Index_View_Work_Prcss_Id);
        cwf_12_Master_Index_View_Work_Prcss_Id_Num = cwf_12_Master_Index_View__R_Field_2.newFieldInGroup("cwf_12_Master_Index_View_Work_Prcss_Id_Num", 
            "WORK-PRCSS-ID-NUM", FieldType.STRING, 4);

        cwf_12_Master_Index_View__R_Field_3 = vw_cwf_12_Master_Index_View.getRecord().newGroupInGroup("cwf_12_Master_Index_View__R_Field_3", "REDEFINE", 
            cwf_12_Master_Index_View_Work_Prcss_Id);
        cwf_12_Master_Index_View_Wpid_1 = cwf_12_Master_Index_View__R_Field_3.newFieldInGroup("cwf_12_Master_Index_View_Wpid_1", "WPID-1", FieldType.STRING, 
            1);
        cwf_12_Master_Index_View_Wpid_2 = cwf_12_Master_Index_View__R_Field_3.newFieldInGroup("cwf_12_Master_Index_View_Wpid_2", "WPID-2", FieldType.STRING, 
            2);
        cwf_12_Master_Index_View_Wpid_4 = cwf_12_Master_Index_View__R_Field_3.newFieldInGroup("cwf_12_Master_Index_View_Wpid_4", "WPID-4", FieldType.STRING, 
            1);
        cwf_12_Master_Index_View_Wpid_5 = cwf_12_Master_Index_View__R_Field_3.newFieldInGroup("cwf_12_Master_Index_View_Wpid_5", "WPID-5", FieldType.STRING, 
            2);
        cwf_12_Master_Index_View_Orgnl_Unit_Cde = vw_cwf_12_Master_Index_View.getRecord().newFieldInGroup("cwf_12_Master_Index_View_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_12_Master_Index_View_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_12_Master_Index_View_Admin_Unit_Cde = vw_cwf_12_Master_Index_View.getRecord().newFieldInGroup("cwf_12_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_12_Master_Index_View_Last_Chnge_Unit_Cde = vw_cwf_12_Master_Index_View.getRecord().newFieldInGroup("cwf_12_Master_Index_View_Last_Chnge_Unit_Cde", 
            "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_12_Master_Index_View_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_12_Master_Index_View_Rqst_Log_Oprtr_Cde = vw_cwf_12_Master_Index_View.getRecord().newFieldInGroup("cwf_12_Master_Index_View_Rqst_Log_Oprtr_Cde", 
            "RQST-LOG-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_12_Master_Index_View_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        registerRecord(vw_cwf_12_Master_Index_View);

        vw_cwf_12_Master_Index_2 = new DataAccessProgramView(new NameInfo("vw_cwf_12_Master_Index_2", "CWF-12-MASTER-INDEX-2"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_12_Master_Index_2_Work_Prcss_Id = vw_cwf_12_Master_Index_2.getRecord().newFieldInGroup("cwf_12_Master_Index_2_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_12_Master_Index_2_Work_Prcss_Id.setDdmHeader("WORK/ID");

        cwf_12_Master_Index_2__R_Field_4 = vw_cwf_12_Master_Index_2.getRecord().newGroupInGroup("cwf_12_Master_Index_2__R_Field_4", "REDEFINE", cwf_12_Master_Index_2_Work_Prcss_Id);
        cwf_12_Master_Index_2_Work_Prcss_Id_Num = cwf_12_Master_Index_2__R_Field_4.newFieldInGroup("cwf_12_Master_Index_2_Work_Prcss_Id_Num", "WORK-PRCSS-ID-NUM", 
            FieldType.STRING, 4);

        cwf_12_Master_Index_2__R_Field_5 = vw_cwf_12_Master_Index_2.getRecord().newGroupInGroup("cwf_12_Master_Index_2__R_Field_5", "REDEFINE", cwf_12_Master_Index_2_Work_Prcss_Id);
        cwf_12_Master_Index_2_Wpid_1 = cwf_12_Master_Index_2__R_Field_5.newFieldInGroup("cwf_12_Master_Index_2_Wpid_1", "WPID-1", FieldType.STRING, 1);
        cwf_12_Master_Index_2_Wpid_2 = cwf_12_Master_Index_2__R_Field_5.newFieldInGroup("cwf_12_Master_Index_2_Wpid_2", "WPID-2", FieldType.STRING, 2);
        cwf_12_Master_Index_2_Wpid_4 = cwf_12_Master_Index_2__R_Field_5.newFieldInGroup("cwf_12_Master_Index_2_Wpid_4", "WPID-4", FieldType.STRING, 1);
        cwf_12_Master_Index_2_Wpid_5 = cwf_12_Master_Index_2__R_Field_5.newFieldInGroup("cwf_12_Master_Index_2_Wpid_5", "WPID-5", FieldType.STRING, 2);
        cwf_12_Master_Index_2_Last_Chnge_Dte_Tme = vw_cwf_12_Master_Index_2.getRecord().newFieldInGroup("cwf_12_Master_Index_2_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_12_Master_Index_2_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        cwf_12_Master_Index_2__R_Field_6 = vw_cwf_12_Master_Index_2.getRecord().newGroupInGroup("cwf_12_Master_Index_2__R_Field_6", "REDEFINE", cwf_12_Master_Index_2_Last_Chnge_Dte_Tme);
        cwf_12_Master_Index_2_Rqst_Yyyy = cwf_12_Master_Index_2__R_Field_6.newFieldInGroup("cwf_12_Master_Index_2_Rqst_Yyyy", "RQST-YYYY", FieldType.STRING, 
            4);
        cwf_12_Master_Index_2_Rqst_Mm = cwf_12_Master_Index_2__R_Field_6.newFieldInGroup("cwf_12_Master_Index_2_Rqst_Mm", "RQST-MM", FieldType.STRING, 
            2);
        cwf_12_Master_Index_2_Rqst_Dd = cwf_12_Master_Index_2__R_Field_6.newFieldInGroup("cwf_12_Master_Index_2_Rqst_Dd", "RQST-DD", FieldType.STRING, 
            2);
        cwf_12_Master_Index_2_Rqst_Log_Dte_Tme = vw_cwf_12_Master_Index_2.getRecord().newFieldInGroup("cwf_12_Master_Index_2_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_12_Master_Index_2_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_12_Master_Index_2_Rqst_Log_Oprtr_Cde = vw_cwf_12_Master_Index_2.getRecord().newFieldInGroup("cwf_12_Master_Index_2_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_12_Master_Index_2_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        registerRecord(vw_cwf_12_Master_Index_2);

        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Record_Count2 = localVariables.newFieldInRecord("pnd_Record_Count2", "#RECORD-COUNT2", FieldType.PACKED_DECIMAL, 9);
        hold_Mjh_Req_No = localVariables.newFieldInRecord("hold_Mjh_Req_No", "HOLD-MJH-REQ-NO", FieldType.NUMERIC, 6);
        hold_Work_Prcss_Id = localVariables.newFieldInRecord("hold_Work_Prcss_Id", "HOLD-WORK-PRCSS-ID", FieldType.STRING, 6);
        hold_Work_Prcss_Id_Desc = localVariables.newFieldInRecord("hold_Work_Prcss_Id_Desc", "HOLD-WORK-PRCSS-ID-DESC", FieldType.STRING, 15);
        hold_Rs_Req_No = localVariables.newFieldInRecord("hold_Rs_Req_No", "HOLD-RS-REQ-NO", FieldType.NUMERIC, 6);
        hold_Date = localVariables.newFieldInRecord("hold_Date", "HOLD-DATE", FieldType.STRING, 10);
        pnd_Work_Key = localVariables.newFieldInRecord("pnd_Work_Key", "#WORK-KEY", FieldType.STRING, 6);

        pnd_Work_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Work_Key__R_Field_7", "REDEFINE", pnd_Work_Key);
        pnd_Work_Key_Pnd_Work_Fill1 = pnd_Work_Key__R_Field_7.newFieldInGroup("pnd_Work_Key_Pnd_Work_Fill1", "#WORK-FILL1", FieldType.NUMERIC, 1);
        pnd_Work_Key_Pnd_Work_Route = pnd_Work_Key__R_Field_7.newFieldInGroup("pnd_Work_Key_Pnd_Work_Route", "#WORK-ROUTE", FieldType.STRING, 4);
        pnd_Work_Key_Pnd_Work_Fill2 = pnd_Work_Key__R_Field_7.newFieldInGroup("pnd_Work_Key_Pnd_Work_Fill2", "#WORK-FILL2", FieldType.NUMERIC, 1);
        pnd_Route_Desc = localVariables.newFieldInRecord("pnd_Route_Desc", "#ROUTE-DESC", FieldType.STRING, 42);
        pnd_Reroute_Desc = localVariables.newFieldInRecord("pnd_Reroute_Desc", "#REROUTE-DESC", FieldType.STRING, 42);
        pnd_Work_Procss_Desc = localVariables.newFieldInRecord("pnd_Work_Procss_Desc", "#WORK-PROCSS-DESC", FieldType.STRING, 25);
        pnd_Unit_Desc = localVariables.newFieldInRecord("pnd_Unit_Desc", "#UNIT-DESC", FieldType.STRING, 24);
        pnd_Hold_Unit = localVariables.newFieldInRecord("pnd_Hold_Unit", "#HOLD-UNIT", FieldType.STRING, 8);
        pnd_Shrt_Nme = localVariables.newFieldInRecord("pnd_Shrt_Nme", "#SHRT-NME", FieldType.STRING, 15);
        pnd_Shrt_Nme2 = localVariables.newFieldInRecord("pnd_Shrt_Nme2", "#SHRT-NME2", FieldType.STRING, 15);
        pnd_Pass_Desc = localVariables.newFieldInRecord("pnd_Pass_Desc", "#PASS-DESC", FieldType.STRING, 15);
        pnd_Pass_Desc2 = localVariables.newFieldInRecord("pnd_Pass_Desc2", "#PASS-DESC2", FieldType.STRING, 25);
        pnd_Status_Desc = localVariables.newFieldInRecord("pnd_Status_Desc", "#STATUS-DESC", FieldType.STRING, 32);
        pnd_Pda_Key = localVariables.newFieldInRecord("pnd_Pda_Key", "#PDA-KEY", FieldType.STRING, 12);

        pnd_Pda_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Pda_Key__R_Field_8", "REDEFINE", pnd_Pda_Key);
        pnd_Pda_Key_Pnd_Pda_Unit = pnd_Pda_Key__R_Field_8.newFieldInGroup("pnd_Pda_Key_Pnd_Pda_Unit", "#PDA-UNIT", FieldType.STRING, 8);
        pnd_Pda_Key_Pnd_Pda_Status = pnd_Pda_Key__R_Field_8.newFieldInGroup("pnd_Pda_Key_Pnd_Pda_Status", "#PDA-STATUS", FieldType.STRING, 4);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_U = localVariables.newFieldInRecord("pnd_U", "#U", FieldType.STRING, 1);
        pnd_Nbr = localVariables.newFieldInRecord("pnd_Nbr", "#NBR", FieldType.PACKED_DECIMAL, 3);
        pnd_Rqst_Routing_Key = localVariables.newFieldInRecord("pnd_Rqst_Routing_Key", "#RQST-ROUTING-KEY", FieldType.STRING, 30);

        pnd_Rqst_Routing_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Rqst_Routing_Key__R_Field_9", "REDEFINE", pnd_Rqst_Routing_Key);
        pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Rqst_Routing_Key__R_Field_9.newFieldInGroup("pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Disp_Wpid = localVariables.newFieldInRecord("pnd_Disp_Wpid", "#DISP-WPID", FieldType.STRING, 6);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Total_Cnt = localVariables.newFieldInRecord("pnd_Total_Cnt", "#TOTAL-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Work1 = localVariables.newFieldInRecord("pnd_Work1", "#WORK1", FieldType.STRING, 87);

        pnd_Work1__R_Field_10 = localVariables.newGroupInRecord("pnd_Work1__R_Field_10", "REDEFINE", pnd_Work1);
        pnd_Work1_Wk_Work_Prcss_Id = pnd_Work1__R_Field_10.newFieldInGroup("pnd_Work1_Wk_Work_Prcss_Id", "WK-WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Work1_Wk_Shrt_Nme = pnd_Work1__R_Field_10.newFieldInGroup("pnd_Work1_Wk_Shrt_Nme", "WK-SHRT-NME", FieldType.STRING, 15);
        pnd_Work1_Wk_Disp_Wpid = pnd_Work1__R_Field_10.newFieldInGroup("pnd_Work1_Wk_Disp_Wpid", "WK-DISP-WPID", FieldType.STRING, 6);
        pnd_Work1_Wk_Shrt_Nme2 = pnd_Work1__R_Field_10.newFieldInGroup("pnd_Work1_Wk_Shrt_Nme2", "WK-SHRT-NME2", FieldType.STRING, 15);
        pnd_Work1_Wk_Orgnl_Unit_Cde = pnd_Work1__R_Field_10.newFieldInGroup("pnd_Work1_Wk_Orgnl_Unit_Cde", "WK-ORGNL-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work1_Wk_Empl_Oprtr = pnd_Work1__R_Field_10.newFieldInGroup("pnd_Work1_Wk_Empl_Oprtr", "WK-EMPL-OPRTR", FieldType.STRING, 30);
        pnd_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_Log_Dte_Tme", "#LOG-DTE-TME", FieldType.STRING, 15);

        pnd_Log_Dte_Tme__R_Field_11 = localVariables.newGroupInRecord("pnd_Log_Dte_Tme__R_Field_11", "REDEFINE", pnd_Log_Dte_Tme);
        pnd_Log_Dte_Tme_Pnd_Log_Dte = pnd_Log_Dte_Tme__R_Field_11.newFieldInGroup("pnd_Log_Dte_Tme_Pnd_Log_Dte", "#LOG-DTE", FieldType.NUMERIC, 8);
        pnd_Log_Dte_Tme_Pnd_Log_Tme = pnd_Log_Dte_Tme__R_Field_11.newFieldInGroup("pnd_Log_Dte_Tme_Pnd_Log_Tme", "#LOG-TME", FieldType.NUMERIC, 7);
        pnd_Display_Date = localVariables.newFieldInRecord("pnd_Display_Date", "#DISPLAY-DATE", FieldType.STRING, 10);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Start_Date__R_Field_12 = localVariables.newGroupInRecord("pnd_Start_Date__R_Field_12", "REDEFINE", pnd_Start_Date);
        pnd_Start_Date_Pnd_Input_St_Dte = pnd_Start_Date__R_Field_12.newFieldInGroup("pnd_Start_Date_Pnd_Input_St_Dte", "#INPUT-ST-DTE", FieldType.NUMERIC, 
            8);

        pnd_Start_Date__R_Field_13 = pnd_Start_Date__R_Field_12.newGroupInGroup("pnd_Start_Date__R_Field_13", "REDEFINE", pnd_Start_Date_Pnd_Input_St_Dte);
        pnd_Start_Date_Pnd_Input_Yyyy = pnd_Start_Date__R_Field_13.newFieldInGroup("pnd_Start_Date_Pnd_Input_Yyyy", "#INPUT-YYYY", FieldType.STRING, 4);
        pnd_Start_Date_Pnd_Input_Mm = pnd_Start_Date__R_Field_13.newFieldInGroup("pnd_Start_Date_Pnd_Input_Mm", "#INPUT-MM", FieldType.STRING, 2);
        pnd_Start_Date_Pnd_Input_Dd = pnd_Start_Date__R_Field_13.newFieldInGroup("pnd_Start_Date_Pnd_Input_Dd", "#INPUT-DD", FieldType.STRING, 2);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_End_Date__R_Field_14 = localVariables.newGroupInRecord("pnd_End_Date__R_Field_14", "REDEFINE", pnd_End_Date);
        pnd_End_Date_Pnd_Input_End_Dte = pnd_End_Date__R_Field_14.newFieldInGroup("pnd_End_Date_Pnd_Input_End_Dte", "#INPUT-END-DTE", FieldType.NUMERIC, 
            8);

        pnd_End_Date__R_Field_15 = pnd_End_Date__R_Field_14.newGroupInGroup("pnd_End_Date__R_Field_15", "REDEFINE", pnd_End_Date_Pnd_Input_End_Dte);
        pnd_End_Date_Pnd_Input_End_Yyyy = pnd_End_Date__R_Field_15.newFieldInGroup("pnd_End_Date_Pnd_Input_End_Yyyy", "#INPUT-END-YYYY", FieldType.STRING, 
            4);
        pnd_End_Date_Pnd_Input_End_Mm = pnd_End_Date__R_Field_15.newFieldInGroup("pnd_End_Date_Pnd_Input_End_Mm", "#INPUT-END-MM", FieldType.STRING, 2);
        pnd_End_Date_Pnd_Input_End_Dd = pnd_End_Date__R_Field_15.newFieldInGroup("pnd_End_Date_Pnd_Input_End_Dd", "#INPUT-END-DD", FieldType.STRING, 2);
        pnd_Work_Start_Date = localVariables.newFieldInRecord("pnd_Work_Start_Date", "#WORK-START-DATE", FieldType.DATE);
        pnd_Work_Comp_Date = localVariables.newFieldInRecord("pnd_Work_Comp_Date", "#WORK-COMP-DATE", FieldType.DATE);
        pnd_Date_Diff = localVariables.newFieldInRecord("pnd_Date_Diff", "#DATE-DIFF", FieldType.NUMERIC, 3);
        pnd_Page_No = localVariables.newFieldInRecord("pnd_Page_No", "#PAGE-NO", FieldType.NUMERIC, 5);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        pnd_Rep_Unit_Cde = localVariables.newFieldInRecord("pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Wk_Work_Prcss_IdOld = internalLoopRecord.newFieldInRecord("Sort01_Wk_Work_Prcss_Id_OLD", "Wk_Work_Prcss_Id_OLD", FieldType.STRING, 6);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Generic_Tbl.reset();
        vw_cwf_12_Master_Index_View.reset();
        vw_cwf_12_Master_Index_2.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Work_Key.setInitialValue("000000");
        pnd_Report_No.setInitialValue(2);
        pnd_Report_Parm.setInitialValue("CWFB3002W*");
        pnd_Parm_Type.setInitialValue("D");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3102() throws Exception
    {
        super("Cwfb3102");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB3102", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* ***********************************************************8
        //* *********************************                                                                                                                             //Natural: FORMAT ( 01 ) LS = 140 PS = 58
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        DbsUtil.callnat(Cwfn3913.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off, pnd_Start_Date,        //Natural: CALLNAT 'CWFN3913' #REPORT-PARM #RACF-ID #PARM-UNIT #FLOOR #BLDG #DROP-OFF #START-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.greater(pnd_Start_Date)))                                                                                                             //Natural: IF #COMP-DATE GT #START-DATE
        {
                                                                                                                                                                          //Natural: PERFORM GET-START-DATE
            sub_Get_Start_Date();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Rep_Unit_Cde.setValue(pnd_Parm_Unit);                                                                                                                         //Natural: MOVE #PARM-UNIT TO #REP-UNIT-CDE
        pnd_End_Date.setValue(pnd_Comp_Date);                                                                                                                             //Natural: MOVE #COMP-DATE TO #END-DATE
        pnd_Display_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Start_Date_Pnd_Input_Mm, "/", pnd_Start_Date_Pnd_Input_Dd, "/",                     //Natural: COMPRESS #INPUT-MM '/' #INPUT-DD '/' #INPUT-YYYY INTO #DISPLAY-DATE LEAVING NO SPACE
            pnd_Start_Date_Pnd_Input_Yyyy));
        pnd_U.setValue("U");                                                                                                                                              //Natural: ASSIGN #U = 'U'
        pnd_Bldg.setValue("W");                                                                                                                                           //Natural: ASSIGN #BLDG = 'W'
        DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pnd_Rep_Unit_Cde, pnd_Floor, pnd_Bldg, pnd_Drop_Off, pnd_Comp_Date);       //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID #REP-UNIT-CDE #FLOOR #BLDG #DROP-OFF #COMP-DATE
        if (condition(Global.isEscape())) return;
        vw_cwf_12_Master_Index_View.startDatabaseRead                                                                                                                     //Natural: READ CWF-12-MASTER-INDEX-VIEW BY RQST-ROUTING-KEY FROM #START-DATE WHERE CWF-12-MASTER-INDEX-VIEW.WPID-1 = 'U' OR CWF-12-MASTER-INDEX-VIEW.WPID-2 = 'U' OR CWF-12-MASTER-INDEX-VIEW.WPID-4 = 'U' OR CWF-12-MASTER-INDEX-VIEW.WPID-5 = 'U'
        (
        "READ1",
        new Wc[] { new Wc("SUBSTRING(WORK_PRCSS_ID,1,1) = 'U' OR SUBSTRING(WORK_PRCSS_ID,2,2) = 'U' OR SUBSTRING(WORK_PRCSS_ID,4,1) = 'U' OR SUBSTRING(WORK_PRCSS_ID,5,2) = 'U'  AND 1 ", 
            "=" , "1", WcType.OTHER) ,
        new Wc("RQST_ROUTING_KEY", ">=", pnd_Start_Date, WcType.BY) },
        new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") }
        );
        READ1:
        while (condition(vw_cwf_12_Master_Index_View.readNextRow("READ1")))
        {
            if (condition(cwf_12_Master_Index_View_Rqst_Log_Index_Dte.greater(pnd_End_Date)))                                                                             //Natural: IF CWF-12-MASTER-INDEX-VIEW.RQST-LOG-INDEX-DTE GT #END-DATE
            {
                if (true) break READ1;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(cwf_12_Master_Index_View_Orgnl_Unit_Cde.equals("CRC") && cwf_12_Master_Index_View_Last_Chnge_Unit_Cde.equals("CRC"))))                        //Natural: ACCEPT IF CWF-12-MASTER-INDEX-VIEW.ORGNL-UNIT-CDE EQ 'CRC' AND CWF-12-MASTER-INDEX-VIEW.LAST-CHNGE-UNIT-CDE EQ 'CRC'
            {
                continue;
            }
            if (condition(cwf_12_Master_Index_View_Wpid_1.equals("U") || cwf_12_Master_Index_View_Wpid_2.equals("U") || cwf_12_Master_Index_View_Wpid_4.equals("U")       //Natural: IF CWF-12-MASTER-INDEX-VIEW.WPID-1 = 'U' OR CWF-12-MASTER-INDEX-VIEW.WPID-2 = 'U' OR CWF-12-MASTER-INDEX-VIEW.WPID-4 = 'U' OR CWF-12-MASTER-INDEX-VIEW.WPID-5 = 'U'
                || cwf_12_Master_Index_View_Wpid_5.equals("U")))
            {
                DbsUtil.callnat(Cwfn5390.class , getCurrentProcessState(), cwf_12_Master_Index_View_Work_Prcss_Id, pnd_Shrt_Nme);                                         //Natural: CALLNAT 'CWFN5390' CWF-12-MASTER-INDEX-VIEW.WORK-PRCSS-ID #SHRT-NME
                if (condition(Global.isEscape())) return;
                pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme.setValue(cwf_12_Master_Index_View_Rqst_Log_Dte_Tme);                                                            //Natural: MOVE CWF-12-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO #RQST-LOG-DTE-TME
                pnd_Work1_Wk_Work_Prcss_Id.setValue(cwf_12_Master_Index_View_Work_Prcss_Id);                                                                              //Natural: MOVE CWF-12-MASTER-INDEX-VIEW.WORK-PRCSS-ID TO WK-WORK-PRCSS-ID
                pnd_Work1_Wk_Shrt_Nme.setValue(pnd_Shrt_Nme);                                                                                                             //Natural: MOVE #SHRT-NME TO WK-SHRT-NME
                vw_cwf_12_Master_Index_2.startDatabaseRead                                                                                                                //Natural: READ CWF-12-MASTER-INDEX-2 BY RQST-ROUTING-KEY FROM #RQST-ROUTING-KEY
                (
                "READ2",
                new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Rqst_Routing_Key, WcType.BY) },
                new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") }
                );
                READ2:
                while (condition(vw_cwf_12_Master_Index_2.readNextRow("READ2")))
                {
                    if (condition(cwf_12_Master_Index_2_Rqst_Log_Dte_Tme.notEquals(pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme)))                                           //Natural: IF CWF-12-MASTER-INDEX-2.RQST-LOG-DTE-TME NE #RQST-LOG-DTE-TME
                    {
                        if (true) break READ2;                                                                                                                            //Natural: ESCAPE BOTTOM ( READ2. )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(! (cwf_12_Master_Index_2_Wpid_1.equals("U") || cwf_12_Master_Index_2_Wpid_2.equals("U") || cwf_12_Master_Index_2_Wpid_4.equals("U")     //Natural: IF NOT ( CWF-12-MASTER-INDEX-2.WPID-1 = 'U' OR CWF-12-MASTER-INDEX-2.WPID-2 = 'U' OR CWF-12-MASTER-INDEX-2.WPID-4 = 'U' OR CWF-12-MASTER-INDEX-2.WPID-5 = 'U' )
                        || cwf_12_Master_Index_2_Wpid_5.equals("U"))))
                    {
                        DbsUtil.callnat(Cwfn5390.class , getCurrentProcessState(), cwf_12_Master_Index_2_Work_Prcss_Id, pnd_Shrt_Nme2);                                   //Natural: CALLNAT 'CWFN5390' CWF-12-MASTER-INDEX-2.WORK-PRCSS-ID #SHRT-NME2
                        if (condition(Global.isEscape())) return;
                        pnd_Disp_Wpid.setValue(cwf_12_Master_Index_2_Work_Prcss_Id);                                                                                      //Natural: MOVE CWF-12-MASTER-INDEX-2.WORK-PRCSS-ID TO #DISP-WPID
                        if (true) break READ2;                                                                                                                            //Natural: ESCAPE BOTTOM ( READ2. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Work1_Wk_Disp_Wpid.setValue(pnd_Disp_Wpid);                                                                                                           //Natural: MOVE #DISP-WPID TO WK-DISP-WPID
                pnd_Work1_Wk_Shrt_Nme2.setValue(pnd_Shrt_Nme2);                                                                                                           //Natural: MOVE #SHRT-NME2 TO WK-SHRT-NME2
                pnd_Work1_Wk_Orgnl_Unit_Cde.setValue(cwf_12_Master_Index_View_Admin_Unit_Cde);                                                                            //Natural: MOVE CWF-12-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE TO WK-ORGNL-UNIT-CDE
                DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), cwf_12_Master_Index_View_Rqst_Log_Oprtr_Cde, pnd_Work1_Wk_Empl_Oprtr);                         //Natural: CALLNAT 'CWFN1107' CWF-12-MASTER-INDEX-VIEW.RQST-LOG-OPRTR-CDE WK-EMPL-OPRTR
                if (condition(Global.isEscape())) return;
                getWorkFiles().write(1, false, pnd_Work1);                                                                                                                //Natural: WRITE WORK FILE 1 #WORK1
                pnd_Work1.reset();                                                                                                                                        //Natural: RESET #WORK1 #WORK-PROCSS-DESC #DISP-WPID #SHRT-NME2 #SHRT-NME
                pnd_Work_Procss_Desc.reset();
                pnd_Disp_Wpid.reset();
                pnd_Shrt_Nme2.reset();
                pnd_Shrt_Nme.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK1
        while (condition(getWorkFiles().read(1, pnd_Work1)))
        {
            getSort().writeSortInData(pnd_Work1_Wk_Work_Prcss_Id, pnd_Work1_Wk_Shrt_Nme, pnd_Work1_Wk_Orgnl_Unit_Cde, pnd_Work1_Wk_Shrt_Nme2, pnd_Work1_Wk_Disp_Wpid,     //Natural: END-ALL
                pnd_Work1_Wk_Empl_Oprtr);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Work1_Wk_Work_Prcss_Id);                                                                                                                   //Natural: SORT BY WK-WORK-PRCSS-ID USING WK-SHRT-NME WK-ORGNL-UNIT-CDE WK-SHRT-NME2 WK-DISP-WPID WK-EMPL-OPRTR
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Work1_Wk_Work_Prcss_Id, pnd_Work1_Wk_Shrt_Nme, pnd_Work1_Wk_Orgnl_Unit_Cde, pnd_Work1_Wk_Shrt_Nme2, 
            pnd_Work1_Wk_Disp_Wpid, pnd_Work1_Wk_Empl_Oprtr)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CNT
            getReports().display(1, "ORIGINALLY/ENTERED WPID",                                                                                                            //Natural: AT BREAK OF WK-WORK-PRCSS-ID;//Natural: DISPLAY ( 1 ) 'ORIGINALLY/ENTERED WPID' WK-WORK-PRCSS-ID '/WPID DESCRIPTION' WK-SHRT-NME 'MODIFIED/WPID' WK-DISP-WPID 'MODIFIED/DESCRIPTION' WK-SHRT-NME2 'ROUTED TO/UNIT' WK-ORGNL-UNIT-CDE 'LOGGED BY/EMPLOYEE' WK-EMPL-OPRTR
            		pnd_Work1_Wk_Work_Prcss_Id,"/WPID DESCRIPTION",
            		pnd_Work1_Wk_Shrt_Nme,"MODIFIED/WPID",
            		pnd_Work1_Wk_Disp_Wpid,"MODIFIED/DESCRIPTION",
            		pnd_Work1_Wk_Shrt_Nme2,"ROUTED TO/UNIT",
            		pnd_Work1_Wk_Orgnl_Unit_Cde,"LOGGED BY/EMPLOYEE",
            		pnd_Work1_Wk_Empl_Oprtr);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            sort01Wk_Work_Prcss_IdOld.setValue(pnd_Work1_Wk_Work_Prcss_Id);                                                                                               //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        if (condition(pnd_Total_Cnt.equals(getZero())))                                                                                                                   //Natural: IF #TOTAL-CNT = 0
        {
            DbsUtil.callnat(Cwfn3915.class , getCurrentProcessState(), pnd_Report_No);                                                                                    //Natural: CALLNAT 'CWFN3915' #REPORT-NO
            if (condition(Global.isEscape())) return;
            getReports().eject(1, true);                                                                                                                                  //Natural: EJECT ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(31),"    GRAND TOTAL.......:",pnd_Total_Cnt, new ReportEditMask                     //Natural: WRITE ( 1 ) NOTITLE // 31T '    GRAND TOTAL.......:' #TOTAL-CNT ( EM = ZZZ9 )
                ("ZZZ9"));
            if (Global.isEscape()) return;
            getReports().eject(1, true);                                                                                                                                  //Natural: EJECT ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tbl_Run_Flag.setValue("Y");                                                                                                                                   //Natural: MOVE 'Y' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Comp_Date, pnd_Tbl_Run_Flag);                                                     //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #COMP-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
        //* ***********************************************************************                                                                                       //Natural: AT TOP OF PAGE ( 1 )
        DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                       //Natural: CALLNAT 'CWFN3911'
        if (condition(Global.isEscape())) return;
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-START-DATE                                                                                                       //Natural: ON ERROR
    }
    private void sub_Get_Start_Date() throws Exception                                                                                                                    //Natural: GET-START-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Comp_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Comp_Date);                                                                                  //Natural: MOVE EDITED #COMP-DATE TO #WORK-COMP-DATE ( EM = YYYYMMDD )
        pnd_Work_Start_Date.setValue(pnd_Work_Comp_Date);                                                                                                                 //Natural: MOVE #WORK-COMP-DATE TO #WORK-START-DATE
        pnd_Work_Start_Date.nsubtract(6);                                                                                                                                 //Natural: COMPUTE #WORK-START-DATE = #WORK-START-DATE - 6
        pnd_Start_Date.setValueEdited(pnd_Work_Start_Date,new ReportEditMask("YYYYMMDD"));                                                                                //Natural: MOVE EDITED #WORK-START-DATE ( EM = YYYYMMDD ) TO #START-DATE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page_No.nadd(1);                                                                                                                                  //Natural: COMPUTE #PAGE-NO = #PAGE-NO + 1
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Env,new TabSetting(26),"CORPORATE WORKFLOW FACILITIES",new TabSetting(70),"PAGE",pnd_Page_No,  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 26T 'CORPORATE WORKFLOW FACILITIES' 70T 'PAGE' #PAGE-NO ( NL = 5 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 28T 'REPORT OF "UNKNOWN" CASES' 72T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (5), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(28),"REPORT OF 'UNKNOWN' CASES",new TabSetting(72),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(100));                                                                                  //Natural: WRITE ( 1 ) '=' ( 100 )
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Tbl_Run_Flag.setValue(" ");                                                                                                                                   //Natural: MOVE ' ' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Comp_Date, pnd_Tbl_Run_Flag);                                                     //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #COMP-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Work1_Wk_Work_Prcss_IdIsBreak = pnd_Work1_Wk_Work_Prcss_Id.isBreak(endOfData);
        if (condition(pnd_Work1_Wk_Work_Prcss_IdIsBreak))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(31),"TOTAL FOR WPID",sort01Wk_Work_Prcss_IdOld,":",pnd_Cnt, new                     //Natural: WRITE ( 1 ) // 31T 'TOTAL FOR WPID' OLD ( WK-WORK-PRCSS-ID ) ':' #CNT ( EM = ZZZ9 ) //
                ReportEditMask ("ZZZ9"),NEWLINE,NEWLINE);
            if (condition(Global.isEscape())) return;
            pnd_Total_Cnt.nadd(pnd_Cnt);                                                                                                                                  //Natural: COMPUTE #TOTAL-CNT = #TOTAL-CNT + #CNT
            pnd_Cnt.reset();                                                                                                                                              //Natural: RESET #CNT
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=140 PS=58");

        getReports().setDisplayColumns(1, "ORIGINALLY/ENTERED WPID",
        		pnd_Work1_Wk_Work_Prcss_Id,"/WPID DESCRIPTION",
        		pnd_Work1_Wk_Shrt_Nme,"MODIFIED/WPID",
        		pnd_Work1_Wk_Disp_Wpid,"MODIFIED/DESCRIPTION",
        		pnd_Work1_Wk_Shrt_Nme2,"ROUTED TO/UNIT",
        		pnd_Work1_Wk_Orgnl_Unit_Cde,"LOGGED BY/EMPLOYEE",
        		pnd_Work1_Wk_Empl_Oprtr);
    }
}
