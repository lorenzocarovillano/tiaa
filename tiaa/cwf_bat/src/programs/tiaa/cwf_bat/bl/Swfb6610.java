/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:24:03 PM
**        * FROM NATURAL PROGRAM : Swfb6610
************************************************************
**        * FILE NAME            : Swfb6610.java
**        * CLASS NAME           : Swfb6610
**        * INSTANCE NAME        : Swfb6610
************************************************************
************************************************************************
* PROGRAM  : SWFB6610
* SYSTEM   : SURVIVOR BENEFITS
* TITLE    : BATCH SPLIT PROCESS
* GENERATED: AUG 19,97 AT 10:37 AM BY C. SARMIENTO
* FUNCTION : THIS PROGRAM UPDATES THE SURVIVOR PIN FROM THE COR FILE.
*            IT ALSO COPIES DOCUMENTS FROM RECURRING PAYMENTS TO
*            A MISCELLANEOUS FOLDER FOR THE NEW PARTICIPANT (FORMERLY
*            THE SURVIVOR).
*
* HISTORY
* AUG 19,97 BY SARMIEC FOR RELEASE ____
* >
* >
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
* 07/29/2019 - COR/NAAD SUNSET
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Swfb6610 extends BLNatBase
{
    // Data Areas
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Sb_Work_Req;
    private DbsField cwf_Sb_Work_Req_Srvvr_Mit_Idntfr;

    private DbsGroup cwf_Sb_Work_Req__R_Field_1;
    private DbsField cwf_Sb_Work_Req_Ph_Pin_Nbr;
    private DbsField cwf_Sb_Work_Req_Srvvr_Mit_Sqnce;
    private DbsField cwf_Sb_Work_Req_Rqst_Log_Dte_Tme;
    private DbsField cwf_Sb_Work_Req_Split_Ind;
    private DbsField cwf_Sb_Work_Req_Last_Updte_Oprtr_Cde;

    private DataAccessProgramView vw_cwf_Sb_Work_Req_2;
    private DbsField pnd_Work_Req_Key;

    private DbsGroup pnd_Work_Req_Key__R_Field_2;
    private DbsField pnd_Work_Req_Key_Srvvr_Mit_Idntfr;
    private DbsField pnd_Work_Req_Key_Rqst_Log_Dte_Tme;

    private DataAccessProgramView vw_cwf_Sb_Bene;
    private DbsField cwf_Sb_Bene_Rcrd_Type;
    private DbsField cwf_Sb_Bene_Srvvr_Mit_Idntfr;

    private DbsGroup cwf_Sb_Bene__R_Field_3;
    private DbsField cwf_Sb_Bene_Ph_Pin_Nbr;
    private DbsField cwf_Sb_Bene_Srvvr_Mit_Sqnce;
    private DbsField cwf_Sb_Bene_Srvvr_Pin;
    private DbsField cwf_Sb_Bene_Srvvr_Ssn_Nbr;

    private DbsGroup cwf_Sb_Bene__R_Field_4;
    private DbsField cwf_Sb_Bene_Srvvr_Ssn_Nbr_N9;
    private DbsField cwf_Sb_Bene_Srvvr_Nme_Fr_Frm;
    private DbsField cwf_Sb_Bene_Last_Updte_Dte_Tme;
    private DbsField pnd_Srvvr_Key;

    private DbsGroup pnd_Srvvr_Key__R_Field_5;
    private DbsField pnd_Srvvr_Key_Rcrd_Type;
    private DbsField pnd_Srvvr_Key_Srvvr_Mit_Idntfr;

    private DataAccessProgramView vw_cwf_Master_1;
    private DbsField cwf_Master_1_Pin_Nbr;
    private DbsField cwf_Master_1_Crprte_Status_Ind;
    private DbsField cwf_Master_1_Status_Cde;
    private DbsField cwf_Master_1_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_1_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_1_Rqst_Id;

    private DbsGroup cwf_Master_1__R_Field_6;
    private DbsField cwf_Master_1_Rqst_Work_Prcss_Id;
    private DbsField cwf_Master_1_Rqst_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_1_Rqst_Case_Id;

    private DbsGroup cwf_Master_1__R_Field_7;
    private DbsField cwf_Master_1_Rqst_Case_Id_Ind;
    private DbsField cwf_Master_1_Rqst_Sub_Rqst_Cde;
    private DbsField cwf_Master_1_Rqst_Pin_Nbr;
    private DbsField cwf_Master_1_Multi_Rqst_Ind;
    private DbsField cwf_Master_1_Elctrnc_Fldr_Ind;

    private DataAccessProgramView vw_cwf_Master_2;
    private DbsField cwf_Master_2_Elctrnc_Fldr_Ind;

    private DataAccessProgramView vw_cwf_Efm_Folder;
    private DbsField cwf_Efm_Folder_Cabinet_Id;
    private DbsField cwf_Efm_Folder_Sub_Rqst_Work_Prcss_Id;

    private DbsGroup cwf_Efm_Folder_Folder_Id;
    private DbsField cwf_Efm_Folder_Tiaa_Rcvd_Dte;
    private DbsField cwf_Efm_Folder_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Efm_Folder__R_Field_8;
    private DbsField cwf_Efm_Folder_Case_Ind;
    private DbsField cwf_Efm_Folder_Originating_Wpid;
    private DbsField cwf_Efm_Folder_Multi_Rqst_Ind;
    private DbsField cwf_Efm_Folder_Sub_Rqst_Ind;
    private DbsField pnd_Work_Prcss_Key;

    private DbsGroup pnd_Work_Prcss_Key__R_Field_9;
    private DbsField pnd_Work_Prcss_Key_Cabinet_Id;

    private DbsGroup pnd_Work_Prcss_Key__R_Field_10;
    private DbsField pnd_Work_Prcss_Key_Cab_Prefix;
    private DbsField pnd_Work_Prcss_Key_Pin_Nbr;
    private DbsField pnd_Work_Prcss_Key_Sub_Rqst_Work_Prcss_Id;

    private DataAccessProgramView vw_cwf_Efm_Document;
    private DbsField cwf_Efm_Document_Document_Key;
    private DbsField cwf_Efm_Document_Cabinet_Id;

    private DbsGroup cwf_Efm_Document_Folder_Id;
    private DbsField cwf_Efm_Document_Tiaa_Rcvd_Dte;
    private DbsField cwf_Efm_Document_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Efm_Document__R_Field_11;
    private DbsField cwf_Efm_Document_Case_Ind;
    private DbsField cwf_Efm_Document_Originating_Wpid;
    private DbsField cwf_Efm_Document_Multi_Rqst_Ind;
    private DbsField cwf_Efm_Document_Sub_Rqst_Ind;
    private DbsField cwf_Efm_Document_Entry_Dte_Tme;
    private DbsField cwf_Efm_Document_Entry_Empl_Racf_Id;
    private DbsField cwf_Efm_Document_Entry_System_Or_Unit;
    private DbsField cwf_Efm_Document_Entry_Jobname;

    private DbsGroup cwf_Efm_Document_Document_Type;
    private DbsField cwf_Efm_Document_Document_Category;
    private DbsField cwf_Efm_Document_Document_Sub_Category;
    private DbsField cwf_Efm_Document_Document_Detail;

    private DbsGroup cwf_Efm_Document__R_Field_12;
    private DbsField cwf_Efm_Document_Doc_Type;
    private DbsField cwf_Efm_Document_Rcvd_Sent_Dte;
    private DbsField cwf_Efm_Document_Document_Direction_Ind;
    private DbsField cwf_Efm_Document_Secured_Document_Ind;
    private DbsField cwf_Efm_Document_Document_Retention_Ind;
    private DbsField cwf_Efm_Document_Important_Ind;
    private DbsField cwf_Efm_Document_Copy_Ind;
    private DbsField cwf_Efm_Document_Documents_Location;
    private DbsField cwf_Efm_Document_Sub_Rqst_Work_Prcss_Id;
    private DbsField cwf_Efm_Document_Sub_Rqst_Work_Prcss_Short_Nme;
    private DbsField cwf_Efm_Document_Business_Document_Description;
    private DbsField cwf_Efm_Document_Invrt_Entry_Dte_Tme;

    private DbsGroup cwf_Efm_Document_Audit_Data;
    private DbsField cwf_Efm_Document_Audit_Action;
    private DbsField cwf_Efm_Document_Audit_Empl_Racf_Id;
    private DbsField cwf_Efm_Document_Audit_System_Or_Unit;
    private DbsField cwf_Efm_Document_Audit_Jobname;
    private DbsField cwf_Efm_Document_Audit_Dte_Tme;
    private DbsField cwf_Efm_Document_Document_Subtype;
    private DbsField cwf_Efm_Document_Text_Pointer;
    private DbsField cwf_Efm_Document_Image_Min;
    private DbsField cwf_Efm_Document_Kdo_Application_Id;
    private DbsField cwf_Efm_Document_Kdo_Pointer;
    private DbsField cwf_Efm_Document_Image_Source_Id;
    private DbsField cwf_Efm_Document_Entry_System;
    private DbsField cwf_Efm_Document_Dgtze_Corres_From_Dte;
    private DbsField cwf_Efm_Document_Dgtze_Corres_To_Dte;
    private DbsField cwf_Efm_Document_Dgtze_Corres_Seq_No;
    private DbsField cwf_Efm_Document_Dgtze_Doc_End_Page;
    private DbsField cwf_Efm_Document_Security_Ind;
    private DbsField cwf_Efm_Document_Dgtze_Ind;

    private DbsGroup pnd_Doc_Key;
    private DbsField pnd_Doc_Key_Cabinet_Id;

    private DbsGroup pnd_Doc_Key__R_Field_13;
    private DbsField pnd_Doc_Key_Cab_Prefix;
    private DbsField pnd_Doc_Key_Cab_Pin;

    private DbsGroup pnd_Doc_Key_Folder_Id;
    private DbsField pnd_Doc_Key_Tiaa_Rcvd_Dte;
    private DbsField pnd_Doc_Key_Case_Workprcss_Multi_Subrqst;

    private DbsGroup pnd_Doc_Key__R_Field_14;
    private DbsField pnd_Doc_Key_Case_Ind;
    private DbsField pnd_Doc_Key_Originating_Wpid;
    private DbsField pnd_Doc_Key_Multi_Rqst_Ind;
    private DbsField pnd_Doc_Key_Sub_Rqst_Ind;
    private DbsField pnd_Doc_Key_Entry_Dte_Tme;

    private DbsGroup pnd_Doc_Key__R_Field_15;
    private DbsField pnd_Doc_Key_Pnd_Doc_Ky;
    private DbsField pnd_Misc_Wpid;
    private DbsField pnd_First_Doc;
    private DbsField pnd_Date_A8;

    private DbsGroup pnd_Date_A8__R_Field_16;
    private DbsField pnd_Date_A8_Pnd_Date_N8;
    private DbsField pnd_Curr_Date_N8;

    private DbsGroup pnd_Curr_Date_N8__R_Field_17;
    private DbsField pnd_Curr_Date_N8_Pnd_Curr_Date_A8;
    private DbsField pnd_Dest_Folder;

    private DbsGroup pnd_Dest_Folder__R_Field_18;
    private DbsField pnd_Dest_Folder_Tiaa_Rcvd_Dte;

    private DbsGroup pnd_Dest_Folder__R_Field_19;
    private DbsField pnd_Dest_Folder_Tiaa_Rcvd_Dte_N;
    private DbsField pnd_Dest_Folder_Case_Ind;
    private DbsField pnd_Dest_Folder_Originating_Wpid;
    private DbsField pnd_Dest_Folder_Multi_Rqst_Ind;
    private DbsField pnd_Dest_Folder_Sub_Rqst_Ind;
    private DbsField pnd_Src_Folder;

    private DbsGroup pnd_Src_Folder__R_Field_20;
    private DbsField pnd_Src_Folder_Tiaa_Rcvd_Dte;
    private DbsField pnd_Src_Folder_Case_Ind;
    private DbsField pnd_Src_Folder_Originating_Wpid;
    private DbsField pnd_Src_Folder_Multi_Rqst_Ind;
    private DbsField pnd_Src_Folder_Sub_Rqst_Ind;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_21;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_22;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Doc_Type;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Doc_Type_P_S;
    private DbsField pnd_Doc_Type_Valid;
    private DbsField pnd_Check_Doc_Type;
    private DbsField pnd_Srvvr_Folder;
    private DbsField pnd_Srvvr_Cnt;
    private DbsField pnd_Ix;
    private DbsField pnd_Doc_Info_Arr;

    private DbsGroup pnd_Doc_Info_Flds;
    private DbsField pnd_Doc_Info_Flds_Document_Subtype;
    private DbsField pnd_Doc_Info_Flds_Image_Source_Id;
    private DbsField pnd_Doc_Info_Flds_Image_Min;
    private DbsField pnd_Doc_Info_Flds_Text_Pointer;

    private DbsGroup pnd_Doc_Info_Flds__R_Field_23;
    private DbsField pnd_Doc_Info_Flds_Pnd_Doc_Info;
    private DbsField pnd_Report;
    private DbsField pnd_Assigned_Cnt;

    private DataAccessProgramView vw_cwf_Support_Tbl_Sb_Bene;
    private DbsField cwf_Support_Tbl_Sb_Bene_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Sb_Bene_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Sb_Bene_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Sb_Bene_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Sb_Bene_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl_Sb_Bene__R_Field_24;
    private DbsField cwf_Support_Tbl_Sb_Bene_Pnd_Tbl_Data_Field1;
    private DbsField cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Sup_Tbl_Prime_Key;

    private DbsGroup pnd_Sup_Tbl_Prime_Key__R_Field_25;
    private DbsField pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Table_Nme;
    private DbsField pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Key_Field;
    private DbsField pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Actve_Ind;
    private DbsField pnd_Last_Upd_Date_Sup_Tbl;
    private DbsField pnd_Last_Upd_Date_Sb_Bene;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables

        vw_cwf_Sb_Work_Req = new DataAccessProgramView(new NameInfo("vw_cwf_Sb_Work_Req", "CWF-SB-WORK-REQ"), "CWF_SB_WORK_REQ", "CWF_SB_WORK_REQ");
        cwf_Sb_Work_Req_Srvvr_Mit_Idntfr = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "SRVVR_MIT_IDNTFR");
        cwf_Sb_Work_Req_Srvvr_Mit_Idntfr.setDdmHeader("SURVIVOR MIT/IDENTIFIER");

        cwf_Sb_Work_Req__R_Field_1 = vw_cwf_Sb_Work_Req.getRecord().newGroupInGroup("cwf_Sb_Work_Req__R_Field_1", "REDEFINE", cwf_Sb_Work_Req_Srvvr_Mit_Idntfr);
        cwf_Sb_Work_Req_Ph_Pin_Nbr = cwf_Sb_Work_Req__R_Field_1.newFieldInGroup("cwf_Sb_Work_Req_Ph_Pin_Nbr", "PH-PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Sb_Work_Req_Srvvr_Mit_Sqnce = cwf_Sb_Work_Req__R_Field_1.newFieldInGroup("cwf_Sb_Work_Req_Srvvr_Mit_Sqnce", "SRVVR-MIT-SQNCE", FieldType.NUMERIC, 
            2);
        cwf_Sb_Work_Req_Rqst_Log_Dte_Tme = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Sb_Work_Req_Rqst_Log_Dte_Tme.setDdmHeader("RQST LOG/DATE/TIME");
        cwf_Sb_Work_Req_Split_Ind = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Split_Ind", "SPLIT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SPLIT_IND");
        cwf_Sb_Work_Req_Last_Updte_Oprtr_Cde = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Sb_Work_Req_Last_Updte_Oprtr_Cde.setDdmHeader("LAST UPDATE/OPERATOR CODE");
        registerRecord(vw_cwf_Sb_Work_Req);

        vw_cwf_Sb_Work_Req_2 = new DataAccessProgramView(new NameInfo("vw_cwf_Sb_Work_Req_2", "CWF-SB-WORK-REQ-2"), "CWF_SB_WORK_REQ", "CWF_SB_WORK_REQ");
        registerRecord(vw_cwf_Sb_Work_Req_2);

        pnd_Work_Req_Key = localVariables.newFieldInRecord("pnd_Work_Req_Key", "#WORK-REQ-KEY", FieldType.STRING, 29);

        pnd_Work_Req_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Work_Req_Key__R_Field_2", "REDEFINE", pnd_Work_Req_Key);
        pnd_Work_Req_Key_Srvvr_Mit_Idntfr = pnd_Work_Req_Key__R_Field_2.newFieldInGroup("pnd_Work_Req_Key_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 
            14);
        pnd_Work_Req_Key_Rqst_Log_Dte_Tme = pnd_Work_Req_Key__R_Field_2.newFieldInGroup("pnd_Work_Req_Key_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15);

        vw_cwf_Sb_Bene = new DataAccessProgramView(new NameInfo("vw_cwf_Sb_Bene", "CWF-SB-BENE"), "CWF_SB_BENE", "CWF_SB_BENE");
        cwf_Sb_Bene_Rcrd_Type = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE");
        cwf_Sb_Bene_Rcrd_Type.setDdmHeader("RECORD/TYPE");
        cwf_Sb_Bene_Srvvr_Mit_Idntfr = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "SRVVR_MIT_IDNTFR");
        cwf_Sb_Bene_Srvvr_Mit_Idntfr.setDdmHeader("SURVIVOR MIT/IDENTIFIER");

        cwf_Sb_Bene__R_Field_3 = vw_cwf_Sb_Bene.getRecord().newGroupInGroup("cwf_Sb_Bene__R_Field_3", "REDEFINE", cwf_Sb_Bene_Srvvr_Mit_Idntfr);
        cwf_Sb_Bene_Ph_Pin_Nbr = cwf_Sb_Bene__R_Field_3.newFieldInGroup("cwf_Sb_Bene_Ph_Pin_Nbr", "PH-PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Sb_Bene_Srvvr_Mit_Sqnce = cwf_Sb_Bene__R_Field_3.newFieldInGroup("cwf_Sb_Bene_Srvvr_Mit_Sqnce", "SRVVR-MIT-SQNCE", FieldType.NUMERIC, 2);
        cwf_Sb_Bene_Srvvr_Pin = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Pin", "SRVVR-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "SRVVR_PIN");
        cwf_Sb_Bene_Srvvr_Ssn_Nbr = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Ssn_Nbr", "SRVVR-SSN-NBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "SRVVR_SSN_NBR");
        cwf_Sb_Bene_Srvvr_Ssn_Nbr.setDdmHeader("SURVIVOR/SSN");

        cwf_Sb_Bene__R_Field_4 = vw_cwf_Sb_Bene.getRecord().newGroupInGroup("cwf_Sb_Bene__R_Field_4", "REDEFINE", cwf_Sb_Bene_Srvvr_Ssn_Nbr);
        cwf_Sb_Bene_Srvvr_Ssn_Nbr_N9 = cwf_Sb_Bene__R_Field_4.newFieldInGroup("cwf_Sb_Bene_Srvvr_Ssn_Nbr_N9", "SRVVR-SSN-NBR-N9", FieldType.NUMERIC, 9);
        cwf_Sb_Bene_Srvvr_Nme_Fr_Frm = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Nme_Fr_Frm", "SRVVR-NME-FR-FRM", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "SRVVR_NME_FR_FRM");
        cwf_Sb_Bene_Srvvr_Nme_Fr_Frm.setDdmHeader("SURVIVOR NME/FREE FORM");
        cwf_Sb_Bene_Last_Updte_Dte_Tme = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Sb_Bene_Last_Updte_Dte_Tme.setDdmHeader("LAST UPDATE/DATE/TIME");
        registerRecord(vw_cwf_Sb_Bene);

        pnd_Srvvr_Key = localVariables.newFieldInRecord("pnd_Srvvr_Key", "#SRVVR-KEY", FieldType.STRING, 15);

        pnd_Srvvr_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Srvvr_Key__R_Field_5", "REDEFINE", pnd_Srvvr_Key);
        pnd_Srvvr_Key_Rcrd_Type = pnd_Srvvr_Key__R_Field_5.newFieldInGroup("pnd_Srvvr_Key_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1);
        pnd_Srvvr_Key_Srvvr_Mit_Idntfr = pnd_Srvvr_Key__R_Field_5.newFieldInGroup("pnd_Srvvr_Key_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 
            14);

        vw_cwf_Master_1 = new DataAccessProgramView(new NameInfo("vw_cwf_Master_1", "CWF-MASTER-1"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_1_Pin_Nbr = vw_cwf_Master_1.getRecord().newFieldInGroup("cwf_Master_1_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_1_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_1_Crprte_Status_Ind = vw_cwf_Master_1.getRecord().newFieldInGroup("cwf_Master_1_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_1_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_1_Status_Cde = vw_cwf_Master_1.getRecord().newFieldInGroup("cwf_Master_1_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        cwf_Master_1_Tiaa_Rcvd_Dte = vw_cwf_Master_1.getRecord().newFieldInGroup("cwf_Master_1_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Master_1_Rqst_Log_Dte_Tme = vw_cwf_Master_1.getRecord().newFieldInGroup("cwf_Master_1_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_1_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_1_Rqst_Id = vw_cwf_Master_1.getRecord().newFieldInGroup("cwf_Master_1_Rqst_Id", "RQST-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, 
            "RQST_ID");
        cwf_Master_1_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Master_1__R_Field_6 = vw_cwf_Master_1.getRecord().newGroupInGroup("cwf_Master_1__R_Field_6", "REDEFINE", cwf_Master_1_Rqst_Id);
        cwf_Master_1_Rqst_Work_Prcss_Id = cwf_Master_1__R_Field_6.newFieldInGroup("cwf_Master_1_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        cwf_Master_1_Rqst_Tiaa_Rcvd_Dte = cwf_Master_1__R_Field_6.newFieldInGroup("cwf_Master_1_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", FieldType.STRING, 
            8);
        cwf_Master_1_Rqst_Case_Id = cwf_Master_1__R_Field_6.newFieldInGroup("cwf_Master_1_Rqst_Case_Id", "RQST-CASE-ID", FieldType.STRING, 2);

        cwf_Master_1__R_Field_7 = cwf_Master_1__R_Field_6.newGroupInGroup("cwf_Master_1__R_Field_7", "REDEFINE", cwf_Master_1_Rqst_Case_Id);
        cwf_Master_1_Rqst_Case_Id_Ind = cwf_Master_1__R_Field_7.newFieldInGroup("cwf_Master_1_Rqst_Case_Id_Ind", "RQST-CASE-ID-IND", FieldType.STRING, 
            1);
        cwf_Master_1_Rqst_Sub_Rqst_Cde = cwf_Master_1__R_Field_7.newFieldInGroup("cwf_Master_1_Rqst_Sub_Rqst_Cde", "RQST-SUB-RQST-CDE", FieldType.STRING, 
            1);
        cwf_Master_1_Rqst_Pin_Nbr = cwf_Master_1__R_Field_6.newFieldInGroup("cwf_Master_1_Rqst_Pin_Nbr", "RQST-PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Master_1_Multi_Rqst_Ind = vw_cwf_Master_1.getRecord().newFieldInGroup("cwf_Master_1_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        cwf_Master_1_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Master_1_Elctrnc_Fldr_Ind = vw_cwf_Master_1.getRecord().newFieldInGroup("cwf_Master_1_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_1_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        registerRecord(vw_cwf_Master_1);

        vw_cwf_Master_2 = new DataAccessProgramView(new NameInfo("vw_cwf_Master_2", "CWF-MASTER-2"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_2_Elctrnc_Fldr_Ind = vw_cwf_Master_2.getRecord().newFieldInGroup("cwf_Master_2_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_2_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        registerRecord(vw_cwf_Master_2);

        vw_cwf_Efm_Folder = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Folder", "CWF-EFM-FOLDER"), "CWF_EFM_FOLDER", "CWF_EFM_FOLDER");
        cwf_Efm_Folder_Cabinet_Id = vw_cwf_Efm_Folder.getRecord().newFieldInGroup("cwf_Efm_Folder_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Efm_Folder_Cabinet_Id.setDdmHeader("CABINET/ID");
        cwf_Efm_Folder_Sub_Rqst_Work_Prcss_Id = vw_cwf_Efm_Folder.getRecord().newFieldInGroup("cwf_Efm_Folder_Sub_Rqst_Work_Prcss_Id", "SUB-RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_ID");
        cwf_Efm_Folder_Sub_Rqst_Work_Prcss_Id.setDdmHeader("SUB-RQST WORK/PROCESS ID");

        cwf_Efm_Folder_Folder_Id = vw_cwf_Efm_Folder.getRecord().newGroupInGroup("CWF_EFM_FOLDER_FOLDER_ID", "FOLDER-ID");
        cwf_Efm_Folder_Tiaa_Rcvd_Dte = cwf_Efm_Folder_Folder_Id.newFieldInGroup("cwf_Efm_Folder_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Efm_Folder_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Efm_Folder_Case_Workprcss_Multi_Subrqst = cwf_Efm_Folder_Folder_Id.newFieldInGroup("cwf_Efm_Folder_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Efm_Folder_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Efm_Folder__R_Field_8 = cwf_Efm_Folder_Folder_Id.newGroupInGroup("cwf_Efm_Folder__R_Field_8", "REDEFINE", cwf_Efm_Folder_Case_Workprcss_Multi_Subrqst);
        cwf_Efm_Folder_Case_Ind = cwf_Efm_Folder__R_Field_8.newFieldInGroup("cwf_Efm_Folder_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        cwf_Efm_Folder_Originating_Wpid = cwf_Efm_Folder__R_Field_8.newFieldInGroup("cwf_Efm_Folder_Originating_Wpid", "ORIGINATING-WPID", FieldType.STRING, 
            6);
        cwf_Efm_Folder_Multi_Rqst_Ind = cwf_Efm_Folder__R_Field_8.newFieldInGroup("cwf_Efm_Folder_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 
            1);
        cwf_Efm_Folder_Sub_Rqst_Ind = cwf_Efm_Folder__R_Field_8.newFieldInGroup("cwf_Efm_Folder_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 1);
        registerRecord(vw_cwf_Efm_Folder);

        pnd_Work_Prcss_Key = localVariables.newFieldInRecord("pnd_Work_Prcss_Key", "#WORK-PRCSS-KEY", FieldType.STRING, 38);

        pnd_Work_Prcss_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Work_Prcss_Key__R_Field_9", "REDEFINE", pnd_Work_Prcss_Key);
        pnd_Work_Prcss_Key_Cabinet_Id = pnd_Work_Prcss_Key__R_Field_9.newFieldInGroup("pnd_Work_Prcss_Key_Cabinet_Id", "CABINET-ID", FieldType.STRING, 
            14);

        pnd_Work_Prcss_Key__R_Field_10 = pnd_Work_Prcss_Key__R_Field_9.newGroupInGroup("pnd_Work_Prcss_Key__R_Field_10", "REDEFINE", pnd_Work_Prcss_Key_Cabinet_Id);
        pnd_Work_Prcss_Key_Cab_Prefix = pnd_Work_Prcss_Key__R_Field_10.newFieldInGroup("pnd_Work_Prcss_Key_Cab_Prefix", "CAB-PREFIX", FieldType.STRING, 
            1);
        pnd_Work_Prcss_Key_Pin_Nbr = pnd_Work_Prcss_Key__R_Field_10.newFieldInGroup("pnd_Work_Prcss_Key_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Work_Prcss_Key_Sub_Rqst_Work_Prcss_Id = pnd_Work_Prcss_Key__R_Field_9.newFieldInGroup("pnd_Work_Prcss_Key_Sub_Rqst_Work_Prcss_Id", "SUB-RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);

        vw_cwf_Efm_Document = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Document", "CWF-EFM-DOCUMENT"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Efm_Document_Document_Key = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Document_Key", "DOCUMENT-KEY", FieldType.BINARY, 
            34, RepeatingFieldStrategy.None, "DOCUMENT_KEY");
        cwf_Efm_Document_Document_Key.setDdmHeader("DOCUMENT/KEY");
        cwf_Efm_Document_Document_Key.setSuperDescriptor(true);
        cwf_Efm_Document_Cabinet_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Document_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Document_Folder_Id = vw_cwf_Efm_Document.getRecord().newGroupInGroup("CWF_EFM_DOCUMENT_FOLDER_ID", "FOLDER-ID");
        cwf_Efm_Document_Folder_Id.setDdmHeader("FOLDER/ID");
        cwf_Efm_Document_Tiaa_Rcvd_Dte = cwf_Efm_Document_Folder_Id.newFieldInGroup("cwf_Efm_Document_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Efm_Document_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Efm_Document_Case_Workprcss_Multi_Subrqst = cwf_Efm_Document_Folder_Id.newFieldInGroup("cwf_Efm_Document_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Efm_Document_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Efm_Document__R_Field_11 = cwf_Efm_Document_Folder_Id.newGroupInGroup("cwf_Efm_Document__R_Field_11", "REDEFINE", cwf_Efm_Document_Case_Workprcss_Multi_Subrqst);
        cwf_Efm_Document_Case_Ind = cwf_Efm_Document__R_Field_11.newFieldInGroup("cwf_Efm_Document_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        cwf_Efm_Document_Originating_Wpid = cwf_Efm_Document__R_Field_11.newFieldInGroup("cwf_Efm_Document_Originating_Wpid", "ORIGINATING-WPID", FieldType.STRING, 
            6);
        cwf_Efm_Document_Multi_Rqst_Ind = cwf_Efm_Document__R_Field_11.newFieldInGroup("cwf_Efm_Document_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 
            1);
        cwf_Efm_Document_Sub_Rqst_Ind = cwf_Efm_Document__R_Field_11.newFieldInGroup("cwf_Efm_Document_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 
            1);
        cwf_Efm_Document_Entry_Dte_Tme = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Efm_Document_Entry_Dte_Tme.setDdmHeader("ENTRY/DATE AND TIME");
        cwf_Efm_Document_Entry_Empl_Racf_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_Empl_Racf_Id", "ENTRY-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_EMPL_RACF_ID");
        cwf_Efm_Document_Entry_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        cwf_Efm_Document_Entry_System_Or_Unit = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_System_Or_Unit", "ENTRY-SYSTEM-OR-UNIT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_SYSTEM_OR_UNIT");
        cwf_Efm_Document_Entry_System_Or_Unit.setDdmHeader("CREATED BY/SYSTEM OR UNIT");
        cwf_Efm_Document_Entry_Jobname = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_Jobname", "ENTRY-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_JOBNAME");
        cwf_Efm_Document_Entry_Jobname.setDdmHeader("CREATED BY/JOBNAME");

        cwf_Efm_Document_Document_Type = vw_cwf_Efm_Document.getRecord().newGroupInGroup("CWF_EFM_DOCUMENT_DOCUMENT_TYPE", "DOCUMENT-TYPE");
        cwf_Efm_Document_Document_Type.setDdmHeader("DOCUMENT/TYPE");
        cwf_Efm_Document_Document_Category = cwf_Efm_Document_Document_Type.newFieldInGroup("cwf_Efm_Document_Document_Category", "DOCUMENT-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_CATEGORY");
        cwf_Efm_Document_Document_Category.setDdmHeader("DOC/CAT");
        cwf_Efm_Document_Document_Sub_Category = cwf_Efm_Document_Document_Type.newFieldInGroup("cwf_Efm_Document_Document_Sub_Category", "DOCUMENT-SUB-CATEGORY", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "DOCUMENT_SUB_CATEGORY");
        cwf_Efm_Document_Document_Sub_Category.setDdmHeader("DOC SUB-/CATEGORY");
        cwf_Efm_Document_Document_Detail = cwf_Efm_Document_Document_Type.newFieldInGroup("cwf_Efm_Document_Document_Detail", "DOCUMENT-DETAIL", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "DOCUMENT_DETAIL");
        cwf_Efm_Document_Document_Detail.setDdmHeader("DOCUMENT/DETAIL");

        cwf_Efm_Document__R_Field_12 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_12", "REDEFINE", cwf_Efm_Document_Document_Type);
        cwf_Efm_Document_Doc_Type = cwf_Efm_Document__R_Field_12.newFieldInGroup("cwf_Efm_Document_Doc_Type", "DOC-TYPE", FieldType.STRING, 8);
        cwf_Efm_Document_Rcvd_Sent_Dte = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Rcvd_Sent_Dte", "RCVD-SENT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "RCVD_SENT_DTE");
        cwf_Efm_Document_Rcvd_Sent_Dte.setDdmHeader("RECEIVED/SENT/DATE");
        cwf_Efm_Document_Document_Direction_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Document_Direction_Ind", "DOCUMENT-DIRECTION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_DIRECTION_IND");
        cwf_Efm_Document_Document_Direction_Ind.setDdmHeader("DOC/DIR");
        cwf_Efm_Document_Secured_Document_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Secured_Document_Ind", "SECURED-DOCUMENT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SECURED_DOCUMENT_IND");
        cwf_Efm_Document_Secured_Document_Ind.setDdmHeader("SECRD/DOC");
        cwf_Efm_Document_Document_Retention_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Document_Retention_Ind", "DOCUMENT-RETENTION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_RETENTION_IND");
        cwf_Efm_Document_Document_Retention_Ind.setDdmHeader("DOC/RETNTN");
        cwf_Efm_Document_Important_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Important_Ind", "IMPORTANT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMPORTANT_IND");
        cwf_Efm_Document_Important_Ind.setDdmHeader("IMP/IND");
        cwf_Efm_Document_Copy_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Copy_Ind", "COPY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "COPY_IND");
        cwf_Efm_Document_Copy_Ind.setDdmHeader("COPY/IND");
        cwf_Efm_Document_Documents_Location = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Documents_Location", "DOCUMENTS-LOCATION", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "DOCUMENTS_LOCATION");
        cwf_Efm_Document_Documents_Location.setDdmHeader("LOCATION OF/DOCUMENTS");
        cwf_Efm_Document_Sub_Rqst_Work_Prcss_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Sub_Rqst_Work_Prcss_Id", "SUB-RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_ID");
        cwf_Efm_Document_Sub_Rqst_Work_Prcss_Id.setDdmHeader("SUB-REQUEST/WORK PROCESS ID");
        cwf_Efm_Document_Sub_Rqst_Work_Prcss_Short_Nme = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Sub_Rqst_Work_Prcss_Short_Nme", 
            "SUB-RQST-WORK-PRCSS-SHORT-NME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_SHORT_NME");
        cwf_Efm_Document_Sub_Rqst_Work_Prcss_Short_Nme.setDdmHeader("SUB-REQUEST WORK/PROCESS NAME");
        cwf_Efm_Document_Business_Document_Description = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Business_Document_Description", 
            "BUSINESS-DOCUMENT-DESCRIPTION", FieldType.STRING, 30, RepeatingFieldStrategy.None, "BUSINESS_DOCUMENT_DESCRIPTION");
        cwf_Efm_Document_Business_Document_Description.setDdmHeader("BUSINESS DOCUMENT/DESCRIPTION");
        cwf_Efm_Document_Invrt_Entry_Dte_Tme = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Invrt_Entry_Dte_Tme", "INVRT-ENTRY-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "INVRT_ENTRY_DTE_TME");
        cwf_Efm_Document_Invrt_Entry_Dte_Tme.setDdmHeader("INVERTED ENTRY/DATE AND TIME");

        cwf_Efm_Document_Audit_Data = vw_cwf_Efm_Document.getRecord().newGroupInGroup("CWF_EFM_DOCUMENT_AUDIT_DATA", "AUDIT-DATA");
        cwf_Efm_Document_Audit_Action = cwf_Efm_Document_Audit_Data.newFieldInGroup("cwf_Efm_Document_Audit_Action", "AUDIT-ACTION", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AUDIT_ACTION");
        cwf_Efm_Document_Audit_Action.setDdmHeader("ACTION");
        cwf_Efm_Document_Audit_Empl_Racf_Id = cwf_Efm_Document_Audit_Data.newFieldInGroup("cwf_Efm_Document_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AUDIT_EMPL_RACF_ID");
        cwf_Efm_Document_Audit_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF ID");
        cwf_Efm_Document_Audit_System_Or_Unit = cwf_Efm_Document_Audit_Data.newFieldInGroup("cwf_Efm_Document_Audit_System_Or_Unit", "AUDIT-SYSTEM-OR-UNIT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AUDIT_SYSTEM_OR_UNIT");
        cwf_Efm_Document_Audit_System_Or_Unit.setDdmHeader("SYSTEM/OR UNIT");
        cwf_Efm_Document_Audit_Jobname = cwf_Efm_Document_Audit_Data.newFieldInGroup("cwf_Efm_Document_Audit_Jobname", "AUDIT-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_JOBNAME");
        cwf_Efm_Document_Audit_Jobname.setDdmHeader("JOBNAME");
        cwf_Efm_Document_Audit_Dte_Tme = cwf_Efm_Document_Audit_Data.newFieldInGroup("cwf_Efm_Document_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "AUDIT_DTE_TME");
        cwf_Efm_Document_Audit_Dte_Tme.setDdmHeader("DATE/& TIME");
        cwf_Efm_Document_Document_Subtype = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Document_Subtype", "DOCUMENT-SUBTYPE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DOCUMENT_SUBTYPE");
        cwf_Efm_Document_Document_Subtype.setDdmHeader("DOC/SUBTP");
        cwf_Efm_Document_Text_Pointer = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Text_Pointer", "TEXT-POINTER", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "TEXT_POINTER");
        cwf_Efm_Document_Text_Pointer.setDdmHeader("TEXT/POINTER");
        cwf_Efm_Document_Image_Min = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        cwf_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");
        cwf_Efm_Document_Kdo_Application_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Kdo_Application_Id", "KDO-APPLICATION-ID", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "KDO_APPLICATION_ID");
        cwf_Efm_Document_Kdo_Application_Id.setDdmHeader("KDO/APPLIC & ID");
        cwf_Efm_Document_Kdo_Pointer = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Kdo_Pointer", "KDO-POINTER", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "KDO_POINTER");
        cwf_Efm_Document_Kdo_Pointer.setDdmHeader("K D O/POINTER");
        cwf_Efm_Document_Image_Source_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        cwf_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        cwf_Efm_Document_Entry_System = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_System", "ENTRY-SYSTEM", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_SYSTEM");
        cwf_Efm_Document_Entry_System.setDdmHeader("ENTRY/SYSTEM");
        cwf_Efm_Document_Dgtze_Corres_From_Dte = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Dgtze_Corres_From_Dte", "DGTZE-CORRES-FROM-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "DGTZE_CORRES_FROM_DTE");
        cwf_Efm_Document_Dgtze_Corres_From_Dte.setDdmHeader("CORRESPONCE FROM/DATE");
        cwf_Efm_Document_Dgtze_Corres_To_Dte = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Dgtze_Corres_To_Dte", "DGTZE-CORRES-TO-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "DGTZE_CORRES_TO_DTE");
        cwf_Efm_Document_Dgtze_Corres_To_Dte.setDdmHeader("CORRESPONDENCE TO/DATE");
        cwf_Efm_Document_Dgtze_Corres_Seq_No = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Dgtze_Corres_Seq_No", "DGTZE-CORRES-SEQ-NO", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "DGTZE_CORRES_SEQ_NO");
        cwf_Efm_Document_Dgtze_Corres_Seq_No.setDdmHeader("CORRESPONDENCE/SEQUENCE NO");
        cwf_Efm_Document_Dgtze_Doc_End_Page = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Dgtze_Doc_End_Page", "DGTZE-DOC-END-PAGE", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "DGTZE_DOC_END_PAGE");
        cwf_Efm_Document_Dgtze_Doc_End_Page.setDdmHeader("DOCUMENT/END PGE");
        cwf_Efm_Document_Security_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Security_Ind", "SECURITY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SECURITY_IND");
        cwf_Efm_Document_Security_Ind.setDdmHeader("SECURED MIN/IND");
        cwf_Efm_Document_Dgtze_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Dgtze_Ind", "DGTZE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DGTZE_IND");
        cwf_Efm_Document_Dgtze_Ind.setDdmHeader("DIGITIZE/IND");
        registerRecord(vw_cwf_Efm_Document);

        pnd_Doc_Key = localVariables.newGroupInRecord("pnd_Doc_Key", "#DOC-KEY");
        pnd_Doc_Key_Cabinet_Id = pnd_Doc_Key.newFieldInGroup("pnd_Doc_Key_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14);

        pnd_Doc_Key__R_Field_13 = pnd_Doc_Key.newGroupInGroup("pnd_Doc_Key__R_Field_13", "REDEFINE", pnd_Doc_Key_Cabinet_Id);
        pnd_Doc_Key_Cab_Prefix = pnd_Doc_Key__R_Field_13.newFieldInGroup("pnd_Doc_Key_Cab_Prefix", "CAB-PREFIX", FieldType.STRING, 1);
        pnd_Doc_Key_Cab_Pin = pnd_Doc_Key__R_Field_13.newFieldInGroup("pnd_Doc_Key_Cab_Pin", "CAB-PIN", FieldType.NUMERIC, 12);

        pnd_Doc_Key_Folder_Id = pnd_Doc_Key.newGroupInGroup("pnd_Doc_Key_Folder_Id", "FOLDER-ID");
        pnd_Doc_Key_Tiaa_Rcvd_Dte = pnd_Doc_Key_Folder_Id.newFieldInGroup("pnd_Doc_Key_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Doc_Key_Case_Workprcss_Multi_Subrqst = pnd_Doc_Key_Folder_Id.newFieldInGroup("pnd_Doc_Key_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9);

        pnd_Doc_Key__R_Field_14 = pnd_Doc_Key_Folder_Id.newGroupInGroup("pnd_Doc_Key__R_Field_14", "REDEFINE", pnd_Doc_Key_Case_Workprcss_Multi_Subrqst);
        pnd_Doc_Key_Case_Ind = pnd_Doc_Key__R_Field_14.newFieldInGroup("pnd_Doc_Key_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        pnd_Doc_Key_Originating_Wpid = pnd_Doc_Key__R_Field_14.newFieldInGroup("pnd_Doc_Key_Originating_Wpid", "ORIGINATING-WPID", FieldType.STRING, 6);
        pnd_Doc_Key_Multi_Rqst_Ind = pnd_Doc_Key__R_Field_14.newFieldInGroup("pnd_Doc_Key_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 1);
        pnd_Doc_Key_Sub_Rqst_Ind = pnd_Doc_Key__R_Field_14.newFieldInGroup("pnd_Doc_Key_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 1);
        pnd_Doc_Key_Entry_Dte_Tme = pnd_Doc_Key.newFieldInGroup("pnd_Doc_Key_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME);

        pnd_Doc_Key__R_Field_15 = localVariables.newGroupInRecord("pnd_Doc_Key__R_Field_15", "REDEFINE", pnd_Doc_Key);
        pnd_Doc_Key_Pnd_Doc_Ky = pnd_Doc_Key__R_Field_15.newFieldInGroup("pnd_Doc_Key_Pnd_Doc_Ky", "#DOC-KY", FieldType.STRING, 34);
        pnd_Misc_Wpid = localVariables.newFieldInRecord("pnd_Misc_Wpid", "#MISC-WPID", FieldType.STRING, 6);
        pnd_First_Doc = localVariables.newFieldInRecord("pnd_First_Doc", "#FIRST-DOC", FieldType.BOOLEAN, 1);
        pnd_Date_A8 = localVariables.newFieldInRecord("pnd_Date_A8", "#DATE-A8", FieldType.STRING, 8);

        pnd_Date_A8__R_Field_16 = localVariables.newGroupInRecord("pnd_Date_A8__R_Field_16", "REDEFINE", pnd_Date_A8);
        pnd_Date_A8_Pnd_Date_N8 = pnd_Date_A8__R_Field_16.newFieldInGroup("pnd_Date_A8_Pnd_Date_N8", "#DATE-N8", FieldType.NUMERIC, 8);
        pnd_Curr_Date_N8 = localVariables.newFieldInRecord("pnd_Curr_Date_N8", "#CURR-DATE-N8", FieldType.NUMERIC, 8);

        pnd_Curr_Date_N8__R_Field_17 = localVariables.newGroupInRecord("pnd_Curr_Date_N8__R_Field_17", "REDEFINE", pnd_Curr_Date_N8);
        pnd_Curr_Date_N8_Pnd_Curr_Date_A8 = pnd_Curr_Date_N8__R_Field_17.newFieldInGroup("pnd_Curr_Date_N8_Pnd_Curr_Date_A8", "#CURR-DATE-A8", FieldType.STRING, 
            8);
        pnd_Dest_Folder = localVariables.newFieldInRecord("pnd_Dest_Folder", "#DEST-FOLDER", FieldType.STRING, 17);

        pnd_Dest_Folder__R_Field_18 = localVariables.newGroupInRecord("pnd_Dest_Folder__R_Field_18", "REDEFINE", pnd_Dest_Folder);
        pnd_Dest_Folder_Tiaa_Rcvd_Dte = pnd_Dest_Folder__R_Field_18.newFieldInGroup("pnd_Dest_Folder_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.STRING, 
            8);

        pnd_Dest_Folder__R_Field_19 = pnd_Dest_Folder__R_Field_18.newGroupInGroup("pnd_Dest_Folder__R_Field_19", "REDEFINE", pnd_Dest_Folder_Tiaa_Rcvd_Dte);
        pnd_Dest_Folder_Tiaa_Rcvd_Dte_N = pnd_Dest_Folder__R_Field_19.newFieldInGroup("pnd_Dest_Folder_Tiaa_Rcvd_Dte_N", "TIAA-RCVD-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_Dest_Folder_Case_Ind = pnd_Dest_Folder__R_Field_18.newFieldInGroup("pnd_Dest_Folder_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        pnd_Dest_Folder_Originating_Wpid = pnd_Dest_Folder__R_Field_18.newFieldInGroup("pnd_Dest_Folder_Originating_Wpid", "ORIGINATING-WPID", FieldType.STRING, 
            6);
        pnd_Dest_Folder_Multi_Rqst_Ind = pnd_Dest_Folder__R_Field_18.newFieldInGroup("pnd_Dest_Folder_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 
            1);
        pnd_Dest_Folder_Sub_Rqst_Ind = pnd_Dest_Folder__R_Field_18.newFieldInGroup("pnd_Dest_Folder_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 1);
        pnd_Src_Folder = localVariables.newFieldInRecord("pnd_Src_Folder", "#SRC-FOLDER", FieldType.STRING, 17);

        pnd_Src_Folder__R_Field_20 = localVariables.newGroupInRecord("pnd_Src_Folder__R_Field_20", "REDEFINE", pnd_Src_Folder);
        pnd_Src_Folder_Tiaa_Rcvd_Dte = pnd_Src_Folder__R_Field_20.newFieldInGroup("pnd_Src_Folder_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.STRING, 8);
        pnd_Src_Folder_Case_Ind = pnd_Src_Folder__R_Field_20.newFieldInGroup("pnd_Src_Folder_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        pnd_Src_Folder_Originating_Wpid = pnd_Src_Folder__R_Field_20.newFieldInGroup("pnd_Src_Folder_Originating_Wpid", "ORIGINATING-WPID", FieldType.STRING, 
            6);
        pnd_Src_Folder_Multi_Rqst_Ind = pnd_Src_Folder__R_Field_20.newFieldInGroup("pnd_Src_Folder_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 
            1);
        pnd_Src_Folder_Sub_Rqst_Ind = pnd_Src_Folder__R_Field_20.newFieldInGroup("pnd_Src_Folder_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 1);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_21 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_21", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_21.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_21.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_21.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30);

        pnd_Tbl_Prime_Key__R_Field_22 = pnd_Tbl_Prime_Key__R_Field_21.newGroupInGroup("pnd_Tbl_Prime_Key__R_Field_22", "REDEFINE", pnd_Tbl_Prime_Key_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_Tbl_Doc_Type = pnd_Tbl_Prime_Key__R_Field_22.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Doc_Type", "TBL-DOC-TYPE", FieldType.STRING, 
            8);
        pnd_Tbl_Prime_Key_Tbl_Doc_Type_P_S = pnd_Tbl_Prime_Key__R_Field_22.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Doc_Type_P_S", "TBL-DOC-TYPE-P-S", FieldType.STRING, 
            1);
        pnd_Doc_Type_Valid = localVariables.newFieldInRecord("pnd_Doc_Type_Valid", "#DOC-TYPE-VALID", FieldType.BOOLEAN, 1);
        pnd_Check_Doc_Type = localVariables.newFieldInRecord("pnd_Check_Doc_Type", "#CHECK-DOC-TYPE", FieldType.BOOLEAN, 1);
        pnd_Srvvr_Folder = localVariables.newFieldInRecord("pnd_Srvvr_Folder", "#SRVVR-FOLDER", FieldType.BOOLEAN, 1);
        pnd_Srvvr_Cnt = localVariables.newFieldInRecord("pnd_Srvvr_Cnt", "#SRVVR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ix = localVariables.newFieldInRecord("pnd_Ix", "#IX", FieldType.PACKED_DECIMAL, 3);
        pnd_Doc_Info_Arr = localVariables.newFieldArrayInRecord("pnd_Doc_Info_Arr", "#DOC-INFO-ARR", FieldType.STRING, 30, new DbsArrayController(1, 400));

        pnd_Doc_Info_Flds = localVariables.newGroupInRecord("pnd_Doc_Info_Flds", "#DOC-INFO-FLDS");
        pnd_Doc_Info_Flds_Document_Subtype = pnd_Doc_Info_Flds.newFieldInGroup("pnd_Doc_Info_Flds_Document_Subtype", "DOCUMENT-SUBTYPE", FieldType.STRING, 
            3);
        pnd_Doc_Info_Flds_Image_Source_Id = pnd_Doc_Info_Flds.newFieldInGroup("pnd_Doc_Info_Flds_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6);
        pnd_Doc_Info_Flds_Image_Min = pnd_Doc_Info_Flds.newFieldInGroup("pnd_Doc_Info_Flds_Image_Min", "IMAGE-MIN", FieldType.STRING, 14);
        pnd_Doc_Info_Flds_Text_Pointer = pnd_Doc_Info_Flds.newFieldInGroup("pnd_Doc_Info_Flds_Text_Pointer", "TEXT-POINTER", FieldType.NUMERIC, 7);

        pnd_Doc_Info_Flds__R_Field_23 = localVariables.newGroupInRecord("pnd_Doc_Info_Flds__R_Field_23", "REDEFINE", pnd_Doc_Info_Flds);
        pnd_Doc_Info_Flds_Pnd_Doc_Info = pnd_Doc_Info_Flds__R_Field_23.newFieldInGroup("pnd_Doc_Info_Flds_Pnd_Doc_Info", "#DOC-INFO", FieldType.STRING, 
            30);
        pnd_Report = localVariables.newFieldInRecord("pnd_Report", "#REPORT", FieldType.NUMERIC, 1);
        pnd_Assigned_Cnt = localVariables.newFieldInRecord("pnd_Assigned_Cnt", "#ASSIGNED-CNT", FieldType.PACKED_DECIMAL, 7);

        vw_cwf_Support_Tbl_Sb_Bene = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl_Sb_Bene", "CWF-SUPPORT-TBL-SB-BENE"), "CWF_SUPPORT_TBL", 
            "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Sb_Bene_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl_Sb_Bene.getRecord().newFieldInGroup("cwf_Support_Tbl_Sb_Bene_Tbl_Scrty_Level_Ind", 
            "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Sb_Bene_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Sb_Bene_Tbl_Actve_Ind = vw_cwf_Support_Tbl_Sb_Bene.getRecord().newFieldInGroup("cwf_Support_Tbl_Sb_Bene_Tbl_Actve_Ind", "TBL-ACTVE-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Sb_Bene_Tbl_Table_Nme = vw_cwf_Support_Tbl_Sb_Bene.getRecord().newFieldInGroup("cwf_Support_Tbl_Sb_Bene_Tbl_Table_Nme", "TBL-TABLE-NME", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Sb_Bene_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Sb_Bene_Tbl_Key_Field = vw_cwf_Support_Tbl_Sb_Bene.getRecord().newFieldInGroup("cwf_Support_Tbl_Sb_Bene_Tbl_Key_Field", "TBL-KEY-FIELD", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Sb_Bene_Tbl_Data_Field = vw_cwf_Support_Tbl_Sb_Bene.getRecord().newFieldInGroup("cwf_Support_Tbl_Sb_Bene_Tbl_Data_Field", "TBL-DATA-FIELD", 
            FieldType.STRING, 253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl_Sb_Bene__R_Field_24 = vw_cwf_Support_Tbl_Sb_Bene.getRecord().newGroupInGroup("cwf_Support_Tbl_Sb_Bene__R_Field_24", "REDEFINE", 
            cwf_Support_Tbl_Sb_Bene_Tbl_Data_Field);
        cwf_Support_Tbl_Sb_Bene_Pnd_Tbl_Data_Field1 = cwf_Support_Tbl_Sb_Bene__R_Field_24.newFieldInGroup("cwf_Support_Tbl_Sb_Bene_Pnd_Tbl_Data_Field1", 
            "#TBL-DATA-FIELD1", FieldType.NUMERIC, 8);
        cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Dte = vw_cwf_Support_Tbl_Sb_Bene.getRecord().newFieldInGroup("cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Dte", "TBL-UPDTE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl_Sb_Bene.getRecord().newFieldInGroup("cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Dte_Tme", 
            "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl_Sb_Bene.getRecord().newFieldInGroup("cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Oprtr_Cde", 
            "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Support_Tbl_Sb_Bene);

        pnd_Sup_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Sup_Tbl_Prime_Key", "#SUP-TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Sup_Tbl_Prime_Key__R_Field_25 = localVariables.newGroupInRecord("pnd_Sup_Tbl_Prime_Key__R_Field_25", "REDEFINE", pnd_Sup_Tbl_Prime_Key);
        pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Scrty_Level_Ind = pnd_Sup_Tbl_Prime_Key__R_Field_25.newFieldInGroup("pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Scrty_Level_Ind", 
            "#SUP-TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Table_Nme = pnd_Sup_Tbl_Prime_Key__R_Field_25.newFieldInGroup("pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Table_Nme", 
            "#SUP-TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Key_Field = pnd_Sup_Tbl_Prime_Key__R_Field_25.newFieldInGroup("pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Key_Field", 
            "#SUP-TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Actve_Ind = pnd_Sup_Tbl_Prime_Key__R_Field_25.newFieldInGroup("pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Actve_Ind", 
            "#SUP-TBL-ACTVE-IND", FieldType.STRING, 1);
        pnd_Last_Upd_Date_Sup_Tbl = localVariables.newFieldInRecord("pnd_Last_Upd_Date_Sup_Tbl", "#LAST-UPD-DATE-SUP-TBL", FieldType.STRING, 8);
        pnd_Last_Upd_Date_Sb_Bene = localVariables.newFieldInRecord("pnd_Last_Upd_Date_Sb_Bene", "#LAST-UPD-DATE-SB-BENE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Sb_Work_Req.reset();
        vw_cwf_Sb_Work_Req_2.reset();
        vw_cwf_Sb_Bene.reset();
        vw_cwf_Master_1.reset();
        vw_cwf_Master_2.reset();
        vw_cwf_Efm_Folder.reset();
        vw_cwf_Efm_Document.reset();
        vw_cwf_Support_Tbl.reset();
        vw_cwf_Support_Tbl_Sb_Bene.reset();

        localVariables.reset();
        pnd_Srvvr_Key.setInitialValue("S");
        pnd_Misc_Wpid.setInitialValue("ZZ4");
        pnd_Curr_Date_N8.setInitialValue(Global.getDATN());
        pnd_Report.setInitialValue(1);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Swfb6610() throws Exception
    {
        super("Swfb6610");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 80 PS = 54 SG = OFF
        getReports().print(0, "START OF PROCESS",Global.getDATX(),Global.getTIMX());                                                                                      //Natural: PRINT 'START OF PROCESS' *DATX *TIMX
        pnd_Sup_Tbl_Prime_Key.reset();                                                                                                                                    //Natural: RESET #SUP-TBL-PRIME-KEY #LAST-UPD-DATE-SUP-TBL
        pnd_Last_Upd_Date_Sup_Tbl.reset();
        pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Scrty_Level_Ind.setValue("A ");                                                                                                 //Natural: ASSIGN #SUP-TBL-SCRTY-LEVEL-IND := 'A '
        pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Table_Nme.setValue("P5030CWD-RUN-DATE");                                                                                        //Natural: ASSIGN #SUP-TBL-TABLE-NME := 'P5030CWD-RUN-DATE'
        pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Key_Field.setValue("P5030CWD-RUN-DATE-KEY");                                                                                    //Natural: ASSIGN #SUP-TBL-KEY-FIELD := 'P5030CWD-RUN-DATE-KEY'
        pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Actve_Ind.setValue(" ");                                                                                                        //Natural: ASSIGN #SUP-TBL-ACTVE-IND := ' '
        vw_cwf_Support_Tbl_Sb_Bene.startDatabaseFind                                                                                                                      //Natural: FIND CWF-SUPPORT-TBL-SB-BENE WITH TBL-PRIME-KEY = #SUP-TBL-PRIME-KEY
        (
        "F2",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Sup_Tbl_Prime_Key, WcType.WITH) }
        );
        F2:
        while (condition(vw_cwf_Support_Tbl_Sb_Bene.readNextRow("F2", true)))
        {
            vw_cwf_Support_Tbl_Sb_Bene.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Support_Tbl_Sb_Bene.getAstCOUNTER().equals(0)))                                                                                          //Natural: IF NO RECORDS FOUND
            {
                getReports().print(0, "Read: No P5030CWD-RUN-DATE Record Found!");                                                                                        //Natural: PRINT 'Read: No P5030CWD-RUN-DATE Record Found!'
                DbsUtil.terminate(4);  if (true) return;                                                                                                                  //Natural: TERMINATE 4
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Last_Upd_Date_Sup_Tbl.setValue(cwf_Support_Tbl_Sb_Bene_Pnd_Tbl_Data_Field1);                                                                              //Natural: MOVE CWF-SUPPORT-TBL-SB-BENE.#TBL-DATA-FIELD1 TO #LAST-UPD-DATE-SUP-TBL
            getReports().print(0, "=",pnd_Last_Upd_Date_Sup_Tbl);                                                                                                         //Natural: PRINT '=' #LAST-UPD-DATE-SUP-TBL
            if (condition(pnd_Last_Upd_Date_Sup_Tbl.equals(" ")))                                                                                                         //Natural: IF #LAST-UPD-DATE-SUP-TBL EQ ' '
            {
                getReports().print(0, "Read: P5030CWD-RUN-DATE Record is Blank!");                                                                                        //Natural: PRINT 'Read: P5030CWD-RUN-DATE Record is Blank!'
                DbsUtil.terminate(4);  if (true) return;                                                                                                                  //Natural: TERMINATE 4
            }                                                                                                                                                             //Natural: END-IF
            //*  (F2.)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //* *  READ SURVIVORS THAT DON'T HAVE PIN AND TRY TO GET FROM COR
        //* ***********************************************************************
        vw_cwf_Sb_Bene.startDatabaseRead                                                                                                                                  //Natural: READ CWF-SB-BENE BY SRVVR-KEY EQ 'S'
        (
        "R1",
        new Wc[] { new Wc("SRVVR_KEY", ">=", "S", WcType.BY) },
        new Oc[] { new Oc("SRVVR_KEY", "ASC") }
        );
        R1:
        while (condition(vw_cwf_Sb_Bene.readNextRow("R1")))
        {
            if (condition(cwf_Sb_Bene_Rcrd_Type.notEquals("S")))                                                                                                          //Natural: IF CWF-SB-BENE.RCRD-TYPE NE 'S'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(cwf_Sb_Bene_Srvvr_Pin.equals(getZero()))))                                                                                                    //Natural: ACCEPT IF CWF-SB-BENE.SRVVR-PIN EQ 0
            {
                continue;
            }
            //* *---  READ THE COR FILE TO GET THE PIN
            //*  COR/NAAD SUNSET START
            //*  #COR-SUPER-SSN-RCDTYPE.PH-SSN     := CWF-SB-BENE.SRVVR-SSN-NBR-N9
            //*  #COR-SUPER-SSN-RCDTYPE.PH-RECTYPE := 01
            //*  FIND COR-XREF-PH WITH COR-SUPER-SSN-RCDTYPE EQ
            //*      #COR-SUPER-SSN-RCDTYPE
            //*  END-FIND
            //*  IF COR-XREF-PH.PH-UNIQUE-ID-NBR EQ 0  /*  IF NOT IN COR THEN
            //*    ESCAPE TOP
            //*  END-IF
            //*  G1. GET CWF-SB-BENE *ISN(R1.)         /*  ELSE UPDATE PIN OF SRVVR
            //*  CWF-SB-BENE.SRVVR-PIN := COR-XREF-PH.PH-UNIQUE-ID-NBR
            //*  WRITE (1)
            //*    CWF-SB-BENE.PH-PIN-NBR                         /*PIN-EXP <<
            //*    CWF-SB-BENE.PH-PIN-NBR (EM=999999999999)
            //*    3X CWF-SB-BENE.SRVVR-MIT-SQNCE
            //*    2X CWF-SB-BENE.SRVVR-MIT-SQNCE
            //*    2X CWF-SB-BENE.SRVVR-NME-FR-FRM
            //*    7X CWF-SB-BENE.SRVVR-NME-FR-FRM
            //*    COR-XREF-PH.PH-UNIQUE-ID-NBR
            //*    COR-XREF-PH.PH-UNIQUE-ID-NBR (EM=999999999999) /*PIN-EXP >>
            pnd_Last_Upd_Date_Sb_Bene.reset();                                                                                                                            //Natural: RESET #LAST-UPD-DATE-SB-BENE
            pnd_Last_Upd_Date_Sb_Bene.setValueEdited(cwf_Sb_Bene_Last_Updte_Dte_Tme,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED CWF-SB-BENE.LAST-UPDTE-DTE-TME ( EM = YYYYMMDD ) TO #LAST-UPD-DATE-SB-BENE
            if (condition(pnd_Last_Upd_Date_Sb_Bene.greaterOrEqual(pnd_Last_Upd_Date_Sup_Tbl)))                                                                           //Natural: IF #LAST-UPD-DATE-SB-BENE GE #LAST-UPD-DATE-SUP-TBL
            {
                if (condition(cwf_Sb_Bene_Srvvr_Ssn_Nbr_N9.notEquals(getZero())))                                                                                         //Natural: IF CWF-SB-BENE.SRVVR-SSN-NBR-N9 NE 0
                {
                    pdaMdma101.getPnd_Mdma101().reset();                                                                                                                  //Natural: RESET #MDMA101
                    pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn().setValue(cwf_Sb_Bene_Srvvr_Ssn_Nbr_N9);                                                                         //Natural: ASSIGN #MDMA101.#I-SSN := CWF-SB-BENE.SRVVR-SSN-NBR-N9
                    DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                          //Natural: FETCH RETURN 'MDMP0011'
                    if (condition(Global.isEscape())) return;
                    if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                    //Natural: IF *DEVICE = 'BATCH'
                    {
                        //*  CALLNAT 'MDMN100A' #MDMA100
                        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
                        if (condition(Global.isEscape())) return;
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  CALLNAT 'MDMN100' #MDMA100
                        DbsUtil.callnat(Mdmn101.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                           //Natural: CALLNAT 'MDMN101' #MDMA101
                        if (condition(Global.isEscape())) return;
                    }                                                                                                                                                     //Natural: END-IF
                    DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                          //Natural: FETCH RETURN 'MDMP0012'
                    if (condition(Global.isEscape())) return;
                    if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().notEquals("0000") || pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12().equals(getZero())))        //Natural: IF #MDMA101.#O-RETURN-CODE NE '0000' OR #MDMA101.#O-PIN-N12 EQ 0
                    {
                        //*   IF NOT IN COR THEN
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*   ELSE UPDATE PIN OF SRVVR
                    }                                                                                                                                                     //Natural: END-IF
                    G1:                                                                                                                                                   //Natural: GET CWF-SB-BENE *ISN ( R1. )
                    vw_cwf_Sb_Bene.readByID(vw_cwf_Sb_Bene.getAstISN("R1"), "G1");
                    cwf_Sb_Bene_Srvvr_Pin.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12());                                                                            //Natural: ASSIGN CWF-SB-BENE.SRVVR-PIN := #O-PIN-N12
                    //* PIN-EXP >>
                    getReports().write(1, ReportOption.NOTITLE,cwf_Sb_Bene_Ph_Pin_Nbr, new ReportEditMask ("999999999999"),new ColumnSpacing(2),cwf_Sb_Bene_Srvvr_Mit_Sqnce,new  //Natural: WRITE ( 1 ) CWF-SB-BENE.PH-PIN-NBR ( EM = 999999999999 ) 2X CWF-SB-BENE.SRVVR-MIT-SQNCE 2X CWF-SB-BENE.SRVVR-NME-FR-FRM #O-PIN-N12 ( EM = 999999999999 )
                        ColumnSpacing(2),cwf_Sb_Bene_Srvvr_Nme_Fr_Frm,pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12(), new ReportEditMask ("999999999999"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*    CWF-SB-BENE.PH-PIN-NBR
                    //*    3X CWF-SB-BENE.SRVVR-MIT-SQNCE
                    //*    7X CWF-SB-BENE.SRVVR-NME-FR-FRM
                    //*    COR-XREF-PH.PH-UNIQUE-ID-NBR
                    //*  COR/NAAD SUNSET END
                    pnd_Assigned_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #ASSIGNED-CNT
                    vw_cwf_Sb_Bene.updateDBRow("G1");                                                                                                                     //Natural: UPDATE ( G1. )
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM UPDATE-RUN-DATE
        sub_Update_Run_Date();
        if (condition(Global.isEscape())) {return;}
        getReports().print(0, "END OF PROCESS",Global.getDATX(),Global.getTIMX());                                                                                        //Natural: PRINT 'END OF PROCESS' *DATX *TIMX
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"TOTAL NUMBER OF SURVIVORS ASSIGNED WITH PIN:",pnd_Assigned_Cnt);                                      //Natural: WRITE ( 1 ) //'TOTAL NUMBER OF SURVIVORS ASSIGNED WITH PIN:' #ASSIGNED-CNT
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Report.setValue(2);                                                                                                                                           //Natural: ASSIGN #REPORT = 2
        //* ***********************************************************************
        //* *  SPLIT PROCESS
        //* ***********************************************************************
        //* *---  READ SURVIVOR WORK REQUEST WITH SPLIT INDICATOR ON
        vw_cwf_Sb_Work_Req.startDatabaseRead                                                                                                                              //Natural: READ CWF-SB-WORK-REQ BY SPLIT-KEY
        (
        "R2",
        new Oc[] { new Oc("SPLIT_KEY", "ASC") }
        );
        R2:
        while (condition(vw_cwf_Sb_Work_Req.readNextRow("R2")))
        {
            //* *---  MAKE SURE THE SRVVR ALREADY HAS A PIN
            pnd_Srvvr_Key_Srvvr_Mit_Idntfr.setValue(cwf_Sb_Work_Req_Srvvr_Mit_Idntfr);                                                                                    //Natural: ASSIGN #SRVVR-KEY.SRVVR-MIT-IDNTFR := CWF-SB-WORK-REQ.SRVVR-MIT-IDNTFR
            vw_cwf_Sb_Bene.startDatabaseFind                                                                                                                              //Natural: FIND CWF-SB-BENE WITH SRVVR-KEY EQ #SRVVR-KEY
            (
            "FIND01",
            new Wc[] { new Wc("SRVVR_KEY", "=", pnd_Srvvr_Key, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_cwf_Sb_Bene.readNextRow("FIND01")))
            {
                vw_cwf_Sb_Bene.setIfNotFoundControlFlag(false);
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(cwf_Sb_Bene_Srvvr_Pin.equals(getZero())))                                                                                                       //Natural: IF CWF-SB-BENE.SRVVR-PIN EQ 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //* *---  CHECK MIT WORK RQST
            vw_cwf_Master_1.startDatabaseFind                                                                                                                             //Natural: FIND CWF-MASTER-1 WITH ACTV-UNQUE-KEY EQ CWF-SB-WORK-REQ.RQST-LOG-DTE-TME
            (
            "FIND02",
            new Wc[] { new Wc("ACTV_UNQUE_KEY", "=", cwf_Sb_Work_Req_Rqst_Log_Dte_Tme, WcType.WITH) }
            );
            FIND02:
            while (condition(vw_cwf_Master_1.readNextRow("FIND02")))
            {
                vw_cwf_Master_1.setIfNotFoundControlFlag(false);
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(cwf_Master_1_Status_Cde.equals(" ")))                                                                                                           //Natural: IF CWF-MASTER-1.STATUS-CDE EQ ' '
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*** ERROR:  MIT work request record is missing.",cwf_Sb_Work_Req_Ph_Pin_Nbr,cwf_Sb_Work_Req_Srvvr_Mit_Sqnce, //Natural: WRITE ( 1 ) / '*** ERROR:  MIT work request record is missing.' CWF-SB-WORK-REQ.PH-PIN-NBR CWF-SB-WORK-REQ.SRVVR-MIT-SQNCE CWF-SB-WORK-REQ.RQST-LOG-DTE-TME
                    cwf_Sb_Work_Req_Rqst_Log_Dte_Tme);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //* *---  SPLIT ONLY IF REQUEST IS CORPORATELY CLOSE
            if (condition(cwf_Master_1_Crprte_Status_Ind.notEquals("9")))                                                                                                 //Natural: IF CWF-MASTER-1.CRPRTE-STATUS-IND NE '9'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //* *---  READ ALL MIT REQUESTS/FOLDERS
            pnd_Doc_Info_Arr.getValue("*").reset();                                                                                                                       //Natural: RESET #DOC-INFO-ARR ( * ) #DOC-INFO #IX
            pnd_Doc_Info_Flds_Pnd_Doc_Info.reset();
            pnd_Ix.reset();
            pnd_First_Doc.setValue(true);                                                                                                                                 //Natural: ASSIGN #FIRST-DOC := TRUE
            vw_cwf_Master_1.startDatabaseRead                                                                                                                             //Natural: READ CWF-MASTER-1 BY PIN-HISTORY-KEY EQ CWF-SB-WORK-REQ.PH-PIN-NBR
            (
            "READ01",
            new Wc[] { new Wc("PIN_HISTORY_KEY", ">=", cwf_Sb_Work_Req_Ph_Pin_Nbr, WcType.BY) },
            new Oc[] { new Oc("PIN_HISTORY_KEY", "ASC") }
            );
            READ01:
            while (condition(vw_cwf_Master_1.readNextRow("READ01")))
            {
                if (condition(cwf_Master_1_Pin_Nbr.greater(cwf_Sb_Work_Req_Ph_Pin_Nbr)))                                                                                  //Natural: IF CWF-MASTER-1.PIN-NBR GT CWF-SB-WORK-REQ.PH-PIN-NBR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*   NO FOLDER
                if (condition(cwf_Master_1_Elctrnc_Fldr_Ind.equals(" ")))                                                                                                 //Natural: IF CWF-MASTER-1.ELCTRNC-FLDR-IND EQ ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //* *---  CHECK IF THIS IS THE REQUEST BEING PROCESSED IF NOT THEN
                //* *---  THE DOC-TYPES MUST BE CHECKED AGAINST THE TABLE
                pnd_Check_Doc_Type.reset();                                                                                                                               //Natural: RESET #CHECK-DOC-TYPE #SRVVR-FOLDER
                pnd_Srvvr_Folder.reset();
                if (condition(cwf_Master_1_Rqst_Log_Dte_Tme.notEquals(cwf_Sb_Work_Req_Rqst_Log_Dte_Tme)))                                                                 //Natural: IF CWF-MASTER-1.RQST-LOG-DTE-TME NE CWF-SB-WORK-REQ.RQST-LOG-DTE-TME
                {
                    pnd_Check_Doc_Type.setValue(true);                                                                                                                    //Natural: ASSIGN #CHECK-DOC-TYPE := TRUE
                    //* *---  CHK IF RQST IS FOR THIS SRVVR BEING PROCESSED
                    pnd_Work_Req_Key_Srvvr_Mit_Idntfr.setValue(cwf_Sb_Work_Req_Srvvr_Mit_Idntfr);                                                                         //Natural: ASSIGN #WORK-REQ-KEY.SRVVR-MIT-IDNTFR := CWF-SB-WORK-REQ.SRVVR-MIT-IDNTFR
                    pnd_Work_Req_Key_Rqst_Log_Dte_Tme.setValue(cwf_Master_1_Rqst_Log_Dte_Tme);                                                                            //Natural: ASSIGN #WORK-REQ-KEY.RQST-LOG-DTE-TME := CWF-MASTER-1.RQST-LOG-DTE-TME
                    vw_cwf_Sb_Work_Req_2.startDatabaseFind                                                                                                                //Natural: FIND CWF-SB-WORK-REQ-2 WITH WORK-REQ-KEY EQ #WORK-REQ-KEY
                    (
                    "FIND03",
                    new Wc[] { new Wc("WORK_REQ_KEY", "=", pnd_Work_Req_Key, WcType.WITH) }
                    );
                    FIND03:
                    while (condition(vw_cwf_Sb_Work_Req_2.readNextRow("FIND03")))
                    {
                        vw_cwf_Sb_Work_Req_2.setIfNotFoundControlFlag(false);
                        pnd_Srvvr_Folder.setValue(true);                                                                                                                  //Natural: ASSIGN #SRVVR-FOLDER := TRUE
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM COPY-DOCUMENTS
                sub_Copy_Documents();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().greater(" ")))                                                                                    //Natural: IF ##MSG GT ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *---  RESET THE SPLIT INDICATOR
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().equals(" ")))                                                                                         //Natural: IF ##MSG EQ ' '
            {
                G2:                                                                                                                                                       //Natural: GET CWF-SB-WORK-REQ *ISN ( R2. )
                vw_cwf_Sb_Work_Req.readByID(vw_cwf_Sb_Work_Req.getAstISN("R2"), "G2");
                cwf_Sb_Work_Req_Split_Ind.setValue(" ");                                                                                                                  //Natural: ASSIGN CWF-SB-WORK-REQ.SPLIT-IND := ' '
                vw_cwf_Sb_Work_Req.updateDBRow("G2");                                                                                                                     //Natural: UPDATE ( G2. )
                pnd_Srvvr_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #SRVVR-CNT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*** ERROR: ",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(), new AlphanumericLength (66));                 //Natural: WRITE ( 1 ) / '*** ERROR: ' ##MSG ( AL = 66 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* ***********************************************************************
        //*    18T CWF-EFM-DOCUMENT.CABINET-ID 28T #SRC-FOLDER (IS=ON)
        //* ***********************************************************************
        //* *---EFSA9120.OLD-DOCUMENT-NAME =
        //* *EFSA9120.OLD-DOC-PACKAGE-NBR
        //* ***********************************************************************
        //* ***********************************************************************
        //* *  INIT THESE FOR 'CD'(COPY DOC) USE
        //* *SA9120.CASE-ID       (1)   := #DEST-FOLDER.CASE-IND
        //* *SA9120.SUB-RQST-CDE  (1)   := #DEST-FOLDER.MULTI-RQST-IND
        //* *SA9120.MULTI-RQST-IND(1)   := #DEST-FOLDER.SUB-RQST-IND
        //* *SA9120.RQST-ENTRY-OP-CDE   := CWF-SB-WORK-REQ.LAST-UPDTE-OPRTR-CDE
        //* *SA9120.BATCH-NBR           := 0
        //* *---
        //*   THE FOLLOWING IS NECESSARY BECAUSE THE CD ACTION ON THE CONTROL
        //*   MODULE DOES NOT SET THE ELCTRNC-FLDR-IND FOR ZZ* WPID
        //* ***********************************************************************
        //*  COR/NAAD SUNSET START
        //* ********************************
        //*  COR/NAAD SUNSET END
        //* ***********************************************************************
        //*   WRITE REPORT HEADING
        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"TOTAL NUMBER OF SRVVRS PROCESSED:",pnd_Srvvr_Cnt);                                            //Natural: AT TOP OF PAGE ( 1 );//Natural: WRITE ( 1 ) /// 'TOTAL NUMBER OF SRVVRS PROCESSED:' #SRVVR-CNT
        if (Global.isEscape()) return;
    }
    private void sub_Copy_Documents() throws Exception                                                                                                                    //Natural: COPY-DOCUMENTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *---  CHECK IF A MISC FOLDER ALREADY EXISTS
        if (condition(pnd_First_Doc.getBoolean()))                                                                                                                        //Natural: IF #FIRST-DOC
        {
                                                                                                                                                                          //Natural: PERFORM CHECK-MISC-FOLDER
            sub_Check_Misc_Folder();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *---  READ ALL DOCUMENTS OF THE WORK REQUEST
        pnd_Doc_Key.reset();                                                                                                                                              //Natural: RESET #DOC-KEY
        pnd_Doc_Key_Cab_Prefix.setValue("P");                                                                                                                             //Natural: ASSIGN #DOC-KEY.CAB-PREFIX := 'P'
        pnd_Doc_Key_Cab_Pin.setValue(cwf_Master_1_Rqst_Pin_Nbr);                                                                                                          //Natural: ASSIGN #DOC-KEY.CAB-PIN := CWF-MASTER-1.RQST-PIN-NBR
        pnd_Doc_Key_Tiaa_Rcvd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Master_1_Rqst_Tiaa_Rcvd_Dte);                                                         //Natural: MOVE EDITED CWF-MASTER-1.RQST-TIAA-RCVD-DTE TO #DOC-KEY.TIAA-RCVD-DTE ( EM = YYYYMMDD )
        pnd_Doc_Key_Case_Ind.setValue(cwf_Master_1_Rqst_Case_Id_Ind);                                                                                                     //Natural: ASSIGN #DOC-KEY.CASE-IND := CWF-MASTER-1.RQST-CASE-ID-IND
        pnd_Doc_Key_Originating_Wpid.setValue(cwf_Master_1_Rqst_Work_Prcss_Id);                                                                                           //Natural: ASSIGN #DOC-KEY.ORIGINATING-WPID := CWF-MASTER-1.RQST-WORK-PRCSS-ID
        pnd_Doc_Key_Multi_Rqst_Ind.setValue(cwf_Master_1_Multi_Rqst_Ind);                                                                                                 //Natural: ASSIGN #DOC-KEY.MULTI-RQST-IND := CWF-MASTER-1.MULTI-RQST-IND
        pnd_Doc_Key_Sub_Rqst_Ind.setValue(cwf_Master_1_Rqst_Sub_Rqst_Cde);                                                                                                //Natural: ASSIGN #DOC-KEY.SUB-RQST-IND := CWF-MASTER-1.RQST-SUB-RQST-CDE
        vw_cwf_Efm_Document.startDatabaseRead                                                                                                                             //Natural: READ CWF-EFM-DOCUMENT BY DOCUMENT-KEY EQ #DOC-KY
        (
        "READ02",
        new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Doc_Key_Pnd_Doc_Ky.getBinary(), WcType.BY) },
        new Oc[] { new Oc("DOCUMENT_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf_Efm_Document.readNextRow("READ02")))
        {
            if (condition(cwf_Efm_Document_Cabinet_Id.notEquals(pnd_Doc_Key_Cabinet_Id) || cwf_Efm_Document_Tiaa_Rcvd_Dte.notEquals(pnd_Doc_Key_Tiaa_Rcvd_Dte)            //Natural: IF CWF-EFM-DOCUMENT.CABINET-ID NE #DOC-KEY.CABINET-ID OR CWF-EFM-DOCUMENT.TIAA-RCVD-DTE NE #DOC-KEY.TIAA-RCVD-DTE OR CWF-EFM-DOCUMENT.CASE-WORKPRCSS-MULTI-SUBRQST NE #DOC-KEY.CASE-WORKPRCSS-MULTI-SUBRQST
                || cwf_Efm_Document_Case_Workprcss_Multi_Subrqst.notEquals(pnd_Doc_Key_Case_Workprcss_Multi_Subrqst)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Efm_Document_Document_Retention_Ind.equals("D")))                                                                                           //Natural: REJECT IF CWF-EFM-DOCUMENT.DOCUMENT-RETENTION-IND = 'D'
            {
                continue;
            }
            //* *---  CHECK DOCUMENT TYPE
            //*   PARTICIPANT LEVEL
            if (condition(pnd_Check_Doc_Type.getBoolean()))                                                                                                               //Natural: IF #CHECK-DOC-TYPE
            {
                pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind.setValue("S");                                                                                                      //Natural: ASSIGN #TBL-PRIME-KEY.TBL-SCRTY-LEVEL-IND := 'S'
                pnd_Tbl_Prime_Key_Tbl_Table_Nme.setValue("CWF-SB-SPLIT-DOCTYPS");                                                                                         //Natural: ASSIGN #TBL-PRIME-KEY.TBL-TABLE-NME := 'CWF-SB-SPLIT-DOCTYPS'
                pnd_Tbl_Prime_Key_Tbl_Doc_Type.setValue(cwf_Efm_Document_Doc_Type);                                                                                       //Natural: ASSIGN #TBL-PRIME-KEY.TBL-DOC-TYPE := CWF-EFM-DOCUMENT.DOC-TYPE
                pnd_Tbl_Prime_Key_Tbl_Doc_Type_P_S.setValue("P");                                                                                                         //Natural: ASSIGN #TBL-PRIME-KEY.TBL-DOC-TYPE-P-S := 'P'
                pnd_Doc_Type_Valid.reset();                                                                                                                               //Natural: RESET #DOC-TYPE-VALID
                vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                      //Natural: FIND ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY EQ #TBL-PRIME-KEY
                (
                "FIND04",
                new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
                1
                );
                FIND04:
                while (condition(vw_cwf_Support_Tbl.readNextRow("FIND04")))
                {
                    vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
                    pnd_Doc_Type_Valid.setValue(true);                                                                                                                    //Natural: ASSIGN #DOC-TYPE-VALID := TRUE
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   SURVIVOR LEVEL
                if (condition(pnd_Srvvr_Folder.getBoolean() && ! (pnd_Doc_Type_Valid.getBoolean())))                                                                      //Natural: IF #SRVVR-FOLDER AND NOT #DOC-TYPE-VALID
                {
                    pnd_Tbl_Prime_Key_Tbl_Doc_Type_P_S.setValue("S");                                                                                                     //Natural: ASSIGN #TBL-PRIME-KEY.TBL-DOC-TYPE-P-S := 'S'
                    vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY EQ #TBL-PRIME-KEY
                    (
                    "FIND05",
                    new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
                    1
                    );
                    FIND05:
                    while (condition(vw_cwf_Support_Tbl.readNextRow("FIND05")))
                    {
                        vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
                        pnd_Doc_Type_Valid.setValue(true);                                                                                                                //Natural: ASSIGN #DOC-TYPE-VALID := TRUE
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*   NOT IN THE TBL OF DOCS TO BE COPIED
                if (condition(! (pnd_Doc_Type_Valid.getBoolean())))                                                                                                       //Natural: IF NOT #DOC-TYPE-VALID
                {
                    //*   THEREFORE IGNORE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *---  IF THIS IS THE FIRST DOCUMENT THEN LOG A REQUEST
            //*   IF TRUE THEN NO MISC FOLDER EXISTS YET
            if (condition(pnd_First_Doc.getBoolean()))                                                                                                                    //Natural: IF #FIRST-DOC
            {
                //*   THEREFORE WE NEED TO LOG A REQUEST
                pnd_First_Doc.reset();                                                                                                                                    //Natural: RESET #FIRST-DOC
                                                                                                                                                                          //Natural: PERFORM LOG-CLOSE-RQST
                sub_Log_Close_Rqst();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,cwf_Sb_Bene_Srvvr_Pin,pnd_Dest_Folder,new ColumnSpacing(3),"* * *  Miscellaneous request logged.");    //Natural: WRITE ( 1 ) / CWF-SB-BENE.SRVVR-PIN #DEST-FOLDER 3X '* * *  Miscellaneous request logged.'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   ERROR IN LOGGING
                if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().greater(" ")))                                                                                    //Natural: IF ##MSG GT ' '
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *---  CHECK IF DOC HAS ALREADY BEEN COPIED
            pnd_Doc_Info_Flds.setValuesByName(vw_cwf_Efm_Document);                                                                                                       //Natural: MOVE BY NAME CWF-EFM-DOCUMENT TO #DOC-INFO-FLDS
            //*   IT'S ALREADY BEEN COPIED
            if (condition(pnd_Doc_Info_Flds_Pnd_Doc_Info.equals(pnd_Doc_Info_Arr.getValue("*"))))                                                                         //Natural: IF #DOC-INFO EQ #DOC-INFO-ARR ( * )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*   OTHERWISE
            pnd_Ix.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #IX
            //*   ADD TO ARRAY
            pnd_Doc_Info_Arr.getValue(pnd_Ix).setValue(pnd_Doc_Info_Flds_Pnd_Doc_Info);                                                                                   //Natural: ASSIGN #DOC-INFO-ARR ( #IX ) = #DOC-INFO
            //* *---  COPY EACH DOCUMENT TO THE SURVIVOR (NEW PARTICIPANT) FOLDER
            //*   COPY THIS DOCUMENT
                                                                                                                                                                          //Natural: PERFORM CALL-CONTROL-MODULE
            sub_Call_Control_Module();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(18),cwf_Efm_Document_Cabinet_Id,new TabSetting(33),pnd_Src_Folder, new IdenticalSuppress(true),     //Natural: WRITE ( 1 ) 18T CWF-EFM-DOCUMENT.CABINET-ID 33T #SRC-FOLDER ( IS = ON ) CWF-EFM-DOCUMENT.DOCUMENT-SUBTYPE CWF-EFM-DOCUMENT.BUSINESS-DOCUMENT-DESCRIPTION
                cwf_Efm_Document_Document_Subtype,cwf_Efm_Document_Business_Document_Description);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   IF ERROR
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().greater(" ")))                                                                                        //Natural: IF ##MSG GT ' '
            {
                //*   BACKOUT THE WHOLE FOLDER
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Call_Control_Module() throws Exception                                                                                                               //Natural: CALL-CONTROL-MODULE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *---  INIT THESE FIELDS FOR REPORT PURPOSE
        pnd_Src_Folder_Tiaa_Rcvd_Dte.setValueEdited(cwf_Efm_Document_Tiaa_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                                       //Natural: MOVE EDITED CWF-EFM-DOCUMENT.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO #SRC-FOLDER.TIAA-RCVD-DTE
        pnd_Src_Folder_Case_Ind.setValue(cwf_Efm_Document_Case_Ind);                                                                                                      //Natural: ASSIGN #SRC-FOLDER.CASE-IND := CWF-EFM-DOCUMENT.CASE-IND
        pnd_Src_Folder_Originating_Wpid.setValue(cwf_Efm_Document_Originating_Wpid);                                                                                      //Natural: ASSIGN #SRC-FOLDER.ORIGINATING-WPID := CWF-EFM-DOCUMENT.ORIGINATING-WPID
        pnd_Src_Folder_Multi_Rqst_Ind.setValue(cwf_Efm_Document_Multi_Rqst_Ind);                                                                                          //Natural: ASSIGN #SRC-FOLDER.MULTI-RQST-IND := CWF-EFM-DOCUMENT.MULTI-RQST-IND
        pnd_Src_Folder_Sub_Rqst_Ind.setValue(cwf_Efm_Document_Sub_Rqst_Ind);                                                                                              //Natural: ASSIGN #SRC-FOLDER.SUB-RQST-IND := CWF-EFM-DOCUMENT.SUB-RQST-IND
        //* *---
        pdaEfsa9120.getEfsa9120().reset();                                                                                                                                //Natural: RESET EFSA9120
        pdaEfsa9120.getEfsa9120_Action().setValue("CD");                                                                                                                  //Natural: ASSIGN EFSA9120.ACTION := 'CD'
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: ASSIGN EFSA9120.CABINET-PREFIX := 'P'
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(cwf_Sb_Bene_Srvvr_Pin);                                                                                                //Natural: ASSIGN EFSA9120.PIN-NBR := CWF-SB-BENE.SRVVR-PIN
        pdaEfsa9120.getEfsa9120_Tiaa_Rcvd_Dte().getValue(1).setValue(pnd_Dest_Folder_Tiaa_Rcvd_Dte_N);                                                                    //Natural: ASSIGN EFSA9120.TIAA-RCVD-DTE ( 1 ) := #DEST-FOLDER.TIAA-RCVD-DTE-N
        pdaEfsa9120.getEfsa9120_Case_Id().getValue(1).setValue(pnd_Dest_Folder_Case_Ind);                                                                                 //Natural: ASSIGN EFSA9120.CASE-ID ( 1 ) := #DEST-FOLDER.CASE-IND
        pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue(pnd_Dest_Folder_Originating_Wpid);                                                                            //Natural: ASSIGN EFSA9120.WPID ( 1 ) := #DEST-FOLDER.ORIGINATING-WPID
        pdaEfsa9120.getEfsa9120_Sub_Rqst_Cde().getValue(1).setValue(pnd_Dest_Folder_Multi_Rqst_Ind);                                                                      //Natural: ASSIGN EFSA9120.SUB-RQST-CDE ( 1 ) := #DEST-FOLDER.MULTI-RQST-IND
        pdaEfsa9120.getEfsa9120_Multi_Rqst_Ind().getValue(1).setValue(pnd_Dest_Folder_Sub_Rqst_Ind);                                                                      //Natural: ASSIGN EFSA9120.MULTI-RQST-IND ( 1 ) := #DEST-FOLDER.SUB-RQST-IND
        pdaEfsa9120.getEfsa9120_Old_Cabinet().setValue(cwf_Efm_Document_Cabinet_Id);                                                                                      //Natural: ASSIGN EFSA9120.OLD-CABINET := CWF-EFM-DOCUMENT.CABINET-ID
        pnd_Date_A8.setValueEdited(cwf_Efm_Document_Tiaa_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED CWF-EFM-DOCUMENT.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO #DATE-A8
        pdaEfsa9120.getEfsa9120_Old_Tiaa_Rcvd_Date().setValue(pnd_Date_A8_Pnd_Date_N8);                                                                                   //Natural: ASSIGN EFSA9120.OLD-TIAA-RCVD-DATE := #DATE-N8
        pdaEfsa9120.getEfsa9120_Old_Case_Id().setValue(cwf_Efm_Document_Case_Ind);                                                                                        //Natural: ASSIGN EFSA9120.OLD-CASE-ID := CWF-EFM-DOCUMENT.CASE-IND
        pdaEfsa9120.getEfsa9120_Old_Wpid().setValue(cwf_Efm_Document_Originating_Wpid);                                                                                   //Natural: ASSIGN EFSA9120.OLD-WPID := CWF-EFM-DOCUMENT.ORIGINATING-WPID
        pdaEfsa9120.getEfsa9120_Old_Sub_Req_Cde().setValue(cwf_Efm_Document_Sub_Rqst_Ind);                                                                                //Natural: ASSIGN EFSA9120.OLD-SUB-REQ-CDE := CWF-EFM-DOCUMENT.SUB-RQST-IND
        pdaEfsa9120.getEfsa9120_Old_Multi_Rqst_Ind().setValue(cwf_Efm_Document_Multi_Rqst_Ind);                                                                           //Natural: ASSIGN EFSA9120.OLD-MULTI-RQST-IND := CWF-EFM-DOCUMENT.MULTI-RQST-IND
        pdaEfsa9120.getEfsa9120_Old_Doc_Category().setValue(cwf_Efm_Document_Document_Category);                                                                          //Natural: ASSIGN EFSA9120.OLD-DOC-CATEGORY := CWF-EFM-DOCUMENT.DOCUMENT-CATEGORY
        pdaEfsa9120.getEfsa9120_Old_Doc_Sub_Category().setValue(cwf_Efm_Document_Document_Sub_Category);                                                                  //Natural: ASSIGN EFSA9120.OLD-DOC-SUB-CATEGORY := CWF-EFM-DOCUMENT.DOCUMENT-SUB-CATEGORY
        pdaEfsa9120.getEfsa9120_Old_Doc_Detail().setValue(cwf_Efm_Document_Document_Detail);                                                                              //Natural: ASSIGN EFSA9120.OLD-DOC-DETAIL := CWF-EFM-DOCUMENT.DOCUMENT-DETAIL
        pdaEfsa9120.getEfsa9120_Old_Doc_Wpid().setValue(cwf_Efm_Document_Originating_Wpid);                                                                               //Natural: ASSIGN EFSA9120.OLD-DOC-WPID := CWF-EFM-DOCUMENT.ORIGINATING-WPID
        //* *---EFSA9120.OLD-DOC-CREATE-DT
        pnd_Date_A8.setValueEdited(cwf_Efm_Document_Entry_Dte_Tme,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED CWF-EFM-DOCUMENT.ENTRY-DTE-TME ( EM = YYYYMMDD ) TO #DATE-A8
        pdaEfsa9120.getEfsa9120_Old_Doc_Create_Yymmdd().setValueEdited(cwf_Efm_Document_Entry_Dte_Tme,new ReportEditMask("YYMMDD"));                                      //Natural: MOVE EDITED CWF-EFM-DOCUMENT.ENTRY-DTE-TME ( EM = YYMMDD ) TO EFSA9120.OLD-DOC-CREATE-YYMMDD
        if (condition(DbsUtil.maskMatches(pnd_Date_A8,"'20'")))                                                                                                           //Natural: IF #DATE-A8 = MASK ( '20' )
        {
            pdaEfsa9120.getEfsa9120_Old_Doc_Create_C_Yr().setValue("T");                                                                                                  //Natural: ASSIGN EFSA9120.OLD-DOC-CREATE-C-YR := 'T'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaEfsa9120.getEfsa9120_Old_Doc_Create_C_Yr().setValue("N");                                                                                                  //Natural: ASSIGN EFSA9120.OLD-DOC-CREATE-C-YR := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        pdaEfsa9120.getEfsa9120_Old_Doc_Direction().setValue(cwf_Efm_Document_Document_Direction_Ind);                                                                    //Natural: ASSIGN EFSA9120.OLD-DOC-DIRECTION := CWF-EFM-DOCUMENT.DOCUMENT-DIRECTION-IND
        pdaEfsa9120.getEfsa9120_Old_Doc_Security_Ind().setValue(cwf_Efm_Document_Secured_Document_Ind);                                                                   //Natural: ASSIGN EFSA9120.OLD-DOC-SECURITY-IND := CWF-EFM-DOCUMENT.SECURED-DOCUMENT-IND
        pdaEfsa9120.getEfsa9120_Old_Doc_Retention().setValue(cwf_Efm_Document_Document_Retention_Ind);                                                                    //Natural: ASSIGN EFSA9120.OLD-DOC-RETENTION := CWF-EFM-DOCUMENT.DOCUMENT-RETENTION-IND
        //* *---EFSA9120.OLD-DOC-FORMAT
        short decideConditionsMet1167 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-EFM-DOCUMENT.DOCUMENT-SUBTYPE EQ MASK ( 'T' )
        if (condition(DbsUtil.maskMatches(cwf_Efm_Document_Document_Subtype,"'T'")))
        {
            decideConditionsMet1167++;
            pdaEfsa9120.getEfsa9120_Old_Doc_Format().setValue("T");                                                                                                       //Natural: ASSIGN EFSA9120.OLD-DOC-FORMAT := 'T'
        }                                                                                                                                                                 //Natural: WHEN CWF-EFM-DOCUMENT.DOCUMENT-SUBTYPE EQ MASK ( .'I' )
        else if (condition(DbsUtil.maskMatches(cwf_Efm_Document_Document_Subtype,".'I'")))
        {
            decideConditionsMet1167++;
            pdaEfsa9120.getEfsa9120_Old_Doc_Format().setValue("I");                                                                                                       //Natural: ASSIGN EFSA9120.OLD-DOC-FORMAT := 'I'
        }                                                                                                                                                                 //Natural: WHEN CWF-EFM-DOCUMENT.DOCUMENT-SUBTYPE EQ MASK ( ..'K' )
        else if (condition(DbsUtil.maskMatches(cwf_Efm_Document_Document_Subtype,"..'K'")))
        {
            decideConditionsMet1167++;
            pdaEfsa9120.getEfsa9120_Old_Doc_Format().setValue("K");                                                                                                       //Natural: ASSIGN EFSA9120.OLD-DOC-FORMAT := 'K'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaEfsa9120.getEfsa9120_Old_Doc_Tie_Breaker().setValue("0");                                                                                                      //Natural: ASSIGN EFSA9120.OLD-DOC-TIE-BREAKER := '0'
        pdaEfsa9120.getEfsa9120_Old_Doc_Copy_Ind().setValue(cwf_Efm_Document_Copy_Ind);                                                                                   //Natural: ASSIGN EFSA9120.OLD-DOC-COPY-IND := CWF-EFM-DOCUMENT.COPY-IND
        pdaEfsa9120.getEfsa9120_Old_Doc_Readme_Ind().setValue(cwf_Efm_Document_Important_Ind);                                                                            //Natural: ASSIGN EFSA9120.OLD-DOC-README-IND := CWF-EFM-DOCUMENT.IMPORTANT-IND
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue(cwf_Sb_Work_Req_Last_Updte_Oprtr_Cde);                                                                       //Natural: ASSIGN EFSA9120.RQST-ENTRY-OP-CDE := CWF-SB-WORK-REQ.LAST-UPDTE-OPRTR-CDE
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("G");                                                                                                          //Natural: ASSIGN EFSA9120.RQST-ORIGIN-CDE := 'G'
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue("CWF");                                                                                                   //Natural: ASSIGN EFSA9120.RQST-ORIGIN-UNIT-CDE := 'CWF'
        pdaEfsa9120.getEfsa9120_Doc_Category().setValue(cwf_Efm_Document_Document_Category);                                                                              //Natural: ASSIGN EFSA9120.DOC-CATEGORY := CWF-EFM-DOCUMENT.DOCUMENT-CATEGORY
        pdaEfsa9120.getEfsa9120_Doc_Class().setValue(cwf_Efm_Document_Document_Sub_Category);                                                                             //Natural: ASSIGN EFSA9120.DOC-CLASS := CWF-EFM-DOCUMENT.DOCUMENT-SUB-CATEGORY
        pdaEfsa9120.getEfsa9120_Doc_Specific().setValue(cwf_Efm_Document_Document_Detail);                                                                                //Natural: ASSIGN EFSA9120.DOC-SPECIFIC := CWF-EFM-DOCUMENT.DOCUMENT-DETAIL
        pdaEfsa9120.getEfsa9120_Doc_Direction().setValue(cwf_Efm_Document_Document_Direction_Ind);                                                                        //Natural: ASSIGN EFSA9120.DOC-DIRECTION := CWF-EFM-DOCUMENT.DOCUMENT-DIRECTION-IND
        pdaEfsa9120.getEfsa9120_Doc_Security_Cde().setValue(cwf_Efm_Document_Secured_Document_Ind);                                                                       //Natural: ASSIGN EFSA9120.DOC-SECURITY-CDE := CWF-EFM-DOCUMENT.SECURED-DOCUMENT-IND
        //* *---EFSA9120.DOC-FORMAT-CDE
        short decideConditionsMet1189 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-EFM-DOCUMENT.DOCUMENT-SUBTYPE EQ MASK ( 'T' )
        if (condition(DbsUtil.maskMatches(cwf_Efm_Document_Document_Subtype,"'T'")))
        {
            decideConditionsMet1189++;
            pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue("T");                                                                                                       //Natural: ASSIGN EFSA9120.DOC-FORMAT-CDE := 'T'
        }                                                                                                                                                                 //Natural: WHEN CWF-EFM-DOCUMENT.DOCUMENT-SUBTYPE EQ MASK ( .'I' )
        else if (condition(DbsUtil.maskMatches(cwf_Efm_Document_Document_Subtype,".'I'")))
        {
            decideConditionsMet1189++;
            pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue("I");                                                                                                       //Natural: ASSIGN EFSA9120.DOC-FORMAT-CDE := 'I'
        }                                                                                                                                                                 //Natural: WHEN CWF-EFM-DOCUMENT.DOCUMENT-SUBTYPE EQ MASK ( ..'K' )
        else if (condition(DbsUtil.maskMatches(cwf_Efm_Document_Document_Subtype,"..'K'")))
        {
            decideConditionsMet1189++;
            pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue("K");                                                                                                       //Natural: ASSIGN EFSA9120.DOC-FORMAT-CDE := 'K'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaEfsa9120.getEfsa9120_Doc_Retention_Cde().setValue(cwf_Efm_Document_Document_Retention_Ind);                                                                    //Natural: ASSIGN EFSA9120.DOC-RETENTION-CDE := CWF-EFM-DOCUMENT.DOCUMENT-RETENTION-IND
        pdaEfsa9120.getEfsa9120_Doc_Copy_Ind().setValue(cwf_Efm_Document_Copy_Ind);                                                                                       //Natural: ASSIGN EFSA9120.DOC-COPY-IND := CWF-EFM-DOCUMENT.COPY-IND
        pdaEfsa9120.getEfsa9120_Doc_Read_Me_Ind().setValue(cwf_Efm_Document_Important_Ind);                                                                               //Natural: ASSIGN EFSA9120.DOC-READ-ME-IND := CWF-EFM-DOCUMENT.IMPORTANT-IND
        pnd_Date_A8.setValueEdited(cwf_Efm_Document_Rcvd_Sent_Dte,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED CWF-EFM-DOCUMENT.RCVD-SENT-DTE ( EM = YYYYMMDD ) TO #DATE-A8
        pdaEfsa9120.getEfsa9120_Doc_Rcvd_Dte().setValue(pnd_Date_A8_Pnd_Date_N8);                                                                                         //Natural: ASSIGN EFSA9120.DOC-RCVD-DTE := #DATE-N8
        pdaEfsa9120.getEfsa9120_System().setValue("MIT");                                                                                                                 //Natural: ASSIGN EFSA9120.SYSTEM := 'MIT'
        pdaEfsa9120.getEfsa9120_Entry_Dte_Tme().setValueEdited(cwf_Efm_Document_Entry_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                     //Natural: MOVE EDITED CWF-EFM-DOCUMENT.ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST ) TO EFSA9120.ENTRY-DTE-TME
        DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                    //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Log_Close_Rqst() throws Exception                                                                                                                    //Natural: LOG-CLOSE-RQST
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Dest_Folder_Tiaa_Rcvd_Dte_N.setValue(pnd_Curr_Date_N8);                                                                                                       //Natural: ASSIGN #DEST-FOLDER.TIAA-RCVD-DTE-N := #CURR-DATE-N8
        pnd_Dest_Folder_Case_Ind.setValue("1");                                                                                                                           //Natural: ASSIGN #DEST-FOLDER.CASE-IND := '1'
        pnd_Dest_Folder_Originating_Wpid.setValue(pnd_Misc_Wpid);                                                                                                         //Natural: ASSIGN #DEST-FOLDER.ORIGINATING-WPID := #MISC-WPID
        pnd_Dest_Folder_Multi_Rqst_Ind.setValue("0");                                                                                                                     //Natural: ASSIGN #DEST-FOLDER.MULTI-RQST-IND := '0'
        pnd_Dest_Folder_Sub_Rqst_Ind.setValue("0");                                                                                                                       //Natural: ASSIGN #DEST-FOLDER.SUB-RQST-IND := '0'
        //* *
        pdaEfsa9120.getEfsa9120().reset();                                                                                                                                //Natural: RESET EFSA9120
        pdaEfsa9120.getEfsa9120_Action().setValue("AR");                                                                                                                  //Natural: ASSIGN EFSA9120.ACTION := 'AR'
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: ASSIGN EFSA9120.CABINET-PREFIX := 'P'
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(cwf_Sb_Bene_Srvvr_Pin);                                                                                                //Natural: ASSIGN EFSA9120.PIN-NBR := CWF-SB-BENE.SRVVR-PIN
        pdaEfsa9120.getEfsa9120_Tiaa_Rcvd_Dte().getValue(1).setValue(pnd_Dest_Folder_Tiaa_Rcvd_Dte_N);                                                                    //Natural: ASSIGN EFSA9120.TIAA-RCVD-DTE ( 1 ) := #DEST-FOLDER.TIAA-RCVD-DTE-N
        pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue(pnd_Dest_Folder_Originating_Wpid);                                                                            //Natural: ASSIGN EFSA9120.WPID ( 1 ) := #DEST-FOLDER.ORIGINATING-WPID
        pdaEfsa9120.getEfsa9120_Corp_Status().getValue(1).setValue("9");                                                                                                  //Natural: ASSIGN EFSA9120.CORP-STATUS ( 1 ) := '9'
        pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).setValue("8000");                                                                                                //Natural: ASSIGN EFSA9120.STATUS-CDE ( 1 ) := '8000'
        pdaEfsa9120.getEfsa9120_Work_Rqst_Prty_Cde().getValue(1).setValue("9");                                                                                           //Natural: ASSIGN EFSA9120.WORK-RQST-PRTY-CDE ( 1 ) := '9'
        pdaEfsa9120.getEfsa9120_Due_Dte_Tme().getValue(1).setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                         //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO EFSA9120.DUE-DTE-TME ( 1 )
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("G");                                                                                                          //Natural: ASSIGN EFSA9120.RQST-ORIGIN-CDE := 'G'
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue("CWFID2");                                                                                                   //Natural: ASSIGN EFSA9120.RQST-ENTRY-OP-CDE := 'CWFID2'
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue("CWF");                                                                                                   //Natural: ASSIGN EFSA9120.RQST-ORIGIN-UNIT-CDE := 'CWF'
        pdaEfsa9120.getEfsa9120_System().setValue("MIT");                                                                                                                 //Natural: ASSIGN EFSA9120.SYSTEM := 'MIT'
        DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                    //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        vw_cwf_Master_2.startDatabaseFind                                                                                                                                 //Natural: FIND CWF-MASTER-2 WITH ACTV-UNQUE-KEY EQ EFSA9120.RETURN-DATE-TIME ( 1 )
        (
        "F1",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", "=", pdaEfsa9120.getEfsa9120_Return_Date_Time().getValue(1), WcType.WITH) }
        );
        F1:
        while (condition(vw_cwf_Master_2.readNextRow("F1")))
        {
            vw_cwf_Master_2.setIfNotFoundControlFlag(false);
            cwf_Master_2_Elctrnc_Fldr_Ind.setValue("Y");                                                                                                                  //Natural: ASSIGN CWF-MASTER-2.ELCTRNC-FLDR-IND = 'Y'
            vw_cwf_Master_2.updateDBRow("F1");                                                                                                                            //Natural: UPDATE ( F1. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Check_Misc_Folder() throws Exception                                                                                                                 //Natural: CHECK-MISC-FOLDER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Dest_Folder.reset();                                                                                                                                          //Natural: RESET #DEST-FOLDER
        pnd_Work_Prcss_Key_Cab_Prefix.setValue("P");                                                                                                                      //Natural: ASSIGN #WORK-PRCSS-KEY.CAB-PREFIX := 'P'
        pnd_Work_Prcss_Key_Pin_Nbr.setValue(cwf_Sb_Bene_Srvvr_Pin);                                                                                                       //Natural: ASSIGN #WORK-PRCSS-KEY.PIN-NBR := CWF-SB-BENE.SRVVR-PIN
        pnd_Work_Prcss_Key_Sub_Rqst_Work_Prcss_Id.setValue(pnd_Misc_Wpid);                                                                                                //Natural: ASSIGN #WORK-PRCSS-KEY.SUB-RQST-WORK-PRCSS-ID := #MISC-WPID
        vw_cwf_Efm_Folder.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) CWF-EFM-FOLDER BY WORK-PRCSS-KEY EQ #WORK-PRCSS-KEY
        (
        "READ03",
        new Wc[] { new Wc("WORK_PRCSS_KEY", ">=", pnd_Work_Prcss_Key, WcType.BY) },
        new Oc[] { new Oc("WORK_PRCSS_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(vw_cwf_Efm_Folder.readNextRow("READ03")))
        {
            if (condition(cwf_Efm_Folder_Cabinet_Id.notEquals(pnd_Work_Prcss_Key_Cabinet_Id) || cwf_Efm_Folder_Sub_Rqst_Work_Prcss_Id.notEquals(pnd_Work_Prcss_Key_Sub_Rqst_Work_Prcss_Id))) //Natural: IF CWF-EFM-FOLDER.CABINET-ID NE #WORK-PRCSS-KEY.CABINET-ID OR CWF-EFM-FOLDER.SUB-RQST-WORK-PRCSS-ID NE #WORK-PRCSS-KEY.SUB-RQST-WORK-PRCSS-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*   NO NEED TO LOG NEW CLOSE REQUEST
            pnd_First_Doc.reset();                                                                                                                                        //Natural: RESET #FIRST-DOC
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,cwf_Sb_Bene_Srvvr_Pin,pnd_Dest_Folder,new ColumnSpacing(3),"* * *  Miscellaneous folder found");           //Natural: WRITE ( 1 ) / CWF-SB-BENE.SRVVR-PIN #DEST-FOLDER 3X '* * *  Miscellaneous folder found'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *---  INITIALIZE THE DESTINATION FOLDER FIELDS
            pnd_Dest_Folder_Tiaa_Rcvd_Dte.setValueEdited(cwf_Efm_Folder_Tiaa_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED CWF-EFM-FOLDER.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO #DEST-FOLDER.TIAA-RCVD-DTE
            pnd_Dest_Folder_Case_Ind.setValue(cwf_Efm_Folder_Case_Ind);                                                                                                   //Natural: ASSIGN #DEST-FOLDER.CASE-IND := CWF-EFM-FOLDER.CASE-IND
            pnd_Dest_Folder_Originating_Wpid.setValue(cwf_Efm_Folder_Originating_Wpid);                                                                                   //Natural: ASSIGN #DEST-FOLDER.ORIGINATING-WPID := CWF-EFM-FOLDER.ORIGINATING-WPID
            pnd_Dest_Folder_Multi_Rqst_Ind.setValue(cwf_Efm_Folder_Multi_Rqst_Ind);                                                                                       //Natural: ASSIGN #DEST-FOLDER.MULTI-RQST-IND := CWF-EFM-FOLDER.MULTI-RQST-IND
            pnd_Dest_Folder_Sub_Rqst_Ind.setValue(cwf_Efm_Folder_Sub_Rqst_Ind);                                                                                           //Natural: ASSIGN #DEST-FOLDER.SUB-RQST-IND := CWF-EFM-FOLDER.SUB-RQST-IND
            //* *---  READ DOCUMENTS OF THIS EXISTING FOLDER AND SAVE IN AN ARRAY
            pnd_Doc_Key.reset();                                                                                                                                          //Natural: RESET #DOC-KEY
            pnd_Doc_Key_Cabinet_Id.setValue(cwf_Efm_Folder_Cabinet_Id);                                                                                                   //Natural: ASSIGN #DOC-KEY.CABINET-ID := CWF-EFM-FOLDER.CABINET-ID
            pnd_Doc_Key.setValuesByName(vw_cwf_Efm_Folder);                                                                                                               //Natural: MOVE BY NAME CWF-EFM-FOLDER TO #DOC-KEY
            vw_cwf_Efm_Document.startDatabaseRead                                                                                                                         //Natural: READ CWF-EFM-DOCUMENT BY DOCUMENT-KEY EQ #DOC-KY
            (
            "READ04",
            new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Doc_Key_Pnd_Doc_Ky.getBinary(), WcType.BY) },
            new Oc[] { new Oc("DOCUMENT_KEY", "ASC") }
            );
            READ04:
            while (condition(vw_cwf_Efm_Document.readNextRow("READ04")))
            {
                if (condition(cwf_Efm_Document_Cabinet_Id.notEquals(pnd_Doc_Key_Cabinet_Id) || cwf_Efm_Document_Tiaa_Rcvd_Dte.notEquals(pnd_Doc_Key_Tiaa_Rcvd_Dte)        //Natural: IF CWF-EFM-DOCUMENT.CABINET-ID NE #DOC-KEY.CABINET-ID OR CWF-EFM-DOCUMENT.TIAA-RCVD-DTE NE #DOC-KEY.TIAA-RCVD-DTE OR CWF-EFM-DOCUMENT.CASE-WORKPRCSS-MULTI-SUBRQST NE #DOC-KEY.CASE-WORKPRCSS-MULTI-SUBRQST
                    || cwf_Efm_Document_Case_Workprcss_Multi_Subrqst.notEquals(pnd_Doc_Key_Case_Workprcss_Multi_Subrqst)))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cwf_Efm_Document_Document_Retention_Ind.equals("D")))                                                                                       //Natural: REJECT IF CWF-EFM-DOCUMENT.DOCUMENT-RETENTION-IND = 'D'
                {
                    continue;
                }
                //*   SAVE IN ARRAY TO BE USED TO CHECK DUPLICATE DOCS
                pnd_Doc_Info_Flds.setValuesByName(vw_cwf_Efm_Document);                                                                                                   //Natural: MOVE BY NAME CWF-EFM-DOCUMENT TO #DOC-INFO-FLDS
                pnd_Ix.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #IX
                pnd_Doc_Info_Arr.getValue(pnd_Ix).setValue(pnd_Doc_Info_Flds_Pnd_Doc_Info);                                                                               //Natural: ASSIGN #DOC-INFO-ARR ( #IX ) = #DOC-INFO
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Update_Run_Date() throws Exception                                                                                                                   //Natural: UPDATE-RUN-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        pnd_Sup_Tbl_Prime_Key.reset();                                                                                                                                    //Natural: RESET #SUP-TBL-PRIME-KEY
        pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Scrty_Level_Ind.setValue("A ");                                                                                                 //Natural: ASSIGN #SUP-TBL-SCRTY-LEVEL-IND := 'A '
        pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Table_Nme.setValue("P5030CWD-RUN-DATE");                                                                                        //Natural: ASSIGN #SUP-TBL-TABLE-NME := 'P5030CWD-RUN-DATE'
        pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Key_Field.setValue("P5030CWD-RUN-DATE-KEY");                                                                                    //Natural: ASSIGN #SUP-TBL-KEY-FIELD := 'P5030CWD-RUN-DATE-KEY'
        pnd_Sup_Tbl_Prime_Key_Pnd_Sup_Tbl_Actve_Ind.setValue(" ");                                                                                                        //Natural: ASSIGN #SUP-TBL-ACTVE-IND := ' '
        vw_cwf_Support_Tbl_Sb_Bene.startDatabaseFind                                                                                                                      //Natural: FIND CWF-SUPPORT-TBL-SB-BENE WITH TBL-PRIME-KEY = #SUP-TBL-PRIME-KEY
        (
        "F3",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Sup_Tbl_Prime_Key, WcType.WITH) }
        );
        F3:
        while (condition(vw_cwf_Support_Tbl_Sb_Bene.readNextRow("F3", true)))
        {
            vw_cwf_Support_Tbl_Sb_Bene.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Support_Tbl_Sb_Bene.getAstCOUNTER().equals(0)))                                                                                          //Natural: IF NO RECORDS FOUND
            {
                getReports().print(0, "Update: No P5030CWD-RUN-DATE Record Found!");                                                                                      //Natural: PRINT 'Update: No P5030CWD-RUN-DATE Record Found!'
                DbsUtil.terminate(4);  if (true) return;                                                                                                                  //Natural: TERMINATE 4
            }                                                                                                                                                             //Natural: END-NOREC
            G3:                                                                                                                                                           //Natural: GET CWF-SUPPORT-TBL-SB-BENE *ISN ( F3. )
            vw_cwf_Support_Tbl_Sb_Bene.readByID(vw_cwf_Support_Tbl_Sb_Bene.getAstISN("F3"), "G3");
            cwf_Support_Tbl_Sb_Bene_Pnd_Tbl_Data_Field1.setValue(Global.getDATN());                                                                                       //Natural: MOVE *DATN TO CWF-SUPPORT-TBL-SB-BENE.#TBL-DATA-FIELD1
            cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                             //Natural: MOVE *DATX TO CWF-SUPPORT-TBL-SB-BENE.TBL-UPDTE-DTE
            cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                         //Natural: MOVE *TIMX TO CWF-SUPPORT-TBL-SB-BENE.TBL-UPDTE-DTE-TME
            cwf_Support_Tbl_Sb_Bene_Tbl_Updte_Oprtr_Cde.setValue("P5030CWD");                                                                                             //Natural: MOVE 'P5030CWD' TO CWF-SUPPORT-TBL-SB-BENE.TBL-UPDTE-OPRTR-CDE
            vw_cwf_Support_Tbl_Sb_Bene.updateDBRow("G3");                                                                                                                 //Natural: UPDATE ( G3. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //*  (F3.)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(28),"SURVIVOR BENEFITS SYSTEM",new TabSetting(70),"PAGE:",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) *PROGRAM 28T 'SURVIVOR BENEFITS SYSTEM' 70T 'PAGE:' *PAGE-NUMBER ( 1 ) ( NL = 4 SG = OFF ) /11T 'BATCH SPLIT PROCESSING (COPY FOLDERS/DOCUMENTS FOR SURVIVOR)' /32T *TIMX ( EM = L ( 9 ) �DD,�YYYY )
                        new NumericLength (4), new SignPosition (false),NEWLINE,new TabSetting(11),"BATCH SPLIT PROCESSING (COPY FOLDERS/DOCUMENTS FOR SURVIVOR)",NEWLINE,new 
                        TabSetting(32),Global.getTIMX(), new ReportEditMask ("LLLLLLLLL DD, YYYY"));
                    if (condition(pnd_Report.equals(1)))                                                                                                                  //Natural: IF #REPORT = 1
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"PH            SURVIVOR                                ASSIGNED",NEWLINE,"PIN           NBR  NAME                               PIN     ",NEWLINE,"------------  ---","-",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / 'PH            SURVIVOR                                ASSIGNED' / 'PIN           NBR  NAME                               PIN     ' / '------------  ---' '-' ( 35 ) '-' ( 7 ) /
                            RepeatItem(35),"-",new RepeatItem(7),NEWLINE);
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,new TabSetting(46),"Document",NEWLINE,"New Folder",new TabSetting(22),"Source Folder",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / 46T 'Document' /'New Folder' 22T 'Source Folder' 46T 'Fmt  Description' /'-' ( 20 ) 22T '-' ( 23 ) 46T '---' '-' ( 28 )
                            TabSetting(46),"Fmt  Description",NEWLINE,"-",new RepeatItem(20),new TabSetting(22),"-",new RepeatItem(23),new TabSetting(46),"---","-",new 
                            RepeatItem(28));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=80 PS=54 SG=OFF");
    }
}
