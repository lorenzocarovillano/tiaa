/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:44:55 PM
**        * FROM NATURAL PROGRAM : Cwfb7979
************************************************************
**        * FILE NAME            : Cwfb7979.java
**        * CLASS NAME           : Cwfb7979
**        * INSTANCE NAME        : Cwfb7979
************************************************************
* =============================================================
* BATCH REPORT - MICROJACKETS AWAITING - MEDIA INDICATOR 5
* --------------------------------------------------------
*   THIS PROGRAM RECEIVES A DATE (YYYYMMDD) FROM CWF-SUPPORT-TBL RECORD
* A CWF-REPORT12-INPUT  CWFB7979 AND READS THE CWF-EFM-CABINET FILE 080
* FOR MATCHES AGAINST DGTZE-AWAIT-DTE-TME.
*   THE SELECTED RECORDS ARE SORTED BY DATE AND PIN-NBR PRODUCING THE
* REPORT.
*
*                                       CAB.     MIT
*         RPT                          SOURCE    ORIG
*         NBR   REPORT                  CODE     UNIT
*         ---   ---------------------   ----     ----
*         01    CIRS LOGGED             L        CRC
*         03    USER LOGGED             L
*         05    MICROJACKET REQUESTED   T
*         06    DIGITIZE SELECT         S
*         07    MERGED RQST (NO MIT)    L,T,S
*         08    MODIFY FUNCTION         M        M
*
* HISTORY
* ------------
* 11/07/96 JHH - 'CWFB7975' IS INDEX FOR SUPPORT TABLE PIN RANGES
*              - 'CWFB7979' IS INDEX FOR SUPPORT TABLE DATES
* 05/01/98 KB  - INCLUDE RECORDS WITH DGTZE-SRCE-CD = 'M'
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
* =============================================================

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb7979 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Tbl;
    private DbsField cwf_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Tbl__R_Field_1;
    private DbsField cwf_Tbl_Next;
    private DbsField cwf_Tbl_Last_Dte;
    private DbsField cwf_Tbl_Prev;
    private DbsField cwf_Tbl_Prev_Dte;
    private DbsField cwf_Tbl_Dash;
    private DbsField cwf_Tbl_Run_Dte;
    private DbsField cwf_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_2;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Name;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Program;

    private DataAccessProgramView vw_cwf_Cab;
    private DbsField cwf_Cab_Cabinet_Id;

    private DbsGroup cwf_Cab__R_Field_3;
    private DbsField cwf_Cab_Pin_Pfx;
    private DbsField cwf_Cab_Pin_Nbr;
    private DbsField cwf_Cab_Media_Updte_Dte_Tme;
    private DbsField cwf_Cab_Dgtze_Rqst_Log_Dte_Tme;
    private DbsField cwf_Cab_Dgtze_Srce_Cd;
    private DbsField cwf_Cab_Dgtze_Await_Dte_Tme;
    private DbsField cwf_Cab_Dgtze_Mj_Convert_Dte_Tme;

    private DataAccessProgramView vw_cwf_Mit;
    private DbsField cwf_Mit_Orgnl_Unit_Cde;
    private DbsField cwf_Mit_Work_Prcss_Id;
    private DbsField cwf_Mit_Tiaa_Rcvd_Dte;

    private DataAccessProgramView vw_cwf_Wpid;
    private DbsField cwf_Wpid_Work_Prcss_Id;
    private DbsField cwf_Wpid_Work_Prcss_Short_Nme;
    private DbsField pnd_Prt_Dest1;
    private DbsField pnd_Date_From;
    private DbsField pnd_Date_To;
    private DbsField pnd_Hold_Date;
    private DbsField pnd_Date_D;
    private DbsField pnd_Ttl_Cde;
    private DbsField pnd_Cab_Cnt;
    private DbsField pnd_Sel_Cnt;
    private DbsField pnd_Dte_Cnt;
    private DbsField pnd_Pge_Cnt;
    private DbsField pnd_First_Time;
    private DbsField pnd_Isn;
    private DbsField pnd_Ranges;

    private DbsGroup pnd_Ranges__R_Field_4;

    private DbsGroup pnd_Ranges_Pnd_Range;
    private DbsField pnd_Ranges_Pnd_R_Limit;
    private DbsField pnd_Ranges_Pnd_R_Space;
    private DbsField pnd_R;
    private DbsField pnd_Hold_R;

    private DbsGroup pnd_Titles;
    private DbsField pnd_Titles_Pnd_T01;
    private DbsField pnd_Titles_Pnd_T02;
    private DbsField pnd_Titles_Pnd_T03;
    private DbsField pnd_Titles_Pnd_T04;
    private DbsField pnd_Titles_Pnd_T05;
    private DbsField pnd_Titles_Pnd_T06;
    private DbsField pnd_Titles_Pnd_T07;
    private DbsField pnd_Titles_Pnd_T08;

    private DbsGroup pnd_Titles__R_Field_5;
    private DbsField pnd_Titles_Pnd_Title;

    private DbsGroup h10;
    private DbsField h10_H10a;
    private DbsField h10_H10b;
    private DbsField h10_H10c;
    private DbsField h10_H10d;
    private DbsField h10_H10pge;

    private DbsGroup h20;
    private DbsField h20_H20a;
    private DbsField h20_H20b;
    private DbsField h22;
    private DbsField h25;

    private DbsGroup d10;
    private DbsField d10_D_Pin_Nbr;
    private DbsField d10_D_Tiaa_Rcvd_Dte;
    private DbsField d10_D_Wpid_Desc;
    private DbsField d10_D_Cor_Dte_Tme;
    private DbsField d10_D_Title;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Tbl", "CWF-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Tbl_Tbl_Data_Field = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");

        cwf_Tbl__R_Field_1 = vw_cwf_Tbl.getRecord().newGroupInGroup("cwf_Tbl__R_Field_1", "REDEFINE", cwf_Tbl_Tbl_Data_Field);
        cwf_Tbl_Next = cwf_Tbl__R_Field_1.newFieldInGroup("cwf_Tbl_Next", "NEXT", FieldType.STRING, 5);
        cwf_Tbl_Last_Dte = cwf_Tbl__R_Field_1.newFieldInGroup("cwf_Tbl_Last_Dte", "LAST-DTE", FieldType.STRING, 8);
        cwf_Tbl_Prev = cwf_Tbl__R_Field_1.newFieldInGroup("cwf_Tbl_Prev", "PREV", FieldType.STRING, 7);
        cwf_Tbl_Prev_Dte = cwf_Tbl__R_Field_1.newFieldInGroup("cwf_Tbl_Prev_Dte", "PREV-DTE", FieldType.STRING, 8);
        cwf_Tbl_Dash = cwf_Tbl__R_Field_1.newFieldInGroup("cwf_Tbl_Dash", "DASH", FieldType.STRING, 3);
        cwf_Tbl_Run_Dte = cwf_Tbl__R_Field_1.newFieldInGroup("cwf_Tbl_Run_Dte", "RUN-DTE", FieldType.STRING, 8);
        cwf_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE_TME");
        cwf_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Tbl_Tbl_Updte_Dte = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_2", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Name = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Name", "#TBL-NAME", FieldType.STRING, 
            22);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Program = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Program", "#TBL-PROGRAM", FieldType.STRING, 
            8);

        vw_cwf_Cab = new DataAccessProgramView(new NameInfo("vw_cwf_Cab", "CWF-CAB"), "CWF_EFM_CABINET", "CWF_EFM_CABINET");
        cwf_Cab_Cabinet_Id = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Cab_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Cab__R_Field_3 = vw_cwf_Cab.getRecord().newGroupInGroup("cwf_Cab__R_Field_3", "REDEFINE", cwf_Cab_Cabinet_Id);
        cwf_Cab_Pin_Pfx = cwf_Cab__R_Field_3.newFieldInGroup("cwf_Cab_Pin_Pfx", "PIN-PFX", FieldType.STRING, 1);
        cwf_Cab_Pin_Nbr = cwf_Cab__R_Field_3.newFieldInGroup("cwf_Cab_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Cab_Media_Updte_Dte_Tme = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Media_Updte_Dte_Tme", "MEDIA-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "MEDIA_UPDTE_DTE_TME");
        cwf_Cab_Media_Updte_Dte_Tme.setDdmHeader("MEDIA UPDATE");
        cwf_Cab_Dgtze_Rqst_Log_Dte_Tme = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Dgtze_Rqst_Log_Dte_Tme", "DGTZE-RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "DGTZE_RQST_LOG_DTE_TME");
        cwf_Cab_Dgtze_Rqst_Log_Dte_Tme.setDdmHeader("DGTZE RQST LOG/DTE TME");
        cwf_Cab_Dgtze_Srce_Cd = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Dgtze_Srce_Cd", "DGTZE-SRCE-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DGTZE_SRCE_CD");
        cwf_Cab_Dgtze_Srce_Cd.setDdmHeader("DGTZE SOURCE/CODE");
        cwf_Cab_Dgtze_Await_Dte_Tme = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Dgtze_Await_Dte_Tme", "DGTZE-AWAIT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "DGTZE_AWAIT_DTE_TME");
        cwf_Cab_Dgtze_Await_Dte_Tme.setDdmHeader("DGTZE AWAIT/DATE TIME");
        cwf_Cab_Dgtze_Mj_Convert_Dte_Tme = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Dgtze_Mj_Convert_Dte_Tme", "DGTZE-MJ-CONVERT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DGTZE_MJ_CONVERT_DTE_TME");
        cwf_Cab_Dgtze_Mj_Convert_Dte_Tme.setDdmHeader("DGTZE MJ CONVERT/DATE TIME");
        registerRecord(vw_cwf_Cab);

        vw_cwf_Mit = new DataAccessProgramView(new NameInfo("vw_cwf_Mit", "CWF-MIT"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Mit_Orgnl_Unit_Cde = vw_cwf_Mit.getRecord().newFieldInGroup("cwf_Mit_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ORGNL_UNIT_CDE");
        cwf_Mit_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Mit_Work_Prcss_Id = vw_cwf_Mit.getRecord().newFieldInGroup("cwf_Mit_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Mit_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Mit_Tiaa_Rcvd_Dte = vw_cwf_Mit.getRecord().newFieldInGroup("cwf_Mit_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        registerRecord(vw_cwf_Mit);

        vw_cwf_Wpid = new DataAccessProgramView(new NameInfo("vw_cwf_Wpid", "CWF-WPID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wpid_Work_Prcss_Id = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Wpid_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wpid_Work_Prcss_Short_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wpid_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        registerRecord(vw_cwf_Wpid);

        pnd_Prt_Dest1 = localVariables.newFieldInRecord("pnd_Prt_Dest1", "#PRT-DEST1", FieldType.STRING, 8);
        pnd_Date_From = localVariables.newFieldInRecord("pnd_Date_From", "#DATE-FROM", FieldType.DATE);
        pnd_Date_To = localVariables.newFieldInRecord("pnd_Date_To", "#DATE-TO", FieldType.DATE);
        pnd_Hold_Date = localVariables.newFieldInRecord("pnd_Hold_Date", "#HOLD-DATE", FieldType.DATE);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Ttl_Cde = localVariables.newFieldInRecord("pnd_Ttl_Cde", "#TTL-CDE", FieldType.NUMERIC, 2);
        pnd_Cab_Cnt = localVariables.newFieldInRecord("pnd_Cab_Cnt", "#CAB-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Sel_Cnt = localVariables.newFieldInRecord("pnd_Sel_Cnt", "#SEL-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Dte_Cnt = localVariables.newFieldInRecord("pnd_Dte_Cnt", "#DTE-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Pge_Cnt = localVariables.newFieldInRecord("pnd_Pge_Cnt", "#PGE-CNT", FieldType.PACKED_DECIMAL, 4);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Ranges = localVariables.newFieldInRecord("pnd_Ranges", "#RANGES", FieldType.STRING, 325);

        pnd_Ranges__R_Field_4 = localVariables.newGroupInRecord("pnd_Ranges__R_Field_4", "REDEFINE", pnd_Ranges);

        pnd_Ranges_Pnd_Range = pnd_Ranges__R_Field_4.newGroupArrayInGroup("pnd_Ranges_Pnd_Range", "#RANGE", new DbsArrayController(1, 25));
        pnd_Ranges_Pnd_R_Limit = pnd_Ranges_Pnd_Range.newFieldInGroup("pnd_Ranges_Pnd_R_Limit", "#R-LIMIT", FieldType.NUMERIC, 12);
        pnd_Ranges_Pnd_R_Space = pnd_Ranges_Pnd_Range.newFieldInGroup("pnd_Ranges_Pnd_R_Space", "#R-SPACE", FieldType.STRING, 1);
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.PACKED_DECIMAL, 3);
        pnd_Hold_R = localVariables.newFieldInRecord("pnd_Hold_R", "#HOLD-R", FieldType.PACKED_DECIMAL, 3);

        pnd_Titles = localVariables.newGroupInRecord("pnd_Titles", "#TITLES");
        pnd_Titles_Pnd_T01 = pnd_Titles.newFieldInGroup("pnd_Titles_Pnd_T01", "#T01", FieldType.STRING, 24);
        pnd_Titles_Pnd_T02 = pnd_Titles.newFieldInGroup("pnd_Titles_Pnd_T02", "#T02", FieldType.STRING, 24);
        pnd_Titles_Pnd_T03 = pnd_Titles.newFieldInGroup("pnd_Titles_Pnd_T03", "#T03", FieldType.STRING, 24);
        pnd_Titles_Pnd_T04 = pnd_Titles.newFieldInGroup("pnd_Titles_Pnd_T04", "#T04", FieldType.STRING, 24);
        pnd_Titles_Pnd_T05 = pnd_Titles.newFieldInGroup("pnd_Titles_Pnd_T05", "#T05", FieldType.STRING, 24);
        pnd_Titles_Pnd_T06 = pnd_Titles.newFieldInGroup("pnd_Titles_Pnd_T06", "#T06", FieldType.STRING, 24);
        pnd_Titles_Pnd_T07 = pnd_Titles.newFieldInGroup("pnd_Titles_Pnd_T07", "#T07", FieldType.STRING, 24);
        pnd_Titles_Pnd_T08 = pnd_Titles.newFieldInGroup("pnd_Titles_Pnd_T08", "#T08", FieldType.STRING, 24);

        pnd_Titles__R_Field_5 = localVariables.newGroupInRecord("pnd_Titles__R_Field_5", "REDEFINE", pnd_Titles);
        pnd_Titles_Pnd_Title = pnd_Titles__R_Field_5.newFieldArrayInGroup("pnd_Titles_Pnd_Title", "#TITLE", FieldType.STRING, 24, new DbsArrayController(1, 
            8));

        h10 = localVariables.newGroupInRecord("h10", "H10");
        h10_H10a = h10.newFieldInGroup("h10_H10a", "H10A", FieldType.STRING, 11);
        h10_H10b = h10.newFieldInGroup("h10_H10b", "H10B", FieldType.STRING, 14);
        h10_H10c = h10.newFieldInGroup("h10_H10c", "H10C", FieldType.STRING, 24);
        h10_H10d = h10.newFieldInGroup("h10_H10d", "H10D", FieldType.STRING, 14);
        h10_H10pge = h10.newFieldInGroup("h10_H10pge", "H10PGE", FieldType.NUMERIC, 4);

        h20 = localVariables.newGroupInRecord("h20", "H20");
        h20_H20a = h20.newFieldInGroup("h20_H20a", "H20A", FieldType.STRING, 39);
        h20_H20b = h20.newFieldInGroup("h20_H20b", "H20B", FieldType.STRING, 13);
        h22 = localVariables.newFieldInRecord("h22", "H22", FieldType.STRING, 60);
        h25 = localVariables.newFieldInRecord("h25", "H25", FieldType.STRING, 60);

        d10 = localVariables.newGroupInRecord("d10", "D10");
        d10_D_Pin_Nbr = d10.newFieldInGroup("d10_D_Pin_Nbr", "D-PIN-NBR", FieldType.STRING, 13);
        d10_D_Tiaa_Rcvd_Dte = d10.newFieldInGroup("d10_D_Tiaa_Rcvd_Dte", "D-TIAA-RCVD-DTE", FieldType.STRING, 11);
        d10_D_Wpid_Desc = d10.newFieldInGroup("d10_D_Wpid_Desc", "D-WPID-DESC", FieldType.STRING, 16);
        d10_D_Cor_Dte_Tme = d10.newFieldInGroup("d10_D_Cor_Dte_Tme", "D-COR-DTE-TME", FieldType.STRING, 18);
        d10_D_Title = d10.newFieldInGroup("d10_D_Title", "D-TITLE", FieldType.STRING, 26);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Tbl.reset();
        vw_cwf_Cab.reset();
        vw_cwf_Mit.reset();
        vw_cwf_Wpid.reset();

        localVariables.reset();
        pnd_Tbl_Prime_Key.setInitialValue("A CWF-REPORT12-INPUT  CWFB7979");
        pnd_Prt_Dest1.setInitialValue("CMPRT01");
        pnd_First_Time.setInitialValue(true);
        pnd_R.setInitialValue(1);
        pnd_Hold_R.setInitialValue(1);
        pnd_Titles_Pnd_T01.setInitialValue("CIRS Logging & Indexing");
        pnd_Titles_Pnd_T03.setInitialValue("User Logging & Indexing");
        pnd_Titles_Pnd_T05.setInitialValue("Microjacket Requested");
        pnd_Titles_Pnd_T06.setInitialValue("Digitize Select  ");
        pnd_Titles_Pnd_T07.setInitialValue("Merged Request (No MIT)");
        pnd_Titles_Pnd_T08.setInitialValue("Modify Function");
        h10_H10b.setInitialValue("  Microjackets");
        h10_H10c.setInitialValue("Awaiting Digitizing");
        h10_H10d.setInitialValue("          Page");
        h20_H20a.setInitialValue("            TIAA ");
        h20_H20b.setInitialValue("--- Await ---");
        h22.setInitialValue("PIN NBR        RCVD DTE    WPID DESC         DATE AND TIME");
        h25.setInitialValue("------------  ----------  ---------------  -----------------");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb7979() throws Exception
    {
        super("Cwfb7979");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB7979", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  ===========================================================
        //*  --------------------------------------                                                                                                                       //Natural: ON ERROR
        getReports().definePrinter(2, "#PRT-DEST1");                                                                                                                      //Natural: DEFINE PRINTER ( 1 ) OUTPUT #PRT-DEST1
        //*  -------------------------------                                                                                                                              //Natural: FORMAT ( 1 ) LS = 132 PS = 58;//Natural: AT TOP OF PAGE ( 1 )
        //*  FILE 183
        vw_cwf_Tbl.startDatabaseFind                                                                                                                                      //Natural: FIND ( 1 ) CWF-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "FIND01",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_cwf_Tbl.readNextRow("FIND01")))
        {
            vw_cwf_Tbl.setIfNotFoundControlFlag(false);
            pnd_Isn.setValue(vw_cwf_Tbl.getAstISN("Find01"));                                                                                                             //Natural: MOVE *ISN TO #ISN
            pnd_Date_From.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Tbl_Last_Dte);                                                                                //Natural: MOVE EDITED CWF-TBL.LAST-DTE TO #DATE-FROM ( EM = YYYYMMDD )
            //*  TODAY's date
            pnd_Date_To.setValue(Global.getDATX());                                                                                                                       //Natural: MOVE *DATX TO #DATE-TO
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  *************************************
        //* * MOVE EDITED '19960701' TO #DTE-FROM (EM=YYYYMMDD)  /* COMMENT ..
        //* * MOVE EDITED '19960731' TO #DTE-TO   (EM=YYYYMMDD)  /* UPDATE BELOW
        //*  *************************************
        if (condition(pnd_Date_To.less(pnd_Date_From)))                                                                                                                   //Natural: IF #DATE-TO < #DATE-FROM
        {
            getReports().write(0, "CWFB7979 Canceled - today",pnd_Date_To,"is less than last date reported",pnd_Date_From);                                               //Natural: WRITE 'CWFB7979 Canceled - today' #DATE-TO 'is less than last date reported' #DATE-FROM
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ---------------------
        pnd_Date_From.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Tbl_Last_Dte);                                                                                    //Natural: MOVE EDITED CWF-TBL.LAST-DTE TO #DATE-FROM ( EM = YYYYMMDD )
        pnd_Date_To.setValue(Global.getDATX());                                                                                                                           //Natural: MOVE *DATX TO #DATE-TO
        vw_cwf_Cab.startDatabaseRead                                                                                                                                      //Natural: READ CWF-CAB
        (
        "READ01",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Cab.readNextRow("READ01")))
        {
            //*  ===
            //*  MOVE MEDIA-UPDTE-DTE-TME TO DGTZE-AWAIT-DTE-TME   /* TEMPORARY
            //*  ===
            pnd_Cab_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #CAB-CNT
            if (condition((cwf_Cab_Dgtze_Await_Dte_Tme.less(pnd_Date_From)) || (cwf_Cab_Dgtze_Await_Dte_Tme.greater(pnd_Date_To.add(1)))))                                //Natural: IF ( DGTZE-AWAIT-DTE-TME < #DATE-FROM ) OR ( DGTZE-AWAIT-DTE-TME > #DATE-TO + 1 )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------------------
            if (condition(cwf_Cab_Dgtze_Srce_Cd.equals("L") || cwf_Cab_Dgtze_Srce_Cd.equals("S") || cwf_Cab_Dgtze_Srce_Cd.equals("T") || cwf_Cab_Dgtze_Srce_Cd.equals("M"))) //Natural: IF CWF-CAB.DGTZE-SRCE-CD = 'L' OR = 'S' OR = 'T' OR = 'M'
            {
                //*    MOVE '5'                      TO #MEDIA-TYPE
                pnd_Date_D.setValue(cwf_Cab_Dgtze_Await_Dte_Tme);                                                                                                         //Natural: MOVE DGTZE-AWAIT-DTE-TME TO #DATE-D
                pnd_Ttl_Cde.reset();                                                                                                                                      //Natural: RESET #TTL-CDE CWF-MIT.TIAA-RCVD-DTE CWF-MIT.WORK-PRCSS-ID
                cwf_Mit_Tiaa_Rcvd_Dte.reset();
                cwf_Mit_Work_Prcss_Id.reset();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  -------
            pnd_Sel_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #SEL-CNT
            //*  DIGITIZE SELECT
            if (condition(cwf_Cab_Dgtze_Srce_Cd.equals("S")))                                                                                                             //Natural: IF CWF-CAB.DGTZE-SRCE-CD = 'S'
            {
                pnd_Ttl_Cde.setValue(6);                                                                                                                                  //Natural: MOVE 06 TO #TTL-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                vw_cwf_Mit.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) CWF-MIT WITH ACTV-UNQUE-KEY = CWF-CAB.DGTZE-RQST-LOG-DTE-TME
                (
                "FIND02",
                new Wc[] { new Wc("ACTV_UNQUE_KEY", "=", cwf_Cab_Dgtze_Rqst_Log_Dte_Tme, WcType.WITH) },
                1
                );
                FIND02:
                while (condition(vw_cwf_Mit.readNextRow("FIND02")))
                {
                    vw_cwf_Mit.setIfNotFoundControlFlag(false);
                    //*  MICROJACKET REQUESTED
                    if (condition(cwf_Cab_Dgtze_Srce_Cd.equals("T")))                                                                                                     //Natural: IF CWF-CAB.DGTZE-SRCE-CD = 'T'
                    {
                        pnd_Ttl_Cde.setValue(5);                                                                                                                          //Natural: MOVE 05 TO #TTL-CDE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  MODIFY FUNCTION
                        if (condition(cwf_Cab_Dgtze_Srce_Cd.equals("M")))                                                                                                 //Natural: IF CWF-CAB.DGTZE-SRCE-CD = 'M'
                        {
                            pnd_Ttl_Cde.setValue(8);                                                                                                                      //Natural: MOVE 08 TO #TTL-CDE
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                       /* SOURCE CODE = 'L'
                    if (condition(cwf_Mit_Orgnl_Unit_Cde.equals("CRC")))                                                                                                  //Natural: IF CWF-MIT.ORGNL-UNIT-CDE = 'CRC'
                    {
                        //*  CIRS LOGGED
                        pnd_Ttl_Cde.setValue(1);                                                                                                                          //Natural: MOVE 01 TO #TTL-CDE
                        //*  USER LOGGED
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ttl_Cde.setValue(3);                                                                                                                          //Natural: MOVE 03 TO #TTL-CDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  ------
            //*  NO MIT
            if (condition(pnd_Ttl_Cde.equals(0)))                                                                                                                         //Natural: IF #TTL-CDE = 00
            {
                pnd_Ttl_Cde.setValue(7);                                                                                                                                  //Natural: MOVE 07 TO #TTL-CDE
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------
            getSort().writeSortInData(pnd_Date_D, cwf_Cab_Pin_Nbr, cwf_Mit_Work_Prcss_Id, cwf_Mit_Tiaa_Rcvd_Dte, cwf_Cab_Dgtze_Await_Dte_Tme, pnd_Ttl_Cde);               //Natural: END-ALL
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ============================================================
        //*  #MEDIA-TYPE
        getSort().sortData(pnd_Date_D, cwf_Cab_Pin_Nbr);                                                                                                                  //Natural: SORT BY #DATE-D PIN-NBR USING CWF-MIT.WORK-PRCSS-ID CWF-MIT.TIAA-RCVD-DTE DGTZE-AWAIT-DTE-TME #TTL-CDE
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Date_D, cwf_Cab_Pin_Nbr, cwf_Mit_Work_Prcss_Id, cwf_Mit_Tiaa_Rcvd_Dte, cwf_Cab_Dgtze_Await_Dte_Tme, 
            pnd_Ttl_Cde)))
        {
            //*  -------------------------
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                //*    MOVE #MEDIA-TYPE TO #HOLD-TYPE
                pnd_Hold_Date.setValue(pnd_Date_D);                                                                                                                       //Natural: MOVE #DATE-D TO #HOLD-DATE
                pnd_Tbl_Prime_Key_Pnd_Tbl_Program.setValue("CWFB7975");                                                                                                   //Natural: MOVE 'CWFB7975' TO #TBL-PROGRAM
                //*  GET PIN RANGE FOR LEKTRIEVER
                vw_cwf_Tbl.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) CWF-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
                (
                "FIND03",
                new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
                1
                );
                FIND03:
                while (condition(vw_cwf_Tbl.readNextRow("FIND03")))
                {
                    vw_cwf_Tbl.setIfNotFoundControlFlag(false);
                    pnd_Ranges.setValue(cwf_Tbl_Tbl_Data_Field);                                                                                                          //Natural: MOVE CWF-TBL.TBL-DATA-FIELD TO #RANGES
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  -------------------------
                                                                                                                                                                          //Natural: PERFORM PIN-BREAK-TEST
            sub_Pin_Break_Test();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_Hold_R.setValue(pnd_R);                                                                                                                               //Natural: MOVE #R TO #HOLD-R
                pnd_First_Time.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-TIME = FALSE
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #MEDIA-TYPE   NOT = #HOLD-TYPE      /* MEDIA BREAK
            //*    WRITE (1) / #MED-CNT 'Total' H10C
            //*    RESET #MED-CNT
            //*    NEWPAGE(1)
            //*    MOVE #MEDIA-TYPE  TO #HOLD-TYPE
            //*  DATE BREAK
            if (condition(pnd_Date_D.notEquals(pnd_Hold_Date)))                                                                                                           //Natural: IF #DATE-D NOT = #HOLD-DATE
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Dte_Cnt,"Total for",pnd_Hold_Date);                                                                //Natural: WRITE ( 1 ) / #DTE-CNT 'Total for' #HOLD-DATE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Dte_Cnt.reset();                                                                                                                                      //Natural: RESET #DTE-CNT
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Hold_Date.setValue(pnd_Date_D);                                                                                                                       //Natural: MOVE #DATE-D TO #HOLD-DATE
                pnd_R.resetInitial();                                                                                                                                     //Natural: RESET INITIAL #R
                                                                                                                                                                          //Natural: PERFORM PIN-BREAK-TEST
                sub_Pin_Break_Test();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Hold_R.setValue(pnd_R);                                                                                                                               //Natural: MOVE #R TO #HOLD-R
            }                                                                                                                                                             //Natural: END-IF
            //*  PIN RANGE BREAK
            if (condition(pnd_R.notEquals(pnd_Hold_R)))                                                                                                                   //Natural: IF #R NOT = #HOLD-R
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Hold_R.setValue(pnd_R);                                                                                                                               //Natural: MOVE #R TO #HOLD-R
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------------------
            //*   ADD 1 TO #MED-CNT
            pnd_Dte_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #DTE-CNT
            d10_D_Pin_Nbr.setValue(cwf_Cab_Pin_Nbr);                                                                                                                      //Natural: MOVE PIN-NBR TO D-PIN-NBR
            d10_D_Tiaa_Rcvd_Dte.setValueEdited(cwf_Mit_Tiaa_Rcvd_Dte,new ReportEditMask("MM/DD/YYYY"));                                                                   //Natural: MOVE EDITED TIAA-RCVD-DTE ( EM = MM/DD/YYYY ) TO D-TIAA-RCVD-DTE
            d10_D_Wpid_Desc.reset();                                                                                                                                      //Natural: RESET D-WPID-DESC
            vw_cwf_Wpid.startDatabaseFind                                                                                                                                 //Natural: FIND ( 1 ) CWF-WPID WITH WPID-UNIQ-KEY = CWF-MIT.WORK-PRCSS-ID
            (
            "FIND04",
            new Wc[] { new Wc("WPID_UNIQ_KEY", "=", cwf_Mit_Work_Prcss_Id, WcType.WITH) },
            1
            );
            FIND04:
            while (condition(vw_cwf_Wpid.readNextRow("FIND04")))
            {
                vw_cwf_Wpid.setIfNotFoundControlFlag(false);
                d10_D_Wpid_Desc.setValue(cwf_Wpid_Work_Prcss_Short_Nme);                                                                                                  //Natural: MOVE CWF-WPID.WORK-PRCSS-SHORT-NME TO D-WPID-DESC
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            d10_D_Cor_Dte_Tme.setValueEdited(cwf_Cab_Dgtze_Await_Dte_Tme,new ReportEditMask("MM/DD/YYYY'  'HH:II"));                                                      //Natural: MOVE EDITED DGTZE-AWAIT-DTE-TME ( EM = MM/DD/YYYY'  'HH:II ) TO D-COR-DTE-TME
            d10_D_Title.setValue(pnd_Titles_Pnd_Title.getValue(pnd_Ttl_Cde));                                                                                             //Natural: MOVE #TITLE ( #TTL-CDE ) TO D-TITLE
            getReports().write(1, ReportOption.NOTITLE,d10_D_Pin_Nbr,d10_D_Tiaa_Rcvd_Dte,d10_D_Wpid_Desc,d10_D_Cor_Dte_Tme,d10_D_Title);                                  //Natural: WRITE ( 1 ) D10
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        //*  ============================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PIN-BREAK-TEST
        //*  ==============================================
        //*  PERFORM BREAKS
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Dte_Cnt,"Total for",pnd_Hold_Date);                                                                        //Natural: WRITE ( 1 ) / #DTE-CNT 'Total for' #HOLD-DATE
        if (Global.isEscape()) return;
        //*  WRITE (1) / #MED-CNT 'Total' H10C                /* MEDIA INDICATOR
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Sel_Cnt,"Total Microjackets Selected");                                                                    //Natural: WRITE ( 1 ) / #SEL-CNT 'Total Microjackets Selected'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Cab_Cnt,"Total Cabinets");                                                                                 //Natural: WRITE ( 1 ) / #CAB-CNT 'Total Cabinets'
        if (Global.isEscape()) return;
        getReports().write(0, pnd_Sel_Cnt,"records from",pnd_Date_From,"thru",pnd_Date_To);                                                                               //Natural: WRITE #SEL-CNT 'records from' #DATE-FROM 'thru' #DATE-TO
        if (Global.isEscape()) return;
        //*  ----------------------------------------------
        GET_TBL:                                                                                                                                                          //Natural: GET CWF-TBL #ISN
        vw_cwf_Tbl.readByID(pnd_Isn.getLong(), "GET_TBL");
        cwf_Tbl_Prev_Dte.setValue(cwf_Tbl_Last_Dte);                                                                                                                      //Natural: MOVE CWF-TBL.LAST-DTE TO CWF-TBL.PREV-DTE
        cwf_Tbl_Last_Dte.setValueEdited(pnd_Date_To,new ReportEditMask("YYYYMMDD"));                                                                                      //Natural: MOVE EDITED #DATE-TO ( EM = YYYYMMDD ) TO CWF-TBL.LAST-DTE
        cwf_Tbl_Run_Dte.setValue(Global.getDATU());                                                                                                                       //Natural: MOVE *DATU TO CWF-TBL.RUN-DTE
        cwf_Tbl_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                             //Natural: MOVE *TIMX TO CWF-TBL.TBL-UPDTE-DTE-TME
        cwf_Tbl_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                                 //Natural: MOVE *DATX TO CWF-TBL.TBL-UPDTE-DTE
        cwf_Tbl_Tbl_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                                      //Natural: MOVE *INIT-USER TO CWF-TBL.TBL-UPDTE-OPRTR-CDE
        vw_cwf_Tbl.updateDBRow("GET_TBL");                                                                                                                                //Natural: UPDATE ( GET-TBL. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  ----------------------------------------------
    }
    private void sub_Pin_Break_Test() throws Exception                                                                                                                    //Natural: PIN-BREAK-TEST
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #R = #R TO 25
        for (pnd_R.setValue(pnd_R); condition(pnd_R.lessOrEqual(25)); pnd_R.nadd(1))
        {
            if (condition(cwf_Cab_Pin_Nbr.less(pnd_Ranges_Pnd_R_Limit.getValue(pnd_R))))                                                                                  //Natural: IF CWF-CAB.PIN-NBR < #R-LIMIT ( #R )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  PIN-BREAK-TEST
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Pge_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #PGE-CNT
                    h10_H10pge.setValue(pnd_Pge_Cnt);                                                                                                                     //Natural: MOVE #PGE-CNT TO H10PGE
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),h10_H10a,h10_H10b,h10_H10c,h10_H10d,h10_H10pge);                                          //Natural: WRITE ( 1 ) NOTITLE *DATU H10
                    getReports().write(1, ReportOption.NOTITLE,Global.getTIMX(),new TabSetting(29)," For",pnd_Hold_Date, new ReportEditMask ("MM/DD/YY"),new              //Natural: WRITE ( 1 ) *TIMX 29T ' For' #HOLD-DATE ( EM = MM/DD/YY ) 73T *PROGRAM
                        TabSetting(73),Global.getPROGRAM());
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,h20_H20a,h20_H20b);                                                                                //Natural: WRITE ( 1 ) / H20
                    getReports().write(1, ReportOption.NOTITLE,h22);                                                                                                      //Natural: WRITE ( 1 ) H22
                    getReports().write(1, ReportOption.NOTITLE,h25);                                                                                                      //Natural: WRITE ( 1 ) H25
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(1, ReportOption.NOTITLE,"Natural error",Global.getERROR_NR(),"in",Global.getPROGRAM(),"on line",Global.getERROR_LINE(),"at record",            //Natural: WRITE ( 1 ) 'Natural error' *ERROR-NR 'in' *PROGRAM 'on line' *ERROR-LINE 'at record' #CAB-CNT 'read and record' #SEL-CNT 'selected'
            pnd_Cab_Cnt,"read and record",pnd_Sel_Cnt,"selected");
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=58");
    }
}
