/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:51 PM
**        * FROM NATURAL PROGRAM : Cwfb3490
************************************************************
**        * FILE NAME            : Cwfb3490.java
**        * CLASS NAME           : Cwfb3490
**        * INSTANCE NAME        : Cwfb3490
************************************************************
************************************************************************
* PROGRAM  : CWFB3490
* SYSTEM   : CWF
* TITLE    :
* GENERATED: OCT 05,2000
* FUNCTION : EXTRACTS ACTIVE UNIT CODES AND THEIR DESCRIPTIONS.
*                 FOR LOADING TO ORACLE (FOR SMART)
*          :
*          :
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------   -------- --------------------------------------------------
*
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3490 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Org_Unit_Tbl;
    private DbsField cwf_Org_Unit_Tbl_Unit_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Short_Nme;
    private DbsField cwf_Org_Unit_Tbl_Unit_Long_Nme;
    private DbsField cwf_Org_Unit_Tbl_Actve_Ind;
    private DbsField cwf_Org_Unit_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Org_Unit_Tbl_Dlte_Oprtr_Cde;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Group_Name;
    private DbsField pnd_Work_File_Unit_Cde;
    private DbsField pnd_Work_File_Unit_Short_Nme;
    private DbsField pnd_Work_File_Unit_Long_Nme;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Org_Unit_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Unit_Tbl", "CWF-ORG-UNIT-TBL"), "CWF_ORG_UNIT_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Unit_Tbl_Unit_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Org_Unit_Tbl_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Unit_Tbl_Unit_Short_Nme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Short_Nme", "UNIT-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_SHORT_NME");
        cwf_Org_Unit_Tbl_Unit_Short_Nme.setDdmHeader("UNIT SHORT NAME");
        cwf_Org_Unit_Tbl_Unit_Long_Nme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Long_Nme", "UNIT-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "UNIT_LONG_NME");
        cwf_Org_Unit_Tbl_Unit_Long_Nme.setDdmHeader("UNIT LONG NAME");
        cwf_Org_Unit_Tbl_Actve_Ind = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Org_Unit_Tbl_Dlte_Dte_Tme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Org_Unit_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        cwf_Org_Unit_Tbl_Dlte_Oprtr_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Org_Unit_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPERATOR");
        registerRecord(vw_cwf_Org_Unit_Tbl);

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Group_Name = pnd_Work_File.newFieldInGroup("pnd_Work_File_Group_Name", "GROUP-NAME", FieldType.STRING, 30);
        pnd_Work_File_Unit_Cde = pnd_Work_File.newFieldInGroup("pnd_Work_File_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_File_Unit_Short_Nme = pnd_Work_File.newFieldInGroup("pnd_Work_File_Unit_Short_Nme", "UNIT-SHORT-NME", FieldType.STRING, 15);
        pnd_Work_File_Unit_Long_Nme = pnd_Work_File.newFieldInGroup("pnd_Work_File_Unit_Long_Nme", "UNIT-LONG-NME", FieldType.STRING, 45);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Org_Unit_Tbl.reset();

        localVariables.reset();
        pnd_Work_File_Group_Name.setInitialValue("UNIT-TBL");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3490() throws Exception
    {
        super("Cwfb3490");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        vw_cwf_Org_Unit_Tbl.startDatabaseRead                                                                                                                             //Natural: READ CWF-ORG-UNIT-TBL BY UNIQ-UNIT-CDE
        (
        "READ01",
        new Oc[] { new Oc("UNIQ_UNIT_CDE", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Org_Unit_Tbl.readNextRow("READ01")))
        {
            if (condition(cwf_Org_Unit_Tbl_Dlte_Dte_Tme.greater(getZero()) || cwf_Org_Unit_Tbl_Dlte_Oprtr_Cde.greater(" ")))                                              //Natural: REJECT IF DLTE-DTE-TME GT 0 OR DLTE-OPRTR-CDE GT ' '
            {
                continue;
            }
            pnd_Work_File.setValuesByName(vw_cwf_Org_Unit_Tbl);                                                                                                           //Natural: MOVE BY NAME CWF-ORG-UNIT-TBL TO #WORK-FILE
            getWorkFiles().write(1, false, pnd_Work_File);                                                                                                                //Natural: WRITE WORK FILE 1 #WORK-FILE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
