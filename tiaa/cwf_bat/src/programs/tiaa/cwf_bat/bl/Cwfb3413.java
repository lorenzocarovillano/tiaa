/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:29 PM
**        * FROM NATURAL PROGRAM : Cwfb3413
************************************************************
**        * FILE NAME            : Cwfb3413.java
**        * CLASS NAME           : Cwfb3413
**        * INSTANCE NAME        : Cwfb3413
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT OF PRESIDENT/CHAIRMAN REFERRED CASES
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3413
* SYSTEM   : CRPCWF
* TITLE    : REPORT 6
* GENERATED: DEC 20,94 AT 07:52 AM
* FUNCTION : REPORT OF PRESIDENT/CHAIRMAN REFERRED CASES
************************************************************************
* MOD BY      MOD DATE DESCRIPTION OF CHANGES
* ----------- -------- ------------------------------------------------
* J. HARGRAVE 08/21/95 CHANGE TO READ CORE FILE INSTEAD OF PIF FILE
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3413 extends BLNatBase
{
    // Data Areas
    private PdaCwfa5372 pdaCwfa5372;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Env;
    private DbsField pnd_Records_Printed;
    private DbsField pnd_Receive_Date;
    private DbsField pnd_Report_Date;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Report_Date_Time;
    private DbsField pnd_Intrnl_Pend_Date;
    private DbsField pnd_Unit_Start_Date_Time;
    private DbsField pnd_Intrnl_Pend_Daysx;
    private DbsField pnd_Extrnl_Pend_Daysx;
    private DbsField pnd_Bsnss_Days_In_Unitx;
    private DbsField pnd_No_Work_Days;
    private DbsField pnd_Report_No;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Run_Date;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Page;
    private DbsField pnd_New_Unit;

    private DbsGroup pnd_Report_Data;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Cde;

    private DbsGroup pnd_Report_Data__R_Field_1;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Id_Cde;

    private DbsGroup pnd_Misc_Parm;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Rep_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count;
    private DbsField pnd_Misc_Parm_Pnd_Todays_Time;
    private DbsField pnd_Misc_Parm_Pnd_Status_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Return_Doc_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Return_Rcvd_Txt;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_2;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_View_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Unit_Cde;
    private DbsField cwf_Master_Index_View_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_View_Step_Id;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Status_Cde;
    private DbsField cwf_Master_Index_View_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Updte_Dte;
    private DbsField cwf_Master_Index_View_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Actve_Ind;
    private DbsField cwf_Master_Index_View_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Effctve_Dte;
    private DbsField cwf_Master_Index_View_Trans_Dte;
    private DbsField cwf_Master_Index_View_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_View_Physcl_Fldr_Id_Nbr;
    private DbsGroup cwf_Master_Index_View_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Cntrct_Nbr;
    private DbsField cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_View_Work_List_Ind;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_3;
    private DbsField cwf_Master_Index_View_Due_Dte;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Ind;
    private DbsField cwf_Master_Index_View_Due_Dte_Prty_Cde;
    private DbsField cwf_Master_Index_View_Due_Dte_Filler;
    private DbsField cwf_Master_Index_View_Bsnss_Reply_Ind;
    private DbsField cwf_Master_Index_View_Status_Freeze_Ind;
    private DbsField cwf_Master_Index_View_Elctrnc_Fldr_Ind;
    private DbsField cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Days;
    private DbsField cwf_Master_Index_View_Step_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Step_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme;

    private DbsGroup extract_Record;
    private DbsField extract_Record_Pin_Nbr;
    private DbsField extract_Record_Rqst_Log_Dte_Tme;

    private DbsGroup extract_Record__R_Field_4;
    private DbsField extract_Record_Rqst_Log_Index_Dte;
    private DbsField extract_Record_Rqst_Log_Index_Tme;
    private DbsField extract_Record_Rqst_Log_Oprtr_Cde;
    private DbsField extract_Record_Orgnl_Log_Dte_Tme;
    private DbsField extract_Record_Orgnl_Unit_Cde;
    private DbsField extract_Record_Work_Prcss_Id;

    private DbsGroup extract_Record__R_Field_5;
    private DbsField extract_Record_Work_Actn_Rqstd_Cde;
    private DbsField extract_Record_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField extract_Record_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField extract_Record_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField extract_Record_Unit_Cde;

    private DbsGroup extract_Record__R_Field_6;
    private DbsField extract_Record_Unit_Id_Cde;
    private DbsField extract_Record_Unit_Rgn_Cde;
    private DbsField extract_Record_Unit_Spcl_Dsgntn_Cde;
    private DbsField extract_Record_Unit_Brnch_Group_Cde;
    private DbsField extract_Record_Unit_Updte_Dte_Tme;
    private DbsField extract_Record_Empl_Oprtr_Cde;

    private DbsGroup extract_Record__R_Field_7;
    private DbsField extract_Record_Empl_Racf_Id;
    private DbsField extract_Record_Empl_Sffx_Cde;
    private DbsField extract_Record_Last_Chnge_Dte_Tme;
    private DbsField extract_Record_Last_Chnge_Oprtr_Cde;
    private DbsField extract_Record_Tbl_Status_Key;

    private DbsGroup extract_Record__R_Field_8;
    private DbsField extract_Record_Last_Chnge_Unit_Cde;
    private DbsField extract_Record_Admin_Status_Cde;
    private DbsField extract_Record_Step_Id;
    private DbsField extract_Record_Admin_Unit_Cde;
    private DbsField extract_Record_Admin_Status_Updte_Dte_Tme;
    private DbsField extract_Record_Admin_Status_Updte_Oprtr_Cde;
    private DbsField extract_Record_Status_Cde;
    private DbsField extract_Record_Status_Updte_Dte_Tme;
    private DbsField extract_Record_Status_Updte_Oprtr_Cde;
    private DbsField extract_Record_Last_Updte_Dte;
    private DbsField extract_Record_Last_Updte_Dte_Tme;
    private DbsField extract_Record_Last_Updte_Oprtr_Cde;
    private DbsField extract_Record_Crprte_Status_Ind;
    private DbsField extract_Record_Tiaa_Rcvd_Dte;
    private DbsField extract_Record_Effctve_Dte;
    private DbsField extract_Record_Trans_Dte;
    private DbsField extract_Record_Cntrct_Nbr;
    private DbsField extract_Record_Pnd_Calendar_Days_In_Tiaa;
    private DbsField extract_Record_Pnd_Business_Days_In_Tiaa;
    private DbsField extract_Record_Extrnl_Pend_Rcv_Dte;
    private DbsField extract_Record_Pnd_Received_In_Unit;
    private DbsField extract_Record_Pnd_Bsnss_Days_In_Unit;
    private DbsField extract_Record_Pnd_Intrnl_Pend_Days;
    private DbsField extract_Record_Pnd_Extrnl_Pend_Days;
    private DbsField extract_Record_Pnd_Partic_Sname;

    private DbsGroup extract_Record__R_Field_9;
    private DbsField extract_Record_Pnd_Folder_Prefix;
    private DbsField extract_Record_Pnd_Folder_Nbr;
    private DbsField pnd_Work_Extrnl_Pend_Rcv_Dte;
    private DbsField pnd_Holiday_And_Weekend;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Orgnl_Unit_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa5372 = new PdaCwfa5372(localVariables);

        // Local Variables
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Records_Printed = localVariables.newFieldInRecord("pnd_Records_Printed", "#RECORDS-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Receive_Date = localVariables.newFieldInRecord("pnd_Receive_Date", "#RECEIVE-DATE", FieldType.DATE);
        pnd_Report_Date = localVariables.newFieldInRecord("pnd_Report_Date", "#REPORT-DATE", FieldType.DATE);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.DATE);
        pnd_Report_Date_Time = localVariables.newFieldInRecord("pnd_Report_Date_Time", "#REPORT-DATE-TIME", FieldType.TIME);
        pnd_Intrnl_Pend_Date = localVariables.newFieldInRecord("pnd_Intrnl_Pend_Date", "#INTRNL-PEND-DATE", FieldType.TIME);
        pnd_Unit_Start_Date_Time = localVariables.newFieldInRecord("pnd_Unit_Start_Date_Time", "#UNIT-START-DATE-TIME", FieldType.TIME);
        pnd_Intrnl_Pend_Daysx = localVariables.newFieldInRecord("pnd_Intrnl_Pend_Daysx", "#INTRNL-PEND-DAYSX", FieldType.PACKED_DECIMAL, 19, 1);
        pnd_Extrnl_Pend_Daysx = localVariables.newFieldInRecord("pnd_Extrnl_Pend_Daysx", "#EXTRNL-PEND-DAYSX", FieldType.PACKED_DECIMAL, 19, 1);
        pnd_Bsnss_Days_In_Unitx = localVariables.newFieldInRecord("pnd_Bsnss_Days_In_Unitx", "#BSNSS-DAYS-IN-UNITX", FieldType.PACKED_DECIMAL, 19, 1);
        pnd_No_Work_Days = localVariables.newFieldInRecord("pnd_No_Work_Days", "#NO-WORK-DAYS", FieldType.NUMERIC, 18);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Run_Date = localVariables.newFieldInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.STRING, 8);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 5);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);

        pnd_Report_Data = localVariables.newGroupInRecord("pnd_Report_Data", "#REPORT-DATA");
        pnd_Report_Data_Pnd_Rep_Unit_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Name", "#REP-UNIT-NAME", FieldType.STRING, 45);
        pnd_Report_Data_Pnd_Rep_Unit_Cde = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);

        pnd_Report_Data__R_Field_1 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_1", "REDEFINE", pnd_Report_Data_Pnd_Rep_Unit_Cde);
        pnd_Report_Data_Pnd_Rep_Unit_Id_Cde = pnd_Report_Data__R_Field_1.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Id_Cde", "#REP-UNIT-ID-CDE", FieldType.STRING, 
            5);

        pnd_Misc_Parm = localVariables.newGroupInRecord("pnd_Misc_Parm", "#MISC-PARM");
        pnd_Misc_Parm_Pnd_Wpid_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Sname", "#WPID-SNAME", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Rep_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Rep_Ctr", "#REP-CTR", FieldType.PACKED_DECIMAL, 2);
        pnd_Misc_Parm_Pnd_Total_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count", "#TOTAL-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Todays_Time = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Todays_Time", "#TODAYS-TIME", FieldType.TIME);
        pnd_Misc_Parm_Pnd_Status_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Status_Sname", "#STATUS-SNAME", FieldType.STRING, 25);
        pnd_Misc_Parm_Pnd_Return_Doc_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Doc_Txt", "#RETURN-DOC-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Return_Rcvd_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Rcvd_Txt", "#RETURN-RCVD-TXT", FieldType.STRING, 8);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Pin_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Master_Index_View_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index_View__R_Field_2 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_2", "REDEFINE", cwf_Master_Index_View_Rqst_Log_Dte_Tme);
        cwf_Master_Index_View_Rqst_Log_Index_Dte = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Log_Index_Tme = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_View_Rqst_Orgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Orgn_Cde", "RQST-ORGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_View_Orgnl_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_View_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_View_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Cde", "UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_CDE");
        cwf_Master_Index_View_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Master_Index_View_Unit_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Index_View_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Index_View_Empl_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_View_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Oprtr_Cde", 
            "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_View_Last_Chnge_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Unit_Cde", 
            "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_View_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_View_Step_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Id", "STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "STEP_ID");
        cwf_Master_Index_View_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_View_Admin_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme", 
            "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_View_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Dte_Tme", 
            "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Index_View_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Oprtr_Cde", 
            "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Updte_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Dte", "LAST-UPDTE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        cwf_Master_Index_View_Last_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Index_View_Last_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Oprtr_Cde", 
            "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Actve_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");
        cwf_Master_Index_View_Crprte_Status_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_View_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Effctve_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_View_Trans_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trans_Dte", "TRANS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        cwf_Master_Index_View_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_View_Mj_Pull_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_View_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_View_Physcl_Fldr_Id_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Index_View_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Index_View_Cntrct_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_View_Cntrct_Nbr = cwf_Master_Index_View_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Cntrct_Nbr", "CNTRCT-NBR", 
            FieldType.STRING, 8, new DbsArrayController(1, 1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte", 
            "EXTRNL-PEND-RCV-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_View_Work_List_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_List_Ind", "WORK-LIST-IND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "WORK_LIST_IND");
        cwf_Master_Index_View_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde", 
            "DUE-DTE-CHG-PRTY-CDE", FieldType.STRING, 16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        cwf_Master_Index_View__R_Field_3 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_3", "REDEFINE", cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Index_View_Due_Dte = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Due_Dte", "DUE-DTE", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Due_Dte_Chg_Ind = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Prty_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Prty_Cde", "DUE-DTE-PRTY-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Filler = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Filler", "DUE-DTE-FILLER", 
            FieldType.STRING, 6);
        cwf_Master_Index_View_Bsnss_Reply_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BSNSS_REPLY_IND");
        cwf_Master_Index_View_Bsnss_Reply_Ind.setDdmHeader("BUSINESS/REPLY");
        cwf_Master_Index_View_Status_Freeze_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_View_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Index_View_Elctrnc_Fldr_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_Index_View_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme", 
            "UNIT-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Index_View_Unit_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Clock_End_Dte_Tme", 
            "UNIT-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme", 
            "EMPL-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        cwf_Master_Index_View_Empl_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Clock_End_Dte_Tme", 
            "EMPL-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme", 
            "INTRNL-PND-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_START_DTE_TME");
        cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme.setDdmHeader("ACKNOWLEDGEMENT DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme", 
            "INTRNL-PND-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_END_DTE_TME");
        cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme.setDdmHeader("INTERNAL PEND END DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_Days = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Days", "INTRNL-PND-DAYS", 
            FieldType.PACKED_DECIMAL, 5, RepeatingFieldStrategy.None, "INTRNL_PND_DAYS");
        cwf_Master_Index_View_Intrnl_Pnd_Days.setDdmHeader("INTERNAL PEND DAYS");
        cwf_Master_Index_View_Step_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Clock_Start_Dte_Tme", 
            "STEP-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        cwf_Master_Index_View_Step_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Clock_End_Dte_Tme", 
            "STEP-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme", 
            "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme", 
            "UNIT-EN-RTE-TO-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_EN_RTE_TO_DTE_TME");
        cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme.setDdmHeader("UNIT EN ROUTE TO DATE TIME");
        registerRecord(vw_cwf_Master_Index_View);

        extract_Record = localVariables.newGroupInRecord("extract_Record", "EXTRACT-RECORD");
        extract_Record_Pin_Nbr = extract_Record.newFieldInGroup("extract_Record_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        extract_Record_Rqst_Log_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15);

        extract_Record__R_Field_4 = extract_Record.newGroupInGroup("extract_Record__R_Field_4", "REDEFINE", extract_Record_Rqst_Log_Dte_Tme);
        extract_Record_Rqst_Log_Index_Dte = extract_Record__R_Field_4.newFieldInGroup("extract_Record_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 
            8);
        extract_Record_Rqst_Log_Index_Tme = extract_Record__R_Field_4.newFieldInGroup("extract_Record_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", FieldType.STRING, 
            7);
        extract_Record_Rqst_Log_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Orgnl_Log_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", FieldType.STRING, 15);
        extract_Record_Orgnl_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        extract_Record_Work_Prcss_Id = extract_Record.newFieldInGroup("extract_Record_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);

        extract_Record__R_Field_5 = extract_Record.newGroupInGroup("extract_Record__R_Field_5", "REDEFINE", extract_Record_Work_Prcss_Id);
        extract_Record_Work_Actn_Rqstd_Cde = extract_Record__R_Field_5.newFieldInGroup("extract_Record_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", FieldType.STRING, 
            1);
        extract_Record_Work_Lob_Cmpny_Prdct_Cde = extract_Record__R_Field_5.newFieldInGroup("extract_Record_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        extract_Record_Work_Mjr_Bsnss_Prcss_Cde = extract_Record__R_Field_5.newFieldInGroup("extract_Record_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        extract_Record_Work_Spcfc_Bsnss_Prcss_Cde = extract_Record__R_Field_5.newFieldInGroup("extract_Record_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        extract_Record_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);

        extract_Record__R_Field_6 = extract_Record.newGroupInGroup("extract_Record__R_Field_6", "REDEFINE", extract_Record_Unit_Cde);
        extract_Record_Unit_Id_Cde = extract_Record__R_Field_6.newFieldInGroup("extract_Record_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        extract_Record_Unit_Rgn_Cde = extract_Record__R_Field_6.newFieldInGroup("extract_Record_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 1);
        extract_Record_Unit_Spcl_Dsgntn_Cde = extract_Record__R_Field_6.newFieldInGroup("extract_Record_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        extract_Record_Unit_Brnch_Group_Cde = extract_Record__R_Field_6.newFieldInGroup("extract_Record_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        extract_Record_Unit_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", FieldType.TIME);
        extract_Record_Empl_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10);

        extract_Record__R_Field_7 = extract_Record.newGroupInGroup("extract_Record__R_Field_7", "REDEFINE", extract_Record_Empl_Oprtr_Cde);
        extract_Record_Empl_Racf_Id = extract_Record__R_Field_7.newFieldInGroup("extract_Record_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        extract_Record_Empl_Sffx_Cde = extract_Record__R_Field_7.newFieldInGroup("extract_Record_Empl_Sffx_Cde", "EMPL-SFFX-CDE", FieldType.STRING, 2);
        extract_Record_Last_Chnge_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15);
        extract_Record_Last_Chnge_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Tbl_Status_Key = extract_Record.newFieldInGroup("extract_Record_Tbl_Status_Key", "TBL-STATUS-KEY", FieldType.STRING, 13);

        extract_Record__R_Field_8 = extract_Record.newGroupInGroup("extract_Record__R_Field_8", "REDEFINE", extract_Record_Tbl_Status_Key);
        extract_Record_Last_Chnge_Unit_Cde = extract_Record__R_Field_8.newFieldInGroup("extract_Record_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8);
        extract_Record_Admin_Status_Cde = extract_Record__R_Field_8.newFieldInGroup("extract_Record_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4);
        extract_Record_Step_Id = extract_Record.newFieldInGroup("extract_Record_Step_Id", "STEP-ID", FieldType.STRING, 6);
        extract_Record_Admin_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        extract_Record_Admin_Status_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME);
        extract_Record_Admin_Status_Updte_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8);
        extract_Record_Status_Cde = extract_Record.newFieldInGroup("extract_Record_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        extract_Record_Status_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME);
        extract_Record_Status_Updte_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Last_Updte_Dte = extract_Record.newFieldInGroup("extract_Record_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 8);
        extract_Record_Last_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME);
        extract_Record_Last_Updte_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Crprte_Status_Ind = extract_Record.newFieldInGroup("extract_Record_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1);
        extract_Record_Tiaa_Rcvd_Dte = extract_Record.newFieldInGroup("extract_Record_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE);
        extract_Record_Effctve_Dte = extract_Record.newFieldInGroup("extract_Record_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE);
        extract_Record_Trans_Dte = extract_Record.newFieldInGroup("extract_Record_Trans_Dte", "TRANS-DTE", FieldType.DATE);
        extract_Record_Cntrct_Nbr = extract_Record.newFieldInGroup("extract_Record_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        extract_Record_Pnd_Calendar_Days_In_Tiaa = extract_Record.newFieldInGroup("extract_Record_Pnd_Calendar_Days_In_Tiaa", "#CALENDAR-DAYS-IN-TIAA", 
            FieldType.NUMERIC, 5);
        extract_Record_Pnd_Business_Days_In_Tiaa = extract_Record.newFieldInGroup("extract_Record_Pnd_Business_Days_In_Tiaa", "#BUSINESS-DAYS-IN-TIAA", 
            FieldType.NUMERIC, 5);
        extract_Record_Extrnl_Pend_Rcv_Dte = extract_Record.newFieldInGroup("extract_Record_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", FieldType.TIME);
        extract_Record_Pnd_Received_In_Unit = extract_Record.newFieldInGroup("extract_Record_Pnd_Received_In_Unit", "#RECEIVED-IN-UNIT", FieldType.TIME);
        extract_Record_Pnd_Bsnss_Days_In_Unit = extract_Record.newFieldInGroup("extract_Record_Pnd_Bsnss_Days_In_Unit", "#BSNSS-DAYS-IN-UNIT", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Intrnl_Pend_Days = extract_Record.newFieldInGroup("extract_Record_Pnd_Intrnl_Pend_Days", "#INTRNL-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Extrnl_Pend_Days = extract_Record.newFieldInGroup("extract_Record_Pnd_Extrnl_Pend_Days", "#EXTRNL-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Partic_Sname = extract_Record.newFieldInGroup("extract_Record_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);

        extract_Record__R_Field_9 = extract_Record.newGroupInGroup("extract_Record__R_Field_9", "REDEFINE", extract_Record_Pnd_Partic_Sname);
        extract_Record_Pnd_Folder_Prefix = extract_Record__R_Field_9.newFieldInGroup("extract_Record_Pnd_Folder_Prefix", "#FOLDER-PREFIX", FieldType.STRING, 
            1);
        extract_Record_Pnd_Folder_Nbr = extract_Record__R_Field_9.newFieldInGroup("extract_Record_Pnd_Folder_Nbr", "#FOLDER-NBR", FieldType.NUMERIC, 6);
        pnd_Work_Extrnl_Pend_Rcv_Dte = localVariables.newFieldInRecord("pnd_Work_Extrnl_Pend_Rcv_Dte", "#WORK-EXTRNL-PEND-RCV-DTE", FieldType.STRING, 
            15);
        pnd_Holiday_And_Weekend = localVariables.newFieldInRecord("pnd_Holiday_And_Weekend", "#HOLIDAY-AND-WEEKEND", FieldType.NUMERIC, 18);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Orgnl_Unit_CdeOld = internalLoopRecord.newFieldInRecord("Sort01_Orgnl_Unit_Cde_OLD", "Orgnl_Unit_Cde_OLD", FieldType.STRING, 8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index_View.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Report_No.setInitialValue(16);
        pnd_Report_Parm.setInitialValue("CWFB3413W*");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3413() throws Exception
    {
        super("Cwfb3413");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB3413", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_New_Unit.setValue(true);                                                                                                                                      //Natural: MOVE TRUE TO #NEW-UNIT
        pnd_Records_Printed.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #RECORDS-PRINTED
        pnd_Report_Date.setValue(Global.getDATX());                                                                                                                       //Natural: MOVE *DATX TO #REPORT-DATE
        pnd_Report_Date_Time.setValue(Global.getTIMX());                                                                                                                  //Natural: MOVE *TIMX TO #REPORT-DATE-TIME
        pnd_Run_Date.setValueEdited(pnd_Report_Date,new ReportEditMask("YYYYMMDD"));                                                                                      //Natural: MOVE EDITED #REPORT-DATE ( EM = YYYYMMDD ) TO #RUN-DATE
        pnd_Page.setValue(0);                                                                                                                                             //Natural: MOVE 0 TO #PAGE
        //*  MOVE 'N' TO #REP-SUBTOTAL-IND                                                                                                                                //Natural: FORMAT ( 1 ) LS = 142 PS = 60
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************** START OF MAIN PROGRAM LOGIC **********************
        vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                        //Natural: READ CWF-MASTER-INDEX-VIEW BY CRPRTE-STATUS-CHNGE-DTE-KEY FROM '0'
        (
        "PROG",
        new Wc[] { new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", ">=", "0", WcType.BY) },
        new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
        );
        PROG:
        while (condition(vw_cwf_Master_Index_View.readNextRow("PROG")))
        {
            if (condition(cwf_Master_Index_View_Crprte_Status_Ind.greater("0")))                                                                                          //Natural: IF CWF-MASTER-INDEX-VIEW.CRPRTE-STATUS-IND GT '0'
            {
                if (true) break PROG;                                                                                                                                     //Natural: ESCAPE BOTTOM ( PROG. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(cwf_Master_Index_View_Orgnl_Unit_Cde.equals("CHAIR") || cwf_Master_Index_View_Orgnl_Unit_Cde.equals("PRES"))))                                //Natural: ACCEPT IF CWF-MASTER-INDEX-VIEW.ORGNL-UNIT-CDE = 'CHAIR' OR = 'PRES'
            {
                continue;
            }
            if (condition(cwf_Master_Index_View_Orgnl_Unit_Cde.equals("CHAIR") || cwf_Master_Index_View_Orgnl_Unit_Cde.equals("PRES")))                                   //Natural: IF CWF-MASTER-INDEX-VIEW.ORGNL-UNIT-CDE = 'CHAIR' OR = 'PRES'
            {
                //*  ---------------------
                //*  CALENDAR DAYS IN TIAA
                //*  ---------------------
                pnd_Work_Extrnl_Pend_Rcv_Dte.setValueEdited(cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte,new ReportEditMask("YYYYMMDDHHIISST"));                             //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.EXTRNL-PEND-RCV-DTE ( EM = YYYYMMDDHHIISST ) TO #WORK-EXTRNL-PEND-RCV-DTE
                if (condition(DbsUtil.maskMatches(pnd_Work_Extrnl_Pend_Rcv_Dte,"1900:2099")))                                                                             //Natural: IF #WORK-EXTRNL-PEND-RCV-DTE = MASK ( 1900:2099 )
                {
                    pnd_Receive_Date.setValue(cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte);                                                                                 //Natural: MOVE CWF-MASTER-INDEX-VIEW.EXTRNL-PEND-RCV-DTE TO #RECEIVE-DATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Receive_Date.setValue(cwf_Master_Index_View_Tiaa_Rcvd_Dte);                                                                                       //Natural: MOVE CWF-MASTER-INDEX-VIEW.TIAA-RCVD-DTE TO #RECEIVE-DATE
                }                                                                                                                                                         //Natural: END-IF
                extract_Record_Pnd_Calendar_Days_In_Tiaa.compute(new ComputeParameters(false, extract_Record_Pnd_Calendar_Days_In_Tiaa), pnd_Report_Date.subtract(pnd_Receive_Date)); //Natural: COMPUTE EXTRACT-RECORD.#CALENDAR-DAYS-IN-TIAA = #REPORT-DATE - #RECEIVE-DATE
                DbsUtil.callnat(Cwfn3901.class , getCurrentProcessState(), pnd_Receive_Date, pnd_Report_Date, pnd_Holiday_And_Weekend);                                   //Natural: CALLNAT 'CWFN3901' #RECEIVE-DATE #REPORT-DATE #HOLIDAY-AND-WEEKEND
                if (condition(Global.isEscape())) return;
                extract_Record_Pnd_Business_Days_In_Tiaa.compute(new ComputeParameters(false, extract_Record_Pnd_Business_Days_In_Tiaa), extract_Record_Pnd_Calendar_Days_In_Tiaa.subtract(pnd_Holiday_And_Weekend)); //Natural: COMPUTE EXTRACT-RECORD.#BUSINESS-DAYS-IN-TIAA = EXTRACT-RECORD.#CALENDAR-DAYS-IN-TIAA - #HOLIDAY-AND-WEEKEND
                //*  ---------------------
                //*  RECEIVED IN UNIT DATE
                //*  ---------------------
                if (condition(DbsUtil.maskMatches(cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme,"YYYYMMDD")))                                                            //Natural: IF CWF-MASTER-INDEX-VIEW.UNIT-CLOCK-START-DTE-TME = MASK ( YYYYMMDD )
                {
                    extract_Record_Pnd_Received_In_Unit.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme);             //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.UNIT-CLOCK-START-DTE-TME TO EXTRACT-RECORD.#RECEIVED-IN-UNIT ( EM = YYYYMMDDHHIISST )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Received_In_Unit.reset();                                                                                                          //Natural: RESET EXTRACT-RECORD.#RECEIVED-IN-UNIT
                }                                                                                                                                                         //Natural: END-IF
                //*  ------------------
                //*  EXTERNAL-PEND-DAYS
                //*  ------------------
                if (condition(cwf_Master_Index_View_Admin_Status_Cde.greaterOrEqual("4500") && cwf_Master_Index_View_Admin_Status_Cde.lessOrEqual("4997")))               //Natural: IF CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE = '4500' THRU '4997'
                {
                    pnd_Extrnl_Pend_Daysx.compute(new ComputeParameters(false, pnd_Extrnl_Pend_Daysx), pnd_Report_Date_Time.subtract(cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme)); //Natural: COMPUTE #EXTRNL-PEND-DAYSX = #REPORT-DATE-TIME - CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-UPDTE-DTE-TME
                    extract_Record_Pnd_Extrnl_Pend_Days.compute(new ComputeParameters(false, extract_Record_Pnd_Extrnl_Pend_Days), pnd_Extrnl_Pend_Daysx.divide(864000)); //Natural: COMPUTE #EXTRNL-PEND-DAYS = #EXTRNL-PEND-DAYSX / 864000
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Extrnl_Pend_Days.reset();                                                                                                          //Natural: RESET #EXTRNL-PEND-DAYS
                }                                                                                                                                                         //Natural: END-IF
                //*  ------------------
                //*  INTERNAL-PEND-DAYS
                //*  ------------------
                if (condition(cwf_Master_Index_View_Work_List_Ind.equals("2IP") || cwf_Master_Index_View_Work_List_Ind.equals("1RA")))                                    //Natural: IF CWF-MASTER-INDEX-VIEW.WORK-LIST-IND = '2IP' OR = '1RA'
                {
                    if (condition(cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme.greater(" ")))                                                                           //Natural: IF CWF-MASTER-INDEX-VIEW.INTRNL-PND-START-DTE-TME GT ' '
                    {
                        pnd_Intrnl_Pend_Date.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme);                        //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.INTRNL-PND-START-DTE-TME TO #INTRNL-PEND-DATE ( EM = YYYYMMDDHHIISST )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Intrnl_Pend_Date.setValue(cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme);                                                                  //Natural: MOVE CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-UPDTE-DTE-TME TO #INTRNL-PEND-DATE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Intrnl_Pend_Daysx.compute(new ComputeParameters(false, pnd_Intrnl_Pend_Daysx), pnd_Report_Date_Time.subtract(pnd_Intrnl_Pend_Date));              //Natural: COMPUTE #INTRNL-PEND-DAYSX = #REPORT-DATE-TIME - #INTRNL-PEND-DATE
                    extract_Record_Pnd_Intrnl_Pend_Days.compute(new ComputeParameters(false, extract_Record_Pnd_Intrnl_Pend_Days), (pnd_Intrnl_Pend_Daysx.divide(864000)).add((cwf_Master_Index_View_Intrnl_Pnd_Days.divide(10)))); //Natural: COMPUTE #INTRNL-PEND-DAYS = ( #INTRNL-PEND-DAYSX / 864000 ) + ( CWF-MASTER-INDEX-VIEW.INTRNL-PND-DAYS / 10 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Intrnl_Pend_Days.reset();                                                                                                          //Natural: RESET #INTRNL-PEND-DAYS
                }                                                                                                                                                         //Natural: END-IF
                //*  -------------------------------------------------------------
                //*  BUSINESS DAYS IN UNIT (LESS HOLIDAYS/WEEKENDS/INTERNAL PENDS)
                //*  -------------------------------------------------------------
                //*        COMPUTE HOLIDAYS/WEEKENDS BETWEEN DAYS
                if (condition(cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme.greater(" ")))                                                                               //Natural: IF CWF-MASTER-INDEX-VIEW.UNIT-CLOCK-START-DTE-TME GT ' '
                {
                    pnd_Start_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme);                                         //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.UNIT-CLOCK-START-DTE-TME TO #START-DATE ( EM = YYYYMMDD )
                    pnd_Unit_Start_Date_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme);                        //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.UNIT-CLOCK-START-DTE-TME TO #UNIT-START-DATE-TIME ( EM = YYYYMMDDHHIISST )
                    DbsUtil.callnat(Cwfn3901.class , getCurrentProcessState(), pnd_Start_Date, pnd_Report_Date, pnd_No_Work_Days);                                        //Natural: CALLNAT 'CWFN3901' #START-DATE #REPORT-DATE #NO-WORK-DAYS
                    if (condition(Global.isEscape())) return;
                    pnd_Bsnss_Days_In_Unitx.compute(new ComputeParameters(false, pnd_Bsnss_Days_In_Unitx), pnd_Report_Date_Time.subtract(pnd_Unit_Start_Date_Time));      //Natural: COMPUTE #BSNSS-DAYS-IN-UNITX = #REPORT-DATE-TIME - #UNIT-START-DATE-TIME
                    extract_Record_Pnd_Bsnss_Days_In_Unit.compute(new ComputeParameters(false, extract_Record_Pnd_Bsnss_Days_In_Unit), (pnd_Bsnss_Days_In_Unitx.divide(864000)).subtract(extract_Record_Pnd_Intrnl_Pend_Days).subtract(pnd_No_Work_Days)); //Natural: COMPUTE EXTRACT-RECORD.#BSNSS-DAYS-IN-UNIT = ( #BSNSS-DAYS-IN-UNITX / 864000 ) - #INTRNL-PEND-DAYS - #NO-WORK-DAYS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Bsnss_Days_In_Unit.setValue(0);                                                                                                    //Natural: MOVE 0 TO EXTRACT-RECORD.#BSNSS-DAYS-IN-UNIT
                }                                                                                                                                                         //Natural: END-IF
                //* *****************************************************************
                if (condition(cwf_Master_Index_View_Admin_Status_Cde.lessOrEqual("0899")))                                                                                //Natural: IF CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE LE '0899'
                {
                    extract_Record_Pnd_Bsnss_Days_In_Unit.reset();                                                                                                        //Natural: RESET EXTRACT-RECORD.#BSNSS-DAYS-IN-UNIT EXTRACT-RECORD.#INTRNL-PEND-DAYS EXTRACT-RECORD.#RECEIVED-IN-UNIT
                    extract_Record_Pnd_Intrnl_Pend_Days.reset();
                    extract_Record_Pnd_Received_In_Unit.reset();
                }                                                                                                                                                         //Natural: END-IF
                //* *
                if (condition(cwf_Master_Index_View_Mj_Pull_Ind.equals("Y") || cwf_Master_Index_View_Mj_Pull_Ind.equals("R")))                                            //Natural: IF CWF-MASTER-INDEX-VIEW.MJ-PULL-IND = 'Y' OR = 'R'
                {
                    extract_Record_Pnd_Folder_Prefix.setValue("M");                                                                                                       //Natural: MOVE 'M' TO #FOLDER-PREFIX
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Folder_Prefix.setValue("F");                                                                                                       //Natural: MOVE 'F' TO #FOLDER-PREFIX
                }                                                                                                                                                         //Natural: END-IF
                extract_Record_Pnd_Folder_Nbr.setValue(cwf_Master_Index_View_Physcl_Fldr_Id_Nbr);                                                                         //Natural: MOVE CWF-MASTER-INDEX-VIEW.PHYSCL-FLDR-ID-NBR TO #FOLDER-NBR
                //* *
                extract_Record.setValuesByName(vw_cwf_Master_Index_View);                                                                                                 //Natural: MOVE BY NAME CWF-MASTER-INDEX-VIEW TO EXTRACT-RECORD
                extract_Record_Last_Chnge_Unit_Cde.setValue(cwf_Master_Index_View_Last_Chnge_Unit_Cde);                                                                   //Natural: MOVE CWF-MASTER-INDEX-VIEW.LAST-CHNGE-UNIT-CDE TO EXTRACT-RECORD.LAST-CHNGE-UNIT-CDE
                extract_Record_Admin_Status_Cde.setValue(cwf_Master_Index_View_Admin_Status_Cde);                                                                         //Natural: MOVE CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE TO EXTRACT-RECORD.ADMIN-STATUS-CDE
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(extract_Record_Orgnl_Unit_Cde, extract_Record_Tiaa_Rcvd_Dte, extract_Record_Work_Prcss_Id, extract_Record_Extrnl_Pend_Rcv_Dte,      //Natural: END-ALL
                extract_Record_Admin_Status_Cde, extract_Record_Empl_Racf_Id, extract_Record_Admin_Unit_Cde, extract_Record_Pnd_Calendar_Days_In_Tiaa, extract_Record_Pnd_Business_Days_In_Tiaa, 
                extract_Record_Pnd_Bsnss_Days_In_Unit, extract_Record_Pin_Nbr, extract_Record_Step_Id, extract_Record_Tbl_Status_Key, extract_Record_Cntrct_Nbr, 
                extract_Record_Admin_Status_Updte_Dte_Tme, extract_Record_Pnd_Received_In_Unit, extract_Record_Pnd_Partic_Sname);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(extract_Record_Orgnl_Unit_Cde, extract_Record_Tiaa_Rcvd_Dte, extract_Record_Work_Prcss_Id, extract_Record_Extrnl_Pend_Rcv_Dte,                 //Natural: SORT BY EXTRACT-RECORD.ORGNL-UNIT-CDE EXTRACT-RECORD.TIAA-RCVD-DTE EXTRACT-RECORD.WORK-PRCSS-ID EXTRACT-RECORD.EXTRNL-PEND-RCV-DTE EXTRACT-RECORD.ADMIN-STATUS-CDE EXTRACT-RECORD.EMPL-RACF-ID USING EXTRACT-RECORD.ADMIN-UNIT-CDE EXTRACT-RECORD.#CALENDAR-DAYS-IN-TIAA EXTRACT-RECORD.#BUSINESS-DAYS-IN-TIAA EXTRACT-RECORD.#BSNSS-DAYS-IN-UNIT EXTRACT-RECORD.PIN-NBR EXTRACT-RECORD.STEP-ID EXTRACT-RECORD.TBL-STATUS-KEY EXTRACT-RECORD.CNTRCT-NBR EXTRACT-RECORD.ADMIN-STATUS-UPDTE-DTE-TME EXTRACT-RECORD.#RECEIVED-IN-UNIT EXTRACT-RECORD.#PARTIC-SNAME
            extract_Record_Admin_Status_Cde, extract_Record_Empl_Racf_Id);
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(extract_Record_Orgnl_Unit_Cde, extract_Record_Tiaa_Rcvd_Dte, extract_Record_Work_Prcss_Id, extract_Record_Extrnl_Pend_Rcv_Dte, 
            extract_Record_Admin_Status_Cde, extract_Record_Empl_Racf_Id, extract_Record_Admin_Unit_Cde, extract_Record_Pnd_Calendar_Days_In_Tiaa, extract_Record_Pnd_Business_Days_In_Tiaa, 
            extract_Record_Pnd_Bsnss_Days_In_Unit, extract_Record_Pin_Nbr, extract_Record_Step_Id, extract_Record_Tbl_Status_Key, extract_Record_Cntrct_Nbr, 
            extract_Record_Admin_Status_Updte_Dte_Tme, extract_Record_Pnd_Received_In_Unit, extract_Record_Pnd_Partic_Sname)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Report_Data_Pnd_Rep_Unit_Cde.setValue(extract_Record_Orgnl_Unit_Cde);                                                                                     //Natural: MOVE EXTRACT-RECORD.ORGNL-UNIT-CDE TO #REP-UNIT-CDE
            if (condition(pnd_New_Unit.getBoolean()))                                                                                                                     //Natural: IF #NEW-UNIT
            {
                DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, extract_Record_Orgnl_Unit_Cde, pnd_Floor, pnd_Bldg,                //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID EXTRACT-RECORD.ORGNL-UNIT-CDE #FLOOR #BLDG #DROP-OFF #RUN-DATE
                    pnd_Drop_Off, pnd_Run_Date);
                if (condition(Global.isEscape())) return;
                DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), extract_Record_Orgnl_Unit_Cde, pnd_Report_Data_Pnd_Rep_Unit_Name);                             //Natural: CALLNAT 'CWFN1103' EXTRACT-RECORD.ORGNL-UNIT-CDE #REP-UNIT-NAME
                if (condition(Global.isEscape())) return;
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
            }                                                                                                                                                             //Natural: END-IF
            setControl("WB");                                                                                                                                             //Natural: SET CONTROL 'WB'
            DbsUtil.callnat(Cwfn5390.class , getCurrentProcessState(), extract_Record_Work_Prcss_Id, pnd_Misc_Parm_Pnd_Wpid_Sname);                                       //Natural: AT TOP OF PAGE ( 1 );//Natural: CALLNAT 'CWFN5390' EXTRACT-RECORD.WORK-PRCSS-ID #WPID-SNAME
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn1102.class , getCurrentProcessState(), extract_Record_Tbl_Status_Key, pnd_Misc_Parm_Pnd_Status_Sname);                                    //Natural: CALLNAT 'CWFN1102' EXTRACT-RECORD.TBL-STATUS-KEY #STATUS-SNAME
            if (condition(Global.isEscape())) return;
            if (condition(extract_Record_Extrnl_Pend_Rcv_Dte.greater(getZero())))                                                                                         //Natural: IF EXTRACT-RECORD.EXTRNL-PEND-RCV-DTE GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Doc_Txt.setValueEdited(extract_Record_Extrnl_Pend_Rcv_Dte,new ReportEditMask("MM/DD/YY"));                                       //Natural: MOVE EDITED EXTRACT-RECORD.EXTRNL-PEND-RCV-DTE ( EM = MM/DD/YY ) TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Doc_Txt.setValue(" ");                                                                                                           //Natural: MOVE ' ' TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(extract_Record_Pnd_Received_In_Unit.greater(getZero())))                                                                                        //Natural: IF EXTRACT-RECORD.#RECEIVED-IN-UNIT GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValueEdited(extract_Record_Pnd_Received_In_Unit,new ReportEditMask("MM/DD/YY"));                                     //Natural: MOVE EDITED EXTRACT-RECORD.#RECEIVED-IN-UNIT ( EM = MM/DD/YY ) TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValue(" ");                                                                                                          //Natural: MOVE ' ' TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: END-IF
            extract_Record_Pnd_Partic_Sname.reset();                                                                                                                      //Natural: RESET EXTRACT-RECORD.#PARTIC-SNAME
            pdaCwfa5372.getCwfa5372_Pnd_Pin_Key().setValue(extract_Record_Pin_Nbr);                                                                                       //Natural: ASSIGN CWFA5372.#PIN-KEY := EXTRACT-RECORD.PIN-NBR
            //*  FETCH RETURN 'MDMP0011'
            //*  CALLNAT 'CWFN5372'
            //*           CWFA5372.#PIN-KEY
            //*           CWFA5372.#PASS-NAME
            //*           CWFA5372.#PASS-SSN
            //*           CWFA5372.#PASS-TLC
            //*           CWFA5372.#PASS-DOB
            //*           CWFA5372.#PASS-FOUND
            //*  FETCH RETURN 'MDMP0012'
            if (condition(pdaCwfa5372.getCwfa5372_Pnd_Pass_Found().getBoolean()))                                                                                         //Natural: IF #PASS-FOUND
            {
                extract_Record_Pnd_Partic_Sname.setValue(pdaCwfa5372.getCwfa5372_Pnd_Pass_Name());                                                                        //Natural: MOVE CWFA5372.#PASS-NAME TO EXTRACT-RECORD.#PARTIC-SNAME
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Records_Printed.getBoolean())))                                                                                                          //Natural: IF NOT #RECORDS-PRINTED
            {
                pnd_Records_Printed.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #RECORDS-PRINTED
                //*  PIN-EXP
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "//RECEIVED/AT TIAA",                                                                                                                 //Natural: DISPLAY ( 1 ) '//RECEIVED/AT TIAA' EXTRACT-RECORD.TIAA-RCVD-DTE ( EM = MM/DD/YY ) 'CALENDAR/DAYS/IN/TIAA' EXTRACT-RECORD.#CALENDAR-DAYS-IN-TIAA ( EM = Z,ZZ9 ) 'BUSINESS/DAYS/IN/TIAA' EXTRACT-RECORD.#BUSINESS-DAYS-IN-TIAA ( EM = Z,ZZ9 ) '//WORK/PROCESS' #WPID-SNAME ( IS = ON ) '//WORK/STEP' EXTRACT-RECORD.STEP-ID '//STATUS/DATE' EXTRACT-RECORD.ADMIN-STATUS-UPDTE-DTE-TME ( EM = MM/DD/YY ) '///STATUS' #STATUS-SNAME '//CURRENT/UNIT' EXTRACT-RECORD.ADMIN-UNIT-CDE '//PARTIC/NAME' EXTRACT-RECORD.#PARTIC-SNAME '///PIN' EXTRACT-RECORD.PIN-NBR ( EM = 999999999999 ) '//CONTRACT/NUMBER' EXTRACT-RECORD.CNTRCT-NBR
            		extract_Record_Tiaa_Rcvd_Dte, new ReportEditMask ("MM/DD/YY"),"CALENDAR/DAYS/IN/TIAA",
            		extract_Record_Pnd_Calendar_Days_In_Tiaa, new ReportEditMask ("Z,ZZ9"),"BUSINESS/DAYS/IN/TIAA",
            		extract_Record_Pnd_Business_Days_In_Tiaa, new ReportEditMask ("Z,ZZ9"),"//WORK/PROCESS",
            		pnd_Misc_Parm_Pnd_Wpid_Sname, new IdenticalSuppress(true),"//WORK/STEP",
            		extract_Record_Step_Id,"//STATUS/DATE",
            		extract_Record_Admin_Status_Updte_Dte_Tme, new ReportEditMask ("MM/DD/YY"),"///STATUS",
            		pnd_Misc_Parm_Pnd_Status_Sname,"//CURRENT/UNIT",
            		extract_Record_Admin_Unit_Cde,"//PARTIC/NAME",
            		extract_Record_Pnd_Partic_Sname,"///PIN",
            		extract_Record_Pin_Nbr, new ReportEditMask ("999999999999"),"//CONTRACT/NUMBER",
            		extract_Record_Cntrct_Nbr);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    '///PIN' EXTRACT-RECORD.PIN-NBR (EM=9999999)
            pnd_Misc_Parm_Pnd_Total_Count.nadd(1);                                                                                                                        //Natural: ADD 1 TO #TOTAL-COUNT
            //*                                                                                                                                                           //Natural: AT BREAK OF EXTRACT-RECORD.ORGNL-UNIT-CDE
            //*  READ-2.
            sort01Orgnl_Unit_CdeOld.setValue(extract_Record_Orgnl_Unit_Cde);                                                                                              //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //* ***********************************************************************
        if (condition(! (pnd_Records_Printed.getBoolean())))                                                                                                              //Natural: IF NOT #RECORDS-PRINTED
        {
            DbsUtil.callnat(Cwfn3915.class , getCurrentProcessState(), pnd_Report_No);                                                                                    //Natural: CALLNAT 'CWFN3915' #REPORT-NO
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* PIN-EXP
        //*                                                                                                                                                               //Natural: ON ERROR
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page.nadd(1);                                                                                                                                     //Natural: COMPUTE #PAGE = #PAGE + 1
                    getReports().write(1, ReportOption.NOTITLE,"CWFB3413",pnd_Env,new TabSetting(52),"CORPORATE WORKFLOW FACILITIES",new TabSetting(122),"PAGE",pnd_Page, //Natural: WRITE ( 1 ) NOTITLE 'CWFB3413' #ENV 52T 'CORPORATE WORKFLOW FACILITIES' 122T 'PAGE' #PAGE ( NL = 5 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 43T 'Report of President/Chairman Referred Cases' 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (5), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
                        TabSetting(43),"Report of President/Chairman Referred Cases",new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,"UNIT      :",pnd_Report_Data_Pnd_Rep_Unit_Cde,pnd_Report_Data_Pnd_Rep_Unit_Name);                         //Natural: WRITE ( 1 ) 'UNIT      :' #REP-UNIT-CDE #REP-UNIT-NAME
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*************** START OF ERROR LOG *****************",NEWLINE,"ERROR NO----->",Global.getERROR_NR(),NEWLINE,"LINE--------->",Global.getERROR_LINE(),NEWLINE,"PROGRAM------>",Global.getPROGRAM(),NEWLINE,"PARM PIN----->",cwf_Master_Index_View_Pin_Nbr,  //Natural: WRITE ( 1 ) / '*************** START OF ERROR LOG *****************' / 'ERROR NO----->' *ERROR-NR / 'LINE--------->' *ERROR-LINE / 'PROGRAM------>' *PROGRAM / 'PARM PIN----->' CWF-MASTER-INDEX-VIEW.PIN-NBR ( EM = 999999999999 ) / 'PARM WPID---->' CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID / 'PARM RQST NO->' CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME / '**************** END OF ERROR LOG ******************'
            new ReportEditMask ("999999999999"),NEWLINE,"PARM WPID---->",cwf_Master_Index_View_Work_Prcss_Id,NEWLINE,"PARM RQST NO->",cwf_Master_Index_View_Rqst_Log_Dte_Tme,
            NEWLINE,"**************** END OF ERROR LOG ******************");
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean extract_Record_Orgnl_Unit_CdeIsBreak = extract_Record_Orgnl_Unit_Cde.isBreak(endOfData);
        if (condition(extract_Record_Orgnl_Unit_CdeIsBreak))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(3),"    GRAND TOTAL                                    :  ",pnd_Misc_Parm_Pnd_Total_Count,  //Natural: WRITE ( 1 ) // 3T '    GRAND TOTAL                                    :  ' #TOTAL-COUNT ( AD = OU ) / 3T '    -------------------------------------------------'
                new FieldAttributes ("AD=OU"),NEWLINE,new TabSetting(3),"    -------------------------------------------------");
            if (condition(Global.isEscape())) return;
            pnd_Misc_Parm_Pnd_Total_Count.reset();                                                                                                                        //Natural: RESET #TOTAL-COUNT
            pnd_Report_Data_Pnd_Rep_Unit_Cde.setValue(sort01Orgnl_Unit_CdeOld);                                                                                           //Natural: MOVE OLD ( EXTRACT-RECORD.ORGNL-UNIT-CDE ) TO #REP-UNIT-CDE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())) return;
            pnd_New_Unit.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-UNIT
            pnd_Page.reset();                                                                                                                                             //Natural: RESET #PAGE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=142 PS=60");

        getReports().setDisplayColumns(1, "//RECEIVED/AT TIAA",
        		extract_Record_Tiaa_Rcvd_Dte, new ReportEditMask ("MM/DD/YY"),"CALENDAR/DAYS/IN/TIAA",
        		extract_Record_Pnd_Calendar_Days_In_Tiaa, new ReportEditMask ("Z,ZZ9"),"BUSINESS/DAYS/IN/TIAA",
        		extract_Record_Pnd_Business_Days_In_Tiaa, new ReportEditMask ("Z,ZZ9"),"//WORK/PROCESS",
        		pnd_Misc_Parm_Pnd_Wpid_Sname, new IdenticalSuppress(true),"//WORK/STEP",
        		extract_Record_Step_Id,"//STATUS/DATE",
        		extract_Record_Admin_Status_Updte_Dte_Tme, new ReportEditMask ("MM/DD/YY"),"///STATUS",
        		pnd_Misc_Parm_Pnd_Status_Sname,"//CURRENT/UNIT",
        		extract_Record_Admin_Unit_Cde,"//PARTIC/NAME",
        		extract_Record_Pnd_Partic_Sname,"///PIN",
        		extract_Record_Pin_Nbr, new ReportEditMask ("999999999999"),"//CONTRACT/NUMBER",
        		extract_Record_Cntrct_Nbr);
    }
}
