/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:26:36 PM
**        * FROM NATURAL PROGRAM : Cwfb3012
************************************************************
**        * FILE NAME            : Cwfb3012.java
**        * CLASS NAME           : Cwfb3012
**        * INSTANCE NAME        : Cwfb3012
************************************************************
************************************************************************
* PROGRAM  : CWFB3012
* SYSTEM   : CRPCWF
* TITLE    : EXTRACT
* GENERATED: OCT 05,93 AT 08:41 AM
* FUNCTION :
*          | - GET REPORT INPUT DATA FROM CWF-SUPPORT-TBL
*          | - CALL EXTRACT PROGRAM
*          | - CALL REPORT PROGRAM
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3012 extends BLNatBase
{
    // Data Areas
    private PdaCwfa3012 pdaCwfa3012;
    private PdaCwfa3300 pdaCwfa3300;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Tbl_Prnt_Flag;
    private DbsField cwf_Support_Tbl_Tbl_Rqst_Unit;
    private DbsField cwf_Support_Tbl_Tbl_Rqst_Racf;
    private DbsField cwf_Support_Tbl_Tbl_Rqst_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Rqst_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Tbl_Input_Data;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;

    private DataAccessProgramView vw_cwf_Support_Tbl1;
    private DbsField cwf_Support_Tbl1_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl1_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl1_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl1__R_Field_3;
    private DbsField cwf_Support_Tbl1_Tbl_Prnt_Flag;
    private DbsField cwf_Support_Tbl1_Tbl_Rqst_Unit;
    private DbsField cwf_Support_Tbl1_Tbl_Rqst_Racf;
    private DbsField cwf_Support_Tbl1_Tbl_Rqst_Dte;
    private DbsField cwf_Support_Tbl1_Tbl_Rqst_Tme;
    private DbsField cwf_Support_Tbl1_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl1__R_Field_4;
    private DbsField cwf_Support_Tbl1_Tbl_Input_Data;
    private DbsField cwf_Support_Tbl1_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl1_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl1_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl1_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl1_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl1_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl1_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl1_Tbl_Table_Rectype;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_5;

    private DbsGroup pnd_Tbl_Key_Pnd_Structure;
    private DbsField pnd_Tbl_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Key_Field;
    private DbsField pnd_Rep_Rqsted;
    private DbsField pnd_Report_No;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Comp_Date;

    private DbsGroup pnd_Comp_Date__R_Field_6;
    private DbsField pnd_Comp_Date_Pnd_Run_Date;
    private DbsField pnd_Work_Start_Date;
    private DbsField pnd_Work_Comp_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa3012 = new PdaCwfa3012(localVariables);
        pdaCwfa3300 = new PdaCwfa3300(localVariables);

        // Local Variables

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_Prnt_Flag = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Tbl_Prnt_Flag", "TBL-PRNT-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Tbl_Rqst_Unit = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Tbl_Rqst_Unit", "TBL-RQST-UNIT", FieldType.STRING, 
            7);
        cwf_Support_Tbl_Tbl_Rqst_Racf = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Tbl_Rqst_Racf", "TBL-RQST-RACF", FieldType.STRING, 
            7);
        cwf_Support_Tbl_Tbl_Rqst_Dte = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Tbl_Rqst_Dte", "TBL-RQST-DTE", FieldType.NUMERIC, 8);
        cwf_Support_Tbl_Tbl_Rqst_Tme = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Tbl_Rqst_Tme", "TBL-RQST-TME", FieldType.NUMERIC, 7);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_2 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Input_Data = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Tbl_Input_Data", "TBL-INPUT-DATA", FieldType.STRING, 
            180);
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        registerRecord(vw_cwf_Support_Tbl);

        vw_cwf_Support_Tbl1 = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl1", "CWF-SUPPORT-TBL1"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl1_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl1_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl1_Tbl_Table_Nme = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl1_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl1_Tbl_Key_Field = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl1__R_Field_3 = vw_cwf_Support_Tbl1.getRecord().newGroupInGroup("cwf_Support_Tbl1__R_Field_3", "REDEFINE", cwf_Support_Tbl1_Tbl_Key_Field);
        cwf_Support_Tbl1_Tbl_Prnt_Flag = cwf_Support_Tbl1__R_Field_3.newFieldInGroup("cwf_Support_Tbl1_Tbl_Prnt_Flag", "TBL-PRNT-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl1_Tbl_Rqst_Unit = cwf_Support_Tbl1__R_Field_3.newFieldInGroup("cwf_Support_Tbl1_Tbl_Rqst_Unit", "TBL-RQST-UNIT", FieldType.STRING, 
            7);
        cwf_Support_Tbl1_Tbl_Rqst_Racf = cwf_Support_Tbl1__R_Field_3.newFieldInGroup("cwf_Support_Tbl1_Tbl_Rqst_Racf", "TBL-RQST-RACF", FieldType.STRING, 
            7);
        cwf_Support_Tbl1_Tbl_Rqst_Dte = cwf_Support_Tbl1__R_Field_3.newFieldInGroup("cwf_Support_Tbl1_Tbl_Rqst_Dte", "TBL-RQST-DTE", FieldType.NUMERIC, 
            8);
        cwf_Support_Tbl1_Tbl_Rqst_Tme = cwf_Support_Tbl1__R_Field_3.newFieldInGroup("cwf_Support_Tbl1_Tbl_Rqst_Tme", "TBL-RQST-TME", FieldType.NUMERIC, 
            7);
        cwf_Support_Tbl1_Tbl_Data_Field = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl1__R_Field_4 = vw_cwf_Support_Tbl1.getRecord().newGroupInGroup("cwf_Support_Tbl1__R_Field_4", "REDEFINE", cwf_Support_Tbl1_Tbl_Data_Field);
        cwf_Support_Tbl1_Tbl_Input_Data = cwf_Support_Tbl1__R_Field_4.newFieldInGroup("cwf_Support_Tbl1_Tbl_Input_Data", "TBL-INPUT-DATA", FieldType.STRING, 
            180);
        cwf_Support_Tbl1_Tbl_Actve_Ind = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl1_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl1_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl1_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl1_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl1_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl1_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl1_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl1_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl1_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl1_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl1_Tbl_Table_Rectype = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        registerRecord(vw_cwf_Support_Tbl1);

        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 53);

        pnd_Tbl_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_5", "REDEFINE", pnd_Tbl_Key);

        pnd_Tbl_Key_Pnd_Structure = pnd_Tbl_Key__R_Field_5.newGroupInGroup("pnd_Tbl_Key_Pnd_Structure", "#STRUCTURE");
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Key_Pnd_Structure.newFieldInGroup("pnd_Tbl_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Tbl_Table_Nme = pnd_Tbl_Key_Pnd_Structure.newFieldInGroup("pnd_Tbl_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Tbl_Key_Field = pnd_Tbl_Key_Pnd_Structure.newFieldInGroup("pnd_Tbl_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);

        pnd_Tbl_Prime_Key = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY");
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Key_Tbl_Table_Nme = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_Tbl_Key_Field = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Rep_Rqsted = localVariables.newFieldInRecord("pnd_Rep_Rqsted", "#REP-RQSTED", FieldType.BOOLEAN, 1);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.NUMERIC, 8);

        pnd_Comp_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Comp_Date__R_Field_6", "REDEFINE", pnd_Comp_Date);
        pnd_Comp_Date_Pnd_Run_Date = pnd_Comp_Date__R_Field_6.newFieldInGroup("pnd_Comp_Date_Pnd_Run_Date", "#RUN-DATE", FieldType.STRING, 8);
        pnd_Work_Start_Date = localVariables.newFieldInRecord("pnd_Work_Start_Date", "#WORK-START-DATE", FieldType.DATE);
        pnd_Work_Comp_Date = localVariables.newFieldInRecord("pnd_Work_Comp_Date", "#WORK-COMP-DATE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();
        vw_cwf_Support_Tbl1.reset();

        localVariables.reset();
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind.setInitialValue("A");
        pnd_Tbl_Prime_Key_Tbl_Table_Nme.setInitialValue("CWF-REPORT12-INPUT");
        pnd_Tbl_Prime_Key_Tbl_Key_Field.setInitialValue("A");
        pnd_Rep_Rqsted.setInitialValue(false);
        pnd_Report_No.setInitialValue(12);
        pnd_Report_Parm.setInitialValue("CWFB3012D*");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3012() throws Exception
    {
        super("Cwfb3012");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Tbl_Key_Pnd_Structure.setValuesByName(pnd_Tbl_Prime_Key);                                                                                                 //Natural: MOVE BY NAME #TBL-PRIME-KEY TO #TBL-KEY.#STRUCTURE
            //*  READ CWF-SUPPORT-TBL
            vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CWF-SUPPORT-TBL BY TBL-PRIME-KEY FROM #TBL-KEY
            (
            "READ_TBL",
            new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Key, WcType.BY) },
            new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
            1
            );
            READ_TBL:
            while (condition(vw_cwf_Support_Tbl.readNextRow("READ_TBL")))
            {
                if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pnd_Tbl_Key_Tbl_Table_Nme) || cwf_Support_Tbl_Tbl_Prnt_Flag.notEquals("A")))                        //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME NE #TBL-KEY.TBL-TABLE-NME OR CWF-SUPPORT-TBL.TBL-PRNT-FLAG NE 'A'
                {
                    if (true) break READ_TBL;                                                                                                                             //Natural: ESCAPE BOTTOM ( READ-TBL. )
                }                                                                                                                                                         //Natural: END-IF
                pdaCwfa3012.getPnd_Input_Data().setValue(cwf_Support_Tbl_Tbl_Input_Data);                                                                                 //Natural: MOVE CWF-SUPPORT-TBL.TBL-INPUT-DATA TO #INPUT-DATA
                pdaCwfa3300.getPnd_Parm_Data_Pnd_Key_Field().setValue(cwf_Support_Tbl_Tbl_Key_Field);                                                                     //Natural: MOVE CWF-SUPPORT-TBL.TBL-KEY-FIELD TO #KEY-FIELD
                pdaCwfa3300.getPnd_Parm_Data_Pnd_Rqst_Unit().setValue(cwf_Support_Tbl_Tbl_Rqst_Unit);                                                                     //Natural: MOVE CWF-SUPPORT-TBL.TBL-RQST-UNIT TO #RQST-UNIT
                pdaCwfa3300.getPnd_Parm_Data_Pnd_Rqst_Count().nadd(1);                                                                                                    //Natural: ADD 1 TO #RQST-COUNT
                pnd_Rep_Rqsted.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #REP-RQSTED
                if (true) break READ_TBL;                                                                                                                                 //Natural: ESCAPE BOTTOM ( READ-TBL. )
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(! (pnd_Rep_Rqsted.getBoolean())))                                                                                                               //Natural: IF NOT #REP-RQSTED
            {
                if (condition(pdaCwfa3300.getPnd_Parm_Data_Pnd_Rqst_Count().equals(getZero())))                                                                           //Natural: IF #RQST-COUNT = 0
                {
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Comp_Date.setValue(Global.getDATN());                                                                                                             //Natural: MOVE *DATN TO #COMP-DATE
                    pdaCwfa3300.getPnd_Parm_Data_Pnd_Rqst_Unit().setValue("CWF     ");                                                                                    //Natural: MOVE 'CWF     ' TO #RQST-UNIT
                    DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pdaCwfa3300.getPnd_Parm_Data_Pnd_Rqst_Unit(),                  //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID #RQST-UNIT #FLOOR #BLDG #DROP-OFF #RUN-DATE
                        pnd_Floor, pnd_Bldg, pnd_Drop_Off, pnd_Comp_Date_Pnd_Run_Date);
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                    DbsUtil.callnat(Cwfn3918.class , getCurrentProcessState(), pnd_Report_No);                                                                            //Natural: CALLNAT 'CWFN3918' #REPORT-NO
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                    DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                           //Natural: CALLNAT 'CWFN3911'
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.terminate();  if (true) return;                                                                                                                   //Natural: TERMINATE
            }                                                                                                                                                             //Natural: END-IF
            //*  ***************
            //*  EXTRACT RECORD
            //*  **************
            pdaCwfa3300.getPnd_Parm_Data_Pnd_Extrct_Count().reset();                                                                                                      //Natural: RESET #EXTRCT-COUNT
            DbsUtil.callnat(Cwfn3300.class , getCurrentProcessState(), pdaCwfa3012.getPnd_Input_Data(), pdaCwfa3300.getPnd_Parm_Data());                                  //Natural: CALLNAT 'CWFN3300' #INPUT-DATA #PARM-DATA
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Floor.reset();                                                                                                                                            //Natural: RESET #FLOOR #BLDG
            pnd_Bldg.reset();
            pnd_Comp_Date.setValue(Global.getDATN());                                                                                                                     //Natural: MOVE *DATN TO #COMP-DATE
            DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pdaCwfa3300.getPnd_Parm_Data_Pnd_Rqst_Unit(), pnd_Floor,               //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID #RQST-UNIT #FLOOR #BLDG #DROP-OFF #RUN-DATE
                pnd_Bldg, pnd_Drop_Off, pnd_Comp_Date_Pnd_Run_Date);
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            DbsUtil.callnat(Cwfn3320.class , getCurrentProcessState(), pdaCwfa3012.getPnd_Input_Data(), pdaCwfa3300.getPnd_Parm_Data());                                  //Natural: CALLNAT 'CWFN3320' #INPUT-DATA #PARM-DATA
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaCwfa3300.getPnd_Parm_Data_Pnd_Extrct_Count().greater(getZero())))                                                                            //Natural: IF #EXTRCT-COUNT > 0
            {
                pdaCwfa3300.getPnd_Parm_Data_Pnd_Rprt_Count().nadd(1);                                                                                                    //Natural: ADD 1 TO #RPRT-COUNT
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            pnd_Tbl_Key_Pnd_Structure.setValuesByName(pnd_Tbl_Prime_Key);                                                                                                 //Natural: MOVE BY NAME #TBL-PRIME-KEY TO #TBL-KEY.#STRUCTURE
            pnd_Tbl_Key_Tbl_Key_Field.setValue(pdaCwfa3300.getPnd_Parm_Data_Pnd_Key_Field());                                                                             //Natural: MOVE #KEY-FIELD TO #TBL-KEY.TBL-KEY-FIELD
            //*  READ CWF-SUPPORT-TBL
            vw_cwf_Support_Tbl1.startDatabaseFind                                                                                                                         //Natural: FIND ( 1 ) CWF-SUPPORT-TBL1 WITH TBL-PRIME-KEY = #TBL-KEY
            (
            "FIND",
            new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Key, WcType.WITH) },
            1
            );
            FIND:
            while (condition(vw_cwf_Support_Tbl1.readNextRow("FIND")))
            {
                vw_cwf_Support_Tbl1.setIfNotFoundControlFlag(false);
                GET:                                                                                                                                                      //Natural: GET CWF-SUPPORT-TBL1 *ISN ( FIND. )
                vw_cwf_Support_Tbl1.readByID(vw_cwf_Support_Tbl1.getAstISN("FIND"), "GET");
                cwf_Support_Tbl1_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                            //Natural: MOVE *TIMX TO CWF-SUPPORT-TBL1.TBL-UPDTE-DTE-TME
                cwf_Support_Tbl1_Tbl_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                     //Natural: MOVE *INIT-USER TO CWF-SUPPORT-TBL1.TBL-UPDTE-OPRTR-CDE
                cwf_Support_Tbl1_Tbl_Prnt_Flag.setValue("D");                                                                                                             //Natural: MOVE 'D' TO CWF-SUPPORT-TBL1.TBL-PRNT-FLAG
                vw_cwf_Support_Tbl1.updateDBRow("GET");                                                                                                                   //Natural: UPDATE ( GET. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                if (true) break FIND;                                                                                                                                     //Natural: ESCAPE BOTTOM ( FIND. )
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rep_Rqsted.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #REP-RQSTED
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }

    //
}
