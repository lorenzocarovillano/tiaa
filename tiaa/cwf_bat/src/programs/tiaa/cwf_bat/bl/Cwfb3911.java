/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:30:58 PM
**        * FROM NATURAL PROGRAM : Cwfb3911
************************************************************
**        * FILE NAME            : Cwfb3911.java
**        * CLASS NAME           : Cwfb3911
**        * INSTANCE NAME        : Cwfb3911
************************************************************
************************************************************************
* PROGRAM  : CWFP3911
* SYSTEM   : CRPCWF
* TITLE    : BATCH WEEKLY STREAM STARTUP
* GENERATED: OCT 05,93 AT 08:41 AM
* FUNCTION :
*          | STANDARD WEEKLY STREAM STARTUP PROGRAM.
*          | THIS PROGRAM READS THE WEEKLY RUN RECORD FROM
*          | THE SUPPORT TABLE AND CHECKS IF LAST RUN FLAG = Y.
*          | IF LAST RUN FLAG NE Y, THE STREAM ABENDED
*          | AND THIS PROGRAM WILL NOT UPDATE THE LAST RUN DATE,
*          | OTHERWISE, THE RUN DATE WILL BE UPDATED TO THE NEXT
*          | BUSINESS DAY.
*          |
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3911 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Tbl_Last_Run_Date;
    private DbsField cwf_Support_Tbl_Tbl_Last_Run_Flag;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_2;

    private DbsGroup pnd_Tbl_Key_Data_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Key_Field;
    private DbsField pnd_Work_Date;
    private DbsField pnd_Ref_Date;
    private DbsField pnd_Msg;
    private DbsField pnd_Sub;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Tbl_Run_Date;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_No_Extract;
    private DbsField pnd_Date;
    private DbsField pnd_Day;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Last_Run_Date = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Tbl_Last_Run_Date", "TBL-LAST-RUN-DATE", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Tbl_Last_Run_Flag = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Tbl_Last_Run_Flag", "TBL-LAST-RUN-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 53);

        pnd_Tbl_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_2", "REDEFINE", pnd_Tbl_Key);

        pnd_Tbl_Key_Data_Nme = pnd_Tbl_Key__R_Field_2.newGroupInGroup("pnd_Tbl_Key_Data_Nme", "DATA-NME");
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Tbl_Table_Nme = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Tbl_Key_Field = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);

        pnd_Tbl_Prime_Key = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY");
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Key_Tbl_Table_Nme = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_Tbl_Key_Field = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_Ref_Date = localVariables.newFieldInRecord("pnd_Ref_Date", "#REF-DATE", FieldType.DATE);
        pnd_Msg = localVariables.newFieldInRecord("pnd_Msg", "#MSG", FieldType.STRING, 79);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.PACKED_DECIMAL, 4);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Tbl_Run_Date = localVariables.newFieldInRecord("pnd_Tbl_Run_Date", "#TBL-RUN-DATE", FieldType.STRING, 8);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_No_Extract = localVariables.newFieldInRecord("pnd_No_Extract", "#NO-EXTRACT", FieldType.NUMERIC, 1);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Day = localVariables.newFieldInRecord("pnd_Day", "#DAY", FieldType.STRING, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind.setInitialValue("A");
        pnd_Tbl_Prime_Key_Tbl_Table_Nme.setInitialValue("CWF-RPRT-RUN-TBL");
        pnd_Tbl_Prime_Key_Tbl_Key_Field.setInitialValue("W");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3911() throws Exception
    {
        super("Cwfb3911");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pnd_Tbl_Key_Data_Nme.setValuesByName(pnd_Tbl_Prime_Key);                                                                                                          //Natural: MOVE BY NAME #TBL-PRIME-KEY TO #TBL-KEY.DATA-NME
        //*  READ CWF-SUPPORT-TBL
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) CWF-SUPPORT-TBL BY TBL-PRIME-KEY FROM #TBL-KEY
        (
        "READ",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ")))
        {
            //*  SUCCESSFUL LAST RUN
            if (condition(cwf_Support_Tbl_Tbl_Last_Run_Flag.equals("Y")))                                                                                                 //Natural: IF TBL-LAST-RUN-FLAG = 'Y'
            {
                GET01:                                                                                                                                                    //Natural: GET CWF-SUPPORT-TBL *ISN ( READ. )
                vw_cwf_Support_Tbl.readByID(vw_cwf_Support_Tbl.getAstISN("READ"), "GET01");
                pnd_Work_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Support_Tbl_Tbl_Last_Run_Date);                                                           //Natural: MOVE EDITED TBL-LAST-RUN-DATE TO #WORK-DATE ( EM = YYYYMMDD )
                cwf_Support_Tbl_Tbl_Last_Run_Flag.reset();                                                                                                                //Natural: RESET TBL-LAST-RUN-FLAG
                                                                                                                                                                          //Natural: PERFORM GET-NEXT-RUN-DATE
                sub_Get_Next_Run_Date();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                      //Natural: MOVE *INIT-USER TO CWF-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE
                cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                             //Natural: MOVE *TIMX TO CWF-SUPPORT-TBL.TBL-UPDTE-DTE-TME
                cwf_Support_Tbl_Tbl_Last_Run_Date.setValueEdited(pnd_Work_Date,new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED #WORK-DATE ( EM = YYYYMMDD ) TO TBL-LAST-RUN-DATE
                vw_cwf_Support_Tbl.updateDBRow("READ");                                                                                                                   //Natural: UPDATE ( READ. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            pnd_Comp_Date.setValue(cwf_Support_Tbl_Tbl_Last_Run_Date);                                                                                                    //Natural: MOVE TBL-LAST-RUN-DATE TO #COMP-DATE
            //*  READ.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NEXT-RUN-DATE
    }
    private void sub_Get_Next_Run_Date() throws Exception                                                                                                                 //Natural: GET-NEXT-RUN-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Date.setValue(Global.getDATX());                                                                                                                              //Natural: MOVE *DATX TO #DATE
        pnd_Ref_Date.setValue(pnd_Date);                                                                                                                                  //Natural: MOVE #DATE TO #REF-DATE
        pnd_Day.setValueEdited(pnd_Date,new ReportEditMask("NNN"));                                                                                                       //Natural: MOVE EDITED #DATE ( EM = NNN ) TO #DAY
        short decideConditionsMet90 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE OF #DAY;//Natural: VALUE 'Sat'
        if (condition((pnd_Day.equals("Sat"))))
        {
            decideConditionsMet90++;
            pnd_Ref_Date.nsubtract(1);                                                                                                                                    //Natural: SUBTRACT 1 FROM #REF-DATE
        }                                                                                                                                                                 //Natural: VALUE 'Sun'
        else if (condition((pnd_Day.equals("Sun"))))
        {
            decideConditionsMet90++;
            pnd_Ref_Date.nsubtract(2);                                                                                                                                    //Natural: SUBTRACT 2 FROM #REF-DATE
        }                                                                                                                                                                 //Natural: VALUE 'Mon'
        else if (condition((pnd_Day.equals("Mon"))))
        {
            decideConditionsMet90++;
            pnd_Ref_Date.nsubtract(3);                                                                                                                                    //Natural: SUBTRACT 3 FROM #REF-DATE
        }                                                                                                                                                                 //Natural: VALUE 'Tue'
        else if (condition((pnd_Day.equals("Tue"))))
        {
            decideConditionsMet90++;
            pnd_Ref_Date.nsubtract(4);                                                                                                                                    //Natural: SUBTRACT 4 FROM #REF-DATE
        }                                                                                                                                                                 //Natural: VALUE 'Wed'
        else if (condition((pnd_Day.equals("Wed"))))
        {
            decideConditionsMet90++;
            pnd_Ref_Date.nsubtract(5);                                                                                                                                    //Natural: SUBTRACT 5 FROM #REF-DATE
        }                                                                                                                                                                 //Natural: VALUE 'Thu'
        else if (condition((pnd_Day.equals("Thu"))))
        {
            decideConditionsMet90++;
            pnd_Ref_Date.nsubtract(6);                                                                                                                                    //Natural: SUBTRACT 6 FROM #REF-DATE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  UPDATE DATE, RERUN DURING DIFF. WEEK
        if (condition(pnd_Ref_Date.notEquals(pnd_Work_Date)))                                                                                                             //Natural: IF #REF-DATE NE #WORK-DATE
        {
            pnd_Work_Date.setValue(pnd_Ref_Date);                                                                                                                         //Natural: MOVE #REF-DATE TO #WORK-DATE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
