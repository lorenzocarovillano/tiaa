/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:43:31 PM
**        * FROM NATURAL PROGRAM : Cwfb6750
************************************************************
**        * FILE NAME            : Cwfb6750.java
**        * CLASS NAME           : Cwfb6750
**        * INSTANCE NAME        : Cwfb6750
************************************************************
************************************************************************
* PROGRAM  : CWFB6750
* SYSTEM   : CWF MUTUAL FUNDS
* TITLE    : CWF SHARE SERVICE REQUEST SWEEP FUNCTION #2
* FUNCTION : READ THE RECORDS LEFT FROM THE SSR SWEEP FUNCTION #1,
*            WHICH ARE EITHER DATA BEEN CORRECTED OR DIDN't run before.
*
* 03/03/97 | GH  CREATED NEW.
* 08/11/97 | GH  ISSUE ET FOR EVERY 20 MIT RECORDS PROCESSED.
* 08/22/97 | GH  DON't create SSR if status is CLOSED and SSR existed..
* 06/12/98 | GH  MODIFIED FOR TRUST SERVICES
* 01/02/02 | GH  INCREASED #COUNT FROM N3 TO N6
* 03/01/02 | GH  ADD THE REJECT ID LOGIC FROM CWF-MIT-SWEEP TABLE.
* 06/13/03 | GH  EXTEND THE ARRAY INDEX OF #SSR-IND-ARRAY TO 100.
* 11/20/10 | GH  COMMENT OUT : CWF-MIT.MSTR-INDX-ACTN-CDE NE '10'
*          |     FOR POA REQUESTS CAN BE PICKED UP.
* 12/23/15 | JB  COR/NAAD RETIREMENT - ADD MQ OPEN/CLOSE
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb6750 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaCwfl6800 ldaCwfl6800;
    private PdaErla1000 pdaErla1000;
    private PdaFfta1300 pdaFfta1300;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup msg_Info;
    private DbsField msg_Info_Pnd_Pnd_Msg;
    private DbsField msg_Info_Pnd_Pnd_Msg_Nr;
    private DbsField msg_Info_Pnd_Pnd_Msg_Data;
    private DbsField msg_Info_Pnd_Pnd_Return_Code;
    private DbsField msg_Info_Pnd_Pnd_Error_Field;
    private DbsField msg_Info_Pnd_Pnd_Error_Field_Index1;
    private DbsField msg_Info_Pnd_Pnd_Error_Field_Index2;
    private DbsField msg_Info_Pnd_Pnd_Error_Field_Index3;

    private DataAccessProgramView vw_cwf_Doc;
    private DbsField cwf_Doc_Cabinet_Id;
    private DbsField cwf_Doc_Tiaa_Rcvd_Dte;
    private DbsField cwf_Doc_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Doc_Document_Type;
    private DbsField cwf_Doc_Document_Category;
    private DbsField cwf_Doc_Document_Sub_Category;
    private DbsField cwf_Doc_Document_Detail;

    private DataAccessProgramView vw_cwf_Mit_Sub;
    private DbsField cwf_Mit_Sub_Pin_Nbr;
    private DbsField cwf_Mit_Sub_Rqst_Log_Dte_Tme;
    private DbsField cwf_Mit_Sub_Case_Id_Cde;

    private DbsGroup cwf_Mit_Sub__R_Field_1;
    private DbsField cwf_Mit_Sub_Case_Ind;
    private DbsField cwf_Mit_Sub_Work_Prcss_Id;
    private DbsField cwf_Mit_Sub_Tiaa_Rcvd_Dte;
    private DbsField cwf_Mit_Sub_Status_Cde;
    private DbsField cwf_Mit_Sub_Crprte_Status_Ind;

    private DataAccessProgramView vw_cwf_H1;
    private DbsField cwf_H1_Pin_Wpid_Key;

    private DbsGroup cwf_H1__R_Field_2;
    private DbsField cwf_H1_Pnd_H1_Pin;
    private DbsField cwf_H1_Pnd_H1_Status;
    private DbsField cwf_H1_Pnd_H1_Wpid;

    private DataAccessProgramView vw_cwf_Suptb;
    private DbsField cwf_Suptb_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Suptb_Tbl_Table_Nme;
    private DbsField cwf_Suptb_Tbl_Key_Field;
    private DbsField cwf_Suptb_Tbl_Data_Field;
    private DbsField cwf_Suptb_Tbl_Actve_Ind;
    private DbsField cwf_Suptb_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Suptb_Tbl_Updte_Dte;
    private DbsField cwf_Suptb_Tbl_Updte_Oprtr_Cde;

    private DataAccessProgramView vw_cwf_Wpid;
    private DbsField cwf_Wpid_Work_Prcss_Id;
    private DbsField cwf_Wpid_Actve_Ind;
    private DbsField cwf_Wpid_Shrd_Srvce_Ind;
    private DbsField cwf_Wpid_Dlte_Oprtr_Cde;
    private DbsField cwf_Wpid_Work_Prcss_Short_Nme;

    private DbsGroup pnd_Vars;
    private DbsField pnd_Vars_Pnd_Actve_Ind_2;
    private DbsField pnd_Vars_Pnd_Case_Ind;
    private DbsField pnd_Vars_Pnd_Case_Id_Key;

    private DbsGroup pnd_Vars__R_Field_3;
    private DbsField pnd_Vars_Pnd_Case_Id_Key_1;
    private DbsField pnd_Vars_Pnd_Case_Id_Key_2;
    private DbsField pnd_Vars_Pnd_Case_Id_Key_3;
    private DbsField pnd_Vars_Pnd_Cntrct_Nbr;
    private DbsField pnd_Vars_Pnd_Count;
    private DbsField pnd_Vars_Pnd_Counter;
    private DbsField pnd_Vars_Pnd_Counter_Et;
    private DbsField pnd_Vars_Pnd_Counter_Mit_Record;
    private DbsField pnd_Vars_Pnd_Counter_Mit_Record_All;
    private DbsField pnd_Vars_Pnd_Crprte_Status_Chnge_Dte_Key;

    private DbsGroup pnd_Vars__R_Field_4;
    private DbsField pnd_Vars_Pnd_Crprte_Status_Ind;
    private DbsField pnd_Vars_Pnd_Last_Chnge_Invrt_Dte_Tme;
    private DbsField pnd_Vars_Pnd_Ctr;
    private DbsField pnd_Vars_Pnd_Cwf_Doc_Found;
    private DbsField pnd_Vars_Pnd_Cwf_Ssr_Found;
    private DbsField pnd_Vars_Pnd_Cwf_Doc_Index;
    private DbsField pnd_Vars_Pnd_Document_Type;

    private DbsGroup pnd_Vars__R_Field_5;
    private DbsField pnd_Vars_Pnd_Document_Category;
    private DbsField pnd_Vars_Pnd_Document_Sub_Category;
    private DbsField pnd_Vars_Pnd_Document_Detail;
    private DbsField pnd_Vars_Pnd_Document_Key;

    private DbsGroup pnd_Vars__R_Field_6;
    private DbsField pnd_Vars_Pnd_Doc_Cabinet_Id;

    private DbsGroup pnd_Vars__R_Field_7;
    private DbsField pnd_Vars_Pnd_Doc_Cabinet_Type;
    private DbsField pnd_Vars_Pnd_Doc_Cabinet_Name;

    private DbsGroup pnd_Vars__R_Field_8;
    private DbsField pnd_Vars_Pnd_Doc_Cabinet_Name_A12;
    private DbsField pnd_Vars_Pnd_Doc_Tiaa_Rcvd_Dte;
    private DbsField pnd_Vars_Pnd_Case_Workprcss_Multi_Subrqst;

    private DbsGroup pnd_Vars__R_Field_9;
    private DbsField pnd_Vars_Pnd_Doc_Case_Ind;
    private DbsField pnd_Vars_Pnd_Doc_Originating_Work_Prcss_Id;
    private DbsField pnd_Vars_Pnd_Doc_Multi_Rqst_Ind;
    private DbsField pnd_Vars_Pnd_Doc_Sub_Rqst_Ind;
    private DbsField pnd_Vars_Pnd_Doc_Entry_Dte_Tme;
    private DbsField pnd_Vars_Pnd_Error_Ind;
    private DbsField pnd_Vars_Pnd_Mf_Control_Rec_Isn;
    private DbsField pnd_Vars_Pnd_Mf_End_Dt;

    private DbsGroup pnd_Vars__R_Field_10;
    private DbsField pnd_Vars_Pnd_Mf_End_Dt_N;
    private DbsField pnd_Vars_Pnd_Mf_Next_End_Read_Dt;
    private DbsField pnd_Vars_Pnd_Mf_Start_Dt;

    private DbsGroup pnd_Vars__R_Field_11;
    private DbsField pnd_Vars_Pnd_Mf_Start_Dt_N;
    private DbsField pnd_Vars_Pnd_Lob_Found;
    private DbsField pnd_Vars_Pnd_Lob_Ind;
    private DbsField pnd_Vars_Pnd_Mf_Wpid_Index;
    private DbsField pnd_Vars_Pnd_Mutual_Fund;
    private DbsField pnd_Vars_Pnd_Mutual_Fund_Account;
    private DbsField pnd_Vars_Pnd_Nines;
    private DbsField pnd_Vars_Pnd_Pass_Name;

    private DbsGroup pnd_Vars__R_Field_12;
    private DbsField pnd_Vars_Pnd_Pass_Name_S;
    private DbsField pnd_Vars_Pnd_Pass_Ssn;

    private DbsGroup pnd_Vars__R_Field_13;
    private DbsField pnd_Vars_Pnd_Pass_Ssn_A;
    private DbsField pnd_Vars_Pnd_Pass_Tlc;
    private DbsField pnd_Vars_Pnd_Pda_Key;
    private DbsField pnd_Vars_Pnd_Pin_Nbr;
    private DbsField pnd_Vars_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_Ind;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_A_Add_Mf;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_A_Upd_Mf;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_D_Add_Mf;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_D_Upd_Mf;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_P_Add_Mf;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_P_Upd_Mf;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_A_Add_Ts;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_A_Upd_Ts;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_D_Add_Ts;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_D_Upd_Ts;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_P_Add_Ts;
    private DbsField pnd_Vars_Pnd_Shrd_Srvce_P_Upd_Ts;
    private DbsField pnd_Vars_Pnd_Ssr_Check_Mf;
    private DbsField pnd_Vars_Pnd_Ssr_Check_Ts;
    private DbsField pnd_Vars_Pnd_Ssr_Doc_Array;
    private DbsField pnd_Vars_Pnd_Ssr_Existed_Mf;
    private DbsField pnd_Vars_Pnd_Ssr_Existed_Ts;
    private DbsField pnd_Vars_Pnd_Ssr_Ind_Array;
    private DbsField pnd_Vars_Pnd_Ssr_Wpid;

    private DbsGroup pnd_Vars__R_Field_14;
    private DbsField pnd_Vars_Pnd_Ssr_Wpid_Act;
    private DbsField pnd_Vars_Pnd_Ssr_Wpid_Lob;
    private DbsField pnd_Vars_Pnd_Store_Msg;
    private DbsField pnd_Vars_Pnd_Tbl_Data_Field;

    private DbsGroup pnd_Vars__R_Field_15;
    private DbsField pnd_Vars_Pnd_Tbl_Data_Field_N;
    private DbsField pnd_Vars_Pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Vars__R_Field_16;
    private DbsField pnd_Vars_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Vars_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Vars_Pnd_Tbl_Key_Field;
    private DbsField pnd_Vars_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Vars_Pnd_Timx;
    private DbsField pnd_Vars_Pnd_Trust_Service;
    private DbsField pnd_Vars_Pnd_Trust_Service_Account;
    private DbsField pnd_Vars_Pnd_Wpid_Index;
    private DbsField pnd_Vars_Pnd_Work_Prcss_Id;
    private DbsField pnd_Vars_Pnd_Work_Prcss_Short_Nme;
    private DbsField pnd_Vars_Pnd_Work_Prcss_Short_Nme_Array;
    private DbsField pnd_Vars_Pnd_Wpid_Array;
    private DbsField pnd_Vars_Pnd_Pin_Wpid_Key;

    private DbsGroup pnd_Vars__R_Field_17;
    private DbsField pnd_Vars_Pnd_Pin_Wpid_Key_Pin;
    private DbsField pnd_Vars_Pnd_Pin_Wpid_Key_Status;
    private DbsField pnd_Vars_Pnd_Pin_Wpid_Key_Wpid;
    private DbsField pnd_Vars_Pnd_Sweep_Reject_Id;

    private DbsGroup pnd_Vars__R_Field_18;
    private DbsField pnd_Vars_Pnd_Sweep_Reject_Id_Array;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaCwfl6800 = new LdaCwfl6800();
        registerRecord(ldaCwfl6800);
        registerRecord(ldaCwfl6800.getVw_cwf_Mit());
        localVariables = new DbsRecord();
        pdaErla1000 = new PdaErla1000(localVariables);
        pdaFfta1300 = new PdaFfta1300(localVariables);

        // Local Variables

        msg_Info = localVariables.newGroupInRecord("msg_Info", "MSG-INFO");
        msg_Info_Pnd_Pnd_Msg = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Msg", "##MSG", FieldType.STRING, 79);
        msg_Info_Pnd_Pnd_Msg_Nr = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Msg_Nr", "##MSG-NR", FieldType.NUMERIC, 4);
        msg_Info_Pnd_Pnd_Msg_Data = msg_Info.newFieldArrayInGroup("msg_Info_Pnd_Pnd_Msg_Data", "##MSG-DATA", FieldType.STRING, 32, new DbsArrayController(1, 
            3));
        msg_Info_Pnd_Pnd_Return_Code = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Return_Code", "##RETURN-CODE", FieldType.STRING, 1);
        msg_Info_Pnd_Pnd_Error_Field = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Error_Field", "##ERROR-FIELD", FieldType.STRING, 32);
        msg_Info_Pnd_Pnd_Error_Field_Index1 = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Error_Field_Index1", "##ERROR-FIELD-INDEX1", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Pnd_Pnd_Error_Field_Index2 = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Error_Field_Index2", "##ERROR-FIELD-INDEX2", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Pnd_Pnd_Error_Field_Index3 = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Error_Field_Index3", "##ERROR-FIELD-INDEX3", FieldType.PACKED_DECIMAL, 
            3);

        vw_cwf_Doc = new DataAccessProgramView(new NameInfo("vw_cwf_Doc", "CWF-DOC"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Doc_Cabinet_Id = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Doc_Cabinet_Id.setDdmHeader("CABINET/ID");
        cwf_Doc_Tiaa_Rcvd_Dte = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Doc_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Doc_Case_Workprcss_Multi_Subrqst = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Doc_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Doc_Document_Type = vw_cwf_Doc.getRecord().newGroupInGroup("CWF_DOC_DOCUMENT_TYPE", "DOCUMENT-TYPE");
        cwf_Doc_Document_Type.setDdmHeader("DOCUMENT/TYPE");
        cwf_Doc_Document_Category = cwf_Doc_Document_Type.newFieldInGroup("cwf_Doc_Document_Category", "DOCUMENT-CATEGORY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DOCUMENT_CATEGORY");
        cwf_Doc_Document_Category.setDdmHeader("DOC/CAT");
        cwf_Doc_Document_Sub_Category = cwf_Doc_Document_Type.newFieldInGroup("cwf_Doc_Document_Sub_Category", "DOCUMENT-SUB-CATEGORY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DOCUMENT_SUB_CATEGORY");
        cwf_Doc_Document_Sub_Category.setDdmHeader("DOC SUB-/CATEGORY");
        cwf_Doc_Document_Detail = cwf_Doc_Document_Type.newFieldInGroup("cwf_Doc_Document_Detail", "DOCUMENT-DETAIL", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DOCUMENT_DETAIL");
        cwf_Doc_Document_Detail.setDdmHeader("DOCUMENT/DETAIL");
        registerRecord(vw_cwf_Doc);

        vw_cwf_Mit_Sub = new DataAccessProgramView(new NameInfo("vw_cwf_Mit_Sub", "CWF-MIT-SUB"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Mit_Sub_Pin_Nbr = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Mit_Sub_Pin_Nbr.setDdmHeader("PIN");
        cwf_Mit_Sub_Rqst_Log_Dte_Tme = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Mit_Sub_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Mit_Sub_Case_Id_Cde = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CASE_ID_CDE");
        cwf_Mit_Sub_Case_Id_Cde.setDdmHeader("CASE/ID");

        cwf_Mit_Sub__R_Field_1 = vw_cwf_Mit_Sub.getRecord().newGroupInGroup("cwf_Mit_Sub__R_Field_1", "REDEFINE", cwf_Mit_Sub_Case_Id_Cde);
        cwf_Mit_Sub_Case_Ind = cwf_Mit_Sub__R_Field_1.newFieldInGroup("cwf_Mit_Sub_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        cwf_Mit_Sub_Work_Prcss_Id = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Mit_Sub_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Mit_Sub_Tiaa_Rcvd_Dte = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Mit_Sub_Status_Cde = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        cwf_Mit_Sub_Crprte_Status_Ind = vw_cwf_Mit_Sub.getRecord().newFieldInGroup("cwf_Mit_Sub_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Mit_Sub_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        registerRecord(vw_cwf_Mit_Sub);

        vw_cwf_H1 = new DataAccessProgramView(new NameInfo("vw_cwf_H1", "CWF-H1"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_H1_Pin_Wpid_Key = vw_cwf_H1.getRecord().newFieldInGroup("cwf_H1_Pin_Wpid_Key", "PIN-WPID-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "PIN_WPID_KEY");
        cwf_H1_Pin_Wpid_Key.setSuperDescriptor(true);

        cwf_H1__R_Field_2 = vw_cwf_H1.getRecord().newGroupInGroup("cwf_H1__R_Field_2", "REDEFINE", cwf_H1_Pin_Wpid_Key);
        cwf_H1_Pnd_H1_Pin = cwf_H1__R_Field_2.newFieldInGroup("cwf_H1_Pnd_H1_Pin", "#H1-PIN", FieldType.STRING, 12);
        cwf_H1_Pnd_H1_Status = cwf_H1__R_Field_2.newFieldInGroup("cwf_H1_Pnd_H1_Status", "#H1-STATUS", FieldType.STRING, 1);
        cwf_H1_Pnd_H1_Wpid = cwf_H1__R_Field_2.newFieldInGroup("cwf_H1_Pnd_H1_Wpid", "#H1-WPID", FieldType.STRING, 6);
        registerRecord(vw_cwf_H1);

        vw_cwf_Suptb = new DataAccessProgramView(new NameInfo("vw_cwf_Suptb", "CWF-SUPTB"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Suptb_Tbl_Scrty_Level_Ind = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Suptb_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Suptb_Tbl_Table_Nme = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TBL_TABLE_NME");
        cwf_Suptb_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Suptb_Tbl_Key_Field = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "TBL_KEY_FIELD");
        cwf_Suptb_Tbl_Data_Field = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");
        cwf_Suptb_Tbl_Actve_Ind = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TBL_ACTVE_IND");
        cwf_Suptb_Tbl_Updte_Dte_Tme = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE_TME");
        cwf_Suptb_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Suptb_Tbl_Updte_Dte = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Suptb_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Suptb_Tbl_Updte_Oprtr_Cde = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Suptb_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Suptb);

        vw_cwf_Wpid = new DataAccessProgramView(new NameInfo("vw_cwf_Wpid", "CWF-WPID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wpid_Work_Prcss_Id = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Wpid_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wpid_Actve_Ind = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Wpid_Shrd_Srvce_Ind = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Shrd_Srvce_Ind", "SHRD-SRVCE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SHRD_SRVCE_IND");
        cwf_Wpid_Shrd_Srvce_Ind.setDdmHeader("SHARED/SERVICE");
        cwf_Wpid_Dlte_Oprtr_Cde = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DLTE_OPRTR_CDE");
        cwf_Wpid_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        cwf_Wpid_Work_Prcss_Short_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wpid_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        registerRecord(vw_cwf_Wpid);

        pnd_Vars = localVariables.newGroupInRecord("pnd_Vars", "#VARS");
        pnd_Vars_Pnd_Actve_Ind_2 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Actve_Ind_2", "#ACTVE-IND-2", FieldType.STRING, 1);
        pnd_Vars_Pnd_Case_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Case_Ind", "#CASE-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_Case_Id_Key = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Case_Id_Key", "#CASE-ID-KEY", FieldType.STRING, 18);

        pnd_Vars__R_Field_3 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_3", "REDEFINE", pnd_Vars_Pnd_Case_Id_Key);
        pnd_Vars_Pnd_Case_Id_Key_1 = pnd_Vars__R_Field_3.newFieldInGroup("pnd_Vars_Pnd_Case_Id_Key_1", "#CASE-ID-KEY-1", FieldType.NUMERIC, 12);
        pnd_Vars_Pnd_Case_Id_Key_2 = pnd_Vars__R_Field_3.newFieldInGroup("pnd_Vars_Pnd_Case_Id_Key_2", "#CASE-ID-KEY-2", FieldType.DATE);
        pnd_Vars_Pnd_Case_Id_Key_3 = pnd_Vars__R_Field_3.newFieldInGroup("pnd_Vars_Pnd_Case_Id_Key_3", "#CASE-ID-KEY-3", FieldType.STRING, 1);
        pnd_Vars_Pnd_Cntrct_Nbr = pnd_Vars.newFieldArrayInGroup("pnd_Vars_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10, new DbsArrayController(1, 
            10));
        pnd_Vars_Pnd_Count = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Count", "#COUNT", FieldType.NUMERIC, 6);
        pnd_Vars_Pnd_Counter = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Counter", "#COUNTER", FieldType.NUMERIC, 2);
        pnd_Vars_Pnd_Counter_Et = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Counter_Et", "#COUNTER-ET", FieldType.NUMERIC, 2);
        pnd_Vars_Pnd_Counter_Mit_Record = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Counter_Mit_Record", "#COUNTER-MIT-RECORD", FieldType.NUMERIC, 8);
        pnd_Vars_Pnd_Counter_Mit_Record_All = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Counter_Mit_Record_All", "#COUNTER-MIT-RECORD-ALL", FieldType.NUMERIC, 
            8);
        pnd_Vars_Pnd_Crprte_Status_Chnge_Dte_Key = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Crprte_Status_Chnge_Dte_Key", "#CRPRTE-STATUS-CHNGE-DTE-KEY", 
            FieldType.STRING, 25);

        pnd_Vars__R_Field_4 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_4", "REDEFINE", pnd_Vars_Pnd_Crprte_Status_Chnge_Dte_Key);
        pnd_Vars_Pnd_Crprte_Status_Ind = pnd_Vars__R_Field_4.newFieldInGroup("pnd_Vars_Pnd_Crprte_Status_Ind", "#CRPRTE-STATUS-IND", FieldType.STRING, 
            1);
        pnd_Vars_Pnd_Last_Chnge_Invrt_Dte_Tme = pnd_Vars__R_Field_4.newFieldInGroup("pnd_Vars_Pnd_Last_Chnge_Invrt_Dte_Tme", "#LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Vars_Pnd_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 3);
        pnd_Vars_Pnd_Cwf_Doc_Found = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Cwf_Doc_Found", "#CWF-DOC-FOUND", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Cwf_Ssr_Found = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Cwf_Ssr_Found", "#CWF-SSR-FOUND", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Cwf_Doc_Index = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Cwf_Doc_Index", "#CWF-DOC-INDEX", FieldType.PACKED_DECIMAL, 3);
        pnd_Vars_Pnd_Document_Type = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Document_Type", "#DOCUMENT-TYPE", FieldType.STRING, 8);

        pnd_Vars__R_Field_5 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_5", "REDEFINE", pnd_Vars_Pnd_Document_Type);
        pnd_Vars_Pnd_Document_Category = pnd_Vars__R_Field_5.newFieldInGroup("pnd_Vars_Pnd_Document_Category", "#DOCUMENT-CATEGORY", FieldType.STRING, 
            1);
        pnd_Vars_Pnd_Document_Sub_Category = pnd_Vars__R_Field_5.newFieldInGroup("pnd_Vars_Pnd_Document_Sub_Category", "#DOCUMENT-SUB-CATEGORY", FieldType.STRING, 
            3);
        pnd_Vars_Pnd_Document_Detail = pnd_Vars__R_Field_5.newFieldInGroup("pnd_Vars_Pnd_Document_Detail", "#DOCUMENT-DETAIL", FieldType.STRING, 4);
        pnd_Vars_Pnd_Document_Key = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Document_Key", "#DOCUMENT-KEY", FieldType.STRING, 34);

        pnd_Vars__R_Field_6 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_6", "REDEFINE", pnd_Vars_Pnd_Document_Key);
        pnd_Vars_Pnd_Doc_Cabinet_Id = pnd_Vars__R_Field_6.newFieldInGroup("pnd_Vars_Pnd_Doc_Cabinet_Id", "#DOC-CABINET-ID", FieldType.STRING, 14);

        pnd_Vars__R_Field_7 = pnd_Vars__R_Field_6.newGroupInGroup("pnd_Vars__R_Field_7", "REDEFINE", pnd_Vars_Pnd_Doc_Cabinet_Id);
        pnd_Vars_Pnd_Doc_Cabinet_Type = pnd_Vars__R_Field_7.newFieldInGroup("pnd_Vars_Pnd_Doc_Cabinet_Type", "#DOC-CABINET-TYPE", FieldType.STRING, 1);
        pnd_Vars_Pnd_Doc_Cabinet_Name = pnd_Vars__R_Field_7.newFieldInGroup("pnd_Vars_Pnd_Doc_Cabinet_Name", "#DOC-CABINET-NAME", FieldType.STRING, 13);

        pnd_Vars__R_Field_8 = pnd_Vars__R_Field_7.newGroupInGroup("pnd_Vars__R_Field_8", "REDEFINE", pnd_Vars_Pnd_Doc_Cabinet_Name);
        pnd_Vars_Pnd_Doc_Cabinet_Name_A12 = pnd_Vars__R_Field_8.newFieldInGroup("pnd_Vars_Pnd_Doc_Cabinet_Name_A12", "#DOC-CABINET-NAME-A12", FieldType.STRING, 
            12);
        pnd_Vars_Pnd_Doc_Tiaa_Rcvd_Dte = pnd_Vars__R_Field_6.newFieldInGroup("pnd_Vars_Pnd_Doc_Tiaa_Rcvd_Dte", "#DOC-TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Vars_Pnd_Case_Workprcss_Multi_Subrqst = pnd_Vars__R_Field_6.newFieldInGroup("pnd_Vars_Pnd_Case_Workprcss_Multi_Subrqst", "#CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9);

        pnd_Vars__R_Field_9 = pnd_Vars__R_Field_6.newGroupInGroup("pnd_Vars__R_Field_9", "REDEFINE", pnd_Vars_Pnd_Case_Workprcss_Multi_Subrqst);
        pnd_Vars_Pnd_Doc_Case_Ind = pnd_Vars__R_Field_9.newFieldInGroup("pnd_Vars_Pnd_Doc_Case_Ind", "#DOC-CASE-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_Doc_Originating_Work_Prcss_Id = pnd_Vars__R_Field_9.newFieldInGroup("pnd_Vars_Pnd_Doc_Originating_Work_Prcss_Id", "#DOC-ORIGINATING-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        pnd_Vars_Pnd_Doc_Multi_Rqst_Ind = pnd_Vars__R_Field_9.newFieldInGroup("pnd_Vars_Pnd_Doc_Multi_Rqst_Ind", "#DOC-MULTI-RQST-IND", FieldType.STRING, 
            1);
        pnd_Vars_Pnd_Doc_Sub_Rqst_Ind = pnd_Vars__R_Field_9.newFieldInGroup("pnd_Vars_Pnd_Doc_Sub_Rqst_Ind", "#DOC-SUB-RQST-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_Doc_Entry_Dte_Tme = pnd_Vars__R_Field_6.newFieldInGroup("pnd_Vars_Pnd_Doc_Entry_Dte_Tme", "#DOC-ENTRY-DTE-TME", FieldType.TIME);
        pnd_Vars_Pnd_Error_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Error_Ind", "#ERROR-IND", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Mf_Control_Rec_Isn = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Mf_Control_Rec_Isn", "#MF-CONTROL-REC-ISN", FieldType.PACKED_DECIMAL, 
            10);
        pnd_Vars_Pnd_Mf_End_Dt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Mf_End_Dt", "#MF-END-DT", FieldType.STRING, 15);

        pnd_Vars__R_Field_10 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_10", "REDEFINE", pnd_Vars_Pnd_Mf_End_Dt);
        pnd_Vars_Pnd_Mf_End_Dt_N = pnd_Vars__R_Field_10.newFieldInGroup("pnd_Vars_Pnd_Mf_End_Dt_N", "#MF-END-DT-N", FieldType.NUMERIC, 15);
        pnd_Vars_Pnd_Mf_Next_End_Read_Dt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Mf_Next_End_Read_Dt", "#MF-NEXT-END-READ-DT", FieldType.STRING, 15);
        pnd_Vars_Pnd_Mf_Start_Dt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Mf_Start_Dt", "#MF-START-DT", FieldType.STRING, 15);

        pnd_Vars__R_Field_11 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_11", "REDEFINE", pnd_Vars_Pnd_Mf_Start_Dt);
        pnd_Vars_Pnd_Mf_Start_Dt_N = pnd_Vars__R_Field_11.newFieldInGroup("pnd_Vars_Pnd_Mf_Start_Dt_N", "#MF-START-DT-N", FieldType.NUMERIC, 15);
        pnd_Vars_Pnd_Lob_Found = pnd_Vars.newFieldArrayInGroup("pnd_Vars_Pnd_Lob_Found", "#LOB-FOUND", FieldType.BOOLEAN, 1, new DbsArrayController(1, 
            10));
        pnd_Vars_Pnd_Lob_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Lob_Ind", "#LOB-IND", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Mf_Wpid_Index = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Mf_Wpid_Index", "#MF-WPID-INDEX", FieldType.PACKED_DECIMAL, 3);
        pnd_Vars_Pnd_Mutual_Fund = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Mutual_Fund", "#MUTUAL-FUND", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Mutual_Fund_Account = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Mutual_Fund_Account", "#MUTUAL-FUND-ACCOUNT", FieldType.STRING, 10);
        pnd_Vars_Pnd_Nines = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Nines", "#NINES", FieldType.NUMERIC, 15);
        pnd_Vars_Pnd_Pass_Name = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Pass_Name", "#PASS-NAME", FieldType.STRING, 30);

        pnd_Vars__R_Field_12 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_12", "REDEFINE", pnd_Vars_Pnd_Pass_Name);
        pnd_Vars_Pnd_Pass_Name_S = pnd_Vars__R_Field_12.newFieldInGroup("pnd_Vars_Pnd_Pass_Name_S", "#PASS-NAME-S", FieldType.STRING, 20);
        pnd_Vars_Pnd_Pass_Ssn = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Pass_Ssn", "#PASS-SSN", FieldType.NUMERIC, 9);

        pnd_Vars__R_Field_13 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_13", "REDEFINE", pnd_Vars_Pnd_Pass_Ssn);
        pnd_Vars_Pnd_Pass_Ssn_A = pnd_Vars__R_Field_13.newFieldInGroup("pnd_Vars_Pnd_Pass_Ssn_A", "#PASS-SSN-A", FieldType.STRING, 9);
        pnd_Vars_Pnd_Pass_Tlc = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Pass_Tlc", "#PASS-TLC", FieldType.STRING, 7);
        pnd_Vars_Pnd_Pda_Key = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Pda_Key", "#PDA-KEY", FieldType.NUMERIC, 12);
        pnd_Vars_Pnd_Pin_Nbr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Vars_Pnd_Rqst_Log_Dte_Tme = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Vars_Pnd_Shrd_Srvce_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_Ind", "#SHRD-SRVCE-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_Shrd_Srvce_A_Add_Mf = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_A_Add_Mf", "#SHRD-SRVCE-A-ADD-MF", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Shrd_Srvce_A_Upd_Mf = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_A_Upd_Mf", "#SHRD-SRVCE-A-UPD-MF", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Shrd_Srvce_D_Add_Mf = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_D_Add_Mf", "#SHRD-SRVCE-D-ADD-MF", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Shrd_Srvce_D_Upd_Mf = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_D_Upd_Mf", "#SHRD-SRVCE-D-UPD-MF", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Shrd_Srvce_P_Add_Mf = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_P_Add_Mf", "#SHRD-SRVCE-P-ADD-MF", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Shrd_Srvce_P_Upd_Mf = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_P_Upd_Mf", "#SHRD-SRVCE-P-UPD-MF", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Shrd_Srvce_A_Add_Ts = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_A_Add_Ts", "#SHRD-SRVCE-A-ADD-TS", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Shrd_Srvce_A_Upd_Ts = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_A_Upd_Ts", "#SHRD-SRVCE-A-UPD-TS", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Shrd_Srvce_D_Add_Ts = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_D_Add_Ts", "#SHRD-SRVCE-D-ADD-TS", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Shrd_Srvce_D_Upd_Ts = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_D_Upd_Ts", "#SHRD-SRVCE-D-UPD-TS", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Shrd_Srvce_P_Add_Ts = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_P_Add_Ts", "#SHRD-SRVCE-P-ADD-TS", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Shrd_Srvce_P_Upd_Ts = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Shrd_Srvce_P_Upd_Ts", "#SHRD-SRVCE-P-UPD-TS", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Ssr_Check_Mf = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ssr_Check_Mf", "#SSR-CHECK-MF", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Ssr_Check_Ts = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ssr_Check_Ts", "#SSR-CHECK-TS", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Ssr_Doc_Array = pnd_Vars.newFieldArrayInGroup("pnd_Vars_Pnd_Ssr_Doc_Array", "#SSR-DOC-ARRAY", FieldType.STRING, 8, new DbsArrayController(1, 
            10));
        pnd_Vars_Pnd_Ssr_Existed_Mf = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ssr_Existed_Mf", "#SSR-EXISTED-MF", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Ssr_Existed_Ts = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ssr_Existed_Ts", "#SSR-EXISTED-TS", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Ssr_Ind_Array = pnd_Vars.newFieldArrayInGroup("pnd_Vars_Pnd_Ssr_Ind_Array", "#SSR-IND-ARRAY", FieldType.STRING, 1, new DbsArrayController(1, 
            100));
        pnd_Vars_Pnd_Ssr_Wpid = pnd_Vars.newFieldArrayInGroup("pnd_Vars_Pnd_Ssr_Wpid", "#SSR-WPID", FieldType.STRING, 6, new DbsArrayController(1, 10));

        pnd_Vars__R_Field_14 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_14", "REDEFINE", pnd_Vars_Pnd_Ssr_Wpid);
        pnd_Vars_Pnd_Ssr_Wpid_Act = pnd_Vars__R_Field_14.newFieldInGroup("pnd_Vars_Pnd_Ssr_Wpid_Act", "#SSR-WPID-ACT", FieldType.STRING, 1);
        pnd_Vars_Pnd_Ssr_Wpid_Lob = pnd_Vars__R_Field_14.newFieldInGroup("pnd_Vars_Pnd_Ssr_Wpid_Lob", "#SSR-WPID-LOB", FieldType.STRING, 2);
        pnd_Vars_Pnd_Store_Msg = pnd_Vars.newFieldArrayInGroup("pnd_Vars_Pnd_Store_Msg", "#STORE-MSG", FieldType.STRING, 70, new DbsArrayController(1, 
            2));
        pnd_Vars_Pnd_Tbl_Data_Field = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Tbl_Data_Field", "#TBL-DATA-FIELD", FieldType.STRING, 15);

        pnd_Vars__R_Field_15 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_15", "REDEFINE", pnd_Vars_Pnd_Tbl_Data_Field);
        pnd_Vars_Pnd_Tbl_Data_Field_N = pnd_Vars__R_Field_15.newFieldInGroup("pnd_Vars_Pnd_Tbl_Data_Field_N", "#TBL-DATA-FIELD-N", FieldType.NUMERIC, 
            15);
        pnd_Vars_Pnd_Tbl_Prime_Key = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Vars__R_Field_16 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_16", "REDEFINE", pnd_Vars_Pnd_Tbl_Prime_Key);
        pnd_Vars_Pnd_Tbl_Scrty_Level_Ind = pnd_Vars__R_Field_16.newFieldInGroup("pnd_Vars_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Vars_Pnd_Tbl_Table_Nme = pnd_Vars__R_Field_16.newFieldInGroup("pnd_Vars_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Vars_Pnd_Tbl_Key_Field = pnd_Vars__R_Field_16.newFieldInGroup("pnd_Vars_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Vars_Pnd_Tiaa_Rcvd_Dte = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Vars_Pnd_Timx = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Vars_Pnd_Trust_Service = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Trust_Service", "#TRUST-SERVICE", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Trust_Service_Account = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Trust_Service_Account", "#TRUST-SERVICE-ACCOUNT", FieldType.STRING, 
            10);
        pnd_Vars_Pnd_Wpid_Index = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Wpid_Index", "#WPID-INDEX", FieldType.PACKED_DECIMAL, 3);
        pnd_Vars_Pnd_Work_Prcss_Id = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Work_Prcss_Id", "#WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Vars_Pnd_Work_Prcss_Short_Nme = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Work_Prcss_Short_Nme", "#WORK-PRCSS-SHORT-NME", FieldType.STRING, 15);
        pnd_Vars_Pnd_Work_Prcss_Short_Nme_Array = pnd_Vars.newFieldArrayInGroup("pnd_Vars_Pnd_Work_Prcss_Short_Nme_Array", "#WORK-PRCSS-SHORT-NME-ARRAY", 
            FieldType.STRING, 15, new DbsArrayController(1, 100));
        pnd_Vars_Pnd_Wpid_Array = pnd_Vars.newFieldArrayInGroup("pnd_Vars_Pnd_Wpid_Array", "#WPID-ARRAY", FieldType.STRING, 6, new DbsArrayController(1, 
            100));
        pnd_Vars_Pnd_Pin_Wpid_Key = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Pin_Wpid_Key", "#PIN-WPID-KEY", FieldType.STRING, 20);

        pnd_Vars__R_Field_17 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_17", "REDEFINE", pnd_Vars_Pnd_Pin_Wpid_Key);
        pnd_Vars_Pnd_Pin_Wpid_Key_Pin = pnd_Vars__R_Field_17.newFieldInGroup("pnd_Vars_Pnd_Pin_Wpid_Key_Pin", "#PIN-WPID-KEY-PIN", FieldType.STRING, 12);
        pnd_Vars_Pnd_Pin_Wpid_Key_Status = pnd_Vars__R_Field_17.newFieldInGroup("pnd_Vars_Pnd_Pin_Wpid_Key_Status", "#PIN-WPID-KEY-STATUS", FieldType.STRING, 
            1);
        pnd_Vars_Pnd_Pin_Wpid_Key_Wpid = pnd_Vars__R_Field_17.newFieldInGroup("pnd_Vars_Pnd_Pin_Wpid_Key_Wpid", "#PIN-WPID-KEY-WPID", FieldType.STRING, 
            6);
        pnd_Vars_Pnd_Sweep_Reject_Id = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Sweep_Reject_Id", "#SWEEP-REJECT-ID", FieldType.STRING, 40);

        pnd_Vars__R_Field_18 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_18", "REDEFINE", pnd_Vars_Pnd_Sweep_Reject_Id);
        pnd_Vars_Pnd_Sweep_Reject_Id_Array = pnd_Vars__R_Field_18.newFieldArrayInGroup("pnd_Vars_Pnd_Sweep_Reject_Id_Array", "#SWEEP-REJECT-ID-ARRAY", 
            FieldType.STRING, 8, new DbsArrayController(1, 5));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Doc.reset();
        vw_cwf_Mit_Sub.reset();
        vw_cwf_H1.reset();
        vw_cwf_Suptb.reset();
        vw_cwf_Wpid.reset();

        ldaCwfl6800.initializeValues();

        localVariables.reset();
        pnd_Vars_Pnd_Cwf_Doc_Found.setInitialValue(false);
        pnd_Vars_Pnd_Cwf_Ssr_Found.setInitialValue(false);
        pnd_Vars_Pnd_Lob_Found.getValue(1).setInitialValue(false);
        pnd_Vars_Pnd_Lob_Ind.setInitialValue(false);
        pnd_Vars_Pnd_Mutual_Fund.setInitialValue(false);
        pnd_Vars_Pnd_Nines.setInitialValue(-2147483648);
        pnd_Vars_Pnd_Ssr_Check_Mf.setInitialValue(true);
        pnd_Vars_Pnd_Ssr_Check_Ts.setInitialValue(true);
        pnd_Vars_Pnd_Ssr_Existed_Mf.setInitialValue(false);
        pnd_Vars_Pnd_Ssr_Existed_Ts.setInitialValue(false);
        pnd_Vars_Pnd_Trust_Service.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb6750() throws Exception
    {
        super("Cwfb6750");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB6750", onError);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  OPEN MQ TO FACILITATE MDM CALL
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 60 LS = 132 SG = OFF;//Natural: FORMAT ( 1 ) PS = 60 LS = 132 SG = OFF;//Natural: FORMAT ( 2 ) PS = 60 LS = 132 SG = OFF
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        pnd_Vars_Pnd_Tbl_Scrty_Level_Ind.setValue("S ");                                                                                                                  //Natural: MOVE 'S ' TO #TBL-SCRTY-LEVEL-IND
        pnd_Vars_Pnd_Tbl_Table_Nme.setValue("CWF-MIT-SWEEP");                                                                                                             //Natural: MOVE 'CWF-MIT-SWEEP' TO #TBL-TABLE-NME
        pnd_Vars_Pnd_Tbl_Key_Field.setValue("LAST-RECORD-READ-SWEEP2");                                                                                                   //Natural: MOVE 'LAST-RECORD-READ-SWEEP2' TO #TBL-KEY-FIELD
        vw_cwf_Suptb.startDatabaseRead                                                                                                                                    //Natural: READ ( 1 ) CWF-SUPTB WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Vars_Pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cwf_Suptb.readNextRow("READ01")))
        {
            if (condition(cwf_Suptb_Tbl_Scrty_Level_Ind.notEquals(pnd_Vars_Pnd_Tbl_Scrty_Level_Ind) || cwf_Suptb_Tbl_Table_Nme.notEquals(pnd_Vars_Pnd_Tbl_Table_Nme)      //Natural: IF TBL-SCRTY-LEVEL-IND NE #TBL-SCRTY-LEVEL-IND OR TBL-TABLE-NME NE #TBL-TABLE-NME OR TBL-KEY-FIELD NE #TBL-KEY-FIELD
                || cwf_Suptb_Tbl_Key_Field.notEquals(pnd_Vars_Pnd_Tbl_Key_Field)))
            {
                pnd_Vars_Pnd_Error_Ind.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #ERROR-IND
                pnd_Vars_Pnd_Store_Msg.getValue(1).setValue("There is no Mutual Fund Control Record");                                                                    //Natural: MOVE 'There is no Mutual Fund Control Record' TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                sub_Log_Msg();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Lob_Ind.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #LOB-IND
            pnd_Vars_Pnd_Tbl_Data_Field.setValue(cwf_Suptb_Tbl_Data_Field);                                                                                               //Natural: MOVE TBL-DATA-FIELD TO #TBL-DATA-FIELD #MF-END-DT
            pnd_Vars_Pnd_Mf_End_Dt.setValue(cwf_Suptb_Tbl_Data_Field);
            pnd_Vars_Pnd_Mf_Control_Rec_Isn.setValue(vw_cwf_Suptb.getAstISN("Read01"));                                                                                   //Natural: MOVE *ISN TO #MF-CONTROL-REC-ISN
            vw_cwf_Wpid.startDatabaseRead                                                                                                                                 //Natural: READ CWF-WPID BY SSR-WPID-KEY
            (
            "READ02",
            new Oc[] { new Oc("SSR_WPID_KEY", "ASC") }
            );
            READ02:
            while (condition(vw_cwf_Wpid.readNextRow("READ02")))
            {
                if (condition(!(cwf_Wpid_Dlte_Oprtr_Cde.equals(" "))))                                                                                                    //Natural: ACCEPT IF DLTE-OPRTR-CDE = ' '
                {
                    continue;
                }
                pnd_Vars_Pnd_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CTR
                if (condition(pnd_Vars_Pnd_Ctr.greater(100)))                                                                                                             //Natural: IF #CTR > 100
                {
                    pnd_Vars_Pnd_Error_Ind.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #ERROR-IND
                    getReports().write(0, "ERROR - More than 50 wpid for Mutual Funds");                                                                                  //Natural: WRITE 'ERROR - More than 50 wpid for Mutual Funds'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Vars_Pnd_Store_Msg.getValue(1).setValue("ERROR - More than 50 wpid for Mutual Funds");                                                            //Natural: MOVE 'ERROR - More than 50 wpid for Mutual Funds' TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                    sub_Log_Msg();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Vars_Pnd_Wpid_Array.getValue(pnd_Vars_Pnd_Ctr).setValue(cwf_Wpid_Work_Prcss_Id);                                                                      //Natural: MOVE CWF-WPID.WORK-PRCSS-ID TO #WPID-ARRAY ( #CTR )
                pnd_Vars_Pnd_Ssr_Ind_Array.getValue(pnd_Vars_Pnd_Ctr).setValue(cwf_Wpid_Shrd_Srvce_Ind);                                                                  //Natural: MOVE CWF-WPID.SHRD-SRVCE-IND TO #SSR-IND-ARRAY ( #CTR )
                pnd_Vars_Pnd_Work_Prcss_Short_Nme_Array.getValue(pnd_Vars_Pnd_Ctr).setValue(cwf_Wpid_Work_Prcss_Short_Nme);                                               //Natural: MOVE CWF-WPID.WORK-PRCSS-SHORT-NME TO #WORK-PRCSS-SHORT-NME-ARRAY ( #CTR )
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  THE ERR OF NO MF CONTROL REC. OR MORE THE 100 WPIDS
        if (condition(pnd_Vars_Pnd_Error_Ind.getBoolean()))                                                                                                               //Natural: IF #ERROR-IND
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Vars_Pnd_Wpid_Array.getValue(1).equals(" ")))                                                                                                   //Natural: IF #WPID-ARRAY ( 1 ) = ' '
        {
            getReports().write(0, "There is no Share Service WPIDs");                                                                                                     //Natural: WRITE 'There is no Share Service WPIDs'
            if (Global.isEscape()) return;
            pnd_Vars_Pnd_Store_Msg.getValue(1).setValue("There is no Share Service WPIDs");                                                                               //Natural: MOVE 'There is no Share Service WPIDs' TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
            sub_Log_Msg();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Vars_Pnd_Tbl_Prime_Key.reset();                                                                                                                               //Natural: RESET #TBL-PRIME-KEY #CTR
        pnd_Vars_Pnd_Ctr.reset();
        pnd_Vars_Pnd_Tbl_Scrty_Level_Ind.setValue("S ");                                                                                                                  //Natural: MOVE 'S ' TO #TBL-SCRTY-LEVEL-IND
        pnd_Vars_Pnd_Tbl_Table_Nme.setValue("CWF-SSR-DOC-TYPES");                                                                                                         //Natural: MOVE 'CWF-SSR-DOC-TYPES' TO #TBL-TABLE-NME
        vw_cwf_Suptb.startDatabaseRead                                                                                                                                    //Natural: READ CWF-SUPTB WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "READ03",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Vars_Pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_cwf_Suptb.readNextRow("READ03")))
        {
            if (condition(cwf_Suptb_Tbl_Scrty_Level_Ind.notEquals(pnd_Vars_Pnd_Tbl_Scrty_Level_Ind) || cwf_Suptb_Tbl_Table_Nme.notEquals(pnd_Vars_Pnd_Tbl_Table_Nme)))    //Natural: IF TBL-SCRTY-LEVEL-IND NE #TBL-SCRTY-LEVEL-IND OR TBL-TABLE-NME NE #TBL-TABLE-NME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CTR
            if (condition(pnd_Vars_Pnd_Ctr.greater(10)))                                                                                                                  //Natural: IF #CTR > 10
            {
                getReports().write(0, "ERROR - More than 10 Share Service Documents");                                                                                    //Natural: WRITE 'ERROR - More than 10 Share Service Documents'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Vars_Pnd_Store_Msg.getValue(1).setValue(" ERROR - More than 10 Share Service Documents");                                                             //Natural: MOVE ' ERROR - More than 10 Share Service Documents' TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                sub_Log_Msg();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Ssr_Doc_Array.getValue(pnd_Vars_Pnd_Ctr).setValue(cwf_Suptb_Tbl_Data_Field);                                                                     //Natural: MOVE TBL-DATA-FIELD TO #SSR-DOC-ARRAY ( #CTR )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Vars_Pnd_Ssr_Doc_Array.getValue(1).equals(" ")))                                                                                                //Natural: IF #SSR-DOC-ARRAY ( 1 ) EQ ' '
        {
            getReports().write(0, "There is no Share Service Document Control Record");                                                                                   //Natural: WRITE 'There is no Share Service Document Control Record'
            if (Global.isEscape()) return;
            pnd_Vars_Pnd_Store_Msg.getValue(1).setValue(" There is no Share Service Document Control Record");                                                            //Natural: MOVE ' There is no Share Service Document Control Record' TO #STORE-MSG ( 1 )
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
            sub_Log_Msg();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROCESS THE SSR FOR MUTUAL FUNDS
        pnd_Vars_Pnd_Tbl_Scrty_Level_Ind.setValue("S ");                                                                                                                  //Natural: MOVE 'S ' TO #TBL-SCRTY-LEVEL-IND
        pnd_Vars_Pnd_Tbl_Table_Nme.setValue("CWF-MIT-SWEEP");                                                                                                             //Natural: MOVE 'CWF-MIT-SWEEP' TO #TBL-TABLE-NME
        pnd_Vars_Pnd_Tbl_Key_Field.setValue("SWEEP-REJECT-ID");                                                                                                           //Natural: MOVE 'SWEEP-REJECT-ID' TO #TBL-KEY-FIELD
        vw_cwf_Suptb.startDatabaseRead                                                                                                                                    //Natural: READ ( 1 ) CWF-SUPTB WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "READ04",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Vars_Pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ04:
        while (condition(vw_cwf_Suptb.readNextRow("READ04")))
        {
            if (condition(cwf_Suptb_Tbl_Scrty_Level_Ind.notEquals(pnd_Vars_Pnd_Tbl_Scrty_Level_Ind) || cwf_Suptb_Tbl_Table_Nme.notEquals(pnd_Vars_Pnd_Tbl_Table_Nme)      //Natural: IF TBL-SCRTY-LEVEL-IND NE #TBL-SCRTY-LEVEL-IND OR TBL-TABLE-NME NE #TBL-TABLE-NME OR TBL-KEY-FIELD NE #TBL-KEY-FIELD
                || cwf_Suptb_Tbl_Key_Field.notEquals(pnd_Vars_Pnd_Tbl_Key_Field)))
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Sweep_Reject_Id.setValue(cwf_Suptb_Tbl_Data_Field);                                                                                              //Natural: MOVE TBL-DATA-FIELD TO #SWEEP-REJECT-ID
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Vars_Pnd_Lob_Ind.getBoolean()))                                                                                                                 //Natural: IF #LOB-IND
        {
            pnd_Vars_Pnd_Timx.compute(new ComputeParameters(false, pnd_Vars_Pnd_Timx), Global.getTIMX().subtract(6000));                                                  //Natural: ASSIGN #TIMX := *TIMX - 6000
            pnd_Vars_Pnd_Mf_Start_Dt.setValueEdited(pnd_Vars_Pnd_Timx,new ReportEditMask("YYYYMMDDHHIISST"));                                                             //Natural: MOVE EDITED #TIMX ( EM = YYYYMMDDHHIISST ) TO #MF-START-DT
            pnd_Vars_Pnd_Mf_Next_End_Read_Dt.setValueEdited(pnd_Vars_Pnd_Timx,new ReportEditMask("YYYYMMDDHHIISST"));                                                     //Natural: MOVE EDITED #TIMX ( EM = YYYYMMDDHHIISST ) TO #MF-NEXT-END-READ-DT
            pnd_Vars_Pnd_Mf_Start_Dt_N.compute(new ComputeParameters(false, pnd_Vars_Pnd_Mf_Start_Dt_N), pnd_Vars_Pnd_Nines.subtract(pnd_Vars_Pnd_Mf_Start_Dt_N));        //Natural: ASSIGN #MF-START-DT-N := #NINES - #MF-START-DT-N
            pnd_Vars_Pnd_Last_Chnge_Invrt_Dte_Tme.setValue(pnd_Vars_Pnd_Mf_Start_Dt_N);                                                                                   //Natural: MOVE #MF-START-DT-N TO #LAST-CHNGE-INVRT-DTE-TME
            pnd_Vars_Pnd_Crprte_Status_Ind.setValue("0");                                                                                                                 //Natural: MOVE '0' TO #CRPRTE-STATUS-IND
                                                                                                                                                                          //Natural: PERFORM MAIN-PROCESSING
            sub_Main_Processing();
            if (condition(Global.isEscape())) {return;}
            pnd_Vars_Pnd_Crprte_Status_Ind.setValue("9");                                                                                                                 //Natural: MOVE '9' TO #CRPRTE-STATUS-IND
                                                                                                                                                                          //Natural: PERFORM MAIN-PROCESSING
            sub_Main_Processing();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-RECORD
            sub_Update_Control_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY
        sub_Write_Summary();
        if (condition(Global.isEscape())) {return;}
        //*  CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MAIN-PROCESSING
        //* *********************************
        //*   GH COMMENT OUT THE FOLLOWING LINE FOR POA REQESTS  11/20/2010  **
        //* ***      AND CWF-MIT.MSTR-INDX-ACTN-CDE NE '10'
        //* *     ********  END CHANGE ****************************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SSR-WPID
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY
        //* ******************************
        //* *************************** *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL-RECORD
        //* *************************** *
        //* *************************** *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOG-MSG
        //*  ******************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Main_Processing() throws Exception                                                                                                                   //Natural: MAIN-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        ldaCwfl6800.getVw_cwf_Mit().startDatabaseRead                                                                                                                     //Natural: READ CWF-MIT WITH CRPRTE-STATUS-CHNGE-DTE-KEY STARTING FROM #CRPRTE-STATUS-CHNGE-DTE-KEY
        (
        "RMIT1",
        new Wc[] { new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", ">=", pnd_Vars_Pnd_Crprte_Status_Chnge_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
        );
        RMIT1:
        while (condition(ldaCwfl6800.getVw_cwf_Mit().readNextRow("RMIT1")))
        {
            if (condition(ldaCwfl6800.getCwf_Mit_Crprte_Status_Ind().notEquals(pnd_Vars_Pnd_Crprte_Status_Ind) || ldaCwfl6800.getCwf_Mit_Last_Chnge_Dte_Tme().less(pnd_Vars_Pnd_Mf_End_Dt_N))) //Natural: IF CRPRTE-STATUS-IND NE #CRPRTE-STATUS-IND OR LAST-CHNGE-DTE-TME < #MF-END-DT-N
            {
                pnd_Vars_Pnd_Tbl_Data_Field.setValue(pnd_Vars_Pnd_Mf_Next_End_Read_Dt);                                                                                   //Natural: MOVE #MF-NEXT-END-READ-DT TO #TBL-DATA-FIELD
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Counter_Mit_Record_All.nadd(1);                                                                                                                  //Natural: ASSIGN #COUNTER-MIT-RECORD-ALL := #COUNTER-MIT-RECORD-ALL + 1
            if (condition(((((((((ldaCwfl6800.getCwf_Mit_Admin_Status_Cde().equals("8100") || ldaCwfl6800.getCwf_Mit_Admin_Status_Cde().equals("8775"))                   //Natural: REJECT IF ( ( CWF-MIT.ADMIN-STATUS-CDE = '8100' OR = '8775' ) OR ( CWF-MIT.STATUS-CDE = '8100' OR = '8775' ) ) AND ( CWF-MIT.UNIT-CDE = 'CRC' OR CWF-MIT.ADMIN-UNIT-CDE = 'CRC' ) OR STATUS-UPDTE-OPRTR-CDE = #SWEEP-REJECT-ID-ARRAY ( 1 ) OR STATUS-UPDTE-OPRTR-CDE = #SWEEP-REJECT-ID-ARRAY ( 2 ) OR STATUS-UPDTE-OPRTR-CDE = #SWEEP-REJECT-ID-ARRAY ( 3 ) OR STATUS-UPDTE-OPRTR-CDE = #SWEEP-REJECT-ID-ARRAY ( 4 ) OR STATUS-UPDTE-OPRTR-CDE = #SWEEP-REJECT-ID-ARRAY ( 5 )
                || (ldaCwfl6800.getCwf_Mit_Status_Cde().equals("8100") || ldaCwfl6800.getCwf_Mit_Status_Cde().equals("8775"))) && (ldaCwfl6800.getCwf_Mit_Unit_Cde().equals("CRC") 
                || ldaCwfl6800.getCwf_Mit_Admin_Unit_Cde().equals("CRC"))) || ldaCwfl6800.getCwf_Mit_Status_Updte_Oprtr_Cde().equals(pnd_Vars_Pnd_Sweep_Reject_Id_Array.getValue(1))) 
                || ldaCwfl6800.getCwf_Mit_Status_Updte_Oprtr_Cde().equals(pnd_Vars_Pnd_Sweep_Reject_Id_Array.getValue(2))) || ldaCwfl6800.getCwf_Mit_Status_Updte_Oprtr_Cde().equals(pnd_Vars_Pnd_Sweep_Reject_Id_Array.getValue(3))) 
                || ldaCwfl6800.getCwf_Mit_Status_Updte_Oprtr_Cde().equals(pnd_Vars_Pnd_Sweep_Reject_Id_Array.getValue(4))) || ldaCwfl6800.getCwf_Mit_Status_Updte_Oprtr_Cde().equals(pnd_Vars_Pnd_Sweep_Reject_Id_Array.getValue(5)))))
            {
                continue;
            }
            pnd_Vars_Pnd_Counter_Mit_Record.nadd(1);                                                                                                                      //Natural: ASSIGN #COUNTER-MIT-RECORD := #COUNTER-MIT-RECORD + 1
            pnd_Vars_Pnd_Counter_Et.nadd(1);                                                                                                                              //Natural: ASSIGN #COUNTER-ET := #COUNTER-ET + 1
            if (condition(pnd_Vars_Pnd_Counter_Et.equals(20)))                                                                                                            //Natural: IF #COUNTER-ET = 20
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-RECORD
                sub_Update_Control_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RMIT1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RMIT1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Vars_Pnd_Counter_Et.reset();                                                                                                                          //Natural: RESET #COUNTER-ET
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Mutual_Fund.reset();                                                                                                                             //Natural: RESET #MUTUAL-FUND #SSR-EXISTED-MF #TRUST-SERVICE #SSR-EXISTED-TS #MUTUAL-FUND-ACCOUNT #TRUST-SERVICE-ACCOUNT
            pnd_Vars_Pnd_Ssr_Existed_Mf.reset();
            pnd_Vars_Pnd_Trust_Service.reset();
            pnd_Vars_Pnd_Ssr_Existed_Ts.reset();
            pnd_Vars_Pnd_Mutual_Fund_Account.reset();
            pnd_Vars_Pnd_Trust_Service_Account.reset();
            pnd_Vars_Pnd_Work_Prcss_Id.setValue(ldaCwfl6800.getCwf_Mit_Work_Prcss_Id());                                                                                  //Natural: MOVE WORK-PRCSS-ID TO #WORK-PRCSS-ID
            pnd_Vars_Pnd_Pin_Nbr.setValue(ldaCwfl6800.getCwf_Mit_Pin_Nbr());                                                                                              //Natural: MOVE PIN-NBR TO #PIN-NBR
            DbsUtil.callnat(Cwfn0075.class , getCurrentProcessState(), msg_Info, pnd_Vars_Pnd_Pin_Nbr, pnd_Vars_Pnd_Lob_Found.getValue("*"), pnd_Vars_Pnd_Cntrct_Nbr.getValue("*"),  //Natural: CALLNAT 'CWFN0075' MSG-INFO #PIN-NBR #LOB-FOUND ( * ) #CNTRCT-NBR ( * ) CWF-MIT.TIAA-RCVD-DTE
                ldaCwfl6800.getCwf_Mit_Tiaa_Rcvd_Dte());
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Vars_Pnd_Lob_Found.getValue(1).getBoolean()))                                                                                               //Natural: IF #LOB-FOUND ( 1 )
            {
                pnd_Vars_Pnd_Mutual_Fund.setValue(true);                                                                                                                  //Natural: MOVE TRUE TO #MUTUAL-FUND
                pnd_Vars_Pnd_Mutual_Fund_Account.setValue(pnd_Vars_Pnd_Cntrct_Nbr.getValue(1));                                                                           //Natural: MOVE #CNTRCT-NBR ( 1 ) TO #MUTUAL-FUND-ACCOUNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Vars_Pnd_Lob_Found.getValue(2).getBoolean()))                                                                                               //Natural: IF #LOB-FOUND ( 2 )
            {
                pnd_Vars_Pnd_Trust_Service.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #TRUST-SERVICE
                pnd_Vars_Pnd_Trust_Service_Account.setValue(pnd_Vars_Pnd_Cntrct_Nbr.getValue(2));                                                                         //Natural: MOVE #CNTRCT-NBR ( 2 ) TO #TRUST-SERVICE-ACCOUNT
            }                                                                                                                                                             //Natural: END-IF
            //*  MAIN CRITERIA FOR MUTUAL FUND AND TRUST SERVICES
            DbsUtil.examine(new ExamineSource(pnd_Vars_Pnd_Wpid_Array.getValue("*"),true), new ExamineSearch(ldaCwfl6800.getCwf_Mit_Work_Prcss_Id(), true),               //Natural: EXAMINE FULL #WPID-ARRAY ( * ) FOR FULL CWF-MIT.WORK-PRCSS-ID GIVING INDEX #WPID-INDEX
                new ExamineGivingIndex(pnd_Vars_Pnd_Wpid_Index));
            if (condition(!(pnd_Vars_Pnd_Wpid_Index.greater(getZero()))))                                                                                                 //Natural: ACCEPT IF #WPID-INDEX > 0
            {
                continue;
            }
            pnd_Vars_Pnd_Shrd_Srvce_Ind.setValue(pnd_Vars_Pnd_Ssr_Ind_Array.getValue(pnd_Vars_Pnd_Wpid_Index));                                                           //Natural: MOVE #SSR-IND-ARRAY ( #WPID-INDEX ) TO #SHRD-SRVCE-IND
            pnd_Vars_Pnd_Work_Prcss_Short_Nme.setValue(pnd_Vars_Pnd_Work_Prcss_Short_Nme_Array.getValue(pnd_Vars_Pnd_Wpid_Index));                                        //Natural: MOVE #WORK-PRCSS-SHORT-NME-ARRAY ( #WPID-INDEX ) TO #WORK-PRCSS-SHORT-NME
            if (condition(pnd_Vars_Pnd_Mutual_Fund.getBoolean()))                                                                                                         //Natural: IF #MUTUAL-FUND
            {
                if (condition(((((((ldaCwfl6800.getCwf_Mit_Bypss_Mutual_Fund_Ind().equals(" ") || ldaCwfl6800.getCwf_Mit_Bypss_Mutual_Fund_Ind().equals("N"))             //Natural: IF ( ( ( CWF-MIT.BYPSS-MUTUAL-FUND-IND = ' ' OR = 'N' ) AND #SHRD-SRVCE-IND = 'A' ) OR #SHRD-SRVCE-IND NE 'A' ) AND CWF-MIT.ORGNL-UNIT-CDE NE 'MFTA' AND CWF-MIT.RQST-ORGN-CDE NE 'T' AND CWF-MIT.RESCAN-IND NE 'Y'
                    && pnd_Vars_Pnd_Shrd_Srvce_Ind.equals("A")) || pnd_Vars_Pnd_Shrd_Srvce_Ind.notEquals("A")) && ldaCwfl6800.getCwf_Mit_Orgnl_Unit_Cde().notEquals("MFTA")) 
                    && ldaCwfl6800.getCwf_Mit_Rqst_Orgn_Cde().notEquals("T")) && ldaCwfl6800.getCwf_Mit_Rescan_Ind().notEquals("Y"))))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Vars_Pnd_Mutual_Fund.setValue(false);                                                                                                             //Natural: MOVE FALSE TO #MUTUAL-FUND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Vars_Pnd_Trust_Service.getBoolean()))                                                                                                       //Natural: IF #TRUST-SERVICE
            {
                if (condition(ldaCwfl6800.getCwf_Mit_Orgnl_Unit_Cde().notEquals("TRUST") && ldaCwfl6800.getCwf_Mit_Rqst_Orgn_Cde().notEquals("L") && ldaCwfl6800.getCwf_Mit_Mstr_Indx_Actn_Cde().notEquals("10")  //Natural: IF CWF-MIT.ORGNL-UNIT-CDE NE 'TRUST' AND CWF-MIT.RQST-ORGN-CDE NE 'L' AND CWF-MIT.MSTR-INDX-ACTN-CDE NE '10' AND CWF-MIT.RESCAN-IND NE 'Y'
                    && ldaCwfl6800.getCwf_Mit_Rescan_Ind().notEquals("Y")))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Vars_Pnd_Trust_Service.setValue(false);                                                                                                           //Natural: MOVE FALSE TO #TRUST-SERVICE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Vars_Pnd_Mutual_Fund.getBoolean() || pnd_Vars_Pnd_Trust_Service.getBoolean())))                                                          //Natural: IF NOT ( #MUTUAL-FUND OR #TRUST-SERVICE )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Pda_Key.setValue(ldaCwfl6800.getCwf_Mit_Pin_Nbr());                                                                                              //Natural: MOVE CWF-MIT.PIN-NBR TO #PDA-KEY
            DbsUtil.callnat(Cwfn5371.class , getCurrentProcessState(), pnd_Vars_Pnd_Pda_Key, pnd_Vars_Pnd_Pass_Name, pnd_Vars_Pnd_Pass_Ssn, pnd_Vars_Pnd_Pass_Tlc);       //Natural: CALLNAT 'CWFN5371' #PDA-KEY #PASS-NAME #PASS-SSN #PASS-TLC
            if (condition(Global.isEscape())) return;
            if (condition(! (ldaCwfl6800.getCwf_Mit_Elctrnc_Fldr_Ind().equals("I") || ldaCwfl6800.getCwf_Mit_Elctrnc_Fldr_Ind().equals("F") || ldaCwfl6800.getCwf_Mit_Elctrnc_Fldr_Ind().equals("C")  //Natural: IF NOT ( ELCTRNC-FLDR-IND = 'I' OR = 'F' OR = 'C' OR = 'Y' )
                || ldaCwfl6800.getCwf_Mit_Elctrnc_Fldr_Ind().equals("Y"))))
            {
                pnd_Vars_Pnd_Counter.nadd(1);                                                                                                                             //Natural: ASSIGN #COUNTER := #COUNTER + 1
                getReports().write(2, pnd_Vars_Pnd_Counter,ldaCwfl6800.getCwf_Mit_Tiaa_Rcvd_Dte(),new ColumnSpacing(2),ldaCwfl6800.getCwf_Mit_Work_Prcss_Id(),new         //Natural: WRITE ( 2 ) #COUNTER TIAA-RCVD-DTE 2X WORK-PRCSS-ID 2X #WORK-PRCSS-SHORT-NME PIN-NBR 2X #MUTUAL-FUND-ACCOUNT 2X #PASS-NAME-S 2X ORGNL-UNIT-CDE 2X RQST-ORGN-CDE
                    ColumnSpacing(2),pnd_Vars_Pnd_Work_Prcss_Short_Nme,ldaCwfl6800.getCwf_Mit_Pin_Nbr(),new ColumnSpacing(2),pnd_Vars_Pnd_Mutual_Fund_Account,new 
                    ColumnSpacing(2),pnd_Vars_Pnd_Pass_Name_S,new ColumnSpacing(2),ldaCwfl6800.getCwf_Mit_Orgnl_Unit_Cde(),new ColumnSpacing(2),ldaCwfl6800.getCwf_Mit_Rqst_Orgn_Cde());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RMIT1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RMIT1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-SSR-WPID
            sub_Get_Ssr_Wpid();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RMIT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RMIT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   DOCUMENT BASED SSR ---> 'D'
            pnd_Vars_Pnd_Ssr_Check_Mf.resetInitial();                                                                                                                     //Natural: RESET INITIAL #SSR-CHECK-MF
            pnd_Vars_Pnd_Ssr_Check_Ts.resetInitial();                                                                                                                     //Natural: RESET INITIAL #SSR-CHECK-TS
            if (condition(pnd_Vars_Pnd_Shrd_Srvce_Ind.equals("D")))                                                                                                       //Natural: IF #SHRD-SRVCE-IND = 'D'
            {
                //*  IF MUTUAL FUND ACCOUNT
                if (condition(pnd_Vars_Pnd_Mutual_Fund.getBoolean()))                                                                                                     //Natural: IF #MUTUAL-FUND
                {
                    pnd_Vars_Pnd_Doc_Cabinet_Type.setValue("P");                                                                                                          //Natural: MOVE 'P' TO #DOC-CABINET-TYPE
                    //*        MOVE PIN-NBR                 TO #DOC-CABINET-NAME     /* PIN-EXP
                    //*  PIN-EXP
                    pnd_Vars_Pnd_Doc_Cabinet_Name_A12.moveAll(ldaCwfl6800.getCwf_Mit_Pin_Nbr());                                                                          //Natural: MOVE ALL PIN-NBR TO #DOC-CABINET-NAME-A12
                    pnd_Vars_Pnd_Doc_Tiaa_Rcvd_Dte.setValue(ldaCwfl6800.getCwf_Mit_Tiaa_Rcvd_Dte());                                                                      //Natural: MOVE TIAA-RCVD-DTE TO #DOC-TIAA-RCVD-DTE
                    pnd_Vars_Pnd_Doc_Case_Ind.setValue(ldaCwfl6800.getCwf_Mit_Case_Ind());                                                                                //Natural: MOVE CASE-IND TO #DOC-CASE-IND
                    pnd_Vars_Pnd_Doc_Originating_Work_Prcss_Id.setValue(ldaCwfl6800.getCwf_Mit_Rqst_Work_Prcss_Id());                                                     //Natural: MOVE RQST-WORK-PRCSS-ID TO #DOC-ORIGINATING-WORK-PRCSS-ID
                    pnd_Vars_Pnd_Doc_Multi_Rqst_Ind.setValue(ldaCwfl6800.getCwf_Mit_Multi_Rqst_Ind());                                                                    //Natural: MOVE MULTI-RQST-IND TO #DOC-MULTI-RQST-IND
                    pnd_Vars_Pnd_Doc_Sub_Rqst_Ind.setValue(ldaCwfl6800.getCwf_Mit_Sub_Rqst_Sqnce_Ind());                                                                  //Natural: MOVE SUB-RQST-SQNCE-IND TO #DOC-SUB-RQST-IND
                    pnd_Vars_Pnd_Cwf_Doc_Found.reset();                                                                                                                   //Natural: RESET #CWF-DOC-FOUND #DOC-ENTRY-DTE-TME
                    pnd_Vars_Pnd_Doc_Entry_Dte_Tme.reset();
                    vw_cwf_Doc.startDatabaseRead                                                                                                                          //Natural: READ CWF-DOC WITH DOCUMENT-KEY = #DOCUMENT-KEY
                    (
                    "READ05",
                    new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Vars_Pnd_Document_Key.getBinary(), WcType.BY) },
                    new Oc[] { new Oc("DOCUMENT_KEY", "ASC") }
                    );
                    READ05:
                    while (condition(vw_cwf_Doc.readNextRow("READ05")))
                    {
                        if (condition(cwf_Doc_Cabinet_Id.notEquals(pnd_Vars_Pnd_Doc_Cabinet_Id) || cwf_Doc_Tiaa_Rcvd_Dte.notEquals(pnd_Vars_Pnd_Doc_Tiaa_Rcvd_Dte)        //Natural: IF CABINET-ID NE #DOC-CABINET-ID OR TIAA-RCVD-DTE NE #DOC-TIAA-RCVD-DTE OR CASE-WORKPRCSS-MULTI-SUBRQST NE #CASE-WORKPRCSS-MULTI-SUBRQST
                            || cwf_Doc_Case_Workprcss_Multi_Subrqst.notEquals(pnd_Vars_Pnd_Case_Workprcss_Multi_Subrqst)))
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM IMMEDIATE
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Vars_Pnd_Document_Category.setValue(cwf_Doc_Document_Category);                                                                               //Natural: MOVE DOCUMENT-CATEGORY TO #DOCUMENT-CATEGORY
                        pnd_Vars_Pnd_Document_Sub_Category.setValue(cwf_Doc_Document_Sub_Category);                                                                       //Natural: MOVE DOCUMENT-SUB-CATEGORY TO #DOCUMENT-SUB-CATEGORY
                        pnd_Vars_Pnd_Document_Detail.setValue(cwf_Doc_Document_Detail);                                                                                   //Natural: MOVE DOCUMENT-DETAIL TO #DOCUMENT-DETAIL
                        DbsUtil.examine(new ExamineSource(pnd_Vars_Pnd_Ssr_Doc_Array.getValue("*")), new ExamineSearch(pnd_Vars_Pnd_Document_Type), new                   //Natural: EXAMINE #SSR-DOC-ARRAY ( * ) FOR #DOCUMENT-TYPE GIVING INDEX #CWF-DOC-INDEX
                            ExamineGivingIndex(pnd_Vars_Pnd_Cwf_Doc_Index));
                        if (condition(pnd_Vars_Pnd_Cwf_Doc_Index.greater(getZero())))                                                                                     //Natural: IF #CWF-DOC-INDEX > 0
                        {
                            pnd_Vars_Pnd_Cwf_Doc_Found.setValue(true);                                                                                                    //Natural: MOVE TRUE TO #CWF-DOC-FOUND
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM IMMEDIATE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RMIT1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RMIT1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(! (pnd_Vars_Pnd_Cwf_Doc_Found.getBoolean())))                                                                                           //Natural: IF NOT #CWF-DOC-FOUND
                    {
                        pnd_Vars_Pnd_Ssr_Check_Mf.setValue(false);                                                                                                        //Natural: MOVE FALSE TO #SSR-CHECK-MF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  IF TRUST SERVICES ACCOUNT
                if (condition(pnd_Vars_Pnd_Trust_Service.getBoolean()))                                                                                                   //Natural: IF #TRUST-SERVICE
                {
                    //*         MOVE PIN-NBR              TO #PIN-WPID-KEY-PIN /* PIN-EXP
                    //*  PIN-EXP
                    pnd_Vars_Pnd_Pin_Wpid_Key_Pin.moveAll(ldaCwfl6800.getCwf_Mit_Pin_Nbr());                                                                              //Natural: MOVE ALL PIN-NBR TO #PIN-WPID-KEY-PIN
                    pnd_Vars_Pnd_Pin_Wpid_Key_Status.setValue(ldaCwfl6800.getCwf_Mit_Crprte_Status_Ind());                                                                //Natural: MOVE CRPRTE-STATUS-IND TO #PIN-WPID-KEY-STATUS
                    pnd_Vars_Pnd_Pin_Wpid_Key_Wpid.setValue(pnd_Vars_Pnd_Ssr_Wpid.getValue(2));                                                                           //Natural: MOVE #SSR-WPID ( 2 ) TO #PIN-WPID-KEY-WPID
                    //* **      MOVE WORK-PRCSS-ID        TO #PIN-WPID-KEY-WPID /* 06/08
                    pnd_Vars_Pnd_Cwf_Ssr_Found.reset();                                                                                                                   //Natural: RESET #CWF-SSR-FOUND
                    vw_cwf_H1.createHistogram                                                                                                                             //Natural: HISTOGRAM CWF-H1 FOR PIN-WPID-KEY STARTING FROM #PIN-WPID-KEY
                    (
                    "HIST01",
                    "PIN_WPID_KEY",
                    new Wc[] { new Wc("PIN_WPID_KEY", ">=", pnd_Vars_Pnd_Pin_Wpid_Key, WcType.WITH) }
                    );
                    HIST01:
                    while (condition(vw_cwf_H1.readNextRow("HIST01")))
                    {
                        if (condition(cwf_H1_Pnd_H1_Pin.equals(pnd_Vars_Pnd_Pin_Wpid_Key_Pin) && cwf_H1_Pnd_H1_Wpid.equals(pnd_Vars_Pnd_Ssr_Wpid.getValue(2))))           //Natural: IF #H1-PIN = #PIN-WPID-KEY-PIN AND #H1-WPID = #SSR-WPID ( 2 )
                        {
                            pnd_Vars_Pnd_Cwf_Ssr_Found.setValue(true);                                                                                                    //Natural: MOVE TRUE TO #CWF-SSR-FOUND
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM IMMEDIATE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-HISTOGRAM
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RMIT1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RMIT1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  IF EXISTED, DON't create new SSR
                    if (condition(pnd_Vars_Pnd_Cwf_Ssr_Found.getBoolean()))                                                                                               //Natural: IF #CWF-SSR-FOUND
                    {
                        pnd_Vars_Pnd_Ssr_Check_Ts.setValue(false);                                                                                                        //Natural: MOVE FALSE TO #SSR-CHECK-TS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  SEND ONLY ONE SSR TO THE TRANSFER AGENT, IF RQST WAS LOGGED BY A
            //*  CONTACT SHEET SYSTEM (PSS, ICSS AND MCSG) AND THERE WERE NO TIAA
            //*  CONTRACTS SELECTED. ONE NIGHTLY/WEEKLY CLEAN UP JOB NEED TO BE
            //*  RUN (TO BE DETERMINED) TO CLOSE THESE OPEN RECORDS.    04/14/97 GH/OB
            //*  SAVE KEY
            pnd_Vars_Pnd_Rqst_Log_Dte_Tme.setValue(ldaCwfl6800.getCwf_Mit_Rqst_Log_Dte_Tme());                                                                            //Natural: MOVE CWF-MIT.RQST-LOG-DTE-TME TO #RQST-LOG-DTE-TME
            pnd_Vars_Pnd_Case_Id_Key_1.setValue(ldaCwfl6800.getCwf_Mit_Pin_Nbr());                                                                                        //Natural: MOVE CWF-MIT.PIN-NBR TO #CASE-ID-KEY-1
            pnd_Vars_Pnd_Case_Id_Key_2.setValue(ldaCwfl6800.getCwf_Mit_Tiaa_Rcvd_Dte());                                                                                  //Natural: MOVE CWF-MIT.TIAA-RCVD-DTE TO #CASE-ID-KEY-2
            pnd_Vars_Pnd_Case_Id_Key_3.setValue(ldaCwfl6800.getCwf_Mit_Case_Ind());                                                                                       //Natural: MOVE CWF-MIT.CASE-IND TO #CASE-ID-KEY-3
            vw_cwf_Mit_Sub.startDatabaseFind                                                                                                                              //Natural: FIND CWF-MIT-SUB WITH CASE-ID-KEY = #CASE-ID-KEY
            (
            "RMIT2",
            new Wc[] { new Wc("CASE_ID_KEY", "=", pnd_Vars_Pnd_Case_Id_Key.getBinary(), WcType.WITH) }
            );
            RMIT2:
            while (condition(vw_cwf_Mit_Sub.readNextRow("RMIT2")))
            {
                vw_cwf_Mit_Sub.setIfNotFoundControlFlag(false);
                if (condition(cwf_Mit_Sub_Status_Cde.less("8700")))                                                                                                       //Natural: IF CWF-MIT-SUB.STATUS-CDE LT '8700'
                {
                    if (condition(cwf_Mit_Sub_Work_Prcss_Id.equals(pnd_Vars_Pnd_Ssr_Wpid.getValue(1))))                                                                   //Natural: IF CWF-MIT-SUB.WORK-PRCSS-ID = #SSR-WPID ( 1 )
                    {
                        pnd_Vars_Pnd_Ssr_Existed_Mf.setValue(true);                                                                                                       //Natural: MOVE TRUE TO #SSR-EXISTED-MF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cwf_Mit_Sub_Work_Prcss_Id.equals(pnd_Vars_Pnd_Ssr_Wpid.getValue(2))))                                                                   //Natural: IF CWF-MIT-SUB.WORK-PRCSS-ID = #SSR-WPID ( 2 )
                    {
                        pnd_Vars_Pnd_Ssr_Existed_Ts.setValue(true);                                                                                                       //Natural: MOVE TRUE TO #SSR-EXISTED-TS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RMIT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RMIT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CREATE MF SSR
            if (condition(pnd_Vars_Pnd_Mutual_Fund.getBoolean() && ! (pnd_Vars_Pnd_Ssr_Existed_Mf.getBoolean()) && pnd_Vars_Pnd_Ssr_Check_Mf.getBoolean()))               //Natural: IF #MUTUAL-FUND AND ( NOT #SSR-EXISTED-MF ) AND #SSR-CHECK-MF
            {
                DbsUtil.callnat(Cwfn6705.class , getCurrentProcessState(), msg_Info, pnd_Vars_Pnd_Count, pnd_Vars_Pnd_Shrd_Srvce_Ind, pnd_Vars_Pnd_Shrd_Srvce_A_Add_Mf,   //Natural: CALLNAT 'CWFN6705' MSG-INFO #COUNT #SHRD-SRVCE-IND #SHRD-SRVCE-A-ADD-MF #SHRD-SRVCE-A-UPD-MF #SHRD-SRVCE-D-ADD-MF #SHRD-SRVCE-D-UPD-MF #SHRD-SRVCE-P-ADD-MF #SHRD-SRVCE-P-UPD-MF #RQST-LOG-DTE-TME
                    pnd_Vars_Pnd_Shrd_Srvce_A_Upd_Mf, pnd_Vars_Pnd_Shrd_Srvce_D_Add_Mf, pnd_Vars_Pnd_Shrd_Srvce_D_Upd_Mf, pnd_Vars_Pnd_Shrd_Srvce_P_Add_Mf, 
                    pnd_Vars_Pnd_Shrd_Srvce_P_Upd_Mf, pnd_Vars_Pnd_Rqst_Log_Dte_Tme);
                if (condition(Global.isEscape())) return;
                if (condition(msg_Info_Pnd_Pnd_Error_Field.equals(" ")))                                                                                                  //Natural: IF MSG-INFO.##ERROR-FIELD = ' '
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-RECORD
                    sub_Update_Control_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RMIT1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RMIT1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  ERROR OCCURED ON SUBPROGRAM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  WRITE ERROR MSG FROM CWFN6705
                    getReports().write(0, "RQST-LOG-DTE-TME:","(",ldaCwfl6800.getCwf_Mit_Rqst_Log_Dte_Tme(),")","Error Msg   :",msg_Info_Pnd_Pnd_Msg,NEWLINE,             //Natural: WRITE 'RQST-LOG-DTE-TME:' '(' CWF-MIT.RQST-LOG-DTE-TME ')' 'Error Msg   :' MSG-INFO.##MSG / 'Error Field :' MSG-INFO.##ERROR-FIELD
                        "Error Field :",msg_Info_Pnd_Pnd_Error_Field);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RMIT1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RMIT1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  CREATE TS SSR
            if (condition(pnd_Vars_Pnd_Trust_Service.getBoolean() && ! (pnd_Vars_Pnd_Ssr_Existed_Ts.getBoolean()) && pnd_Vars_Pnd_Ssr_Check_Ts.getBoolean()))             //Natural: IF #TRUST-SERVICE AND ( NOT #SSR-EXISTED-TS ) AND #SSR-CHECK-TS
            {
                DbsUtil.callnat(Cwfn6706.class , getCurrentProcessState(), msg_Info, pnd_Vars_Pnd_Count, pnd_Vars_Pnd_Shrd_Srvce_Ind, pnd_Vars_Pnd_Shrd_Srvce_A_Add_Ts,   //Natural: CALLNAT 'CWFN6706' MSG-INFO #COUNT #SHRD-SRVCE-IND #SHRD-SRVCE-A-ADD-TS #SHRD-SRVCE-A-UPD-TS #SHRD-SRVCE-D-ADD-TS #SHRD-SRVCE-D-UPD-TS #SHRD-SRVCE-P-ADD-TS #SHRD-SRVCE-P-UPD-TS #RQST-LOG-DTE-TME
                    pnd_Vars_Pnd_Shrd_Srvce_A_Upd_Ts, pnd_Vars_Pnd_Shrd_Srvce_D_Add_Ts, pnd_Vars_Pnd_Shrd_Srvce_D_Upd_Ts, pnd_Vars_Pnd_Shrd_Srvce_P_Add_Ts, 
                    pnd_Vars_Pnd_Shrd_Srvce_P_Upd_Ts, pnd_Vars_Pnd_Rqst_Log_Dte_Tme);
                if (condition(Global.isEscape())) return;
                if (condition(msg_Info_Pnd_Pnd_Error_Field.equals(" ")))                                                                                                  //Natural: IF MSG-INFO.##ERROR-FIELD = ' '
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-RECORD
                    sub_Update_Control_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RMIT1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RMIT1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  ERROR OCCURED ON SUBPROGRAM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  WRITE ERROR MSG FROM CWFN6705
                    getReports().write(0, "RQST-LOG-DTE-TME:","(",ldaCwfl6800.getCwf_Mit_Rqst_Log_Dte_Tme(),")","Error Msg   :",msg_Info_Pnd_Pnd_Msg,NEWLINE,             //Natural: WRITE 'RQST-LOG-DTE-TME:' '(' CWF-MIT.RQST-LOG-DTE-TME ')' 'Error Msg   :' MSG-INFO.##MSG / 'Error Field :' MSG-INFO.##ERROR-FIELD
                        "Error Field :",msg_Info_Pnd_Pnd_Error_Field);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RMIT1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RMIT1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  END MIT READ
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Ssr_Wpid() throws Exception                                                                                                                      //Natural: GET-SSR-WPID
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        //*  GET TA-SSR WPID
        DbsUtil.callnat(Cwfn6701.class , getCurrentProcessState(), pnd_Vars_Pnd_Work_Prcss_Id, pnd_Vars_Pnd_Shrd_Srvce_Ind, pnd_Vars_Pnd_Ssr_Wpid.getValue("*"),          //Natural: CALLNAT 'CWFN6701' #WORK-PRCSS-ID #SHRD-SRVCE-IND #SSR-WPID ( * ) MSG-INFO
            msg_Info);
        if (condition(Global.isEscape())) return;
        //*  GET-SSR-WPID
    }
    private void sub_Write_Summary() throws Exception                                                                                                                     //Natural: WRITE-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, NEWLINE,NEWLINE,"The Start Date/Time ",pnd_Vars_Pnd_Mf_End_Dt,NEWLINE,"The End   Date/Time ",pnd_Vars_Pnd_Mf_Next_End_Read_Dt,NEWLINE,NEWLINE,NEWLINE,"Mutual Funds :",NEWLINE,NEWLINE,"Total","Account-Based ",  //Natural: WRITE ( 1 ) // 'The Start Date/Time ' #MF-END-DT / 'The End   Date/Time ' #MF-NEXT-END-READ-DT /// 'Mutual Funds :' // 'Total' 'Account-Based ' ( I ) 'Share Service Request Created:' #SHRD-SRVCE-A-ADD-MF / 'Total' 'Account-Based ' ( I ) 'Share Service Request Updated:' #SHRD-SRVCE-A-UPD-MF / 'Total' 'Document-Based' ( I ) 'Share Service Request created:' #SHRD-SRVCE-D-ADD-MF / 'Total' 'Document-Based' ( I ) 'Share Service Request Updated:' #SHRD-SRVCE-D-UPD-MF / 'Total' 'Pin-Based     ' ( I ) 'Share Service Request Created:' #SHRD-SRVCE-P-ADD-MF / 'Total' 'Pin-Based     ' ( I ) 'Share Service Request Updated:' #SHRD-SRVCE-P-UPD-MF // 'Trust Services :' // 'Total' 'Account-Based ' ( I ) 'Share Service Request Created:' #SHRD-SRVCE-A-ADD-TS / 'Total' 'Account-Based ' ( I ) 'Share Service Request Updated:' #SHRD-SRVCE-A-UPD-TS / 'Total' 'Document-Based' ( I ) 'Share Service Request created:' #SHRD-SRVCE-D-ADD-TS / 'Total' 'Document-Based' ( I ) 'Share Service Request Updated:' #SHRD-SRVCE-D-UPD-TS / 'Total' 'Pin-Based     ' ( I ) 'Share Service Request Created:' #SHRD-SRVCE-P-ADD-TS / 'Total' 'Pin-Based     ' ( I ) 'Share Service Request Updated:' #SHRD-SRVCE-P-UPD-TS /// 'Total MIT Records Processed' ( I ) '-------------------- :' #COUNTER-MIT-RECORD // 'Total MIT Records Read' ( I ) '------------------------- :' #COUNTER-MIT-RECORD-ALL
            new FieldAttributes ("AD=I"),"Share Service Request Created:",pnd_Vars_Pnd_Shrd_Srvce_A_Add_Mf,NEWLINE,"Total","Account-Based ", new FieldAttributes 
            ("AD=I"),"Share Service Request Updated:",pnd_Vars_Pnd_Shrd_Srvce_A_Upd_Mf,NEWLINE,"Total","Document-Based", new FieldAttributes ("AD=I"),"Share Service Request created:",pnd_Vars_Pnd_Shrd_Srvce_D_Add_Mf,NEWLINE,"Total","Document-Based", 
            new FieldAttributes ("AD=I"),"Share Service Request Updated:",pnd_Vars_Pnd_Shrd_Srvce_D_Upd_Mf,NEWLINE,"Total","Pin-Based     ", new FieldAttributes 
            ("AD=I"),"Share Service Request Created:",pnd_Vars_Pnd_Shrd_Srvce_P_Add_Mf,NEWLINE,"Total","Pin-Based     ", new FieldAttributes ("AD=I"),"Share Service Request Updated:",pnd_Vars_Pnd_Shrd_Srvce_P_Upd_Mf,NEWLINE,NEWLINE,"Trust Services :",NEWLINE,NEWLINE,"Total","Account-Based ", 
            new FieldAttributes ("AD=I"),"Share Service Request Created:",pnd_Vars_Pnd_Shrd_Srvce_A_Add_Ts,NEWLINE,"Total","Account-Based ", new FieldAttributes 
            ("AD=I"),"Share Service Request Updated:",pnd_Vars_Pnd_Shrd_Srvce_A_Upd_Ts,NEWLINE,"Total","Document-Based", new FieldAttributes ("AD=I"),"Share Service Request created:",pnd_Vars_Pnd_Shrd_Srvce_D_Add_Ts,NEWLINE,"Total","Document-Based", 
            new FieldAttributes ("AD=I"),"Share Service Request Updated:",pnd_Vars_Pnd_Shrd_Srvce_D_Upd_Ts,NEWLINE,"Total","Pin-Based     ", new FieldAttributes 
            ("AD=I"),"Share Service Request Created:",pnd_Vars_Pnd_Shrd_Srvce_P_Add_Ts,NEWLINE,"Total","Pin-Based     ", new FieldAttributes ("AD=I"),"Share Service Request Updated:",pnd_Vars_Pnd_Shrd_Srvce_P_Upd_Ts,NEWLINE,NEWLINE,NEWLINE,"Total MIT Records Processed", 
            new FieldAttributes ("AD=I"),"-------------------- :",pnd_Vars_Pnd_Counter_Mit_Record,NEWLINE,NEWLINE,"Total MIT Records Read", new FieldAttributes 
            ("AD=I"),"------------------------- :",pnd_Vars_Pnd_Counter_Mit_Record_All);
        if (Global.isEscape()) return;
        //*  WRITE-SUMMARY
    }
    private void sub_Update_Control_Record() throws Exception                                                                                                             //Natural: UPDATE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        G:                                                                                                                                                                //Natural: GET CWF-SUPTB #MF-CONTROL-REC-ISN
        vw_cwf_Suptb.readByID(pnd_Vars_Pnd_Mf_Control_Rec_Isn.getLong(), "G");
        cwf_Suptb_Tbl_Data_Field.setValue(pnd_Vars_Pnd_Tbl_Data_Field);                                                                                                   //Natural: MOVE #TBL-DATA-FIELD TO CWF-SUPTB.TBL-DATA-FIELD
        cwf_Suptb_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                           //Natural: MOVE *TIMX TO CWF-SUPTB.TBL-UPDTE-DTE-TME
        cwf_Suptb_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                               //Natural: MOVE *DATX TO CWF-SUPTB.TBL-UPDTE-DTE
        cwf_Suptb_Tbl_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                                    //Natural: MOVE *INIT-USER TO CWF-SUPTB.TBL-UPDTE-OPRTR-CDE
        vw_cwf_Suptb.updateDBRow("G");                                                                                                                                    //Natural: UPDATE ( G. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  UPDATE-CONTROL-RECORD
    }
    private void sub_Log_Msg() throws Exception                                                                                                                           //Natural: LOG-MSG
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************** *
        //*  OBJECT..: ERLC1000
        //*  FUNCTION: INVOKE INFRASTRUCTURE MODULE TO STORE ERROR DETAILS TO
        //*            ERROR-HANDLER FILE.
        //*  MODIFICATION LOG:
        //*  =================
        //* ********************************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pdaErla1000.getErla1000_Err_Pgm_Name().setValue(Global.getPROGRAM());                                                                                             //Natural: ASSIGN ERLA1000.ERR-PGM-NAME = *PROGRAM
        pdaErla1000.getErla1000_Err_Pgm_Level().setValue(Global.getLEVEL());                                                                                              //Natural: ASSIGN ERLA1000.ERR-PGM-LEVEL = *LEVEL
        pdaErla1000.getErla1000_Err_Nbr().setValue(Global.getERROR_NR());                                                                                                 //Natural: ASSIGN ERLA1000.ERR-NBR = *ERROR-NR
        pdaErla1000.getErla1000_Err_Line_Nbr().setValue(Global.getERROR_LINE());                                                                                          //Natural: ASSIGN ERLA1000.ERR-LINE-NBR = *ERROR-LINE
        pdaErla1000.getErla1000_Err_Status_Cde().setValue("U");                                                                                                           //Natural: ASSIGN ERLA1000.ERR-STATUS-CDE = 'U'
        pdaErla1000.getErla1000_Err_Type_Cde().setValue("N");                                                                                                             //Natural: ASSIGN ERLA1000.ERR-TYPE-CDE = 'N'
        pdaErla1000.getErla1000_Err_Notes().setValue(DbsUtil.compress(pnd_Vars_Pnd_Store_Msg.getValue(1)));                                                               //Natural: COMPRESS #STORE-MSG ( 1 ) INTO ERLA1000.ERR-NOTES
        DbsUtil.callnat(Erln1000.class , getCurrentProcessState(), pdaErla1000.getErla1000());                                                                            //Natural: CALLNAT 'ERLN1000' ERLA1000
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, NEWLINE,NEWLINE,new TabSetting(24),"Share Service Requests Without Electronic Documents",NEWLINE,new TabSetting(4),"RcvdDate  -WPID-  ----Request----  --PIN--  Account-No ","--------NAME--------  Log Unit  MOC",NEWLINE,new  //Natural: WRITE ( 2 ) // 24T 'Share Service Requests Without Electronic Documents' / 4T 'RcvdDate  -WPID-  ----Request----  --PIN--  Account-No ' '--------NAME--------  Log Unit  MOC' / 4T '--------  ------  ---------------  ------------  ---------- ' '--------------------  --------  ---' /
                        TabSetting(4),"--------  ------  ---------------  ------------  ---------- ","--------------------  --------  ---",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        msg_Info_Pnd_Pnd_Msg.setValue(DbsUtil.compress("Natural Error", Global.getERROR_NR(), "on line", Global.getERROR_LINE(), "of", Global.getPROGRAM()));             //Natural: COMPRESS 'Natural Error' *ERROR-NR 'on line' *ERROR-LINE 'of' *PROGRAM INTO MSG-INFO.##MSG
        getReports().write(0, msg_Info_Pnd_Pnd_Msg);                                                                                                                      //Natural: WRITE MSG-INFO.##MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY
        sub_Write_Summary();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
        sub_Log_Msg();
        if (condition(Global.isEscape())) {return;}
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132 SG=OFF");
        Global.format(1, "PS=60 LS=132 SG=OFF");
        Global.format(2, "PS=60 LS=132 SG=OFF");
    }
}
