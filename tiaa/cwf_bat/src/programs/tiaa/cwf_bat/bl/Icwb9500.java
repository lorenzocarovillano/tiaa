/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:40:59 PM
**        * FROM NATURAL PROGRAM : Icwb9500
************************************************************
**        * FILE NAME            : Icwb9500.java
**        * CLASS NAME           : Icwb9500
**        * INSTANCE NAME        : Icwb9500
************************************************************
************************************************************************
* PROGRAM  - ICWB9500
* FUNCTION - IRIS INTERFACE BATCH MASS LOAD IRIS TABLE
* AUTHOR   - JOSEPH S. PATINGO
*
* CMPRT01  - REPORT OUTPUT
* CMPRT02  - ERROR REPORT
* CMWKF01  - TSO DATASET FOR LOADING
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwb9500 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private PdaIcwa2615 pdaIcwa2615;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Tbl_Table_Access_Level;
    private DbsField pnd_Record;

    private DbsGroup pnd_Record__R_Field_1;
    private DbsField pnd_Record_Pnd_Ppg;
    private DbsField pnd_Record_Pnd_Fil2;
    private DbsField pnd_Record_Pnd_Ra_Gra_Ind;
    private DbsField pnd_Record_Pnd_Sra_Gsra_Ind;
    private DbsField pnd_Header_Rec;

    private DbsGroup pnd_Header_Rec__R_Field_2;
    private DbsField pnd_Header_Rec_Pnd_Fil1;
    private DbsField pnd_Header_Rec_Pnd_Date;
    private DbsField pnd_Header_Rec_Pnd_Fil2;
    private DbsField pnd_Header_Rec_Pnd_Rec_Cnt;

    private DbsGroup pnd_Header_Rec__R_Field_3;
    private DbsField pnd_Header_Rec_Pnd_Records;
    private DbsField pnd_Record_Exists;
    private DbsField pnd_Rec_Cntr;
    private DbsField pnd_Reject_Cntr;
    private DbsField pnd_Del_Ctr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaIcwa2615 = new PdaIcwa2615(localVariables);

        // Local Variables

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 15);

        pnd_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Record__R_Field_1", "REDEFINE", pnd_Record);
        pnd_Record_Pnd_Ppg = pnd_Record__R_Field_1.newFieldInGroup("pnd_Record_Pnd_Ppg", "#PPG", FieldType.STRING, 6);
        pnd_Record_Pnd_Fil2 = pnd_Record__R_Field_1.newFieldInGroup("pnd_Record_Pnd_Fil2", "#FIL2", FieldType.STRING, 2);
        pnd_Record_Pnd_Ra_Gra_Ind = pnd_Record__R_Field_1.newFieldInGroup("pnd_Record_Pnd_Ra_Gra_Ind", "#RA-GRA-IND", FieldType.STRING, 1);
        pnd_Record_Pnd_Sra_Gsra_Ind = pnd_Record__R_Field_1.newFieldInGroup("pnd_Record_Pnd_Sra_Gsra_Ind", "#SRA-GSRA-IND", FieldType.STRING, 1);
        pnd_Header_Rec = localVariables.newFieldInRecord("pnd_Header_Rec", "#HEADER-REC", FieldType.STRING, 15);

        pnd_Header_Rec__R_Field_2 = localVariables.newGroupInRecord("pnd_Header_Rec__R_Field_2", "REDEFINE", pnd_Header_Rec);
        pnd_Header_Rec_Pnd_Fil1 = pnd_Header_Rec__R_Field_2.newFieldInGroup("pnd_Header_Rec_Pnd_Fil1", "#FIL1", FieldType.STRING, 1);
        pnd_Header_Rec_Pnd_Date = pnd_Header_Rec__R_Field_2.newFieldInGroup("pnd_Header_Rec_Pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_Header_Rec_Pnd_Fil2 = pnd_Header_Rec__R_Field_2.newFieldInGroup("pnd_Header_Rec_Pnd_Fil2", "#FIL2", FieldType.STRING, 1);
        pnd_Header_Rec_Pnd_Rec_Cnt = pnd_Header_Rec__R_Field_2.newFieldInGroup("pnd_Header_Rec_Pnd_Rec_Cnt", "#REC-CNT", FieldType.STRING, 5);

        pnd_Header_Rec__R_Field_3 = pnd_Header_Rec__R_Field_2.newGroupInGroup("pnd_Header_Rec__R_Field_3", "REDEFINE", pnd_Header_Rec_Pnd_Rec_Cnt);
        pnd_Header_Rec_Pnd_Records = pnd_Header_Rec__R_Field_3.newFieldInGroup("pnd_Header_Rec_Pnd_Records", "#RECORDS", FieldType.NUMERIC, 5);
        pnd_Record_Exists = localVariables.newFieldInRecord("pnd_Record_Exists", "#RECORD-EXISTS", FieldType.BOOLEAN, 1);
        pnd_Rec_Cntr = localVariables.newFieldInRecord("pnd_Rec_Cntr", "#REC-CNTR", FieldType.NUMERIC, 5);
        pnd_Reject_Cntr = localVariables.newFieldInRecord("pnd_Reject_Cntr", "#REJECT-CNTR", FieldType.NUMERIC, 5);
        pnd_Del_Ctr = localVariables.newFieldInRecord("pnd_Del_Ctr", "#DEL-CTR", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Record_Exists.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Icwb9500() throws Exception
    {
        super("Icwb9500");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        gdaCwfg000.getParm_Employee_Info_Pnd_Pnd_Racf_Id().setValue(Global.getINIT_USER());                                                                               //Natural: FORMAT ( 1 ) LS = 100 PS = 57;//Natural: FORMAT ( 2 ) LS = 100 PS = 57;//Natural: MOVE *INIT-USER TO ##RACF-ID
        gdaCwfg000.getParm_Unit_Info_Pnd_Pnd_Unit_Cde().setValue("CWF");                                                                                                  //Natural: MOVE 'CWF' TO ##UNIT-CDE
        getWorkFiles().read(1, pnd_Record);                                                                                                                               //Natural: READ WORK 1 ONCE #RECORD
        pnd_Header_Rec.setValue(pnd_Record);                                                                                                                              //Natural: MOVE #RECORD TO #HEADER-REC
        if (condition(DbsUtil.maskMatches(pnd_Header_Rec_Pnd_Date,"MMDDYYYY")))                                                                                           //Natural: IF #DATE = MASK ( MMDDYYYY ) THEN
        {
                                                                                                                                                                          //Natural: PERFORM VALIDATE-DATE
            sub_Validate_Date();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Record_Exists.getBoolean()))                                                                                                                //Natural: IF #RECORD-EXISTS
            {
                getReports().write(1, "FILE FOR THIS DATE ALREADY LOADED");                                                                                               //Natural: WRITE ( 1 ) 'FILE FOR THIS DATE ALREADY LOADED'
                if (Global.isEscape()) return;
                DbsUtil.stop();  if (true) return;                                                                                                                        //Natural: STOP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        R1:                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 );//Natural: READ WORK 1 #RECORD
        while (condition(getWorkFiles().read(1, pnd_Record)))
        {
            pnd_Rec_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-CNTR
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-WORK
        R1_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            if (condition(pnd_Rec_Cntr.equals(pnd_Header_Rec_Pnd_Records)))                                                                                               //Natural: IF #REC-CNTR = #RECORDS
            {
                Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "R1");                                                                                    //Natural: ESCAPE BOTTOM ( R1. )
                if (true) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, "ACTUAL RECORDS AND HEADER COUNT DOES NOT MATCH");                                                                                  //Natural: WRITE ( 1 ) 'ACTUAL RECORDS AND HEADER COUNT DOES NOT MATCH'
                if (condition(Global.isEscape())) return;
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        pnd_Rec_Cntr.reset();                                                                                                                                             //Natural: RESET #REC-CNTR
                                                                                                                                                                          //Natural: PERFORM EMPTY-SUPPORT-TABLE
        sub_Empty_Support_Table();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().read(1, pnd_Header_Rec);                                                                                                                           //Natural: READ WORK 1 ONCE #HEADER-REC
        R2:                                                                                                                                                               //Natural: READ WORK 1 #RECORD
        while (condition(getWorkFiles().read(1, pnd_Record)))
        {
            if (condition(pnd_Record_Pnd_Ra_Gra_Ind.equals("Y") || pnd_Record_Pnd_Sra_Gsra_Ind.equals("Y")))                                                              //Natural: IF #RA-GRA-IND = 'Y' OR #SRA-GSRA-IND = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM ADD-RECORDS
                sub_Add_Records();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg_Nr().equals(getZero())))                                                                                 //Natural: IF MSG-INFO.##MSG-NR = 0
                {
                    DbsUtil.examine(new ExamineSource(gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg()), new ExamineSearch(":1:"), new ExamineReplace("XXXXX#"));                     //Natural: EXAMINE ##MSG FOR ':1:' REPLACE WITH 'XXXXX#'
                    DbsUtil.examine(new ExamineSource(gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg()), new ExamineSearch("XXXXX"), new ExamineReplace(pnd_Record_Pnd_Ppg));         //Natural: EXAMINE ##MSG FOR 'XXXXX' REPLACE WITH #PPG
                    DbsUtil.examine(new ExamineSource(gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg()), new ExamineSearch("#"), new ExamineReplace(" "));                            //Natural: EXAMINE ##MSG FOR '#' REPLACE WITH ' '
                    pnd_Rec_Cntr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-CNTR
                    getReports().display(1, "PPG CODE",                                                                                                                   //Natural: DISPLAY ( 1 ) 'PPG CODE' #PPG 'RA-GRA/IND' #RA-GRA-IND 'SRA-GSRA/IND' #SRA-GSRA-IND 'MSG' ##MSG ( AL = 40 )
                    		pnd_Record_Pnd_Ppg,"RA-GRA/IND",
                    		pnd_Record_Pnd_Ra_Gra_Ind,"SRA-GSRA/IND",
                    		pnd_Record_Pnd_Sra_Gsra_Ind,"MSG",
                    		gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg(), new AlphanumericLength (40));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().display(2, "PPG CODE",                                                                                                                   //Natural: DISPLAY ( 2 ) 'PPG CODE' #PPG 'RA-GRA/IND' #RA-GRA-IND 'SRA-GSRA/IND' #SRA-GSRA-IND 'MSG' ##MSG ( AL = 40 ) 'Return/Code' ##RETURN-CODE
                    		pnd_Record_Pnd_Ppg,"RA-GRA/IND",
                    		pnd_Record_Pnd_Ra_Gra_Ind,"SRA-GSRA/IND",
                    		pnd_Record_Pnd_Sra_Gsra_Ind,"MSG",
                    		gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg(), new AlphanumericLength (40),"Return/Code",
                    		gdaCwfg000.getMsg_Info_Pnd_Pnd_Return_Code());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg().setValue(DbsUtil.compress(pnd_Record_Pnd_Ppg, "skipped. No IRIS instructions."));                                    //Natural: COMPRESS #PPG 'skipped. No IRIS instructions.' INTO MSG-INFO.##MSG
                getReports().write(2, ReportOption.NOTITLE,new ReportTAsterisk(pnd_Record_Pnd_Ppg),pnd_Record_Pnd_Ppg,new ReportTAsterisk(pnd_Record_Pnd_Ra_Gra_Ind),pnd_Record_Pnd_Ra_Gra_Ind,new  //Natural: WRITE ( 2 ) T*#PPG #PPG T*#RA-GRA-IND #RA-GRA-IND T*#SRA-GSRA-IND #SRA-GSRA-IND T*##MSG ##MSG ( AL = 40 )
                    ReportTAsterisk(pnd_Record_Pnd_Sra_Gsra_Ind),pnd_Record_Pnd_Sra_Gsra_Ind,new ReportTAsterisk(gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg()),gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg(), 
                    new AlphanumericLength (40));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Reject_Cntr.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REJECT-CNTR
            }                                                                                                                                                             //Natural: END-IF
            //*  UPLOAD LOG ICW-IRIS-HEADER                                                                                                                               //Natural: AT END OF DATA
            DbsUtil.callnat(Icwn2614.class , getCurrentProcessState(), pnd_Header_Rec_Pnd_Date, pnd_Header_Rec_Pnd_Records, gdaCwfg000.getDialog_Info(),                  //Natural: CALLNAT 'ICWN2614' #DATE #RECORDS DIALOG-INFO MSG-INFO PASS PARM-UNIT-INFO PARM-EMPLOYEE-INFO
                gdaCwfg000.getMsg_Info(), gdaCwfg000.getPass(), gdaCwfg000.getParm_Unit_Info(), gdaCwfg000.getParm_Employee_Info());
            if (condition(Global.isEscape())) return;
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-WORK
        R2_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            getReports().write(1, NEWLINE,"RECORDS ADDED           ",pnd_Rec_Cntr,NEWLINE,"RECORDS SKIPPED         ",pnd_Reject_Cntr,NEWLINE,"RECORD COUNT FROM HEADER",  //Natural: WRITE ( 1 ) / 'RECORDS ADDED           ' #REC-CNTR / 'RECORDS SKIPPED         ' #REJECT-CNTR / 'RECORD COUNT FROM HEADER' #RECORDS
                pnd_Header_Rec_Pnd_Records);
            if (condition(Global.isEscape())) return;
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,"RECORDS ADDED           ",pnd_Rec_Cntr,NEWLINE,"RECORDS SKIPPED         ",pnd_Reject_Cntr,                //Natural: WRITE ( 2 ) / 'RECORDS ADDED           ' #REC-CNTR / 'RECORDS SKIPPED         ' #REJECT-CNTR / 'RECORD COUNT FROM HEADER' #RECORDS
                NEWLINE,"RECORD COUNT FROM HEADER",pnd_Header_Rec_Pnd_Records);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-DATE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EMPTY-SUPPORT-TABLE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-RECORDS
    }
    private void sub_Validate_Date() throws Exception                                                                                                                     //Natural: VALIDATE-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Record_Exists.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #RECORD-EXISTS
        DbsUtil.callnat(Icwn2613.class , getCurrentProcessState(), pnd_Header_Rec_Pnd_Date, pnd_Header_Rec_Pnd_Records, gdaCwfg000.getDialog_Info(), gdaCwfg000.getMsg_Info(),  //Natural: CALLNAT 'ICWN2613' #DATE #RECORDS DIALOG-INFO MSG-INFO PASS PARM-UNIT-INFO PARM-EMPLOYEE-INFO
            gdaCwfg000.getPass(), gdaCwfg000.getParm_Unit_Info(), gdaCwfg000.getParm_Employee_Info());
        if (condition(Global.isEscape())) return;
        if (condition(gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg().equals("RECORD ALREADY EXISTS")))                                                                              //Natural: IF MSG-INFO.##MSG = 'RECORD ALREADY EXISTS'
        {
            pnd_Record_Exists.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #RECORD-EXISTS
        }                                                                                                                                                                 //Natural: END-IF
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }
    private void sub_Empty_Support_Table() throws Exception                                                                                                               //Natural: EMPTY-SUPPORT-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pdaIcwa2615.getIcwa2615_Tbl_Scrty_Level_Ind().setValue("A");                                                                                                      //Natural: MOVE 'A' TO ICWA2615.TBL-SCRTY-LEVEL-IND
        pdaIcwa2615.getIcwa2615_Tbl_Actve_Ind().setValue("A");                                                                                                            //Natural: MOVE 'A' TO ICWA2615.TBL-ACTVE-IND
        pdaIcwa2615.getIcwa2615_Tbl_Table_Nme().setValue("ICW-IRIS-INSTRUCTION");                                                                                         //Natural: MOVE 'ICW-IRIS-INSTRUCTION' TO ICWA2615.TBL-TABLE-NME
        pdaIcwa2615.getIcwa2615_Id_Structure().setValuesByName(pdaIcwa2615.getIcwa2615());                                                                                //Natural: MOVE BY NAME ICWA2615 TO ICWA2615-ID.STRUCTURE
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY FROM ICWA2615-ID
        (
        "RD",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pdaIcwa2615.getIcwa2615_Id(), WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        RD:
        while (condition(vw_cwf_Support_Tbl.readNextRow("RD")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.greater(pdaIcwa2615.getIcwa2615_Tbl_Table_Nme())))                                                                //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME GT ICWA2615.TBL-TABLE-NME
            {
                if (true) break RD;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RD. )
            }                                                                                                                                                             //Natural: END-IF
            cwf_Support_Tbl_Tbl_Dlte_Dte_Tme.setValue(Global.getTIMX());                                                                                                  //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-DLTE-DTE-TME := *TIMX
            cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-DLTE-OPRTR-CDE := *INIT-USER
            cwf_Support_Tbl_Tbl_Data_Field.reset();                                                                                                                       //Natural: RESET CWF-SUPPORT-TBL.TBL-DATA-FIELD
            vw_cwf_Support_Tbl.updateDBRow("RD");                                                                                                                         //Natural: UPDATE ( RD. )
            pnd_Del_Ctr.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #DEL-CTR
            if (condition(pnd_Del_Ctr.greater(10)))                                                                                                                       //Natural: IF #DEL-CTR GT 10
            {
                pnd_Del_Ctr.reset();                                                                                                                                      //Natural: RESET #DEL-CTR
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }
    private void sub_Add_Records() throws Exception                                                                                                                       //Natural: ADD-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg_Data().getValue(3).setValue(Global.getPROGRAM());                                                                              //Natural: MOVE *PROGRAM TO MSG-INFO.##MSG-DATA ( 3 )
        DbsUtil.callnat(Icwn2612.class , getCurrentProcessState(), pnd_Record_Pnd_Ppg, pnd_Record_Pnd_Ra_Gra_Ind, pnd_Record_Pnd_Sra_Gsra_Ind, gdaCwfg000.getDialog_Info(),  //Natural: CALLNAT 'ICWN2612' #PPG #RA-GRA-IND #SRA-GSRA-IND DIALOG-INFO MSG-INFO PASS PARM-UNIT-INFO PARM-EMPLOYEE-INFO
            gdaCwfg000.getMsg_Info(), gdaCwfg000.getPass(), gdaCwfg000.getParm_Unit_Info(), gdaCwfg000.getParm_Employee_Info());
        if (condition(Global.isEscape())) return;
        if (condition(gdaCwfg000.getMsg_Info_Pnd_Pnd_Return_Code().equals(" ")))                                                                                          //Natural: IF MSG-INFO.##RETURN-CODE = ' '
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, "ICW-IRIS BATCH LOAD",NEWLINE,Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,Global.getTIMX(),                     //Natural: WRITE ( 1 ) 'ICW-IRIS BATCH LOAD' / *DATX ( EM = MM/DD/YYYY ) / *TIMX ( EM = HH:II:SS.T ) / '_' ( 80 ) /
                        new ReportEditMask ("HH:II:SS.T"),NEWLINE,"_",new RepeatItem(80),NEWLINE);
                    if (condition(getReports().getPageNumberDbs(1).equals(1)))                                                                                            //Natural: IF *PAGE-NUMBER ( 1 ) = 1
                    {
                        getReports().write(1, "FILE DATE",pnd_Header_Rec_Pnd_Date,NEWLINE,"RECORDS  ",pnd_Header_Rec_Pnd_Records,NEWLINE,NEWLINE);                        //Natural: WRITE ( 1 ) 'FILE DATE' #DATE / 'RECORDS  ' #RECORDS //
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(getReports().getPageNumberDbs(2).equals(1)))                                                                                            //Natural: IF *PAGE-NUMBER ( 2 ) = 1
                    {
                        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,"�E�&l1o2a5.6c72p3e66F�&a2L�(9905X�&l0L");                                          //Natural: WRITE ( 2 ) NOTITLE NOHDR '�E�&l1o2a5.6c72p3e66F�&a2L�(9905X�&l0L'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE,"ICW-IRIS BATCH LOAD",NEWLINE,"ERROR/REJECT LOG",NEWLINE,Global.getDATX(), new ReportEditMask              //Natural: WRITE ( 2 ) 'ICW-IRIS BATCH LOAD' / 'ERROR/REJECT LOG' / *DATX ( EM = MM/DD/YYYY ) / *TIMX ( EM = HH:II:SS.T ) / '_' ( 80 ) /
                        ("MM/DD/YYYY"),NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"),NEWLINE,"_",new RepeatItem(80),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=100 PS=57");
        Global.format(2, "LS=100 PS=57");

        getReports().setDisplayColumns(1, "PPG CODE",
        		pnd_Record_Pnd_Ppg,"RA-GRA/IND",
        		pnd_Record_Pnd_Ra_Gra_Ind,"SRA-GSRA/IND",
        		pnd_Record_Pnd_Sra_Gsra_Ind,"MSG",
        		gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg(), new AlphanumericLength (40));
        getReports().setDisplayColumns(2, "PPG CODE",
        		pnd_Record_Pnd_Ppg,"RA-GRA/IND",
        		pnd_Record_Pnd_Ra_Gra_Ind,"SRA-GSRA/IND",
        		pnd_Record_Pnd_Sra_Gsra_Ind,"MSG",
        		gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg(), new AlphanumericLength (40),"Return/Code",
        		gdaCwfg000.getMsg_Info_Pnd_Pnd_Return_Code());
    }
}
