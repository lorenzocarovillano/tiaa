/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:34:45 PM
**        * FROM NATURAL PROGRAM : Cwfb4450
************************************************************
**        * FILE NAME            : Cwfb4450.java
**        * CLASS NAME           : Cwfb4450
**        * INSTANCE NAME        : Cwfb4450
************************************************************
************************************************************************
* PROGRAM  : CWFB4450
* SYSTEM   : PROJCWF
* FUNCTION : TO MONITOR ON A MONTHLY BASIS ADDITIONS AND CHANGES MADE
*            TO THE WPID, STATUS AND EMPLOYEE TABLES FOR THE PURPOSE OF
*            INSURING THAT CUSTOMER VIEW DESCRIPTIONS ADHERE TO COMPANY
*            REQUIREMENTS.
*
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
*
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4450 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Vars;
    private DbsField pnd_Vars_Pnd_Hold_Isn;
    private DbsField pnd_Vars_Pnd_Status_Tot_Cnt;
    private DbsField pnd_Vars_Pnd_Wpid_Tot_Cnt;
    private DbsField pnd_Vars_Pnd_Empl_Tot_Cnt;
    private DbsField pnd_Vars_Pnd_Seq_Cnt;
    private DbsField pnd_Vars_Pnd_Hold_Unit_Cde;
    private DbsField pnd_Vars_Pnd_First_Detail_Line;
    private DbsField pnd_Vars_Pnd_Hold_Empl_Nme;
    private DbsField pnd_Vars_Pnd_Empl_Nme_1;
    private DbsField pnd_Vars_Pnd_Empl_Nme_2;
    private DbsField pnd_Vars_Pnd_Empl_Nme_3;
    private DbsField pnd_Vars_Pnd_Racf_Id;
    private DbsField pnd_Run_Monthly_Date;

    private DbsGroup pnd_Run_Monthly_Date__R_Field_1;
    private DbsField pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm;

    private DbsGroup pnd_Run_Monthly_Date__R_Field_2;
    private DbsField pnd_Run_Monthly_Date_Pnd_Run_Yyyy;
    private DbsField pnd_Run_Monthly_Date_Pnd_Run_Mm;
    private DbsField pnd_Run_Monthly_Date_Pnd_Run_Dd;

    private DbsGroup pnd_Run_Monthly_Date__R_Field_3;
    private DbsField pnd_Run_Monthly_Date_Pnd_Run_Monthly_Dte_A;
    private DbsField pnd_Run_Monthly_Dte_D;
    private DbsField pnd_Entry_Dte_Tme;

    private DbsGroup pnd_Entry_Dte_Tme__R_Field_4;
    private DbsField pnd_Entry_Dte_Tme_Pnd_Entry_Dte;

    private DbsGroup pnd_Entry_Dte_Tme__R_Field_5;
    private DbsField pnd_Entry_Dte_Tme_Pnd_Entry_Yyyy_Mm;
    private DbsField pnd_Entry_Dte_Tme_Pnd_Entry_Dd;

    private DbsGroup pnd_Entry_Dte_Tme__R_Field_6;
    private DbsField pnd_Entry_Dte_Tme_Pnd_Entry_Yyyy;
    private DbsField pnd_Entry_Dte_Tme_Pnd_Entry_Mm_Dd;
    private DbsField pnd_Entry_Dte_Tme_Pnd_Entry_Tme;
    private DbsField pnd_Entry_Dte_A;
    private DbsField pnd_Updte_Dte_Tme;

    private DbsGroup pnd_Updte_Dte_Tme__R_Field_7;
    private DbsField pnd_Updte_Dte_Tme_Pnd_Updte_Dte;

    private DbsGroup pnd_Updte_Dte_Tme__R_Field_8;
    private DbsField pnd_Updte_Dte_Tme_Pnd_Updte_Yyyy_Mm;
    private DbsField pnd_Updte_Dte_Tme_Pnd_Updte_Dd;

    private DbsGroup pnd_Updte_Dte_Tme__R_Field_9;
    private DbsField pnd_Updte_Dte_Tme_Pnd_Updte_Yyyy;
    private DbsField pnd_Updte_Dte_Tme_Pnd_Updte_Mm_Dd;
    private DbsField pnd_Updte_Dte_Tme_Pnd_Updte_Tme;
    private DbsField pnd_Updte_Dte_A;
    private DbsField pnd_Dlte_Dte_Tme;

    private DbsGroup pnd_Dlte_Dte_Tme__R_Field_10;
    private DbsField pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte;

    private DbsGroup pnd_Dlte_Dte_Tme__R_Field_11;
    private DbsField pnd_Dlte_Dte_Tme_Pnd_Dlte_Yyyy_Mm;
    private DbsField pnd_Dlte_Dte_Tme_Pnd_Dlte_Dd;

    private DbsGroup pnd_Dlte_Dte_Tme__R_Field_12;
    private DbsField pnd_Dlte_Dte_Tme_Pnd_Dlte_Yyyy;
    private DbsField pnd_Dlte_Dte_Tme_Pnd_Dlte_Mm_Dd;
    private DbsField pnd_Dlte_Dte_Tme_Pnd_Dlte_Tme;
    private DbsField pnd_Dlte_Dte_A;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_13;
    private DbsField cwf_Support_Tbl_Tbl_Updt_Yyyymmdd;

    private DataAccessProgramView vw_status_Tbl;
    private DbsField status_Tbl_Status_Cde;
    private DbsField status_Tbl_Status_Unit_Cde;
    private DbsField status_Tbl_Status_Dscrptn_Txt;
    private DbsField status_Tbl_Dlte_Dte_Tme;
    private DbsField status_Tbl_Dlte_Oprtr_Cde;
    private DbsField status_Tbl_Entry_Dte_Tme;
    private DbsField status_Tbl_Entry_Oprtr_Cde;
    private DbsField status_Tbl_Updte_Dte_Tme;
    private DbsField status_Tbl_Updte_Oprtr_Cde;
    private DbsField status_Tbl_Internet_Dscrptn_Txt;

    private DataAccessProgramView vw_wpid_Tbl;
    private DbsField wpid_Tbl_Work_Prcss_Id;
    private DbsField wpid_Tbl_Work_Prcss_Long_Nme;
    private DbsField wpid_Tbl_Entry_Dte_Tme;
    private DbsField wpid_Tbl_Entry_Oprtr_Cde;
    private DbsField wpid_Tbl_Updte_Dte_Tme;
    private DbsField wpid_Tbl_Updte_Oprtr_Cde;
    private DbsField wpid_Tbl_Dlte_Dte_Tme;
    private DbsField wpid_Tbl_Dlte_Oprtr_Cde;
    private DbsField wpid_Tbl_Wpid_Intrnet_Nme;

    private DataAccessProgramView vw_empl_Tbl_Post;
    private DbsField empl_Tbl_Post_Empl_Unit_Cde;
    private DbsField empl_Tbl_Post_Empl_Racf_Id;
    private DbsField empl_Tbl_Post_Empl_Nme;

    private DbsGroup empl_Tbl_Post__R_Field_14;
    private DbsField empl_Tbl_Post_Empl_Nme_First;
    private DbsField empl_Tbl_Post_Empl_Nme_Last;
    private DbsField empl_Tbl_Post_Empl_Signatory;
    private DbsField empl_Tbl_Post_Dlte_Dte_Tme;
    private DbsField empl_Tbl_Post_Dlte_Oprtr_Cde;
    private DbsField empl_Tbl_Post_Entry_Dte_Tme;
    private DbsField empl_Tbl_Post_Entry_Oprtr_Cde;
    private DbsField empl_Tbl_Post_Updte_Dte_Tme;
    private DbsField empl_Tbl_Post_Updte_Oprtr_Cde;

    private DataAccessProgramView vw_etp_Name;
    private DbsField etp_Name_Empl_Racf_Id;
    private DbsField etp_Name_Empl_Nme;

    private DbsGroup etp_Name__R_Field_15;
    private DbsField etp_Name_Empl_Nme_First;
    private DbsField etp_Name_Empl_Nme_Last;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Vars = localVariables.newGroupInRecord("pnd_Vars", "#VARS");
        pnd_Vars_Pnd_Hold_Isn = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Hold_Isn", "#HOLD-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Vars_Pnd_Status_Tot_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Status_Tot_Cnt", "#STATUS-TOT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Wpid_Tot_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Wpid_Tot_Cnt", "#WPID-TOT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Empl_Tot_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Empl_Tot_Cnt", "#EMPL-TOT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Seq_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Seq_Cnt", "#SEQ-CNT", FieldType.NUMERIC, 4);
        pnd_Vars_Pnd_Hold_Unit_Cde = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Hold_Unit_Cde", "#HOLD-UNIT-CDE", FieldType.STRING, 8);
        pnd_Vars_Pnd_First_Detail_Line = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_First_Detail_Line", "#FIRST-DETAIL-LINE", FieldType.STRING, 1);
        pnd_Vars_Pnd_Hold_Empl_Nme = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Hold_Empl_Nme", "#HOLD-EMPL-NME", FieldType.STRING, 25);
        pnd_Vars_Pnd_Empl_Nme_1 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Empl_Nme_1", "#EMPL-NME-1", FieldType.STRING, 25);
        pnd_Vars_Pnd_Empl_Nme_2 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Empl_Nme_2", "#EMPL-NME-2", FieldType.STRING, 25);
        pnd_Vars_Pnd_Empl_Nme_3 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Empl_Nme_3", "#EMPL-NME-3", FieldType.STRING, 25);
        pnd_Vars_Pnd_Racf_Id = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Run_Monthly_Date = localVariables.newFieldInRecord("pnd_Run_Monthly_Date", "#RUN-MONTHLY-DATE", FieldType.NUMERIC, 8);

        pnd_Run_Monthly_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Run_Monthly_Date__R_Field_1", "REDEFINE", pnd_Run_Monthly_Date);
        pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm = pnd_Run_Monthly_Date__R_Field_1.newFieldInGroup("pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm", "#RUN-YYYY-MM", 
            FieldType.NUMERIC, 6);

        pnd_Run_Monthly_Date__R_Field_2 = pnd_Run_Monthly_Date__R_Field_1.newGroupInGroup("pnd_Run_Monthly_Date__R_Field_2", "REDEFINE", pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm);
        pnd_Run_Monthly_Date_Pnd_Run_Yyyy = pnd_Run_Monthly_Date__R_Field_2.newFieldInGroup("pnd_Run_Monthly_Date_Pnd_Run_Yyyy", "#RUN-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Run_Monthly_Date_Pnd_Run_Mm = pnd_Run_Monthly_Date__R_Field_2.newFieldInGroup("pnd_Run_Monthly_Date_Pnd_Run_Mm", "#RUN-MM", FieldType.NUMERIC, 
            2);
        pnd_Run_Monthly_Date_Pnd_Run_Dd = pnd_Run_Monthly_Date__R_Field_1.newFieldInGroup("pnd_Run_Monthly_Date_Pnd_Run_Dd", "#RUN-DD", FieldType.NUMERIC, 
            2);

        pnd_Run_Monthly_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Run_Monthly_Date__R_Field_3", "REDEFINE", pnd_Run_Monthly_Date);
        pnd_Run_Monthly_Date_Pnd_Run_Monthly_Dte_A = pnd_Run_Monthly_Date__R_Field_3.newFieldInGroup("pnd_Run_Monthly_Date_Pnd_Run_Monthly_Dte_A", "#RUN-MONTHLY-DTE-A", 
            FieldType.STRING, 8);
        pnd_Run_Monthly_Dte_D = localVariables.newFieldInRecord("pnd_Run_Monthly_Dte_D", "#RUN-MONTHLY-DTE-D", FieldType.DATE);
        pnd_Entry_Dte_Tme = localVariables.newFieldInRecord("pnd_Entry_Dte_Tme", "#ENTRY-DTE-TME", FieldType.STRING, 15);

        pnd_Entry_Dte_Tme__R_Field_4 = localVariables.newGroupInRecord("pnd_Entry_Dte_Tme__R_Field_4", "REDEFINE", pnd_Entry_Dte_Tme);
        pnd_Entry_Dte_Tme_Pnd_Entry_Dte = pnd_Entry_Dte_Tme__R_Field_4.newFieldInGroup("pnd_Entry_Dte_Tme_Pnd_Entry_Dte", "#ENTRY-DTE", FieldType.STRING, 
            8);

        pnd_Entry_Dte_Tme__R_Field_5 = pnd_Entry_Dte_Tme__R_Field_4.newGroupInGroup("pnd_Entry_Dte_Tme__R_Field_5", "REDEFINE", pnd_Entry_Dte_Tme_Pnd_Entry_Dte);
        pnd_Entry_Dte_Tme_Pnd_Entry_Yyyy_Mm = pnd_Entry_Dte_Tme__R_Field_5.newFieldInGroup("pnd_Entry_Dte_Tme_Pnd_Entry_Yyyy_Mm", "#ENTRY-YYYY-MM", FieldType.NUMERIC, 
            6);
        pnd_Entry_Dte_Tme_Pnd_Entry_Dd = pnd_Entry_Dte_Tme__R_Field_5.newFieldInGroup("pnd_Entry_Dte_Tme_Pnd_Entry_Dd", "#ENTRY-DD", FieldType.NUMERIC, 
            2);

        pnd_Entry_Dte_Tme__R_Field_6 = pnd_Entry_Dte_Tme__R_Field_4.newGroupInGroup("pnd_Entry_Dte_Tme__R_Field_6", "REDEFINE", pnd_Entry_Dte_Tme_Pnd_Entry_Dte);
        pnd_Entry_Dte_Tme_Pnd_Entry_Yyyy = pnd_Entry_Dte_Tme__R_Field_6.newFieldInGroup("pnd_Entry_Dte_Tme_Pnd_Entry_Yyyy", "#ENTRY-YYYY", FieldType.STRING, 
            4);
        pnd_Entry_Dte_Tme_Pnd_Entry_Mm_Dd = pnd_Entry_Dte_Tme__R_Field_6.newFieldInGroup("pnd_Entry_Dte_Tme_Pnd_Entry_Mm_Dd", "#ENTRY-MM-DD", FieldType.STRING, 
            4);
        pnd_Entry_Dte_Tme_Pnd_Entry_Tme = pnd_Entry_Dte_Tme__R_Field_4.newFieldInGroup("pnd_Entry_Dte_Tme_Pnd_Entry_Tme", "#ENTRY-TME", FieldType.STRING, 
            7);
        pnd_Entry_Dte_A = localVariables.newFieldInRecord("pnd_Entry_Dte_A", "#ENTRY-DTE-A", FieldType.STRING, 8);
        pnd_Updte_Dte_Tme = localVariables.newFieldInRecord("pnd_Updte_Dte_Tme", "#UPDTE-DTE-TME", FieldType.STRING, 15);

        pnd_Updte_Dte_Tme__R_Field_7 = localVariables.newGroupInRecord("pnd_Updte_Dte_Tme__R_Field_7", "REDEFINE", pnd_Updte_Dte_Tme);
        pnd_Updte_Dte_Tme_Pnd_Updte_Dte = pnd_Updte_Dte_Tme__R_Field_7.newFieldInGroup("pnd_Updte_Dte_Tme_Pnd_Updte_Dte", "#UPDTE-DTE", FieldType.STRING, 
            8);

        pnd_Updte_Dte_Tme__R_Field_8 = pnd_Updte_Dte_Tme__R_Field_7.newGroupInGroup("pnd_Updte_Dte_Tme__R_Field_8", "REDEFINE", pnd_Updte_Dte_Tme_Pnd_Updte_Dte);
        pnd_Updte_Dte_Tme_Pnd_Updte_Yyyy_Mm = pnd_Updte_Dte_Tme__R_Field_8.newFieldInGroup("pnd_Updte_Dte_Tme_Pnd_Updte_Yyyy_Mm", "#UPDTE-YYYY-MM", FieldType.NUMERIC, 
            6);
        pnd_Updte_Dte_Tme_Pnd_Updte_Dd = pnd_Updte_Dte_Tme__R_Field_8.newFieldInGroup("pnd_Updte_Dte_Tme_Pnd_Updte_Dd", "#UPDTE-DD", FieldType.NUMERIC, 
            2);

        pnd_Updte_Dte_Tme__R_Field_9 = pnd_Updte_Dte_Tme__R_Field_7.newGroupInGroup("pnd_Updte_Dte_Tme__R_Field_9", "REDEFINE", pnd_Updte_Dte_Tme_Pnd_Updte_Dte);
        pnd_Updte_Dte_Tme_Pnd_Updte_Yyyy = pnd_Updte_Dte_Tme__R_Field_9.newFieldInGroup("pnd_Updte_Dte_Tme_Pnd_Updte_Yyyy", "#UPDTE-YYYY", FieldType.STRING, 
            4);
        pnd_Updte_Dte_Tme_Pnd_Updte_Mm_Dd = pnd_Updte_Dte_Tme__R_Field_9.newFieldInGroup("pnd_Updte_Dte_Tme_Pnd_Updte_Mm_Dd", "#UPDTE-MM-DD", FieldType.STRING, 
            4);
        pnd_Updte_Dte_Tme_Pnd_Updte_Tme = pnd_Updte_Dte_Tme__R_Field_7.newFieldInGroup("pnd_Updte_Dte_Tme_Pnd_Updte_Tme", "#UPDTE-TME", FieldType.STRING, 
            7);
        pnd_Updte_Dte_A = localVariables.newFieldInRecord("pnd_Updte_Dte_A", "#UPDTE-DTE-A", FieldType.STRING, 8);
        pnd_Dlte_Dte_Tme = localVariables.newFieldInRecord("pnd_Dlte_Dte_Tme", "#DLTE-DTE-TME", FieldType.STRING, 15);

        pnd_Dlte_Dte_Tme__R_Field_10 = localVariables.newGroupInRecord("pnd_Dlte_Dte_Tme__R_Field_10", "REDEFINE", pnd_Dlte_Dte_Tme);
        pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte = pnd_Dlte_Dte_Tme__R_Field_10.newFieldInGroup("pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte", "#DLTE-DTE", FieldType.STRING, 8);

        pnd_Dlte_Dte_Tme__R_Field_11 = pnd_Dlte_Dte_Tme__R_Field_10.newGroupInGroup("pnd_Dlte_Dte_Tme__R_Field_11", "REDEFINE", pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte);
        pnd_Dlte_Dte_Tme_Pnd_Dlte_Yyyy_Mm = pnd_Dlte_Dte_Tme__R_Field_11.newFieldInGroup("pnd_Dlte_Dte_Tme_Pnd_Dlte_Yyyy_Mm", "#DLTE-YYYY-MM", FieldType.NUMERIC, 
            6);
        pnd_Dlte_Dte_Tme_Pnd_Dlte_Dd = pnd_Dlte_Dte_Tme__R_Field_11.newFieldInGroup("pnd_Dlte_Dte_Tme_Pnd_Dlte_Dd", "#DLTE-DD", FieldType.NUMERIC, 2);

        pnd_Dlte_Dte_Tme__R_Field_12 = pnd_Dlte_Dte_Tme__R_Field_10.newGroupInGroup("pnd_Dlte_Dte_Tme__R_Field_12", "REDEFINE", pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte);
        pnd_Dlte_Dte_Tme_Pnd_Dlte_Yyyy = pnd_Dlte_Dte_Tme__R_Field_12.newFieldInGroup("pnd_Dlte_Dte_Tme_Pnd_Dlte_Yyyy", "#DLTE-YYYY", FieldType.STRING, 
            4);
        pnd_Dlte_Dte_Tme_Pnd_Dlte_Mm_Dd = pnd_Dlte_Dte_Tme__R_Field_12.newFieldInGroup("pnd_Dlte_Dte_Tme_Pnd_Dlte_Mm_Dd", "#DLTE-MM-DD", FieldType.STRING, 
            4);
        pnd_Dlte_Dte_Tme_Pnd_Dlte_Tme = pnd_Dlte_Dte_Tme__R_Field_10.newFieldInGroup("pnd_Dlte_Dte_Tme_Pnd_Dlte_Tme", "#DLTE-TME", FieldType.STRING, 7);
        pnd_Dlte_Dte_A = localVariables.newFieldInRecord("pnd_Dlte_Dte_A", "#DLTE-DTE-A", FieldType.STRING, 8);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_13 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_13", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Updt_Yyyymmdd = cwf_Support_Tbl__R_Field_13.newFieldInGroup("cwf_Support_Tbl_Tbl_Updt_Yyyymmdd", "TBL-UPDT-YYYYMMDD", FieldType.NUMERIC, 
            8);
        registerRecord(vw_cwf_Support_Tbl);

        vw_status_Tbl = new DataAccessProgramView(new NameInfo("vw_status_Tbl", "STATUS-TBL"), "CWF_ORG_STATUS_TBL", "CWF_ASSIGN_RULE");
        status_Tbl_Status_Cde = vw_status_Tbl.getRecord().newFieldInGroup("status_Tbl_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        status_Tbl_Status_Cde.setDdmHeader("STATUS/CODE");
        status_Tbl_Status_Unit_Cde = vw_status_Tbl.getRecord().newFieldInGroup("status_Tbl_Status_Unit_Cde", "STATUS-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "STATUS_UNIT_CDE");
        status_Tbl_Status_Unit_Cde.setDdmHeader("UNIT/CODE");
        status_Tbl_Status_Dscrptn_Txt = vw_status_Tbl.getRecord().newFieldInGroup("status_Tbl_Status_Dscrptn_Txt", "STATUS-DSCRPTN-TXT", FieldType.STRING, 
            25, RepeatingFieldStrategy.None, "STATUS_DSCRPTN_TXT");
        status_Tbl_Status_Dscrptn_Txt.setDdmHeader("STEP/DESCRIPTION");
        status_Tbl_Dlte_Dte_Tme = vw_status_Tbl.getRecord().newFieldInGroup("status_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "DLTE_DTE_TME");
        status_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        status_Tbl_Dlte_Oprtr_Cde = vw_status_Tbl.getRecord().newFieldInGroup("status_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DLTE_OPRTR_CDE");
        status_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPERATOR");
        status_Tbl_Entry_Dte_Tme = vw_status_Tbl.getRecord().newFieldInGroup("status_Tbl_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ENTRY_DTE_TME");
        status_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        status_Tbl_Entry_Oprtr_Cde = vw_status_Tbl.getRecord().newFieldInGroup("status_Tbl_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ENTRY_OPRTR_CDE");
        status_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        status_Tbl_Updte_Dte_Tme = vw_status_Tbl.getRecord().newFieldInGroup("status_Tbl_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "UPDTE_DTE_TME");
        status_Tbl_Updte_Oprtr_Cde = vw_status_Tbl.getRecord().newFieldInGroup("status_Tbl_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UPDTE_OPRTR_CDE");
        status_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        status_Tbl_Internet_Dscrptn_Txt = vw_status_Tbl.getRecord().newFieldInGroup("status_Tbl_Internet_Dscrptn_Txt", "INTERNET-DSCRPTN-TXT", FieldType.STRING, 
            65, RepeatingFieldStrategy.None, "INTERNET_DSCRPTN_TXT");
        registerRecord(vw_status_Tbl);

        vw_wpid_Tbl = new DataAccessProgramView(new NameInfo("vw_wpid_Tbl", "WPID-TBL"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        wpid_Tbl_Work_Prcss_Id = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        wpid_Tbl_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        wpid_Tbl_Work_Prcss_Long_Nme = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        wpid_Tbl_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        wpid_Tbl_Entry_Dte_Tme = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ENTRY_DTE_TME");
        wpid_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        wpid_Tbl_Entry_Oprtr_Cde = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ENTRY_OPRTR_CDE");
        wpid_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        wpid_Tbl_Updte_Dte_Tme = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "UPDTE_DTE_TME");
        wpid_Tbl_Updte_Oprtr_Cde = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UPDTE_OPRTR_CDE");
        wpid_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPER");
        wpid_Tbl_Dlte_Dte_Tme = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "DLTE_DTE_TME");
        wpid_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DT-TM");
        wpid_Tbl_Dlte_Oprtr_Cde = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DLTE_OPRTR_CDE");
        wpid_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        wpid_Tbl_Wpid_Intrnet_Nme = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Wpid_Intrnet_Nme", "WPID-INTRNET-NME", FieldType.STRING, 60, RepeatingFieldStrategy.None, 
            "WPID_INTRNET_NME");
        wpid_Tbl_Wpid_Intrnet_Nme.setDdmHeader("INTER/INTRANET WPID NAME");
        registerRecord(vw_wpid_Tbl);

        vw_empl_Tbl_Post = new DataAccessProgramView(new NameInfo("vw_empl_Tbl_Post", "EMPL-TBL-POST"), "CWF_ORG_EMPL_TBL_POST", "CWF_ASSIGN_RULE");
        empl_Tbl_Post_Empl_Unit_Cde = vw_empl_Tbl_Post.getRecord().newFieldInGroup("empl_Tbl_Post_Empl_Unit_Cde", "EMPL-UNIT-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "EMPL_UNIT_CDE");
        empl_Tbl_Post_Empl_Unit_Cde.setDdmHeader("UNIT/CODE");
        empl_Tbl_Post_Empl_Racf_Id = vw_empl_Tbl_Post.getRecord().newFieldInGroup("empl_Tbl_Post_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "EMPL_RACF_ID");
        empl_Tbl_Post_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF  ID");
        empl_Tbl_Post_Empl_Nme = vw_empl_Tbl_Post.getRecord().newFieldInGroup("empl_Tbl_Post_Empl_Nme", "EMPL-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "EMPL_NME");
        empl_Tbl_Post_Empl_Nme.setDdmHeader("EMPLOYEE NAME");

        empl_Tbl_Post__R_Field_14 = vw_empl_Tbl_Post.getRecord().newGroupInGroup("empl_Tbl_Post__R_Field_14", "REDEFINE", empl_Tbl_Post_Empl_Nme);
        empl_Tbl_Post_Empl_Nme_First = empl_Tbl_Post__R_Field_14.newFieldInGroup("empl_Tbl_Post_Empl_Nme_First", "EMPL-NME-FIRST", FieldType.STRING, 20);
        empl_Tbl_Post_Empl_Nme_Last = empl_Tbl_Post__R_Field_14.newFieldInGroup("empl_Tbl_Post_Empl_Nme_Last", "EMPL-NME-LAST", FieldType.STRING, 20);
        empl_Tbl_Post_Empl_Signatory = vw_empl_Tbl_Post.getRecord().newFieldInGroup("empl_Tbl_Post_Empl_Signatory", "EMPL-SIGNATORY", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "EMPL_SIGNATORY");
        empl_Tbl_Post_Empl_Signatory.setDdmHeader("SIGNATORY");
        empl_Tbl_Post_Dlte_Dte_Tme = vw_empl_Tbl_Post.getRecord().newFieldInGroup("empl_Tbl_Post_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "DLTE_DTE_TME");
        empl_Tbl_Post_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        empl_Tbl_Post_Dlte_Oprtr_Cde = vw_empl_Tbl_Post.getRecord().newFieldInGroup("empl_Tbl_Post_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        empl_Tbl_Post_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPERATOR");
        empl_Tbl_Post_Entry_Dte_Tme = vw_empl_Tbl_Post.getRecord().newFieldInGroup("empl_Tbl_Post_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ENTRY_DTE_TME");
        empl_Tbl_Post_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        empl_Tbl_Post_Entry_Oprtr_Cde = vw_empl_Tbl_Post.getRecord().newFieldInGroup("empl_Tbl_Post_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");
        empl_Tbl_Post_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        empl_Tbl_Post_Updte_Dte_Tme = vw_empl_Tbl_Post.getRecord().newFieldInGroup("empl_Tbl_Post_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "UPDTE_DTE_TME");
        empl_Tbl_Post_Updte_Oprtr_Cde = vw_empl_Tbl_Post.getRecord().newFieldInGroup("empl_Tbl_Post_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        empl_Tbl_Post_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_empl_Tbl_Post);

        vw_etp_Name = new DataAccessProgramView(new NameInfo("vw_etp_Name", "ETP-NAME"), "CWF_ORG_EMPL_TBL_POST", "CWF_ASSIGN_RULE");
        etp_Name_Empl_Racf_Id = vw_etp_Name.getRecord().newFieldInGroup("etp_Name_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "EMPL_RACF_ID");
        etp_Name_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF  ID");
        etp_Name_Empl_Nme = vw_etp_Name.getRecord().newFieldInGroup("etp_Name_Empl_Nme", "EMPL-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "EMPL_NME");
        etp_Name_Empl_Nme.setDdmHeader("EMPLOYEE NAME");

        etp_Name__R_Field_15 = vw_etp_Name.getRecord().newGroupInGroup("etp_Name__R_Field_15", "REDEFINE", etp_Name_Empl_Nme);
        etp_Name_Empl_Nme_First = etp_Name__R_Field_15.newFieldInGroup("etp_Name_Empl_Nme_First", "EMPL-NME-FIRST", FieldType.STRING, 20);
        etp_Name_Empl_Nme_Last = etp_Name__R_Field_15.newFieldInGroup("etp_Name_Empl_Nme_Last", "EMPL-NME-LAST", FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();
        vw_status_Tbl.reset();
        vw_wpid_Tbl.reset();
        vw_empl_Tbl_Post.reset();
        vw_etp_Name.reset();

        localVariables.reset();
        pnd_Vars_Pnd_First_Detail_Line.setInitialValue("N");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4450() throws Exception
    {
        super("Cwfb4450");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB4450", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 60 ES = OFF;//Natural: FORMAT ( 2 ) LS = 132 PS = 60 ES = OFF;//Natural: FORMAT ( 3 ) LS = 132 PS = 60 ES = OFF;//Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY FROM 'A CWF-RPRT-RUN-TBL'
        (
        "TBL",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", "A CWF-RPRT-RUN-TBL", WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        TBL:
        while (condition(vw_cwf_Support_Tbl.readNextRow("TBL")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals("CWF-RPRT-RUN-TBL")))                                                                                   //Natural: IF TBL-TABLE-NME NE 'CWF-RPRT-RUN-TBL'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Support_Tbl_Tbl_Key_Field.equals("CWFB4450")))                                                                                              //Natural: IF TBL-KEY-FIELD = 'CWFB4450'
            {
                pnd_Run_Monthly_Date.setValue(cwf_Support_Tbl_Tbl_Updt_Yyyymmdd);                                                                                         //Natural: MOVE TBL-UPDT-YYYYMMDD TO #RUN-MONTHLY-DATE
                pnd_Run_Monthly_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Run_Monthly_Date_Pnd_Run_Monthly_Dte_A);                                          //Natural: MOVE EDITED #RUN-MONTHLY-DTE-A TO #RUN-MONTHLY-DTE-D ( EM = YYYYMMDD )
                pnd_Vars_Pnd_Hold_Isn.setValue(vw_cwf_Support_Tbl.getAstISN("TBL"));                                                                                      //Natural: MOVE *ISN TO #HOLD-ISN
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_status_Tbl.startDatabaseRead                                                                                                                                   //Natural: READ STATUS-TBL BY STATUS-KEY
        (
        "READ_1",
        new Oc[] { new Oc("STATUS_KEY", "ASC") }
        );
        READ_1:
        while (condition(vw_status_Tbl.readNextRow("READ_1")))
        {
            pnd_Entry_Dte_Tme.setValueEdited(status_Tbl_Entry_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                             //Natural: MOVE EDITED STATUS-TBL.ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #ENTRY-DTE-TME
            pnd_Updte_Dte_Tme.setValueEdited(status_Tbl_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                             //Natural: MOVE EDITED STATUS-TBL.UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #UPDTE-DTE-TME
            pnd_Dlte_Dte_Tme.setValueEdited(status_Tbl_Dlte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                               //Natural: MOVE EDITED STATUS-TBL.DLTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #DLTE-DTE-TME
            if (condition(pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm.equals(pnd_Entry_Dte_Tme_Pnd_Entry_Yyyy_Mm) || pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm.equals(pnd_Updte_Dte_Tme_Pnd_Updte_Yyyy_Mm)  //Natural: IF #RUN-YYYY-MM = #ENTRY-YYYY-MM OR #RUN-YYYY-MM = #UPDTE-YYYY-MM OR #RUN-YYYY-MM = #DLTE-YYYY-MM
                || pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm.equals(pnd_Dlte_Dte_Tme_Pnd_Dlte_Yyyy_Mm)))
            {
                pnd_Vars_Pnd_Status_Tot_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #STATUS-TOT-CNT
                if (condition(status_Tbl_Status_Unit_Cde.notEquals(pnd_Vars_Pnd_Hold_Unit_Cde)))                                                                          //Natural: IF STATUS-UNIT-CDE NE #HOLD-UNIT-CDE
                {
                    pnd_Vars_Pnd_Hold_Unit_Cde.setValue(status_Tbl_Status_Unit_Cde);                                                                                      //Natural: MOVE STATUS-UNIT-CDE TO #HOLD-UNIT-CDE
                    pnd_Vars_Pnd_Seq_Cnt.reset();                                                                                                                         //Natural: RESET #SEQ-CNT
                    if (condition(pnd_Vars_Pnd_First_Detail_Line.equals("Y")))                                                                                            //Natural: IF #FIRST-DETAIL-LINE = 'Y'
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(6),"UNIT CODE:",new TabSetting(17),status_Tbl_Status_Unit_Cde,  //Natural: WRITE ( 1 ) /// 006T 'UNIT CODE:' 017T STATUS-UNIT-CDE //
                            NEWLINE,NEWLINE);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Vars_Pnd_First_Detail_Line.setValue("Y");                                                                                                     //Natural: MOVE 'Y' TO #FIRST-DETAIL-LINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Vars_Pnd_Seq_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #SEQ-CNT
                if (condition(status_Tbl_Entry_Oprtr_Cde.notEquals(" ")))                                                                                                 //Natural: IF STATUS-TBL.ENTRY-OPRTR-CDE NE ' '
                {
                    pnd_Vars_Pnd_Racf_Id.setValue(status_Tbl_Entry_Oprtr_Cde);                                                                                            //Natural: MOVE STATUS-TBL.ENTRY-OPRTR-CDE TO #RACF-ID
                                                                                                                                                                          //Natural: PERFORM READ-EMPL-TBL-NAME
                    sub_Read_Empl_Tbl_Name();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Vars_Pnd_Empl_Nme_1.setValue(pnd_Vars_Pnd_Hold_Empl_Nme);                                                                                         //Natural: MOVE #HOLD-EMPL-NME TO #EMPL-NME-1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Vars_Pnd_Empl_Nme_1.reset();                                                                                                                      //Natural: RESET #EMPL-NME-1
                }                                                                                                                                                         //Natural: END-IF
                if (condition(status_Tbl_Updte_Oprtr_Cde.equals(status_Tbl_Entry_Oprtr_Cde)))                                                                             //Natural: IF STATUS-TBL.UPDTE-OPRTR-CDE = STATUS-TBL.ENTRY-OPRTR-CDE
                {
                    pnd_Vars_Pnd_Empl_Nme_2.setValue(pnd_Vars_Pnd_Empl_Nme_1);                                                                                            //Natural: MOVE #EMPL-NME-1 TO #EMPL-NME-2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(status_Tbl_Updte_Oprtr_Cde.notEquals(" ")))                                                                                             //Natural: IF STATUS-TBL.UPDTE-OPRTR-CDE NE ' '
                    {
                        pnd_Vars_Pnd_Racf_Id.setValue(status_Tbl_Updte_Oprtr_Cde);                                                                                        //Natural: MOVE STATUS-TBL.UPDTE-OPRTR-CDE TO #RACF-ID
                                                                                                                                                                          //Natural: PERFORM READ-EMPL-TBL-NAME
                        sub_Read_Empl_Tbl_Name();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Vars_Pnd_Empl_Nme_2.setValue(pnd_Vars_Pnd_Hold_Empl_Nme);                                                                                     //Natural: MOVE #HOLD-EMPL-NME TO #EMPL-NME-2
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Vars_Pnd_Empl_Nme_2.reset();                                                                                                                  //Natural: RESET #EMPL-NME-2
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(status_Tbl_Dlte_Oprtr_Cde.equals(status_Tbl_Entry_Oprtr_Cde)))                                                                              //Natural: IF STATUS-TBL.DLTE-OPRTR-CDE = STATUS-TBL.ENTRY-OPRTR-CDE
                {
                    pnd_Vars_Pnd_Empl_Nme_3.setValue(pnd_Vars_Pnd_Empl_Nme_1);                                                                                            //Natural: MOVE #EMPL-NME-1 TO #EMPL-NME-3
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(status_Tbl_Dlte_Oprtr_Cde.equals(status_Tbl_Updte_Oprtr_Cde)))                                                                          //Natural: IF STATUS-TBL.DLTE-OPRTR-CDE = STATUS-TBL.UPDTE-OPRTR-CDE
                    {
                        pnd_Vars_Pnd_Empl_Nme_3.setValue(pnd_Vars_Pnd_Empl_Nme_2);                                                                                        //Natural: MOVE #EMPL-NME-2 TO #EMPL-NME-3
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(status_Tbl_Dlte_Oprtr_Cde.notEquals(" ")))                                                                                          //Natural: IF STATUS-TBL.DLTE-OPRTR-CDE NE ' '
                        {
                            pnd_Vars_Pnd_Racf_Id.setValue(status_Tbl_Dlte_Oprtr_Cde);                                                                                     //Natural: MOVE STATUS-TBL.DLTE-OPRTR-CDE TO #RACF-ID
                                                                                                                                                                          //Natural: PERFORM READ-EMPL-TBL-NAME
                            sub_Read_Empl_Tbl_Name();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Vars_Pnd_Empl_Nme_3.setValue(pnd_Vars_Pnd_Hold_Empl_Nme);                                                                                 //Natural: MOVE #HOLD-EMPL-NME TO #EMPL-NME-3
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Vars_Pnd_Empl_Nme_3.reset();                                                                                                              //Natural: RESET #EMPL-NME-3
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM TEST-DTE-FORMAT
                sub_Test_Dte_Format();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Vars_Pnd_Seq_Cnt, new ReportEditMask ("ZZZ9"),new TabSetting(7),status_Tbl_Status_Cde,new  //Natural: WRITE ( 1 ) / 001T #SEQ-CNT ( EM = ZZZ9 ) 007T STATUS-TBL.STATUS-CDE 014T STATUS-TBL.STATUS-DSCRPTN-TXT 041T STATUS-TBL.INTERNET-DSCRPTN-TXT
                    TabSetting(14),status_Tbl_Status_Dscrptn_Txt,new TabSetting(41),status_Tbl_Internet_Dscrptn_Txt);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(14),"Added By:",new TabSetting(24),pnd_Vars_Pnd_Empl_Nme_1,new TabSetting(50),"Updated By:",new //Natural: WRITE ( 1 ) 014T 'Added By:' 024T #EMPL-NME-1 050T 'Updated By:' 062T #EMPL-NME-2 088T 'Erased By:' 100T #EMPL-NME-3
                    TabSetting(62),pnd_Vars_Pnd_Empl_Nme_2,new TabSetting(88),"Erased By:",new TabSetting(100),pnd_Vars_Pnd_Empl_Nme_3);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(14),"Added On:",new TabSetting(24),pnd_Entry_Dte_A,new TabSetting(50),"Updated On:",new         //Natural: WRITE ( 1 ) 014T 'Added On:' 024T #ENTRY-DTE-A 050T 'Updated On:' 062T #UPDTE-DTE-A 088T 'Erased On:' 100T #DLTE-DTE-A
                    TabSetting(62),pnd_Updte_Dte_A,new TabSetting(88),"Erased On:",new TabSetting(100),pnd_Dlte_Dte_A);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Vars_Pnd_Seq_Cnt.reset();                                                                                                                                     //Natural: RESET #SEQ-CNT
        vw_wpid_Tbl.startDatabaseRead                                                                                                                                     //Natural: READ WPID-TBL BY WPID-UNIQ-KEY
        (
        "READ_2",
        new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") }
        );
        READ_2:
        while (condition(vw_wpid_Tbl.readNextRow("READ_2")))
        {
            pnd_Entry_Dte_Tme.setValueEdited(wpid_Tbl_Entry_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                               //Natural: MOVE EDITED WPID-TBL.ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #ENTRY-DTE-TME
            pnd_Updte_Dte_Tme.setValueEdited(wpid_Tbl_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                               //Natural: MOVE EDITED WPID-TBL.UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #UPDTE-DTE-TME
            pnd_Dlte_Dte_Tme.setValueEdited(wpid_Tbl_Dlte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                                 //Natural: MOVE EDITED WPID-TBL.DLTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #DLTE-DTE-TME
            if (condition(pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm.equals(pnd_Entry_Dte_Tme_Pnd_Entry_Yyyy_Mm) || pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm.equals(pnd_Updte_Dte_Tme_Pnd_Updte_Yyyy_Mm)  //Natural: IF #RUN-YYYY-MM = #ENTRY-YYYY-MM OR #RUN-YYYY-MM = #UPDTE-YYYY-MM OR #RUN-YYYY-MM = #DLTE-YYYY-MM
                || pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm.equals(pnd_Dlte_Dte_Tme_Pnd_Dlte_Yyyy_Mm)))
            {
                pnd_Vars_Pnd_Wpid_Tot_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #WPID-TOT-CNT
                pnd_Vars_Pnd_Seq_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #SEQ-CNT
                if (condition(wpid_Tbl_Entry_Oprtr_Cde.notEquals(" ")))                                                                                                   //Natural: IF WPID-TBL.ENTRY-OPRTR-CDE NE ' '
                {
                    pnd_Vars_Pnd_Racf_Id.setValue(wpid_Tbl_Entry_Oprtr_Cde);                                                                                              //Natural: MOVE WPID-TBL.ENTRY-OPRTR-CDE TO #RACF-ID
                                                                                                                                                                          //Natural: PERFORM READ-EMPL-TBL-NAME
                    sub_Read_Empl_Tbl_Name();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Vars_Pnd_Empl_Nme_1.setValue(pnd_Vars_Pnd_Hold_Empl_Nme);                                                                                         //Natural: MOVE #HOLD-EMPL-NME TO #EMPL-NME-1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Vars_Pnd_Empl_Nme_1.reset();                                                                                                                      //Natural: RESET #EMPL-NME-1
                }                                                                                                                                                         //Natural: END-IF
                if (condition(wpid_Tbl_Updte_Oprtr_Cde.equals(wpid_Tbl_Entry_Oprtr_Cde)))                                                                                 //Natural: IF WPID-TBL.UPDTE-OPRTR-CDE = WPID-TBL.ENTRY-OPRTR-CDE
                {
                    pnd_Vars_Pnd_Empl_Nme_2.setValue(pnd_Vars_Pnd_Empl_Nme_1);                                                                                            //Natural: MOVE #EMPL-NME-1 TO #EMPL-NME-2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(wpid_Tbl_Updte_Oprtr_Cde.notEquals(" ")))                                                                                               //Natural: IF WPID-TBL.UPDTE-OPRTR-CDE NE ' '
                    {
                        pnd_Vars_Pnd_Racf_Id.setValue(wpid_Tbl_Updte_Oprtr_Cde);                                                                                          //Natural: MOVE WPID-TBL.UPDTE-OPRTR-CDE TO #RACF-ID
                                                                                                                                                                          //Natural: PERFORM READ-EMPL-TBL-NAME
                        sub_Read_Empl_Tbl_Name();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Vars_Pnd_Empl_Nme_2.setValue(pnd_Vars_Pnd_Hold_Empl_Nme);                                                                                     //Natural: MOVE #HOLD-EMPL-NME TO #EMPL-NME-2
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Vars_Pnd_Empl_Nme_2.reset();                                                                                                                  //Natural: RESET #EMPL-NME-2
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(wpid_Tbl_Dlte_Oprtr_Cde.equals(wpid_Tbl_Entry_Oprtr_Cde)))                                                                                  //Natural: IF WPID-TBL.DLTE-OPRTR-CDE = WPID-TBL.ENTRY-OPRTR-CDE
                {
                    pnd_Vars_Pnd_Empl_Nme_3.setValue(pnd_Vars_Pnd_Empl_Nme_1);                                                                                            //Natural: MOVE #EMPL-NME-1 TO #EMPL-NME-3
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(wpid_Tbl_Dlte_Oprtr_Cde.equals(wpid_Tbl_Updte_Oprtr_Cde)))                                                                              //Natural: IF WPID-TBL.DLTE-OPRTR-CDE = WPID-TBL.UPDTE-OPRTR-CDE
                    {
                        pnd_Vars_Pnd_Empl_Nme_3.setValue(pnd_Vars_Pnd_Empl_Nme_2);                                                                                        //Natural: MOVE #EMPL-NME-2 TO #EMPL-NME-3
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(wpid_Tbl_Dlte_Oprtr_Cde.notEquals(" ")))                                                                                            //Natural: IF WPID-TBL.DLTE-OPRTR-CDE NE ' '
                        {
                            pnd_Vars_Pnd_Racf_Id.setValue(wpid_Tbl_Dlte_Oprtr_Cde);                                                                                       //Natural: MOVE WPID-TBL.DLTE-OPRTR-CDE TO #RACF-ID
                                                                                                                                                                          //Natural: PERFORM READ-EMPL-TBL-NAME
                            sub_Read_Empl_Tbl_Name();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_2"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_2"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Vars_Pnd_Empl_Nme_3.setValue(pnd_Vars_Pnd_Hold_Empl_Nme);                                                                                 //Natural: MOVE #HOLD-EMPL-NME TO #EMPL-NME-3
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Vars_Pnd_Empl_Nme_3.reset();                                                                                                              //Natural: RESET #EMPL-NME-3
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM TEST-DTE-FORMAT
                sub_Test_Dte_Format();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Vars_Pnd_Seq_Cnt, new ReportEditMask ("ZZZ9"),new TabSetting(7),wpid_Tbl_Work_Prcss_Id,new  //Natural: WRITE ( 2 ) / 001T #SEQ-CNT ( EM = ZZZ9 ) 007T WORK-PRCSS-ID 014T WORK-PRCSS-LONG-NME 061T WPID-INTRNET-NME
                    TabSetting(14),wpid_Tbl_Work_Prcss_Long_Nme,new TabSetting(61),wpid_Tbl_Wpid_Intrnet_Nme);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(14),"Added By:",new TabSetting(24),pnd_Vars_Pnd_Empl_Nme_1,new TabSetting(50),"Updated By:",new //Natural: WRITE ( 2 ) 014T 'Added By:' 024T #EMPL-NME-1 050T 'Updated By:' 062T #EMPL-NME-2 088T 'Erased By:' 100T #EMPL-NME-3
                    TabSetting(62),pnd_Vars_Pnd_Empl_Nme_2,new TabSetting(88),"Erased By:",new TabSetting(100),pnd_Vars_Pnd_Empl_Nme_3);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(14),"Added On:",new TabSetting(24),pnd_Entry_Dte_A,new TabSetting(50),"Updated On:",new         //Natural: WRITE ( 2 ) 014T 'Added On:' 024T #ENTRY-DTE-A 050T 'Updated On:' 062T #UPDTE-DTE-A 088T 'Erased On:' 100T #DLTE-DTE-A
                    TabSetting(62),pnd_Updte_Dte_A,new TabSetting(88),"Erased On:",new TabSetting(100),pnd_Dlte_Dte_A);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Vars_Pnd_First_Detail_Line.setValue("N");                                                                                                                     //Natural: MOVE 'N' TO #FIRST-DETAIL-LINE
        pnd_Vars_Pnd_Seq_Cnt.reset();                                                                                                                                     //Natural: RESET #SEQ-CNT
        vw_empl_Tbl_Post.startDatabaseRead                                                                                                                                //Natural: READ EMPL-TBL-POST BY UNIT-EMPL-RACF-ID-KEY
        (
        "READ_3",
        new Oc[] { new Oc("UNIT_EMPL_RACF_ID_KEY", "ASC") }
        );
        READ_3:
        while (condition(vw_empl_Tbl_Post.readNextRow("READ_3")))
        {
            pnd_Entry_Dte_Tme.setValueEdited(empl_Tbl_Post_Entry_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                          //Natural: MOVE EDITED EMPL-TBL-POST.ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #ENTRY-DTE-TME
            pnd_Updte_Dte_Tme.setValueEdited(empl_Tbl_Post_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                          //Natural: MOVE EDITED EMPL-TBL-POST.UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #UPDTE-DTE-TME
            pnd_Dlte_Dte_Tme.setValueEdited(empl_Tbl_Post_Dlte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                            //Natural: MOVE EDITED EMPL-TBL-POST.DLTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #DLTE-DTE-TME
            if (condition(pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm.equals(pnd_Entry_Dte_Tme_Pnd_Entry_Yyyy_Mm) || pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm.equals(pnd_Updte_Dte_Tme_Pnd_Updte_Yyyy_Mm)  //Natural: IF #RUN-YYYY-MM = #ENTRY-YYYY-MM OR #RUN-YYYY-MM = #UPDTE-YYYY-MM OR #RUN-YYYY-MM = #DLTE-YYYY-MM
                || pnd_Run_Monthly_Date_Pnd_Run_Yyyy_Mm.equals(pnd_Dlte_Dte_Tme_Pnd_Dlte_Yyyy_Mm)))
            {
                pnd_Vars_Pnd_Empl_Tot_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #EMPL-TOT-CNT
                if (condition(empl_Tbl_Post_Empl_Unit_Cde.notEquals(pnd_Vars_Pnd_Hold_Unit_Cde)))                                                                         //Natural: IF EMPL-UNIT-CDE NE #HOLD-UNIT-CDE
                {
                    pnd_Vars_Pnd_Hold_Unit_Cde.setValue(empl_Tbl_Post_Empl_Unit_Cde);                                                                                     //Natural: MOVE EMPL-UNIT-CDE TO #HOLD-UNIT-CDE
                    pnd_Vars_Pnd_Seq_Cnt.reset();                                                                                                                         //Natural: RESET #SEQ-CNT
                    if (condition(pnd_Vars_Pnd_First_Detail_Line.equals("Y")))                                                                                            //Natural: IF #FIRST-DETAIL-LINE = 'Y'
                    {
                        getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(7),"UNIT CODE:",new TabSetting(18),empl_Tbl_Post_Empl_Unit_Cde, //Natural: WRITE ( 3 ) /// 007T 'UNIT CODE:' 018T EMPL-UNIT-CDE //
                            NEWLINE,NEWLINE);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Vars_Pnd_First_Detail_Line.setValue("Y");                                                                                                     //Natural: MOVE 'Y' TO #FIRST-DETAIL-LINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Vars_Pnd_Seq_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #SEQ-CNT
                if (condition(empl_Tbl_Post_Entry_Oprtr_Cde.notEquals(" ")))                                                                                              //Natural: IF EMPL-TBL-POST.ENTRY-OPRTR-CDE NE ' '
                {
                    pnd_Vars_Pnd_Racf_Id.setValue(empl_Tbl_Post_Entry_Oprtr_Cde);                                                                                         //Natural: MOVE EMPL-TBL-POST.ENTRY-OPRTR-CDE TO #RACF-ID
                                                                                                                                                                          //Natural: PERFORM READ-EMPL-TBL-NAME
                    sub_Read_Empl_Tbl_Name();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Vars_Pnd_Empl_Nme_1.setValue(pnd_Vars_Pnd_Hold_Empl_Nme);                                                                                         //Natural: MOVE #HOLD-EMPL-NME TO #EMPL-NME-1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Vars_Pnd_Empl_Nme_1.reset();                                                                                                                      //Natural: RESET #EMPL-NME-1
                }                                                                                                                                                         //Natural: END-IF
                if (condition(empl_Tbl_Post_Updte_Oprtr_Cde.equals(empl_Tbl_Post_Entry_Oprtr_Cde)))                                                                       //Natural: IF EMPL-TBL-POST.UPDTE-OPRTR-CDE = EMPL-TBL-POST.ENTRY-OPRTR-CDE
                {
                    pnd_Vars_Pnd_Empl_Nme_2.setValue(pnd_Vars_Pnd_Empl_Nme_1);                                                                                            //Natural: MOVE #EMPL-NME-1 TO #EMPL-NME-2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(empl_Tbl_Post_Updte_Oprtr_Cde.notEquals(" ")))                                                                                          //Natural: IF EMPL-TBL-POST.UPDTE-OPRTR-CDE NE ' '
                    {
                        pnd_Vars_Pnd_Racf_Id.setValue(empl_Tbl_Post_Updte_Oprtr_Cde);                                                                                     //Natural: MOVE EMPL-TBL-POST.UPDTE-OPRTR-CDE TO #RACF-ID
                                                                                                                                                                          //Natural: PERFORM READ-EMPL-TBL-NAME
                        sub_Read_Empl_Tbl_Name();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Vars_Pnd_Empl_Nme_2.setValue(pnd_Vars_Pnd_Hold_Empl_Nme);                                                                                     //Natural: MOVE #HOLD-EMPL-NME TO #EMPL-NME-2
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Vars_Pnd_Empl_Nme_2.reset();                                                                                                                  //Natural: RESET #EMPL-NME-2
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(empl_Tbl_Post_Dlte_Oprtr_Cde.equals(empl_Tbl_Post_Entry_Oprtr_Cde)))                                                                        //Natural: IF EMPL-TBL-POST.DLTE-OPRTR-CDE = EMPL-TBL-POST.ENTRY-OPRTR-CDE
                {
                    pnd_Vars_Pnd_Empl_Nme_3.setValue(pnd_Vars_Pnd_Empl_Nme_1);                                                                                            //Natural: MOVE #EMPL-NME-1 TO #EMPL-NME-3
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(empl_Tbl_Post_Dlte_Oprtr_Cde.equals(empl_Tbl_Post_Updte_Oprtr_Cde)))                                                                    //Natural: IF EMPL-TBL-POST.DLTE-OPRTR-CDE = EMPL-TBL-POST.UPDTE-OPRTR-CDE
                    {
                        pnd_Vars_Pnd_Empl_Nme_3.setValue(pnd_Vars_Pnd_Empl_Nme_2);                                                                                        //Natural: MOVE #EMPL-NME-2 TO #EMPL-NME-3
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(empl_Tbl_Post_Dlte_Oprtr_Cde.notEquals(" ")))                                                                                       //Natural: IF EMPL-TBL-POST.DLTE-OPRTR-CDE NE ' '
                        {
                            pnd_Vars_Pnd_Racf_Id.setValue(empl_Tbl_Post_Dlte_Oprtr_Cde);                                                                                  //Natural: MOVE EMPL-TBL-POST.DLTE-OPRTR-CDE TO #RACF-ID
                                                                                                                                                                          //Natural: PERFORM READ-EMPL-TBL-NAME
                            sub_Read_Empl_Tbl_Name();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_3"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_3"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Vars_Pnd_Empl_Nme_3.setValue(pnd_Vars_Pnd_Hold_Empl_Nme);                                                                                 //Natural: MOVE #HOLD-EMPL-NME TO #EMPL-NME-3
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Vars_Pnd_Empl_Nme_3.reset();                                                                                                              //Natural: RESET #EMPL-NME-3
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Vars_Pnd_Hold_Empl_Nme.setValue(DbsUtil.compress(empl_Tbl_Post_Empl_Nme_First, empl_Tbl_Post_Empl_Nme_Last));                                         //Natural: COMPRESS EMPL-TBL-POST.EMPL-NME-FIRST EMPL-TBL-POST.EMPL-NME-LAST INTO #HOLD-EMPL-NME
                                                                                                                                                                          //Natural: PERFORM TEST-DTE-FORMAT
                sub_Test_Dte_Format();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(3, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Vars_Pnd_Seq_Cnt, new ReportEditMask ("ZZZ9"),new TabSetting(7),empl_Tbl_Post_Empl_Racf_Id,new  //Natural: WRITE ( 3 ) / 001T #SEQ-CNT ( EM = ZZZ9 ) 007T EMPL-RACF-ID 017T #HOLD-EMPL-NME 060T EMPL-SIGNATORY
                    TabSetting(17),pnd_Vars_Pnd_Hold_Empl_Nme,new TabSetting(60),empl_Tbl_Post_Empl_Signatory);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(3, ReportOption.NOTITLE,new TabSetting(17),"Added By:",new TabSetting(27),pnd_Vars_Pnd_Empl_Nme_1,new TabSetting(53),"Updated By:",new //Natural: WRITE ( 3 ) 017T 'Added By:' 027T #EMPL-NME-1 053T 'Updated By:' 065T #EMPL-NME-2 091T 'Erased By:' 103T #EMPL-NME-3
                    TabSetting(65),pnd_Vars_Pnd_Empl_Nme_2,new TabSetting(91),"Erased By:",new TabSetting(103),pnd_Vars_Pnd_Empl_Nme_3);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(3, ReportOption.NOTITLE,new TabSetting(17),"Added On:",new TabSetting(27),pnd_Entry_Dte_A,new TabSetting(53),"Updated On:",new         //Natural: WRITE ( 3 ) 017T 'Added On:' 027T #ENTRY-DTE-A 053T 'Updated On:' 065T #UPDTE-DTE-A 091T 'Erased On:' 103T #DLTE-DTE-A
                    TabSetting(65),pnd_Updte_Dte_A,new TabSetting(91),"Erased On:",new TabSetting(103),pnd_Dlte_Dte_A);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-EMPL-TBL-NAME
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TEST-DTE-FORMAT
        if (condition(pnd_Run_Monthly_Date_Pnd_Run_Mm.equals(12)))                                                                                                        //Natural: IF #RUN-MM = 12
        {
            pnd_Run_Monthly_Date_Pnd_Run_Mm.setValue(1);                                                                                                                  //Natural: MOVE 1 TO #RUN-MM
            pnd_Run_Monthly_Date_Pnd_Run_Yyyy.nadd(1);                                                                                                                    //Natural: ADD 1 TO #RUN-YYYY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Run_Monthly_Date_Pnd_Run_Mm.nadd(1);                                                                                                                      //Natural: ADD 1 TO #RUN-MM
        }                                                                                                                                                                 //Natural: END-IF
        GET1:                                                                                                                                                             //Natural: GET CWF-SUPPORT-TBL #HOLD-ISN
        vw_cwf_Support_Tbl.readByID(pnd_Vars_Pnd_Hold_Isn.getLong(), "GET1");
        cwf_Support_Tbl_Tbl_Updt_Yyyymmdd.setValue(pnd_Run_Monthly_Date);                                                                                                 //Natural: MOVE #RUN-MONTHLY-DATE TO TBL-UPDT-YYYYMMDD
        vw_cwf_Support_Tbl.updateDBRow("GET1");                                                                                                                           //Natural: UPDATE ( GET1. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"Total records printed from STATUS file",pnd_Vars_Pnd_Status_Tot_Cnt);                         //Natural: WRITE ( 1 ) /// 'Total records printed from STATUS file' #STATUS-TOT-CNT
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"Total records printed from WPID file",pnd_Vars_Pnd_Wpid_Tot_Cnt);                             //Natural: WRITE ( 2 ) /// 'Total records printed from WPID file' #WPID-TOT-CNT
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"Total records printed from EMPL file",pnd_Vars_Pnd_Empl_Tot_Cnt);                             //Natural: WRITE ( 3 ) /// 'Total records printed from EMPL file' #EMPL-TOT-CNT
        if (Global.isEscape()) return;
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Read_Empl_Tbl_Name() throws Exception                                                                                                                //Natural: READ-EMPL-TBL-NAME
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Vars_Pnd_Hold_Empl_Nme.reset();                                                                                                                               //Natural: RESET #HOLD-EMPL-NME
        vw_etp_Name.startDatabaseRead                                                                                                                                     //Natural: READ ( 1 ) ETP-NAME BY EMPL-RACF-ID-KEY FROM #RACF-ID
        (
        "READ_4",
        new Wc[] { new Wc("EMPL_RACF_ID_KEY", ">=", pnd_Vars_Pnd_Racf_Id, WcType.BY) },
        new Oc[] { new Oc("EMPL_RACF_ID_KEY", "ASC") },
        1
        );
        READ_4:
        while (condition(vw_etp_Name.readNextRow("READ_4")))
        {
            pnd_Vars_Pnd_Hold_Empl_Nme.setValue(DbsUtil.compress(etp_Name_Empl_Nme_First, etp_Name_Empl_Nme_Last));                                                       //Natural: COMPRESS ETP-NAME.EMPL-NME-FIRST ETP-NAME.EMPL-NME-LAST INTO #HOLD-EMPL-NME
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Test_Dte_Format() throws Exception                                                                                                                   //Natural: TEST-DTE-FORMAT
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Entry_Dte_Tme_Pnd_Entry_Yyyy.equals("0000")))                                                                                                   //Natural: IF #ENTRY-YYYY = '0000'
        {
            pnd_Entry_Dte_A.reset();                                                                                                                                      //Natural: RESET #ENTRY-DTE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Entry_Dte_A.setValue(pnd_Entry_Dte_Tme_Pnd_Entry_Dte);                                                                                                    //Natural: MOVE #ENTRY-DTE TO #ENTRY-DTE-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Updte_Dte_Tme_Pnd_Updte_Yyyy.equals("0000")))                                                                                                   //Natural: IF #UPDTE-YYYY = '0000'
        {
            pnd_Updte_Dte_A.reset();                                                                                                                                      //Natural: RESET #UPDTE-DTE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Updte_Dte_A.setValue(pnd_Updte_Dte_Tme_Pnd_Updte_Dte);                                                                                                    //Natural: MOVE #UPDTE-DTE TO #UPDTE-DTE-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Dlte_Dte_Tme_Pnd_Dlte_Yyyy.equals("0000")))                                                                                                     //Natural: IF #DLTE-YYYY = '0000'
        {
            pnd_Dlte_Dte_A.reset();                                                                                                                                       //Natural: RESET #DLTE-DTE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Dlte_Dte_A.setValue(pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte);                                                                                                       //Natural: MOVE #DLTE-DTE TO #DLTE-DTE-A
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"PROGRAM",Global.getPROGRAM(),new TabSetting(20),"MIT Administration Report - Additions & Changes",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 'PROGRAM' *PROGRAM 020T 'MIT Administration Report - Additions & Changes' 068T 'from the STATUS TABLE file' 115T 'TIME:' *TIME / 001T 'DATE:' *DATX 039T 'For the month of' #RUN-MONTHLY-DTE-D ( EM = LLLLLLLLL', 'YYYY ) 115T 'PAGE:' *PAGE-NUMBER ( 1 ) // 006T 'ST CD' 014T 'DESCRIPTION' 041T 'INTERNET-DESC' //// 006T 'UNIT CODE:' 017T STATUS-UNIT-CDE ///
                        TabSetting(68),"from the STATUS TABLE file",new TabSetting(115),"TIME:",Global.getTIME(),NEWLINE,new TabSetting(1),"DATE:",Global.getDATX(),new 
                        TabSetting(39),"For the month of",pnd_Run_Monthly_Dte_D, new ReportEditMask ("LLLLLLLLL', 'YYYY"),new TabSetting(115),"PAGE:",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new 
                        TabSetting(6),"ST CD",new TabSetting(14),"DESCRIPTION",new TabSetting(41),"INTERNET-DESC",NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(6),"UNIT CODE:",new 
                        TabSetting(17),status_Tbl_Status_Unit_Cde,NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,"PROGRAM",Global.getPROGRAM(),new TabSetting(20),"MIT Administration Report - Additions & Changes",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR 'PROGRAM' *PROGRAM 020T 'MIT Administration Report - Additions & Changes' 068T 'from the WPID TABLE file' 115T 'TIME:' *TIME / 001T 'DATE:' *DATX 039T 'For the month of' #RUN-MONTHLY-DTE-D ( EM = LLLLLLLLL', 'YYYY ) 115T 'PAGE:' *PAGE-NUMBER ( 2 ) // 007T 'WPID' 014T 'LONG NAME' 061T 'INTERNET-DESC' ///
                        TabSetting(68),"from the WPID TABLE file",new TabSetting(115),"TIME:",Global.getTIME(),NEWLINE,new TabSetting(1),"DATE:",Global.getDATX(),new 
                        TabSetting(39),"For the month of",pnd_Run_Monthly_Dte_D, new ReportEditMask ("LLLLLLLLL', 'YYYY"),new TabSetting(115),"PAGE:",getReports().getPageNumberDbs(2),NEWLINE,NEWLINE,new 
                        TabSetting(7),"WPID",new TabSetting(14),"LONG NAME",new TabSetting(61),"INTERNET-DESC",NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,"PROGRAM",Global.getPROGRAM(),new TabSetting(20),"MIT Administration Report - Additions & Changes",new  //Natural: WRITE ( 3 ) NOTITLE NOHDR 'PROGRAM' *PROGRAM 020T 'MIT Administration Report - Additions & Changes' 068T 'from the EMPLOYEE TABLE file' 115T 'TIME:' *TIME / 001T 'DATE:' *DATX 039T 'For the month of' #RUN-MONTHLY-DTE-D ( EM = LLLLLLLLL', 'YYYY ) 115T 'PAGE:' *PAGE-NUMBER ( 3 ) // 007T 'RACF ID' 017T 'EMPL NAME' 060T 'SIGNATURE' //// 007T 'UNIT CODE:' 018T EMPL-UNIT-CDE ///
                        TabSetting(68),"from the EMPLOYEE TABLE file",new TabSetting(115),"TIME:",Global.getTIME(),NEWLINE,new TabSetting(1),"DATE:",Global.getDATX(),new 
                        TabSetting(39),"For the month of",pnd_Run_Monthly_Dte_D, new ReportEditMask ("LLLLLLLLL', 'YYYY"),new TabSetting(115),"PAGE:",getReports().getPageNumberDbs(3),NEWLINE,NEWLINE,new 
                        TabSetting(7),"RACF ID",new TabSetting(17),"EMPL NAME",new TabSetting(60),"SIGNATURE",NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(7),"UNIT CODE:",new 
                        TabSetting(18),empl_Tbl_Post_Empl_Unit_Cde,NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "Natural error",Global.getERROR_NR(),"on line",Global.getERROR_LINE(),"of",Global.getPROGRAM());                                            //Natural: WRITE 'Natural error' *ERROR-NR 'on line' *ERROR-LINE 'of' *PROGRAM
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60 ES=OFF");
        Global.format(2, "LS=132 PS=60 ES=OFF");
        Global.format(3, "LS=132 PS=60 ES=OFF");
    }
}
