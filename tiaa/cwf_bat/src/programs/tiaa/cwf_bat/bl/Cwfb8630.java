/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:46:11 PM
**        * FROM NATURAL PROGRAM : Cwfb8630
************************************************************
**        * FILE NAME            : Cwfb8630.java
**        * CLASS NAME           : Cwfb8630
**        * INSTANCE NAME        : Cwfb8630
************************************************************
************************************************************************
* SYSTEM   : CORPORATE WORKFLOW
* PROGRAM  : CWFB8630
* TITLE    : PAYMENT OPERATIONS STAFF ACTIVITY EXTRACT
* FUNCTION : READ CWF-MASTER-INDEX-VIEW (045/181); EXTRACT OPERATOR
*          : INFORMATION FOR A WEEK OR MONTH; CREATE A WORKFILE.
* HISTORY  : CLONED FROM CWFB8629, MAY 2012 - VINODH KUMAR RAMACHANDRAN
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8630 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_mit;
    private DbsField mit_Pin_Nbr;
    private DbsField mit_Rqst_Log_Dte_Tme;

    private DbsGroup mit__R_Field_1;
    private DbsField mit_Rqst_Log_Dte;
    private DbsField mit_Rqst_Log_Tme;
    private DbsField mit_Rqst_Log_Invrt_Dte_Tme;
    private DbsField mit_Work_Prcss_Id;
    private DbsField mit_Last_Chnge_Dte_Tme;
    private DbsField mit_Tiaa_Rcvd_Dte_Tme;
    private DbsField mit_Crprte_Due_Dte_Tme;
    private DbsField mit_Unit_Clock_Start_Dte_Tme;
    private DbsField mit_Unit_Clock_End_Dte_Tme;
    private DbsField mit_Empl_Clock_Start_Dte_Tme;
    private DbsField mit_Empl_Clock_End_Dte_Tme;
    private DbsField mit_Step_Clock_Start_Dte_Tme;
    private DbsField mit_Step_Clock_End_Dte_Tme;
    private DbsField mit_Empl_Elpsd_Clndr_Days_Tme;
    private DbsField mit_Empl_Elpsd_Bsnss_Days_Tme;
    private DbsField mit_Unit_Elpsd_Clndr_Days_Tme;
    private DbsField mit_Unit_Elpsd_Bsnss_Days_Tme;
    private DbsField mit_Assgn_Dte_Tme;

    private DataAccessProgramView vw_mit_Hist;
    private DbsField mit_Hist_Pin_Nbr;
    private DbsField mit_Hist_Empl_Oprtr_Cde;
    private DbsField mit_Hist_Rqst_Log_Dte_Tme;

    private DbsGroup mit_Hist__R_Field_2;
    private DbsField mit_Hist_Rqst_Log_Dte;
    private DbsField mit_Hist_Rqst_Log_Tme;
    private DbsField mit_Hist_Rqst_Log_Invrt_Dte_Tme;
    private DbsField mit_Hist_Work_Prcss_Id;
    private DbsField mit_Hist_Orgnl_Unit_Cde;
    private DbsField mit_Hist_Unit_Cde;
    private DbsField mit_Hist_Status_Cde;
    private DbsField mit_Hist_Last_Chnge_Dte_Tme;
    private DbsField mit_Hist_Last_Chnge_Oprtr_Cde;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_3;
    private DbsField cwf_Support_Tbl_Tbl_Unit_Key;

    private DbsGroup cwf_Support_Tbl__R_Field_4;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_User_Key;

    private DbsGroup cwf_Support_Tbl__R_Field_5;
    private DbsField cwf_Support_Tbl_Tbl_Wpid_Key;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_6;
    private DbsField cwf_Support_Tbl_Tbl_Unit;

    private DbsGroup cwf_Support_Tbl__R_Field_7;
    private DbsField cwf_Support_Tbl_Tbl_User;

    private DbsGroup cwf_Support_Tbl__R_Field_8;
    private DbsField cwf_Support_Tbl_Tbl_Wpid;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_9;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_I_Unit;
    private DbsField pnd_I_User;
    private DbsField pnd_I_Wpid;

    private DbsGroup pnd_Units;
    private DbsField pnd_Units_Pnd_T_Unit;

    private DbsGroup pnd_Operators;
    private DbsField pnd_Operators_Pnd_T_Oprtr;

    private DbsGroup pnd_Wpids;
    private DbsField pnd_Wpids_Pnd_T_Wpid;
    private DbsField pnd_Status_Date_Key;

    private DbsGroup pnd_Status_Date_Key__R_Field_10;
    private DbsField pnd_Status_Date_Key_Pnd_K_Status;
    private DbsField pnd_Status_Date_Key_Pnd_K_Invert_Upd_Dte;

    private DbsGroup pnd_Status_Date_Key__R_Field_11;
    private DbsField pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd;
    private DbsField pnd_Status_Date_Key_Pnd_K_Invert_Hhiisst;
    private DbsField pnd_Status_Date_Key__Filler1;
    private DbsField pnd_History_Key;

    private DbsGroup pnd_History_Key__R_Field_12;
    private DbsField pnd_History_Key_Pnd_K_Log_Dte_Tme;
    private DbsField pnd_History_Key_Pnd_K_Last_Chnge_Invrt_D_T;
    private DbsField pnd_Frequency;
    private DbsField pnd_Work_File;

    private DbsGroup pnd_Work_File__R_Field_13;
    private DbsField pnd_Work_File_Pnd_Wf_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_Wpid;
    private DbsField pnd_Work_File_Pnd_Wf_Pin;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_14;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Tme;
    private DbsField pnd_Work_File__Filler2;
    private DbsField pnd_Work_File_Pnd_Wf_Due_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_15;
    private DbsField pnd_Work_File_Pnd_Wf_Due_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Due_Tme;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_16;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Tme;
    private DbsField pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Stts;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_17;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Tme;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_18;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Tme;

    private DbsGroup pnd_Work_File__R_Field_19;
    private DbsField pnd_Work_File__Filler3;
    private DbsField pnd_Work_File_Pnd_Wf_Run;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_End_Dte;
    private DbsField pnd_Start_Datd;
    private DbsField pnd_Start_Yyyymmdd;

    private DbsGroup pnd_Start_Yyyymmdd__R_Field_20;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Date;

    private DbsGroup pnd_Start_Yyyymmdd__R_Field_21;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Yyyymm;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Dd;
    private DbsField pnd_End_Datd;
    private DbsField pnd_End_Yyyymmdd;

    private DbsGroup pnd_End_Yyyymmdd__R_Field_22;
    private DbsField pnd_End_Yyyymmdd_Pnd_End_Date;

    private DbsGroup pnd_End_Yyyymmdd__R_Field_23;
    private DbsField pnd_End_Yyyymmdd_Pnd_End_Yyyymm;
    private DbsField pnd_End_Yyyymmdd_Pnd_End_Dd;
    private DbsField pnd_Last_Chnge_Dte_Tme;

    private DbsGroup pnd_Last_Chnge_Dte_Tme__R_Field_24;
    private DbsField pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Yyyymmdd;
    private DbsField pnd_Log_Dte_Tme;
    private DbsField pnd_Write_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_mit = new DataAccessProgramView(new NameInfo("vw_mit", "MIT"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        mit_Pin_Nbr = vw_mit.getRecord().newFieldInGroup("mit_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        mit_Pin_Nbr.setDdmHeader("PIN");
        mit_Rqst_Log_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        mit_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        mit__R_Field_1 = vw_mit.getRecord().newGroupInGroup("mit__R_Field_1", "REDEFINE", mit_Rqst_Log_Dte_Tme);
        mit_Rqst_Log_Dte = mit__R_Field_1.newFieldInGroup("mit_Rqst_Log_Dte", "RQST-LOG-DTE", FieldType.STRING, 8);
        mit_Rqst_Log_Tme = mit__R_Field_1.newFieldInGroup("mit_Rqst_Log_Tme", "RQST-LOG-TME", FieldType.STRING, 7);
        mit_Rqst_Log_Invrt_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Rqst_Log_Invrt_Dte_Tme", "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        mit_Work_Prcss_Id = vw_mit.getRecord().newFieldInGroup("mit_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        mit_Work_Prcss_Id.setDdmHeader("WORK/ID");
        mit_Last_Chnge_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, 
            "LAST_CHNGE_DTE_TME");
        mit_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        mit_Tiaa_Rcvd_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE_TME");
        mit_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        mit_Crprte_Due_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CRPRTE_DUE_DTE_TME");
        mit_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        mit_Unit_Clock_Start_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Unit_Clock_Start_Dte_Tme", "UNIT-CLOCK-START-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        mit_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        mit_Unit_Clock_End_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Unit_Clock_End_Dte_Tme", "UNIT-CLOCK-END-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        mit_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        mit_Empl_Clock_Start_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Empl_Clock_Start_Dte_Tme", "EMPL-CLOCK-START-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        mit_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        mit_Empl_Clock_End_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Empl_Clock_End_Dte_Tme", "EMPL-CLOCK-END-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        mit_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        mit_Step_Clock_Start_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Step_Clock_Start_Dte_Tme", "STEP-CLOCK-START-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        mit_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        mit_Step_Clock_End_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Step_Clock_End_Dte_Tme", "STEP-CLOCK-END-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        mit_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        mit_Empl_Elpsd_Clndr_Days_Tme = vw_mit.getRecord().newFieldInGroup("mit_Empl_Elpsd_Clndr_Days_Tme", "EMPL-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "EMPL_ELPSD_CLNDR_DAYS_TME");
        mit_Empl_Elpsd_Clndr_Days_Tme.setDdmHeader("ASSOCIATE WAIT TIME");
        mit_Empl_Elpsd_Bsnss_Days_Tme = vw_mit.getRecord().newFieldInGroup("mit_Empl_Elpsd_Bsnss_Days_Tme", "EMPL-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "EMPL_ELPSD_BSNSS_DAYS_TME");
        mit_Empl_Elpsd_Bsnss_Days_Tme.setDdmHeader("ASSOCIATE PROCESS TIME");
        mit_Unit_Elpsd_Clndr_Days_Tme = vw_mit.getRecord().newFieldInGroup("mit_Unit_Elpsd_Clndr_Days_Tme", "UNIT-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "UNIT_ELPSD_CLNDR_DAYS_TME");
        mit_Unit_Elpsd_Clndr_Days_Tme.setDdmHeader("UNIT WAIT TIME");
        mit_Unit_Elpsd_Bsnss_Days_Tme = vw_mit.getRecord().newFieldInGroup("mit_Unit_Elpsd_Bsnss_Days_Tme", "UNIT-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "UNIT_ELPSD_BSNSS_DAYS_TME");
        mit_Unit_Elpsd_Bsnss_Days_Tme.setDdmHeader("UNIT PROCESS TIME");
        mit_Assgn_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Assgn_Dte_Tme", "ASSGN-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ASSGN_DTE_TME");
        mit_Assgn_Dte_Tme.setDdmHeader("ASSIGNMENT/DATE-TIME");
        registerRecord(vw_mit);

        vw_mit_Hist = new DataAccessProgramView(new NameInfo("vw_mit_Hist", "MIT-HIST"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        mit_Hist_Pin_Nbr = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        mit_Hist_Pin_Nbr.setDdmHeader("PIN");
        mit_Hist_Empl_Oprtr_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "EMPL_OPRTR_CDE");
        mit_Hist_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        mit_Hist_Rqst_Log_Dte_Tme = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        mit_Hist_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        mit_Hist__R_Field_2 = vw_mit_Hist.getRecord().newGroupInGroup("mit_Hist__R_Field_2", "REDEFINE", mit_Hist_Rqst_Log_Dte_Tme);
        mit_Hist_Rqst_Log_Dte = mit_Hist__R_Field_2.newFieldInGroup("mit_Hist_Rqst_Log_Dte", "RQST-LOG-DTE", FieldType.STRING, 8);
        mit_Hist_Rqst_Log_Tme = mit_Hist__R_Field_2.newFieldInGroup("mit_Hist_Rqst_Log_Tme", "RQST-LOG-TME", FieldType.STRING, 7);
        mit_Hist_Rqst_Log_Invrt_Dte_Tme = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Rqst_Log_Invrt_Dte_Tme", "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        mit_Hist_Work_Prcss_Id = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        mit_Hist_Work_Prcss_Id.setDdmHeader("WORK/ID");
        mit_Hist_Orgnl_Unit_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ORGNL_UNIT_CDE");
        mit_Hist_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        mit_Hist_Unit_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        mit_Hist_Unit_Cde.setDdmHeader("UNIT/CODE");
        mit_Hist_Status_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        mit_Hist_Last_Chnge_Dte_Tme = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        mit_Hist_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        mit_Hist_Last_Chnge_Oprtr_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        mit_Hist_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        registerRecord(vw_mit_Hist);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_3 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_3", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_Unit_Key = cwf_Support_Tbl__R_Field_3.newFieldInGroup("cwf_Support_Tbl_Tbl_Unit_Key", "TBL-UNIT-KEY", FieldType.STRING, 15);

        cwf_Support_Tbl__R_Field_4 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_4", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Pnd_Tbl_User_Key = cwf_Support_Tbl__R_Field_4.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_User_Key", "#TBL-USER-KEY", FieldType.STRING, 
            15);

        cwf_Support_Tbl__R_Field_5 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_5", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_Wpid_Key = cwf_Support_Tbl__R_Field_5.newFieldInGroup("cwf_Support_Tbl_Tbl_Wpid_Key", "TBL-WPID-KEY", FieldType.STRING, 15);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_6 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_6", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Unit = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Tbl_Unit", "TBL-UNIT", FieldType.STRING, 8);

        cwf_Support_Tbl__R_Field_7 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_7", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_User = cwf_Support_Tbl__R_Field_7.newFieldInGroup("cwf_Support_Tbl_Tbl_User", "TBL-USER", FieldType.STRING, 8);

        cwf_Support_Tbl__R_Field_8 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_8", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Wpid = cwf_Support_Tbl__R_Field_8.newFieldInGroup("cwf_Support_Tbl_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_9", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_9.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_9.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_9.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_I_Unit = localVariables.newFieldInRecord("pnd_I_Unit", "#I-UNIT", FieldType.NUMERIC, 4);
        pnd_I_User = localVariables.newFieldInRecord("pnd_I_User", "#I-USER", FieldType.NUMERIC, 4);
        pnd_I_Wpid = localVariables.newFieldInRecord("pnd_I_Wpid", "#I-WPID", FieldType.NUMERIC, 4);

        pnd_Units = localVariables.newGroupInRecord("pnd_Units", "#UNITS");
        pnd_Units_Pnd_T_Unit = pnd_Units.newFieldArrayInGroup("pnd_Units_Pnd_T_Unit", "#T-UNIT", FieldType.STRING, 8, new DbsArrayController(1, 9999));

        pnd_Operators = localVariables.newGroupInRecord("pnd_Operators", "#OPERATORS");
        pnd_Operators_Pnd_T_Oprtr = pnd_Operators.newFieldArrayInGroup("pnd_Operators_Pnd_T_Oprtr", "#T-OPRTR", FieldType.STRING, 8, new DbsArrayController(1, 
            9999));

        pnd_Wpids = localVariables.newGroupInRecord("pnd_Wpids", "#WPIDS");
        pnd_Wpids_Pnd_T_Wpid = pnd_Wpids.newFieldArrayInGroup("pnd_Wpids_Pnd_T_Wpid", "#T-WPID", FieldType.STRING, 6, new DbsArrayController(1, 9999));
        pnd_Status_Date_Key = localVariables.newFieldInRecord("pnd_Status_Date_Key", "#STATUS-DATE-KEY", FieldType.STRING, 25);

        pnd_Status_Date_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Status_Date_Key__R_Field_10", "REDEFINE", pnd_Status_Date_Key);
        pnd_Status_Date_Key_Pnd_K_Status = pnd_Status_Date_Key__R_Field_10.newFieldInGroup("pnd_Status_Date_Key_Pnd_K_Status", "#K-STATUS", FieldType.STRING, 
            1);
        pnd_Status_Date_Key_Pnd_K_Invert_Upd_Dte = pnd_Status_Date_Key__R_Field_10.newFieldInGroup("pnd_Status_Date_Key_Pnd_K_Invert_Upd_Dte", "#K-INVERT-UPD-DTE", 
            FieldType.NUMERIC, 15);

        pnd_Status_Date_Key__R_Field_11 = pnd_Status_Date_Key__R_Field_10.newGroupInGroup("pnd_Status_Date_Key__R_Field_11", "REDEFINE", pnd_Status_Date_Key_Pnd_K_Invert_Upd_Dte);
        pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd = pnd_Status_Date_Key__R_Field_11.newFieldInGroup("pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd", "#K-INVERT-YYMMDD", 
            FieldType.NUMERIC, 8);
        pnd_Status_Date_Key_Pnd_K_Invert_Hhiisst = pnd_Status_Date_Key__R_Field_11.newFieldInGroup("pnd_Status_Date_Key_Pnd_K_Invert_Hhiisst", "#K-INVERT-HHIISST", 
            FieldType.NUMERIC, 7);
        pnd_Status_Date_Key__Filler1 = pnd_Status_Date_Key__R_Field_10.newFieldInGroup("pnd_Status_Date_Key__Filler1", "_FILLER1", FieldType.STRING, 9);
        pnd_History_Key = localVariables.newFieldInRecord("pnd_History_Key", "#HISTORY-KEY", FieldType.STRING, 30);

        pnd_History_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_History_Key__R_Field_12", "REDEFINE", pnd_History_Key);
        pnd_History_Key_Pnd_K_Log_Dte_Tme = pnd_History_Key__R_Field_12.newFieldInGroup("pnd_History_Key_Pnd_K_Log_Dte_Tme", "#K-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_History_Key_Pnd_K_Last_Chnge_Invrt_D_T = pnd_History_Key__R_Field_12.newFieldInGroup("pnd_History_Key_Pnd_K_Last_Chnge_Invrt_D_T", "#K-LAST-CHNGE-INVRT-D-T", 
            FieldType.NUMERIC, 15);
        pnd_Frequency = localVariables.newFieldInRecord("pnd_Frequency", "#FREQUENCY", FieldType.STRING, 7);
        pnd_Work_File = localVariables.newFieldInRecord("pnd_Work_File", "#WORK-FILE", FieldType.STRING, 250);

        pnd_Work_File__R_Field_13 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_13", "REDEFINE", pnd_Work_File);
        pnd_Work_File_Pnd_Wf_Oprtr = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Oprtr", "#WF-OPRTR", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Wpid = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Wpid", "#WF-WPID", FieldType.STRING, 6);
        pnd_Work_File_Pnd_Wf_Pin = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Pin", "#WF-PIN", FieldType.NUMERIC, 12);
        pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme", "#WF-RECVD-DTE-TME", FieldType.STRING, 
            15);

        pnd_Work_File__R_Field_14 = pnd_Work_File__R_Field_13.newGroupInGroup("pnd_Work_File__R_Field_14", "REDEFINE", pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Recvd_Dte = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Dte", "#WF-RECVD-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Recvd_Tme = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Tme", "#WF-RECVD-TME", FieldType.NUMERIC, 
            4);
        pnd_Work_File__Filler2 = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File__Filler2", "_FILLER2", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_Due_Dte_Tme = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Due_Dte_Tme", "#WF-DUE-DTE-TME", FieldType.STRING, 
            15);

        pnd_Work_File__R_Field_15 = pnd_Work_File__R_Field_13.newGroupInGroup("pnd_Work_File__R_Field_15", "REDEFINE", pnd_Work_File_Pnd_Wf_Due_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Due_Dte = pnd_Work_File__R_Field_15.newFieldInGroup("pnd_Work_File_Pnd_Wf_Due_Dte", "#WF-DUE-DTE", FieldType.NUMERIC, 8);
        pnd_Work_File_Pnd_Wf_Due_Tme = pnd_Work_File__R_Field_15.newFieldInGroup("pnd_Work_File_Pnd_Wf_Due_Tme", "#WF-DUE-TME", FieldType.NUMERIC, 7);
        pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme", "#WF-COMPLTD-DTE-TME", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_16 = pnd_Work_File__R_Field_13.newGroupInGroup("pnd_Work_File__R_Field_16", "REDEFINE", pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Compltd_Dte = pnd_Work_File__R_Field_16.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Dte", "#WF-COMPLTD-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Compltd_Tme = pnd_Work_File__R_Field_16.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Tme", "#WF-COMPLTD-TME", FieldType.NUMERIC, 
            7);
        pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr", "#WF-LAST-CHNGE-OPRTR", 
            FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Unit_Open_Stts = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Stts", "#WF-UNIT-OPEN-STTS", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme", "#WF-UNIT-OPEN-DTE-TME", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_17 = pnd_Work_File__R_Field_13.newGroupInGroup("pnd_Work_File__R_Field_17", "REDEFINE", pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Unit_Open_Dte = pnd_Work_File__R_Field_17.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Dte", "#WF-UNIT-OPEN-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Unit_Open_Tme = pnd_Work_File__R_Field_17.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Tme", "#WF-UNIT-OPEN-TME", FieldType.NUMERIC, 
            7);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts", "#WF-UNIT-CLSD-STTS", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme", "#WF-UNIT-CLSD-DTE-TME", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_18 = pnd_Work_File__R_Field_13.newGroupInGroup("pnd_Work_File__R_Field_18", "REDEFINE", pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte = pnd_Work_File__R_Field_18.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte", "#WF-UNIT-CLSD-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Tme = pnd_Work_File__R_Field_18.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Tme", "#WF-UNIT-CLSD-TME", FieldType.NUMERIC, 
            7);

        pnd_Work_File__R_Field_19 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_19", "REDEFINE", pnd_Work_File);
        pnd_Work_File__Filler3 = pnd_Work_File__R_Field_19.newFieldInGroup("pnd_Work_File__Filler3", "_FILLER3", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Run = pnd_Work_File__R_Field_19.newFieldInGroup("pnd_Work_File_Pnd_Wf_Run", "#WF-RUN", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Start_Dte = pnd_Work_File__R_Field_19.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Dte", "#WF-START-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_End_Dte = pnd_Work_File__R_Field_19.newFieldInGroup("pnd_Work_File_Pnd_Wf_End_Dte", "#WF-END-DTE", FieldType.NUMERIC, 8);
        pnd_Start_Datd = localVariables.newFieldInRecord("pnd_Start_Datd", "#START-DATD", FieldType.DATE);
        pnd_Start_Yyyymmdd = localVariables.newFieldInRecord("pnd_Start_Yyyymmdd", "#START-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Start_Yyyymmdd__R_Field_20 = localVariables.newGroupInRecord("pnd_Start_Yyyymmdd__R_Field_20", "REDEFINE", pnd_Start_Yyyymmdd);
        pnd_Start_Yyyymmdd_Pnd_Start_Date = pnd_Start_Yyyymmdd__R_Field_20.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 
            8);

        pnd_Start_Yyyymmdd__R_Field_21 = localVariables.newGroupInRecord("pnd_Start_Yyyymmdd__R_Field_21", "REDEFINE", pnd_Start_Yyyymmdd);
        pnd_Start_Yyyymmdd_Pnd_Start_Yyyymm = pnd_Start_Yyyymmdd__R_Field_21.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Yyyymm", "#START-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_Start_Yyyymmdd_Pnd_Start_Dd = pnd_Start_Yyyymmdd__R_Field_21.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Dd", "#START-DD", FieldType.NUMERIC, 
            2);
        pnd_End_Datd = localVariables.newFieldInRecord("pnd_End_Datd", "#END-DATD", FieldType.DATE);
        pnd_End_Yyyymmdd = localVariables.newFieldInRecord("pnd_End_Yyyymmdd", "#END-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_End_Yyyymmdd__R_Field_22 = localVariables.newGroupInRecord("pnd_End_Yyyymmdd__R_Field_22", "REDEFINE", pnd_End_Yyyymmdd);
        pnd_End_Yyyymmdd_Pnd_End_Date = pnd_End_Yyyymmdd__R_Field_22.newFieldInGroup("pnd_End_Yyyymmdd_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_End_Yyyymmdd__R_Field_23 = localVariables.newGroupInRecord("pnd_End_Yyyymmdd__R_Field_23", "REDEFINE", pnd_End_Yyyymmdd);
        pnd_End_Yyyymmdd_Pnd_End_Yyyymm = pnd_End_Yyyymmdd__R_Field_23.newFieldInGroup("pnd_End_Yyyymmdd_Pnd_End_Yyyymm", "#END-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_End_Yyyymmdd_Pnd_End_Dd = pnd_End_Yyyymmdd__R_Field_23.newFieldInGroup("pnd_End_Yyyymmdd_Pnd_End_Dd", "#END-DD", FieldType.NUMERIC, 2);
        pnd_Last_Chnge_Dte_Tme = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_Tme", "#LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);

        pnd_Last_Chnge_Dte_Tme__R_Field_24 = localVariables.newGroupInRecord("pnd_Last_Chnge_Dte_Tme__R_Field_24", "REDEFINE", pnd_Last_Chnge_Dte_Tme);
        pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Yyyymmdd = pnd_Last_Chnge_Dte_Tme__R_Field_24.newFieldInGroup("pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Yyyymmdd", 
            "#LAST-CHNGE-YYYYMMDD", FieldType.NUMERIC, 8);
        pnd_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_Log_Dte_Tme", "#LOG-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Write_Count = localVariables.newFieldInRecord("pnd_Write_Count", "#WRITE-COUNT", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_mit.reset();
        vw_mit_Hist.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8630() throws Exception
    {
        super("Cwfb8630");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Cwfb8630|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT PS = 22 LS = 132
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Frequency);                                                                                        //Natural: INPUT #FREQUENCY
                //*  RUN EVERY WEEK
                if (condition(pnd_Frequency.equals("WEEKLY ")))                                                                                                           //Natural: IF #FREQUENCY = 'WEEKLY '
                {
                    pnd_Start_Datd.compute(new ComputeParameters(false, pnd_Start_Datd), Global.getDATX().subtract(7));                                                   //Natural: ASSIGN #START-DATD := *DATX - 7
                    pnd_End_Datd.compute(new ComputeParameters(false, pnd_End_Datd), Global.getDATX().subtract(1));                                                       //Natural: ASSIGN #END-DATD := *DATX - 1
                    pnd_Start_Yyyymmdd_Pnd_Start_Date.setValueEdited(pnd_Start_Datd,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #START-DATD ( EM = YYYYMMDD ) TO #START-DATE
                    pnd_End_Yyyymmdd_Pnd_End_Date.setValueEdited(pnd_End_Datd,new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED #END-DATD ( EM = YYYYMMDD ) TO #END-DATE
                }                                                                                                                                                         //Natural: END-IF
                //*  GET PRIOR MONTH's date
                if (condition(pnd_Frequency.equals("MONTHLY")))                                                                                                           //Natural: IF #FREQUENCY = 'MONTHLY'
                {
                    pnd_Start_Datd.compute(new ComputeParameters(false, pnd_Start_Datd), Global.getDATX().subtract(28));                                                  //Natural: ASSIGN #START-DATD := *DATX - 28
                    //*  FIRST DAY OF
                    pnd_Start_Yyyymmdd_Pnd_Start_Date.setValueEdited(pnd_Start_Datd,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #START-DATD ( EM = YYYYMMDD ) TO #START-DATE
                    //*    PRIOR MONTH
                    pnd_Start_Yyyymmdd_Pnd_Start_Dd.setValue(1);                                                                                                          //Natural: MOVE 01 TO #START-DD
                    //*  LAST DATE OF
                    pnd_End_Yyyymmdd_Pnd_End_Date.setValue(pnd_Start_Yyyymmdd_Pnd_Start_Date);                                                                            //Natural: MOVE #START-DATE TO #END-DATE
                    //*    PRIOR MONTH
                    pnd_End_Yyyymmdd_Pnd_End_Dd.setValue(31);                                                                                                             //Natural: MOVE 31 TO #END-DD
                    //*  CLOSED REQUEST
                }                                                                                                                                                         //Natural: END-IF
                //* **************** TEST ** TEST ** TEST ** TEST ** TEST ** TEST ** TEST
                //*      #START-DATE  := 20120107
                //*      #END-DATE    := 20120113
                //* **************** TEST ** TEST ** TEST ** TEST ** TEST ** TEST ** TEST
                //* *
                //*  READ BY INVERT DATE
                //* *
                pnd_Status_Date_Key.setValue(" ");                                                                                                                        //Natural: ASSIGN #STATUS-DATE-KEY := ' '
                pnd_Status_Date_Key_Pnd_K_Status.setValue("9");                                                                                                           //Natural: ASSIGN #K-STATUS := '9'
                pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd.compute(new ComputeParameters(false, pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd), DbsField.subtract(99999999,        //Natural: COMPUTE #K-INVERT-YYMMDD = 99999999 - #END-YYYYMMDD
                    pnd_End_Yyyymmdd));
                pnd_Status_Date_Key_Pnd_K_Invert_Hhiisst.setValue(0);                                                                                                     //Natural: ASSIGN #K-INVERT-HHIISST := 0000000
                getReports().write(0, "=",pnd_Start_Yyyymmdd,"=",pnd_End_Yyyymmdd);                                                                                       //Natural: WRITE '=' #START-YYYYMMDD '=' #END-YYYYMMDD
                if (Global.isEscape()) return;
                //*  LOAD TABLES (UNITS, OPERATORS, WPIDS)
                pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                  //Natural: ASSIGN #TBL-SCRTY-LEVEL-IND := 'A'
                pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("PAYMENT-OPS-REPORT");                                                                                       //Natural: ASSIGN #TBL-TABLE-NME := 'PAYMENT-OPS-REPORT'
                pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue(" ");                                                                                                        //Natural: ASSIGN #TBL-KEY-FIELD := ' '
                vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                      //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
                (
                "READ01",
                new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
                new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
                );
                READ01:
                while (condition(vw_cwf_Support_Tbl.readNextRow("READ01")))
                {
                    if (condition(cwf_Support_Tbl_Tbl_Scrty_Level_Ind.notEquals("A ") || cwf_Support_Tbl_Tbl_Table_Nme.notEquals("PAYMENT-OPS-REPORT  ")))                //Natural: IF TBL-SCRTY-LEVEL-IND NE 'A ' OR TBL-TABLE-NME NE 'PAYMENT-OPS-REPORT  '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    short decideConditionsMet215 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTRING ( CWF-SUPPORT-TBL.TBL-KEY-FIELD,1,16 ) = 'PAYMENT-OPS-UNIT'
                    if (condition(cwf_Support_Tbl_Tbl_Key_Field.getSubstring(1,16).equals("PAYMENT-OPS-UNIT")))
                    {
                        decideConditionsMet215++;
                        pnd_I_Unit.nadd(1);                                                                                                                               //Natural: ADD 1 TO #I-UNIT
                        pnd_Units_Pnd_T_Unit.getValue(pnd_I_Unit).setValue(cwf_Support_Tbl_Tbl_Unit);                                                                     //Natural: ASSIGN #T-UNIT ( #I-UNIT ) := CWF-SUPPORT-TBL.TBL-UNIT
                    }                                                                                                                                                     //Natural: WHEN SUBSTRING ( CWF-SUPPORT-TBL.TBL-KEY-FIELD,1,16 ) = 'PAYMENT-OPS-USER'
                    else if (condition(cwf_Support_Tbl_Tbl_Key_Field.getSubstring(1,16).equals("PAYMENT-OPS-USER")))
                    {
                        decideConditionsMet215++;
                        pnd_I_User.nadd(1);                                                                                                                               //Natural: ADD 1 TO #I-USER
                        pnd_Operators_Pnd_T_Oprtr.getValue(pnd_I_User).setValue(cwf_Support_Tbl_Tbl_User);                                                                //Natural: ASSIGN #T-OPRTR ( #I-USER ) := CWF-SUPPORT-TBL.TBL-USER
                    }                                                                                                                                                     //Natural: WHEN SUBSTRING ( CWF-SUPPORT-TBL.TBL-KEY-FIELD,1,16 ) = 'PAYMENT-OPS-WPID'
                    else if (condition(cwf_Support_Tbl_Tbl_Key_Field.getSubstring(1,16).equals("PAYMENT-OPS-WPID")))
                    {
                        decideConditionsMet215++;
                        pnd_I_Wpid.nadd(1);                                                                                                                               //Natural: ADD 1 TO #I-WPID
                        pnd_Wpids_Pnd_T_Wpid.getValue(pnd_I_Wpid).setValue(cwf_Support_Tbl_Tbl_Wpid);                                                                     //Natural: ASSIGN #T-WPID ( #I-WPID ) := CWF-SUPPORT-TBL.TBL-WPID
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //*  HEADER RECORD
                pnd_Work_File_Pnd_Wf_Run.setValue(pnd_Frequency);                                                                                                         //Natural: ASSIGN #WF-RUN := #FREQUENCY
                pnd_Work_File_Pnd_Wf_Start_Dte.setValue(pnd_Start_Yyyymmdd);                                                                                              //Natural: ASSIGN #WF-START-DTE := #START-YYYYMMDD
                pnd_Work_File_Pnd_Wf_End_Dte.setValue(pnd_End_Yyyymmdd);                                                                                                  //Natural: ASSIGN #WF-END-DTE := #END-YYYYMMDD
                getWorkFiles().write(1, false, pnd_Work_File);                                                                                                            //Natural: WRITE WORK FILE 1 #WORK-FILE
                pnd_Work_File.reset();                                                                                                                                    //Natural: RESET #WORK-FILE
                vw_mit.startDatabaseRead                                                                                                                                  //Natural: READ MIT BY CRPRTE-STATUS-CHNGE-DTE-KEY STARTING FROM #STATUS-DATE-KEY
                (
                "READ02",
                new Wc[] { new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", ">=", pnd_Status_Date_Key, WcType.BY) },
                new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
                );
                READ02:
                while (condition(vw_mit.readNextRow("READ02")))
                {
                    pnd_Last_Chnge_Dte_Tme.setValue(mit_Last_Chnge_Dte_Tme);                                                                                              //Natural: ASSIGN #LAST-CHNGE-DTE-TME := MIT.LAST-CHNGE-DTE-TME
                    //*  REQUESTS ARE READ IN REVERSE DATE ORDER
                    if (condition(pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Yyyymmdd.less(pnd_Start_Yyyymmdd)))                                                               //Natural: IF #LAST-CHNGE-YYYYMMDD LT #START-YYYYMMDD
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_History_Key.reset();                                                                                                                              //Natural: RESET #HISTORY-KEY
                    pnd_History_Key_Pnd_K_Log_Dte_Tme.setValue(mit_Rqst_Log_Dte_Tme);                                                                                     //Natural: ASSIGN #K-LOG-DTE-TME := MIT.RQST-LOG-DTE-TME
                    pnd_History_Key_Pnd_K_Last_Chnge_Invrt_D_T.setValue(0);                                                                                               //Natural: ASSIGN #K-LAST-CHNGE-INVRT-D-T := 0
                                                                                                                                                                          //Natural: PERFORM DETERMINE-REQUEST-HISTORY
                    sub_Determine_Request_History();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  RQST NOT OPENED BY UNIT
                    //*  RQST NOT CLOSED BY UNIT
                    if (condition(pnd_Work_File_Pnd_Wf_Unit_Open_Stts.equals(" ") || pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.equals(" ")))                                    //Natural: IF #WF-UNIT-OPEN-STTS = ' ' OR #WF-UNIT-CLSD-STTS = ' '
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(mit_Work_Prcss_Id.equals(pnd_Wpids_Pnd_T_Wpid.getValue("*"))))                                                                          //Natural: IF MIT.WORK-PRCSS-ID = #T-WPID ( * )
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ASSIGNED TO PAYMENT OPS
                    if (condition(pnd_Work_File_Pnd_Wf_Oprtr.equals(pnd_Operators_Pnd_T_Oprtr.getValue("*"))))                                                            //Natural: IF #WF-OPRTR = #T-OPRTR ( * )
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_File_Pnd_Wf_Wpid.setValue(mit_Work_Prcss_Id);                                                                                                //Natural: ASSIGN #WF-WPID := MIT.WORK-PRCSS-ID
                    pnd_Work_File_Pnd_Wf_Pin.setValue(mit_Pin_Nbr);                                                                                                       //Natural: ASSIGN #WF-PIN := MIT.PIN-NBR
                    pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme.setValueEdited(mit_Tiaa_Rcvd_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                       //Natural: MOVE EDITED MIT.TIAA-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #WF-RECVD-DTE-TME
                    pnd_Work_File_Pnd_Wf_Due_Dte_Tme.setValueEdited(mit_Crprte_Due_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                        //Natural: MOVE EDITED MIT.CRPRTE-DUE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #WF-DUE-DTE-TME
                    pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme.setValue(mit_Last_Chnge_Dte_Tme);                                                                                //Natural: ASSIGN #WF-COMPLTD-DTE-TME := MIT.LAST-CHNGE-DTE-TME
                    getReports().display(0, "oprtr",                                                                                                                      //Natural: DISPLAY 'oprtr' #WF-OPRTR 'wpid' #WF-WPID 'pin' #WF-PIN 'rcvd' #WF-RECVD-DTE ' ' #WF-RECVD-TME 'due' #WF-DUE-DTE 'cmpltd' #WF-COMPLTD-DTE 'open' #WF-UNIT-OPEN-STTS ' ' #WF-UNIT-OPEN-DTE 'clsd' #WF-UNIT-CLSD-STTS ' ' #WF-UNIT-CLSD-DTE
                    		pnd_Work_File_Pnd_Wf_Oprtr,"wpid",
                    		pnd_Work_File_Pnd_Wf_Wpid,"pin",
                    		pnd_Work_File_Pnd_Wf_Pin,"rcvd",
                    		pnd_Work_File_Pnd_Wf_Recvd_Dte," ",
                    		pnd_Work_File_Pnd_Wf_Recvd_Tme,"due",
                    		pnd_Work_File_Pnd_Wf_Due_Dte,"cmpltd",
                    		pnd_Work_File_Pnd_Wf_Compltd_Dte,"open",
                    		pnd_Work_File_Pnd_Wf_Unit_Open_Stts," ",
                    		pnd_Work_File_Pnd_Wf_Unit_Open_Dte,"clsd",
                    		pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts," ",
                    		pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getWorkFiles().write(1, false, pnd_Work_File);                                                                                                        //Natural: WRITE WORK FILE 1 #WORK-FILE
                    pnd_Work_File.reset();                                                                                                                                //Natural: RESET #WORK-FILE
                    pnd_Write_Count.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WRITE-COUNT
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Write_Count);                                                                                                               //Natural: WRITE '=' #WRITE-COUNT
                if (Global.isEscape()) return;
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-REQUEST-HISTORY
                //* ***********************************************************************
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Determine_Request_History() throws Exception                                                                                                         //Natural: DETERMINE-REQUEST-HISTORY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Work_File_Pnd_Wf_Oprtr.reset();                                                                                                                               //Natural: RESET #WF-OPRTR #WF-UNIT-OPEN-STTS #WF-UNIT-CLSD-STTS #WF-UNIT-OPEN-DTE #WF-UNIT-CLSD-DTE
        pnd_Work_File_Pnd_Wf_Unit_Open_Stts.reset();
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.reset();
        pnd_Work_File_Pnd_Wf_Unit_Open_Dte.reset();
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte.reset();
        vw_mit_Hist.startDatabaseRead                                                                                                                                     //Natural: READ MIT-HIST BY RQST-HISTORY-KEY STARTING FROM #HISTORY-KEY
        (
        "READ03",
        new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", pnd_History_Key, WcType.BY) },
        new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_mit_Hist.readNextRow("READ03")))
        {
            if (condition(mit_Hist_Rqst_Log_Dte_Tme.greater(mit_Rqst_Log_Dte_Tme)))                                                                                       //Natural: IF MIT-HIST.RQST-LOG-DTE-TME > MIT.RQST-LOG-DTE-TME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(mit_Hist_Pin_Nbr.notEquals(mit_Pin_Nbr)))                                                                                                       //Natural: IF MIT-HIST.PIN-NBR NE MIT.PIN-NBR
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(mit_Hist_Work_Prcss_Id.equals(pnd_Wpids_Pnd_T_Wpid.getValue("*"))))                                                                             //Natural: IF MIT-HIST.WORK-PRCSS-ID = #T-WPID ( * )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(mit_Hist_Orgnl_Unit_Cde.equals(pnd_Units_Pnd_T_Unit.getValue("*")) || mit_Hist_Unit_Cde.equals(pnd_Units_Pnd_T_Unit.getValue("*"))))            //Natural: IF MIT-HIST.ORGNL-UNIT-CDE = #T-UNIT ( * ) OR MIT-HIST.UNIT-CDE = #T-UNIT ( * )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  TAKE THE FIRST "assign to" OPERATOR AND THE FIRST "closed" DATE
            //*  ASSIGNED
            if (condition(DbsUtil.maskMatches(mit_Hist_Status_Cde,"'2'...")))                                                                                             //Natural: IF MIT-HIST.STATUS-CDE = MASK ( '2'... )
            {
                pnd_Work_File_Pnd_Wf_Oprtr.setValue(mit_Hist_Empl_Oprtr_Cde);                                                                                             //Natural: ASSIGN #WF-OPRTR := MIT-HIST.EMPL-OPRTR-CDE
                pnd_Work_File_Pnd_Wf_Unit_Open_Stts.setValue(mit_Hist_Status_Cde);                                                                                        //Natural: ASSIGN #WF-UNIT-OPEN-STTS := MIT-HIST.STATUS-CDE
                pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme.setValue(mit_Hist_Last_Chnge_Dte_Tme);                                                                             //Natural: ASSIGN #WF-UNIT-OPEN-DTE-TME := MIT-HIST.LAST-CHNGE-DTE-TME
            }                                                                                                                                                             //Natural: END-IF
            //*  COMPLETED
            if (condition(mit_Hist_Status_Cde.equals("4325") || DbsUtil.maskMatches(mit_Hist_Status_Cde,"'7'...") || DbsUtil.maskMatches(mit_Hist_Status_Cde,             //Natural: IF MIT-HIST.STATUS-CDE = '4325' OR = MASK ( '7'... ) OR = MASK ( '8'... )
                "'8'...")))
            {
                pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.setValue(mit_Hist_Status_Cde);                                                                                        //Natural: ASSIGN #WF-UNIT-CLSD-STTS := MIT-HIST.STATUS-CDE
                pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme.setValue(mit_Hist_Last_Chnge_Dte_Tme);                                                                             //Natural: ASSIGN #WF-UNIT-CLSD-DTE-TME := MIT-HIST.LAST-CHNGE-DTE-TME
                pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr.setValue(mit_Hist_Last_Chnge_Oprtr_Cde);                                                                            //Natural: ASSIGN #WF-LAST-CHNGE-OPRTR := MIT-HIST.LAST-CHNGE-OPRTR-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  DETERMINE-REQUEST-HISTORY
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=22 LS=132");

        getReports().setDisplayColumns(0, "oprtr",
        		pnd_Work_File_Pnd_Wf_Oprtr,"wpid",
        		pnd_Work_File_Pnd_Wf_Wpid,"pin",
        		pnd_Work_File_Pnd_Wf_Pin,"rcvd",
        		pnd_Work_File_Pnd_Wf_Recvd_Dte," ",
        		pnd_Work_File_Pnd_Wf_Recvd_Tme,"due",
        		pnd_Work_File_Pnd_Wf_Due_Dte,"cmpltd",
        		pnd_Work_File_Pnd_Wf_Compltd_Dte,"open",
        		pnd_Work_File_Pnd_Wf_Unit_Open_Stts," ",
        		pnd_Work_File_Pnd_Wf_Unit_Open_Dte,"clsd",
        		pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts," ",
        		pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte);
    }
}
