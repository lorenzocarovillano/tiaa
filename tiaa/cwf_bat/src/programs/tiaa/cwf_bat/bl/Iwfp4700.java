/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:52:25 PM
**        * FROM NATURAL PROGRAM : Iwfp4700
************************************************************
**        * FILE NAME            : Iwfp4700.java
**        * CLASS NAME           : Iwfp4700
**        * INSTANCE NAME        : Iwfp4700
************************************************************
************************************************************************
* PROGRAM   : IWFP4700
* SYSTEM    : CORPORATE WORK FLOW - ENHANCED WORK LISTS
* TITLE     : CHANGE SUMMARY REPORTS
* GENERATED : 04/02/1999
* FUNCTION  : TO CALCULATE CHANGES MADE TO PARTICIPANT, NON-PARTICIPANT,
* AND INSTITUTION RECORDS USING THE CWF-SUPPORT-TBL.  CREATE SUMMARY
* REPORTS DISPLAYING THIS INFORMATION.
* HISTORY :  ZIVY JOHNSON
* 07/30/99:  JOHNSZI  ADDED THE ABILITY TO DELETE RECORDS GREATER THAN
*                     21 DAYS OLD
* 08/03/99:  JOHNSZI  ADDED CHECK FOR BLANKS FOR #START-TIME AND
*                     #END-TIME
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iwfp4700 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf;
    private DbsField cwf_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Tbl_Table_Nme;
    private DbsField cwf_Tbl_Key_Field;

    private DbsGroup cwf__R_Field_1;
    private DbsField cwf_Pnd_Invrtd_Date_Stamp;
    private DbsField cwf_Pnd_Initial_Load;
    private DbsField cwf_Pnd_Invrtd_Time_Stamp;
    private DbsField cwf_Tbl_Data_Field;

    private DbsGroup cwf__R_Field_2;
    private DbsField cwf_Pnd_Start_Date_Time_Stamp;

    private DbsGroup cwf__R_Field_3;
    private DbsField cwf_Pnd_Start_Date;
    private DbsField cwf_Pnd_Start_Time;
    private DbsField cwf_Pnd_Fill1;
    private DbsField cwf_Pnd_End_Date_Time_Stamp;

    private DbsGroup cwf__R_Field_4;
    private DbsField cwf_Pnd_End_Date;
    private DbsField cwf_Pnd_End_Time;
    private DbsField cwf_Pnd_Fill2;
    private DbsField cwf_Pnd_Message_Info;
    private DbsField cwf_Pnd_Fill3;
    private DbsField cwf_Pnd_Icw_Total_Recs_Read_Ctr;
    private DbsField cwf_Pnd_Fill4;
    private DbsField cwf_Pnd_Icw_Total_Recs_Added_Ctr;
    private DbsField cwf_Pnd_Fill5;
    private DbsField cwf_Pnd_Icw_Total_Recs_Updated_Ctr;
    private DbsField cwf_Pnd_Fill6;
    private DbsField cwf_Pnd_Icw_Total_Recs_Deleted_Ctr;
    private DbsField cwf_Pnd_Fill7;
    private DbsField cwf_Pnd_Fill8;
    private DbsField cwf_Pnd_Cwf_Total_Recs_Read_Ctr;
    private DbsField cwf_Pnd_Fill9;
    private DbsField cwf_Pnd_Cwf_Total_Recs_Added_Ctr;
    private DbsField cwf_Pnd_Fill10;
    private DbsField cwf_Pnd_Cwf_Total_Recs_Updated_Ctr;
    private DbsField cwf_Pnd_Fill11;
    private DbsField cwf_Pnd_Cwf_Total_Recs_Deleted_Ctr;
    private DbsField cwf_Pnd_Fill12;
    private DbsField cwf_Pnd_Fill13;
    private DbsField cwf_Pnd_Ncw_Total_Recs_Read_Ctr;
    private DbsField cwf_Pnd_Fill14;
    private DbsField cwf_Pnd_Ncw_Total_Recs_Added_Ctr;
    private DbsField cwf_Pnd_Fill15;
    private DbsField cwf_Pnd_Ncw_Total_Recs_Updated_Ctr;
    private DbsField cwf_Pnd_Fill16;
    private DbsField cwf_Pnd_Ncw_Total_Recs_Deleted_Ctr;
    private DbsField cwf_Pnd_Fill17;
    private DbsField cwf_Tbl_Actve_Ind;
    private DbsField pnd_Program;
    private DbsField pnd_Day_Counter;
    private DbsField pnd_Current_Rec_Date;
    private DbsField pnd_Current_Rec_Date1;
    private DbsField pnd_Current_Rec_Date2;
    private DbsField pnd_Current_Rec_Date3;
    private DbsField pnd_Current_Rec_Date4;
    private DbsField pnd_Current_Rec_Date5;
    private DbsField pnd_Current_Rec_Date6;
    private DbsField pnd_Current_Rec_Date7;
    private DbsField pnd_Icw_Total;
    private DbsField pnd_Total_Adds;
    private DbsField pnd_Total_Dels;
    private DbsField pnd_Total_Upds;
    private DbsField pnd_Total_Auds;
    private DbsField pnd_Icw_Adds;
    private DbsField pnd_Cwf_Adds;
    private DbsField pnd_Ncw_Adds;
    private DbsField pnd_Icw_Upds;
    private DbsField pnd_Cwf_Upds;
    private DbsField pnd_Ncw_Upds;
    private DbsField pnd_Icw_Dels;
    private DbsField pnd_Cwf_Dels;
    private DbsField pnd_Ncw_Dels;
    private DbsField pnd_Icw_Auds;
    private DbsField pnd_Cwf_Auds;
    private DbsField pnd_Ncw_Auds;
    private DbsField pnd_Icw_U_Total;
    private DbsField pnd_Cwf_U_Total;
    private DbsField pnd_Ncw_U_Total;
    private DbsField pnd_Total_Processed;
    private DbsField pnd_Icw_Week_Avg_Del;
    private DbsField pnd_Initial_Adds;
    private DbsField pnd_Page;
    private DbsField pnd_Counter;
    private DbsField pnd_Counter_Total;
    private DbsField pnd_Counter_Total2;
    private DbsField pnd_Counter_Total3;
    private DbsField pnd_Counter1;
    private DbsField pnd_Counter2;
    private DbsField pnd_X;
    private DbsField pnd_Invrtd_Date_Stamp_2;
    private DbsField pnd_Invert_Current_Date;
    private DbsField pnd_Delete_Date_D;
    private DbsField pnd_Current_Date_D;
    private DbsField pnd_Current_Date_A;
    private DbsField pnd_Delete_Date;
    private DbsField pnd_Delete_Date_N;

    private DbsGroup pnd_Delete_Date_N__R_Field_5;
    private DbsField pnd_Delete_Date_N_Pnd_Delete_Date_A;
    private DbsField pnd_Delete_Date_2;
    private DbsField pnd_Ddcounter;
    private DbsField pnd_Ddcounter_2;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_6;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Array;
    private DbsField pnd_Font;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf = new DataAccessProgramView(new NameInfo("vw_cwf", "CWF"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Tbl_Scrty_Level_Ind = vw_cwf.getRecord().newFieldInGroup("cwf_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TBL_SCRTY_LEVEL_IND");
        cwf_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Tbl_Table_Nme = vw_cwf.getRecord().newFieldInGroup("cwf_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TBL_TABLE_NME");
        cwf_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Tbl_Key_Field = vw_cwf.getRecord().newFieldInGroup("cwf_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "TBL_KEY_FIELD");

        cwf__R_Field_1 = vw_cwf.getRecord().newGroupInGroup("cwf__R_Field_1", "REDEFINE", cwf_Tbl_Key_Field);
        cwf_Pnd_Invrtd_Date_Stamp = cwf__R_Field_1.newFieldInGroup("cwf_Pnd_Invrtd_Date_Stamp", "#INVRTD-DATE-STAMP", FieldType.NUMERIC, 8);
        cwf_Pnd_Initial_Load = cwf__R_Field_1.newFieldInGroup("cwf_Pnd_Initial_Load", "#INITIAL-LOAD", FieldType.STRING, 1);
        cwf_Pnd_Invrtd_Time_Stamp = cwf__R_Field_1.newFieldInGroup("cwf_Pnd_Invrtd_Time_Stamp", "#INVRTD-TIME-STAMP", FieldType.NUMERIC, 7);
        cwf_Tbl_Data_Field = vw_cwf.getRecord().newFieldInGroup("cwf_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");

        cwf__R_Field_2 = vw_cwf.getRecord().newGroupInGroup("cwf__R_Field_2", "REDEFINE", cwf_Tbl_Data_Field);
        cwf_Pnd_Start_Date_Time_Stamp = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Start_Date_Time_Stamp", "#START-DATE-TIME-STAMP", FieldType.NUMERIC, 15);

        cwf__R_Field_3 = cwf__R_Field_2.newGroupInGroup("cwf__R_Field_3", "REDEFINE", cwf_Pnd_Start_Date_Time_Stamp);
        cwf_Pnd_Start_Date = cwf__R_Field_3.newFieldInGroup("cwf_Pnd_Start_Date", "#START-DATE", FieldType.NUMERIC, 8);
        cwf_Pnd_Start_Time = cwf__R_Field_3.newFieldInGroup("cwf_Pnd_Start_Time", "#START-TIME", FieldType.NUMERIC, 7);
        cwf_Pnd_Fill1 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill1", "#FILL1", FieldType.STRING, 1);
        cwf_Pnd_End_Date_Time_Stamp = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_End_Date_Time_Stamp", "#END-DATE-TIME-STAMP", FieldType.NUMERIC, 15);

        cwf__R_Field_4 = cwf__R_Field_2.newGroupInGroup("cwf__R_Field_4", "REDEFINE", cwf_Pnd_End_Date_Time_Stamp);
        cwf_Pnd_End_Date = cwf__R_Field_4.newFieldInGroup("cwf_Pnd_End_Date", "#END-DATE", FieldType.NUMERIC, 8);
        cwf_Pnd_End_Time = cwf__R_Field_4.newFieldInGroup("cwf_Pnd_End_Time", "#END-TIME", FieldType.NUMERIC, 7);
        cwf_Pnd_Fill2 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill2", "#FILL2", FieldType.STRING, 1);
        cwf_Pnd_Message_Info = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Message_Info", "#MESSAGE-INFO", FieldType.STRING, 31);
        cwf_Pnd_Fill3 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill3", "#FILL3", FieldType.STRING, 7);
        cwf_Pnd_Icw_Total_Recs_Read_Ctr = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Icw_Total_Recs_Read_Ctr", "#ICW-TOTAL-RECS-READ-CTR", FieldType.NUMERIC, 
            7);
        cwf_Pnd_Fill4 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill4", "#FILL4", FieldType.STRING, 4);
        cwf_Pnd_Icw_Total_Recs_Added_Ctr = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Icw_Total_Recs_Added_Ctr", "#ICW-TOTAL-RECS-ADDED-CTR", FieldType.NUMERIC, 
            7);
        cwf_Pnd_Fill5 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill5", "#FILL5", FieldType.STRING, 4);
        cwf_Pnd_Icw_Total_Recs_Updated_Ctr = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Icw_Total_Recs_Updated_Ctr", "#ICW-TOTAL-RECS-UPDATED-CTR", FieldType.NUMERIC, 
            7);
        cwf_Pnd_Fill6 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill6", "#FILL6", FieldType.STRING, 4);
        cwf_Pnd_Icw_Total_Recs_Deleted_Ctr = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Icw_Total_Recs_Deleted_Ctr", "#ICW-TOTAL-RECS-DELETED-CTR", FieldType.NUMERIC, 
            7);
        cwf_Pnd_Fill7 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill7", "#FILL7", FieldType.STRING, 16);
        cwf_Pnd_Fill8 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill8", "#FILL8", FieldType.STRING, 7);
        cwf_Pnd_Cwf_Total_Recs_Read_Ctr = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Cwf_Total_Recs_Read_Ctr", "#CWF-TOTAL-RECS-READ-CTR", FieldType.NUMERIC, 
            7);
        cwf_Pnd_Fill9 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill9", "#FILL9", FieldType.STRING, 4);
        cwf_Pnd_Cwf_Total_Recs_Added_Ctr = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Cwf_Total_Recs_Added_Ctr", "#CWF-TOTAL-RECS-ADDED-CTR", FieldType.NUMERIC, 
            7);
        cwf_Pnd_Fill10 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill10", "#FILL10", FieldType.STRING, 4);
        cwf_Pnd_Cwf_Total_Recs_Updated_Ctr = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Cwf_Total_Recs_Updated_Ctr", "#CWF-TOTAL-RECS-UPDATED-CTR", FieldType.NUMERIC, 
            7);
        cwf_Pnd_Fill11 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill11", "#FILL11", FieldType.STRING, 4);
        cwf_Pnd_Cwf_Total_Recs_Deleted_Ctr = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Cwf_Total_Recs_Deleted_Ctr", "#CWF-TOTAL-RECS-DELETED-CTR", FieldType.NUMERIC, 
            7);
        cwf_Pnd_Fill12 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill12", "#FILL12", FieldType.STRING, 16);
        cwf_Pnd_Fill13 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill13", "#FILL13", FieldType.STRING, 7);
        cwf_Pnd_Ncw_Total_Recs_Read_Ctr = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Ncw_Total_Recs_Read_Ctr", "#NCW-TOTAL-RECS-READ-CTR", FieldType.NUMERIC, 
            7);
        cwf_Pnd_Fill14 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill14", "#FILL14", FieldType.STRING, 4);
        cwf_Pnd_Ncw_Total_Recs_Added_Ctr = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Ncw_Total_Recs_Added_Ctr", "#NCW-TOTAL-RECS-ADDED-CTR", FieldType.NUMERIC, 
            7);
        cwf_Pnd_Fill15 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill15", "#FILL15", FieldType.STRING, 4);
        cwf_Pnd_Ncw_Total_Recs_Updated_Ctr = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Ncw_Total_Recs_Updated_Ctr", "#NCW-TOTAL-RECS-UPDATED-CTR", FieldType.NUMERIC, 
            7);
        cwf_Pnd_Fill16 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill16", "#FILL16", FieldType.STRING, 4);
        cwf_Pnd_Ncw_Total_Recs_Deleted_Ctr = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Ncw_Total_Recs_Deleted_Ctr", "#NCW-TOTAL-RECS-DELETED-CTR", FieldType.NUMERIC, 
            7);
        cwf_Pnd_Fill17 = cwf__R_Field_2.newFieldInGroup("cwf_Pnd_Fill17", "#FILL17", FieldType.STRING, 16);
        cwf_Tbl_Actve_Ind = vw_cwf.getRecord().newFieldInGroup("cwf_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TBL_ACTVE_IND");
        registerRecord(vw_cwf);

        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Day_Counter = localVariables.newFieldInRecord("pnd_Day_Counter", "#DAY-COUNTER", FieldType.NUMERIC, 8);
        pnd_Current_Rec_Date = localVariables.newFieldInRecord("pnd_Current_Rec_Date", "#CURRENT-REC-DATE", FieldType.NUMERIC, 8);
        pnd_Current_Rec_Date1 = localVariables.newFieldInRecord("pnd_Current_Rec_Date1", "#CURRENT-REC-DATE1", FieldType.NUMERIC, 8);
        pnd_Current_Rec_Date2 = localVariables.newFieldInRecord("pnd_Current_Rec_Date2", "#CURRENT-REC-DATE2", FieldType.NUMERIC, 8);
        pnd_Current_Rec_Date3 = localVariables.newFieldInRecord("pnd_Current_Rec_Date3", "#CURRENT-REC-DATE3", FieldType.NUMERIC, 8);
        pnd_Current_Rec_Date4 = localVariables.newFieldInRecord("pnd_Current_Rec_Date4", "#CURRENT-REC-DATE4", FieldType.NUMERIC, 8);
        pnd_Current_Rec_Date5 = localVariables.newFieldInRecord("pnd_Current_Rec_Date5", "#CURRENT-REC-DATE5", FieldType.NUMERIC, 8);
        pnd_Current_Rec_Date6 = localVariables.newFieldInRecord("pnd_Current_Rec_Date6", "#CURRENT-REC-DATE6", FieldType.NUMERIC, 8);
        pnd_Current_Rec_Date7 = localVariables.newFieldInRecord("pnd_Current_Rec_Date7", "#CURRENT-REC-DATE7", FieldType.NUMERIC, 8);
        pnd_Icw_Total = localVariables.newFieldInRecord("pnd_Icw_Total", "#ICW-TOTAL", FieldType.NUMERIC, 9);
        pnd_Total_Adds = localVariables.newFieldInRecord("pnd_Total_Adds", "#TOTAL-ADDS", FieldType.NUMERIC, 9);
        pnd_Total_Dels = localVariables.newFieldInRecord("pnd_Total_Dels", "#TOTAL-DELS", FieldType.NUMERIC, 9);
        pnd_Total_Upds = localVariables.newFieldInRecord("pnd_Total_Upds", "#TOTAL-UPDS", FieldType.NUMERIC, 9);
        pnd_Total_Auds = localVariables.newFieldInRecord("pnd_Total_Auds", "#TOTAL-AUDS", FieldType.NUMERIC, 9);
        pnd_Icw_Adds = localVariables.newFieldInRecord("pnd_Icw_Adds", "#ICW-ADDS", FieldType.NUMERIC, 9);
        pnd_Cwf_Adds = localVariables.newFieldInRecord("pnd_Cwf_Adds", "#CWF-ADDS", FieldType.NUMERIC, 9);
        pnd_Ncw_Adds = localVariables.newFieldInRecord("pnd_Ncw_Adds", "#NCW-ADDS", FieldType.NUMERIC, 9);
        pnd_Icw_Upds = localVariables.newFieldInRecord("pnd_Icw_Upds", "#ICW-UPDS", FieldType.NUMERIC, 9);
        pnd_Cwf_Upds = localVariables.newFieldInRecord("pnd_Cwf_Upds", "#CWF-UPDS", FieldType.NUMERIC, 9);
        pnd_Ncw_Upds = localVariables.newFieldInRecord("pnd_Ncw_Upds", "#NCW-UPDS", FieldType.NUMERIC, 9);
        pnd_Icw_Dels = localVariables.newFieldInRecord("pnd_Icw_Dels", "#ICW-DELS", FieldType.NUMERIC, 9);
        pnd_Cwf_Dels = localVariables.newFieldInRecord("pnd_Cwf_Dels", "#CWF-DELS", FieldType.NUMERIC, 9);
        pnd_Ncw_Dels = localVariables.newFieldInRecord("pnd_Ncw_Dels", "#NCW-DELS", FieldType.NUMERIC, 9);
        pnd_Icw_Auds = localVariables.newFieldInRecord("pnd_Icw_Auds", "#ICW-AUDS", FieldType.NUMERIC, 9);
        pnd_Cwf_Auds = localVariables.newFieldInRecord("pnd_Cwf_Auds", "#CWF-AUDS", FieldType.NUMERIC, 9);
        pnd_Ncw_Auds = localVariables.newFieldInRecord("pnd_Ncw_Auds", "#NCW-AUDS", FieldType.NUMERIC, 9);
        pnd_Icw_U_Total = localVariables.newFieldInRecord("pnd_Icw_U_Total", "#ICW-U-TOTAL", FieldType.NUMERIC, 9);
        pnd_Cwf_U_Total = localVariables.newFieldInRecord("pnd_Cwf_U_Total", "#CWF-U-TOTAL", FieldType.NUMERIC, 9);
        pnd_Ncw_U_Total = localVariables.newFieldInRecord("pnd_Ncw_U_Total", "#NCW-U-TOTAL", FieldType.NUMERIC, 9);
        pnd_Total_Processed = localVariables.newFieldInRecord("pnd_Total_Processed", "#TOTAL-PROCESSED", FieldType.NUMERIC, 9);
        pnd_Icw_Week_Avg_Del = localVariables.newFieldInRecord("pnd_Icw_Week_Avg_Del", "#ICW-WEEK-AVG-DEL", FieldType.NUMERIC, 9);
        pnd_Initial_Adds = localVariables.newFieldInRecord("pnd_Initial_Adds", "#INITIAL-ADDS", FieldType.NUMERIC, 9);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 5);
        pnd_Counter = localVariables.newFieldArrayInRecord("pnd_Counter", "#COUNTER", FieldType.NUMERIC, 9, new DbsArrayController(1, 7));
        pnd_Counter_Total = localVariables.newFieldInRecord("pnd_Counter_Total", "#COUNTER-TOTAL", FieldType.NUMERIC, 9);
        pnd_Counter_Total2 = localVariables.newFieldInRecord("pnd_Counter_Total2", "#COUNTER-TOTAL2", FieldType.NUMERIC, 9);
        pnd_Counter_Total3 = localVariables.newFieldInRecord("pnd_Counter_Total3", "#COUNTER-TOTAL3", FieldType.NUMERIC, 9);
        pnd_Counter1 = localVariables.newFieldArrayInRecord("pnd_Counter1", "#COUNTER1", FieldType.NUMERIC, 9, new DbsArrayController(1, 7));
        pnd_Counter2 = localVariables.newFieldInRecord("pnd_Counter2", "#COUNTER2", FieldType.NUMERIC, 9);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pnd_Invrtd_Date_Stamp_2 = localVariables.newFieldInRecord("pnd_Invrtd_Date_Stamp_2", "#INVRTD-DATE-STAMP-2", FieldType.NUMERIC, 8);
        pnd_Invert_Current_Date = localVariables.newFieldInRecord("pnd_Invert_Current_Date", "#INVERT-CURRENT-DATE", FieldType.NUMERIC, 8);
        pnd_Delete_Date_D = localVariables.newFieldInRecord("pnd_Delete_Date_D", "#DELETE-DATE-D", FieldType.DATE);
        pnd_Current_Date_D = localVariables.newFieldInRecord("pnd_Current_Date_D", "#CURRENT-DATE-D", FieldType.DATE);
        pnd_Current_Date_A = localVariables.newFieldInRecord("pnd_Current_Date_A", "#CURRENT-DATE-A", FieldType.STRING, 8);
        pnd_Delete_Date = localVariables.newFieldInRecord("pnd_Delete_Date", "#DELETE-DATE", FieldType.NUMERIC, 8);
        pnd_Delete_Date_N = localVariables.newFieldInRecord("pnd_Delete_Date_N", "#DELETE-DATE-N", FieldType.NUMERIC, 8);

        pnd_Delete_Date_N__R_Field_5 = localVariables.newGroupInRecord("pnd_Delete_Date_N__R_Field_5", "REDEFINE", pnd_Delete_Date_N);
        pnd_Delete_Date_N_Pnd_Delete_Date_A = pnd_Delete_Date_N__R_Field_5.newFieldInGroup("pnd_Delete_Date_N_Pnd_Delete_Date_A", "#DELETE-DATE-A", FieldType.STRING, 
            8);
        pnd_Delete_Date_2 = localVariables.newFieldInRecord("pnd_Delete_Date_2", "#DELETE-DATE-2", FieldType.NUMERIC, 8);
        pnd_Ddcounter = localVariables.newFieldInRecord("pnd_Ddcounter", "#DDCOUNTER", FieldType.NUMERIC, 9);
        pnd_Ddcounter_2 = localVariables.newFieldInRecord("pnd_Ddcounter_2", "#DDCOUNTER-2", FieldType.NUMERIC, 9);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_6", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Array = localVariables.newFieldArrayInRecord("pnd_Array", "#ARRAY", FieldType.NUMERIC, 9, new DbsArrayController(1, 36, 1, 8));
        pnd_Font = localVariables.newFieldInRecord("pnd_Font", "#FONT", FieldType.STRING, 54);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf.reset();

        localVariables.reset();
        pnd_Font.setInitialValue("-�E�&l1o2a5.6c72p3e66F�&a2L�(9905X");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iwfp4700() throws Exception
    {
        super("Iwfp4700");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( 01 ) LS = 133 PS = 60 ZP = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
        pnd_Page.setValue(getReports().getPageNumberDbs(0));                                                                                                              //Natural: ASSIGN #PAGE = *PAGE-NUMBER
        vw_cwf.startDatabaseRead                                                                                                                                          //Natural: READ ( 1 ) CWF BY TBL-PRIME-KEY = 'A ENHANCED-WK-LST-RUNS'
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", "A ENHANCED-WK-LST-RUNS", WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cwf.readNextRow("READ01")))
        {
            if (condition(cwf_Tbl_Scrty_Level_Ind.equals("A") && cwf_Tbl_Table_Nme.equals("ENHANCED-WK-LST-RUNS")))                                                       //Natural: IF TBL-SCRTY-LEVEL-IND = 'A' AND TBL-TABLE-NME = 'ENHANCED-WK-LST-RUNS'
            {
                pnd_Day_Counter.setValue(1);                                                                                                                              //Natural: ASSIGN #DAY-COUNTER = #X = 1
                pnd_X.setValue(1);
                pnd_Current_Rec_Date.setValue(cwf_Pnd_Start_Date);                                                                                                        //Natural: ASSIGN #CURRENT-REC-DATE = #START-DATE
                pnd_Current_Rec_Date1.setValue(pnd_Current_Rec_Date);                                                                                                     //Natural: ASSIGN #CURRENT-REC-DATE1 := #CURRENT-REC-DATE
                //* *ELSE
                //* **WRITE ERROR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_cwf.startDatabaseRead                                                                                                                                          //Natural: READ CWF BY TBL-PRIME-KEY = 'A ENHANCED-WK-LST-RUNS'
        (
        "READ02",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", "A ENHANCED-WK-LST-RUNS", WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf.readNextRow("READ02")))
        {
            if (condition(cwf_Tbl_Scrty_Level_Ind.notEquals("A") || cwf_Tbl_Table_Nme.notEquals("ENHANCED-WK-LST-RUNS")))                                                 //Natural: IF TBL-SCRTY-LEVEL-IND NE 'A' OR TBL-TABLE-NME NE 'ENHANCED-WK-LST-RUNS'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Current_Rec_Date.notEquals(cwf_Pnd_Start_Date)))                                                                                            //Natural: IF #CURRENT-REC-DATE NE #START-DATE
            {
                pnd_Day_Counter.nadd(1);                                                                                                                                  //Natural: ASSIGN #DAY-COUNTER := #DAY-COUNTER + 1
                pnd_Current_Rec_Date.setValue(cwf_Pnd_Start_Date);                                                                                                        //Natural: ASSIGN #CURRENT-REC-DATE := #START-DATE
                pnd_X.setValue(pnd_Day_Counter);                                                                                                                          //Natural: ASSIGN #X := #DAY-COUNTER
                short decideConditionsMet175 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #X;//Natural: VALUE 2
                if (condition((pnd_X.equals(2))))
                {
                    decideConditionsMet175++;
                    pnd_Current_Rec_Date2.setValue(pnd_Current_Rec_Date);                                                                                                 //Natural: ASSIGN #CURRENT-REC-DATE2 := #CURRENT-REC-DATE
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_X.equals(3))))
                {
                    decideConditionsMet175++;
                    pnd_Current_Rec_Date3.setValue(pnd_Current_Rec_Date);                                                                                                 //Natural: ASSIGN #CURRENT-REC-DATE3 := #CURRENT-REC-DATE
                }                                                                                                                                                         //Natural: VALUE 4
                else if (condition((pnd_X.equals(4))))
                {
                    decideConditionsMet175++;
                    pnd_Current_Rec_Date4.setValue(pnd_Current_Rec_Date);                                                                                                 //Natural: ASSIGN #CURRENT-REC-DATE4 := #CURRENT-REC-DATE
                }                                                                                                                                                         //Natural: VALUE 5
                else if (condition((pnd_X.equals(5))))
                {
                    decideConditionsMet175++;
                    pnd_Current_Rec_Date5.setValue(pnd_Current_Rec_Date);                                                                                                 //Natural: ASSIGN #CURRENT-REC-DATE5 := #CURRENT-REC-DATE
                }                                                                                                                                                         //Natural: VALUE 6
                else if (condition((pnd_X.equals(6))))
                {
                    decideConditionsMet175++;
                    pnd_Current_Rec_Date6.setValue(pnd_Current_Rec_Date);                                                                                                 //Natural: ASSIGN #CURRENT-REC-DATE6 := #CURRENT-REC-DATE
                }                                                                                                                                                         //Natural: VALUE 7
                else if (condition((pnd_X.equals(7))))
                {
                    decideConditionsMet175++;
                    pnd_Current_Rec_Date7.setValue(pnd_Current_Rec_Date);                                                                                                 //Natural: ASSIGN #CURRENT-REC-DATE7 := #CURRENT-REC-DATE
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Day_Counter.equals(8)))                                                                                                                     //Natural: IF #DAY-COUNTER = 8
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //* ***********************************************************************
            //*  INITIAL ICW/CWF/NCW ADDS
            //* ***********************************************************************
            if (condition(cwf_Pnd_Initial_Load.equals("I")))                                                                                                              //Natural: IF #INITIAL-LOAD = 'I'
            {
                pnd_Counter1.getValue(pnd_X).nadd(1);                                                                                                                     //Natural: COMPUTE #COUNTER1 ( #X ) = #COUNTER1 ( #X ) + 1
                pnd_Array.getValue(1,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(1,pnd_X)), cwf_Pnd_Icw_Total_Recs_Added_Ctr.add(pnd_Array.getValue(1, //Natural: COMPUTE #ARRAY ( 1,#X ) = #ICW-TOTAL-RECS-ADDED-CTR + #ARRAY ( 1,#X )
                    pnd_X)));
                pnd_Array.getValue(2,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(2,pnd_X)), cwf_Pnd_Cwf_Total_Recs_Added_Ctr.add(pnd_Array.getValue(2, //Natural: COMPUTE #ARRAY ( 2,#X ) = #CWF-TOTAL-RECS-ADDED-CTR + #ARRAY ( 2,#X )
                    pnd_X)));
                pnd_Array.getValue(3,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(3,pnd_X)), cwf_Pnd_Ncw_Total_Recs_Added_Ctr.add(pnd_Array.getValue(3, //Natural: COMPUTE #ARRAY ( 3,#X ) = #NCW-TOTAL-RECS-ADDED-CTR + #ARRAY ( 3,#X )
                    pnd_X)));
            }                                                                                                                                                             //Natural: END-IF
            //* ***********************************************************************
            //*  ICW/CWF/NCW UPD AND DEL CALCULATIONS
            //* ***********************************************************************
            if (condition(cwf_Pnd_Initial_Load.equals("U")))                                                                                                              //Natural: IF #INITIAL-LOAD = 'U'
            {
                pnd_Counter.getValue(pnd_X).nadd(1);                                                                                                                      //Natural: COMPUTE #COUNTER ( #X ) = #COUNTER ( #X ) + 1
                pnd_Array.getValue(6,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(6,pnd_X)), cwf_Pnd_Icw_Total_Recs_Updated_Ctr.add(pnd_Array.getValue(6, //Natural: COMPUTE #ARRAY ( 6,#X ) = #ICW-TOTAL-RECS-UPDATED-CTR + #ARRAY ( 6,#X )
                    pnd_X)));
                pnd_Array.getValue(7,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(7,pnd_X)), cwf_Pnd_Icw_Total_Recs_Deleted_Ctr.add(pnd_Array.getValue(7, //Natural: COMPUTE #ARRAY ( 7,#X ) = #ICW-TOTAL-RECS-DELETED-CTR + #ARRAY ( 7,#X )
                    pnd_X)));
                pnd_Array.getValue(14,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(14,pnd_X)), cwf_Pnd_Cwf_Total_Recs_Updated_Ctr.add(pnd_Array.getValue(14, //Natural: COMPUTE #ARRAY ( 14,#X ) = #CWF-TOTAL-RECS-UPDATED-CTR + #ARRAY ( 14,#X )
                    pnd_X)));
                pnd_Array.getValue(15,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(15,pnd_X)), cwf_Pnd_Cwf_Total_Recs_Deleted_Ctr.add(pnd_Array.getValue(15, //Natural: COMPUTE #ARRAY ( 15,#X ) = #CWF-TOTAL-RECS-DELETED-CTR + #ARRAY ( 15,#X )
                    pnd_X)));
                pnd_Array.getValue(22,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(22,pnd_X)), cwf_Pnd_Ncw_Total_Recs_Updated_Ctr.add(pnd_Array.getValue(22, //Natural: COMPUTE #ARRAY ( 22,#X ) = #NCW-TOTAL-RECS-UPDATED-CTR + #ARRAY ( 22,#X )
                    pnd_X)));
                pnd_Array.getValue(23,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(23,pnd_X)), cwf_Pnd_Ncw_Total_Recs_Deleted_Ctr.add(pnd_Array.getValue(23, //Natural: COMPUTE #ARRAY ( 23,#X ) = #NCW-TOTAL-RECS-DELETED-CTR + #ARRAY ( 23,#X )
                    pnd_X)));
                //* ***********************************************************************
                //*  PERIODIC ICW/CWF/NCW ADDS
                //* ***********************************************************************
                pnd_Array.getValue(5,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(5,pnd_X)), cwf_Pnd_Icw_Total_Recs_Added_Ctr.add(pnd_Array.getValue(5, //Natural: COMPUTE #ARRAY ( 5,#X ) = #ICW-TOTAL-RECS-ADDED-CTR + #ARRAY ( 5,#X )
                    pnd_X)));
                pnd_Array.getValue(13,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(13,pnd_X)), cwf_Pnd_Cwf_Total_Recs_Added_Ctr.add(pnd_Array.getValue(13, //Natural: COMPUTE #ARRAY ( 13,#X ) = #CWF-TOTAL-RECS-ADDED-CTR + #ARRAY ( 13,#X )
                    pnd_X)));
                pnd_Array.getValue(21,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(21,pnd_X)), cwf_Pnd_Ncw_Total_Recs_Added_Ctr.add(pnd_Array.getValue(21, //Natural: COMPUTE #ARRAY ( 21,#X ) = #NCW-TOTAL-RECS-ADDED-CTR + #ARRAY ( 21,#X )
                    pnd_X)));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Pnd_Initial_Load.equals("I")))                                                                                                              //Natural: IF #INITIAL-LOAD = 'I'
            {
                pnd_Icw_U_Total.setValue(cwf_Pnd_Icw_Total_Recs_Added_Ctr);                                                                                               //Natural: ASSIGN #ICW-U-TOTAL := #ICW-TOTAL-RECS-ADDED-CTR
                pnd_Cwf_U_Total.setValue(cwf_Pnd_Cwf_Total_Recs_Added_Ctr);                                                                                               //Natural: ASSIGN #CWF-U-TOTAL := #CWF-TOTAL-RECS-ADDED-CTR
                pnd_Ncw_U_Total.setValue(cwf_Pnd_Ncw_Total_Recs_Added_Ctr);                                                                                               //Natural: ASSIGN #NCW-U-TOTAL := #NCW-TOTAL-RECS-ADDED-CTR
                pnd_Total_Processed.compute(new ComputeParameters(false, pnd_Total_Processed), pnd_Icw_U_Total.add(pnd_Cwf_U_Total).add(pnd_Ncw_U_Total));                //Natural: COMPUTE #TOTAL-PROCESSED = #ICW-U-TOTAL + #CWF-U-TOTAL + #NCW-U-TOTAL
                cwf_Pnd_Initial_Load.setValue("I");                                                                                                                       //Natural: ASSIGN #INITIAL-LOAD := 'I'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Pnd_Initial_Load.equals("U")))                                                                                                              //Natural: IF #INITIAL-LOAD = 'U'
            {
                pnd_Icw_U_Total.compute(new ComputeParameters(false, pnd_Icw_U_Total), cwf_Pnd_Icw_Total_Recs_Added_Ctr.add(cwf_Pnd_Icw_Total_Recs_Updated_Ctr).add(cwf_Pnd_Icw_Total_Recs_Deleted_Ctr)); //Natural: COMPUTE #ICW-U-TOTAL = #ICW-TOTAL-RECS-ADDED-CTR + #ICW-TOTAL-RECS-UPDATED-CTR + #ICW-TOTAL-RECS-DELETED-CTR
                pnd_Cwf_U_Total.compute(new ComputeParameters(false, pnd_Cwf_U_Total), cwf_Pnd_Cwf_Total_Recs_Added_Ctr.add(cwf_Pnd_Cwf_Total_Recs_Updated_Ctr).add(cwf_Pnd_Cwf_Total_Recs_Deleted_Ctr)); //Natural: COMPUTE #CWF-U-TOTAL = #CWF-TOTAL-RECS-ADDED-CTR + #CWF-TOTAL-RECS-UPDATED-CTR + #CWF-TOTAL-RECS-DELETED-CTR
                pnd_Ncw_U_Total.compute(new ComputeParameters(false, pnd_Ncw_U_Total), cwf_Pnd_Ncw_Total_Recs_Added_Ctr.add(cwf_Pnd_Ncw_Total_Recs_Updated_Ctr).add(cwf_Pnd_Ncw_Total_Recs_Deleted_Ctr)); //Natural: COMPUTE #NCW-U-TOTAL = #NCW-TOTAL-RECS-ADDED-CTR + #NCW-TOTAL-RECS-UPDATED-CTR + #NCW-TOTAL-RECS-DELETED-CTR
                pnd_Total_Processed.compute(new ComputeParameters(false, pnd_Total_Processed), pnd_Icw_U_Total.add(pnd_Cwf_U_Total).add(pnd_Ncw_U_Total));                //Natural: COMPUTE #TOTAL-PROCESSED = #ICW-U-TOTAL + #CWF-U-TOTAL + #NCW-U-TOTAL
                cwf_Pnd_Initial_Load.setValue("U");                                                                                                                       //Natural: ASSIGN #INITIAL-LOAD := 'U'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (DbsUtil.maskMatches(cwf_Pnd_Start_Time,"9999999"))))                                                                                         //Natural: IF #START-TIME NE MASK ( 9999999 )
            {
                cwf_Pnd_Start_Time.setValue(0);                                                                                                                           //Natural: ASSIGN #START-TIME := 0000000
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (DbsUtil.maskMatches(cwf_Pnd_End_Time,"9999999"))))                                                                                           //Natural: IF #END-TIME NE MASK ( 9999999 )
            {
                cwf_Pnd_End_Time.setValue(0);                                                                                                                             //Natural: ASSIGN #END-TIME := 0000000
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, pnd_Current_Rec_Date, pnd_Total_Processed, cwf_Pnd_Initial_Load, cwf_Pnd_Start_Time, cwf_Pnd_End_Time, pnd_Icw_U_Total,        //Natural: WRITE WORK FILE 01 #CURRENT-REC-DATE #TOTAL-PROCESSED #INITIAL-LOAD #START-TIME #END-TIME #ICW-U-TOTAL #CWF-U-TOTAL #NCW-U-TOTAL
                pnd_Cwf_U_Total, pnd_Ncw_U_Total);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  DELETE ALL RECORDS PAST 21 DAYS OLD...ZJ 7/30 BEGIN
        //* ***********************************************************************
        pnd_Current_Date_A.setValue(pnd_Current_Rec_Date1);                                                                                                               //Natural: MOVE #CURRENT-REC-DATE1 TO #CURRENT-DATE-A
        pnd_Current_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Current_Date_A);                                                                             //Natural: MOVE EDITED #CURRENT-DATE-A TO #CURRENT-DATE-D ( EM = YYYYMMDD )
        pnd_Delete_Date_D.compute(new ComputeParameters(false, pnd_Delete_Date_D), pnd_Current_Date_D.subtract(21));                                                      //Natural: COMPUTE #DELETE-DATE-D = #CURRENT-DATE-D - 21
        pnd_Delete_Date_N_Pnd_Delete_Date_A.setValueEdited(pnd_Delete_Date_D,new ReportEditMask("YYYYMMDD"));                                                             //Natural: MOVE EDITED #DELETE-DATE-D ( EM = YYYYMMDD ) TO #DELETE-DATE-A
        pnd_Delete_Date.compute(new ComputeParameters(false, pnd_Delete_Date), DbsField.subtract(99999999,pnd_Delete_Date_N));                                            //Natural: COMPUTE #DELETE-DATE = 99999999 - #DELETE-DATE-N
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A ");                                                                                                         //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND = 'A '
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("ENHANCED-WK-LST-RUNS");                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME = 'ENHANCED-WK-LST-RUNS'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue(pnd_Delete_Date);                                                                                                    //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-KEY-FIELD = #DELETE-DATE
        pnd_Invrtd_Date_Stamp_2.reset();                                                                                                                                  //Natural: RESET #INVRTD-DATE-STAMP-2 #DDCOUNTER #DDCOUNTER-2 #DELETE-DATE-2
        pnd_Ddcounter.reset();
        pnd_Ddcounter_2.reset();
        pnd_Delete_Date_2.reset();
        vw_cwf.startDatabaseRead                                                                                                                                          //Natural: READ CWF BY TBL-PRIME-KEY FROM #TBL-PRIME-KEY
        (
        "READ03",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_cwf.readNextRow("READ03")))
        {
            if (condition(cwf_Tbl_Scrty_Level_Ind.notEquals("A") || cwf_Tbl_Table_Nme.notEquals("ENHANCED-WK-LST-RUNS")))                                                 //Natural: IF TBL-SCRTY-LEVEL-IND NE 'A' OR TBL-TABLE-NME NE 'ENHANCED-WK-LST-RUNS'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ddcounter.nadd(1);                                                                                                                                        //Natural: COMPUTE #DDCOUNTER = #DDCOUNTER + 1
            pnd_Ddcounter_2.nadd(1);                                                                                                                                      //Natural: COMPUTE #DDCOUNTER-2 = #DDCOUNTER-2 + 1
            if (condition(pnd_Ddcounter_2.equals(1)))                                                                                                                     //Natural: IF #DDCOUNTER-2 = 1
            {
                pnd_Delete_Date_2.compute(new ComputeParameters(false, pnd_Delete_Date_2), DbsField.subtract(99999999,cwf_Pnd_Invrtd_Date_Stamp));                        //Natural: COMPUTE #DELETE-DATE-2 = 99999999 - #INVRTD-DATE-STAMP
            }                                                                                                                                                             //Natural: END-IF
            vw_cwf.deleteDBRow("READ03");                                                                                                                                 //Natural: DELETE
            pnd_Invrtd_Date_Stamp_2.setValue(cwf_Pnd_Start_Date);                                                                                                         //Natural: ASSIGN #INVRTD-DATE-STAMP-2 = #START-DATE
            if (condition(pnd_Ddcounter.equals(100)))                                                                                                                     //Natural: IF #DDCOUNTER = 100
            {
                pnd_Ddcounter.reset();                                                                                                                                    //Natural: RESET #DDCOUNTER
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* * ZJ 7/30 END
        //* ***********************************************************************
        //*  INITIAL/PERIODIC/DAILY TOTALS - INCLUDING DAILY ADDS/UPDATES/DELETES
        //* ***********************************************************************
        FOR01:                                                                                                                                                            //Natural: FOR #X = 1 7
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(7)); pnd_X.nadd(1))
        {
            pnd_Initial_Adds.compute(new ComputeParameters(false, pnd_Initial_Adds), pnd_Array.getValue(1,pnd_X).add(pnd_Array.getValue(2,pnd_X)).add(pnd_Array.getValue(3, //Natural: COMPUTE #INITIAL-ADDS = #ARRAY ( 1,#X ) + #ARRAY ( 2,#X ) + #ARRAY ( 3,#X )
                pnd_X)));
            pnd_Array.getValue(4,pnd_X).setValue(pnd_Initial_Adds);                                                                                                       //Natural: ASSIGN #ARRAY ( 4,#X ) := #INITIAL-ADDS
            pnd_Array.getValue(8,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(8,pnd_X)), pnd_Array.getValue(5,pnd_X).add(pnd_Array.getValue(6,          //Natural: COMPUTE #ARRAY ( 8,#X ) = #ARRAY ( 5,#X ) + #ARRAY ( 6,#X ) + #ARRAY ( 7,#X )
                pnd_X)).add(pnd_Array.getValue(7,pnd_X)));
            pnd_Array.getValue(16,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(16,pnd_X)), pnd_Array.getValue(13,pnd_X).add(pnd_Array.getValue(14,      //Natural: COMPUTE #ARRAY ( 16,#X ) = #ARRAY ( 13,#X ) + #ARRAY ( 14,#X ) + #ARRAY ( 15,#X )
                pnd_X)).add(pnd_Array.getValue(15,pnd_X)));
            pnd_Array.getValue(24,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(24,pnd_X)), pnd_Array.getValue(21,pnd_X).add(pnd_Array.getValue(22,      //Natural: COMPUTE #ARRAY ( 24,#X ) = #ARRAY ( 21,#X ) + #ARRAY ( 22,#X ) + #ARRAY ( 23,#X )
                pnd_X)).add(pnd_Array.getValue(23,pnd_X)));
            pnd_Array.getValue(29,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(29,pnd_X)), pnd_Array.getValue(5,pnd_X).add(pnd_Array.getValue(13,       //Natural: COMPUTE #ARRAY ( 29,#X ) = #ARRAY ( 5,#X ) + #ARRAY ( 13,#X ) + #ARRAY ( 21,#X )
                pnd_X)).add(pnd_Array.getValue(21,pnd_X)));
            pnd_Array.getValue(30,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(30,pnd_X)), pnd_Array.getValue(6,pnd_X).add(pnd_Array.getValue(14,       //Natural: COMPUTE #ARRAY ( 30,#X ) = #ARRAY ( 6,#X ) + #ARRAY ( 14,#X ) + #ARRAY ( 22,#X )
                pnd_X)).add(pnd_Array.getValue(22,pnd_X)));
            pnd_Array.getValue(31,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(31,pnd_X)), pnd_Array.getValue(7,pnd_X).add(pnd_Array.getValue(15,       //Natural: COMPUTE #ARRAY ( 31,#X ) = #ARRAY ( 7,#X ) + #ARRAY ( 15,#X ) + #ARRAY ( 23,#X )
                pnd_X)).add(pnd_Array.getValue(23,pnd_X)));
            pnd_Array.getValue(32,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(32,pnd_X)), pnd_Array.getValue(29,pnd_X).add(pnd_Array.getValue(30,      //Natural: COMPUTE #ARRAY ( 32,#X ) = #ARRAY ( 29,#X ) + #ARRAY ( 30,#X ) + #ARRAY ( 31,#X )
                pnd_X)).add(pnd_Array.getValue(31,pnd_X)));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  WEEKLY TOTALS
        //* ***********************************************************************
        pnd_Array.getValue(1,8).compute(new ComputeParameters(false, pnd_Array.getValue(1,8)), pnd_Array.getValue(1,1,":",7).add(getZero()));                             //Natural: COMPUTE #ARRAY ( 1,8 ) = #ARRAY ( 1,1:7 ) + 0
        pnd_Array.getValue(2,8).compute(new ComputeParameters(false, pnd_Array.getValue(2,8)), pnd_Array.getValue(2,1,":",7).add(getZero()));                             //Natural: COMPUTE #ARRAY ( 2,8 ) = #ARRAY ( 2,1:7 ) + 0
        pnd_Array.getValue(3,8).compute(new ComputeParameters(false, pnd_Array.getValue(3,8)), pnd_Array.getValue(3,1,":",7).add(getZero()));                             //Natural: COMPUTE #ARRAY ( 3,8 ) = #ARRAY ( 3,1:7 ) + 0
        pnd_Array.getValue(4,8).compute(new ComputeParameters(false, pnd_Array.getValue(4,8)), pnd_Array.getValue(4,1,":",7).add(getZero()));                             //Natural: COMPUTE #ARRAY ( 4,8 ) = #ARRAY ( 4,1:7 ) + 0
        pnd_Icw_Adds.compute(new ComputeParameters(false, pnd_Icw_Adds), pnd_Array.getValue(5,1,":",7).add(getZero()));                                                   //Natural: COMPUTE #ICW-ADDS = #ARRAY ( 5,1:7 ) + 0
        pnd_Array.getValue(5,8).setValue(pnd_Icw_Adds);                                                                                                                   //Natural: ASSIGN #ARRAY ( 5,8 ) := #ICW-ADDS
        pnd_Cwf_Adds.compute(new ComputeParameters(false, pnd_Cwf_Adds), pnd_Array.getValue(13,1,":",7).add(getZero()));                                                  //Natural: COMPUTE #CWF-ADDS = #ARRAY ( 13,1:7 ) + 0
        pnd_Array.getValue(13,8).setValue(pnd_Cwf_Adds);                                                                                                                  //Natural: ASSIGN #ARRAY ( 13,8 ) := #CWF-ADDS
        pnd_Ncw_Adds.compute(new ComputeParameters(false, pnd_Ncw_Adds), pnd_Array.getValue(21,1,":",7).add(getZero()));                                                  //Natural: COMPUTE #NCW-ADDS = #ARRAY ( 21,1:7 ) + 0
        pnd_Array.getValue(21,8).setValue(pnd_Ncw_Adds);                                                                                                                  //Natural: ASSIGN #ARRAY ( 21,8 ) := #NCW-ADDS
        pnd_Icw_Upds.compute(new ComputeParameters(false, pnd_Icw_Upds), pnd_Array.getValue(6,1,":",7).add(getZero()));                                                   //Natural: COMPUTE #ICW-UPDS = #ARRAY ( 6,1:7 ) + 0
        pnd_Array.getValue(6,8).setValue(pnd_Icw_Upds);                                                                                                                   //Natural: ASSIGN #ARRAY ( 6,8 ) := #ICW-UPDS
        pnd_Cwf_Upds.compute(new ComputeParameters(false, pnd_Cwf_Upds), pnd_Array.getValue(14,1,":",7).add(getZero()));                                                  //Natural: COMPUTE #CWF-UPDS = #ARRAY ( 14,1:7 ) + 0
        pnd_Array.getValue(14,8).setValue(pnd_Cwf_Upds);                                                                                                                  //Natural: ASSIGN #ARRAY ( 14,8 ) := #CWF-UPDS
        pnd_Ncw_Upds.compute(new ComputeParameters(false, pnd_Ncw_Upds), pnd_Array.getValue(22,1,":",7).add(getZero()));                                                  //Natural: COMPUTE #NCW-UPDS = #ARRAY ( 22,1:7 ) + 0
        pnd_Array.getValue(22,8).setValue(pnd_Ncw_Upds);                                                                                                                  //Natural: ASSIGN #ARRAY ( 22,8 ) := #NCW-UPDS
        pnd_Icw_Dels.compute(new ComputeParameters(false, pnd_Icw_Dels), pnd_Array.getValue(7,1,":",7).add(getZero()));                                                   //Natural: COMPUTE #ICW-DELS = #ARRAY ( 7,1:7 ) + 0
        pnd_Array.getValue(7,8).setValue(pnd_Icw_Dels);                                                                                                                   //Natural: ASSIGN #ARRAY ( 7,8 ) := #ICW-DELS
        pnd_Cwf_Dels.compute(new ComputeParameters(false, pnd_Cwf_Dels), pnd_Array.getValue(15,1,":",7).add(getZero()));                                                  //Natural: COMPUTE #CWF-DELS = #ARRAY ( 15,1:7 ) + 0
        pnd_Array.getValue(15,8).setValue(pnd_Cwf_Dels);                                                                                                                  //Natural: ASSIGN #ARRAY ( 15,8 ) := #CWF-DELS
        pnd_Ncw_Dels.compute(new ComputeParameters(false, pnd_Ncw_Dels), pnd_Array.getValue(23,1,":",7).add(getZero()));                                                  //Natural: COMPUTE #NCW-DELS = #ARRAY ( 23,1:7 ) + 0
        pnd_Array.getValue(23,8).setValue(pnd_Ncw_Dels);                                                                                                                  //Natural: ASSIGN #ARRAY ( 23,8 ) := #NCW-DELS
        pnd_Icw_Auds.compute(new ComputeParameters(false, pnd_Icw_Auds), pnd_Array.getValue(8,1,":",7).add(getZero()));                                                   //Natural: COMPUTE #ICW-AUDS = #ARRAY ( 8,1:7 ) + 0
        pnd_Array.getValue(8,8).setValue(pnd_Icw_Auds);                                                                                                                   //Natural: ASSIGN #ARRAY ( 8,8 ) := #ICW-AUDS
        pnd_Cwf_Auds.compute(new ComputeParameters(false, pnd_Cwf_Auds), pnd_Array.getValue(16,1,":",7).add(getZero()));                                                  //Natural: COMPUTE #CWF-AUDS = #ARRAY ( 16,1:7 ) + 0
        pnd_Array.getValue(16,8).setValue(pnd_Cwf_Auds);                                                                                                                  //Natural: ASSIGN #ARRAY ( 16,8 ) := #CWF-AUDS
        pnd_Ncw_Auds.compute(new ComputeParameters(false, pnd_Ncw_Auds), pnd_Array.getValue(24,1,":",7).add(getZero()));                                                  //Natural: COMPUTE #NCW-AUDS = #ARRAY ( 24,1:7 ) + 0
        pnd_Array.getValue(24,8).setValue(pnd_Ncw_Auds);                                                                                                                  //Natural: ASSIGN #ARRAY ( 24,8 ) := #NCW-AUDS
        pnd_Total_Adds.compute(new ComputeParameters(false, pnd_Total_Adds), pnd_Array.getValue(29,1,":",7).add(getZero()));                                              //Natural: COMPUTE #TOTAL-ADDS = #ARRAY ( 29,1:7 ) + 0
        pnd_Array.getValue(29,8).setValue(pnd_Total_Adds);                                                                                                                //Natural: ASSIGN #ARRAY ( 29,8 ) := #TOTAL-ADDS
        pnd_Total_Upds.compute(new ComputeParameters(false, pnd_Total_Upds), pnd_Array.getValue(30,1,":",7).add(getZero()));                                              //Natural: COMPUTE #TOTAL-UPDS = #ARRAY ( 30,1:7 ) + 0
        pnd_Array.getValue(30,8).setValue(pnd_Total_Upds);                                                                                                                //Natural: ASSIGN #ARRAY ( 30,8 ) := #TOTAL-UPDS
        pnd_Total_Dels.compute(new ComputeParameters(false, pnd_Total_Dels), pnd_Array.getValue(31,1,":",7).add(getZero()));                                              //Natural: COMPUTE #TOTAL-DELS = #ARRAY ( 31,1:7 ) + 0
        pnd_Array.getValue(31,8).setValue(pnd_Total_Dels);                                                                                                                //Natural: ASSIGN #ARRAY ( 31,8 ) := #TOTAL-DELS
        pnd_Total_Auds.compute(new ComputeParameters(false, pnd_Total_Auds), pnd_Array.getValue(32,1,":",7).add(getZero()));                                              //Natural: COMPUTE #TOTAL-AUDS = #ARRAY ( 32,1:7 ) + 0
        pnd_Array.getValue(32,8).setValue(pnd_Total_Auds);                                                                                                                //Natural: ASSIGN #ARRAY ( 32,8 ) := #TOTAL-AUDS
        //* ***********************************************************************
        //*  AVERAGES
        //* ***********************************************************************
        FOR02:                                                                                                                                                            //Natural: FOR #X = 1 7
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(7)); pnd_X.nadd(1))
        {
            if (condition(pnd_Counter.getValue(pnd_X).equals(getZero())))                                                                                                 //Natural: IF #COUNTER ( #X ) = 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Array.getValue(9,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(9,pnd_X)), pnd_Array.getValue(5,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 5,#X ) GIVING #ARRAY ( 9,#X )
            pnd_Array.getValue(10,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(10,pnd_X)), pnd_Array.getValue(6,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 6,#X ) GIVING #ARRAY ( 10,#X )
            pnd_Array.getValue(11,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(11,pnd_X)), pnd_Array.getValue(7,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 7,#X ) GIVING #ARRAY ( 11,#X )
            pnd_Array.getValue(17,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(17,pnd_X)), pnd_Array.getValue(13,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 13,#X ) GIVING #ARRAY ( 17,#X )
            pnd_Array.getValue(18,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(18,pnd_X)), pnd_Array.getValue(14,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 14,#X ) GIVING #ARRAY ( 18,#X )
            pnd_Array.getValue(19,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(19,pnd_X)), pnd_Array.getValue(15,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 15,#X ) GIVING #ARRAY ( 19,#X )
            pnd_Array.getValue(25,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(25,pnd_X)), pnd_Array.getValue(21,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 21,#X ) GIVING #ARRAY ( 25,#X )
            pnd_Array.getValue(26,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(26,pnd_X)), pnd_Array.getValue(22,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 22,#X ) GIVING #ARRAY ( 26,#X )
            pnd_Array.getValue(27,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(27,pnd_X)), pnd_Array.getValue(23,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 23,#X ) GIVING #ARRAY ( 27,#X )
            pnd_Array.getValue(12,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(12,pnd_X)), pnd_Array.getValue(8,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 8,#X ) GIVING #ARRAY ( 12,#X )
            pnd_Array.getValue(20,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(20,pnd_X)), pnd_Array.getValue(16,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 16,#X ) GIVING #ARRAY ( 20,#X )
            pnd_Array.getValue(28,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(28,pnd_X)), pnd_Array.getValue(24,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 24,#X ) GIVING #ARRAY ( 28,#X )
            //* *  COMPUTE #COUNTER2 = #COUNTER(#X) + #COUNTER1(#X)
            pnd_Array.getValue(33,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(33,pnd_X)), pnd_Array.getValue(29,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 29,#X ) GIVING #ARRAY ( 33,#X )
            pnd_Array.getValue(34,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(34,pnd_X)), pnd_Array.getValue(30,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 30,#X ) GIVING #ARRAY ( 34,#X )
            pnd_Array.getValue(35,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(35,pnd_X)), pnd_Array.getValue(31,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 31,#X ) GIVING #ARRAY ( 35,#X )
            pnd_Array.getValue(36,pnd_X).compute(new ComputeParameters(false, pnd_Array.getValue(36,pnd_X)), pnd_Array.getValue(32,pnd_X).divide(pnd_Counter.getValue(pnd_X))); //Natural: DIVIDE #COUNTER ( #X ) INTO #ARRAY ( 32,#X ) GIVING #ARRAY ( 36,#X )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  WEEKLY AVERAGES
        //* ***********************************************************************
        pnd_Counter_Total.compute(new ComputeParameters(false, pnd_Counter_Total), pnd_Counter.getValue(1,":",7).add(getZero()));                                         //Natural: COMPUTE #COUNTER-TOTAL = #COUNTER ( 1:7 ) + 0
        pnd_Counter_Total2.compute(new ComputeParameters(false, pnd_Counter_Total2), pnd_Counter1.getValue(1,":",7).add(getZero()));                                      //Natural: COMPUTE #COUNTER-TOTAL2 = #COUNTER1 ( 1:7 ) + 0
        pnd_Counter_Total3.compute(new ComputeParameters(false, pnd_Counter_Total3), pnd_Counter_Total.add(pnd_Counter_Total2));                                          //Natural: COMPUTE #COUNTER-TOTAL3 = #COUNTER-TOTAL + #COUNTER-TOTAL2
        if (condition(pnd_Counter_Total.equals(getZero())))                                                                                                               //Natural: IF #COUNTER-TOTAL = 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Array.getValue(9,8).compute(new ComputeParameters(false, pnd_Array.getValue(9,8)), pnd_Icw_Adds.divide(pnd_Counter_Total));                               //Natural: DIVIDE #COUNTER-TOTAL INTO #ICW-ADDS GIVING #ARRAY ( 9,8 )
            pnd_Array.getValue(10,8).compute(new ComputeParameters(false, pnd_Array.getValue(10,8)), pnd_Icw_Upds.divide(pnd_Counter_Total));                             //Natural: DIVIDE #COUNTER-TOTAL INTO #ICW-UPDS GIVING #ARRAY ( 10,8 )
            pnd_Array.getValue(11,8).compute(new ComputeParameters(false, pnd_Array.getValue(11,8)), pnd_Icw_Dels.divide(pnd_Counter_Total));                             //Natural: DIVIDE #COUNTER-TOTAL INTO #ICW-DELS GIVING #ARRAY ( 11,8 )
            pnd_Array.getValue(12,8).compute(new ComputeParameters(false, pnd_Array.getValue(12,8)), pnd_Icw_Auds.divide(pnd_Counter_Total));                             //Natural: DIVIDE #COUNTER-TOTAL INTO #ICW-AUDS GIVING #ARRAY ( 12,8 )
            pnd_Array.getValue(17,8).compute(new ComputeParameters(false, pnd_Array.getValue(17,8)), pnd_Cwf_Adds.divide(pnd_Counter_Total));                             //Natural: DIVIDE #COUNTER-TOTAL INTO #CWF-ADDS GIVING #ARRAY ( 17,8 )
            pnd_Array.getValue(18,8).compute(new ComputeParameters(false, pnd_Array.getValue(18,8)), pnd_Cwf_Upds.divide(pnd_Counter_Total));                             //Natural: DIVIDE #COUNTER-TOTAL INTO #CWF-UPDS GIVING #ARRAY ( 18,8 )
            pnd_Array.getValue(19,8).compute(new ComputeParameters(false, pnd_Array.getValue(19,8)), pnd_Cwf_Dels.divide(pnd_Counter_Total));                             //Natural: DIVIDE #COUNTER-TOTAL INTO #CWF-DELS GIVING #ARRAY ( 19,8 )
            pnd_Array.getValue(20,8).compute(new ComputeParameters(false, pnd_Array.getValue(20,8)), pnd_Cwf_Auds.divide(pnd_Counter_Total));                             //Natural: DIVIDE #COUNTER-TOTAL INTO #CWF-AUDS GIVING #ARRAY ( 20,8 )
            pnd_Array.getValue(25,8).compute(new ComputeParameters(false, pnd_Array.getValue(25,8)), pnd_Ncw_Adds.divide(pnd_Counter_Total));                             //Natural: DIVIDE #COUNTER-TOTAL INTO #NCW-ADDS GIVING #ARRAY ( 25,8 )
            pnd_Array.getValue(26,8).compute(new ComputeParameters(false, pnd_Array.getValue(26,8)), pnd_Ncw_Upds.divide(pnd_Counter_Total));                             //Natural: DIVIDE #COUNTER-TOTAL INTO #NCW-UPDS GIVING #ARRAY ( 26,8 )
            pnd_Array.getValue(27,8).compute(new ComputeParameters(false, pnd_Array.getValue(27,8)), pnd_Ncw_Dels.divide(pnd_Counter_Total));                             //Natural: DIVIDE #COUNTER-TOTAL INTO #NCW-DELS GIVING #ARRAY ( 27,8 )
            pnd_Array.getValue(28,8).compute(new ComputeParameters(false, pnd_Array.getValue(28,8)), pnd_Ncw_Auds.divide(pnd_Counter_Total));                             //Natural: DIVIDE #COUNTER-TOTAL INTO #NCW-AUDS GIVING #ARRAY ( 28,8 )
            pnd_Array.getValue(33,8).compute(new ComputeParameters(false, pnd_Array.getValue(33,8)), pnd_Total_Adds.divide(pnd_Counter_Total));                           //Natural: DIVIDE #COUNTER-TOTAL INTO #TOTAL-ADDS GIVING #ARRAY ( 33,8 )
            pnd_Array.getValue(34,8).compute(new ComputeParameters(false, pnd_Array.getValue(34,8)), pnd_Total_Upds.divide(pnd_Counter_Total));                           //Natural: DIVIDE #COUNTER-TOTAL INTO #TOTAL-UPDS GIVING #ARRAY ( 34,8 )
            pnd_Array.getValue(35,8).compute(new ComputeParameters(false, pnd_Array.getValue(35,8)), pnd_Total_Dels.divide(pnd_Counter_Total));                           //Natural: DIVIDE #COUNTER-TOTAL INTO #TOTAL-DELS GIVING #ARRAY ( 35,8 )
            pnd_Array.getValue(36,8).compute(new ComputeParameters(false, pnd_Array.getValue(36,8)), pnd_Total_Auds.divide(pnd_Counter_Total));                           //Natural: DIVIDE #COUNTER-TOTAL INTO #TOTAL-AUDS GIVING #ARRAY ( 36,8 )
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  WRITE STATEMENTS
        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,pnd_Font);                                                                                                             //Natural: WRITE ( 01 ) NOTITLE #FONT
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"PROGRAM NAME      :",Global.getPROGRAM(),new TabSetting(114),"DATE :",Global.getDATN(),             //Natural: WRITE ( 01 ) 1T 'PROGRAM NAME      :' *PROGRAM 114T 'DATE :' *DATN ( EM = 9999'/'99'/'99 ) / 1T 'ENHANCED WORK LIST RUNS - WEEKLY SUMMARY' 114T 'PAGE :' *PAGE-NUMBER ( 1 )
            new ReportEditMask ("9999'/'99'/'99"),NEWLINE,new TabSetting(1),"ENHANCED WORK LIST RUNS - WEEKLY SUMMARY",new TabSetting(114),"PAGE :",getReports().getPageNumberDbs(1));
        if (Global.isEscape()) return;
        getReports().write(1, new ReportZeroPrint (true),ReportOption.NOTITLE,NEWLINE,new TabSetting(17),pnd_Current_Rec_Date1,new ColumnSpacing(2),pnd_Current_Rec_Date2,new  //Natural: WRITE ( 01 ) ( ZP = ON ) / 17T #CURRENT-REC-DATE1 2X #CURRENT-REC-DATE2 2X #CURRENT-REC-DATE3 2X #CURRENT-REC-DATE4 2X #CURRENT-REC-DATE5 2X #CURRENT-REC-DATE6 2X #CURRENT-REC-DATE7 '      WEEK' / 'INITIAL RUNS' / '-' ( 131 ) //'ICW ADDED     ' #ARRAY ( 1,1:8 ) / 'CWF ADDED     ' #ARRAY ( 2,1:8 ) / 'NCW ADDED     ' #ARRAY ( 3,1:8 ) / 'TOTAL         ' #ARRAY ( 4,1:8 ) / '-' ( 131 ) / 'PERIODIC RUNS' / '-' ( 131 ) //'ICW ADDED     ' #ARRAY ( 5,1:8 ) / 'ICW UPDATED   ' #ARRAY ( 6,1:8 ) / 'ICW DELETED   ' #ARRAY ( 7,1:8 ) / 'ICW TOTAL     ' #ARRAY ( 8,1:8 ) //'ICW AVG ADD   ' #ARRAY ( 9,1:8 ) / 'ICW AVG UPD   ' #ARRAY ( 10,1:8 ) / 'ICW AVG DEL   ' #ARRAY ( 11,1:8 ) / 'ICW AVG TOT   ' #ARRAY ( 12,1:8 ) //'CWF ADDED     ' #ARRAY ( 13,1:8 ) / 'CWF UPDATED   ' #ARRAY ( 14,1:8 ) / 'CWF DELETED   ' #ARRAY ( 15,1:8 ) / 'CWF TOTAL     ' #ARRAY ( 16,1:8 ) //'CWF AVG ADD   ' #ARRAY ( 17,1:8 ) / 'CWF AVG UPD   ' #ARRAY ( 18,1:8 ) / 'CWF AVG DEL   ' #ARRAY ( 19,1:8 ) / 'CWF AVG TOT   ' #ARRAY ( 20,1:8 ) //'NCW ADDED     ' #ARRAY ( 21,1:8 ) / 'NCW UPDATED   ' #ARRAY ( 22,1:8 ) / 'NCW DELETED   ' #ARRAY ( 23,1:8 ) / 'NCW TOTAL     ' #ARRAY ( 24,1:8 ) //'NCW AVG ADD   ' #ARRAY ( 25,1:8 ) / 'NCW AVG UPD   ' #ARRAY ( 26,1:8 ) / 'NCW AVG DEL   ' #ARRAY ( 27,1:8 ) / 'NCW AVG TOT   ' #ARRAY ( 28,1:8 ) / '-' ( 131 ) / 'DAILY TOTALS' / '-' ( 131 ) //'TOTAL ADDS    ' #ARRAY ( 29,1:8 ) / 'TOTAL UPDS    ' #ARRAY ( 30,1:8 ) / 'TOTAL DELS    ' #ARRAY ( 31,1:8 ) / 'TOTAL A/U/D   ' #ARRAY ( 32,1:8 ) //'TOTAL AVG A   ' #ARRAY ( 33,1:8 ) / 'TOTAL AVG U   ' #ARRAY ( 34,1:8 ) / 'TOTAL AVG D   ' #ARRAY ( 35,1:8 ) / 'TOTAL AVG     ' #ARRAY ( 36,1:8 ) / '_' ( 131 ) / '_' ( 131 ) /// 'TOTAL DELETED' #DDCOUNTER-2 'BEGIN DELETE DATE' #DELETE-DATE-2 'END DELETE DATE' #INVRTD-DATE-STAMP-2
            ColumnSpacing(2),pnd_Current_Rec_Date3,new ColumnSpacing(2),pnd_Current_Rec_Date4,new ColumnSpacing(2),pnd_Current_Rec_Date5,new ColumnSpacing(2),pnd_Current_Rec_Date6,new 
            ColumnSpacing(2),pnd_Current_Rec_Date7,"      WEEK",NEWLINE,"INITIAL RUNS",NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE,"ICW ADDED     ",pnd_Array.getValue(1,1,":",8),NEWLINE,"CWF ADDED     ",pnd_Array.getValue(2,1,":",8),NEWLINE,"NCW ADDED     ",pnd_Array.getValue(3,1,":",8),NEWLINE,"TOTAL         ",pnd_Array.getValue(4,1,":",8),NEWLINE,"-",new 
            RepeatItem(131),NEWLINE,"PERIODIC RUNS",NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE,"ICW ADDED     ",pnd_Array.getValue(5,1,":",8),NEWLINE,"ICW UPDATED   ",pnd_Array.getValue(6,1,":",8),NEWLINE,"ICW DELETED   ",pnd_Array.getValue(7,1,":",8),NEWLINE,"ICW TOTAL     ",pnd_Array.getValue(8,1,":",8),NEWLINE,NEWLINE,"ICW AVG ADD   ",pnd_Array.getValue(9,1,":",8),NEWLINE,"ICW AVG UPD   ",pnd_Array.getValue(10,1,":",8),NEWLINE,"ICW AVG DEL   ",pnd_Array.getValue(11,1,":",8),NEWLINE,"ICW AVG TOT   ",pnd_Array.getValue(12,1,":",8),NEWLINE,NEWLINE,"CWF ADDED     ",pnd_Array.getValue(13,1,":",8),NEWLINE,"CWF UPDATED   ",pnd_Array.getValue(14,1,":",8),NEWLINE,"CWF DELETED   ",pnd_Array.getValue(15,1,":",8),NEWLINE,"CWF TOTAL     ",pnd_Array.getValue(16,1,":",8),NEWLINE,NEWLINE,"CWF AVG ADD   ",pnd_Array.getValue(17,1,":",8),NEWLINE,"CWF AVG UPD   ",pnd_Array.getValue(18,1,":",8),NEWLINE,"CWF AVG DEL   ",pnd_Array.getValue(19,1,":",8),NEWLINE,"CWF AVG TOT   ",pnd_Array.getValue(20,1,":",8),NEWLINE,NEWLINE,"NCW ADDED     ",pnd_Array.getValue(21,1,":",8),NEWLINE,"NCW UPDATED   ",pnd_Array.getValue(22,1,":",8),NEWLINE,"NCW DELETED   ",pnd_Array.getValue(23,1,":",8),NEWLINE,"NCW TOTAL     ",pnd_Array.getValue(24,1,":",8),NEWLINE,NEWLINE,"NCW AVG ADD   ",pnd_Array.getValue(25,1,":",8),NEWLINE,"NCW AVG UPD   ",pnd_Array.getValue(26,1,":",8),NEWLINE,"NCW AVG DEL   ",pnd_Array.getValue(27,1,":",8),NEWLINE,"NCW AVG TOT   ",pnd_Array.getValue(28,1,":",8),NEWLINE,"-",new 
            RepeatItem(131),NEWLINE,"DAILY TOTALS",NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE,"TOTAL ADDS    ",pnd_Array.getValue(29,1,":",8),NEWLINE,"TOTAL UPDS    ",pnd_Array.getValue(30,1,":",8),NEWLINE,"TOTAL DELS    ",pnd_Array.getValue(31,1,":",8),NEWLINE,"TOTAL A/U/D   ",pnd_Array.getValue(32,1,":",8),NEWLINE,NEWLINE,"TOTAL AVG A   ",pnd_Array.getValue(33,1,":",8),NEWLINE,"TOTAL AVG U   ",pnd_Array.getValue(34,1,":",8),NEWLINE,"TOTAL AVG D   ",pnd_Array.getValue(35,1,":",8),NEWLINE,"TOTAL AVG     ",pnd_Array.getValue(36,1,":",8),NEWLINE,"_",new 
            RepeatItem(131),NEWLINE,"_",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE,"TOTAL DELETED",pnd_Ddcounter_2,"BEGIN DELETE DATE",pnd_Delete_Date_2,
            "END DELETE DATE",pnd_Invrtd_Date_Stamp_2);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=OFF");
    }
}
