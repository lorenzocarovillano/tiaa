/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:28:12 PM
**        * FROM NATURAL PROGRAM : Cwfb3105
************************************************************
**        * FILE NAME            : Cwfb3105.java
**        * CLASS NAME           : Cwfb3105
**        * INSTANCE NAME        : Cwfb3105
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 5
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFP3005
* SYSTEM   : CRPCWF
* TITLE    : REPORT 5
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF WORK ACTIVITY FOR A PERIOD
*          |
*          |
*          |
*          |
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3105 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Tbl_Pin;
    private DbsField pnd_Work_Record_Tbl_Wpid_Act;
    private DbsField pnd_Work_Record_Tbl_Wpid;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Tbl_Wpid_Action;
    private DbsField pnd_Work_Record_Tbl_Status_Cde;

    private DbsGroup pnd_Work_Record__R_Field_2;
    private DbsField pnd_Work_Record_Tbl_Status_Cde_N;
    private DbsField pnd_Work_Record_Tbl_Last_Updte_Dte;

    private DbsGroup pnd_Work_Record__R_Field_3;
    private DbsField pnd_Work_Record_Tbl_Last_Updte_Dte_A;
    private DbsField pnd_Work_Record_Tbl_Last_Chge_Dte;
    private DbsField pnd_Work_Record_Tbl_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Log_Unit_Cde;
    private DbsField pnd_Work_Record_Tbl_Chnge_Unit;
    private DbsField pnd_Work_Record_Tbl_Admin_Unit;
    private DbsField pnd_Work_Record_Tbl_Log_By;
    private DbsField pnd_Work_Record_Pnd_New_Ind;
    private DbsField pnd_Work_Record_Pnd_Merg_Ind;
    private DbsField pnd_Work_Record_Pnd_E_Pended_Ind;
    private DbsField pnd_Work_Record_Pnd_Partic_Closed_Ind;
    private DbsField pnd_Work_Record_Pnd_Assigned_Ind;
    private DbsField pnd_Work_Record_Pnd_Unassigned_Ind;

    private DbsGroup pnd_Report_Data;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Cde;

    private DbsGroup pnd_Report_Data__R_Field_4;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Id_Cde;
    private DbsField pnd_Report_Data_Pnd_Rep_Start_Dd;

    private DbsGroup pnd_Report_Data__R_Field_5;
    private DbsField pnd_Report_Data_Pnd_Rep_Start_Dd_A;
    private DbsField pnd_Report_Data_Pnd_Rep_Start_Mm;

    private DbsGroup pnd_Report_Data__R_Field_6;
    private DbsField pnd_Report_Data_Pnd_Rep_Start_Mm_A;
    private DbsField pnd_Report_Data_Pnd_Rep_Start_Yy;

    private DbsGroup pnd_Report_Data__R_Field_7;
    private DbsField pnd_Report_Data_Pnd_Rep_Start_Yy_A;
    private DbsField pnd_Report_Data_Pnd_Rep_End_Dd;

    private DbsGroup pnd_Report_Data__R_Field_8;
    private DbsField pnd_Report_Data_Pnd_Rep_End_Dd_A;
    private DbsField pnd_Report_Data_Pnd_Rep_End_Mm;

    private DbsGroup pnd_Report_Data__R_Field_9;
    private DbsField pnd_Report_Data_Pnd_Rep_End_Mm_A;
    private DbsField pnd_Report_Data_Pnd_Rep_End_Yy;

    private DbsGroup pnd_Report_Data__R_Field_10;
    private DbsField pnd_Report_Data_Pnd_Rep_End_Yy_A;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid;

    private DbsGroup pnd_Report_Data__R_Field_11;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Lob;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Mbp;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Sbp;
    private DbsField pnd_Report_Data_Pnd_Rep_Racf_Id;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Empl_Name;

    private DbsGroup pnd_Misc_Data;
    private DbsField pnd_Misc_Data_Pnd_Wrk_Unit_Cde;

    private DbsGroup pnd_Misc_Data__R_Field_12;
    private DbsField pnd_Misc_Data_Pnd_Wrk_Unit_Id_Cde;
    private DbsField pnd_Misc_Data_Pnd_Unit_Name;
    private DbsField pnd_Misc_Data_Pnd_Wpid_Code;
    private DbsField pnd_Misc_Data_Pnd_Wpid_Desc;
    private DbsField pnd_Misc_Data_Pnd_Open_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Open_P_Ctr;
    private DbsField pnd_Misc_Data_Pnd_New_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Merg_Ctr;
    private DbsField pnd_Misc_Data_Pnd_I_Pended;
    private DbsField pnd_Misc_Data_Pnd_E_Pended;
    private DbsField pnd_Misc_Data_Pnd_Routed_C;
    private DbsField pnd_Misc_Data_Pnd_Routed_I;
    private DbsField pnd_Misc_Data_Pnd_Routed_N;
    private DbsField pnd_Misc_Data_Pnd_Partic_Closed;
    private DbsField pnd_Misc_Data_Pnd_Assigned_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Unassigned_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Pended_I_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Pended_E_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Open_End;
    private DbsField pnd_Misc_Data_Pnd_Total_Open_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Total_Open_P_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Total_New_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Total_Merg_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Total_I_Pended;
    private DbsField pnd_Misc_Data_Pnd_Total_E_Pended;
    private DbsField pnd_Misc_Data_Pnd_Total_Routed_C;
    private DbsField pnd_Misc_Data_Pnd_Total_Routed_I;
    private DbsField pnd_Misc_Data_Pnd_Total_Routed_N;
    private DbsField pnd_Misc_Data_Pnd_Total_Partic_Closed;
    private DbsField pnd_Misc_Data_Pnd_Total_Assigned_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Total_Unassigned_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Total_Pended_I_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Total_Pended_E_Ctr;
    private DbsField pnd_Misc_Data_Pnd_Total_Open_End;
    private DbsField pnd_Misc_Data_Pnd_Start_Date;

    private DbsGroup pnd_Misc_Data__R_Field_13;
    private DbsField pnd_Misc_Data_Pnd_Start_Date_N;
    private DbsField pnd_Misc_Data_Pnd_End_Date;

    private DbsGroup pnd_Misc_Data__R_Field_14;
    private DbsField pnd_Misc_Data_Pnd_End_Date_N;
    private DbsField pnd_Misc_Data_Pnd_Work_Start_Date_A;

    private DbsGroup pnd_Misc_Data__R_Field_15;
    private DbsField pnd_Misc_Data_Pnd_Work_Mm;
    private DbsField pnd_Misc_Data_Pnd_Filler1;
    private DbsField pnd_Misc_Data_Pnd_Work_Dd;
    private DbsField pnd_Misc_Data_Pnd_Filler2;
    private DbsField pnd_Misc_Data_Pnd_Work_Yy;
    private DbsField pnd_Misc_Data_Pnd_Work_End_Date_A;

    private DbsGroup pnd_Misc_Data__R_Field_16;
    private DbsField pnd_Misc_Data_Pnd_Work_Mmm;
    private DbsField pnd_Misc_Data_Pnd_Fillera;
    private DbsField pnd_Misc_Data_Pnd_Work_Ddd;
    private DbsField pnd_Misc_Data_Pnd_Fillerb;
    private DbsField pnd_Misc_Data_Pnd_Work_Yyy;
    private DbsField pnd_Misc_Data_Pnd_Confirmed;

    private DataAccessProgramView vw_hist_Mstr_Index;
    private DbsField hist_Mstr_Index_Pin_Nbr;
    private DbsField hist_Mstr_Index_Rqst_Log_Dte_Tme;
    private DbsField hist_Mstr_Index_Rqst_Log_Oprtr_Cde;
    private DbsField hist_Mstr_Index_Orgnl_Unit_Cde;

    private DbsGroup hist_Mstr_Index__R_Field_17;
    private DbsField hist_Mstr_Index_Orgnl_Unit_Id_Cde;
    private DbsField hist_Mstr_Index_Work_Prcss_Id;

    private DbsGroup hist_Mstr_Index__R_Field_18;
    private DbsField hist_Mstr_Index_Work_Actn_Rqstd_Cde;
    private DbsField hist_Mstr_Index_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField hist_Mstr_Index_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField hist_Mstr_Index_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField hist_Mstr_Index_Unit_Cde;

    private DbsGroup hist_Mstr_Index__R_Field_19;
    private DbsField hist_Mstr_Index_Unit_Id_Cde;
    private DbsField hist_Mstr_Index_Unit_Rgn_Cde;
    private DbsField hist_Mstr_Index_Unit_Spcl_Dsgntn_Cde;
    private DbsField hist_Mstr_Index_Unit_Brnch_Group_Cde;
    private DbsField hist_Mstr_Index_Empl_Oprtr_Cde;

    private DbsGroup hist_Mstr_Index__R_Field_20;
    private DbsField hist_Mstr_Index_Empl_Racf_Id;
    private DbsField hist_Mstr_Index_Last_Chnge_Dte_Tme;
    private DbsField hist_Mstr_Index_Last_Chnge_Oprtr_Cde;
    private DbsField hist_Mstr_Index_Step_Id;
    private DbsField hist_Mstr_Index_Rt_Sqnce_Nbr;
    private DbsField hist_Mstr_Index_Step_Sqnce_Nbr;
    private DbsField hist_Mstr_Index_Admin_Unit_Cde;

    private DbsGroup hist_Mstr_Index__R_Field_21;
    private DbsField hist_Mstr_Index_Admin_Unit_Id_Cde;
    private DbsField hist_Mstr_Index_Admin_Status_Cde;
    private DbsField hist_Mstr_Index_Admin_Status_Updte_Dte_Tme;
    private DbsField hist_Mstr_Index_Admin_Status_Updte_Oprtr_Cde;
    private DbsField hist_Mstr_Index_Last_Updte_Dte;
    private DbsField hist_Mstr_Index_Last_Updte_Dte_Tme;
    private DbsField hist_Mstr_Index_Last_Updte_Oprtr_Cde;
    private DbsField hist_Mstr_Index_Actve_Ind;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Save_Unit;
    private DbsField pnd_First_Unit;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Env;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Work_Date;
    private DbsField pnd_Parm_Report_No;
    private DbsField pnd_Parm_Empl_Racf_Id;
    private DbsField pnd_Parm_Unit_Cde;
    private DbsField pnd_Parm_Floor;
    private DbsField pnd_Parm_Bldg;
    private DbsField pnd_Parm_Drop_Off;
    private DbsField pnd_Rep_Unit_Nme;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Parm_Type;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Tbl_Wpid_ActOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Tbl_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Pin", "TBL-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Tbl_Wpid_Act = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 1);
        pnd_Work_Record_Tbl_Wpid = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        pnd_Work_Record__R_Field_1 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record_Tbl_Wpid);
        pnd_Work_Record_Tbl_Wpid_Action = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Work_Record_Tbl_Status_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde", "TBL-STATUS-CDE", FieldType.STRING, 4);

        pnd_Work_Record__R_Field_2 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_2", "REDEFINE", pnd_Work_Record_Tbl_Status_Cde);
        pnd_Work_Record_Tbl_Status_Cde_N = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde_N", "TBL-STATUS-CDE-N", FieldType.NUMERIC, 
            4);
        pnd_Work_Record_Tbl_Last_Updte_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Last_Updte_Dte", "TBL-LAST-UPDTE-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_Record__R_Field_3 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_3", "REDEFINE", pnd_Work_Record_Tbl_Last_Updte_Dte);
        pnd_Work_Record_Tbl_Last_Updte_Dte_A = pnd_Work_Record__R_Field_3.newFieldInGroup("pnd_Work_Record_Tbl_Last_Updte_Dte_A", "TBL-LAST-UPDTE-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Last_Chge_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chge_Dte", "TBL-LAST-CHGE-DTE", FieldType.STRING, 
            15);
        pnd_Work_Record_Tbl_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Dte_Tme", "TBL-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_Record_Tbl_Log_Unit_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Unit_Cde", "TBL-LOG-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Chnge_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Chnge_Unit", "TBL-CHNGE-UNIT", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Admin_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Admin_Unit", "TBL-ADMIN-UNIT", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Log_By = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_By", "TBL-LOG-BY", FieldType.STRING, 8);
        pnd_Work_Record_Pnd_New_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_New_Ind", "#NEW-IND", FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_Merg_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Merg_Ind", "#MERG-IND", FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_E_Pended_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_E_Pended_Ind", "#E-PENDED-IND", FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_Partic_Closed_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Partic_Closed_Ind", "#PARTIC-CLOSED-IND", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_Pnd_Assigned_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Assigned_Ind", "#ASSIGNED-IND", FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_Unassigned_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Unassigned_Ind", "#UNASSIGNED-IND", FieldType.NUMERIC, 
            2);

        pnd_Report_Data = localVariables.newGroupInRecord("pnd_Report_Data", "#REPORT-DATA");
        pnd_Report_Data_Pnd_Rep_Unit_Cde = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);

        pnd_Report_Data__R_Field_4 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_4", "REDEFINE", pnd_Report_Data_Pnd_Rep_Unit_Cde);
        pnd_Report_Data_Pnd_Rep_Unit_Id_Cde = pnd_Report_Data__R_Field_4.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Id_Cde", "#REP-UNIT-ID-CDE", FieldType.STRING, 
            5);
        pnd_Report_Data_Pnd_Rep_Start_Dd = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Start_Dd", "#REP-START-DD", FieldType.NUMERIC, 2);

        pnd_Report_Data__R_Field_5 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_5", "REDEFINE", pnd_Report_Data_Pnd_Rep_Start_Dd);
        pnd_Report_Data_Pnd_Rep_Start_Dd_A = pnd_Report_Data__R_Field_5.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Start_Dd_A", "#REP-START-DD-A", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Start_Mm = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Start_Mm", "#REP-START-MM", FieldType.NUMERIC, 2);

        pnd_Report_Data__R_Field_6 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_6", "REDEFINE", pnd_Report_Data_Pnd_Rep_Start_Mm);
        pnd_Report_Data_Pnd_Rep_Start_Mm_A = pnd_Report_Data__R_Field_6.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Start_Mm_A", "#REP-START-MM-A", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Start_Yy = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Start_Yy", "#REP-START-YY", FieldType.NUMERIC, 2);

        pnd_Report_Data__R_Field_7 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_7", "REDEFINE", pnd_Report_Data_Pnd_Rep_Start_Yy);
        pnd_Report_Data_Pnd_Rep_Start_Yy_A = pnd_Report_Data__R_Field_7.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Start_Yy_A", "#REP-START-YY-A", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_End_Dd = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_End_Dd", "#REP-END-DD", FieldType.NUMERIC, 2);

        pnd_Report_Data__R_Field_8 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_8", "REDEFINE", pnd_Report_Data_Pnd_Rep_End_Dd);
        pnd_Report_Data_Pnd_Rep_End_Dd_A = pnd_Report_Data__R_Field_8.newFieldInGroup("pnd_Report_Data_Pnd_Rep_End_Dd_A", "#REP-END-DD-A", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_End_Mm = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_End_Mm", "#REP-END-MM", FieldType.NUMERIC, 2);

        pnd_Report_Data__R_Field_9 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_9", "REDEFINE", pnd_Report_Data_Pnd_Rep_End_Mm);
        pnd_Report_Data_Pnd_Rep_End_Mm_A = pnd_Report_Data__R_Field_9.newFieldInGroup("pnd_Report_Data_Pnd_Rep_End_Mm_A", "#REP-END-MM-A", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_End_Yy = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_End_Yy", "#REP-END-YY", FieldType.NUMERIC, 2);

        pnd_Report_Data__R_Field_10 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_10", "REDEFINE", pnd_Report_Data_Pnd_Rep_End_Yy);
        pnd_Report_Data_Pnd_Rep_End_Yy_A = pnd_Report_Data__R_Field_10.newFieldInGroup("pnd_Report_Data_Pnd_Rep_End_Yy_A", "#REP-END-YY-A", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Wpid = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid", "#REP-WPID", FieldType.STRING, 6);

        pnd_Report_Data__R_Field_11 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_11", "REDEFINE", pnd_Report_Data_Pnd_Rep_Wpid);
        pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde = pnd_Report_Data__R_Field_11.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde", "#REP-WPID-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        pnd_Report_Data_Pnd_Rep_Wpid_Lob = pnd_Report_Data__R_Field_11.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Lob", "#REP-WPID-LOB", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Wpid_Mbp = pnd_Report_Data__R_Field_11.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Mbp", "#REP-WPID-MBP", FieldType.STRING, 
            1);
        pnd_Report_Data_Pnd_Rep_Wpid_Sbp = pnd_Report_Data__R_Field_11.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Sbp", "#REP-WPID-SBP", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Racf_Id = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Racf_Id", "#REP-RACF-ID", FieldType.STRING, 8);
        pnd_Report_Data_Pnd_Rep_Wpid_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Name", "#REP-WPID-NAME", FieldType.STRING, 45);
        pnd_Report_Data_Pnd_Rep_Empl_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Empl_Name", "#REP-EMPL-NAME", FieldType.STRING, 30);

        pnd_Misc_Data = localVariables.newGroupInRecord("pnd_Misc_Data", "#MISC-DATA");
        pnd_Misc_Data_Pnd_Wrk_Unit_Cde = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Wrk_Unit_Cde", "#WRK-UNIT-CDE", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_12 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_12", "REDEFINE", pnd_Misc_Data_Pnd_Wrk_Unit_Cde);
        pnd_Misc_Data_Pnd_Wrk_Unit_Id_Cde = pnd_Misc_Data__R_Field_12.newFieldInGroup("pnd_Misc_Data_Pnd_Wrk_Unit_Id_Cde", "#WRK-UNIT-ID-CDE", FieldType.STRING, 
            5);
        pnd_Misc_Data_Pnd_Unit_Name = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Misc_Data_Pnd_Wpid_Code = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Wpid_Code", "#WPID-CODE", FieldType.STRING, 6);
        pnd_Misc_Data_Pnd_Wpid_Desc = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Wpid_Desc", "#WPID-DESC", FieldType.STRING, 15);
        pnd_Misc_Data_Pnd_Open_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Open_Ctr", "#OPEN-CTR", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Open_P_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Open_P_Ctr", "#OPEN-P-CTR", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_New_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_New_Ctr", "#NEW-CTR", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Merg_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Merg_Ctr", "#MERG-CTR", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_I_Pended = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_I_Pended", "#I-PENDED", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_E_Pended = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_E_Pended", "#E-PENDED", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Routed_C = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Routed_C", "#ROUTED-C", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Routed_I = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Routed_I", "#ROUTED-I", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Routed_N = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Routed_N", "#ROUTED-N", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Partic_Closed = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Partic_Closed", "#PARTIC-CLOSED", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Assigned_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Assigned_Ctr", "#ASSIGNED-CTR", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Unassigned_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Unassigned_Ctr", "#UNASSIGNED-CTR", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Pended_I_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Pended_I_Ctr", "#PENDED-I-CTR", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Pended_E_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Pended_E_Ctr", "#PENDED-E-CTR", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Open_End = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Open_End", "#OPEN-END", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Total_Open_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_Open_Ctr", "#TOTAL-OPEN-CTR", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Total_Open_P_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_Open_P_Ctr", "#TOTAL-OPEN-P-CTR", FieldType.NUMERIC, 
            6);
        pnd_Misc_Data_Pnd_Total_New_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_New_Ctr", "#TOTAL-NEW-CTR", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Total_Merg_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_Merg_Ctr", "#TOTAL-MERG-CTR", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Total_I_Pended = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_I_Pended", "#TOTAL-I-PENDED", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Total_E_Pended = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_E_Pended", "#TOTAL-E-PENDED", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Total_Routed_C = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_Routed_C", "#TOTAL-ROUTED-C", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Total_Routed_I = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_Routed_I", "#TOTAL-ROUTED-I", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Total_Routed_N = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_Routed_N", "#TOTAL-ROUTED-N", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Total_Partic_Closed = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_Partic_Closed", "#TOTAL-PARTIC-CLOSED", FieldType.NUMERIC, 
            6);
        pnd_Misc_Data_Pnd_Total_Assigned_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_Assigned_Ctr", "#TOTAL-ASSIGNED-CTR", FieldType.NUMERIC, 
            6);
        pnd_Misc_Data_Pnd_Total_Unassigned_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_Unassigned_Ctr", "#TOTAL-UNASSIGNED-CTR", FieldType.NUMERIC, 
            6);
        pnd_Misc_Data_Pnd_Total_Pended_I_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_Pended_I_Ctr", "#TOTAL-PENDED-I-CTR", FieldType.NUMERIC, 
            6);
        pnd_Misc_Data_Pnd_Total_Pended_E_Ctr = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_Pended_E_Ctr", "#TOTAL-PENDED-E-CTR", FieldType.NUMERIC, 
            6);
        pnd_Misc_Data_Pnd_Total_Open_End = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Total_Open_End", "#TOTAL-OPEN-END", FieldType.NUMERIC, 6);
        pnd_Misc_Data_Pnd_Start_Date = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_13 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_13", "REDEFINE", pnd_Misc_Data_Pnd_Start_Date);
        pnd_Misc_Data_Pnd_Start_Date_N = pnd_Misc_Data__R_Field_13.newFieldInGroup("pnd_Misc_Data_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Misc_Data_Pnd_End_Date = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_14 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_14", "REDEFINE", pnd_Misc_Data_Pnd_End_Date);
        pnd_Misc_Data_Pnd_End_Date_N = pnd_Misc_Data__R_Field_14.newFieldInGroup("pnd_Misc_Data_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 8);
        pnd_Misc_Data_Pnd_Work_Start_Date_A = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Start_Date_A", "#WORK-START-DATE-A", FieldType.STRING, 
            8);

        pnd_Misc_Data__R_Field_15 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_15", "REDEFINE", pnd_Misc_Data_Pnd_Work_Start_Date_A);
        pnd_Misc_Data_Pnd_Work_Mm = pnd_Misc_Data__R_Field_15.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Mm", "#WORK-MM", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Filler1 = pnd_Misc_Data__R_Field_15.newFieldInGroup("pnd_Misc_Data_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Dd = pnd_Misc_Data__R_Field_15.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Filler2 = pnd_Misc_Data__R_Field_15.newFieldInGroup("pnd_Misc_Data_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Yy = pnd_Misc_Data__R_Field_15.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Yy", "#WORK-YY", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Work_End_Date_A = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Work_End_Date_A", "#WORK-END-DATE-A", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_16 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_16", "REDEFINE", pnd_Misc_Data_Pnd_Work_End_Date_A);
        pnd_Misc_Data_Pnd_Work_Mmm = pnd_Misc_Data__R_Field_16.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Mmm", "#WORK-MMM", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Fillera = pnd_Misc_Data__R_Field_16.newFieldInGroup("pnd_Misc_Data_Pnd_Fillera", "#FILLERA", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Ddd = pnd_Misc_Data__R_Field_16.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Ddd", "#WORK-DDD", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Fillerb = pnd_Misc_Data__R_Field_16.newFieldInGroup("pnd_Misc_Data_Pnd_Fillerb", "#FILLERB", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Yyy = pnd_Misc_Data__R_Field_16.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Yyy", "#WORK-YYY", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Confirmed = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Confirmed", "#CONFIRMED", FieldType.BOOLEAN, 1);

        vw_hist_Mstr_Index = new DataAccessProgramView(new NameInfo("vw_hist_Mstr_Index", "HIST-MSTR-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        hist_Mstr_Index_Pin_Nbr = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        hist_Mstr_Index_Pin_Nbr.setDdmHeader("PIN");
        hist_Mstr_Index_Rqst_Log_Dte_Tme = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        hist_Mstr_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        hist_Mstr_Index_Rqst_Log_Oprtr_Cde = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        hist_Mstr_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        hist_Mstr_Index_Orgnl_Unit_Cde = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        hist_Mstr_Index_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");

        hist_Mstr_Index__R_Field_17 = vw_hist_Mstr_Index.getRecord().newGroupInGroup("hist_Mstr_Index__R_Field_17", "REDEFINE", hist_Mstr_Index_Orgnl_Unit_Cde);
        hist_Mstr_Index_Orgnl_Unit_Id_Cde = hist_Mstr_Index__R_Field_17.newFieldInGroup("hist_Mstr_Index_Orgnl_Unit_Id_Cde", "ORGNL-UNIT-ID-CDE", FieldType.STRING, 
            5);
        hist_Mstr_Index_Work_Prcss_Id = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        hist_Mstr_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");

        hist_Mstr_Index__R_Field_18 = vw_hist_Mstr_Index.getRecord().newGroupInGroup("hist_Mstr_Index__R_Field_18", "REDEFINE", hist_Mstr_Index_Work_Prcss_Id);
        hist_Mstr_Index_Work_Actn_Rqstd_Cde = hist_Mstr_Index__R_Field_18.newFieldInGroup("hist_Mstr_Index_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        hist_Mstr_Index_Work_Lob_Cmpny_Prdct_Cde = hist_Mstr_Index__R_Field_18.newFieldInGroup("hist_Mstr_Index_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        hist_Mstr_Index_Work_Mjr_Bsnss_Prcss_Cde = hist_Mstr_Index__R_Field_18.newFieldInGroup("hist_Mstr_Index_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        hist_Mstr_Index_Work_Spcfc_Bsnss_Prcss_Cde = hist_Mstr_Index__R_Field_18.newFieldInGroup("hist_Mstr_Index_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        hist_Mstr_Index_Unit_Cde = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        hist_Mstr_Index_Unit_Cde.setDdmHeader("UNIT/CODE");

        hist_Mstr_Index__R_Field_19 = vw_hist_Mstr_Index.getRecord().newGroupInGroup("hist_Mstr_Index__R_Field_19", "REDEFINE", hist_Mstr_Index_Unit_Cde);
        hist_Mstr_Index_Unit_Id_Cde = hist_Mstr_Index__R_Field_19.newFieldInGroup("hist_Mstr_Index_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        hist_Mstr_Index_Unit_Rgn_Cde = hist_Mstr_Index__R_Field_19.newFieldInGroup("hist_Mstr_Index_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 1);
        hist_Mstr_Index_Unit_Spcl_Dsgntn_Cde = hist_Mstr_Index__R_Field_19.newFieldInGroup("hist_Mstr_Index_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        hist_Mstr_Index_Unit_Brnch_Group_Cde = hist_Mstr_Index__R_Field_19.newFieldInGroup("hist_Mstr_Index_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        hist_Mstr_Index_Empl_Oprtr_Cde = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        hist_Mstr_Index_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");

        hist_Mstr_Index__R_Field_20 = vw_hist_Mstr_Index.getRecord().newGroupInGroup("hist_Mstr_Index__R_Field_20", "REDEFINE", hist_Mstr_Index_Empl_Oprtr_Cde);
        hist_Mstr_Index_Empl_Racf_Id = hist_Mstr_Index__R_Field_20.newFieldInGroup("hist_Mstr_Index_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        hist_Mstr_Index_Last_Chnge_Dte_Tme = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        hist_Mstr_Index_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        hist_Mstr_Index_Last_Chnge_Oprtr_Cde = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        hist_Mstr_Index_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        hist_Mstr_Index_Step_Id = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        hist_Mstr_Index_Step_Id.setDdmHeader("STEP/ID");
        hist_Mstr_Index_Rt_Sqnce_Nbr = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        hist_Mstr_Index_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        hist_Mstr_Index_Step_Sqnce_Nbr = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Step_Sqnce_Nbr", "STEP-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "STEP_SQNCE_NBR");
        hist_Mstr_Index_Admin_Unit_Cde = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");

        hist_Mstr_Index__R_Field_21 = vw_hist_Mstr_Index.getRecord().newGroupInGroup("hist_Mstr_Index__R_Field_21", "REDEFINE", hist_Mstr_Index_Admin_Unit_Cde);
        hist_Mstr_Index_Admin_Unit_Id_Cde = hist_Mstr_Index__R_Field_21.newFieldInGroup("hist_Mstr_Index_Admin_Unit_Id_Cde", "ADMIN-UNIT-ID-CDE", FieldType.STRING, 
            5);
        hist_Mstr_Index_Admin_Status_Cde = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        hist_Mstr_Index_Admin_Status_Updte_Dte_Tme = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        hist_Mstr_Index_Admin_Status_Updte_Oprtr_Cde = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        hist_Mstr_Index_Last_Updte_Dte = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        hist_Mstr_Index_Last_Updte_Dte_Tme = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        hist_Mstr_Index_Last_Updte_Oprtr_Cde = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        hist_Mstr_Index_Actve_Ind = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        registerRecord(vw_hist_Mstr_Index);

        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Save_Unit = localVariables.newFieldInRecord("pnd_Save_Unit", "#SAVE-UNIT", FieldType.STRING, 8);
        pnd_First_Unit = localVariables.newFieldInRecord("pnd_First_Unit", "#FIRST-UNIT", FieldType.BOOLEAN, 1);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_Parm_Report_No = localVariables.newFieldInRecord("pnd_Parm_Report_No", "#PARM-REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Parm_Empl_Racf_Id = localVariables.newFieldInRecord("pnd_Parm_Empl_Racf_Id", "#PARM-EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Parm_Unit_Cde = localVariables.newFieldInRecord("pnd_Parm_Unit_Cde", "#PARM-UNIT-CDE", FieldType.STRING, 7);
        pnd_Parm_Floor = localVariables.newFieldInRecord("pnd_Parm_Floor", "#PARM-FLOOR", FieldType.NUMERIC, 2);
        pnd_Parm_Bldg = localVariables.newFieldInRecord("pnd_Parm_Bldg", "#PARM-BLDG", FieldType.STRING, 3);
        pnd_Parm_Drop_Off = localVariables.newFieldInRecord("pnd_Parm_Drop_Off", "#PARM-DROP-OFF", FieldType.STRING, 2);
        pnd_Rep_Unit_Nme = localVariables.newFieldInRecord("pnd_Rep_Unit_Nme", "#REP-UNIT-NME", FieldType.STRING, 45);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Tbl_Wpid_ActOld = internalLoopRecord.newFieldInRecord("ReadWork01_Tbl_Wpid_Act_OLD", "Tbl_Wpid_Act_OLD", FieldType.STRING, 1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_hist_Mstr_Index.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Parm_Report_No.setInitialValue(5);
        pnd_Report_Parm.setInitialValue("CWFB3005W*");
        pnd_Parm_Type.setInitialValue("W");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3105() throws Exception
    {
        super("Cwfb3105", true);
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB3105", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        setKeys(ControlKeys.PF3, true);                                                                                                                                   //Natural: SET KEY PF3 = ON
        setControl("WBFL65C22B2/25");                                                                                                                                     //Natural: FORMAT ( 1 ) LS = 144 PS = 56;//Natural: SET CONTROL 'WBFL65C22B2/25'
        pnd_Misc_Data_Pnd_Filler1.setValue("/");                                                                                                                          //Natural: MOVE '/' TO #FILLER1 #FILLER2
        pnd_Misc_Data_Pnd_Filler2.setValue("/");
        pnd_Misc_Data_Pnd_Fillera.setValue("/");                                                                                                                          //Natural: MOVE '/' TO #FILLERA #FILLERB
        pnd_Misc_Data_Pnd_Fillerb.setValue("/");
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        pnd_Misc_Data_Pnd_End_Date.setValue(pnd_Comp_Date);                                                                                                               //Natural: MOVE #COMP-DATE TO #END-DATE
        pnd_Work_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Misc_Data_Pnd_End_Date);                                                                          //Natural: MOVE EDITED #END-DATE TO #WORK-DATE ( EM = YYYYMMDD )
        pnd_Work_Date.nsubtract(6);                                                                                                                                       //Natural: COMPUTE #WORK-DATE = #WORK-DATE - 6
        pnd_Misc_Data_Pnd_Start_Date.setValueEdited(pnd_Work_Date,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED #WORK-DATE ( EM = YYYYMMDD ) TO #START-DATE
        pnd_Work_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Misc_Data_Pnd_Start_Date);                                                                        //Natural: MOVE EDITED #START-DATE TO #WORK-DATE ( EM = YYYYMMDD )
        pnd_Misc_Data_Pnd_Work_Start_Date_A.setValueEdited(pnd_Work_Date,new ReportEditMask("MM'/'DD'/'YY"));                                                             //Natural: MOVE EDITED #WORK-DATE ( EM = MM'/'DD'/'YY ) TO #WORK-START-DATE-A
        pnd_Work_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Misc_Data_Pnd_End_Date);                                                                          //Natural: MOVE EDITED #END-DATE TO #WORK-DATE ( EM = YYYYMMDD )
        pnd_Misc_Data_Pnd_Work_End_Date_A.setValueEdited(pnd_Work_Date,new ReportEditMask("MM'/'DD'/'YY"));                                                               //Natural: MOVE EDITED #WORK-DATE ( EM = MM'/'DD'/'YY ) TO #WORK-END-DATE-A
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK 2 #WORK-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(2, pnd_Work_Record)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(pnd_Save_Unit.equals(" ")))                                                                                                                     //Natural: IF #SAVE-UNIT = ' '
            {
                pnd_First_Unit.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #FIRST-UNIT
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
                pnd_Save_Unit.setValue(pnd_Work_Record_Tbl_Log_Unit_Cde);                                                                                                 //Natural: MOVE #WORK-RECORD.TBL-LOG-UNIT-CDE TO #SAVE-UNIT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_New_Unit.getBoolean() || pnd_First_Unit.getBoolean()))                                                                                      //Natural: IF #NEW-UNIT OR #FIRST-UNIT
            {
                pnd_Parm_Bldg.setValue("W");                                                                                                                              //Natural: MOVE 'W' TO #PARM-BLDG
                DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Parm_Report_No, pnd_Parm_Empl_Racf_Id, pnd_Save_Unit, pnd_Parm_Floor, pnd_Parm_Bldg,       //Natural: CALLNAT 'CWFN3910' #PARM-REPORT-NO #PARM-EMPL-RACF-ID #SAVE-UNIT #PARM-FLOOR #PARM-BLDG #PARM-DROP-OFF #COMP-DATE
                    pnd_Parm_Drop_Off, pnd_Comp_Date);
                if (condition(Global.isEscape())) return;
                pnd_First_Unit.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #FIRST-UNIT
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
            short decideConditionsMet384 = 0;                                                                                                                             //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.TBL-STATUS-CDE;//Natural: VALUE '0000':'0999'
            if (condition(pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("0000") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("0999")))
            {
                decideConditionsMet384++;
                if (condition(pnd_Work_Record_Tbl_Last_Updte_Dte_A.equals(pnd_Misc_Data_Pnd_Start_Date) && pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("0900")))        //Natural: IF #WORK-RECORD.TBL-LAST-UPDTE-DTE-A = #START-DATE AND #WORK-RECORD.TBL-STATUS-CDE GE '0900'
                {
                    pnd_Misc_Data_Pnd_Open_P_Ctr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #OPEN-P-CTR
                    pnd_Misc_Data_Pnd_Total_Open_P_Ctr.nadd(1);                                                                                                           //Natural: ADD 1 TO #TOTAL-OPEN-P-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '4000':'4997'
            if (condition(pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("4000") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("4997")))
            {
                decideConditionsMet384++;
                if (condition(pnd_Work_Record_Tbl_Last_Updte_Dte_A.equals(pnd_Misc_Data_Pnd_Start_Date)))                                                                 //Natural: IF #WORK-RECORD.TBL-LAST-UPDTE-DTE-A = #START-DATE
                {
                    pnd_Misc_Data_Pnd_Open_P_Ctr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #OPEN-P-CTR
                    pnd_Misc_Data_Pnd_Total_Open_P_Ctr.nadd(1);                                                                                                           //Natural: ADD 1 TO #TOTAL-OPEN-P-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '8000':'9999'
            if (condition(pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("8000") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("9999")))
            {
                decideConditionsMet384++;
                ignore();
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet384 == 0))
            {
                if (condition(pnd_Work_Record_Tbl_Last_Updte_Dte_A.equals(pnd_Misc_Data_Pnd_Start_Date)))                                                                 //Natural: IF #WORK-RECORD.TBL-LAST-UPDTE-DTE-A = #START-DATE
                {
                    if (condition(((pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("1000") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("3999")) || (pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("4998")  //Natural: IF #WORK-RECORD.TBL-STATUS-CDE = '1000' THRU '3999' OR #WORK-RECORD.TBL-STATUS-CDE = '4998' THRU '7999'
                        && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("7999")))))
                    {
                        pnd_Misc_Data_Pnd_Open_Ctr.nadd(1);                                                                                                               //Natural: ADD 1 TO #OPEN-CTR
                        pnd_Misc_Data_Pnd_Total_Open_Ctr.nadd(1);                                                                                                         //Natural: ADD 1 TO #TOTAL-OPEN-CTR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Work_Record_Tbl_Last_Updte_Dte_A.equals(pnd_Misc_Data_Pnd_End_Date)))                                                                   //Natural: IF #WORK-RECORD.TBL-LAST-UPDTE-DTE-A = #END-DATE
                {
                    if (condition(((((pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("0000") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("0899")) ||                     //Natural: IF #WORK-RECORD.TBL-STATUS-CDE = '0000' THRU '0899' OR #WORK-RECORD.TBL-STATUS-CDE = '1000' THRU '3999' OR #WORK-RECORD.TBL-STATUS-CDE = '5000' THRU '6999' OR #WORK-RECORD.TBL-STATUS-CDE = '7500' THRU '7999'
                        (pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("1000") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("3999"))) || (pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("5000") 
                        && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("6999"))) || (pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("7500") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("7999")))))
                    {
                        pnd_Misc_Data_Pnd_Open_End.nadd(1);                                                                                                               //Natural: ADD 1 TO #OPEN-END
                        pnd_Misc_Data_Pnd_Total_Open_End.nadd(1);                                                                                                         //Natural: ADD 1 TO #TOTAL-OPEN-END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet411 = 0;                                                                                                                             //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.TBL-STATUS-CDE;//Natural: VALUES '0900':'0999'
            if (condition(pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("0900") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("0999")))
            {
                decideConditionsMet411++;
                if (condition(pnd_Work_Record_Tbl_Last_Updte_Dte_A.equals(pnd_Misc_Data_Pnd_End_Date)))                                                                   //Natural: IF #WORK-RECORD.TBL-LAST-UPDTE-DTE-A = #END-DATE
                {
                    pnd_Misc_Data_Pnd_Pended_I_Ctr.nadd(1);                                                                                                               //Natural: ADD 1 TO #PENDED-I-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUES '4000':'4997'
            if (condition(pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("4000") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("4997")))
            {
                decideConditionsMet411++;
                if (condition(pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("4449")))                                                                                        //Natural: IF #WORK-RECORD.TBL-STATUS-CDE LE '4449'
                {
                    if (condition(pnd_Work_Record_Tbl_Chnge_Unit.notEquals(pnd_Work_Record_Tbl_Admin_Unit)))                                                              //Natural: IF #WORK-RECORD.TBL-CHNGE-UNIT NE #WORK-RECORD.TBL-ADMIN-UNIT
                    {
                        pnd_Misc_Data_Pnd_Routed_I.nadd(1);                                                                                                               //Natural: ADD 1 TO #ROUTED-I
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Work_Record_Tbl_Last_Updte_Dte_A.equals(pnd_Misc_Data_Pnd_End_Date)))                                                               //Natural: IF #WORK-RECORD.TBL-LAST-UPDTE-DTE-A = #END-DATE
                    {
                        if (condition(pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("4499")))                                                                                //Natural: IF #WORK-RECORD.TBL-STATUS-CDE LE '4499'
                        {
                            pnd_Misc_Data_Pnd_Pended_I_Ctr.nadd(1);                                                                                                       //Natural: ADD 1 TO #PENDED-I-CTR
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("4500")))                                                                             //Natural: IF #WORK-RECORD.TBL-STATUS-CDE GE '4500'
                        {
                            pnd_Misc_Data_Pnd_Pended_E_Ctr.nadd(1);                                                                                                       //Natural: ADD 1 TO #PENDED-E-CTR
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUES '7000':'7499'
            if (condition(pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("7000") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("7499")))
            {
                decideConditionsMet411++;
                pnd_Misc_Data_Pnd_Routed_C.nadd(1);                                                                                                                       //Natural: ADD 1 TO #ROUTED-C
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet411 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(((pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("0001") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("0199")) || (pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("0300")  //Natural: IF #WORK-RECORD.TBL-STATUS-CDE = '0001' THRU '0199' OR #WORK-RECORD.TBL-STATUS-CDE = '0300' THRU '0899'
                && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("0899")))))
            {
                pnd_Misc_Data_Pnd_Routed_N.nadd(1);                                                                                                                       //Natural: ADD 1 TO #ROUTED-N
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_New_Ind.greater(getZero())))                                                                                                //Natural: IF #NEW-IND GT 0
            {
                pnd_Misc_Data_Pnd_New_Ctr.nadd(1);                                                                                                                        //Natural: ADD 1 TO #NEW-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Merg_Ind.greater(getZero())))                                                                                               //Natural: IF #MERG-IND GT 0
            {
                pnd_Misc_Data_Pnd_Merg_Ctr.nadd(1);                                                                                                                       //Natural: ADD 1 TO #MERG-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_E_Pended_Ind.greater(getZero())))                                                                                           //Natural: IF #E-PENDED-IND GT 0
            {
                pnd_Misc_Data_Pnd_E_Pended.nadd(1);                                                                                                                       //Natural: ADD 1 TO #E-PENDED
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Partic_Closed_Ind.equals(getZero()) && pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("8000") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("9999")  //Natural: IF #PARTIC-CLOSED-IND = 0 AND #WORK-RECORD.TBL-STATUS-CDE = '8000' THRU '9999' AND #WORK-RECORD.TBL-LAST-UPDTE-DTE = #END-DATE-N
                && pnd_Work_Record_Tbl_Last_Updte_Dte.equals(pnd_Misc_Data_Pnd_End_Date_N)))
            {
                pnd_Misc_Data_Pnd_Partic_Closed.nadd(1);                                                                                                                  //Natural: ADD 1 TO #PARTIC-CLOSED
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Assigned_Ind.greater(getZero())))                                                                                           //Natural: IF #ASSIGNED-IND GT 0
            {
                pnd_Misc_Data_Pnd_Assigned_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #ASSIGNED-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Unassigned_Ind.greater(getZero())))                                                                                         //Natural: IF #UNASSIGNED-IND GT 0
            {
                pnd_Misc_Data_Pnd_Unassigned_Ctr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #UNASSIGNED-CTR
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT BREAK OF #WORK-RECORD.TBL-WPID-ACT
            //*                                                                                                                                                           //Natural: AT BREAK OF TBL-LOG-UNIT-CDE
            //*  READ-2.
            readWork01Tbl_Wpid_ActOld.setValue(pnd_Work_Record_Tbl_Wpid_Act);                                                                                             //Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        pnd_Tbl_Run_Flag.setValue("Y");                                                                                                                                   //Natural: ON ERROR;//Natural: MOVE 'Y' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Comp_Date, pnd_Tbl_Run_Flag);                                                     //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #COMP-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Report_Data_Pnd_Rep_Unit_Cde.setValue(pnd_Save_Unit);                                                                                             //Natural: MOVE #SAVE-UNIT TO #REP-UNIT-CDE
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),"(",pnd_Env,")",new TabSetting(52),"CORPORATE WORKFLOW FACILITIES",new                 //Natural: WRITE ( 1 ) NOTITLE *PROGRAM '(' #ENV ')' 52T 'CORPORATE WORKFLOW FACILITIES' 124T 'Page' *PAGE-NUMBER ( 1 ) ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 53T 'WORK ACTIVITY FOR A  PERIOD' 124T *TIMX ( EM = HH':'II' 'AP )
                        TabSetting(124),"Page",getReports().getPageNumberDbs(1), new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), 
                        new ReportEditMask ("LLL' 'DD','YY"),new TabSetting(53),"WORK ACTIVITY FOR A  PERIOD",new TabSetting(124),Global.getTIMX(), new 
                        ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Report_Data_Pnd_Rep_Unit_Cde, pnd_Rep_Unit_Nme);                                       //Natural: CALLNAT 'CWFN1103' #REP-UNIT-CDE #REP-UNIT-NME
                    if (condition(Global.isEscape())) return;
                    getReports().write(1, ReportOption.NOTITLE,"UNIT      :",pnd_Report_Data_Pnd_Rep_Unit_Cde,pnd_Rep_Unit_Nme);                                          //Natural: WRITE ( 1 ) 'UNIT      :' #REP-UNIT-CDE #REP-UNIT-NME
                    pnd_Report_Data_Pnd_Rep_Empl_Name.setValue(pnd_Report_Data_Pnd_Rep_Unit_Cde);                                                                         //Natural: MOVE #REP-UNIT-CDE TO #REP-EMPL-NAME
                    if (condition(pnd_Report_Data_Pnd_Rep_Racf_Id.greater(" ")))                                                                                          //Natural: IF #REP-RACF-ID GT ' '
                    {
                        DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), pnd_Report_Data_Pnd_Rep_Racf_Id, pnd_Report_Data_Pnd_Rep_Empl_Name);                   //Natural: CALLNAT 'CWFN1107' #REP-RACF-ID #REP-EMPL-NAME
                        if (condition(Global.isEscape())) return;
                        getReports().write(1, ReportOption.NOTITLE,"EMPLOYEE  :",pnd_Report_Data_Pnd_Rep_Empl_Name);                                                      //Natural: WRITE ( 1 ) 'EMPLOYEE  :' #REP-EMPL-NAME
                        getReports().write(1, ReportOption.NOTITLE,"START-DATE:",pnd_Misc_Data_Pnd_Work_Start_Date_A);                                                    //Natural: WRITE ( 1 ) 'START-DATE:' #WORK-START-DATE-A
                        getReports().write(1, ReportOption.NOTITLE,"END-DATE  :",pnd_Misc_Data_Pnd_Work_End_Date_A);                                                      //Natural: WRITE ( 1 ) 'END-DATE  :' #WORK-END-DATE-A
                        getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(131));                                                                              //Natural: WRITE ( 1 ) '=' ( 131 )
                        getReports().skip(1, 1);                                                                                                                          //Natural: SKIP ( 1 ) 1
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Tbl_Run_Flag.setValue(" ");                                                                                                                                   //Natural: MOVE ' ' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Comp_Date, pnd_Tbl_Run_Flag);                                                     //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #COMP-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Work_Record_Tbl_Wpid_ActIsBreak = pnd_Work_Record_Tbl_Wpid_Act.isBreak(endOfData);
        boolean pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak = pnd_Work_Record_Tbl_Log_Unit_Cde.isBreak(endOfData);
        if (condition(pnd_Work_Record_Tbl_Wpid_ActIsBreak || pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak))
        {
            pnd_Misc_Data_Pnd_Wpid_Code.setValue(readWork01Tbl_Wpid_ActOld);                                                                                              //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID-ACT ) TO #WPID-CODE
            short decideConditionsMet460 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WPID-CODE;//Natural: VALUE 'B'
            if (condition((pnd_Misc_Data_Pnd_Wpid_Code.equals("B"))))
            {
                decideConditionsMet460++;
                pnd_Misc_Data_Pnd_Wpid_Desc.setValue("BOOKLET");                                                                                                          //Natural: MOVE 'BOOKLET' TO #WPID-DESC
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Misc_Data_Pnd_Wpid_Code.equals("F"))))
            {
                decideConditionsMet460++;
                pnd_Misc_Data_Pnd_Wpid_Desc.setValue("FORMS");                                                                                                            //Natural: MOVE 'FORMS' TO #WPID-DESC
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Misc_Data_Pnd_Wpid_Code.equals("I"))))
            {
                decideConditionsMet460++;
                pnd_Misc_Data_Pnd_Wpid_Desc.setValue("INQUIRIES");                                                                                                        //Natural: MOVE 'INQUIRIES' TO #WPID-DESC
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Misc_Data_Pnd_Wpid_Code.equals("R"))))
            {
                decideConditionsMet460++;
                pnd_Misc_Data_Pnd_Wpid_Desc.setValue("RESEARCH");                                                                                                         //Natural: MOVE 'RESEARCH' TO #WPID-DESC
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Misc_Data_Pnd_Wpid_Code.equals("T"))))
            {
                decideConditionsMet460++;
                pnd_Misc_Data_Pnd_Wpid_Desc.setValue("TRANSACTIONS");                                                                                                     //Natural: MOVE 'TRANSACTIONS' TO #WPID-DESC
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Misc_Data_Pnd_Wpid_Code.equals("X"))))
            {
                decideConditionsMet460++;
                pnd_Misc_Data_Pnd_Wpid_Desc.setValue("COMPLAINTS");                                                                                                       //Natural: MOVE 'COMPLAINTS' TO #WPID-DESC
            }                                                                                                                                                             //Natural: VALUE 'Z'
            else if (condition((pnd_Misc_Data_Pnd_Wpid_Code.equals("Z"))))
            {
                decideConditionsMet460++;
                pnd_Misc_Data_Pnd_Wpid_Desc.setValue("OTHERS");                                                                                                           //Natural: MOVE 'OTHERS' TO #WPID-DESC
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Misc_Data_Pnd_Wpid_Desc.setValue(" ");                                                                                                                //Natural: MOVE ' ' TO #WPID-DESC
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Misc_Data_Pnd_Total_New_Ctr.nadd(pnd_Misc_Data_Pnd_New_Ctr);                                                                                              //Natural: COMPUTE #TOTAL-NEW-CTR = #TOTAL-NEW-CTR + #NEW-CTR
            pnd_Misc_Data_Pnd_Total_Assigned_Ctr.nadd(pnd_Misc_Data_Pnd_Assigned_Ctr);                                                                                    //Natural: COMPUTE #TOTAL-ASSIGNED-CTR = #TOTAL-ASSIGNED-CTR + #ASSIGNED-CTR
            pnd_Misc_Data_Pnd_Total_Unassigned_Ctr.nadd(pnd_Misc_Data_Pnd_Unassigned_Ctr);                                                                                //Natural: COMPUTE #TOTAL-UNASSIGNED-CTR = #TOTAL-UNASSIGNED-CTR + #UNASSIGNED-CTR
            pnd_Misc_Data_Pnd_Total_Pended_I_Ctr.nadd(pnd_Misc_Data_Pnd_Pended_I_Ctr);                                                                                    //Natural: COMPUTE #TOTAL-PENDED-I-CTR = #TOTAL-PENDED-I-CTR + #PENDED-I-CTR
            pnd_Misc_Data_Pnd_Total_Pended_E_Ctr.nadd(pnd_Misc_Data_Pnd_Pended_E_Ctr);                                                                                    //Natural: COMPUTE #TOTAL-PENDED-E-CTR = #TOTAL-PENDED-E-CTR + #PENDED-E-CTR
            pnd_Misc_Data_Pnd_Total_E_Pended.nadd(pnd_Misc_Data_Pnd_E_Pended);                                                                                            //Natural: COMPUTE #TOTAL-E-PENDED = #TOTAL-E-PENDED + #E-PENDED
            pnd_Misc_Data_Pnd_Total_Routed_C.nadd(pnd_Misc_Data_Pnd_Routed_C);                                                                                            //Natural: COMPUTE #TOTAL-ROUTED-C = #TOTAL-ROUTED-C + #ROUTED-C
            pnd_Misc_Data_Pnd_Total_Routed_I.nadd(pnd_Misc_Data_Pnd_Routed_I);                                                                                            //Natural: COMPUTE #TOTAL-ROUTED-I = #TOTAL-ROUTED-I + #ROUTED-I
            pnd_Misc_Data_Pnd_Total_Routed_N.nadd(pnd_Misc_Data_Pnd_Routed_N);                                                                                            //Natural: COMPUTE #TOTAL-ROUTED-N = #TOTAL-ROUTED-N + #ROUTED-N
            pnd_Misc_Data_Pnd_Total_Merg_Ctr.nadd(pnd_Misc_Data_Pnd_Merg_Ctr);                                                                                            //Natural: COMPUTE #TOTAL-MERG-CTR = #TOTAL-MERG-CTR + #MERG-CTR
            pnd_Misc_Data_Pnd_Total_Partic_Closed.nadd(pnd_Misc_Data_Pnd_Partic_Closed);                                                                                  //Natural: COMPUTE #TOTAL-PARTIC-CLOSED = #TOTAL-PARTIC-CLOSED + #PARTIC-CLOSED
            getReports().display(1, "/",                                                                                                                                  //Natural: DISPLAY ( 1 ) '/' #WPID-DESC ( AL = 13 ) '/OPEN AT/NOT/PENDED' #OPEN-CTR ( EM = ZZZ,ZZ9 ) '/START  //PENDED' #OPEN-P-CTR ( EM = ZZZ,ZZ9 ) '//NEW/CASES' #NEW-CTR ( EM = ZZZ,ZZ9 ) '/MERGED/DURING/PERIOD' #MERG-CTR ( EM = ZZZ,ZZ9 ) 'EXT/PENDED/DURING/PERIOD' #E-PENDED ( EM = ZZZ,ZZ9 ) '/ROUTED/UNIT WRK/COMPLETE' #ROUTED-C ( EM = ZZZ,ZZ9 ) '/ROUTED/UNIT WRK/INCOMPL' #ROUTED-I ( EM = ZZZ,ZZ9 ) '/ROUTED/NO WORK/DONE' #ROUTED-N ( EM = ZZZ,ZZ9 ) '/PARTIC/CLOSED/CASES' #PARTIC-CLOSED ( EM = ZZZ,ZZ9 ) '//ASS"D/AT END' #ASSIGNED-CTR ( EM = ZZZ,ZZ9 ) '/NOT/ASS"D/AT END' #UNASSIGNED-CTR ( EM = ZZZ,ZZ9 ) '//  PENDED/ INTERN' #PENDED-I-CTR ( EM = ZZZ,ZZ9 ) '//AT END/EXTERN' #PENDED-E-CTR ( EM = ZZZ,ZZ9 ) '/OPEN/(NOT PEND)/AT END' #OPEN-END ( EM = ZZZ,ZZ9 )
            		pnd_Misc_Data_Pnd_Wpid_Desc, new AlphanumericLength (13),"/OPEN AT/NOT/PENDED",
            		pnd_Misc_Data_Pnd_Open_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"/START  //PENDED",
            		pnd_Misc_Data_Pnd_Open_P_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"//NEW/CASES",
            		pnd_Misc_Data_Pnd_New_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"/MERGED/DURING/PERIOD",
            		pnd_Misc_Data_Pnd_Merg_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"EXT/PENDED/DURING/PERIOD",
            		pnd_Misc_Data_Pnd_E_Pended, new ReportEditMask ("ZZZ,ZZ9"),"/ROUTED/UNIT WRK/COMPLETE",
            		pnd_Misc_Data_Pnd_Routed_C, new ReportEditMask ("ZZZ,ZZ9"),"/ROUTED/UNIT WRK/INCOMPL",
            		pnd_Misc_Data_Pnd_Routed_I, new ReportEditMask ("ZZZ,ZZ9"),"/ROUTED/NO WORK/DONE",
            		pnd_Misc_Data_Pnd_Routed_N, new ReportEditMask ("ZZZ,ZZ9"),"/PARTIC/CLOSED/CASES",
            		pnd_Misc_Data_Pnd_Partic_Closed, new ReportEditMask ("ZZZ,ZZ9"),"//ASS'D/AT END",
            		pnd_Misc_Data_Pnd_Assigned_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"/NOT/ASS'D/AT END",
            		pnd_Misc_Data_Pnd_Unassigned_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"//  PENDED/ INTERN",
            		pnd_Misc_Data_Pnd_Pended_I_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"//AT END/EXTERN",
            		pnd_Misc_Data_Pnd_Pended_E_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"/OPEN/(NOT PEND)/AT END",
            		pnd_Misc_Data_Pnd_Open_End, new ReportEditMask ("ZZZ,ZZ9"));
            if (condition(Global.isEscape())) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Misc_Data_Pnd_Open_Ctr.reset();                                                                                                                           //Natural: RESET #OPEN-CTR #OPEN-P-CTR #NEW-CTR #MERG-CTR #E-PENDED #ROUTED-C #ROUTED-I #ROUTED-N #PARTIC-CLOSED #ASSIGNED-CTR #UNASSIGNED-CTR #PENDED-I-CTR #PENDED-E-CTR #OPEN-END
            pnd_Misc_Data_Pnd_Open_P_Ctr.reset();
            pnd_Misc_Data_Pnd_New_Ctr.reset();
            pnd_Misc_Data_Pnd_Merg_Ctr.reset();
            pnd_Misc_Data_Pnd_E_Pended.reset();
            pnd_Misc_Data_Pnd_Routed_C.reset();
            pnd_Misc_Data_Pnd_Routed_I.reset();
            pnd_Misc_Data_Pnd_Routed_N.reset();
            pnd_Misc_Data_Pnd_Partic_Closed.reset();
            pnd_Misc_Data_Pnd_Assigned_Ctr.reset();
            pnd_Misc_Data_Pnd_Unassigned_Ctr.reset();
            pnd_Misc_Data_Pnd_Pended_I_Ctr.reset();
            pnd_Misc_Data_Pnd_Pended_E_Ctr.reset();
            pnd_Misc_Data_Pnd_Open_End.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak))
        {
            getReports().write(1, ReportOption.NOTITLE,"-------",new ReportTAsterisk(pnd_Misc_Data_Pnd_Open_Ctr),"-------",new ReportTAsterisk(pnd_Misc_Data_Pnd_Open_P_Ctr),"-------",new  //Natural: WRITE ( 1 ) NOTITLE '-------' T*#OPEN-CTR '-------' T*#OPEN-P-CTR '-------' T*#NEW-CTR '-------' T*#MERG-CTR '-------' T*#E-PENDED '-------' T*#ROUTED-C '-------' T*#ROUTED-I '-------' T*#ROUTED-N '-------' T*#PARTIC-CLOSED '-------' T*#ASSIGNED-CTR '-------' T*#UNASSIGNED-CTR '-------' T*#PENDED-I-CTR '-------' T*#PENDED-E-CTR '-------' T*#OPEN-END '-------' / 'TOTALS' T*#OPEN-CTR #TOTAL-OPEN-CTR ( EM = ZZZ,ZZ9 ) T*#OPEN-P-CTR #TOTAL-OPEN-P-CTR ( EM = ZZZ,ZZ9 ) T*#NEW-CTR #TOTAL-NEW-CTR ( EM = ZZZ,ZZ9 ) T*#MERG-CTR #TOTAL-MERG-CTR ( EM = ZZZ,ZZ9 ) T*#E-PENDED #TOTAL-E-PENDED ( EM = ZZZ,ZZ9 ) T*#ROUTED-C #TOTAL-ROUTED-C ( EM = ZZZ,ZZ9 ) T*#ROUTED-I #TOTAL-ROUTED-I ( EM = ZZZ,ZZ9 ) T*#ROUTED-N #TOTAL-ROUTED-N ( EM = ZZZ,ZZ9 ) T*#PARTIC-CLOSED #TOTAL-PARTIC-CLOSED ( EM = ZZZ,ZZ9 ) T*#ASSIGNED-CTR #TOTAL-ASSIGNED-CTR ( EM = ZZZ,ZZ9 ) T*#UNASSIGNED-CTR #TOTAL-UNASSIGNED-CTR ( EM = ZZZ,ZZ9 ) T*#PENDED-I-CTR #TOTAL-PENDED-I-CTR ( EM = ZZZ,ZZ9 ) T*#PENDED-E-CTR #TOTAL-PENDED-E-CTR ( EM = ZZZ,ZZ9 ) T*#OPEN-END #TOTAL-OPEN-END ( EM = ZZZ,ZZ9 )
                ReportTAsterisk(pnd_Misc_Data_Pnd_New_Ctr),"-------",new ReportTAsterisk(pnd_Misc_Data_Pnd_Merg_Ctr),"-------",new ReportTAsterisk(pnd_Misc_Data_Pnd_E_Pended),"-------",new 
                ReportTAsterisk(pnd_Misc_Data_Pnd_Routed_C),"-------",new ReportTAsterisk(pnd_Misc_Data_Pnd_Routed_I),"-------",new ReportTAsterisk(pnd_Misc_Data_Pnd_Routed_N),"-------",new 
                ReportTAsterisk(pnd_Misc_Data_Pnd_Partic_Closed),"-------",new ReportTAsterisk(pnd_Misc_Data_Pnd_Assigned_Ctr),"-------",new ReportTAsterisk(pnd_Misc_Data_Pnd_Unassigned_Ctr),"-------",new 
                ReportTAsterisk(pnd_Misc_Data_Pnd_Pended_I_Ctr),"-------",new ReportTAsterisk(pnd_Misc_Data_Pnd_Pended_E_Ctr),"-------",new ReportTAsterisk(pnd_Misc_Data_Pnd_Open_End),"-------",NEWLINE,"TOTALS",new 
                ReportTAsterisk(pnd_Misc_Data_Pnd_Open_Ctr),pnd_Misc_Data_Pnd_Total_Open_Ctr, new ReportEditMask ("ZZZ,ZZ9"),new ReportTAsterisk(pnd_Misc_Data_Pnd_Open_P_Ctr),pnd_Misc_Data_Pnd_Total_Open_P_Ctr, 
                new ReportEditMask ("ZZZ,ZZ9"),new ReportTAsterisk(pnd_Misc_Data_Pnd_New_Ctr),pnd_Misc_Data_Pnd_Total_New_Ctr, new ReportEditMask ("ZZZ,ZZ9"),new 
                ReportTAsterisk(pnd_Misc_Data_Pnd_Merg_Ctr),pnd_Misc_Data_Pnd_Total_Merg_Ctr, new ReportEditMask ("ZZZ,ZZ9"),new ReportTAsterisk(pnd_Misc_Data_Pnd_E_Pended),pnd_Misc_Data_Pnd_Total_E_Pended, 
                new ReportEditMask ("ZZZ,ZZ9"),new ReportTAsterisk(pnd_Misc_Data_Pnd_Routed_C),pnd_Misc_Data_Pnd_Total_Routed_C, new ReportEditMask ("ZZZ,ZZ9"),new 
                ReportTAsterisk(pnd_Misc_Data_Pnd_Routed_I),pnd_Misc_Data_Pnd_Total_Routed_I, new ReportEditMask ("ZZZ,ZZ9"),new ReportTAsterisk(pnd_Misc_Data_Pnd_Routed_N),pnd_Misc_Data_Pnd_Total_Routed_N, 
                new ReportEditMask ("ZZZ,ZZ9"),new ReportTAsterisk(pnd_Misc_Data_Pnd_Partic_Closed),pnd_Misc_Data_Pnd_Total_Partic_Closed, new ReportEditMask 
                ("ZZZ,ZZ9"),new ReportTAsterisk(pnd_Misc_Data_Pnd_Assigned_Ctr),pnd_Misc_Data_Pnd_Total_Assigned_Ctr, new ReportEditMask ("ZZZ,ZZ9"),new 
                ReportTAsterisk(pnd_Misc_Data_Pnd_Unassigned_Ctr),pnd_Misc_Data_Pnd_Total_Unassigned_Ctr, new ReportEditMask ("ZZZ,ZZ9"),new ReportTAsterisk(pnd_Misc_Data_Pnd_Pended_I_Ctr),pnd_Misc_Data_Pnd_Total_Pended_I_Ctr, 
                new ReportEditMask ("ZZZ,ZZ9"),new ReportTAsterisk(pnd_Misc_Data_Pnd_Pended_E_Ctr),pnd_Misc_Data_Pnd_Total_Pended_E_Ctr, new ReportEditMask 
                ("ZZZ,ZZ9"),new ReportTAsterisk(pnd_Misc_Data_Pnd_Open_End),pnd_Misc_Data_Pnd_Total_Open_End, new ReportEditMask ("ZZZ,ZZ9"));
            if (condition(Global.isEscape())) return;
            pnd_Misc_Data_Pnd_Total_Open_Ctr.reset();                                                                                                                     //Natural: RESET #TOTAL-OPEN-CTR #TOTAL-OPEN-P-CTR #TOTAL-NEW-CTR #TOTAL-MERG-CTR #TOTAL-E-PENDED #TOTAL-ROUTED-C #TOTAL-ROUTED-I #TOTAL-ROUTED-N #TOTAL-PARTIC-CLOSED #TOTAL-ASSIGNED-CTR #TOTAL-UNASSIGNED-CTR #TOTAL-PENDED-I-CTR #TOTAL-PENDED-E-CTR #TOTAL-OPEN-END
            pnd_Misc_Data_Pnd_Total_Open_P_Ctr.reset();
            pnd_Misc_Data_Pnd_Total_New_Ctr.reset();
            pnd_Misc_Data_Pnd_Total_Merg_Ctr.reset();
            pnd_Misc_Data_Pnd_Total_E_Pended.reset();
            pnd_Misc_Data_Pnd_Total_Routed_C.reset();
            pnd_Misc_Data_Pnd_Total_Routed_I.reset();
            pnd_Misc_Data_Pnd_Total_Routed_N.reset();
            pnd_Misc_Data_Pnd_Total_Partic_Closed.reset();
            pnd_Misc_Data_Pnd_Total_Assigned_Ctr.reset();
            pnd_Misc_Data_Pnd_Total_Unassigned_Ctr.reset();
            pnd_Misc_Data_Pnd_Total_Pended_I_Ctr.reset();
            pnd_Misc_Data_Pnd_Total_Pended_E_Ctr.reset();
            pnd_Misc_Data_Pnd_Total_Open_End.reset();
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())) return;
            pnd_New_Unit.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-UNIT
            pnd_Save_Unit.setValue(pnd_Work_Record_Tbl_Log_Unit_Cde);                                                                                                     //Natural: MOVE #WORK-RECORD.TBL-LOG-UNIT-CDE TO #SAVE-UNIT
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=144 PS=56");

        getReports().setDisplayColumns(1, "/",
        		pnd_Misc_Data_Pnd_Wpid_Desc, new AlphanumericLength (13),"/OPEN AT/NOT/PENDED",
        		pnd_Misc_Data_Pnd_Open_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"/START  //PENDED",
        		pnd_Misc_Data_Pnd_Open_P_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"//NEW/CASES",
        		pnd_Misc_Data_Pnd_New_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"/MERGED/DURING/PERIOD",
        		pnd_Misc_Data_Pnd_Merg_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"EXT/PENDED/DURING/PERIOD",
        		pnd_Misc_Data_Pnd_E_Pended, new ReportEditMask ("ZZZ,ZZ9"),"/ROUTED/UNIT WRK/COMPLETE",
        		pnd_Misc_Data_Pnd_Routed_C, new ReportEditMask ("ZZZ,ZZ9"),"/ROUTED/UNIT WRK/INCOMPL",
        		pnd_Misc_Data_Pnd_Routed_I, new ReportEditMask ("ZZZ,ZZ9"),"/ROUTED/NO WORK/DONE",
        		pnd_Misc_Data_Pnd_Routed_N, new ReportEditMask ("ZZZ,ZZ9"),"/PARTIC/CLOSED/CASES",
        		pnd_Misc_Data_Pnd_Partic_Closed, new ReportEditMask ("ZZZ,ZZ9"),"//ASS'D/AT END",
        		pnd_Misc_Data_Pnd_Assigned_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"/NOT/ASS'D/AT END",
        		pnd_Misc_Data_Pnd_Unassigned_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"//  PENDED/ INTERN",
        		pnd_Misc_Data_Pnd_Pended_I_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"//AT END/EXTERN",
        		pnd_Misc_Data_Pnd_Pended_E_Ctr, new ReportEditMask ("ZZZ,ZZ9"),"/OPEN/(NOT PEND)/AT END",
        		pnd_Misc_Data_Pnd_Open_End, new ReportEditMask ("ZZZ,ZZ9"));
    }
}
