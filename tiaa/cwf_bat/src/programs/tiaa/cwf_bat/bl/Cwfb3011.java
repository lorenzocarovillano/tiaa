/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:26:33 PM
**        * FROM NATURAL PROGRAM : Cwfb3011
************************************************************
**        * FILE NAME            : Cwfb3011.java
**        * CLASS NAME           : Cwfb3011
**        * INSTANCE NAME        : Cwfb3011
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 11
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFP3011
* SYSTEM   : CRPCWF
* TITLE    : REPORT 10
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF PARTICIPANT-CLOSED CASES
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3011 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Tbl_Admin_Unit;
    private DbsField pnd_Work_Record_Tbl_Racf;
    private DbsField pnd_Work_Record_Tbl_Calendar_Days;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Bsnss_Days;
    private DbsField pnd_Work_Record_Tbl_Business_Days;
    private DbsField pnd_Work_Record_Tbl_Complete_Days;
    private DbsField pnd_Work_Record_Tbl_Wpid_Act;
    private DbsField pnd_Work_Record_Tbl_Wpid;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Tbl_Wpid_Action;
    private DbsField pnd_Work_Record_Tbl_Status_Key;

    private DbsGroup pnd_Work_Record__R_Field_2;
    private DbsField pnd_Work_Record_Tbl_Last_Chnge_Unit;
    private DbsField pnd_Work_Record_Tbl_Status_Cde;
    private DbsField pnd_Work_Record_Tbl_Close_Unit;
    private DbsField pnd_Work_Record_Tbl_Pin;
    private DbsField pnd_Work_Record_Tbl_Contracts;
    private DbsField pnd_Work_Record_Tbl_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Dte;
    private DbsField pnd_Work_Record_Tbl_Status_Dte;
    private DbsField pnd_Work_Record_Return_Doc_Rec_Dte_Tme;
    private DbsField pnd_Work_Record_Return_Rcvd_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Last_Chge_Dte;
    private DbsField pnd_Work_Record_Pnd_Partic_Sname;

    private DbsGroup pnd_Work_Record__R_Field_3;
    private DbsField pnd_Work_Record_Pnd_Fldr_Prefix;
    private DbsField pnd_Work_Record_Pnd_Physcl_Fldr_Id_Nbr;
    private DbsField pnd_Work_Record_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Pnd_Last_Chnge_Dte_Tme;
    private DbsField pnd_Rep_Break_Ind;

    private DbsGroup pnd_Misc_Parm;
    private DbsField pnd_Misc_Parm_Pnd_Save_Action;
    private DbsField pnd_Misc_Parm_Pnd_Wpid;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Action;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Count;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Sum;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Bum;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Ave;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Bve;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Text;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Ave_Text;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Bve_Text;
    private DbsField pnd_Misc_Parm_Pnd_Sub_Count;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count_3;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count_4;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count_7;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count_14;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count_Over;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count;
    private DbsField pnd_Misc_Parm_Pnd_Ptotal_Count_3;
    private DbsField pnd_Misc_Parm_Pnd_Ptotal_Count_4;
    private DbsField pnd_Misc_Parm_Pnd_Ptotal_Count_7;
    private DbsField pnd_Misc_Parm_Pnd_Ptotal_Count_14;
    private DbsField pnd_Misc_Parm_Pnd_Ptotal_Count_Over;
    private DbsField pnd_Misc_Parm_Pnd_Rep_Ctr;

    private DbsGroup pnd_Misc_Parm_Pnd_Array_Ctrs;
    private DbsField pnd_Misc_Parm_Pnd_Booklet_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Forms_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Inquire_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Research_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Trans_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Complaint_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Others_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Booklet_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Forms_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Inquire_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Research_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Trans_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Complaint_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Others_Btr;

    private DbsGroup pnd_Misc_Parm_Pnd_Percent_Ctrs;
    private DbsField pnd_Misc_Parm_Pnd_Pbooklet_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Pforms_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Pinquire_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Presearch_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Ptrans_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Pcomplaint_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Pothers_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Pbooklet_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Pforms_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Pinquire_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Presearch_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Ptrans_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Pcomplaint_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Pothers_Btr;

    private DbsGroup pnd_Misc_Parm_Pnd_Total_Ctrs;
    private DbsField pnd_Misc_Parm_Pnd_Tbooklet_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Tforms_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Tinquire_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Tresearch_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Ttrans_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Tcomplaint_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Tothers_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Tbooklet_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Tforms_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Tinquire_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Tresearch_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Ttrans_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Tcomplaint_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Tothers_Btr;
    private DbsField pnd_Misc_Parm_Pnd_Read_Count;
    private DbsField pnd_Misc_Parm_Pnd_Actve_Unque_Key;
    private DbsField pnd_Misc_Parm_Pnd_Todays_Time;
    private DbsField pnd_Misc_Parm_Pnd_Return_Code;
    private DbsField pnd_Misc_Parm_Pnd_Return_Msg;
    private DbsField pnd_Misc_Parm_Pnd_Status_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Return_Doc_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Return_Rcvd_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Work_Date_D;
    private DbsField pnd_Misc_Parm_Pnd_Confirmed;
    private DbsField pnd_Report_No;
    private DbsField pnd_Rep_Unit_Cde;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Env;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Start_Dte_Tme;
    private DbsField pnd_End_Dte_Tme;
    private DbsField pnd_Work_St_Date;
    private DbsField pnd_Work_End_Date;
    private DbsField pnd_Work_Date_Time;

    private DbsGroup pnd_Work_Date_Time__R_Field_4;
    private DbsField pnd_Work_Date_Time_Pnd_Work_Date;
    private DbsField pnd_Work_Date_Time_Pnd_Work_Time;
    private DbsField pnd_Save_Unit;
    private DbsField pnd_First_Unit;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Page_Num;
    private DbsField pnd_Rep_Unit_Nme;
    private DbsField pnd_Parm_Report_No;
    private DbsField pnd_Parm_Empl_Racf_Id;
    private DbsField pnd_Parm_Unit_Cde;
    private DbsField pnd_Parm_Floor;
    private DbsField pnd_Parm_Bldg;
    private DbsField pnd_Parm_Drop_Off;
    private DbsField pnd_Parm_Type;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Tbl_WpidOld;
    private DbsField readWork01Tbl_WpidCount443;
    private DbsField readWork01Tbl_WpidCount;
    private DbsField readWork01Tbl_Wpid_ActOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Tbl_Admin_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Admin_Unit", "TBL-ADMIN-UNIT", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Racf = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Racf", "TBL-RACF", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Calendar_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 
            5);
        pnd_Work_Record_Tbl_Tiaa_Bsnss_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 
            5);
        pnd_Work_Record_Tbl_Business_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.NUMERIC, 
            5, 1);
        pnd_Work_Record_Tbl_Complete_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Complete_Days", "TBL-COMPLETE-DAYS", FieldType.NUMERIC, 
            19, 1);
        pnd_Work_Record_Tbl_Wpid_Act = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 1);
        pnd_Work_Record_Tbl_Wpid = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        pnd_Work_Record__R_Field_1 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record_Tbl_Wpid);
        pnd_Work_Record_Tbl_Wpid_Action = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Work_Record_Tbl_Status_Key = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Key", "TBL-STATUS-KEY", FieldType.STRING, 13);

        pnd_Work_Record__R_Field_2 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_2", "REDEFINE", pnd_Work_Record_Tbl_Status_Key);
        pnd_Work_Record_Tbl_Last_Chnge_Unit = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chnge_Unit", "TBL-LAST-CHNGE-UNIT", 
            FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Status_Cde = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde", "TBL-STATUS-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record_Tbl_Close_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Close_Unit", "TBL-CLOSE-UNIT", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Pin", "TBL-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Tbl_Contracts = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Contracts", "TBL-CONTRACTS", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Dte_Tme", "TBL-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_Record_Tbl_Tiaa_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Dte", "TBL-TIAA-DTE", FieldType.DATE);
        pnd_Work_Record_Tbl_Status_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Dte", "TBL-STATUS-DTE", FieldType.TIME);
        pnd_Work_Record_Return_Doc_Rec_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Return_Rcvd_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Tbl_Last_Chge_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chge_Dte", "TBL-LAST-CHGE-DTE", FieldType.NUMERIC, 
            15);
        pnd_Work_Record_Pnd_Partic_Sname = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);

        pnd_Work_Record__R_Field_3 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_3", "REDEFINE", pnd_Work_Record_Pnd_Partic_Sname);
        pnd_Work_Record_Pnd_Fldr_Prefix = pnd_Work_Record__R_Field_3.newFieldInGroup("pnd_Work_Record_Pnd_Fldr_Prefix", "#FLDR-PREFIX", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_Physcl_Fldr_Id_Nbr = pnd_Work_Record__R_Field_3.newFieldInGroup("pnd_Work_Record_Pnd_Physcl_Fldr_Id_Nbr", "#PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_Pnd_Rqst_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_Work_Record_Pnd_Last_Chnge_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Last_Chnge_Dte_Tme", "#LAST-CHNGE-DTE-TME", FieldType.STRING, 
            15);
        pnd_Rep_Break_Ind = localVariables.newFieldInRecord("pnd_Rep_Break_Ind", "#REP-BREAK-IND", FieldType.STRING, 1);

        pnd_Misc_Parm = localVariables.newGroupInRecord("pnd_Misc_Parm", "#MISC-PARM");
        pnd_Misc_Parm_Pnd_Save_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Save_Action", "#SAVE-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Wpid = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Misc_Parm_Pnd_Wpid_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Action", "#WPID-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Wpid_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Count", "#WPID-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Wpid_Sum = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Sum", "#WPID-SUM", FieldType.NUMERIC, 18);
        pnd_Misc_Parm_Pnd_Wpid_Bum = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Bum", "#WPID-BUM", FieldType.NUMERIC, 18);
        pnd_Misc_Parm_Pnd_Wpid_Ave = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Ave", "#WPID-AVE", FieldType.NUMERIC, 8, 3);
        pnd_Misc_Parm_Pnd_Wpid_Bve = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Bve", "#WPID-BVE", FieldType.NUMERIC, 8, 3);
        pnd_Misc_Parm_Pnd_Wpid_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Sname", "#WPID-SNAME", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Wpid_Text = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Text", "#WPID-TEXT", FieldType.STRING, 66);
        pnd_Misc_Parm_Pnd_Wpid_Ave_Text = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Ave_Text", "#WPID-AVE-TEXT", FieldType.STRING, 66);
        pnd_Misc_Parm_Pnd_Wpid_Bve_Text = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Bve_Text", "#WPID-BVE-TEXT", FieldType.STRING, 66);
        pnd_Misc_Parm_Pnd_Sub_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Sub_Count", "#SUB-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Total_Count_3 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count_3", "#TOTAL-COUNT-3", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Total_Count_4 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count_4", "#TOTAL-COUNT-4", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Total_Count_7 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count_7", "#TOTAL-COUNT-7", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Total_Count_14 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count_14", "#TOTAL-COUNT-14", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Total_Count_Over = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count_Over", "#TOTAL-COUNT-OVER", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Total_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count", "#TOTAL-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Ptotal_Count_3 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptotal_Count_3", "#PTOTAL-COUNT-3", FieldType.FLOAT, 8);
        pnd_Misc_Parm_Pnd_Ptotal_Count_4 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptotal_Count_4", "#PTOTAL-COUNT-4", FieldType.FLOAT, 8);
        pnd_Misc_Parm_Pnd_Ptotal_Count_7 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptotal_Count_7", "#PTOTAL-COUNT-7", FieldType.FLOAT, 8);
        pnd_Misc_Parm_Pnd_Ptotal_Count_14 = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptotal_Count_14", "#PTOTAL-COUNT-14", FieldType.FLOAT, 8);
        pnd_Misc_Parm_Pnd_Ptotal_Count_Over = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptotal_Count_Over", "#PTOTAL-COUNT-OVER", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Rep_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Rep_Ctr", "#REP-CTR", FieldType.NUMERIC, 5);

        pnd_Misc_Parm_Pnd_Array_Ctrs = pnd_Misc_Parm.newGroupArrayInGroup("pnd_Misc_Parm_Pnd_Array_Ctrs", "#ARRAY-CTRS", new DbsArrayController(1, 7));
        pnd_Misc_Parm_Pnd_Booklet_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Booklet_Ctr", "#BOOKLET-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Forms_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Forms_Ctr", "#FORMS-CTR", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Inquire_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Inquire_Ctr", "#INQUIRE-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Research_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Research_Ctr", "#RESEARCH-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Trans_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Trans_Ctr", "#TRANS-CTR", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Complaint_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Complaint_Ctr", "#COMPLAINT-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Others_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Others_Ctr", "#OTHERS-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Booklet_Btr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Booklet_Btr", "#BOOKLET-BTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Forms_Btr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Forms_Btr", "#FORMS-BTR", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Inquire_Btr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Inquire_Btr", "#INQUIRE-BTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Research_Btr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Research_Btr", "#RESEARCH-BTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Trans_Btr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Trans_Btr", "#TRANS-BTR", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Complaint_Btr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Complaint_Btr", "#COMPLAINT-BTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Others_Btr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Others_Btr", "#OTHERS-BTR", FieldType.NUMERIC, 
            5);

        pnd_Misc_Parm_Pnd_Percent_Ctrs = pnd_Misc_Parm.newGroupArrayInGroup("pnd_Misc_Parm_Pnd_Percent_Ctrs", "#PERCENT-CTRS", new DbsArrayController(1, 
            7));
        pnd_Misc_Parm_Pnd_Pbooklet_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pbooklet_Ctr", "#PBOOKLET-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pforms_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pforms_Ctr", "#PFORMS-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pinquire_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pinquire_Ctr", "#PINQUIRE-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Presearch_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Presearch_Ctr", "#PRESEARCH-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Ptrans_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptrans_Ctr", "#PTRANS-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pcomplaint_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pcomplaint_Ctr", "#PCOMPLAINT-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pothers_Ctr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pothers_Ctr", "#POTHERS-CTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pbooklet_Btr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pbooklet_Btr", "#PBOOKLET-BTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pforms_Btr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pforms_Btr", "#PFORMS-BTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pinquire_Btr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pinquire_Btr", "#PINQUIRE-BTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Presearch_Btr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Presearch_Btr", "#PRESEARCH-BTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Ptrans_Btr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Ptrans_Btr", "#PTRANS-BTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pcomplaint_Btr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pcomplaint_Btr", "#PCOMPLAINT-BTR", FieldType.FLOAT, 
            8);
        pnd_Misc_Parm_Pnd_Pothers_Btr = pnd_Misc_Parm_Pnd_Percent_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Pothers_Btr", "#POTHERS-BTR", FieldType.FLOAT, 
            8);

        pnd_Misc_Parm_Pnd_Total_Ctrs = pnd_Misc_Parm.newGroupInGroup("pnd_Misc_Parm_Pnd_Total_Ctrs", "#TOTAL-CTRS");
        pnd_Misc_Parm_Pnd_Tbooklet_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tbooklet_Ctr", "#TBOOKLET-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tforms_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tforms_Ctr", "#TFORMS-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tinquire_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tinquire_Ctr", "#TINQUIRE-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tresearch_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tresearch_Ctr", "#TRESEARCH-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Ttrans_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Ttrans_Ctr", "#TTRANS-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tcomplaint_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tcomplaint_Ctr", "#TCOMPLAINT-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tothers_Ctr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tothers_Ctr", "#TOTHERS-CTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tbooklet_Btr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tbooklet_Btr", "#TBOOKLET-BTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tforms_Btr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tforms_Btr", "#TFORMS-BTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tinquire_Btr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tinquire_Btr", "#TINQUIRE-BTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tresearch_Btr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tresearch_Btr", "#TRESEARCH-BTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Ttrans_Btr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Ttrans_Btr", "#TTRANS-BTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tcomplaint_Btr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tcomplaint_Btr", "#TCOMPLAINT-BTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Tothers_Btr = pnd_Misc_Parm_Pnd_Total_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Tothers_Btr", "#TOTHERS-BTR", FieldType.NUMERIC, 
            5);
        pnd_Misc_Parm_Pnd_Read_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Read_Count", "#READ-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Actve_Unque_Key = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Actve_Unque_Key", "#ACTVE-UNQUE-KEY", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Todays_Time = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Todays_Time", "#TODAYS-TIME", FieldType.TIME);
        pnd_Misc_Parm_Pnd_Return_Code = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 1);
        pnd_Misc_Parm_Pnd_Return_Msg = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 20);
        pnd_Misc_Parm_Pnd_Status_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Status_Sname", "#STATUS-SNAME", FieldType.STRING, 25);
        pnd_Misc_Parm_Pnd_Return_Doc_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Doc_Txt", "#RETURN-DOC-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Return_Rcvd_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Rcvd_Txt", "#RETURN-RCVD-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Work_Date_D = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Work_Date_D", "#WORK-DATE-D", FieldType.DATE);
        pnd_Misc_Parm_Pnd_Confirmed = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Confirmed", "#CONFIRMED", FieldType.BOOLEAN, 1);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Rep_Unit_Cde = localVariables.newFieldInRecord("pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Start_Dte_Tme = localVariables.newFieldInRecord("pnd_Start_Dte_Tme", "#START-DTE-TME", FieldType.TIME);
        pnd_End_Dte_Tme = localVariables.newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.TIME);
        pnd_Work_St_Date = localVariables.newFieldInRecord("pnd_Work_St_Date", "#WORK-ST-DATE", FieldType.STRING, 8);
        pnd_Work_End_Date = localVariables.newFieldInRecord("pnd_Work_End_Date", "#WORK-END-DATE", FieldType.STRING, 8);
        pnd_Work_Date_Time = localVariables.newFieldInRecord("pnd_Work_Date_Time", "#WORK-DATE-TIME", FieldType.NUMERIC, 15);

        pnd_Work_Date_Time__R_Field_4 = localVariables.newGroupInRecord("pnd_Work_Date_Time__R_Field_4", "REDEFINE", pnd_Work_Date_Time);
        pnd_Work_Date_Time_Pnd_Work_Date = pnd_Work_Date_Time__R_Field_4.newFieldInGroup("pnd_Work_Date_Time_Pnd_Work_Date", "#WORK-DATE", FieldType.STRING, 
            8);
        pnd_Work_Date_Time_Pnd_Work_Time = pnd_Work_Date_Time__R_Field_4.newFieldInGroup("pnd_Work_Date_Time_Pnd_Work_Time", "#WORK-TIME", FieldType.STRING, 
            7);
        pnd_Save_Unit = localVariables.newFieldInRecord("pnd_Save_Unit", "#SAVE-UNIT", FieldType.STRING, 8);
        pnd_First_Unit = localVariables.newFieldInRecord("pnd_First_Unit", "#FIRST-UNIT", FieldType.BOOLEAN, 1);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Page_Num = localVariables.newFieldInRecord("pnd_Page_Num", "#PAGE-NUM", FieldType.NUMERIC, 4);
        pnd_Rep_Unit_Nme = localVariables.newFieldInRecord("pnd_Rep_Unit_Nme", "#REP-UNIT-NME", FieldType.STRING, 45);
        pnd_Parm_Report_No = localVariables.newFieldInRecord("pnd_Parm_Report_No", "#PARM-REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Parm_Empl_Racf_Id = localVariables.newFieldInRecord("pnd_Parm_Empl_Racf_Id", "#PARM-EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Parm_Unit_Cde = localVariables.newFieldInRecord("pnd_Parm_Unit_Cde", "#PARM-UNIT-CDE", FieldType.STRING, 8);
        pnd_Parm_Floor = localVariables.newFieldInRecord("pnd_Parm_Floor", "#PARM-FLOOR", FieldType.NUMERIC, 2);
        pnd_Parm_Bldg = localVariables.newFieldInRecord("pnd_Parm_Bldg", "#PARM-BLDG", FieldType.STRING, 3);
        pnd_Parm_Drop_Off = localVariables.newFieldInRecord("pnd_Parm_Drop_Off", "#PARM-DROP-OFF", FieldType.STRING, 2);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Tbl_WpidOld = internalLoopRecord.newFieldInRecord("ReadWork01_Tbl_Wpid_OLD", "Tbl_Wpid_OLD", FieldType.STRING, 6);
        readWork01Tbl_WpidCount443 = internalLoopRecord.newFieldInRecord("ReadWork01_Tbl_Wpid_COUNT_443", "Tbl_Wpid_COUNT_443", FieldType.NUMERIC, 9);
        readWork01Tbl_WpidCount = internalLoopRecord.newFieldInRecord("ReadWork01_Tbl_Wpid_COUNT", "Tbl_Wpid_COUNT", FieldType.NUMERIC, 9);
        readWork01Tbl_Wpid_ActOld = internalLoopRecord.newFieldInRecord("ReadWork01_Tbl_Wpid_Act_OLD", "Tbl_Wpid_Act_OLD", FieldType.STRING, 1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        localVariables.reset();
        pnd_Report_No.setInitialValue(11);
        pnd_Report_Parm.setInitialValue("CWFB3011W*");
        pnd_Parm_Report_No.setInitialValue(11);
        pnd_Parm_Type.setInitialValue("W");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3011() throws Exception
    {
        super("Cwfb3011");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB3011", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Misc_Parm_Pnd_Todays_Time.setValue(Global.getTIMX());                                                                                                         //Natural: MOVE *TIMX TO #TODAYS-TIME
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 142 PS = 58
        //*  GETS SYSTEM RUN DATE ( NEW ONE )
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        //*  RESETS THE Y/N FLAG BUT DOES NOT BUMP UP THE DATE
        //*  YET
        DbsUtil.callnat(Cwfn3913.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off, pnd_Start_Date,        //Natural: CALLNAT 'CWFN3913' #REPORT-PARM #RACF-ID #PARM-UNIT #FLOOR #BLDG #DROP-OFF #START-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.greaterOrEqual(pnd_Start_Date)))                                                                                                      //Natural: IF #COMP-DATE GE #START-DATE
        {
                                                                                                                                                                          //Natural: PERFORM GET-START-DATE
            sub_Get_Start_Date();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ CWF-MASTER-INDEX
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK 1 #WORK-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Work_Record)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(pnd_Save_Unit.equals(" ")))                                                                                                                     //Natural: IF #SAVE-UNIT = ' '
            {
                pnd_First_Unit.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #FIRST-UNIT
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
                pnd_Save_Unit.setValue(pnd_Work_Record_Tbl_Close_Unit);                                                                                                   //Natural: MOVE #WORK-RECORD.TBL-CLOSE-UNIT TO #SAVE-UNIT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_New_Unit.getBoolean() || pnd_First_Unit.getBoolean()))                                                                                      //Natural: IF #NEW-UNIT OR #FIRST-UNIT
            {
                pnd_Parm_Bldg.setValue("W");                                                                                                                              //Natural: MOVE 'W' TO #PARM-BLDG
                DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Parm_Report_No, pnd_Parm_Empl_Racf_Id, pnd_Save_Unit, pnd_Parm_Floor, pnd_Parm_Bldg,       //Natural: CALLNAT 'CWFN3910' #PARM-REPORT-NO #PARM-EMPL-RACF-ID #SAVE-UNIT #PARM-FLOOR #PARM-BLDG #PARM-DROP-OFF #COMP-DATE
                    pnd_Parm_Drop_Off, pnd_Comp_Date);
                if (condition(Global.isEscape())) return;
                pnd_First_Unit.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #FIRST-UNIT
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Cwfn5390.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Wpid, pnd_Misc_Parm_Pnd_Wpid_Sname);                                           //Natural: AT TOP OF PAGE ( 1 );//Natural: CALLNAT 'CWFN5390' #WORK-RECORD.TBL-WPID #WPID-SNAME
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn1102.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Status_Key, pnd_Misc_Parm_Pnd_Status_Sname);                                   //Natural: CALLNAT 'CWFN1102' #WORK-RECORD.TBL-STATUS-KEY #STATUS-SNAME
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Work_Record_Return_Doc_Rec_Dte_Tme.greater(getZero())))                                                                                     //Natural: IF #WORK-RECORD.RETURN-DOC-REC-DTE-TME GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Doc_Txt.setValueEdited(pnd_Work_Record_Return_Doc_Rec_Dte_Tme,new ReportEditMask("MM/DD/YY"));                                   //Natural: MOVE EDITED #WORK-RECORD.RETURN-DOC-REC-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Doc_Txt.setValue(" ");                                                                                                           //Natural: MOVE ' ' TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Return_Rcvd_Dte_Tme.greater(getZero())))                                                                                        //Natural: IF #WORK-RECORD.RETURN-RCVD-DTE-TME GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValueEdited(pnd_Work_Record_Return_Rcvd_Dte_Tme,new ReportEditMask("MM/DD/YY"));                                     //Natural: MOVE EDITED #WORK-RECORD.RETURN-RCVD-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValue(" ");                                                                                                          //Natural: MOVE ' ' TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Misc_Parm_Pnd_Save_Action.notEquals(pnd_Work_Record_Tbl_Wpid_Act)))                                                                         //Natural: IF #SAVE-ACTION NE #WORK-RECORD.TBL-WPID-ACT
            {
                pnd_Misc_Parm_Pnd_Save_Action.setValue(pnd_Work_Record_Tbl_Wpid_Act);                                                                                     //Natural: MOVE #WORK-RECORD.TBL-WPID-ACT TO #SAVE-ACTION
                short decideConditionsMet261 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #SAVE-ACTION;//Natural: VALUE 'B'
                if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("B"))))
                {
                    decideConditionsMet261++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"BOOKLETS    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'BOOKLETS    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("F"))))
                {
                    decideConditionsMet261++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"FORMS       :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'FORMS       :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("I"))))
                {
                    decideConditionsMet261++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"INQUIRY     :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'INQUIRY     :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("R"))))
                {
                    decideConditionsMet261++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"RESEARCH    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'RESEARCH    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("T"))))
                {
                    decideConditionsMet261++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TRANSACTIONS:",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'TRANSACTIONS:' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("X"))))
                {
                    decideConditionsMet261++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"COMPLAINTS  :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'COMPLAINTS  :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("Z"))))
                {
                    decideConditionsMet261++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"OTHERS      :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'OTHERS      :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //* PIN-EXP
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "//RECEIVED/AT TIAA",                                                                                                                 //Natural: DISPLAY ( 1 ) '//RECEIVED/AT TIAA' #WORK-RECORD.TBL-TIAA-DTE ( EM = MM/DD/YY ) '//DOC/REC"D' #RETURN-DOC-TXT '//REC"D IN/UNIT' #RETURN-RCVD-TXT '//WORK/PROCESS' #WPID-SNAME ( IS = ON ) '/PARTICIPANT/CLOSED/STATUS' #STATUS-SNAME ( AL = 15 ) '//STATUS/DATE' #WORK-RECORD.TBL-STATUS-DTE ( EM = MM/DD/YY ) 'CALENDAR/DAYS/IN/TIAA' #WORK-RECORD.TBL-CALENDAR-DAYS ( EM = Z,ZZ9 ) 'BUSINESS/DAYS/IN/TIAA' #WORK-RECORD.TBL-TIAA-BSNSS-DAYS ( EM = Z,ZZ9 ) '//PARTIC/NAME' #PARTIC-SNAME '///PIN' #WORK-RECORD.TBL-PIN ( EM = 999999999999 ) '//CONTRACT/NUMBER' #WORK-RECORD.TBL-CONTRACTS '///EMPLOYEE' #WORK-RECORD.TBL-RACF
            		pnd_Work_Record_Tbl_Tiaa_Dte, new ReportEditMask ("MM/DD/YY"),"//DOC/REC'D",
            		pnd_Misc_Parm_Pnd_Return_Doc_Txt,"//REC'D IN/UNIT",
            		pnd_Misc_Parm_Pnd_Return_Rcvd_Txt,"//WORK/PROCESS",
            		pnd_Misc_Parm_Pnd_Wpid_Sname, new IdenticalSuppress(true),"/PARTICIPANT/CLOSED/STATUS",
            		pnd_Misc_Parm_Pnd_Status_Sname, new AlphanumericLength (15),"//STATUS/DATE",
            		pnd_Work_Record_Tbl_Status_Dte, new ReportEditMask ("MM/DD/YY"),"CALENDAR/DAYS/IN/TIAA",
            		pnd_Work_Record_Tbl_Calendar_Days, new ReportEditMask ("Z,ZZ9"),"BUSINESS/DAYS/IN/TIAA",
            		pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, new ReportEditMask ("Z,ZZ9"),"//PARTIC/NAME",
            		pnd_Work_Record_Pnd_Partic_Sname,"///PIN",
            		pnd_Work_Record_Tbl_Pin, new ReportEditMask ("999999999999"),"//CONTRACT/NUMBER",
            		pnd_Work_Record_Tbl_Contracts,"///EMPLOYEE",
            		pnd_Work_Record_Tbl_Racf);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    '///PIN' #WORK-RECORD.TBL-PIN (EM=9999999)
            short decideConditionsMet284 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-COMPLETE-DAYS;//Natural: VALUE -9999.9:7.9
            if (condition(((pnd_Work_Record_Tbl_Complete_Days.greaterOrEqual(- 9999.9) && pnd_Work_Record_Tbl_Complete_Days.lessOrEqual(7)))))
            {
                decideConditionsMet284++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet287 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("B")))
                {
                    decideConditionsMet287++;
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(1).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 1 )
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(4).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("F")))
                {
                    decideConditionsMet287++;
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(1).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 1 )
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(4).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("I")))
                {
                    decideConditionsMet287++;
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(1).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 1 )
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(4).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("R")))
                {
                    decideConditionsMet287++;
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(1).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 1 )
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(4).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("T")))
                {
                    decideConditionsMet287++;
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(1).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 1 )
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(4).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("X")))
                {
                    decideConditionsMet287++;
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(1).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 1 )
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(4).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("Z")))
                {
                    decideConditionsMet287++;
                    pnd_Misc_Parm_Pnd_Others_Ctr.getValue(1).nadd(1);                                                                                                     //Natural: ADD 1 TO #OTHERS-CTR ( 1 )
                    pnd_Misc_Parm_Pnd_Others_Ctr.getValue(4).nadd(1);                                                                                                     //Natural: ADD 1 TO #OTHERS-CTR ( 4 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet287 > 0))
                {
                    pnd_Misc_Parm_Pnd_Sub_Count.nadd(1);                                                                                                                  //Natural: ADD 1 TO #SUB-COUNT
                    pnd_Misc_Parm_Pnd_Total_Count_7.nadd(1);                                                                                                              //Natural: ADD 1 TO #TOTAL-COUNT-7
                }                                                                                                                                                         //Natural: NONE
                if (condition(decideConditionsMet287 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 8:14.9
            else if (condition(((pnd_Work_Record_Tbl_Complete_Days.greaterOrEqual(8) && pnd_Work_Record_Tbl_Complete_Days.lessOrEqual(14)))))
            {
                decideConditionsMet284++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet317 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("B")))
                {
                    decideConditionsMet317++;
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(2).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 2 )
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(5).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("F")))
                {
                    decideConditionsMet317++;
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(2).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 2 )
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(5).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("I")))
                {
                    decideConditionsMet317++;
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(2).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 2 )
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(5).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("R")))
                {
                    decideConditionsMet317++;
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(2).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 2 )
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(5).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("T")))
                {
                    decideConditionsMet317++;
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(2).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 2 )
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(5).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("X")))
                {
                    decideConditionsMet317++;
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(2).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 2 )
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(5).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("Z")))
                {
                    decideConditionsMet317++;
                    pnd_Misc_Parm_Pnd_Others_Ctr.getValue(2).nadd(1);                                                                                                     //Natural: ADD 1 TO #OTHERS-CTR ( 2 )
                    pnd_Misc_Parm_Pnd_Others_Ctr.getValue(5).nadd(1);                                                                                                     //Natural: ADD 1 TO #OTHERS-CTR ( 5 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet317 > 0))
                {
                    pnd_Misc_Parm_Pnd_Sub_Count.nadd(1);                                                                                                                  //Natural: ADD 1 TO #SUB-COUNT
                    pnd_Misc_Parm_Pnd_Total_Count_14.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOTAL-COUNT-14
                }                                                                                                                                                         //Natural: NONE
                if (condition(decideConditionsMet317 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 15:9999.9
            else if (condition(((pnd_Work_Record_Tbl_Complete_Days.greaterOrEqual(15) && pnd_Work_Record_Tbl_Complete_Days.lessOrEqual(9999)))))
            {
                decideConditionsMet284++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet347 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("B")))
                {
                    decideConditionsMet347++;
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(3).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 3 )
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(6).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 6 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("F")))
                {
                    decideConditionsMet347++;
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(3).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 3 )
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(6).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 6 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("I")))
                {
                    decideConditionsMet347++;
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(3).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 3 )
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(6).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 6 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("R")))
                {
                    decideConditionsMet347++;
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(3).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 3 )
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(6).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 6 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("T")))
                {
                    decideConditionsMet347++;
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(3).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 3 )
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(6).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 6 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("X")))
                {
                    decideConditionsMet347++;
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(3).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 3 )
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(6).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 6 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("Z")))
                {
                    decideConditionsMet347++;
                    pnd_Misc_Parm_Pnd_Others_Ctr.getValue(3).nadd(1);                                                                                                     //Natural: ADD 1 TO #OTHERS-CTR ( 3 )
                    pnd_Misc_Parm_Pnd_Others_Ctr.getValue(6).nadd(1);                                                                                                     //Natural: ADD 1 TO #OTHERS-CTR ( 6 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet347 > 0))
                {
                    pnd_Misc_Parm_Pnd_Sub_Count.nadd(1);                                                                                                                  //Natural: ADD 1 TO #SUB-COUNT
                    pnd_Misc_Parm_Pnd_Total_Count_Over.nadd(1);                                                                                                           //Natural: ADD 1 TO #TOTAL-COUNT-OVER
                }                                                                                                                                                         //Natural: NONE
                if (condition(decideConditionsMet347 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet378 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-TIAA-BSNSS-DAYS;//Natural: VALUE -9999.9:3.9
            if (condition(((pnd_Work_Record_Tbl_Tiaa_Bsnss_Days.greaterOrEqual(- 9999.9) && pnd_Work_Record_Tbl_Tiaa_Bsnss_Days.lessOrEqual(3)))))
            {
                decideConditionsMet378++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet381 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("B")))
                {
                    decideConditionsMet381++;
                    pnd_Misc_Parm_Pnd_Booklet_Btr.getValue(1).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-BTR ( 1 )
                    pnd_Misc_Parm_Pnd_Booklet_Btr.getValue(4).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-BTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("F")))
                {
                    decideConditionsMet381++;
                    pnd_Misc_Parm_Pnd_Forms_Btr.getValue(1).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-BTR ( 1 )
                    pnd_Misc_Parm_Pnd_Forms_Btr.getValue(4).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-BTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("I")))
                {
                    decideConditionsMet381++;
                    pnd_Misc_Parm_Pnd_Inquire_Btr.getValue(1).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-BTR ( 1 )
                    pnd_Misc_Parm_Pnd_Inquire_Btr.getValue(4).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-BTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("R")))
                {
                    decideConditionsMet381++;
                    pnd_Misc_Parm_Pnd_Research_Btr.getValue(1).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-BTR ( 1 )
                    pnd_Misc_Parm_Pnd_Research_Btr.getValue(4).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-BTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("T")))
                {
                    decideConditionsMet381++;
                    pnd_Misc_Parm_Pnd_Trans_Btr.getValue(1).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-BTR ( 1 )
                    pnd_Misc_Parm_Pnd_Trans_Btr.getValue(4).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-BTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("X")))
                {
                    decideConditionsMet381++;
                    pnd_Misc_Parm_Pnd_Complaint_Btr.getValue(1).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-BTR ( 1 )
                    pnd_Misc_Parm_Pnd_Complaint_Btr.getValue(4).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-BTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("Z")))
                {
                    decideConditionsMet381++;
                    pnd_Misc_Parm_Pnd_Others_Btr.getValue(1).nadd(1);                                                                                                     //Natural: ADD 1 TO #OTHERS-BTR ( 1 )
                    pnd_Misc_Parm_Pnd_Others_Btr.getValue(4).nadd(1);                                                                                                     //Natural: ADD 1 TO #OTHERS-BTR ( 4 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet381 > 0))
                {
                    pnd_Misc_Parm_Pnd_Total_Count_3.nadd(1);                                                                                                              //Natural: ADD 1 TO #TOTAL-COUNT-3
                }                                                                                                                                                         //Natural: NONE
                if (condition(decideConditionsMet381 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 4:9999.9
            else if (condition(((pnd_Work_Record_Tbl_Tiaa_Bsnss_Days.greaterOrEqual(4) && pnd_Work_Record_Tbl_Tiaa_Bsnss_Days.lessOrEqual(9999)))))
            {
                decideConditionsMet378++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet410 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("B")))
                {
                    decideConditionsMet410++;
                    pnd_Misc_Parm_Pnd_Booklet_Btr.getValue(2).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-BTR ( 2 )
                    pnd_Misc_Parm_Pnd_Booklet_Btr.getValue(5).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-BTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("F")))
                {
                    decideConditionsMet410++;
                    pnd_Misc_Parm_Pnd_Forms_Btr.getValue(2).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-BTR ( 2 )
                    pnd_Misc_Parm_Pnd_Forms_Btr.getValue(5).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-BTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("I")))
                {
                    decideConditionsMet410++;
                    pnd_Misc_Parm_Pnd_Inquire_Btr.getValue(2).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-BTR ( 2 )
                    pnd_Misc_Parm_Pnd_Inquire_Btr.getValue(5).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-BTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("R")))
                {
                    decideConditionsMet410++;
                    pnd_Misc_Parm_Pnd_Research_Btr.getValue(2).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-BTR ( 2 )
                    pnd_Misc_Parm_Pnd_Research_Btr.getValue(5).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-BTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("T")))
                {
                    decideConditionsMet410++;
                    pnd_Misc_Parm_Pnd_Trans_Btr.getValue(2).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-BTR ( 2 )
                    pnd_Misc_Parm_Pnd_Trans_Btr.getValue(5).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-BTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("X")))
                {
                    decideConditionsMet410++;
                    pnd_Misc_Parm_Pnd_Complaint_Btr.getValue(2).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-BTR ( 2 )
                    pnd_Misc_Parm_Pnd_Complaint_Btr.getValue(5).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-BTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                if (condition(pnd_Work_Record_Tbl_Wpid_Act.equals("Z")))
                {
                    decideConditionsMet410++;
                    pnd_Misc_Parm_Pnd_Others_Btr.getValue(2).nadd(1);                                                                                                     //Natural: ADD 1 TO #OTHERS-BTR ( 2 )
                    pnd_Misc_Parm_Pnd_Others_Btr.getValue(5).nadd(1);                                                                                                     //Natural: ADD 1 TO #OTHERS-BTR ( 5 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet410 > 0))
                {
                    pnd_Misc_Parm_Pnd_Total_Count_4.nadd(1);                                                                                                              //Natural: ADD 1 TO #TOTAL-COUNT-4
                }                                                                                                                                                         //Natural: NONE
                if (condition(decideConditionsMet410 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Misc_Parm_Pnd_Wpid_Sum.nadd(pnd_Work_Record_Tbl_Calendar_Days);                                                                                           //Natural: ADD #WORK-RECORD.TBL-CALENDAR-DAYS TO #WPID-SUM
            pnd_Misc_Parm_Pnd_Wpid_Bum.nadd(pnd_Work_Record_Tbl_Tiaa_Bsnss_Days);                                                                                         //Natural: ADD #WORK-RECORD.TBL-TIAA-BSNSS-DAYS TO #WPID-BUM
            //*                                                                                                                                                           //Natural: AT BREAK OF #WORK-RECORD.TBL-WPID;//Natural: AT BREAK OF #WORK-RECORD.TBL-WPID-ACT;//Natural: AT BREAK OF TBL-CLOSE-UNIT
            //*  READ-2.
            readWork01Tbl_WpidOld.setValue(pnd_Work_Record_Tbl_Wpid);                                                                                                     //Natural: END-WORK
            readWork01Tbl_Wpid_ActOld.setValue(pnd_Work_Record_Tbl_Wpid_Act);
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            readWork01Tbl_WpidCount443.resetBreak();
            readWork01Tbl_WpidCount.resetBreak();
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        pnd_Tbl_Run_Flag.setValue("Y");                                                                                                                                   //Natural: ON ERROR;//Natural: MOVE 'Y' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Comp_Date, pnd_Tbl_Run_Flag);                                                     //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #COMP-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-START-DATE
    }
    private void sub_Get_Start_Date() throws Exception                                                                                                                    //Natural: GET-START-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Misc_Parm_Pnd_Work_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Comp_Date);                                                                       //Natural: MOVE EDITED #COMP-DATE TO #WORK-DATE-D ( EM = YYYYMMDD )
        pnd_Work_End_Date.setValueEdited(pnd_Misc_Parm_Pnd_Work_Date_D,new ReportEditMask("MM'/'DD'/'YY"));                                                               //Natural: MOVE EDITED #WORK-DATE-D ( EM = MM'/'DD'/'YY ) TO #WORK-END-DATE
        pnd_Work_Date_Time_Pnd_Work_Date.setValueEdited(pnd_Misc_Parm_Pnd_Work_Date_D,new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED #WORK-DATE-D ( EM = YYYYMMDD ) TO #WORK-DATE
        pnd_End_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_Date_Time_Pnd_Work_Date);                                                                  //Natural: MOVE EDITED #WORK-DATE TO #END-DTE-TME ( EM = YYYYMMDD )
        pnd_End_Dte_Tme.nadd(863999);                                                                                                                                     //Natural: ADD 863999 TO #END-DTE-TME
        pnd_Misc_Parm_Pnd_Work_Date_D.nsubtract(6);                                                                                                                       //Natural: COMPUTE #WORK-DATE-D = #WORK-DATE-D - 6
        pnd_Work_St_Date.setValueEdited(pnd_Misc_Parm_Pnd_Work_Date_D,new ReportEditMask("MM'/'DD'/'YY"));                                                                //Natural: MOVE EDITED #WORK-DATE-D ( EM = MM'/'DD'/'YY ) TO #WORK-ST-DATE
        pnd_Work_Date_Time_Pnd_Work_Date.setValueEdited(pnd_Misc_Parm_Pnd_Work_Date_D,new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED #WORK-DATE-D ( EM = YYYYMMDD ) TO #WORK-DATE
        pnd_Start_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_Date_Time_Pnd_Work_Date);                                                                //Natural: MOVE EDITED #WORK-DATE TO #START-DTE-TME ( EM = YYYYMMDD )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page_Num.nadd(1);                                                                                                                                 //Natural: COMPUTE #PAGE-NUM = #PAGE-NUM + 1
                    pnd_Rep_Unit_Cde.setValue(pnd_Work_Record_Tbl_Close_Unit);                                                                                            //Natural: MOVE #WORK-RECORD.TBL-CLOSE-UNIT TO #REP-UNIT-CDE
                    DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Rep_Unit_Cde, pnd_Rep_Unit_Nme);                                                       //Natural: CALLNAT 'CWFN1103' #REP-UNIT-CDE #REP-UNIT-NME
                    if (condition(Global.isEscape())) return;
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Env,new TabSetting(52),"CORPORATE WORKFLOW FACILITIES",new TabSetting(124),"PAGE",pnd_Page_Num,  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 52T 'CORPORATE WORKFLOW FACILITIES' 124T 'PAGE' #PAGE-NUM ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 45T 'REPORT OF PARTICIPANT CLOSED WORK REQUESTS' 124T *TIMX ( EM = HH':'II' 'AP ) / 58T 'TURN AROUND REPORT'
                        new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(45),"REPORT OF PARTICIPANT CLOSED WORK REQUESTS",new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new 
                        TabSetting(58),"TURN AROUND REPORT");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    if (condition(pnd_Page_Num.equals(1)))                                                                                                                //Natural: IF #PAGE-NUM = 1
                    {
                        DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Rep_Unit_Cde, pnd_Unit_Name);                                                      //Natural: CALLNAT 'CWFN1103' #REP-UNIT-CDE #UNIT-NAME
                        if (condition(Global.isEscape())) return;
                        getReports().write(1, ReportOption.NOTITLE,"UNIT      :",pnd_Unit_Name);                                                                          //Natural: WRITE ( 1 ) 'UNIT      :' #UNIT-NAME
                        getReports().write(1, ReportOption.NOTITLE,"START DATE:",pnd_Work_St_Date,NEWLINE,"END DATE  :",pnd_Work_End_Date);                               //Natural: WRITE ( 1 ) 'START DATE:' #WORK-ST-DATE / 'END DATE  :' #WORK-END-DATE
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Tbl_Run_Flag.setValue(" ");                                                                                                                                   //Natural: MOVE ' ' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Comp_Date, pnd_Tbl_Run_Flag);                                                     //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #COMP-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Work_Record_Tbl_WpidIsBreak = pnd_Work_Record_Tbl_Wpid.isBreak(endOfData);
        boolean pnd_Work_Record_Tbl_Wpid_ActIsBreak = pnd_Work_Record_Tbl_Wpid_Act.isBreak(endOfData);
        boolean pnd_Work_Record_Tbl_Close_UnitIsBreak = pnd_Work_Record_Tbl_Close_Unit.isBreak(endOfData);
        if (condition(pnd_Work_Record_Tbl_WpidIsBreak || pnd_Work_Record_Tbl_Wpid_ActIsBreak || pnd_Work_Record_Tbl_Close_UnitIsBreak))
        {
            if (condition(pnd_Rep_Break_Ind.equals(" ")))                                                                                                                 //Natural: IF #REP-BREAK-IND = ' '
            {
                pnd_Misc_Parm_Pnd_Wpid.setValue(readWork01Tbl_WpidOld);                                                                                                   //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID ) TO #WPID
                pnd_Misc_Parm_Pnd_Wpid_Count.setValue(readWork01Tbl_WpidCount443);                                                                                        //Natural: MOVE COUNT ( #WORK-RECORD.TBL-WPID ) TO #WPID-COUNT
                pnd_Misc_Parm_Pnd_Wpid_Action.setValue(readWork01Tbl_Wpid_ActOld);                                                                                        //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID-ACT ) TO #WPID-ACTION
                pnd_Misc_Parm_Pnd_Wpid_Ave.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Wpid_Ave), pnd_Misc_Parm_Pnd_Wpid_Sum.divide(pnd_Misc_Parm_Pnd_Wpid_Count)); //Natural: COMPUTE #WPID-AVE = #WPID-SUM / #WPID-COUNT
                pnd_Misc_Parm_Pnd_Wpid_Bve.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Wpid_Bve), pnd_Misc_Parm_Pnd_Wpid_Bum.divide(pnd_Misc_Parm_Pnd_Wpid_Count)); //Natural: COMPUTE #WPID-BVE = #WPID-BUM / #WPID-COUNT
                pnd_Misc_Parm_Pnd_Wpid_Ave_Text.setValue(DbsUtil.compress("AVERAGE CORPORATE TURNAROUND FOR WPID", pnd_Misc_Parm_Pnd_Wpid, "(CALENDAR DAYS)"));           //Natural: COMPRESS 'AVERAGE CORPORATE TURNAROUND FOR WPID' #WPID '(CALENDAR DAYS)' INTO #WPID-AVE-TEXT
                pnd_Misc_Parm_Pnd_Wpid_Bve_Text.setValue(DbsUtil.compress("AVERAGE CORPORATE TURNAROUND FOR WPID", pnd_Misc_Parm_Pnd_Wpid, "(BUSINESS DAYS)"));           //Natural: COMPRESS 'AVERAGE CORPORATE TURNAROUND FOR WPID' #WPID '(BUSINESS DAYS)' INTO #WPID-BVE-TEXT
                //*                                                                                                                                                       //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION
                short decideConditionsMet454 = 0;                                                                                                                         //Natural: VALUE 'B'
                if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("B"))))
                {
                    decideConditionsMet454++;
                    pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL BOOKLETS COMPLETED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                  //Natural: COMPRESS 'TOTAL BOOKLETS COMPLETED FOR WPID' #WPID INTO #WPID-TEXT
                    pnd_Misc_Parm_Pnd_Tbooklet_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Tbooklet_Ctr), pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(4).add(pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(5)).add(pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(6))); //Natural: COMPUTE #TBOOKLET-CTR = #BOOKLET-CTR ( 4 ) + #BOOKLET-CTR ( 5 ) + #BOOKLET-CTR ( 6 )
                    if (condition(pnd_Misc_Parm_Pnd_Tbooklet_Ctr.greater(getZero())))                                                                                     //Natural: IF #TBOOKLET-CTR GT 0
                    {
                        pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(4)), (pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(4).divide(pnd_Misc_Parm_Pnd_Tbooklet_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 4 ) = ( #BOOKLET-CTR ( 4 ) / #TBOOKLET-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(5)), (pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(5).divide(pnd_Misc_Parm_Pnd_Tbooklet_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 5 ) = ( #BOOKLET-CTR ( 5 ) / #TBOOKLET-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(6).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(6)), (pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(6).divide(pnd_Misc_Parm_Pnd_Tbooklet_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 6 ) = ( #BOOKLET-CTR ( 6 ) / #TBOOKLET-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pbooklet_Btr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pbooklet_Btr.getValue(4)), (pnd_Misc_Parm_Pnd_Booklet_Btr.getValue(4).divide(pnd_Misc_Parm_Pnd_Tbooklet_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PBOOKLET-BTR ( 4 ) = ( #BOOKLET-BTR ( 4 ) / #TBOOKLET-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pbooklet_Btr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pbooklet_Btr.getValue(5)), (pnd_Misc_Parm_Pnd_Booklet_Btr.getValue(5).divide(pnd_Misc_Parm_Pnd_Tbooklet_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PBOOKLET-BTR ( 5 ) = ( #BOOKLET-BTR ( 5 ) / #TBOOKLET-CTR ) * 100
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL BOOKLETS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 7 CALENDAR DAYS OR LESS  :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(4),  //Natural: WRITE ( 1 ) / 'TOTAL BOOKLETS COMPLETED FOR WPID' #WPID ' IN 7 CALENDAR DAYS OR LESS  :' #BOOKLET-CTR ( 4 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / 'TOTAL BOOKLETS COMPLETED FOR WPID' #WPID ' IN 8 - 14 CALENDAR DAYS     :' #BOOKLET-CTR ( 5 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / 'TOTAL BOOKLETS COMPLETED FOR WPID' #WPID ' IN OVER 14 CALENDAR DAYS    :' #BOOKLET-CTR ( 6 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' // 'TOTAL BOOKLETS COMPLETED FOR WPID' #WPID ' IN 3 BUSINESS DAYS OR LESS  :' #BOOKLET-BTR ( 4 ) ( AD = OU ) ' (' #PBOOKLET-BTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / 'TOTAL BOOKLETS COMPLETED FOR WPID' #WPID ' IN 4 BUSINESS DAYS OR MORE  :' #BOOKLET-BTR ( 5 ) ( AD = OU ) ' (' #PBOOKLET-BTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' // #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL BOOKLETS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 8 - 14 CALENDAR DAYS     :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL BOOKLETS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN OVER 14 CALENDAR DAYS    :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(6), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,"TOTAL BOOKLETS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 3 BUSINESS DAYS OR LESS  :",pnd_Misc_Parm_Pnd_Booklet_Btr.getValue(4), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pbooklet_Btr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL BOOKLETS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 4 BUSINESS DAYS OR MORE  :",pnd_Misc_Parm_Pnd_Booklet_Btr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pbooklet_Btr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,pnd_Misc_Parm_Pnd_Wpid_Text,"    ",pnd_Misc_Parm_Pnd_Wpid_Count, 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("F"))))
                {
                    decideConditionsMet454++;
                    pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL FORMS SENT FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                          //Natural: COMPRESS 'TOTAL FORMS SENT FOR WPID' #WPID INTO #WPID-TEXT
                    pnd_Misc_Parm_Pnd_Tforms_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Tforms_Ctr), pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(4).add(pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(5)).add(pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(6))); //Natural: COMPUTE #TFORMS-CTR = #FORMS-CTR ( 4 ) + #FORMS-CTR ( 5 ) + #FORMS-CTR ( 6 )
                    if (condition(pnd_Misc_Parm_Pnd_Tforms_Ctr.greater(getZero())))                                                                                       //Natural: IF #TFORMS-CTR GT 0
                    {
                        pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(4)), (pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(4).divide(pnd_Misc_Parm_Pnd_Tforms_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 4 ) = ( #FORMS-CTR ( 4 ) / #TFORMS-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(5)), (pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(5).divide(pnd_Misc_Parm_Pnd_Tforms_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 5 ) = ( #FORMS-CTR ( 5 ) / #TFORMS-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(6).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(6)), (pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(6).divide(pnd_Misc_Parm_Pnd_Tforms_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 6 ) = ( #FORMS-CTR ( 6 ) / #TFORMS-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pforms_Btr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pforms_Btr.getValue(4)), (pnd_Misc_Parm_Pnd_Forms_Btr.getValue(4).divide(pnd_Misc_Parm_Pnd_Tforms_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PFORMS-BTR ( 4 ) = ( #FORMS-BTR ( 4 ) / #TFORMS-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pforms_Btr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pforms_Btr.getValue(5)), (pnd_Misc_Parm_Pnd_Forms_Btr.getValue(5).divide(pnd_Misc_Parm_Pnd_Tforms_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PFORMS-BTR ( 5 ) = ( #FORMS-BTR ( 5 ) / #TFORMS-CTR ) * 100
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL FORMS SENT FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 7 CALENDAR DAYS OR LESS          :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(4),  //Natural: WRITE ( 1 ) / 'TOTAL FORMS SENT FOR WPID' #WPID ' IN 7 CALENDAR DAYS OR LESS          :' #FORMS-CTR ( 4 ) ( AD = OU ) ' (' #PFORMS-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / 'TOTAL FORMS SENT FOR WPID' #WPID ' IN 8 - 14 CALENDAR DAYS             :' #FORMS-CTR ( 5 ) ( AD = OU ) ' (' #PFORMS-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / 'TOTAL FORMS SENT FOR WPID' #WPID ' IN OVER 14 CALENDAR DAYS            :' #FORMS-CTR ( 6 ) ( AD = OU ) ' (' #PFORMS-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' // 'TOTAL FORMS SENT FOR WPID' #WPID ' IN 3 BUSINESS DAYS OR LESS          :' #FORMS-BTR ( 4 ) ( AD = OU ) ' (' #PFORMS-BTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / 'TOTAL FORMS SENT FOR WPID' #WPID ' IN 4 BUSINESS DAYS OR MORE          :' #FORMS-BTR ( 5 ) ( AD = OU ) ' (' #PFORMS-BTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' // #WPID-TEXT '    ' #WPID-COUNT ( AD = OI ) /
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL FORMS SENT FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 8 - 14 CALENDAR DAYS             :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL FORMS SENT FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN OVER 14 CALENDAR DAYS            :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(6), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,"TOTAL FORMS SENT FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 3 BUSINESS DAYS OR LESS          :",pnd_Misc_Parm_Pnd_Forms_Btr.getValue(4), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pforms_Btr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL FORMS SENT FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 4 BUSINESS DAYS OR MORE          :",pnd_Misc_Parm_Pnd_Forms_Btr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pforms_Btr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,pnd_Misc_Parm_Pnd_Wpid_Text,"    ",pnd_Misc_Parm_Pnd_Wpid_Count, 
                        new FieldAttributes ("AD=OI"),NEWLINE);
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("I"))))
                {
                    decideConditionsMet454++;
                    pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL INQUIRIES COMPLETED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                 //Natural: COMPRESS 'TOTAL INQUIRIES COMPLETED FOR WPID' #WPID INTO #WPID-TEXT
                    pnd_Misc_Parm_Pnd_Tinquire_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Tinquire_Ctr), pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(4).add(pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(5)).add(pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(6))); //Natural: COMPUTE #TINQUIRE-CTR = #INQUIRE-CTR ( 4 ) + #INQUIRE-CTR ( 5 ) + #INQUIRE-CTR ( 6 )
                    if (condition(pnd_Misc_Parm_Pnd_Tinquire_Ctr.greater(getZero())))                                                                                     //Natural: IF #TINQUIRE-CTR GT 0
                    {
                        pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(4)), (pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(4).divide(pnd_Misc_Parm_Pnd_Tinquire_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 4 ) = ( #INQUIRE-CTR ( 4 ) / #TINQUIRE-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(5)), (pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(5).divide(pnd_Misc_Parm_Pnd_Tinquire_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 5 ) = ( #INQUIRE-CTR ( 5 ) / #TINQUIRE-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(6).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(6)), (pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(6).divide(pnd_Misc_Parm_Pnd_Tinquire_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 6 ) = ( #INQUIRE-CTR ( 6 ) / #TINQUIRE-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pinquire_Btr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pinquire_Btr.getValue(4)), (pnd_Misc_Parm_Pnd_Inquire_Btr.getValue(4).divide(pnd_Misc_Parm_Pnd_Tinquire_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PINQUIRE-BTR ( 4 ) = ( #INQUIRE-BTR ( 4 ) / #TINQUIRE-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pinquire_Btr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pinquire_Btr.getValue(5)), (pnd_Misc_Parm_Pnd_Inquire_Btr.getValue(5).divide(pnd_Misc_Parm_Pnd_Tinquire_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PINQUIRE-BTR ( 5 ) = ( #INQUIRE-BTR ( 5 ) / #TINQUIRE-CTR ) * 100
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL INQUIRIES COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 7 CALENDAR DAYS OR LESS :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(4),  //Natural: WRITE ( 1 ) / 'TOTAL INQUIRIES COMPLETED FOR WPID' #WPID ' IN 7 CALENDAR DAYS OR LESS :' #INQUIRE-CTR ( 4 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / 'TOTAL INQUIRIES COMPLETED FOR WPID' #WPID ' IN 8 - 14 CALENDAR DAYS    :' #INQUIRE-CTR ( 5 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / 'TOTAL INQUIRIES COMPLETED FOR WPID' #WPID ' IN OVER 14 CALENDAR DAYS   :' #INQUIRE-CTR ( 6 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' // 'TOTAL INQUIRIES COMPLETED FOR WPID' #WPID ' IN 3 BUSINESS DAYS OR LESS :' #INQUIRE-BTR ( 4 ) ( AD = OU ) ' (' #PINQUIRE-BTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / 'TOTAL INQUIRIES COMPLETED FOR WPID' #WPID ' IN 4 BUSINESS DAYS OR MORE :' #INQUIRE-BTR ( 5 ) ( AD = OU ) ' (' #PINQUIRE-BTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' // #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL INQUIRIES COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 8 - 14 CALENDAR DAYS    :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL INQUIRIES COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN OVER 14 CALENDAR DAYS   :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(6), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,"TOTAL INQUIRIES COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 3 BUSINESS DAYS OR LESS :",pnd_Misc_Parm_Pnd_Inquire_Btr.getValue(4), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pinquire_Btr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL INQUIRIES COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 4 BUSINESS DAYS OR MORE :",pnd_Misc_Parm_Pnd_Inquire_Btr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pinquire_Btr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,pnd_Misc_Parm_Pnd_Wpid_Text,"    ",pnd_Misc_Parm_Pnd_Wpid_Count, 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("R"))))
                {
                    decideConditionsMet454++;
                    pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL RESEARCH COMPLETED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                  //Natural: COMPRESS 'TOTAL RESEARCH COMPLETED FOR WPID' #WPID INTO #WPID-TEXT
                    pnd_Misc_Parm_Pnd_Tresearch_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Tresearch_Ctr), pnd_Misc_Parm_Pnd_Research_Ctr.getValue(4).add(pnd_Misc_Parm_Pnd_Research_Ctr.getValue(5)).add(pnd_Misc_Parm_Pnd_Research_Ctr.getValue(6))); //Natural: COMPUTE #TRESEARCH-CTR = #RESEARCH-CTR ( 4 ) + #RESEARCH-CTR ( 5 ) + #RESEARCH-CTR ( 6 )
                    if (condition(pnd_Misc_Parm_Pnd_Tresearch_Ctr.greater(getZero())))                                                                                    //Natural: IF #TRESEARCH-CTR GT 0
                    {
                        pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(4)),                     //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 4 ) = ( #RESEARCH-CTR ( 4 ) / #TRESEARCH-CTR ) * 100
                            (pnd_Misc_Parm_Pnd_Research_Ctr.getValue(4).divide(pnd_Misc_Parm_Pnd_Tresearch_Ctr)).multiply(100));
                        pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(5)),                     //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 5 ) = ( #RESEARCH-CTR ( 5 ) / #TRESEARCH-CTR ) * 100
                            (pnd_Misc_Parm_Pnd_Research_Ctr.getValue(5).divide(pnd_Misc_Parm_Pnd_Tresearch_Ctr)).multiply(100));
                        pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(6).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(6)),                     //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 6 ) = ( #RESEARCH-CTR ( 6 ) / #TRESEARCH-CTR ) * 100
                            (pnd_Misc_Parm_Pnd_Research_Ctr.getValue(6).divide(pnd_Misc_Parm_Pnd_Tresearch_Ctr)).multiply(100));
                        pnd_Misc_Parm_Pnd_Presearch_Btr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Presearch_Btr.getValue(4)),                     //Natural: COMPUTE ROUNDED #PRESEARCH-BTR ( 4 ) = ( #RESEARCH-BTR ( 4 ) / #TRESEARCH-CTR ) * 100
                            (pnd_Misc_Parm_Pnd_Research_Btr.getValue(4).divide(pnd_Misc_Parm_Pnd_Tresearch_Ctr)).multiply(100));
                        pnd_Misc_Parm_Pnd_Presearch_Btr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Presearch_Btr.getValue(5)),                     //Natural: COMPUTE ROUNDED #PRESEARCH-BTR ( 5 ) = ( #RESEARCH-BTR ( 5 ) / #TRESEARCH-CTR ) * 100
                            (pnd_Misc_Parm_Pnd_Research_Btr.getValue(5).divide(pnd_Misc_Parm_Pnd_Tresearch_Ctr)).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL RESEARCH COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 7 CALENDAR DAYS OR LESS  :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(4),  //Natural: WRITE ( 1 ) / 'TOTAL RESEARCH COMPLETED FOR WPID' #WPID ' IN 7 CALENDAR DAYS OR LESS  :' #RESEARCH-CTR ( 4 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / 'TOTAL RESEARCH COMPLETED FOR WPID' #WPID ' IN 8 - 14 CALENDAR DAYS     :' #RESEARCH-CTR ( 5 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / 'TOTAL RESEARCH COMPLETED FOR WPID' #WPID ' IN OVER 14 CALENDAR DAYS    :' #RESEARCH-CTR ( 6 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' // 'TOTAL RESEARCH COMPLETED FOR WPID' #WPID ' IN 3 BUSINESS DAYS OR LESS  :' #RESEARCH-BTR ( 4 ) ( AD = OU ) ' (' #PRESEARCH-BTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / 'TOTAL RESEARCH COMPLETED FOR WPID' #WPID ' IN 4 BUSINESS DAYS OR MORE  :' #RESEARCH-BTR ( 5 ) ( AD = OU ) ' (' #PRESEARCH-BTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' // #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL RESEARCH COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 8 - 14 CALENDAR DAYS     :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL RESEARCH COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN OVER 14 CALENDAR DAYS    :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(6), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,"TOTAL RESEARCH COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 3 BUSINESS DAYS OR LESS  :",pnd_Misc_Parm_Pnd_Research_Btr.getValue(4), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Presearch_Btr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL RESEARCH COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 4 BUSINESS DAYS OR MORE  :",pnd_Misc_Parm_Pnd_Research_Btr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Presearch_Btr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,pnd_Misc_Parm_Pnd_Wpid_Text,"    ",pnd_Misc_Parm_Pnd_Wpid_Count, 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("T"))))
                {
                    decideConditionsMet454++;
                    pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL TRANSACTIONS COMPLETED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                              //Natural: COMPRESS 'TOTAL TRANSACTIONS COMPLETED FOR WPID' #WPID INTO #WPID-TEXT
                    pnd_Misc_Parm_Pnd_Ttrans_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Ttrans_Ctr), pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(4).add(pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(5)).add(pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(6))); //Natural: COMPUTE #TTRANS-CTR = #TRANS-CTR ( 4 ) + #TRANS-CTR ( 5 ) + #TRANS-CTR ( 6 )
                    if (condition(pnd_Misc_Parm_Pnd_Ttrans_Ctr.greater(getZero())))                                                                                       //Natural: IF #TTRANS-CTR GT 0
                    {
                        pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(4)), (pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(4).divide(pnd_Misc_Parm_Pnd_Ttrans_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 4 ) = ( #TRANS-CTR ( 4 ) / #TTRANS-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(5)), (pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(5).divide(pnd_Misc_Parm_Pnd_Ttrans_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 5 ) = ( #TRANS-CTR ( 5 ) / #TTRANS-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(6).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(6)), (pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(6).divide(pnd_Misc_Parm_Pnd_Ttrans_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 6 ) = ( #TRANS-CTR ( 6 ) / #TTRANS-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Ptrans_Btr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptrans_Btr.getValue(4)), (pnd_Misc_Parm_Pnd_Trans_Btr.getValue(4).divide(pnd_Misc_Parm_Pnd_Ttrans_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PTRANS-BTR ( 4 ) = ( #TRANS-BTR ( 4 ) / #TTRANS-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Ptrans_Btr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptrans_Btr.getValue(5)), (pnd_Misc_Parm_Pnd_Trans_Btr.getValue(5).divide(pnd_Misc_Parm_Pnd_Ttrans_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PTRANS-BTR ( 5 ) = ( #TRANS-BTR ( 5 ) / #TTRANS-CTR ) * 100
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL TRANSACTIONS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 7 CAL. DAYS OR LESS  :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(4),  //Natural: WRITE ( 1 ) / 'TOTAL TRANSACTIONS COMPLETED FOR WPID' #WPID ' IN 7 CAL. DAYS OR LESS  :' #TRANS-CTR ( 4 ) ( AD = OU ) ' (' #PTRANS-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / 'TOTAL TRANSACTIONS COMPLETED FOR WPID' #WPID ' IN 8 - 14 CAL. DAYS     :' #TRANS-CTR ( 5 ) ( AD = OU ) ' (' #PTRANS-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / 'TOTAL TRANSACTIONS COMPLETED FOR WPID' #WPID ' IN OVER 14 CAL. DAYS    :' #TRANS-CTR ( 6 ) ( AD = OU ) ' (' #PTRANS-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' // 'TOTAL TRANSACTIONS COMPLETED FOR WPID' #WPID ' IN 3 BSNSS DAYS OR LESS :' #TRANS-BTR ( 4 ) ( AD = OU ) ' (' #PTRANS-BTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / 'TOTAL TRANSACTIONS COMPLETED FOR WPID' #WPID ' IN 4 BSNSS DAYS OR MORE :' #TRANS-BTR ( 5 ) ( AD = OU ) ' (' #PTRANS-BTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' // #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL TRANSACTIONS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 8 - 14 CAL. DAYS     :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL TRANSACTIONS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN OVER 14 CAL. DAYS    :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(6), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,"TOTAL TRANSACTIONS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 3 BSNSS DAYS OR LESS :",pnd_Misc_Parm_Pnd_Trans_Btr.getValue(4), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Ptrans_Btr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL TRANSACTIONS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 4 BSNSS DAYS OR MORE :",pnd_Misc_Parm_Pnd_Trans_Btr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Ptrans_Btr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,pnd_Misc_Parm_Pnd_Wpid_Text,"    ",pnd_Misc_Parm_Pnd_Wpid_Count, 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("X"))))
                {
                    decideConditionsMet454++;
                    pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL COMPLAINTS PROCESSED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                //Natural: COMPRESS 'TOTAL COMPLAINTS PROCESSED FOR WPID' #WPID INTO #WPID-TEXT
                    pnd_Misc_Parm_Pnd_Tcomplaint_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Tcomplaint_Ctr), pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(4).add(pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(5)).add(pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(6))); //Natural: COMPUTE #TCOMPLAINT-CTR = #COMPLAINT-CTR ( 4 ) + #COMPLAINT-CTR ( 5 ) + #COMPLAINT-CTR ( 6 )
                    if (condition(pnd_Misc_Parm_Pnd_Tcomplaint_Ctr.greater(getZero())))                                                                                   //Natural: IF #TCOMPLAINT-CTR GT 0
                    {
                        pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(4)),                   //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 4 ) = ( #COMPLAINT-CTR ( 4 ) / #TCOMPLAINT-CTR ) * 100
                            (pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(4).divide(pnd_Misc_Parm_Pnd_Tcomplaint_Ctr)).multiply(100));
                        pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(5)),                   //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 5 ) = ( #COMPLAINT-CTR ( 5 ) / #TCOMPLAINT-CTR ) * 100
                            (pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(5).divide(pnd_Misc_Parm_Pnd_Tcomplaint_Ctr)).multiply(100));
                        pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(6).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(6)),                   //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 6 ) = ( #COMPLAINT-CTR ( 6 ) / #TCOMPLAINT-CTR ) * 100
                            (pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(6).divide(pnd_Misc_Parm_Pnd_Tcomplaint_Ctr)).multiply(100));
                        pnd_Misc_Parm_Pnd_Pcomplaint_Btr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pcomplaint_Btr.getValue(4)),                   //Natural: COMPUTE ROUNDED #PCOMPLAINT-BTR ( 4 ) = ( #COMPLAINT-BTR ( 4 ) / #TCOMPLAINT-CTR ) * 100
                            (pnd_Misc_Parm_Pnd_Complaint_Btr.getValue(4).divide(pnd_Misc_Parm_Pnd_Tcomplaint_Ctr)).multiply(100));
                        pnd_Misc_Parm_Pnd_Pcomplaint_Btr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pcomplaint_Btr.getValue(5)),                   //Natural: COMPUTE ROUNDED #PCOMPLAINT-BTR ( 5 ) = ( #COMPLAINT-BTR ( 5 ) / #TCOMPLAINT-CTR ) * 100
                            (pnd_Misc_Parm_Pnd_Complaint_Btr.getValue(5).divide(pnd_Misc_Parm_Pnd_Tcomplaint_Ctr)).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL COMPLAINTS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 7 CAL. DAYS OR LESS    :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(4),  //Natural: WRITE ( 1 ) / 'TOTAL COMPLAINTS COMPLETED FOR WPID' #WPID ' IN 7 CAL. DAYS OR LESS    :' #COMPLAINT-CTR ( 4 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / 'TOTAL COMPLAINTS COMPLETED FOR WPID' #WPID ' IN 8 - 14 CAL. DAYS       :' #COMPLAINT-CTR ( 5 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / 'TOTAL COMPLAINTS COMPLETED FOR WPID' #WPID ' IN OVER 14 CAL. DAYS      :' #COMPLAINT-CTR ( 6 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' // 'TOTAL COMPLAINTS COMPLETED FOR WPID' #WPID ' IN 3 BSNSS DAYS OR LESS   :' #COMPLAINT-BTR ( 4 ) ( AD = OU ) ' (' #PCOMPLAINT-BTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / 'TOTAL COMPLAINTS COMPLETED FOR WPID' #WPID ' IN 4 BSNSS DAYS OR MORE   :' #COMPLAINT-BTR ( 5 ) ( AD = OU ) ' (' #PCOMPLAINT-BTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' // #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL COMPLAINTS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 8 - 14 CAL. DAYS       :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL COMPLAINTS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN OVER 14 CAL. DAYS      :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(6), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,"TOTAL COMPLAINTS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 3 BSNSS DAYS OR LESS   :",pnd_Misc_Parm_Pnd_Complaint_Btr.getValue(4), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pcomplaint_Btr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL COMPLAINTS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 4 BSNSS DAYS OR MORE   :",pnd_Misc_Parm_Pnd_Complaint_Btr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pcomplaint_Btr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,pnd_Misc_Parm_Pnd_Wpid_Text,"    ",pnd_Misc_Parm_Pnd_Wpid_Count, 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("Z"))))
                {
                    decideConditionsMet454++;
                    pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OTHERS PROCESSED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                    //Natural: COMPRESS 'TOTAL OTHERS PROCESSED FOR WPID' #WPID INTO #WPID-TEXT
                    pnd_Misc_Parm_Pnd_Tothers_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Tothers_Ctr), pnd_Misc_Parm_Pnd_Others_Ctr.getValue(4).add(pnd_Misc_Parm_Pnd_Others_Ctr.getValue(5)).add(pnd_Misc_Parm_Pnd_Others_Ctr.getValue(6))); //Natural: COMPUTE #TOTHERS-CTR = #OTHERS-CTR ( 4 ) + #OTHERS-CTR ( 5 ) + #OTHERS-CTR ( 6 )
                    if (condition(pnd_Misc_Parm_Pnd_Tothers_Ctr.greater(getZero())))                                                                                      //Natural: IF #TOTHERS-CTR GT 0
                    {
                        pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(4)), (pnd_Misc_Parm_Pnd_Others_Ctr.getValue(4).divide(pnd_Misc_Parm_Pnd_Tothers_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #POTHERS-CTR ( 4 ) = ( #OTHERS-CTR ( 4 ) / #TOTHERS-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(5)), (pnd_Misc_Parm_Pnd_Others_Ctr.getValue(5).divide(pnd_Misc_Parm_Pnd_Tothers_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #POTHERS-CTR ( 5 ) = ( #OTHERS-CTR ( 5 ) / #TOTHERS-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(6).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(6)), (pnd_Misc_Parm_Pnd_Others_Ctr.getValue(6).divide(pnd_Misc_Parm_Pnd_Tothers_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #POTHERS-CTR ( 6 ) = ( #OTHERS-CTR ( 6 ) / #TOTHERS-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pothers_Btr.getValue(4).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pothers_Btr.getValue(4)), (pnd_Misc_Parm_Pnd_Others_Btr.getValue(4).divide(pnd_Misc_Parm_Pnd_Tothers_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #POTHERS-BTR ( 4 ) = ( #OTHERS-BTR ( 4 ) / #TOTHERS-CTR ) * 100
                        pnd_Misc_Parm_Pnd_Pothers_Btr.getValue(5).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pothers_Btr.getValue(5)), (pnd_Misc_Parm_Pnd_Others_Btr.getValue(5).divide(pnd_Misc_Parm_Pnd_Tothers_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #POTHERS-BTR ( 5 ) = ( #OTHERS-BTR ( 5 ) / #TOTHERS-CTR ) * 100
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL OTHERS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 7 CALENDAR DAYS OR LESS    :",pnd_Misc_Parm_Pnd_Others_Ctr.getValue(4),  //Natural: WRITE ( 1 ) / 'TOTAL OTHERS COMPLETED FOR WPID' #WPID ' IN 7 CALENDAR DAYS OR LESS    :' #OTHERS-CTR ( 4 ) ( AD = OU ) ' (' #POTHERS-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS FOR WPID' #WPID ')' / 'TOTAL OTHERS COMPLETED FOR WPID' #WPID ' IN 8 - 14 CALENDAR DAYS       :' #OTHERS-CTR ( 5 ) ( AD = OU ) ' (' #POTHERS-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS FOR WPID' #WPID ')' / 'TOTAL OTHERS COMPLETED FOR WPID' #WPID ' IN OVER 14 CALENDAR DAYS      :' #OTHERS-CTR ( 6 ) ( AD = OU ) ' (' #POTHERS-CTR ( 6 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS FOR WPID' #WPID ')' // 'TOTAL OTHERS COMPLETED FOR WPID' #WPID ' IN 3 BUSINESS DAYS OR LESS    :' #OTHERS-BTR ( 4 ) ( AD = OU ) ' (' #POTHERS-BTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS FOR WPID' #WPID ')' / 'TOTAL OTHERS COMPLETED FOR WPID' #WPID ' IN 4 BUSINESS DAYS OR MORE    :' #OTHERS-BTR ( 5 ) ( AD = OU ) ' (' #POTHERS-BTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS FOR WPID' #WPID ')' // #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL OTHERS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 8 - 14 CALENDAR DAYS       :",pnd_Misc_Parm_Pnd_Others_Ctr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL OTHERS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN OVER 14 CALENDAR DAYS      :",pnd_Misc_Parm_Pnd_Others_Ctr.getValue(6), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(6), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,"TOTAL OTHERS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 3 BUSINESS DAYS OR LESS    :",pnd_Misc_Parm_Pnd_Others_Btr.getValue(4), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pothers_Btr.getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,"TOTAL OTHERS COMPLETED FOR WPID",pnd_Misc_Parm_Pnd_Wpid," IN 4 BUSINESS DAYS OR MORE    :",pnd_Misc_Parm_Pnd_Others_Btr.getValue(5), 
                        new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pothers_Btr.getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS FOR WPID",pnd_Misc_Parm_Pnd_Wpid,")",NEWLINE,NEWLINE,pnd_Misc_Parm_Pnd_Wpid_Text,"    ",pnd_Misc_Parm_Pnd_Wpid_Count, 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet454 > 0))
                {
                    getReports().write(1, ReportOption.NOTITLE,pnd_Misc_Parm_Pnd_Wpid_Ave_Text,"    ",pnd_Misc_Parm_Pnd_Wpid_Ave, new FieldAttributes                     //Natural: WRITE ( 1 ) #WPID-AVE-TEXT '    ' #WPID-AVE ( AD = OI ) / #WPID-BVE-TEXT '    ' #WPID-BVE ( AD = OI ) /
                        ("AD=OI"),NEWLINE,pnd_Misc_Parm_Pnd_Wpid_Bve_Text,"    ",pnd_Misc_Parm_Pnd_Wpid_Bve, new FieldAttributes ("AD=OI"),NEWLINE);
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Misc_Parm_Pnd_Array_Ctrs.getValue(4).reset();                                                                                                         //Natural: RESET #ARRAY-CTRS ( 4 )
                pnd_Misc_Parm_Pnd_Array_Ctrs.getValue(5).reset();                                                                                                         //Natural: RESET #ARRAY-CTRS ( 5 )
                pnd_Misc_Parm_Pnd_Array_Ctrs.getValue(6).reset();                                                                                                         //Natural: RESET #ARRAY-CTRS ( 6 )
                pnd_Misc_Parm_Pnd_Percent_Ctrs.getValue(4).reset();                                                                                                       //Natural: RESET #PERCENT-CTRS ( 4 )
                pnd_Misc_Parm_Pnd_Percent_Ctrs.getValue(5).reset();                                                                                                       //Natural: RESET #PERCENT-CTRS ( 5 )
                pnd_Misc_Parm_Pnd_Percent_Ctrs.getValue(6).reset();                                                                                                       //Natural: RESET #PERCENT-CTRS ( 6 )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Misc_Parm_Pnd_Wpid_Sum.reset();                                                                                                                           //Natural: RESET #WPID-SUM #WPID-BUM
            pnd_Misc_Parm_Pnd_Wpid_Bum.reset();
            readWork01Tbl_WpidCount443.setDec(new DbsDecimal(0));                                                                                                         //Natural: END-BREAK
        }
        if (condition(pnd_Work_Record_Tbl_Wpid_ActIsBreak || pnd_Work_Record_Tbl_Close_UnitIsBreak))
        {
            pnd_Misc_Parm_Pnd_Total_Count.nadd(pnd_Misc_Parm_Pnd_Sub_Count);                                                                                              //Natural: COMPUTE #TOTAL-COUNT = #TOTAL-COUNT + #SUB-COUNT
            pnd_Misc_Parm_Pnd_Wpid_Action.setValue(readWork01Tbl_Wpid_ActOld);                                                                                            //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID-ACT ) TO #WPID-ACTION
            short decideConditionsMet591 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
            if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("B"))))
            {
                decideConditionsMet591++;
                pnd_Misc_Parm_Pnd_Tbooklet_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Tbooklet_Ctr), pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(1).add(pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(2)).add(pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(3))); //Natural: COMPUTE #TBOOKLET-CTR = #BOOKLET-CTR ( 1 ) + #BOOKLET-CTR ( 2 ) + #BOOKLET-CTR ( 3 )
                if (condition(pnd_Misc_Parm_Pnd_Tbooklet_Ctr.greater(getZero())))                                                                                         //Natural: IF #TBOOKLET-CTR GT 0
                {
                    pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(1)), (pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(1).divide(pnd_Misc_Parm_Pnd_Tbooklet_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 1 ) = ( #BOOKLET-CTR ( 1 ) / #TBOOKLET-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(2)), (pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(2).divide(pnd_Misc_Parm_Pnd_Tbooklet_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 2 ) = ( #BOOKLET-CTR ( 2 ) / #TBOOKLET-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(3).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(3)), (pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(3).divide(pnd_Misc_Parm_Pnd_Tbooklet_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 3 ) = ( #BOOKLET-CTR ( 3 ) / #TBOOKLET-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pbooklet_Btr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pbooklet_Btr.getValue(1)), (pnd_Misc_Parm_Pnd_Booklet_Btr.getValue(1).divide(pnd_Misc_Parm_Pnd_Tbooklet_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PBOOKLET-BTR ( 1 ) = ( #BOOKLET-BTR ( 1 ) / #TBOOKLET-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pbooklet_Btr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pbooklet_Btr.getValue(2)), (pnd_Misc_Parm_Pnd_Booklet_Btr.getValue(2).divide(pnd_Misc_Parm_Pnd_Tbooklet_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PBOOKLET-BTR ( 2 ) = ( #BOOKLET-BTR ( 2 ) / #TBOOKLET-CTR ) * 100
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL BOOKLETS COMPLETED IN 7 CALENDAR DAYS OR LESS                   :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(1),  //Natural: WRITE ( 1 ) / 'TOTAL BOOKLETS COMPLETED IN 7 CALENDAR DAYS OR LESS                   :' #BOOKLET-CTR ( 1 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS COMPLETED IN 8 - 14 CALENDAR DAYS                      :' #BOOKLET-CTR ( 2 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS COMPLETED IN OVER 14 CALENDAR DAYS                     :' #BOOKLET-CTR ( 3 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' // 'TOTAL BOOKLETS COMPLETED IN 3 BUSINESS DAYS OR LESS                   :' #BOOKLET-BTR ( 1 ) ( AD = OU ) ' (' #PBOOKLET-BTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS COMPLETED IN 4 BUSINESS DAYS OR MORE                   :' #BOOKLET-BTR ( 2 ) ( AD = OU ) ' (' #PBOOKLET-BTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' // 'TOTAL BOOKLETS COMPLETED                                              :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS COMPLETED IN 8 - 14 CALENDAR DAYS                      :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS COMPLETED IN OVER 14 CALENDAR DAYS                     :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pbooklet_Ctr.getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,NEWLINE,"TOTAL BOOKLETS COMPLETED IN 3 BUSINESS DAYS OR LESS                   :",pnd_Misc_Parm_Pnd_Booklet_Btr.getValue(1), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pbooklet_Btr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS COMPLETED IN 4 BUSINESS DAYS OR MORE                   :",pnd_Misc_Parm_Pnd_Booklet_Btr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pbooklet_Btr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,NEWLINE,"TOTAL BOOKLETS COMPLETED                                              :",pnd_Misc_Parm_Pnd_Sub_Count, 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("F"))))
            {
                decideConditionsMet591++;
                pnd_Misc_Parm_Pnd_Tforms_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Tforms_Ctr), pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(1).add(pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(2)).add(pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(3))); //Natural: COMPUTE #TFORMS-CTR = #FORMS-CTR ( 1 ) + #FORMS-CTR ( 2 ) + #FORMS-CTR ( 3 )
                if (condition(pnd_Misc_Parm_Pnd_Tforms_Ctr.greater(getZero())))                                                                                           //Natural: IF #TFORMS-CTR GT 0
                {
                    pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(1)), (pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(1).divide(pnd_Misc_Parm_Pnd_Tforms_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 1 ) = ( #FORMS-CTR ( 1 ) / #TFORMS-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(2)), (pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(2).divide(pnd_Misc_Parm_Pnd_Tforms_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 2 ) = ( #FORMS-CTR ( 2 ) / #TFORMS-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(3).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(3)), (pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(3).divide(pnd_Misc_Parm_Pnd_Tforms_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 3 ) = ( #FORMS-CTR ( 3 ) / #TFORMS-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pforms_Btr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pforms_Btr.getValue(1)), (pnd_Misc_Parm_Pnd_Forms_Btr.getValue(1).divide(pnd_Misc_Parm_Pnd_Tforms_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PFORMS-BTR ( 1 ) = ( #FORMS-BTR ( 1 ) / #TFORMS-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pforms_Btr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pforms_Btr.getValue(2)), (pnd_Misc_Parm_Pnd_Forms_Btr.getValue(2).divide(pnd_Misc_Parm_Pnd_Tforms_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PFORMS-BTR ( 2 ) = ( #FORMS-BTR ( 2 ) / #TFORMS-CTR ) * 100
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL FORMS SENT IN 7 CALENDAR DAYS OR LESS                           :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(1),  //Natural: WRITE ( 1 ) / 'TOTAL FORMS SENT IN 7 CALENDAR DAYS OR LESS                           :' #FORMS-CTR ( 1 ) ( AD = OU ) ' (' #PFORMS-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS SENT IN 8 - 14 CALENDAR DAYS                              :' #FORMS-CTR ( 2 ) ( AD = OU ) ' (' #PFORMS-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS SENT IN OVER 14 CALENDAR DAYS                             :' #FORMS-CTR ( 3 ) ( AD = OU ) ' (' #PFORMS-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' // 'TOTAL FORMS SENT IN 3 BUSINESS DAYS OR LESS                           :' #FORMS-BTR ( 1 ) ( AD = OU ) ' (' #PFORMS-BTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS SENT IN 4 BUSINESS DAYS OR MORE                           :' #FORMS-BTR ( 2 ) ( AD = OU ) ' (' #PFORMS-BTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' // 'TOTAL FORMS SENT                                                      :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS SENT IN 8 - 14 CALENDAR DAYS                              :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS SENT IN OVER 14 CALENDAR DAYS                             :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pforms_Ctr.getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,NEWLINE,"TOTAL FORMS SENT IN 3 BUSINESS DAYS OR LESS                           :",pnd_Misc_Parm_Pnd_Forms_Btr.getValue(1), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pforms_Btr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS SENT IN 4 BUSINESS DAYS OR MORE                           :",pnd_Misc_Parm_Pnd_Forms_Btr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pforms_Btr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,NEWLINE,"TOTAL FORMS SENT                                                      :",pnd_Misc_Parm_Pnd_Sub_Count, 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("I"))))
            {
                decideConditionsMet591++;
                pnd_Misc_Parm_Pnd_Tinquire_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Tinquire_Ctr), pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(1).add(pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(2)).add(pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(3))); //Natural: COMPUTE #TINQUIRE-CTR = #INQUIRE-CTR ( 1 ) + #INQUIRE-CTR ( 2 ) + #INQUIRE-CTR ( 3 )
                if (condition(pnd_Misc_Parm_Pnd_Tinquire_Ctr.greater(getZero())))                                                                                         //Natural: IF #TINQUIRE-CTR GT 0
                {
                    pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(1)), (pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(1).divide(pnd_Misc_Parm_Pnd_Tinquire_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 1 ) = ( #INQUIRE-CTR ( 1 ) / #TINQUIRE-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(2)), (pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(2).divide(pnd_Misc_Parm_Pnd_Tinquire_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 2 ) = ( #INQUIRE-CTR ( 2 ) / #TINQUIRE-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(3).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(3)), (pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(3).divide(pnd_Misc_Parm_Pnd_Tinquire_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 3 ) = ( #INQUIRE-CTR ( 3 ) / #TINQUIRE-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pinquire_Btr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pinquire_Btr.getValue(1)), (pnd_Misc_Parm_Pnd_Inquire_Btr.getValue(1).divide(pnd_Misc_Parm_Pnd_Tinquire_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PINQUIRE-BTR ( 1 ) = ( #INQUIRE-BTR ( 1 ) / #TINQUIRE-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pinquire_Btr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pinquire_Btr.getValue(2)), (pnd_Misc_Parm_Pnd_Inquire_Btr.getValue(2).divide(pnd_Misc_Parm_Pnd_Tinquire_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PINQUIRE-BTR ( 2 ) = ( #INQUIRE-BTR ( 2 ) / #TINQUIRE-CTR ) * 100
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL INQUIRIES COMPLETED IN 7 CALENDAR DAYS OR LESS                  :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(1),  //Natural: WRITE ( 1 ) / 'TOTAL INQUIRIES COMPLETED IN 7 CALENDAR DAYS OR LESS                  :' #INQUIRE-CTR ( 1 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES COMPLETED IN 8 - 14 CALENDAR DAYS                     :' #INQUIRE-CTR ( 2 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES COMPLETED IN OVER 14 CALENDAR DAYS                    :' #INQUIRE-CTR ( 3 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' // 'TOTAL INQUIRIES COMPLETED IN 3 BUSINESS DAYS OR LESS                  :' #INQUIRE-BTR ( 1 ) ( AD = OU ) ' (' #PINQUIRE-BTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES COMPLETED IN 4 BUSINESS DAYS OR MORE                  :' #INQUIRE-BTR ( 2 ) ( AD = OU ) ' (' #PINQUIRE-BTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' // 'TOTAL INQUIRIES COMPLETED                                             :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES COMPLETED IN 8 - 14 CALENDAR DAYS                     :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES COMPLETED IN OVER 14 CALENDAR DAYS                    :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pinquire_Ctr.getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,NEWLINE,"TOTAL INQUIRIES COMPLETED IN 3 BUSINESS DAYS OR LESS                  :",pnd_Misc_Parm_Pnd_Inquire_Btr.getValue(1), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pinquire_Btr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES COMPLETED IN 4 BUSINESS DAYS OR MORE                  :",pnd_Misc_Parm_Pnd_Inquire_Btr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pinquire_Btr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,NEWLINE,"TOTAL INQUIRIES COMPLETED                                             :",pnd_Misc_Parm_Pnd_Sub_Count, 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("R"))))
            {
                decideConditionsMet591++;
                pnd_Misc_Parm_Pnd_Tresearch_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Tresearch_Ctr), pnd_Misc_Parm_Pnd_Research_Ctr.getValue(1).add(pnd_Misc_Parm_Pnd_Research_Ctr.getValue(2)).add(pnd_Misc_Parm_Pnd_Research_Ctr.getValue(3))); //Natural: COMPUTE #TRESEARCH-CTR = #RESEARCH-CTR ( 1 ) + #RESEARCH-CTR ( 2 ) + #RESEARCH-CTR ( 3 )
                if (condition(pnd_Misc_Parm_Pnd_Tresearch_Ctr.greater(getZero())))                                                                                        //Natural: IF #TRESEARCH-CTR GT 0
                {
                    pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(1)), (pnd_Misc_Parm_Pnd_Research_Ctr.getValue(1).divide(pnd_Misc_Parm_Pnd_Tresearch_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 1 ) = ( #RESEARCH-CTR ( 1 ) / #TRESEARCH-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(2)), (pnd_Misc_Parm_Pnd_Research_Ctr.getValue(2).divide(pnd_Misc_Parm_Pnd_Tresearch_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 2 ) = ( #RESEARCH-CTR ( 2 ) / #TRESEARCH-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(3).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(3)), (pnd_Misc_Parm_Pnd_Research_Ctr.getValue(3).divide(pnd_Misc_Parm_Pnd_Tresearch_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 3 ) = ( #RESEARCH-CTR ( 3 ) / #TRESEARCH-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Presearch_Btr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Presearch_Btr.getValue(1)), (pnd_Misc_Parm_Pnd_Research_Btr.getValue(1).divide(pnd_Misc_Parm_Pnd_Tresearch_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PRESEARCH-BTR ( 1 ) = ( #RESEARCH-BTR ( 1 ) / #TRESEARCH-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Presearch_Btr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Presearch_Btr.getValue(2)), (pnd_Misc_Parm_Pnd_Research_Btr.getValue(2).divide(pnd_Misc_Parm_Pnd_Tresearch_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PRESEARCH-BTR ( 2 ) = ( #RESEARCH-BTR ( 2 ) / #TRESEARCH-CTR ) * 100
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL RESEARCH COMPLETED IN 7 CALENDAR DAYS OR LESS                   :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(1),  //Natural: WRITE ( 1 ) / 'TOTAL RESEARCH COMPLETED IN 7 CALENDAR DAYS OR LESS                   :' #RESEARCH-CTR ( 1 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH COMPLETED IN 8 - 14 CALENDAR DAYS                      :' #RESEARCH-CTR ( 2 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH COMPLETED IN OVER 14 CALENDAR DAYS                     :' #RESEARCH-CTR ( 3 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' // 'TOTAL RESEARCH COMPLETED IN 3 BUSINESS DAYS OR LESS                   :' #RESEARCH-BTR ( 1 ) ( AD = OU ) ' (' #PRESEARCH-BTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH COMPLETED IN 4 BUSINESS DAYS OR MORE                   :' #RESEARCH-BTR ( 2 ) ( AD = OU ) ' (' #PRESEARCH-BTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' // 'TOTAL RESEARCH COMPLETED                                              :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH COMPLETED IN 8 - 14 CALENDAR DAYS                      :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH COMPLETED IN OVER 14 CALENDAR DAYS                     :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Presearch_Ctr.getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,NEWLINE,"TOTAL RESEARCH COMPLETED IN 3 BUSINESS DAYS OR LESS                   :",pnd_Misc_Parm_Pnd_Research_Btr.getValue(1), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Presearch_Btr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH COMPLETED IN 4 BUSINESS DAYS OR MORE                   :",pnd_Misc_Parm_Pnd_Research_Btr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Presearch_Btr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,NEWLINE,"TOTAL RESEARCH COMPLETED                                              :",pnd_Misc_Parm_Pnd_Sub_Count, 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("T"))))
            {
                decideConditionsMet591++;
                pnd_Misc_Parm_Pnd_Ttrans_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Ttrans_Ctr), pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(1).add(pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(2)).add(pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(3))); //Natural: COMPUTE #TTRANS-CTR = #TRANS-CTR ( 1 ) + #TRANS-CTR ( 2 ) + #TRANS-CTR ( 3 )
                if (condition(pnd_Misc_Parm_Pnd_Ttrans_Ctr.greater(getZero())))                                                                                           //Natural: IF #TTRANS-CTR GT 0
                {
                    pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(1)), (pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(1).divide(pnd_Misc_Parm_Pnd_Ttrans_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 1 ) = ( #TRANS-CTR ( 1 ) / #TTRANS-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(2)), (pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(2).divide(pnd_Misc_Parm_Pnd_Ttrans_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 2 ) = ( #TRANS-CTR ( 2 ) / #TTRANS-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(3).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(3)), (pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(3).divide(pnd_Misc_Parm_Pnd_Ttrans_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 3 ) = ( #TRANS-CTR ( 3 ) / #TTRANS-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Ptrans_Btr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptrans_Btr.getValue(1)), (pnd_Misc_Parm_Pnd_Trans_Btr.getValue(1).divide(pnd_Misc_Parm_Pnd_Ttrans_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PTRANS-BTR ( 1 ) = ( #TRANS-BTR ( 1 ) / #TTRANS-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Ptrans_Btr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptrans_Btr.getValue(2)), (pnd_Misc_Parm_Pnd_Trans_Btr.getValue(2).divide(pnd_Misc_Parm_Pnd_Ttrans_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PTRANS-BTR ( 2 ) = ( #TRANS-BTR ( 2 ) / #TTRANS-CTR ) * 100
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL TRANSACTIONS COMPLETED IN 7 CALENDAR DAYS OR LESS               :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(1),  //Natural: WRITE ( 1 ) / 'TOTAL TRANSACTIONS COMPLETED IN 7 CALENDAR DAYS OR LESS               :' #TRANS-CTR ( 1 ) ( AD = OU ) ' (' #PTRANS-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS COMPLETED IN 8 - 14 CALENDAR DAYS                  :' #TRANS-CTR ( 2 ) ( AD = OU ) ' (' #PTRANS-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS COMPLETED IN OVER 14 CALENDAR DAYS                 :' #TRANS-CTR ( 3 ) ( AD = OU ) ' (' #PTRANS-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' // 'TOTAL TRANSACTIONS COMPLETED IN 3 BUSINESS DAYS OR LESS               :' #TRANS-BTR ( 1 ) ( AD = OU ) ' (' #PTRANS-BTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS COMPLETED IN 4 BUSINESS DAYS OR MORE               :' #TRANS-BTR ( 2 ) ( AD = OU ) ' (' #PTRANS-BTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' // 'TOTAL TRANSACTIONS COMPLETED                                          :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS COMPLETED IN 8 - 14 CALENDAR DAYS                  :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS COMPLETED IN OVER 14 CALENDAR DAYS                 :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Ptrans_Ctr.getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,NEWLINE,"TOTAL TRANSACTIONS COMPLETED IN 3 BUSINESS DAYS OR LESS               :",pnd_Misc_Parm_Pnd_Trans_Btr.getValue(1), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Ptrans_Btr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS COMPLETED IN 4 BUSINESS DAYS OR MORE               :",pnd_Misc_Parm_Pnd_Trans_Btr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Ptrans_Btr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,NEWLINE,"TOTAL TRANSACTIONS COMPLETED                                          :",pnd_Misc_Parm_Pnd_Sub_Count, 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("X"))))
            {
                decideConditionsMet591++;
                pnd_Misc_Parm_Pnd_Tcomplaint_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Tcomplaint_Ctr), pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(1).add(pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(2)).add(pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(3))); //Natural: COMPUTE #TCOMPLAINT-CTR = #COMPLAINT-CTR ( 1 ) + #COMPLAINT-CTR ( 2 ) + #COMPLAINT-CTR ( 3 )
                if (condition(pnd_Misc_Parm_Pnd_Tcomplaint_Ctr.greater(getZero())))                                                                                       //Natural: IF #TCOMPLAINT-CTR GT 0
                {
                    pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(1)), (pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(1).divide(pnd_Misc_Parm_Pnd_Tcomplaint_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 1 ) = ( #COMPLAINT-CTR ( 1 ) / #TCOMPLAINT-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(2)), (pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(2).divide(pnd_Misc_Parm_Pnd_Tcomplaint_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 2 ) = ( #COMPLAINT-CTR ( 2 ) / #TCOMPLAINT-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(3).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(3)), (pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(3).divide(pnd_Misc_Parm_Pnd_Tcomplaint_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 3 ) = ( #COMPLAINT-CTR ( 3 ) / #TCOMPLAINT-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pcomplaint_Btr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pcomplaint_Btr.getValue(1)), (pnd_Misc_Parm_Pnd_Complaint_Btr.getValue(1).divide(pnd_Misc_Parm_Pnd_Tcomplaint_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PCOMPLAINT-BTR ( 1 ) = ( #COMPLAINT-BTR ( 1 ) / #TCOMPLAINT-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pcomplaint_Btr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pcomplaint_Btr.getValue(2)), (pnd_Misc_Parm_Pnd_Complaint_Btr.getValue(2).divide(pnd_Misc_Parm_Pnd_Tcomplaint_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #PCOMPLAINT-BTR ( 2 ) = ( #COMPLAINT-BTR ( 2 ) / #TCOMPLAINT-CTR ) * 100
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL COMPLAINTS PROCESSED IN 7 CALENDAR DAYS OR LESS                 :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(1),  //Natural: WRITE ( 1 ) / 'TOTAL COMPLAINTS PROCESSED IN 7 CALENDAR DAYS OR LESS                 :' #COMPLAINT-CTR ( 1 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS PROCESSED IN 8 - 14 CALENDAR DAYS                    :' #COMPLAINT-CTR ( 2 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS PROCESSED IN OVER 14 CALENDAR DAYS                   :' #COMPLAINT-CTR ( 3 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)'// 'TOTAL COMPLAINTS PROCESSED IN 3 BUSINESS DAYS OR LESS                 :' #COMPLAINT-BTR ( 1 ) ( AD = OU ) ' (' #PCOMPLAINT-BTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS PROCESSED IN 4 BUSINESS DAYS OR MORE                 :' #COMPLAINT-BTR ( 2 ) ( AD = OU ) ' (' #PCOMPLAINT-BTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)'// 'TOTAL COMPLAINTS PROCESSED                                            :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS PROCESSED IN 8 - 14 CALENDAR DAYS                    :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS PROCESSED IN OVER 14 CALENDAR DAYS                   :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pcomplaint_Ctr.getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,NEWLINE,"TOTAL COMPLAINTS PROCESSED IN 3 BUSINESS DAYS OR LESS                 :",pnd_Misc_Parm_Pnd_Complaint_Btr.getValue(1), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pcomplaint_Btr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS PROCESSED IN 4 BUSINESS DAYS OR MORE                 :",pnd_Misc_Parm_Pnd_Complaint_Btr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pcomplaint_Btr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,NEWLINE,"TOTAL COMPLAINTS PROCESSED                                            :",pnd_Misc_Parm_Pnd_Sub_Count, 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'Z'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("Z"))))
            {
                decideConditionsMet591++;
                pnd_Misc_Parm_Pnd_Tothers_Ctr.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Tothers_Ctr), pnd_Misc_Parm_Pnd_Others_Ctr.getValue(1).add(pnd_Misc_Parm_Pnd_Others_Ctr.getValue(2)).add(pnd_Misc_Parm_Pnd_Others_Ctr.getValue(3))); //Natural: COMPUTE #TOTHERS-CTR = #OTHERS-CTR ( 1 ) + #OTHERS-CTR ( 2 ) + #OTHERS-CTR ( 3 )
                if (condition(pnd_Misc_Parm_Pnd_Tothers_Ctr.greater(getZero())))                                                                                          //Natural: IF #TOTHERS-CTR GT 0
                {
                    pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(1)), (pnd_Misc_Parm_Pnd_Others_Ctr.getValue(1).divide(pnd_Misc_Parm_Pnd_Tothers_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #POTHERS-CTR ( 1 ) = ( #OTHERS-CTR ( 1 ) / #TOTHERS-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(2)), (pnd_Misc_Parm_Pnd_Others_Ctr.getValue(2).divide(pnd_Misc_Parm_Pnd_Tothers_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #POTHERS-CTR ( 2 ) = ( #OTHERS-CTR ( 2 ) / #TOTHERS-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(3).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(3)), (pnd_Misc_Parm_Pnd_Others_Ctr.getValue(3).divide(pnd_Misc_Parm_Pnd_Tothers_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #POTHERS-CTR ( 3 ) = ( #OTHERS-CTR ( 3 ) / #TOTHERS-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pothers_Btr.getValue(1).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pothers_Btr.getValue(1)), (pnd_Misc_Parm_Pnd_Others_Btr.getValue(1).divide(pnd_Misc_Parm_Pnd_Tothers_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #POTHERS-BTR ( 1 ) = ( #OTHERS-BTR ( 1 ) / #TOTHERS-CTR ) * 100
                    pnd_Misc_Parm_Pnd_Pothers_Btr.getValue(2).compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Pothers_Btr.getValue(2)), (pnd_Misc_Parm_Pnd_Others_Btr.getValue(2).divide(pnd_Misc_Parm_Pnd_Tothers_Ctr)).multiply(100)); //Natural: COMPUTE ROUNDED #POTHERS-BTR ( 2 ) = ( #OTHERS-BTR ( 2 ) / #TOTHERS-CTR ) * 100
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL OTHERS PROCESSED IN 7 CALENDAR DAYS OR LESS                     :",pnd_Misc_Parm_Pnd_Others_Ctr.getValue(1),  //Natural: WRITE ( 1 ) / 'TOTAL OTHERS PROCESSED IN 7 CALENDAR DAYS OR LESS                     :' #OTHERS-CTR ( 1 ) ( AD = OU ) ' (' #POTHERS-CTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS)' / 'TOTAL OTHERS PROCESSED IN 8 - 14 CALENDAR DAYS                        :' #OTHERS-CTR ( 2 ) ( AD = OU ) ' (' #POTHERS-CTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS)' / 'TOTAL OTHERS PROCESSED IN OVER 14 CALENDAR DAYS                       :' #OTHERS-CTR ( 3 ) ( AD = OU ) ' (' #POTHERS-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS)' // 'TOTAL OTHERS PROCESSED IN 3 BUSINESS DAYS OR LESS                     :' #OTHERS-BTR ( 1 ) ( AD = OU ) ' (' #POTHERS-BTR ( 1 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS)' / 'TOTAL OTHERS PROCESSED IN 4 BUSINESS DAYS OR MORE                     :' #OTHERS-BTR ( 2 ) ( AD = OU ) ' (' #POTHERS-BTR ( 2 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS)' // 'TOTAL OTHERS PROCESSED                                                :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS)",NEWLINE,"TOTAL OTHERS PROCESSED IN 8 - 14 CALENDAR DAYS                        :",pnd_Misc_Parm_Pnd_Others_Ctr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS)",NEWLINE,"TOTAL OTHERS PROCESSED IN OVER 14 CALENDAR DAYS                       :",pnd_Misc_Parm_Pnd_Others_Ctr.getValue(3), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pothers_Ctr.getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS)",NEWLINE,NEWLINE,"TOTAL OTHERS PROCESSED IN 3 BUSINESS DAYS OR LESS                     :",pnd_Misc_Parm_Pnd_Others_Btr.getValue(1), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pothers_Btr.getValue(1), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS)",NEWLINE,"TOTAL OTHERS PROCESSED IN 4 BUSINESS DAYS OR MORE                     :",pnd_Misc_Parm_Pnd_Others_Btr.getValue(2), 
                    new FieldAttributes ("AD=OU")," (",pnd_Misc_Parm_Pnd_Pothers_Btr.getValue(2), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS)",NEWLINE,NEWLINE,"TOTAL OTHERS PROCESSED                                                :",pnd_Misc_Parm_Pnd_Sub_Count, 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue("*").reset();                                                                                                          //Natural: RESET #BOOKLET-CTR ( * ) #FORMS-CTR ( * ) #INQUIRE-CTR ( * ) #RESEARCH-CTR ( * ) #TRANS-CTR ( * ) #COMPLAINT-CTR ( * ) #OTHERS-CTR ( * ) #BOOKLET-BTR ( * ) #FORMS-BTR ( * ) #INQUIRE-BTR ( * ) #RESEARCH-BTR ( * ) #TRANS-BTR ( * ) #COMPLAINT-BTR ( * ) #OTHERS-BTR ( * ) #SUB-COUNT
            pnd_Misc_Parm_Pnd_Forms_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Research_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Trans_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Others_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Booklet_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Forms_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Inquire_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Research_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Trans_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Complaint_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Others_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Sub_Count.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Work_Record_Tbl_Close_UnitIsBreak))
        {
            if (condition(pnd_Misc_Parm_Pnd_Total_Count.greater(getZero())))                                                                                              //Natural: IF #TOTAL-COUNT GT 0
            {
                pnd_Misc_Parm_Pnd_Ptotal_Count_3.compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptotal_Count_3), (pnd_Misc_Parm_Pnd_Total_Count_3.divide(pnd_Misc_Parm_Pnd_Total_Count)).multiply(100)); //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-3 = ( #TOTAL-COUNT-3 / #TOTAL-COUNT ) * 100
                pnd_Misc_Parm_Pnd_Ptotal_Count_4.compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptotal_Count_4), (pnd_Misc_Parm_Pnd_Total_Count_4.divide(pnd_Misc_Parm_Pnd_Total_Count)).multiply(100)); //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-4 = ( #TOTAL-COUNT-4 / #TOTAL-COUNT ) * 100
                pnd_Misc_Parm_Pnd_Ptotal_Count_7.compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptotal_Count_7), (pnd_Misc_Parm_Pnd_Total_Count_7.divide(pnd_Misc_Parm_Pnd_Total_Count)).multiply(100)); //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-7 = ( #TOTAL-COUNT-7 / #TOTAL-COUNT ) * 100
                pnd_Misc_Parm_Pnd_Ptotal_Count_14.compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptotal_Count_14), (pnd_Misc_Parm_Pnd_Total_Count_14.divide(pnd_Misc_Parm_Pnd_Total_Count)).multiply(100)); //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-14 = ( #TOTAL-COUNT-14 / #TOTAL-COUNT ) * 100
                pnd_Misc_Parm_Pnd_Ptotal_Count_Over.compute(new ComputeParameters(true, pnd_Misc_Parm_Pnd_Ptotal_Count_Over), (pnd_Misc_Parm_Pnd_Total_Count_Over.divide(pnd_Misc_Parm_Pnd_Total_Count)).multiply(100)); //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-OVER = ( #TOTAL-COUNT-OVER / #TOTAL-COUNT ) * 100
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"GRAND TOTAL OF WORK REQUESTS","COMPLETED IN 7 CALENDAR DAYS OR LESS.....:",pnd_Misc_Parm_Pnd_Total_Count_7," (",pnd_Misc_Parm_Pnd_Ptotal_Count_7,  //Natural: WRITE ( 1 ) // 'GRAND TOTAL OF WORK REQUESTS' 'COMPLETED IN 7 CALENDAR DAYS OR LESS.....:' #TOTAL-COUNT-7 ' (' #PTOTAL-COUNT-7 ( EM = ZZ9.999 ) '% OF TOTAL WORK REQUESTS)' / 'GRAND TOTAL OF WORK REQUESTS' 'COMPLETED IN 8 - 14 CALENDAR DAYS........:' #TOTAL-COUNT-14 ' (' #PTOTAL-COUNT-14 ( EM = ZZ9.999 ) '% OF TOTAL WORK REQUESTS)' / 'GRAND TOTAL OF WORK REQUESTS' 'COMPLETED IN OVER 14 CALENDAR DAYS.......:' #TOTAL-COUNT-OVER ' (' #PTOTAL-COUNT-OVER ( EM = ZZ9.999 ) '% OF TOTAL WORK REQUESTS)' // 'GRAND TOTAL OF WORK REQUESTS' 'COMPLETED IN 3 BUSINESS DAYS OR LESS.....:' #TOTAL-COUNT-3 ' (' #PTOTAL-COUNT-3 ( EM = ZZ9.999 ) '% OF TOTAL WORK REQUESTS)' / 'GRAND TOTAL OF WORK REQUESTS' 'COMPLETED IN 4 BUSINESS DAYS OR MORE.....:' #TOTAL-COUNT-4 ' (' #PTOTAL-COUNT-4 ( EM = ZZ9.999 ) '% OF TOTAL WORK REQUESTS)' // 'GRAND TOTAL.................' '.........................................:' #TOTAL-COUNT /
                new ReportEditMask ("ZZ9.999"),"% OF TOTAL WORK REQUESTS)",NEWLINE,"GRAND TOTAL OF WORK REQUESTS","COMPLETED IN 8 - 14 CALENDAR DAYS........:",pnd_Misc_Parm_Pnd_Total_Count_14," (",pnd_Misc_Parm_Pnd_Ptotal_Count_14, 
                new ReportEditMask ("ZZ9.999"),"% OF TOTAL WORK REQUESTS)",NEWLINE,"GRAND TOTAL OF WORK REQUESTS","COMPLETED IN OVER 14 CALENDAR DAYS.......:",pnd_Misc_Parm_Pnd_Total_Count_Over," (",pnd_Misc_Parm_Pnd_Ptotal_Count_Over, 
                new ReportEditMask ("ZZ9.999"),"% OF TOTAL WORK REQUESTS)",NEWLINE,NEWLINE,"GRAND TOTAL OF WORK REQUESTS","COMPLETED IN 3 BUSINESS DAYS OR LESS.....:",pnd_Misc_Parm_Pnd_Total_Count_3," (",pnd_Misc_Parm_Pnd_Ptotal_Count_3, 
                new ReportEditMask ("ZZ9.999"),"% OF TOTAL WORK REQUESTS)",NEWLINE,"GRAND TOTAL OF WORK REQUESTS","COMPLETED IN 4 BUSINESS DAYS OR MORE.....:",pnd_Misc_Parm_Pnd_Total_Count_4," (",pnd_Misc_Parm_Pnd_Ptotal_Count_4, 
                new ReportEditMask ("ZZ9.999"),"% OF TOTAL WORK REQUESTS)",NEWLINE,NEWLINE,"GRAND TOTAL.................",".........................................:",
                pnd_Misc_Parm_Pnd_Total_Count,NEWLINE);
            if (condition(Global.isEscape())) return;
            pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue("*").reset();                                                                                                          //Natural: RESET #BOOKLET-CTR ( * ) #FORMS-CTR ( * ) #INQUIRE-CTR ( * ) #RESEARCH-CTR ( * ) #TRANS-CTR ( * ) #COMPLAINT-CTR ( * ) #OTHERS-CTR ( * ) #BOOKLET-BTR ( * ) #FORMS-BTR ( * ) #INQUIRE-BTR ( * ) #RESEARCH-BTR ( * ) #TRANS-BTR ( * ) #COMPLAINT-BTR ( * ) #OTHERS-BTR ( * ) #SUB-COUNT #PAGE-NUM #TOTAL-COUNT-3 #TOTAL-COUNT-4 #TOTAL-COUNT-7 #TOTAL-COUNT-14 #TOTAL-COUNT-OVER #TOTAL-COUNT
            pnd_Misc_Parm_Pnd_Forms_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Research_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Trans_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Others_Ctr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Booklet_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Forms_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Inquire_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Research_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Trans_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Complaint_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Others_Btr.getValue("*").reset();
            pnd_Misc_Parm_Pnd_Sub_Count.reset();
            pnd_Page_Num.reset();
            pnd_Misc_Parm_Pnd_Total_Count_3.reset();
            pnd_Misc_Parm_Pnd_Total_Count_4.reset();
            pnd_Misc_Parm_Pnd_Total_Count_7.reset();
            pnd_Misc_Parm_Pnd_Total_Count_14.reset();
            pnd_Misc_Parm_Pnd_Total_Count_Over.reset();
            pnd_Misc_Parm_Pnd_Total_Count.reset();
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())) return;
            pnd_New_Unit.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-UNIT
            pnd_Save_Unit.setValue(pnd_Work_Record_Tbl_Close_Unit);                                                                                                       //Natural: MOVE #WORK-RECORD.TBL-CLOSE-UNIT TO #SAVE-UNIT
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=142 PS=58");

        getReports().setDisplayColumns(1, "//RECEIVED/AT TIAA",
        		pnd_Work_Record_Tbl_Tiaa_Dte, new ReportEditMask ("MM/DD/YY"),"//DOC/REC'D",
        		pnd_Misc_Parm_Pnd_Return_Doc_Txt,"//REC'D IN/UNIT",
        		pnd_Misc_Parm_Pnd_Return_Rcvd_Txt,"//WORK/PROCESS",
        		pnd_Misc_Parm_Pnd_Wpid_Sname, new IdenticalSuppress(true),"/PARTICIPANT/CLOSED/STATUS",
        		pnd_Misc_Parm_Pnd_Status_Sname, new AlphanumericLength (15),"//STATUS/DATE",
        		pnd_Work_Record_Tbl_Status_Dte, new ReportEditMask ("MM/DD/YY"),"CALENDAR/DAYS/IN/TIAA",
        		pnd_Work_Record_Tbl_Calendar_Days, new ReportEditMask ("Z,ZZ9"),"BUSINESS/DAYS/IN/TIAA",
        		pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, new ReportEditMask ("Z,ZZ9"),"//PARTIC/NAME",
        		pnd_Work_Record_Pnd_Partic_Sname,"///PIN",
        		pnd_Work_Record_Tbl_Pin, new ReportEditMask ("999999999999"),"//CONTRACT/NUMBER",
        		pnd_Work_Record_Tbl_Contracts,"///EMPLOYEE",
        		pnd_Work_Record_Tbl_Racf);
    }
}
