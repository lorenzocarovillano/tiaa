/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:32:46 PM
**        * FROM NATURAL PROGRAM : Cwfb4035
************************************************************
**        * FILE NAME            : Cwfb4035.java
**        * CLASS NAME           : Cwfb4035
**        * INSTANCE NAME        : Cwfb4035
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: DOCUMENT TYPE
**SAG SYSTEM: CRPCWF
**SAG REPORT-HEADING(1): CWF DOCUMENT TYPE  CODES EXTRACT REPORT
**SAG PRINT-FILE(1): 1
**SAG HEADING-LINE: 10000000
**SAG DESCS(1): THIS PROGRAM READS THE CWF-WP-DCUMENT TABLE AND WRITES
**SAG DESCS(2): A WORK FILE TO BE DOWNLOADED TO THE SQL SERVER.
**SAG PRIMARY-FILE: CWF-WP-DCMNT-TYPE-TBL
**SAG PRIMARY-KEY: DCMNT-TYPE-CDE-KEY
************************************************************************
* PROGRAM  : CWFB4035
* SYSTEM   : CRPCWF
* TITLE    : DOCUMENT TYPE
* GENERATED: AUG 12,96 AT 05:25 PM
* FUNCTION : THIS PROGRAM READS THE CWF-WP-DCUMENT TABLE AND WRITES
*            A WORK FILE TO BE DOWNLOADED TO THE SQL SERVER.
*
*
* HISTORY
**SAG DEFINE EXIT CHANGE-HISTORY
* CHANGED ON SEP 12,95 BY HARGRAV FOR RELEASE ____
* >
* >
* >
**SAG END-EXIT
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4035 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Wp_Dcmnt_Type_Tbl;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde;

    private DbsGroup cwf_Wp_Dcmnt_Type_Tbl__R_Field_1;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde_1_1;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde_2_4;

    private DbsGroup cwf_Wp_Dcmnt_Type_Tbl__R_Field_2;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_Fill_1;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde_2_8;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Nme;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_Actve_Ind;

    private DbsGroup cwf_Wp_Dcmnt_Type_Tbl__R_Field_3;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_Fill_2;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_Actve_Ind_2_2;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_Dlte_Oprtr_Cde;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;

    private DbsGroup work_Record;
    private DbsField work_Record_Pnd_Work_Record;

    private DbsGroup work_Record__R_Field_4;
    private DbsField work_Record_Dcmnt_Type_Cde;
    private DbsField work_Record_Pnd_C1;
    private DbsField work_Record_Dcmnt_Nme;
    private DbsField work_Record_Pnd_C2;
    private DbsField work_Record_Attrbte_Work_Prcss_Id;
    private DbsField work_Record_Pnd_C3;
    private DbsField work_Record_Attrbte_Drctn_Cde;
    private DbsField work_Record_Pnd_C4;
    private DbsField work_Record_Attrbte_Scrty_Cde;
    private DbsField work_Record_Pnd_C5;
    private DbsField work_Record_Attrbte_Rtntn_Cde;
    private DbsField work_Record_Pnd_C6;
    private DbsField work_Record_Form_Nbr;
    private DbsField work_Record_Pnd_C7;
    private DbsField work_Record_Form_Nme;
    private DbsField work_Record_Pnd_C8;
    private DbsField work_Record_Eye_Rdble_Cde;

    private DataAccessProgramView vw_cwf_Wp_Form_Nbr_Erc_Tbl;
    private DbsField cwf_Wp_Form_Nbr_Erc_Tbl_Form_Dcmnt_Type_Cde;
    private DbsField cwf_Wp_Form_Nbr_Erc_Tbl_Form_Nbr;
    private DbsField cwf_Wp_Form_Nbr_Erc_Tbl_Form_Nme;
    private DbsField cwf_Wp_Form_Nbr_Erc_Tbl_Form_Work_Prcss_Id;
    private DbsField cwf_Wp_Form_Nbr_Erc_Tbl_Eye_Rdble_Cde;

    private DataAccessProgramView vw_cwf_Wp_Dcmnt_Attrbte_Tbl;
    private DbsField cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Dcmnt_Type_Cde;
    private DbsField cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Work_Prcss_Id;
    private DbsField cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Drctn_Cde;
    private DbsField cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Rtntn_Cde;
    private DbsField cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Scrty_Cde;

    private DbsGroup counters;
    private DbsField counters_Pnd_Doc_Type_Rex_Read;
    private DbsField counters_Pnd_Erc_Rex_Read;
    private DbsField counters_Pnd_No_Doc_Rex;
    private DbsField counters_Pnd_Attr_Rex_Read;
    private DbsField counters_Pnd_No_Attr_Rex;
    private DbsField counters_Pnd_Rex_Written;
    private DbsField pnd_Doc_Type;
    private DbsField pnd_No_Records_Found;
    private DbsField pnd_Delimiter;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Wp_Dcmnt_Type_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Dcmnt_Type_Tbl", "CWF-WP-DCMNT-TYPE-TBL"), "CWF_WP_DCMNT_TYPE_TBL", 
            "CWF_PROFILE");
        cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde = vw_cwf_Wp_Dcmnt_Type_Tbl.getRecord().newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde", "DCMNT-TYPE-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "DCMNT_TYPE_CDE");
        cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde.setDdmHeader("DOCTYPE");

        cwf_Wp_Dcmnt_Type_Tbl__R_Field_1 = vw_cwf_Wp_Dcmnt_Type_Tbl.getRecord().newGroupInGroup("cwf_Wp_Dcmnt_Type_Tbl__R_Field_1", "REDEFINE", cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde);
        cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde_1_1 = cwf_Wp_Dcmnt_Type_Tbl__R_Field_1.newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde_1_1", "DCMNT-TYPE-CDE-1-1", 
            FieldType.STRING, 1);
        cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde_2_4 = cwf_Wp_Dcmnt_Type_Tbl__R_Field_1.newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde_2_4", "DCMNT-TYPE-CDE-2-4", 
            FieldType.STRING, 3);

        cwf_Wp_Dcmnt_Type_Tbl__R_Field_2 = vw_cwf_Wp_Dcmnt_Type_Tbl.getRecord().newGroupInGroup("cwf_Wp_Dcmnt_Type_Tbl__R_Field_2", "REDEFINE", cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde);
        cwf_Wp_Dcmnt_Type_Tbl_Fill_1 = cwf_Wp_Dcmnt_Type_Tbl__R_Field_2.newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde_2_8 = cwf_Wp_Dcmnt_Type_Tbl__R_Field_2.newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde_2_8", "DCMNT-TYPE-CDE-2-8", 
            FieldType.STRING, 7);
        cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Nme = vw_cwf_Wp_Dcmnt_Type_Tbl.getRecord().newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Nme", "DCMNT-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "DCMNT_NME");
        cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Nme.setDdmHeader("DOCNAME");
        cwf_Wp_Dcmnt_Type_Tbl_Actve_Ind = vw_cwf_Wp_Dcmnt_Type_Tbl.getRecord().newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");

        cwf_Wp_Dcmnt_Type_Tbl__R_Field_3 = vw_cwf_Wp_Dcmnt_Type_Tbl.getRecord().newGroupInGroup("cwf_Wp_Dcmnt_Type_Tbl__R_Field_3", "REDEFINE", cwf_Wp_Dcmnt_Type_Tbl_Actve_Ind);
        cwf_Wp_Dcmnt_Type_Tbl_Fill_2 = cwf_Wp_Dcmnt_Type_Tbl__R_Field_3.newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_Fill_2", "FILL-2", FieldType.STRING, 1);
        cwf_Wp_Dcmnt_Type_Tbl_Actve_Ind_2_2 = cwf_Wp_Dcmnt_Type_Tbl__R_Field_3.newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_Actve_Ind_2_2", "ACTVE-IND-2-2", 
            FieldType.STRING, 1);
        cwf_Wp_Dcmnt_Type_Tbl_Dlte_Oprtr_Cde = vw_cwf_Wp_Dcmnt_Type_Tbl.getRecord().newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Wp_Dcmnt_Type_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        registerRecord(vw_cwf_Wp_Dcmnt_Type_Tbl);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);

        work_Record = localVariables.newGroupInRecord("work_Record", "WORK-RECORD");
        work_Record_Pnd_Work_Record = work_Record.newFieldInGroup("work_Record_Pnd_Work_Record", "#WORK-RECORD", FieldType.STRING, 157);

        work_Record__R_Field_4 = work_Record.newGroupInGroup("work_Record__R_Field_4", "REDEFINE", work_Record_Pnd_Work_Record);
        work_Record_Dcmnt_Type_Cde = work_Record__R_Field_4.newFieldInGroup("work_Record_Dcmnt_Type_Cde", "DCMNT-TYPE-CDE", FieldType.STRING, 8);
        work_Record_Pnd_C1 = work_Record__R_Field_4.newFieldInGroup("work_Record_Pnd_C1", "#C1", FieldType.STRING, 1);
        work_Record_Dcmnt_Nme = work_Record__R_Field_4.newFieldInGroup("work_Record_Dcmnt_Nme", "DCMNT-NME", FieldType.STRING, 45);
        work_Record_Pnd_C2 = work_Record__R_Field_4.newFieldInGroup("work_Record_Pnd_C2", "#C2", FieldType.STRING, 1);
        work_Record_Attrbte_Work_Prcss_Id = work_Record__R_Field_4.newFieldInGroup("work_Record_Attrbte_Work_Prcss_Id", "ATTRBTE-WORK-PRCSS-ID", FieldType.STRING, 
            8);
        work_Record_Pnd_C3 = work_Record__R_Field_4.newFieldInGroup("work_Record_Pnd_C3", "#C3", FieldType.STRING, 1);
        work_Record_Attrbte_Drctn_Cde = work_Record__R_Field_4.newFieldInGroup("work_Record_Attrbte_Drctn_Cde", "ATTRBTE-DRCTN-CDE", FieldType.STRING, 
            1);
        work_Record_Pnd_C4 = work_Record__R_Field_4.newFieldInGroup("work_Record_Pnd_C4", "#C4", FieldType.STRING, 1);
        work_Record_Attrbte_Scrty_Cde = work_Record__R_Field_4.newFieldInGroup("work_Record_Attrbte_Scrty_Cde", "ATTRBTE-SCRTY-CDE", FieldType.STRING, 
            1);
        work_Record_Pnd_C5 = work_Record__R_Field_4.newFieldInGroup("work_Record_Pnd_C5", "#C5", FieldType.STRING, 1);
        work_Record_Attrbte_Rtntn_Cde = work_Record__R_Field_4.newFieldInGroup("work_Record_Attrbte_Rtntn_Cde", "ATTRBTE-RTNTN-CDE", FieldType.STRING, 
            1);
        work_Record_Pnd_C6 = work_Record__R_Field_4.newFieldInGroup("work_Record_Pnd_C6", "#C6", FieldType.STRING, 1);
        work_Record_Form_Nbr = work_Record__R_Field_4.newFieldInGroup("work_Record_Form_Nbr", "FORM-NBR", FieldType.STRING, 20);
        work_Record_Pnd_C7 = work_Record__R_Field_4.newFieldInGroup("work_Record_Pnd_C7", "#C7", FieldType.STRING, 1);
        work_Record_Form_Nme = work_Record__R_Field_4.newFieldInGroup("work_Record_Form_Nme", "FORM-NME", FieldType.STRING, 45);
        work_Record_Pnd_C8 = work_Record__R_Field_4.newFieldInGroup("work_Record_Pnd_C8", "#C8", FieldType.STRING, 1);
        work_Record_Eye_Rdble_Cde = work_Record__R_Field_4.newFieldInGroup("work_Record_Eye_Rdble_Cde", "EYE-RDBLE-CDE", FieldType.STRING, 20);

        vw_cwf_Wp_Form_Nbr_Erc_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Form_Nbr_Erc_Tbl", "CWF-WP-FORM-NBR-ERC-TBL"), "CWF_WP_FORM_NBR_ERC_TBL", 
            "CWF_PROFILE");
        cwf_Wp_Form_Nbr_Erc_Tbl_Form_Dcmnt_Type_Cde = vw_cwf_Wp_Form_Nbr_Erc_Tbl.getRecord().newFieldInGroup("cwf_Wp_Form_Nbr_Erc_Tbl_Form_Dcmnt_Type_Cde", 
            "FORM-DCMNT-TYPE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "FORM_DCMNT_TYPE_CDE");
        cwf_Wp_Form_Nbr_Erc_Tbl_Form_Dcmnt_Type_Cde.setDdmHeader("FORM/DOCTYPE");
        cwf_Wp_Form_Nbr_Erc_Tbl_Form_Nbr = vw_cwf_Wp_Form_Nbr_Erc_Tbl.getRecord().newFieldInGroup("cwf_Wp_Form_Nbr_Erc_Tbl_Form_Nbr", "FORM-NBR", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "FORM_NBR");
        cwf_Wp_Form_Nbr_Erc_Tbl_Form_Nbr.setDdmHeader("FORMNUMBER");
        cwf_Wp_Form_Nbr_Erc_Tbl_Form_Nme = vw_cwf_Wp_Form_Nbr_Erc_Tbl.getRecord().newFieldInGroup("cwf_Wp_Form_Nbr_Erc_Tbl_Form_Nme", "FORM-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "FORM_NME");
        cwf_Wp_Form_Nbr_Erc_Tbl_Form_Nme.setDdmHeader("FORMNAME");
        cwf_Wp_Form_Nbr_Erc_Tbl_Form_Work_Prcss_Id = vw_cwf_Wp_Form_Nbr_Erc_Tbl.getRecord().newFieldInGroup("cwf_Wp_Form_Nbr_Erc_Tbl_Form_Work_Prcss_Id", 
            "FORM-WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, "FORM_WORK_PRCSS_ID");
        cwf_Wp_Form_Nbr_Erc_Tbl_Form_Work_Prcss_Id.setDdmHeader("FORM/WORKPRCSSID");
        cwf_Wp_Form_Nbr_Erc_Tbl_Eye_Rdble_Cde = vw_cwf_Wp_Form_Nbr_Erc_Tbl.getRecord().newFieldInGroup("cwf_Wp_Form_Nbr_Erc_Tbl_Eye_Rdble_Cde", "EYE-RDBLE-CDE", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "EYE_RDBLE_CDE");
        cwf_Wp_Form_Nbr_Erc_Tbl_Eye_Rdble_Cde.setDdmHeader("EYEREADABLE/CODE");
        registerRecord(vw_cwf_Wp_Form_Nbr_Erc_Tbl);

        vw_cwf_Wp_Dcmnt_Attrbte_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Dcmnt_Attrbte_Tbl", "CWF-WP-DCMNT-ATTRBTE-TBL"), "CWF_WP_DCMNT_ATTRBTE_TBL", 
            "CWF_PROFILE");
        cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Dcmnt_Type_Cde = vw_cwf_Wp_Dcmnt_Attrbte_Tbl.getRecord().newFieldInGroup("cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Dcmnt_Type_Cde", 
            "ATTRBTE-DCMNT-TYPE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ATTRBTE_DCMNT_TYPE_CDE");
        cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Dcmnt_Type_Cde.setDdmHeader("DOCUMENT TYPE");
        cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Work_Prcss_Id = vw_cwf_Wp_Dcmnt_Attrbte_Tbl.getRecord().newFieldInGroup("cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Work_Prcss_Id", 
            "ATTRBTE-WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, "ATTRBTE_WORK_PRCSS_ID");
        cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Work_Prcss_Id.setDdmHeader("DOCUMENT/WORKPRCSSID");
        cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Drctn_Cde = vw_cwf_Wp_Dcmnt_Attrbte_Tbl.getRecord().newFieldInGroup("cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Drctn_Cde", 
            "ATTRBTE-DRCTN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ATTRBTE_DRCTN_CDE");
        cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Drctn_Cde.setDdmHeader("DOCUMENT/DIRECTION");
        cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Rtntn_Cde = vw_cwf_Wp_Dcmnt_Attrbte_Tbl.getRecord().newFieldInGroup("cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Rtntn_Cde", 
            "ATTRBTE-RTNTN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ATTRBTE_RTNTN_CDE");
        cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Rtntn_Cde.setDdmHeader("DOCUMENT/RETENTION");
        cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Scrty_Cde = vw_cwf_Wp_Dcmnt_Attrbte_Tbl.getRecord().newFieldInGroup("cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Scrty_Cde", 
            "ATTRBTE-SCRTY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ATTRBTE_SCRTY_CDE");
        cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Scrty_Cde.setDdmHeader("DOCUMENT/SECURITY");
        registerRecord(vw_cwf_Wp_Dcmnt_Attrbte_Tbl);

        counters = localVariables.newGroupInRecord("counters", "COUNTERS");
        counters_Pnd_Doc_Type_Rex_Read = counters.newFieldInGroup("counters_Pnd_Doc_Type_Rex_Read", "#DOC-TYPE-REX-READ", FieldType.PACKED_DECIMAL, 7);
        counters_Pnd_Erc_Rex_Read = counters.newFieldInGroup("counters_Pnd_Erc_Rex_Read", "#ERC-REX-READ", FieldType.PACKED_DECIMAL, 7);
        counters_Pnd_No_Doc_Rex = counters.newFieldInGroup("counters_Pnd_No_Doc_Rex", "#NO-DOC-REX", FieldType.PACKED_DECIMAL, 7);
        counters_Pnd_Attr_Rex_Read = counters.newFieldInGroup("counters_Pnd_Attr_Rex_Read", "#ATTR-REX-READ", FieldType.PACKED_DECIMAL, 7);
        counters_Pnd_No_Attr_Rex = counters.newFieldInGroup("counters_Pnd_No_Attr_Rex", "#NO-ATTR-REX", FieldType.PACKED_DECIMAL, 7);
        counters_Pnd_Rex_Written = counters.newFieldInGroup("counters_Pnd_Rex_Written", "#REX-WRITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Doc_Type = localVariables.newFieldInRecord("pnd_Doc_Type", "#DOC-TYPE", FieldType.STRING, 8);
        pnd_No_Records_Found = localVariables.newFieldInRecord("pnd_No_Records_Found", "#NO-RECORDS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Delimiter = localVariables.newFieldInRecord("pnd_Delimiter", "#DELIMITER", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Wp_Dcmnt_Type_Tbl.reset();
        vw_cwf_Wp_Form_Nbr_Erc_Tbl.reset();
        vw_cwf_Wp_Dcmnt_Attrbte_Tbl.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("     CWF Document Type  Codes Extract Report");
        pnd_Header1_2.setInitialValue("                        �");
        pnd_Delimiter.setInitialValue("@");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4035() throws Exception
    {
        super("Cwfb4035");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        vw_cwf_Wp_Form_Nbr_Erc_Tbl.startDatabaseRead                                                                                                                      //Natural: READ CWF-WP-FORM-NBR-ERC-TBL BY FORM-NBR-DOC-TYPE-KEY
        (
        "READ_PRIME",
        new Oc[] { new Oc("FORM_NBR_DOC_TYPE_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Wp_Form_Nbr_Erc_Tbl.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            //*     REJECT IF  DLTE-OPRTR-CDE GT ' '
            counters_Pnd_Erc_Rex_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #ERC-REX-READ
            work_Record.reset();                                                                                                                                          //Natural: RESET WORK-RECORD
            pnd_Doc_Type.setValue(cwf_Wp_Form_Nbr_Erc_Tbl_Form_Dcmnt_Type_Cde);                                                                                           //Natural: MOVE CWF-WP-FORM-NBR-ERC-TBL.FORM-DCMNT-TYPE-CDE TO #DOC-TYPE
            work_Record.setValuesByName(vw_cwf_Wp_Form_Nbr_Erc_Tbl);                                                                                                      //Natural: MOVE BY NAME CWF-WP-FORM-NBR-ERC-TBL TO WORK-RECORD
                                                                                                                                                                          //Natural: PERFORM READ-DOC-FILE
            sub_Read_Doc_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM READ-ATTR-FILE
            sub_Read_Attr_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *SAG END-EXIT
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        getReports().write(1, ReportOption.NOTITLE,"Total Number of Document Table Records Read    :",counters_Pnd_Doc_Type_Rex_Read,NEWLINE,"Total Number of ERC Table Records Read         :",counters_Pnd_Erc_Rex_Read,NEWLINE,"Total Number of ATTR Table Records Read        :",counters_Pnd_Attr_Rex_Read,NEWLINE,"Total Number of ERC records with no DOC record :",counters_Pnd_Attr_Rex_Read,NEWLINE,"Total Number of ERC records with no ATTr record:",counters_Pnd_Attr_Rex_Read,NEWLINE,"Total Number of Work File Records Written      :",counters_Pnd_Rex_Written,NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) 'Total Number of Document Table Records Read    :' #DOC-TYPE-REX-READ / 'Total Number of ERC Table Records Read         :' #ERC-REX-READ / 'Total Number of ATTR Table Records Read        :' #ATTR-REX-READ / 'Total Number of ERC records with no DOC record :' #ATTR-REX-READ / 'Total Number of ERC records with no ATTr record:' #ATTR-REX-READ / 'Total Number of Work File Records Written      :' #REX-WRITTEN //56T 'End of Report'
            TabSetting(56),"End of Report");
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *****************************
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-DOC-FILE
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-ATTR-FILE
        //* *************
        //* *SAG END-EXIT
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  AMINE FULL #WORK-RECORD FOR ',' REPLACE WITH ' '
        work_Record_Pnd_C1.setValue(pnd_Delimiter);                                                                                                                       //Natural: MOVE #DELIMITER TO #C1 #C2 #C3 #C4 #C5 #C6 #C7 #C8
        work_Record_Pnd_C2.setValue(pnd_Delimiter);
        work_Record_Pnd_C3.setValue(pnd_Delimiter);
        work_Record_Pnd_C4.setValue(pnd_Delimiter);
        work_Record_Pnd_C5.setValue(pnd_Delimiter);
        work_Record_Pnd_C6.setValue(pnd_Delimiter);
        work_Record_Pnd_C7.setValue(pnd_Delimiter);
        work_Record_Pnd_C8.setValue(pnd_Delimiter);
        getWorkFiles().write(1, false, work_Record);                                                                                                                      //Natural: WRITE WORK FILE 1 WORK-RECORD
        counters_Pnd_Rex_Written.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REX-WRITTEN
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        //* ********************************
        //*  WRITE-WORK-FILE
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "Doc/Type",                                                                                                                               //Natural: DISPLAY ( 1 ) 'Doc/Type' WORK-RECORD.DCMNT-TYPE-CDE 'DOC/NAME' WORK-RECORD.DCMNT-NME ( AL = 20 ) '/WPID' WORK-RECORD.ATTRBTE-WORK-PRCSS-ID 'Drctn/Cde' WORK-RECORD.ATTRBTE-DRCTN-CDE 'Scrty/Cde' WORK-RECORD.ATTRBTE-SCRTY-CDE 'Retention/Cde' WORK-RECORD.ATTRBTE-RTNTN-CDE 'Form/Nbr' WORK-RECORD.FORM-NBR 'Form/Name' WORK-RECORD.FORM-NME ( AL = 20 ) 'Eye/Rdbl Cde' WORK-RECORD.EYE-RDBLE-CDE ( AL = 10 )
        		work_Record_Dcmnt_Type_Cde,"DOC/NAME",
        		work_Record_Dcmnt_Nme, new AlphanumericLength (20),"/WPID",
        		work_Record_Attrbte_Work_Prcss_Id,"Drctn/Cde",
        		work_Record_Attrbte_Drctn_Cde,"Scrty/Cde",
        		work_Record_Attrbte_Scrty_Cde,"Retention/Cde",
        		work_Record_Attrbte_Rtntn_Cde,"Form/Nbr",
        		work_Record_Form_Nbr,"Form/Name",
        		work_Record_Form_Nme, new AlphanumericLength (20),"Eye/Rdbl Cde",
        		work_Record_Eye_Rdble_Cde, new AlphanumericLength (10));
        if (Global.isEscape()) return;
        //* *************
    }
    private void sub_Read_Doc_File() throws Exception                                                                                                                     //Natural: READ-DOC-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_No_Records_Found.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NO-RECORDS-FOUND
        vw_cwf_Wp_Dcmnt_Type_Tbl.startDatabaseRead                                                                                                                        //Natural: READ CWF-WP-DCMNT-TYPE-TBL BY DCMNT-TYPE-CDE-KEY STARTING FROM #DOC-TYPE
        (
        "READ_DOC",
        new Wc[] { new Wc("DCMNT_TYPE_CDE_KEY", ">=", pnd_Doc_Type, WcType.BY) },
        new Oc[] { new Oc("DCMNT_TYPE_CDE_KEY", "ASC") }
        );
        READ_DOC:
        while (condition(vw_cwf_Wp_Dcmnt_Type_Tbl.readNextRow("READ_DOC")))
        {
            if (condition(cwf_Wp_Dcmnt_Type_Tbl_Dcmnt_Type_Cde.notEquals(pnd_Doc_Type)))                                                                                  //Natural: IF CWF-WP-DCMNT-TYPE-TBL.DCMNT-TYPE-CDE NE #DOC-TYPE
            {
                if (true) break READ_DOC;                                                                                                                                 //Natural: ESCAPE BOTTOM ( READ-DOC. )
            }                                                                                                                                                             //Natural: END-IF
            work_Record.setValuesByName(vw_cwf_Wp_Dcmnt_Type_Tbl);                                                                                                        //Natural: MOVE BY NAME CWF-WP-DCMNT-TYPE-TBL TO WORK-RECORD
            counters_Pnd_Doc_Type_Rex_Read.nadd(1);                                                                                                                       //Natural: ADD 1 TO #DOC-TYPE-REX-READ
            //*  READ-DOC.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_No_Records_Found.getBoolean()))                                                                                                                 //Natural: IF #NO-RECORDS-FOUND
        {
            counters_Pnd_No_Doc_Rex.nadd(1);                                                                                                                              //Natural: ADD 1 TO #NO-DOC-REX
            getReports().write(2, "No record on DOCUMENT table for form nbr:",pnd_Doc_Type);                                                                              //Natural: WRITE ( 2 ) 'No record on DOCUMENT table for form nbr:' #DOC-TYPE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  READ-ERC-FILE
    }
    private void sub_Read_Attr_File() throws Exception                                                                                                                    //Natural: READ-ATTR-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_No_Records_Found.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NO-RECORDS-FOUND
        //*  READ CWF-WP-DCMNT-ATTRBTE-TBL
        vw_cwf_Wp_Dcmnt_Attrbte_Tbl.startDatabaseRead                                                                                                                     //Natural: READ CWF-WP-DCMNT-ATTRBTE-TBL BY ATTRBTE-DOC-WPID-KEY STARTING FROM #DOC-TYPE
        (
        "READ_ATTRIB",
        new Wc[] { new Wc("ATTRBTE_DOC_WPID_KEY", ">=", pnd_Doc_Type, WcType.BY) },
        new Oc[] { new Oc("ATTRBTE_DOC_WPID_KEY", "ASC") }
        );
        READ_ATTRIB:
        while (condition(vw_cwf_Wp_Dcmnt_Attrbte_Tbl.readNextRow("READ_ATTRIB")))
        {
            if (condition(cwf_Wp_Dcmnt_Attrbte_Tbl_Attrbte_Dcmnt_Type_Cde.notEquals(pnd_Doc_Type)))                                                                       //Natural: IF CWF-WP-DCMNT-ATTRBTE-TBL.ATTRBTE-DCMNT-TYPE-CDE NE #DOC-TYPE
            {
                if (true) break READ_ATTRIB;                                                                                                                              //Natural: ESCAPE BOTTOM ( READ-ATTRIB. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            work_Record.setValuesByName(vw_cwf_Wp_Dcmnt_Attrbte_Tbl);                                                                                                     //Natural: MOVE BY NAME CWF-WP-DCMNT-ATTRBTE-TBL TO WORK-RECORD
            counters_Pnd_Attr_Rex_Read.nadd(1);                                                                                                                           //Natural: ADD 1 TO #ATTR-REX-READ
            //*  READ-ATTRIB.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_No_Records_Found.getBoolean()))                                                                                                                 //Natural: IF #NO-RECORDS-FOUND
        {
            counters_Pnd_No_Attr_Rex.nadd(1);                                                                                                                             //Natural: ADD 1 TO #NO-ATTR-REX
            getReports().write(2, "No records on ATTR Table for form number:",pnd_Doc_Type);                                                                              //Natural: WRITE ( 2 ) 'No records on ATTR Table for form number:' #DOC-TYPE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  READ-ATTRIBUTE-TABLE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),pnd_Header1_2,new 
                        TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "Doc/Type",
        		work_Record_Dcmnt_Type_Cde,"DOC/NAME",
        		work_Record_Dcmnt_Nme, new AlphanumericLength (20),"/WPID",
        		work_Record_Attrbte_Work_Prcss_Id,"Drctn/Cde",
        		work_Record_Attrbte_Drctn_Cde,"Scrty/Cde",
        		work_Record_Attrbte_Scrty_Cde,"Retention/Cde",
        		work_Record_Attrbte_Rtntn_Cde,"Form/Nbr",
        		work_Record_Form_Nbr,"Form/Name",
        		work_Record_Form_Nme, new AlphanumericLength (20),"Eye/Rdbl Cde",
        		work_Record_Eye_Rdble_Cde, new AlphanumericLength (10));
    }
}
