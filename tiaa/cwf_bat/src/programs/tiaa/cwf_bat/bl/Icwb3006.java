/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:39:07 PM
**        * FROM NATURAL PROGRAM : Icwb3006
************************************************************
**        * FILE NAME            : Icwb3006.java
**        * CLASS NAME           : Icwb3006
**        * INSTANCE NAME        : Icwb3006
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 6
**SAG SYSTEM: CRPICW
************************************************************************
* PROGRAM  : ICWB3006
* SYSTEM   : CRPICW
* TITLE    : INSTITUTIONAL CORPORATE WORKFLOW
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF OPEN CASES
*
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
* ---------
* 09/26/96 JHH - CLONED PROGRAM FROM CWFB3006 .. AND MODIFIED
* 10/24/96 JHH - EXPANDED DAYS BUCKETS TO N18
* 11/15/96 JHH - USE ADMIN-STATUS-UPDTE-DTE-TME FOR REPORT FIELD
* JULY 97  JSP - USE ADMIN UNIT CODE INSTEAD OF LAST CHNGE UNIT CDE
* JULY 97  JSP - USE STATUS CDE FOR REPORT
* JULY 97  JSP - PEND STATUS = 4000 TO 4997
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwb3006 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Tbl_Calendar_Days;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Bsnss_Days;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Bsnss_Days14;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Bsnss_Days4;
    private DbsField pnd_Work_Record_Tbl_Wpid_Act;
    private DbsField pnd_Work_Record_Tbl_Status_Key;

    private DbsGroup pnd_Work_Record__R_Field_2;
    private DbsField pnd_Work_Record_Tbl_Last_Chnge_Unit_Cde;
    private DbsField pnd_Work_Record_Tbl_Status_Cde;

    private DbsGroup pnd_Report_Data;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Cde;

    private DbsGroup pnd_Misc_Parm;
    private DbsField pnd_Misc_Parm_Pnd_Save_Action;
    private DbsField pnd_Misc_Parm_Pnd_Wpid;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Action;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Count;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Text;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count;
    private DbsField pnd_Misc_Parm_Pnd_Sub_Count;
    private DbsField pnd_Misc_Parm_Pnd_Booklet_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Forms_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Inquire_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Research_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Trans_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Complaint_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Other_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Read_Count;
    private DbsField pnd_Misc_Parm_Pnd_Status_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Return_Rcvd_Txt;
    private DbsField pnd_Page;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Env;
    private DbsField pnd_All_Work_Rqst_Key;
    private DbsField pnd_Date_D;
    private DbsField pnd_Date_Msg;
    private DbsField pnd_Pend_Ind;
    private DbsField pnd_Pend_Msg;
    private DbsField pnd_Receive_Dte;
    private DbsField pnd_Report_Dte;
    private DbsField pnd_Non_Work_Days;

    private DataAccessProgramView vw_icw_Master_Index;
    private DbsField icw_Master_Index_Rqst_Log_Dte_Tme;
    private DbsField icw_Master_Index_Modify_Unit_Cde;
    private DbsField icw_Master_Index_Ppg_Cde;
    private DbsField icw_Master_Index_Tiaa_Rcvd_Dte_Tme;
    private DbsField icw_Master_Index_Work_Prcss_Id;
    private DbsField icw_Master_Index_Unit_Cde;
    private DbsField icw_Master_Index_Unit_Updte_Dte_Tme;
    private DbsField icw_Master_Index_Empl_Racf_Id;
    private DbsField icw_Master_Index_Admin_Unit_Cde;
    private DbsField icw_Master_Index_Admin_Status_Cde;
    private DbsField icw_Master_Index_Admin_Status_Updte_Dte_Tme;
    private DbsField icw_Master_Index_Status_Cde;
    private DbsField icw_Master_Index_Step_Id;
    private DbsField icw_Master_Index_List_Data_Rcvd_Dte;
    private DbsField icw_Master_Index_Prtcpnt_Dte;
    private DbsField icw_Master_Index_Schdle_Pay_Dte;
    private DbsField icw_Master_Index_Check_Ind;
    private DbsField icw_Master_Index_Actve_Ind;
    private DbsField pnd_Tbl_Run_Flag;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Work_Prcss_IdOld;
    private DbsField sort01Tbl_Wpid_ActOld;
    private DbsField sort01Work_Prcss_IdCount243;
    private DbsField sort01Work_Prcss_IdCount;
    private DbsField sort01Admin_Unit_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Tbl_Calendar_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 
            18);
        pnd_Work_Record_Tbl_Tiaa_Bsnss_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 
            18);

        pnd_Work_Record__R_Field_1 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record_Tbl_Tiaa_Bsnss_Days);
        pnd_Work_Record_Tbl_Tiaa_Bsnss_Days14 = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Bsnss_Days14", "TBL-TIAA-BSNSS-DAYS14", 
            FieldType.NUMERIC, 14);
        pnd_Work_Record_Tbl_Tiaa_Bsnss_Days4 = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Bsnss_Days4", "TBL-TIAA-BSNSS-DAYS4", 
            FieldType.NUMERIC, 4);
        pnd_Work_Record_Tbl_Wpid_Act = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 1);
        pnd_Work_Record_Tbl_Status_Key = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Key", "TBL-STATUS-KEY", FieldType.STRING, 13);

        pnd_Work_Record__R_Field_2 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_2", "REDEFINE", pnd_Work_Record_Tbl_Status_Key);
        pnd_Work_Record_Tbl_Last_Chnge_Unit_Cde = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chnge_Unit_Cde", "TBL-LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Status_Cde = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde", "TBL-STATUS-CDE", FieldType.STRING, 
            4);

        pnd_Report_Data = localVariables.newGroupInRecord("pnd_Report_Data", "#REPORT-DATA");
        pnd_Report_Data_Pnd_Rep_Unit_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Name", "#REP-UNIT-NAME", FieldType.STRING, 45);
        pnd_Report_Data_Pnd_Rep_Unit_Cde = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);

        pnd_Misc_Parm = localVariables.newGroupInRecord("pnd_Misc_Parm", "#MISC-PARM");
        pnd_Misc_Parm_Pnd_Save_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Save_Action", "#SAVE-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Wpid = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Misc_Parm_Pnd_Wpid_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Action", "#WPID-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Wpid_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Count", "#WPID-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Wpid_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Sname", "#WPID-SNAME", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Wpid_Text = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Text", "#WPID-TEXT", FieldType.STRING, 56);
        pnd_Misc_Parm_Pnd_Total_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count", "#TOTAL-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Sub_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Sub_Count", "#SUB-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Booklet_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Booklet_Ctr", "#BOOKLET-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Forms_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Forms_Ctr", "#FORMS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Inquire_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Inquire_Ctr", "#INQUIRE-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Research_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Research_Ctr", "#RESEARCH-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Trans_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Trans_Ctr", "#TRANS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Complaint_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Complaint_Ctr", "#COMPLAINT-CTR", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Misc_Parm_Pnd_Other_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Other_Ctr", "#OTHER-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Read_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Read_Count", "#READ-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Status_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Status_Sname", "#STATUS-SNAME", FieldType.STRING, 25);
        pnd_Misc_Parm_Pnd_Return_Rcvd_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Rcvd_Txt", "#RETURN-RCVD-TXT", FieldType.STRING, 8);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 5);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_All_Work_Rqst_Key = localVariables.newFieldInRecord("pnd_All_Work_Rqst_Key", "#ALL-WORK-RQST-KEY", FieldType.STRING, 39);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Date_Msg = localVariables.newFieldInRecord("pnd_Date_Msg", "#DATE-MSG", FieldType.STRING, 8);
        pnd_Pend_Ind = localVariables.newFieldInRecord("pnd_Pend_Ind", "#PEND-IND", FieldType.STRING, 1);
        pnd_Pend_Msg = localVariables.newFieldInRecord("pnd_Pend_Msg", "#PEND-MSG", FieldType.STRING, 12);
        pnd_Receive_Dte = localVariables.newFieldInRecord("pnd_Receive_Dte", "#RECEIVE-DTE", FieldType.DATE);
        pnd_Report_Dte = localVariables.newFieldInRecord("pnd_Report_Dte", "#REPORT-DTE", FieldType.DATE);
        pnd_Non_Work_Days = localVariables.newFieldInRecord("pnd_Non_Work_Days", "#NON-WORK-DAYS", FieldType.NUMERIC, 18);

        vw_icw_Master_Index = new DataAccessProgramView(new NameInfo("vw_icw_Master_Index", "ICW-MASTER-INDEX"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        icw_Master_Index_Rqst_Log_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        icw_Master_Index_Modify_Unit_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Modify_Unit_Cde", "MODIFY-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "MODIFY_UNIT_CDE");
        icw_Master_Index_Ppg_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PPG_CDE");
        icw_Master_Index_Tiaa_Rcvd_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        icw_Master_Index_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        icw_Master_Index_Work_Prcss_Id = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        icw_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        icw_Master_Index_Unit_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        icw_Master_Index_Unit_Cde.setDdmHeader("UNIT/CODE");
        icw_Master_Index_Unit_Updte_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        icw_Master_Index_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        icw_Master_Index_Empl_Racf_Id = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        icw_Master_Index_Admin_Unit_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        icw_Master_Index_Admin_Status_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        icw_Master_Index_Admin_Status_Updte_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        icw_Master_Index_Status_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "STATUS_CDE");
        icw_Master_Index_Step_Id = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        icw_Master_Index_Step_Id.setDdmHeader("STEP/ID");
        icw_Master_Index_List_Data_Rcvd_Dte = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_List_Data_Rcvd_Dte", "LIST-DATA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "LIST_DATA_RCVD_DTE");
        icw_Master_Index_Prtcpnt_Dte = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Prtcpnt_Dte", "PRTCPNT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PRTCPNT_DTE");
        icw_Master_Index_Schdle_Pay_Dte = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Schdle_Pay_Dte", "SCHDLE-PAY-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "SCHDLE_PAY_DTE");
        icw_Master_Index_Check_Ind = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Check_Ind", "CHECK-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHECK_IND");
        icw_Master_Index_Check_Ind.setDdmHeader("CHK");
        icw_Master_Index_Actve_Ind = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        registerRecord(vw_icw_Master_Index);

        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Work_Prcss_IdOld = internalLoopRecord.newFieldInRecord("Sort01_Work_Prcss_Id_OLD", "Work_Prcss_Id_OLD", FieldType.STRING, 6);
        sort01Tbl_Wpid_ActOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_Act_OLD", "Tbl_Wpid_Act_OLD", FieldType.STRING, 1);
        sort01Work_Prcss_IdCount243 = internalLoopRecord.newFieldInRecord("Sort01_Work_Prcss_Id_COUNT_243", "Work_Prcss_Id_COUNT_243", FieldType.NUMERIC, 
            9);
        sort01Work_Prcss_IdCount = internalLoopRecord.newFieldInRecord("Sort01_Work_Prcss_Id_COUNT", "Work_Prcss_Id_COUNT", FieldType.NUMERIC, 9);
        sort01Admin_Unit_CdeOld = internalLoopRecord.newFieldInRecord("Sort01_Admin_Unit_Cde_OLD", "Admin_Unit_Cde_OLD", FieldType.STRING, 8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_icw_Master_Index.reset();
        internalLoopRecord.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Icwb3006() throws Exception
    {
        super("Icwb3006");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("ICWB3006", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  ===================================================
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: FORMAT ( 1 ) LS = 142 PS = 58;//Natural: MOVE *LIBRARY-ID TO #ENV
        pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                             //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        vw_icw_Master_Index.startDatabaseRead                                                                                                                             //Natural: READ ICW-MASTER-INDEX BY ALL-WORK-RQST-KEY STARTING FROM #ALL-WORK-RQST-KEY
        (
        "READ_MASTER",
        new Wc[] { new Wc("ALL_WORK_RQST_KEY", ">=", pnd_All_Work_Rqst_Key, WcType.BY) },
        new Oc[] { new Oc("ALL_WORK_RQST_KEY", "ASC") }
        );
        READ_MASTER:
        while (condition(vw_icw_Master_Index.readNextRow("READ_MASTER")))
        {
            if (condition(icw_Master_Index_Admin_Unit_Cde.equals("ICW") || icw_Master_Index_Actve_Ind.equals(" ")))                                                       //Natural: REJECT IF ICW-MASTER-INDEX.ADMIN-UNIT-CDE = 'ICW' OR ICW-MASTER-INDEX.ACTVE-IND = ' '
            {
                continue;
            }
            //*  IP
            short decideConditionsMet117 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ICW-MASTER-INDEX.STATUS-CDE = '4000' THRU '4997'
            if (condition(icw_Master_Index_Status_Cde.greaterOrEqual("4000") && icw_Master_Index_Status_Cde.lessOrEqual("4997")))
            {
                decideConditionsMet117++;
                pnd_Pend_Ind.setValue("2");                                                                                                                               //Natural: MOVE '2' TO #PEND-IND
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                //*  NON PEND
                pnd_Pend_Ind.setValue("1");                                                                                                                               //Natural: MOVE '1' TO #PEND-IND
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(DbsUtil.maskMatches(icw_Master_Index_Work_Prcss_Id,"NN....")))                                                                                  //Natural: IF ICW-MASTER-INDEX.WORK-PRCSS-ID = MASK ( NN.... )
            {
                pnd_Work_Record_Tbl_Wpid_Act.setValue("Z");                                                                                                               //Natural: MOVE 'Z' TO #WORK-RECORD.TBL-WPID-ACT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Record_Tbl_Wpid_Act.setValue(icw_Master_Index_Work_Prcss_Id);                                                                                    //Natural: MOVE WORK-PRCSS-ID TO #WORK-RECORD.TBL-WPID-ACT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Record_Tbl_Last_Chnge_Unit_Cde.setValue(icw_Master_Index_Unit_Cde);                                                                                  //Natural: MOVE ICW-MASTER-INDEX.UNIT-CDE TO #WORK-RECORD.TBL-LAST-CHNGE-UNIT-CDE
            pnd_Work_Record_Tbl_Status_Cde.setValue(icw_Master_Index_Status_Cde);                                                                                         //Natural: MOVE ICW-MASTER-INDEX.STATUS-CDE TO #WORK-RECORD.TBL-STATUS-CDE
            pnd_Misc_Parm_Pnd_Read_Count.nadd(1);                                                                                                                         //Natural: ADD 1 TO #READ-COUNT
            //*  ---------------------------------------------
            if (condition(pnd_Misc_Parm_Pnd_Read_Count.equals(getZero())))                                                                                                //Natural: IF #READ-COUNT = 0
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_MASTER"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_MASTER"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            setControl("WB");                                                                                                                                             //Natural: SET CONTROL 'WB'
            pnd_New_Unit.setValue(true);                                                                                                                                  //Natural: FORMAT ( 1 ) LS = 142 PS = 60;//Natural: MOVE TRUE TO #NEW-UNIT
            pnd_Page.setValue(0);                                                                                                                                         //Natural: MOVE 0 TO #PAGE
            getSort().writeSortInData(pnd_Pend_Ind, icw_Master_Index_Admin_Unit_Cde, pnd_Work_Record_Tbl_Wpid_Act, icw_Master_Index_Work_Prcss_Id, icw_Master_Index_List_Data_Rcvd_Dte,  //Natural: END-ALL
                icw_Master_Index_Tiaa_Rcvd_Dte_Tme, icw_Master_Index_Status_Cde, icw_Master_Index_Empl_Racf_Id, icw_Master_Index_Step_Id, pnd_Work_Record_Tbl_Status_Key, 
                icw_Master_Index_Admin_Status_Updte_Dte_Tme, icw_Master_Index_Unit_Updte_Dte_Tme, icw_Master_Index_Prtcpnt_Dte, icw_Master_Index_Schdle_Pay_Dte, 
                icw_Master_Index_Check_Ind, icw_Master_Index_Ppg_Cde);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ---------------------------------------------
        getSort().sortData(pnd_Pend_Ind, icw_Master_Index_Admin_Unit_Cde, pnd_Work_Record_Tbl_Wpid_Act, icw_Master_Index_Work_Prcss_Id, icw_Master_Index_List_Data_Rcvd_Dte,  //Natural: SORT BY #PEND-IND ADMIN-UNIT-CDE #WORK-RECORD.TBL-WPID-ACT WORK-PRCSS-ID LIST-DATA-RCVD-DTE TIAA-RCVD-DTE-TME STATUS-CDE EMPL-RACF-ID USING STEP-ID #WORK-RECORD.TBL-STATUS-KEY ADMIN-STATUS-UPDTE-DTE-TME UNIT-UPDTE-DTE-TME PRTCPNT-DTE SCHDLE-PAY-DTE CHECK-IND PPG-CDE
            icw_Master_Index_Tiaa_Rcvd_Dte_Tme, icw_Master_Index_Status_Cde, icw_Master_Index_Empl_Racf_Id);
        sort01Work_Prcss_IdCount243.setDec(new DbsDecimal(0));
        sort01Work_Prcss_IdCount.setDec(new DbsDecimal(0));
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Pend_Ind, icw_Master_Index_Admin_Unit_Cde, pnd_Work_Record_Tbl_Wpid_Act, icw_Master_Index_Work_Prcss_Id, 
            icw_Master_Index_List_Data_Rcvd_Dte, icw_Master_Index_Tiaa_Rcvd_Dte_Tme, icw_Master_Index_Status_Cde, icw_Master_Index_Empl_Racf_Id, icw_Master_Index_Step_Id, 
            pnd_Work_Record_Tbl_Status_Key, icw_Master_Index_Admin_Status_Updte_Dte_Tme, icw_Master_Index_Unit_Updte_Dte_Tme, icw_Master_Index_Prtcpnt_Dte, 
            icw_Master_Index_Schdle_Pay_Dte, icw_Master_Index_Check_Ind, icw_Master_Index_Ppg_Cde)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            sort01Work_Prcss_IdCount243.setInt(sort01Work_Prcss_IdCount243.getInt() + 1);
            sort01Work_Prcss_IdCount.setInt(sort01Work_Prcss_IdCount.getInt() + 1);
            pnd_Report_Data_Pnd_Rep_Unit_Cde.setValue(icw_Master_Index_Admin_Unit_Cde);                                                                                   //Natural: MOVE ADMIN-UNIT-CDE TO #REP-UNIT-CDE
            if (condition(pnd_New_Unit.getBoolean()))                                                                                                                     //Natural: IF #NEW-UNIT
            {
                DbsUtil.callnat(Icwn1103.class , getCurrentProcessState(), icw_Master_Index_Admin_Unit_Cde, pnd_Report_Data_Pnd_Rep_Unit_Name);                           //Natural: CALLNAT 'ICWN1103' ADMIN-UNIT-CDE #REP-UNIT-NAME
                if (condition(Global.isEscape())) return;
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
            }                                                                                                                                                             //Natural: END-IF
            //*  ----------------------------
            //*  ----------------------------                                                                                                                             //Natural: AT TOP OF PAGE ( 1 )
            short decideConditionsMet169 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
            if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("B"))))
            {
                decideConditionsMet169++;
                pnd_Misc_Parm_Pnd_Booklet_Ctr.nadd(1);                                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("F"))))
            {
                decideConditionsMet169++;
                pnd_Misc_Parm_Pnd_Forms_Ctr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #FORMS-CTR
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("I"))))
            {
                decideConditionsMet169++;
                pnd_Misc_Parm_Pnd_Inquire_Ctr.nadd(1);                                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("R"))))
            {
                decideConditionsMet169++;
                pnd_Misc_Parm_Pnd_Research_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("T"))))
            {
                decideConditionsMet169++;
                pnd_Misc_Parm_Pnd_Trans_Ctr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TRANS-CTR
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("X"))))
            {
                decideConditionsMet169++;
                pnd_Misc_Parm_Pnd_Complaint_Ctr.nadd(1);                                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR
            }                                                                                                                                                             //Natural: VALUES '0':'9'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("0' : '9"))))
            {
                decideConditionsMet169++;
                pnd_Misc_Parm_Pnd_Other_Ctr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #OTHER-CTR
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet169 > 0))
            {
                pnd_Misc_Parm_Pnd_Sub_Count.nadd(1);                                                                                                                      //Natural: ADD 1 TO #SUB-COUNT
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  ----------------------------
            DbsUtil.callnat(Icwn5390.class , getCurrentProcessState(), icw_Master_Index_Work_Prcss_Id, pnd_Misc_Parm_Pnd_Wpid_Sname);                                     //Natural: CALLNAT 'ICWN5390' WORK-PRCSS-ID #WPID-SNAME
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Icwn1102.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Status_Key, pnd_Misc_Parm_Pnd_Status_Sname);                                   //Natural: CALLNAT 'ICWN1102' #WORK-RECORD.TBL-STATUS-KEY #STATUS-SNAME
            if (condition(Global.isEscape())) return;
            if (condition(icw_Master_Index_Unit_Updte_Dte_Tme.greater(getZero())))                                                                                        //Natural: IF UNIT-UPDTE-DTE-TME GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValueEdited(icw_Master_Index_Unit_Updte_Dte_Tme,new ReportEditMask("MM/DD/YY"));                                     //Natural: MOVE EDITED UNIT-UPDTE-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValue(" ");                                                                                                          //Natural: MOVE ' ' TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(icw_Master_Index_Schdle_Pay_Dte,"YYYYMMDD")))                                                                               //Natural: IF SCHDLE-PAY-DTE = MASK ( YYYYMMDD )
            {
                pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),icw_Master_Index_Schdle_Pay_Dte);                                                                //Natural: MOVE EDITED SCHDLE-PAY-DTE TO #DATE-D ( EM = YYYYMMDD )
                pnd_Date_Msg.setValueEdited(pnd_Date_D,new ReportEditMask("MM/DD/YY"));                                                                                   //Natural: MOVE EDITED #DATE-D ( EM = MM/DD/YY ) TO #DATE-MSG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Date_Msg.reset();                                                                                                                                     //Natural: RESET #DATE-MSG
            }                                                                                                                                                             //Natural: END-IF
            if (condition(icw_Master_Index_List_Data_Rcvd_Dte.greater(getZero())))                                                                                        //Natural: IF LIST-DATA-RCVD-DTE > 0
            {
                pnd_Receive_Dte.setValue(icw_Master_Index_List_Data_Rcvd_Dte);                                                                                            //Natural: MOVE LIST-DATA-RCVD-DTE TO #RECEIVE-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Receive_Dte.setValue(icw_Master_Index_Tiaa_Rcvd_Dte_Tme);                                                                                             //Natural: MOVE TIAA-RCVD-DTE-TME TO #RECEIVE-DTE
            }                                                                                                                                                             //Natural: END-IF
            //*  10/25/96 JHH
            if (condition(pnd_Receive_Dte.greater(getZero())))                                                                                                            //Natural: IF #RECEIVE-DTE > 0
            {
                pnd_Report_Dte.setValue(Global.getDATX());                                                                                                                //Natural: MOVE *DATX TO #REPORT-DTE
                pnd_Work_Record_Tbl_Calendar_Days.compute(new ComputeParameters(false, pnd_Work_Record_Tbl_Calendar_Days), pnd_Report_Dte.subtract(pnd_Receive_Dte));     //Natural: COMPUTE TBL-CALENDAR-DAYS = #REPORT-DTE - #RECEIVE-DTE
                DbsUtil.callnat(Cwfn3901.class , getCurrentProcessState(), pnd_Receive_Dte, pnd_Report_Dte, pnd_Non_Work_Days);                                           //Natural: CALLNAT 'CWFN3901' #RECEIVE-DTE #REPORT-DTE #NON-WORK-DAYS
                if (condition(Global.isEscape())) return;
                pnd_Work_Record_Tbl_Tiaa_Bsnss_Days.compute(new ComputeParameters(false, pnd_Work_Record_Tbl_Tiaa_Bsnss_Days), pnd_Work_Record_Tbl_Calendar_Days.subtract(pnd_Non_Work_Days)); //Natural: COMPUTE TBL-TIAA-BSNSS-DAYS = TBL-CALENDAR-DAYS - #NON-WORK-DAYS
            }                                                                                                                                                             //Natural: END-IF
            //*  -----------------------------------------------------
            if (condition(pnd_Misc_Parm_Pnd_Save_Action.notEquals(pnd_Work_Record_Tbl_Wpid_Act)))                                                                         //Natural: IF #SAVE-ACTION NE #WORK-RECORD.TBL-WPID-ACT
            {
                pnd_Misc_Parm_Pnd_Save_Action.setValue(pnd_Work_Record_Tbl_Wpid_Act);                                                                                     //Natural: MOVE #WORK-RECORD.TBL-WPID-ACT TO #SAVE-ACTION
                short decideConditionsMet221 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #SAVE-ACTION;//Natural: VALUE 'B'
                if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("B"))))
                {
                    decideConditionsMet221++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"BOOKLETS    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'BOOKLETS    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("F"))))
                {
                    decideConditionsMet221++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"FORMS       :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'FORMS       :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("I"))))
                {
                    decideConditionsMet221++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"INQUIRY     :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'INQUIRY     :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("R"))))
                {
                    decideConditionsMet221++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"RESEARCH    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'RESEARCH    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("T"))))
                {
                    decideConditionsMet221++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TRANSACTIONS:",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'TRANSACTIONS:' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("X"))))
                {
                    decideConditionsMet221++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"COMPLAINTS  :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'COMPLAINTS  :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("Z"))))
                {
                    decideConditionsMet221++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"OTHERS      :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'OTHERS      :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "//RECEIVED/AT TIAA",                                                                                                                 //Natural: DISPLAY ( 1 ) '//RECEIVED/AT TIAA' TIAA-RCVD-DTE-TME ( EM = MM/DD/YY ) '//LIST/RECEIVED' LIST-DATA-RCVD-DTE ( EM = MM/DD/YY ) '//RECEIVED/IN UNIT' #RETURN-RCVD-TXT '//PART./DATE' PRTCPNT-DTE ( EM = MM/DD/YY ) '//WORK/PROCESS' #WPID-SNAME ( IS = ON AL = 12 ) '//WORK/STEP' STEP-ID '///STATUS' #STATUS-SNAME ( AL = 20 ) '//STATUS/DATE' ADMIN-STATUS-UPDTE-DTE-TME ( EM = MM/DD/YY ) 'BUS/DAYS/IN/TIAA' #WORK-RECORD.TBL-TIAA-BSNSS-DAYS4 ( EM = ZZZ9 ) '//PPG/CDE' PPG-CDE '/SCHEDLD/PAY/DATE' #DATE-MSG '//CHK/IND' CHECK-IND '///EMPLOYEE' EMPL-RACF-ID
            		icw_Master_Index_Tiaa_Rcvd_Dte_Tme, new ReportEditMask ("MM/DD/YY"),"//LIST/RECEIVED",
            		icw_Master_Index_List_Data_Rcvd_Dte, new ReportEditMask ("MM/DD/YY"),"//RECEIVED/IN UNIT",
            		pnd_Misc_Parm_Pnd_Return_Rcvd_Txt,"//PART./DATE",
            		icw_Master_Index_Prtcpnt_Dte, new ReportEditMask ("MM/DD/YY"),"//WORK/PROCESS",
            		pnd_Misc_Parm_Pnd_Wpid_Sname, new IdenticalSuppress(true), new AlphanumericLength (12),"//WORK/STEP",
            		icw_Master_Index_Step_Id,"///STATUS",
            		pnd_Misc_Parm_Pnd_Status_Sname, new AlphanumericLength (20),"//STATUS/DATE",
            		icw_Master_Index_Admin_Status_Updte_Dte_Tme, new ReportEditMask ("MM/DD/YY"),"BUS/DAYS/IN/TIAA",
            		pnd_Work_Record_Tbl_Tiaa_Bsnss_Days4, new ReportEditMask ("ZZZ9"),"//PPG/CDE",
            		icw_Master_Index_Ppg_Cde,"/SCHEDLD/PAY/DATE",
            		pnd_Date_Msg,"//CHK/IND",
            		icw_Master_Index_Check_Ind,"///EMPLOYEE",
            		icw_Master_Index_Empl_Racf_Id);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ------------------------------------------------------
            //*  ---------------------------------------------                                                                                                            //Natural: AT BREAK OF WORK-PRCSS-ID
            //*  ---------------------------------------------                                                                                                            //Natural: AT BREAK OF #WORK-RECORD.TBL-WPID-ACT
            //*  ---------------------------------------------                                                                                                            //Natural: AT BREAK OF ADMIN-UNIT-CDE
            sort01Work_Prcss_IdOld.setValue(icw_Master_Index_Work_Prcss_Id);                                                                                              //Natural: END-SORT
            sort01Tbl_Wpid_ActOld.setValue(pnd_Work_Record_Tbl_Wpid_Act);
            sort01Admin_Unit_CdeOld.setValue(icw_Master_Index_Admin_Unit_Cde);
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            sort01Work_Prcss_IdCount243.resetBreak();
            sort01Work_Prcss_IdCount.resetBreak();
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page.nadd(1);                                                                                                                                     //Natural: COMPUTE #PAGE = #PAGE + 1
                    short decideConditionsMet157 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #PEND-IND;//Natural: VALUE '1'
                    if (condition((pnd_Pend_Ind.equals("1"))))
                    {
                        decideConditionsMet157++;
                        pnd_Pend_Msg.setValue("(Not Pended)");                                                                                                            //Natural: MOVE '(Not Pended)' TO #PEND-MSG
                    }                                                                                                                                                     //Natural: VALUE '2'
                    else if (condition((pnd_Pend_Ind.equals("2"))))
                    {
                        decideConditionsMet157++;
                        pnd_Pend_Msg.setValue("(  Pended  )");                                                                                                            //Natural: MOVE '(  Pended  )' TO #PEND-MSG
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    getReports().write(1, ReportOption.NOTITLE,"ICWB3006",pnd_Env,new TabSetting(51),"INSTITUTIONAL CORPORATE WORKFLOW",new TabSetting(126),"PAGE",pnd_Page,  //Natural: WRITE ( 1 ) NOTITLE 'ICWB3006' #ENV 51T 'INSTITUTIONAL CORPORATE WORKFLOW' 126T 'PAGE' #PAGE ( NL = 5 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 57T 'Report of Open Cases' 124T *TIMX ( EM = HH':'II' 'AP ) / 61T #PEND-MSG / 'UNIT:' #REP-UNIT-CDE #REP-UNIT-NAME
                        new NumericLength (5), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
                        TabSetting(57),"Report of Open Cases",new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(61),
                        pnd_Pend_Msg,NEWLINE,"UNIT:",pnd_Report_Data_Pnd_Rep_Unit_Cde,pnd_Report_Data_Pnd_Rep_Unit_Name);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Tbl_Run_Flag.setValue(" ");                                                                                                                                   //Natural: MOVE ' ' TO #TBL-RUN-FLAG
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean icw_Master_Index_Work_Prcss_IdIsBreak = icw_Master_Index_Work_Prcss_Id.isBreak(endOfData);
        boolean pnd_Work_Record_Tbl_Wpid_ActIsBreak = pnd_Work_Record_Tbl_Wpid_Act.isBreak(endOfData);
        boolean icw_Master_Index_Admin_Unit_CdeIsBreak = icw_Master_Index_Admin_Unit_Cde.isBreak(endOfData);
        if (condition(icw_Master_Index_Work_Prcss_IdIsBreak || pnd_Work_Record_Tbl_Wpid_ActIsBreak || icw_Master_Index_Admin_Unit_CdeIsBreak))
        {
            pnd_Misc_Parm_Pnd_Wpid.setValue(sort01Work_Prcss_IdOld);                                                                                                      //Natural: MOVE OLD ( WORK-PRCSS-ID ) TO #WPID
            pnd_Misc_Parm_Pnd_Wpid_Action.setValue(sort01Tbl_Wpid_ActOld);                                                                                                //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID-ACT ) TO #WPID-ACTION
            pnd_Misc_Parm_Pnd_Wpid_Count.setValue(sort01Work_Prcss_IdCount243);                                                                                           //Natural: MOVE COUNT ( WORK-PRCSS-ID ) TO #WPID-COUNT
            short decideConditionsMet247 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
            if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("B"))))
            {
                decideConditionsMet247++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR BOOKLETS FOR WPID....:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR BOOKLETS FOR WPID....:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("F"))))
            {
                decideConditionsMet247++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR FORMS FOR WPID.......:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR FORMS FOR WPID.......:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("I"))))
            {
                decideConditionsMet247++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR INQUIRY FOR WPID.....:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR INQUIRY FOR WPID.....:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("R"))))
            {
                decideConditionsMet247++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR RESEARCH FOR WPID....:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR RESEARCH FOR WPID....:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("T"))))
            {
                decideConditionsMet247++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR TRANSACTIONS FOR WPID:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR TRANSACTIONS FOR WPID:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("X"))))
            {
                decideConditionsMet247++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR COMPLAINTS FOR WPID..:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR COMPLAINTS FOR WPID..:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUES 'Z'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("Z"))))
            {
                decideConditionsMet247++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OTHER OPEN REQUESTS FOR WPID...........:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OTHER OPEN REQUESTS FOR WPID...........:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet247 > 0))
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),pnd_Misc_Parm_Pnd_Wpid_Text," ",pnd_Misc_Parm_Pnd_Wpid_Count, new                    //Natural: WRITE ( 1 ) / 3T #WPID-TEXT ' ' #WPID-COUNT ( AD = OI ) /
                    FieldAttributes ("AD=OI"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            sort01Work_Prcss_IdCount243.setDec(new DbsDecimal(0));                                                                                                        //Natural: END-BREAK
        }
        if (condition(pnd_Work_Record_Tbl_Wpid_ActIsBreak || icw_Master_Index_Admin_Unit_CdeIsBreak))
        {
            pnd_Misc_Parm_Pnd_Total_Count.nadd(pnd_Misc_Parm_Pnd_Sub_Count);                                                                                              //Natural: COMPUTE #TOTAL-COUNT = #TOTAL-COUNT + #SUB-COUNT
            pnd_Misc_Parm_Pnd_Wpid_Action.setValue(sort01Tbl_Wpid_ActOld);                                                                                                //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID-ACT ) TO #WPID-ACTION
            short decideConditionsMet272 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
            if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("B"))))
            {
                decideConditionsMet272++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR BOOKLETS (ALL WPIDS).....:  ",pnd_Misc_Parm_Pnd_Booklet_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR BOOKLETS (ALL WPIDS).....:  ' #BOOKLET-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("F"))))
            {
                decideConditionsMet272++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR FORMS (ALL WPIDS)........:  ",pnd_Misc_Parm_Pnd_Forms_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR FORMS (ALL WPIDS)........:  ' #FORMS-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("I"))))
            {
                decideConditionsMet272++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR INQUIRY (ALL WPIDS)......:  ",pnd_Misc_Parm_Pnd_Inquire_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR INQUIRY (ALL WPIDS)......:  ' #INQUIRE-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("R"))))
            {
                decideConditionsMet272++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR RESEARCH (ALL WPIDS).....:  ",pnd_Misc_Parm_Pnd_Research_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR RESEARCH (ALL WPIDS).....:  ' #RESEARCH-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("T"))))
            {
                decideConditionsMet272++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR TRANSACTIONS (ALL WPIDS).:  ",pnd_Misc_Parm_Pnd_Trans_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR TRANSACTIONS (ALL WPIDS).:  ' #TRANS-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("X"))))
            {
                decideConditionsMet272++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR COMPLAINTS (ALL WPIDS)...:  ",pnd_Misc_Parm_Pnd_Complaint_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR COMPLAINTS (ALL WPIDS)...:  ' #COMPLAINT-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'Z'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("Z"))))
            {
                decideConditionsMet272++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OTHER OPEN REQUESTS (ALL WPIDS)............:  ",pnd_Misc_Parm_Pnd_Other_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OTHER OPEN REQUESTS (ALL WPIDS)............:  ' #OTHER-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Misc_Parm_Pnd_Booklet_Ctr.reset();                                                                                                                        //Natural: RESET #BOOKLET-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANS-CTR #COMPLAINT-CTR #OTHER-CTR #SUB-COUNT
            pnd_Misc_Parm_Pnd_Forms_Ctr.reset();
            pnd_Misc_Parm_Pnd_Inquire_Ctr.reset();
            pnd_Misc_Parm_Pnd_Research_Ctr.reset();
            pnd_Misc_Parm_Pnd_Trans_Ctr.reset();
            pnd_Misc_Parm_Pnd_Complaint_Ctr.reset();
            pnd_Misc_Parm_Pnd_Other_Ctr.reset();
            pnd_Misc_Parm_Pnd_Sub_Count.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(icw_Master_Index_Admin_Unit_CdeIsBreak))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(3),"    GRAND TOTAL OF ALL OPEN REQUESTS               :  ",pnd_Misc_Parm_Pnd_Total_Count,  //Natural: WRITE ( 1 ) // 3T '    GRAND TOTAL OF ALL OPEN REQUESTS               :  ' #TOTAL-COUNT ( AD = OU ) / 3T '    -------------------------------------------------'
                new FieldAttributes ("AD=OU"),NEWLINE,new TabSetting(3),"    -------------------------------------------------");
            if (condition(Global.isEscape())) return;
            pnd_Misc_Parm_Pnd_Booklet_Ctr.reset();                                                                                                                        //Natural: RESET #BOOKLET-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANS-CTR #COMPLAINT-CTR #OTHER-CTR #SUB-COUNT #TOTAL-COUNT
            pnd_Misc_Parm_Pnd_Forms_Ctr.reset();
            pnd_Misc_Parm_Pnd_Inquire_Ctr.reset();
            pnd_Misc_Parm_Pnd_Research_Ctr.reset();
            pnd_Misc_Parm_Pnd_Trans_Ctr.reset();
            pnd_Misc_Parm_Pnd_Complaint_Ctr.reset();
            pnd_Misc_Parm_Pnd_Other_Ctr.reset();
            pnd_Misc_Parm_Pnd_Sub_Count.reset();
            pnd_Misc_Parm_Pnd_Total_Count.reset();
            pnd_Report_Data_Pnd_Rep_Unit_Cde.setValue(sort01Admin_Unit_CdeOld);                                                                                           //Natural: MOVE OLD ( ADMIN-UNIT-CDE ) TO #REP-UNIT-CDE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            //*    CALLNAT 'CWFN3911'
            pnd_New_Unit.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-UNIT
            pnd_Page.reset();                                                                                                                                             //Natural: RESET #PAGE #SAVE-ACTION
            pnd_Misc_Parm_Pnd_Save_Action.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=142 PS=58");
        Global.format(1, "LS=142 PS=60");

        getReports().setDisplayColumns(1, "//RECEIVED/AT TIAA",
        		icw_Master_Index_Tiaa_Rcvd_Dte_Tme, new ReportEditMask ("MM/DD/YY"),"//LIST/RECEIVED",
        		icw_Master_Index_List_Data_Rcvd_Dte, new ReportEditMask ("MM/DD/YY"),"//RECEIVED/IN UNIT",
        		pnd_Misc_Parm_Pnd_Return_Rcvd_Txt,"//PART./DATE",
        		icw_Master_Index_Prtcpnt_Dte, new ReportEditMask ("MM/DD/YY"),"//WORK/PROCESS",
        		pnd_Misc_Parm_Pnd_Wpid_Sname, new IdenticalSuppress(true), new AlphanumericLength (12),"//WORK/STEP",
        		icw_Master_Index_Step_Id,"///STATUS",
        		pnd_Misc_Parm_Pnd_Status_Sname, new AlphanumericLength (20),"//STATUS/DATE",
        		icw_Master_Index_Admin_Status_Updte_Dte_Tme, new ReportEditMask ("MM/DD/YY"),"BUS/DAYS/IN/TIAA",
        		pnd_Work_Record_Tbl_Tiaa_Bsnss_Days4, new ReportEditMask ("ZZZ9"),"//PPG/CDE",
        		icw_Master_Index_Ppg_Cde,"/SCHEDLD/PAY/DATE",
        		pnd_Date_Msg,"//CHK/IND",
        		icw_Master_Index_Check_Ind,"///EMPLOYEE",
        		icw_Master_Index_Empl_Racf_Id);
    }
}
