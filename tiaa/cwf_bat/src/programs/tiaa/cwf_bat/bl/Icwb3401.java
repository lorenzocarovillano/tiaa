/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:39:28 PM
**        * FROM NATURAL PROGRAM : Icwb3401
************************************************************
**        * FILE NAME            : Icwb3401.java
**        * CLASS NAME           : Icwb3401
**        * INSTANCE NAME        : Icwb3401
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 1
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : ICWB3401/CWFB3401
* SYSTEM   : CRPCWF
* TITLE    : REPORT 1
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF WORK REQUEST LOGGED AND INDEXED
*          | BATCH SPECIAL - CRC WEEKLY REPORT OF LOGGED + RETURNING DOC
*          |
*          |
*          |
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwb3401 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Rep_Parm;
    private DbsField pnd_Rep_Parm_Pnd_Opt;
    private DbsField pnd_Rep_Parm_Pnd_Opt_Var;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Racf_Id;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Empl_Extn;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Dd;

    private DbsGroup pnd_Rep_Parm__R_Field_1;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Dd_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Mm;

    private DbsGroup pnd_Rep_Parm__R_Field_2;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Mm_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Yy;

    private DbsGroup pnd_Rep_Parm__R_Field_3;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Yy_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Dd;

    private DbsGroup pnd_Rep_Parm__R_Field_4;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Dd_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Mm;

    private DbsGroup pnd_Rep_Parm__R_Field_5;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Mm_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Yy;

    private DbsGroup pnd_Rep_Parm__R_Field_6;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Yy_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Unit_Cde;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Wpid;

    private DbsGroup pnd_Rep_Parm__R_Field_7;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Wpid_Actn_Rqstd_Cde;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Wpid_Lob;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Wpid_Mbp;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Wpid_Sbp;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date;

    private DbsGroup pnd_Rep_Parm__R_Field_8;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date_N;
    private DbsField pnd_Rep_Parm_Pnd_End_Date;

    private DbsGroup pnd_Rep_Parm__R_Field_9;
    private DbsField pnd_Rep_Parm_Pnd_End_Date_N;
    private DbsField pnd_Rep_Parm_Pnd_Work_Start_Date_A;

    private DbsGroup pnd_Rep_Parm__R_Field_10;
    private DbsField pnd_Rep_Parm_Pnd_Work_Mm;
    private DbsField pnd_Rep_Parm_Pnd_Filler1;
    private DbsField pnd_Rep_Parm_Pnd_Work_Dd;
    private DbsField pnd_Rep_Parm_Pnd_Filler2;
    private DbsField pnd_Rep_Parm_Pnd_Work_Yy;
    private DbsField pnd_Rep_Parm_Pnd_Work_End_Date_A;

    private DbsGroup pnd_Rep_Parm__R_Field_11;
    private DbsField pnd_Rep_Parm_Pnd_Work_Mmm;
    private DbsField pnd_Rep_Parm_Pnd_Fillera;
    private DbsField pnd_Rep_Parm_Pnd_Work_Ddd;
    private DbsField pnd_Rep_Parm_Pnd_Fillerb;
    private DbsField pnd_Rep_Parm_Pnd_Work_Yyy;
    private DbsField pnd_Start_Key;

    private DbsGroup pnd_Start_Key__R_Field_12;
    private DbsField pnd_Start_Key_Pnd_Start_Key_Dt;
    private DbsField pnd_Start_Key_Pnd_Start_Key_Tm;
    private DbsField pnd_Start_Key_Pnd_Start_Key_Ind;
    private DbsField pnd_End_Key;

    private DbsGroup pnd_End_Key__R_Field_13;
    private DbsField pnd_End_Key_Pnd_End_Key_Dt;
    private DbsField pnd_End_Key_Pnd_End_Key_Tm;
    private DbsField pnd_End_Key_Pnd_End_Key_Ind;

    private DbsGroup pnd_Local_Data;

    private DbsGroup pnd_Local_Data_Pnd_Work_Record;
    private DbsField pnd_Local_Data_Tbl_Wpid;

    private DbsGroup pnd_Local_Data__R_Field_14;
    private DbsField pnd_Local_Data_Tbl_Wpid_Action;
    private DbsField pnd_Local_Data_Tbl_Wpid_Lob;
    private DbsField pnd_Local_Data_Tbl_Wpid_Mbp;
    private DbsField pnd_Local_Data_Tbl_Wpid_Sbp;
    private DbsField pnd_Local_Data_Tbl_Wpid_Act;
    private DbsField pnd_Local_Data_Tbl_Status;
    private DbsField pnd_Local_Data_Tbl_Racf_Id;
    private DbsField pnd_Local_Data_Pnd_Work_Date_D;
    private DbsField pnd_Local_Data_Pnd_Rep_Wpid_Name;
    private DbsField pnd_Local_Data_Pnd_Rep_Empl_Name;
    private DbsField pnd_Local_Data_Pnd_Pad_Space;
    private DbsField pnd_Local_Data_Pnd_Work_Prcss_Id;
    private DbsField pnd_Local_Data_Pnd_Wpid_Code;
    private DbsField pnd_Local_Data_Pnd_Wpid_Desc;
    private DbsField pnd_Local_Data_Pnd_Work_Status;
    private DbsField pnd_Local_Data_Pnd_Rep_Ctr;
    private DbsField pnd_Local_Data_Pnd_Max;
    private DbsField pnd_Local_Data_Pnd_Max_Less_1;
    private DbsField pnd_Local_Data_Pnd_Total_New;
    private DbsField pnd_Local_Data_Pnd_Total_Logged;
    private DbsField pnd_Local_Data_Pnd_Wpid_Count;
    private DbsField pnd_Local_Data_Pnd_Wpid_New;
    private DbsField pnd_Local_Data_Pnd_Unclear_Ctr;
    private DbsField pnd_Local_Data_Pnd_Returned_Doc;
    private DbsField pnd_Local_Data_Pnd_Total_Returned_Doc;
    private DbsField pnd_Local_Data_Pnd_Total_Unclear;
    private DbsField pnd_Local_Data_Pnd_Booklet_Ctr;
    private DbsField pnd_Local_Data_Pnd_Forms_Ctr;
    private DbsField pnd_Local_Data_Pnd_Inquire_Ctr;
    private DbsField pnd_Local_Data_Pnd_Research_Ctr;
    private DbsField pnd_Local_Data_Pnd_Trans_Ctr;
    private DbsField pnd_Local_Data_Pnd_Complaint_Ctr;
    private DbsField pnd_Local_Data_Pnd_Other_Ctr;
    private DbsField pnd_Local_Data_Pnd_Rbooklet_Ctr;
    private DbsField pnd_Local_Data_Pnd_Rforms_Ctr;
    private DbsField pnd_Local_Data_Pnd_Rinquire_Ctr;
    private DbsField pnd_Local_Data_Pnd_Rresearch_Ctr;
    private DbsField pnd_Local_Data_Pnd_Rtrans_Ctr;
    private DbsField pnd_Local_Data_Pnd_Rcomplaint_Ctr;
    private DbsField pnd_Local_Data_Pnd_Rother_Ctr;
    private DbsField pnd_Local_Data_Pnd_Spec_Rem_Ctr;
    private DbsField pnd_Local_Data_Pnd_Annt_Con_Ctr;
    private DbsField pnd_Local_Data_Pnd_Autm_Rem_Ctr;
    private DbsField pnd_Local_Data_Pnd_Cont_Rep_Ctr;
    private DbsField pnd_Local_Data_Pnd_End_Of_Data;
    private DbsField pnd_Local_Data_Pnd_Confirmed;

    private DataAccessProgramView vw_icw_Master_Index;
    private DbsField icw_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup icw_Master_Index__R_Field_15;
    private DbsField icw_Master_Index_Rqst_Log_Index_Dte;
    private DbsField icw_Master_Index_Rqst_Log_Index_Tme;
    private DbsField icw_Master_Index_Rqst_Log_Unit_Cde;
    private DbsField icw_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField icw_Master_Index_Modify_Unit_Cde;
    private DbsField icw_Master_Index_Work_Prcss_Id;
    private DbsField icw_Master_Index_Admin_Status_Cde;
    private DbsField icw_Master_Index_Actve_Ind;

    private DbsGroup icw_Master_Index__R_Field_16;
    private DbsField icw_Master_Index_Actve_Ind_1;
    private DbsField icw_Master_Index_Actve_Ind_2;
    private DbsField icw_Master_Index_Actv_Unque_Key;
    private DbsField pnd_Env;
    private DbsField pnd_Page;
    private DbsField pnd_Sort_Unit;
    private DbsField pnd_Work_Date;
    private DbsField pnd_To;
    private DbsField pnd_From;
    private DbsField pnd_Day;
    private DbsField pnd_Sub;
    private DbsField pnd_New_Racf;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Yyyymmdd;

    private DbsGroup pnd_Yyyymmdd__R_Field_17;
    private DbsField pnd_Yyyymmdd_Pnd_Century;
    private DbsField pnd_Yyyymmdd_Pnd_Yy;
    private DbsField pnd_Yyyymmdd_Pnd_Mm;
    private DbsField pnd_Yyyymmdd_Pnd_Dd;
    private DbsField pnd_Report_No;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Reccount;
    private DbsField pnd_Work_Start_Date;
    private DbsField pnd_Work_Comp_Date;
    private DbsField pnd_Date_Diff;
    private DbsField pnd_Oprtr_Cde;
    private DbsField pnd_Day_Of_Week;
    private DbsField pnd_Days_To_Subtract;
    private DbsField pnd_Cirs_Unit;
    private DbsField pnd_Pend_Dte_Tme;

    private DbsGroup pnd_Pend_Dte_Tme__R_Field_18;
    private DbsField pnd_Pend_Dte_Tme_Pnd_Pend_Dte;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Tbl_WpidOld;
    private DbsField sort01Tbl_WpidCount424;
    private DbsField sort01Tbl_WpidCount;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Rep_Parm = localVariables.newGroupInRecord("pnd_Rep_Parm", "#REP-PARM");
        pnd_Rep_Parm_Pnd_Opt = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Opt", "#OPT", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Opt_Var = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Opt_Var", "#OPT-VAR", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Rep_Parm_Pnd_Rep_Racf_Id = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Racf_Id", "#REP-RACF-ID", FieldType.STRING, 8);
        pnd_Rep_Parm_Pnd_Rep_Empl_Extn = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Empl_Extn", "#REP-EMPL-EXTN", FieldType.STRING, 4);
        pnd_Rep_Parm_Pnd_Rep_Start_Dd = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Dd", "#REP-START-DD", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_1 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_1", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_Start_Dd);
        pnd_Rep_Parm_Pnd_Rep_Start_Dd_A = pnd_Rep_Parm__R_Field_1.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Dd_A", "#REP-START-DD-A", FieldType.STRING, 
            2);
        pnd_Rep_Parm_Pnd_Rep_Start_Mm = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Mm", "#REP-START-MM", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_2 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_2", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_Start_Mm);
        pnd_Rep_Parm_Pnd_Rep_Start_Mm_A = pnd_Rep_Parm__R_Field_2.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Mm_A", "#REP-START-MM-A", FieldType.STRING, 
            2);
        pnd_Rep_Parm_Pnd_Rep_Start_Yy = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Yy", "#REP-START-YY", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_3 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_3", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_Start_Yy);
        pnd_Rep_Parm_Pnd_Rep_Start_Yy_A = pnd_Rep_Parm__R_Field_3.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Yy_A", "#REP-START-YY-A", FieldType.STRING, 
            2);
        pnd_Rep_Parm_Pnd_Rep_End_Dd = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Dd", "#REP-END-DD", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_4 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_4", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_End_Dd);
        pnd_Rep_Parm_Pnd_Rep_End_Dd_A = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Dd_A", "#REP-END-DD-A", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Rep_End_Mm = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Mm", "#REP-END-MM", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_5 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_5", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_End_Mm);
        pnd_Rep_Parm_Pnd_Rep_End_Mm_A = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Mm_A", "#REP-END-MM-A", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Rep_End_Yy = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Yy", "#REP-END-YY", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_6 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_6", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_End_Yy);
        pnd_Rep_Parm_Pnd_Rep_End_Yy_A = pnd_Rep_Parm__R_Field_6.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Yy_A", "#REP-END-YY-A", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Rep_Unit_Cde = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);
        pnd_Rep_Parm_Pnd_Rep_Wpid = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Wpid", "#REP-WPID", FieldType.STRING, 6);

        pnd_Rep_Parm__R_Field_7 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_7", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_Wpid);
        pnd_Rep_Parm_Pnd_Rep_Wpid_Actn_Rqstd_Cde = pnd_Rep_Parm__R_Field_7.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Wpid_Actn_Rqstd_Cde", "#REP-WPID-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Rep_Wpid_Lob = pnd_Rep_Parm__R_Field_7.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Wpid_Lob", "#REP-WPID-LOB", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Rep_Wpid_Mbp = pnd_Rep_Parm__R_Field_7.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Wpid_Mbp", "#REP-WPID-MBP", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Rep_Wpid_Sbp = pnd_Rep_Parm__R_Field_7.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Wpid_Sbp", "#REP-WPID-SBP", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Start_Date = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_8 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_8", "REDEFINE", pnd_Rep_Parm_Pnd_Start_Date);
        pnd_Rep_Parm_Pnd_Start_Date_N = pnd_Rep_Parm__R_Field_8.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 8);
        pnd_Rep_Parm_Pnd_End_Date = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_9 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_9", "REDEFINE", pnd_Rep_Parm_Pnd_End_Date);
        pnd_Rep_Parm_Pnd_End_Date_N = pnd_Rep_Parm__R_Field_9.newFieldInGroup("pnd_Rep_Parm_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 8);
        pnd_Rep_Parm_Pnd_Work_Start_Date_A = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Start_Date_A", "#WORK-START-DATE-A", FieldType.STRING, 
            8);

        pnd_Rep_Parm__R_Field_10 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_10", "REDEFINE", pnd_Rep_Parm_Pnd_Work_Start_Date_A);
        pnd_Rep_Parm_Pnd_Work_Mm = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Mm", "#WORK-MM", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Filler1 = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Dd = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Filler2 = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Yy = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Yy", "#WORK-YY", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Work_End_Date_A = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_End_Date_A", "#WORK-END-DATE-A", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_11 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_11", "REDEFINE", pnd_Rep_Parm_Pnd_Work_End_Date_A);
        pnd_Rep_Parm_Pnd_Work_Mmm = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Mmm", "#WORK-MMM", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Fillera = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Fillera", "#FILLERA", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Ddd = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Ddd", "#WORK-DDD", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Fillerb = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Fillerb", "#FILLERB", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Yyy = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Yyy", "#WORK-YYY", FieldType.STRING, 2);
        pnd_Start_Key = localVariables.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.STRING, 16);

        pnd_Start_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Start_Key__R_Field_12", "REDEFINE", pnd_Start_Key);
        pnd_Start_Key_Pnd_Start_Key_Dt = pnd_Start_Key__R_Field_12.newFieldInGroup("pnd_Start_Key_Pnd_Start_Key_Dt", "#START-KEY-DT", FieldType.STRING, 
            8);
        pnd_Start_Key_Pnd_Start_Key_Tm = pnd_Start_Key__R_Field_12.newFieldInGroup("pnd_Start_Key_Pnd_Start_Key_Tm", "#START-KEY-TM", FieldType.NUMERIC, 
            7);
        pnd_Start_Key_Pnd_Start_Key_Ind = pnd_Start_Key__R_Field_12.newFieldInGroup("pnd_Start_Key_Pnd_Start_Key_Ind", "#START-KEY-IND", FieldType.STRING, 
            1);
        pnd_End_Key = localVariables.newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.STRING, 16);

        pnd_End_Key__R_Field_13 = localVariables.newGroupInRecord("pnd_End_Key__R_Field_13", "REDEFINE", pnd_End_Key);
        pnd_End_Key_Pnd_End_Key_Dt = pnd_End_Key__R_Field_13.newFieldInGroup("pnd_End_Key_Pnd_End_Key_Dt", "#END-KEY-DT", FieldType.STRING, 8);
        pnd_End_Key_Pnd_End_Key_Tm = pnd_End_Key__R_Field_13.newFieldInGroup("pnd_End_Key_Pnd_End_Key_Tm", "#END-KEY-TM", FieldType.NUMERIC, 7);
        pnd_End_Key_Pnd_End_Key_Ind = pnd_End_Key__R_Field_13.newFieldInGroup("pnd_End_Key_Pnd_End_Key_Ind", "#END-KEY-IND", FieldType.STRING, 1);

        pnd_Local_Data = localVariables.newGroupInRecord("pnd_Local_Data", "#LOCAL-DATA");

        pnd_Local_Data_Pnd_Work_Record = pnd_Local_Data.newGroupInGroup("pnd_Local_Data_Pnd_Work_Record", "#WORK-RECORD");
        pnd_Local_Data_Tbl_Wpid = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        pnd_Local_Data__R_Field_14 = pnd_Local_Data_Pnd_Work_Record.newGroupInGroup("pnd_Local_Data__R_Field_14", "REDEFINE", pnd_Local_Data_Tbl_Wpid);
        pnd_Local_Data_Tbl_Wpid_Action = pnd_Local_Data__R_Field_14.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Local_Data_Tbl_Wpid_Lob = pnd_Local_Data__R_Field_14.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Lob", "TBL-WPID-LOB", FieldType.STRING, 2);
        pnd_Local_Data_Tbl_Wpid_Mbp = pnd_Local_Data__R_Field_14.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Mbp", "TBL-WPID-MBP", FieldType.STRING, 1);
        pnd_Local_Data_Tbl_Wpid_Sbp = pnd_Local_Data__R_Field_14.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Sbp", "TBL-WPID-SBP", FieldType.STRING, 2);
        pnd_Local_Data_Tbl_Wpid_Act = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 
            1);
        pnd_Local_Data_Tbl_Status = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Status", "TBL-STATUS", FieldType.STRING, 4);
        pnd_Local_Data_Tbl_Racf_Id = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Racf_Id", "TBL-RACF-ID", FieldType.STRING, 8);
        pnd_Local_Data_Pnd_Work_Date_D = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Work_Date_D", "#WORK-DATE-D", FieldType.DATE);
        pnd_Local_Data_Pnd_Rep_Wpid_Name = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rep_Wpid_Name", "#REP-WPID-NAME", FieldType.STRING, 45);
        pnd_Local_Data_Pnd_Rep_Empl_Name = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rep_Empl_Name", "#REP-EMPL-NAME", FieldType.STRING, 30);
        pnd_Local_Data_Pnd_Pad_Space = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Pad_Space", "#PAD-SPACE", FieldType.STRING, 8);
        pnd_Local_Data_Pnd_Work_Prcss_Id = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Work_Prcss_Id", "#WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Local_Data_Pnd_Wpid_Code = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Code", "#WPID-CODE", FieldType.STRING, 6);
        pnd_Local_Data_Pnd_Wpid_Desc = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Desc", "#WPID-DESC", FieldType.STRING, 45);
        pnd_Local_Data_Pnd_Work_Status = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Work_Status", "#WORK-STATUS", FieldType.STRING, 4);
        pnd_Local_Data_Pnd_Rep_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rep_Ctr", "#REP-CTR", FieldType.PACKED_DECIMAL, 2);
        pnd_Local_Data_Pnd_Max = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Max", "#MAX", FieldType.PACKED_DECIMAL, 2);
        pnd_Local_Data_Pnd_Max_Less_1 = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Max_Less_1", "#MAX-LESS-1", FieldType.PACKED_DECIMAL, 2);
        pnd_Local_Data_Pnd_Total_New = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_New", "#TOTAL-NEW", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Total_Logged = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_Logged", "#TOTAL-LOGGED", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Wpid_Count = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Count", "#WPID-COUNT", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Wpid_New = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_New", "#WPID-NEW", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Unclear_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Unclear_Ctr", "#UNCLEAR-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Returned_Doc = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Returned_Doc", "#RETURNED-DOC", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Total_Returned_Doc = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_Returned_Doc", "#TOTAL-RETURNED-DOC", FieldType.NUMERIC, 
            5);
        pnd_Local_Data_Pnd_Total_Unclear = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_Unclear", "#TOTAL-UNCLEAR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Booklet_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Booklet_Ctr", "#BOOKLET-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Forms_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Forms_Ctr", "#FORMS-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Inquire_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Inquire_Ctr", "#INQUIRE-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Research_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Research_Ctr", "#RESEARCH-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Trans_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Trans_Ctr", "#TRANS-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Complaint_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Complaint_Ctr", "#COMPLAINT-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Other_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Other_Ctr", "#OTHER-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Rbooklet_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rbooklet_Ctr", "#RBOOKLET-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Rforms_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rforms_Ctr", "#RFORMS-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Rinquire_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rinquire_Ctr", "#RINQUIRE-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Rresearch_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rresearch_Ctr", "#RRESEARCH-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Rtrans_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rtrans_Ctr", "#RTRANS-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Rcomplaint_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rcomplaint_Ctr", "#RCOMPLAINT-CTR", FieldType.NUMERIC, 
            5);
        pnd_Local_Data_Pnd_Rother_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rother_Ctr", "#ROTHER-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Spec_Rem_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Spec_Rem_Ctr", "#SPEC-REM-CTR", FieldType.NUMERIC, 7);
        pnd_Local_Data_Pnd_Annt_Con_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Annt_Con_Ctr", "#ANNT-CON-CTR", FieldType.NUMERIC, 7);
        pnd_Local_Data_Pnd_Autm_Rem_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Autm_Rem_Ctr", "#AUTM-REM-CTR", FieldType.NUMERIC, 7);
        pnd_Local_Data_Pnd_Cont_Rep_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Cont_Rep_Ctr", "#CONT-REP-CTR", FieldType.NUMERIC, 7);
        pnd_Local_Data_Pnd_End_Of_Data = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_End_Of_Data", "#END-OF-DATA", FieldType.BOOLEAN, 1);
        pnd_Local_Data_Pnd_Confirmed = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Confirmed", "#CONFIRMED", FieldType.BOOLEAN, 1);

        vw_icw_Master_Index = new DataAccessProgramView(new NameInfo("vw_icw_Master_Index", "ICW-MASTER-INDEX"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        icw_Master_Index_Rqst_Log_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");

        icw_Master_Index__R_Field_15 = vw_icw_Master_Index.getRecord().newGroupInGroup("icw_Master_Index__R_Field_15", "REDEFINE", icw_Master_Index_Rqst_Log_Dte_Tme);
        icw_Master_Index_Rqst_Log_Index_Dte = icw_Master_Index__R_Field_15.newFieldInGroup("icw_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        icw_Master_Index_Rqst_Log_Index_Tme = icw_Master_Index__R_Field_15.newFieldInGroup("icw_Master_Index_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        icw_Master_Index_Rqst_Log_Unit_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Log_Unit_Cde", "RQST-LOG-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_UNIT_CDE");
        icw_Master_Index_Rqst_Log_Oprtr_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        icw_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/TIME");
        icw_Master_Index_Modify_Unit_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Modify_Unit_Cde", "MODIFY-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "MODIFY_UNIT_CDE");
        icw_Master_Index_Work_Prcss_Id = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        icw_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        icw_Master_Index_Admin_Status_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        icw_Master_Index_Actve_Ind = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");

        icw_Master_Index__R_Field_16 = vw_icw_Master_Index.getRecord().newGroupInGroup("icw_Master_Index__R_Field_16", "REDEFINE", icw_Master_Index_Actve_Ind);
        icw_Master_Index_Actve_Ind_1 = icw_Master_Index__R_Field_16.newFieldInGroup("icw_Master_Index_Actve_Ind_1", "ACTVE-IND-1", FieldType.STRING, 1);
        icw_Master_Index_Actve_Ind_2 = icw_Master_Index__R_Field_16.newFieldInGroup("icw_Master_Index_Actve_Ind_2", "ACTVE-IND-2", FieldType.STRING, 1);
        icw_Master_Index_Actv_Unque_Key = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Actv_Unque_Key", "ACTV-UNQUE-KEY", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "ACTV_UNQUE_KEY");
        icw_Master_Index_Actv_Unque_Key.setDdmHeader("UNIQUE/KEY");
        icw_Master_Index_Actv_Unque_Key.setSuperDescriptor(true);
        registerRecord(vw_icw_Master_Index);

        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 5);
        pnd_Sort_Unit = localVariables.newFieldInRecord("pnd_Sort_Unit", "#SORT-UNIT", FieldType.STRING, 1);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_To = localVariables.newFieldInRecord("pnd_To", "#TO", FieldType.DATE);
        pnd_From = localVariables.newFieldInRecord("pnd_From", "#FROM", FieldType.DATE);
        pnd_Day = localVariables.newFieldInRecord("pnd_Day", "#DAY", FieldType.STRING, 3);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.NUMERIC, 1);
        pnd_New_Racf = localVariables.newFieldInRecord("pnd_New_Racf", "#NEW-RACF", FieldType.BOOLEAN, 1);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Yyyymmdd = localVariables.newFieldInRecord("pnd_Yyyymmdd", "#YYYYMMDD", FieldType.STRING, 8);

        pnd_Yyyymmdd__R_Field_17 = localVariables.newGroupInRecord("pnd_Yyyymmdd__R_Field_17", "REDEFINE", pnd_Yyyymmdd);
        pnd_Yyyymmdd_Pnd_Century = pnd_Yyyymmdd__R_Field_17.newFieldInGroup("pnd_Yyyymmdd_Pnd_Century", "#CENTURY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Yy = pnd_Yyyymmdd__R_Field_17.newFieldInGroup("pnd_Yyyymmdd_Pnd_Yy", "#YY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Mm = pnd_Yyyymmdd__R_Field_17.newFieldInGroup("pnd_Yyyymmdd_Pnd_Mm", "#MM", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Dd = pnd_Yyyymmdd__R_Field_17.newFieldInGroup("pnd_Yyyymmdd_Pnd_Dd", "#DD", FieldType.STRING, 2);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Reccount = localVariables.newFieldInRecord("pnd_Reccount", "#RECCOUNT", FieldType.NUMERIC, 5);
        pnd_Work_Start_Date = localVariables.newFieldInRecord("pnd_Work_Start_Date", "#WORK-START-DATE", FieldType.DATE);
        pnd_Work_Comp_Date = localVariables.newFieldInRecord("pnd_Work_Comp_Date", "#WORK-COMP-DATE", FieldType.DATE);
        pnd_Date_Diff = localVariables.newFieldInRecord("pnd_Date_Diff", "#DATE-DIFF", FieldType.NUMERIC, 3);
        pnd_Oprtr_Cde = localVariables.newFieldInRecord("pnd_Oprtr_Cde", "#OPRTR-CDE", FieldType.STRING, 8);
        pnd_Day_Of_Week = localVariables.newFieldInRecord("pnd_Day_Of_Week", "#DAY-OF-WEEK", FieldType.STRING, 3);
        pnd_Days_To_Subtract = localVariables.newFieldInRecord("pnd_Days_To_Subtract", "#DAYS-TO-SUBTRACT", FieldType.NUMERIC, 2);
        pnd_Cirs_Unit = localVariables.newFieldInRecord("pnd_Cirs_Unit", "#CIRS-UNIT", FieldType.STRING, 8);
        pnd_Pend_Dte_Tme = localVariables.newFieldInRecord("pnd_Pend_Dte_Tme", "#PEND-DTE-TME", FieldType.STRING, 15);

        pnd_Pend_Dte_Tme__R_Field_18 = localVariables.newGroupInRecord("pnd_Pend_Dte_Tme__R_Field_18", "REDEFINE", pnd_Pend_Dte_Tme);
        pnd_Pend_Dte_Tme_Pnd_Pend_Dte = pnd_Pend_Dte_Tme__R_Field_18.newFieldInGroup("pnd_Pend_Dte_Tme_Pnd_Pend_Dte", "#PEND-DTE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Tbl_WpidOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_OLD", "Tbl_Wpid_OLD", FieldType.STRING, 6);
        sort01Tbl_WpidCount424 = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT_424", "Tbl_Wpid_COUNT_424", FieldType.NUMERIC, 9);
        sort01Tbl_WpidCount = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT", "Tbl_Wpid_COUNT", FieldType.NUMERIC, 9);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_icw_Master_Index.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Report_No.setInitialValue(1);
        pnd_Cirs_Unit.setInitialValue("CIRS");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Icwb3401() throws Exception
    {
        super("Icwb3401");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJICW") || pnd_Env.equals("PROJICW")))                                                                                            //Natural: IF #ENV = 'PROJICW' OR #ENV = 'PROJICW'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        //* ********************
        //*                    *
        //*  REPORT SECTION    *
        //* ********************
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 58
        pnd_Rep_Parm_Pnd_Filler1.setValue("/");                                                                                                                           //Natural: MOVE '/' TO #FILLER1 #FILLER2
        pnd_Rep_Parm_Pnd_Filler2.setValue("/");
        pnd_Rep_Parm_Pnd_Fillera.setValue("/");                                                                                                                           //Natural: MOVE '/' TO #FILLERA #FILLERB
        pnd_Rep_Parm_Pnd_Fillerb.setValue("/");
        pnd_Comp_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                    //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #COMP-DATE
        pnd_Work_Comp_Date.setValue(Global.getDATX());                                                                                                                    //Natural: MOVE *DATX TO #WORK-COMP-DATE
        pnd_Day_Of_Week.setValueEdited(Global.getDATX(),new ReportEditMask("NNN"));                                                                                       //Natural: MOVE EDITED *DATX ( EM = NNN ) TO #DAY-OF-WEEK
        //*  REPORT GENERATED FROM LAST SUNDAY TO LATEST SATURDAY (7 DAYS)
        short decideConditionsMet230 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #DAY-OF-WEEK;//Natural: VALUE 'Sat'
        if (condition((pnd_Day_Of_Week.equals("Sat"))))
        {
            decideConditionsMet230++;
            pnd_Days_To_Subtract.setValue(6);                                                                                                                             //Natural: MOVE 6 TO #DAYS-TO-SUBTRACT
        }                                                                                                                                                                 //Natural: VALUE 'Sun'
        else if (condition((pnd_Day_Of_Week.equals("Sun"))))
        {
            decideConditionsMet230++;
            pnd_Days_To_Subtract.setValue(7);                                                                                                                             //Natural: MOVE 7 TO #DAYS-TO-SUBTRACT
        }                                                                                                                                                                 //Natural: VALUE 'Mon'
        else if (condition((pnd_Day_Of_Week.equals("Mon"))))
        {
            decideConditionsMet230++;
            pnd_Days_To_Subtract.setValue(8);                                                                                                                             //Natural: MOVE 8 TO #DAYS-TO-SUBTRACT
        }                                                                                                                                                                 //Natural: VALUE 'Tue'
        else if (condition((pnd_Day_Of_Week.equals("Tue"))))
        {
            decideConditionsMet230++;
            pnd_Days_To_Subtract.setValue(9);                                                                                                                             //Natural: MOVE 9 TO #DAYS-TO-SUBTRACT
        }                                                                                                                                                                 //Natural: VALUE 'Wed'
        else if (condition((pnd_Day_Of_Week.equals("Wed"))))
        {
            decideConditionsMet230++;
            pnd_Days_To_Subtract.setValue(10);                                                                                                                            //Natural: MOVE 10 TO #DAYS-TO-SUBTRACT
        }                                                                                                                                                                 //Natural: VALUE 'Thu'
        else if (condition((pnd_Day_Of_Week.equals("Thu"))))
        {
            decideConditionsMet230++;
            pnd_Days_To_Subtract.setValue(11);                                                                                                                            //Natural: MOVE 11 TO #DAYS-TO-SUBTRACT
        }                                                                                                                                                                 //Natural: VALUE 'Fri'
        else if (condition((pnd_Day_Of_Week.equals("Fri"))))
        {
            decideConditionsMet230++;
            pnd_Days_To_Subtract.setValue(12);                                                                                                                            //Natural: MOVE 12 TO #DAYS-TO-SUBTRACT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Work_Comp_Date.nsubtract(pnd_Days_To_Subtract);                                                                                                               //Natural: SUBTRACT #DAYS-TO-SUBTRACT FROM #WORK-COMP-DATE
        pnd_Rep_Parm_Pnd_Start_Date.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = YYYYMMDD ) TO #START-DATE
        pnd_Work_Comp_Date.nadd(6);                                                                                                                                       //Natural: ADD 6 TO #WORK-COMP-DATE
        pnd_Rep_Parm_Pnd_End_Date.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("YYYYMMDD"));                                                                      //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = YYYYMMDD ) TO #END-DATE
        pnd_Start_Key.reset();                                                                                                                                            //Natural: RESET #START-KEY #END-KEY
        pnd_End_Key.reset();
        pnd_Start_Key_Pnd_Start_Key_Dt.setValue(pnd_Rep_Parm_Pnd_Start_Date);                                                                                             //Natural: MOVE #START-DATE TO #START-KEY-DT
        //*  VE  '19960101'  TO #START-KEY-DT
        pnd_Start_Key_Pnd_Start_Key_Tm.setValue(0);                                                                                                                       //Natural: MOVE 0 TO #START-KEY-TM
        pnd_End_Key_Pnd_End_Key_Dt.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                                   //Natural: MOVE #END-DATE TO #END-KEY-DT
        pnd_End_Key_Pnd_End_Key_Tm.setValue(9999999);                                                                                                                     //Natural: MOVE 9999999 TO #END-KEY-TM
        pnd_Comp_Date.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                                                //Natural: MOVE #END-DATE TO #COMP-DATE
        pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_Start_Date);                                                                                                               //Natural: MOVE #START-DATE TO #YYYYMMDD
        pnd_Rep_Parm_Pnd_Work_Mm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                                           //Natural: MOVE #MM TO #WORK-MM
        pnd_Rep_Parm_Pnd_Work_Dd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                                           //Natural: MOVE #DD TO #WORK-DD
        pnd_Rep_Parm_Pnd_Work_Yy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                                           //Natural: MOVE #YY TO #WORK-YY
        pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                                                 //Natural: MOVE #END-DATE TO #YYYYMMDD
        pnd_Rep_Parm_Pnd_Work_Mmm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                                          //Natural: MOVE #MM TO #WORK-MMM
        pnd_Rep_Parm_Pnd_Work_Ddd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                                          //Natural: MOVE #DD TO #WORK-DDD
        pnd_Rep_Parm_Pnd_Work_Yyy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                                          //Natural: MOVE #YY TO #WORK-YYY
        vw_icw_Master_Index.startDatabaseRead                                                                                                                             //Natural: READ ICW-MASTER-INDEX BY ACTV-UNQUE-KEY FROM #START-KEY
        (
        "READ_MASTER",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pnd_Start_Key, WcType.BY) },
        new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") }
        );
        READ_MASTER:
        while (condition(vw_icw_Master_Index.readNextRow("READ_MASTER")))
        {
            if (condition(icw_Master_Index_Rqst_Log_Dte_Tme.greater(pnd_End_Key)))                                                                                        //Natural: IF RQST-LOG-DTE-TME GT #END-KEY
            {
                if (true) break READ_MASTER;                                                                                                                              //Natural: ESCAPE BOTTOM ( READ-MASTER. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(icw_Master_Index_Rqst_Log_Unit_Cde.equals(pnd_Cirs_Unit))))                                                                                   //Natural: ACCEPT IF ICW-MASTER-INDEX.RQST-LOG-UNIT-CDE = #CIRS-UNIT
            {
                continue;
            }
            pnd_Local_Data_Pnd_Work_Prcss_Id.setValue(icw_Master_Index_Work_Prcss_Id);                                                                                    //Natural: MOVE ICW-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-PRCSS-ID
            pnd_Oprtr_Cde.setValue(icw_Master_Index_Rqst_Log_Oprtr_Cde);                                                                                                  //Natural: MOVE ICW-MASTER-INDEX.RQST-LOG-OPRTR-CDE TO #OPRTR-CDE
            pnd_Local_Data_Pnd_Work_Status.setValue(icw_Master_Index_Admin_Status_Cde);                                                                                   //Natural: MOVE ICW-MASTER-INDEX.ADMIN-STATUS-CDE TO #WORK-STATUS
            //*  MOVE EDITED CWF-MASTER-INDEX.EXTRNL-PEND-RCV-DTE(EM=YYYYMMDDHHIISST)
            //*    TO #PEND-DTE-TME
            //* EMPL+UNIT
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_MASTER"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_MASTER"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  READ-MASTER.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Reccount.equals(getZero())))                                                                                                                    //Natural: IF #RECCOUNT = 0
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Bldg.setValue("W");                                                                                                                                       //Natural: MOVE 'W' TO #BLDG
            DbsUtil.callnat(Icwn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pnd_Rep_Parm_Pnd_Rep_Empl_Extn, pnd_Cirs_Unit, pnd_Floor,              //Natural: CALLNAT 'ICWN3910' #REPORT-NO #RACF-ID #REP-EMPL-EXTN #CIRS-UNIT #FLOOR #BLDG #DROP-OFF #COMP-DATE
                pnd_Bldg, pnd_Drop_Off, pnd_Comp_Date);
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Icwn3915.class , getCurrentProcessState(), pnd_Report_No);                                                                                    //Natural: CALLNAT 'ICWN3915' #REPORT-NO
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Icwn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'ICWN3911'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Unit.setValue(true);                                                                                                                                      //Natural: MOVE TRUE TO #NEW-UNIT #NEW-RACF
        pnd_New_Racf.setValue(true);
        setControl("WB");                                                                                                                                                 //Natural: SET CONTROL 'WB'
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, pnd_Local_Data_Pnd_Work_Record)))
        {
            getSort().writeSortInData(pnd_Local_Data_Tbl_Racf_Id, pnd_Local_Data_Tbl_Wpid_Act, pnd_Local_Data_Tbl_Wpid, pnd_Local_Data_Tbl_Status, pnd_Sort_Unit);        //Natural: END-ALL
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Local_Data_Tbl_Racf_Id, pnd_Local_Data_Tbl_Wpid_Act, pnd_Local_Data_Tbl_Wpid, pnd_Local_Data_Tbl_Status);                                  //Natural: SORT BY TBL-RACF-ID TBL-WPID-ACT TBL-WPID TBL-STATUS USING #SORT-UNIT
        sort01Tbl_WpidCount424.setDec(new DbsDecimal(0));
        sort01Tbl_WpidCount.setDec(new DbsDecimal(0));
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Local_Data_Tbl_Racf_Id, pnd_Local_Data_Tbl_Wpid_Act, pnd_Local_Data_Tbl_Wpid, pnd_Local_Data_Tbl_Status, 
            pnd_Sort_Unit)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            sort01Tbl_WpidCount424.setInt(sort01Tbl_WpidCount424.getInt() + 1);
            sort01Tbl_WpidCount.setInt(sort01Tbl_WpidCount.getInt() + 1);
            pnd_Rep_Parm_Pnd_Rep_Unit_Cde.setValue(pnd_Cirs_Unit);                                                                                                        //Natural: MOVE #CIRS-UNIT TO #REP-UNIT-CDE #REP-EMPL-NAME
            pnd_Local_Data_Pnd_Rep_Empl_Name.setValue(pnd_Cirs_Unit);
            pnd_Rep_Parm_Pnd_Rep_Racf_Id.setValue(pnd_Local_Data_Tbl_Racf_Id);                                                                                            //Natural: MOVE TBL-RACF-ID TO #REP-RACF-ID
            DbsUtil.callnat(Icwn1107.class , getCurrentProcessState(), pnd_Rep_Parm_Pnd_Rep_Racf_Id, pnd_Local_Data_Pnd_Rep_Empl_Name, pnd_Rep_Parm_Pnd_Rep_Empl_Extn);   //Natural: CALLNAT 'ICWN1107' #REP-RACF-ID #REP-EMPL-NAME #REP-EMPL-EXTN
            if (condition(Global.isEscape())) return;
            if (condition(pnd_New_Unit.getBoolean()))                                                                                                                     //Natural: IF #NEW-UNIT
            {
                pnd_Bldg.setValue("W");                                                                                                                                   //Natural: MOVE 'W' TO #BLDG
                DbsUtil.callnat(Icwn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pnd_Rep_Parm_Pnd_Rep_Empl_Extn, pnd_Cirs_Unit,                     //Natural: CALLNAT 'ICWN3910' #REPORT-NO #RACF-ID #REP-EMPL-EXTN #CIRS-UNIT #FLOOR #BLDG #DROP-OFF #COMP-DATE
                    pnd_Floor, pnd_Bldg, pnd_Drop_Off, pnd_Comp_Date);
                if (condition(Global.isEscape())) return;
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet337 = 0;                                                                                                                             //Natural: AT TOP OF PAGE ( 1 );//Natural: DECIDE ON FIRST VALUE OF TBL-WPID-ACT;//Natural: VALUE 'B'
            if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("B"))))
            {
                decideConditionsMet337++;
                if (condition(pnd_Local_Data_Tbl_Status.equals("0200")))                                                                                                  //Natural: IF TBL-STATUS = '0200'
                {
                    pnd_Local_Data_Pnd_Rbooklet_Ctr.nadd(1);                                                                                                              //Natural: ADD 1 TO #RBOOKLET-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Local_Data_Pnd_Booklet_Ctr.nadd(1);                                                                                                               //Natural: ADD 1 TO #BOOKLET-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("F"))))
            {
                decideConditionsMet337++;
                if (condition(pnd_Local_Data_Tbl_Status.equals("0200")))                                                                                                  //Natural: IF TBL-STATUS = '0200'
                {
                    pnd_Local_Data_Pnd_Rforms_Ctr.nadd(1);                                                                                                                //Natural: ADD 1 TO #RFORMS-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Local_Data_Pnd_Forms_Ctr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #FORMS-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("I"))))
            {
                decideConditionsMet337++;
                if (condition(pnd_Local_Data_Tbl_Status.equals("0200")))                                                                                                  //Natural: IF TBL-STATUS = '0200'
                {
                    pnd_Local_Data_Pnd_Rinquire_Ctr.nadd(1);                                                                                                              //Natural: ADD 1 TO #RINQUIRE-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Local_Data_Pnd_Inquire_Ctr.nadd(1);                                                                                                               //Natural: ADD 1 TO #INQUIRE-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("R"))))
            {
                decideConditionsMet337++;
                if (condition(pnd_Local_Data_Tbl_Status.equals("0200")))                                                                                                  //Natural: IF TBL-STATUS = '0200'
                {
                    pnd_Local_Data_Pnd_Rresearch_Ctr.nadd(1);                                                                                                             //Natural: ADD 1 TO #RRESEARCH-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Local_Data_Pnd_Research_Ctr.nadd(1);                                                                                                              //Natural: ADD 1 TO #RESEARCH-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("T"))))
            {
                decideConditionsMet337++;
                if (condition(pnd_Local_Data_Tbl_Status.equals("0200")))                                                                                                  //Natural: IF TBL-STATUS = '0200'
                {
                    pnd_Local_Data_Pnd_Rtrans_Ctr.nadd(1);                                                                                                                //Natural: ADD 1 TO #RTRANS-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Local_Data_Pnd_Trans_Ctr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TRANS-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("X"))))
            {
                decideConditionsMet337++;
                if (condition(pnd_Local_Data_Tbl_Status.equals("0200")))                                                                                                  //Natural: IF TBL-STATUS = '0200'
                {
                    pnd_Local_Data_Pnd_Rcomplaint_Ctr.nadd(1);                                                                                                            //Natural: ADD 1 TO #RCOMPLAINT-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Local_Data_Pnd_Complaint_Ctr.nadd(1);                                                                                                             //Natural: ADD 1 TO #COMPLAINT-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'Z'
            else if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("Z"))))
            {
                decideConditionsMet337++;
                if (condition(pnd_Local_Data_Tbl_Status.equals("0200")))                                                                                                  //Natural: IF TBL-STATUS = '0200'
                {
                    pnd_Local_Data_Pnd_Rother_Ctr.nadd(1);                                                                                                                //Natural: ADD 1 TO #ROTHER-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Local_Data_Pnd_Other_Ctr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #OTHER-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *** COLLECTING COUNTERS FOR SPECIFIC BUSINESS PROCESS
            //*  SPECIAL REMITTER
            if (condition(pnd_Local_Data_Tbl_Wpid_Sbp.equals("SP")))                                                                                                      //Natural: IF TBL-WPID-SBP = 'SP'
            {
                pnd_Local_Data_Pnd_Spec_Rem_Ctr.nadd(1);                                                                                                                  //Natural: ADD 1 TO #SPEC-REM-CTR
            }                                                                                                                                                             //Natural: END-IF
            //*  ANNUITY CONTRIBUTION STATEMENT
            if (condition(pnd_Local_Data_Tbl_Wpid_Sbp.equals("AC")))                                                                                                      //Natural: IF TBL-WPID-SBP = 'AC'
            {
                pnd_Local_Data_Pnd_Annt_Con_Ctr.nadd(1);                                                                                                                  //Natural: ADD 1 TO #ANNT-CON-CTR
            }                                                                                                                                                             //Natural: END-IF
            //*  AUTOMATED REMITTER
            if (condition(pnd_Local_Data_Tbl_Wpid_Sbp.equals("AR")))                                                                                                      //Natural: IF TBL-WPID-SBP = 'AR'
            {
                pnd_Local_Data_Pnd_Autm_Rem_Ctr.nadd(1);                                                                                                                  //Natural: ADD 1 TO #AUTM-REM-CTR
            }                                                                                                                                                             //Natural: END-IF
            //*  CONTRIBUTION REPORTING SYSTEM
            if (condition(pnd_Local_Data_Tbl_Wpid_Sbp.equals("CR")))                                                                                                      //Natural: IF TBL-WPID-SBP = 'CR'
            {
                pnd_Local_Data_Pnd_Cont_Rep_Ctr.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CONT-REP-CTR
            }                                                                                                                                                             //Natural: END-IF
            //* *********
            if (condition(pnd_Local_Data_Tbl_Status.equals("0200")))                                                                                                      //Natural: IF TBL-STATUS = '0200'
            {
                pnd_Local_Data_Pnd_Returned_Doc.nadd(1);                                                                                                                  //Natural: ADD 1 TO #RETURNED-DOC
            }                                                                                                                                                             //Natural: END-IF
            pnd_Local_Data_Pnd_Unclear_Ctr.reset();                                                                                                                       //Natural: RESET #UNCLEAR-CTR
            if (condition(pnd_Local_Data_Tbl_Wpid_Action.equals("U")))                                                                                                    //Natural: IF TBL-WPID-ACTION = 'U'
            {
                pnd_Local_Data_Pnd_Unclear_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #UNCLEAR-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Local_Data_Tbl_Wpid_Lob.equals("U")))                                                                                                       //Natural: IF TBL-WPID-LOB = 'U'
            {
                pnd_Local_Data_Pnd_Unclear_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #UNCLEAR-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Local_Data_Tbl_Wpid_Mbp.equals("U")))                                                                                                       //Natural: IF TBL-WPID-MBP = 'U'
            {
                pnd_Local_Data_Pnd_Unclear_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #UNCLEAR-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Local_Data_Tbl_Wpid_Sbp.equals("U")))                                                                                                       //Natural: IF TBL-WPID-SBP = 'U'
            {
                pnd_Local_Data_Pnd_Unclear_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #UNCLEAR-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Local_Data_Pnd_Unclear_Ctr.greater(getZero())))                                                                                             //Natural: IF #UNCLEAR-CTR GT 0
            {
                pnd_Local_Data_Pnd_Unclear_Ctr.reset();                                                                                                                   //Natural: RESET #UNCLEAR-CTR
                pnd_Local_Data_Pnd_Total_Unclear.nadd(1);                                                                                                                 //Natural: COMPUTE #TOTAL-UNCLEAR = #TOTAL-UNCLEAR + 1
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Icwn1105.class , getCurrentProcessState(), pnd_Local_Data_Tbl_Wpid, pnd_Local_Data_Pnd_Wpid_Desc);                                            //Natural: CALLNAT 'ICWN1105' TBL-WPID #WPID-DESC
            if (condition(Global.isEscape())) return;
            //*                                                                                                                                                           //Natural: AT BREAK OF TBL-WPID
            //*                                                                                                                                                           //Natural: AT BREAK OF TBL-RACF-ID;//Natural: AT END OF DATA
            //*  READ-2.
            sort01Tbl_WpidOld.setValue(pnd_Local_Data_Tbl_Wpid);                                                                                                          //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            sort01Tbl_WpidCount424.resetBreak();
            sort01Tbl_WpidCount.resetBreak();
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
            pnd_Local_Data_Pnd_Booklet_Ctr.reset();                                                                                                                       //Natural: RESET #BOOKLET-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANS-CTR #COMPLAINT-CTR #TOTAL-NEW #TOTAL-RETURNED-DOC #TOTAL-LOGGED #TOTAL-UNCLEAR #PAGE
            pnd_Local_Data_Pnd_Forms_Ctr.reset();
            pnd_Local_Data_Pnd_Inquire_Ctr.reset();
            pnd_Local_Data_Pnd_Research_Ctr.reset();
            pnd_Local_Data_Pnd_Trans_Ctr.reset();
            pnd_Local_Data_Pnd_Complaint_Ctr.reset();
            pnd_Local_Data_Pnd_Total_New.reset();
            pnd_Local_Data_Pnd_Total_Returned_Doc.reset();
            pnd_Local_Data_Pnd_Total_Logged.reset();
            pnd_Local_Data_Pnd_Total_Unclear.reset();
            pnd_Page.reset();
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        DbsUtil.invokeMain(DbsUtil.getBlType("ICWB3402"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'ICWB3402'
        if (condition(Global.isEscape())) return;
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(DbsUtil.maskMatches(pnd_Local_Data_Pnd_Work_Prcss_Id,"N.....")))                                                                                    //Natural: IF #WORK-PRCSS-ID = MASK ( N..... )
        {
            pnd_Local_Data_Tbl_Wpid_Act.setValue("Z");                                                                                                                    //Natural: MOVE 'Z' TO TBL-WPID-ACT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Local_Data_Tbl_Wpid_Act.setValue(pnd_Local_Data_Pnd_Work_Prcss_Id);                                                                                       //Natural: MOVE #WORK-PRCSS-ID TO TBL-WPID-ACT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Local_Data_Tbl_Wpid.setValue(pnd_Local_Data_Pnd_Work_Prcss_Id);                                                                                               //Natural: MOVE #WORK-PRCSS-ID TO TBL-WPID
        pnd_Local_Data_Tbl_Racf_Id.setValue(pnd_Oprtr_Cde);                                                                                                               //Natural: MOVE #OPRTR-CDE TO TBL-RACF-ID
        pnd_Local_Data_Tbl_Status.setValue(pnd_Local_Data_Pnd_Work_Status);                                                                                               //Natural: MOVE #WORK-STATUS TO TBL-STATUS
        getWorkFiles().write(1, false, pnd_Local_Data_Pnd_Work_Record);                                                                                                   //Natural: WRITE WORK FILE 1 #WORK-RECORD
        pnd_Reccount.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #RECCOUNT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page.nadd(1);                                                                                                                                     //Natural: COMPUTE #PAGE = #PAGE + 1
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Env,new TabSetting(25),"INSTITUTIONAL WORKFLOW FACILITIES",new                     //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 25T 'INSTITUTIONAL WORKFLOW FACILITIES' 72T 'PAGE' #PAGE ( NL = 5 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 25T 'WORK REQUESTS LOGGED AND INDEXED' 72T *TIMX ( EM = HH':'II' 'AP )
                        TabSetting(72),"PAGE",pnd_Page, new NumericLength (5), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), 
                        new ReportEditMask ("LLL' 'DD','YY"),new TabSetting(25),"WORK REQUESTS LOGGED AND INDEXED",new TabSetting(72),Global.getTIMX(), 
                        new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    if (condition(pnd_New_Racf.getBoolean()))                                                                                                             //Natural: IF #NEW-RACF
                    {
                        DbsUtil.callnat(Icwn1103.class , getCurrentProcessState(), pnd_Rep_Parm_Pnd_Rep_Unit_Cde, pnd_Unit_Name);                                         //Natural: CALLNAT 'ICWN1103' #REP-UNIT-CDE #UNIT-NAME
                        if (condition(Global.isEscape())) return;
                        getReports().write(1, ReportOption.NOTITLE,"UNIT      :",pnd_Rep_Parm_Pnd_Rep_Unit_Cde,pnd_Unit_Name);                                            //Natural: WRITE ( 1 ) 'UNIT      :' #REP-UNIT-CDE #UNIT-NAME
                        getReports().write(1, ReportOption.NOTITLE,"START-DATE:",pnd_Rep_Parm_Pnd_Work_Start_Date_A);                                                     //Natural: WRITE ( 1 ) 'START-DATE:' #WORK-START-DATE-A
                        getReports().write(1, ReportOption.NOTITLE,"END-DATE  :",pnd_Rep_Parm_Pnd_Work_End_Date_A);                                                       //Natural: WRITE ( 1 ) 'END-DATE  :' #WORK-END-DATE-A
                        if (condition(pnd_Local_Data_Pnd_Rep_Empl_Name.equals("NOT FOUND")))                                                                              //Natural: IF #REP-EMPL-NAME = 'NOT FOUND'
                        {
                            pnd_Local_Data_Pnd_Rep_Empl_Name.setValue(pnd_Rep_Parm_Pnd_Rep_Racf_Id);                                                                      //Natural: MOVE #REP-RACF-ID TO #REP-EMPL-NAME
                        }                                                                                                                                                 //Natural: END-IF
                        getReports().write(1, ReportOption.NOTITLE,"EMPLOYEE  :",pnd_Local_Data_Pnd_Rep_Empl_Name);                                                       //Natural: WRITE ( 1 ) 'EMPLOYEE  :' #REP-EMPL-NAME
                        pnd_New_Racf.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #NEW-RACF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,"EMPLOYEE  :",pnd_Local_Data_Pnd_Rep_Empl_Name,"(continued)");                                         //Natural: WRITE ( 1 ) 'EMPLOYEE  :' #REP-EMPL-NAME '(continued)'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(130));                                                                                  //Natural: WRITE ( 1 ) '=' ( 130 )
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    if (condition(! (pnd_Local_Data_Pnd_End_Of_Data.getBoolean())))                                                                                       //Natural: IF NOT #END-OF-DATA
                    {
                        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(62),"NEW       RETURNING TOTAL LOGGED",NEWLINE,new ColumnSpacing(10),"WPID",new      //Natural: WRITE ( 1 ) 62X 'NEW       RETURNING TOTAL LOGGED' / 10X 'WPID' 19X 'DESCRIPTION' 18X 'REQUESTS  DOCUMENTS REQUESTS ' / 9X '-' ( 6 ) '-' ( 45 ) '-' ( 9 ) '-' ( 9 ) '-' ( 9 )
                            ColumnSpacing(19),"DESCRIPTION",new ColumnSpacing(18),"REQUESTS  DOCUMENTS REQUESTS ",NEWLINE,new ColumnSpacing(9),"-",new RepeatItem(6),"-",new 
                            RepeatItem(45),"-",new RepeatItem(9),"-",new RepeatItem(9),"-",new RepeatItem(9));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Local_Data_Tbl_WpidIsBreak = pnd_Local_Data_Tbl_Wpid.isBreak(endOfData);
        boolean pnd_Local_Data_Tbl_Racf_IdIsBreak = pnd_Local_Data_Tbl_Racf_Id.isBreak(endOfData);
        if (condition(pnd_Local_Data_Tbl_WpidIsBreak || pnd_Local_Data_Tbl_Racf_IdIsBreak))
        {
            pnd_Local_Data_Pnd_Wpid_Code.setValue(sort01Tbl_WpidOld);                                                                                                     //Natural: MOVE OLD ( TBL-WPID ) TO #WPID-CODE
            pnd_Local_Data_Pnd_Wpid_Count.setValue(sort01Tbl_WpidCount424);                                                                                               //Natural: MOVE COUNT ( TBL-WPID ) TO #WPID-COUNT
            pnd_Local_Data_Pnd_Wpid_New.compute(new ComputeParameters(false, pnd_Local_Data_Pnd_Wpid_New), pnd_Local_Data_Pnd_Wpid_Count.subtract(pnd_Local_Data_Pnd_Returned_Doc)); //Natural: COMPUTE #WPID-NEW = #WPID-COUNT - #RETURNED-DOC
            pnd_Local_Data_Pnd_Total_New.nadd(pnd_Local_Data_Pnd_Wpid_New);                                                                                               //Natural: COMPUTE #TOTAL-NEW = #TOTAL-NEW + #WPID-NEW
            pnd_Local_Data_Pnd_Total_Returned_Doc.nadd(pnd_Local_Data_Pnd_Returned_Doc);                                                                                  //Natural: COMPUTE #TOTAL-RETURNED-DOC = #TOTAL-RETURNED-DOC + #RETURNED-DOC
            pnd_Local_Data_Pnd_Total_Logged.nadd(pnd_Local_Data_Pnd_Wpid_Count);                                                                                          //Natural: COMPUTE #TOTAL-LOGGED = #TOTAL-LOGGED + #WPID-COUNT
            getReports().write(1, ReportOption.NOTITLE,pnd_Local_Data_Pnd_Pad_Space,pnd_Local_Data_Pnd_Wpid_Code,pnd_Local_Data_Pnd_Wpid_Desc,pnd_Local_Data_Pnd_Wpid_New,  //Natural: WRITE ( 1 ) #PAD-SPACE #WPID-CODE #WPID-DESC #WPID-NEW ( EM = ZZ,ZZ9 ) '   ' #RETURNED-DOC ( EM = ZZ,ZZ9 ) '  ' #WPID-COUNT ( EM = ZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"),"   ",pnd_Local_Data_Pnd_Returned_Doc, new ReportEditMask ("ZZ,ZZ9"),"  ",pnd_Local_Data_Pnd_Wpid_Count, new 
                ReportEditMask ("ZZ,ZZ9"));
            if (condition(Global.isEscape())) return;
            pnd_Local_Data_Pnd_Returned_Doc.reset();                                                                                                                      //Natural: RESET #RETURNED-DOC
            sort01Tbl_WpidCount424.setDec(new DbsDecimal(0));                                                                                                             //Natural: END-BREAK
        }
        if (condition(pnd_Local_Data_Tbl_Racf_IdIsBreak))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(35),"TOTAL ",new TabSetting(63),pnd_Local_Data_Pnd_Total_New,"   ",pnd_Local_Data_Pnd_Total_Returned_Doc, //Natural: WRITE ( 1 ) NOTITLE / 35T 'TOTAL ' 63T #TOTAL-NEW '   ' #TOTAL-RETURNED-DOC '  ' #TOTAL-LOGGED /
                "  ",pnd_Local_Data_Pnd_Total_Logged,NEWLINE);
            if (condition(Global.isEscape())) return;
            //*      / 'NUMBER OF WORK REQUESTS LOGGED AND '
            //*      -         'INDEXED WITH "U" IN ANY ELEMENT:'
            //*      #TOTAL-UNCLEAR (EM=ZZ,ZZ9)
            pnd_Rep_Parm_Pnd_Rep_Racf_Id.setValue(pnd_Local_Data_Tbl_Racf_Id);                                                                                            //Natural: MOVE TBL-RACF-ID TO #REP-RACF-ID
            if (condition(getReports().getAstLineCount(1).greater(53)))                                                                                                   //Natural: IF *LINE-COUNT ( 1 ) GT 53
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(130),NEWLINE,NEWLINE,"Number of Special Remitter Requests:      ",pnd_Local_Data_Pnd_Spec_Rem_Ctr,NEWLINE,"Number of Annuity Contribution Statements:",pnd_Local_Data_Pnd_Annt_Con_Ctr,NEWLINE,"Number of Automated Remitters:            ",pnd_Local_Data_Pnd_Autm_Rem_Ctr,NEWLINE,"Number of Contribution Reporting Systems: ",pnd_Local_Data_Pnd_Cont_Rep_Ctr,NEWLINE,"*",new  //Natural: WRITE ( 1 ) NOTITLE / / '*' ( 130 ) // 'Number of Special Remitter Requests:      ' #SPEC-REM-CTR / 'Number of Annuity Contribution Statements:' #ANNT-CON-CTR / 'Number of Automated Remitters:            ' #AUTM-REM-CTR / 'Number of Contribution Reporting Systems: ' #CONT-REP-CTR / '*' ( 130 )
                RepeatItem(130));
            if (condition(Global.isEscape())) return;
            //*      'BOOKLETS  FORMS'
            //*      /
            //*      'REQUESTED REQUESTED INQUIRIES RESEARCH TRANSACTIONS COMPLAINTS'
            //*      - '  OTHERS   '
            //*      /
            //*      '--------- --------- --------- -------- ------------ ----------'
            //*      - '  ---------'
            //*      /
            //*      4T  #BOOKLET-CTR
            //*      14T  #FORMS-CTR
            //*      24T  #INQUIRE-CTR
            //*      33T  #RESEARCH-CTR
            //*      46T  #TRANS-CTR
            //*      57T  #COMPLAINT-CTR
            //*      68T  #OTHER-CTR
            //*      '(LOGGED)'
            //*      /
            //*      4T   #RBOOKLET-CTR
            //*      14T  #RFORMS-CTR
            //*      24T  #RINQUIRE-CTR
            //*      33T  #RRESEARCH-CTR
            //*      46T  #RTRANS-CTR
            //*      57T  #RCOMPLAINT-CTR
            //*      68T  #ROTHER-CTR
            //*      '(RETURNING DOCUMENTS)'
            pnd_Local_Data_Pnd_Booklet_Ctr.reset();                                                                                                                       //Natural: RESET #BOOKLET-CTR #SPEC-REM-CTR #ANNT-CON-CTR #AUTM-REM-CTR #CONT-REP-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANS-CTR #COMPLAINT-CTR #OTHER-CTR #TOTAL-NEW #TOTAL-RETURNED-DOC #TOTAL-LOGGED #TOTAL-UNCLEAR
            pnd_Local_Data_Pnd_Spec_Rem_Ctr.reset();
            pnd_Local_Data_Pnd_Annt_Con_Ctr.reset();
            pnd_Local_Data_Pnd_Autm_Rem_Ctr.reset();
            pnd_Local_Data_Pnd_Cont_Rep_Ctr.reset();
            pnd_Local_Data_Pnd_Forms_Ctr.reset();
            pnd_Local_Data_Pnd_Inquire_Ctr.reset();
            pnd_Local_Data_Pnd_Research_Ctr.reset();
            pnd_Local_Data_Pnd_Trans_Ctr.reset();
            pnd_Local_Data_Pnd_Complaint_Ctr.reset();
            pnd_Local_Data_Pnd_Other_Ctr.reset();
            pnd_Local_Data_Pnd_Total_New.reset();
            pnd_Local_Data_Pnd_Total_Returned_Doc.reset();
            pnd_Local_Data_Pnd_Total_Logged.reset();
            pnd_Local_Data_Pnd_Total_Unclear.reset();
            pnd_Local_Data_Pnd_Rbooklet_Ctr.reset();                                                                                                                      //Natural: RESET #RBOOKLET-CTR #RFORMS-CTR #RINQUIRE-CTR #RRESEARCH-CTR #RTRANS-CTR #RCOMPLAINT-CTR #ROTHER-CTR
            pnd_Local_Data_Pnd_Rforms_Ctr.reset();
            pnd_Local_Data_Pnd_Rinquire_Ctr.reset();
            pnd_Local_Data_Pnd_Rresearch_Ctr.reset();
            pnd_Local_Data_Pnd_Rtrans_Ctr.reset();
            pnd_Local_Data_Pnd_Rcomplaint_Ctr.reset();
            pnd_Local_Data_Pnd_Rother_Ctr.reset();
            pnd_New_Racf.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-RACF
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=58");
    }
}
