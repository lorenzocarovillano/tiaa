/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:51:48 PM
**        * FROM NATURAL PROGRAM : Iwfb4700
************************************************************
**        * FILE NAME            : Iwfb4700.java
**        * CLASS NAME           : Iwfb4700
**        * INSTANCE NAME        : Iwfb4700
************************************************************
***********************************************************************
* PROGRAM      : IWFB4700
* SYSTEM       : ENHANCED UNIT WORK LISTS
* DATE WRITTEN : 03/25/1999
* FUNCTION     : CWF-SCRATCH INITIAL LOAD
*              :
* HISTORY      : 07/16/99 JRF  CHECK FOR PREVIOUS RUN STATUS AND WAIT
*              :               TIME OVERRIDE
*              : 08/31/99 JRF  RESTOWED WITH NEW VERSION OF IWFA4702
*              : 09/09/99 JRF  FIX PROBLEM ON "on error" LOGIC
*              :
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iwfb4700 extends BLNatBase
{
    // Data Areas
    private LdaIwfl4800 ldaIwfl4800;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaIwfa4702 pdaIwfa4702;
    private PdaIwfa4703 pdaIwfa4703;
    private PdaCdaobj pdaCdaobj;
    private PdaErla1000 pdaErla1000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Org_Unit_Tbl;
    private DbsField cwf_Org_Unit_Tbl_Unit_Cde;
    private DbsField cwf_Org_Unit_Tbl_Team_Ind;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Unit_Cde;
    private DbsField pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Key_Field__R_Field_2;
    private DbsField pnd_Tbl_Key_Field_Pnd_Invrtd_Dte_Stamp;
    private DbsField pnd_Tbl_Key_Field_Pnd_Run_Type;
    private DbsField pnd_Tbl_Key_Field_Pnd_Invrtd_Tme_Stamp;
    private DbsField pnd_Tbl_Key_Field_Pnd_Key_Filler1;
    private DbsField pnd_Tbl_Key_Field_Pnd_Icw_Limit_Mins;
    private DbsField pnd_Tbl_Key_Field_Pnd_Key_Filler2;
    private DbsField pnd_Tbl_Key_Field_Pnd_Cwf_Limit_Mins;
    private DbsField pnd_Tbl_Key_Field_Pnd_Key_Filler3;
    private DbsField pnd_Tbl_Key_Field_Pnd_Ncw_Limit_Mins;
    private DbsField pnd_Tbl_Key_Field_Pnd_Key_Filler4;
    private DbsField pnd_Tbl_Key_Field_Pnd_Others;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_3;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Actve_Ind;
    private DbsField pnd_Invrtd_Dte_Tme_Hold;

    private DbsGroup pnd_Invrtd_Dte_Tme_Hold__R_Field_4;
    private DbsField pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Dte_Hold;
    private DbsField pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Tme_Hold;

    private DbsGroup pnd_Invrtd_Dte_Tme_Hold__R_Field_5;
    private DbsField pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Dte_Tme_Hold_A;
    private DbsField pnd_Loop_Fl;
    private DbsField pnd_Stat_Fl;
    private DbsField pnd_Refreshed;
    private DbsField pnd_Enhanced_Unit;
    private DbsField pnd_Last_Msg;
    private DbsField pnd_Last_Msg_Data;
    private DbsField pnd_Last_Return_Code;
    private DbsField pnd_Prev_Run_Status;
    private DbsField pnd_Prev_Run_Others;
    private DbsField pnd_Prev_Run_Others_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIwfl4800 = new LdaIwfl4800();
        registerRecord(ldaIwfl4800);
        registerRecord(ldaIwfl4800.getVw_cwf_Scratch_View());
        localVariables = new DbsRecord();
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaIwfa4702 = new PdaIwfa4702(localVariables);
        pdaIwfa4703 = new PdaIwfa4703(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaErla1000 = new PdaErla1000(localVariables);

        // Local Variables

        vw_cwf_Org_Unit_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Unit_Tbl", "CWF-ORG-UNIT-TBL"), "CWF_ORG_UNIT_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Unit_Tbl_Unit_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Org_Unit_Tbl_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Unit_Tbl_Team_Ind = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Team_Ind", "TEAM-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TEAM_IND");
        registerRecord(vw_cwf_Org_Unit_Tbl);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Unit_Cde = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Key_Field = localVariables.newFieldInRecord("pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 30);

        pnd_Tbl_Key_Field__R_Field_2 = localVariables.newGroupInRecord("pnd_Tbl_Key_Field__R_Field_2", "REDEFINE", pnd_Tbl_Key_Field);
        pnd_Tbl_Key_Field_Pnd_Invrtd_Dte_Stamp = pnd_Tbl_Key_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Key_Field_Pnd_Invrtd_Dte_Stamp", "#INVRTD-DTE-STAMP", 
            FieldType.STRING, 8);
        pnd_Tbl_Key_Field_Pnd_Run_Type = pnd_Tbl_Key_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Key_Field_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 
            1);
        pnd_Tbl_Key_Field_Pnd_Invrtd_Tme_Stamp = pnd_Tbl_Key_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Key_Field_Pnd_Invrtd_Tme_Stamp", "#INVRTD-TME-STAMP", 
            FieldType.STRING, 7);
        pnd_Tbl_Key_Field_Pnd_Key_Filler1 = pnd_Tbl_Key_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Key_Field_Pnd_Key_Filler1", "#KEY-FILLER1", FieldType.STRING, 
            1);
        pnd_Tbl_Key_Field_Pnd_Icw_Limit_Mins = pnd_Tbl_Key_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Key_Field_Pnd_Icw_Limit_Mins", "#ICW-LIMIT-MINS", 
            FieldType.NUMERIC, 2);
        pnd_Tbl_Key_Field_Pnd_Key_Filler2 = pnd_Tbl_Key_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Key_Field_Pnd_Key_Filler2", "#KEY-FILLER2", FieldType.STRING, 
            1);
        pnd_Tbl_Key_Field_Pnd_Cwf_Limit_Mins = pnd_Tbl_Key_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Key_Field_Pnd_Cwf_Limit_Mins", "#CWF-LIMIT-MINS", 
            FieldType.NUMERIC, 2);
        pnd_Tbl_Key_Field_Pnd_Key_Filler3 = pnd_Tbl_Key_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Key_Field_Pnd_Key_Filler3", "#KEY-FILLER3", FieldType.STRING, 
            1);
        pnd_Tbl_Key_Field_Pnd_Ncw_Limit_Mins = pnd_Tbl_Key_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Key_Field_Pnd_Ncw_Limit_Mins", "#NCW-LIMIT-MINS", 
            FieldType.NUMERIC, 2);
        pnd_Tbl_Key_Field_Pnd_Key_Filler4 = pnd_Tbl_Key_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Key_Field_Pnd_Key_Filler4", "#KEY-FILLER4", FieldType.STRING, 
            1);
        pnd_Tbl_Key_Field_Pnd_Others = pnd_Tbl_Key_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Key_Field_Pnd_Others", "#OTHERS", FieldType.NUMERIC, 2);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_3", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Invrtd_Dte_Tme_Hold = localVariables.newFieldInRecord("pnd_Invrtd_Dte_Tme_Hold", "#INVRTD-DTE-TME-HOLD", FieldType.NUMERIC, 15);

        pnd_Invrtd_Dte_Tme_Hold__R_Field_4 = localVariables.newGroupInRecord("pnd_Invrtd_Dte_Tme_Hold__R_Field_4", "REDEFINE", pnd_Invrtd_Dte_Tme_Hold);
        pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Dte_Hold = pnd_Invrtd_Dte_Tme_Hold__R_Field_4.newFieldInGroup("pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Dte_Hold", 
            "#INVRTD-DTE-HOLD", FieldType.STRING, 8);
        pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Tme_Hold = pnd_Invrtd_Dte_Tme_Hold__R_Field_4.newFieldInGroup("pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Tme_Hold", 
            "#INVRTD-TME-HOLD", FieldType.STRING, 7);

        pnd_Invrtd_Dte_Tme_Hold__R_Field_5 = localVariables.newGroupInRecord("pnd_Invrtd_Dte_Tme_Hold__R_Field_5", "REDEFINE", pnd_Invrtd_Dte_Tme_Hold);
        pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Dte_Tme_Hold_A = pnd_Invrtd_Dte_Tme_Hold__R_Field_5.newFieldInGroup("pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Dte_Tme_Hold_A", 
            "#INVRTD-DTE-TME-HOLD-A", FieldType.STRING, 15);
        pnd_Loop_Fl = localVariables.newFieldInRecord("pnd_Loop_Fl", "#LOOP-FL", FieldType.BOOLEAN, 1);
        pnd_Stat_Fl = localVariables.newFieldInRecord("pnd_Stat_Fl", "#STAT-FL", FieldType.BOOLEAN, 1);
        pnd_Refreshed = localVariables.newFieldInRecord("pnd_Refreshed", "#REFRESHED", FieldType.BOOLEAN, 1);
        pnd_Enhanced_Unit = localVariables.newFieldInRecord("pnd_Enhanced_Unit", "#ENHANCED-UNIT", FieldType.STRING, 8);
        pnd_Last_Msg = localVariables.newFieldInRecord("pnd_Last_Msg", "#LAST-MSG", FieldType.STRING, 79);
        pnd_Last_Msg_Data = localVariables.newFieldInRecord("pnd_Last_Msg_Data", "#LAST-MSG-DATA", FieldType.STRING, 8);
        pnd_Last_Return_Code = localVariables.newFieldInRecord("pnd_Last_Return_Code", "#LAST-RETURN-CODE", FieldType.STRING, 1);
        pnd_Prev_Run_Status = localVariables.newFieldInRecord("pnd_Prev_Run_Status", "#PREV-RUN-STATUS", FieldType.STRING, 1);
        pnd_Prev_Run_Others = localVariables.newFieldInRecord("pnd_Prev_Run_Others", "#PREV-RUN-OTHERS", FieldType.STRING, 2);
        pnd_Prev_Run_Others_N = localVariables.newFieldInRecord("pnd_Prev_Run_Others_N", "#PREV-RUN-OTHERS-N", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Org_Unit_Tbl.reset();
        vw_cwf_Support_Tbl.reset();

        ldaIwfl4800.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iwfb4700() throws Exception
    {
        super("Iwfb4700");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IWFB4700", onError);
        //*  ===================== START OF MAIN LOGIC ============================
        pnd_Last_Return_Code.reset();                                                                                                                                     //Natural: RESET #LAST-RETURN-CODE
        pnd_Loop_Fl.setValue(false);                                                                                                                                      //Natural: ASSIGN #LOOP-FL := FALSE
        pnd_Stat_Fl.setValue(false);                                                                                                                                      //Natural: ASSIGN #STAT-FL := FALSE
        PROG:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Loop_Fl.setValue(true);                                                                                                                                   //Natural: ASSIGN #LOOP-FL := TRUE
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(Global.getPROGRAM());                                                                     //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = *PROGRAM
            pnd_Last_Msg_Data.setValue(Global.getPROGRAM());
            //*  JF071699
                                                                                                                                                                          //Natural: PERFORM GET-PREV-RUN-STATUS
            sub_Get_Prev_Run_Status();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  SET UP START DATE AND TIME STAMP
                                                                                                                                                                          //Natural: PERFORM START-DTE-TME-STAMP
            sub_Start_Dte_Tme_Stamp();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CHECK IF SCRATCHPAD IS REFRESHED
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-REFRESHED
            sub_Check_If_Refreshed();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ==============================
            //*  PROCESS TEAM AND LIAISON UNITS
            //*  ==============================
            vw_cwf_Org_Unit_Tbl.startDatabaseRead                                                                                                                         //Natural: READ CWF-ORG-UNIT-TBL BY UNIQ-UNIT-CDE FROM 'LL'
            (
            "R1",
            new Wc[] { new Wc("UNIQ_UNIT_CDE", ">=", "LL", WcType.BY) },
            new Oc[] { new Oc("UNIQ_UNIT_CDE", "ASC") }
            );
            R1:
            while (condition(vw_cwf_Org_Unit_Tbl.readNextRow("R1")))
            {
                if (condition(cwf_Org_Unit_Tbl_Unit_Cde.getSubstring(1,2).compareTo("TM") > 0))                                                                           //Natural: IF SUBSTRING ( CWF-ORG-UNIT-TBL.UNIT-CDE,1,2 ) > 'TM'
                {
                    if (true) break R1;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R1. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                //*  TEAM UNIT
                //*  LIAISON UNIT
                if (condition(! (cwf_Org_Unit_Tbl_Team_Ind.equals("T") || cwf_Org_Unit_Tbl_Team_Ind.equals("L"))))                                                        //Natural: IF NOT ( CWF-ORG-UNIT-TBL.TEAM-IND = 'T' OR = 'L' )
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Enhanced_Unit.setValue(cwf_Org_Unit_Tbl_Unit_Cde);                                                                                                    //Natural: ASSIGN #ENHANCED-UNIT := CWF-ORG-UNIT-TBL.UNIT-CDE
                //*    ------------------------
                //*    LOAD ENHANCED WORK LISTS
                //*    ------------------------
                                                                                                                                                                          //Natural: PERFORM LOAD-PARTICIPANTS
                sub_Load_Participants();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM LOAD-INSTITUTIONAL
                sub_Load_Institutional();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM LOAD-NON-PARTICIPANTS
                sub_Load_Non_Participants();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  R1.
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ===========================
            //*  LOAD OTHER DESIGNATED UNITS
            //*  ===========================
                                                                                                                                                                          //Natural: PERFORM OTHER-DESIGNATED-UNITS-KEY
            sub_Other_Designated_Units_Key();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                          //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY FROM #TBL-PRIME-KEY
            (
            "R2",
            new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
            new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
            );
            R2:
            while (condition(vw_cwf_Support_Tbl.readNextRow("R2")))
            {
                if (condition(! (cwf_Support_Tbl_Tbl_Table_Nme.equals(pnd_Tbl_Prime_Key_Tbl_Table_Nme))))                                                                 //Natural: IF NOT CWF-SUPPORT-TBL.TBL-TABLE-NME = #TBL-PRIME-KEY.TBL-TABLE-NME
                {
                    if (true) break R2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R2. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Enhanced_Unit.setValue(cwf_Support_Tbl_Unit_Cde);                                                                                                     //Natural: ASSIGN #ENHANCED-UNIT := CWF-SUPPORT-TBL.UNIT-CDE
                //*    ------------------------
                //*    LOAD ENHANCED WORK LISTS
                //*    ------------------------
                                                                                                                                                                          //Natural: PERFORM LOAD-PARTICIPANTS
                sub_Load_Participants();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM LOAD-INSTITUTIONAL
                sub_Load_Institutional();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM LOAD-NON-PARTICIPANTS
                sub_Load_Non_Participants();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  R2.
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("IWFB4709");                                                                              //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'IWFB4709'
            pnd_Last_Msg_Data.setValue("IWFB4709");
                                                                                                                                                                          //Natural: PERFORM CREATE-SUMMARY-REC
            sub_Create_Summary_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
            sub_Check_For_Error();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break PROG;                                                                                                                                         //Natural: ESCAPE BOTTOM ( PROG. )
            //*  ----------------------------------------------------------------------
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXIT-PROG
            //*  PROG.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Loop_Fl.setValue(false);                                                                                                                                      //Natural: ASSIGN #LOOP-FL := FALSE
        //*  SET END DATE-TIME STAMP
                                                                                                                                                                          //Natural: PERFORM END-DTE-TME-STAMP
        sub_End_Dte_Tme_Stamp();
        if (condition(Global.isEscape())) {return;}
        //*  -------------------------
        //*  RETURN CODE 0004 IF ERROR
        //*  -------------------------
        if (condition(pnd_Last_Return_Code.equals("E")))                                                                                                                  //Natural: IF #LAST-RETURN-CODE = 'E'
        {
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 0004
        }                                                                                                                                                                 //Natural: END-IF
        //*  ======================================================================
        //*                          SUBROUTINES
        //*  ======================================================================
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-TBL
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OTHER-DESIGNATED-UNITS-KEY
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-PARTICIPANTS
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-INSTITUTIONAL
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-NON-PARTICIPANTS
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-SUMMARY-REC
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-OBJECT
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CWFA2100-WK-LST-RUNS-KEY
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-REFRESHED
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PREV-RUN-STATUS
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: START-DTE-TME-STAMP
        //*  ------------------------------------------------------
        //*  SET PERIODIC RUN LIMITS FOR EACH WORKFLOW - IN MINUTES
        //*  ------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-DTE-TME-STAMP
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-SUMMARY-ON-ERROR
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-ERROR
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOG-MSG
        //*  ERLA1000.ERR-LINE-NBR   := 0
        //*  -------------------------
        //*  ON NATURAL RUN TIME ERROR
        //*  -------------------------
        //*  ----------------------------------------------------------------------                                                                                       //Natural: ON ERROR
    }
    //*  JF090999
    private void sub_Exit_Prog() throws Exception                                                                                                                         //Natural: EXIT-PROG
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        if (condition(pnd_Loop_Fl.getBoolean()))                                                                                                                          //Natural: IF #LOOP-FL
        {
            Global.setEscape(true);                                                                                                                                       //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
            Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
            if (true) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Initialize_Tbl() throws Exception                                                                                                                    //Natural: INITIALIZE-TBL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  SUPPORT TABLE ENHANCED-WK-LST-RUNS
        pdaIwfa4702.getIwfa4702().reset();                                                                                                                                //Natural: RESET IWFA4702
        pdaIwfa4702.getIwfa4702_Pnd_Data2_Literal_Icw().setValue("ICW");                                                                                                  //Natural: ASSIGN IWFA4702.#DATA2-LITERAL-ICW := 'ICW'
        pdaIwfa4702.getIwfa4702_Pnd_Data3_Literal_Cwf().setValue("CWF");                                                                                                  //Natural: ASSIGN IWFA4702.#DATA3-LITERAL-CWF := 'CWF'
        pdaIwfa4702.getIwfa4702_Pnd_Data4_Literal_Ncw().setValue("NCW");                                                                                                  //Natural: ASSIGN IWFA4702.#DATA4-LITERAL-NCW := 'NCW'
        pdaIwfa4702.getIwfa4702_Pnd_Data2_Literal_Read().setValue("R");                                                                                                   //Natural: ASSIGN IWFA4702.#DATA2-LITERAL-READ = IWFA4702.#DATA3-LITERAL-READ = IWFA4702.#DATA4-LITERAL-READ = 'R'
        pdaIwfa4702.getIwfa4702_Pnd_Data3_Literal_Read().setValue("R");
        pdaIwfa4702.getIwfa4702_Pnd_Data4_Literal_Read().setValue("R");
        pdaIwfa4702.getIwfa4702_Pnd_Data2_Literal_Add().setValue("A");                                                                                                    //Natural: ASSIGN IWFA4702.#DATA2-LITERAL-ADD = IWFA4702.#DATA3-LITERAL-ADD = IWFA4702.#DATA4-LITERAL-ADD = 'A'
        pdaIwfa4702.getIwfa4702_Pnd_Data3_Literal_Add().setValue("A");
        pdaIwfa4702.getIwfa4702_Pnd_Data4_Literal_Add().setValue("A");
        pdaIwfa4702.getIwfa4702_Pnd_Data2_Literal_Updt().setValue("U");                                                                                                   //Natural: ASSIGN IWFA4702.#DATA2-LITERAL-UPDT = IWFA4702.#DATA3-LITERAL-UPDT = IWFA4702.#DATA4-LITERAL-UPDT = 'U'
        pdaIwfa4702.getIwfa4702_Pnd_Data3_Literal_Updt().setValue("U");
        pdaIwfa4702.getIwfa4702_Pnd_Data4_Literal_Updt().setValue("U");
        pdaIwfa4702.getIwfa4702_Pnd_Data2_Literal_Del().setValue("D");                                                                                                    //Natural: ASSIGN IWFA4702.#DATA2-LITERAL-DEL = IWFA4702.#DATA3-LITERAL-DEL = IWFA4702.#DATA4-LITERAL-DEL = 'D'
        pdaIwfa4702.getIwfa4702_Pnd_Data3_Literal_Del().setValue("D");
        pdaIwfa4702.getIwfa4702_Pnd_Data4_Literal_Del().setValue("D");
    }
    private void sub_Other_Designated_Units_Key() throws Exception                                                                                                        //Natural: OTHER-DESIGNATED-UNITS-KEY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                              //Natural: ASSIGN #TBL-PRIME-KEY.TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_Tbl_Table_Nme.setValue("OTHER-ENHANCED-UNIT");                                                                                                  //Natural: ASSIGN #TBL-PRIME-KEY.TBL-TABLE-NME := 'OTHER-ENHANCED-UNIT'
        pnd_Tbl_Prime_Key_Tbl_Key_Field.setValue(" ");                                                                                                                    //Natural: ASSIGN #TBL-PRIME-KEY.TBL-KEY-FIELD := ' '
        pnd_Tbl_Prime_Key_Tbl_Actve_Ind.setValue("A ");                                                                                                                   //Natural: ASSIGN #TBL-PRIME-KEY.TBL-ACTVE-IND := 'A '
    }
    private void sub_Load_Participants() throws Exception                                                                                                                 //Natural: LOAD-PARTICIPANTS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("IWFN4710");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'IWFN4710'
        pnd_Last_Msg_Data.setValue("IWFN4710");
        DbsUtil.callnat(Iwfn4710.class , getCurrentProcessState(), pnd_Tbl_Key_Field, pnd_Enhanced_Unit, pdaCwfpda_M.getMsg_Info_Sub());                                  //Natural: CALLNAT 'IWFN4710' #TBL-KEY-FIELD #ENHANCED-UNIT MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
        sub_Check_For_Error();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Load_Institutional() throws Exception                                                                                                                //Natural: LOAD-INSTITUTIONAL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("IWFN4711");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'IWFN4711'
        pnd_Last_Msg_Data.setValue("IWFN4711");
        DbsUtil.callnat(Iwfn4711.class , getCurrentProcessState(), pnd_Tbl_Key_Field, pnd_Enhanced_Unit, pdaCwfpda_M.getMsg_Info_Sub());                                  //Natural: CALLNAT 'IWFN4711' #TBL-KEY-FIELD #ENHANCED-UNIT MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
        sub_Check_For_Error();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Load_Non_Participants() throws Exception                                                                                                             //Natural: LOAD-NON-PARTICIPANTS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("IWFN4712");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'IWFN4712'
        pnd_Last_Msg_Data.setValue("IWFN4712");
        DbsUtil.callnat(Iwfn4712.class , getCurrentProcessState(), pnd_Tbl_Key_Field, pnd_Enhanced_Unit, pdaCwfpda_M.getMsg_Info_Sub());                                  //Natural: CALLNAT 'IWFN4712' #TBL-KEY-FIELD #ENHANCED-UNIT MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
        sub_Check_For_Error();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Create_Summary_Rec() throws Exception                                                                                                                //Natural: CREATE-SUMMARY-REC
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("IWFB4700|sub_Create_Summary_Rec");
        while(true)
        {
            try
            {
                //*  ----------------------------------------------------------------------
                //*  ======================
                //*  CREATE SUMMARY RECORDS
                //*  ======================
                Global.getSTACK().pushData(StackOption.TOP,pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Dte_Tme_Hold_A, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(),                  //Natural: STACK TOP DATA #INVRTD-DTE-TME-HOLD-A MSG-INFO-SUB
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr(), pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1), pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2), 
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3), pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code(), pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field(), 
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field_Index1(), pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field_Index2(), pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field_Index3());
                DbsUtil.invokeMain(DbsUtil.getBlType("IWFB4709"), getCurrentProcessState());                                                                              //Natural: FETCH RETURN 'IWFB4709'
                if (condition(Global.isEscape())) return;
                if (condition(Global.getSTACK().getDatacount().greater(getZero()), INPUT_1))                                                                              //Natural: IF *DATA > 0
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Dte_Tme_Hold_A,pdaCwfpda_M.getMsg_Info_Sub());                  //Natural: INPUT #INVRTD-DTE-TME-HOLD-A MSG-INFO-SUB
                }                                                                                                                                                         //Natural: END-IF
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Call_Object() throws Exception                                                                                                                       //Natural: CALL-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("CWFN2100");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'CWFN2100'
        pnd_Last_Msg_Data.setValue("CWFN2100");
        DbsUtil.callnat(Cwfn2100.class , getCurrentProcessState(), pdaIwfa4702.getIwfa4702(), pdaIwfa4702.getIwfa4702_Id(), pdaIwfa4703.getIwfa4703(),                    //Natural: CALLNAT 'CWFN2100' IWFA4702 IWFA4702-ID IWFA4703 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
        sub_Check_For_Error();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE") || pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE")))                                         //Natural: IF CDAOBJ.#FUNCTION = 'STORE' OR = 'UPDATE'
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Cwfa2100_Wk_Lst_Runs_Key() throws Exception                                                                                                          //Natural: CWFA2100-WK-LST-RUNS-KEY
    {
        if (BLNatReinput.isReinput()) return;

        pdaIwfa4702.getIwfa4702_Tbl_Scrty_Level_Ind().setValue("A");                                                                                                      //Natural: ASSIGN IWFA4702.TBL-SCRTY-LEVEL-IND := 'A'
        pdaIwfa4702.getIwfa4702_Tbl_Table_Nme().setValue("ENHANCED-WK-LST-RUNS");                                                                                         //Natural: ASSIGN IWFA4702.TBL-TABLE-NME := 'ENHANCED-WK-LST-RUNS'
        pdaIwfa4702.getIwfa4702_Tbl_Key_Field().setValue(pnd_Tbl_Key_Field);                                                                                              //Natural: ASSIGN IWFA4702.TBL-KEY-FIELD := #TBL-KEY-FIELD
        pdaIwfa4702.getIwfa4702_Tbl_Actve_Ind().setValue("A ");                                                                                                           //Natural: ASSIGN IWFA4702.TBL-ACTVE-IND := 'A '
    }
    private void sub_Check_If_Refreshed() throws Exception                                                                                                                //Natural: CHECK-IF-REFRESHED
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Refreshed.setValue(true);                                                                                                                                     //Natural: ASSIGN #REFRESHED := TRUE
        ldaIwfl4800.getVw_cwf_Scratch_View().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) CWF-SCRATCH-VIEW
        (
        "READ01",
        new Oc[] { new Oc("ISN", "ASC") },
        1
        );
        READ01:
        while (condition(ldaIwfl4800.getVw_cwf_Scratch_View().readNextRow("READ01")))
        {
            pnd_Refreshed.setValue(false);                                                                                                                                //Natural: ASSIGN #REFRESHED := FALSE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(! (pnd_Refreshed.getBoolean())))                                                                                                                    //Natural: IF NOT #REFRESHED
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Scratchpad is not refreshed");                                                                            //Natural: ASSIGN MSG-INFO-SUB.##MSG := 'Scratchpad is not refreshed'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM EXIT-PROG
            sub_Exit_Prog();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  JF071699
    private void sub_Get_Prev_Run_Status() throws Exception                                                                                                               //Natural: GET-PREV-RUN-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("IWFN4798");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = 'IWFN4798'
        pnd_Last_Msg_Data.setValue("IWFN4798");
        //*  GET LAST RUN STATS
        DbsUtil.callnat(Iwfn4798.class , getCurrentProcessState(), pdaIwfa4702.getIwfa4702(), pdaIwfa4702.getIwfa4702_Id(), pdaCwfpda_M.getMsg_Info_Sub());               //Natural: CALLNAT 'IWFN4798' IWFA4702 IWFA4702-ID MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        //*  IF FORCE TERMINATE
        if (condition(pdaIwfa4702.getIwfa4702_Pnd_Others().equals(99)))                                                                                                   //Natural: IF IWFA4702.#OTHERS = 99
        {
            pnd_Prev_Run_Others_N.setValue(pdaIwfa4702.getIwfa4702_Pnd_Others());                                                                                         //Natural: ASSIGN #PREV-RUN-OTHERS-N := IWFA4702.#OTHERS
            pnd_Prev_Run_Status.setValue(pdaIwfa4702.getIwfa4702_Pnd_Run_Status_Flag());                                                                                  //Natural: ASSIGN #PREV-RUN-STATUS := IWFA4702.#RUN-STATUS-FLAG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  JF072299
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERROR
            sub_Check_For_Error();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Prev_Run_Status.setValue(pdaIwfa4702.getIwfa4702_Pnd_Run_Status_Flag());                                                                                  //Natural: ASSIGN #PREV-RUN-STATUS := IWFA4702.#RUN-STATUS-FLAG
            pnd_Prev_Run_Others.setValueEdited(pdaIwfa4702.getIwfa4702_Pnd_Others(),new ReportEditMask("99"));                                                            //Natural: MOVE EDITED IWFA4702.#OTHERS ( EM = 99 ) TO #PREV-RUN-OTHERS
            if (condition(DbsUtil.is(pnd_Prev_Run_Others.getText(),"N2")))                                                                                                //Natural: IF #PREV-RUN-OTHERS IS ( N2 )
            {
                pnd_Prev_Run_Others_N.compute(new ComputeParameters(false, pnd_Prev_Run_Others_N), pnd_Prev_Run_Others.val());                                            //Natural: ASSIGN #PREV-RUN-OTHERS-N := VAL ( #PREV-RUN-OTHERS )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prev_Run_Others_N.setValue(0);                                                                                                                        //Natural: ASSIGN #PREV-RUN-OTHERS-N := 00
            }                                                                                                                                                             //Natural: END-IF
            //*  END JF072299
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Start_Dte_Tme_Stamp() throws Exception                                                                                                               //Natural: START-DTE-TME-STAMP
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TBL
        sub_Initialize_Tbl();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pdaIwfa4702.getIwfa4702_Pnd_Start_Dte_Tme_Stamp().setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                         //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO IWFA4702.#START-DTE-TME-STAMP
        //*  INITIAL LOAD
        pnd_Invrtd_Dte_Tme_Hold.compute(new ComputeParameters(false, pnd_Invrtd_Dte_Tme_Hold), new DbsDecimal("999999999999999").subtract(pdaIwfa4702.getIwfa4702_Pnd_Start_Dte_Tme_Stamp_N())); //Natural: COMPUTE #INVRTD-DTE-TME-HOLD = 999999999999999 - IWFA4702.#START-DTE-TME-STAMP-N
        pnd_Tbl_Key_Field_Pnd_Run_Type.setValue("I");                                                                                                                     //Natural: ASSIGN #TBL-KEY-FIELD.#RUN-TYPE := 'I'
        pnd_Tbl_Key_Field_Pnd_Invrtd_Dte_Stamp.setValue(pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Dte_Hold);                                                                     //Natural: ASSIGN #TBL-KEY-FIELD.#INVRTD-DTE-STAMP := #INVRTD-DTE-HOLD
        pnd_Tbl_Key_Field_Pnd_Invrtd_Tme_Stamp.setValue(pnd_Invrtd_Dte_Tme_Hold_Pnd_Invrtd_Tme_Hold);                                                                     //Natural: ASSIGN #TBL-KEY-FIELD.#INVRTD-TME-STAMP := #INVRTD-TME-HOLD
        pnd_Tbl_Key_Field_Pnd_Icw_Limit_Mins.setValue(5);                                                                                                                 //Natural: ASSIGN #TBL-KEY-FIELD.#ICW-LIMIT-MINS := 05
        pnd_Tbl_Key_Field_Pnd_Cwf_Limit_Mins.setValue(5);                                                                                                                 //Natural: ASSIGN #TBL-KEY-FIELD.#CWF-LIMIT-MINS := 05
        pnd_Tbl_Key_Field_Pnd_Ncw_Limit_Mins.setValue(5);                                                                                                                 //Natural: ASSIGN #TBL-KEY-FIELD.#NCW-LIMIT-MINS := 05
        //*  SET TO DEFAULT IF PREV JF071699
        //*  RUN WAIT TIME IS NEGATIVE OR
        //*  FORCED TERMINATED
        if (condition(pnd_Prev_Run_Others_N.equals(99) || pnd_Prev_Run_Others_N.less(getZero())))                                                                         //Natural: IF #PREV-RUN-OTHERS-N = 99 OR #PREV-RUN-OTHERS-N < 0
        {
            pnd_Tbl_Key_Field_Pnd_Others.setValue(0);                                                                                                                     //Natural: ASSIGN #TBL-KEY-FIELD.#OTHERS := 00
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tbl_Key_Field_Pnd_Others.setValue(pnd_Prev_Run_Others_N);                                                                                                 //Natural: ASSIGN #TBL-KEY-FIELD.#OTHERS := #PREV-RUN-OTHERS-N
            //*  IN PROGRESS JF071699
        }                                                                                                                                                                 //Natural: END-IF
        pdaIwfa4702.getIwfa4702_Pnd_Data2_Last_Updte().setValue(pdaIwfa4702.getIwfa4702_Pnd_Start_Dte_Tme_Stamp());                                                       //Natural: ASSIGN IWFA4702.#DATA2-LAST-UPDTE := IWFA4702.#START-DTE-TME-STAMP
        pdaIwfa4702.getIwfa4702_Pnd_Data3_Last_Updte().setValue(pdaIwfa4702.getIwfa4702_Pnd_Start_Dte_Tme_Stamp());                                                       //Natural: ASSIGN IWFA4702.#DATA3-LAST-UPDTE := IWFA4702.#START-DTE-TME-STAMP
        pdaIwfa4702.getIwfa4702_Pnd_Data4_Last_Updte().setValue(pdaIwfa4702.getIwfa4702_Pnd_Start_Dte_Tme_Stamp());                                                       //Natural: ASSIGN IWFA4702.#DATA4-LAST-UPDTE := IWFA4702.#START-DTE-TME-STAMP
        pdaIwfa4702.getIwfa4702_Pnd_Run_Status_Flag().setValue("P");                                                                                                      //Natural: ASSIGN IWFA4702.#RUN-STATUS-FLAG := 'P'
        //*  STORE KEY & INITIAL TIME-STAMP
                                                                                                                                                                          //Natural: PERFORM CWFA2100-WK-LST-RUNS-KEY
        sub_Cwfa2100_Wk_Lst_Runs_Key();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        //*  THERE's entry in support tbl
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
        sub_Call_Object();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Stat_Fl.setValue(true);                                                                                                                                       //Natural: ASSIGN #STAT-FL := TRUE
        //*  -----
        //*  CHECK IF OTHER JOB STILL RUNNING    JF071699
        //*  -----
        //*  OTHER JOB STILL RUNNING; ABORT RUN
        if (condition(pnd_Prev_Run_Status.equals("P")))                                                                                                                   //Natural: IF #PREV-RUN-STATUS = 'P'
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Other job still running");                                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG := 'Other job still running'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM EXIT-PROG
            sub_Exit_Prog();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_End_Dte_Tme_Stamp() throws Exception                                                                                                                 //Natural: END-DTE-TME-STAMP
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Last_Return_Code.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                                                 //Natural: ASSIGN #LAST-RETURN-CODE := MSG-INFO-SUB.##RETURN-CODE
        if (condition(! (pnd_Last_Return_Code.equals("E"))))                                                                                                              //Natural: IF NOT #LAST-RETURN-CODE = 'E'
        {
            pnd_Last_Msg.setValue("SUCCESSFUL RUN");                                                                                                                      //Natural: ASSIGN #LAST-MSG := 'SUCCESSFUL RUN'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Last_Msg.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                             //Natural: ASSIGN #LAST-MSG := MSG-INFO-SUB.##MSG
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET KEY
        //*  CHECK IF THERE IS START DTE
                                                                                                                                                                          //Natural: PERFORM CWFA2100-WK-LST-RUNS-KEY
        sub_Cwfa2100_Wk_Lst_Runs_Key();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("EXISTS");                                                                                                            //Natural: ASSIGN CDAOBJ.#FUNCTION := 'EXISTS'
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
        sub_Call_Object();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(pdaCdaobj.getCdaobj_Pnd_Exists().getBoolean()))                                                                                                     //Natural: IF CDAOBJ.#EXISTS
        {
            //*  SET KEY
            //*  GET RECORD
                                                                                                                                                                          //Natural: PERFORM CWFA2100-WK-LST-RUNS-KEY
            sub_Cwfa2100_Wk_Lst_Runs_Key();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                           //Natural: ASSIGN CDAOBJ.#FUNCTION := 'GET'
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
            sub_Call_Object();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pdaIwfa4702.getIwfa4702_Pnd_Run_Status_Flag().setValue(pnd_Last_Return_Code);                                                                                 //Natural: ASSIGN IWFA4702.#RUN-STATUS-FLAG := #LAST-RETURN-CODE
            pdaIwfa4702.getIwfa4702_Pnd_Run_Status_Desc().setValue(pnd_Last_Msg);                                                                                         //Natural: ASSIGN IWFA4702.#RUN-STATUS-DESC := #LAST-MSG
            //*  UPDATE END-TME-STAMP
            pdaIwfa4702.getIwfa4702_Pnd_End_Dte_Tme_Stamp().setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                       //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO IWFA4702.#END-DTE-TME-STAMP
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("UPDATE");                                                                                                        //Natural: ASSIGN CDAOBJ.#FUNCTION := 'UPDATE'
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
            sub_Call_Object();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  SAVE MSG TO
    //*  TEMP VARIABLES
    private void sub_Create_Summary_On_Error() throws Exception                                                                                                           //Natural: CREATE-SUMMARY-ON-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Last_Return_Code.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                                                 //Natural: ASSIGN #LAST-RETURN-CODE := MSG-INFO-SUB.##RETURN-CODE
        pnd_Last_Msg.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                                 //Natural: ASSIGN #LAST-MSG := MSG-INFO-SUB.##MSG
                                                                                                                                                                          //Natural: PERFORM CREATE-SUMMARY-REC
        sub_Create_Summary_Rec();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue(pnd_Last_Return_Code);                                                                                 //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := #LAST-RETURN-CODE
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(pnd_Last_Msg);                                                                                                 //Natural: ASSIGN MSG-INFO-SUB.##MSG := #LAST-MSG
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(pnd_Last_Msg_Data);                                                                           //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) := #LAST-MSG-DATA
    }
    private void sub_Check_For_Error() throws Exception                                                                                                                   //Natural: CHECK-FOR-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ")))                   //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E' OR MSG-INFO-SUB.##ERROR-FIELD > ' '
        {
            //*  IF DETAIL UPDATED AND ERROR NOT IN SUMMARY RUN, PROCESS SUMMARY
            //*  JF072799
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).greater(" ")))                                                                       //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 3 ) > ' '
            {
                pnd_Last_Msg_Data.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3));                                                                   //Natural: ASSIGN #LAST-MSG-DATA := MSG-INFO-SUB.##MSG-DATA ( 3 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Last_Msg_Data.equals("IWFB4709") || ! (pnd_Stat_Fl.getBoolean())))                                                                          //Natural: IF #LAST-MSG-DATA = 'IWFB4709' OR NOT #STAT-FL
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-SUMMARY-ON-ERROR
                sub_Create_Summary_On_Error();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM EXIT-PROG
            sub_Exit_Prog();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                        //Natural: RESET MSG-INFO-SUB
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(Global.getPROGRAM());                                                                     //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = #LAST-MSG-DATA = *PROGRAM
            pnd_Last_Msg_Data.setValue(Global.getPROGRAM());
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  JF090999
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).equals(" ")))                                                                            //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 3 ) = ' '
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue(pnd_Last_Msg_Data);                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) := #LAST-MSG-DATA
        }                                                                                                                                                                 //Natural: END-IF
        //*  JF072799
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3), pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg())); //Natural: COMPRESS MSG-INFO-SUB.##MSG-DATA ( 3 ) MSG-INFO-SUB.##MSG INTO MSG-INFO-SUB.##MSG
        //*  LOG ERROR
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
        sub_Log_Msg();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Log_Msg() throws Exception                                                                                                                           //Natural: LOG-MSG
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pdaErla1000.getErla1000().reset();                                                                                                                                //Natural: RESET ERLA1000
        pdaErla1000.getErla1000_Err_Nbr().setValue(Global.getERROR_NR());                                                                                                 //Natural: ASSIGN ERLA1000.ERR-NBR = *ERROR-NR
        pdaErla1000.getErla1000_Err_Line_Nbr().setValue(Global.getERROR_LINE());                                                                                          //Natural: ASSIGN ERLA1000.ERR-LINE-NBR = *ERROR-LINE
        pdaErla1000.getErla1000_Err_Pgm_Name().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3));                                                      //Natural: ASSIGN ERLA1000.ERR-PGM-NAME = MSG-INFO-SUB.##MSG-DATA ( 3 )
        pdaErla1000.getErla1000_Err_Notes().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                          //Natural: ASSIGN ERLA1000.ERR-NOTES = MSG-INFO-SUB.##MSG
        if (condition(Global.getERROR_NR().greater(getZero())))                                                                                                           //Natural: IF *ERROR-NR > 0
        {
            //*  EXECUTION TIME ERROR
            pdaErla1000.getErla1000_Err_Status_Cde().setValue("O");                                                                                                       //Natural: ASSIGN ERLA1000.ERR-STATUS-CDE = 'O'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  ERROR DETECTED BY PROGRAM
            pdaErla1000.getErla1000_Err_Status_Cde().setValue("U");                                                                                                       //Natural: ASSIGN ERLA1000.ERR-STATUS-CDE = 'U'
        }                                                                                                                                                                 //Natural: END-IF
        pdaErla1000.getErla1000_Err_Type_Cde().setValue("N");                                                                                                             //Natural: ASSIGN ERLA1000.ERR-TYPE-CDE = 'N'
        pdaErla1000.getErla1000_Err_User_Id().setValue(Global.getUSER());                                                                                                 //Natural: ASSIGN ERLA1000.ERR-USER-ID := *USER
        pdaErla1000.getErla1000_Err_Job_Name().setValue(Global.getINIT_PROGRAM());                                                                                        //Natural: ASSIGN ERLA1000.ERR-JOB-NAME := *INIT-PROGRAM
        DbsUtil.callnat(Erln1000.class , getCurrentProcessState(), pdaErla1000.getErla1000());                                                                            //Natural: CALLNAT 'ERLN1000' ERLA1000
        if (condition(Global.isEscape())) return;
        if (condition(pdaErla1000.getErla1000_Good_Update().getBoolean()))                                                                                                //Natural: IF ERLA1000.GOOD-UPDATE
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //*    IF MSG-INFO-SUB.##MSG-DATA(3) = ' '
        //*      MSG-INFO-SUB.##MSG-DATA(3) := #LAST-MSG-DATA
        //*    END-IF
        //*  JF072799
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("NATERR", Global.getERROR_NR(), "LINE", Global.getERROR_LINE()));                             //Natural: COMPRESS 'NATERR' *ERROR-NR 'LINE' *ERROR-LINE INTO MSG-INFO-SUB.##MSG
        if (condition(pnd_Stat_Fl.getBoolean()))                                                                                                                          //Natural: IF #STAT-FL
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-SUMMARY-ON-ERROR
            sub_Create_Summary_On_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  JF090999
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
        sub_Process_Error();
        if (condition(Global.isEscape())) {return;}
        //*  SET END DATE-TIME STAMP
                                                                                                                                                                          //Natural: PERFORM END-DTE-TME-STAMP
        sub_End_Dte_Tme_Stamp();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 0004
    };                                                                                                                                                                    //Natural: END-ERROR
}
