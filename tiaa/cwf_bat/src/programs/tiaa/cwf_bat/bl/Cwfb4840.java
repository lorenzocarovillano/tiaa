/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:35:36 PM
**        * FROM NATURAL PROGRAM : Cwfb4840
************************************************************
**        * FILE NAME            : Cwfb4840.java
**        * CLASS NAME           : Cwfb4840
**        * INSTANCE NAME        : Cwfb4840
************************************************************
**********************************************************************
* PROGRAM  : CWFB4840
* SYSTEM   : CRPCWF
* TITLE    : CORPORATE REPORTING EXTRACT FROM NCW (NON-PARTICIPANTS)
*          : SMART (SALES MANAGEMENT & ASSET RETENTION TRACKING)
* FUNCTION : THIS PROGRAM EXTRACT NCW RECORDS WITHIN "Window" PERIOD
*          : AND CREATE TWO FILES WHICH WILL CONTAIN WORK REQUESTS LEAD
*          : & CUSTOMER FILE FOR ORACLE REPORTING PURPOSES.
*
* MODIFICATIONS:
* NAME            DATE  DESCRIPTION
* --------------  ----- -----------
* GEORGE YEARWOOD 09/01 MODIFY TO BYPASS DOCUMENT FILE WHEN STATUS
*                       IS 4173 AS PER HSIU-MEI LIN.
* J.B. 04/2015 NAAD RETIREMENT - REMOVE OBSOLETE NAS CODE
* AUG 2016 - NPIRSUNSET
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
**********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4840 extends BLNatBase
{
    // Data Areas
    private PdaCwfa4825 pdaCwfa4825;
    private PdaNsta1245 pdaNsta1245;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaMdma2003 pdaMdma2003;
    private PdaMdma2001 pdaMdma2001;
    private PdaMdma2002 pdaMdma2002;
    private PdaNasa032 pdaNasa032;
    private LdaCwfl4827 ldaCwfl4827;
    private LdaCwfl4818 ldaCwfl4818;
    private LdaCwfl4815 ldaCwfl4815;
    private LdaCwfl4834 ldaCwfl4834;
    private LdaCwfl4835 ldaCwfl4835;
    private LdaCwfl4824 ldaCwfl4824;
    private LdaCwfl4829 ldaCwfl4829;
    private PdaCwfa1400 pdaCwfa1400;
    private PdaCwfa1401 pdaCwfa1401;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Dcmnt_Search_Key;

    private DbsGroup pnd_Dcmnt_Search_Key__R_Field_1;
    private DbsField pnd_Dcmnt_Search_Key_Pnd_Np_Pin;
    private DbsField pnd_Dcmnt_Search_Key_Pnd_Crte_Dte_Tme;
    private DbsField pnd_Dcmnt_Search_Key_Pnd_Cre_Sqnce_Nbr;

    private DataAccessProgramView vw_ncw_Efm_Document;
    private DbsField ncw_Efm_Document_Np_Pin;
    private DbsField ncw_Efm_Document_Crte_Dte_Tme;

    private DbsGroup ncw_Efm_Document_Dcmnt_Type;
    private DbsField ncw_Efm_Document_Dcmnt_Ctgry;
    private DbsField ncw_Efm_Document_Dcmnt_Sub_Ctgry;
    private DbsField ncw_Efm_Document_Dcmnt_Dtl;

    private DbsGroup pnd_Work_File_1;
    private DbsField pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme;

    private DataAccessProgramView vw_cwf_Sb_Ph;
    private DbsField cwf_Sb_Ph_Rcrd_Type;
    private DbsField cwf_Sb_Ph_Srvvr_Mit_Idntfr;
    private DbsField cwf_Sb_Ph_Dod_Not_Dte;

    private DbsGroup cwf_Sb_Ph__R_Field_2;
    private DbsField cwf_Sb_Ph_Dod_Not_Dte_A;
    private DbsField pnd_Owner_Prjct_Key;

    private DbsGroup pnd_Owner_Prjct_Key__R_Field_3;
    private DbsField pnd_Owner_Prjct_Key_Pnd_Owner_Unit;
    private DbsField pnd_Owner_Prjct_Key_Pnd_Prjct_Id;
    private DbsField pnd_Owner_Prjct_Key_Pnd_Actve_Ind;

    private DataAccessProgramView vw_cwf_Efm_Document;
    private DbsField cwf_Efm_Document_Cabinet_Id;

    private DbsGroup cwf_Efm_Document__R_Field_4;
    private DbsField cwf_Efm_Document_Pnd_P_Value;
    private DbsField cwf_Efm_Document_Pnd_Pin_Value;
    private DbsField cwf_Efm_Document_Tiaa_Rcvd_Dte;
    private DbsField cwf_Efm_Document_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Efm_Document__R_Field_5;
    private DbsField cwf_Efm_Document_Case_Ind;
    private DbsField cwf_Efm_Document_Originating_Work_Prcss_Id;
    private DbsField cwf_Efm_Document_Multi_Rqst_Ind;
    private DbsField cwf_Efm_Document_Sub_Rqst_Ind;
    private DbsField cwf_Efm_Document_Document_Direction_Ind;

    private DbsGroup cwf_Efm_Document_Document_Type;
    private DbsField cwf_Efm_Document_Document_Category;
    private DbsField cwf_Efm_Document_Document_Sub_Category;
    private DbsField cwf_Efm_Document_Document_Detail;
    private DbsField pnd_Document_Key;

    private DbsGroup pnd_Document_Key__R_Field_6;
    private DbsField pnd_Document_Key_Pnd_Cabinet_Id;

    private DbsGroup pnd_Document_Key__R_Field_7;
    private DbsField pnd_Document_Key_Pnd_P_Value;
    private DbsField pnd_Document_Key_Pnd_Pin_Value;
    private DbsField pnd_Document_Key_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst;

    private DbsGroup pnd_Document_Key__R_Field_8;
    private DbsField pnd_Document_Key_Pnd_Case_Ind;
    private DbsField pnd_Document_Key_Pnd_Originating_Work_Prcss_Id;
    private DbsField pnd_Document_Key_Pnd_Multi_Rqst_Ind;
    private DbsField pnd_Document_Key_Pnd_Sub_Rqst_Ind;
    private DbsField pnd_Document_Key_Pnd_Entry_Dte_Tme;
    private DbsField pnd_Doc_Type_Combo;

    private DbsGroup pnd_Doc_Type_Combo__R_Field_9;
    private DbsField pnd_Doc_Type_Combo_Pnd_Doc_Category;
    private DbsField pnd_Doc_Type_Combo_Pnd_Doc_Sub_Category;
    private DbsField pnd_Doc_Type_Combo_Pnd_Doc_Detail;
    private DbsField pnd_Tbl_Start_Dte_Tme_T;
    private DbsField pnd_Tbl_End_Dte_Tme_T;
    private DbsField pnd_Fix_Time;
    private DbsField pnd_Fix_Dte_Tme;

    private DbsGroup pnd_Fix_Dte_Tme__R_Field_10;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Dte;

    private DbsGroup pnd_Fix_Dte_Tme__R_Field_11;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Dte_Yyyy;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Mm;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Dd;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Tme;
    private DbsField pnd_Dte_Tme_Convert;

    private DbsGroup pnd_Dte_Tme_Convert__R_Field_12;
    private DbsField pnd_Dte_Tme_Convert_Pnd_Dte_Tme_Convert_Dte;
    private DbsField pnd_Npir_Birth_Dte;

    private DbsGroup pnd_Npir_Birth_Dte__R_Field_13;
    private DbsField pnd_Npir_Birth_Dte_Pnd_Npir_Birth_Yyyy;
    private DbsField pnd_Npir_Birth_Dte_Pnd_Npir_Birth_Mm;
    private DbsField pnd_Npir_Birth_Dte_Pnd_Npir_Birth_Dd;
    private DbsField pnd_Fix_Dte_10;

    private DbsGroup pnd_Fix_Dte_10__R_Field_14;
    private DbsField pnd_Fix_Dte_10_Pnd_Fix_Dte_6;
    private DbsField pnd_Fix_Dte_10_Pnd_Fix_Dte_Yyyy;
    private DbsField pnd_Fix_Dte;
    private DbsField pnd_Effctve_Dte;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Trnsctn_Dte;
    private DbsField pnd_Sec_Updte_Dte;
    private DbsField pnd_Date_Split;

    private DbsGroup pnd_Date_Split__R_Field_15;
    private DbsField pnd_Date_Split_Pnd_Date_Split_Dte;
    private DbsField pnd_Date_Split_Pnd_Date_Split_Tme;
    private DbsField pnd_Dob;
    private DbsField pnd_Dod_D;
    private DbsField pnd_Time;
    private DbsField pnd_Rqst_Log_Dte_Tme_Save;
    private DbsField pnd_Pin_Number_Save;
    private DbsField pnd_End_Dte_Tme;

    private DbsGroup pnd_End_Dte_Tme__R_Field_16;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Yyyymmdd;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Hhiisst;

    private DbsGroup pnd_End_Dte_Tme__R_Field_17;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde;

    private DbsGroup pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_18;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_1;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Recreate_Ind;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_2;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Prcssng_Type_Cde;
    private DbsField pnd_Start_Date_T;
    private DbsField pnd_End_Date_T;
    private DbsField pnd_Start_Date_D;
    private DbsField pnd_End_Date_D;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Pass_Tlc;
    private DbsField pnd_Pass_Dob;

    private DbsGroup pnd_Pass_Dob__R_Field_19;
    private DbsField pnd_Pass_Dob_Pnd_Pass_Dob_A;
    private DbsField pnd_Pass_Pin;
    private DbsField pnd_Pass_Name;
    private DbsField pnd_Pass_Ssn;
    private DbsField pnd_Pass_Found;
    private DbsField pnd_Found_One;
    private DbsField pnd_Pass_Unit;
    private DbsField pnd_Pass_Division;
    private DbsField pnd_Isn;
    private DbsField pnd_Atsign;
    private DbsField pnd_Base_Dte_Tme_D;
    private DbsField pnd_End_Dte_Tme_D;
    private DbsField pnd_Pin_Split;

    private DbsGroup pnd_Pin_Split__R_Field_20;
    private DbsField pnd_Pin_Split_Pnd_Pin_Split_1;
    private DbsField pnd_Time_T;
    private DbsField pnd_Last_Chnge_Dte_Tme_T;
    private DbsField pnd_Number_W_Decimals;
    private DbsField pnd_Number_W_Decimals_13;
    private DbsField pnd_Pin_Base_Key;

    private DbsGroup pnd_Pin_Base_Key__R_Field_21;
    private DbsField pnd_Pin_Base_Key_Pin_Nbr;
    private DbsField pnd_Actv_Unque_Key;

    private DbsGroup pnd_Actv_Unque_Key__R_Field_22;
    private DbsField pnd_Actv_Unque_Key_Rqst_Log_Dte_Tme;
    private DbsField pnd_Actv_Unque_Key_Actve_Ind;

    private DbsGroup corr_Cntrct_Payee_Key;
    private DbsField corr_Cntrct_Payee_Key_Pnd_Corr_Cntrct_Payee_Key;

    private DbsGroup corr_Cntrct_Payee_Key__R_Field_23;
    private DbsField corr_Cntrct_Payee_Key_Correspondence_Addrss_Ind;
    private DbsField corr_Cntrct_Payee_Key_Cntrct_Nmbr;
    private DbsField corr_Cntrct_Payee_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Cab_Key;

    private DbsGroup pnd_Cab_Key__R_Field_24;
    private DbsField pnd_Cab_Key_Pnd_Cabinet_Type;
    private DbsField pnd_Cab_Key_Pnd_Pin_Nbr;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_25;
    private DbsField pnd_Tbl_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Geo_Grp;

    private DbsGroup pnd_Geo_Grp__R_Field_26;
    private DbsField pnd_Geo_Grp_Pnd_State;
    private DbsField pnd_Geo_Grp_Pnd_Geo_Code;
    private DbsField pnd_Ph_Key;

    private DbsGroup pnd_Ph_Key__R_Field_27;
    private DbsField pnd_Ph_Key_Rcrd_Type;
    private DbsField pnd_Ph_Key_Srvvr_Mit_Idntfr;
    private DbsField pnd_Ncw_Read_Cntr;
    private DbsField pnd_Work_Read_Cntr;
    private DbsField pnd_Wr_Lead_Cntr;
    private DbsField pnd_Customr_Cntr;
    private DbsField pnd_No_Base_Rec;
    private DbsField pnd_Vac_Address;
    private DbsField pnd_No_Base_Rec_V;
    private DbsField pnd_No_State_In_Table;
    private DbsField pnd_Pass_Nfnd_Cntr;
    private DbsField pnd_Add;
    private DbsField pnd_Update;
    private DbsField pnd_Foreign_Address;
    private DbsField pnd_Activity_Type;
    private DbsField pnd_Pin_Nbr_Save;

    private DbsGroup pnd_Pin_Nbr_Save__R_Field_28;
    private DbsField pnd_Pin_Nbr_Save_Pnd_Pin_Nbr_Save_N;

    private DbsGroup pnd_Frgn_Geo_Pass;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_1;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_2;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_3;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_4;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_5;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_6;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_7;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_8;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Return_Cde;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Mail_Cde;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Cde;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_St_Cde1;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_St_Cde2;

    private DataAccessProgramView vw_cwf_Efm_Cabinet;
    private DbsField cwf_Efm_Cabinet_Cabinet_Id;

    private DbsGroup cwf_Efm_Cabinet__R_Field_29;
    private DbsField cwf_Efm_Cabinet_Pnd_Policy_Holder;
    private DbsField cwf_Efm_Cabinet_Pnd_Pin_Nbr;
    private DbsField cwf_Efm_Cabinet_Active_Ind;
    private DbsField cwf_Efm_Cabinet_Media_Ind;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa4825 = new PdaCwfa4825(localVariables);
        pdaNsta1245 = new PdaNsta1245(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaMdma2003 = new PdaMdma2003(localVariables);
        pdaMdma2001 = new PdaMdma2001(localVariables);
        pdaMdma2002 = new PdaMdma2002(localVariables);
        pdaNasa032 = new PdaNasa032(localVariables);
        ldaCwfl4827 = new LdaCwfl4827();
        registerRecord(ldaCwfl4827);
        registerRecord(ldaCwfl4827.getVw_ncw_Master());
        ldaCwfl4818 = new LdaCwfl4818();
        registerRecord(ldaCwfl4818);
        registerRecord(ldaCwfl4818.getVw_cwf_Route_Support_Tbl());
        ldaCwfl4815 = new LdaCwfl4815();
        registerRecord(ldaCwfl4815);
        registerRecord(ldaCwfl4815.getVw_cwf_Support_Tbl());
        ldaCwfl4834 = new LdaCwfl4834();
        registerRecord(ldaCwfl4834);
        ldaCwfl4835 = new LdaCwfl4835();
        registerRecord(ldaCwfl4835);
        ldaCwfl4824 = new LdaCwfl4824();
        registerRecord(ldaCwfl4824);
        ldaCwfl4829 = new LdaCwfl4829();
        registerRecord(ldaCwfl4829);
        registerRecord(ldaCwfl4829.getVw_cwf_Bsnss_Data_File());
        pdaCwfa1400 = new PdaCwfa1400(localVariables);
        pdaCwfa1401 = new PdaCwfa1401(localVariables);

        // Local Variables
        pnd_Dcmnt_Search_Key = localVariables.newFieldInRecord("pnd_Dcmnt_Search_Key", "#DCMNT-SEARCH-KEY", FieldType.STRING, 17);

        pnd_Dcmnt_Search_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Dcmnt_Search_Key__R_Field_1", "REDEFINE", pnd_Dcmnt_Search_Key);
        pnd_Dcmnt_Search_Key_Pnd_Np_Pin = pnd_Dcmnt_Search_Key__R_Field_1.newFieldInGroup("pnd_Dcmnt_Search_Key_Pnd_Np_Pin", "#NP-PIN", FieldType.STRING, 
            7);
        pnd_Dcmnt_Search_Key_Pnd_Crte_Dte_Tme = pnd_Dcmnt_Search_Key__R_Field_1.newFieldInGroup("pnd_Dcmnt_Search_Key_Pnd_Crte_Dte_Tme", "#CRTE-DTE-TME", 
            FieldType.TIME);
        pnd_Dcmnt_Search_Key_Pnd_Cre_Sqnce_Nbr = pnd_Dcmnt_Search_Key__R_Field_1.newFieldInGroup("pnd_Dcmnt_Search_Key_Pnd_Cre_Sqnce_Nbr", "#CRE-SQNCE-NBR", 
            FieldType.STRING, 3);

        vw_ncw_Efm_Document = new DataAccessProgramView(new NameInfo("vw_ncw_Efm_Document", "NCW-EFM-DOCUMENT"), "NCW_EFM_DOCUMENT", "NCW_EFM_DOCUMENT");
        ncw_Efm_Document_Np_Pin = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Efm_Document_Crte_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Dte_Tme", "CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRTE_DTE_TME");
        ncw_Efm_Document_Crte_Dte_Tme.setDdmHeader("CREATE/DATE AND TIME");

        ncw_Efm_Document_Dcmnt_Type = vw_ncw_Efm_Document.getRecord().newGroupInGroup("NCW_EFM_DOCUMENT_DCMNT_TYPE", "DCMNT-TYPE");
        ncw_Efm_Document_Dcmnt_Type.setDdmHeader("DOCUMENT/TYPE");
        ncw_Efm_Document_Dcmnt_Ctgry = ncw_Efm_Document_Dcmnt_Type.newFieldInGroup("ncw_Efm_Document_Dcmnt_Ctgry", "DCMNT-CTGRY", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "DCMNT_CTGRY");
        ncw_Efm_Document_Dcmnt_Ctgry.setDdmHeader("DOC/CAT");
        ncw_Efm_Document_Dcmnt_Sub_Ctgry = ncw_Efm_Document_Dcmnt_Type.newFieldInGroup("ncw_Efm_Document_Dcmnt_Sub_Ctgry", "DCMNT-SUB-CTGRY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DCMNT_SUB_CTGRY");
        ncw_Efm_Document_Dcmnt_Sub_Ctgry.setDdmHeader("DOC SUB-/CATEGORY");
        ncw_Efm_Document_Dcmnt_Dtl = ncw_Efm_Document_Dcmnt_Type.newFieldInGroup("ncw_Efm_Document_Dcmnt_Dtl", "DCMNT-DTL", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DCMNT_DTL");
        ncw_Efm_Document_Dcmnt_Dtl.setDdmHeader("DOCUMENT/DETAIL");
        registerRecord(vw_ncw_Efm_Document);

        pnd_Work_File_1 = localVariables.newGroupInRecord("pnd_Work_File_1", "#WORK-FILE-1");
        pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme = pnd_Work_File_1.newFieldInGroup("pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 
            15);

        vw_cwf_Sb_Ph = new DataAccessProgramView(new NameInfo("vw_cwf_Sb_Ph", "CWF-SB-PH"), "CWF_SB_PH", "CWF_SB_BENE");
        cwf_Sb_Ph_Rcrd_Type = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE");
        cwf_Sb_Ph_Rcrd_Type.setDdmHeader("RECORD/TYPE");
        cwf_Sb_Ph_Srvvr_Mit_Idntfr = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "SRVVR_MIT_IDNTFR");
        cwf_Sb_Ph_Srvvr_Mit_Idntfr.setDdmHeader("SURVIVOR MIT/IDENTIFIER");
        cwf_Sb_Ph_Dod_Not_Dte = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Dod_Not_Dte", "DOD-NOT-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "DOD_NOT_DTE");
        cwf_Sb_Ph_Dod_Not_Dte.setDdmHeader("DOD NOTIF/DATE");

        cwf_Sb_Ph__R_Field_2 = vw_cwf_Sb_Ph.getRecord().newGroupInGroup("cwf_Sb_Ph__R_Field_2", "REDEFINE", cwf_Sb_Ph_Dod_Not_Dte);
        cwf_Sb_Ph_Dod_Not_Dte_A = cwf_Sb_Ph__R_Field_2.newFieldInGroup("cwf_Sb_Ph_Dod_Not_Dte_A", "DOD-NOT-DTE-A", FieldType.STRING, 8);
        registerRecord(vw_cwf_Sb_Ph);

        pnd_Owner_Prjct_Key = localVariables.newFieldInRecord("pnd_Owner_Prjct_Key", "#OWNER-PRJCT-KEY", FieldType.STRING, 17);

        pnd_Owner_Prjct_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Owner_Prjct_Key__R_Field_3", "REDEFINE", pnd_Owner_Prjct_Key);
        pnd_Owner_Prjct_Key_Pnd_Owner_Unit = pnd_Owner_Prjct_Key__R_Field_3.newFieldInGroup("pnd_Owner_Prjct_Key_Pnd_Owner_Unit", "#OWNER-UNIT", FieldType.STRING, 
            8);
        pnd_Owner_Prjct_Key_Pnd_Prjct_Id = pnd_Owner_Prjct_Key__R_Field_3.newFieldInGroup("pnd_Owner_Prjct_Key_Pnd_Prjct_Id", "#PRJCT-ID", FieldType.STRING, 
            8);
        pnd_Owner_Prjct_Key_Pnd_Actve_Ind = pnd_Owner_Prjct_Key__R_Field_3.newFieldInGroup("pnd_Owner_Prjct_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 
            1);

        vw_cwf_Efm_Document = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Document", "CWF-EFM-DOCUMENT"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Efm_Document_Cabinet_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Document_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Document__R_Field_4 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_4", "REDEFINE", cwf_Efm_Document_Cabinet_Id);
        cwf_Efm_Document_Pnd_P_Value = cwf_Efm_Document__R_Field_4.newFieldInGroup("cwf_Efm_Document_Pnd_P_Value", "#P-VALUE", FieldType.STRING, 1);
        cwf_Efm_Document_Pnd_Pin_Value = cwf_Efm_Document__R_Field_4.newFieldInGroup("cwf_Efm_Document_Pnd_Pin_Value", "#PIN-VALUE", FieldType.STRING, 
            12);
        cwf_Efm_Document_Tiaa_Rcvd_Dte = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Efm_Document_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Efm_Document_Case_Workprcss_Multi_Subrqst = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Case_Workprcss_Multi_Subrqst", 
            "CASE-WORKPRCSS-MULTI-SUBRQST", FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Efm_Document_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Efm_Document__R_Field_5 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_5", "REDEFINE", cwf_Efm_Document_Case_Workprcss_Multi_Subrqst);
        cwf_Efm_Document_Case_Ind = cwf_Efm_Document__R_Field_5.newFieldInGroup("cwf_Efm_Document_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        cwf_Efm_Document_Originating_Work_Prcss_Id = cwf_Efm_Document__R_Field_5.newFieldInGroup("cwf_Efm_Document_Originating_Work_Prcss_Id", "ORIGINATING-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Efm_Document_Multi_Rqst_Ind = cwf_Efm_Document__R_Field_5.newFieldInGroup("cwf_Efm_Document_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 
            1);
        cwf_Efm_Document_Sub_Rqst_Ind = cwf_Efm_Document__R_Field_5.newFieldInGroup("cwf_Efm_Document_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 
            1);
        cwf_Efm_Document_Document_Direction_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Document_Direction_Ind", "DOCUMENT-DIRECTION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_DIRECTION_IND");
        cwf_Efm_Document_Document_Direction_Ind.setDdmHeader("DOC/DIR");

        cwf_Efm_Document_Document_Type = vw_cwf_Efm_Document.getRecord().newGroupInGroup("CWF_EFM_DOCUMENT_DOCUMENT_TYPE", "DOCUMENT-TYPE");
        cwf_Efm_Document_Document_Type.setDdmHeader("DOCUMENT/TYPE");
        cwf_Efm_Document_Document_Category = cwf_Efm_Document_Document_Type.newFieldInGroup("cwf_Efm_Document_Document_Category", "DOCUMENT-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_CATEGORY");
        cwf_Efm_Document_Document_Category.setDdmHeader("DOC/CAT");
        cwf_Efm_Document_Document_Sub_Category = cwf_Efm_Document_Document_Type.newFieldInGroup("cwf_Efm_Document_Document_Sub_Category", "DOCUMENT-SUB-CATEGORY", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "DOCUMENT_SUB_CATEGORY");
        cwf_Efm_Document_Document_Sub_Category.setDdmHeader("DOC SUB-/CATEGORY");
        cwf_Efm_Document_Document_Detail = cwf_Efm_Document_Document_Type.newFieldInGroup("cwf_Efm_Document_Document_Detail", "DOCUMENT-DETAIL", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "DOCUMENT_DETAIL");
        cwf_Efm_Document_Document_Detail.setDdmHeader("DOCUMENT/DETAIL");
        registerRecord(vw_cwf_Efm_Document);

        pnd_Document_Key = localVariables.newFieldInRecord("pnd_Document_Key", "#DOCUMENT-KEY", FieldType.STRING, 34);

        pnd_Document_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Document_Key__R_Field_6", "REDEFINE", pnd_Document_Key);
        pnd_Document_Key_Pnd_Cabinet_Id = pnd_Document_Key__R_Field_6.newFieldInGroup("pnd_Document_Key_Pnd_Cabinet_Id", "#CABINET-ID", FieldType.STRING, 
            14);

        pnd_Document_Key__R_Field_7 = pnd_Document_Key__R_Field_6.newGroupInGroup("pnd_Document_Key__R_Field_7", "REDEFINE", pnd_Document_Key_Pnd_Cabinet_Id);
        pnd_Document_Key_Pnd_P_Value = pnd_Document_Key__R_Field_7.newFieldInGroup("pnd_Document_Key_Pnd_P_Value", "#P-VALUE", FieldType.STRING, 1);
        pnd_Document_Key_Pnd_Pin_Value = pnd_Document_Key__R_Field_7.newFieldInGroup("pnd_Document_Key_Pnd_Pin_Value", "#PIN-VALUE", FieldType.STRING, 
            12);
        pnd_Document_Key_Pnd_Tiaa_Rcvd_Dte = pnd_Document_Key__R_Field_6.newFieldInGroup("pnd_Document_Key_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst = pnd_Document_Key__R_Field_6.newFieldInGroup("pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst", 
            "#CASE-WORKPRCSS-MULTI-SUBRQST", FieldType.STRING, 9);

        pnd_Document_Key__R_Field_8 = pnd_Document_Key__R_Field_6.newGroupInGroup("pnd_Document_Key__R_Field_8", "REDEFINE", pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst);
        pnd_Document_Key_Pnd_Case_Ind = pnd_Document_Key__R_Field_8.newFieldInGroup("pnd_Document_Key_Pnd_Case_Ind", "#CASE-IND", FieldType.STRING, 1);
        pnd_Document_Key_Pnd_Originating_Work_Prcss_Id = pnd_Document_Key__R_Field_8.newFieldInGroup("pnd_Document_Key_Pnd_Originating_Work_Prcss_Id", 
            "#ORIGINATING-WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Document_Key_Pnd_Multi_Rqst_Ind = pnd_Document_Key__R_Field_8.newFieldInGroup("pnd_Document_Key_Pnd_Multi_Rqst_Ind", "#MULTI-RQST-IND", FieldType.STRING, 
            1);
        pnd_Document_Key_Pnd_Sub_Rqst_Ind = pnd_Document_Key__R_Field_8.newFieldInGroup("pnd_Document_Key_Pnd_Sub_Rqst_Ind", "#SUB-RQST-IND", FieldType.STRING, 
            1);
        pnd_Document_Key_Pnd_Entry_Dte_Tme = pnd_Document_Key__R_Field_6.newFieldInGroup("pnd_Document_Key_Pnd_Entry_Dte_Tme", "#ENTRY-DTE-TME", FieldType.TIME);
        pnd_Doc_Type_Combo = localVariables.newFieldInRecord("pnd_Doc_Type_Combo", "#DOC-TYPE-COMBO", FieldType.STRING, 8);

        pnd_Doc_Type_Combo__R_Field_9 = localVariables.newGroupInRecord("pnd_Doc_Type_Combo__R_Field_9", "REDEFINE", pnd_Doc_Type_Combo);
        pnd_Doc_Type_Combo_Pnd_Doc_Category = pnd_Doc_Type_Combo__R_Field_9.newFieldInGroup("pnd_Doc_Type_Combo_Pnd_Doc_Category", "#DOC-CATEGORY", FieldType.STRING, 
            1);
        pnd_Doc_Type_Combo_Pnd_Doc_Sub_Category = pnd_Doc_Type_Combo__R_Field_9.newFieldInGroup("pnd_Doc_Type_Combo_Pnd_Doc_Sub_Category", "#DOC-SUB-CATEGORY", 
            FieldType.STRING, 3);
        pnd_Doc_Type_Combo_Pnd_Doc_Detail = pnd_Doc_Type_Combo__R_Field_9.newFieldInGroup("pnd_Doc_Type_Combo_Pnd_Doc_Detail", "#DOC-DETAIL", FieldType.STRING, 
            4);
        pnd_Tbl_Start_Dte_Tme_T = localVariables.newFieldInRecord("pnd_Tbl_Start_Dte_Tme_T", "#TBL-START-DTE-TME-T", FieldType.TIME);
        pnd_Tbl_End_Dte_Tme_T = localVariables.newFieldInRecord("pnd_Tbl_End_Dte_Tme_T", "#TBL-END-DTE-TME-T", FieldType.TIME);
        pnd_Fix_Time = localVariables.newFieldInRecord("pnd_Fix_Time", "#FIX-TIME", FieldType.STRING, 15);
        pnd_Fix_Dte_Tme = localVariables.newFieldInRecord("pnd_Fix_Dte_Tme", "#FIX-DTE-TME", FieldType.STRING, 15);

        pnd_Fix_Dte_Tme__R_Field_10 = localVariables.newGroupInRecord("pnd_Fix_Dte_Tme__R_Field_10", "REDEFINE", pnd_Fix_Dte_Tme);
        pnd_Fix_Dte_Tme_Pnd_Dte = pnd_Fix_Dte_Tme__R_Field_10.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Dte", "#DTE", FieldType.STRING, 8);

        pnd_Fix_Dte_Tme__R_Field_11 = pnd_Fix_Dte_Tme__R_Field_10.newGroupInGroup("pnd_Fix_Dte_Tme__R_Field_11", "REDEFINE", pnd_Fix_Dte_Tme_Pnd_Dte);
        pnd_Fix_Dte_Tme_Pnd_Dte_Yyyy = pnd_Fix_Dte_Tme__R_Field_11.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Dte_Yyyy", "#DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Fix_Dte_Tme_Pnd_Mm = pnd_Fix_Dte_Tme__R_Field_11.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Fix_Dte_Tme_Pnd_Dd = pnd_Fix_Dte_Tme__R_Field_11.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Fix_Dte_Tme_Pnd_Tme = pnd_Fix_Dte_Tme__R_Field_10.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Tme", "#TME", FieldType.STRING, 7);
        pnd_Dte_Tme_Convert = localVariables.newFieldInRecord("pnd_Dte_Tme_Convert", "#DTE-TME-CONVERT", FieldType.STRING, 15);

        pnd_Dte_Tme_Convert__R_Field_12 = localVariables.newGroupInRecord("pnd_Dte_Tme_Convert__R_Field_12", "REDEFINE", pnd_Dte_Tme_Convert);
        pnd_Dte_Tme_Convert_Pnd_Dte_Tme_Convert_Dte = pnd_Dte_Tme_Convert__R_Field_12.newFieldInGroup("pnd_Dte_Tme_Convert_Pnd_Dte_Tme_Convert_Dte", "#DTE-TME-CONVERT-DTE", 
            FieldType.STRING, 8);
        pnd_Npir_Birth_Dte = localVariables.newFieldInRecord("pnd_Npir_Birth_Dte", "#NPIR-BIRTH-DTE", FieldType.STRING, 8);

        pnd_Npir_Birth_Dte__R_Field_13 = localVariables.newGroupInRecord("pnd_Npir_Birth_Dte__R_Field_13", "REDEFINE", pnd_Npir_Birth_Dte);
        pnd_Npir_Birth_Dte_Pnd_Npir_Birth_Yyyy = pnd_Npir_Birth_Dte__R_Field_13.newFieldInGroup("pnd_Npir_Birth_Dte_Pnd_Npir_Birth_Yyyy", "#NPIR-BIRTH-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Npir_Birth_Dte_Pnd_Npir_Birth_Mm = pnd_Npir_Birth_Dte__R_Field_13.newFieldInGroup("pnd_Npir_Birth_Dte_Pnd_Npir_Birth_Mm", "#NPIR-BIRTH-MM", 
            FieldType.STRING, 2);
        pnd_Npir_Birth_Dte_Pnd_Npir_Birth_Dd = pnd_Npir_Birth_Dte__R_Field_13.newFieldInGroup("pnd_Npir_Birth_Dte_Pnd_Npir_Birth_Dd", "#NPIR-BIRTH-DD", 
            FieldType.STRING, 2);
        pnd_Fix_Dte_10 = localVariables.newFieldInRecord("pnd_Fix_Dte_10", "#FIX-DTE-10", FieldType.STRING, 10);

        pnd_Fix_Dte_10__R_Field_14 = localVariables.newGroupInRecord("pnd_Fix_Dte_10__R_Field_14", "REDEFINE", pnd_Fix_Dte_10);
        pnd_Fix_Dte_10_Pnd_Fix_Dte_6 = pnd_Fix_Dte_10__R_Field_14.newFieldInGroup("pnd_Fix_Dte_10_Pnd_Fix_Dte_6", "#FIX-DTE-6", FieldType.STRING, 6);
        pnd_Fix_Dte_10_Pnd_Fix_Dte_Yyyy = pnd_Fix_Dte_10__R_Field_14.newFieldInGroup("pnd_Fix_Dte_10_Pnd_Fix_Dte_Yyyy", "#FIX-DTE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Fix_Dte = localVariables.newFieldInRecord("pnd_Fix_Dte", "#FIX-DTE", FieldType.STRING, 8);
        pnd_Effctve_Dte = localVariables.newFieldInRecord("pnd_Effctve_Dte", "#EFFCTVE-DTE", FieldType.STRING, 8);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 8);
        pnd_Trnsctn_Dte = localVariables.newFieldInRecord("pnd_Trnsctn_Dte", "#TRNSCTN-DTE", FieldType.STRING, 8);
        pnd_Sec_Updte_Dte = localVariables.newFieldInRecord("pnd_Sec_Updte_Dte", "#SEC-UPDTE-DTE", FieldType.STRING, 8);
        pnd_Date_Split = localVariables.newFieldInRecord("pnd_Date_Split", "#DATE-SPLIT", FieldType.STRING, 15);

        pnd_Date_Split__R_Field_15 = localVariables.newGroupInRecord("pnd_Date_Split__R_Field_15", "REDEFINE", pnd_Date_Split);
        pnd_Date_Split_Pnd_Date_Split_Dte = pnd_Date_Split__R_Field_15.newFieldInGroup("pnd_Date_Split_Pnd_Date_Split_Dte", "#DATE-SPLIT-DTE", FieldType.STRING, 
            8);
        pnd_Date_Split_Pnd_Date_Split_Tme = pnd_Date_Split__R_Field_15.newFieldInGroup("pnd_Date_Split_Pnd_Date_Split_Tme", "#DATE-SPLIT-TME", FieldType.STRING, 
            7);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.DATE);
        pnd_Dod_D = localVariables.newFieldInRecord("pnd_Dod_D", "#DOD-D", FieldType.DATE);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Rqst_Log_Dte_Tme_Save = localVariables.newFieldInRecord("pnd_Rqst_Log_Dte_Tme_Save", "#RQST-LOG-DTE-TME-SAVE", FieldType.STRING, 15);
        pnd_Pin_Number_Save = localVariables.newFieldInRecord("pnd_Pin_Number_Save", "#PIN-NUMBER-SAVE", FieldType.STRING, 7);
        pnd_End_Dte_Tme = localVariables.newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.STRING, 15);

        pnd_End_Dte_Tme__R_Field_16 = localVariables.newGroupInRecord("pnd_End_Dte_Tme__R_Field_16", "REDEFINE", pnd_End_Dte_Tme);
        pnd_End_Dte_Tme_Pnd_End_Yyyymmdd = pnd_End_Dte_Tme__R_Field_16.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Yyyymmdd", "#END-YYYYMMDD", FieldType.STRING, 
            8);
        pnd_End_Dte_Tme_Pnd_End_Hhiisst = pnd_End_Dte_Tme__R_Field_16.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Hhiisst", "#END-HHIISST", FieldType.STRING, 
            7);

        pnd_End_Dte_Tme__R_Field_17 = localVariables.newGroupInRecord("pnd_End_Dte_Tme__R_Field_17", "REDEFINE", pnd_End_Dte_Tme);
        pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N = pnd_End_Dte_Tme__R_Field_17.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N", "#END-DTE-TME-N", FieldType.NUMERIC, 
            15);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde = localVariables.newFieldInRecord("pnd_Conversion_Due_Dte_Chg_Prty_Cde", "#CONVERSION-DUE-DTE-CHG-PRTY-CDE", 
            FieldType.STRING, 16);

        pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_18 = localVariables.newGroupInRecord("pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_18", "REDEFINE", 
            pnd_Conversion_Due_Dte_Chg_Prty_Cde);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_1 = pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_18.newFieldInGroup("pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_1", 
            "#CONVERSION-FILL-1", FieldType.STRING, 11);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Recreate_Ind = pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_18.newFieldInGroup("pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Recreate_Ind", 
            "#RECREATE-IND", FieldType.STRING, 1);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_2 = pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_18.newFieldInGroup("pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_2", 
            "#CONVERSION-FILL-2", FieldType.STRING, 3);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Prcssng_Type_Cde = pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_18.newFieldInGroup("pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Prcssng_Type_Cde", 
            "#PRCSSNG-TYPE-CDE", FieldType.STRING, 1);
        pnd_Start_Date_T = localVariables.newFieldInRecord("pnd_Start_Date_T", "#START-DATE-T", FieldType.TIME);
        pnd_End_Date_T = localVariables.newFieldInRecord("pnd_End_Date_T", "#END-DATE-T", FieldType.TIME);
        pnd_Start_Date_D = localVariables.newFieldInRecord("pnd_Start_Date_D", "#START-DATE-D", FieldType.DATE);
        pnd_End_Date_D = localVariables.newFieldInRecord("pnd_End_Date_D", "#END-DATE-D", FieldType.DATE);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 132);
        pnd_Pass_Tlc = localVariables.newFieldInRecord("pnd_Pass_Tlc", "#PASS-TLC", FieldType.STRING, 7);
        pnd_Pass_Dob = localVariables.newFieldInRecord("pnd_Pass_Dob", "#PASS-DOB", FieldType.NUMERIC, 8);

        pnd_Pass_Dob__R_Field_19 = localVariables.newGroupInRecord("pnd_Pass_Dob__R_Field_19", "REDEFINE", pnd_Pass_Dob);
        pnd_Pass_Dob_Pnd_Pass_Dob_A = pnd_Pass_Dob__R_Field_19.newFieldInGroup("pnd_Pass_Dob_Pnd_Pass_Dob_A", "#PASS-DOB-A", FieldType.STRING, 8);
        pnd_Pass_Pin = localVariables.newFieldInRecord("pnd_Pass_Pin", "#PASS-PIN", FieldType.STRING, 7);
        pnd_Pass_Name = localVariables.newFieldInRecord("pnd_Pass_Name", "#PASS-NAME", FieldType.STRING, 40);
        pnd_Pass_Ssn = localVariables.newFieldInRecord("pnd_Pass_Ssn", "#PASS-SSN", FieldType.NUMERIC, 9);
        pnd_Pass_Found = localVariables.newFieldInRecord("pnd_Pass_Found", "#PASS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Found_One = localVariables.newFieldInRecord("pnd_Found_One", "#FOUND-ONE", FieldType.BOOLEAN, 1);
        pnd_Pass_Unit = localVariables.newFieldInRecord("pnd_Pass_Unit", "#PASS-UNIT", FieldType.STRING, 8);
        pnd_Pass_Division = localVariables.newFieldInRecord("pnd_Pass_Division", "#PASS-DIVISION", FieldType.STRING, 6);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Atsign = localVariables.newFieldInRecord("pnd_Atsign", "#ATSIGN", FieldType.STRING, 1);
        pnd_Base_Dte_Tme_D = localVariables.newFieldInRecord("pnd_Base_Dte_Tme_D", "#BASE-DTE-TME-D", FieldType.DATE);
        pnd_End_Dte_Tme_D = localVariables.newFieldInRecord("pnd_End_Dte_Tme_D", "#END-DTE-TME-D", FieldType.DATE);
        pnd_Pin_Split = localVariables.newFieldInRecord("pnd_Pin_Split", "#PIN-SPLIT", FieldType.STRING, 7);

        pnd_Pin_Split__R_Field_20 = localVariables.newGroupInRecord("pnd_Pin_Split__R_Field_20", "REDEFINE", pnd_Pin_Split);
        pnd_Pin_Split_Pnd_Pin_Split_1 = pnd_Pin_Split__R_Field_20.newFieldInGroup("pnd_Pin_Split_Pnd_Pin_Split_1", "#PIN-SPLIT-1", FieldType.STRING, 1);
        pnd_Time_T = localVariables.newFieldInRecord("pnd_Time_T", "#TIME-T", FieldType.TIME);
        pnd_Last_Chnge_Dte_Tme_T = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_Tme_T", "#LAST-CHNGE-DTE-TME-T", FieldType.TIME);
        pnd_Number_W_Decimals = localVariables.newFieldInRecord("pnd_Number_W_Decimals", "#NUMBER-W-DECIMALS", FieldType.NUMERIC, 9, 2);
        pnd_Number_W_Decimals_13 = localVariables.newFieldInRecord("pnd_Number_W_Decimals_13", "#NUMBER-W-DECIMALS-13", FieldType.NUMERIC, 13, 2);
        pnd_Pin_Base_Key = localVariables.newFieldInRecord("pnd_Pin_Base_Key", "#PIN-BASE-KEY", FieldType.STRING, 8);

        pnd_Pin_Base_Key__R_Field_21 = localVariables.newGroupInRecord("pnd_Pin_Base_Key__R_Field_21", "REDEFINE", pnd_Pin_Base_Key);
        pnd_Pin_Base_Key_Pin_Nbr = pnd_Pin_Base_Key__R_Field_21.newFieldInGroup("pnd_Pin_Base_Key_Pin_Nbr", "PIN-NBR", FieldType.STRING, 7);
        pnd_Actv_Unque_Key = localVariables.newFieldInRecord("pnd_Actv_Unque_Key", "#ACTV-UNQUE-KEY", FieldType.STRING, 16);

        pnd_Actv_Unque_Key__R_Field_22 = localVariables.newGroupInRecord("pnd_Actv_Unque_Key__R_Field_22", "REDEFINE", pnd_Actv_Unque_Key);
        pnd_Actv_Unque_Key_Rqst_Log_Dte_Tme = pnd_Actv_Unque_Key__R_Field_22.newFieldInGroup("pnd_Actv_Unque_Key_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Actv_Unque_Key_Actve_Ind = pnd_Actv_Unque_Key__R_Field_22.newFieldInGroup("pnd_Actv_Unque_Key_Actve_Ind", "ACTVE-IND", FieldType.STRING, 1);

        corr_Cntrct_Payee_Key = localVariables.newGroupInRecord("corr_Cntrct_Payee_Key", "CORR-CNTRCT-PAYEE-KEY");
        corr_Cntrct_Payee_Key_Pnd_Corr_Cntrct_Payee_Key = corr_Cntrct_Payee_Key.newFieldInGroup("corr_Cntrct_Payee_Key_Pnd_Corr_Cntrct_Payee_Key", "#CORR-CNTRCT-PAYEE-KEY", 
            FieldType.STRING, 13);

        corr_Cntrct_Payee_Key__R_Field_23 = corr_Cntrct_Payee_Key.newGroupInGroup("corr_Cntrct_Payee_Key__R_Field_23", "REDEFINE", corr_Cntrct_Payee_Key_Pnd_Corr_Cntrct_Payee_Key);
        corr_Cntrct_Payee_Key_Correspondence_Addrss_Ind = corr_Cntrct_Payee_Key__R_Field_23.newFieldInGroup("corr_Cntrct_Payee_Key_Correspondence_Addrss_Ind", 
            "CORRESPONDENCE-ADDRSS-IND", FieldType.STRING, 1);
        corr_Cntrct_Payee_Key_Cntrct_Nmbr = corr_Cntrct_Payee_Key__R_Field_23.newFieldInGroup("corr_Cntrct_Payee_Key_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 
            10);
        corr_Cntrct_Payee_Key_Cntrct_Payee_Cde = corr_Cntrct_Payee_Key__R_Field_23.newFieldInGroup("corr_Cntrct_Payee_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cab_Key = localVariables.newFieldInRecord("pnd_Cab_Key", "#CAB-KEY", FieldType.STRING, 14);

        pnd_Cab_Key__R_Field_24 = localVariables.newGroupInRecord("pnd_Cab_Key__R_Field_24", "REDEFINE", pnd_Cab_Key);
        pnd_Cab_Key_Pnd_Cabinet_Type = pnd_Cab_Key__R_Field_24.newFieldInGroup("pnd_Cab_Key_Pnd_Cabinet_Type", "#CABINET-TYPE", FieldType.STRING, 1);
        pnd_Cab_Key_Pnd_Pin_Nbr = pnd_Cab_Key__R_Field_24.newFieldInGroup("pnd_Cab_Key_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 53);

        pnd_Tbl_Key__R_Field_25 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_25", "REDEFINE", pnd_Tbl_Key);
        pnd_Tbl_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Key__R_Field_25.newFieldInGroup("pnd_Tbl_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Key__R_Field_25.newFieldInGroup("pnd_Tbl_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Key__R_Field_25.newFieldInGroup("pnd_Tbl_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Tbl_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Key__R_Field_25.newFieldInGroup("pnd_Tbl_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 1);
        pnd_Geo_Grp = localVariables.newFieldInRecord("pnd_Geo_Grp", "#GEO-GRP", FieldType.STRING, 4);

        pnd_Geo_Grp__R_Field_26 = localVariables.newGroupInRecord("pnd_Geo_Grp__R_Field_26", "REDEFINE", pnd_Geo_Grp);
        pnd_Geo_Grp_Pnd_State = pnd_Geo_Grp__R_Field_26.newFieldInGroup("pnd_Geo_Grp_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Geo_Grp_Pnd_Geo_Code = pnd_Geo_Grp__R_Field_26.newFieldInGroup("pnd_Geo_Grp_Pnd_Geo_Code", "#GEO-CODE", FieldType.STRING, 2);
        pnd_Ph_Key = localVariables.newFieldInRecord("pnd_Ph_Key", "#PH-KEY", FieldType.STRING, 8);

        pnd_Ph_Key__R_Field_27 = localVariables.newGroupInRecord("pnd_Ph_Key__R_Field_27", "REDEFINE", pnd_Ph_Key);
        pnd_Ph_Key_Rcrd_Type = pnd_Ph_Key__R_Field_27.newFieldInGroup("pnd_Ph_Key_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1);
        pnd_Ph_Key_Srvvr_Mit_Idntfr = pnd_Ph_Key__R_Field_27.newFieldInGroup("pnd_Ph_Key_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 7);
        pnd_Ncw_Read_Cntr = localVariables.newFieldInRecord("pnd_Ncw_Read_Cntr", "#NCW-READ-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Work_Read_Cntr = localVariables.newFieldInRecord("pnd_Work_Read_Cntr", "#WORK-READ-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Wr_Lead_Cntr = localVariables.newFieldInRecord("pnd_Wr_Lead_Cntr", "#WR-LEAD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Customr_Cntr = localVariables.newFieldInRecord("pnd_Customr_Cntr", "#CUSTOMR-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_No_Base_Rec = localVariables.newFieldInRecord("pnd_No_Base_Rec", "#NO-BASE-REC", FieldType.PACKED_DECIMAL, 8);
        pnd_Vac_Address = localVariables.newFieldInRecord("pnd_Vac_Address", "#VAC-ADDRESS", FieldType.PACKED_DECIMAL, 8);
        pnd_No_Base_Rec_V = localVariables.newFieldInRecord("pnd_No_Base_Rec_V", "#NO-BASE-REC-V", FieldType.PACKED_DECIMAL, 8);
        pnd_No_State_In_Table = localVariables.newFieldInRecord("pnd_No_State_In_Table", "#NO-STATE-IN-TABLE", FieldType.PACKED_DECIMAL, 8);
        pnd_Pass_Nfnd_Cntr = localVariables.newFieldInRecord("pnd_Pass_Nfnd_Cntr", "#PASS-NFND-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Add = localVariables.newFieldInRecord("pnd_Add", "#ADD", FieldType.BOOLEAN, 1);
        pnd_Update = localVariables.newFieldInRecord("pnd_Update", "#UPDATE", FieldType.BOOLEAN, 1);
        pnd_Foreign_Address = localVariables.newFieldInRecord("pnd_Foreign_Address", "#FOREIGN-ADDRESS", FieldType.BOOLEAN, 1);
        pnd_Activity_Type = localVariables.newFieldInRecord("pnd_Activity_Type", "#ACTIVITY-TYPE", FieldType.STRING, 4);
        pnd_Pin_Nbr_Save = localVariables.newFieldInRecord("pnd_Pin_Nbr_Save", "#PIN-NBR-SAVE", FieldType.STRING, 12);

        pnd_Pin_Nbr_Save__R_Field_28 = localVariables.newGroupInRecord("pnd_Pin_Nbr_Save__R_Field_28", "REDEFINE", pnd_Pin_Nbr_Save);
        pnd_Pin_Nbr_Save_Pnd_Pin_Nbr_Save_N = pnd_Pin_Nbr_Save__R_Field_28.newFieldInGroup("pnd_Pin_Nbr_Save_Pnd_Pin_Nbr_Save_N", "#PIN-NBR-SAVE-N", FieldType.NUMERIC, 
            12);

        pnd_Frgn_Geo_Pass = localVariables.newGroupInRecord("pnd_Frgn_Geo_Pass", "#FRGN-GEO-PASS");
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_1 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_1", "#GEO-ADDR-LINE-1", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_2 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_2", "#GEO-ADDR-LINE-2", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_3 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_3", "#GEO-ADDR-LINE-3", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_4 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_4", "#GEO-ADDR-LINE-4", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_5 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_5", "#GEO-ADDR-LINE-5", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_6 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_6", "#GEO-ADDR-LINE-6", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_7 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_7", "#GEO-ADDR-LINE-7", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_8 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_8", "#GEO-ADDR-LINE-8", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Return_Cde = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Return_Cde", "#RETURN-CDE", FieldType.NUMERIC, 1);
        pnd_Frgn_Geo_Pass_Pnd_Mail_Cde = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Mail_Cde", "#MAIL-CDE", FieldType.STRING, 1);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Cde = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Cde", "#GEO-CDE", FieldType.STRING, 2);
        pnd_Frgn_Geo_Pass_Pnd_St_Cde1 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_St_Cde1", "#ST-CDE1", FieldType.STRING, 2);
        pnd_Frgn_Geo_Pass_Pnd_St_Cde2 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_St_Cde2", "#ST-CDE2", FieldType.STRING, 2);

        vw_cwf_Efm_Cabinet = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Cabinet", "CWF-EFM-CABINET"), "CWF_EFM_CABINET", "CWF_EFM_CABINET");
        cwf_Efm_Cabinet_Cabinet_Id = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Cabinet_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Cabinet__R_Field_29 = vw_cwf_Efm_Cabinet.getRecord().newGroupInGroup("cwf_Efm_Cabinet__R_Field_29", "REDEFINE", cwf_Efm_Cabinet_Cabinet_Id);
        cwf_Efm_Cabinet_Pnd_Policy_Holder = cwf_Efm_Cabinet__R_Field_29.newFieldInGroup("cwf_Efm_Cabinet_Pnd_Policy_Holder", "#POLICY-HOLDER", FieldType.STRING, 
            1);
        cwf_Efm_Cabinet_Pnd_Pin_Nbr = cwf_Efm_Cabinet__R_Field_29.newFieldInGroup("cwf_Efm_Cabinet_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Efm_Cabinet_Active_Ind = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ACTIVE_IND");
        cwf_Efm_Cabinet_Active_Ind.setDdmHeader("ACTV/IND");
        cwf_Efm_Cabinet_Media_Ind = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Media_Ind", "MEDIA-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MEDIA_IND");
        cwf_Efm_Cabinet_Media_Ind.setDdmHeader("MEDIA IND");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ncw_Efm_Document.reset();
        vw_cwf_Sb_Ph.reset();
        vw_cwf_Efm_Document.reset();
        vw_cwf_Efm_Cabinet.reset();

        ldaCwfl4827.initializeValues();
        ldaCwfl4818.initializeValues();
        ldaCwfl4815.initializeValues();
        ldaCwfl4834.initializeValues();
        ldaCwfl4835.initializeValues();
        ldaCwfl4824.initializeValues();
        ldaCwfl4829.initializeValues();

        localVariables.reset();
        pnd_Atsign.setInitialValue("@");
        pnd_Pin_Base_Key.setInitialValue("       B");
        pnd_Cab_Key.setInitialValue("P");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4840() throws Exception
    {
        super("Cwfb4840");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB4840", onError);
        setupReports();
        //*  ----------- INITIALIZATION ---------------------
        //*  ----------- MAIN LOGIC -------------------------                                                                                                             //Natural: FORMAT ( 0 ) PS = 56 LS = 131;//Natural: FORMAT ( 1 ) PS = 56 LS = 131
        //*   TO ALLOW FOR EASY ESCAPE FROM PROGRAM.
        REP1:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            st = Global.getTIMN();                                                                                                                                        //Natural: SET TIME
                                                                                                                                                                          //Natural: PERFORM READ-RUN-CONTROL
            sub_Read_Run_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM SELECT-MASTER-RECORDS
            sub_Select_Master_Records();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
            sub_Write_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL
            sub_Update_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            //*   (REP1.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-RUN-CONTROL
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-MASTER-RECORDS
        //* **************************************
        //* ***************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-LEAD
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-WORK-LEAD
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-WR-LEAD-DATES
        //* *************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DERIVE-INDICATORS
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DOCUMENT-TYPE
        //* ***************************
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CUSTOMER-RECORD
        //* *************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PERSONAL-INFO
        //*          MDMA2000
        //*          MDMA2000-ID
        //*        /'MAILING ADDRESS        ' MDMA2000.MAILING-ADDRSS-STATE
        //*        /'SSN NUMBER             ' MDMA2000.SSN-NBR
        //*        /'BIRTHDAY               ' MDMA2000.BIRTH-DTE
        //* **************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PART-AGE
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL
        //* *******************************                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( REP1. )
        Global.setEscapeCode(EscapeType.Bottom, "REP1");
        if (true) return;
    }
    private void sub_Read_Run_Control() throws Exception                                                                                                                  //Natural: READ-RUN-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        //*  CALL THE SUB PROGRAM TO FIND "Acitive" RUN CONTROL RECORD WHICH HAS
        //*  BEEN ESTABLISH IN A PREVIOUS STEP.
        //*  ---------------------------------------------------------------------
        DbsUtil.callnat(Cwfn4826.class , getCurrentProcessState(), pdaCwfa4825.getCwfa4825_Output(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),             //Natural: CALLNAT 'CWFN4826' CWFA4825-OUTPUT CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("W")))                    //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E' OR MSG-INFO-SUB.##RETURN-CODE = 'W'
        {
            getReports().write(0, ReportOption.NOTITLE,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                     //Natural: WRITE 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
            {
                DbsUtil.terminate(10);  if (true) return;                                                                                                                 //Natural: TERMINATE 10
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(999)))                                                                                          //Natural: IF MSG-INFO-SUB.##MSG-NR = 999
        {
            getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                  //Natural: WRITE ( 0 ) NOTITLE NOHDR 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            DbsUtil.terminate(11);  if (true) return;                                                                                                                     //Natural: TERMINATE 11
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------------------------------------
        GET1:                                                                                                                                                             //Natural: GET CWF-SUPPORT-TBL CWFA4825-OUTPUT.RUN-CONTROL-ISN
        ldaCwfl4815.getVw_cwf_Support_Tbl().readByID(pdaCwfa4825.getCwfa4825_Output_Run_Control_Isn().getLong(), "GET1");
        pnd_Tbl_Start_Dte_Tme_T.setValue(ldaCwfl4815.getCwf_Support_Tbl_Starting_Date());                                                                                 //Natural: MOVE CWF-SUPPORT-TBL.STARTING-DATE TO #TBL-START-DTE-TME-T
        pnd_Tbl_End_Dte_Tme_T.setValue(ldaCwfl4815.getCwf_Support_Tbl_Ending_Date());                                                                                     //Natural: MOVE CWF-SUPPORT-TBL.ENDING-DATE TO #TBL-END-DTE-TME-T
    }
    private void sub_Select_Master_Records() throws Exception                                                                                                             //Natural: SELECT-MASTER-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        R1:                                                                                                                                                               //Natural: READ WORK FILE 1 #WORK-FILE-1
        while (condition(getWorkFiles().read(1, pnd_Work_File_1)))
        {
            //*  --------------------- CHECK RQST-LOG-DTE-TME -----------------
            if (condition(! (DbsUtil.maskMatches(pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme,"YYYYMMDD00-2400-6000-60N"))))                                                      //Natural: IF #WORK-FILE-1.#RQST-LOG-DTE-TME NE MASK ( YYYYMMDD00-2400-6000-60N )
            {
                getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Work-file.Rqst-Log-Dte-Tme has wrong value = ",pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme);                 //Natural: WRITE /'Work-file.Rqst-Log-Dte-Tme has wrong value = ' #WORK-FILE-1.#RQST-LOG-DTE-TME
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Read_Cntr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WORK-READ-CNTR
            ldaCwfl4827.getVw_ncw_Master().startDatabaseRead                                                                                                              //Natural: READ NCW-MASTER BY RQST-HISTORY-KEY STARTING FROM #WORK-FILE-1.#RQST-LOG-DTE-TME
            (
            "R2",
            new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme, WcType.BY) },
            new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
            );
            R2:
            while (condition(ldaCwfl4827.getVw_ncw_Master().readNextRow("R2")))
            {
                //*  --- AVOID PROCESSING RECORD FOR THE FOLLOWING REASONS -----
                if (condition(ldaCwfl4827.getNcw_Master_Rqst_Log_Dte_Tme().notEquals(pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme)))                                              //Natural: IF NCW-MASTER.RQST-LOG-DTE-TME NE #WORK-FILE-1.#RQST-LOG-DTE-TME
                {
                    if (true) break R2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R2. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCwfl4827.getNcw_Master_System_Updte_Dte_Tme().less(ldaCwfl4815.getCwf_Support_Tbl_Starting_Date())))                                     //Natural: IF NCW-MASTER.SYSTEM-UPDTE-DTE-TME LT CWF-SUPPORT-TBL.STARTING-DATE
                {
                    if (true) break R2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R2. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                //*  JVH 06/01
                if (condition(ldaCwfl4827.getNcw_Master_System_Updte_Dte_Tme().greater(ldaCwfl4815.getCwf_Support_Tbl_Ending_Date())))                                    //Natural: IF NCW-MASTER.SYSTEM-UPDTE-DTE-TME GT CWF-SUPPORT-TBL.ENDING-DATE
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  ----------------------------------------------------------------
                pnd_Rqst_Log_Dte_Tme_Save.setValue(ldaCwfl4827.getNcw_Master_Rqst_Log_Dte_Tme());                                                                         //Natural: MOVE NCW-MASTER.RQST-LOG-DTE-TME TO #RQST-LOG-DTE-TME-SAVE
                pnd_Isn.setValue(ldaCwfl4827.getVw_ncw_Master().getAstISN("R2"));                                                                                         //Natural: ASSIGN #ISN := *ISN
                pnd_Ncw_Read_Cntr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #NCW-READ-CNTR
                //* *****
                //*  ------------- CREATE WORK REQUEST LEAD RECORD  -----------------------
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-LEAD
                sub_Write_Work_Lead();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ------------- CREATE CUSTOMER RECORD ALWAYS    -----------------------
                if (condition(ldaCwfl4827.getNcw_Master_Np_Pin().notEquals(pnd_Pin_Nbr_Save)))                                                                            //Natural: IF NCW-MASTER.NP-PIN NE #PIN-NBR-SAVE
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-CUSTOMER-RECORD
                    sub_Write_Customer_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Pin_Nbr_Save.setValue(ldaCwfl4827.getNcw_Master_Np_Pin());                                                                                        //Natural: MOVE NCW-MASTER.NP-PIN TO #PIN-NBR-SAVE
                }                                                                                                                                                         //Natural: END-IF
                //*  ----------------------------------------------------------------------
                //*  R2.
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  R1.
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (Global.isEscape()) return;
    }
    private void sub_Write_Work_Lead() throws Exception                                                                                                                   //Natural: WRITE-WORK-LEAD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        ldaCwfl4834.getNp_Work_Lead().reset();                                                                                                                            //Natural: RESET NP-WORK-LEAD NP-CUSTOMER
        ldaCwfl4835.getNp_Customer().reset();
                                                                                                                                                                          //Natural: PERFORM FILL-WORK-LEAD
        sub_Fill_Work_Lead();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().write(2, false, ldaCwfl4834.getNp_Work_Lead());                                                                                                    //Natural: WRITE WORK FILE 2 NP-WORK-LEAD
        pnd_Wr_Lead_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #WR-LEAD-CNTR
    }
    private void sub_Fill_Work_Lead() throws Exception                                                                                                                    //Natural: FILL-WORK-LEAD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        ldaCwfl4834.getNp_Work_Lead().setValuesByName(ldaCwfl4827.getVw_ncw_Master());                                                                                    //Natural: MOVE BY NAME NCW-MASTER TO NP-WORK-LEAD
        ldaCwfl4834.getNp_Work_Lead_Pin_Npin().setValue(ldaCwfl4827.getNcw_Master_Np_Pin());                                                                              //Natural: MOVE NCW-MASTER.NP-PIN TO NP-WORK-LEAD.PIN-NPIN
        ldaCwfl4834.getNp_Work_Lead_A1().setValue(pnd_Atsign);                                                                                                            //Natural: MOVE #ATSIGN TO A1 A2 A3 A4 A5 A6 A7 A8 A9 A10 A11 A12 A13 A14 A15 A16 A17 A18 A19 A20 A21
        ldaCwfl4834.getNp_Work_Lead_A2().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A3().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A4().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A5().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A6().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A7().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A8().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A9().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A10().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A11().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A12().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A13().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A14().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A15().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A16().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A17().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A18().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A19().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A20().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_A21().setValue(pnd_Atsign);
        ldaCwfl4834.getNp_Work_Lead_Last_Chnge_Oprtr_Cde().setValue(ldaCwfl4827.getNcw_Master_Modify_Oprtr_Cde());                                                        //Natural: MOVE NCW-MASTER.MODIFY-OPRTR-CDE TO NP-WORK-LEAD.LAST-CHNGE-OPRTR-CDE
        ldaCwfl4834.getNp_Work_Lead_Empl_Oprtr_Cde().setValue(ldaCwfl4827.getNcw_Master_Empl_Racf_Id());                                                                  //Natural: MOVE NCW-MASTER.EMPL-RACF-ID TO NP-WORK-LEAD.EMPL-OPRTR-CDE
        ldaCwfl4834.getNp_Work_Lead_Orgnl_Unit_Cde().setValue(ldaCwfl4827.getNcw_Master_Rqst_Log_Unit_Cde());                                                             //Natural: MOVE NCW-MASTER.RQST-LOG-UNIT-CDE TO NP-WORK-LEAD.ORGNL-UNIT-CDE
        if (condition(ldaCwfl4827.getNcw_Master_Status_Cde().less("8000")))                                                                                               //Natural: IF NCW-MASTER.STATUS-CDE LT '8000'
        {
            ldaCwfl4834.getNp_Work_Lead_Part_Close_Status_A().setValue(" ");                                                                                              //Natural: MOVE ' ' TO NP-WORK-LEAD.PART-CLOSE-STATUS-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4834.getNp_Work_Lead_Part_Close_Status_A().setValue(ldaCwfl4827.getNcw_Master_Status_Cde());                                                           //Natural: MOVE NCW-MASTER.STATUS-CDE TO NP-WORK-LEAD.PART-CLOSE-STATUS-A
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF ADMIN-STATUS-CDE-1 = 'C'
        //*    MOVE  NCW-MASTER.STATUS-CDE TO NP-WORK-LEAD.ADMIN-STATUS-CDE
        //*  END-IF
        //*  FOR CUSTOMER FILE
                                                                                                                                                                          //Natural: PERFORM GET-PERSONAL-INFO
        sub_Get_Personal_Info();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-WR-LEAD-DATES
        sub_Format_Wr_Lead_Dates();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DERIVE-INDICATORS
        sub_Derive_Indicators();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-PART-AGE
        sub_Get_Part_Age();
        if (condition(Global.isEscape())) {return;}
        //*  ----------- SET UP DOCUMENT KEY -------------------
        pnd_Dcmnt_Search_Key_Pnd_Np_Pin.setValue(ldaCwfl4827.getNcw_Master_Np_Pin());                                                                                     //Natural: MOVE NCW-MASTER.NP-PIN TO #DCMNT-SEARCH-KEY.#NP-PIN
        pnd_Dcmnt_Search_Key_Pnd_Crte_Dte_Tme.setValue(ldaCwfl4827.getNcw_Master_System_Updte_Dte_Tme());                                                                 //Natural: MOVE NCW-MASTER.SYSTEM-UPDTE-DTE-TME TO #DCMNT-SEARCH-KEY.#CRTE-DTE-TME
        //*  GY 09/01
        if (condition(ldaCwfl4827.getNcw_Master_Admin_Status_Cde().equals("4173")))                                                                                       //Natural: IF NCW-MASTER.ADMIN-STATUS-CDE = '4173'
        {
            //*  GY 09/01
                                                                                                                                                                          //Natural: PERFORM GET-DOCUMENT-TYPE
            sub_Get_Document_Type();
            if (condition(Global.isEscape())) {return;}
            //*  GY 09/01
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  GY 09/01
            pnd_Doc_Type_Combo.reset();                                                                                                                                   //Natural: RESET #DOC-TYPE-COMBO NP-WORK-LEAD.DOCUMENT-TYPE
            ldaCwfl4834.getNp_Work_Lead_Document_Type().reset();
            //*  GY 09/01
        }                                                                                                                                                                 //Natural: END-IF
        //*  FILL-WORK-REQUEST
    }
    private void sub_Format_Wr_Lead_Dates() throws Exception                                                                                                              //Natural: FORMAT-WR-LEAD-DATES
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        ldaCwfl4834.getNp_Work_Lead_Rqst_Log_Dte_Tme_A_15().setValue(ldaCwfl4827.getNcw_Master_Rqst_Log_Dte_Tme());                                                       //Natural: MOVE NCW-MASTER.RQST-LOG-DTE-TME TO NP-WORK-LEAD.RQST-LOG-DTE-TME-A-15
        pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4827.getNcw_Master_Rqst_Log_Dte_Tme());                                                    //Natural: MOVE EDITED NCW-MASTER.RQST-LOG-DTE-TME TO #TIME-T ( EM = YYYYMMDDHHIISST )
        ldaCwfl4834.getNp_Work_Lead_Crprte_Rqst_Log_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                      //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO NP-WORK-LEAD.CRPRTE-RQST-LOG-DTE-TME-A
        //*   NCW-MASTER.CRPRTE-CLOCK-END-DTE-TME =
        //*                                 MASK(YYYYMMDD00-2400-6000-60N)
        if (condition(ldaCwfl4827.getNcw_Master_Crprte_Close_Dte_Tme().greater(getZero())))                                                                               //Natural: IF NCW-MASTER.CRPRTE-CLOSE-DTE-TME GT 0
        {
            //*   MOVE EDITED NCW-MASTER.CRPRTE-CLOCK-END-DTE-TME
            //*                        TO #TIME-T (EM=YYYYMMDDHHIISST)
            //*   MOVE EDITED #TIME-T
            ldaCwfl4834.getNp_Work_Lead_Crprte_Clock_End_Dte_Tme_A().setValueEdited(ldaCwfl4827.getNcw_Master_Crprte_Close_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED NCW-MASTER.CRPRTE-CLOSE-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO NP-WORK-LEAD.CRPRTE-CLOCK-END-DTE-TME-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4834.getNp_Work_Lead_Crprte_Clock_End_Dte_Tme_A().setValue(" ");                                                                                       //Natural: MOVE ' ' TO NP-WORK-LEAD.CRPRTE-CLOCK-END-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCwfl4827.getNcw_Master_Crprte_Due_Dte_Tme().greater(getZero())))                                                                                 //Natural: IF NCW-MASTER.CRPRTE-DUE-DTE-TME GT 0
        {
            ldaCwfl4834.getNp_Work_Lead_Crprte_Due_Dte_Tme_A().setValueEdited(ldaCwfl4827.getNcw_Master_Crprte_Due_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED NCW-MASTER.CRPRTE-DUE-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO NP-WORK-LEAD.CRPRTE-DUE-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCwfl4827.getNcw_Master_Tiaa_Rcvd_Dte_Tme().greater(getZero())))                                                                                  //Natural: IF NCW-MASTER.TIAA-RCVD-DTE-TME GT 0
        {
            ldaCwfl4834.getNp_Work_Lead_Tiaa_Rcvd_Dte_Tme_A().setValueEdited(ldaCwfl4827.getNcw_Master_Tiaa_Rcvd_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED NCW-MASTER.TIAA-RCVD-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO NP-WORK-LEAD.TIAA-RCVD-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        ldaCwfl4834.getNp_Work_Lead_Last_Chnge_Dte_Tme_A().setValueEdited(ldaCwfl4827.getNcw_Master_System_Updte_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));        //Natural: MOVE EDITED NCW-MASTER.SYSTEM-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO NP-WORK-LEAD.LAST-CHNGE-DTE-TME-A
        ldaCwfl4834.getNp_Work_Lead_Admin_Status_Updte_Dte_Tme_A().setValueEdited(ldaCwfl4827.getNcw_Master_Admin_Status_Updte_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST")); //Natural: MOVE EDITED NCW-MASTER.ADMIN-STATUS-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO NP-WORK-LEAD.ADMIN-STATUS-UPDTE-DTE-TME-A
        pdaNsta1245.getNsta1245_Pnd_Eff_Dte().reset();                                                                                                                    //Natural: RESET NSTA1245.#EFF-DTE
        if (condition(ldaCwfl4827.getNcw_Master_Tiaa_Rcvd_Dte_Tme().notEquals(getZero())))                                                                                //Natural: IF NCW-MASTER.TIAA-RCVD-DTE-TME NE 0
        {
            pnd_Date_Split.setValueEdited(ldaCwfl4827.getNcw_Master_Tiaa_Rcvd_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                                           //Natural: MOVE EDITED NCW-MASTER.TIAA-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #DATE-SPLIT
            //*  AGE
            pdaNsta1245.getNsta1245_Pnd_Eff_Dte().setValue(pnd_Date_Split_Pnd_Date_Split_Dte);                                                                            //Natural: MOVE #DATE-SPLIT-DTE TO NSTA1245.#EFF-DTE
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  FORMAT-WR-DATES
    }
    private void sub_Derive_Indicators() throws Exception                                                                                                                 //Natural: DERIVE-INDICATORS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        ldaCwfl4834.getNp_Work_Lead_Crprte_Status_Ind().setValue(ldaCwfl4827.getNcw_Master_Crprte_Status_Ind());                                                          //Natural: MOVE NCW-MASTER.CRPRTE-STATUS-IND TO NP-WORK-LEAD.CRPRTE-STATUS-IND
        //*  METHOD OF COMMUNIC.
        ldaCwfl4834.getNp_Work_Lead_Moc().setValue(ldaCwfl4827.getNcw_Master_Rqst_Orgn_Cde());                                                                            //Natural: MOVE NCW-MASTER.RQST-ORGN-CDE TO NP-WORK-LEAD.MOC
        if (condition(ldaCwfl4827.getNcw_Master_Actve_Ind().equals(" ")))                                                                                                 //Natural: IF NCW-MASTER.ACTVE-IND = ' '
        {
            ldaCwfl4834.getNp_Work_Lead_Actve_Ind().setValue("N ");                                                                                                       //Natural: MOVE 'N ' TO NP-WORK-LEAD.ACTVE-IND
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------
        //*  DERIVE-INDICATORS
    }
    private void sub_Get_Document_Type() throws Exception                                                                                                                 //Natural: GET-DOCUMENT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        vw_ncw_Efm_Document.startDatabaseRead                                                                                                                             //Natural: READ NCW-EFM-DOCUMENT BY DCMNT-SEARCH-KEY STARTING FROM #DCMNT-SEARCH-KEY
        (
        "READ_EFM",
        new Wc[] { new Wc("DCMNT_SEARCH_KEY", ">=", pnd_Dcmnt_Search_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("DCMNT_SEARCH_KEY", "ASC") }
        );
        READ_EFM:
        while (condition(vw_ncw_Efm_Document.readNextRow("READ_EFM")))
        {
            if (condition(ncw_Efm_Document_Np_Pin.notEquals(pnd_Dcmnt_Search_Key_Pnd_Np_Pin)))                                                                            //Natural: IF NCW-EFM-DOCUMENT.NP-PIN NE #DCMNT-SEARCH-KEY.#NP-PIN
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ncw_Efm_Document_Dcmnt_Ctgry.equals("L")))                                                                                                      //Natural: IF NCW-EFM-DOCUMENT.DCMNT-CTGRY = 'L'
            {
                pnd_Doc_Type_Combo_Pnd_Doc_Category.setValue(ncw_Efm_Document_Dcmnt_Ctgry);                                                                               //Natural: MOVE DCMNT-CTGRY TO #DOC-TYPE-COMBO.#DOC-CATEGORY
                pnd_Doc_Type_Combo_Pnd_Doc_Sub_Category.setValue(ncw_Efm_Document_Dcmnt_Sub_Ctgry);                                                                       //Natural: MOVE DCMNT-SUB-CTGRY TO #DOC-TYPE-COMBO.#DOC-SUB-CATEGORY
                pnd_Doc_Type_Combo_Pnd_Doc_Detail.setValue(ncw_Efm_Document_Dcmnt_Dtl);                                                                                   //Natural: MOVE DCMNT-DTL TO #DOC-TYPE-COMBO.#DOC-DETAIL
                ldaCwfl4834.getNp_Work_Lead_Document_Type().setValue(pnd_Doc_Type_Combo);                                                                                 //Natural: MOVE #DOC-TYPE-COMBO TO NP-WORK-LEAD.DOCUMENT-TYPE
                if (true) break READ_EFM;                                                                                                                                 //Natural: ESCAPE BOTTOM ( READ-EFM. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-IF
            //*  READ EFM-DOCUMENT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  GET-DOCUMENT-TYPE
    }
    private void sub_Write_Customer_Record() throws Exception                                                                                                             //Natural: WRITE-CUSTOMER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        ldaCwfl4835.getNp_Customer_B1().setValue(pnd_Atsign);                                                                                                             //Natural: MOVE #ATSIGN TO B1 B2 B3 B4 B5 B6
        ldaCwfl4835.getNp_Customer_B2().setValue(pnd_Atsign);
        ldaCwfl4835.getNp_Customer_B3().setValue(pnd_Atsign);
        ldaCwfl4835.getNp_Customer_B4().setValue(pnd_Atsign);
        ldaCwfl4835.getNp_Customer_B5().setValue(pnd_Atsign);
        ldaCwfl4835.getNp_Customer_B6().setValue(pnd_Atsign);
        ldaCwfl4835.getNp_Customer_Pin_Npin().setValue(ldaCwfl4827.getNcw_Master_Np_Pin());                                                                               //Natural: MOVE NCW-MASTER.NP-PIN TO NP-CUSTOMER.PIN-NPIN #PIN-SPLIT
        pnd_Pin_Split.setValue(ldaCwfl4827.getNcw_Master_Np_Pin());
        if (condition(DbsUtil.maskMatches(pnd_Pin_Split_Pnd_Pin_Split_1,"N")))                                                                                            //Natural: IF #PIN-SPLIT-1 = MASK ( N )
        {
            ldaCwfl4835.getNp_Customer_Pin_Type().setValue("P");                                                                                                          //Natural: MOVE 'P' TO NP-CUSTOMER.PIN-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4835.getNp_Customer_Pin_Type().setValue("N");                                                                                                          //Natural: MOVE 'N' TO NP-CUSTOMER.PIN-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCwfl4835.getNp_Customer_Last_Chnge_Dte_Tme_A().setValueEdited(ldaCwfl4827.getNcw_Master_System_Updte_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));         //Natural: MOVE EDITED NCW-MASTER.SYSTEM-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO NP-CUSTOMER.LAST-CHNGE-DTE-TME-A
        //*  ------------
        getWorkFiles().write(3, false, ldaCwfl4835.getNp_Customer());                                                                                                     //Natural: WRITE WORK FILE 3 NP-CUSTOMER
        pnd_Customr_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CUSTOMR-CNTR
        //*  ------------
        //*  WRITE-CUSTOMER-RECORD
    }
    private void sub_Get_Personal_Info() throws Exception                                                                                                                 //Natural: GET-PERSONAL-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  NPIRSUNSET START 03/09/2016
        //*  RESET NPRA2000
        //*  MOVE BY NAME  NCW-MASTER TO NPRA2000
        //*  WRITE 'FETCH of MDMP0011 for MQ CONN and OPEN'
        //*  FETCH RETURN 'MDMP0011'
        //*  RESET MDMA2000   /* MDM PIN-EXP >>
        pdaMdma2003.getMdma2003().reset();                                                                                                                                //Natural: RESET MDMA2003
        //*  MOVE BY NAME  NCW-MASTER TO MDMA2000
        //*  MDM PIN-EXP <<
        pdaMdma2003.getMdma2003().setValuesByName(ldaCwfl4827.getVw_ncw_Master());                                                                                        //Natural: MOVE BY NAME NCW-MASTER TO MDMA2003
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                               //Natural: MOVE 'GET' TO CDAOBJ.#FUNCTION
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue(" ");                                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := ' '
        //*  WRITE /'BEFORE GETING PERSONAL INFO.......' NPRA2000.NP-PIN
        //*  WRITE /'BEFORE GETING PERSONAL INFO.......' MDMA2000.NP-PIN
        //*                                       /* MDM PIN-EXP
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"BEFORE GETING PERSONAL INFO.......",pdaMdma2003.getMdma2003_Np_Pin());                                        //Natural: WRITE /'BEFORE GETING PERSONAL INFO.......' MDMA2003.NP-PIN
        if (Global.isEscape()) return;
        //*                                       /* MDM PIN-EXP
        //*  CALLNAT 'NPRN2000'
        //*           NPRA2000
        //*           NPRA2000-ID
        //*           NPRA2001
        //*           CDAOBJ
        //*           DIALOG-INFO-SUB
        //*           MSG-INFO-SUB
        //*           PASS-SUB
        //*           NPRA2002
        //*          #CSM-PARAMETER-DATA
        //*          #FRGN-GEO-PASS
        //*  CALLNAT 'MDMN2000'         /* MDM PIN-EXP >>
        //*  MDM PIN-EXP <<
        DbsUtil.callnat(Mdmn2003.class , getCurrentProcessState(), pdaMdma2003.getMdma2003(), pdaMdma2003.getMdma2003_Id(), pdaMdma2001.getMdma2001(),                    //Natural: CALLNAT 'MDMN2003' MDMA2003 MDMA2003-ID MDMA2001 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB MDMA2002 #CSM-PARAMETER-DATA #FRGN-GEO-PASS
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaMdma2002.getMdma2002(), 
            pdaNasa032.getPnd_Csm_Parameter_Data(), pnd_Frgn_Geo_Pass);
        if (condition(Global.isEscape())) return;
        //*  WRITE /'AFTER GETING PERSONAL INFO.......' NPRA2000.NP-PIN
        //*        /'RETURN CODE =                   .' MSG-INFO-SUB.##RETURN-CODE
        //*        /'MAILING ADDRESS        ' NPRA2000.MAILING-ADDRSS-STATE
        //*        /'SSN NUMBER             ' NPRA2000.SSN-NBR
        //*        /'BIRTHDAY               ' NPRA2000.BIRTH-DTE
        //*  WRITE /'AFTER GETING PERSONAL INFO.......' MDMA2000.NP-PIN
        //*                                       /* MDM PIN-EXP
        //*  MDM PIN-EXP <<
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"AFTER GETING PERSONAL INFO.......",pdaMdma2003.getMdma2003_Np_Pin(),NEWLINE,"RETURN CODE =                   .", //Natural: WRITE /'AFTER GETING PERSONAL INFO.......' MDMA2003.NP-PIN /'RETURN CODE =                   .' MSG-INFO-SUB.##RETURN-CODE /'MAILING ADDRESS        ' MDMA2003.MAILING-ADDRSS-STATE /'SSN NUMBER             ' MDMA2003.SSN-NBR /'BIRTHDAY               ' MDMA2003.BIRTH-DTE
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code(),NEWLINE,"MAILING ADDRESS        ",pdaMdma2003.getMdma2003_Mailing_Addrss_State(),NEWLINE,"SSN NUMBER             ",
            pdaMdma2003.getMdma2003_Ssn_Nbr(),NEWLINE,"BIRTHDAY               ",pdaMdma2003.getMdma2003_Birth_Dte());
        if (Global.isEscape()) return;
        //*  NPIRSUNSET STOP  03/09/2016
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().greater(" ")))                                                                                    //Natural: IF MSG-INFO-SUB.##RETURN-CODE GT ' '
        {
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Name and Address record was NOT found NP-PIN = ",ldaCwfl4827.getNcw_Master_Np_Pin());                     //Natural: WRITE /'Name and Address record was NOT found NP-PIN = ' NCW-MASTER.NP-PIN
            if (Global.isEscape()) return;
            pnd_No_Base_Rec.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #NO-BASE-REC
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  NPIRSUNSET START 03/09/2016
        //*  MOVE NPRA2000.MAILING-ADDRSS-STATE TO NP-CUSTOMER.STATE-OF-RES
        //*  COMPRESS NPRA2000.PRFX-NME NPRA2000.FIRST-NME NPRA2000.LAST-NME
        //*                                 INTO NP-CUSTOMER.PART-NAME
        //*  MOVE NPRA2000.SSN-NBR              TO NP-CUSTOMER.SSN
        //*  MOVE MDMA2000.MAILING-ADDRSS-STATE TO NP-CUSTOMER.STATE-OF-RES
        //*                                       /* MDM PIN-EXP >>
        ldaCwfl4835.getNp_Customer_State_Of_Res().setValue(pdaMdma2003.getMdma2003_Mailing_Addrss_State());                                                               //Natural: MOVE MDMA2003.MAILING-ADDRSS-STATE TO NP-CUSTOMER.STATE-OF-RES
        //*  COMPRESS MDMA2000.PRFX-NME MDMA2000.FIRST-NME MDMA2000.LAST-NME
        ldaCwfl4835.getNp_Customer_Part_Name().setValue(DbsUtil.compress(pdaMdma2003.getMdma2003_Prfx_Nme(), pdaMdma2003.getMdma2003_First_Nme(), pdaMdma2003.getMdma2003_Last_Nme())); //Natural: COMPRESS MDMA2003.PRFX-NME MDMA2003.FIRST-NME MDMA2003.LAST-NME INTO NP-CUSTOMER.PART-NAME
        //*  MOVE MDMA2000.SSN-NBR              TO NP-CUSTOMER.SSN
        //*  MDM PIN-EXP<<
        ldaCwfl4835.getNp_Customer_Ssn().setValue(pdaMdma2003.getMdma2003_Ssn_Nbr());                                                                                     //Natural: MOVE MDMA2003.SSN-NBR TO NP-CUSTOMER.SSN
        //*  NPIRSUNSET STOP  03/09/2016
        //*  NPIRSUNSET START 03/09/2016
        //*  IF NPRA2000.BIRTH-DTE  GT 0
        //*   MOVE NPRA2000.BIRTH-DTE         TO NSTA1245.#DOB-DTE     /* AGE CALC
        //*   MOVE NPRA2000.BIRTH-DTE         TO #NPIR-BIRTH-DTE
        //*   MOVE #NPIR-BIRTH-YYYY           TO #FIX-DTE-YYYY
        //*   COMPRESS #NPIR-BIRTH-MM '/' #NPIR-BIRTH-DD '/' INTO #FIX-DTE-6
        //*    LEAVING NO SPACE
        //*   COMPRESS '"' #FIX-DTE-10 '"' INTO NP-CUSTOMER.DTE-OF-BIRTH
        //*    LEAVING NO SPACE
        //*  IF MDMA2000.BIRTH-DTE  GT 0
        //*                                       /* MDM PIN-EXP >>
        if (condition(pdaMdma2003.getMdma2003_Birth_Dte().greater(getZero())))                                                                                            //Natural: IF MDMA2003.BIRTH-DTE GT 0
        {
            //*  MOVE MDMA2000.BIRTH-DTE         TO NSTA1245.#DOB-DTE     /* AGE CALC
            //*  AGE CALC
            pdaNsta1245.getNsta1245_Pnd_Dob_Dte().setValue(pdaMdma2003.getMdma2003_Birth_Dte());                                                                          //Natural: MOVE MDMA2003.BIRTH-DTE TO NSTA1245.#DOB-DTE
            //*  MOVE MDMA2000.BIRTH-DTE         TO #NPIR-BIRTH-DTE
            //*  MDM PIN-EXP <<
            pnd_Npir_Birth_Dte.setValue(pdaMdma2003.getMdma2003_Birth_Dte());                                                                                             //Natural: MOVE MDMA2003.BIRTH-DTE TO #NPIR-BIRTH-DTE
            pnd_Fix_Dte_10_Pnd_Fix_Dte_Yyyy.setValue(pnd_Npir_Birth_Dte_Pnd_Npir_Birth_Yyyy);                                                                             //Natural: MOVE #NPIR-BIRTH-YYYY TO #FIX-DTE-YYYY
            pnd_Fix_Dte_10_Pnd_Fix_Dte_6.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Npir_Birth_Dte_Pnd_Npir_Birth_Mm, "/", pnd_Npir_Birth_Dte_Pnd_Npir_Birth_Dd,  //Natural: COMPRESS #NPIR-BIRTH-MM '/' #NPIR-BIRTH-DD '/' INTO #FIX-DTE-6 LEAVING NO SPACE
                "/"));
            ldaCwfl4835.getNp_Customer_Dte_Of_Birth().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Fix_Dte_10, "'"));                                //Natural: COMPRESS '"' #FIX-DTE-10 '"' INTO NP-CUSTOMER.DTE-OF-BIRTH LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4835.getNp_Customer_Dte_Of_Birth().setValue(" ");                                                                                                      //Natural: MOVE ' ' TO NP-CUSTOMER.DTE-OF-BIRTH
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE 'FETCH of MDMP0012 for MQ CLOSE'
        //*  FETCH RETURN 'MDMP0012'
        //*  NPIRSUNSET STOP  03/09/2016
        //* **************
        //*  GET-PERSONAL-INFO
    }
    private void sub_Get_Part_Age() throws Exception                                                                                                                      //Natural: GET-PART-AGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pdaNsta1245.getNsta1245_Pnd_Years().reset();                                                                                                                      //Natural: RESET NSTA1245.#YEARS
        if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte().greater(" ") && pdaNsta1245.getNsta1245_Pnd_Dob_Dte().greater(" ")))                                          //Natural: IF NSTA1245.#EFF-DTE GT ' ' AND NSTA1245.#DOB-DTE GT ' '
        {
            DbsUtil.callnat(Nstn1245.class , getCurrentProcessState(), pdaNsta1245.getNsta1245(), pdaCwfpda_M.getMsg_Info_Sub());                                         //Natural: CALLNAT 'NSTN1245' NSTA1245 MSG-INFO-SUB
            if (condition(Global.isEscape())) return;
            ldaCwfl4834.getNp_Work_Lead_Part_Age().setValue(pdaNsta1245.getNsta1245_Pnd_Years());                                                                         //Natural: ASSIGN NP-WORK-LEAD.PART-AGE := NSTA1245.#YEARS
        }                                                                                                                                                                 //Natural: END-IF
        //* GET-PART-AGE
    }
    private void sub_Write_Totals() throws Exception                                                                                                                      //Natural: WRITE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().write(1, NEWLINE,"Program:",Global.getPROGRAM(),new TabSetting(45),"Run Date:",Global.getDATX(),NEWLINE,"******* NCW Extract Statistics for Corporate Reporting ********",NEWLINE,"Begining period    = ",pnd_Tbl_Start_Dte_Tme_T,  //Natural: WRITE ( 1 ) / 'Program:' *PROGRAM 45T 'Run Date:' *DATX / '******* NCW Extract Statistics for Corporate Reporting ********' / 'Begining period    = ' #TBL-START-DTE-TME-T ( EM = YYYYMMDDHHIISST ) / 'Ending   period    = ' #TBL-END-DTE-TME-T ( EM = YYYYMMDDHHIISST ) / '***************************************************************' / 'Number of Work-File Records which has been read = ' #WORK-READ-CNTR / 'Number of NCW Records which has been read       = ' #NCW-READ-CNTR / '---------------- Work Request Lead Statistic -------------' / 'NUMBER OF Non-Part. Work Request Lead Records   = ' #WR-LEAD-CNTR / '---------------- Customer Records Statistic  -------------' / 'NUMBER OF Non-Part. Customer Records Created    = ' #CUSTOMR-CNTR / '***************************************************' / 'Elapsed Time to Process Records ' '(HH:MM:SS.T) :' *TIMD ( ST. ) ( EM = 99:99:99'.'9 )
            new ReportEditMask ("YYYYMMDDHHIISST"),NEWLINE,"Ending   period    = ",pnd_Tbl_End_Dte_Tme_T, new ReportEditMask ("YYYYMMDDHHIISST"),NEWLINE,"***************************************************************",NEWLINE,"Number of Work-File Records which has been read = ",pnd_Work_Read_Cntr,NEWLINE,"Number of NCW Records which has been read       = ",pnd_Ncw_Read_Cntr,NEWLINE,"---------------- Work Request Lead Statistic -------------",NEWLINE,"NUMBER OF Non-Part. Work Request Lead Records   = ",pnd_Wr_Lead_Cntr,NEWLINE,"---------------- Customer Records Statistic  -------------",NEWLINE,"NUMBER OF Non-Part. Customer Records Created    = ",pnd_Customr_Cntr,NEWLINE,"***************************************************",NEWLINE,"Elapsed Time to Process Records ","(HH:MM:SS.T) :",st, 
            new ReportEditMask ("99:99:99'.'9"));
        if (Global.isEscape()) return;
    }
    private void sub_Update_Control() throws Exception                                                                                                                    //Natural: UPDATE-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  PULL CONTROL RECORD TO APPLY STATISTIC COUNTERS
        //*  -----------------------------------------------
        GET_CONTROL_REC:                                                                                                                                                  //Natural: GET CWF-SUPPORT-TBL CWFA4825-OUTPUT.RUN-CONTROL-ISN
        ldaCwfl4815.getVw_cwf_Support_Tbl().readByID(pdaCwfa4825.getCwfa4825_Output_Run_Control_Isn().getLong(), "GET_CONTROL_REC");
        ldaCwfl4815.getCwf_Support_Tbl_Program_Name().setValue("CWFB4840");                                                                                               //Natural: ASSIGN CWF-SUPPORT-TBL.PROGRAM-NAME := 'CWFB4840'
        ldaCwfl4815.getCwf_Support_Tbl_Run_Status().setValue("C");                                                                                                        //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-STATUS := 'C'
        ldaCwfl4815.getCwf_Support_Tbl_Run_Date().setValue(Global.getTIMX());                                                                                             //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-DATE := *TIMX
        ldaCwfl4815.getVw_cwf_Support_Tbl().updateDBRow("GET_CONTROL_REC");                                                                                               //Natural: UPDATE ( GET-CONTROL-REC. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  (7020)
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"RQST-LOG-DTE-TME",ldaCwfl4827.getNcw_Master_Rqst_Log_Dte_Tme(),NEWLINE,"PIN NUMBER      ",                    //Natural: WRITE / 'RQST-LOG-DTE-TME' NCW-MASTER.RQST-LOG-DTE-TME / 'PIN NUMBER      ' NCW-MASTER.NP-PIN / 'ISN NUMBER      ' #ISN / 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE / ' '
            ldaCwfl4827.getNcw_Master_Np_Pin(),NEWLINE,"ISN NUMBER      ",pnd_Isn,NEWLINE,"NATURAL ERROR",Global.getERROR_NR(),"IN",Global.getPROGRAM(),
            "....LINE",Global.getERROR_LINE(),NEWLINE," ");
        DbsUtil.terminate(10);  if (true) return;                                                                                                                         //Natural: TERMINATE 10
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=56 LS=131");
        Global.format(1, "PS=56 LS=131");
    }
}
