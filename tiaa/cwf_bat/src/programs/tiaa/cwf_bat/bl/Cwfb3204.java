/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:28:27 PM
**        * FROM NATURAL PROGRAM : Cwfb3204
************************************************************
**        * FILE NAME            : Cwfb3204.java
**        * CLASS NAME           : Cwfb3204
**        * INSTANCE NAME        : Cwfb3204
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 11
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3204
* SYSTEM   : CRPCWF
* TITLE    : REPORT 10
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF PARTICIPANT-CLOSED CASES
*          | SORT
*          |
*          |
*          |
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3204 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Tbl_Admin_Unit;
    private DbsField pnd_Work_Record_Tbl_Racf;
    private DbsField pnd_Work_Record_Tbl_Calendar_Days;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Bsnss_Days;
    private DbsField pnd_Work_Record_Tbl_Business_Days;
    private DbsField pnd_Work_Record_Tbl_Complete_Days;
    private DbsField pnd_Work_Record_Tbl_Wpid_Act;
    private DbsField pnd_Work_Record_Tbl_Wpid;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Tbl_Wpid_Action;
    private DbsField pnd_Work_Record_Tbl_Status_Key;

    private DbsGroup pnd_Work_Record__R_Field_2;
    private DbsField pnd_Work_Record_Tbl_Last_Chnge_Unit;
    private DbsField pnd_Work_Record_Tbl_Status_Cde;
    private DbsField pnd_Work_Record_Tbl_Close_Unit;
    private DbsField pnd_Work_Record_Tbl_Pin;
    private DbsField pnd_Work_Record_Tbl_Contracts;
    private DbsField pnd_Work_Record_Tbl_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Dte;
    private DbsField pnd_Work_Record_Tbl_Status_Dte;
    private DbsField pnd_Work_Record_Return_Doc_Rec_Dte_Tme;
    private DbsField pnd_Work_Record_Return_Rcvd_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Last_Chge_Dte;
    private DbsField pnd_Work_Record_Pnd_Partic_Sname;
    private DbsField pnd_Work_Record_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Pnd_Last_Chnge_Dte_Tme;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Tbl_Admin_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Admin_Unit", "TBL-ADMIN-UNIT", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Racf = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Racf", "TBL-RACF", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Calendar_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 
            5);
        pnd_Work_Record_Tbl_Tiaa_Bsnss_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 
            5);
        pnd_Work_Record_Tbl_Business_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.NUMERIC, 
            5, 1);
        pnd_Work_Record_Tbl_Complete_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Complete_Days", "TBL-COMPLETE-DAYS", FieldType.NUMERIC, 
            19, 1);
        pnd_Work_Record_Tbl_Wpid_Act = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 1);
        pnd_Work_Record_Tbl_Wpid = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        pnd_Work_Record__R_Field_1 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record_Tbl_Wpid);
        pnd_Work_Record_Tbl_Wpid_Action = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Work_Record_Tbl_Status_Key = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Key", "TBL-STATUS-KEY", FieldType.STRING, 13);

        pnd_Work_Record__R_Field_2 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_2", "REDEFINE", pnd_Work_Record_Tbl_Status_Key);
        pnd_Work_Record_Tbl_Last_Chnge_Unit = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chnge_Unit", "TBL-LAST-CHNGE-UNIT", 
            FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Status_Cde = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde", "TBL-STATUS-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record_Tbl_Close_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Close_Unit", "TBL-CLOSE-UNIT", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Pin", "TBL-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Tbl_Contracts = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Contracts", "TBL-CONTRACTS", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Dte_Tme", "TBL-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_Record_Tbl_Tiaa_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Dte", "TBL-TIAA-DTE", FieldType.DATE);
        pnd_Work_Record_Tbl_Status_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Dte", "TBL-STATUS-DTE", FieldType.TIME);
        pnd_Work_Record_Return_Doc_Rec_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Return_Rcvd_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Tbl_Last_Chge_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chge_Dte", "TBL-LAST-CHGE-DTE", FieldType.NUMERIC, 
            15);
        pnd_Work_Record_Pnd_Partic_Sname = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);
        pnd_Work_Record_Pnd_Rqst_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_Work_Record_Pnd_Last_Chnge_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Last_Chnge_Dte_Tme", "#LAST-CHNGE-DTE-TME", FieldType.STRING, 
            15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3204() throws Exception
    {
        super("Cwfb3204");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        READWORK01:                                                                                                                                                       //Natural: READ WORK 5 #WORK-RECORD
        while (condition(getWorkFiles().read(5, pnd_Work_Record)))
        {
            if (condition(pnd_Work_Record_Tbl_Close_Unit.equals("RSSMP")))                                                                                                //Natural: REJECT IF TBL-CLOSE-UNIT = 'RSSMP'
            {
                continue;
            }
            getSort().writeSortInData(pnd_Work_Record_Tbl_Close_Unit, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Tbl_Calendar_Days,          //Natural: END-ALL
                pnd_Work_Record_Tbl_Pin, pnd_Work_Record_Tbl_Tiaa_Dte, pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, pnd_Work_Record_Return_Doc_Rec_Dte_Tme, pnd_Work_Record_Tbl_Complete_Days, 
                pnd_Work_Record_Tbl_Status_Cde, pnd_Work_Record_Tbl_Racf, pnd_Work_Record_Tbl_Business_Days, pnd_Work_Record_Tbl_Last_Chnge_Unit, pnd_Work_Record_Tbl_Contracts, 
                pnd_Work_Record_Tbl_Status_Dte, pnd_Work_Record_Return_Rcvd_Dte_Tme, pnd_Work_Record_Pnd_Partic_Sname, pnd_Work_Record_Tbl_Admin_Unit, pnd_Work_Record_Tbl_Log_Dte_Tme, 
                pnd_Work_Record_Tbl_Last_Chge_Dte, pnd_Work_Record_Pnd_Rqst_Log_Dte_Tme, pnd_Work_Record_Pnd_Last_Chnge_Dte_Tme);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  UNIT WHERE WORK WAS CLOSED
        getSort().sortData(pnd_Work_Record_Tbl_Close_Unit, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Tbl_Calendar_Days,                     //Natural: SORT BY #WORK-RECORD.TBL-CLOSE-UNIT #WORK-RECORD.TBL-WPID-ACT #WORK-RECORD.TBL-WPID #WORK-RECORD.TBL-CALENDAR-DAYS #WORK-RECORD.TBL-PIN #WORK-RECORD.TBL-TIAA-DTE USING #WORK-RECORD.TBL-TIAA-BSNSS-DAYS #WORK-RECORD.RETURN-DOC-REC-DTE-TME #WORK-RECORD.TBL-COMPLETE-DAYS #WORK-RECORD.TBL-STATUS-CDE #WORK-RECORD.TBL-RACF #WORK-RECORD.TBL-BUSINESS-DAYS #WORK-RECORD.TBL-LAST-CHNGE-UNIT #WORK-RECORD.TBL-CONTRACTS #WORK-RECORD.TBL-STATUS-DTE #WORK-RECORD.RETURN-RCVD-DTE-TME #WORK-RECORD.#PARTIC-SNAME #WORK-RECORD.TBL-ADMIN-UNIT #WORK-RECORD.TBL-LOG-DTE-TME #WORK-RECORD.TBL-LAST-CHGE-DTE #WORK-RECORD.#RQST-LOG-DTE-TME #WORK-RECORD.#LAST-CHNGE-DTE-TME
            pnd_Work_Record_Tbl_Pin, pnd_Work_Record_Tbl_Tiaa_Dte);
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Work_Record_Tbl_Close_Unit, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Tbl_Calendar_Days, 
            pnd_Work_Record_Tbl_Pin, pnd_Work_Record_Tbl_Tiaa_Dte, pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, pnd_Work_Record_Return_Doc_Rec_Dte_Tme, pnd_Work_Record_Tbl_Complete_Days, 
            pnd_Work_Record_Tbl_Status_Cde, pnd_Work_Record_Tbl_Racf, pnd_Work_Record_Tbl_Business_Days, pnd_Work_Record_Tbl_Last_Chnge_Unit, pnd_Work_Record_Tbl_Contracts, 
            pnd_Work_Record_Tbl_Status_Dte, pnd_Work_Record_Return_Rcvd_Dte_Tme, pnd_Work_Record_Pnd_Partic_Sname, pnd_Work_Record_Tbl_Admin_Unit, pnd_Work_Record_Tbl_Log_Dte_Tme, 
            pnd_Work_Record_Tbl_Last_Chge_Dte, pnd_Work_Record_Pnd_Rqst_Log_Dte_Tme, pnd_Work_Record_Pnd_Last_Chnge_Dte_Tme)))
        {
            getWorkFiles().write(1, false, pnd_Work_Record);                                                                                                              //Natural: WRITE WORK FILE 1 #WORK-RECORD
            //*  READ-2.
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
    }

    //
}
