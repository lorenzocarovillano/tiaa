/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:13:42 PM
**        * FROM NATURAL PROGRAM : Efsp9026
************************************************************
**        * FILE NAME            : Efsp9026.java
**        * CLASS NAME           : Efsp9026
**        * INSTANCE NAME        : Efsp9026
************************************************************
************************************************************************
* PROGRAM  : EFSP9026 (FORMERLY EFSP0050)
* SYSTEM   : PROJCWF
* TITLE    : DELETED MIN's report from Folder-Audit file.
* GENERATED: 08/15/95
* FUNCTION : THIS PROGRAM READS A SORTED CWF-FOLDER-AUDIT WORK FILE
*            DAILY AND CREATES A REPORT FOR DELETED MIN's.
* HISTORY
* ----------
* 10/17/95 JHH - READ WORK FILE, NOT CWF-FOLDER-AUDIT
* 02/23/2017 - PIN EXPANSION - AUG 2017
************************************************************************
* GLOBAL USING CWFG000

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsp9026 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Program;
    private DbsField pnd_Source_Id;
    private DbsField pnd_Batch_Count;

    private DbsGroup cwf_Folder_Audit;
    private DbsField cwf_Folder_Audit_Source_Id;
    private DbsField cwf_Folder_Audit_Image_Min;
    private DbsField cwf_Folder_Audit_Pin_Nbr;
    private DbsField cwf_Folder_Audit_Rqst_Id;
    private DbsField cwf_Folder_Audit_Empl_Oprtr_Cde;
    private DbsField cwf_Folder_Audit_Error_Msg;
    private DbsField cwf_Folder_Audit_Log_Dte_Tme;
    private DbsField pnd_Rqst_Id;

    private DbsGroup pnd_Rqst_Id__R_Field_1;
    private DbsField pnd_Rqst_Id_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Rqst_Id_Pnd_Case_Id;
    private DbsField pnd_Rqst_Id_Pnd_Wpid;
    private DbsField pnd_Rqst_Id_Pnd_Subrqst_Id;

    private DbsRecord internalLoopRecord;
    private DbsField rEAD_AUDITSource_IdOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Source_Id = localVariables.newFieldInRecord("pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 6);
        pnd_Batch_Count = localVariables.newFieldInRecord("pnd_Batch_Count", "#BATCH-COUNT", FieldType.NUMERIC, 5);

        cwf_Folder_Audit = localVariables.newGroupInRecord("cwf_Folder_Audit", "CWF-FOLDER-AUDIT");
        cwf_Folder_Audit_Source_Id = cwf_Folder_Audit.newFieldInGroup("cwf_Folder_Audit_Source_Id", "SOURCE-ID", FieldType.STRING, 6);
        cwf_Folder_Audit_Image_Min = cwf_Folder_Audit.newFieldInGroup("cwf_Folder_Audit_Image_Min", "IMAGE-MIN", FieldType.STRING, 11);
        cwf_Folder_Audit_Pin_Nbr = cwf_Folder_Audit.newFieldInGroup("cwf_Folder_Audit_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Folder_Audit_Rqst_Id = cwf_Folder_Audit.newFieldArrayInGroup("cwf_Folder_Audit_Rqst_Id", "RQST-ID", FieldType.STRING, 16, new DbsArrayController(1, 
            10));
        cwf_Folder_Audit_Empl_Oprtr_Cde = cwf_Folder_Audit.newFieldInGroup("cwf_Folder_Audit_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 8);
        cwf_Folder_Audit_Error_Msg = cwf_Folder_Audit.newFieldInGroup("cwf_Folder_Audit_Error_Msg", "ERROR-MSG", FieldType.STRING, 30);
        cwf_Folder_Audit_Log_Dte_Tme = cwf_Folder_Audit.newFieldInGroup("cwf_Folder_Audit_Log_Dte_Tme", "LOG-DTE-TME", FieldType.TIME);
        pnd_Rqst_Id = localVariables.newFieldInRecord("pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 16);

        pnd_Rqst_Id__R_Field_1 = localVariables.newGroupInRecord("pnd_Rqst_Id__R_Field_1", "REDEFINE", pnd_Rqst_Id);
        pnd_Rqst_Id_Pnd_Tiaa_Rcvd_Dte = pnd_Rqst_Id__R_Field_1.newFieldInGroup("pnd_Rqst_Id_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.STRING, 8);
        pnd_Rqst_Id_Pnd_Case_Id = pnd_Rqst_Id__R_Field_1.newFieldInGroup("pnd_Rqst_Id_Pnd_Case_Id", "#CASE-ID", FieldType.STRING, 1);
        pnd_Rqst_Id_Pnd_Wpid = pnd_Rqst_Id__R_Field_1.newFieldInGroup("pnd_Rqst_Id_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Rqst_Id_Pnd_Subrqst_Id = pnd_Rqst_Id__R_Field_1.newFieldInGroup("pnd_Rqst_Id_Pnd_Subrqst_Id", "#SUBRQST-ID", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rEAD_AUDITSource_IdOld = internalLoopRecord.newFieldInRecord("READ_AUDIT_Source_Id_OLD", "Source_Id_OLD", FieldType.STRING, 6);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Efsp9026() throws Exception
    {
        super("Efsp9026");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt4, 4);
        setupReports();
        //*  ------------------------------------------------------
        getReports().definePrinter(5, "AUDIT");                                                                                                                           //Natural: DEFINE PRINTER ( AUDIT = 4 )
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( AUDIT ) LS = 133 PS = 60 ZP = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  -------------------------------------
        boolean endOfDataReadAudit = true;                                                                                                                                //Natural: READ WORK 3 CWF-FOLDER-AUDIT
        boolean firstReadAudit = true;
        READ_AUDIT:
        while (condition(getWorkFiles().read(3, cwf_Folder_Audit)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventRead_Audit();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadAudit = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(cwf_Folder_Audit_Source_Id.equals("000000")))                                                                                                   //Natural: REJECT IF CWF-FOLDER-AUDIT.SOURCE-ID = '000000'
            {
                continue;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF CWF-FOLDER-AUDIT.SOURCE-ID
            pnd_Source_Id.setValue(cwf_Folder_Audit_Source_Id);                                                                                                           //Natural: ASSIGN #SOURCE-ID = CWF-FOLDER-AUDIT.SOURCE-ID
            pnd_Rqst_Id.setValue(cwf_Folder_Audit_Rqst_Id.getValue(1));                                                                                                   //Natural: ASSIGN #RQST-ID = CWF-FOLDER-AUDIT.RQST-ID ( 1 )
            getReports().write(5, ReportOption.NOTITLE, writeMapToStringOutput(Efsf0051.class));                                                                          //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF0051'
            pnd_Batch_Count.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #BATCH-COUNT
            rEAD_AUDITSource_IdOld.setValue(cwf_Folder_Audit_Source_Id);                                                                                                  //Natural: END-WORK
        }
        READ_AUDIT_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventRead_Audit(endOfDataReadAudit);
        }
        if (Global.isEscape()) return;
        //*  -------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( AUDIT )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(5, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf0050.class));                                              //Natural: WRITE ( AUDIT ) NOTITLE NOHDR USING FORM 'EFSF0050'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventRead_Audit() throws Exception {atBreakEventRead_Audit(false);}
    private void atBreakEventRead_Audit(boolean endOfData) throws Exception
    {
        boolean cwf_Folder_Audit_Source_IdIsBreak = cwf_Folder_Audit_Source_Id.isBreak(endOfData);
        if (condition(cwf_Folder_Audit_Source_IdIsBreak))
        {
            pnd_Source_Id.setValue(rEAD_AUDITSource_IdOld);                                                                                                               //Natural: ASSIGN #SOURCE-ID = OLD ( CWF-FOLDER-AUDIT.SOURCE-ID )
            if (condition(getReports().getAstLinesLeft(0).less(5)))                                                                                                       //Natural: NEWPAGE ( AUDIT ) IF LESS THAN 5 LINES LEFT
            {
                getReports().newPage(0);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(5, NEWLINE,NEWLINE,new TabSetting(1),"TOTAL FOR SOURCE ID",pnd_Source_Id," : ",pnd_Batch_Count);                                           //Natural: WRITE ( AUDIT ) // 1T 'TOTAL FOR SOURCE ID' #SOURCE-ID ' : ' #BATCH-COUNT
            if (condition(Global.isEscape())) return;
            pnd_Batch_Count.reset();                                                                                                                                      //Natural: RESET #BATCH-COUNT
            getReports().getPageNumberDbs(5).reset();                                                                                                                     //Natural: RESET *PAGE-NUMBER ( AUDIT )
            pnd_Source_Id.setValue(cwf_Folder_Audit_Source_Id);                                                                                                           //Natural: ASSIGN #SOURCE-ID = CWF-FOLDER-AUDIT.SOURCE-ID
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( AUDIT )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(5, "LS=133 PS=60 ZP=OFF");
    }
}
