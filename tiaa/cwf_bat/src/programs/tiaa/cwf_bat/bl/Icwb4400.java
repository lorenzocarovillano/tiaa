/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:39:49 PM
**        * FROM NATURAL PROGRAM : Icwb4400
************************************************************
**        * FILE NAME            : Icwb4400.java
**        * CLASS NAME           : Icwb4400
**        * INSTANCE NAME        : Icwb4400
************************************************************
**********************************************************************
*  PROGRAM  : ICWB4400                                               *
*  FUNCTION : CLOSES OUT WORK REQUESTS FOR UNIT 'PROC WT' HAVING A   *
*             WPID EQUAL TO 'RA P'                                   *
*  SYSTEM   : INTITUTIONAL CORPORATE WORKFLOW                        *
*  MOD DATE :                                                        *
**********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwb4400 extends BLNatBase
{
    // Data Areas
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaIcwa8010 pdaIcwa8010;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_icw_Master;
    private DbsField icw_Master_Rqst_Log_Dte_Tme;
    private DbsField icw_Master_Ppg_Cde;
    private DbsField icw_Master_Work_Prcss_Id;
    private DbsField icw_Master_Unit_Cde;
    private DbsField icw_Master_Admin_Unit_Cde;
    private DbsField icw_Master_Admin_Status_Cde;
    private DbsField icw_Master_Status_Cde;
    private DbsField icw_Master_Crprte_Status_Ind;
    private DbsField icw_Master_Tiaa_Rcvd_Dte_Tme;
    private DbsField icw_Master_Crrnt_Due_Dte_Tme;
    private DbsField icw_Master_Crprte_Due_Dte_Tme;
    private DbsField icw_Master_Instn_Close_Dte_Tme;
    private DbsField icw_Master_Crprte_Close_Dte_Tme;

    private DbsGroup pnd_Vars;
    private DbsField pnd_Vars_Pnd_Tot_Ctr;
    private DbsField pnd_Vars_Pnd_Updt_Ctr;
    private DbsField pnd_Vars_Pnd_Err_Ctr;
    private DbsField pnd_Vars_Pnd_H_Wpid;
    private DbsField pnd_Vars_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Vars_Pnd_Start_Key;

    private DbsGroup pnd_Vars__R_Field_1;
    private DbsField pnd_Vars_Pnd_Start_Unit;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaIcwa8010 = new PdaIcwa8010(localVariables);

        // Local Variables

        vw_icw_Master = new DataAccessProgramView(new NameInfo("vw_icw_Master", "ICW-MASTER"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        icw_Master_Rqst_Log_Dte_Tme = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        icw_Master_Ppg_Cde = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PPG_CDE");
        icw_Master_Work_Prcss_Id = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        icw_Master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        icw_Master_Unit_Cde = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        icw_Master_Unit_Cde.setDdmHeader("UNIT/CODE");
        icw_Master_Admin_Unit_Cde = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        icw_Master_Admin_Status_Cde = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        icw_Master_Status_Cde = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        icw_Master_Crprte_Status_Ind = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        icw_Master_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        icw_Master_Tiaa_Rcvd_Dte_Tme = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        icw_Master_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        icw_Master_Crrnt_Due_Dte_Tme = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Crrnt_Due_Dte_Tme", "CRRNT-DUE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRRNT_DUE_DTE_TME");
        icw_Master_Crprte_Due_Dte_Tme = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRPRTE_DUE_DTE_TME");
        icw_Master_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        icw_Master_Instn_Close_Dte_Tme = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Instn_Close_Dte_Tme", "INSTN-CLOSE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "INSTN_CLOSE_DTE_TME");
        icw_Master_Crprte_Close_Dte_Tme = vw_icw_Master.getRecord().newFieldInGroup("icw_Master_Crprte_Close_Dte_Tme", "CRPRTE-CLOSE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRPRTE_CLOSE_DTE_TME");
        registerRecord(vw_icw_Master);

        pnd_Vars = localVariables.newGroupInRecord("pnd_Vars", "#VARS");
        pnd_Vars_Pnd_Tot_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Tot_Ctr", "#TOT-CTR", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Updt_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Updt_Ctr", "#UPDT-CTR", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Err_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Err_Ctr", "#ERR-CTR", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_H_Wpid = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_H_Wpid", "#H-WPID", FieldType.STRING, 6);
        pnd_Vars_Pnd_Rqst_Log_Dte_Tme = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.TIME);
        pnd_Vars_Pnd_Start_Key = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Start_Key", "#START-KEY", FieldType.STRING, 39);

        pnd_Vars__R_Field_1 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_1", "REDEFINE", pnd_Vars_Pnd_Start_Key);
        pnd_Vars_Pnd_Start_Unit = pnd_Vars__R_Field_1.newFieldInGroup("pnd_Vars_Pnd_Start_Unit", "#START-UNIT", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_icw_Master.reset();

        localVariables.reset();
        pnd_Vars_Pnd_H_Wpid.setInitialValue("RA P");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Icwb4400() throws Exception
    {
        super("Icwb4400");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("ICWB4400", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        st = Global.getTIMN();                                                                                                                                            //Natural: SET TIME
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 58 LS = 131;//Natural: FORMAT ( 2 ) PS = 58 LS = 131
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        pnd_Vars_Pnd_Start_Unit.setValue("PROC WT");                                                                                                                      //Natural: ASSIGN #START-UNIT := 'PROC WT'
        vw_icw_Master.startDatabaseRead                                                                                                                                   //Natural: READ ICW-MASTER WITH ALL-WORK-RQST-KEY FROM #START-KEY
        (
        "R1",
        new Wc[] { new Wc("ALL_WORK_RQST_KEY", ">=", pnd_Vars_Pnd_Start_Key, WcType.BY) },
        new Oc[] { new Oc("ALL_WORK_RQST_KEY", "ASC") }
        );
        R1:
        while (condition(vw_icw_Master.readNextRow("R1")))
        {
            if (condition(icw_Master_Admin_Unit_Cde.notEquals("PROC WT")))                                                                                                //Natural: IF ICW-MASTER.ADMIN-UNIT-CDE NE 'PROC WT'
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(icw_Master_Work_Prcss_Id.equals("RA P") && icw_Master_Crprte_Status_Ind.equals("0"))))                                                        //Natural: ACCEPT IF ICW-MASTER.WORK-PRCSS-ID = 'RA P' AND ICW-MASTER.CRPRTE-STATUS-IND = '0'
            {
                continue;
            }
            pnd_Vars_Pnd_Tot_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOT-CTR
            pdaIcwa8010.getIcwa8010().reset();                                                                                                                            //Natural: RESET ICWA8010 MSG-INFO-SUB.##RETURN-CODE
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();
            pdaIcwa8010.getIcwa8010_Action_Cde().setValue("GT");                                                                                                          //Natural: ASSIGN ICWA8010.ACTION-CDE := 'GT'
            pdaIcwa8010.getIcwa8010_Modify_Applctn_Cde().setValue("BATCHCWF");                                                                                            //Natural: ASSIGN ICWA8010.MODIFY-APPLCTN-CDE := 'BATCHCWF'
            pdaIcwa8010.getIcwa8010_Rqst_Log_Dte_Tme().setValue(icw_Master_Rqst_Log_Dte_Tme);                                                                             //Natural: ASSIGN ICWA8010.RQST-LOG-DTE-TME := ICW-MASTER.RQST-LOG-DTE-TME
            pdaIcwa8010.getIcwa8010_Rqst_Log_Oprtr_Cde().setValue(Global.getINIT_USER());                                                                                 //Natural: ASSIGN ICWA8010.RQST-LOG-OPRTR-CDE := *INIT-USER
            DbsUtil.callnat(Icwn8010.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaCwfpda_D.getDialog_Info_Sub(),        //Natural: CALLNAT 'ICWN8010' MSG-INFO-SUB PASS-SUB DIALOG-INFO-SUB ICWA8010
                pdaIcwa8010.getIcwa8010());
            if (condition(Global.isEscape())) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals(" ")))                                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = ' '
            {
                pdaIcwa8010.getIcwa8010_Action_Cde().setValue("MO");                                                                                                      //Natural: ASSIGN ICWA8010.ACTION-CDE := 'MO'
                pdaIcwa8010.getIcwa8010_Wpid_Vldte_Ind().setValue("Y");                                                                                                   //Natural: ASSIGN ICWA8010.WPID-VLDTE-IND := 'Y'
                pdaIcwa8010.getIcwa8010_Modify_Applctn_Cde().setValue("BATCHCWF");                                                                                        //Natural: ASSIGN ICWA8010.MODIFY-APPLCTN-CDE := 'BATCHCWF'
                pdaIcwa8010.getIcwa8010_Rqst_Log_Oprtr_Cde().setValue(Global.getINIT_USER());                                                                             //Natural: ASSIGN ICWA8010.RQST-LOG-OPRTR-CDE := *INIT-USER
                pdaIcwa8010.getIcwa8010_Status_Cde().setValue("8799");                                                                                                    //Natural: ASSIGN ICWA8010.STATUS-CDE := '8799'
                DbsUtil.callnat(Icwn8010.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaCwfpda_D.getDialog_Info_Sub(),    //Natural: CALLNAT 'ICWN8010' MSG-INFO-SUB PASS-SUB DIALOG-INFO-SUB ICWA8010
                    pdaIcwa8010.getIcwa8010());
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().greater(getZero())))             //Natural: IF MSG-INFO-SUB.##ERROR-FIELD GT ' ' OR MSG-INFO-SUB.##MSG-NR GT 0
            {
                pnd_Vars_Pnd_Err_Ctr.nadd(1);                                                                                                                             //Natural: ADD 1 TO #ERR-CTR
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),pnd_Vars_Pnd_Err_Ctr,new TabSetting(8),icw_Master_Rqst_Log_Dte_Tme,new TabSetting(24),icw_Master_Crprte_Status_Ind,new  //Natural: WRITE ( 2 ) 001T #ERR-CTR 008T ICW-MASTER.RQST-LOG-DTE-TME 024T ICW-MASTER.CRPRTE-STATUS-IND 027T ICW-MASTER.UNIT-CDE 036T ICW-MASTER.WORK-PRCSS-ID 043T ICW-MASTER.PPG-CDE 050T ICW-MASTER.ADMIN-UNIT-CDE 060T ICW-MASTER.STATUS-CDE 068T ICW-MASTER.ADMIN-STATUS-CDE 075T MSG-INFO-SUB.##ERROR-FIELD ( AL = 18 ) 096T MSG-INFO-SUB.##MSG ( AL = 23 ) 120T MSG-INFO-SUB.##MSG-NR 128T MSG-INFO-SUB.##RETURN-CODE
                    TabSetting(27),icw_Master_Unit_Cde,new TabSetting(36),icw_Master_Work_Prcss_Id,new TabSetting(43),icw_Master_Ppg_Cde,new TabSetting(50),icw_Master_Admin_Unit_Cde,new 
                    TabSetting(60),icw_Master_Status_Cde,new TabSetting(68),icw_Master_Admin_Status_Cde,new TabSetting(75),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field(), 
                    new AlphanumericLength (18),new TabSetting(96),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(), new AlphanumericLength (23),new TabSetting(120),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr(),new 
                    TabSetting(128),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().reset();                                                                                                //Natural: RESET MSG-INFO-SUB.##ERROR-FIELD MSG-INFO-SUB.##MSG-NR MSG-INFO-SUB.##MSG MSG-INFO-SUB.##RETURN-CODE
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().reset();
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Vars_Pnd_Updt_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #UPDT-CTR
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Vars_Pnd_Updt_Ctr,new TabSetting(8),icw_Master_Rqst_Log_Dte_Tme,new TabSetting(24),icw_Master_Crprte_Status_Ind,new  //Natural: WRITE ( 1 ) 001T #UPDT-CTR 008T ICW-MASTER.RQST-LOG-DTE-TME 024T ICW-MASTER.CRPRTE-STATUS-IND 027T ICW-MASTER.UNIT-CDE 036T ICW-MASTER.WORK-PRCSS-ID 043T ICW-MASTER.PPG-CDE 050T ICW-MASTER.ADMIN-UNIT-CDE 060T ICW-MASTER.STATUS-CDE 068T ICW-MASTER.ADMIN-STATUS-CDE 075T ICW-MASTER.TIAA-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST ) 091T ICW-MASTER.CRPRTE-DUE-DTE-TME ( EM = YYYYMMDDHHIISST )
                    TabSetting(27),icw_Master_Unit_Cde,new TabSetting(36),icw_Master_Work_Prcss_Id,new TabSetting(43),icw_Master_Ppg_Cde,new TabSetting(50),icw_Master_Admin_Unit_Cde,new 
                    TabSetting(60),icw_Master_Status_Cde,new TabSetting(68),icw_Master_Admin_Status_Cde,new TabSetting(75),icw_Master_Tiaa_Rcvd_Dte_Tme, 
                    new ReportEditMask ("YYYYMMDDHHIISST"),new TabSetting(91),icw_Master_Crprte_Due_Dte_Tme, new ReportEditMask ("YYYYMMDDHHIISST"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"Total records read    ",pnd_Vars_Pnd_Tot_Ctr,NEWLINE,"Total records updated ",pnd_Vars_Pnd_Updt_Ctr,NEWLINE,"Total records in error",pnd_Vars_Pnd_Err_Ctr,NEWLINE,NEWLINE,"Time Elapsed: ",st,  //Natural: WRITE ( 1 ) /// 'Total records read    ' #TOT-CTR / 'Total records updated ' #UPDT-CTR / 'Total records in error' #ERR-CTR // 'Time Elapsed: ' *TIMD ( ST. ) ( EM = 99':'99':'99'.'9 )
            new ReportEditMask ("99':'99':'99'.'9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"Total records read    ",pnd_Vars_Pnd_Tot_Ctr,NEWLINE,"Total records updated ",pnd_Vars_Pnd_Updt_Ctr,NEWLINE,"Total records in error",pnd_Vars_Pnd_Err_Ctr,NEWLINE,NEWLINE,"Time Elapsed: ",st,  //Natural: WRITE ( 2 ) /// 'Total records read    ' #TOT-CTR / 'Total records updated ' #UPDT-CTR / 'Total records in error' #ERR-CTR // 'Time Elapsed: ' *TIMD ( ST. ) ( EM = 99':'99':'99'.'9 )
            new ReportEditMask ("99':'99':'99'.'9"));
        if (Global.isEscape()) return;
    }                                                                                                                                                                     //Natural: ON ERROR

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM:",Global.getPROGRAM(),new TabSetting(40),"Closeout of Work Requests Unit ",pnd_Vars_Pnd_Start_Unit,"for WPID",pnd_Vars_Pnd_H_Wpid,new  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM:' *PROGRAM 40T 'Closeout of Work Requests Unit ' #START-UNIT 'for WPID' #H-WPID 115T 'TIME:' *TIME / 'DATE   :' *DATX 115T 'PAGE:' *PAGE-NUMBER ( 1 ) // 012T 'RLDT' 023T 'CSI' 027T 'UNIT-CD' 036T 'WPID' 042T 'PPG-CD' 051T 'ADMIN' 059T 'STAT-CD' 068T 'ADMIN' 080T 'TIAA' 095T 'CRPRTE' / 050T 'UNIT-CD' 067T 'STAT-CD' 078T 'RCVD DTE' 095T 'DUE DTE' ///
                        TabSetting(115),"TIME:",Global.getTIME(),NEWLINE,"DATE   :",Global.getDATX(),new TabSetting(115),"PAGE:",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new 
                        TabSetting(12),"RLDT",new TabSetting(23),"CSI",new TabSetting(27),"UNIT-CD",new TabSetting(36),"WPID",new TabSetting(42),"PPG-CD",new 
                        TabSetting(51),"ADMIN",new TabSetting(59),"STAT-CD",new TabSetting(68),"ADMIN",new TabSetting(80),"TIAA",new TabSetting(95),"CRPRTE",NEWLINE,new 
                        TabSetting(50),"UNIT-CD",new TabSetting(67),"STAT-CD",new TabSetting(78),"RCVD DTE",new TabSetting(95),"DUE DTE",NEWLINE,NEWLINE,
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM:",Global.getPROGRAM(),new TabSetting(40),"Error report of Work Rqst Unit ",pnd_Vars_Pnd_Start_Unit,"for WPID",pnd_Vars_Pnd_H_Wpid,new  //Natural: WRITE ( 2 ) NOTITLE 'PROGRAM:' *PROGRAM 40T 'Error report of Work Rqst Unit ' #START-UNIT 'for WPID' #H-WPID 115T 'TIME:' *TIME / 'DATE   :' *DATX 115T 'PAGE:' *PAGE-NUMBER ( 2 ) // 012T 'RLDT' 023T 'CSI' 027T 'UNIT-CD' 036T 'WPID' 042T 'PPG-CD' 051T 'ADMIN' 059T 'STAT-CD' 068T 'ADMIN' 075T 'ERROR FIELD' 096T 'ERROR MSG' 121T 'ERROR' 127T 'RETN' / 050T 'UNIT-CD' 067T 'STAT-CD' 122T 'NBR' 127T 'CDE' ///
                        TabSetting(115),"TIME:",Global.getTIME(),NEWLINE,"DATE   :",Global.getDATX(),new TabSetting(115),"PAGE:",getReports().getPageNumberDbs(2),NEWLINE,NEWLINE,new 
                        TabSetting(12),"RLDT",new TabSetting(23),"CSI",new TabSetting(27),"UNIT-CD",new TabSetting(36),"WPID",new TabSetting(42),"PPG-CD",new 
                        TabSetting(51),"ADMIN",new TabSetting(59),"STAT-CD",new TabSetting(68),"ADMIN",new TabSetting(75),"ERROR FIELD",new TabSetting(96),"ERROR MSG",new 
                        TabSetting(121),"ERROR",new TabSetting(127),"RETN",NEWLINE,new TabSetting(50),"UNIT-CD",new TabSetting(67),"STAT-CD",new TabSetting(122),"NBR",new 
                        TabSetting(127),"CDE",NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "Natural Error",Global.getERROR_NR(),"on line",Global.getERROR_LINE(),"of",Global.getPROGRAM());                                            //Natural: WRITE 'Natural Error' *ERROR-NR 'on line' *ERROR-LINE 'of' *PROGRAM
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=131");
        Global.format(2, "PS=58 LS=131");
    }
}
