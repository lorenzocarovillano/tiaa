/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:47:45 PM
**        * FROM NATURAL PROGRAM : Cwfb9994
************************************************************
**        * FILE NAME            : Cwfb9994.java
**        * CLASS NAME           : Cwfb9994
**        * INSTANCE NAME        : Cwfb9994
************************************************************
************************************************************************
* SYSTEM       : CORPORATE WORKFLOW
* PROGRAM      : CWFB9994
* TITLE        : CWF - ADHOC DATA FIX PROGRAM
* FUNCTION     : CLOSE THE OPEN CORRESPONDENCE WORK REQUESTS
*                CREATED IN THE UNIT 'CIRS'
*                FILTER WORK REQUESTS BASED ON THE FOLLOWING CRITERIA
*                   1. DATE RANGE     - LAST 7 DAYS
*                   2. ORGNL-UNIT-CDE - CRC
*                   3. UNIT-CDE       - CIRS
*                   4. WPID           - 'TZ YCO'
*                   5. STATUS CODE    - NE MASK('8')
* DATE WRITTEN : OCT 1, 2014
* AUTHOR       : VINODH KUMAR RAMACHANDRAN
* REQUESTOR    : GARNET EVANS (BUSINESS USER - CIRS)
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb9994 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_master;
    private DbsField master_Rqst_Log_Dte_Tme;

    private DbsGroup master__R_Field_1;
    private DbsField master_Pnd_Rqst_Log_Dte;
    private DbsField master_Pnd_Rqst_Log_Tme;
    private DbsField master_Work_Prcss_Id;
    private DbsField master_Wpid_Vldte_Ind;
    private DbsField master_Pin_Nbr;
    private DbsField master_Actve_Ind;
    private DbsField master_Unit_Cde;
    private DbsField master_Orgnl_Unit_Cde;
    private DbsField master_Last_Chnge_Oprtr_Cde;
    private DbsField master_Status_Cde;
    private DbsField master_Admin_Status_Cde;
    private DbsField master_Crprte_Status_Ind;
    private DbsField master_Last_Updte_Dte;
    private DbsField master_Final_Close_Out_Dte_Tme;
    private DbsField master_Final_Close_Out_Oprtr_Cde;
    private DbsField master_Crprte_Clock_End_Dte_Tme;
    private DbsField master_Assgn_Sprvsr_Oprtr_Cde;
    private DbsField master_Assgn_Dte_Tme;
    private DbsField master_Last_Chnge_Unit_Cde;
    private DbsField master_Admin_Status_Updte_Dte_Tme;
    private DbsField master_Admin_Status_Updte_Oprtr_Cde;
    private DbsField master_Status_Updte_Dte_Tme;
    private DbsField master_Status_Updte_Oprtr_Cde;
    private DbsField master_Last_Updte_Dte_Tme;
    private DbsField master_Last_Updte_Oprtr_Cde;
    private DbsField master_Status_Freeze_Ind;
    private DbsField master_Status_Clock_Start_Dte_Tme;
    private DbsField master_Status_Clock_End_Dte_Tme;
    private DbsField master_Cntct_Dte_Tme;
    private DbsField master_Cntct_Invrt_Dte_Tme;
    private DbsField master_Cntct_Oprtr_Id;
    private DbsField master_Cntct_Orgn_Type_Cde;
    private DbsField master_Unit_Clock_Start_Dte_Tme;
    private DbsField master_Unit_Clock_End_Dte_Tme;
    private DbsField master_Work_List_Ind;
    private DbsField master_Crprte_On_Tme_Ind;
    private DbsField pnd_Rqst_History_Key;

    private DbsGroup pnd_Rqst_History_Key__R_Field_2;
    private DbsField pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Dte_Tme;

    private DbsGroup pnd_Rqst_History_Key__R_Field_3;
    private DbsField pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Dte;
    private DbsField pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Tme;
    private DbsField pnd_Rqst_History_Key_Pnd_Key_Last_Chnge_Invrt_Dte_Tme;
    private DbsField pnd_Actv_Unque_Key;

    private DbsGroup pnd_Actv_Unque_Key__R_Field_4;
    private DbsField pnd_Actv_Unque_Key_Pnd_Key_Rqst_Log_Dte_Tme;
    private DbsField pnd_Actv_Unque_Key_Pnd_Key_Actve_Ind;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Write_Cnt;
    private DbsField pnd_Isn;
    private DbsField pnd_Date;
    private DbsField pnd_Final_Close_Out_Dte_Tme;
    private DbsField pnd_Assgn_Dte_Tme;
    private DbsField pnd_Admin_Status_Updte_Dte_Tme;
    private DbsField pnd_Status_Updte_Dte_Tme;
    private DbsField pnd_Last_Updte_Dte_Tme;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_master = new DataAccessProgramView(new NameInfo("vw_master", "MASTER"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        master_Rqst_Log_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        master_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        master__R_Field_1 = vw_master.getRecord().newGroupInGroup("master__R_Field_1", "REDEFINE", master_Rqst_Log_Dte_Tme);
        master_Pnd_Rqst_Log_Dte = master__R_Field_1.newFieldInGroup("master_Pnd_Rqst_Log_Dte", "#RQST-LOG-DTE", FieldType.NUMERIC, 8);
        master_Pnd_Rqst_Log_Tme = master__R_Field_1.newFieldInGroup("master_Pnd_Rqst_Log_Tme", "#RQST-LOG-TME", FieldType.NUMERIC, 7);
        master_Work_Prcss_Id = vw_master.getRecord().newFieldInGroup("master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        master_Wpid_Vldte_Ind = vw_master.getRecord().newFieldInGroup("master_Wpid_Vldte_Ind", "WPID-VLDTE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "WPID_VLDTE_IND");
        master_Pin_Nbr = vw_master.getRecord().newFieldInGroup("master_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        master_Pin_Nbr.setDdmHeader("PIN");
        master_Actve_Ind = vw_master.getRecord().newFieldInGroup("master_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, "ACTVE_IND");
        master_Unit_Cde = vw_master.getRecord().newFieldInGroup("master_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "UNIT_CDE");
        master_Unit_Cde.setDdmHeader("UNIT/CODE");
        master_Orgnl_Unit_Cde = vw_master.getRecord().newFieldInGroup("master_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ORGNL_UNIT_CDE");
        master_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        master_Last_Chnge_Oprtr_Cde = vw_master.getRecord().newFieldInGroup("master_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        master_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        master_Status_Cde = vw_master.getRecord().newFieldInGroup("master_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        master_Admin_Status_Cde = vw_master.getRecord().newFieldInGroup("master_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "ADMIN_STATUS_CDE");
        master_Crprte_Status_Ind = vw_master.getRecord().newFieldInGroup("master_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_STATUS_IND");
        master_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        master_Last_Updte_Dte = vw_master.getRecord().newFieldInGroup("master_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "LAST_UPDTE_DTE");
        master_Final_Close_Out_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Final_Close_Out_Dte_Tme", "FINAL-CLOSE-OUT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_DTE_TME");
        master_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        master_Final_Close_Out_Oprtr_Cde = vw_master.getRecord().newFieldInGroup("master_Final_Close_Out_Oprtr_Cde", "FINAL-CLOSE-OUT-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_OPRTR_CDE");
        master_Crprte_Clock_End_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        master_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        master_Assgn_Sprvsr_Oprtr_Cde = vw_master.getRecord().newFieldInGroup("master_Assgn_Sprvsr_Oprtr_Cde", "ASSGN-SPRVSR-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ASSGN_SPRVSR_OPRTR_CDE");
        master_Assgn_Sprvsr_Oprtr_Cde.setDdmHeader("SUPER/VISOR");
        master_Assgn_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Assgn_Dte_Tme", "ASSGN-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ASSGN_DTE_TME");
        master_Assgn_Dte_Tme.setDdmHeader("ASSIGNMENT/DATE-TIME");
        master_Last_Chnge_Unit_Cde = vw_master.getRecord().newFieldInGroup("master_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_CHNGE_UNIT_CDE");
        master_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        master_Admin_Status_Updte_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        master_Admin_Status_Updte_Oprtr_Cde = vw_master.getRecord().newFieldInGroup("master_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        master_Status_Updte_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "STATUS_UPDTE_DTE_TME");
        master_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        master_Status_Updte_Oprtr_Cde = vw_master.getRecord().newFieldInGroup("master_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        master_Last_Updte_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LAST_UPDTE_DTE_TME");
        master_Last_Updte_Oprtr_Cde = vw_master.getRecord().newFieldInGroup("master_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        master_Status_Freeze_Ind = vw_master.getRecord().newFieldInGroup("master_Status_Freeze_Ind", "STATUS-FREEZE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "STATUS_FREEZE_IND");
        master_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        master_Status_Clock_Start_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Status_Clock_Start_Dte_Tme", "STATUS-CLOCK-START-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "STATUS_CLOCK_START_DTE_TME");
        master_Status_Clock_Start_Dte_Tme.setDdmHeader("STATUS/START CLOCK");
        master_Status_Clock_End_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Status_Clock_End_Dte_Tme", "STATUS-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "STATUS_CLOCK_END_DTE_TME");
        master_Status_Clock_End_Dte_Tme.setDdmHeader("STATUS/CLOCK END");
        master_Cntct_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Cntct_Dte_Tme", "CNTCT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, 
            "CNTCT_DTE_TME");
        master_Cntct_Invrt_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Cntct_Invrt_Dte_Tme", "CNTCT-INVRT-DTE-TME", FieldType.NUMERIC, 15, 
            RepeatingFieldStrategy.None, "CNTCT_INVRT_DTE_TME");
        master_Cntct_Oprtr_Id = vw_master.getRecord().newFieldInGroup("master_Cntct_Oprtr_Id", "CNTCT-OPRTR-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTCT_OPRTR_ID");
        master_Cntct_Orgn_Type_Cde = vw_master.getRecord().newFieldInGroup("master_Cntct_Orgn_Type_Cde", "CNTCT-ORGN-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTCT_ORGN_TYPE_CDE");
        master_Cntct_Orgn_Type_Cde.setDdmHeader("CONTACT/CODE");
        master_Unit_Clock_Start_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Unit_Clock_Start_Dte_Tme", "UNIT-CLOCK-START-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        master_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        master_Unit_Clock_End_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Unit_Clock_End_Dte_Tme", "UNIT-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        master_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        master_Work_List_Ind = vw_master.getRecord().newFieldInGroup("master_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "WORK_LIST_IND");
        master_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        master_Crprte_On_Tme_Ind = vw_master.getRecord().newFieldInGroup("master_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_ON_TME_IND");
        master_Crprte_On_Tme_Ind.setDdmHeader("WORKRQST/DUE DATE");
        registerRecord(vw_master);

        pnd_Rqst_History_Key = localVariables.newFieldInRecord("pnd_Rqst_History_Key", "#RQST-HISTORY-KEY", FieldType.STRING, 30);

        pnd_Rqst_History_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Rqst_History_Key__R_Field_2", "REDEFINE", pnd_Rqst_History_Key);
        pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Dte_Tme = pnd_Rqst_History_Key__R_Field_2.newFieldInGroup("pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Dte_Tme", 
            "#KEY-RQST-LOG-DTE-TME", FieldType.STRING, 15);

        pnd_Rqst_History_Key__R_Field_3 = pnd_Rqst_History_Key__R_Field_2.newGroupInGroup("pnd_Rqst_History_Key__R_Field_3", "REDEFINE", pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Dte_Tme);
        pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Dte = pnd_Rqst_History_Key__R_Field_3.newFieldInGroup("pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Dte", "#KEY-RQST-LOG-DTE", 
            FieldType.STRING, 8);
        pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Tme = pnd_Rqst_History_Key__R_Field_3.newFieldInGroup("pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Tme", "#KEY-RQST-LOG-TME", 
            FieldType.NUMERIC, 7);
        pnd_Rqst_History_Key_Pnd_Key_Last_Chnge_Invrt_Dte_Tme = pnd_Rqst_History_Key__R_Field_2.newFieldInGroup("pnd_Rqst_History_Key_Pnd_Key_Last_Chnge_Invrt_Dte_Tme", 
            "#KEY-LAST-CHNGE-INVRT-DTE-TME", FieldType.STRING, 15);
        pnd_Actv_Unque_Key = localVariables.newFieldInRecord("pnd_Actv_Unque_Key", "#ACTV-UNQUE-KEY", FieldType.STRING, 16);

        pnd_Actv_Unque_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Actv_Unque_Key__R_Field_4", "REDEFINE", pnd_Actv_Unque_Key);
        pnd_Actv_Unque_Key_Pnd_Key_Rqst_Log_Dte_Tme = pnd_Actv_Unque_Key__R_Field_4.newFieldInGroup("pnd_Actv_Unque_Key_Pnd_Key_Rqst_Log_Dte_Tme", "#KEY-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Actv_Unque_Key_Pnd_Key_Actve_Ind = pnd_Actv_Unque_Key__R_Field_4.newFieldInGroup("pnd_Actv_Unque_Key_Pnd_Key_Actve_Ind", "#KEY-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.NUMERIC, 9);
        pnd_Write_Cnt = localVariables.newFieldInRecord("pnd_Write_Cnt", "#WRITE-CNT", FieldType.NUMERIC, 9);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.STRING, 16);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Final_Close_Out_Dte_Tme = localVariables.newFieldInRecord("pnd_Final_Close_Out_Dte_Tme", "#FINAL-CLOSE-OUT-DTE-TME", FieldType.STRING, 15);
        pnd_Assgn_Dte_Tme = localVariables.newFieldInRecord("pnd_Assgn_Dte_Tme", "#ASSGN-DTE-TME", FieldType.STRING, 15);
        pnd_Admin_Status_Updte_Dte_Tme = localVariables.newFieldInRecord("pnd_Admin_Status_Updte_Dte_Tme", "#ADMIN-STATUS-UPDTE-DTE-TME", FieldType.STRING, 
            15);
        pnd_Status_Updte_Dte_Tme = localVariables.newFieldInRecord("pnd_Status_Updte_Dte_Tme", "#STATUS-UPDTE-DTE-TME", FieldType.STRING, 15);
        pnd_Last_Updte_Dte_Tme = localVariables.newFieldInRecord("pnd_Last_Updte_Dte_Tme", "#LAST-UPDTE-DTE-TME", FieldType.STRING, 15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_master.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb9994() throws Exception
    {
        super("Cwfb9994");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* *----------------------------------------------------------------------
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE IMMEDIATE
        //*  PICK LAST 7 DAYS WORK REQUESTS
        pnd_Date.compute(new ComputeParameters(false, pnd_Date), Global.getDATX().subtract(7));                                                                           //Natural: COMPUTE #DATE = *DATX - 7
        pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Dte.setValueEdited(pnd_Date,new ReportEditMask("YYYYMMDD"));                                                                //Natural: MOVE EDITED #DATE ( EM = YYYYMMDD ) TO #KEY-RQST-LOG-DTE
        pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Tme.setValue(0);                                                                                                            //Natural: ASSIGN #KEY-RQST-LOG-TME := 0
        pnd_Rqst_History_Key_Pnd_Key_Last_Chnge_Invrt_Dte_Tme.setValue(" ");                                                                                              //Natural: ASSIGN #KEY-LAST-CHNGE-INVRT-DTE-TME := ' '
        getReports().write(0, "Start Date =",pnd_Rqst_History_Key_Pnd_Key_Rqst_Log_Dte,"End Date =",Global.getDATX(), new ReportEditMask ("YYYYMMDD"));                   //Natural: WRITE 'Start Date =' #KEY-RQST-LOG-DTE 'End Date =' *DATX ( EM = YYYYMMDD )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rqst_History_Key);                                                                                                                  //Natural: WRITE '=' #RQST-HISTORY-KEY
        if (Global.isEscape()) return;
        vw_master.startDatabaseRead                                                                                                                                       //Natural: READ MASTER BY RQST-HISTORY-KEY STARTING FROM #RQST-HISTORY-KEY
        (
        "R1",
        new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", pnd_Rqst_History_Key, WcType.BY) },
        new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
        );
        R1:
        while (condition(vw_master.readNextRow("R1")))
        {
            if (condition(((((master_Actve_Ind.equals("A ") && DbsUtil.maskMatches(master_Unit_Cde,"'CIRS'")) && DbsUtil.maskMatches(master_Orgnl_Unit_Cde,"'CRC'"))      //Natural: IF MASTER.ACTVE-IND = 'A ' AND MASTER.UNIT-CDE EQ MASK ( 'CIRS' ) AND MASTER.ORGNL-UNIT-CDE EQ MASK ( 'CRC' ) AND ( MASTER.WORK-PRCSS-ID EQ MASK ( 'TZ YCO' ) OR MASTER.WORK-PRCSS-ID EQ MASK ( 'TZ YCC' ) ) AND MASTER.STATUS-CDE NE MASK ( '8' )
                && (DbsUtil.maskMatches(master_Work_Prcss_Id,"'TZ YCO'") || DbsUtil.maskMatches(master_Work_Prcss_Id,"'TZ YCC'"))) && ! (DbsUtil.maskMatches(master_Status_Cde,
                "'8'")))))
            {
                //*      AND R1.ADMIN-STATUS-CDE     EQ '1000'
                pnd_Read_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #READ-CNT
                pnd_Isn.setValueEdited(new ReportEditMask(""),vw_master.getAstISN("R1"));                                                                                 //Natural: MOVE EDITED *ISN ( EM = 9999999999999999 ) TO #ISN
                pnd_Final_Close_Out_Dte_Tme.setValueEdited(master_Final_Close_Out_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                         //Natural: MOVE EDITED MASTER.FINAL-CLOSE-OUT-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #FINAL-CLOSE-OUT-DTE-TME
                pnd_Assgn_Dte_Tme.setValueEdited(master_Assgn_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                             //Natural: MOVE EDITED ASSGN-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #ASSGN-DTE-TME
                pnd_Admin_Status_Updte_Dte_Tme.setValueEdited(master_Admin_Status_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                   //Natural: MOVE EDITED ADMIN-STATUS-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #ADMIN-STATUS-UPDTE-DTE-TME
                pnd_Status_Updte_Dte_Tme.setValueEdited(master_Status_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                               //Natural: MOVE EDITED STATUS-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #STATUS-UPDTE-DTE-TME
                pnd_Last_Updte_Dte_Tme.setValueEdited(master_Last_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                   //Natural: MOVE EDITED LAST-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #LAST-UPDTE-DTE-TME
                //*  (R1.)
                getWorkFiles().write(1, false, master_Pin_Nbr, ",", master_Pnd_Rqst_Log_Dte, ",", master_Pnd_Rqst_Log_Tme, ",", master_Last_Chnge_Oprtr_Cde,              //Natural: WRITE WORK FILE 1 MASTER.PIN-NBR ',' #RQST-LOG-DTE ',' #RQST-LOG-TME ',' MASTER.LAST-CHNGE-OPRTR-CDE ',' MASTER.UNIT-CDE ',' MASTER.ORGNL-UNIT-CDE ',' MASTER.WORK-PRCSS-ID ',' MASTER.STATUS-CDE ',' MASTER.ADMIN-STATUS-CDE ',' MASTER.CRPRTE-STATUS-IND ',' MASTER.WORK-LIST-IND ',' MASTER.CNTCT-ORGN-TYPE-CDE ',' MASTER.CRPRTE-ON-TME-IND ',' MASTER.WPID-VLDTE-IND ',' MASTER.STATUS-FREEZE-IND ',' MASTER.LAST-CHNGE-UNIT-CDE ',' MASTER.CNTCT-OPRTR-ID ',' MASTER.FINAL-CLOSE-OUT-OPRTR-CDE ',' MASTER.ASSGN-SPRVSR-OPRTR-CDE ',' MASTER.ADMIN-STATUS-UPDTE-OPRTR-CDE ',' MASTER.STATUS-UPDTE-OPRTR-CDE ',' MASTER.LAST-UPDTE-OPRTR-CDE ',' MASTER.FINAL-CLOSE-OUT-OPRTR-CDE ',' MASTER.STATUS-CLOCK-START-DTE-TME ',' MASTER.STATUS-CLOCK-END-DTE-TME ',' MASTER.UNIT-CLOCK-START-DTE-TME ',' MASTER.UNIT-CLOCK-END-DTE-TME ',' MASTER.LAST-UPDTE-DTE ',' MASTER.CRPRTE-CLOCK-END-DTE-TME ',' #FINAL-CLOSE-OUT-DTE-TME ',' #ASSGN-DTE-TME ',' #ADMIN-STATUS-UPDTE-DTE-TME ',' #STATUS-UPDTE-DTE-TME ',' #LAST-UPDTE-DTE-TME ',' MASTER.CNTCT-INVRT-DTE-TME ',' MASTER.CNTCT-DTE-TME ',' #ISN
                    ",", master_Unit_Cde, ",", master_Orgnl_Unit_Cde, ",", master_Work_Prcss_Id, ",", master_Status_Cde, ",", master_Admin_Status_Cde, ",", 
                    master_Crprte_Status_Ind, ",", master_Work_List_Ind, ",", master_Cntct_Orgn_Type_Cde, ",", master_Crprte_On_Tme_Ind, ",", master_Wpid_Vldte_Ind, 
                    ",", master_Status_Freeze_Ind, ",", master_Last_Chnge_Unit_Cde, ",", master_Cntct_Oprtr_Id, ",", master_Final_Close_Out_Oprtr_Cde, ",", 
                    master_Assgn_Sprvsr_Oprtr_Cde, ",", master_Admin_Status_Updte_Oprtr_Cde, ",", master_Status_Updte_Oprtr_Cde, ",", master_Last_Updte_Oprtr_Cde, 
                    ",", master_Final_Close_Out_Oprtr_Cde, ",", master_Status_Clock_Start_Dte_Tme, ",", master_Status_Clock_End_Dte_Tme, ",", master_Unit_Clock_Start_Dte_Tme, 
                    ",", master_Unit_Clock_End_Dte_Tme, ",", master_Last_Updte_Dte, ",", master_Crprte_Clock_End_Dte_Tme, ",", pnd_Final_Close_Out_Dte_Tme, 
                    ",", pnd_Assgn_Dte_Tme, ",", pnd_Admin_Status_Updte_Dte_Tme, ",", pnd_Status_Updte_Dte_Tme, ",", pnd_Last_Updte_Dte_Tme, ",", master_Cntct_Invrt_Dte_Tme, 
                    ",", master_Cntct_Dte_Tme, ",", pnd_Isn);
                G1:                                                                                                                                                       //Natural: GET MASTER *ISN
                vw_master.readByID(vw_master.getAstISN("R1"), "G1");
                master_Work_List_Ind.reset();                                                                                                                             //Natural: RESET MASTER.WORK-LIST-IND MASTER.UNIT-CLOCK-START-DTE-TME MASTER.UNIT-CLOCK-END-DTE-TME MASTER.CNTCT-OPRTR-ID MASTER.CNTCT-ORGN-TYPE-CDE MASTER.STATUS-CLOCK-START-DTE-TME MASTER.STATUS-CLOCK-END-DTE-TME
                master_Unit_Clock_Start_Dte_Tme.reset();
                master_Unit_Clock_End_Dte_Tme.reset();
                master_Cntct_Oprtr_Id.reset();
                master_Cntct_Orgn_Type_Cde.reset();
                master_Status_Clock_Start_Dte_Tme.reset();
                master_Status_Clock_End_Dte_Tme.reset();
                master_Crprte_On_Tme_Ind.setValue("L");                                                                                                                   //Natural: MOVE 'L' TO MASTER.CRPRTE-ON-TME-IND
                master_Wpid_Vldte_Ind.setValue("Y");                                                                                                                      //Natural: MOVE 'Y' TO MASTER.WPID-VLDTE-IND
                master_Last_Chnge_Unit_Cde.setValue("CIRS");                                                                                                              //Natural: MOVE 'CIRS' TO MASTER.LAST-CHNGE-UNIT-CDE
                master_Status_Cde.setValue("8461");                                                                                                                       //Natural: MOVE '8461' TO MASTER.STATUS-CDE MASTER.ADMIN-STATUS-CDE
                master_Admin_Status_Cde.setValue("8461");
                master_Crprte_Status_Ind.setValue(9);                                                                                                                     //Natural: MOVE 9 TO MASTER.CRPRTE-STATUS-IND
                master_Final_Close_Out_Oprtr_Cde.setValue("BATCHSYN");                                                                                                    //Natural: MOVE 'BATCHSYN' TO MASTER.FINAL-CLOSE-OUT-OPRTR-CDE MASTER.ASSGN-SPRVSR-OPRTR-CDE MASTER.ADMIN-STATUS-UPDTE-OPRTR-CDE MASTER.STATUS-UPDTE-OPRTR-CDE MASTER.LAST-UPDTE-OPRTR-CDE MASTER.FINAL-CLOSE-OUT-OPRTR-CDE MASTER.LAST-CHNGE-OPRTR-CDE
                master_Assgn_Sprvsr_Oprtr_Cde.setValue("BATCHSYN");
                master_Admin_Status_Updte_Oprtr_Cde.setValue("BATCHSYN");
                master_Status_Updte_Oprtr_Cde.setValue("BATCHSYN");
                master_Last_Updte_Oprtr_Cde.setValue("BATCHSYN");
                master_Final_Close_Out_Oprtr_Cde.setValue("BATCHSYN");
                master_Last_Chnge_Oprtr_Cde.setValue("BATCHSYN");
                master_Last_Updte_Dte.setValue(Global.getDATN());                                                                                                         //Natural: MOVE *DATN TO MASTER.LAST-UPDTE-DTE
                master_Crprte_Clock_End_Dte_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                                   //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO MASTER.CRPRTE-CLOCK-END-DTE-TME
                //*    MOVE EDITED *TIMX (EM=YYYYMMDDHHIISST)
                //*                                  TO R1.STATUS-CLOCK-START-DTE-TME
                //*    MOVE '999999999999999'        TO R1.STATUS-CLOCK-END-DTE-TME
                master_Final_Close_Out_Dte_Tme.setValue(Global.getTIMX());                                                                                                //Natural: MOVE *TIMX TO MASTER.FINAL-CLOSE-OUT-DTE-TME MASTER.ASSGN-DTE-TME MASTER.ADMIN-STATUS-UPDTE-DTE-TME MASTER.STATUS-UPDTE-DTE-TME MASTER.LAST-UPDTE-DTE-TME
                master_Assgn_Dte_Tme.setValue(Global.getTIMX());
                master_Admin_Status_Updte_Dte_Tme.setValue(Global.getTIMX());
                master_Status_Updte_Dte_Tme.setValue(Global.getTIMX());
                master_Last_Updte_Dte_Tme.setValue(Global.getTIMX());
                master_Status_Freeze_Ind.setValue("Y");                                                                                                                   //Natural: MOVE 'Y' TO MASTER.STATUS-FREEZE-IND
                master_Cntct_Invrt_Dte_Tme.setValue(0);                                                                                                                   //Natural: MOVE 0 TO MASTER.CNTCT-INVRT-DTE-TME MASTER.CNTCT-DTE-TME
                master_Cntct_Dte_Tme.setValue(0);
                vw_master.updateDBRow("G1");                                                                                                                              //Natural: UPDATE ( G1. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Final_Close_Out_Dte_Tme.setValueEdited(master_Final_Close_Out_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                         //Natural: MOVE EDITED MASTER.FINAL-CLOSE-OUT-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #FINAL-CLOSE-OUT-DTE-TME
                pnd_Assgn_Dte_Tme.setValueEdited(master_Assgn_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                             //Natural: MOVE EDITED ASSGN-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #ASSGN-DTE-TME
                pnd_Admin_Status_Updte_Dte_Tme.setValueEdited(master_Admin_Status_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                   //Natural: MOVE EDITED ADMIN-STATUS-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #ADMIN-STATUS-UPDTE-DTE-TME
                pnd_Status_Updte_Dte_Tme.setValueEdited(master_Status_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                               //Natural: MOVE EDITED STATUS-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #STATUS-UPDTE-DTE-TME
                pnd_Last_Updte_Dte_Tme.setValueEdited(master_Last_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                   //Natural: MOVE EDITED LAST-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #LAST-UPDTE-DTE-TME
                //*  (R1.)
                getWorkFiles().write(1, false, master_Pin_Nbr, ",", master_Pnd_Rqst_Log_Dte, ",", master_Pnd_Rqst_Log_Tme, ",", master_Last_Chnge_Oprtr_Cde,              //Natural: WRITE WORK FILE 1 MASTER.PIN-NBR ',' #RQST-LOG-DTE ',' #RQST-LOG-TME ',' MASTER.LAST-CHNGE-OPRTR-CDE ',' MASTER.UNIT-CDE ',' MASTER.ORGNL-UNIT-CDE ',' MASTER.WORK-PRCSS-ID ',' MASTER.STATUS-CDE ',' MASTER.ADMIN-STATUS-CDE ',' MASTER.CRPRTE-STATUS-IND ',' MASTER.WORK-LIST-IND ',' MASTER.CNTCT-ORGN-TYPE-CDE ',' MASTER.CRPRTE-ON-TME-IND ',' MASTER.WPID-VLDTE-IND ',' MASTER.STATUS-FREEZE-IND ',' MASTER.LAST-CHNGE-UNIT-CDE ',' MASTER.CNTCT-OPRTR-ID ',' MASTER.FINAL-CLOSE-OUT-OPRTR-CDE ',' MASTER.ASSGN-SPRVSR-OPRTR-CDE ',' MASTER.ADMIN-STATUS-UPDTE-OPRTR-CDE ',' MASTER.STATUS-UPDTE-OPRTR-CDE ',' MASTER.LAST-UPDTE-OPRTR-CDE ',' MASTER.FINAL-CLOSE-OUT-OPRTR-CDE ',' MASTER.STATUS-CLOCK-START-DTE-TME ',' MASTER.STATUS-CLOCK-END-DTE-TME ',' MASTER.UNIT-CLOCK-START-DTE-TME ',' MASTER.UNIT-CLOCK-END-DTE-TME ',' MASTER.LAST-UPDTE-DTE ',' MASTER.CRPRTE-CLOCK-END-DTE-TME ',' #FINAL-CLOSE-OUT-DTE-TME ',' #ASSGN-DTE-TME ',' #ADMIN-STATUS-UPDTE-DTE-TME ',' #STATUS-UPDTE-DTE-TME ',' #LAST-UPDTE-DTE-TME ',' MASTER.CNTCT-INVRT-DTE-TME ',' MASTER.CNTCT-DTE-TME ',' #ISN
                    ",", master_Unit_Cde, ",", master_Orgnl_Unit_Cde, ",", master_Work_Prcss_Id, ",", master_Status_Cde, ",", master_Admin_Status_Cde, ",", 
                    master_Crprte_Status_Ind, ",", master_Work_List_Ind, ",", master_Cntct_Orgn_Type_Cde, ",", master_Crprte_On_Tme_Ind, ",", master_Wpid_Vldte_Ind, 
                    ",", master_Status_Freeze_Ind, ",", master_Last_Chnge_Unit_Cde, ",", master_Cntct_Oprtr_Id, ",", master_Final_Close_Out_Oprtr_Cde, ",", 
                    master_Assgn_Sprvsr_Oprtr_Cde, ",", master_Admin_Status_Updte_Oprtr_Cde, ",", master_Status_Updte_Oprtr_Cde, ",", master_Last_Updte_Oprtr_Cde, 
                    ",", master_Final_Close_Out_Oprtr_Cde, ",", master_Status_Clock_Start_Dte_Tme, ",", master_Status_Clock_End_Dte_Tme, ",", master_Unit_Clock_Start_Dte_Tme, 
                    ",", master_Unit_Clock_End_Dte_Tme, ",", master_Last_Updte_Dte, ",", master_Crprte_Clock_End_Dte_Tme, ",", pnd_Final_Close_Out_Dte_Tme, 
                    ",", pnd_Assgn_Dte_Tme, ",", pnd_Admin_Status_Updte_Dte_Tme, ",", pnd_Status_Updte_Dte_Tme, ",", pnd_Last_Updte_Dte_Tme, ",", master_Cntct_Invrt_Dte_Tme, 
                    ",", master_Cntct_Dte_Tme, ",", pnd_Isn);
                pnd_Write_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #WRITE-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "Records Read    =",pnd_Read_Cnt,NEWLINE,"Records Updated =",pnd_Write_Cnt);                                                                //Natural: WRITE 'Records Read    =' #READ-CNT / 'Records Updated =' #WRITE-CNT
        if (Global.isEscape()) return;
    }

    //
}
