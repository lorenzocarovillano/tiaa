/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:38:51 PM
**        * FROM NATURAL PROGRAM : Cwfb5390
************************************************************
**        * FILE NAME            : Cwfb5390.java
**        * CLASS NAME           : Cwfb5390
**        * INSTANCE NAME        : Cwfb5390
************************************************************
************************************************************************
* PROGRAM  : CWFB5390
* SYSTEM   : CRPCWF
* FUNCTION : RQST LOGGED BY CRC WITH CHECKS FOR A CERTAIN DAY
*          |
* 02/07/95 | OB TOOK OUT INPUT STATEMENT/PARAMETER, USE SYSTEM DATE AS
*          |     PARAMETER
* 08/16/95 | OB READ COR INSTEAD OF PIF
* 02/23/2017 - PIN-EXP - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb5390 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_wpid_Tbl;
    private DbsField wpid_Tbl_Work_Prcss_Id;
    private DbsField wpid_Tbl_Work_Prcss_Short_Nme;

    private DataAccessProgramView vw_master1;
    private DbsField master1_Rqst_Log_Dte_Tme;
    private DbsField master1_Admin_Unit_Cde;

    private DataAccessProgramView vw_master;
    private DbsField master_Rqst_Log_Dte_Tme;

    private DbsGroup master__R_Field_1;
    private DbsField master_Rqst_Log_Dte;
    private DbsField master_Rqst_Orgn_Cde;
    private DbsField master_Pin_Nbr;
    private DbsField master_Case_Id_Cde;
    private DbsField master_Tiaa_Rcvd_Dte;
    private DbsField master_Mstr_Indx_Actn_Cde;
    private DbsField master_Status_Cde;
    private DbsField master_Admin_Status_Cde;
    private DbsField master_Work_Prcss_Id;
    private DbsField master_Print_Batch_Id_Nbr;
    private DbsField master_Rqst_Log_Oprtr_Cde;
    private DbsField master_Orgnl_Unit_Cde;
    private DbsField master_Last_Chnge_Unit_Cde;
    private DbsField master_Crprte_Status_Ind;
    private DbsField master_Check_Ind;
    private DbsField master_Spcl_Hndlng_Txt;

    private DbsGroup master__R_Field_2;
    private DbsField master_Pnd_Spcl_Txt;
    private DbsField master_Pnd_Spcl_Ssn;
    private DbsField master_Pnd_Spcl_Nme;

    private DbsGroup pnd_Vars;
    private DbsField pnd_Vars_Pnd_Ctr;
    private DbsField pnd_Vars_Pnd_Ctr1;
    private DbsField pnd_Vars_Pnd_Day;
    private DbsField pnd_Vars_Pnd_Dte;
    private DbsField pnd_Vars_Pnd_Input_Parm;

    private DbsGroup pnd_Vars__R_Field_3;
    private DbsField pnd_Vars_Pnd_Ip_Yy;
    private DbsField pnd_Vars_Pnd_Ip_Mm;
    private DbsField pnd_Vars_Pnd_Ip_Dd;
    private DbsField pnd_Vars_Pnd_Ip_Days;
    private DbsField pnd_Vars_Pnd_Msg;
    private DbsField pnd_Vars_Pnd_O_C;
    private DbsField pnd_Vars_Pnd_Ph_Name;
    private DbsField pnd_Vars_Pnd_Ph_Ssn;
    private DbsField pnd_Vars_Pnd_Ph_Tlc;
    private DbsField pnd_Vars_Pnd_Prev_Ld;
    private DbsField pnd_Vars_Pnd_Rldt;

    private DbsGroup pnd_Vars__R_Field_4;
    private DbsField pnd_Vars_Pnd_Rldt_Yyyymmdd;

    private DbsGroup pnd_Vars__R_Field_5;
    private DbsField pnd_Vars_Pnd_Rldt_Cc;
    private DbsField pnd_Vars_Pnd_Rldt_Yy;
    private DbsField pnd_Vars_Pnd_Rldt_Mm;
    private DbsField pnd_Vars_Pnd_Rldt_Dd;
    private DbsField pnd_Vars_Pnd_Rld_Max;
    private DbsField pnd_Vars_Pnd_Rldt_Max;

    private DbsGroup pnd_Vars__R_Field_6;
    private DbsField pnd_Vars_Pnd_Rldt_Max_Yyyymmdd;
    private DbsField pnd_Record;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_wpid_Tbl = new DataAccessProgramView(new NameInfo("vw_wpid_Tbl", "WPID-TBL"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        wpid_Tbl_Work_Prcss_Id = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        wpid_Tbl_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        wpid_Tbl_Work_Prcss_Short_Nme = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        wpid_Tbl_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        registerRecord(vw_wpid_Tbl);

        vw_master1 = new DataAccessProgramView(new NameInfo("vw_master1", "MASTER1"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        master1_Rqst_Log_Dte_Tme = vw_master1.getRecord().newFieldInGroup("master1_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        master1_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        master1_Admin_Unit_Cde = vw_master1.getRecord().newFieldInGroup("master1_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        registerRecord(vw_master1);

        vw_master = new DataAccessProgramView(new NameInfo("vw_master", "MASTER"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        master_Rqst_Log_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        master_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        master__R_Field_1 = vw_master.getRecord().newGroupInGroup("master__R_Field_1", "REDEFINE", master_Rqst_Log_Dte_Tme);
        master_Rqst_Log_Dte = master__R_Field_1.newFieldInGroup("master_Rqst_Log_Dte", "RQST-LOG-DTE", FieldType.STRING, 8);
        master_Rqst_Orgn_Cde = vw_master.getRecord().newFieldInGroup("master_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_ORGN_CDE");
        master_Pin_Nbr = vw_master.getRecord().newFieldInGroup("master_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        master_Pin_Nbr.setDdmHeader("PIN");
        master_Case_Id_Cde = vw_master.getRecord().newFieldInGroup("master_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CASE_ID_CDE");
        master_Case_Id_Cde.setDdmHeader("CASE/ID");
        master_Tiaa_Rcvd_Dte = vw_master.getRecord().newFieldInGroup("master_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        master_Mstr_Indx_Actn_Cde = vw_master.getRecord().newFieldInGroup("master_Mstr_Indx_Actn_Cde", "MSTR-INDX-ACTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "MSTR_INDX_ACTN_CDE");
        master_Mstr_Indx_Actn_Cde.setDdmHeader("CHARGEOUT/ACTION CODE");
        master_Status_Cde = vw_master.getRecord().newFieldInGroup("master_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        master_Admin_Status_Cde = vw_master.getRecord().newFieldInGroup("master_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "ADMIN_STATUS_CDE");
        master_Work_Prcss_Id = vw_master.getRecord().newFieldInGroup("master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        master_Print_Batch_Id_Nbr = vw_master.getRecord().newFieldInGroup("master_Print_Batch_Id_Nbr", "PRINT-BATCH-ID-NBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "PRINT_BATCH_ID_NBR");
        master_Rqst_Log_Oprtr_Cde = vw_master.getRecord().newFieldInGroup("master_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_LOG_OPRTR_CDE");
        master_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        master_Orgnl_Unit_Cde = vw_master.getRecord().newFieldInGroup("master_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ORGNL_UNIT_CDE");
        master_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        master_Last_Chnge_Unit_Cde = vw_master.getRecord().newFieldInGroup("master_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_CHNGE_UNIT_CDE");
        master_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        master_Crprte_Status_Ind = vw_master.getRecord().newFieldInGroup("master_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_STATUS_IND");
        master_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        master_Check_Ind = vw_master.getRecord().newFieldInGroup("master_Check_Ind", "CHECK-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CHECK_IND");
        master_Check_Ind.setDdmHeader("CHK");
        master_Spcl_Hndlng_Txt = vw_master.getRecord().newFieldInGroup("master_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", FieldType.STRING, 72, RepeatingFieldStrategy.None, 
            "SPCL_HNDLNG_TXT");
        master_Spcl_Hndlng_Txt.setDdmHeader("MESSAGES");

        master__R_Field_2 = vw_master.getRecord().newGroupInGroup("master__R_Field_2", "REDEFINE", master_Spcl_Hndlng_Txt);
        master_Pnd_Spcl_Txt = master__R_Field_2.newFieldInGroup("master_Pnd_Spcl_Txt", "#SPCL-TXT", FieldType.STRING, 45);
        master_Pnd_Spcl_Ssn = master__R_Field_2.newFieldInGroup("master_Pnd_Spcl_Ssn", "#SPCL-SSN", FieldType.STRING, 9);
        master_Pnd_Spcl_Nme = master__R_Field_2.newFieldInGroup("master_Pnd_Spcl_Nme", "#SPCL-NME", FieldType.STRING, 18);
        registerRecord(vw_master);

        pnd_Vars = localVariables.newGroupInRecord("pnd_Vars", "#VARS");
        pnd_Vars_Pnd_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ctr", "#CTR", FieldType.NUMERIC, 8);
        pnd_Vars_Pnd_Ctr1 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ctr1", "#CTR1", FieldType.NUMERIC, 8);
        pnd_Vars_Pnd_Day = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Day", "#DAY", FieldType.STRING, 9);
        pnd_Vars_Pnd_Dte = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Dte", "#DTE", FieldType.DATE);
        pnd_Vars_Pnd_Input_Parm = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 8);

        pnd_Vars__R_Field_3 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_3", "REDEFINE", pnd_Vars_Pnd_Input_Parm);
        pnd_Vars_Pnd_Ip_Yy = pnd_Vars__R_Field_3.newFieldInGroup("pnd_Vars_Pnd_Ip_Yy", "#IP-YY", FieldType.STRING, 2);
        pnd_Vars_Pnd_Ip_Mm = pnd_Vars__R_Field_3.newFieldInGroup("pnd_Vars_Pnd_Ip_Mm", "#IP-MM", FieldType.STRING, 2);
        pnd_Vars_Pnd_Ip_Dd = pnd_Vars__R_Field_3.newFieldInGroup("pnd_Vars_Pnd_Ip_Dd", "#IP-DD", FieldType.STRING, 2);
        pnd_Vars_Pnd_Ip_Days = pnd_Vars__R_Field_3.newFieldInGroup("pnd_Vars_Pnd_Ip_Days", "#IP-DAYS", FieldType.NUMERIC, 2);
        pnd_Vars_Pnd_Msg = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Msg", "#MSG", FieldType.STRING, 60);
        pnd_Vars_Pnd_O_C = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_O_C", "#O-C", FieldType.STRING, 7);
        pnd_Vars_Pnd_Ph_Name = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ph_Name", "#PH-NAME", FieldType.STRING, 30);
        pnd_Vars_Pnd_Ph_Ssn = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ph_Ssn", "#PH-SSN", FieldType.NUMERIC, 9);
        pnd_Vars_Pnd_Ph_Tlc = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ph_Tlc", "#PH-TLC", FieldType.STRING, 7);
        pnd_Vars_Pnd_Prev_Ld = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Prev_Ld", "#PREV-LD", FieldType.STRING, 8);
        pnd_Vars_Pnd_Rldt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Rldt", "#RLDT", FieldType.STRING, 15);

        pnd_Vars__R_Field_4 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_4", "REDEFINE", pnd_Vars_Pnd_Rldt);
        pnd_Vars_Pnd_Rldt_Yyyymmdd = pnd_Vars__R_Field_4.newFieldInGroup("pnd_Vars_Pnd_Rldt_Yyyymmdd", "#RLDT-YYYYMMDD", FieldType.STRING, 8);

        pnd_Vars__R_Field_5 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_5", "REDEFINE", pnd_Vars_Pnd_Rldt);
        pnd_Vars_Pnd_Rldt_Cc = pnd_Vars__R_Field_5.newFieldInGroup("pnd_Vars_Pnd_Rldt_Cc", "#RLDT-CC", FieldType.STRING, 2);
        pnd_Vars_Pnd_Rldt_Yy = pnd_Vars__R_Field_5.newFieldInGroup("pnd_Vars_Pnd_Rldt_Yy", "#RLDT-YY", FieldType.STRING, 2);
        pnd_Vars_Pnd_Rldt_Mm = pnd_Vars__R_Field_5.newFieldInGroup("pnd_Vars_Pnd_Rldt_Mm", "#RLDT-MM", FieldType.STRING, 2);
        pnd_Vars_Pnd_Rldt_Dd = pnd_Vars__R_Field_5.newFieldInGroup("pnd_Vars_Pnd_Rldt_Dd", "#RLDT-DD", FieldType.STRING, 2);
        pnd_Vars_Pnd_Rld_Max = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Rld_Max", "#RLD-MAX", FieldType.STRING, 8);
        pnd_Vars_Pnd_Rldt_Max = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Rldt_Max", "#RLDT-MAX", FieldType.STRING, 15);

        pnd_Vars__R_Field_6 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_6", "REDEFINE", pnd_Vars_Pnd_Rldt_Max);
        pnd_Vars_Pnd_Rldt_Max_Yyyymmdd = pnd_Vars__R_Field_6.newFieldInGroup("pnd_Vars_Pnd_Rldt_Max_Yyyymmdd", "#RLDT-MAX-YYYYMMDD", FieldType.STRING, 
            8);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_wpid_Tbl.reset();
        vw_master1.reset();
        vw_master.reset();

        localVariables.reset();
        pnd_Vars_Pnd_Rldt.setInitialValue("000000000000000");
        pnd_Vars_Pnd_Rldt_Max.setInitialValue("000000000000000");
        pnd_Record.setInitialValue("�E�&l1o2a5.6c72p3e66F�&a2L�(9905X�&l0L");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb5390() throws Exception
    {
        super("Cwfb5390");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        st = Global.getTIMN();                                                                                                                                            //Natural: SET TIME
        getReports().getPageNumberDbs(1).reset();                                                                                                                         //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: RESET *PAGE-NUMBER ( 1 )
        //* *** 02/07/95 OB
        pnd_Vars_Pnd_Day.setValueEdited(Global.getDATX(),new ReportEditMask("NNNNNNNNN"));                                                                                //Natural: MOVE EDITED *DATX ( EM = N ( 9 ) ) TO #DAY
        pnd_Vars_Pnd_Dte.setValue(Global.getDATX());                                                                                                                      //Natural: MOVE *DATX TO #DTE
        short decideConditionsMet84 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE OF #DAY;//Natural: VALUE 'Saturday'
        if (condition((pnd_Vars_Pnd_Day.equals("Saturday"))))
        {
            decideConditionsMet84++;
            pnd_Vars_Pnd_Dte.nsubtract(5);                                                                                                                                //Natural: SUBTRACT 5 FROM #DTE
        }                                                                                                                                                                 //Natural: VALUE 'Sunday'
        else if (condition((pnd_Vars_Pnd_Day.equals("Sunday"))))
        {
            decideConditionsMet84++;
            pnd_Vars_Pnd_Dte.nsubtract(6);                                                                                                                                //Natural: SUBTRACT 6 FROM #DTE
        }                                                                                                                                                                 //Natural: VALUE 'Monday'
        else if (condition((pnd_Vars_Pnd_Day.equals("Monday"))))
        {
            decideConditionsMet84++;
            pnd_Vars_Pnd_Dte.nsubtract(7);                                                                                                                                //Natural: SUBTRACT 7 FROM #DTE
        }                                                                                                                                                                 //Natural: VALUE 'Tuesday'
        else if (condition((pnd_Vars_Pnd_Day.equals("Tuesday"))))
        {
            decideConditionsMet84++;
            pnd_Vars_Pnd_Dte.nsubtract(8);                                                                                                                                //Natural: SUBTRACT 8 FROM #DTE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Vars_Pnd_Dte.nsubtract(6);                                                                                                                                //Natural: SUBTRACT 6 FROM #DTE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* ***
        pnd_Vars_Pnd_Rldt_Yyyymmdd.setValueEdited(pnd_Vars_Pnd_Dte,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED #DTE ( EM = YYYYMMDD ) TO #RLDT-YYYYMMDD
        //*  ADD #IP-DAYS TO #DTE
        pnd_Vars_Pnd_Dte.nadd(7);                                                                                                                                         //Natural: ADD 7 TO #DTE
        pnd_Vars_Pnd_Rldt_Max_Yyyymmdd.setValueEdited(pnd_Vars_Pnd_Dte,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED #DTE ( EM = YYYYMMDD ) TO #RLDT-MAX-YYYYMMDD
        pnd_Vars_Pnd_Dte.nsubtract(1);                                                                                                                                    //Natural: SUBTRACT 1 FROM #DTE
        pnd_Vars_Pnd_Rld_Max.setValueEdited(pnd_Vars_Pnd_Dte,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED #DTE ( EM = YYYYMMDD ) TO #RLD-MAX
        vw_master.startDatabaseRead                                                                                                                                       //Natural: READ MASTER BY ACTV-UNQUE-KEY FROM #RLDT
        (
        "READ01",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pnd_Vars_Pnd_Rldt, WcType.BY) },
        new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_master.readNextRow("READ01")))
        {
            if (condition(master_Rqst_Log_Dte_Tme.greater(pnd_Vars_Pnd_Rldt_Max)))                                                                                        //Natural: IF MASTER.RQST-LOG-DTE-TME GT #RLDT-MAX
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Ctr1.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CTR1
            if (condition(master_Check_Ind.notEquals("Y")))                                                                                                               //Natural: REJECT IF MASTER.CHECK-IND NE 'Y'
            {
                continue;
            }
            if (condition(master_Orgnl_Unit_Cde.notEquals("CRC")))                                                                                                        //Natural: REJECT IF MASTER.ORGNL-UNIT-CDE NE 'CRC'
            {
                continue;
            }
            if (condition(master_Rqst_Log_Dte.notEquals(pnd_Vars_Pnd_Prev_Ld)))                                                                                           //Natural: IF MASTER.RQST-LOG-DTE NE #PREV-LD
            {
                pnd_Vars_Pnd_Prev_Ld.setValue(master_Rqst_Log_Dte);                                                                                                       //Natural: MOVE MASTER.RQST-LOG-DTE TO #PREV-LD
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master_Crprte_Status_Ind.equals("9")))                                                                                                          //Natural: IF MASTER.CRPRTE-STATUS-IND EQ '9'
            {
                pnd_Vars_Pnd_O_C.setValue("Closed");                                                                                                                      //Natural: MOVE 'Closed' TO #O-C
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Vars_Pnd_O_C.setValue("Open");                                                                                                                        //Natural: MOVE 'Open' TO #O-C
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master_Pin_Nbr.notEquals(getZero()) && master_Pin_Nbr.notEquals(9999999)))                                                                      //Natural: IF MASTER.PIN-NBR NE 0 AND MASTER.PIN-NBR NE 9999999
            {
                //*    FETCH RETURN 'MDMP0011'
                //*    CALLNAT 'CWFN5371'
                //*             MASTER.PIN-NBR
                //*             #PH-NAME
                //*             #PH-SSN
                //*             #PH-TLC
                //*    FETCH RETURN 'MDMP0012'
                master_Pnd_Spcl_Nme.setValue(pnd_Vars_Pnd_Ph_Name);                                                                                                       //Natural: MOVE #PH-NAME TO #SPCL-NME
            }                                                                                                                                                             //Natural: END-IF
            vw_master1.startDatabaseRead                                                                                                                                  //Natural: READ ( 01 ) MASTER1 BY RQST-ROUTING-KEY FROM MASTER.RQST-LOG-DTE-TME
            (
            "READ02",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", master_Rqst_Log_Dte_Tme, WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
            01
            );
            READ02:
            while (condition(vw_master1.readNextRow("READ02")))
            {
                if (condition(master1_Rqst_Log_Dte_Tme.notEquals(master_Rqst_Log_Dte_Tme)))                                                                               //Natural: IF MASTER1.RQST-LOG-DTE-TME NE MASTER.RQST-LOG-DTE-TME
                {
                    master1_Admin_Unit_Cde.setValue(" ");                                                                                                                 //Natural: MOVE ' ' TO MASTER1.ADMIN-UNIT-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            vw_wpid_Tbl.startDatabaseRead                                                                                                                                 //Natural: READ ( 01 ) WPID-TBL BY WPID-UNIQ-KEY FROM MASTER.WORK-PRCSS-ID
            (
            "READ03",
            new Wc[] { new Wc("WPID_UNIQ_KEY", ">=", master_Work_Prcss_Id, WcType.BY) },
            new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") },
            01
            );
            READ03:
            while (condition(vw_wpid_Tbl.readNextRow("READ03")))
            {
                if (condition(wpid_Tbl_Work_Prcss_Id.notEquals(master_Work_Prcss_Id)))                                                                                    //Natural: IF WPID-TBL.WORK-PRCSS-ID NE MASTER.WORK-PRCSS-ID
                {
                    wpid_Tbl_Work_Prcss_Short_Nme.setValue(" ");                                                                                                          //Natural: MOVE ' ' TO WPID-TBL.WORK-PRCSS-SHORT-NME
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Vars_Pnd_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CTR
            //*  PIN-EXP
            //*  PIN-EXP
            //*  PIN-EXP
            //*  PIN-EXP
            //*  PIN-EXP
            //*  PIN-EXP
            //*  PIN-EXP
            getReports().write(1, pnd_Vars_Pnd_Ctr, new ReportEditMask ("ZZZ9"),new TabSetting(8),master_Rqst_Log_Dte,new TabSetting(18),master_Pnd_Spcl_Nme,new          //Natural: WRITE ( 1 ) #CTR ( EM = ZZZ9 ) 8T MASTER.RQST-LOG-DTE 18T MASTER.#SPCL-NME 38T MASTER.PIN-NBR 52T MASTER.TIAA-RCVD-DTE ( EM = MM/DD/YY ) 62T MASTER.RQST-LOG-OPRTR-CDE 72T WPID-TBL.WORK-PRCSS-SHORT-NME 101T MASTER1.ADMIN-UNIT-CDE 110T MASTER.STATUS-CDE 119T MASTER.ADMIN-STATUS-CDE 126T #O-C
                TabSetting(38),master_Pin_Nbr,new TabSetting(52),master_Tiaa_Rcvd_Dte, new ReportEditMask ("MM/DD/YY"),new TabSetting(62),master_Rqst_Log_Oprtr_Cde,new 
                TabSetting(72),wpid_Tbl_Work_Prcss_Short_Nme,new TabSetting(101),master1_Admin_Unit_Cde,new TabSetting(110),master_Status_Cde,new TabSetting(119),master_Admin_Status_Cde,new 
                TabSetting(126),pnd_Vars_Pnd_O_C);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    47T MASTER.TIAA-RCVD-DTE (EM=MM/DD/YY)
            //*    57T MASTER.RQST-LOG-OPRTR-CDE
            //*    67T WPID-TBL.WORK-PRCSS-SHORT-NME
            //*    96T MASTER1.ADMIN-UNIT-CDE
            //*    105T MASTER.STATUS-CDE
            //*    114T MASTER.ADMIN-STATUS-CDE
            //*    121T #O-C
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        getReports().write(1, NEWLINE,NEWLINE,"Total records read:",pnd_Vars_Pnd_Ctr1);                                                                                   //Natural: WRITE ( 1 ) // 'Total records read:' #CTR1
        if (Global.isEscape()) return;
        getReports().write(1, "Time Elapsed......:",st, new ReportEditMask ("99':'99':'99'.'9"));                                                                         //Natural: WRITE ( 1 ) 'Time Elapsed......:' *TIMD ( ST. ) ( EM = 99':'99':'99'.'9 )
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().getPageNumberDbs(1).reset();                                                                                                                         //Natural: RESET *PAGE-NUMBER ( 1 )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(0, pnd_Record);                                                                                                                    //Natural: WRITE #RECORD
                    getReports().write(1, "      Request logged by CRC with checks from",pnd_Vars_Pnd_Rldt_Yyyymmdd,"to",pnd_Vars_Pnd_Rld_Max);                           //Natural: WRITE ( 1 ) '      Request logged by CRC with checks from' #RLDT-YYYYMMDD 'to' #RLD-MAX
                    getReports().write(1, NEWLINE,new TabSetting(8),"Log  Dte",new TabSetting(18),"   Name ",new TabSetting(38),"  PIN",new TabSetting(47),"TIAA Dte",new //Natural: WRITE ( 1 ) / 8T 'Log  Dte' 18T '   Name ' 38T '  PIN' 47T 'TIAA Dte' 57T 'LoggedBy' 67T 'WPID Short Name' 94T 'InitUnit' 104T 'Status' 112T 'AdmStat' 121T 'C/O' /
                        TabSetting(57),"LoggedBy",new TabSetting(67),"WPID Short Name",new TabSetting(94),"InitUnit",new TabSetting(104),"Status",new TabSetting(112),"AdmStat",new 
                        TabSetting(121),"C/O",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
