/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:28:37 PM
**        * FROM NATURAL PROGRAM : Cwfb3207
************************************************************
**        * FILE NAME            : Cwfb3207.java
**        * CLASS NAME           : Cwfb3207
**        * INSTANCE NAME        : Cwfb3207
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: SRA TXNS FROM SRA REQUEST
**SAG SYSTEM: CRPCWF
**SAG REPORT-HEADING(1): SRA REQUESTS WITH NO TXNS
**SAG PRINT-FILE(1): 1
**SAG REPORT-HEADING(2): EXTRACT CREATE REPORT
**SAG PRINT-FILE(2): 1
**SAG REPORT-HEADING(3): SRA REQUESTS WITH TXNS EXTRACT CREATE REPORT
**SAG PRINT-FILE(3): 2
**SAG HEADING-LINE: 12100000
**SAG DESCS(1): THIS PROGRAM READS THE CWF-MASTER-INDEX-VIEW BY CRPRTE-
**SAG DESCS(2): CLOSED-DATE AND CREATES EXTACT OF SRA REQUESTS WITH
**SAG DESCS(3): TXNS RECIEVED SINCE IT WAS CLOSED AND SRA REQUESTS WITH
**SAG DESCS(4): NO TXNS RECEIVED SINCE IT WAS CLOSED.
**SAG PRIMARY-FILE: CWF-MASTER-INDEX-VIEW
**SAG PRIMARY-KEY: PRTCPNT-CLOSED-KEY
************************************************************************
* PROGRAM  : CWFB3207
* SYSTEM   : CRPCWF
* TITLE    : SRA TXNS FROM SRA REQUEST
* GENERATED: APR 03,96 AT 02:49 PM
* FUNCTION : THIS PROGRAM READS THE CWF-MASTER-INDEX-VIEW BY CRPRTE-
*            CLOSED-DATE AND CREATES EXTACT OF SRA REQUESTS WITH
*            TXNS RECIEVED SINCE IT WAS CLOSED AND SRA REQUESTS WITH
*            NO TXNS RECEIVED SINCE IT WAS CLOSED.
* HISTORY
**SAG DEFINE EXIT CHANGE-HISTORY
* CHANGED ON SEP 26,95 BY HARGRAV FOR RELEASE ____
* CHANGED IN OCT 20,95 BY JAMIE HARGRAVE - CHECK FOR CASES CLOSED 3
* > JAN 14 1997 JVH. ADD WPID's BASB, BABC
* >
* >
**SAG END-EXIT
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3207 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private PdaCwfa5372 pdaCwfa5372;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_1;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Rgn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Brnch_Cde;
    private DbsField cwf_Master_Index_View_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Case_Id_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_2;
    private DbsField cwf_Master_Index_View_Case_Ind;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind;
    private DbsField cwf_Master_Index_View_Multi_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_3;
    private DbsField cwf_Master_Index_View_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Wpid_Vldte_Ind;
    private DbsField cwf_Master_Index_View_Unit_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_4;
    private DbsField cwf_Master_Index_View_Unit_Id_Cde;
    private DbsField cwf_Master_Index_View_Unit_Rgn_Cde;
    private DbsField cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_View_Unit_Brnch_Group_Cde;
    private DbsField cwf_Master_Index_View_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Old_Route_Cde;
    private DbsField cwf_Master_Index_View_Work_Rqst_Prty_Cde;
    private DbsField cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Assgn_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_5;
    private DbsField cwf_Master_Index_View_Empl_Racf_Id;
    private DbsField cwf_Master_Index_View_Empl_Sffx_Cde;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_6;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_Tme_8_15;
    private DbsField cwf_Master_Index_View_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_View_Step_Id;
    private DbsField cwf_Master_Index_View_Rt_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Step_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Step_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Status_Cde;
    private DbsField cwf_Master_Index_View_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Updte_Dte;
    private DbsField cwf_Master_Index_View_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Cntct_Orgn_Type_Cde;
    private DbsField cwf_Master_Index_View_Cntct_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Oprtr_Id;
    private DbsField cwf_Master_Index_View_Actve_Ind;

    private DbsGroup cwf_Master_Index_View__R_Field_7;
    private DbsField cwf_Master_Index_View_Fill_1;
    private DbsField cwf_Master_Index_View_Actve_Ind_2_2;
    private DbsField cwf_Master_Index_View_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_View_Cnflct_Ind;
    private DbsField cwf_Master_Index_View_Spcl_Hndlng_Txt;
    private DbsField cwf_Master_Index_View_Instn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Instn_Cde;
    private DbsGroup cwf_Master_Index_View_Spcl_Policy_Srce_CdeMuGroup;
    private DbsField cwf_Master_Index_View_Spcl_Policy_Srce_Cde;
    private DbsField cwf_Master_Index_View_Attntn_Txt;
    private DbsField cwf_Master_Index_View_Check_Ind;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_View_Effctve_Dte;
    private DbsField cwf_Master_Index_View_Trnsctn_Dte;
    private DbsField cwf_Master_Index_View_Trans_Dte;
    private DbsField cwf_Master_Index_View_Physcl_Fldr_Id_Nbr;
    private DbsField cwf_Master_Index_View_Mj_Chrge_Dte_Tme;
    private DbsField cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Mstr_Indx_Actn_Cde;
    private DbsField cwf_Master_Index_View_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_View_Final_Close_Out_Dte_Tme;
    private DbsField cwf_Master_Index_View_Final_Close_Out_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Mj_Emrgncy_Rqst_Dte_Tme;
    private DbsField cwf_Master_Index_View_Mj_Emrgncy_Rqst_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Print_Q_Ind;
    private DbsField cwf_Master_Index_View_Print_Dte_Tme;
    private DbsField cwf_Master_Index_View_Print_Batch_Id_Nbr;

    private DbsGroup cwf_Master_Index_View__R_Field_8;
    private DbsField cwf_Master_Index_View_Print_Prefix_Ind;
    private DbsField cwf_Master_Index_View_Print_Batch_Nbr;
    private DbsField cwf_Master_Index_View_Print_Batch_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Print_Batch_Ind;
    private DbsField cwf_Master_Index_View_Printer_Id_Cde;
    private DbsField cwf_Master_Index_View_Rescan_Ind;
    private DbsField cwf_Master_Index_View_Dup_Ind;
    private DbsField cwf_Master_Index_View_Cmplnt_Ind;
    private DbsGroup cwf_Master_Index_View_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Cntrct_Nbr;
    private DbsGroup cwf_Master_Index_View_Mail_Item_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Mail_Item_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_9;
    private DbsField cwf_Master_Index_View_Rqst_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Case_Id_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rlte_Rqst_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_10;
    private DbsField cwf_Master_Index_View_Rlte_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Rlte_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rlte_Case_Id_Cde;
    private DbsField cwf_Master_Index_View_Rlte_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_View_Work_List_Ind;

    private DbsGroup cwf_Master_Index_View__R_Field_11;
    private DbsField cwf_Master_Index_View_Work_List_Ind_1_1;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_12;
    private DbsField cwf_Master_Index_View_Due_Dte;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Ind;
    private DbsField cwf_Master_Index_View_Due_Dte_Prty;
    private DbsField cwf_Master_Index_View_Filler_1406_9818;
    private DbsField cwf_Master_Index_View_Sbsqnt_Cntct_Actn_Rqrd;
    private DbsField cwf_Master_Index_View_Sbsqnt_Cntct_Ind;
    private DbsField cwf_Master_Index_View_Prcssng_Type;
    private DbsField cwf_Master_Index_View_Bsnss_Reply_Ind;
    private DbsField cwf_Master_Index_View_Status_Freeze_Ind;
    private DbsField cwf_Master_Index_View_Elctrnc_Fldr_Ind;
    private DbsField cwf_Master_Index_View_Log_Insttn_Srce_Cde;
    private DbsField cwf_Master_Index_View_Log_Rqstr_Cde;
    private DbsField cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Days;
    private DbsField cwf_Master_Index_View_Step_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Step_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_13;
    private DbsField cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme_1_8;
    private DbsField cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme;
    private DbsField cwf_Master_Index_View_Acknwldgmnt_Cde;
    private DbsField cwf_Master_Index_View_Acknwldgmnt_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Acknwldgmnt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Sheet_Print_Cde;
    private DbsField cwf_Master_Index_View_Cntct_Sheet_Print_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Sheet_Printer_Id_Cde;
    private DbsField cwf_Master_Index_View_Shphrd_Id;
    private DbsField cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_14;
    private DbsField cwf_Master_Index_View_Crrnt_Due_Dte;
    private DbsField cwf_Master_Index_View_Crrnt_Cmt_Ind;
    private DbsField cwf_Master_Index_View_Crrnt_Prrty_Cde;
    private DbsField cwf_Master_Index_View_Crrnt_Due_Tme;
    private DbsField cwf_Master_Index_View_Crprte_Due_Dte_Tme;
    private DbsField cwf_Master_Index_View_Archvd_Dte;
    private DbsField cwf_Master_Index_View_Rstr_To_Crrnt_Dte;
    private DbsField cwf_Master_Index_View_Off_Rtng_Ind;
    private DbsField cwf_Master_Index_View_Unit_On_Tme_Ind;
    private DbsField cwf_Master_Index_View_Crprte_On_Tme_Ind;
    private DbsField cwf_Master_Index_View_Owner_Unit_Cde;
    private DbsField cwf_Master_Index_View_Physcl_Fldr_Owner_Id;
    private DbsField cwf_Master_Index_View_Physcl_Fldr_Owner_Unit_Cde;
    private DbsField cwf_Master_Index_View_Step_Re_Do_Ind;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_View_Trade_Dte_Tme;
    private DbsField cwf_Master_Index_View_Future_Pymnt_Ind;
    private DbsField cwf_Master_Index_View_Status_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_15;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_16;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_17;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_18;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_19;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_20;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_21;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_22;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_23;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_24;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Minutes;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;
    private DbsField pnd_Header2_1;
    private DbsField pnd_Header2_2;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Rex_Read;
    private DbsField pnd_Counters_Pnd_Sar_Forms_With;
    private DbsField pnd_Counters_Pnd_Sar_Forms_Without;
    private DbsField pnd_Txn_Key;

    private DbsGroup pnd_Txn_Key__R_Field_25;
    private DbsField pnd_Txn_Key_Pin_Nbr;
    private DbsField pnd_Txn_Key_Crpte_Status_Ind;
    private DbsField pnd_Txn_Key_Work_Prcss_Id;

    private DataAccessProgramView vw_cwf_Txn;
    private DbsField cwf_Txn_Pin_Nbr;
    private DbsField cwf_Txn_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Txn__R_Field_26;
    private DbsField cwf_Txn_Rqst_Log_Index_Dte;
    private DbsField cwf_Txn_Rqst_Log_Index_Tme;
    private DbsField cwf_Txn_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Txn_Multi_Rqst_Ind;
    private DbsField cwf_Txn_Work_Prcss_Id;
    private DbsField cwf_Txn_Unit_Cde;
    private DbsField cwf_Txn_Unit_Updte_Dte_Tme;
    private DbsField cwf_Txn_Admin_Status_Cde;
    private DbsField cwf_Txn_Status_Cde;
    private DbsField cwf_Txn_Actve_Ind;
    private DbsField cwf_Txn_Crprte_Status_Ind;
    private DbsField cwf_Txn_Tiaa_Rcvd_Dte;
    private DbsField cwf_Txn_Crprte_Clock_End_Dte_Tme;

    private DbsGroup work_File_With;
    private DbsField work_File_With_Pin_Nbr;
    private DbsField work_File_With_Work_Prcss_Id;
    private DbsField work_File_With_Tiaa_Rcvd_Dte;
    private DbsField work_File_With_Txn_Wpid;
    private DbsField work_File_With_Txn_Tiaa_Rcvd_Dte;
    private DbsField work_File_With_Name;
    private DbsField work_File_With_Ssn;
    private DbsField work_File_With_Orgnl_Unit_Cde;

    private DbsGroup work_File_Without;
    private DbsField work_File_Without_Pin_Nbr;
    private DbsField work_File_Without_Work_Prcss_Id;
    private DbsField work_File_Without_Tiaa_Rcvd_Dte;
    private DbsField work_File_Without_Name;
    private DbsField work_File_Without_Ssn;
    private DbsField work_File_Without_Orgnl_Unit_Cde;
    private DbsField pnd_No_Records_Found;
    private DbsField pnd_No_Txn_Found;
    private DbsField pnd_Forms_Clock_Date;
    private DbsField pnd_Txn_Idx;
    private DbsField pnd_I;

    private DbsGroup pnd_Misc_Data;
    private DbsField pnd_Misc_Data_Pnd_Start_Date;

    private DbsGroup pnd_Misc_Data__R_Field_27;
    private DbsField pnd_Misc_Data_Pnd_Start_Date_N;
    private DbsField pnd_Misc_Data_Pnd_Today;

    private DbsGroup pnd_Misc_Data__R_Field_28;
    private DbsField pnd_Misc_Data_Pnd_Today_N;
    private DbsField pnd_Misc_Data_Pnd_Work_Start_Date_A;

    private DbsGroup pnd_Misc_Data__R_Field_29;
    private DbsField pnd_Misc_Data_Pnd_Work_Mm;
    private DbsField pnd_Misc_Data_Pnd_Filler1;
    private DbsField pnd_Misc_Data_Pnd_Work_Dd;
    private DbsField pnd_Misc_Data_Pnd_Filler2;
    private DbsField pnd_Misc_Data_Pnd_Work_Yy;
    private DbsField pnd_Misc_Data_Pnd_Work_End_Date_A;

    private DbsGroup pnd_Misc_Data__R_Field_30;
    private DbsField pnd_Misc_Data_Pnd_Work_Mmm;
    private DbsField pnd_Misc_Data_Pnd_Fillera;
    private DbsField pnd_Misc_Data_Pnd_Work_Ddd;
    private DbsField pnd_Misc_Data_Pnd_Fillerb;
    private DbsField pnd_Misc_Data_Pnd_Work_Yyy;
    private DbsField pnd_Parm_Type;
    private DbsField pnd_Tiaa_Comp_Date_Start;
    private DbsField pnd_Tiaa_Comp_Date_End;
    private DbsField pnd_Tiaa_Recvd_Date;
    private DbsField pnd_Work_Date;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Clock_Dte_Tme;

    private DbsGroup pnd_Clock_Dte_Tme__R_Field_31;
    private DbsField pnd_Clock_Dte_Tme_Pnd_Clock_Check_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        localVariables = new DbsRecord();
        pdaCwfa5372 = new PdaCwfa5372(localVariables);

        // Local Variables
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Pin_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Master_Index_View_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index_View__R_Field_1 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_1", "REDEFINE", cwf_Master_Index_View_Rqst_Log_Dte_Tme);
        cwf_Master_Index_View_Rqst_Log_Index_Dte = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Log_Index_Tme = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme", 
            "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_View_Rqst_Orgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Orgn_Cde", "RQST-ORGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_View_Rqst_Rgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Rgn_Cde", "RQST-RGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_RGN_CDE");
        cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde", 
            "RQST-SPCL-DSGNTN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_SPCL_DSGNTN_CDE");
        cwf_Master_Index_View_Rqst_Brnch_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Brnch_Cde", "RQST-BRNCH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_BRNCH_CDE");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_View_Sub_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Ind", "SUB-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SUB_RQST_IND");
        cwf_Master_Index_View_Sub_Rqst_Ind.setDdmHeader("SUB-REQUEST COUNT");
        cwf_Master_Index_View_Case_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CASE_ID_CDE");
        cwf_Master_Index_View_Case_Id_Cde.setDdmHeader("CASE/ID");

        cwf_Master_Index_View__R_Field_2 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_2", "REDEFINE", cwf_Master_Index_View_Case_Id_Cde);
        cwf_Master_Index_View_Case_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Case_Ind", "CASE-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind", "SUB-RQST-SQNCE-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Multi_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Multi_Rqst_Ind", "MULTI-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        cwf_Master_Index_View_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Master_Index_View_Orgnl_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_View_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");

        cwf_Master_Index_View__R_Field_3 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_3", "REDEFINE", cwf_Master_Index_View_Work_Prcss_Id);
        cwf_Master_Index_View_Work_Actn_Rqstd_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde", 
            "WORK-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde", 
            "WORK-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Wpid_Vldte_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Wpid_Vldte_Ind", "WPID-VLDTE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WPID_VLDTE_IND");
        cwf_Master_Index_View_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Cde", "UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_CDE");
        cwf_Master_Index_View_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Master_Index_View__R_Field_4 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_4", "REDEFINE", cwf_Master_Index_View_Unit_Cde);
        cwf_Master_Index_View_Unit_Id_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 
            5);
        cwf_Master_Index_View_Unit_Rgn_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Unit_Brnch_Group_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Unit_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Index_View_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Index_View_Old_Route_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Old_Route_Cde", "OLD-ROUTE-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "OLD_ROUTE_CDE");
        cwf_Master_Index_View_Old_Route_Cde.setDdmHeader("ACTIVITY-END/STATUS");
        cwf_Master_Index_View_Work_Rqst_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Rqst_Prty_Cde", "WORK-RQST-PRTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WORK_RQST_PRTY_CDE");
        cwf_Master_Index_View_Work_Rqst_Prty_Cde.setDdmHeader("PRIO");
        cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde", 
            "ASSGN-SPRVSR-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ASSGN_SPRVSR_OPRTR_CDE");
        cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde.setDdmHeader("SUPER/VISOR");
        cwf_Master_Index_View_Assgn_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Assgn_Dte_Tme", "ASSGN-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ASSGN_DTE_TME");
        cwf_Master_Index_View_Assgn_Dte_Tme.setDdmHeader("ASSIGNMENT/DATE-TIME");
        cwf_Master_Index_View_Empl_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_View_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");

        cwf_Master_Index_View__R_Field_5 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_5", "REDEFINE", cwf_Master_Index_View_Empl_Oprtr_Cde);
        cwf_Master_Index_View_Empl_Racf_Id = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Empl_Sffx_Cde = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Empl_Sffx_Cde", "EMPL-SFFX-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Last_Chnge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        cwf_Master_Index_View__R_Field_6 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_6", "REDEFINE", cwf_Master_Index_View_Last_Chnge_Dte_Tme);
        cwf_Master_Index_View_Last_Chnge_Dte_Tme_8_15 = cwf_Master_Index_View__R_Field_6.newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_Tme_8_15", 
            "LAST-CHNGE-DTE-TME-8-15", FieldType.NUMERIC, 8);
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Oprtr_Cde", 
            "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme", 
            "LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Unit_Cde", 
            "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_View_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_View_Step_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Id", "STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "STEP_ID");
        cwf_Master_Index_View_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Index_View_Rt_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        cwf_Master_Index_View_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        cwf_Master_Index_View_Step_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Sqnce_Nbr", "STEP-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "STEP_SQNCE_NBR");
        cwf_Master_Index_View_Step_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Updte_Dte_Tme", "STEP-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STEP_UPDTE_DTE_TME");
        cwf_Master_Index_View_Step_Updte_Dte_Tme.setDdmHeader("STEP UPDATE/DATE-TIME");
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_View_Admin_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme", 
            "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_View_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Dte_Tme", 
            "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Index_View_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Oprtr_Cde", 
            "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Updte_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Dte", "LAST-UPDTE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        cwf_Master_Index_View_Last_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Index_View_Last_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Oprtr_Cde", 
            "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Cntct_Orgn_Type_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Orgn_Type_Cde", 
            "CNTCT-ORGN-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTCT_ORGN_TYPE_CDE");
        cwf_Master_Index_View_Cntct_Orgn_Type_Cde.setDdmHeader("CONTACT/CODE");
        cwf_Master_Index_View_Cntct_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Dte_Tme", "CNTCT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "CNTCT_DTE_TME");
        cwf_Master_Index_View_Cntct_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Invrt_Dte_Tme", 
            "CNTCT-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "CNTCT_INVRT_DTE_TME");
        cwf_Master_Index_View_Cntct_Oprtr_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Oprtr_Id", "CNTCT-OPRTR-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTCT_OPRTR_ID");
        cwf_Master_Index_View_Actve_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");

        cwf_Master_Index_View__R_Field_7 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_7", "REDEFINE", cwf_Master_Index_View_Actve_Ind);
        cwf_Master_Index_View_Fill_1 = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Master_Index_View_Actve_Ind_2_2 = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Actve_Ind_2_2", "ACTVE-IND-2-2", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Crprte_Status_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_View_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_View_Cnflct_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cnflct_Ind", "CNFLCT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNFLCT_IND");
        cwf_Master_Index_View_Cnflct_Ind.setDdmHeader("CONFLICT/REQUEST");
        cwf_Master_Index_View_Spcl_Hndlng_Txt = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", 
            FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        cwf_Master_Index_View_Spcl_Hndlng_Txt.setDdmHeader("MESSAGES");
        cwf_Master_Index_View_Instn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Instn_Cde", "INSTN-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "INSTN_CDE");
        cwf_Master_Index_View_Rqst_Instn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Instn_Cde", "RQST-INSTN-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "RQST_INSTN_CDE");
        cwf_Master_Index_View_Spcl_Policy_Srce_CdeMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_SPCL_POLICY_SRCE_CDEMuGroup", 
            "SPCL_POLICY_SRCE_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_SPCL_POLICY_SRCE_CDE");
        cwf_Master_Index_View_Spcl_Policy_Srce_Cde = cwf_Master_Index_View_Spcl_Policy_Srce_CdeMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Spcl_Policy_Srce_Cde", 
            "SPCL-POLICY-SRCE-CDE", FieldType.STRING, 5, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "SPCL_POLICY_SRCE_CDE");
        cwf_Master_Index_View_Attntn_Txt = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Attntn_Txt", "ATTNTN-TXT", FieldType.STRING, 
            25, RepeatingFieldStrategy.None, "ATTNTN_TXT");
        cwf_Master_Index_View_Attntn_Txt.setDdmHeader("ATTENTION");
        cwf_Master_Index_View_Check_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Check_Ind", "CHECK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CHECK_IND");
        cwf_Master_Index_View_Check_Ind.setDdmHeader("CHK");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme", 
            "RQST-INVRT-RCVD-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_INVRT_RCVD_DTE_TME");
        cwf_Master_Index_View_Effctve_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_View_Trnsctn_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trnsctn_Dte", "TRNSCTN-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRNSCTN_DTE");
        cwf_Master_Index_View_Trnsctn_Dte.setDdmHeader("TRANSACTION/DATE");
        cwf_Master_Index_View_Trans_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trans_Dte", "TRANS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        cwf_Master_Index_View_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_View_Physcl_Fldr_Id_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Index_View_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Index_View_Mj_Chrge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Chrge_Dte_Tme", "MJ-CHRGE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "MJ_CHRGE_DTE_TME");
        cwf_Master_Index_View_Mj_Chrge_Dte_Tme.setDdmHeader("CHARGEOUT/DATE-TIME");
        cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde", "MJ-CHRGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_CHRGE_OPRTR_CDE");
        cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde.setDdmHeader("CHARGEOUT/OPERATOR");
        cwf_Master_Index_View_Mstr_Indx_Actn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mstr_Indx_Actn_Cde", "MSTR-INDX-ACTN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "MSTR_INDX_ACTN_CDE");
        cwf_Master_Index_View_Mstr_Indx_Actn_Cde.setDdmHeader("CHARGEOUT/ACTION CODE");
        cwf_Master_Index_View_Mj_Pull_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_View_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_View_Final_Close_Out_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Final_Close_Out_Dte_Tme", 
            "FINAL-CLOSE-OUT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_DTE_TME");
        cwf_Master_Index_View_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        cwf_Master_Index_View_Final_Close_Out_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Final_Close_Out_Oprtr_Cde", 
            "FINAL-CLOSE-OUT-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_OPRTR_CDE");
        cwf_Master_Index_View_Mj_Emrgncy_Rqst_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Emrgncy_Rqst_Dte_Tme", 
            "MJ-EMRGNCY-RQST-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_DTE_TME");
        cwf_Master_Index_View_Mj_Emrgncy_Rqst_Dte_Tme.setDdmHeader("EMERGENCY REQ/DATE-TIME");
        cwf_Master_Index_View_Mj_Emrgncy_Rqst_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Emrgncy_Rqst_Oprtr_Cde", 
            "MJ-EMRGNCY-RQST-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_OPRTR_CDE");
        cwf_Master_Index_View_Mj_Emrgncy_Rqst_Oprtr_Cde.setDdmHeader("EMERGENCY/REQUESTOR");
        cwf_Master_Index_View_Print_Q_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Q_Ind", "PRINT-Q-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PRINT_Q_IND");
        cwf_Master_Index_View_Print_Q_Ind.setDdmHeader("MJ PRINT/QUEUE");
        cwf_Master_Index_View_Print_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Dte_Tme", "PRINT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "PRINT_DTE_TME");
        cwf_Master_Index_View_Print_Dte_Tme.setDdmHeader("MJ PRINT/DATE-TIME");
        cwf_Master_Index_View_Print_Batch_Id_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Batch_Id_Nbr", "PRINT-BATCH-ID-NBR", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "PRINT_BATCH_ID_NBR");

        cwf_Master_Index_View__R_Field_8 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_8", "REDEFINE", cwf_Master_Index_View_Print_Batch_Id_Nbr);
        cwf_Master_Index_View_Print_Prefix_Ind = cwf_Master_Index_View__R_Field_8.newFieldInGroup("cwf_Master_Index_View_Print_Prefix_Ind", "PRINT-PREFIX-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Print_Batch_Nbr = cwf_Master_Index_View__R_Field_8.newFieldInGroup("cwf_Master_Index_View_Print_Batch_Nbr", "PRINT-BATCH-NBR", 
            FieldType.NUMERIC, 8);
        cwf_Master_Index_View_Print_Batch_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Batch_Sqnce_Nbr", 
            "PRINT-BATCH-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRINT_BATCH_SQNCE_NBR");
        cwf_Master_Index_View_Print_Batch_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Batch_Ind", "PRINT-BATCH-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRINT_BATCH_IND");
        cwf_Master_Index_View_Printer_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Printer_Id_Cde", "PRINTER-ID-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "PRINTER_ID_CDE");
        cwf_Master_Index_View_Rescan_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RESCAN_IND");
        cwf_Master_Index_View_Rescan_Ind.setDdmHeader("RE-SCAN/INDICATOR");
        cwf_Master_Index_View_Dup_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Dup_Ind", "DUP-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DUP_IND");
        cwf_Master_Index_View_Dup_Ind.setDdmHeader("DUP/IND");
        cwf_Master_Index_View_Cmplnt_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CMPLNT_IND");
        cwf_Master_Index_View_Cntrct_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_View_Cntrct_Nbr = cwf_Master_Index_View_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Cntrct_Nbr", "CNTRCT-NBR", 
            FieldType.STRING, 8, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_View_Mail_Item_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_MAIL_ITEM_NBRMuGroup", 
            "MAIL_ITEM_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_MAIL_ITEM_NBR");
        cwf_Master_Index_View_Mail_Item_Nbr = cwf_Master_Index_View_Mail_Item_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Mail_Item_Nbr", "MAIL-ITEM-NBR", 
            FieldType.STRING, 11, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "MAIL_ITEM_NBR");
        cwf_Master_Index_View_Mail_Item_Nbr.setDdmHeader("MIN");
        cwf_Master_Index_View_Rqst_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Id", "RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Master_Index_View_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Master_Index_View__R_Field_9 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_9", "REDEFINE", cwf_Master_Index_View_Rqst_Id);
        cwf_Master_Index_View_Rqst_Work_Prcss_Id = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Case_Id_Cde = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Rqst_Pin_Nbr = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Rqst_Pin_Nbr", "RQST-PIN-NBR", FieldType.NUMERIC, 
            12);
        cwf_Master_Index_View_Rlte_Rqst_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rlte_Rqst_Id", "RLTE-RQST-ID", 
            FieldType.STRING, 28, RepeatingFieldStrategy.None, "RLTE_RQST_ID");

        cwf_Master_Index_View__R_Field_10 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_10", "REDEFINE", cwf_Master_Index_View_Rlte_Rqst_Id);
        cwf_Master_Index_View_Rlte_Work_Prcss_Id = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rlte_Work_Prcss_Id", "RLTE-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Master_Index_View_Rlte_Tiaa_Rcvd_Dte = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rlte_Tiaa_Rcvd_Dte", "RLTE-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rlte_Case_Id_Cde = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rlte_Case_Id_Cde", "RLTE-CASE-ID-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Rlte_Pin_Nbr = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rlte_Pin_Nbr", "RLTE-PIN-NBR", FieldType.NUMERIC, 
            12);
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte", 
            "EXTRNL-PEND-RCV-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_View_Work_List_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_List_Ind", "WORK-LIST-IND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "WORK_LIST_IND");
        cwf_Master_Index_View_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");

        cwf_Master_Index_View__R_Field_11 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_11", "REDEFINE", cwf_Master_Index_View_Work_List_Ind);
        cwf_Master_Index_View_Work_List_Ind_1_1 = cwf_Master_Index_View__R_Field_11.newFieldInGroup("cwf_Master_Index_View_Work_List_Ind_1_1", "WORK-LIST-IND-1-1", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde", 
            "DUE-DTE-CHG-PRTY-CDE", FieldType.STRING, 16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        cwf_Master_Index_View__R_Field_12 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_12", "REDEFINE", cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Index_View_Due_Dte = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Due_Dte", "DUE-DTE", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Due_Dte_Chg_Ind = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Prty = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Prty", "DUE-DTE-PRTY", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Filler_1406_9818 = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Filler_1406_9818", "FILLER-1406-9818", 
            FieldType.STRING, 3);
        cwf_Master_Index_View_Sbsqnt_Cntct_Actn_Rqrd = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Sbsqnt_Cntct_Actn_Rqrd", 
            "SBSQNT-CNTCT-ACTN-RQRD", FieldType.STRING, 1);
        cwf_Master_Index_View_Sbsqnt_Cntct_Ind = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Sbsqnt_Cntct_Ind", "SBSQNT-CNTCT-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Prcssng_Type = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Prcssng_Type", "PRCSSNG-TYPE", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Bsnss_Reply_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BSNSS_REPLY_IND");
        cwf_Master_Index_View_Bsnss_Reply_Ind.setDdmHeader("BUSINESS/REPLY");
        cwf_Master_Index_View_Status_Freeze_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_View_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Index_View_Elctrnc_Fldr_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_Index_View_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        cwf_Master_Index_View_Log_Insttn_Srce_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Log_Insttn_Srce_Cde", 
            "LOG-INSTTN-SRCE-CDE", FieldType.STRING, 5, RepeatingFieldStrategy.None, "LOG_INSTTN_SRCE_CDE");
        cwf_Master_Index_View_Log_Insttn_Srce_Cde.setDdmHeader("REQUESTOR/INSTITUTION");
        cwf_Master_Index_View_Log_Rqstr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Log_Rqstr_Cde", "LOG-RQSTR-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "LOG_RQSTR_CDE");
        cwf_Master_Index_View_Log_Rqstr_Cde.setDdmHeader("REQUESTOR/TYPE");
        cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme", 
            "UNIT-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Index_View_Unit_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Clock_End_Dte_Tme", 
            "UNIT-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme", 
            "EMPL-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        cwf_Master_Index_View_Empl_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Clock_End_Dte_Tme", 
            "EMPL-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme", 
            "INTRNL-PND-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_START_DTE_TME");
        cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme.setDdmHeader("ACKNOWLEDGEMENT DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme", 
            "INTRNL-PND-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_END_DTE_TME");
        cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme.setDdmHeader("INTERNAL PEND END DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_Days = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Days", "INTRNL-PND-DAYS", 
            FieldType.PACKED_DECIMAL, 5, RepeatingFieldStrategy.None, "INTRNL_PND_DAYS");
        cwf_Master_Index_View_Intrnl_Pnd_Days.setDdmHeader("INTERNAL PEND DAYS");
        cwf_Master_Index_View_Step_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Clock_Start_Dte_Tme", 
            "STEP-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        cwf_Master_Index_View_Step_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Clock_End_Dte_Tme", 
            "STEP-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme", 
            "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");

        cwf_Master_Index_View__R_Field_13 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_13", "REDEFINE", cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme);
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme_1_8 = cwf_Master_Index_View__R_Field_13.newFieldInGroup("cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme_1_8", 
            "CRPRTE-CLOCK-END-DTE-TME-1-8", FieldType.STRING, 8);
        cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme", 
            "UNIT-EN-RTE-TO-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_EN_RTE_TO_DTE_TME");
        cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme.setDdmHeader("UNIT EN ROUTE TO DATE TIME");
        cwf_Master_Index_View_Acknwldgmnt_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Acknwldgmnt_Cde", "ACKNWLDGMNT-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ACKNWLDGMNT_CDE");
        cwf_Master_Index_View_Acknwldgmnt_Cde.setDdmHeader("ACKNOWLEDGEMENT/CODE");
        cwf_Master_Index_View_Acknwldgmnt_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Acknwldgmnt_Oprtr_Cde", 
            "ACKNWLDGMNT-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ACKNWLDGMNT_OPRTR_CDE");
        cwf_Master_Index_View_Acknwldgmnt_Oprtr_Cde.setDdmHeader("ACKNOWLEDGED/BY");
        cwf_Master_Index_View_Acknwldgmnt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Acknwldgmnt_Dte_Tme", 
            "ACKNWLDGMNT-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "ACKNWLDGMNT_DTE_TME");
        cwf_Master_Index_View_Acknwldgmnt_Dte_Tme.setDdmHeader("ACKNOWLEDGED/ON");
        cwf_Master_Index_View_Cntct_Sheet_Print_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Sheet_Print_Cde", 
            "CNTCT-SHEET-PRINT-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_CDE");
        cwf_Master_Index_View_Cntct_Sheet_Print_Cde.setDdmHeader("CONTACT SHEET/PRINT CODE");
        cwf_Master_Index_View_Cntct_Sheet_Print_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Sheet_Print_Dte_Tme", 
            "CNTCT-SHEET-PRINT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_DTE_TME");
        cwf_Master_Index_View_Cntct_Sheet_Printer_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Sheet_Printer_Id_Cde", 
            "CNTCT-SHEET-PRINTER-ID-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINTER_ID_CDE");
        cwf_Master_Index_View_Shphrd_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Shphrd_Id", "SHPHRD-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "SHPHRD_ID");
        cwf_Master_Index_View_Shphrd_Id.setDdmHeader("SHEPHERD");
        cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme", 
            "CRRNT-DUE-DTE-CMT-PRTY-TME", FieldType.STRING, 17, RepeatingFieldStrategy.None, "CRRNT_DUE_DTE_CMT_PRTY_TME");
        cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme.setDdmHeader("CURR-DUE/TME-FIELD");

        cwf_Master_Index_View__R_Field_14 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_14", "REDEFINE", cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme);
        cwf_Master_Index_View_Crrnt_Due_Dte = cwf_Master_Index_View__R_Field_14.newFieldInGroup("cwf_Master_Index_View_Crrnt_Due_Dte", "CRRNT-DUE-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Crrnt_Cmt_Ind = cwf_Master_Index_View__R_Field_14.newFieldInGroup("cwf_Master_Index_View_Crrnt_Cmt_Ind", "CRRNT-CMT-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Crrnt_Prrty_Cde = cwf_Master_Index_View__R_Field_14.newFieldInGroup("cwf_Master_Index_View_Crrnt_Prrty_Cde", "CRRNT-PRRTY-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Crrnt_Due_Tme = cwf_Master_Index_View__R_Field_14.newFieldInGroup("cwf_Master_Index_View_Crrnt_Due_Tme", "CRRNT-DUE-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_View_Crprte_Due_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CRPRTE_DUE_DTE_TME");
        cwf_Master_Index_View_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Master_Index_View_Archvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Archvd_Dte", "ARCHVD-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ARCHVD_DTE");
        cwf_Master_Index_View_Archvd_Dte.setDdmHeader("ARCHIVED/DATE");
        cwf_Master_Index_View_Rstr_To_Crrnt_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rstr_To_Crrnt_Dte", "RSTR-TO-CRRNT-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RSTR_TO_CRRNT_DTE");
        cwf_Master_Index_View_Rstr_To_Crrnt_Dte.setDdmHeader("RESTORE/DATE");
        cwf_Master_Index_View_Off_Rtng_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Off_Rtng_Ind", "OFF-RTNG-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "OFF_RTNG_IND");
        cwf_Master_Index_View_Off_Rtng_Ind.setDdmHeader("OFF-ROUTING");
        cwf_Master_Index_View_Unit_On_Tme_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_On_Tme_Ind", "UNIT-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "UNIT_ON_TME_IND");
        cwf_Master_Index_View_Crprte_On_Tme_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_ON_TME_IND");
        cwf_Master_Index_View_Crprte_On_Tme_Ind.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Master_Index_View_Owner_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Owner_Unit_Cde", "OWNER-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "OWNER_UNIT_CDE");
        cwf_Master_Index_View_Owner_Unit_Cde.setDdmHeader("OWNERUNIT");
        cwf_Master_Index_View_Physcl_Fldr_Owner_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Physcl_Fldr_Owner_Id", 
            "PHYSCL-FLDR-OWNER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "PHYSCL_FLDR_OWNER_ID");
        cwf_Master_Index_View_Physcl_Fldr_Owner_Id.setDdmHeader("PHYSICAL/FOLDER-OWNER");
        cwf_Master_Index_View_Physcl_Fldr_Owner_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Physcl_Fldr_Owner_Unit_Cde", 
            "PHYSCL-FLDR-OWNER-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "PHYSCL_FLDR_OWNER_UNIT_CDE");
        cwf_Master_Index_View_Physcl_Fldr_Owner_Unit_Cde.setDdmHeader("PHYSICAL/FOLDER-UNIT");
        cwf_Master_Index_View_Step_Re_Do_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Re_Do_Ind", "STEP-RE-DO-IND", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "STEP_RE_DO_IND");
        cwf_Master_Index_View_Step_Re_Do_Ind.setDdmHeader("REDO/COUNT");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        cwf_Master_Index_View_Trade_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trade_Dte_Tme", "TRADE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TRADE_DTE_TME");
        cwf_Master_Index_View_Trade_Dte_Tme.setDdmHeader("TRADE/DTE-TME");
        cwf_Master_Index_View_Future_Pymnt_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Future_Pymnt_Ind", "FUTURE-PYMNT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "FUTURE_PYMNT_IND");
        cwf_Master_Index_View_Future_Pymnt_Ind.setDdmHeader("ACKNOWLEDGEMENT/PAYMENT-DTE");
        cwf_Master_Index_View_Status_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Clock_Start_Dte_Tme", 
            "STATUS-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STATUS_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Status_Clock_Start_Dte_Tme.setDdmHeader("STATUS/START CLOCK");
        cwf_Master_Index_View_Status_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Clock_End_Dte_Tme", 
            "STATUS-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STATUS_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Status_Clock_End_Dte_Tme.setDdmHeader("STATUS/CLOCK END");
        cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme", 
            "STATUS-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "STATUS_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme.setDdmHeader("STATUS ACTIVITY/TURNAROUND");

        cwf_Master_Index_View__R_Field_15 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_15", "REDEFINE", cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme);
        cwf_Master_Index_View_Status_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_15.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Clndr_Days", 
            "STATUS-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Status_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_15.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Clndr_Hours", 
            "STATUS-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Status_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_15.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Clndr_Minutes", 
            "STATUS-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme", 
            "STATUS-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "STATUS_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme.setDdmHeader("STATUS/(BUSINESS DAYS)");

        cwf_Master_Index_View__R_Field_16 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_16", "REDEFINE", cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_16.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Bsnss_Days", 
            "STATUS-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_16.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Bsnss_Hours", 
            "STATUS-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_16.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Bsnss_Minutes", 
            "STATUS-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme", 
            "STEP-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "STEP_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme.setDdmHeader("STEP WAIT TIME");

        cwf_Master_Index_View__R_Field_17 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_17", "REDEFINE", cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme);
        cwf_Master_Index_View_Step_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_17.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Clndr_Days", 
            "STEP-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Step_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_17.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Clndr_Hours", 
            "STEP-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Step_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_17.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Clndr_Minutes", 
            "STEP-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme", 
            "STEP-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "STEP_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme.setDdmHeader("STEP PROCESS TIME");

        cwf_Master_Index_View__R_Field_18 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_18", "REDEFINE", cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_18.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Bsnss_Days", 
            "STEP-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_18.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Bsnss_Hours", 
            "STEP-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_18.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Bsnss_Minutes", 
            "STEP-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme", 
            "EMPL-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "EMPL_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme.setDdmHeader("ASSOCIATE WAIT TIME");

        cwf_Master_Index_View__R_Field_19 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_19", "REDEFINE", cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme);
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_19.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Clndr_Days", 
            "EMPL-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_19.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Clndr_Hours", 
            "EMPL-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_19.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Clndr_Minutes", 
            "EMPL-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme", 
            "EMPL-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "EMPL_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme.setDdmHeader("ASSOCIATE PROCESS TIME");

        cwf_Master_Index_View__R_Field_20 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_20", "REDEFINE", cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_20.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days", 
            "EMPL-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_20.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Bsnss_Hours", 
            "EMPL-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_20.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Bsnss_Minutes", 
            "EMPL-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme", 
            "UNIT-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "UNIT_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme.setDdmHeader("UNIT WAIT TIME");

        cwf_Master_Index_View__R_Field_21 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_21", "REDEFINE", cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme);
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_21.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Clndr_Days", 
            "UNIT-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_21.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Clndr_Hours", 
            "UNIT-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_21.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Clndr_Minutes", 
            "UNIT-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme", 
            "UNIT-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "UNIT_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme.setDdmHeader("UNIT PROCESS TIME");

        cwf_Master_Index_View__R_Field_22 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_22", "REDEFINE", cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_22.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days", 
            "UNIT-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_22.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Bsnss_Hours", 
            "UNIT-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_22.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Bsnss_Minutes", 
            "UNIT-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme", 
            "INTRNL-PND-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "INTRNL_PND_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme.setDdmHeader("INTERNAL-PEND/(CALENDAR DAYS)");

        cwf_Master_Index_View__R_Field_23 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_23", "REDEFINE", cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_23.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Days", 
            "INTRNL-PND-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_23.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Hours", 
            "INTRNL-PND-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_23.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Minutes", 
            "INTRNL-PND-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme", 
            "INTRNL-PND-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "INTRNL_PND_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme.setDdmHeader("INTERNAL PEND/(BUSSINESS DAYS)");

        cwf_Master_Index_View__R_Field_24 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_24", "REDEFINE", cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_24.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Days", 
            "INTRNL-PND-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_24.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Hours", 
            "INTRNL-PND-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_24.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Minutes", 
            "INTRNL-PND-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        registerRecord(vw_cwf_Master_Index_View);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);
        pnd_Header2_1 = localVariables.newFieldInRecord("pnd_Header2_1", "#HEADER2-1", FieldType.STRING, 50);
        pnd_Header2_2 = localVariables.newFieldInRecord("pnd_Header2_2", "#HEADER2-2", FieldType.STRING, 50);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Rex_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Read", "#REX-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Counters_Pnd_Sar_Forms_With = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Sar_Forms_With", "#SAR-FORMS-WITH", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Counters_Pnd_Sar_Forms_Without = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Sar_Forms_Without", "#SAR-FORMS-WITHOUT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Txn_Key = localVariables.newFieldInRecord("pnd_Txn_Key", "#TXN-KEY", FieldType.STRING, 19);

        pnd_Txn_Key__R_Field_25 = localVariables.newGroupInRecord("pnd_Txn_Key__R_Field_25", "REDEFINE", pnd_Txn_Key);
        pnd_Txn_Key_Pin_Nbr = pnd_Txn_Key__R_Field_25.newFieldInGroup("pnd_Txn_Key_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Txn_Key_Crpte_Status_Ind = pnd_Txn_Key__R_Field_25.newFieldInGroup("pnd_Txn_Key_Crpte_Status_Ind", "CRPTE-STATUS-IND", FieldType.STRING, 1);
        pnd_Txn_Key_Work_Prcss_Id = pnd_Txn_Key__R_Field_25.newFieldInGroup("pnd_Txn_Key_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);

        vw_cwf_Txn = new DataAccessProgramView(new NameInfo("vw_cwf_Txn", "CWF-TXN"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Txn_Pin_Nbr = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Txn_Pin_Nbr.setDdmHeader("PIN");
        cwf_Txn_Rqst_Log_Dte_Tme = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        cwf_Txn_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Txn__R_Field_26 = vw_cwf_Txn.getRecord().newGroupInGroup("cwf_Txn__R_Field_26", "REDEFINE", cwf_Txn_Rqst_Log_Dte_Tme);
        cwf_Txn_Rqst_Log_Index_Dte = cwf_Txn__R_Field_26.newFieldInGroup("cwf_Txn_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 8);
        cwf_Txn_Rqst_Log_Index_Tme = cwf_Txn__R_Field_26.newFieldInGroup("cwf_Txn_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", FieldType.STRING, 7);
        cwf_Txn_Orgnl_Log_Dte_Tme = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "ORGNL_LOG_DTE_TME");
        cwf_Txn_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Txn_Multi_Rqst_Ind = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MULTI_RQST_IND");
        cwf_Txn_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Txn_Work_Prcss_Id = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Txn_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Txn_Unit_Cde = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "UNIT_CDE");
        cwf_Txn_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Txn_Unit_Updte_Dte_Tme = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "UNIT_UPDTE_DTE_TME");
        cwf_Txn_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Txn_Admin_Status_Cde = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "ADMIN_STATUS_CDE");
        cwf_Txn_Status_Cde = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        cwf_Txn_Actve_Ind = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Txn_Crprte_Status_Ind = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_STATUS_IND");
        cwf_Txn_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Txn_Tiaa_Rcvd_Dte = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Txn_Crprte_Clock_End_Dte_Tme = vw_cwf_Txn.getRecord().newFieldInGroup("cwf_Txn_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Txn_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        registerRecord(vw_cwf_Txn);

        work_File_With = localVariables.newGroupInRecord("work_File_With", "WORK-FILE-WITH");
        work_File_With_Pin_Nbr = work_File_With.newFieldInGroup("work_File_With_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        work_File_With_Work_Prcss_Id = work_File_With.newFieldInGroup("work_File_With_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        work_File_With_Tiaa_Rcvd_Dte = work_File_With.newFieldInGroup("work_File_With_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.STRING, 8);
        work_File_With_Txn_Wpid = work_File_With.newFieldInGroup("work_File_With_Txn_Wpid", "TXN-WPID", FieldType.STRING, 6);
        work_File_With_Txn_Tiaa_Rcvd_Dte = work_File_With.newFieldInGroup("work_File_With_Txn_Tiaa_Rcvd_Dte", "TXN-TIAA-RCVD-DTE", FieldType.STRING, 8);
        work_File_With_Name = work_File_With.newFieldInGroup("work_File_With_Name", "NAME", FieldType.STRING, 45);
        work_File_With_Ssn = work_File_With.newFieldInGroup("work_File_With_Ssn", "SSN", FieldType.NUMERIC, 9);
        work_File_With_Orgnl_Unit_Cde = work_File_With.newFieldInGroup("work_File_With_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);

        work_File_Without = localVariables.newGroupInRecord("work_File_Without", "WORK-FILE-WITHOUT");
        work_File_Without_Pin_Nbr = work_File_Without.newFieldInGroup("work_File_Without_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        work_File_Without_Work_Prcss_Id = work_File_Without.newFieldInGroup("work_File_Without_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        work_File_Without_Tiaa_Rcvd_Dte = work_File_Without.newFieldInGroup("work_File_Without_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.STRING, 8);
        work_File_Without_Name = work_File_Without.newFieldInGroup("work_File_Without_Name", "NAME", FieldType.STRING, 45);
        work_File_Without_Ssn = work_File_Without.newFieldInGroup("work_File_Without_Ssn", "SSN", FieldType.NUMERIC, 9);
        work_File_Without_Orgnl_Unit_Cde = work_File_Without.newFieldInGroup("work_File_Without_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        pnd_No_Records_Found = localVariables.newFieldInRecord("pnd_No_Records_Found", "#NO-RECORDS-FOUND", FieldType.BOOLEAN, 1);
        pnd_No_Txn_Found = localVariables.newFieldInRecord("pnd_No_Txn_Found", "#NO-TXN-FOUND", FieldType.BOOLEAN, 1);
        pnd_Forms_Clock_Date = localVariables.newFieldInRecord("pnd_Forms_Clock_Date", "#FORMS-CLOCK-DATE", FieldType.STRING, 15);
        pnd_Txn_Idx = localVariables.newFieldInRecord("pnd_Txn_Idx", "#TXN-IDX", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);

        pnd_Misc_Data = localVariables.newGroupInRecord("pnd_Misc_Data", "#MISC-DATA");
        pnd_Misc_Data_Pnd_Start_Date = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_27 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_27", "REDEFINE", pnd_Misc_Data_Pnd_Start_Date);
        pnd_Misc_Data_Pnd_Start_Date_N = pnd_Misc_Data__R_Field_27.newFieldInGroup("pnd_Misc_Data_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Misc_Data_Pnd_Today = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Today", "#TODAY", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_28 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_28", "REDEFINE", pnd_Misc_Data_Pnd_Today);
        pnd_Misc_Data_Pnd_Today_N = pnd_Misc_Data__R_Field_28.newFieldInGroup("pnd_Misc_Data_Pnd_Today_N", "#TODAY-N", FieldType.NUMERIC, 8);
        pnd_Misc_Data_Pnd_Work_Start_Date_A = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Start_Date_A", "#WORK-START-DATE-A", FieldType.STRING, 
            8);

        pnd_Misc_Data__R_Field_29 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_29", "REDEFINE", pnd_Misc_Data_Pnd_Work_Start_Date_A);
        pnd_Misc_Data_Pnd_Work_Mm = pnd_Misc_Data__R_Field_29.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Mm", "#WORK-MM", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Filler1 = pnd_Misc_Data__R_Field_29.newFieldInGroup("pnd_Misc_Data_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Dd = pnd_Misc_Data__R_Field_29.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Filler2 = pnd_Misc_Data__R_Field_29.newFieldInGroup("pnd_Misc_Data_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Yy = pnd_Misc_Data__R_Field_29.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Yy", "#WORK-YY", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Work_End_Date_A = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Work_End_Date_A", "#WORK-END-DATE-A", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_30 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_30", "REDEFINE", pnd_Misc_Data_Pnd_Work_End_Date_A);
        pnd_Misc_Data_Pnd_Work_Mmm = pnd_Misc_Data__R_Field_30.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Mmm", "#WORK-MMM", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Fillera = pnd_Misc_Data__R_Field_30.newFieldInGroup("pnd_Misc_Data_Pnd_Fillera", "#FILLERA", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Ddd = pnd_Misc_Data__R_Field_30.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Ddd", "#WORK-DDD", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Fillerb = pnd_Misc_Data__R_Field_30.newFieldInGroup("pnd_Misc_Data_Pnd_Fillerb", "#FILLERB", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Yyy = pnd_Misc_Data__R_Field_30.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Yyy", "#WORK-YYY", FieldType.STRING, 2);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        pnd_Tiaa_Comp_Date_Start = localVariables.newFieldInRecord("pnd_Tiaa_Comp_Date_Start", "#TIAA-COMP-DATE-START", FieldType.STRING, 8);
        pnd_Tiaa_Comp_Date_End = localVariables.newFieldInRecord("pnd_Tiaa_Comp_Date_End", "#TIAA-COMP-DATE-END", FieldType.STRING, 8);
        pnd_Tiaa_Recvd_Date = localVariables.newFieldInRecord("pnd_Tiaa_Recvd_Date", "#TIAA-RECVD-DATE", FieldType.STRING, 8);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Clock_Dte_Tme = localVariables.newFieldInRecord("pnd_Clock_Dte_Tme", "#CLOCK-DTE-TME", FieldType.STRING, 15);

        pnd_Clock_Dte_Tme__R_Field_31 = localVariables.newGroupInRecord("pnd_Clock_Dte_Tme__R_Field_31", "REDEFINE", pnd_Clock_Dte_Tme);
        pnd_Clock_Dte_Tme_Pnd_Clock_Check_Date = pnd_Clock_Dte_Tme__R_Field_31.newFieldInGroup("pnd_Clock_Dte_Tme_Pnd_Clock_Check_Date", "#CLOCK-CHECK-DATE", 
            FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index_View.reset();
        vw_cwf_Txn.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("            SRA Requests With No Txns");
        pnd_Header1_2.setInitialValue("              Extract Create Report");
        pnd_Header2_1.setInitialValue("   SRA Requests With Txns Extract Create Report");
        pnd_Header2_2.setInitialValue("                        �");
        pnd_Parm_Type.setInitialValue("W");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3207() throws Exception
    {
        super("Cwfb3207");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        getReports().definePrinter(3, "NOT DEFINED");                                                                                                                     //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: DEFINE PRINTER ( 2 )
        //*                                                                                                                                                               //Natural: FORMAT ( 2 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        pnd_Misc_Data_Pnd_Filler1.setValue("/");                                                                                                                          //Natural: MOVE '/' TO #FILLER1 #FILLER2
        pnd_Misc_Data_Pnd_Filler2.setValue("/");
        pnd_Misc_Data_Pnd_Fillera.setValue("/");                                                                                                                          //Natural: MOVE '/' TO #FILLERA #FILLERB
        pnd_Misc_Data_Pnd_Fillerb.setValue("/");
        //*  GET THE RUN DATE
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.greater(pnd_Misc_Data_Pnd_Start_Date)))                                                                                               //Natural: IF #COMP-DATE GT #START-DATE
        {
            pnd_Misc_Data_Pnd_Today.setValue(pnd_Comp_Date);                                                                                                              //Natural: MOVE #COMP-DATE TO #TODAY
            //*  WEEK ENDING DATE 3 WEEKS AGO
            pnd_Work_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Misc_Data_Pnd_Today);                                                                         //Natural: MOVE EDITED #TODAY TO #WORK-DATE ( EM = YYYYMMDD )
            pnd_Work_Date.nsubtract(21);                                                                                                                                  //Natural: ASSIGN #WORK-DATE := #WORK-DATE - 21
            //*  WEEK STARTING DATE 3 WEEKS AGO
            pnd_Tiaa_Comp_Date_End.setValueEdited(pnd_Work_Date,new ReportEditMask("YYYYMMDD"));                                                                          //Natural: MOVE EDITED #WORK-DATE ( EM = YYYYMMDD ) TO #TIAA-COMP-DATE-END
            pnd_Work_Date.nsubtract(6);                                                                                                                                   //Natural: ASSIGN #WORK-DATE := #WORK-DATE - 6
            pnd_Tiaa_Comp_Date_Start.setValueEdited(pnd_Work_Date,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED #WORK-DATE ( EM = YYYYMMDD ) TO #TIAA-COMP-DATE-START
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "INVALID DATE:",pnd_Comp_Date,"ON TABLE");                                                                                              //Natural: WRITE 'INVALID DATE:' #COMP-DATE 'ON TABLE'
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, NEWLINE,"COMPARE START DATE",pnd_Tiaa_Comp_Date_Start,NEWLINE,"COMPARE END DATE  ",pnd_Tiaa_Comp_Date_End,NEWLINE,"TODAY             ",     //Natural: WRITE / 'COMPARE START DATE' #TIAA-COMP-DATE-START / 'COMPARE END DATE  ' #TIAA-COMP-DATE-END / 'TODAY             ' #TODAY
            pnd_Misc_Data_Pnd_Today);
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  PRIMARY FILE
        vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                        //Natural: READ CWF-MASTER-INDEX-VIEW BY PRTCPNT-CLOSED-KEY STARTING FROM #TIAA-COMP-DATE-START
        (
        "READ_PRIME",
        new Wc[] { new Wc("PRTCPNT_CLOSED_KEY", ">=", pnd_Tiaa_Comp_Date_Start, WcType.BY) },
        new Oc[] { new Oc("PRTCPNT_CLOSED_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Master_Index_View.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            //*   CHECK THAT THE CASE WAS CLOSED DURING THE WEEK LONG PERIOD ENDED
            //*   SIX WEEKS AGO
            pnd_Clock_Dte_Tme_Pnd_Clock_Check_Date.setValue(cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme);                                                              //Natural: MOVE CWF-MASTER-INDEX-VIEW.CRPRTE-CLOCK-END-DTE-TME TO #CLOCK-CHECK-DATE
            if (condition(pnd_Clock_Dte_Tme_Pnd_Clock_Check_Date.greater(pnd_Tiaa_Comp_Date_End)))                                                                        //Natural: IF #CLOCK-CHECK-DATE GT #TIAA-COMP-DATE-END
            {
                if (true) break READ_PRIME;                                                                                                                               //Natural: ESCAPE BOTTOM ( READ-PRIME. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //*   FIND ANY PARTICIPANT CLOSED (STATUS-CDE GE 8000) SRA FORMS
            //*   REQUEST (WPID = ONE OF THE VALUES IN #WPID-TBL)
            if (condition(!(cwf_Master_Index_View_Work_Prcss_Id.equals("FABIT") || cwf_Master_Index_View_Work_Prcss_Id.equals("FASIT") || cwf_Master_Index_View_Work_Prcss_Id.equals("BASB")  //Natural: ACCEPT IF CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID EQ 'FABIT' OR EQ 'FASIT' OR EQ 'BASB' OR EQ 'BABC'
                || cwf_Master_Index_View_Work_Prcss_Id.equals("BABC"))))
            {
                continue;
            }
            pnd_Counters_Pnd_Rex_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #REX-READ
            work_File_With.setValuesByName(vw_cwf_Master_Index_View);                                                                                                     //Natural: MOVE BY NAME CWF-MASTER-INDEX-VIEW TO WORK-FILE-WITH
            work_File_Without.setValuesByName(vw_cwf_Master_Index_View);                                                                                                  //Natural: MOVE BY NAME CWF-MASTER-INDEX-VIEW TO WORK-FILE-WITHOUT
            pnd_Forms_Clock_Date.setValue(cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme);                                                                                //Natural: ASSIGN #FORMS-CLOCK-DATE := CWF-MASTER-INDEX-VIEW.CRPRTE-CLOCK-END-DTE-TME
            //*  NOW GET THE TXN WPID ASSOCIATED WITH THE REQUEST WPID
            //*  USE THE CORRESPONDING ENTRY IN THE TXN-TBL
            pnd_Txn_Key_Pin_Nbr.setValue(cwf_Master_Index_View_Pin_Nbr);                                                                                                  //Natural: MOVE CWF-MASTER-INDEX-VIEW.PIN-NBR TO #TXN-KEY.PIN-NBR
            pnd_No_Txn_Found.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NO-TXN-FOUND
            pnd_Txn_Key_Work_Prcss_Id.setValue("TA I  ");                                                                                                                 //Natural: MOVE 'TA I  ' TO #TXN-KEY.WORK-PRCSS-ID
                                                                                                                                                                          //Natural: PERFORM READ-TXN
            sub_Read_Txn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_No_Txn_Found.getBoolean()))                                                                                                                 //Natural: IF #NO-TXN-FOUND
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-WITHOUT
                sub_Write_Work_Without();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* *SAG END-EXIT
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        if (condition(getReports().getAstLinesLeft(1).less(10)))                                                                                                          //Natural: NEWPAGE ( 1 ) IF LESS THAN 10 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total Number of Records with SRA WPID's and Participant Closed     :",pnd_Counters_Pnd_Rex_Read,NEWLINE,"Total Number of SAR Forms Request With Txns Written to Work File   :",pnd_Counters_Pnd_Sar_Forms_With,"Total Number of SAR Forms Request Without Txns Written to Work File:",pnd_Counters_Pnd_Sar_Forms_Without,NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) // 'Total Number of Records with SRA WPID"s and Participant Closed     :' #REX-READ / 'Total Number of SAR Forms Request With Txns Written to Work File   :' #SAR-FORMS-WITH 'Total Number of SAR Forms Request Without Txns Written to Work File:' #SAR-FORMS-WITHOUT //56T '**** End of Report ****'
            TabSetting(56),"**** End of Report ****");
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(2).less(10)))                                                                                                          //Natural: NEWPAGE ( 2 ) IF LESS THAN 10 LINES LEFT
        {
            getReports().newPage(2);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total Number of Records with SRA WPID's and Participant Closed     :",pnd_Counters_Pnd_Rex_Read,NEWLINE,"Total Number of SAR Forms Request With Txns Written to Work File   :",pnd_Counters_Pnd_Sar_Forms_With,"Total Number of SAR Forms Request Without Txns Written to Work File:",pnd_Counters_Pnd_Sar_Forms_Without,NEWLINE,NEWLINE,new  //Natural: WRITE ( 2 ) // 'Total Number of Records with SRA WPID"s and Participant Closed     :' #REX-READ / 'Total Number of SAR Forms Request With Txns Written to Work File   :' #SAR-FORMS-WITH 'Total Number of SAR Forms Request Without Txns Written to Work File:' #SAR-FORMS-WITHOUT //56T '**** End of Report ****'
            TabSetting(56),"**** End of Report ****");
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-TXN
        //* *****************************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-WITHOUT
        //*  'PIN/NBR'              WORK-FILE-WITHOUT.PIN-NBR
        //*                          (EM=9999999)
        //* *************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-WITH
        //* *************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME-SSN
        //* *****************************
        //* *SAG END-EXIT
    }
    private void sub_Read_Txn() throws Exception                                                                                                                          //Natural: READ-TXN
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        vw_cwf_Txn.startDatabaseRead                                                                                                                                      //Natural: READ CWF-TXN BY PIN-WPID-KEY STARTING FROM #TXN-KEY
        (
        "READ_TXN",
        new Wc[] { new Wc("PIN_WPID_KEY", ">=", pnd_Txn_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_WPID_KEY", "ASC") }
        );
        READ_TXN:
        while (condition(vw_cwf_Txn.readNextRow("READ_TXN")))
        {
            if (condition(cwf_Txn_Pin_Nbr.notEquals(pnd_Txn_Key_Pin_Nbr)))                                                                                                //Natural: IF CWF-TXN.PIN-NBR NE #TXN-KEY.PIN-NBR
            {
                if (true) break READ_TXN;                                                                                                                                 //Natural: ESCAPE BOTTOM ( READ-TXN. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Recvd_Date.setValueEdited(cwf_Txn_Tiaa_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED CWF-TXN.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO #TIAA-RECVD-DATE
            if (condition(!(cwf_Txn_Work_Prcss_Id.equals(pnd_Txn_Key_Work_Prcss_Id) && pnd_Tiaa_Recvd_Date.greater(pnd_Forms_Clock_Date) && pnd_Tiaa_Recvd_Date.lessOrEqual(pnd_Misc_Data_Pnd_Today)))) //Natural: ACCEPT IF CWF-TXN.WORK-PRCSS-ID EQ #TXN-KEY.WORK-PRCSS-ID AND #TIAA-RECVD-DATE GT #FORMS-CLOCK-DATE AND #TIAA-RECVD-DATE LE #TODAY
            {
                continue;
            }
            pnd_No_Txn_Found.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NO-TXN-FOUND
            work_File_With_Txn_Wpid.setValue(cwf_Txn_Work_Prcss_Id);                                                                                                      //Natural: ASSIGN WORK-FILE-WITH.TXN-WPID := CWF-TXN.WORK-PRCSS-ID
            work_File_With_Txn_Tiaa_Rcvd_Dte.setValue(cwf_Txn_Tiaa_Rcvd_Dte);                                                                                             //Natural: ASSIGN WORK-FILE-WITH.TXN-TIAA-RCVD-DTE := CWF-TXN.TIAA-RCVD-DTE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-WITH
            sub_Write_Work_With();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_TXN"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_TXN"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *****************************
        //*  READ-FOR-TXN
    }
    private void sub_Write_Work_Without() throws Exception                                                                                                                //Natural: WRITE-WORK-WITHOUT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        pdaCwfa5372.getCwfa5372_Pnd_Pin_Key().setValue(work_File_Without_Pin_Nbr);                                                                                        //Natural: MOVE WORK-FILE-WITHOUT.PIN-NBR TO CWFA5372.#PIN-KEY
                                                                                                                                                                          //Natural: PERFORM GET-NAME-SSN
        sub_Get_Name_Ssn();
        if (condition(Global.isEscape())) {return;}
        work_File_Without_Name.setValue(pdaCwfa5372.getCwfa5372_Pnd_Pass_Name());                                                                                         //Natural: MOVE #PASS-NAME TO WORK-FILE-WITHOUT.NAME
        work_File_Without_Ssn.setValue(pdaCwfa5372.getCwfa5372_Pnd_Pass_Ssn());                                                                                           //Natural: MOVE #PASS-SSN TO WORK-FILE-WITHOUT.SSN
        pnd_Counters_Pnd_Sar_Forms_Without.nadd(1);                                                                                                                       //Natural: ADD 1 TO #SAR-FORMS-WITHOUT
        getWorkFiles().write(1, false, work_File_Without);                                                                                                                //Natural: WRITE WORK FILE 1 WORK-FILE-WITHOUT
        //*  PIN-EXP <<
        getReports().display(1, "PIN/NBR",                                                                                                                                //Natural: DISPLAY ( 1 ) 'PIN/NBR' WORK-FILE-WITHOUT.PIN-NBR ( EM = 999999999999 ) '/NAME' WORK-FILE-WITHOUT.NAME '/SSN' WORK-FILE-WITHOUT.SSN '/TIAA REC"D DATE' WORK-FILE-WITHOUT.TIAA-RCVD-DTE 'FORMS/WPID' WORK-FILE-WITHOUT.WORK-PRCSS-ID 'CLOCK/END DT' #FORMS-CLOCK-DATE
        		work_File_Without_Pin_Nbr, new ReportEditMask ("999999999999"),"/NAME",
        		work_File_Without_Name,"/SSN",
        		work_File_Without_Ssn,"/TIAA REC'D DATE",
        		work_File_Without_Tiaa_Rcvd_Dte,"FORMS/WPID",
        		work_File_Without_Work_Prcss_Id,"CLOCK/END DT",
        		pnd_Forms_Clock_Date);
        if (Global.isEscape()) return;
        //* *************
        //*  WRITE-WORK-WITHOUT
    }
    private void sub_Write_Work_With() throws Exception                                                                                                                   //Natural: WRITE-WORK-WITH
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pdaCwfa5372.getCwfa5372_Pnd_Pin_Key().setValue(work_File_With_Pin_Nbr);                                                                                           //Natural: MOVE WORK-FILE-WITH.PIN-NBR TO CWFA5372.#PIN-KEY
                                                                                                                                                                          //Natural: PERFORM GET-NAME-SSN
        sub_Get_Name_Ssn();
        if (condition(Global.isEscape())) {return;}
        work_File_With_Name.setValue(pdaCwfa5372.getCwfa5372_Pnd_Pass_Name());                                                                                            //Natural: MOVE #PASS-NAME TO WORK-FILE-WITH.NAME
        work_File_With_Ssn.setValue(pdaCwfa5372.getCwfa5372_Pnd_Pass_Ssn());                                                                                              //Natural: MOVE #PASS-SSN TO WORK-FILE-WITH.SSN
        pnd_Counters_Pnd_Sar_Forms_With.nadd(1);                                                                                                                          //Natural: ADD 1 TO #SAR-FORMS-WITH
        getWorkFiles().write(2, false, work_File_With);                                                                                                                   //Natural: WRITE WORK FILE 2 WORK-FILE-WITH
        //* PIN-EXP
        getReports().display(2, "PIN/NBR",                                                                                                                                //Natural: DISPLAY ( 2 ) 'PIN/NBR' WORK-FILE-WITH.PIN-NBR ( EM = 999999999999 ) '/NAME' WORK-FILE-WITH.NAME '/SSN' WORK-FILE-WITH.SSN '/TIAA REC"D DATE' WORK-FILE-WITH.TIAA-RCVD-DTE 'FORMS/WPID' WORK-FILE-WITH.WORK-PRCSS-ID 'TXN/WPID' WORK-FILE-WITH.TXN-WPID 'TXN/RECVD DTE' WORK-FILE-WITH.TXN-TIAA-RCVD-DTE 'CLOCK/END DT' #FORMS-CLOCK-DATE
        		work_File_With_Pin_Nbr, new ReportEditMask ("999999999999"),"/NAME",
        		work_File_With_Name,"/SSN",
        		work_File_With_Ssn,"/TIAA REC'D DATE",
        		work_File_With_Tiaa_Rcvd_Dte,"FORMS/WPID",
        		work_File_With_Work_Prcss_Id,"TXN/WPID",
        		work_File_With_Txn_Wpid,"TXN/RECVD DTE",
        		work_File_With_Txn_Tiaa_Rcvd_Dte,"CLOCK/END DT",
        		pnd_Forms_Clock_Date);
        if (Global.isEscape()) return;
        //* *************
        //*  WRITE-WORK-WITH
    }
    private void sub_Get_Name_Ssn() throws Exception                                                                                                                      //Natural: GET-NAME-SSN
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        DbsUtil.callnat(Cwfn5372.class , getCurrentProcessState(), pdaCwfa5372.getCwfa5372_Pnd_Pin_Key(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Name(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Ssn(),  //Natural: CALLNAT 'CWFN5372' #PIN-KEY #PASS-NAME #PASS-SSN #PASS-TLC #PASS-DOB #PASS-FOUND
            pdaCwfa5372.getCwfa5372_Pnd_Pass_Tlc(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Dob(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Found());
        if (condition(Global.isEscape())) return;
        if (condition(! (pdaCwfa5372.getCwfa5372_Pnd_Pass_Found().getBoolean())))                                                                                         //Natural: IF NOT #PASS-FOUND
        {
            pdaCwfa5372.getCwfa5372_Pnd_Pass_Ssn().setValue(999999999);                                                                                                   //Natural: MOVE 999999999 TO #PASS-SSN
            pdaCwfa5372.getCwfa5372_Pnd_Pass_Name().setValue(DbsUtil.compress("NO NAME ON FILE FOR PIN NBR:", pdaCwfa5372.getCwfa5372_Pnd_Pin_Key()));                    //Natural: COMPRESS 'NO NAME ON FILE FOR PIN NBR:' #PIN-KEY INTO #PASS-NAME
        }                                                                                                                                                                 //Natural: END-IF
        //* *****************************
        //*  GET-NAME-SSN
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP ) / '_' ( 132 )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header1_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,"_",new RepeatItem(132));
                    //* *SAG DEFINE EXIT REPORT1-AT-TOP-OF-PAGE
                    //* *SAG END-EXIT
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header2_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(2),  //Natural: WRITE ( 2 ) NOTITLE *PROGRAM 41T #HEADER2-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 2 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) #HEADER2-2 124T *TIMX ( EM = HH':'II' 'AP ) / '_' ( 132 )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),pnd_Header2_2,new 
                        TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,"_",new RepeatItem(132));
                    //* *SAG DEFINE EXIT REPORT2-AT-TOP-OF-PAGE
                    //* *SAG END-EXIT
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "PIN/NBR",
        		work_File_Without_Pin_Nbr, new ReportEditMask ("999999999999"),"/NAME",
        		work_File_Without_Name,"/SSN",
        		work_File_Without_Ssn,"/TIAA REC'D DATE",
        		work_File_Without_Tiaa_Rcvd_Dte,"FORMS/WPID",
        		work_File_Without_Work_Prcss_Id,"CLOCK/END DT",
        		pnd_Forms_Clock_Date);
        getReports().setDisplayColumns(2, "PIN/NBR",
        		work_File_With_Pin_Nbr, new ReportEditMask ("999999999999"),"/NAME",
        		work_File_With_Name,"/SSN",
        		work_File_With_Ssn,"/TIAA REC'D DATE",
        		work_File_With_Tiaa_Rcvd_Dte,"FORMS/WPID",
        		work_File_With_Work_Prcss_Id,"TXN/WPID",
        		work_File_With_Txn_Wpid,"TXN/RECVD DTE",
        		work_File_With_Txn_Tiaa_Rcvd_Dte,"CLOCK/END DT",
        		pnd_Forms_Clock_Date);
    }
}
