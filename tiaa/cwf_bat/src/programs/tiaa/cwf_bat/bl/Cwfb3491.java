/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:53 PM
**        * FROM NATURAL PROGRAM : Cwfb3491
************************************************************
**        * FILE NAME            : Cwfb3491.java
**        * CLASS NAME           : Cwfb3491
**        * INSTANCE NAME        : Cwfb3491
************************************************************
************************************************************************
* PROGRAM  : CWFB3491
* SYSTEM   : CWF
* TITLE    :
* GENERATED: OCT 05,2000
* FUNCTION : EXTRACTS ALL ACTIVE EMPLOYEE RACF ID's and Names
*          :   FOR A SPECIFIC UNIT FROM THE SUPPORT TABLE
*          :   FOR LOADING TO ORACLE (FOR SMART)
*          :
*          :
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------   -------- --------------------------------------------------
*
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3491 extends BLNatBase
{
    // Data Areas
    private PdaCwfl5312 pdaCwfl5312;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Org_Empl_Tbl;
    private DbsField cwf_Org_Empl_Tbl_Empl_Unit_Cde;
    private DbsField cwf_Org_Empl_Tbl_Empl_Racf_Id;
    private DbsField cwf_Org_Empl_Tbl_Empl_Nme;

    private DbsGroup cwf_Org_Empl_Tbl__R_Field_1;
    private DbsField cwf_Org_Empl_Tbl_Empl_First_Nme;
    private DbsField cwf_Org_Empl_Tbl_Empl_Last_Nme;
    private DbsField cwf_Org_Empl_Tbl_Actve_Ind;
    private DbsField cwf_Org_Empl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Org_Empl_Tbl_Dlte_Oprtr_Cde;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Tbl_Employee;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Group_Name;
    private DbsField pnd_Work_File_Key_Name;

    private DbsGroup pnd_Work_File__R_Field_3;
    private DbsField pnd_Work_File_Empl_Unit_Cde;
    private DbsField pnd_Work_File_Empl_Racf_Id;
    private DbsField pnd_Work_File_Empl_Nme;
    private DbsField pnd_Unit_Empl_Racf_Id_Key;

    private DbsGroup pnd_Unit_Empl_Racf_Id_Key__R_Field_4;
    private DbsField pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Unit_Cde;
    private DbsField pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Racf_Id;
    private DbsField pnd_Unit_Empl_Racf_Id_Key_Pnd_Actve_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfl5312 = new PdaCwfl5312(localVariables);

        // Local Variables

        vw_cwf_Org_Empl_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Empl_Tbl", "CWF-ORG-EMPL-TBL"), "CWF_ORG_EMPL_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Empl_Tbl_Empl_Unit_Cde = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Unit_Cde", "EMPL-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_UNIT_CDE");
        cwf_Org_Empl_Tbl_Empl_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Empl_Tbl_Empl_Racf_Id = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        cwf_Org_Empl_Tbl_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF  ID");
        cwf_Org_Empl_Tbl_Empl_Nme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Nme", "EMPL-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "EMPL_NME");
        cwf_Org_Empl_Tbl_Empl_Nme.setDdmHeader("EMPLOYEE NAME");

        cwf_Org_Empl_Tbl__R_Field_1 = vw_cwf_Org_Empl_Tbl.getRecord().newGroupInGroup("cwf_Org_Empl_Tbl__R_Field_1", "REDEFINE", cwf_Org_Empl_Tbl_Empl_Nme);
        cwf_Org_Empl_Tbl_Empl_First_Nme = cwf_Org_Empl_Tbl__R_Field_1.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_First_Nme", "EMPL-FIRST-NME", FieldType.STRING, 
            20);
        cwf_Org_Empl_Tbl_Empl_Last_Nme = cwf_Org_Empl_Tbl__R_Field_1.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Last_Nme", "EMPL-LAST-NME", FieldType.STRING, 
            20);
        cwf_Org_Empl_Tbl_Actve_Ind = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Org_Empl_Tbl_Dlte_Dte_Tme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Org_Empl_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        cwf_Org_Empl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Org_Empl_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPERATOR");
        registerRecord(vw_cwf_Org_Empl_Tbl);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_2 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_Employee = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Tbl_Employee", "TBL-EMPLOYEE", FieldType.STRING, 8);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Group_Name = pnd_Work_File.newFieldInGroup("pnd_Work_File_Group_Name", "GROUP-NAME", FieldType.STRING, 30);
        pnd_Work_File_Key_Name = pnd_Work_File.newFieldInGroup("pnd_Work_File_Key_Name", "KEY-NAME", FieldType.STRING, 16);

        pnd_Work_File__R_Field_3 = pnd_Work_File.newGroupInGroup("pnd_Work_File__R_Field_3", "REDEFINE", pnd_Work_File_Key_Name);
        pnd_Work_File_Empl_Unit_Cde = pnd_Work_File__R_Field_3.newFieldInGroup("pnd_Work_File_Empl_Unit_Cde", "EMPL-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_File_Empl_Racf_Id = pnd_Work_File__R_Field_3.newFieldInGroup("pnd_Work_File_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Work_File_Empl_Nme = pnd_Work_File.newFieldInGroup("pnd_Work_File_Empl_Nme", "EMPL-NME", FieldType.STRING, 40);
        pnd_Unit_Empl_Racf_Id_Key = localVariables.newFieldInRecord("pnd_Unit_Empl_Racf_Id_Key", "#UNIT-EMPL-RACF-ID-KEY", FieldType.STRING, 17);

        pnd_Unit_Empl_Racf_Id_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Unit_Empl_Racf_Id_Key__R_Field_4", "REDEFINE", pnd_Unit_Empl_Racf_Id_Key);
        pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Unit_Cde = pnd_Unit_Empl_Racf_Id_Key__R_Field_4.newFieldInGroup("pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Unit_Cde", 
            "#EMPL-UNIT-CDE", FieldType.STRING, 8);
        pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Racf_Id = pnd_Unit_Empl_Racf_Id_Key__R_Field_4.newFieldInGroup("pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Racf_Id", 
            "#EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Unit_Empl_Racf_Id_Key_Pnd_Actve_Ind = pnd_Unit_Empl_Racf_Id_Key__R_Field_4.newFieldInGroup("pnd_Unit_Empl_Racf_Id_Key_Pnd_Actve_Ind", "#ACTVE-IND", 
            FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Org_Empl_Tbl.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Work_File_Group_Name.setInitialValue("UNIT-EMPLOYEE-TBL");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3491() throws Exception
    {
        super("Cwfb3491");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Authrty().setValue("A");                                                                                                   //Natural: MOVE 'A' TO #GEN-TBL-AUTHRTY
        pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Name().setValue("SMART-EXTRACT");                                                                                          //Natural: MOVE 'SMART-EXTRACT' TO #GEN-TBL-NAME
        pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Code().setValue("EMPLOYEE");                                                                                               //Natural: MOVE 'EMPLOYEE' TO #GEN-TBL-CODE
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY FROM #GEN-TBL-KEY
        (
        "RD1",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Key(), WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        RD1:
        while (condition(vw_cwf_Support_Tbl.readNextRow("RD1")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Scrty_Level_Ind.notEquals(pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Authrty()) || cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Name())  //Natural: IF TBL-SCRTY-LEVEL-IND NE #GEN-TBL-AUTHRTY OR TBL-TABLE-NME NE #GEN-TBL-NAME OR TBL-EMPLOYEE NE #GEN-TBL-CODE
                || cwf_Support_Tbl_Tbl_Employee.notEquals(pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Code())))
            {
                if (true) break RD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Unit_Empl_Racf_Id_Key.reset();                                                                                                                            //Natural: RESET #UNIT-EMPL-RACF-ID-KEY
            pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Unit_Cde.setValue(cwf_Support_Tbl_Tbl_Data_Field);                                                                         //Natural: MOVE TBL-DATA-FIELD TO #EMPL-UNIT-CDE
            vw_cwf_Org_Empl_Tbl.startDatabaseRead                                                                                                                         //Natural: READ CWF-ORG-EMPL-TBL BY UNIT-EMPL-RACF-ID-KEY #UNIT-EMPL-RACF-ID-KEY
            (
            "RD2",
            new Wc[] { new Wc("UNIT_EMPL_RACF_ID_KEY", ">=", pnd_Unit_Empl_Racf_Id_Key, WcType.BY) },
            new Oc[] { new Oc("UNIT_EMPL_RACF_ID_KEY", "ASC") }
            );
            RD2:
            while (condition(vw_cwf_Org_Empl_Tbl.readNextRow("RD2")))
            {
                if (condition(cwf_Org_Empl_Tbl_Empl_Unit_Cde.notEquals(pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Unit_Cde)))                                                     //Natural: IF CWF-ORG-EMPL-TBL.EMPL-UNIT-CDE NE #EMPL-UNIT-CDE
                {
                    if (true) break RD2;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RD2. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cwf_Org_Empl_Tbl_Dlte_Dte_Tme.greater(getZero()) || cwf_Org_Empl_Tbl_Dlte_Oprtr_Cde.greater(" ")))                                          //Natural: REJECT IF DLTE-DTE-TME GT 0 OR DLTE-OPRTR-CDE GT ' '
                {
                    continue;
                }
                pnd_Work_File.setValuesByName(vw_cwf_Org_Empl_Tbl);                                                                                                       //Natural: MOVE BY NAME CWF-ORG-EMPL-TBL TO #WORK-FILE
                getWorkFiles().write(1, false, pnd_Work_File);                                                                                                            //Natural: WRITE WORK FILE 1 #WORK-FILE
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
