/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:26:42 PM
**        * FROM NATURAL PROGRAM : Cwfb3015
************************************************************
**        * FILE NAME            : Cwfb3015.java
**        * CLASS NAME           : Cwfb3015
**        * INSTANCE NAME        : Cwfb3015
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: MICRO JACKET RUSH REQUEST
**SAG SYSTEM: CRPCWF
**SAG REPORT-HEADING(1): CORPORATE WORKFLOW FACILITIES
**SAG PRINT-FILE(1): 1
**SAG REPORT-HEADING(2): DAILY REPORT OF RUSH MICROJACKETS REQUESTED
**SAG PRINT-FILE(2): 1
**SAG HEADING-LINE: 12000000
**SAG DESCS(1): THIS PROGRAM READS THE MASTER INDEX FILE AND WRITES A R
**SAG DESCS(2): EPORT OF RUSH MICRO JACKET REQUESTS
**SAG PRIMARY-FILE: CWF-MASTER-INDEX-VIEW
**SAG PRIMARY-KEY: LAST-CHNGE-DTE-KEY
**SAG PRIME-START: #TODAY
************************************************************************
* PROGRAM  : CWFB3015
* SYSTEM   : CRPCWF
* TITLE    : MICRO JACKET RUSH REQUEST
* GENERATED: SEP 05,95 AT 10:16 AM
* FUNCTION : THIS PROGRAM READS THE MASTER INDEX FILE AND WRITES A R
*            EPORT OF RUSH MICRO JACKET REQUESTS
*
*
* HISTORY
* 09/6/95   JVH FIX TO USE CORRECT BUSINESS DATE
* 09/14/95  JVH ADD COLUMN TO REPORT TO SHOW RUSH/NON RUSH AT PRTR TZFP
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3015 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_1;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Rgn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Brnch_Cde;
    private DbsField cwf_Master_Index_View_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Case_Id_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_2;
    private DbsField cwf_Master_Index_View_Case_Ind;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind;
    private DbsField cwf_Master_Index_View_Multi_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_3;
    private DbsField cwf_Master_Index_View_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_View_Wpid_Vldte_Ind;
    private DbsField cwf_Master_Index_View_Unit_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_4;
    private DbsField cwf_Master_Index_View_Unit_Id_Cde;
    private DbsField cwf_Master_Index_View_Unit_Rgn_Cde;
    private DbsField cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_View_Unit_Brnch_Group_Cde;
    private DbsField cwf_Master_Index_View_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Old_Route_Cde;
    private DbsField cwf_Master_Index_View_Work_Rqst_Prty_Cde;
    private DbsField cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Assgn_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_5;
    private DbsField cwf_Master_Index_View_Empl_Racf_Id;
    private DbsField cwf_Master_Index_View_Empl_Sffx_Cde;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_6;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_Tme_8_15;
    private DbsField cwf_Master_Index_View_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_View_Step_Id;
    private DbsField cwf_Master_Index_View_Rt_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Step_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Step_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Status_Cde;
    private DbsField cwf_Master_Index_View_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Last_Updte_Dte;
    private DbsField cwf_Master_Index_View_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Cntct_Orgn_Type_Cde;
    private DbsField cwf_Master_Index_View_Cntct_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Oprtr_Id;
    private DbsField cwf_Master_Index_View_Actve_Ind;

    private DbsGroup cwf_Master_Index_View__R_Field_7;
    private DbsField cwf_Master_Index_View_Fill_1;
    private DbsField cwf_Master_Index_View_Actve_Ind_2_2;
    private DbsField cwf_Master_Index_View_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_View_Cnflct_Ind;
    private DbsField cwf_Master_Index_View_Spcl_Hndlng_Txt;
    private DbsField cwf_Master_Index_View_Instn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Instn_Cde;
    private DbsGroup cwf_Master_Index_View_Spcl_Policy_Srce_CdeMuGroup;
    private DbsField cwf_Master_Index_View_Spcl_Policy_Srce_Cde;
    private DbsField cwf_Master_Index_View_Attntn_Txt;
    private DbsField cwf_Master_Index_View_Check_Ind;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_View_Effctve_Dte;
    private DbsField cwf_Master_Index_View_Trnsctn_Dte;
    private DbsField cwf_Master_Index_View_Trans_Dte;
    private DbsField cwf_Master_Index_View_Physcl_Fldr_Id_Nbr;
    private DbsField cwf_Master_Index_View_Mj_Chrge_Dte_Tme;
    private DbsField cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Mstr_Indx_Actn_Cde;
    private DbsField cwf_Master_Index_View_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_View_Final_Close_Out_Dte_Tme;
    private DbsField cwf_Master_Index_View_Final_Close_Out_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Mj_Emrgncy_Rqst_Dte_Tme;
    private DbsField cwf_Master_Index_View_Mj_Emrgncy_Rqst_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Print_Q_Ind;
    private DbsField cwf_Master_Index_View_Print_Dte_Tme;
    private DbsField cwf_Master_Index_View_Print_Batch_Id_Nbr;

    private DbsGroup cwf_Master_Index_View__R_Field_8;
    private DbsField cwf_Master_Index_View_Print_Prefix_Ind;
    private DbsField cwf_Master_Index_View_Print_Batch_Nbr;
    private DbsField cwf_Master_Index_View_Print_Batch_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Print_Batch_Ind;
    private DbsField cwf_Master_Index_View_Printer_Id_Cde;
    private DbsField cwf_Master_Index_View_Rescan_Ind;
    private DbsField cwf_Master_Index_View_Dup_Ind;
    private DbsField cwf_Master_Index_View_Cmplnt_Ind;
    private DbsGroup cwf_Master_Index_View_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Cntrct_Nbr;
    private DbsGroup cwf_Master_Index_View_Mail_Item_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Mail_Item_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_9;
    private DbsField cwf_Master_Index_View_Rqst_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Case_Id_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rlte_Rqst_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_10;
    private DbsField cwf_Master_Index_View_Rlte_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Rlte_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rlte_Case_Id_Cde;
    private DbsField cwf_Master_Index_View_Rlte_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_View_Work_List_Ind;

    private DbsGroup cwf_Master_Index_View__R_Field_11;
    private DbsField cwf_Master_Index_View_Work_List_Ind_1_1;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_12;
    private DbsField cwf_Master_Index_View_Due_Dte;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Ind;
    private DbsField cwf_Master_Index_View_Due_Dte_Prty;
    private DbsField cwf_Master_Index_View_Filler_1406_9818;
    private DbsField cwf_Master_Index_View_Sbsqnt_Cntct_Actn_Rqrd;
    private DbsField cwf_Master_Index_View_Sbsqnt_Cntct_Ind;
    private DbsField cwf_Master_Index_View_Prcssng_Type;
    private DbsField cwf_Master_Index_View_Bsnss_Reply_Ind;
    private DbsField cwf_Master_Index_View_Status_Freeze_Ind;
    private DbsField cwf_Master_Index_View_Elctrnc_Fldr_Ind;
    private DbsField cwf_Master_Index_View_Log_Insttn_Srce_Cde;
    private DbsField cwf_Master_Index_View_Log_Rqstr_Cde;
    private DbsField cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Empl_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Days;
    private DbsField cwf_Master_Index_View_Step_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Step_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_13;
    private DbsField cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme_1_8;
    private DbsField cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme;
    private DbsField cwf_Master_Index_View_Acknwldgmnt_Cde;
    private DbsField cwf_Master_Index_View_Acknwldgmnt_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Acknwldgmnt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Sheet_Print_Cde;
    private DbsField cwf_Master_Index_View_Cntct_Sheet_Print_Dte_Tme;
    private DbsField cwf_Master_Index_View_Cntct_Sheet_Printer_Id_Cde;
    private DbsField cwf_Master_Index_View_Shphrd_Id;
    private DbsField cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_14;
    private DbsField cwf_Master_Index_View_Crrnt_Due_Dte;
    private DbsField cwf_Master_Index_View_Crrnt_Cmt_Ind;
    private DbsField cwf_Master_Index_View_Crrnt_Prrty_Cde;
    private DbsField cwf_Master_Index_View_Crrnt_Due_Tme;
    private DbsField cwf_Master_Index_View_Crprte_Due_Dte_Tme;
    private DbsField cwf_Master_Index_View_Archvd_Dte;
    private DbsField cwf_Master_Index_View_Rstr_To_Crrnt_Dte;
    private DbsField cwf_Master_Index_View_Off_Rtng_Ind;
    private DbsField cwf_Master_Index_View_Unit_On_Tme_Ind;
    private DbsField cwf_Master_Index_View_Crprte_On_Tme_Ind;
    private DbsField cwf_Master_Index_View_Owner_Unit_Cde;
    private DbsField cwf_Master_Index_View_Physcl_Fldr_Owner_Id;
    private DbsField cwf_Master_Index_View_Physcl_Fldr_Owner_Unit_Cde;
    private DbsField cwf_Master_Index_View_Step_Re_Do_Ind;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_View_Trade_Dte_Tme;
    private DbsField cwf_Master_Index_View_Future_Pymnt_Ind;
    private DbsField cwf_Master_Index_View_Status_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_15;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_16;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Status_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_17;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_18;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Step_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_19;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_20;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Empl_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_21;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_22;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Unit_Elpsd_Bsnss_Minutes;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_23;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Days;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Hours;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Minutes;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme;

    private DbsGroup cwf_Master_Index_View__R_Field_24;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Days;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Hours;
    private DbsField cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Minutes;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;
    private DbsField pnd_Prev_Unit_Cde;
    private DbsField pnd_Wpid;
    private DbsField pnd_Unit;
    private DbsField pnd_Today;

    private DbsGroup pnd_Today__R_Field_25;
    private DbsField pnd_Today_Pnd_Today_A;
    private DbsField pnd_Diff;
    private DbsField pnd_Alpha_Date;
    private DbsField pnd_Mj_Date_Time;

    private DbsGroup pnd_Mj_Date_Time__R_Field_26;
    private DbsField pnd_Mj_Date_Time_Pnd_Mj_Date;

    private DbsGroup pnd_Mj_Date_Time__R_Field_27;
    private DbsField pnd_Mj_Date_Time_Pnd_Mj_Date_Time_N;
    private DbsField pnd_Last_Chng_Date_Time;

    private DbsGroup counters;
    private DbsField counters_Pnd_Tot_Work;
    private DbsField counters_Pnd_Tot_Unit;
    private DbsField counters_Pnd_Total;
    private DbsField counters_Pnd_Tot_Rush;
    private DbsField counters_Pnd_Tot_Not_Rush;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_28;
    private DbsField cwf_Support_Tbl_Tbl_Last_Run_Date;
    private DbsField cwf_Support_Tbl_Tbl_Last_Run_Flag;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_29;

    private DbsGroup pnd_Tbl_Key_Data_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Key_Field;
    private DbsField pnd_Msg;

    private DbsGroup constants;
    private DbsField constants_Pnd_Rush;
    private DbsField constants_Pnd_Not_Rush;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Work_Prcss_IdOld;
    private DbsField sort01Unit_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Pin_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Master_Index_View_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index_View__R_Field_1 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_1", "REDEFINE", cwf_Master_Index_View_Rqst_Log_Dte_Tme);
        cwf_Master_Index_View_Rqst_Log_Index_Dte = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Log_Index_Tme = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme", 
            "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_View_Rqst_Orgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Orgn_Cde", "RQST-ORGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_View_Rqst_Rgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Rgn_Cde", "RQST-RGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_RGN_CDE");
        cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde", 
            "RQST-SPCL-DSGNTN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_SPCL_DSGNTN_CDE");
        cwf_Master_Index_View_Rqst_Brnch_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Brnch_Cde", "RQST-BRNCH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_BRNCH_CDE");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_View_Sub_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Ind", "SUB-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SUB_RQST_IND");
        cwf_Master_Index_View_Sub_Rqst_Ind.setDdmHeader("SUB-REQUEST COUNT");
        cwf_Master_Index_View_Case_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CASE_ID_CDE");
        cwf_Master_Index_View_Case_Id_Cde.setDdmHeader("CASE/ID");

        cwf_Master_Index_View__R_Field_2 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_2", "REDEFINE", cwf_Master_Index_View_Case_Id_Cde);
        cwf_Master_Index_View_Case_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Case_Ind", "CASE-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Sqnce_Ind", "SUB-RQST-SQNCE-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Multi_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Multi_Rqst_Ind", "MULTI-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        cwf_Master_Index_View_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Master_Index_View_Orgnl_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_View_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");

        cwf_Master_Index_View__R_Field_3 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_3", "REDEFINE", cwf_Master_Index_View_Work_Prcss_Id);
        cwf_Master_Index_View_Work_Actn_Rqstd_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Lob_Cmpny_Prdct_Cde", 
            "WORK-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Mjr_Bsnss_Prcss_Cde", 
            "WORK-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Master_Index_View__R_Field_3.newFieldInGroup("cwf_Master_Index_View_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        cwf_Master_Index_View_Wpid_Vldte_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Wpid_Vldte_Ind", "WPID-VLDTE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WPID_VLDTE_IND");
        cwf_Master_Index_View_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Cde", "UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_CDE");
        cwf_Master_Index_View_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Master_Index_View__R_Field_4 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_4", "REDEFINE", cwf_Master_Index_View_Unit_Cde);
        cwf_Master_Index_View_Unit_Id_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 
            5);
        cwf_Master_Index_View_Unit_Rgn_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Unit_Brnch_Group_Cde = cwf_Master_Index_View__R_Field_4.newFieldInGroup("cwf_Master_Index_View_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Unit_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Index_View_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Index_View_Old_Route_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Old_Route_Cde", "OLD-ROUTE-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "OLD_ROUTE_CDE");
        cwf_Master_Index_View_Old_Route_Cde.setDdmHeader("ACTIVITY-END/STATUS");
        cwf_Master_Index_View_Work_Rqst_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Rqst_Prty_Cde", "WORK-RQST-PRTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WORK_RQST_PRTY_CDE");
        cwf_Master_Index_View_Work_Rqst_Prty_Cde.setDdmHeader("PRIO");
        cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde", 
            "ASSGN-SPRVSR-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ASSGN_SPRVSR_OPRTR_CDE");
        cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde.setDdmHeader("SUPER/VISOR");
        cwf_Master_Index_View_Assgn_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Assgn_Dte_Tme", "ASSGN-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ASSGN_DTE_TME");
        cwf_Master_Index_View_Assgn_Dte_Tme.setDdmHeader("ASSIGNMENT/DATE-TIME");
        cwf_Master_Index_View_Empl_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_View_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");

        cwf_Master_Index_View__R_Field_5 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_5", "REDEFINE", cwf_Master_Index_View_Empl_Oprtr_Cde);
        cwf_Master_Index_View_Empl_Racf_Id = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Empl_Sffx_Cde = cwf_Master_Index_View__R_Field_5.newFieldInGroup("cwf_Master_Index_View_Empl_Sffx_Cde", "EMPL-SFFX-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Last_Chnge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        cwf_Master_Index_View__R_Field_6 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_6", "REDEFINE", cwf_Master_Index_View_Last_Chnge_Dte_Tme);
        cwf_Master_Index_View_Last_Chnge_Dte_Tme_8_15 = cwf_Master_Index_View__R_Field_6.newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_Tme_8_15", 
            "LAST-CHNGE-DTE-TME-8-15", FieldType.NUMERIC, 8);
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Oprtr_Cde", 
            "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme", 
            "LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Unit_Cde", 
            "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_View_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_View_Step_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Id", "STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "STEP_ID");
        cwf_Master_Index_View_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Index_View_Rt_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        cwf_Master_Index_View_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        cwf_Master_Index_View_Step_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Sqnce_Nbr", "STEP-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "STEP_SQNCE_NBR");
        cwf_Master_Index_View_Step_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Updte_Dte_Tme", "STEP-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STEP_UPDTE_DTE_TME");
        cwf_Master_Index_View_Step_Updte_Dte_Tme.setDdmHeader("STEP UPDATE/DATE-TIME");
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_View_Admin_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme", 
            "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_View_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Dte_Tme", 
            "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Index_View_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Updte_Oprtr_Cde", 
            "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Updte_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Dte", "LAST-UPDTE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        cwf_Master_Index_View_Last_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Index_View_Last_Updte_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Updte_Oprtr_Cde", 
            "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Index_View_Cntct_Orgn_Type_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Orgn_Type_Cde", 
            "CNTCT-ORGN-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTCT_ORGN_TYPE_CDE");
        cwf_Master_Index_View_Cntct_Orgn_Type_Cde.setDdmHeader("CONTACT/CODE");
        cwf_Master_Index_View_Cntct_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Dte_Tme", "CNTCT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "CNTCT_DTE_TME");
        cwf_Master_Index_View_Cntct_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Invrt_Dte_Tme", 
            "CNTCT-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "CNTCT_INVRT_DTE_TME");
        cwf_Master_Index_View_Cntct_Oprtr_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Oprtr_Id", "CNTCT-OPRTR-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTCT_OPRTR_ID");
        cwf_Master_Index_View_Actve_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");

        cwf_Master_Index_View__R_Field_7 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_7", "REDEFINE", cwf_Master_Index_View_Actve_Ind);
        cwf_Master_Index_View_Fill_1 = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Master_Index_View_Actve_Ind_2_2 = cwf_Master_Index_View__R_Field_7.newFieldInGroup("cwf_Master_Index_View_Actve_Ind_2_2", "ACTVE-IND-2-2", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Crprte_Status_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_View_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_View_Cnflct_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cnflct_Ind", "CNFLCT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNFLCT_IND");
        cwf_Master_Index_View_Cnflct_Ind.setDdmHeader("CONFLICT/REQUEST");
        cwf_Master_Index_View_Spcl_Hndlng_Txt = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", 
            FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        cwf_Master_Index_View_Spcl_Hndlng_Txt.setDdmHeader("MESSAGES");
        cwf_Master_Index_View_Instn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Instn_Cde", "INSTN-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "INSTN_CDE");
        cwf_Master_Index_View_Rqst_Instn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Instn_Cde", "RQST-INSTN-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "RQST_INSTN_CDE");
        cwf_Master_Index_View_Spcl_Policy_Srce_CdeMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_SPCL_POLICY_SRCE_CDEMuGroup", 
            "SPCL_POLICY_SRCE_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_SPCL_POLICY_SRCE_CDE");
        cwf_Master_Index_View_Spcl_Policy_Srce_Cde = cwf_Master_Index_View_Spcl_Policy_Srce_CdeMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Spcl_Policy_Srce_Cde", 
            "SPCL-POLICY-SRCE-CDE", FieldType.STRING, 5, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "SPCL_POLICY_SRCE_CDE");
        cwf_Master_Index_View_Attntn_Txt = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Attntn_Txt", "ATTNTN-TXT", FieldType.STRING, 
            25, RepeatingFieldStrategy.None, "ATTNTN_TXT");
        cwf_Master_Index_View_Attntn_Txt.setDdmHeader("ATTENTION");
        cwf_Master_Index_View_Check_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Check_Ind", "CHECK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CHECK_IND");
        cwf_Master_Index_View_Check_Ind.setDdmHeader("CHK");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme", 
            "RQST-INVRT-RCVD-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_INVRT_RCVD_DTE_TME");
        cwf_Master_Index_View_Effctve_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_View_Trnsctn_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trnsctn_Dte", "TRNSCTN-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRNSCTN_DTE");
        cwf_Master_Index_View_Trnsctn_Dte.setDdmHeader("TRANSACTION/DATE");
        cwf_Master_Index_View_Trans_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trans_Dte", "TRANS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        cwf_Master_Index_View_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_View_Physcl_Fldr_Id_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Index_View_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Index_View_Mj_Chrge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Chrge_Dte_Tme", "MJ-CHRGE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "MJ_CHRGE_DTE_TME");
        cwf_Master_Index_View_Mj_Chrge_Dte_Tme.setDdmHeader("CHARGEOUT/DATE-TIME");
        cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde", "MJ-CHRGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_CHRGE_OPRTR_CDE");
        cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde.setDdmHeader("CHARGEOUT/OPERATOR");
        cwf_Master_Index_View_Mstr_Indx_Actn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mstr_Indx_Actn_Cde", "MSTR-INDX-ACTN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "MSTR_INDX_ACTN_CDE");
        cwf_Master_Index_View_Mstr_Indx_Actn_Cde.setDdmHeader("CHARGEOUT/ACTION CODE");
        cwf_Master_Index_View_Mj_Pull_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_View_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_View_Final_Close_Out_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Final_Close_Out_Dte_Tme", 
            "FINAL-CLOSE-OUT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_DTE_TME");
        cwf_Master_Index_View_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        cwf_Master_Index_View_Final_Close_Out_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Final_Close_Out_Oprtr_Cde", 
            "FINAL-CLOSE-OUT-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_OPRTR_CDE");
        cwf_Master_Index_View_Mj_Emrgncy_Rqst_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Emrgncy_Rqst_Dte_Tme", 
            "MJ-EMRGNCY-RQST-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_DTE_TME");
        cwf_Master_Index_View_Mj_Emrgncy_Rqst_Dte_Tme.setDdmHeader("EMERGENCY REQ/DATE-TIME");
        cwf_Master_Index_View_Mj_Emrgncy_Rqst_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Emrgncy_Rqst_Oprtr_Cde", 
            "MJ-EMRGNCY-RQST-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_OPRTR_CDE");
        cwf_Master_Index_View_Mj_Emrgncy_Rqst_Oprtr_Cde.setDdmHeader("EMERGENCY/REQUESTOR");
        cwf_Master_Index_View_Print_Q_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Q_Ind", "PRINT-Q-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PRINT_Q_IND");
        cwf_Master_Index_View_Print_Q_Ind.setDdmHeader("MJ PRINT/QUEUE");
        cwf_Master_Index_View_Print_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Dte_Tme", "PRINT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "PRINT_DTE_TME");
        cwf_Master_Index_View_Print_Dte_Tme.setDdmHeader("MJ PRINT/DATE-TIME");
        cwf_Master_Index_View_Print_Batch_Id_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Batch_Id_Nbr", "PRINT-BATCH-ID-NBR", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "PRINT_BATCH_ID_NBR");

        cwf_Master_Index_View__R_Field_8 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_8", "REDEFINE", cwf_Master_Index_View_Print_Batch_Id_Nbr);
        cwf_Master_Index_View_Print_Prefix_Ind = cwf_Master_Index_View__R_Field_8.newFieldInGroup("cwf_Master_Index_View_Print_Prefix_Ind", "PRINT-PREFIX-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Print_Batch_Nbr = cwf_Master_Index_View__R_Field_8.newFieldInGroup("cwf_Master_Index_View_Print_Batch_Nbr", "PRINT-BATCH-NBR", 
            FieldType.NUMERIC, 8);
        cwf_Master_Index_View_Print_Batch_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Batch_Sqnce_Nbr", 
            "PRINT-BATCH-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRINT_BATCH_SQNCE_NBR");
        cwf_Master_Index_View_Print_Batch_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Print_Batch_Ind", "PRINT-BATCH-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRINT_BATCH_IND");
        cwf_Master_Index_View_Printer_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Printer_Id_Cde", "PRINTER-ID-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "PRINTER_ID_CDE");
        cwf_Master_Index_View_Rescan_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RESCAN_IND");
        cwf_Master_Index_View_Rescan_Ind.setDdmHeader("RE-SCAN/INDICATOR");
        cwf_Master_Index_View_Dup_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Dup_Ind", "DUP-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DUP_IND");
        cwf_Master_Index_View_Dup_Ind.setDdmHeader("DUP/IND");
        cwf_Master_Index_View_Cmplnt_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CMPLNT_IND");
        cwf_Master_Index_View_Cntrct_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_View_Cntrct_Nbr = cwf_Master_Index_View_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Cntrct_Nbr", "CNTRCT-NBR", 
            FieldType.STRING, 8, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_View_Mail_Item_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_MAIL_ITEM_NBRMuGroup", 
            "MAIL_ITEM_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_MAIL_ITEM_NBR");
        cwf_Master_Index_View_Mail_Item_Nbr = cwf_Master_Index_View_Mail_Item_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Mail_Item_Nbr", "MAIL-ITEM-NBR", 
            FieldType.STRING, 11, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "MAIL_ITEM_NBR");
        cwf_Master_Index_View_Mail_Item_Nbr.setDdmHeader("MIN");
        cwf_Master_Index_View_Rqst_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Id", "RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Master_Index_View_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Master_Index_View__R_Field_9 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_9", "REDEFINE", cwf_Master_Index_View_Rqst_Id);
        cwf_Master_Index_View_Rqst_Work_Prcss_Id = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Case_Id_Cde = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Rqst_Pin_Nbr = cwf_Master_Index_View__R_Field_9.newFieldInGroup("cwf_Master_Index_View_Rqst_Pin_Nbr", "RQST-PIN-NBR", FieldType.NUMERIC, 
            12);
        cwf_Master_Index_View_Rlte_Rqst_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rlte_Rqst_Id", "RLTE-RQST-ID", 
            FieldType.STRING, 28, RepeatingFieldStrategy.None, "RLTE_RQST_ID");

        cwf_Master_Index_View__R_Field_10 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_10", "REDEFINE", cwf_Master_Index_View_Rlte_Rqst_Id);
        cwf_Master_Index_View_Rlte_Work_Prcss_Id = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rlte_Work_Prcss_Id", "RLTE-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Master_Index_View_Rlte_Tiaa_Rcvd_Dte = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rlte_Tiaa_Rcvd_Dte", "RLTE-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rlte_Case_Id_Cde = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rlte_Case_Id_Cde", "RLTE-CASE-ID-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_View_Rlte_Pin_Nbr = cwf_Master_Index_View__R_Field_10.newFieldInGroup("cwf_Master_Index_View_Rlte_Pin_Nbr", "RLTE-PIN-NBR", FieldType.NUMERIC, 
            12);
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte", 
            "EXTRNL-PEND-RCV-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_View_Work_List_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_List_Ind", "WORK-LIST-IND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "WORK_LIST_IND");
        cwf_Master_Index_View_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");

        cwf_Master_Index_View__R_Field_11 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_11", "REDEFINE", cwf_Master_Index_View_Work_List_Ind);
        cwf_Master_Index_View_Work_List_Ind_1_1 = cwf_Master_Index_View__R_Field_11.newFieldInGroup("cwf_Master_Index_View_Work_List_Ind_1_1", "WORK-LIST-IND-1-1", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde", 
            "DUE-DTE-CHG-PRTY-CDE", FieldType.STRING, 16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        cwf_Master_Index_View__R_Field_12 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_12", "REDEFINE", cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Index_View_Due_Dte = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Due_Dte", "DUE-DTE", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Due_Dte_Chg_Ind = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Prty = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Prty", "DUE-DTE-PRTY", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Filler_1406_9818 = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Filler_1406_9818", "FILLER-1406-9818", 
            FieldType.STRING, 3);
        cwf_Master_Index_View_Sbsqnt_Cntct_Actn_Rqrd = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Sbsqnt_Cntct_Actn_Rqrd", 
            "SBSQNT-CNTCT-ACTN-RQRD", FieldType.STRING, 1);
        cwf_Master_Index_View_Sbsqnt_Cntct_Ind = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Sbsqnt_Cntct_Ind", "SBSQNT-CNTCT-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Prcssng_Type = cwf_Master_Index_View__R_Field_12.newFieldInGroup("cwf_Master_Index_View_Prcssng_Type", "PRCSSNG-TYPE", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Bsnss_Reply_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BSNSS_REPLY_IND");
        cwf_Master_Index_View_Bsnss_Reply_Ind.setDdmHeader("BUSINESS/REPLY");
        cwf_Master_Index_View_Status_Freeze_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_View_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Index_View_Elctrnc_Fldr_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_Index_View_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        cwf_Master_Index_View_Log_Insttn_Srce_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Log_Insttn_Srce_Cde", 
            "LOG-INSTTN-SRCE-CDE", FieldType.STRING, 5, RepeatingFieldStrategy.None, "LOG_INSTTN_SRCE_CDE");
        cwf_Master_Index_View_Log_Insttn_Srce_Cde.setDdmHeader("REQUESTOR/INSTITUTION");
        cwf_Master_Index_View_Log_Rqstr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Log_Rqstr_Cde", "LOG-RQSTR-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "LOG_RQSTR_CDE");
        cwf_Master_Index_View_Log_Rqstr_Cde.setDdmHeader("REQUESTOR/TYPE");
        cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme", 
            "UNIT-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Index_View_Unit_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Clock_End_Dte_Tme", 
            "UNIT-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme", 
            "EMPL-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        cwf_Master_Index_View_Empl_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Clock_End_Dte_Tme", 
            "EMPL-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme", 
            "INTRNL-PND-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_START_DTE_TME");
        cwf_Master_Index_View_Intrnl_Pnd_Start_Dte_Tme.setDdmHeader("ACKNOWLEDGEMENT DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme", 
            "INTRNL-PND-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_END_DTE_TME");
        cwf_Master_Index_View_Intrnl_Pnd_End_Dte_Tme.setDdmHeader("INTERNAL PEND END DATE TIME");
        cwf_Master_Index_View_Intrnl_Pnd_Days = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Days", "INTRNL-PND-DAYS", 
            FieldType.PACKED_DECIMAL, 5, RepeatingFieldStrategy.None, "INTRNL_PND_DAYS");
        cwf_Master_Index_View_Intrnl_Pnd_Days.setDdmHeader("INTERNAL PEND DAYS");
        cwf_Master_Index_View_Step_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Clock_Start_Dte_Tme", 
            "STEP-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        cwf_Master_Index_View_Step_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Clock_End_Dte_Tme", 
            "STEP-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme", 
            "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");

        cwf_Master_Index_View__R_Field_13 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_13", "REDEFINE", cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme);
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme_1_8 = cwf_Master_Index_View__R_Field_13.newFieldInGroup("cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme_1_8", 
            "CRPRTE-CLOCK-END-DTE-TME-1-8", FieldType.STRING, 8);
        cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme", 
            "UNIT-EN-RTE-TO-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_EN_RTE_TO_DTE_TME");
        cwf_Master_Index_View_Unit_En_Rte_To_Dte_Tme.setDdmHeader("UNIT EN ROUTE TO DATE TIME");
        cwf_Master_Index_View_Acknwldgmnt_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Acknwldgmnt_Cde", "ACKNWLDGMNT-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ACKNWLDGMNT_CDE");
        cwf_Master_Index_View_Acknwldgmnt_Cde.setDdmHeader("ACKNOWLEDGEMENT/CODE");
        cwf_Master_Index_View_Acknwldgmnt_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Acknwldgmnt_Oprtr_Cde", 
            "ACKNWLDGMNT-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ACKNWLDGMNT_OPRTR_CDE");
        cwf_Master_Index_View_Acknwldgmnt_Oprtr_Cde.setDdmHeader("ACKNOWLEDGED/BY");
        cwf_Master_Index_View_Acknwldgmnt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Acknwldgmnt_Dte_Tme", 
            "ACKNWLDGMNT-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "ACKNWLDGMNT_DTE_TME");
        cwf_Master_Index_View_Acknwldgmnt_Dte_Tme.setDdmHeader("ACKNOWLEDGED/ON");
        cwf_Master_Index_View_Cntct_Sheet_Print_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Sheet_Print_Cde", 
            "CNTCT-SHEET-PRINT-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_CDE");
        cwf_Master_Index_View_Cntct_Sheet_Print_Cde.setDdmHeader("CONTACT SHEET/PRINT CODE");
        cwf_Master_Index_View_Cntct_Sheet_Print_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Sheet_Print_Dte_Tme", 
            "CNTCT-SHEET-PRINT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_DTE_TME");
        cwf_Master_Index_View_Cntct_Sheet_Printer_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cntct_Sheet_Printer_Id_Cde", 
            "CNTCT-SHEET-PRINTER-ID-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINTER_ID_CDE");
        cwf_Master_Index_View_Shphrd_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Shphrd_Id", "SHPHRD-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "SHPHRD_ID");
        cwf_Master_Index_View_Shphrd_Id.setDdmHeader("SHEPHERD");
        cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme", 
            "CRRNT-DUE-DTE-CMT-PRTY-TME", FieldType.STRING, 17, RepeatingFieldStrategy.None, "CRRNT_DUE_DTE_CMT_PRTY_TME");
        cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme.setDdmHeader("CURR-DUE/TME-FIELD");

        cwf_Master_Index_View__R_Field_14 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_14", "REDEFINE", cwf_Master_Index_View_Crrnt_Due_Dte_Cmt_Prty_Tme);
        cwf_Master_Index_View_Crrnt_Due_Dte = cwf_Master_Index_View__R_Field_14.newFieldInGroup("cwf_Master_Index_View_Crrnt_Due_Dte", "CRRNT-DUE-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Crrnt_Cmt_Ind = cwf_Master_Index_View__R_Field_14.newFieldInGroup("cwf_Master_Index_View_Crrnt_Cmt_Ind", "CRRNT-CMT-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Crrnt_Prrty_Cde = cwf_Master_Index_View__R_Field_14.newFieldInGroup("cwf_Master_Index_View_Crrnt_Prrty_Cde", "CRRNT-PRRTY-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Crrnt_Due_Tme = cwf_Master_Index_View__R_Field_14.newFieldInGroup("cwf_Master_Index_View_Crrnt_Due_Tme", "CRRNT-DUE-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_View_Crprte_Due_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CRPRTE_DUE_DTE_TME");
        cwf_Master_Index_View_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Master_Index_View_Archvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Archvd_Dte", "ARCHVD-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ARCHVD_DTE");
        cwf_Master_Index_View_Archvd_Dte.setDdmHeader("ARCHIVED/DATE");
        cwf_Master_Index_View_Rstr_To_Crrnt_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rstr_To_Crrnt_Dte", "RSTR-TO-CRRNT-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RSTR_TO_CRRNT_DTE");
        cwf_Master_Index_View_Rstr_To_Crrnt_Dte.setDdmHeader("RESTORE/DATE");
        cwf_Master_Index_View_Off_Rtng_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Off_Rtng_Ind", "OFF-RTNG-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "OFF_RTNG_IND");
        cwf_Master_Index_View_Off_Rtng_Ind.setDdmHeader("OFF-ROUTING");
        cwf_Master_Index_View_Unit_On_Tme_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_On_Tme_Ind", "UNIT-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "UNIT_ON_TME_IND");
        cwf_Master_Index_View_Crprte_On_Tme_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_ON_TME_IND");
        cwf_Master_Index_View_Crprte_On_Tme_Ind.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Master_Index_View_Owner_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Owner_Unit_Cde", "OWNER-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "OWNER_UNIT_CDE");
        cwf_Master_Index_View_Owner_Unit_Cde.setDdmHeader("OWNERUNIT");
        cwf_Master_Index_View_Physcl_Fldr_Owner_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Physcl_Fldr_Owner_Id", 
            "PHYSCL-FLDR-OWNER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "PHYSCL_FLDR_OWNER_ID");
        cwf_Master_Index_View_Physcl_Fldr_Owner_Id.setDdmHeader("PHYSICAL/FOLDER-OWNER");
        cwf_Master_Index_View_Physcl_Fldr_Owner_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Physcl_Fldr_Owner_Unit_Cde", 
            "PHYSCL-FLDR-OWNER-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "PHYSCL_FLDR_OWNER_UNIT_CDE");
        cwf_Master_Index_View_Physcl_Fldr_Owner_Unit_Cde.setDdmHeader("PHYSICAL/FOLDER-UNIT");
        cwf_Master_Index_View_Step_Re_Do_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Re_Do_Ind", "STEP-RE-DO-IND", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "STEP_RE_DO_IND");
        cwf_Master_Index_View_Step_Re_Do_Ind.setDdmHeader("REDO/COUNT");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        cwf_Master_Index_View_Trade_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trade_Dte_Tme", "TRADE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TRADE_DTE_TME");
        cwf_Master_Index_View_Trade_Dte_Tme.setDdmHeader("TRADE/DTE-TME");
        cwf_Master_Index_View_Future_Pymnt_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Future_Pymnt_Ind", "FUTURE-PYMNT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "FUTURE_PYMNT_IND");
        cwf_Master_Index_View_Future_Pymnt_Ind.setDdmHeader("ACKNOWLEDGEMENT/PAYMENT-DTE");
        cwf_Master_Index_View_Status_Clock_Start_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Clock_Start_Dte_Tme", 
            "STATUS-CLOCK-START-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STATUS_CLOCK_START_DTE_TME");
        cwf_Master_Index_View_Status_Clock_Start_Dte_Tme.setDdmHeader("STATUS/START CLOCK");
        cwf_Master_Index_View_Status_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Clock_End_Dte_Tme", 
            "STATUS-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "STATUS_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Status_Clock_End_Dte_Tme.setDdmHeader("STATUS/CLOCK END");
        cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme", 
            "STATUS-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "STATUS_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme.setDdmHeader("STATUS ACTIVITY/TURNAROUND");

        cwf_Master_Index_View__R_Field_15 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_15", "REDEFINE", cwf_Master_Index_View_Status_Elpsd_Clndr_Days_Tme);
        cwf_Master_Index_View_Status_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_15.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Clndr_Days", 
            "STATUS-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Status_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_15.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Clndr_Hours", 
            "STATUS-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Status_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_15.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Clndr_Minutes", 
            "STATUS-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme", 
            "STATUS-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "STATUS_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme.setDdmHeader("STATUS/(BUSINESS DAYS)");

        cwf_Master_Index_View__R_Field_16 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_16", "REDEFINE", cwf_Master_Index_View_Status_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_16.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Bsnss_Days", 
            "STATUS-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_16.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Bsnss_Hours", 
            "STATUS-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Status_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_16.newFieldInGroup("cwf_Master_Index_View_Status_Elpsd_Bsnss_Minutes", 
            "STATUS-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme", 
            "STEP-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "STEP_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme.setDdmHeader("STEP WAIT TIME");

        cwf_Master_Index_View__R_Field_17 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_17", "REDEFINE", cwf_Master_Index_View_Step_Elpsd_Clndr_Days_Tme);
        cwf_Master_Index_View_Step_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_17.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Clndr_Days", 
            "STEP-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Step_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_17.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Clndr_Hours", 
            "STEP-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Step_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_17.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Clndr_Minutes", 
            "STEP-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme", 
            "STEP-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "STEP_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme.setDdmHeader("STEP PROCESS TIME");

        cwf_Master_Index_View__R_Field_18 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_18", "REDEFINE", cwf_Master_Index_View_Step_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_18.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Bsnss_Days", 
            "STEP-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_18.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Bsnss_Hours", 
            "STEP-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Step_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_18.newFieldInGroup("cwf_Master_Index_View_Step_Elpsd_Bsnss_Minutes", 
            "STEP-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme", 
            "EMPL-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "EMPL_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme.setDdmHeader("ASSOCIATE WAIT TIME");

        cwf_Master_Index_View__R_Field_19 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_19", "REDEFINE", cwf_Master_Index_View_Empl_Elpsd_Clndr_Days_Tme);
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_19.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Clndr_Days", 
            "EMPL-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_19.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Clndr_Hours", 
            "EMPL-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Empl_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_19.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Clndr_Minutes", 
            "EMPL-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme", 
            "EMPL-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "EMPL_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme.setDdmHeader("ASSOCIATE PROCESS TIME");

        cwf_Master_Index_View__R_Field_20 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_20", "REDEFINE", cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_20.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Bsnss_Days", 
            "EMPL-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_20.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Bsnss_Hours", 
            "EMPL-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Empl_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_20.newFieldInGroup("cwf_Master_Index_View_Empl_Elpsd_Bsnss_Minutes", 
            "EMPL-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme", 
            "UNIT-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "UNIT_ELPSD_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme.setDdmHeader("UNIT WAIT TIME");

        cwf_Master_Index_View__R_Field_21 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_21", "REDEFINE", cwf_Master_Index_View_Unit_Elpsd_Clndr_Days_Tme);
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_21.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Clndr_Days", 
            "UNIT-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_21.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Clndr_Hours", 
            "UNIT-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Unit_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_21.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Clndr_Minutes", 
            "UNIT-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme", 
            "UNIT-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "UNIT_ELPSD_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme.setDdmHeader("UNIT PROCESS TIME");

        cwf_Master_Index_View__R_Field_22 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_22", "REDEFINE", cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_22.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Bsnss_Days", 
            "UNIT-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_22.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Bsnss_Hours", 
            "UNIT-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Unit_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_22.newFieldInGroup("cwf_Master_Index_View_Unit_Elpsd_Bsnss_Minutes", 
            "UNIT-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme", 
            "INTRNL-PND-CLNDR-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "INTRNL_PND_CLNDR_DAYS_TME");
        cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme.setDdmHeader("INTERNAL-PEND/(CALENDAR DAYS)");

        cwf_Master_Index_View__R_Field_23 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_23", "REDEFINE", cwf_Master_Index_View_Intrnl_Pnd_Clndr_Days_Tme);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Days = cwf_Master_Index_View__R_Field_23.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Days", 
            "INTRNL-PND-ELPSD-CLNDR-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Hours = cwf_Master_Index_View__R_Field_23.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Hours", 
            "INTRNL-PND-ELPSD-CLNDR-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Minutes = cwf_Master_Index_View__R_Field_23.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Clndr_Minutes", 
            "INTRNL-PND-ELPSD-CLNDR-MINUTES", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme", 
            "INTRNL-PND-BSNSS-DAYS-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "INTRNL_PND_BSNSS_DAYS_TME");
        cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme.setDdmHeader("INTERNAL PEND/(BUSSINESS DAYS)");

        cwf_Master_Index_View__R_Field_24 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_24", "REDEFINE", cwf_Master_Index_View_Intrnl_Pnd_Bsnss_Days_Tme);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Days = cwf_Master_Index_View__R_Field_24.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Days", 
            "INTRNL-PND-ELPSD-BSNSS-DAYS", FieldType.NUMERIC, 3);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Hours = cwf_Master_Index_View__R_Field_24.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Hours", 
            "INTRNL-PND-ELPSD-BSNSS-HOURS", FieldType.NUMERIC, 2);
        cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Minutes = cwf_Master_Index_View__R_Field_24.newFieldInGroup("cwf_Master_Index_View_Intrnl_Pnd_Elpsd_Bsnss_Minutes", 
            "INTRNL-PND-ELPSD-BSNSS-MINUTES", FieldType.NUMERIC, 2);
        registerRecord(vw_cwf_Master_Index_View);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);
        pnd_Prev_Unit_Cde = localVariables.newFieldInRecord("pnd_Prev_Unit_Cde", "#PREV-UNIT-CDE", FieldType.STRING, 8);
        pnd_Wpid = localVariables.newFieldInRecord("pnd_Wpid", "#WPID", FieldType.STRING, 8);
        pnd_Unit = localVariables.newFieldInRecord("pnd_Unit", "#UNIT", FieldType.STRING, 8);
        pnd_Today = localVariables.newFieldInRecord("pnd_Today", "#TODAY", FieldType.NUMERIC, 8);

        pnd_Today__R_Field_25 = localVariables.newGroupInRecord("pnd_Today__R_Field_25", "REDEFINE", pnd_Today);
        pnd_Today_Pnd_Today_A = pnd_Today__R_Field_25.newFieldInGroup("pnd_Today_Pnd_Today_A", "#TODAY-A", FieldType.STRING, 8);
        pnd_Diff = localVariables.newFieldInRecord("pnd_Diff", "#DIFF", FieldType.NUMERIC, 15);
        pnd_Alpha_Date = localVariables.newFieldInRecord("pnd_Alpha_Date", "#ALPHA-DATE", FieldType.DATE);
        pnd_Mj_Date_Time = localVariables.newFieldInRecord("pnd_Mj_Date_Time", "#MJ-DATE-TIME", FieldType.STRING, 15);

        pnd_Mj_Date_Time__R_Field_26 = localVariables.newGroupInRecord("pnd_Mj_Date_Time__R_Field_26", "REDEFINE", pnd_Mj_Date_Time);
        pnd_Mj_Date_Time_Pnd_Mj_Date = pnd_Mj_Date_Time__R_Field_26.newFieldInGroup("pnd_Mj_Date_Time_Pnd_Mj_Date", "#MJ-DATE", FieldType.NUMERIC, 8);

        pnd_Mj_Date_Time__R_Field_27 = localVariables.newGroupInRecord("pnd_Mj_Date_Time__R_Field_27", "REDEFINE", pnd_Mj_Date_Time);
        pnd_Mj_Date_Time_Pnd_Mj_Date_Time_N = pnd_Mj_Date_Time__R_Field_27.newFieldInGroup("pnd_Mj_Date_Time_Pnd_Mj_Date_Time_N", "#MJ-DATE-TIME-N", FieldType.NUMERIC, 
            15);
        pnd_Last_Chng_Date_Time = localVariables.newFieldInRecord("pnd_Last_Chng_Date_Time", "#LAST-CHNG-DATE-TIME", FieldType.NUMERIC, 15);

        counters = localVariables.newGroupInRecord("counters", "COUNTERS");
        counters_Pnd_Tot_Work = counters.newFieldInGroup("counters_Pnd_Tot_Work", "#TOT-WORK", FieldType.PACKED_DECIMAL, 7);
        counters_Pnd_Tot_Unit = counters.newFieldInGroup("counters_Pnd_Tot_Unit", "#TOT-UNIT", FieldType.PACKED_DECIMAL, 7);
        counters_Pnd_Total = counters.newFieldInGroup("counters_Pnd_Total", "#TOTAL", FieldType.PACKED_DECIMAL, 7);
        counters_Pnd_Tot_Rush = counters.newFieldInGroup("counters_Pnd_Tot_Rush", "#TOT-RUSH", FieldType.PACKED_DECIMAL, 7);
        counters_Pnd_Tot_Not_Rush = counters.newFieldInGroup("counters_Pnd_Tot_Not_Rush", "#TOT-NOT-RUSH", FieldType.PACKED_DECIMAL, 7);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_28 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_28", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Last_Run_Date = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Tbl_Last_Run_Date", "TBL-LAST-RUN-DATE", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Tbl_Last_Run_Flag = cwf_Support_Tbl__R_Field_28.newFieldInGroup("cwf_Support_Tbl_Tbl_Last_Run_Flag", "TBL-LAST-RUN-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 53);

        pnd_Tbl_Key__R_Field_29 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_29", "REDEFINE", pnd_Tbl_Key);

        pnd_Tbl_Key_Data_Nme = pnd_Tbl_Key__R_Field_29.newGroupInGroup("pnd_Tbl_Key_Data_Nme", "DATA-NME");
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Tbl_Table_Nme = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Tbl_Key_Field = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);

        pnd_Tbl_Prime_Key = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY");
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Key_Tbl_Table_Nme = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_Tbl_Key_Field = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Msg = localVariables.newFieldInRecord("pnd_Msg", "#MSG", FieldType.STRING, 50);

        constants = localVariables.newGroupInRecord("constants", "CONSTANTS");
        constants_Pnd_Rush = constants.newFieldInGroup("constants_Pnd_Rush", "#RUSH", FieldType.STRING, 10);
        constants_Pnd_Not_Rush = constants.newFieldInGroup("constants_Pnd_Not_Rush", "#NOT-RUSH", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Work_Prcss_IdOld = internalLoopRecord.newFieldInRecord("Sort01_Work_Prcss_Id_OLD", "Work_Prcss_Id_OLD", FieldType.STRING, 6);
        sort01Unit_CdeOld = internalLoopRecord.newFieldInRecord("Sort01_Unit_Cde_OLD", "Unit_Cde_OLD", FieldType.STRING, 8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index_View.reset();
        vw_cwf_Support_Tbl.reset();
        internalLoopRecord.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("          Corporate Workflow Facilities");
        pnd_Header1_2.setInitialValue("   Daily Report of Rush Microjackets Requested");
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind.setInitialValue("A");
        pnd_Tbl_Prime_Key_Tbl_Table_Nme.setInitialValue("CWF-RPRT-RUN-TBL");
        pnd_Tbl_Prime_Key_Tbl_Key_Field.setInitialValue("D");
        constants_Pnd_Rush.setInitialValue("RUSH");
        constants_Pnd_Not_Rush.setInitialValue("NOT RUSH");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3015() throws Exception
    {
        super("Cwfb3015");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
                                                                                                                                                                          //Natural: PERFORM GET-TODAY
        sub_Get_Today();
        if (condition(Global.isEscape())) {return;}
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  PRIMARY FILE
        vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                        //Natural: READ CWF-MASTER-INDEX-VIEW BY LAST-CHNGE-DTE-KEY STARTING FROM #TODAY
        (
        "READ_PRIME",
        new Wc[] { new Wc("LAST_CHNGE_DTE_KEY", ">=", pnd_Today, WcType.BY) },
        new Oc[] { new Oc("LAST_CHNGE_DTE_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Master_Index_View.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT AFTER-PRIME-READ
            //* NEED TO ISOLATE 8 CHARACTER
            pnd_Mj_Date_Time.setValueEdited(cwf_Master_Index_View_Mj_Chrge_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.MJ-CHRGE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #MJ-DATE-TIME
            //*                                           /*DATE TO CHECK AGAINST TODAY
            if (condition(pnd_Mj_Date_Time_Pnd_Mj_Date.equals(pnd_Today)))                                                                                                //Natural: IF #MJ-DATE EQ #TODAY
            {
                pnd_Last_Chng_Date_Time.setValue(cwf_Master_Index_View_Last_Chnge_Dte_Tme);                                                                               //Natural: MOVE CWF-MASTER-INDEX-VIEW.LAST-CHNGE-DTE-TME TO #LAST-CHNG-DATE-TIME
                //*   CAN BE MULTIPLE "R" RECORDS FOR TODAY TRY TO GET MOST RECENT BY
                //*   COMPARING UPDATE DATE WITH MJ-REQUEST-UPDATE-DATE... IF THE DIFFEREN
                //*   CE IS RELATIVELY SMALL(LT 6 SECONDS) WE ASSUME IT IS THE MOST RECENT
                //*   RECORD UPDATED
                short decideConditionsMet393 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #LAST-CHNG-DATE-TIME GT #MJ-DATE-TIME-N
                if (condition(pnd_Last_Chng_Date_Time.greater(pnd_Mj_Date_Time_Pnd_Mj_Date_Time_N)))
                {
                    decideConditionsMet393++;
                    pnd_Diff.compute(new ComputeParameters(false, pnd_Diff), pnd_Last_Chng_Date_Time.subtract(pnd_Mj_Date_Time_Pnd_Mj_Date_Time_N));                      //Natural: ASSIGN #DIFF := #LAST-CHNG-DATE-TIME - #MJ-DATE-TIME-N
                }                                                                                                                                                         //Natural: WHEN #LAST-CHNG-DATE-TIME LT #MJ-DATE-TIME-N
                else if (condition(pnd_Last_Chng_Date_Time.less(pnd_Mj_Date_Time_Pnd_Mj_Date_Time_N)))
                {
                    decideConditionsMet393++;
                    pnd_Diff.compute(new ComputeParameters(false, pnd_Diff), pnd_Mj_Date_Time_Pnd_Mj_Date_Time_N.subtract(pnd_Last_Chng_Date_Time));                      //Natural: ASSIGN #DIFF := #MJ-DATE-TIME-N - #LAST-CHNG-DATE-TIME
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Diff.reset();                                                                                                                                     //Natural: RESET #DIFF
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  IF DIFFERENCE IS LARGE (GT 60) WE ASSUME IT
                if (condition(pnd_Diff.greater(60)))                                                                                                                      //Natural: IF #DIFF GT 60
                {
                    //*  WAS UPDATED AGAIN SO READ ANOTHER RECORD TO GET
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(!(cwf_Master_Index_View_Mj_Pull_Ind.equals("R") || cwf_Master_Index_View_Printer_Id_Cde.equals("TZFP"))))                               //Natural: ACCEPT IF CWF-MASTER-INDEX-VIEW.MJ-PULL-IND EQ 'R' OR CWF-MASTER-INDEX-VIEW.PRINTER-ID-CDE EQ 'TZFP'
                    {
                        continue;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(cwf_Master_Index_View_Unit_Cde, cwf_Master_Index_View_Work_Prcss_Id, cwf_Master_Index_View_Pin_Nbr, cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde,  //Natural: END-ALL
                cwf_Master_Index_View_Mj_Pull_Ind, cwf_Master_Index_View_Printer_Id_Cde);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(cwf_Master_Index_View_Unit_Cde, cwf_Master_Index_View_Work_Prcss_Id, cwf_Master_Index_View_Pin_Nbr);                                           //Natural: SORT BY CWF-MASTER-INDEX-VIEW.UNIT-CDE CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID CWF-MASTER-INDEX-VIEW.PIN-NBR USING CWF-MASTER-INDEX-VIEW.MJ-CHRGE-OPRTR-CDE CWF-MASTER-INDEX-VIEW.MJ-PULL-IND CWF-MASTER-INDEX-VIEW.PRINTER-ID-CDE
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Master_Index_View_Unit_Cde, cwf_Master_Index_View_Work_Prcss_Id, cwf_Master_Index_View_Pin_Nbr, 
            cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde, cwf_Master_Index_View_Mj_Pull_Ind, cwf_Master_Index_View_Printer_Id_Cde)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //* *SAG END-EXIT
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            counters_Pnd_Tot_Work.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOT-WORK
            counters_Pnd_Tot_Unit.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOT-UNIT
            counters_Pnd_Total.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TOTAL
            //*   FIRST RECORD; PRIME PREV-UNIT-CODE
            short decideConditionsMet425 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TOTAL EQ 1
            if (condition(counters_Pnd_Total.equals(1)))
            {
                decideConditionsMet425++;
                //*  NOT FIRST RECORD CHECK IF UNIT HAS CHANGED
                pnd_Prev_Unit_Cde.setValue(cwf_Master_Index_View_Unit_Cde);                                                                                               //Natural: MOVE CWF-MASTER-INDEX-VIEW.UNIT-CDE TO #PREV-UNIT-CDE
            }                                                                                                                                                             //Natural: WHEN #TOTAL GT 1
            else if (condition(counters_Pnd_Total.greater(1)))
            {
                decideConditionsMet425++;
                if (condition(cwf_Master_Index_View_Unit_Cde.notEquals(pnd_Prev_Unit_Cde)))                                                                               //Natural: IF CWF-MASTER-INDEX-VIEW.UNIT-CDE NE #PREV-UNIT-CDE
                {
                    //*  NEW UNIT - START NEW PAGE
                    pnd_Prev_Unit_Cde.setValue(cwf_Master_Index_View_Unit_Cde);                                                                                           //Natural: MOVE CWF-MASTER-INDEX-VIEW.UNIT-CDE TO #PREV-UNIT-CDE
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  (USE THIS INSTEAD OF NEWPAGE IN THE AT BREAK
                    //*  SO THE GRAND TOTAL LINE ISN't on a page by
                    //*  ITSELF AT THE END OF THE REPORT)
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(cwf_Master_Index_View_Mj_Pull_Ind.equals("R")))                                                                                                 //Natural: IF CWF-MASTER-INDEX-VIEW.MJ-PULL-IND EQ 'R'
            {
                pnd_Msg.setValue(constants_Pnd_Rush);                                                                                                                     //Natural: MOVE #RUSH TO #MSG
                counters_Pnd_Tot_Rush.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOT-RUSH
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Msg.setValue(constants_Pnd_Not_Rush);                                                                                                                 //Natural: MOVE #NOT-RUSH TO #MSG
                counters_Pnd_Tot_Not_Rush.nadd(1);                                                                                                                        //Natural: ADD 1 TO #TOT-NOT-RUSH
                //*  PIN-EXP <<
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "/WPID",                                                                                                                              //Natural: DISPLAY ( 1 ) '/WPID' CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID 10X '/PIN' CWF-MASTER-INDEX-VIEW.PIN-NBR ( EM = 999999999999 ) 10X 'RACF/ID' CWF-MASTER-INDEX-VIEW.MJ-CHRGE-OPRTR-CDE 10X 'MJ/IND' CWF-MASTER-INDEX-VIEW.MJ-PULL-IND 10X 'PRINTER/ID' CWF-MASTER-INDEX-VIEW.PRINTER-ID-CDE 10X '/COMMENTS' #MSG ( AL = 10 )
            		cwf_Master_Index_View_Work_Prcss_Id,new ColumnSpacing(10),"/PIN",
            		cwf_Master_Index_View_Pin_Nbr, new ReportEditMask ("999999999999"),new ColumnSpacing(10),"RACF/ID",
            		cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde,new ColumnSpacing(10),"MJ/IND",
            		cwf_Master_Index_View_Mj_Pull_Ind,new ColumnSpacing(10),"PRINTER/ID",
            		cwf_Master_Index_View_Printer_Id_Cde,new ColumnSpacing(10),"/COMMENTS",
            		pnd_Msg, new AlphanumericLength (10));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  10X  '/PIN'          CWF-MASTER-INDEX-VIEW.PIN-NBR
            //*                           (EM=9999999)
            //*                                                                                                                                                           //Natural: AT BREAK OF CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID
            //* *SAG END-EXIT                                                                                                                                             //Natural: AT BREAK OF CWF-MASTER-INDEX-VIEW.UNIT-CDE
            //* *SAG DEFINE EXIT SORT-FIELDS
            //*  THIS USER EXIT ALLOWS FIELDS OF SELECTED PRIME SEC TER FILES TO
            //*  BE SORTED
            //* *SAG END-EXIT
            sort01Work_Prcss_IdOld.setValue(cwf_Master_Index_View_Work_Prcss_Id);                                                                                         //Natural: END-SORT
            sort01Unit_CdeOld.setValue(cwf_Master_Index_View_Unit_Cde);
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        pnd_Alpha_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Today_Pnd_Today_A);                                                                              //Natural: MOVE EDITED #TODAY-A TO #ALPHA-DATE ( EM = YYYYMMDD )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total Rush Requests Printed on TZFP    :",counters_Pnd_Tot_Rush,NEWLINE,"Total Non Rush Requests Printed on TZFP:",counters_Pnd_Tot_Not_Rush,NEWLINE,NEWLINE,NEWLINE,"Total Micro Jacket Rush Requests for",pnd_Alpha_Date,  //Natural: WRITE ( 1 ) // 'Total Rush Requests Printed on TZFP    :' #TOT-RUSH / 'Total Non Rush Requests Printed on TZFP:' #TOT-NOT-RUSH /// 'Total Micro Jacket Rush Requests for' #ALPHA-DATE ( EM = N ( 9 ) ' 'L ( 9 ) ' 'DD,' 'YYYY ) ':' #TOTAL ( EM = Z,ZZZ,ZZZ ) // 56T '**** End of Report ****'
            new ReportEditMask ("NNNNNNNNN' 'L(9)' 'DD,' 'YYYY"),":",counters_Pnd_Total, new ReportEditMask ("Z,ZZZ,ZZZ"),NEWLINE,NEWLINE,new TabSetting(56),
            "**** End of Report ****");
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TODAY
        //* *************
        //* *SAG END-EXIT
    }
    private void sub_Get_Today() throws Exception                                                                                                                         //Natural: GET-TODAY
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        pnd_Tbl_Key_Data_Nme.setValuesByName(pnd_Tbl_Prime_Key);                                                                                                          //Natural: MOVE BY NAME #TBL-PRIME-KEY TO #TBL-KEY.DATA-NME
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) CWF-SUPPORT-TBL BY TBL-PRIME-KEY FROM #TBL-KEY
        (
        "READ",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ")))
        {
            pnd_Today_Pnd_Today_A.setValue(cwf_Support_Tbl_Tbl_Last_Run_Date);                                                                                            //Natural: MOVE TBL-LAST-RUN-DATE TO #TODAY-A
            //*  READ.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(! (DbsUtil.maskMatches(pnd_Today,"YYYYMMDD"))))                                                                                                     //Natural: IF #TODAY NE MASK ( YYYYMMDD )
        {
            pnd_Today.setValue(Global.getDATN());                                                                                                                         //Natural: MOVE *DATN TO #TODAY
            pnd_Msg.setValue("INVALID RUN DATE ON TABLE; USING SYSTEM DATE");                                                                                             //Natural: MOVE 'INVALID RUN DATE ON TABLE; USING SYSTEM DATE' TO #MSG
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  GET-TODAY
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP ) // 'UNIT :' CWF-MASTER-INDEX-VIEW.UNIT-CDE / '-' ( 20 )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header1_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,NEWLINE,"UNIT :",cwf_Master_Index_View_Unit_Cde,NEWLINE,"-",new 
                        RepeatItem(20));
                    //* *SAG DEFINE EXIT REPORT1-AT-TOP-OF-PAGE
                    //* *SAG END-EXIT
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Master_Index_View_Work_Prcss_IdIsBreak = cwf_Master_Index_View_Work_Prcss_Id.isBreak(endOfData);
        boolean cwf_Master_Index_View_Unit_CdeIsBreak = cwf_Master_Index_View_Unit_Cde.isBreak(endOfData);
        if (condition(cwf_Master_Index_View_Work_Prcss_IdIsBreak || cwf_Master_Index_View_Unit_CdeIsBreak))
        {
            pnd_Wpid.setValue(sort01Work_Prcss_IdOld);                                                                                                                    //Natural: ASSIGN #WPID := OLD ( CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total Rush Requests for Work Process ID",pnd_Wpid,":",counters_Pnd_Tot_Work, new ReportEditMask           //Natural: WRITE ( 1 ) / 'Total Rush Requests for Work Process ID' #WPID ':' #TOT-WORK ( EM = Z,ZZZ,ZZZ ) '_' ( 132 ) /
                ("Z,ZZZ,ZZZ"),"_",new RepeatItem(132),NEWLINE);
            if (condition(Global.isEscape())) return;
            counters_Pnd_Tot_Work.reset();                                                                                                                                //Natural: RESET #TOT-WORK
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(cwf_Master_Index_View_Unit_CdeIsBreak))
        {
            pnd_Unit.setValue(sort01Unit_CdeOld);                                                                                                                         //Natural: ASSIGN #UNIT := OLD ( CWF-MASTER-INDEX-VIEW.UNIT-CDE )
            getReports().write(1, ReportOption.NOTITLE,"Total Rush Requests for Unit           ",pnd_Unit,":",counters_Pnd_Tot_Unit, new ReportEditMask                   //Natural: WRITE ( 1 ) 'Total Rush Requests for Unit           ' #UNIT ':' #TOT-UNIT ( EM = Z,ZZZ,ZZZ )
                ("Z,ZZZ,ZZZ"));
            if (condition(Global.isEscape())) return;
            counters_Pnd_Tot_Unit.reset();                                                                                                                                //Natural: RESET #TOT-UNIT
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "/WPID",
        		cwf_Master_Index_View_Work_Prcss_Id,new ColumnSpacing(10),"/PIN",
        		cwf_Master_Index_View_Pin_Nbr, new ReportEditMask ("999999999999"),new ColumnSpacing(10),"RACF/ID",
        		cwf_Master_Index_View_Mj_Chrge_Oprtr_Cde,new ColumnSpacing(10),"MJ/IND",
        		cwf_Master_Index_View_Mj_Pull_Ind,new ColumnSpacing(10),"PRINTER/ID",
        		cwf_Master_Index_View_Printer_Id_Cde,new ColumnSpacing(10),"/COMMENTS",
        		pnd_Msg, new AlphanumericLength (10));
    }
}
