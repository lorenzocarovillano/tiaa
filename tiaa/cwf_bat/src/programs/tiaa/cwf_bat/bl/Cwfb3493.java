/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:57 PM
**        * FROM NATURAL PROGRAM : Cwfb3493
************************************************************
**        * FILE NAME            : Cwfb3493.java
**        * CLASS NAME           : Cwfb3493
**        * INSTANCE NAME        : Cwfb3493
************************************************************
************************************************************************
* PROGRAM  : CWFB3493
* SYSTEM   : CWF
* TITLE    :
* GENERATED: OCT 05,2000
* FUNCTION : EXTRACTS ACTIVE WPID's and their descriptions
*          :   FOR LOADING TO ORACLE (FOR SMART)
*          :
*          :
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------   -------- --------------------------------------------------
*
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3493 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Wp_Work_Prcss_Id;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Id;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme;
    private DbsField cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme;
    private DbsField cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde;
    private DbsField cwf_Wp_Work_Prcss_Id_Actve_Ind;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Group_Name;
    private DbsField pnd_Work_File_Work_Prcss_Id;
    private DbsField pnd_Work_File_Work_Prcss_Long_Nme;
    private DbsField pnd_Work_File_Work_Prcss_Short_Nme;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Wp_Work_Prcss_Id = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Work_Prcss_Id", "CWF-WP-WORK-PRCSS-ID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Id = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", 
            FieldType.STRING, 45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme.setDdmHeader("DELETE/DT-TM");
        cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        cwf_Wp_Work_Prcss_Id_Actve_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");
        registerRecord(vw_cwf_Wp_Work_Prcss_Id);

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Group_Name = pnd_Work_File.newFieldInGroup("pnd_Work_File_Group_Name", "GROUP-NAME", FieldType.STRING, 30);
        pnd_Work_File_Work_Prcss_Id = pnd_Work_File.newFieldInGroup("pnd_Work_File_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Work_File_Work_Prcss_Long_Nme = pnd_Work_File.newFieldInGroup("pnd_Work_File_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 
            45);
        pnd_Work_File_Work_Prcss_Short_Nme = pnd_Work_File.newFieldInGroup("pnd_Work_File_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Wp_Work_Prcss_Id.reset();

        localVariables.reset();
        pnd_Work_File_Group_Name.setInitialValue("WPID-TBL");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3493() throws Exception
    {
        super("Cwfb3493");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        vw_cwf_Wp_Work_Prcss_Id.startDatabaseRead                                                                                                                         //Natural: READ CWF-WP-WORK-PRCSS-ID BY WPID-UNIQ-KEY
        (
        "READ01",
        new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Wp_Work_Prcss_Id.readNextRow("READ01")))
        {
            if (condition(cwf_Wp_Work_Prcss_Id_Dlte_Dte_Tme.greater(getZero()) || cwf_Wp_Work_Prcss_Id_Dlte_Oprtr_Cde.greater(" ")))                                      //Natural: REJECT IF DLTE-DTE-TME GT 0 OR DLTE-OPRTR-CDE GT ' '
            {
                continue;
            }
            pnd_Work_File.setValuesByName(vw_cwf_Wp_Work_Prcss_Id);                                                                                                       //Natural: MOVE BY NAME CWF-WP-WORK-PRCSS-ID TO #WORK-FILE
            getWorkFiles().write(1, false, pnd_Work_File);                                                                                                                //Natural: WRITE WORK FILE 1 #WORK-FILE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
