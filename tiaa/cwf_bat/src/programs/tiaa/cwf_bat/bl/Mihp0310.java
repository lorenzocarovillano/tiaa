/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:58:52 PM
**        * FROM NATURAL PROGRAM : Mihp0310
************************************************************
**        * FILE NAME            : Mihp0310.java
**        * CLASS NAME           : Mihp0310
**        * INSTANCE NAME        : Mihp0310
************************************************************
************************************************************************
* PROGRAM  : MIHP0310
* SYSTEM   : CRPCWF
* TITLE    : ARCHIVING ACTIVATION PROGRAM
* WRITTEN  : FEB 08,96
* AUTHOR   : LEON EPSTEIN
* FUNCTION : THIS PROGRAM IS USED TO START PROGRAM MIHP0300 WHICH
*            PERFORMS ARCHIVING PROCESS.
*            PROGRAM WILL START PROCESS ONLY IF TIME-GAP WILL PERMIT.
*
* HISTORY
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mihp0310 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaMihl0100 ldaMihl0100;
    private LdaMihl0170 ldaMihl0170;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Record_1;
    private DbsField pnd_Work_Record_1_Pnd_Work_Message;
    private DbsField pnd_Work_Record_1_Pnd_Work_Filler;
    private DbsField pnd_Work_Record_1_Pnd_Work_Dash1;
    private DbsField pnd_Work_Record_1_Pnd_Work_Errn;
    private DbsField pnd_Work_Record_1_Pnd_Work_Dash2;
    private DbsField pnd_Work_Record_1_Pnd_Work_Text;
    private DbsField pnd_Contr_T;
    private DbsField pnd_Today_T;
    private DbsField pnd_Calc_Time;
    private DbsField pnd_Contr_Dt_Tm;

    private DbsGroup pnd_Contr_Dt_Tm__R_Field_1;
    private DbsField pnd_Contr_Dt_Tm_Pnd_Contr_Dt;

    private DbsGroup pnd_Contr_Dt_Tm__R_Field_2;
    private DbsField pnd_Contr_Dt_Tm_Pnd_Contr_Dt_A;
    private DbsField pnd_Contr_Dt_Tm_Pnd_Contr_Tm;
    private DbsField pnd_Today_Dt_Tm;

    private DbsGroup pnd_Today_Dt_Tm__R_Field_3;
    private DbsField pnd_Today_Dt_Tm_Pnd_Today_Dt;
    private DbsField pnd_Today_Dt_Tm_Pnd_Today_Tm;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_4;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2;
    private DbsField pnd_Msg_Split;

    private DbsGroup pnd_Msg_Split__R_Field_5;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Nat;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Fil1;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Err;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Fil2;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Errn;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Fil3;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_In;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Fil4;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Pgm;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Fil5;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Lin;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Fil6;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Linn;
    private DbsField pnd_Msg_Split_Pnd_Msg_Split_Fil7;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaMihl0100 = new LdaMihl0100();
        registerRecord(ldaMihl0100);
        registerRecord(ldaMihl0100.getVw_cwf_Who_Support_Tbl_Last_Run_Id());
        ldaMihl0170 = new LdaMihl0170();
        registerRecord(ldaMihl0170);
        registerRecord(ldaMihl0170.getVw_cwf_Who_Support_Tbl());

        // Local Variables

        pnd_Work_Record_1 = localVariables.newGroupInRecord("pnd_Work_Record_1", "#WORK-RECORD-1");
        pnd_Work_Record_1_Pnd_Work_Message = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Work_Message", "#WORK-MESSAGE", FieldType.STRING, 
            10);
        pnd_Work_Record_1_Pnd_Work_Filler = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Work_Filler", "#WORK-FILLER", FieldType.STRING, 4);
        pnd_Work_Record_1_Pnd_Work_Dash1 = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Work_Dash1", "#WORK-DASH1", FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_Work_Errn = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Work_Errn", "#WORK-ERRN", FieldType.STRING, 4);
        pnd_Work_Record_1_Pnd_Work_Dash2 = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Work_Dash2", "#WORK-DASH2", FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_Work_Text = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Work_Text", "#WORK-TEXT", FieldType.STRING, 74);
        pnd_Contr_T = localVariables.newFieldInRecord("pnd_Contr_T", "#CONTR-T", FieldType.TIME);
        pnd_Today_T = localVariables.newFieldInRecord("pnd_Today_T", "#TODAY-T", FieldType.TIME);
        pnd_Calc_Time = localVariables.newFieldInRecord("pnd_Calc_Time", "#CALC-TIME", FieldType.NUMERIC, 12);
        pnd_Contr_Dt_Tm = localVariables.newFieldInRecord("pnd_Contr_Dt_Tm", "#CONTR-DT-TM", FieldType.STRING, 15);

        pnd_Contr_Dt_Tm__R_Field_1 = localVariables.newGroupInRecord("pnd_Contr_Dt_Tm__R_Field_1", "REDEFINE", pnd_Contr_Dt_Tm);
        pnd_Contr_Dt_Tm_Pnd_Contr_Dt = pnd_Contr_Dt_Tm__R_Field_1.newFieldInGroup("pnd_Contr_Dt_Tm_Pnd_Contr_Dt", "#CONTR-DT", FieldType.NUMERIC, 8);

        pnd_Contr_Dt_Tm__R_Field_2 = pnd_Contr_Dt_Tm__R_Field_1.newGroupInGroup("pnd_Contr_Dt_Tm__R_Field_2", "REDEFINE", pnd_Contr_Dt_Tm_Pnd_Contr_Dt);
        pnd_Contr_Dt_Tm_Pnd_Contr_Dt_A = pnd_Contr_Dt_Tm__R_Field_2.newFieldInGroup("pnd_Contr_Dt_Tm_Pnd_Contr_Dt_A", "#CONTR-DT-A", FieldType.STRING, 
            8);
        pnd_Contr_Dt_Tm_Pnd_Contr_Tm = pnd_Contr_Dt_Tm__R_Field_1.newFieldInGroup("pnd_Contr_Dt_Tm_Pnd_Contr_Tm", "#CONTR-TM", FieldType.NUMERIC, 7);
        pnd_Today_Dt_Tm = localVariables.newFieldInRecord("pnd_Today_Dt_Tm", "#TODAY-DT-TM", FieldType.STRING, 15);

        pnd_Today_Dt_Tm__R_Field_3 = localVariables.newGroupInRecord("pnd_Today_Dt_Tm__R_Field_3", "REDEFINE", pnd_Today_Dt_Tm);
        pnd_Today_Dt_Tm_Pnd_Today_Dt = pnd_Today_Dt_Tm__R_Field_3.newFieldInGroup("pnd_Today_Dt_Tm_Pnd_Today_Dt", "#TODAY-DT", FieldType.NUMERIC, 8);
        pnd_Today_Dt_Tm_Pnd_Today_Tm = pnd_Today_Dt_Tm__R_Field_3.newFieldInGroup("pnd_Today_Dt_Tm_Pnd_Today_Tm", "#TODAY-TM", FieldType.NUMERIC, 7);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_4", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2 = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2", "#TBL-ACTVE-IND-2-2", 
            FieldType.STRING, 1);
        pnd_Msg_Split = localVariables.newFieldInRecord("pnd_Msg_Split", "#MSG-SPLIT", FieldType.STRING, 45);

        pnd_Msg_Split__R_Field_5 = localVariables.newGroupInRecord("pnd_Msg_Split__R_Field_5", "REDEFINE", pnd_Msg_Split);
        pnd_Msg_Split_Pnd_Msg_Split_Nat = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Nat", "#MSG-SPLIT-NAT", FieldType.STRING, 
            7);
        pnd_Msg_Split_Pnd_Msg_Split_Fil1 = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Fil1", "#MSG-SPLIT-FIL1", FieldType.STRING, 
            1);
        pnd_Msg_Split_Pnd_Msg_Split_Err = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Err", "#MSG-SPLIT-ERR", FieldType.STRING, 
            5);
        pnd_Msg_Split_Pnd_Msg_Split_Fil2 = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Fil2", "#MSG-SPLIT-FIL2", FieldType.STRING, 
            1);
        pnd_Msg_Split_Pnd_Msg_Split_Errn = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Errn", "#MSG-SPLIT-ERRN", FieldType.NUMERIC, 
            4);
        pnd_Msg_Split_Pnd_Msg_Split_Fil3 = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Fil3", "#MSG-SPLIT-FIL3", FieldType.STRING, 
            1);
        pnd_Msg_Split_Pnd_Msg_Split_In = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_In", "#MSG-SPLIT-IN", FieldType.STRING, 
            2);
        pnd_Msg_Split_Pnd_Msg_Split_Fil4 = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Fil4", "#MSG-SPLIT-FIL4", FieldType.STRING, 
            1);
        pnd_Msg_Split_Pnd_Msg_Split_Pgm = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Pgm", "#MSG-SPLIT-PGM", FieldType.STRING, 
            8);
        pnd_Msg_Split_Pnd_Msg_Split_Fil5 = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Fil5", "#MSG-SPLIT-FIL5", FieldType.STRING, 
            1);
        pnd_Msg_Split_Pnd_Msg_Split_Lin = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Lin", "#MSG-SPLIT-LIN", FieldType.STRING, 
            8);
        pnd_Msg_Split_Pnd_Msg_Split_Fil6 = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Fil6", "#MSG-SPLIT-FIL6", FieldType.STRING, 
            1);
        pnd_Msg_Split_Pnd_Msg_Split_Linn = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Linn", "#MSG-SPLIT-LINN", FieldType.NUMERIC, 
            4);
        pnd_Msg_Split_Pnd_Msg_Split_Fil7 = pnd_Msg_Split__R_Field_5.newFieldInGroup("pnd_Msg_Split_Pnd_Msg_Split_Fil7", "#MSG-SPLIT-FIL7", FieldType.STRING, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaMihl0100.initializeValues();
        ldaMihl0170.initializeValues();

        localVariables.reset();
        pnd_Work_Record_1_Pnd_Work_Message.setInitialValue("MESSAGE   ");
        pnd_Work_Record_1_Pnd_Work_Dash1.setInitialValue("-");
        pnd_Work_Record_1_Pnd_Work_Dash2.setInitialValue("-");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Mihp0310() throws Exception
    {
        super("Mihp0310");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("MIHP0310", onError);
        //*  WRITE /'MIHP0310.....Started...'
        pnd_Tbl_Prime_Key.reset();                                                                                                                                        //Natural: RESET #TBL-PRIME-KEY
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: MOVE 'A' TO #TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("MIT ARCHIVE LAST RUN");                                                                                             //Natural: MOVE 'MIT ARCHIVE LAST RUN' TO #TBL-TABLE-NME
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("A");                                                                                                                //Natural: MOVE 'A' TO #TBL-KEY-FIELD
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2.setValue("A");                                                                                                            //Natural: MOVE 'A' TO #TBL-ACTVE-IND-2-2
        //*  GET THE PRIMARY OBJECT RECORD.
        ldaMihl0100.getVw_cwf_Who_Support_Tbl_Last_Run_Id().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) CWF-WHO-SUPPORT-TBL-LAST-RUN-ID WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "FIND01",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaMihl0100.getVw_cwf_Who_Support_Tbl_Last_Run_Id().readNextRow("FIND01", true)))
        {
            ldaMihl0100.getVw_cwf_Who_Support_Tbl_Last_Run_Id().setIfNotFoundControlFlag(false);
            if (condition(ldaMihl0100.getVw_cwf_Who_Support_Tbl_Last_Run_Id().getAstCOUNTER().equals(0)))                                                                 //Natural: IF NO RECORD FOUND
            {
                pnd_Work_Record_1_Pnd_Work_Text.setValue("Control record was not found Call Systems Group CWF");                                                          //Natural: MOVE 'Control record was not found Call Systems Group CWF' TO #WORK-TEXT
                getWorkFiles().write(1, false, pnd_Work_Record_1);                                                                                                        //Natural: WRITE WORK FILE 1 #WORK-RECORD-1
                DbsUtil.terminate(24);  if (true) return;                                                                                                                 //Natural: TERMINATE 24
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(ldaMihl0100.getCwf_Who_Support_Tbl_Last_Run_Id_Run_Flag().equals("Y")))                                                                             //Natural: IF RUN-FLAG = 'Y'
        {
            getReports().write(0, "MIHP0310-Archiving process is Stoped due to Maintenance");                                                                             //Natural: WRITE 'MIHP0310-Archiving process is Stoped due to Maintenance'
            if (Global.isEscape()) return;
            //*  MOVE 'Archiving process is Stoped due to Maintenance, contact CWF'
            //*              TO #WORK-TEXT
            //*        WRITE WORK FILE 1 #WORK-RECORD-1
            DbsUtil.terminate(10);  if (true) return;                                                                                                                     //Natural: TERMINATE 10
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Contr_Dt_Tm_Pnd_Contr_Dt_A.setValue(ldaMihl0100.getCwf_Who_Support_Tbl_Last_Run_Id_Run_Date());                                                               //Natural: MOVE RUN-DATE TO #CONTR-DT-A
        //*     MOVE RUN-TIME      TO #CONTR-TM
        //*     MOVE EDITED #CONTR-DT-TM  TO #CONTR-T (EM=YYYYMMDDHHIISST)
        //*     MOVE *DATN         TO #TODAY-DT
        //*     MOVE *TIMN         TO #TODAY-TM
        //*     MOVE EDITED #TODAY-DT-TM  TO #TODAY-T (EM=YYYYMMDDHHIISST)
        //*     COMPUTE #CALC-TIME =(#TODAY-T - #CONTR-T)
        //*    WRITE /'CALCULATED TIME = ' #CALC-TIME
        //*          /'GAP        TIME = ' GAP-TIME
        //*    TERMINATE 44
        //*     IF #CALC-TIME GT GAP-TIME
        DbsUtil.invokeMain(DbsUtil.getBlType("MIHP0300"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MIHP0300'
        if (condition(Global.isEscape())) return;
        //*     ELSE
        //*  ITE 'MIHP0310-CALL TO MIHP0300 HAS BEEN CANCELED BECAUSE OF GAP TIME'
        //*  MOVE 'Call to PGM=MIHP0300 has been canceled because of Waiting Time'
        //*              TO #WORK-TEXT
        //*        WRITE WORK FILE 1 #WORK-RECORD-1
        //*       TERMINATE 11
        //*     END-IF
        if (condition(gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg().greater(" ")))                                                                                                 //Natural: IF MSG-INFO.##MSG GT ' '
        {
            //*     MOVE MSG-INFO.##MSG TO #MSG-SPLIT
            //*   IF #MSG-SPLIT-ERRN = 3113 OR       /* SPECIFIED ISN IS INVALID
            //*      #MSG-SPLIT-ERRN = 3145 OR       /* RECORD IS ON HOLD STATUS
            //*      #MSG-SPLIT-ERRN = 3199 OR       /* INDEX INCONSISTENCY
            //*      #MSG-SPLIT-ERRN = 3009          /* TIMED OUT(BACKOUT TRANSACTION)
            //*      PERFORM DELETE-CONTROL-REC
            //*      MOVE  #MSG-SPLIT-ERRN TO #WORK-ERRN
            //*  RITE 'One of the following Error was detected..3131, 3145, 3199, 3009'
            //*  MOVE 'ONE OF THE FOLLOWING ERROR WAS DETECTED..3131, 3145, 3199, 3009'
            //*             TO #WORK-TEXT
            //*       WRITE WORK FILE 1 #WORK-RECORD-1
            getReports().write(0, gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg());                                                                                                  //Natural: WRITE MSG-INFO.##MSG
            if (Global.isEscape()) return;
            DbsUtil.terminate(12);  if (true) return;                                                                                                                     //Natural: TERMINATE 12
        }                                                                                                                                                                 //Natural: END-IF
        //*   IF #MSG-SPLIT-ERRN = 3148
        //*     MOVE  #MSG-SPLIT-ERRN TO #WORK-ERRN
        //*  ITE      'Database 145 or Database 050 is not UP, take actions'
        //*     MOVE  'Database 145 or Database 050 is not UP, take actions'
        //*             TO #WORK-TEXT
        //*       WRITE WORK FILE 1 #WORK-RECORD-1
        //*     TERMINATE 24
        //*   ELSE
        //*     MOVE  #MSG-SPLIT-ERRN TO #WORK-ERRN
        //*  ITE    'There is a serious problem with Archiving, Call CWF Systems'
        //*   MOVE  'There is a serious problem with Archiving, Call CWF Systems'
        //*             TO #WORK-TEXT
        //*       WRITE WORK FILE 1 #WORK-RECORD-1
        //*     TERMINATE 24
        //*   END-IF
        //*  ELSE
        ldaMihl0100.getVw_cwf_Who_Support_Tbl_Last_Run_Id().startDatabaseRead                                                                                             //Natural: READ ( 1 ) CWF-WHO-SUPPORT-TBL-LAST-RUN-ID WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(ldaMihl0100.getVw_cwf_Who_Support_Tbl_Last_Run_Id().readNextRow("READ01")))
        {
            //* YYYYMMDD
            ldaMihl0100.getCwf_Who_Support_Tbl_Last_Run_Id_Run_Date().setValue(Global.getDATN());                                                                         //Natural: MOVE *DATN TO RUN-DATE
            //* HHMMSST
            ldaMihl0100.getCwf_Who_Support_Tbl_Last_Run_Id_Run_Time().setValue(Global.getTIMN());                                                                         //Natural: MOVE *TIMN TO RUN-TIME
            ldaMihl0100.getVw_cwf_Who_Support_Tbl_Last_Run_Id().updateDBRow("READ01");                                                                                    //Natural: UPDATE RECORD
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //*       TERMINATE 20
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  END-IF
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-CONTROL-REC
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Delete_Control_Rec() throws Exception                                                                                                                //Natural: DELETE-CONTROL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        ldaMihl0100.getVw_cwf_Who_Support_Tbl_Last_Run_Id().startDatabaseRead                                                                                             //Natural: READ ( 1 ) CWF-WHO-SUPPORT-TBL-LAST-RUN-ID WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "READ02",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(ldaMihl0100.getVw_cwf_Who_Support_Tbl_Last_Run_Id().readNextRow("READ02")))
        {
            ldaMihl0100.getCwf_Who_Support_Tbl_Last_Run_Id_Last_Run_Id().compute(new ComputeParameters(false, ldaMihl0100.getCwf_Who_Support_Tbl_Last_Run_Id_Last_Run_Id()),  //Natural: COMPUTE LAST-RUN-ID = ( LAST-RUN-ID - 1 )
                (ldaMihl0100.getCwf_Who_Support_Tbl_Last_Run_Id_Last_Run_Id().subtract(1)));
            ldaMihl0100.getVw_cwf_Who_Support_Tbl_Last_Run_Id().updateDBRow("READ02");                                                                                    //Natural: UPDATE RECORD
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("MIT ARCHIVE");                                                                                                      //Natural: MOVE 'MIT ARCHIVE' TO #TBL-TABLE-NME
        ldaMihl0170.getVw_cwf_Who_Support_Tbl().startDatabaseRead                                                                                                         //Natural: READ ( 1 ) CWF-WHO-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ03",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(ldaMihl0170.getVw_cwf_Who_Support_Tbl().readNextRow("READ03")))
        {
            ldaMihl0170.getVw_cwf_Who_Support_Tbl().deleteDBRow("READ03");                                                                                                //Natural: DELETE RECORD
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,"NATURAL ERROR",Global.getERROR_NR(),"IN",Global.getPROGRAM(),"....LINE",Global.getERROR_LINE());                                   //Natural: WRITE / 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE
        DbsUtil.terminate(13);  if (true) return;                                                                                                                         //Natural: TERMINATE 13
    };                                                                                                                                                                    //Natural: END-ERROR
}
