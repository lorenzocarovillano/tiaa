/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:26:15 PM
**        * FROM NATURAL PROGRAM : Cwfb3002
************************************************************
**        * FILE NAME            : Cwfb3002.java
**        * CLASS NAME           : Cwfb3002
**        * INSTANCE NAME        : Cwfb3002
************************************************************
*****************************************************************
* PROGRAM  : CWFB3002                                           *
* AUTHOR   : LEON EPSTEIN                                       *
* DATE     : 03/11/98                                           *
* TITLE    : WORK PROCESS ID TABLE CORRECTION                   *
* PROCESS  : POPULATES OWNER-UNIT-CDE WITH 1-ST OCCURENCE       *
*          : OF UNIT FROM ROUTING-SEQUENCE TABLE                *
*****************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3002 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Wp_Work_Prcss_Id;
    private DbsField cwf_Wp_Work_Prcss_Id_Work_Prcss_Id;
    private DbsField cwf_Wp_Work_Prcss_Id_Actve_Ind;
    private DbsField cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde;

    private DataAccessProgramView vw_cwf_Wp_Routing;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Prcss_Id;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Nbr;
    private DbsField cwf_Wp_Routing_Unit_Cde;
    private DbsField pnd_Read_Cntr;
    private DbsField pnd_Recs_Updt;
    private DbsField pnd_Rout_Nfnd;
    private DbsField pnd_Unit_Exist_Cntr;
    private DbsField pnd_Et_Cntr;
    private DbsField pnd_Et_Limit;
    private DbsField pnd_Routing_Sqnce_Key;

    private DbsGroup pnd_Routing_Sqnce_Key__R_Field_1;
    private DbsField pnd_Routing_Sqnce_Key_Pnd_Rt_Sqnce_Prcss;
    private DbsField pnd_Routing_Sqnce_Key_Pnd_Rt_Sqnce_Nbr;
    private DbsField pnd_Routing_Sqnce_Key_Pnd_Actve_Ind;
    private DbsField pnd_Isn;
    private DbsField pnd_Unit_Save;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Wp_Work_Prcss_Id = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Work_Prcss_Id", "CWF-WP-WORK-PRCSS-ID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Id = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Wp_Work_Prcss_Id_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wp_Work_Prcss_Id_Actve_Ind = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");
        cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde = vw_cwf_Wp_Work_Prcss_Id.getRecord().newFieldInGroup("cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde", "OWNER-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "OWNER_UNIT_CDE");
        cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde.setDdmHeader("OWNER/UNIT");
        registerRecord(vw_cwf_Wp_Work_Prcss_Id);

        vw_cwf_Wp_Routing = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Routing", "CWF-WP-ROUTING"), "CWF_WP_ROUTING", "CWF_PROFILE");
        cwf_Wp_Routing_Rt_Sqnce_Prcss_Id = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Prcss_Id", "RT-SQNCE-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "RT_SQNCE_PRCSS_ID");
        cwf_Wp_Routing_Rt_Sqnce_Nbr = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        cwf_Wp_Routing_Rt_Sqnce_Nbr.setDdmHeader("ROUTING/SEQUENCE");
        cwf_Wp_Routing_Unit_Cde = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Wp_Routing_Unit_Cde.setDdmHeader("UNIT/CODE");
        registerRecord(vw_cwf_Wp_Routing);

        pnd_Read_Cntr = localVariables.newFieldInRecord("pnd_Read_Cntr", "#READ-CNTR", FieldType.NUMERIC, 7);
        pnd_Recs_Updt = localVariables.newFieldInRecord("pnd_Recs_Updt", "#RECS-UPDT", FieldType.NUMERIC, 7);
        pnd_Rout_Nfnd = localVariables.newFieldInRecord("pnd_Rout_Nfnd", "#ROUT-NFND", FieldType.NUMERIC, 7);
        pnd_Unit_Exist_Cntr = localVariables.newFieldInRecord("pnd_Unit_Exist_Cntr", "#UNIT-EXIST-CNTR", FieldType.NUMERIC, 7);
        pnd_Et_Cntr = localVariables.newFieldInRecord("pnd_Et_Cntr", "#ET-CNTR", FieldType.NUMERIC, 2);
        pnd_Et_Limit = localVariables.newFieldInRecord("pnd_Et_Limit", "#ET-LIMIT", FieldType.NUMERIC, 2);
        pnd_Routing_Sqnce_Key = localVariables.newFieldInRecord("pnd_Routing_Sqnce_Key", "#ROUTING-SQNCE-KEY", FieldType.STRING, 10);

        pnd_Routing_Sqnce_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Routing_Sqnce_Key__R_Field_1", "REDEFINE", pnd_Routing_Sqnce_Key);
        pnd_Routing_Sqnce_Key_Pnd_Rt_Sqnce_Prcss = pnd_Routing_Sqnce_Key__R_Field_1.newFieldInGroup("pnd_Routing_Sqnce_Key_Pnd_Rt_Sqnce_Prcss", "#RT-SQNCE-PRCSS", 
            FieldType.STRING, 6);
        pnd_Routing_Sqnce_Key_Pnd_Rt_Sqnce_Nbr = pnd_Routing_Sqnce_Key__R_Field_1.newFieldInGroup("pnd_Routing_Sqnce_Key_Pnd_Rt_Sqnce_Nbr", "#RT-SQNCE-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Routing_Sqnce_Key_Pnd_Actve_Ind = pnd_Routing_Sqnce_Key__R_Field_1.newFieldInGroup("pnd_Routing_Sqnce_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Unit_Save = localVariables.newFieldInRecord("pnd_Unit_Save", "#UNIT-SAVE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Wp_Work_Prcss_Id.reset();
        vw_cwf_Wp_Routing.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3002() throws Exception
    {
        super("Cwfb3002");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pnd_Et_Limit.setValue(10);                                                                                                                                        //Natural: ASSIGN #ET-LIMIT = 10
        vw_cwf_Wp_Work_Prcss_Id.startDatabaseRead                                                                                                                         //Natural: READ CWF-WP-WORK-PRCSS-ID BY WPID-UNIQ-KEY
        (
        "R1",
        new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") }
        );
        R1:
        while (condition(vw_cwf_Wp_Work_Prcss_Id.readNextRow("R1")))
        {
            pnd_Read_Cntr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #READ-CNTR
            if (condition(cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde.equals(" ")))                                                                                               //Natural: IF CWF-WP-WORK-PRCSS-ID.OWNER-UNIT-CDE = ' '
            {
                pnd_Routing_Sqnce_Key_Pnd_Rt_Sqnce_Prcss.setValue(cwf_Wp_Work_Prcss_Id_Work_Prcss_Id);                                                                    //Natural: MOVE CWF-WP-WORK-PRCSS-ID.WORK-PRCSS-ID TO #RT-SQNCE-PRCSS
                pnd_Routing_Sqnce_Key_Pnd_Rt_Sqnce_Nbr.setValue(1);                                                                                                       //Natural: MOVE 1 TO #RT-SQNCE-NBR
                //*       MOVE 'A'                                TO #ACTVE-IND
                pnd_Isn.setValue(vw_cwf_Wp_Work_Prcss_Id.getAstISN("R1"));                                                                                                //Natural: MOVE *ISN TO #ISN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Unit_Exist_Cntr.nadd(1);                                                                                                                              //Natural: ADD 1 TO #UNIT-EXIST-CNTR
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT END OF DATA
            vw_cwf_Wp_Routing.startDatabaseFind                                                                                                                           //Natural: FIND ( 1 ) CWF-WP-ROUTING WITH ROUTING-SQNCE-KEY = #ROUTING-SQNCE-KEY
            (
            "F1",
            new Wc[] { new Wc("ROUTING_SQNCE_KEY", "=", pnd_Routing_Sqnce_Key, WcType.WITH) },
            1
            );
            F1:
            while (condition(vw_cwf_Wp_Routing.readNextRow("F1", true)))
            {
                vw_cwf_Wp_Routing.setIfNotFoundControlFlag(false);
                if (condition(vw_cwf_Wp_Routing.getAstCOUNTER().equals(0)))                                                                                               //Natural: IF NO RECORD FOUND
                {
                    getReports().write(0, NEWLINE,"Record Not Found on Routing Table, Key =",pnd_Routing_Sqnce_Key);                                                      //Natural: WRITE /'Record Not Found on Routing Table, Key =' #ROUTING-SQNCE-KEY
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Rout_Nfnd.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ROUT-NFND
                    if (true) break F1;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F1. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Unit_Save.setValue(cwf_Wp_Routing_Unit_Cde);                                                                                                          //Natural: MOVE CWF-WP-ROUTING.UNIT-CDE TO #UNIT-SAVE
                                                                                                                                                                          //Natural: PERFORM UPDATE-RTN
                sub_Update_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  RITE /'Record Found ====== Key & Unit =' #ROUTING-SQNCE-KEY #UNIT-SAVE
                //*  F1.
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  R1.
        }                                                                                                                                                                 //Natural: END-READ
        if (condition(vw_cwf_Wp_Work_Prcss_Id.getAtEndOfData()))
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            getReports().write(0, NEWLINE,"Program = ",Global.getPROGRAM(),NEWLINE,NEWLINE," Work Process ID Correction Statistics",NEWLINE,"***************************************", //Natural: WRITE / 'Program = ' *PROGRAM // ' Work Process ID Correction Statistics' / '***************************************' / 'Number of WPID recs read  = ' #READ-CNTR / 'Number of WPID recs w/Unit= ' #UNIT-EXIST-CNTR / 'Number of WPID recs updtd = ' #RECS-UPDT / 'Number of route recs nfnd = ' #ROUT-NFND
                NEWLINE,"Number of WPID recs read  = ",pnd_Read_Cntr,NEWLINE,"Number of WPID recs w/Unit= ",pnd_Unit_Exist_Cntr,NEWLINE,"Number of WPID recs updtd = ",
                pnd_Recs_Updt,NEWLINE,"Number of route recs nfnd = ",pnd_Rout_Nfnd);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RTN
    }
    private void sub_Update_Rtn() throws Exception                                                                                                                        //Natural: UPDATE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        GET01:                                                                                                                                                            //Natural: GET CWF-WP-WORK-PRCSS-ID #ISN
        vw_cwf_Wp_Work_Prcss_Id.readByID(pnd_Isn.getLong(), "GET01");
        cwf_Wp_Work_Prcss_Id_Owner_Unit_Cde.setValue(pnd_Unit_Save);                                                                                                      //Natural: ASSIGN CWF-WP-WORK-PRCSS-ID.OWNER-UNIT-CDE := #UNIT-SAVE
        vw_cwf_Wp_Work_Prcss_Id.updateDBRow("GET01");                                                                                                                     //Natural: UPDATE
        pnd_Et_Cntr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #ET-CNTR
        if (condition(pnd_Et_Cntr.greaterOrEqual(pnd_Et_Limit)))                                                                                                          //Natural: IF #ET-CNTR GE #ET-LIMIT
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Et_Cntr.reset();                                                                                                                                          //Natural: RESET #ET-CNTR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recs_Updt.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #RECS-UPDT
    }

    //
}
