/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:33:14 PM
**        * FROM NATURAL PROGRAM : Cwfb4150
************************************************************
**        * FILE NAME            : Cwfb4150.java
**        * CLASS NAME           : Cwfb4150
**        * INSTANCE NAME        : Cwfb4150
************************************************************
************************************************************************
* PROGRAM  : CWFB4150
* FUNCTION : REVISED VERSION OF CWFB4140. QUARTERLY UPDATE TO CLOSE
*            ALL WORK REQUESTS FOR UNITS IDENTIFIED ON CWF CONTROL
*            TABLE AND WITH LOG DATE BETWEEN START/END DATES ON ANOTHER
*            CWF CONTROL TABLE. READ OF UNIT CONTROL TABLE CAN
*            ACCOMMODATE GAPS OR PARTIALLY FILLED LINES.
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017 <STOW ONLY>
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4150 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_mit;
    private DbsField mit_Pin_Nbr;
    private DbsField mit_Rqst_Log_Dte_Tme;
    private DbsField mit_Crprte_Status_Ind;
    private DbsField mit_Admin_Unit_Cde;
    private DbsField mit_Work_Prcss_Id;
    private DbsField mit_Last_Chnge_Dte_Tme;
    private DbsField mit_Last_Chnge_Invrt_Dte_Tme;
    private DbsField mit_Last_Chnge_Oprtr_Cde;
    private DbsField mit_Last_Chnge_Unit_Cde;
    private DbsField mit_Last_Updte_Dte;
    private DbsField mit_Last_Updte_Dte_Tme;
    private DbsField mit_Last_Updte_Oprtr_Cde;
    private DbsField mit_Final_Close_Out_Dte_Tme;
    private DbsField mit_Crprte_Clock_End_Dte_Tme;

    private DbsGroup mit__R_Field_1;
    private DbsField mit_Pnd_Corp_Clock_End_Dt_Tm_Num;

    private DbsGroup mit__R_Field_2;
    private DbsField mit_Pnd_Corp_Clock_End_Dte;
    private DbsField mit_Pnd_Corp_Clock_End_Tme;
    private DbsField mit_Tiaa_Rcvd_Dte_Tme;
    private DbsField mit_Status_Cde;
    private DbsField mit_Status_Updte_Dte_Tme;
    private DbsField mit_Status_Updte_Oprtr_Cde;
    private DbsField mit_Admin_Status_Cde;
    private DbsField mit_Admin_Status_Updte_Dte_Tme;
    private DbsField mit_Admin_Status_Updte_Oprtr_Cde;
    private DbsField pnd_Unit_Wpid_Key;

    private DbsGroup pnd_Unit_Wpid_Key__R_Field_3;
    private DbsField pnd_Unit_Wpid_Key_Pnd_Unit_Key_Status;
    private DbsField pnd_Unit_Wpid_Key_Pnd_Unit_Key_Admin_Unit;
    private DbsField pnd_Unit_Wpid_Key_Pnd_Unit_Key_Wpid;
    private DbsField pnd_Unit_Wpid_Key_Pnd_Unit_Key_Updte_Dte_Tme;
    private DbsField pnd_Unit_Wpid_Key_Pnd_Unit_Key_Actve_Ind;
    private DbsField pnd_Rcvd_Dt_Tm;

    private DbsGroup pnd_Rcvd_Dt_Tm__R_Field_4;
    private DbsField pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Dte_Num;

    private DbsGroup pnd_Rcvd_Dt_Tm__R_Field_5;
    private DbsField pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Date_Alph;
    private DbsField pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Tme;
    private DbsField pnd_Isn;
    private DbsField pnd_Read_Cntr;
    private DbsField pnd_Update_Cntr;
    private DbsField pnd_Et_Cntr;

    private DataAccessProgramView vw_cwf_Suptb;
    private DbsField cwf_Suptb_Tbl_Table_Rectype;
    private DbsField cwf_Suptb_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Suptb_Tbl_Table_Nme;
    private DbsField cwf_Suptb_Tbl_Key_Field;
    private DbsField cwf_Suptb_Tbl_Data_Field;
    private DbsField cwf_Suptb_Tbl_Actve_Ind;
    private DbsField cwf_Suptb_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Suptb_Tbl_Updte_Dte;
    private DbsField cwf_Suptb_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Data;

    private DbsGroup pnd_Tbl_Data__R_Field_6;
    private DbsField pnd_Tbl_Data_Pnd_Tbl_Line;
    private DbsField pnd_Tbl_Data_Pnd_Chaff;
    private DbsField pnd_Units_Only;

    private DbsGroup pnd_Units_Only__R_Field_7;
    private DbsField pnd_Units_Only_Pnd_Unit_Plus_One;
    private DbsField pnd_Unit;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_8;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Dates_And_Limit;

    private DbsGroup pnd_Dates_And_Limit__R_Field_9;
    private DbsField pnd_Dates_And_Limit_Pnd_Start_Date;
    private DbsField pnd_Dates_And_Limit_Pnd_Filla;
    private DbsField pnd_Dates_And_Limit_Pnd_End_Date;
    private DbsField pnd_Dates_And_Limit_Pnd_Fillah;
    private DbsField pnd_Dates_And_Limit_Pnd_Limit;
    private DbsField pnd_Date_Plus_90_Days_Alph;

    private DbsGroup pnd_Date_Plus_90_Days_Alph__R_Field_10;
    private DbsField pnd_Date_Plus_90_Days_Alph_Pnd_Date_Plus_90_Days_Num;
    private DbsField pnd_Date_Format_Dte;
    private DbsField pnd_Open_90_Days;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_mit = new DataAccessProgramView(new NameInfo("vw_mit", "MIT"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        mit_Pin_Nbr = vw_mit.getRecord().newFieldInGroup("mit_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        mit_Pin_Nbr.setDdmHeader("PIN");
        mit_Rqst_Log_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        mit_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        mit_Crprte_Status_Ind = vw_mit.getRecord().newFieldInGroup("mit_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_STATUS_IND");
        mit_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        mit_Admin_Unit_Cde = vw_mit.getRecord().newFieldInGroup("mit_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        mit_Work_Prcss_Id = vw_mit.getRecord().newFieldInGroup("mit_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        mit_Work_Prcss_Id.setDdmHeader("WORK/ID");
        mit_Last_Chnge_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, 
            "LAST_CHNGE_DTE_TME");
        mit_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        mit_Last_Chnge_Invrt_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Last_Chnge_Invrt_Dte_Tme", "LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        mit_Last_Chnge_Oprtr_Cde = vw_mit.getRecord().newFieldInGroup("mit_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_CHNGE_OPRTR_CDE");
        mit_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        mit_Last_Chnge_Unit_Cde = vw_mit.getRecord().newFieldInGroup("mit_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_CHNGE_UNIT_CDE");
        mit_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        mit_Last_Updte_Dte = vw_mit.getRecord().newFieldInGroup("mit_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "LAST_UPDTE_DTE");
        mit_Last_Updte_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LAST_UPDTE_DTE_TME");
        mit_Last_Updte_Oprtr_Cde = vw_mit.getRecord().newFieldInGroup("mit_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_UPDTE_OPRTR_CDE");
        mit_Final_Close_Out_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Final_Close_Out_Dte_Tme", "FINAL-CLOSE-OUT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "FINAL_CLOSE_OUT_DTE_TME");
        mit_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        mit_Crprte_Clock_End_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        mit_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");

        mit__R_Field_1 = vw_mit.getRecord().newGroupInGroup("mit__R_Field_1", "REDEFINE", mit_Crprte_Clock_End_Dte_Tme);
        mit_Pnd_Corp_Clock_End_Dt_Tm_Num = mit__R_Field_1.newFieldInGroup("mit_Pnd_Corp_Clock_End_Dt_Tm_Num", "#CORP-CLOCK-END-DT-TM-NUM", FieldType.NUMERIC, 
            15);

        mit__R_Field_2 = mit__R_Field_1.newGroupInGroup("mit__R_Field_2", "REDEFINE", mit_Pnd_Corp_Clock_End_Dt_Tm_Num);
        mit_Pnd_Corp_Clock_End_Dte = mit__R_Field_2.newFieldInGroup("mit_Pnd_Corp_Clock_End_Dte", "#CORP-CLOCK-END-DTE", FieldType.NUMERIC, 8);
        mit_Pnd_Corp_Clock_End_Tme = mit__R_Field_2.newFieldInGroup("mit_Pnd_Corp_Clock_End_Tme", "#CORP-CLOCK-END-TME", FieldType.NUMERIC, 7);
        mit_Tiaa_Rcvd_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE_TME");
        mit_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        mit_Status_Cde = vw_mit.getRecord().newFieldInGroup("mit_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "STATUS_CDE");
        mit_Status_Updte_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "STATUS_UPDTE_DTE_TME");
        mit_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        mit_Status_Updte_Oprtr_Cde = vw_mit.getRecord().newFieldInGroup("mit_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "STATUS_UPDTE_OPRTR_CDE");
        mit_Admin_Status_Cde = vw_mit.getRecord().newFieldInGroup("mit_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "ADMIN_STATUS_CDE");
        mit_Admin_Status_Updte_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        mit_Admin_Status_Updte_Oprtr_Cde = vw_mit.getRecord().newFieldInGroup("mit_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        registerRecord(vw_mit);

        pnd_Unit_Wpid_Key = localVariables.newFieldInRecord("pnd_Unit_Wpid_Key", "#UNIT-WPID-KEY", FieldType.STRING, 23);

        pnd_Unit_Wpid_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Unit_Wpid_Key__R_Field_3", "REDEFINE", pnd_Unit_Wpid_Key);
        pnd_Unit_Wpid_Key_Pnd_Unit_Key_Status = pnd_Unit_Wpid_Key__R_Field_3.newFieldInGroup("pnd_Unit_Wpid_Key_Pnd_Unit_Key_Status", "#UNIT-KEY-STATUS", 
            FieldType.STRING, 1);
        pnd_Unit_Wpid_Key_Pnd_Unit_Key_Admin_Unit = pnd_Unit_Wpid_Key__R_Field_3.newFieldInGroup("pnd_Unit_Wpid_Key_Pnd_Unit_Key_Admin_Unit", "#UNIT-KEY-ADMIN-UNIT", 
            FieldType.STRING, 8);
        pnd_Unit_Wpid_Key_Pnd_Unit_Key_Wpid = pnd_Unit_Wpid_Key__R_Field_3.newFieldInGroup("pnd_Unit_Wpid_Key_Pnd_Unit_Key_Wpid", "#UNIT-KEY-WPID", FieldType.STRING, 
            6);
        pnd_Unit_Wpid_Key_Pnd_Unit_Key_Updte_Dte_Tme = pnd_Unit_Wpid_Key__R_Field_3.newFieldInGroup("pnd_Unit_Wpid_Key_Pnd_Unit_Key_Updte_Dte_Tme", "#UNIT-KEY-UPDTE-DTE-TME", 
            FieldType.TIME);
        pnd_Unit_Wpid_Key_Pnd_Unit_Key_Actve_Ind = pnd_Unit_Wpid_Key__R_Field_3.newFieldInGroup("pnd_Unit_Wpid_Key_Pnd_Unit_Key_Actve_Ind", "#UNIT-KEY-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Rcvd_Dt_Tm = localVariables.newFieldInRecord("pnd_Rcvd_Dt_Tm", "#RCVD-DT-TM", FieldType.STRING, 15);

        pnd_Rcvd_Dt_Tm__R_Field_4 = localVariables.newGroupInRecord("pnd_Rcvd_Dt_Tm__R_Field_4", "REDEFINE", pnd_Rcvd_Dt_Tm);
        pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Dte_Num = pnd_Rcvd_Dt_Tm__R_Field_4.newFieldInGroup("pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Dte_Num", "#RCVD-DTE-NUM", FieldType.NUMERIC, 
            8);

        pnd_Rcvd_Dt_Tm__R_Field_5 = pnd_Rcvd_Dt_Tm__R_Field_4.newGroupInGroup("pnd_Rcvd_Dt_Tm__R_Field_5", "REDEFINE", pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Dte_Num);
        pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Date_Alph = pnd_Rcvd_Dt_Tm__R_Field_5.newFieldInGroup("pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Date_Alph", "#RCVD-DATE-ALPH", FieldType.STRING, 
            8);
        pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Tme = pnd_Rcvd_Dt_Tm__R_Field_4.newFieldInGroup("pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Tme", "#RCVD-TME", FieldType.NUMERIC, 7);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.BINARY, 4);
        pnd_Read_Cntr = localVariables.newFieldInRecord("pnd_Read_Cntr", "#READ-CNTR", FieldType.NUMERIC, 7);
        pnd_Update_Cntr = localVariables.newFieldInRecord("pnd_Update_Cntr", "#UPDATE-CNTR", FieldType.NUMERIC, 7);
        pnd_Et_Cntr = localVariables.newFieldInRecord("pnd_Et_Cntr", "#ET-CNTR", FieldType.NUMERIC, 3);

        vw_cwf_Suptb = new DataAccessProgramView(new NameInfo("vw_cwf_Suptb", "CWF-SUPTB"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Suptb_Tbl_Table_Rectype = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Suptb_Tbl_Scrty_Level_Ind = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Suptb_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Suptb_Tbl_Table_Nme = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TBL_TABLE_NME");
        cwf_Suptb_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Suptb_Tbl_Key_Field = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "TBL_KEY_FIELD");
        cwf_Suptb_Tbl_Data_Field = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");
        cwf_Suptb_Tbl_Actve_Ind = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TBL_ACTVE_IND");
        cwf_Suptb_Tbl_Updte_Dte_Tme = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE_TME");
        cwf_Suptb_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Suptb_Tbl_Updte_Dte = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Suptb_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Suptb_Tbl_Updte_Oprtr_Cde = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Suptb_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Suptb);

        pnd_Tbl_Data = localVariables.newFieldInRecord("pnd_Tbl_Data", "#TBL-DATA", FieldType.STRING, 253);

        pnd_Tbl_Data__R_Field_6 = localVariables.newGroupInRecord("pnd_Tbl_Data__R_Field_6", "REDEFINE", pnd_Tbl_Data);
        pnd_Tbl_Data_Pnd_Tbl_Line = pnd_Tbl_Data__R_Field_6.newFieldArrayInGroup("pnd_Tbl_Data_Pnd_Tbl_Line", "#TBL-LINE", FieldType.STRING, 63, new DbsArrayController(1, 
            4));
        pnd_Tbl_Data_Pnd_Chaff = pnd_Tbl_Data__R_Field_6.newFieldInGroup("pnd_Tbl_Data_Pnd_Chaff", "#CHAFF", FieldType.STRING, 1);
        pnd_Units_Only = localVariables.newFieldInRecord("pnd_Units_Only", "#UNITS-ONLY", FieldType.STRING, 63);

        pnd_Units_Only__R_Field_7 = localVariables.newGroupInRecord("pnd_Units_Only__R_Field_7", "REDEFINE", pnd_Units_Only);
        pnd_Units_Only_Pnd_Unit_Plus_One = pnd_Units_Only__R_Field_7.newFieldArrayInGroup("pnd_Units_Only_Pnd_Unit_Plus_One", "#UNIT-PLUS-ONE", FieldType.STRING, 
            9, new DbsArrayController(1, 7));
        pnd_Unit = localVariables.newFieldInRecord("pnd_Unit", "#UNIT", FieldType.STRING, 9);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 4);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.INTEGER, 4);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_8", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_8.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_8.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_8.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_8.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Dates_And_Limit = localVariables.newFieldInRecord("pnd_Dates_And_Limit", "#DATES-AND-LIMIT", FieldType.STRING, 240);

        pnd_Dates_And_Limit__R_Field_9 = localVariables.newGroupInRecord("pnd_Dates_And_Limit__R_Field_9", "REDEFINE", pnd_Dates_And_Limit);
        pnd_Dates_And_Limit_Pnd_Start_Date = pnd_Dates_And_Limit__R_Field_9.newFieldInGroup("pnd_Dates_And_Limit_Pnd_Start_Date", "#START-DATE", FieldType.NUMERIC, 
            8);
        pnd_Dates_And_Limit_Pnd_Filla = pnd_Dates_And_Limit__R_Field_9.newFieldInGroup("pnd_Dates_And_Limit_Pnd_Filla", "#FILLA", FieldType.STRING, 1);
        pnd_Dates_And_Limit_Pnd_End_Date = pnd_Dates_And_Limit__R_Field_9.newFieldInGroup("pnd_Dates_And_Limit_Pnd_End_Date", "#END-DATE", FieldType.NUMERIC, 
            8);
        pnd_Dates_And_Limit_Pnd_Fillah = pnd_Dates_And_Limit__R_Field_9.newFieldInGroup("pnd_Dates_And_Limit_Pnd_Fillah", "#FILLAH", FieldType.STRING, 
            1);
        pnd_Dates_And_Limit_Pnd_Limit = pnd_Dates_And_Limit__R_Field_9.newFieldInGroup("pnd_Dates_And_Limit_Pnd_Limit", "#LIMIT", FieldType.NUMERIC, 9);
        pnd_Date_Plus_90_Days_Alph = localVariables.newFieldInRecord("pnd_Date_Plus_90_Days_Alph", "#DATE-PLUS-90-DAYS-ALPH", FieldType.STRING, 8);

        pnd_Date_Plus_90_Days_Alph__R_Field_10 = localVariables.newGroupInRecord("pnd_Date_Plus_90_Days_Alph__R_Field_10", "REDEFINE", pnd_Date_Plus_90_Days_Alph);
        pnd_Date_Plus_90_Days_Alph_Pnd_Date_Plus_90_Days_Num = pnd_Date_Plus_90_Days_Alph__R_Field_10.newFieldInGroup("pnd_Date_Plus_90_Days_Alph_Pnd_Date_Plus_90_Days_Num", 
            "#DATE-PLUS-90-DAYS-NUM", FieldType.NUMERIC, 8);
        pnd_Date_Format_Dte = localVariables.newFieldInRecord("pnd_Date_Format_Dte", "#DATE-FORMAT-DTE", FieldType.DATE);
        pnd_Open_90_Days = localVariables.newFieldInRecord("pnd_Open_90_Days", "#OPEN-90-DAYS", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_mit.reset();
        vw_cwf_Suptb.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4150() throws Exception
    {
        super("Cwfb4150");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ***************************************************************                                                                                               //Natural: FORMAT LS = 80 PS = 38;//Natural: FORMAT ( 1 ) LS = 80 PS = 50
                                                                                                                                                                          //Natural: PERFORM READ-DATE-CONTROL-RECORD
        sub_Read_Date_Control_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-AND-PROCESS-TARGET-UNITS
        sub_Read_And_Process_Target_Units();
        if (condition(Global.isEscape())) {return;}
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(0, "OPEN MIT WRK RQSTS IN TARGET UNITS:",pnd_Read_Cntr, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                     //Natural: WRITE 'OPEN MIT WRK RQSTS IN TARGET UNITS:' #READ-CNTR ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "CLSED RQSTS - RCVD DTES IN RANGE:  ",pnd_Update_Cntr, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                   //Natural: WRITE 'CLSED RQSTS - RCVD DTES IN RANGE:  ' #UPDATE-CNTR ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-DATE-CONTROL-RECORD
        //* ************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-AND-PROCESS-TARGET-UNITS
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-SCREEN-DATA
        //* ************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-MIT
        //* ******************************************************************
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-90-DAYS-OPEN
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AND-UPDATE
    }
    private void sub_Read_Date_Control_Record() throws Exception                                                                                                          //Natural: READ-DATE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("S ");                                                                                                         //Natural: MOVE 'S ' TO #TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("CWF-CLOSE-WORK-REQ-D");                                                                                             //Natural: MOVE 'CWF-CLOSE-WORK-REQ-D' TO #TBL-TABLE-NME
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue(" ");                                                                                                                //Natural: MOVE ' ' TO #TBL-KEY-FIELD
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind.setValue(" ");                                                                                                                //Natural: MOVE ' ' TO #TBL-ACTVE-IND
        vw_cwf_Suptb.startDatabaseRead                                                                                                                                    //Natural: READ CWF-SUPTB BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Suptb.readNextRow("READ01")))
        {
            if (condition(cwf_Suptb_Tbl_Scrty_Level_Ind.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind) || cwf_Suptb_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme))) //Natural: IF TBL-SCRTY-LEVEL-IND NE #TBL-SCRTY-LEVEL-IND OR TBL-TABLE-NME NE #TBL-TABLE-NME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Dates_And_Limit.setValue(cwf_Suptb_Tbl_Data_Field);                                                                                                       //Natural: MOVE TBL-DATA-FIELD TO #DATES-AND-LIMIT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_And_Process_Target_Units() throws Exception                                                                                                     //Natural: READ-AND-PROCESS-TARGET-UNITS
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************************
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("CWF-CLOSE-WORK-REQ  ");                                                                                             //Natural: MOVE 'CWF-CLOSE-WORK-REQ  ' TO #TBL-TABLE-NME
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("UNITS ONLY          ");                                                                                             //Natural: MOVE 'UNITS ONLY          ' TO #TBL-KEY-FIELD
        vw_cwf_Suptb.startDatabaseRead                                                                                                                                    //Natural: READ CWF-SUPTB BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ02",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf_Suptb.readNextRow("READ02")))
        {
            if (condition(cwf_Suptb_Tbl_Scrty_Level_Ind.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind) || cwf_Suptb_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)  //Natural: IF TBL-SCRTY-LEVEL-IND NE #TBL-SCRTY-LEVEL-IND OR TBL-TABLE-NME NE #TBL-TABLE-NME OR TBL-KEY-FIELD NE #TBL-KEY-FIELD
                || cwf_Suptb_Tbl_Key_Field.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tbl_Data.setValue(cwf_Suptb_Tbl_Data_Field);                                                                                                              //Natural: MOVE TBL-DATA-FIELD TO #TBL-DATA
                                                                                                                                                                          //Natural: PERFORM PROCESS-SCREEN-DATA
            sub_Process_Screen_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Process_Screen_Data() throws Exception                                                                                                               //Natural: PROCESS-SCREEN-DATA
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #X 1 4
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(4)); pnd_X.nadd(1))
        {
            if (condition(pnd_Tbl_Data_Pnd_Tbl_Line.getValue(pnd_X).equals(" ")))                                                                                         //Natural: IF #TBL-LINE ( #X ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Units_Only.setValue(pnd_Tbl_Data_Pnd_Tbl_Line.getValue(pnd_X));                                                                                       //Natural: MOVE #TBL-LINE ( #X ) TO #UNITS-ONLY
                FOR02:                                                                                                                                                    //Natural: FOR #Y 1 7
                for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(7)); pnd_Y.nadd(1))
                {
                    if (condition(pnd_Units_Only_Pnd_Unit_Plus_One.getValue(pnd_Y).notEquals(" ")))                                                                       //Natural: IF #UNIT-PLUS-ONE ( #Y ) NE ' '
                    {
                        pnd_Unit.setValue(pnd_Units_Only_Pnd_Unit_Plus_One.getValue(pnd_Y));                                                                              //Natural: MOVE #UNIT-PLUS-ONE ( #Y ) TO #UNIT
                                                                                                                                                                          //Natural: PERFORM READ-MIT
                        sub_Read_Mit();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Read_Mit() throws Exception                                                                                                                          //Natural: READ-MIT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Unit_Wpid_Key_Pnd_Unit_Key_Status.setValue("0");                                                                                                              //Natural: ASSIGN #UNIT-KEY-STATUS := '0'
        pnd_Unit_Wpid_Key_Pnd_Unit_Key_Admin_Unit.setValue(pnd_Unit);                                                                                                     //Natural: ASSIGN #UNIT-KEY-ADMIN-UNIT := #UNIT
        pnd_Unit_Wpid_Key_Pnd_Unit_Key_Wpid.setValue(" ");                                                                                                                //Natural: ASSIGN #UNIT-KEY-WPID := ' '
        pnd_Unit_Wpid_Key_Pnd_Unit_Key_Updte_Dte_Tme.reset();                                                                                                             //Natural: RESET #UNIT-KEY-UPDTE-DTE-TME
        pnd_Unit_Wpid_Key_Pnd_Unit_Key_Actve_Ind.setValue(" ");                                                                                                           //Natural: ASSIGN #UNIT-KEY-ACTVE-IND := ' '
        vw_mit.startDatabaseRead                                                                                                                                          //Natural: READ MIT BY UNIT-WPID-KEY STARTING FROM #UNIT-WPID-KEY
        (
        "READ03",
        new Wc[] { new Wc("UNIT_WPID_KEY", ">=", pnd_Unit_Wpid_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("UNIT_WPID_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_mit.readNextRow("READ03")))
        {
            if (condition(mit_Crprte_Status_Ind.notEquals(pnd_Unit_Wpid_Key_Pnd_Unit_Key_Status) || mit_Admin_Unit_Cde.notEquals(pnd_Unit_Wpid_Key_Pnd_Unit_Key_Admin_Unit))) //Natural: IF MIT.CRPRTE-STATUS-IND NE #UNIT-KEY-STATUS OR MIT.ADMIN-UNIT-CDE NE #UNIT-KEY-ADMIN-UNIT
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Read_Cntr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #READ-CNTR
            if (condition(pnd_Read_Cntr.greater(pnd_Dates_And_Limit_Pnd_Limit)))                                                                                          //Natural: IF #READ-CNTR GT #LIMIT
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rcvd_Dt_Tm.setValueEdited(mit_Tiaa_Rcvd_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                                   //Natural: MOVE EDITED MIT.TIAA-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #RCVD-DT-TM
            if (condition(pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Dte_Num.greaterOrEqual(pnd_Dates_And_Limit_Pnd_Start_Date) && pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Dte_Num.lessOrEqual(pnd_Dates_And_Limit_Pnd_End_Date))) //Natural: IF #RCVD-DTE-NUM GE #START-DATE AND #RCVD-DTE-NUM LE #END-DATE
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-90-DAYS-OPEN
                sub_Check_If_90_Days_Open();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Open_90_Days.getBoolean()))                                                                                                             //Natural: IF #OPEN-90-DAYS
                {
                    getReports().display(1, "UNIT",                                                                                                                       //Natural: DISPLAY ( 1 ) 'UNIT' #UNIT 'PIN' PIN-NBR 'LOG DTE TME' RQST-LOG-DTE-TME ( EM = XXXX/XX/XX/XX:XX:XX:X )
                    		pnd_Unit,"PIN",
                    		mit_Pin_Nbr,"LOG DTE TME",
                    		mit_Rqst_Log_Dte_Tme, new ReportEditMask ("XXXX/XX/XX/XX:XX:XX:X"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Isn.setValue(vw_mit.getAstISN("Read03"));                                                                                                         //Natural: MOVE *ISN TO #ISN
                                                                                                                                                                          //Natural: PERFORM GET-AND-UPDATE
                    sub_Get_And_Update();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Check_If_90_Days_Open() throws Exception                                                                                                             //Natural: CHECK-IF-90-DAYS-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        pnd_Date_Format_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Rcvd_Dt_Tm_Pnd_Rcvd_Date_Alph);                                                             //Natural: MOVE EDITED #RCVD-DATE-ALPH TO #DATE-FORMAT-DTE ( EM = YYYYMMDD )
        pnd_Date_Format_Dte.nadd(90);                                                                                                                                     //Natural: ADD 90 TO #DATE-FORMAT-DTE
        pnd_Date_Plus_90_Days_Alph.setValueEdited(pnd_Date_Format_Dte,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED #DATE-FORMAT-DTE ( EM = YYYYMMDD ) TO #DATE-PLUS-90-DAYS-ALPH
        if (condition(pnd_Date_Plus_90_Days_Alph_Pnd_Date_Plus_90_Days_Num.less(Global.getDATN())))                                                                       //Natural: IF #DATE-PLUS-90-DAYS-NUM LT *DATN
        {
            pnd_Open_90_Days.setValue(true);                                                                                                                              //Natural: ASSIGN #OPEN-90-DAYS := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Open_90_Days.setValue(false);                                                                                                                             //Natural: ASSIGN #OPEN-90-DAYS := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_And_Update() throws Exception                                                                                                                    //Natural: GET-AND-UPDATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        GET01:                                                                                                                                                            //Natural: GET MIT #ISN
        vw_mit.readByID(pnd_Isn.getLong(), "GET01");
        mit_Crprte_Status_Ind.setValue("9");                                                                                                                              //Natural: ASSIGN MIT.CRPRTE-STATUS-IND := '9'
        mit_Final_Close_Out_Dte_Tme.setValue(Global.getTIMX());                                                                                                           //Natural: MOVE *TIMX TO MIT.FINAL-CLOSE-OUT-DTE-TME MIT.ADMIN-STATUS-UPDTE-DTE-TME MIT.STATUS-UPDTE-DTE-TME MIT.ADMIN-STATUS-UPDTE-DTE-TME MIT.LAST-UPDTE-DTE-TME
        mit_Admin_Status_Updte_Dte_Tme.setValue(Global.getTIMX());
        mit_Status_Updte_Dte_Tme.setValue(Global.getTIMX());
        mit_Admin_Status_Updte_Dte_Tme.setValue(Global.getTIMX());
        mit_Last_Updte_Dte_Tme.setValue(Global.getTIMX());
        mit_Crprte_Clock_End_Dte_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                                              //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO MIT.CRPRTE-CLOCK-END-DTE-TME
        mit_Last_Chnge_Dte_Tme.setValue(mit_Pnd_Corp_Clock_End_Dt_Tm_Num);                                                                                                //Natural: MOVE #CORP-CLOCK-END-DT-TM-NUM TO MIT.LAST-CHNGE-DTE-TME
        mit_Last_Chnge_Invrt_Dte_Tme.compute(new ComputeParameters(false, mit_Last_Chnge_Invrt_Dte_Tme), (new DbsDecimal("999999999999999").subtract(mit_Pnd_Corp_Clock_End_Dt_Tm_Num))); //Natural: COMPUTE MIT.LAST-CHNGE-INVRT-DTE-TME = ( 999999999999999 - #CORP-CLOCK-END-DT-TM-NUM )
        mit_Last_Updte_Dte.setValue(mit_Pnd_Corp_Clock_End_Dte);                                                                                                          //Natural: MOVE #CORP-CLOCK-END-DTE TO MIT.LAST-UPDTE-DTE
        mit_Last_Chnge_Oprtr_Cde.setValue("CWFB4150");                                                                                                                    //Natural: MOVE 'CWFB4150' TO MIT.LAST-CHNGE-OPRTR-CDE MIT.LAST-UPDTE-OPRTR-CDE MIT.STATUS-UPDTE-OPRTR-CDE MIT.ADMIN-STATUS-UPDTE-OPRTR-CDE
        mit_Last_Updte_Oprtr_Cde.setValue("CWFB4150");
        mit_Status_Updte_Oprtr_Cde.setValue("CWFB4150");
        mit_Admin_Status_Updte_Oprtr_Cde.setValue("CWFB4150");
        mit_Last_Chnge_Unit_Cde.setValue("CWF  ");                                                                                                                        //Natural: ASSIGN MIT.LAST-CHNGE-UNIT-CDE := 'CWF  '
        mit_Status_Cde.setValue("8046");                                                                                                                                  //Natural: ASSIGN MIT.STATUS-CDE := '8046'
        mit_Admin_Status_Cde.setValue("8046");                                                                                                                            //Natural: ASSIGN MIT.ADMIN-STATUS-CDE := '8046'
        vw_mit.updateDBRow("GET01");                                                                                                                                      //Natural: UPDATE
        pnd_Update_Cntr.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #UPDATE-CNTR
        pnd_Et_Cntr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #ET-CNTR
        if (condition(pnd_Et_Cntr.greater(99)))                                                                                                                           //Natural: IF #ET-CNTR GT 99
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Et_Cntr.setValue(0);                                                                                                                                      //Natural: MOVE 0 TO #ET-CNTR
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=80 PS=38");
        Global.format(1, "LS=80 PS=50");

        getReports().setDisplayColumns(1, "UNIT",
        		pnd_Unit,"PIN",
        		mit_Pin_Nbr,"LOG DTE TME",
        		mit_Rqst_Log_Dte_Tme, new ReportEditMask ("XXXX/XX/XX/XX:XX:XX:X"));
    }
}
