/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:46:37 PM
**        * FROM NATURAL PROGRAM : Cwfb8641
************************************************************
**        * FILE NAME            : Cwfb8641.java
**        * CLASS NAME           : Cwfb8641
**        * INSTANCE NAME        : Cwfb8641
************************************************************
************************************************************************
* SYSTEM   : CORPORATE WORKFLOW
* PROGRAM  : CWFB8641
* TITLE    : PENDING/AGING ATA & LIFE INSURANCE DISBURSEMENT REPORT
* FUNCTION : ADD HEADER TO THE SORTED EXTRACT FILE AND WRITE THE REPORT
* HISTORY  : CREATED ON JUL 1, 2015 - VINODHKUMAR RAMACHANDRAN
* 02/23/2017 - MONDALP - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8641 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Workfile;

    private DbsGroup pnd_Input_Workfile__R_Field_1;
    private DbsField pnd_Input_Workfile_Pnd_I_Last_Chnge_Oprtr;
    private DbsField pnd_Input_Workfile_Pnd_I_Filler1;
    private DbsField pnd_Input_Workfile_Pnd_I_Cntrct_Nbr;
    private DbsField pnd_Input_Workfile_Pnd_I_Filler2;
    private DbsField pnd_Input_Workfile_Pnd_I_Pin_Nbr;
    private DbsField pnd_Input_Workfile_Pnd_I_Filler3;
    private DbsField pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte;

    private DbsGroup pnd_Input_Workfile__R_Field_2;
    private DbsField pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte_Yyyy;
    private DbsField pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte_Mm;
    private DbsField pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte_Dd;
    private DbsField pnd_Input_Workfile_Pnd_I_Filler4;
    private DbsField pnd_Input_Workfile_Pnd_I_Work_Prcss_Id;
    private DbsField pnd_Input_Workfile_Pnd_I_Filler5;
    private DbsField pnd_Input_Workfile_Pnd_I_Transaction_Type;
    private DbsField pnd_Input_Workfile_Pnd_I_Filler6;
    private DbsField pnd_Input_Workfile_Pnd_I_Clndr_Days_Aging;
    private DbsField pnd_Output_Workfile;

    private DbsGroup pnd_Output_Workfile__R_Field_3;
    private DbsField pnd_Output_Workfile_Pnd_O_Last_Chnge_Oprtr;
    private DbsField pnd_Output_Workfile_Pnd_O_Filler1;
    private DbsField pnd_Output_Workfile_Pnd_O_Cntrct_Nbr;
    private DbsField pnd_Output_Workfile_Pnd_O_Filler2;
    private DbsField pnd_Output_Workfile_Pnd_O_Pin_Nbr;
    private DbsField pnd_Output_Workfile_Pnd_O_Filler3;
    private DbsField pnd_Output_Workfile_Pnd_O_Tiaa_Rcvd_Dte;
    private DbsField pnd_Output_Workfile_Pnd_O_Filler4;
    private DbsField pnd_Output_Workfile_Pnd_O_Work_Prcss_Id;
    private DbsField pnd_Output_Workfile_Pnd_O_Filler5;
    private DbsField pnd_Output_Workfile_Pnd_O_Transaction_Type;
    private DbsField pnd_Output_Workfile_Pnd_O_Filler6;
    private DbsField pnd_Output_Workfile_Pnd_O_Clndr_Days_Aging;
    private DbsField pnd_Rec_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Input_Workfile = localVariables.newFieldInRecord("pnd_Input_Workfile", "#INPUT-WORKFILE", FieldType.STRING, 181);

        pnd_Input_Workfile__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Workfile__R_Field_1", "REDEFINE", pnd_Input_Workfile);
        pnd_Input_Workfile_Pnd_I_Last_Chnge_Oprtr = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Last_Chnge_Oprtr", "#I-LAST-CHNGE-OPRTR", 
            FieldType.STRING, 40);
        pnd_Input_Workfile_Pnd_I_Filler1 = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Filler1", "#I-FILLER1", FieldType.STRING, 
            1);
        pnd_Input_Workfile_Pnd_I_Cntrct_Nbr = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Cntrct_Nbr", "#I-CNTRCT-NBR", FieldType.STRING, 
            90);
        pnd_Input_Workfile_Pnd_I_Filler2 = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Filler2", "#I-FILLER2", FieldType.STRING, 
            1);
        pnd_Input_Workfile_Pnd_I_Pin_Nbr = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Pin_Nbr", "#I-PIN-NBR", FieldType.STRING, 
            12);
        pnd_Input_Workfile_Pnd_I_Filler3 = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Filler3", "#I-FILLER3", FieldType.STRING, 
            1);
        pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte", "#I-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);

        pnd_Input_Workfile__R_Field_2 = pnd_Input_Workfile__R_Field_1.newGroupInGroup("pnd_Input_Workfile__R_Field_2", "REDEFINE", pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte);
        pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte_Yyyy = pnd_Input_Workfile__R_Field_2.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte_Yyyy", "#I-TIAA-RCVD-DTE-YYYY", 
            FieldType.STRING, 4);
        pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte_Mm = pnd_Input_Workfile__R_Field_2.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte_Mm", "#I-TIAA-RCVD-DTE-MM", 
            FieldType.STRING, 2);
        pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte_Dd = pnd_Input_Workfile__R_Field_2.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte_Dd", "#I-TIAA-RCVD-DTE-DD", 
            FieldType.STRING, 2);
        pnd_Input_Workfile_Pnd_I_Filler4 = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Filler4", "#I-FILLER4", FieldType.STRING, 
            1);
        pnd_Input_Workfile_Pnd_I_Work_Prcss_Id = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Work_Prcss_Id", "#I-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        pnd_Input_Workfile_Pnd_I_Filler5 = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Filler5", "#I-FILLER5", FieldType.STRING, 
            1);
        pnd_Input_Workfile_Pnd_I_Transaction_Type = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Transaction_Type", "#I-TRANSACTION-TYPE", 
            FieldType.STRING, 15);
        pnd_Input_Workfile_Pnd_I_Filler6 = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Filler6", "#I-FILLER6", FieldType.STRING, 
            1);
        pnd_Input_Workfile_Pnd_I_Clndr_Days_Aging = pnd_Input_Workfile__R_Field_1.newFieldInGroup("pnd_Input_Workfile_Pnd_I_Clndr_Days_Aging", "#I-CLNDR-DAYS-AGING", 
            FieldType.NUMERIC, 4);
        pnd_Output_Workfile = localVariables.newFieldInRecord("pnd_Output_Workfile", "#OUTPUT-WORKFILE", FieldType.STRING, 200);

        pnd_Output_Workfile__R_Field_3 = localVariables.newGroupInRecord("pnd_Output_Workfile__R_Field_3", "REDEFINE", pnd_Output_Workfile);
        pnd_Output_Workfile_Pnd_O_Last_Chnge_Oprtr = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Last_Chnge_Oprtr", "#O-LAST-CHNGE-OPRTR", 
            FieldType.STRING, 40);
        pnd_Output_Workfile_Pnd_O_Filler1 = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Filler1", "#O-FILLER1", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_O_Cntrct_Nbr = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Cntrct_Nbr", "#O-CNTRCT-NBR", 
            FieldType.STRING, 90);
        pnd_Output_Workfile_Pnd_O_Filler2 = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Filler2", "#O-FILLER2", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_O_Pin_Nbr = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Pin_Nbr", "#O-PIN-NBR", FieldType.STRING, 
            12);
        pnd_Output_Workfile_Pnd_O_Filler3 = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Filler3", "#O-FILLER3", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_O_Tiaa_Rcvd_Dte = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Tiaa_Rcvd_Dte", "#O-TIAA-RCVD-DTE", 
            FieldType.STRING, 10);
        pnd_Output_Workfile_Pnd_O_Filler4 = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Filler4", "#O-FILLER4", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_O_Work_Prcss_Id = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Work_Prcss_Id", "#O-WORK-PRCSS-ID", 
            FieldType.STRING, 16);
        pnd_Output_Workfile_Pnd_O_Filler5 = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Filler5", "#O-FILLER5", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_O_Transaction_Type = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Transaction_Type", "#O-TRANSACTION-TYPE", 
            FieldType.STRING, 16);
        pnd_Output_Workfile_Pnd_O_Filler6 = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Filler6", "#O-FILLER6", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_O_Clndr_Days_Aging = pnd_Output_Workfile__R_Field_3.newFieldInGroup("pnd_Output_Workfile_Pnd_O_Clndr_Days_Aging", "#O-CLNDR-DAYS-AGING", 
            FieldType.STRING, 10);
        pnd_Rec_Count = localVariables.newFieldInRecord("pnd_Rec_Count", "#REC-COUNT", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8641() throws Exception
    {
        super("Cwfb8641");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* *----------------------------------------------------------------------
        pnd_Input_Workfile.reset();                                                                                                                                       //Natural: RESET #INPUT-WORKFILE #OUTPUT-WORKFILE #REC-COUNT
        pnd_Output_Workfile.reset();
        pnd_Rec_Count.reset();
        pnd_Output_Workfile.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "IFPDATADSB,", Global.getDATN(), ",", "InForce Ops Pending Disbursement Report,",    //Natural: COMPRESS 'IFPDATADSB,' *DATN ',' 'InForce Ops Pending Disbursement Report,' 'CWF' INTO #OUTPUT-WORKFILE LEAVING NO
            "CWF"));
        getWorkFiles().write(2, false, pnd_Output_Workfile);                                                                                                              //Natural: WRITE WORK FILE 2 #OUTPUT-WORKFILE
        pnd_Output_Workfile.reset();                                                                                                                                      //Natural: RESET #OUTPUT-WORKFILE
        pnd_Output_Workfile_Pnd_O_Last_Chnge_Oprtr.setValue("Assigned To");                                                                                               //Natural: MOVE 'Assigned To' TO #O-LAST-CHNGE-OPRTR
        pnd_Output_Workfile_Pnd_O_Cntrct_Nbr.setValue("Contract Number");                                                                                                 //Natural: MOVE 'Contract Number' TO #O-CNTRCT-NBR
        pnd_Output_Workfile_Pnd_O_Pin_Nbr.setValue("PIN");                                                                                                                //Natural: MOVE 'PIN' TO #O-PIN-NBR
        pnd_Output_Workfile_Pnd_O_Tiaa_Rcvd_Dte.setValue("Recvd Date");                                                                                                   //Natural: MOVE 'Recvd Date' TO #O-TIAA-RCVD-DTE
        pnd_Output_Workfile_Pnd_O_Work_Prcss_Id.setValue("Transaction WPID");                                                                                             //Natural: MOVE 'Transaction WPID' TO #O-WORK-PRCSS-ID
        pnd_Output_Workfile_Pnd_O_Transaction_Type.setValue("Transaction Type");                                                                                          //Natural: MOVE 'Transaction Type' TO #O-TRANSACTION-TYPE
        pnd_Output_Workfile_Pnd_O_Clndr_Days_Aging.setValue("Days Aging");                                                                                                //Natural: MOVE 'Days Aging' TO #O-CLNDR-DAYS-AGING
        pnd_Output_Workfile_Pnd_O_Filler1.setValue(",");                                                                                                                  //Natural: MOVE ',' TO #O-FILLER1 #O-FILLER2 #O-FILLER3 #O-FILLER4 #O-FILLER5 #O-FILLER6
        pnd_Output_Workfile_Pnd_O_Filler2.setValue(",");
        pnd_Output_Workfile_Pnd_O_Filler3.setValue(",");
        pnd_Output_Workfile_Pnd_O_Filler4.setValue(",");
        pnd_Output_Workfile_Pnd_O_Filler5.setValue(",");
        pnd_Output_Workfile_Pnd_O_Filler6.setValue(",");
        getWorkFiles().write(2, false, pnd_Output_Workfile);                                                                                                              //Natural: WRITE WORK FILE 2 #OUTPUT-WORKFILE
        pnd_Output_Workfile.reset();                                                                                                                                      //Natural: RESET #OUTPUT-WORKFILE
        pnd_Rec_Count.reset();                                                                                                                                            //Natural: RESET #REC-COUNT
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT-WORKFILE
        while (condition(getWorkFiles().read(1, pnd_Input_Workfile)))
        {
            pnd_Output_Workfile.reset();                                                                                                                                  //Natural: RESET #OUTPUT-WORKFILE
            pnd_Output_Workfile_Pnd_O_Last_Chnge_Oprtr.setValue(pnd_Input_Workfile_Pnd_I_Last_Chnge_Oprtr);                                                               //Natural: MOVE #I-LAST-CHNGE-OPRTR TO #O-LAST-CHNGE-OPRTR
            pnd_Output_Workfile_Pnd_O_Cntrct_Nbr.setValue(pnd_Input_Workfile_Pnd_I_Cntrct_Nbr);                                                                           //Natural: MOVE #I-CNTRCT-NBR TO #O-CNTRCT-NBR
            pnd_Output_Workfile_Pnd_O_Pin_Nbr.setValue(pnd_Input_Workfile_Pnd_I_Pin_Nbr);                                                                                 //Natural: MOVE #I-PIN-NBR TO #O-PIN-NBR
            pnd_Output_Workfile_Pnd_O_Tiaa_Rcvd_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte_Mm,                   //Natural: COMPRESS #I-TIAA-RCVD-DTE-MM '/' #I-TIAA-RCVD-DTE-DD '/' #I-TIAA-RCVD-DTE-YYYY INTO #O-TIAA-RCVD-DTE LEAVING NO
                "/", pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte_Dd, "/", pnd_Input_Workfile_Pnd_I_Tiaa_Rcvd_Dte_Yyyy));
            pnd_Output_Workfile_Pnd_O_Work_Prcss_Id.setValue(pnd_Input_Workfile_Pnd_I_Work_Prcss_Id);                                                                     //Natural: MOVE #I-WORK-PRCSS-ID TO #O-WORK-PRCSS-ID
            pnd_Output_Workfile_Pnd_O_Transaction_Type.setValue(pnd_Input_Workfile_Pnd_I_Transaction_Type);                                                               //Natural: MOVE #I-TRANSACTION-TYPE TO #O-TRANSACTION-TYPE
            pnd_Output_Workfile_Pnd_O_Clndr_Days_Aging.setValue(pnd_Input_Workfile_Pnd_I_Clndr_Days_Aging);                                                               //Natural: MOVE #I-CLNDR-DAYS-AGING TO #O-CLNDR-DAYS-AGING
            pnd_Output_Workfile_Pnd_O_Filler1.setValue(",");                                                                                                              //Natural: MOVE ',' TO #O-FILLER1 #O-FILLER2 #O-FILLER3 #O-FILLER4 #O-FILLER5 #O-FILLER6
            pnd_Output_Workfile_Pnd_O_Filler2.setValue(",");
            pnd_Output_Workfile_Pnd_O_Filler3.setValue(",");
            pnd_Output_Workfile_Pnd_O_Filler4.setValue(",");
            pnd_Output_Workfile_Pnd_O_Filler5.setValue(",");
            pnd_Output_Workfile_Pnd_O_Filler6.setValue(",");
            getWorkFiles().write(2, false, pnd_Output_Workfile);                                                                                                          //Natural: WRITE WORK FILE 2 #OUTPUT-WORKFILE
            pnd_Rec_Count.nadd(1);                                                                                                                                        //Natural: ASSIGN #REC-COUNT := #REC-COUNT + 1
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().print(0, "Number of Records written to the Report:",pnd_Rec_Count, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                //Natural: PRINT 'Number of Records written to the Report:' #REC-COUNT ( EM = Z,ZZZ,ZZ9 )
    }

    //
}
