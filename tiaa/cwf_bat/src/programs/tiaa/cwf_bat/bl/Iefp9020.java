/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:48:33 PM
**        * FROM NATURAL PROGRAM : Iefp9020
************************************************************
**        * FILE NAME            : Iefp9020.java
**        * CLASS NAME           : Iefp9020
**        * INSTANCE NAME        : Iefp9020
************************************************************
************************************************************************
* PROGRAM  : IEFP9020
* SYSTEM   : CRPCWF
* TITLE    : CONTROL MODULE TRANSACTION AUDIT SELECT
* GENERATED: MARCH 4TH, 1997 AT  10:00 AM
* FUNCTION : THIS PROGRAM SELECTS TRANSACTIONS LOGGED BY THE CONTROL
*            MODULE ON THE ICW-EFM-AUDIT FILE (204).  THE SELECTION
*            IS BASED ON A DATE IN THE CWF-SUPPORT-TBL.  WORK FILES ARE
*            CREATED FOR LATER SORTING, REPORTING, AND DELETING.
*         1. CONTROL MODULE TRANSACTIONS AUDIT LIST (EFSF9040,9041)
*         2. CONTROL MODULE STATISTICS SUMMARY (EFSF9030,9033,9034)
*         3. MINS DELETED (EFSF0050,0051)
*         4. RECORDS TO BE DELETED FROM THE ICW-EFM-AUDIT FILE
*
* HISTORY
* ---------------------------------------------
* 10/11/95 JHH - CHANGE FILE 190 TO ISN REUSE
*              - ELIMINATE DAILY REFRESH
*              - KEEP 'Last-Report-Date' ON CWF-SUPPORT-TBL
*              - REPORT FROM AFTER 'Last-Report-Date' TO YESTERDAY
*              - DELETE ANYTHING 7 DAYS OLDER THAN 'Last-Report-Date'
************************************************************************
* GLOBAL USING CWFG000

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iefp9020 extends BLNatBase
{
    // Data Areas
    private LdaIefl9020 ldaIefl9020;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_icw_Efm_Audit;
    private DbsField icw_Efm_Audit_Log_Dte_Tme;
    private DbsField icw_Efm_Audit_System_Cde;
    private DbsField icw_Efm_Audit_Action_Cde;
    private DbsField icw_Efm_Audit_Empl_Oprtr_Cde;
    private DbsField icw_Efm_Audit_Rqst_Prcss_Tme;
    private DbsField icw_Efm_Audit_Recursive_Ind;
    private DbsField icw_Efm_Audit_Error_Cde;
    private DbsField icw_Efm_Audit_Mit_Added_Cnt;
    private DbsField icw_Efm_Audit_Mit_Updated_Cnt;
    private DbsField icw_Efm_Audit_Dcmnt_Added_Cnt;
    private DbsField icw_Efm_Audit_Dcmnt_Renamed_Cnt;
    private DbsField icw_Efm_Audit_Cabinet_Id;
    private DbsGroup icw_Efm_Audit_Count_Castrqst_IdMuGroup;
    private DbsField icw_Efm_Audit_Count_Castrqst_Id;
    private DbsGroup icw_Efm_Audit_Rqst_IdMuGroup;
    private DbsField icw_Efm_Audit_Rqst_Id;
    private DbsField icw_Efm_Audit_Error_Msg;
    private DbsField icw_Efm_Audit_Image_Min;
    private DbsField icw_Efm_Audit_Image_Source_Id;

    private DbsGroup icw_Efm_Audit__R_Field_1;

    private DbsGroup icw_Efm_Audit_Pnd_Icw_Efm_Audit;
    private DbsField icw_Efm_Audit_Pnd_Icw_Efm_Audit_Part1;
    private DbsField icw_Efm_Audit_Pnd_Icw_Efm_Audit_Part2;

    private DataAccessProgramView vw_cwf_Tbl;
    private DbsField cwf_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Tbl__R_Field_2;
    private DbsField cwf_Tbl_Next;
    private DbsField cwf_Tbl_Last_Dte;
    private DbsField cwf_Tbl_Prev;
    private DbsField cwf_Tbl_Prev_Dte;
    private DbsField cwf_Tbl_Dash1;
    private DbsField cwf_Tbl_Days;
    private DbsField cwf_Tbl_Dash2;
    private DbsField cwf_Tbl_Run_Dte;
    private DbsField cwf_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Prime_Key;
    private DbsField pnd_Program;
    private DbsField pnd_Hi_Rpt_Dte_T;
    private DbsField pnd_Low_Rpt_Dte_T;
    private DbsField pnd_Del_Dte;
    private DbsField pnd_Del_Cnt;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Dte_From;
    private DbsField pnd_Dte_To;
    private DbsField pnd_Log_Dte;
    private DbsField pnd_Tbl_Isn;
    private DbsField pnd_Aud_Isn;
    private DbsField pnd_X;
    private DbsField pnd_Rpt1_Cnt;
    private DbsField pnd_Rpt2_Cnt;
    private DbsField pnd_Err2_Cnt;
    private DbsField pnd_Rpt3_Cnt;

    private DbsGroup pnd_Rpt3_Work;
    private DbsField pnd_Rpt3_Work_Image_Source_Id;
    private DbsField pnd_Rpt3_Work_Image_Min;
    private DbsField pnd_Rpt3_Work_Cabinet_Id;
    private DbsField pnd_Rpt3_Work_Rqst_Id;
    private DbsField pnd_Rpt3_Work_Empl_Oprtr_Cde;
    private DbsField pnd_Rpt3_Work_Error_Msg;
    private DbsField pnd_Rpt3_Work_Log_Dte_Tme;

    private DbsGroup pnd_Rpt4_Work;
    private DbsField pnd_Rpt4_Work_Isn;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIefl9020 = new LdaIefl9020();
        registerRecord(ldaIefl9020);

        // Local Variables
        localVariables = new DbsRecord();

        vw_icw_Efm_Audit = new DataAccessProgramView(new NameInfo("vw_icw_Efm_Audit", "ICW-EFM-AUDIT"), "ICW_EFM_AUDIT", "ICW_EFM_AUDIT");
        icw_Efm_Audit_Log_Dte_Tme = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Log_Dte_Tme", "LOG-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LOG_DTE_TME");
        icw_Efm_Audit_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        icw_Efm_Audit_System_Cde = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_System_Cde", "SYSTEM-CDE", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTEM_CDE");
        icw_Efm_Audit_System_Cde.setDdmHeader("SYSTEM");
        icw_Efm_Audit_Action_Cde = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Action_Cde", "ACTION-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTION_CDE");
        icw_Efm_Audit_Action_Cde.setDdmHeader("ACTION");
        icw_Efm_Audit_Empl_Oprtr_Cde = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        icw_Efm_Audit_Empl_Oprtr_Cde.setDdmHeader("OPERATOR/CODE");
        icw_Efm_Audit_Rqst_Prcss_Tme = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Rqst_Prcss_Tme", "RQST-PRCSS-TME", FieldType.NUMERIC, 
            5, RepeatingFieldStrategy.None, "RQST_PRCSS_TME");
        icw_Efm_Audit_Rqst_Prcss_Tme.setDdmHeader("PROCESS/TIME");
        icw_Efm_Audit_Recursive_Ind = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Recursive_Ind", "RECURSIVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RECURSIVE_IND");
        icw_Efm_Audit_Recursive_Ind.setDdmHeader("RECUR/IND");
        icw_Efm_Audit_Error_Cde = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Error_Cde", "ERROR-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "ERROR_CDE");
        icw_Efm_Audit_Error_Cde.setDdmHeader("ERR/CDE");
        icw_Efm_Audit_Mit_Added_Cnt = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Mit_Added_Cnt", "MIT-ADDED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MIT_ADDED_CNT");
        icw_Efm_Audit_Mit_Added_Cnt.setDdmHeader("NBR OF MIT/RECS ADDED");
        icw_Efm_Audit_Mit_Updated_Cnt = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Mit_Updated_Cnt", "MIT-UPDATED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MIT_UPDATED_CNT");
        icw_Efm_Audit_Mit_Updated_Cnt.setDdmHeader("NBR OF MIT/RECS UPDATED");
        icw_Efm_Audit_Dcmnt_Added_Cnt = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Dcmnt_Added_Cnt", "DCMNT-ADDED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DCMNT_ADDED_CNT");
        icw_Efm_Audit_Dcmnt_Added_Cnt.setDdmHeader("NBR OF DCMT/ADDED");
        icw_Efm_Audit_Dcmnt_Renamed_Cnt = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Dcmnt_Renamed_Cnt", "DCMNT-RENAMED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DCMNT_RENAMED_CNT");
        icw_Efm_Audit_Dcmnt_Renamed_Cnt.setDdmHeader("NBR OF DCMT/RENAMED");
        icw_Efm_Audit_Cabinet_Id = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Cabinet_Id", "CABINET-ID", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        icw_Efm_Audit_Cabinet_Id.setDdmHeader("PIN");
        icw_Efm_Audit_Count_Castrqst_Id = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Count_Castrqst_Id", "C*RQST-ID", RepeatingFieldStrategy.CAsteriskVariable, 
            "ICW_EFM_AUDIT_RQST_ID");
        icw_Efm_Audit_Rqst_IdMuGroup = vw_icw_Efm_Audit.getRecord().newGroupInGroup("ICW_EFM_AUDIT_RQST_IDMuGroup", "RQST_IDMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ICW_EFM_AUDIT_RQST_ID");
        icw_Efm_Audit_Rqst_Id = icw_Efm_Audit_Rqst_IdMuGroup.newFieldArrayInGroup("icw_Efm_Audit_Rqst_Id", "RQST-ID", FieldType.STRING, 22, new DbsArrayController(1, 
            10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RQST_ID");
        icw_Efm_Audit_Rqst_Id.setDdmHeader("RQST ID");
        icw_Efm_Audit_Error_Msg = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Error_Msg", "ERROR-MSG", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "ERROR_MSG");
        icw_Efm_Audit_Error_Msg.setDdmHeader("ERROR/MESSAGE");
        icw_Efm_Audit_Image_Min = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Image_Min", "IMAGE-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "IMAGE_MIN");
        icw_Efm_Audit_Image_Source_Id = vw_icw_Efm_Audit.getRecord().newFieldInGroup("icw_Efm_Audit_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");

        icw_Efm_Audit__R_Field_1 = localVariables.newGroupInRecord("icw_Efm_Audit__R_Field_1", "REDEFINE", vw_icw_Efm_Audit);

        icw_Efm_Audit_Pnd_Icw_Efm_Audit = icw_Efm_Audit__R_Field_1.newGroupInGroup("icw_Efm_Audit_Pnd_Icw_Efm_Audit", "#ICW-EFM-AUDIT");
        icw_Efm_Audit_Pnd_Icw_Efm_Audit_Part1 = icw_Efm_Audit_Pnd_Icw_Efm_Audit.newFieldInGroup("icw_Efm_Audit_Pnd_Icw_Efm_Audit_Part1", "#ICW-EFM-AUDIT-PART1", 
            FieldType.STRING, 185);
        icw_Efm_Audit_Pnd_Icw_Efm_Audit_Part2 = icw_Efm_Audit_Pnd_Icw_Efm_Audit.newFieldInGroup("icw_Efm_Audit_Pnd_Icw_Efm_Audit_Part2", "#ICW-EFM-AUDIT-PART2", 
            FieldType.STRING, 100);
        registerRecord(vw_icw_Efm_Audit);

        vw_cwf_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Tbl", "CWF-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Tbl_Tbl_Data_Field = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");

        cwf_Tbl__R_Field_2 = vw_cwf_Tbl.getRecord().newGroupInGroup("cwf_Tbl__R_Field_2", "REDEFINE", cwf_Tbl_Tbl_Data_Field);
        cwf_Tbl_Next = cwf_Tbl__R_Field_2.newFieldInGroup("cwf_Tbl_Next", "NEXT", FieldType.STRING, 5);
        cwf_Tbl_Last_Dte = cwf_Tbl__R_Field_2.newFieldInGroup("cwf_Tbl_Last_Dte", "LAST-DTE", FieldType.STRING, 8);
        cwf_Tbl_Prev = cwf_Tbl__R_Field_2.newFieldInGroup("cwf_Tbl_Prev", "PREV", FieldType.STRING, 7);
        cwf_Tbl_Prev_Dte = cwf_Tbl__R_Field_2.newFieldInGroup("cwf_Tbl_Prev_Dte", "PREV-DTE", FieldType.STRING, 8);
        cwf_Tbl_Dash1 = cwf_Tbl__R_Field_2.newFieldInGroup("cwf_Tbl_Dash1", "DASH1", FieldType.STRING, 3);
        cwf_Tbl_Days = cwf_Tbl__R_Field_2.newFieldInGroup("cwf_Tbl_Days", "DAYS", FieldType.NUMERIC, 2);
        cwf_Tbl_Dash2 = cwf_Tbl__R_Field_2.newFieldInGroup("cwf_Tbl_Dash2", "DASH2", FieldType.STRING, 3);
        cwf_Tbl_Run_Dte = cwf_Tbl__R_Field_2.newFieldInGroup("cwf_Tbl_Run_Dte", "RUN-DTE", FieldType.STRING, 8);
        cwf_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE_TME");
        cwf_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Tbl_Tbl_Updte_Dte = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Hi_Rpt_Dte_T = localVariables.newFieldInRecord("pnd_Hi_Rpt_Dte_T", "#HI-RPT-DTE-T", FieldType.TIME);
        pnd_Low_Rpt_Dte_T = localVariables.newFieldInRecord("pnd_Low_Rpt_Dte_T", "#LOW-RPT-DTE-T", FieldType.TIME);
        pnd_Del_Dte = localVariables.newFieldInRecord("pnd_Del_Dte", "#DEL-DTE", FieldType.DATE);
        pnd_Del_Cnt = localVariables.newFieldInRecord("pnd_Del_Cnt", "#DEL-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Dte_From = localVariables.newFieldInRecord("pnd_Dte_From", "#DTE-FROM", FieldType.DATE);
        pnd_Dte_To = localVariables.newFieldInRecord("pnd_Dte_To", "#DTE-TO", FieldType.DATE);
        pnd_Log_Dte = localVariables.newFieldInRecord("pnd_Log_Dte", "#LOG-DTE", FieldType.DATE);
        pnd_Tbl_Isn = localVariables.newFieldInRecord("pnd_Tbl_Isn", "#TBL-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Aud_Isn = localVariables.newFieldInRecord("pnd_Aud_Isn", "#AUD-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 5);
        pnd_Rpt1_Cnt = localVariables.newFieldInRecord("pnd_Rpt1_Cnt", "#RPT1-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Rpt2_Cnt = localVariables.newFieldInRecord("pnd_Rpt2_Cnt", "#RPT2-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Err2_Cnt = localVariables.newFieldInRecord("pnd_Err2_Cnt", "#ERR2-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Rpt3_Cnt = localVariables.newFieldInRecord("pnd_Rpt3_Cnt", "#RPT3-CNT", FieldType.PACKED_DECIMAL, 7);

        pnd_Rpt3_Work = localVariables.newGroupInRecord("pnd_Rpt3_Work", "#RPT3-WORK");
        pnd_Rpt3_Work_Image_Source_Id = pnd_Rpt3_Work.newFieldInGroup("pnd_Rpt3_Work_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 6);
        pnd_Rpt3_Work_Image_Min = pnd_Rpt3_Work.newFieldInGroup("pnd_Rpt3_Work_Image_Min", "IMAGE-MIN", FieldType.STRING, 11);
        pnd_Rpt3_Work_Cabinet_Id = pnd_Rpt3_Work.newFieldInGroup("pnd_Rpt3_Work_Cabinet_Id", "CABINET-ID", FieldType.STRING, 12);
        pnd_Rpt3_Work_Rqst_Id = pnd_Rpt3_Work.newFieldArrayInGroup("pnd_Rpt3_Work_Rqst_Id", "RQST-ID", FieldType.STRING, 22, new DbsArrayController(1, 
            10));
        pnd_Rpt3_Work_Empl_Oprtr_Cde = pnd_Rpt3_Work.newFieldInGroup("pnd_Rpt3_Work_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 8);
        pnd_Rpt3_Work_Error_Msg = pnd_Rpt3_Work.newFieldInGroup("pnd_Rpt3_Work_Error_Msg", "ERROR-MSG", FieldType.STRING, 30);
        pnd_Rpt3_Work_Log_Dte_Tme = pnd_Rpt3_Work.newFieldInGroup("pnd_Rpt3_Work_Log_Dte_Tme", "LOG-DTE-TME", FieldType.TIME);

        pnd_Rpt4_Work = localVariables.newGroupInRecord("pnd_Rpt4_Work", "#RPT4-WORK");
        pnd_Rpt4_Work_Isn = pnd_Rpt4_Work.newFieldArrayInGroup("pnd_Rpt4_Work_Isn", "ISN", FieldType.PACKED_DECIMAL, 8, new DbsArrayController(1, 1000));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_icw_Efm_Audit.reset();
        vw_cwf_Tbl.reset();

        ldaIefl9020.initializeValues();

        localVariables.reset();
        pnd_Tbl_Prime_Key.setInitialValue("A ICW-EFM-REPORTS     IEFP9020");
        pnd_Hi_Rpt_Dte_T.setInitialValue(0);
        pnd_Low_Rpt_Dte_T.setInitialValue(-2147483648);
        pnd_Del_Cnt.setInitialValue(0);
        pnd_Rec_Cnt.setInitialValue(0);
        pnd_X.setInitialValue(0);
        pnd_Rpt1_Cnt.setInitialValue(0);
        pnd_Rpt2_Cnt.setInitialValue(0);
        pnd_Err2_Cnt.setInitialValue(0);
        pnd_Rpt3_Cnt.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iefp9020() throws Exception
    {
        super("Iefp9020");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IEFP9020", onError);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*  ---------------------------------------------
        getReports().definePrinter(4, "ERR");                                                                                                                             //Natural: DEFINE PRINTER ( ERR = 3 )
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( ERR ) LS = 132 PS = 60 ZP = OFF SG = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  =============================================
        //*  FILE 183
        vw_cwf_Tbl.startDatabaseFind                                                                                                                                      //Natural: FIND ( 1 ) CWF-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "FIND_TBL",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        FIND_TBL:
        while (condition(vw_cwf_Tbl.readNextRow("FIND_TBL")))
        {
            vw_cwf_Tbl.setIfNotFoundControlFlag(false);
            pnd_Tbl_Isn.setValue(vw_cwf_Tbl.getAstISN("FIND_TBL"));                                                                                                       //Natural: MOVE *ISN TO #TBL-ISN
            pnd_Dte_From.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Tbl_Last_Dte);                                                                                 //Natural: MOVE EDITED CWF-TBL.LAST-DTE TO #DTE-FROM ( EM = YYYYMMDD )
            //*  TODAY's date
            pnd_Dte_To.setValue(Global.getDATX());                                                                                                                        //Natural: MOVE *DATX TO #DTE-TO
            //*  LAST FULL DAY (YESTERDAY)
            pnd_Dte_To.nsubtract(1);                                                                                                                                      //Natural: SUBTRACT 1 FROM #DTE-TO
            pnd_Del_Dte.compute(new ComputeParameters(false, pnd_Del_Dte), pnd_Dte_From.subtract(cwf_Tbl_Days));                                                          //Natural: COMPUTE #DEL-DTE = #DTE-FROM - CWF-TBL.DAYS
            if (condition(pnd_Dte_To.lessOrEqual(pnd_Dte_From)))                                                                                                          //Natural: IF #DTE-TO NOT > #DTE-FROM
            {
                getReports().write(0, ReportOption.NOTITLE,"P3520CWD Cancelled - yesterday,",pnd_Dte_To,"not later than last full day reported,",pnd_Dte_From);           //Natural: WRITE 'P3520CWD Cancelled - yesterday,' #DTE-TO 'not later than last full day reported,' #DTE-FROM
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_TBL"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_TBL"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(5);  if (true) return;                                                                                                                  //Natural: TERMINATE 05
            }                                                                                                                                                             //Natural: END-IF
            icw_Efm_Audit_Log_Dte_Tme.setValue(0);                                                                                                                        //Natural: ASSIGN ICW-EFM-AUDIT.LOG-DTE-TME = 0
            getWorkFiles().write(1, false, icw_Efm_Audit_Pnd_Icw_Efm_Audit);                                                                                              //Natural: WRITE WORK FILE 1 #ICW-EFM-AUDIT
            ldaIefl9020.getPnd_Work_Record_Log_Dte().setValue("00000000");                                                                                                //Natural: ASSIGN #WORK-RECORD.LOG-DTE = '00000000'
            getWorkFiles().write(2, false, ldaIefl9020.getPnd_Work_Record());                                                                                             //Natural: WRITE WORK FILE 2 #WORK-RECORD
            pnd_Rpt3_Work_Image_Source_Id.setValue("000000");                                                                                                             //Natural: ASSIGN #RPT3-WORK.IMAGE-SOURCE-ID = '000000'
            getWorkFiles().write(3, false, pnd_Rpt3_Work);                                                                                                                //Natural: WRITE WORK FILE 3 #RPT3-WORK
            //*  ------------------------------------------------------------------
            //*  FILE 204
            vw_icw_Efm_Audit.startDatabaseRead                                                                                                                            //Natural: READ ICW-EFM-AUDIT
            (
            "READ_AUD",
            new Oc[] { new Oc("ISN", "ASC") }
            );
            READ_AUD:
            while (condition(vw_icw_Efm_Audit.readNextRow("READ_AUD")))
            {
                pnd_Aud_Isn.setValue(vw_icw_Efm_Audit.getAstISN("READ_AUD"));                                                                                             //Natural: MOVE *ISN TO #AUD-ISN
                pnd_Rec_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-CNT
                if (condition(icw_Efm_Audit_Log_Dte_Tme.greater(pnd_Hi_Rpt_Dte_T)))                                                                                       //Natural: IF ICW-EFM-AUDIT.LOG-DTE-TME > #HI-RPT-DTE-T
                {
                    pnd_Hi_Rpt_Dte_T.setValue(icw_Efm_Audit_Log_Dte_Tme);                                                                                                 //Natural: MOVE ICW-EFM-AUDIT.LOG-DTE-TME TO #HI-RPT-DTE-T
                }                                                                                                                                                         //Natural: END-IF
                if (condition(icw_Efm_Audit_Log_Dte_Tme.less(pnd_Low_Rpt_Dte_T)))                                                                                         //Natural: IF ICW-EFM-AUDIT.LOG-DTE-TME < #LOW-RPT-DTE-T
                {
                    pnd_Low_Rpt_Dte_T.setValue(icw_Efm_Audit_Log_Dte_Tme);                                                                                                //Natural: MOVE ICW-EFM-AUDIT.LOG-DTE-TME TO #LOW-RPT-DTE-T
                }                                                                                                                                                         //Natural: END-IF
                pnd_Log_Dte.setValue(icw_Efm_Audit_Log_Dte_Tme);                                                                                                          //Natural: MOVE ICW-EFM-AUDIT.LOG-DTE-TME TO #LOG-DTE
                //* *    WRITE 'In ' ICW-EFM-AUDIT.LOG-DTE-TME (EM=YYYYMMDDHHIISST)
                //* *      ICW-EFM-AUDIT.CABINET-ID  'RECS =' #REC-CNT
                if (condition(pnd_Log_Dte.greater(pnd_Dte_From) && pnd_Log_Dte.lessOrEqual(pnd_Dte_To)))                                                                  //Natural: IF #LOG-DTE > #DTE-FROM AND #LOG-DTE NOT > #DTE-TO
                {
                    //*  =============================== REPORT 1 - CTL MODULE TRANS AUDIT LIST
                    pnd_Rpt1_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #RPT1-CNT
                    getWorkFiles().write(1, false, icw_Efm_Audit_Pnd_Icw_Efm_Audit);                                                                                      //Natural: WRITE WORK FILE 1 #ICW-EFM-AUDIT
                    //* *    WRITE 'Rpt1' ICW-EFM-AUDIT.LOG-DTE-TME (EM=YYYYMMDDHHIISST)
                    //* *      ICW-EFM-AUDIT.CABINET-ID  'RECS =' #REC-CNT
                    //*  =============================== REPORT 2 - CONTROL MODULE STAT SUMMARY
                    pnd_Rpt2_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #RPT2-CNT
                    if (condition(icw_Efm_Audit_Error_Cde.notEquals(getZero())))                                                                                          //Natural: IF ICW-EFM-AUDIT.ERROR-CDE NOT = 0
                    {
                        pnd_Err2_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #ERR2-CNT
                        getReports().write(4, ReportOption.NOHDR, writeMapToStringOutput(Ieff9032.class));                                                                //Natural: WRITE ( ERR ) NOHDR USING FORM 'IEFF9032'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIefl9020.getPnd_Work_Record_Pnd_Work_Record_Grp().reset();                                                                                     //Natural: RESET #WORK-RECORD-GRP
                        ldaIefl9020.getPnd_Work_Record_Pnd_Work_Record_Grp().setValuesByName(vw_icw_Efm_Audit);                                                           //Natural: MOVE BY NAME ICW-EFM-AUDIT TO #WORK-RECORD-GRP
                        ldaIefl9020.getPnd_Work_Record_Log_Dte().setValueEdited(pnd_Log_Dte,new ReportEditMask("YYYYMMDD"));                                              //Natural: MOVE EDITED #LOG-DTE ( EM = YYYYMMDD ) TO #WORK-RECORD.LOG-DTE
                        getWorkFiles().write(2, false, ldaIefl9020.getPnd_Work_Record());                                                                                 //Natural: WRITE WORK FILE 2 #WORK-RECORD
                        //* *      WRITE 'Rpt2' ICW-EFM-AUDIT.LOG-DTE-TME (EM=YYYYMMDDHHIISST)
                        //* *        ICW-EFM-AUDIT.CABINET-ID  'RECS =' #REC-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  =============================== REPORT 3 - MINS DELETED AS OF MM/DD/YY
                if (condition(icw_Efm_Audit_Action_Cde.equals("DM")))                                                                                                     //Natural: IF ICW-EFM-AUDIT.ACTION-CDE = 'DM'
                {
                    pnd_Rpt3_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #RPT3-CNT
                    pnd_Rpt3_Work.setValuesByName(vw_icw_Efm_Audit);                                                                                                      //Natural: MOVE BY NAME ICW-EFM-AUDIT TO #RPT3-WORK
                    getWorkFiles().write(3, false, pnd_Rpt3_Work);                                                                                                        //Natural: WRITE WORK FILE 3 #RPT3-WORK
                    //* *    WRITE 'Rpt3' ICW-EFM-AUDIT.LOG-DTE-TME (EM=YYYYMMDDHHIISST)
                    //* *      ICW-EFM-AUDIT.CABINET-ID  'RECS =' #REC-CNT
                }                                                                                                                                                         //Natural: END-IF
                //*  =============================== DELETE ROUTINE
                if (condition(pnd_Log_Dte.less(pnd_Del_Dte)))                                                                                                             //Natural: IF #LOG-DTE < #DEL-DTE
                {
                    //* *    WRITE 'Del' ICW-EFM-AUDIT.LOG-DTE-TME (EM=YYYYMMDDHHIISST)
                    //* *      ICW-EFM-AUDIT.CABINET-ID  'RECS =' #REC-CNT
                    pnd_Del_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #DEL-CNT
                    pnd_X.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #X
                    pnd_Rpt4_Work_Isn.getValue(pnd_X).setValue(pnd_Aud_Isn);                                                                                              //Natural: MOVE #AUD-ISN TO #RPT4-WORK.ISN ( #X )
                    if (condition(pnd_X.greater(999)))                                                                                                                    //Natural: IF #X > 999
                    {
                        getWorkFiles().write(4, false, pnd_Rpt4_Work);                                                                                                    //Natural: WRITE WORK FILE 4 #RPT4-WORK
                        pnd_X.setValue(0);                                                                                                                                //Natural: ASSIGN #X = 0
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_TBL"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_TBL"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ------------------------------------------------------------------
            pnd_X.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #X
            FOR01:                                                                                                                                                        //Natural: FOR #X = #X TO 1000
            for (pnd_X.setValue(pnd_X); condition(pnd_X.lessOrEqual(1000)); pnd_X.nadd(1))
            {
                pnd_Rpt4_Work_Isn.getValue(pnd_X).setValue(0);                                                                                                            //Natural: MOVE 00000000 TO #RPT4-WORK.ISN ( #X )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_TBL"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_TBL"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(4, false, pnd_Rpt4_Work);                                                                                                                //Natural: WRITE WORK FILE 4 #RPT4-WORK
            //*  =============================================
            GET_TBL:                                                                                                                                                      //Natural: GET CWF-TBL #TBL-ISN
            vw_cwf_Tbl.readByID(pnd_Tbl_Isn.getLong(), "GET_TBL");
            cwf_Tbl_Prev_Dte.setValue(cwf_Tbl_Last_Dte);                                                                                                                  //Natural: MOVE CWF-TBL.LAST-DTE TO CWF-TBL.PREV-DTE
            cwf_Tbl_Last_Dte.setValueEdited(pnd_Dte_To,new ReportEditMask("YYYYMMDD"));                                                                                   //Natural: MOVE EDITED #DTE-TO ( EM = YYYYMMDD ) TO CWF-TBL.LAST-DTE
            cwf_Tbl_Run_Dte.setValue(Global.getDATU());                                                                                                                   //Natural: MOVE *DATU TO CWF-TBL.RUN-DTE
            cwf_Tbl_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                         //Natural: MOVE *TIMX TO CWF-TBL.TBL-UPDTE-DTE-TME
            cwf_Tbl_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                             //Natural: MOVE *DATX TO CWF-TBL.TBL-UPDTE-DTE
            cwf_Tbl_Tbl_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                                  //Natural: MOVE *INIT-USER TO CWF-TBL.TBL-UPDTE-OPRTR-CDE
            vw_cwf_Tbl.updateDBRow("GET_TBL");                                                                                                                            //Natural: UPDATE ( GET-TBL. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  =============================================
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,pnd_Rec_Cnt,"Audit Records read");                                                                             //Natural: WRITE / #REC-CNT 'Audit Records read'
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,pnd_Del_Cnt,"Records prior to",pnd_Del_Dte,"deleted in IEFP9028");                                                     //Natural: WRITE #DEL-CNT 'Records prior to' #DEL-DTE 'deleted in IEFP9028'
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,pnd_Rpt1_Cnt,"Records after",pnd_Dte_From,"thru",pnd_Dte_To,"reported in IEFP9022");                                   //Natural: WRITE #RPT1-CNT 'Records after' #DTE-FROM 'thru' #DTE-TO 'reported in IEFP9022'
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,pnd_Rpt2_Cnt,"Records after",pnd_Dte_From,"thru",pnd_Dte_To,"summarized in IEFP9031 (less",pnd_Err2_Cnt,               //Natural: WRITE #RPT2-CNT 'Records after' #DTE-FROM 'thru' #DTE-TO 'summarized in IEFP9031 (less' #ERR2-CNT 'errors)'
            "errors)");
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,pnd_Rpt3_Cnt,"DM records on ICW-EFM-AUDIT");                                                                           //Natural: WRITE #RPT3-CNT 'DM records on ICW-EFM-AUDIT'
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Highest Log-Dte-Tme read was",pnd_Hi_Rpt_Dte_T, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS.T"));              //Natural: WRITE / 'Highest Log-Dte-Tme read was' #HI-RPT-DTE-T ( EM = MM/DD/YYYY' 'HH:II:SS.T )
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE," Lowest Log-Dte-Tme read was",pnd_Low_Rpt_Dte_T, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS.T"));                     //Natural: WRITE ' Lowest Log-Dte-Tme read was' #LOW-RPT-DTE-T ( EM = MM/DD/YYYY' 'HH:II:SS.T )
        if (Global.isEscape()) return;
        //*  -------------------------------
        //*  -------------------------------                                                                                                                              //Natural: AT TOP OF PAGE ( ERR )
        //*  -------------------------------                                                                                                                              //Natural: ON ERROR
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Ieff9031.class));                                              //Natural: WRITE ( ERR ) NOTITLE NOHDR USING FORM 'IEFF9031'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOTITLE,"NATURAL error",Global.getERROR_NR(),"in",Global.getPROGRAM(),"at line",Global.getERROR_LINE(),"READ CTR:",            //Natural: WRITE 'NATURAL error' *ERROR-NR 'in' *PROGRAM 'at line' *ERROR-LINE 'READ CTR:' #REC-CNT
            pnd_Rec_Cnt);
        DbsUtil.terminate(5);  if (true) return;                                                                                                                          //Natural: TERMINATE 05
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(4, "LS=132 PS=60 ZP=OFF SG=OFF");
    }
}
