/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:45:35 PM
**        * FROM NATURAL PROGRAM : Cwfb8610
************************************************************
**        * FILE NAME            : Cwfb8610.java
**        * CLASS NAME           : Cwfb8610
**        * INSTANCE NAME        : Cwfb8610
************************************************************
************************************************************************
*
* SECURITY CERTIFICATION REMEDIATION
*
* READ CWF-ORG-EMPL-TBL4 (045/184); CREATE WORK FILE OF INFORMATION
* FOR CWF AND EWS REPORTS.
*
* 2008, 06 - GH USING CWF TABLE FOR RACF CRITERIA.
*
* 11/2008  - CS INCREASED TABLE-VAR FROM 40 TO 150 BYTES (5 ID'S TO 30)
*            PREVIOUSLY, THE REPORT HAD A HARD-CODED MAX OF 5 RACF-IDS
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8610 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_emp_Tbl;
    private DbsField emp_Tbl_Empl_Unit_Cde;
    private DbsField emp_Tbl_Empl_Racf_Id;
    private DbsField emp_Tbl_Empl_Table_Updte_Authrty_Cde;

    private DbsGroup emp_Tbl_Empl_Scrty_Group;
    private DbsField emp_Tbl_Empl_Systm_Cde;

    private DbsGroup emp_Tbl__R_Field_1;
    private DbsField emp_Tbl_Empl_Systm_Cde_Prefix;
    private DbsField emp_Tbl_Empl_Scrty_Lvl_Ind;
    private DbsField emp_Tbl_Dlte_Dte_Tme;
    private DbsField emp_Tbl_Dlte_Oprtr_Cde;
    private DbsField emp_Tbl_Entry_Dte_Tme;
    private DbsField emp_Tbl_Entry_Oprtr_Cde;
    private DbsField emp_Tbl_Updte_Actn_Cde;
    private DbsField emp_Tbl_Updte_Dte;
    private DbsField emp_Tbl_Updte_Dte_Tme;
    private DbsField emp_Tbl_Updte_Oprtr_Cde;
    private DbsField emp_Tbl_Blltn_Board_Read_Dte;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Pnd_Wf_A;
    private DbsField pnd_Work_File_Pnd_Wf_B;

    private DbsGroup pnd_Work_File__R_Field_2;
    private DbsField pnd_Work_File_Pnd_Wf_System;
    private DbsField pnd_Work_File_Pnd_Wf_Action_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Empl_Racf;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Cde;
    private DbsField pnd_Work_File_Pnd_Wf_Action;
    private DbsField pnd_Work_File_Pnd_Wf_Admin_Racf;
    private DbsField pnd_Work_File_Pnd_Wf_Empl_System;
    private DbsField pnd_Work_File_Pnd_Wf_Empl_Scrty;

    private DbsGroup pnd_Work_File__R_Field_3;
    private DbsField pnd_Work_File__Filler1;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_End_Dte;
    private DbsField pnd_Start_Datd;
    private DbsField pnd_Today_Datd;
    private DbsField pnd_Start_Yyyymmdd;

    private DbsGroup pnd_Start_Yyyymmdd__R_Field_4;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Date;
    private DbsField pnd_Today_Yyyymmdd;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_O;
    private DbsField pnd_Found;
    private DbsField pnd_Dlte_Dte_Tme;

    private DbsGroup pnd_Dlte_Dte_Tme__R_Field_5;
    private DbsField pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte;
    private DbsField pnd_Entry_Dte_Tme;

    private DbsGroup pnd_Entry_Dte_Tme__R_Field_6;
    private DbsField pnd_Entry_Dte_Tme_Pnd_Entry_Dte;
    private DbsField pnd_Updte_Dte_Tme;

    private DbsGroup pnd_Updte_Dte_Tme__R_Field_7;
    private DbsField pnd_Updte_Dte_Tme_Pnd_Updte_Dte;
    private DbsField pnd_Process_Dte;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Action;
    private DbsField pnd_Cwf_Ct;
    private DbsField pnd_Ews_Ct;

    private DataAccessProgramView vw_cwf_Suptb;
    private DbsField cwf_Suptb_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Suptb_Tbl_Table_Nme;
    private DbsField cwf_Suptb_Tbl_Key_Field;
    private DbsField cwf_Suptb_Tbl_Data_Field;
    private DbsField cwf_Suptb_Tbl_Actve_Ind;
    private DbsField cwf_Suptb_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Suptb_Tbl_Updte_Dte;
    private DbsField cwf_Suptb_Tbl_Updte_Oprtr_Cde;

    private DbsGroup pnd_Table_Var;
    private DbsField pnd_Table_Var_Pnd_Cwf_Sec_Id;

    private DbsGroup pnd_Table_Var__R_Field_8;
    private DbsField pnd_Table_Var_Pnd_Cwf_Sec_Id_Array;
    private DbsField pnd_Table_Var_Pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Table_Var__R_Field_9;
    private DbsField pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Table_Var_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Table_Var_Pnd_Tbl_Key_Field;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_emp_Tbl = new DataAccessProgramView(new NameInfo("vw_emp_Tbl", "EMP-TBL"), "CWF_ORG_EMPL_TBL", "CWF_ASSIGN_RULE", DdmPeriodicGroups.getInstance().getGroups("CWF_ORG_EMPL_TBL"));
        emp_Tbl_Empl_Unit_Cde = vw_emp_Tbl.getRecord().newFieldInGroup("emp_Tbl_Empl_Unit_Cde", "EMPL-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "EMPL_UNIT_CDE");
        emp_Tbl_Empl_Unit_Cde.setDdmHeader("UNIT/CODE");
        emp_Tbl_Empl_Racf_Id = vw_emp_Tbl.getRecord().newFieldInGroup("emp_Tbl_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "EMPL_RACF_ID");
        emp_Tbl_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF  ID");
        emp_Tbl_Empl_Table_Updte_Authrty_Cde = vw_emp_Tbl.getRecord().newFieldInGroup("emp_Tbl_Empl_Table_Updte_Authrty_Cde", "EMPL-TABLE-UPDTE-AUTHRTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "EMPL_TABLE_UPDTE_AUTHRTY_CDE");
        emp_Tbl_Empl_Table_Updte_Authrty_Cde.setDdmHeader("TABLE UPDATE/AUTHORITY");

        emp_Tbl_Empl_Scrty_Group = vw_emp_Tbl.getRecord().newGroupArrayInGroup("emp_Tbl_Empl_Scrty_Group", "EMPL-SCRTY-GROUP", new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_ASSIGN_RULE_EMPL_SCRTY_GROUP");
        emp_Tbl_Empl_Systm_Cde = emp_Tbl_Empl_Scrty_Group.newFieldInGroup("emp_Tbl_Empl_Systm_Cde", "EMPL-SYSTM-CDE", FieldType.STRING, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "EMPL_SYSTM_CDE", "CWF_ASSIGN_RULE_EMPL_SCRTY_GROUP");
        emp_Tbl_Empl_Systm_Cde.setDdmHeader("SYSTEM/APPLICATION");

        emp_Tbl__R_Field_1 = emp_Tbl_Empl_Scrty_Group.newGroupInGroup("emp_Tbl__R_Field_1", "REDEFINE", emp_Tbl_Empl_Systm_Cde);
        emp_Tbl_Empl_Systm_Cde_Prefix = emp_Tbl__R_Field_1.newFieldInGroup("emp_Tbl_Empl_Systm_Cde_Prefix", "EMPL-SYSTM-CDE-PREFIX", FieldType.STRING, 
            4);
        emp_Tbl_Empl_Scrty_Lvl_Ind = emp_Tbl_Empl_Scrty_Group.newFieldInGroup("emp_Tbl_Empl_Scrty_Lvl_Ind", "EMPL-SCRTY-LVL-IND", FieldType.STRING, 2, 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "EMPL_SCRTY_LVL_IND", "CWF_ASSIGN_RULE_EMPL_SCRTY_GROUP");
        emp_Tbl_Empl_Scrty_Lvl_Ind.setDdmHeader("SECURITY/LEVEL");
        emp_Tbl_Dlte_Dte_Tme = vw_emp_Tbl.getRecord().newFieldInGroup("emp_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "DLTE_DTE_TME");
        emp_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        emp_Tbl_Dlte_Oprtr_Cde = vw_emp_Tbl.getRecord().newFieldInGroup("emp_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DLTE_OPRTR_CDE");
        emp_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPERATOR");
        emp_Tbl_Entry_Dte_Tme = vw_emp_Tbl.getRecord().newFieldInGroup("emp_Tbl_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ENTRY_DTE_TME");
        emp_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        emp_Tbl_Entry_Oprtr_Cde = vw_emp_Tbl.getRecord().newFieldInGroup("emp_Tbl_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ENTRY_OPRTR_CDE");
        emp_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        emp_Tbl_Updte_Actn_Cde = vw_emp_Tbl.getRecord().newFieldInGroup("emp_Tbl_Updte_Actn_Cde", "UPDTE-ACTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "UPDTE_ACTN_CDE");
        emp_Tbl_Updte_Actn_Cde.setDdmHeader("UPDT/ACTN");
        emp_Tbl_Updte_Dte = vw_emp_Tbl.getRecord().newFieldInGroup("emp_Tbl_Updte_Dte", "UPDTE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "UPDTE_DTE");
        emp_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        emp_Tbl_Updte_Dte_Tme = vw_emp_Tbl.getRecord().newFieldInGroup("emp_Tbl_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "UPDTE_DTE_TME");
        emp_Tbl_Updte_Oprtr_Cde = vw_emp_Tbl.getRecord().newFieldInGroup("emp_Tbl_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UPDTE_OPRTR_CDE");
        emp_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        emp_Tbl_Blltn_Board_Read_Dte = vw_emp_Tbl.getRecord().newFieldInGroup("emp_Tbl_Blltn_Board_Read_Dte", "BLLTN-BOARD-READ-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "BLLTN_BOARD_READ_DTE");
        emp_Tbl_Blltn_Board_Read_Dte.setDdmHeader("BULLETIN/READ-DTE");
        registerRecord(vw_emp_Tbl);

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Pnd_Wf_A = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_A", "#WF-A", FieldType.STRING, 240);
        pnd_Work_File_Pnd_Wf_B = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_B", "#WF-B", FieldType.STRING, 60);

        pnd_Work_File__R_Field_2 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_2", "REDEFINE", pnd_Work_File);
        pnd_Work_File_Pnd_Wf_System = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_System", "#WF-SYSTEM", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_Action_Dte = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_Action_Dte", "#WF-ACTION-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Empl_Racf = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_Empl_Racf", "#WF-EMPL-RACF", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_Unit_Cde = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Cde", "#WF-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Action = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_Action", "#WF-ACTION", FieldType.STRING, 5);
        pnd_Work_File_Pnd_Wf_Admin_Racf = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_Admin_Racf", "#WF-ADMIN-RACF", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_Empl_System = pnd_Work_File__R_Field_2.newFieldArrayInGroup("pnd_Work_File_Pnd_Wf_Empl_System", "#WF-EMPL-SYSTEM", FieldType.STRING, 
            8, new DbsArrayController(1, 20));
        pnd_Work_File_Pnd_Wf_Empl_Scrty = pnd_Work_File__R_Field_2.newFieldArrayInGroup("pnd_Work_File_Pnd_Wf_Empl_Scrty", "#WF-EMPL-SCRTY", FieldType.STRING, 
            2, new DbsArrayController(1, 20));

        pnd_Work_File__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_3", "REDEFINE", pnd_Work_File);
        pnd_Work_File__Filler1 = pnd_Work_File__R_Field_3.newFieldInGroup("pnd_Work_File__Filler1", "_FILLER1", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_Start_Dte = pnd_Work_File__R_Field_3.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Dte", "#WF-START-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_End_Dte = pnd_Work_File__R_Field_3.newFieldInGroup("pnd_Work_File_Pnd_Wf_End_Dte", "#WF-END-DTE", FieldType.NUMERIC, 8);
        pnd_Start_Datd = localVariables.newFieldInRecord("pnd_Start_Datd", "#START-DATD", FieldType.DATE);
        pnd_Today_Datd = localVariables.newFieldInRecord("pnd_Today_Datd", "#TODAY-DATD", FieldType.DATE);
        pnd_Start_Yyyymmdd = localVariables.newFieldInRecord("pnd_Start_Yyyymmdd", "#START-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Start_Yyyymmdd__R_Field_4 = localVariables.newGroupInRecord("pnd_Start_Yyyymmdd__R_Field_4", "REDEFINE", pnd_Start_Yyyymmdd);
        pnd_Start_Yyyymmdd_Pnd_Start_Date = pnd_Start_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 
            8);
        pnd_Today_Yyyymmdd = localVariables.newFieldInRecord("pnd_Today_Yyyymmdd", "#TODAY-YYYYMMDD", FieldType.NUMERIC, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 7);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 7);
        pnd_O = localVariables.newFieldInRecord("pnd_O", "#O", FieldType.NUMERIC, 7);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.STRING, 1);
        pnd_Dlte_Dte_Tme = localVariables.newFieldInRecord("pnd_Dlte_Dte_Tme", "#DLTE-DTE-TME", FieldType.STRING, 15);

        pnd_Dlte_Dte_Tme__R_Field_5 = localVariables.newGroupInRecord("pnd_Dlte_Dte_Tme__R_Field_5", "REDEFINE", pnd_Dlte_Dte_Tme);
        pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte = pnd_Dlte_Dte_Tme__R_Field_5.newFieldInGroup("pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte", "#DLTE-DTE", FieldType.NUMERIC, 8);
        pnd_Entry_Dte_Tme = localVariables.newFieldInRecord("pnd_Entry_Dte_Tme", "#ENTRY-DTE-TME", FieldType.STRING, 15);

        pnd_Entry_Dte_Tme__R_Field_6 = localVariables.newGroupInRecord("pnd_Entry_Dte_Tme__R_Field_6", "REDEFINE", pnd_Entry_Dte_Tme);
        pnd_Entry_Dte_Tme_Pnd_Entry_Dte = pnd_Entry_Dte_Tme__R_Field_6.newFieldInGroup("pnd_Entry_Dte_Tme_Pnd_Entry_Dte", "#ENTRY-DTE", FieldType.NUMERIC, 
            8);
        pnd_Updte_Dte_Tme = localVariables.newFieldInRecord("pnd_Updte_Dte_Tme", "#UPDTE-DTE-TME", FieldType.STRING, 15);

        pnd_Updte_Dte_Tme__R_Field_7 = localVariables.newGroupInRecord("pnd_Updte_Dte_Tme__R_Field_7", "REDEFINE", pnd_Updte_Dte_Tme);
        pnd_Updte_Dte_Tme_Pnd_Updte_Dte = pnd_Updte_Dte_Tme__R_Field_7.newFieldInGroup("pnd_Updte_Dte_Tme_Pnd_Updte_Dte", "#UPDTE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Process_Dte = localVariables.newFieldInRecord("pnd_Process_Dte", "#PROCESS-DTE", FieldType.NUMERIC, 8);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Action = localVariables.newFieldInRecord("pnd_Action", "#ACTION", FieldType.STRING, 5);
        pnd_Cwf_Ct = localVariables.newFieldInRecord("pnd_Cwf_Ct", "#CWF-CT", FieldType.NUMERIC, 7);
        pnd_Ews_Ct = localVariables.newFieldInRecord("pnd_Ews_Ct", "#EWS-CT", FieldType.NUMERIC, 7);

        vw_cwf_Suptb = new DataAccessProgramView(new NameInfo("vw_cwf_Suptb", "CWF-SUPTB"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Suptb_Tbl_Scrty_Level_Ind = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Suptb_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Suptb_Tbl_Table_Nme = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TBL_TABLE_NME");
        cwf_Suptb_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Suptb_Tbl_Key_Field = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "TBL_KEY_FIELD");
        cwf_Suptb_Tbl_Data_Field = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");
        cwf_Suptb_Tbl_Actve_Ind = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TBL_ACTVE_IND");
        cwf_Suptb_Tbl_Updte_Dte_Tme = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE_TME");
        cwf_Suptb_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Suptb_Tbl_Updte_Dte = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Suptb_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Suptb_Tbl_Updte_Oprtr_Cde = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Suptb_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Suptb);

        pnd_Table_Var = localVariables.newGroupInRecord("pnd_Table_Var", "#TABLE-VAR");
        pnd_Table_Var_Pnd_Cwf_Sec_Id = pnd_Table_Var.newFieldInGroup("pnd_Table_Var_Pnd_Cwf_Sec_Id", "#CWF-SEC-ID", FieldType.STRING, 240);

        pnd_Table_Var__R_Field_8 = pnd_Table_Var.newGroupInGroup("pnd_Table_Var__R_Field_8", "REDEFINE", pnd_Table_Var_Pnd_Cwf_Sec_Id);
        pnd_Table_Var_Pnd_Cwf_Sec_Id_Array = pnd_Table_Var__R_Field_8.newFieldArrayInGroup("pnd_Table_Var_Pnd_Cwf_Sec_Id_Array", "#CWF-SEC-ID-ARRAY", 
            FieldType.STRING, 8, new DbsArrayController(1, 30));
        pnd_Table_Var_Pnd_Tbl_Prime_Key = pnd_Table_Var.newFieldInGroup("pnd_Table_Var_Pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Table_Var__R_Field_9 = pnd_Table_Var.newGroupInGroup("pnd_Table_Var__R_Field_9", "REDEFINE", pnd_Table_Var_Pnd_Tbl_Prime_Key);
        pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind = pnd_Table_Var__R_Field_9.newFieldInGroup("pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Table_Var_Pnd_Tbl_Table_Nme = pnd_Table_Var__R_Field_9.newFieldInGroup("pnd_Table_Var_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Table_Var_Pnd_Tbl_Key_Field = pnd_Table_Var__R_Field_9.newFieldInGroup("pnd_Table_Var_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_emp_Tbl.reset();
        vw_cwf_Suptb.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8610() throws Exception
    {
        super("Cwfb8610");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* *#TODAY-DATD     := *DATX
        //*  RUN EVERY WEEK
        pnd_Start_Datd.compute(new ComputeParameters(false, pnd_Start_Datd), Global.getDATX().subtract(7));                                                               //Natural: ASSIGN #START-DATD := *DATX - 7
        pnd_Today_Yyyymmdd.setValue(Global.getDATN());                                                                                                                    //Natural: ASSIGN #TODAY-YYYYMMDD := *DATN
        pnd_Start_Yyyymmdd_Pnd_Start_Date.setValueEdited(pnd_Start_Datd,new ReportEditMask("YYYYMMDD"));                                                                  //Natural: MOVE EDITED #START-DATD ( EM = YYYYMMDD ) TO #START-DATE
        //*  RETRIEVE THE CWF SEC IDS    /* GH 2008, JUNE
        pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind.setValue("S ");                                                                                                             //Natural: MOVE 'S ' TO #TBL-SCRTY-LEVEL-IND
        pnd_Table_Var_Pnd_Tbl_Table_Nme.setValue("CWF-EWS-SEC-CERTIFY");                                                                                                  //Natural: MOVE 'CWF-EWS-SEC-CERTIFY' TO #TBL-TABLE-NME
        pnd_Table_Var_Pnd_Tbl_Key_Field.setValue("CWF PRODUCTION SUPPORT ID");                                                                                            //Natural: MOVE 'CWF PRODUCTION SUPPORT ID' TO #TBL-KEY-FIELD
        vw_cwf_Suptb.startDatabaseRead                                                                                                                                    //Natural: READ ( 1 ) CWF-SUPTB WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Table_Var_Pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cwf_Suptb.readNextRow("READ01")))
        {
            if (condition(cwf_Suptb_Tbl_Scrty_Level_Ind.notEquals(pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind) || cwf_Suptb_Tbl_Table_Nme.notEquals(pnd_Table_Var_Pnd_Tbl_Table_Nme)  //Natural: IF TBL-SCRTY-LEVEL-IND NE #TBL-SCRTY-LEVEL-IND OR TBL-TABLE-NME NE #TBL-TABLE-NME OR TBL-KEY-FIELD NE #TBL-KEY-FIELD
                || cwf_Suptb_Tbl_Key_Field.notEquals(pnd_Table_Var_Pnd_Tbl_Key_Field)))
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Table_Var_Pnd_Cwf_Sec_Id.setValue(cwf_Suptb_Tbl_Data_Field);                                                                                              //Natural: MOVE TBL-DATA-FIELD TO #CWF-SEC-ID
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Start_Yyyymmdd,"=",pnd_Today_Yyyymmdd);                                                                                             //Natural: WRITE '=' #START-YYYYMMDD '=' #TODAY-YYYYMMDD
        if (Global.isEscape()) return;
        pnd_Work_File_Pnd_Wf_System.setValue("   ");                                                                                                                      //Natural: ASSIGN #WF-SYSTEM := '   '
        pnd_Work_File_Pnd_Wf_Start_Dte.setValue(pnd_Start_Yyyymmdd);                                                                                                      //Natural: ASSIGN #WF-START-DTE := #START-YYYYMMDD
        pnd_Work_File_Pnd_Wf_End_Dte.setValue(pnd_Today_Yyyymmdd);                                                                                                        //Natural: ASSIGN #WF-END-DTE := #TODAY-YYYYMMDD
        getWorkFiles().write(1, false, pnd_Work_File);                                                                                                                    //Natural: WRITE WORK FILE 1 #WORK-FILE
        vw_emp_Tbl.startDatabaseRead                                                                                                                                      //Natural: READ EMP-TBL
        (
        "READ02",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        READ02:
        while (condition(vw_emp_Tbl.readNextRow("READ02")))
        {
            pnd_Dlte_Dte_Tme.setValueEdited(emp_Tbl_Dlte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                                  //Natural: MOVE EDITED DLTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #DLTE-DTE-TME
            pnd_Entry_Dte_Tme.setValueEdited(emp_Tbl_Entry_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                                //Natural: MOVE EDITED ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #ENTRY-DTE-TME
            pnd_Updte_Dte_Tme.setValueEdited(emp_Tbl_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                                //Natural: MOVE EDITED UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #UPDTE-DTE-TME
            pnd_Action.reset();                                                                                                                                           //Natural: RESET #ACTION
            short decideConditionsMet163 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #ENTRY-DTE GE #START-YYYYMMDD AND #ENTRY-DTE LE #TODAY-YYYYMMDD
            if (condition(pnd_Entry_Dte_Tme_Pnd_Entry_Dte.greaterOrEqual(pnd_Start_Yyyymmdd) && pnd_Entry_Dte_Tme_Pnd_Entry_Dte.lessOrEqual(pnd_Today_Yyyymmdd)))
            {
                decideConditionsMet163++;
                pnd_Process_Dte.setValue(pnd_Entry_Dte_Tme_Pnd_Entry_Dte);                                                                                                //Natural: ASSIGN #PROCESS-DTE := #ENTRY-DTE
                pnd_Racf_Id.setValue(emp_Tbl_Entry_Oprtr_Cde);                                                                                                            //Natural: ASSIGN #RACF-ID := ENTRY-OPRTR-CDE
                pnd_Action.setValue("Add");                                                                                                                               //Natural: ASSIGN #ACTION := 'Add'
            }                                                                                                                                                             //Natural: WHEN #DLTE-DTE GE #START-YYYYMMDD AND #DLTE-DTE LE #TODAY-YYYYMMDD
            if (condition(pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte.greaterOrEqual(pnd_Start_Yyyymmdd) && pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte.lessOrEqual(pnd_Today_Yyyymmdd)))
            {
                decideConditionsMet163++;
                pnd_Process_Dte.setValue(pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte);                                                                                                  //Natural: ASSIGN #PROCESS-DTE := #DLTE-DTE
                pnd_Racf_Id.setValue(emp_Tbl_Dlte_Oprtr_Cde);                                                                                                             //Natural: ASSIGN #RACF-ID := DLTE-OPRTR-CDE
                pnd_Action.setValue("Dlete");                                                                                                                             //Natural: ASSIGN #ACTION := 'Dlete'
            }                                                                                                                                                             //Natural: WHEN #UPDTE-DTE GE #START-YYYYMMDD AND #UPDTE-DTE LE #TODAY-YYYYMMDD
            if (condition(pnd_Updte_Dte_Tme_Pnd_Updte_Dte.greaterOrEqual(pnd_Start_Yyyymmdd) && pnd_Updte_Dte_Tme_Pnd_Updte_Dte.lessOrEqual(pnd_Today_Yyyymmdd)))
            {
                decideConditionsMet163++;
                pnd_Process_Dte.setValue(pnd_Updte_Dte_Tme_Pnd_Updte_Dte);                                                                                                //Natural: ASSIGN #PROCESS-DTE := #UPDTE-DTE
                pnd_Racf_Id.setValue(emp_Tbl_Updte_Oprtr_Cde);                                                                                                            //Natural: ASSIGN #RACF-ID := UPDTE-OPRTR-CDE
                pnd_Action.setValue("Updte");                                                                                                                             //Natural: ASSIGN #ACTION := 'Updte'
                //*  BOTH FILLED WHEN REC ADDED
                if (condition(pnd_Updte_Dte_Tme.equals(pnd_Entry_Dte_Tme)))                                                                                               //Natural: IF #UPDTE-DTE-TME = #ENTRY-DTE-TME
                {
                    pnd_Process_Dte.setValue(pnd_Entry_Dte_Tme_Pnd_Entry_Dte);                                                                                            //Natural: ASSIGN #PROCESS-DTE := #ENTRY-DTE
                    pnd_Racf_Id.setValue(emp_Tbl_Entry_Oprtr_Cde);                                                                                                        //Natural: ASSIGN #RACF-ID := ENTRY-OPRTR-CDE
                    pnd_Action.setValue("Add");                                                                                                                           //Natural: ASSIGN #ACTION := 'Add'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  BOTH FILLED WHEN REC DLTED
                    if (condition(pnd_Updte_Dte_Tme.equals(pnd_Dlte_Dte_Tme)))                                                                                            //Natural: IF #UPDTE-DTE-TME = #DLTE-DTE-TME
                    {
                        pnd_Process_Dte.setValue(pnd_Dlte_Dte_Tme_Pnd_Dlte_Dte);                                                                                          //Natural: ASSIGN #PROCESS-DTE := #DLTE-DTE
                        pnd_Racf_Id.setValue(emp_Tbl_Dlte_Oprtr_Cde);                                                                                                     //Natural: ASSIGN #RACF-ID := DLTE-OPRTR-CDE
                        pnd_Action.setValue("Dlete");                                                                                                                     //Natural: ASSIGN #ACTION := 'Dlete'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet163 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Found.setValue(" ");                                                                                                                                      //Natural: MOVE ' ' TO #FOUND
                                                                                                                                                                          //Natural: PERFORM CHECK-RACF-ID
            sub_Check_Racf_Id();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  IF #ACTION  =  ' ' OR
            //*      NOT (#RACF-ID = #CWF-SEC-ID-ARRAY(1:5) )
            //*    ESCAPE TOP
            //*  END-IF
            //*  NO MATCH ON RACF-ID
            if (condition(pnd_Found.equals(" ")))                                                                                                                         //Natural: IF #FOUND = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //* ***** TEST
            getReports().write(0, "=",pnd_Dlte_Dte_Tme, new ReportEditMask ("XXXXXXXXXXXX"),emp_Tbl_Dlte_Oprtr_Cde,NEWLINE,"=",pnd_Entry_Dte_Tme, new                     //Natural: WRITE '=' #DLTE-DTE-TME ( EM = X ( 12 ) ) DLTE-OPRTR-CDE / '=' #ENTRY-DTE-TME ( EM = X ( 12 ) ) ENTRY-OPRTR-CDE / '=' #UPDTE-DTE-TME ( EM = X ( 12 ) ) UPDTE-OPRTR-CDE
                ReportEditMask ("XXXXXXXXXXXX"),emp_Tbl_Entry_Oprtr_Cde,NEWLINE,"=",pnd_Updte_Dte_Tme, new ReportEditMask ("XXXXXXXXXXXX"),emp_Tbl_Updte_Oprtr_Cde);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Work_File_Pnd_Wf_A.setValue(" ");                                                                                                                         //Natural: ASSIGN #WF-A := ' '
            pnd_Work_File_Pnd_Wf_B.setValue(" ");                                                                                                                         //Natural: ASSIGN #WF-B := ' '
            pnd_Work_File_Pnd_Wf_System.setValue("CWF");                                                                                                                  //Natural: ASSIGN #WF-SYSTEM := 'CWF'
            pnd_Work_File_Pnd_Wf_Action_Dte.setValue(pnd_Process_Dte);                                                                                                    //Natural: ASSIGN #WF-ACTION-DTE := #PROCESS-DTE
            pnd_Work_File_Pnd_Wf_Empl_Racf.setValue(emp_Tbl_Empl_Racf_Id);                                                                                                //Natural: ASSIGN #WF-EMPL-RACF := EMPL-RACF-ID
            pnd_Work_File_Pnd_Wf_Unit_Cde.setValue(emp_Tbl_Empl_Unit_Cde);                                                                                                //Natural: ASSIGN #WF-UNIT-CDE := EMPL-UNIT-CDE
            pnd_Work_File_Pnd_Wf_Action.setValue(pnd_Action);                                                                                                             //Natural: ASSIGN #WF-ACTION := #ACTION
            pnd_Work_File_Pnd_Wf_Admin_Racf.setValue(pnd_Racf_Id);                                                                                                        //Natural: ASSIGN #WF-ADMIN-RACF := #RACF-ID
            getWorkFiles().write(1, false, pnd_Work_File);                                                                                                                //Natural: WRITE WORK FILE 1 #WORK-FILE
            //*   DISPLAY '=' #WF-SYSTEM
            //*   DISPLAY '=' #WF-ACTION-DTE
            //*   DISPLAY '=' #WF-EMPL-RACF
            //*   DISPLAY '=' #WF-UNIT-CDE
            //*   DISPLAY '=' #WF-ACTION
            //*   DISPLAY '=' #WF-ADMIN-RACF
            pnd_Cwf_Ct.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #CWF-CT
            pnd_Work_File_Pnd_Wf_A.setValue(" ");                                                                                                                         //Natural: ASSIGN #WF-A := ' '
            pnd_Work_File_Pnd_Wf_B.setValue(" ");                                                                                                                         //Natural: ASSIGN #WF-B := ' '
            pnd_O.setValue(0);                                                                                                                                            //Natural: ASSIGN #O := 0
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(emp_Tbl_Empl_Systm_Cde.getValue(pnd_I).equals(" ")))                                                                                        //Natural: IF EMPL-SYSTM-CDE ( #I ) = ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(DbsUtil.maskMatches(emp_Tbl_Empl_Systm_Cde.getValue(pnd_I),"'IWKS'....")))                                                                  //Natural: IF EMPL-SYSTM-CDE ( #I ) = MASK ( 'IWKS'.... )
                {
                    pnd_O.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #O
                    pnd_Work_File_Pnd_Wf_Empl_System.getValue(pnd_O).setValue(emp_Tbl_Empl_Systm_Cde.getValue(pnd_I));                                                    //Natural: ASSIGN #WF-EMPL-SYSTEM ( #O ) := EMPL-SYSTM-CDE ( #I )
                    pnd_Work_File_Pnd_Wf_Empl_Scrty.getValue(pnd_O).setValue(emp_Tbl_Empl_Scrty_Lvl_Ind.getValue(pnd_I));                                                 //Natural: ASSIGN #WF-EMPL-SCRTY ( #O ) := EMPL-SCRTY-LVL-IND ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_O.greater(getZero())))                                                                                                                      //Natural: IF #O > 0
            {
                pnd_Work_File_Pnd_Wf_System.setValue("EWS");                                                                                                              //Natural: ASSIGN #WF-SYSTEM := 'EWS'
                pnd_Work_File_Pnd_Wf_Action_Dte.setValue(pnd_Process_Dte);                                                                                                //Natural: ASSIGN #WF-ACTION-DTE := #PROCESS-DTE
                pnd_Work_File_Pnd_Wf_Empl_Racf.setValue(emp_Tbl_Empl_Racf_Id);                                                                                            //Natural: ASSIGN #WF-EMPL-RACF := EMPL-RACF-ID
                pnd_Work_File_Pnd_Wf_Unit_Cde.setValue(emp_Tbl_Empl_Unit_Cde);                                                                                            //Natural: ASSIGN #WF-UNIT-CDE := EMPL-UNIT-CDE
                pnd_Work_File_Pnd_Wf_Action.setValue(pnd_Action);                                                                                                         //Natural: ASSIGN #WF-ACTION := #ACTION
                pnd_Work_File_Pnd_Wf_Admin_Racf.setValue(pnd_Racf_Id);                                                                                                    //Natural: ASSIGN #WF-ADMIN-RACF := #RACF-ID
                getWorkFiles().write(1, false, pnd_Work_File);                                                                                                            //Natural: WRITE WORK FILE 1 #WORK-FILE
                pnd_Ews_Ct.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #EWS-CT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,NEWLINE,"=",pnd_Cwf_Ct, new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,"=",pnd_Ews_Ct, new ReportEditMask                  //Natural: WRITE /// / '=' #CWF-CT ( EM = ZZZZ,ZZ9 ) / '=' #EWS-CT ( EM = ZZZZ,ZZ9 )
            ("ZZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //* *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RACF-ID
        //* *****************************************************
    }
    //*  CS 11/08
    private void sub_Check_Racf_Id() throws Exception                                                                                                                     //Natural: CHECK-RACF-ID
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #J 1 30
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(30)); pnd_J.nadd(1))
        {
            //*  READ NEXT REC
            if (condition(pnd_Action.equals(" ") || ! (pnd_Racf_Id.equals(pnd_Table_Var_Pnd_Cwf_Sec_Id_Array.getValue(pnd_J)))))                                          //Natural: IF #ACTION = ' ' OR NOT ( #RACF-ID = #CWF-SEC-ID-ARRAY ( #J ) )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Found.setValue("Y");                                                                                                                                  //Natural: MOVE 'Y' TO #FOUND
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //
}
