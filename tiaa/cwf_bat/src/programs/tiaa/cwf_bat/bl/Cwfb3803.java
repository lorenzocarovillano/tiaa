/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:30:21 PM
**        * FROM NATURAL PROGRAM : Cwfb3803
************************************************************
**        * FILE NAME            : Cwfb3803.java
**        * CLASS NAME           : Cwfb3803
**        * INSTANCE NAME        : Cwfb3803
************************************************************
************************************************************************
* PROGRAM  : CWFP3803
* SYSTEM   : CRPCWF
* TITLE    : REPORT 10
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF PARTICIPANT-CLOSED CASES - PREMIUM DIVISION
*          | MONTHLY
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3803 extends BLNatBase
{
    // Data Areas
    private LdaCwfl3802 ldaCwfl3802;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_Day;

    private DbsGroup pnd_Work_Day__R_Field_1;
    private DbsField pnd_Work_Day_Pnd_Work_Day_N;
    private DbsField pnd_Work_Month;

    private DbsGroup pnd_Work_Month__R_Field_2;
    private DbsField pnd_Work_Month_Pnd_Work_Month_N;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Tbl_WpidOld;
    private DbsField sort01Tbl_WpidCount648;
    private DbsField sort01Tbl_WpidCount;
    private DbsField sort01Tbl_Wpid_ActOld;
    private DbsField sort01Tbl_MocOld;
    private DbsField sort01Tbl_Close_UnitOld;
    private DbsField sort01Tbl_MocCount829;
    private DbsField sort01Tbl_MocCount;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCwfl3802 = new LdaCwfl3802();
        registerRecord(ldaCwfl3802);
        registerRecord(ldaCwfl3802.getVw_cwf_Master_Index());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Work_Day = localVariables.newFieldInRecord("pnd_Work_Day", "#WORK-DAY", FieldType.STRING, 2);

        pnd_Work_Day__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_Day__R_Field_1", "REDEFINE", pnd_Work_Day);
        pnd_Work_Day_Pnd_Work_Day_N = pnd_Work_Day__R_Field_1.newFieldInGroup("pnd_Work_Day_Pnd_Work_Day_N", "#WORK-DAY-N", FieldType.NUMERIC, 2);
        pnd_Work_Month = localVariables.newFieldInRecord("pnd_Work_Month", "#WORK-MONTH", FieldType.STRING, 2);

        pnd_Work_Month__R_Field_2 = localVariables.newGroupInRecord("pnd_Work_Month__R_Field_2", "REDEFINE", pnd_Work_Month);
        pnd_Work_Month_Pnd_Work_Month_N = pnd_Work_Month__R_Field_2.newFieldInGroup("pnd_Work_Month_Pnd_Work_Month_N", "#WORK-MONTH-N", FieldType.STRING, 
            2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Tbl_WpidOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_OLD", "Tbl_Wpid_OLD", FieldType.STRING, 6);
        sort01Tbl_WpidCount648 = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT_648", "Tbl_Wpid_COUNT_648", FieldType.NUMERIC, 9);
        sort01Tbl_WpidCount = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT", "Tbl_Wpid_COUNT", FieldType.NUMERIC, 9);
        sort01Tbl_Wpid_ActOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_Act_OLD", "Tbl_Wpid_Act_OLD", FieldType.STRING, 1);
        sort01Tbl_MocOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Moc_OLD", "Tbl_Moc_OLD", FieldType.STRING, 1);
        sort01Tbl_Close_UnitOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Close_Unit_OLD", "Tbl_Close_Unit_OLD", FieldType.STRING, 8);
        sort01Tbl_MocCount829 = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Moc_COUNT_829", "Tbl_Moc_COUNT_829", FieldType.NUMERIC, 9);
        sort01Tbl_MocCount = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Moc_COUNT", "Tbl_Moc_COUNT", FieldType.NUMERIC, 9);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaCwfl3802.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3803() throws Exception
    {
        super("Cwfb3803");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        ldaCwfl3802.getPnd_Grand_Title2().setValue("(MONTHLY)");                                                                                                          //Natural: MOVE '(MONTHLY)' TO #GRAND-TITLE2
        ldaCwfl3802.getPnd_Todays_Time().setValue(Global.getTIMX());                                                                                                      //Natural: MOVE *TIMX TO #TODAYS-TIME
        ldaCwfl3802.getPnd_Mid_Total().setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #MID-TOTAL
        ldaCwfl3802.getPnd_Todays_Time().setValue(Global.getTIMX());                                                                                                      //Natural: FORMAT ( 1 ) LS = 142 PS = 58;//Natural: MOVE *TIMX TO #TODAYS-TIME
        ldaCwfl3802.getPnd_Base_Date().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Base_Date()), Global.getDATX().subtract(9999));                            //Natural: COMPUTE #BASE-DATE = *DATX - 9999
        ldaCwfl3802.getPnd_Datx().setValue(Global.getDATX());                                                                                                             //Natural: MOVE *DATX TO #DATX
        pnd_Work_Day.setValueEdited(ldaCwfl3802.getPnd_Datx(),new ReportEditMask("DD"));                                                                                  //Natural: MOVE EDITED #DATX ( EM = DD ) TO #WORK-DAY
        //*  JVH - MUST USE DATE FROM NEXT MONTH FOR THIS
        //*  DATE RANGE LOGIC TO WORK
        if (condition(pnd_Work_Day.greater("27")))                                                                                                                        //Natural: IF #WORK-DAY > '27'
        {
            ldaCwfl3802.getPnd_Datx().nadd(5);                                                                                                                            //Natural: ASSIGN #DATX := #DATX + 5
        }                                                                                                                                                                 //Natural: END-IF
        ldaCwfl3802.getPnd_Comp_Date().setValueEdited(ldaCwfl3802.getPnd_Datx(),new ReportEditMask("YYYYMMDD"));                                                          //Natural: MOVE EDITED #DATX ( EM = YYYYMMDD ) TO #COMP-DATE
        ldaCwfl3802.getPnd_Work_Date_D().setValue(ldaCwfl3802.getPnd_Datx());                                                                                             //Natural: MOVE #DATX TO #WORK-DATE-D
        pnd_Work_Month.setValueEdited(ldaCwfl3802.getPnd_Datx(),new ReportEditMask("MM"));                                                                                //Natural: MOVE EDITED #DATX ( EM = MM ) TO #WORK-MONTH
        pnd_Work_Day.setValueEdited(ldaCwfl3802.getPnd_Datx(),new ReportEditMask("DD"));                                                                                  //Natural: MOVE EDITED #DATX ( EM = DD ) TO #WORK-DAY
        ldaCwfl3802.getPnd_Work_Date_D().nsubtract(pnd_Work_Day_Pnd_Work_Day_N);                                                                                          //Natural: COMPUTE #WORK-DATE-D = #WORK-DATE-D - #WORK-DAY-N
        ldaCwfl3802.getPnd_Work_Date_Time_Pnd_Work_Date().setValueEdited(ldaCwfl3802.getPnd_Work_Date_D(),new ReportEditMask("YYYYMMDD"));                                //Natural: MOVE EDITED #WORK-DATE-D ( EM = YYYYMMDD ) TO #WORK-DATE
        ldaCwfl3802.getPnd_End_Dte_Tme().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaCwfl3802.getPnd_Work_Date_Time_Pnd_Work_Date());                                //Natural: MOVE EDITED #WORK-DATE TO #END-DTE-TME ( EM = YYYYMMDD )
        ldaCwfl3802.getPnd_End_Dte_Tme().nadd(863999);                                                                                                                    //Natural: ADD 863999 TO #END-DTE-TME
        ldaCwfl3802.getPnd_End_Dte_Tme_A().setValueEdited(ldaCwfl3802.getPnd_End_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                                        //Natural: MOVE EDITED #END-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #END-DTE-TME-A
        pnd_Work_Day.setValueEdited(ldaCwfl3802.getPnd_Work_Date_D(),new ReportEditMask("DD"));                                                                           //Natural: MOVE EDITED #WORK-DATE-D ( EM = DD ) TO #WORK-DAY
        ldaCwfl3802.getPnd_Work_Date_D().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Work_Date_D()), ldaCwfl3802.getPnd_Work_Date_D().add(1).subtract(pnd_Work_Day_Pnd_Work_Day_N)); //Natural: COMPUTE #WORK-DATE-D = #WORK-DATE-D + 1 - #WORK-DAY-N
        ldaCwfl3802.getPnd_Work_Date_Time_Pnd_Work_Date().setValueEdited(ldaCwfl3802.getPnd_Work_Date_D(),new ReportEditMask("YYYYMMDD"));                                //Natural: MOVE EDITED #WORK-DATE-D ( EM = YYYYMMDD ) TO #WORK-DATE
        ldaCwfl3802.getPnd_Start_Dte_Tme().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaCwfl3802.getPnd_Work_Date_Time_Pnd_Work_Date());                              //Natural: MOVE EDITED #WORK-DATE TO #START-DTE-TME ( EM = YYYYMMDD )
        ldaCwfl3802.getPnd_Start_Dte_Tme_A().setValueEdited(ldaCwfl3802.getPnd_Start_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                                    //Natural: MOVE EDITED #START-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #START-DTE-TME-A
        ldaCwfl3802.getPnd_Start_Key_Crprte_Status_Ind().setValue("0");                                                                                                   //Natural: MOVE '0' TO #START-KEY.CRPRTE-STATUS-IND
        //*  READ CWF-MASTER-INDEX
        ldaCwfl3802.getVw_cwf_Master_Index().startDatabaseRead                                                                                                            //Natural: READ CWF-MASTER-INDEX BY ACTV-UNQUE-KEY
        (
        "READ_MASTER_1",
        new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") }
        );
        READ_MASTER_1:
        while (condition(ldaCwfl3802.getVw_cwf_Master_Index().readNextRow("READ_MASTER_1")))
        {
            if (condition(!((((((ldaCwfl3802.getCwf_Master_Index_Status_Cde().greaterOrEqual("4500") && ldaCwfl3802.getCwf_Master_Index_Status_Cde().lessOrEqual("4997")) //Natural: ACCEPT IF NOT ( CWF-MASTER-INDEX.STATUS-CDE = '4500' THRU '4997' OR CWF-MASTER-INDEX.ADMIN-STATUS-CDE = '4500' THRU '4997' OR CWF-MASTER-INDEX.UNIT-CDE = 'RSSMP' OR CWF-MASTER-INDEX.PIN-NBR = 0000000 OR = 999999999999 ) AND ( CWF-MASTER-INDEX.UNIT-ID-CDE = #VALID-UNITS ( * ) AND CWF-MASTER-INDEX.CRPRTE-CLOCK-END-DTE-TME = MASK ( YYYYMMDD....... ) AND CWF-MASTER-INDEX.CRPRTE-CLOCK-END-DTE-TME = MASK ( ........00:23..... ) AND CWF-MASTER-INDEX.CRPRTE-CLOCK-END-DTE-TME = MASK ( ..........00:59... ) AND CWF-MASTER-INDEX.CRPRTE-CLOCK-END-DTE-TME = MASK ( ............00:59. ) AND CWF-MASTER-INDEX.CRPRTE-CLOCK-END-DTE-TME = MASK ( ..............0:9 ) AND CWF-MASTER-INDEX.CRPRTE-CLOCK-END-DTE-TME = #START-DTE-TME-A THRU #END-DTE-TME-A )
                || (ldaCwfl3802.getCwf_Master_Index_Admin_Status_Cde().greaterOrEqual("4500") && ldaCwfl3802.getCwf_Master_Index_Admin_Status_Cde().lessOrEqual("4997"))) 
                || ldaCwfl3802.getCwf_Master_Index_Unit_Cde().equals("RSSMP")) || ! ((ldaCwfl3802.getCwf_Master_Index_Pin_Nbr().equals(0) || ldaCwfl3802.getCwf_Master_Index_Pin_Nbr().equals(new 
                DbsDecimal("999999999999"))))) && ((((((ldaCwfl3802.getCwf_Master_Index_Unit_Id_Cde().equals(ldaCwfl3802.getPnd_Valid_Units().getValue("*")) 
                && DbsUtil.maskMatches(ldaCwfl3802.getCwf_Master_Index_Crprte_Clock_End_Dte_Tme(),"YYYYMMDD.......")) && DbsUtil.maskMatches(ldaCwfl3802.getCwf_Master_Index_Crprte_Clock_End_Dte_Tme(),"........00:23.....")) 
                && DbsUtil.maskMatches(ldaCwfl3802.getCwf_Master_Index_Crprte_Clock_End_Dte_Tme(),"..........00:59...")) && DbsUtil.maskMatches(ldaCwfl3802.getCwf_Master_Index_Crprte_Clock_End_Dte_Tme(),"............00:59.")) 
                && DbsUtil.maskMatches(ldaCwfl3802.getCwf_Master_Index_Crprte_Clock_End_Dte_Tme(),"..............0:9")) && (ldaCwfl3802.getCwf_Master_Index_Crprte_Clock_End_Dte_Tme().greaterOrEqual(ldaCwfl3802.getPnd_Start_Dte_Tme_A()) 
                && ldaCwfl3802.getCwf_Master_Index_Crprte_Clock_End_Dte_Tme().lessOrEqual(ldaCwfl3802.getPnd_End_Dte_Tme_A())))))))
            {
                continue;
            }
            //*  CWF-MASTER-INDEX.PIN-NBR = 0000000 OR = 9999999) AND
            //*  REPORT CORE ROUTINE 1
            ldaCwfl3802.getPnd_Work_Record().reset();                                                                                                                     //Natural: RESET #WORK-RECORD
            ldaCwfl3802.getPnd_Work_Record_Tbl_Business_Days().setValue(0);                                                                                               //Natural: MOVE 0 TO #WORK-RECORD.TBL-BUSINESS-DAYS
            //*  --------------------------
            //*  EXTERNAL DOCUMENT RECEIVED
            //*  --------------------------
            if (condition(ldaCwfl3802.getCwf_Master_Index_Extrnl_Pend_Rcv_Dte().greater(getZero())))                                                                      //Natural: IF CWF-MASTER-INDEX.EXTRNL-PEND-RCV-DTE GT 0
            {
                ldaCwfl3802.getPnd_Work_Record_Return_Doc_Rec_Dte_Tme().setValue(ldaCwfl3802.getCwf_Master_Index_Extrnl_Pend_Rcv_Dte());                                  //Natural: MOVE CWF-MASTER-INDEX.EXTRNL-PEND-RCV-DTE TO #WORK-RECORD.RETURN-DOC-REC-DTE-TME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl3802.getPnd_Work_Record_Return_Doc_Rec_Dte_Tme().reset();                                                                                          //Natural: RESET #WORK-RECORD.RETURN-DOC-REC-DTE-TME
            }                                                                                                                                                             //Natural: END-IF
            //*  ---------------------
            //*  RECEIVED IN UNIT DATE
            //*  ---------------------
            if (condition(DbsUtil.maskMatches(ldaCwfl3802.getCwf_Master_Index_Unit_Clock_Start_Dte_Tme(),"YYYYMMDD")))                                                    //Natural: IF CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME = MASK ( YYYYMMDD )
            {
                ldaCwfl3802.getPnd_Work_Record_Return_Rcvd_Dte_Tme().setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl3802.getCwf_Master_Index_Unit_Clock_Start_Dte_Tme()); //Natural: MOVE EDITED CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME TO #WORK-RECORD.RETURN-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl3802.getPnd_Work_Record_Return_Rcvd_Dte_Tme().reset();                                                                                             //Natural: RESET #WORK-RECORD.RETURN-RCVD-DTE-TME
            }                                                                                                                                                             //Natural: END-IF
            //*  ---------------------
            //*  CALENDAR DAYS IN TIAA
            //*  ---------------------
            if (condition(ldaCwfl3802.getCwf_Master_Index_Extrnl_Pend_Rcv_Dte().greater(getZero())))                                                                      //Natural: IF CWF-MASTER-INDEX.EXTRNL-PEND-RCV-DTE GT 0
            {
                ldaCwfl3802.getPnd_Receive_Date().setValue(ldaCwfl3802.getCwf_Master_Index_Extrnl_Pend_Rcv_Dte());                                                        //Natural: MOVE CWF-MASTER-INDEX.EXTRNL-PEND-RCV-DTE TO #RECEIVE-DATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl3802.getPnd_Receive_Date().setValue(ldaCwfl3802.getCwf_Master_Index_Tiaa_Rcvd_Dte());                                                              //Natural: MOVE CWF-MASTER-INDEX.TIAA-RCVD-DTE TO #RECEIVE-DATE
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------
            //*  NUMBER OF DAYS COMPLETED  (BUSINSS DAYS)
            //*  ------------------------
            //*  RECEIVE DATE TO PARTICIPANT CLOSE DATE ( WHOLE DAYS)
            ldaCwfl3802.getPnd_Participant_Closed_Date_Time().setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl3802.getCwf_Master_Index_Crprte_Clock_End_Dte_Tme()); //Natural: MOVE EDITED CWF-MASTER-INDEX.CRPRTE-CLOCK-END-DTE-TME TO #PARTICIPANT-CLOSED-DATE-TIME ( EM = YYYYMMDDHHIISST )
            ldaCwfl3802.getPnd_Status_Updte_Dte().setValue(ldaCwfl3802.getPnd_Participant_Closed_Date_Time());                                                            //Natural: MOVE #PARTICIPANT-CLOSED-DATE-TIME TO #STATUS-UPDTE-DTE
            if (condition(ldaCwfl3802.getPnd_Receive_Date().greaterOrEqual(ldaCwfl3802.getPnd_Base_Date())))                                                              //Natural: IF #RECEIVE-DATE GE #BASE-DATE
            {
                ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days()),              //Natural: ASSIGN #WORK-RECORD.TBL-COMPLETE-DAYS := #STATUS-UPDTE-DTE - #RECEIVE-DATE
                    ldaCwfl3802.getPnd_Status_Updte_Dte().subtract(ldaCwfl3802.getPnd_Receive_Date()));
                DbsUtil.callnat(Cwfn3901.class , getCurrentProcessState(), ldaCwfl3802.getPnd_Receive_Date(), ldaCwfl3802.getPnd_Status_Updte_Dte(), ldaCwfl3802.getPnd_Holiday()); //Natural: CALLNAT 'CWFN3901' #RECEIVE-DATE #STATUS-UPDTE-DTE #HOLIDAY
                if (condition(Global.isEscape())) return;
                ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().nsubtract(ldaCwfl3802.getPnd_Holiday());                                                               //Natural: ASSIGN #WORK-RECORD.TBL-COMPLETE-DAYS := #WORK-RECORD.TBL-COMPLETE-DAYS - #HOLIDAY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().setValue(9999);                                                                                        //Natural: MOVE 9999 TO #WORK-RECORD.TBL-COMPLETE-DAYS
            }                                                                                                                                                             //Natural: END-IF
            ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Bsnss_Days().setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days());                                            //Natural: MOVE #WORK-RECORD.TBL-COMPLETE-DAYS TO #WORK-RECORD.TBL-TIAA-BSNSS-DAYS
            if (condition(ldaCwfl3802.getCwf_Master_Index_Status_Cde().greaterOrEqual("7000") && ldaCwfl3802.getCwf_Master_Index_Status_Cde().lessOrEqual("7499")))       //Natural: IF CWF-MASTER-INDEX.STATUS-CDE = '7000' THRU '7499'
            {
                ldaCwfl3802.getPnd_Work_Record_Tbl_Racf().setValue(" ");                                                                                                  //Natural: MOVE ' ' TO #WORK-RECORD.TBL-RACF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(((ldaCwfl3802.getCwf_Master_Index_Status_Cde().greaterOrEqual("8000") && ldaCwfl3802.getCwf_Master_Index_Status_Cde().lessOrEqual("8799"))  //Natural: IF CWF-MASTER-INDEX.STATUS-CDE = '8000' THRU '8799' OR CWF-MASTER-INDEX.STATUS-CDE = '9000' THRU '9999'
                    || (ldaCwfl3802.getCwf_Master_Index_Status_Cde().greaterOrEqual("9000") && ldaCwfl3802.getCwf_Master_Index_Status_Cde().lessOrEqual("9999")))))
                {
                    ldaCwfl3802.getPnd_Work_Record_Tbl_Racf().setValue(" ");                                                                                              //Natural: MOVE ' ' TO #WORK-RECORD.TBL-RACF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(ldaCwfl3802.getCwf_Master_Index_Work_Prcss_Id(),"N.....")))                                                                 //Natural: IF CWF-MASTER-INDEX.WORK-PRCSS-ID = MASK ( N..... )
            {
                ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().setValue("Z");                                                                                              //Natural: MOVE 'Z' TO #WORK-RECORD.TBL-WPID-ACT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().setValue(ldaCwfl3802.getCwf_Master_Index_Work_Prcss_Id());                                                  //Natural: MOVE CWF-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-RECORD.TBL-WPID-ACT
            }                                                                                                                                                             //Natural: END-IF
            ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid().setValue(ldaCwfl3802.getCwf_Master_Index_Work_Prcss_Id());                                                          //Natural: MOVE CWF-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-RECORD.TBL-WPID
            ldaCwfl3802.getPnd_Work_Record_Tbl_Pin().setValue(ldaCwfl3802.getCwf_Master_Index_Pin_Nbr());                                                                 //Natural: MOVE CWF-MASTER-INDEX.PIN-NBR TO #WORK-RECORD.TBL-PIN
            ldaCwfl3802.getPnd_Work_Record_Tbl_Admin_Unit().setValue(ldaCwfl3802.getCwf_Master_Index_Admin_Unit_Cde());                                                   //Natural: MOVE CWF-MASTER-INDEX.ADMIN-UNIT-CDE TO #WORK-RECORD.TBL-ADMIN-UNIT
            ldaCwfl3802.getPnd_Work_Record_Tbl_Close_Unit().setValue(ldaCwfl3802.getCwf_Master_Index_Unit_Cde());                                                         //Natural: MOVE CWF-MASTER-INDEX.UNIT-CDE TO #WORK-RECORD.TBL-CLOSE-UNIT
            ldaCwfl3802.getPnd_Work_Record_Tbl_Racf().setValue(ldaCwfl3802.getCwf_Master_Index_Empl_Racf_Id());                                                           //Natural: MOVE CWF-MASTER-INDEX.EMPL-RACF-ID TO #WORK-RECORD.TBL-RACF
            ldaCwfl3802.getPnd_Work_Record_Tbl_Last_Chnge_Unit().setValue(ldaCwfl3802.getCwf_Master_Index_Unit_Cde());                                                    //Natural: MOVE CWF-MASTER-INDEX.UNIT-CDE TO #WORK-RECORD.TBL-LAST-CHNGE-UNIT
            ldaCwfl3802.getPnd_Work_Record_Tbl_Last_Chge_Dte().setValue(ldaCwfl3802.getCwf_Master_Index_Last_Chnge_Dte_Tme());                                            //Natural: MOVE CWF-MASTER-INDEX.LAST-CHNGE-DTE-TME TO #WORK-RECORD.TBL-LAST-CHGE-DTE
            ldaCwfl3802.getPnd_Work_Record_Tbl_Log_Dte_Tme().setValue(ldaCwfl3802.getCwf_Master_Index_Rqst_Log_Dte_Tme());                                                //Natural: MOVE CWF-MASTER-INDEX.RQST-LOG-DTE-TME TO #WORK-RECORD.TBL-LOG-DTE-TME
            if (condition(DbsUtil.maskMatches(ldaCwfl3802.getCwf_Master_Index_Unit_Cde(),"'APC'.....")))                                                                  //Natural: IF CWF-MASTER-INDEX.UNIT-CDE = MASK ( 'APC'..... )
            {
                if (condition(ldaCwfl3802.getCwf_Master_Index_Work_Prcss_Id().equals(ldaCwfl3802.getPnd_Exclude_Wpids().getValue("*"))))                                  //Natural: IF CWF-MASTER-INDEX.WORK-PRCSS-ID = #EXCLUDE-WPIDS ( * )
                {
                    ldaCwfl3802.getPnd_Work_Record_Tbl_Moc().setValue("3");                                                                                               //Natural: MOVE '3' TO #WORK-RECORD.TBL-MOC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaCwfl3802.getCwf_Master_Index_Rqst_Orgn_Cde().equals("I")))                                                                           //Natural: IF CWF-MASTER-INDEX.RQST-ORGN-CDE = 'I'
                    {
                        ldaCwfl3802.getPnd_Work_Record_Tbl_Moc().setValue("1");                                                                                           //Natural: MOVE '1' TO #WORK-RECORD.TBL-MOC
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaCwfl3802.getPnd_Work_Record_Tbl_Moc().setValue("2");                                                                                           //Natural: MOVE '2' TO #WORK-RECORD.TBL-MOC
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaCwfl3802.getCwf_Master_Index_Rqst_Orgn_Cde().equals("I")))                                                                               //Natural: IF CWF-MASTER-INDEX.RQST-ORGN-CDE = 'I'
                {
                    ldaCwfl3802.getPnd_Work_Record_Tbl_Moc().setValue("1");                                                                                               //Natural: MOVE '1' TO #WORK-RECORD.TBL-MOC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCwfl3802.getPnd_Work_Record_Tbl_Moc().setValue("2");                                                                                               //Natural: MOVE '2' TO #WORK-RECORD.TBL-MOC
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Dte().setValue(ldaCwfl3802.getCwf_Master_Index_Tiaa_Rcvd_Dte());                                                      //Natural: MOVE CWF-MASTER-INDEX.TIAA-RCVD-DTE TO #WORK-RECORD.TBL-TIAA-DTE
            ldaCwfl3802.getPnd_Work_Record_Tbl_Status_Cde().setValue(ldaCwfl3802.getCwf_Master_Index_Status_Cde());                                                       //Natural: MOVE CWF-MASTER-INDEX.STATUS-CDE TO #WORK-RECORD.TBL-STATUS-CDE
            ldaCwfl3802.getPnd_Work_Record_Tbl_Status_Dte().setValue(ldaCwfl3802.getCwf_Master_Index_Admin_Status_Updte_Dte_Tme());                                       //Natural: MOVE CWF-MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME TO #WORK-RECORD.TBL-STATUS-DTE
            ldaCwfl3802.getPnd_Work_Record_Tbl_Contracts().setValue(ldaCwfl3802.getCwf_Master_Index_Cntrct_Nbr().getValue(1));                                            //Natural: MOVE CWF-MASTER-INDEX.CNTRCT-NBR ( 1 ) TO #WORK-RECORD.TBL-CONTRACTS
            ldaCwfl3802.getPnd_Work_Record_Pnd_Rqst_Log_Dte_Tme().setValue(ldaCwfl3802.getCwf_Master_Index_Rqst_Log_Dte_Tme());                                           //Natural: MOVE CWF-MASTER-INDEX.RQST-LOG-DTE-TME TO #RQST-LOG-DTE-TME
            ldaCwfl3802.getPnd_Work_Record_Pnd_Last_Chnge_Dte_Tme().setValue(ldaCwfl3802.getCwf_Master_Index_Last_Chnge_Dte_Tme());                                       //Natural: MOVE CWF-MASTER-INDEX.LAST-CHNGE-DTE-TME TO #LAST-CHNGE-DTE-TME
            ldaCwfl3802.getPnd_Work_Record_Pnd_Partic_Sname().reset();                                                                                                    //Natural: RESET #PARTIC-SNAME
            DbsUtil.callnat(Cwfn3802.class , getCurrentProcessState(), ldaCwfl3802.getCwf_Master_Index_Pin_Nbr(), ldaCwfl3802.getCwf_Master_Index_Rqst_Orgn_Cde(),        //Natural: CALLNAT 'CWFN3802' CWF-MASTER-INDEX.PIN-NBR CWF-MASTER-INDEX.RQST-ORGN-CDE CWF-MASTER-INDEX.MJ-PULL-IND CWF-MASTER-INDEX.PHYSCL-FLDR-ID-NBR #PARTIC-SNAME
                ldaCwfl3802.getCwf_Master_Index_Mj_Pull_Ind(), ldaCwfl3802.getCwf_Master_Index_Physcl_Fldr_Id_Nbr(), ldaCwfl3802.getPnd_Work_Record_Pnd_Partic_Sname());
            if (condition(Global.isEscape())) return;
            getSort().writeSortInData(ldaCwfl3802.getPnd_Work_Record_Tbl_Close_Unit(), ldaCwfl3802.getPnd_Work_Record_Tbl_Moc(), ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act(),  //Natural: END-ALL
                ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid(), ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Bsnss_Days(), ldaCwfl3802.getPnd_Work_Record_Tbl_Pin(), 
                ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Dte(), ldaCwfl3802.getPnd_Work_Record_Return_Doc_Rec_Dte_Tme(), ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days(), 
                ldaCwfl3802.getPnd_Work_Record_Tbl_Status_Cde(), ldaCwfl3802.getPnd_Work_Record_Tbl_Racf(), ldaCwfl3802.getPnd_Work_Record_Tbl_Business_Days(), 
                ldaCwfl3802.getPnd_Work_Record_Tbl_Last_Chnge_Unit(), ldaCwfl3802.getPnd_Work_Record_Tbl_Contracts(), ldaCwfl3802.getPnd_Work_Record_Tbl_Status_Dte(), 
                ldaCwfl3802.getPnd_Work_Record_Return_Rcvd_Dte_Tme(), ldaCwfl3802.getPnd_Work_Record_Pnd_Partic_Sname());
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(ldaCwfl3802.getPnd_Work_Record_Tbl_Close_Unit(), ldaCwfl3802.getPnd_Work_Record_Tbl_Moc(), ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act(),      //Natural: SORT BY #WORK-RECORD.TBL-CLOSE-UNIT #WORK-RECORD.TBL-MOC #WORK-RECORD.TBL-WPID-ACT #WORK-RECORD.TBL-WPID #WORK-RECORD.TBL-TIAA-BSNSS-DAYS #WORK-RECORD.TBL-PIN #WORK-RECORD.TBL-TIAA-DTE USING #WORK-RECORD.RETURN-DOC-REC-DTE-TME #WORK-RECORD.TBL-COMPLETE-DAYS #WORK-RECORD.TBL-STATUS-CDE #WORK-RECORD.TBL-RACF #WORK-RECORD.TBL-BUSINESS-DAYS #WORK-RECORD.TBL-LAST-CHNGE-UNIT #WORK-RECORD.TBL-CONTRACTS #WORK-RECORD.TBL-STATUS-DTE #WORK-RECORD.RETURN-RCVD-DTE-TME #WORK-RECORD.#PARTIC-SNAME
            ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid(), ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Bsnss_Days(), ldaCwfl3802.getPnd_Work_Record_Tbl_Pin(), ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Dte());
        sort01Tbl_WpidCount648.setDec(new DbsDecimal(0));
        sort01Tbl_WpidCount.setDec(new DbsDecimal(0));
        sort01Tbl_MocCount829.setDec(new DbsDecimal(0));
        sort01Tbl_MocCount.setDec(new DbsDecimal(0));
        sort01Tbl_MocCount829.setDec(new DbsDecimal(0));
        sort01Tbl_MocCount829.setDec(new DbsDecimal(0));
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(ldaCwfl3802.getPnd_Work_Record_Tbl_Close_Unit(), ldaCwfl3802.getPnd_Work_Record_Tbl_Moc(), ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act(), 
            ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid(), ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Bsnss_Days(), ldaCwfl3802.getPnd_Work_Record_Tbl_Pin(), ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Dte(), 
            ldaCwfl3802.getPnd_Work_Record_Return_Doc_Rec_Dte_Tme(), ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days(), ldaCwfl3802.getPnd_Work_Record_Tbl_Status_Cde(), 
            ldaCwfl3802.getPnd_Work_Record_Tbl_Racf(), ldaCwfl3802.getPnd_Work_Record_Tbl_Business_Days(), ldaCwfl3802.getPnd_Work_Record_Tbl_Last_Chnge_Unit(), 
            ldaCwfl3802.getPnd_Work_Record_Tbl_Contracts(), ldaCwfl3802.getPnd_Work_Record_Tbl_Status_Dte(), ldaCwfl3802.getPnd_Work_Record_Return_Rcvd_Dte_Tme(), 
            ldaCwfl3802.getPnd_Work_Record_Pnd_Partic_Sname())))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            sort01Tbl_WpidCount648.setInt(sort01Tbl_WpidCount648.getInt() + 1);
            sort01Tbl_WpidCount.setInt(sort01Tbl_WpidCount.getInt() + 1);
            sort01Tbl_MocCount829.setInt(sort01Tbl_MocCount829.getInt() + 1);
            sort01Tbl_MocCount.setInt(sort01Tbl_MocCount.getInt() + 1);
            sort01Tbl_MocCount829.setInt(sort01Tbl_MocCount829.getInt() + 1);
            sort01Tbl_MocCount829.setInt(sort01Tbl_MocCount829.getInt() + 1);
            //* REPORT CODE ROUTINE 2
            if (condition(ldaCwfl3802.getPnd_Old_Moc().equals(" ")))                                                                                                      //Natural: IF #OLD-MOC = ' '
            {
                ldaCwfl3802.getPnd_Old_Moc().setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Moc());                                                                          //Natural: MOVE #WORK-RECORD.TBL-MOC TO #OLD-MOC
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCwfl3802.getPnd_Save_Unit().equals(" ")))                                                                                                    //Natural: IF #SAVE-UNIT = ' '
            {
                ldaCwfl3802.getPnd_First_Unit().setValue(true);                                                                                                           //Natural: MOVE TRUE TO #FIRST-UNIT
                ldaCwfl3802.getPnd_New_Unit().setValue(false);                                                                                                            //Natural: MOVE FALSE TO #NEW-UNIT
                ldaCwfl3802.getPnd_Save_Unit().setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Close_Unit());                                                                 //Natural: MOVE #WORK-RECORD.TBL-CLOSE-UNIT TO #SAVE-UNIT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCwfl3802.getPnd_New_Unit().getBoolean() || ldaCwfl3802.getPnd_First_Unit().getBoolean()))                                                    //Natural: IF #NEW-UNIT OR #FIRST-UNIT
            {
                ldaCwfl3802.getPnd_First_Unit().setValue(false);                                                                                                          //Natural: MOVE FALSE TO #FIRST-UNIT
                ldaCwfl3802.getPnd_New_Unit().setValue(false);                                                                                                            //Natural: MOVE FALSE TO #NEW-UNIT
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Cwfn5390.class , getCurrentProcessState(), ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid(), ldaCwfl3802.getPnd_Wpid_Sname());                       //Natural: AT TOP OF PAGE ( 1 );//Natural: CALLNAT 'CWFN5390' #WORK-RECORD.TBL-WPID #WPID-SNAME
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn1102.class , getCurrentProcessState(), ldaCwfl3802.getPnd_Work_Record_Tbl_Status_Key(), ldaCwfl3802.getPnd_Status_Sname());               //Natural: CALLNAT 'CWFN1102' #WORK-RECORD.TBL-STATUS-KEY #STATUS-SNAME
            if (condition(Global.isEscape())) return;
            if (condition(ldaCwfl3802.getPnd_Work_Record_Return_Doc_Rec_Dte_Tme().greater(getZero())))                                                                    //Natural: IF #WORK-RECORD.RETURN-DOC-REC-DTE-TME GT 0
            {
                ldaCwfl3802.getPnd_Return_Doc_Txt().setValueEdited(ldaCwfl3802.getPnd_Work_Record_Return_Doc_Rec_Dte_Tme(),new ReportEditMask("MM/DD/YY"));               //Natural: MOVE EDITED #WORK-RECORD.RETURN-DOC-REC-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl3802.getPnd_Return_Doc_Txt().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCwfl3802.getPnd_Work_Record_Return_Rcvd_Dte_Tme().greater(getZero())))                                                                       //Natural: IF #WORK-RECORD.RETURN-RCVD-DTE-TME GT 0
            {
                ldaCwfl3802.getPnd_Return_Rcvd_Txt().setValueEdited(ldaCwfl3802.getPnd_Work_Record_Return_Rcvd_Dte_Tme(),new ReportEditMask("MM/DD/YY"));                 //Natural: MOVE EDITED #WORK-RECORD.RETURN-RCVD-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl3802.getPnd_Return_Rcvd_Txt().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCwfl3802.getPnd_Save_Action().notEquals(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act())))                                                     //Natural: IF #SAVE-ACTION NE #WORK-RECORD.TBL-WPID-ACT
            {
                ldaCwfl3802.getPnd_Save_Action().setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act());                                                                 //Natural: MOVE #WORK-RECORD.TBL-WPID-ACT TO #SAVE-ACTION
                short decideConditionsMet469 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #SAVE-ACTION;//Natural: VALUE 'B'
                if (condition((ldaCwfl3802.getPnd_Save_Action().equals("B"))))
                {
                    decideConditionsMet469++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"BOOKLETS    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'BOOKLETS    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((ldaCwfl3802.getPnd_Save_Action().equals("F"))))
                {
                    decideConditionsMet469++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"FORMS       :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'FORMS       :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((ldaCwfl3802.getPnd_Save_Action().equals("I"))))
                {
                    decideConditionsMet469++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"INQUIRY     :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'INQUIRY     :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((ldaCwfl3802.getPnd_Save_Action().equals("R"))))
                {
                    decideConditionsMet469++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"RESEARCH    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'RESEARCH    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((ldaCwfl3802.getPnd_Save_Action().equals("T"))))
                {
                    decideConditionsMet469++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TRANSACTIONS:",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'TRANSACTIONS:' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((ldaCwfl3802.getPnd_Save_Action().equals("X"))))
                {
                    decideConditionsMet469++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"COMPLAINTS  :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'COMPLAINTS  :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((ldaCwfl3802.getPnd_Save_Action().equals("Z"))))
                {
                    decideConditionsMet469++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"OTHERS      :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'OTHERS      :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "//RECEIVED/AT TIAA",                                                                                                                 //Natural: DISPLAY ( 1 ) '//RECEIVED/AT TIAA' #WORK-RECORD.TBL-TIAA-DTE ( EM = MM/DD/YY ) '//DOC/REC"D' #RETURN-DOC-TXT '//REC"D IN/UNIT' #RETURN-RCVD-TXT '//WORK/PROCESS' #WPID-SNAME ( IS = ON ) '/PARTICIPANT/CLOSED/STATUS' #STATUS-SNAME '//STATUS/DATE' #WORK-RECORD.TBL-STATUS-DTE ( EM = MM/DD/YY ) 'BUSINESS/DAYS/IN/TIAA' #WORK-RECORD.TBL-TIAA-BSNSS-DAYS ( EM = Z,ZZ9 ) '//PARTIC/NAME' #PARTIC-SNAME '///PIN' #WORK-RECORD.TBL-PIN '//CONTRACT/NUMBER' #WORK-RECORD.TBL-CONTRACTS '///EMPLOYEE' #WORK-RECORD.TBL-RACF
            		ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Dte(), new ReportEditMask ("MM/DD/YY"),"//DOC/REC'D",
            		ldaCwfl3802.getPnd_Return_Doc_Txt(),"//REC'D IN/UNIT",
            		ldaCwfl3802.getPnd_Return_Rcvd_Txt(),"//WORK/PROCESS",
            		ldaCwfl3802.getPnd_Wpid_Sname(), new IdenticalSuppress(true),"/PARTICIPANT/CLOSED/STATUS",
            		ldaCwfl3802.getPnd_Status_Sname(),"//STATUS/DATE",
            		ldaCwfl3802.getPnd_Work_Record_Tbl_Status_Dte(), new ReportEditMask ("MM/DD/YY"),"BUSINESS/DAYS/IN/TIAA",
            		ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Bsnss_Days(), new ReportEditMask ("Z,ZZ9"),"//PARTIC/NAME",
            		ldaCwfl3802.getPnd_Work_Record_Pnd_Partic_Sname(),"///PIN",
            		ldaCwfl3802.getPnd_Work_Record_Tbl_Pin(),"//CONTRACT/NUMBER",
            		ldaCwfl3802.getPnd_Work_Record_Tbl_Contracts(),"///EMPLOYEE",
            		ldaCwfl3802.getPnd_Work_Record_Tbl_Racf());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            short decideConditionsMet489 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-COMPLETE-DAYS;//Natural: VALUE -9999.9:3.9
            if (condition(((ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().greaterOrEqual(- 9999.9) && ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().lessOrEqual(3)))))
            {
                decideConditionsMet489++;
                //*  ADD DAYS PENDED LOGIC ADD 1 TO #BOOKLET-CTR(3)
                short decideConditionsMet492 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("B")))
                {
                    decideConditionsMet492++;
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(3).nadd(1);                                                                                  //Natural: ADD 1 TO #BOOKLET-CNT ( 3 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("F")))
                {
                    decideConditionsMet492++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(3).nadd(1);                                                                                    //Natural: ADD 1 TO #FORMS-CTR ( 3 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(3).nadd(1);                                                                                    //Natural: ADD 1 TO #FORMS-CNT ( 3 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("I")))
                {
                    decideConditionsMet492++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(3).nadd(1);                                                                                  //Natural: ADD 1 TO #INQUIRE-CTR ( 3 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(3).nadd(1);                                                                                  //Natural: ADD 1 TO #INQUIRE-CNT ( 3 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("R")))
                {
                    decideConditionsMet492++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(3).nadd(1);                                                                                 //Natural: ADD 1 TO #RESEARCH-CTR ( 3 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(3).nadd(1);                                                                                 //Natural: ADD 1 TO #RESEARCH-CNT ( 3 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("T")))
                {
                    decideConditionsMet492++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(3).nadd(1);                                                                                    //Natural: ADD 1 TO #TRANS-CTR ( 3 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(3).nadd(1);                                                                                    //Natural: ADD 1 TO #TRANS-CNT ( 3 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("X")))
                {
                    decideConditionsMet492++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(3).nadd(1);                                                                                //Natural: ADD 1 TO #COMPLAINT-CTR ( 3 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(3).nadd(1);                                                                                //Natural: ADD 1 TO #COMPLAINT-CNT ( 3 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("Z")))
                {
                    decideConditionsMet492++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(3).nadd(1);                                                                                   //Natural: ADD 1 TO #OTHERS-CTR ( 3 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(3).nadd(1);                                                                                   //Natural: ADD 1 TO #OTHERS-CNT ( 3 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet492 > 0))
                {
                    ldaCwfl3802.getPnd_Sub_Count().nadd(1);                                                                                                               //Natural: ADD 1 TO #SUB-COUNT
                    ldaCwfl3802.getPnd_Total_Count_3().nadd(1);                                                                                                           //Natural: ADD 1 TO #TOTAL-COUNT-3
                }                                                                                                                                                         //Natural: NONE
                if (condition(decideConditionsMet492 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_3().nadd(1);                                                                                              //Natural: ADD 1 TO #TOTAL-MOC-3
            }                                                                                                                                                             //Natural: VALUE -9999.9:4.9
            else if (condition(((ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().greaterOrEqual(- 9999.9) && ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().lessOrEqual(4)))))
            {
                decideConditionsMet489++;
                //*  ADD DAYS PENDED LOGIC ADD 1 TO #BOOKLET-CTR(4)
                short decideConditionsMet522 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("B")))
                {
                    decideConditionsMet522++;
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(4).nadd(1);                                                                                  //Natural: ADD 1 TO #BOOKLET-CNT ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("F")))
                {
                    decideConditionsMet522++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(4).nadd(1);                                                                                    //Natural: ADD 1 TO #FORMS-CTR ( 4 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(4).nadd(1);                                                                                    //Natural: ADD 1 TO #FORMS-CNT ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("I")))
                {
                    decideConditionsMet522++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(4).nadd(1);                                                                                  //Natural: ADD 1 TO #INQUIRE-CTR ( 4 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(4).nadd(1);                                                                                  //Natural: ADD 1 TO #INQUIRE-CNT ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("R")))
                {
                    decideConditionsMet522++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(4).nadd(1);                                                                                 //Natural: ADD 1 TO #RESEARCH-CTR ( 4 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(4).nadd(1);                                                                                 //Natural: ADD 1 TO #RESEARCH-CNT ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("T")))
                {
                    decideConditionsMet522++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(4).nadd(1);                                                                                    //Natural: ADD 1 TO #TRANS-CTR ( 4 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(4).nadd(1);                                                                                    //Natural: ADD 1 TO #TRANS-CNT ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("X")))
                {
                    decideConditionsMet522++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(4).nadd(1);                                                                                //Natural: ADD 1 TO #COMPLAINT-CTR ( 4 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(4).nadd(1);                                                                                //Natural: ADD 1 TO #COMPLAINT-CNT ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("Z")))
                {
                    decideConditionsMet522++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(4).nadd(1);                                                                                   //Natural: ADD 1 TO #OTHERS-CTR ( 4 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(4).nadd(1);                                                                                   //Natural: ADD 1 TO #OTHERS-CNT ( 4 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet522 > 0))
                {
                    ldaCwfl3802.getPnd_Sub_Count().nadd(1);                                                                                                               //Natural: ADD 1 TO #SUB-COUNT
                    ldaCwfl3802.getPnd_Total_Count_4().nadd(1);                                                                                                           //Natural: ADD 1 TO #TOTAL-COUNT-4
                }                                                                                                                                                         //Natural: NONE
                if (condition(decideConditionsMet522 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_4().nadd(1);                                                                                              //Natural: ADD 1 TO #TOTAL-MOC-4
            }                                                                                                                                                             //Natural: VALUE -9999.9:5.9
            else if (condition(((ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().greaterOrEqual(- 9999.9) && ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().lessOrEqual(5)))))
            {
                decideConditionsMet489++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet552 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("B")))
                {
                    decideConditionsMet552++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(5).nadd(1);                                                                                  //Natural: ADD 1 TO #BOOKLET-CTR ( 5 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(5).nadd(1);                                                                                  //Natural: ADD 1 TO #BOOKLET-CNT ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("F")))
                {
                    decideConditionsMet552++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(5).nadd(1);                                                                                    //Natural: ADD 1 TO #FORMS-CTR ( 5 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(5).nadd(1);                                                                                    //Natural: ADD 1 TO #FORMS-CNT ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("I")))
                {
                    decideConditionsMet552++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(5).nadd(1);                                                                                  //Natural: ADD 1 TO #INQUIRE-CTR ( 5 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(5).nadd(1);                                                                                  //Natural: ADD 1 TO #INQUIRE-CNT ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("R")))
                {
                    decideConditionsMet552++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(5).nadd(1);                                                                                 //Natural: ADD 1 TO #RESEARCH-CTR ( 5 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(5).nadd(1);                                                                                 //Natural: ADD 1 TO #RESEARCH-CNT ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("T")))
                {
                    decideConditionsMet552++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(5).nadd(1);                                                                                    //Natural: ADD 1 TO #TRANS-CTR ( 5 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(5).nadd(1);                                                                                    //Natural: ADD 1 TO #TRANS-CNT ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("X")))
                {
                    decideConditionsMet552++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(5).nadd(1);                                                                                //Natural: ADD 1 TO #COMPLAINT-CTR ( 5 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(5).nadd(1);                                                                                //Natural: ADD 1 TO #COMPLAINT-CNT ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("Z")))
                {
                    decideConditionsMet552++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(5).nadd(1);                                                                                   //Natural: ADD 1 TO #OTHERS-CTR ( 5 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(5).nadd(1);                                                                                   //Natural: ADD 1 TO #OTHERS-CNT ( 5 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet552 > 0))
                {
                    ldaCwfl3802.getPnd_Sub_Count().nadd(1);                                                                                                               //Natural: ADD 1 TO #SUB-COUNT
                    ldaCwfl3802.getPnd_Total_Count_5().nadd(1);                                                                                                           //Natural: ADD 1 TO #TOTAL-COUNT-5
                }                                                                                                                                                         //Natural: NONE
                if (condition(decideConditionsMet552 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_5().nadd(1);                                                                                              //Natural: ADD 1 TO #TOTAL-MOC-5
            }                                                                                                                                                             //Natural: VALUE 6:10.9
            else if (condition(((ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().greaterOrEqual(6) && ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().lessOrEqual(10)))))
            {
                decideConditionsMet489++;
                short decideConditionsMet582 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("B")))
                {
                    decideConditionsMet582++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(10).nadd(1);                                                                                 //Natural: ADD 1 TO #BOOKLET-CTR ( 10 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(10).nadd(1);                                                                                 //Natural: ADD 1 TO #BOOKLET-CNT ( 10 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("F")))
                {
                    decideConditionsMet582++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(10).nadd(1);                                                                                   //Natural: ADD 1 TO #FORMS-CTR ( 10 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(10).nadd(1);                                                                                   //Natural: ADD 1 TO #FORMS-CNT ( 10 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("I")))
                {
                    decideConditionsMet582++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(10).nadd(1);                                                                                 //Natural: ADD 1 TO #INQUIRE-CTR ( 10 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(10).nadd(1);                                                                                 //Natural: ADD 1 TO #INQUIRE-CNT ( 10 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("R")))
                {
                    decideConditionsMet582++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(10).nadd(1);                                                                                //Natural: ADD 1 TO #RESEARCH-CTR ( 10 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(10).nadd(1);                                                                                //Natural: ADD 1 TO #RESEARCH-CNT ( 10 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("T")))
                {
                    decideConditionsMet582++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(10).nadd(1);                                                                                   //Natural: ADD 1 TO #TRANS-CTR ( 10 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(10).nadd(1);                                                                                   //Natural: ADD 1 TO #TRANS-CNT ( 10 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("X")))
                {
                    decideConditionsMet582++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(10).nadd(1);                                                                               //Natural: ADD 1 TO #COMPLAINT-CTR ( 10 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(10).nadd(1);                                                                               //Natural: ADD 1 TO #COMPLAINT-CNT ( 10 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("Z")))
                {
                    decideConditionsMet582++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(10).nadd(1);                                                                                  //Natural: ADD 1 TO #OTHERS-CTR ( 10 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(10).nadd(1);                                                                                  //Natural: ADD 1 TO #OTHERS-CNT ( 10 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet582 > 0))
                {
                    ldaCwfl3802.getPnd_Sub_Count().nadd(1);                                                                                                               //Natural: ADD 1 TO #SUB-COUNT
                    ldaCwfl3802.getPnd_Total_Count_10().nadd(1);                                                                                                          //Natural: ADD 1 TO #TOTAL-COUNT-10
                }                                                                                                                                                         //Natural: NONE
                if (condition(decideConditionsMet582 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_10().nadd(1);                                                                                             //Natural: ADD 1 TO #TOTAL-MOC-10
            }                                                                                                                                                             //Natural: VALUE 11:9999.9
            else if (condition(((ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().greaterOrEqual(11) && ldaCwfl3802.getPnd_Work_Record_Tbl_Complete_Days().lessOrEqual(9999)))))
            {
                decideConditionsMet489++;
                short decideConditionsMet612 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("B")))
                {
                    decideConditionsMet612++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(11).nadd(1);                                                                                 //Natural: ADD 1 TO #BOOKLET-CTR ( 11 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(11).nadd(1);                                                                                 //Natural: ADD 1 TO #BOOKLET-CNT ( 11 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("F")))
                {
                    decideConditionsMet612++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(11).nadd(1);                                                                                   //Natural: ADD 1 TO #FORMS-CTR ( 11 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(11).nadd(1);                                                                                   //Natural: ADD 1 TO #FORMS-CNT ( 11 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("I")))
                {
                    decideConditionsMet612++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(11).nadd(1);                                                                                 //Natural: ADD 1 TO #INQUIRE-CTR ( 11 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(11).nadd(1);                                                                                 //Natural: ADD 1 TO #INQUIRE-CNT ( 11 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("R")))
                {
                    decideConditionsMet612++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(11).nadd(1);                                                                                //Natural: ADD 1 TO #RESEARCH-CTR ( 11 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(11).nadd(1);                                                                                //Natural: ADD 1 TO #RESEARCH-CNT ( 11 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("T")))
                {
                    decideConditionsMet612++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(11).nadd(1);                                                                                   //Natural: ADD 1 TO #TRANS-CTR ( 11 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(11).nadd(1);                                                                                   //Natural: ADD 1 TO #TRANS-CNT ( 11 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("X")))
                {
                    decideConditionsMet612++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(11).nadd(1);                                                                               //Natural: ADD 1 TO #COMPLAINT-CTR ( 11 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(11).nadd(1);                                                                               //Natural: ADD 1 TO #COMPLAINT-CNT ( 11 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                if (condition(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().equals("Z")))
                {
                    decideConditionsMet612++;
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(11).nadd(1);                                                                                  //Natural: ADD 1 TO #OTHERS-CTR ( 11 )
                    ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(11).nadd(1);                                                                                  //Natural: ADD 1 TO #OTHERS-CNT ( 11 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet612 > 0))
                {
                    ldaCwfl3802.getPnd_Sub_Count().nadd(1);                                                                                                               //Natural: ADD 1 TO #SUB-COUNT
                    ldaCwfl3802.getPnd_Total_Count_Over().nadd(1);                                                                                                        //Natural: ADD 1 TO #TOTAL-COUNT-OVER
                }                                                                                                                                                         //Natural: NONE
                if (condition(decideConditionsMet612 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_Over().nadd(1);                                                                                           //Natural: ADD 1 TO #TOTAL-MOC-OVER
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaCwfl3802.getPnd_Wpid_Sum().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Wpid_Sum()), ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Bsnss_Days().add(ldaCwfl3802.getPnd_Wpid_Sum())); //Natural: ASSIGN #WPID-SUM := #WORK-RECORD.TBL-TIAA-BSNSS-DAYS + #WPID-SUM
            //* REPORT CODE ROUTINE 3
            //* REPORT CODE ROUTINE 4                                                                                                                                     //Natural: AT BREAK OF #WORK-RECORD.TBL-WPID;//Natural: AT BREAK OF #WORK-RECORD.TBL-WPID-ACT
            //*  READ-2.                                                                                                                                                  //Natural: AT BREAK OF TBL-MOC;//Natural: AT BREAK OF TBL-CLOSE-UNIT
            sort01Tbl_WpidOld.setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid());                                                                                        //Natural: END-SORT
            sort01Tbl_Wpid_ActOld.setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act());
            sort01Tbl_MocOld.setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Moc());
            sort01Tbl_Close_UnitOld.setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Close_Unit());
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            sort01Tbl_WpidCount648.resetBreak();
            sort01Tbl_WpidCount.resetBreak();
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //* REPORT CODE ROUTINE 5
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-GTOTAL-PERCENT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-GTOTAL-LINES
    }
    private void sub_Compute_Gtotal_Percent() throws Exception                                                                                                            //Natural: COMPUTE-GTOTAL-PERCENT
    {
        if (BLNatReinput.isReinput()) return;

        ldaCwfl3802.getPnd_Ptotal_Count_3().compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Ptotal_Count_3()), (ldaCwfl3802.getPnd_Total_Count_3().divide(ldaCwfl3802.getPnd_Total_Count())).multiply(100)); //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-3 = ( #TOTAL-COUNT-3 / #TOTAL-COUNT ) * 100
        ldaCwfl3802.getPnd_Ptotal_Count_4().compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Ptotal_Count_4()), (ldaCwfl3802.getPnd_Total_Count_4().divide(ldaCwfl3802.getPnd_Total_Count())).multiply(100)); //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-4 = ( #TOTAL-COUNT-4 / #TOTAL-COUNT ) * 100
        ldaCwfl3802.getPnd_Ptotal_Count_5().compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Ptotal_Count_5()), (ldaCwfl3802.getPnd_Total_Count_5().divide(ldaCwfl3802.getPnd_Total_Count())).multiply(100)); //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-5 = ( #TOTAL-COUNT-5 / #TOTAL-COUNT ) * 100
        ldaCwfl3802.getPnd_Ptotal_Count_10().compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Ptotal_Count_10()), (ldaCwfl3802.getPnd_Total_Count_10().divide(ldaCwfl3802.getPnd_Total_Count())).multiply(100)); //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-10 = ( #TOTAL-COUNT-10 / #TOTAL-COUNT ) * 100
        ldaCwfl3802.getPnd_Ptotal_Count_Over().compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Ptotal_Count_Over()), (ldaCwfl3802.getPnd_Total_Count_Over().divide(ldaCwfl3802.getPnd_Total_Count())).multiply(100)); //Natural: COMPUTE ROUNDED #PTOTAL-COUNT-OVER = ( #TOTAL-COUNT-OVER / #TOTAL-COUNT ) * 100
    }
    private void sub_Write_Gtotal_Lines() throws Exception                                                                                                                //Natural: WRITE-GTOTAL-LINES
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,"GRAND TOTAL OF WORK REQUESTS","COMPLETED IN 3 DAYS OR LESS.................:",ldaCwfl3802.getPnd_Total_Count_3()," (",ldaCwfl3802.getPnd_Ptotal_Count_3(),  //Natural: WRITE ( 1 ) 'GRAND TOTAL OF WORK REQUESTS' 'COMPLETED IN 3 DAYS OR LESS.................:' #TOTAL-COUNT-3 ' (' #PTOTAL-COUNT-3 ( EM = ZZ9.999 ) '% OF TOTAL WORK REQUESTS)' / 'GRAND TOTAL OF WORK REQUESTS' 'COMPLETED IN 4 DAYS OR LESS.................:' #TOTAL-COUNT-4 ' (' #PTOTAL-COUNT-4 ( EM = ZZ9.999 ) '% OF TOTAL WORK REQUESTS)' / 'GRAND TOTAL OF WORK REQUESTS' 'COMPLETED IN 5 DAYS OR LESS.................:' #TOTAL-COUNT-5 ' (' #PTOTAL-COUNT-5 ( EM = ZZ9.999 ) '% OF TOTAL WORK REQUESTS)' / 'GRAND TOTAL OF WORK REQUESTS' 'COMPLETED IN 6 - 10 DAYS....................:' #TOTAL-COUNT-10 ' (' #PTOTAL-COUNT-10 ( EM = ZZ9.999 ) '% OF TOTAL WORK REQUESTS)' / 'GRAND TOTAL OF WORK REQUESTS' 'COMPLETED IN OVER 10 DAYS...................:' #TOTAL-COUNT-OVER ' (' #PTOTAL-COUNT-OVER ( EM = ZZ9.999 ) '% OF TOTAL WORK REQUESTS)' / 'GRAND TOTAL.................' '............................................:' #TOTAL-COUNT / '_' ( 132 ) //
            new ReportEditMask ("ZZ9.999"),"% OF TOTAL WORK REQUESTS)",NEWLINE,"GRAND TOTAL OF WORK REQUESTS","COMPLETED IN 4 DAYS OR LESS.................:",ldaCwfl3802.getPnd_Total_Count_4()," (",ldaCwfl3802.getPnd_Ptotal_Count_4(), 
            new ReportEditMask ("ZZ9.999"),"% OF TOTAL WORK REQUESTS)",NEWLINE,"GRAND TOTAL OF WORK REQUESTS","COMPLETED IN 5 DAYS OR LESS.................:",ldaCwfl3802.getPnd_Total_Count_5()," (",ldaCwfl3802.getPnd_Ptotal_Count_5(), 
            new ReportEditMask ("ZZ9.999"),"% OF TOTAL WORK REQUESTS)",NEWLINE,"GRAND TOTAL OF WORK REQUESTS","COMPLETED IN 6 - 10 DAYS....................:",ldaCwfl3802.getPnd_Total_Count_10()," (",ldaCwfl3802.getPnd_Ptotal_Count_10(), 
            new ReportEditMask ("ZZ9.999"),"% OF TOTAL WORK REQUESTS)",NEWLINE,"GRAND TOTAL OF WORK REQUESTS","COMPLETED IN OVER 10 DAYS...................:",ldaCwfl3802.getPnd_Total_Count_Over()," (",ldaCwfl3802.getPnd_Ptotal_Count_Over(), 
            new ReportEditMask ("ZZ9.999"),"% OF TOTAL WORK REQUESTS)",NEWLINE,"GRAND TOTAL.................","............................................:",ldaCwfl3802.getPnd_Total_Count(),NEWLINE,"_",new 
            RepeatItem(132),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    ldaCwfl3802.getPnd_Page_Num().nadd(1);                                                                                                                //Natural: COMPUTE #PAGE-NUM = #PAGE-NUM + 1
                    ldaCwfl3802.getPnd_Rep_Unit_Cde().setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Close_Unit());                                                          //Natural: MOVE #WORK-RECORD.TBL-CLOSE-UNIT TO #REP-UNIT-CDE
                    DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), ldaCwfl3802.getPnd_Rep_Unit_Cde(), ldaCwfl3802.getPnd_Rep_Unit_Nme());                     //Natural: CALLNAT 'CWFN1103' #REP-UNIT-CDE #REP-UNIT-NME
                    if (condition(Global.isEscape())) return;
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),ldaCwfl3802.getPnd_Env(),new TabSetting(52),"CORPORATE WORKFLOW FACILITIES",new        //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 52T 'CORPORATE WORKFLOW FACILITIES' 124T 'PAGE' #PAGE-NUM ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 36T 'PREMIUM DIVISION  REPORT OF PARTICIPANT CLOSED WORK REQUESTS' 124T *TIMX ( EM = HH':'II' 'AP )
                        TabSetting(124),"PAGE",ldaCwfl3802.getPnd_Page_Num(), new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), 
                        new ReportEditMask ("LLL' 'DD','YY"),new TabSetting(36),"PREMIUM DIVISION  REPORT OF PARTICIPANT CLOSED WORK REQUESTS",new TabSetting(124),Global.getTIMX(), 
                        new ReportEditMask ("HH':'II' 'AP"));
                    if (condition(ldaCwfl3802.getPnd_Save_Moc_1().notEquals("9")))                                                                                        //Natural: IF #SAVE-MOC-1 NE '9'
                    {
                        ldaCwfl3802.getPnd_Save_Moc_1().setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Moc());                                                               //Natural: MOVE #WORK-RECORD.TBL-MOC TO #SAVE-MOC-1
                    }                                                                                                                                                     //Natural: END-IF
                    short decideConditionsMet432 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #OLD-MOC;//Natural: VALUE '1'
                    if (condition((ldaCwfl3802.getPnd_Old_Moc().equals("1"))))
                    {
                        decideConditionsMet432++;
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(52),"INTERNALLY GENERATED REQUESTS");                                                   //Natural: WRITE ( 1 ) 52T 'INTERNALLY GENERATED REQUESTS'
                    }                                                                                                                                                     //Natural: VALUE '2'
                    else if (condition((ldaCwfl3802.getPnd_Old_Moc().equals("2"))))
                    {
                        decideConditionsMet432++;
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(52),"EXTERNALLY GENERATED REQUESTS");                                                   //Natural: WRITE ( 1 ) 52T 'EXTERNALLY GENERATED REQUESTS'
                    }                                                                                                                                                     //Natural: VALUE '3'
                    else if (condition((ldaCwfl3802.getPnd_Old_Moc().equals("3"))))
                    {
                        decideConditionsMet432++;
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(50),"         ADJUSTMENTS");                                                            //Natural: WRITE ( 1 ) 50T '         ADJUSTMENTS'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(52),"         TOTALS PAGE");                                                            //Natural: WRITE ( 1 ) 52T '         TOTALS PAGE'
                    }                                                                                                                                                     //Natural: END-DECIDE
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(60),ldaCwfl3802.getPnd_Grand_Title2());                                                     //Natural: WRITE ( 1 ) 60T #GRAND-TITLE2
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    if (condition(ldaCwfl3802.getPnd_Page_Num().equals(1)))                                                                                               //Natural: IF #PAGE-NUM = 1
                    {
                        DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), ldaCwfl3802.getPnd_Rep_Unit_Cde(), ldaCwfl3802.getPnd_Unit_Name());                    //Natural: CALLNAT 'CWFN1103' #REP-UNIT-CDE #UNIT-NAME
                        if (condition(Global.isEscape())) return;
                        getReports().write(1, ReportOption.NOTITLE,"      UNIT:",ldaCwfl3802.getPnd_Unit_Name(),"(",ldaCwfl3802.getPnd_Rep_Unit_Cde(),                    //Natural: WRITE ( 1 ) '      UNIT:' #UNIT-NAME '(' #REP-UNIT-CDE ')'
                            ")");
                        getReports().write(1, ReportOption.NOTITLE,"START DATE:",ldaCwfl3802.getPnd_Start_Dte_Tme(), new ReportEditMask ("MM'/'DD'/'YY"),NEWLINE,"  END DATE:",ldaCwfl3802.getPnd_End_Dte_Tme(),  //Natural: WRITE ( 1 ) 'START DATE:' #START-DTE-TME ( EM = MM'/'DD'/'YY ) / '  END DATE:' #END-DTE-TME ( EM = MM'/'DD'/'YY )
                            new ReportEditMask ("MM'/'DD'/'YY"));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,"      UNIT:",ldaCwfl3802.getPnd_Unit_Name());                                                         //Natural: WRITE ( 1 ) '      UNIT:' #UNIT-NAME
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaCwfl3802_getPnd_Work_Record_Tbl_WpidIsBreak = ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid().isBreak(endOfData);
        boolean ldaCwfl3802_getPnd_Work_Record_Tbl_Wpid_ActIsBreak = ldaCwfl3802.getPnd_Work_Record_Tbl_Wpid_Act().isBreak(endOfData);
        boolean ldaCwfl3802_getPnd_Work_Record_Tbl_MocIsBreak = ldaCwfl3802.getPnd_Work_Record_Tbl_Moc().isBreak(endOfData);
        boolean ldaCwfl3802_getPnd_Work_Record_Tbl_Close_UnitIsBreak = ldaCwfl3802.getPnd_Work_Record_Tbl_Close_Unit().isBreak(endOfData);
        if (condition(ldaCwfl3802_getPnd_Work_Record_Tbl_WpidIsBreak || ldaCwfl3802_getPnd_Work_Record_Tbl_Wpid_ActIsBreak || ldaCwfl3802_getPnd_Work_Record_Tbl_MocIsBreak 
            || ldaCwfl3802_getPnd_Work_Record_Tbl_Close_UnitIsBreak))
        {
            if (condition(ldaCwfl3802.getPnd_Rep_Break_Ind().equals(" ")))                                                                                                //Natural: IF #REP-BREAK-IND = ' '
            {
                ldaCwfl3802.getPnd_Wpid().setValue(sort01Tbl_WpidOld);                                                                                                    //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID ) TO #WPID
                ldaCwfl3802.getPnd_Wpid_Count().setValue(sort01Tbl_WpidCount648);                                                                                         //Natural: MOVE COUNT ( #WORK-RECORD.TBL-WPID ) TO #WPID-COUNT
                ldaCwfl3802.getPnd_Wpid_Action().setValue(sort01Tbl_Wpid_ActOld);                                                                                         //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID-ACT ) TO #WPID-ACTION
                ldaCwfl3802.getPnd_Wpid_Ave().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Wpid_Ave()), ldaCwfl3802.getPnd_Wpid_Sum().divide(ldaCwfl3802.getPnd_Wpid_Count())); //Natural: COMPUTE #WPID-AVE = #WPID-SUM / #WPID-COUNT
                ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total_Num().nadd(ldaCwfl3802.getPnd_Wpid_Sum());                                                                //Natural: COMPUTE #AVE-TOTAL-NUM = #AVE-TOTAL-NUM + #WPID-SUM
                ldaCwfl3802.getPnd_Wpid_Sum().reset();                                                                                                                    //Natural: RESET #WPID-SUM
                ldaCwfl3802.getPnd_Wpid_Ave_Text().setValue(DbsUtil.compress("AVERAGE CORPORATE TURNAROUND FOR WPID", ldaCwfl3802.getPnd_Wpid()));                        //Natural: COMPRESS 'AVERAGE CORPORATE TURNAROUND FOR WPID' #WPID INTO #WPID-AVE-TEXT
                short decideConditionsMet657 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
                if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("B"))))
                {
                    decideConditionsMet657++;
                    ldaCwfl3802.getPnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL BOOKLETS COMPLETED FOR WPID", ldaCwfl3802.getPnd_Wpid()));                            //Natural: COMPRESS 'TOTAL BOOKLETS COMPLETED FOR WPID' #WPID INTO #WPID-TEXT
                    ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr()),              //Natural: COMPUTE #TBOOKLET-CTR = #BOOKLET-CNT ( 3 ) + #BOOKLET-CNT ( 4 ) + #BOOKLET-CNT ( 5 ) + #BOOKLET-CNT ( 10 ) + #BOOKLET-CNT ( 11 )
                        ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(3).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(4)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(5)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(10)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(11)));
                    if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr().greater(getZero())))                                                                   //Natural: IF #TBOOKLET-CTR GT 0
                    {
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(3)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CNT ( 3 ) = ( #BOOKLET-CNT ( 3 ) / #TBOOKLET-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(4)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CNT ( 4 ) = ( #BOOKLET-CNT ( 4 ) / #TBOOKLET-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(5)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CNT ( 5 ) = ( #BOOKLET-CNT ( 5 ) / #TBOOKLET-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(10)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CNT ( 10 ) = ( #BOOKLET-CNT ( 10 ) / #TBOOKLET-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(11)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CNT ( 11 ) = ( #BOOKLET-CNT ( 11 ) / #TBOOKLET-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL BOOKLETS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 3 BUSINESS DAYS OR LESS     :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL BOOKLETS COMPLETED FOR WPID' #WPID ' IN 3 BUSINESS DAYS OR LESS     :' #BOOKLET-CNT ( 3 ) ( AD = OU ) ' (' #PBOOKLET-CNT ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / 'TOTAL BOOKLETS COMPLETED FOR WPID' #WPID ' IN 4 BUSINESS DAYS OR LESS     :' #BOOKLET-CNT ( 4 ) ( AD = OU ) ' (' #PBOOKLET-CNT ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / 'TOTAL BOOKLETS COMPLETED FOR WPID' #WPID ' IN 5 BUSINESS DAYS OR LESS     :' #BOOKLET-CNT ( 5 ) ( AD = OU ) ' (' #PBOOKLET-CNT ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / 'TOTAL BOOKLETS COMPLETED FOR WPID' #WPID ' IN 6 - 10 BUSINESS DAYS        :' #BOOKLET-CNT ( 10 ) ( AD = OU ) ' (' #PBOOKLET-CNT ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / 'TOTAL BOOKLETS COMPLETED FOR WPID' #WPID ' IN OVER 10 BUSINESS DAYS       :' #BOOKLET-CNT ( 11 ) ( AD = OU ) ' (' #PBOOKLET-CNT ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS FOR WPID' #WPID ')' / #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL BOOKLETS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 4 BUSINESS DAYS OR LESS     :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(4), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL BOOKLETS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 5 BUSINESS DAYS OR LESS     :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(5), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL BOOKLETS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 6 - 10 BUSINESS DAYS        :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL BOOKLETS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN OVER 10 BUSINESS DAYS       :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Booklet_Cnt().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pbooklet_Cnt().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,ldaCwfl3802.getPnd_Wpid_Text(),"    ",ldaCwfl3802.getPnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("F"))))
                {
                    decideConditionsMet657++;
                    ldaCwfl3802.getPnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL FORMS SENT FOR WPID", ldaCwfl3802.getPnd_Wpid()));                                    //Natural: COMPRESS 'TOTAL FORMS SENT FOR WPID' #WPID INTO #WPID-TEXT
                    ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr()),                  //Natural: COMPUTE #TFORMS-CTR = #FORMS-CNT ( 3 ) + #FORMS-CNT ( 4 ) + #FORMS-CNT ( 5 ) + #FORMS-CNT ( 10 ) + #FORMS-CNT ( 11 )
                        ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(3).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(4)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(5)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(10)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(11)));
                    if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr().greater(getZero())))                                                                     //Natural: IF #TFORMS-CTR GT 0
                    {
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(3)),  //Natural: COMPUTE ROUNDED #PFORMS-CNT ( 3 ) = ( #FORMS-CNT ( 3 ) / #TFORMS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(4)),  //Natural: COMPUTE ROUNDED #PFORMS-CNT ( 4 ) = ( #FORMS-CNT ( 4 ) / #TFORMS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(5)),  //Natural: COMPUTE ROUNDED #PFORMS-CNT ( 5 ) = ( #FORMS-CNT ( 5 ) / #TFORMS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(10)),  //Natural: COMPUTE ROUNDED #PFORMS-CNT ( 10 ) = ( #FORMS-CNT ( 10 ) / #TFORMS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(11)),  //Natural: COMPUTE ROUNDED #PFORMS-CNT ( 11 ) = ( #FORMS-CNT ( 11 ) / #TFORMS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL FORMS SENT FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 3 BUSINESS DAYS OR LESS             :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL FORMS SENT FOR WPID' #WPID ' IN 3 BUSINESS DAYS OR LESS             :' #FORMS-CNT ( 3 ) ( AD = OU ) ' (' #PFORMS-CNT ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / 'TOTAL FORMS SENT FOR WPID' #WPID ' IN 4 BUSINESS DAYS OR LESS             :' #FORMS-CNT ( 4 ) ( AD = OU ) ' (' #PFORMS-CNT ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / 'TOTAL FORMS SENT FOR WPID' #WPID ' IN 5 BUSINESS DAYS OR LESS             :' #FORMS-CNT ( 5 ) ( AD = OU ) ' (' #PFORMS-CNT ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / 'TOTAL FORMS SENT FOR WPID' #WPID ' IN 6 - 10 BUSINESS DAYS                :' #FORMS-CNT ( 10 ) ( AD = OU ) ' (' #PFORMS-CNT ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / 'TOTAL FORMS SENT FOR WPID' #WPID ' IN OVER 10 BUSINESS DAYS               :' #FORMS-CNT ( 11 ) ( AD = OU ) ' (' #PFORMS-CNT ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS FOR WPID' #WPID ')' / #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL FORMS SENT FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 4 BUSINESS DAYS OR LESS             :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(4), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL FORMS SENT FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 5 BUSINESS DAYS OR LESS             :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(5), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL FORMS SENT FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 6 - 10 BUSINESS DAYS                :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL FORMS SENT FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN OVER 10 BUSINESS DAYS               :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Forms_Cnt().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pforms_Cnt().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,ldaCwfl3802.getPnd_Wpid_Text(),"    ",ldaCwfl3802.getPnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("I"))))
                {
                    decideConditionsMet657++;
                    ldaCwfl3802.getPnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL INQUIRIES COMPLETED FOR WPID", ldaCwfl3802.getPnd_Wpid()));                           //Natural: COMPRESS 'TOTAL INQUIRIES COMPLETED FOR WPID' #WPID INTO #WPID-TEXT
                    ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr()),              //Natural: COMPUTE #TINQUIRE-CTR = #INQUIRE-CNT ( 3 ) + #INQUIRE-CNT ( 4 ) + #INQUIRE-CNT ( 5 ) + #INQUIRE-CNT ( 10 ) + #INQUIRE-CNT ( 11 )
                        ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(3).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(4)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(5)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(10)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(11)));
                    if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr().greater(getZero())))                                                                   //Natural: IF #TINQUIRE-CTR GT 0
                    {
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(3)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CNT ( 3 ) = ( #INQUIRE-CNT ( 3 ) / #TINQUIRE-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(4)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CNT ( 4 ) = ( #INQUIRE-CNT ( 4 ) / #TINQUIRE-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(5)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CNT ( 5 ) = ( #INQUIRE-CNT ( 5 ) / #TINQUIRE-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(10)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CNT ( 10 ) = ( #INQUIRE-CNT ( 10 ) / #TINQUIRE-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(11)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CNT ( 11 ) = ( #INQUIRE-CNT ( 11 ) / #TINQUIRE-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL INQUIRIES COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 3 BUSINESS DAYS OR LESS    :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL INQUIRIES COMPLETED FOR WPID' #WPID ' IN 3 BUSINESS DAYS OR LESS    :' #INQUIRE-CNT ( 3 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / 'TOTAL INQUIRIES COMPLETED FOR WPID' #WPID ' IN 4 BUSINESS DAYS OR LESS    :' #INQUIRE-CNT ( 4 ) ( AD = OU ) ' (' #PINQUIRE-CNT ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / 'TOTAL INQUIRIES COMPLETED FOR WPID' #WPID ' IN 5 BUSINESS DAYS OR LESS    :' #INQUIRE-CNT ( 5 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / 'TOTAL INQUIRIES COMPLETED FOR WPID' #WPID ' IN 6 - 10 BUSINESS DAYS       :' #INQUIRE-CNT ( 10 ) ( AD = OU ) ' (' #PINQUIRE-CNT ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / 'TOTAL INQUIRIES COMPLETED FOR WPID' #WPID ' IN OVER 10 BUSINESS DAYS      :' #INQUIRE-CNT ( 11 ) ( AD = OU ) ' (' #PINQUIRE-CNT ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY FOR WPID' #WPID ')' / #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL INQUIRIES COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 4 BUSINESS DAYS OR LESS    :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(4), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL INQUIRIES COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 5 BUSINESS DAYS OR LESS    :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(5), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL INQUIRIES COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 6 - 10 BUSINESS DAYS       :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL INQUIRIES COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN OVER 10 BUSINESS DAYS      :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Inquire_Cnt().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pinquire_Cnt().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,ldaCwfl3802.getPnd_Wpid_Text(),"    ",ldaCwfl3802.getPnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("R"))))
                {
                    decideConditionsMet657++;
                    ldaCwfl3802.getPnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL RESEARCH COMPLETED FOR WPID", ldaCwfl3802.getPnd_Wpid()));                            //Natural: COMPRESS 'TOTAL RESEARCH COMPLETED FOR WPID' #WPID INTO #WPID-TEXT
                    ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr()),            //Natural: COMPUTE #TRESEARCH-CTR = #RESEARCH-CNT ( 3 ) + #RESEARCH-CNT ( 4 ) + #RESEARCH-CNT ( 5 ) + #RESEARCH-CNT ( 10 ) + #RESEARCH-CNT ( 11 )
                        ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(3).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(4)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(5)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(10)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(11)));
                    if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr().greater(getZero())))                                                                  //Natural: IF #TRESEARCH-CTR GT 0
                    {
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(3)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CNT ( 3 ) = ( #RESEARCH-CNT ( 3 ) / #TRESEARCH-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(4)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CNT ( 4 ) = ( #RESEARCH-CNT ( 4 ) / #TRESEARCH-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(5)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CNT ( 5 ) = ( #RESEARCH-CNT ( 5 ) / #TRESEARCH-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(10)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CNT ( 10 ) = ( #RESEARCH-CNT ( 10 ) / #TRESEARCH-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(11)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CNT ( 11 ) = ( #RESEARCH-CNT ( 11 ) / #TRESEARCH-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL RESEARCH COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 3 BUSINESS DAYS OR LESS     :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL RESEARCH COMPLETED FOR WPID' #WPID ' IN 3 BUSINESS DAYS OR LESS     :' #RESEARCH-CNT ( 3 ) ( AD = OU ) ' (' #PRESEARCH-CNT ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / 'TOTAL RESEARCH COMPLETED FOR WPID' #WPID ' IN 4 BUSINESS DAYS OR LESS     :' #RESEARCH-CNT ( 4 ) ( AD = OU ) ' (' #PRESEARCH-CNT ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / 'TOTAL RESEARCH COMPLETED FOR WPID' #WPID ' IN 5 BUSINESS DAYS OR LESS     :' #RESEARCH-CNT ( 5 ) ( AD = OU ) ' (' #PRESEARCH-CNT ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / 'TOTAL RESEARCH COMPLETED FOR WPID' #WPID ' IN 6 - 10 BUSINESS DAYS        :' #RESEARCH-CNT ( 10 ) ( AD = OU ) ' (' #PRESEARCH-CNT ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / 'TOTAL RESEARCH COMPLETED FOR WPID' #WPID ' IN OVER 10 BUSINESS DAYS       :' #RESEARCH-CNT ( 11 ) ( AD = OU ) ' (' #PRESEARCH-CNT ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH FOR WPID' #WPID ')' / #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL RESEARCH COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 4 BUSINESS DAYS OR LESS     :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(4), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL RESEARCH COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 5 BUSINESS DAYS OR LESS     :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(5), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL RESEARCH COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 6 - 10 BUSINESS DAYS        :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL RESEARCH COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN OVER 10 BUSINESS DAYS       :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Research_Cnt().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Presearch_Cnt().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,ldaCwfl3802.getPnd_Wpid_Text(),"    ",ldaCwfl3802.getPnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("T"))))
                {
                    decideConditionsMet657++;
                    ldaCwfl3802.getPnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL TRANSACTIONS COMPLETED FOR WPID", ldaCwfl3802.getPnd_Wpid()));                        //Natural: COMPRESS 'TOTAL TRANSACTIONS COMPLETED FOR WPID' #WPID INTO #WPID-TEXT
                    ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr()),                  //Natural: COMPUTE #TTRANS-CTR = #TRANS-CNT ( 3 ) + #TRANS-CNT ( 4 ) + #TRANS-CNT ( 5 ) + #TRANS-CNT ( 10 ) + #TRANS-CNT ( 11 )
                        ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(3).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(4)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(5)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(10)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(11)));
                    if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr().greater(getZero())))                                                                     //Natural: IF #TTRANS-CTR GT 0
                    {
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(3)),  //Natural: COMPUTE ROUNDED #PTRANS-CNT ( 3 ) = ( #TRANS-CNT ( 3 ) / #TTRANS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(4)),  //Natural: COMPUTE ROUNDED #PTRANS-CNT ( 4 ) = ( #TRANS-CNT ( 4 ) / #TTRANS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(5)),  //Natural: COMPUTE ROUNDED #PTRANS-CNT ( 5 ) = ( #TRANS-CNT ( 5 ) / #TTRANS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(10)),  //Natural: COMPUTE ROUNDED #PTRANS-CNT ( 10 ) = ( #TRANS-CNT ( 10 ) / #TTRANS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(11)),  //Natural: COMPUTE ROUNDED #PTRANS-CNT ( 11 ) = ( #TRANS-CNT ( 11 ) / #TTRANS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL TRANSACTIONS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid(),"IN 3 BUSINESS DAYS OR LESS  :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL TRANSACTIONS COMPLETED FOR WPID' #WPID 'IN 3 BUSINESS DAYS OR LESS  :' #TRANS-CNT ( 3 ) ( AD = OU ) ' (' #PTRANS-CNT ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / 'TOTAL TRANSACTIONS COMPLETED FOR WPID' #WPID 'IN 4 BUSINESS DAYS OR LESS  :' #TRANS-CNT ( 4 ) ( AD = OU ) ' (' #PTRANS-CNT ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / 'TOTAL TRANSACTIONS COMPLETED FOR WPID' #WPID 'IN 5 BUSINESS DAYS OR LESS  :' #TRANS-CNT ( 5 ) ( AD = OU ) ' (' #PTRANS-CNT ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / 'TOTAL TRANSACTIONS COMPLETED FOR WPID' #WPID 'IN 6 - 10 BUSINESS DAYS     :' #TRANS-CNT ( 10 ) ( AD = OU ) ' (' #PTRANS-CNT ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / 'TOTAL TRANSACTIONS COMPLETED FOR WPID' #WPID 'IN OVER 10 BUSINESS DAYS    :' #TRANS-CNT ( 11 ) ( AD = OU ) ' (' #PTRANS-CNT ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS FOR WPID' #WPID ')' / #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL TRANSACTIONS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid(),"IN 4 BUSINESS DAYS OR LESS  :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(4), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL TRANSACTIONS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid(),"IN 5 BUSINESS DAYS OR LESS  :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(5), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL TRANSACTIONS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid(),"IN 6 - 10 BUSINESS DAYS     :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL TRANSACTIONS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid(),"IN OVER 10 BUSINESS DAYS    :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Trans_Cnt().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Ptrans_Cnt().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,ldaCwfl3802.getPnd_Wpid_Text(),"    ",ldaCwfl3802.getPnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("X"))))
                {
                    decideConditionsMet657++;
                    ldaCwfl3802.getPnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL COMPLAINTS PROCESSED FOR WPID", ldaCwfl3802.getPnd_Wpid()));                          //Natural: COMPRESS 'TOTAL COMPLAINTS PROCESSED FOR WPID' #WPID INTO #WPID-TEXT
                    ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr()),          //Natural: COMPUTE #TCOMPLAINT-CTR = #COMPLAINT-CNT ( 3 ) + #COMPLAINT-CNT ( 4 ) + #COMPLAINT-CNT ( 11 )
                        ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(3).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(4)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(11)));
                    if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr().greater(getZero())))                                                                 //Natural: IF #TCOMPLAINT-CTR GT 0
                    {
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(3)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CNT ( 3 ) = ( #COMPLAINT-CNT ( 3 ) / #TCOMPLAINT-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(4)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CNT ( 4 ) = ( #COMPLAINT-CNT ( 4 ) / #TCOMPLAINT-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(5)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CNT ( 5 ) = ( #COMPLAINT-CNT ( 5 ) / #TCOMPLAINT-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(10)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CNT ( 10 ) = ( #COMPLAINT-CNT ( 10 ) / #TCOMPLAINT-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(11)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CNT ( 11 ) = ( #COMPLAINT-CNT ( 11 ) / #TCOMPLAINT-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL COMPLAINTS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 3 BUSINESS DAYS OR LESS   :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL COMPLAINTS COMPLETED FOR WPID' #WPID ' IN 3 BUSINESS DAYS OR LESS   :' #COMPLAINT-CNT ( 3 ) ( AD = OU ) ' (' #PCOMPLAINT-CNT ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / 'TOTAL COMPLAINTS COMPLETED FOR WPID' #WPID ' IN 4 BUSINESS DAYS OR LESS   :' #COMPLAINT-CNT ( 4 ) ( AD = OU ) ' (' #PCOMPLAINT-CNT ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / 'TOTAL COMPLAINTS COMPLETED FOR WPID' #WPID ' IN 5 BUSINESS DAYS OR LESS   :' #COMPLAINT-CNT ( 5 ) ( AD = OU ) ' (' #PCOMPLAINT-CNT ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / 'TOTAL COMPLAINTS COMPLETED FOR WPID' #WPID ' IN 6 - 10 BUSINESS DAYS      :' #COMPLAINT-CNT ( 10 ) ( AD = OU ) ' (' #PCOMPLAINT-CNT ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / 'TOTAL COMPLAINTS COMPLETED FOR WPID' #WPID ' IN OVER 10 BUSINESS DAYS     :' #COMPLAINT-CNT ( 11 ) ( AD = OU ) ' (' #PCOMPLAINT-CNT ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS FOR WPID' #WPID ')' / #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL COMPLAINTS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 4 BUSINESS DAYS OR LESS   :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(4), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL COMPLAINTS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 5 BUSINESS DAYS OR LESS   :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(5), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL COMPLAINTS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 6 - 10 BUSINESS DAYS      :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL COMPLAINTS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN OVER 10 BUSINESS DAYS     :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Complaint_Cnt().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pcomplaint_Cnt().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,ldaCwfl3802.getPnd_Wpid_Text(),"    ",ldaCwfl3802.getPnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("Z"))))
                {
                    decideConditionsMet657++;
                    ldaCwfl3802.getPnd_Wpid_Text().setValue(DbsUtil.compress("TOTAL OTHERS PROCESSED FOR WPID", ldaCwfl3802.getPnd_Wpid()));                              //Natural: COMPRESS 'TOTAL OTHERS PROCESSED FOR WPID' #WPID INTO #WPID-TEXT
                    ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr()),                //Natural: COMPUTE #TOTHERS-CTR = #OTHERS-CNT ( 3 ) + #OTHERS-CNT ( 4 ) + #OTHERS-CNT ( 5 ) + #OTHERS-CNT ( 10 ) + #OTHERS-CNT ( 11 )
                        ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(3).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(4)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(5)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(10)).add(ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(11)));
                    if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr().greater(getZero())))                                                                    //Natural: IF #TOTHERS-CTR GT 0
                    {
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(3)),  //Natural: COMPUTE ROUNDED #POTHERS-CNT ( 3 ) = ( #OTHERS-CNT ( 3 ) / #TOTHERS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(4)),  //Natural: COMPUTE ROUNDED #POTHERS-CNT ( 4 ) = ( #OTHERS-CNT ( 4 ) / #TOTHERS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(5)),  //Natural: COMPUTE ROUNDED #POTHERS-CNT ( 5 ) = ( #OTHERS-CNT ( 5 ) / #TOTHERS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(10)),  //Natural: COMPUTE ROUNDED #POTHERS-CNT ( 10 ) = ( #OTHERS-CNT ( 10 ) / #TOTHERS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr())).multiply(100));
                        ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(11)),  //Natural: COMPUTE ROUNDED #POTHERS-CNT ( 11 ) = ( #OTHERS-CNT ( 11 ) / #TOTHERS-CTR ) * 100
                            (ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr())).multiply(100));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL OTHERS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 3 BUSINESS DAYS OR LESS       :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL OTHERS COMPLETED FOR WPID' #WPID ' IN 3 BUSINESS DAYS OR LESS       :' #OTHERS-CNT ( 3 ) ( AD = OU ) ' (' #POTHERS-CNT ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS FOR WPID' #WPID ')' / 'TOTAL OTHERS COMPLETED FOR WPID' #WPID ' IN 4 BUSINESS DAYS OR LESS       :' #OTHERS-CNT ( 4 ) ( AD = OU ) ' (' #POTHERS-CNT ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS FOR WPID' #WPID ')' / 'TOTAL OTHERS COMPLETED FOR WPID' #WPID ' IN 5 BUSINESS DAYS OR LESS       :' #OTHERS-CNT ( 5 ) ( AD = OU ) ' (' #POTHERS-CNT ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS FOR WPID' #WPID ')' / 'TOTAL OTHERS COMPLETED FOR WPID' #WPID ' IN 6 - 10 BUSINESS DAYS          :' #OTHERS-CNT ( 10 ) ( AD = OU ) ' (' #POTHERS-CNT ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS FOR WPID' #WPID ')' / 'TOTAL OTHERS COMPLETED FOR WPID' #WPID ' IN OVER 10 BUSINESS DAYS         :' #OTHERS-CNT ( 11 ) ( AD = OU ) ' (' #POTHERS-CNT ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS FOR WPID' #WPID ')' / #WPID-TEXT '    ' #WPID-COUNT ( AD = OI )
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL OTHERS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 4 BUSINESS DAYS OR LESS       :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(4), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL OTHERS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 5 BUSINESS DAYS OR LESS       :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(5), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL OTHERS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN 6 - 10 BUSINESS DAYS          :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(10), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,"TOTAL OTHERS COMPLETED FOR WPID",ldaCwfl3802.getPnd_Wpid()," IN OVER 10 BUSINESS DAYS         :",ldaCwfl3802.getPnd_Array_Cnts_Pnd_Others_Cnt().getValue(11), 
                        new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Cnts_Pnd_Pothers_Cnt().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS FOR WPID",ldaCwfl3802.getPnd_Wpid(),")",NEWLINE,ldaCwfl3802.getPnd_Wpid_Text(),"    ",ldaCwfl3802.getPnd_Wpid_Count(), 
                        new FieldAttributes ("AD=OI"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet657 > 0))
                {
                    getReports().write(1, ReportOption.NOTITLE,ldaCwfl3802.getPnd_Wpid_Ave_Text(),"    ",ldaCwfl3802.getPnd_Wpid_Ave(), new FieldAttributes               //Natural: WRITE ( 1 ) #WPID-AVE-TEXT '    ' #WPID-AVE ( AD = OI ) /
                        ("AD=OI"),NEWLINE);
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                ldaCwfl3802.getPnd_Array_Cnts().getValue("*").reset();                                                                                                    //Natural: RESET #ARRAY-CNTS ( * ) #PERCENT-CNTS ( * )
                ldaCwfl3802.getPnd_Percent_Cnts().getValue("*").reset();
            }                                                                                                                                                             //Natural: END-IF
            sort01Tbl_WpidCount648.setDec(new DbsDecimal(0));                                                                                                             //Natural: END-BREAK
        }
        if (condition(ldaCwfl3802_getPnd_Work_Record_Tbl_Wpid_ActIsBreak || ldaCwfl3802_getPnd_Work_Record_Tbl_MocIsBreak || ldaCwfl3802_getPnd_Work_Record_Tbl_Close_UnitIsBreak))
        {
            ldaCwfl3802.getPnd_Total_Count().nadd(ldaCwfl3802.getPnd_Sub_Count());                                                                                        //Natural: COMPUTE #TOTAL-COUNT = #TOTAL-COUNT + #SUB-COUNT
            ldaCwfl3802.getPnd_Wpid_Action().setValue(sort01Tbl_Wpid_ActOld);                                                                                             //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID-ACT ) TO #WPID-ACTION
            short decideConditionsMet751 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
            if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("B"))))
            {
                decideConditionsMet751++;
                ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr()),                  //Natural: COMPUTE #TBOOKLET-CTR = #BOOKLET-CTR ( 3 ) + #BOOKLET-CTR ( 4 ) + #BOOKLET-CTR ( 5 ) + #BOOKLET-CTR ( 10 ) + #BOOKLET-CTR ( 11 )
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(3).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(4)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(5)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(10)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(11)));
                if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr().greater(getZero())))                                                                       //Natural: IF #TBOOKLET-CTR GT 0
                {
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 3 ) = ( #BOOKLET-CTR ( 3 ) / #TBOOKLET-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 4 ) = ( #BOOKLET-CTR ( 4 ) / #TBOOKLET-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 5 ) = ( #BOOKLET-CTR ( 5 ) / #TBOOKLET-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 10 ) = ( #BOOKLET-CTR ( 10 ) / #TBOOKLET-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #PBOOKLET-CTR ( 11 ) = ( #BOOKLET-CTR ( 11 ) / #TBOOKLET-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tbooklet_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL BOOKLETS COMPLETED IN 3 BUSINESS DAYS OR LESS                      :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL BOOKLETS COMPLETED IN 3 BUSINESS DAYS OR LESS                      :' #BOOKLET-CTR ( 3 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS COMPLETED IN 4 BUSINESS DAYS OR LESS                      :' #BOOKLET-CTR ( 4 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS COMPLETED IN 5 BUSINESS DAYS OR LESS                      :' #BOOKLET-CTR ( 5 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS COMPLETED IN 6 - 10 BUSINESS DAYS                         :' #BOOKLET-CTR ( 10 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS COMPLETED IN OVER 10 BUSINESS DAYS                        :' #BOOKLET-CTR ( 11 ) ( AD = OU ) ' (' #PBOOKLET-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL BOOKLETS)' / 'TOTAL BOOKLETS COMPLETED                                                 :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS COMPLETED IN 4 BUSINESS DAYS OR LESS                      :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS COMPLETED IN 5 BUSINESS DAYS OR LESS                      :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS COMPLETED IN 6 - 10 BUSINESS DAYS                         :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(10), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS COMPLETED IN OVER 10 BUSINESS DAYS                        :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue(11), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pbooklet_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL BOOKLETS)",NEWLINE,"TOTAL BOOKLETS COMPLETED                                                 :",ldaCwfl3802.getPnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("F"))))
            {
                decideConditionsMet751++;
                ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr()), ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(3).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(4)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(5)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(10)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(11))); //Natural: COMPUTE #TFORMS-CTR = #FORMS-CTR ( 3 ) + #FORMS-CTR ( 4 ) + #FORMS-CTR ( 5 ) + #FORMS-CTR ( 10 ) + #FORMS-CTR ( 11 )
                if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr().greater(getZero())))                                                                         //Natural: IF #TFORMS-CTR GT 0
                {
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 3 ) = ( #FORMS-CTR ( 3 ) / #TFORMS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 4 ) = ( #FORMS-CTR ( 4 ) / #TFORMS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 5 ) = ( #FORMS-CTR ( 5 ) / #TFORMS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 10 ) = ( #FORMS-CTR ( 10 ) / #TFORMS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #PFORMS-CTR ( 11 ) = ( #FORMS-CTR ( 11 ) / #TFORMS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tforms_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL FORMS SENT IN 3 BUSINESS DAYS OR LESS                              :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL FORMS SENT IN 3 BUSINESS DAYS OR LESS                              :' #FORMS-CTR ( 3 ) ( AD = OU ) ' (' #PFORMS-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS SENT IN 4 BUSINESS DAYS OR LESS                              :' #FORMS-CTR ( 4 ) ( AD = OU ) ' (' #PFORMS-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS SENT IN 5 BUSINESS DAYS OR LESS                              :' #FORMS-CTR ( 5 ) ( AD = OU ) ' (' #PFORMS-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS SENT IN 6 - 10 BUSINESS DAYS                                 :' #FORMS-CTR ( 10 ) ( AD = OU ) ' (' #PFORMS-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS SENT IN OVER 10 BUSINESS DAYS                                :' #FORMS-CTR ( 11 ) ( AD = OU ) ' (' #PFORMS-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL FORMS)' / 'TOTAL FORMS SENT                                                         :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS SENT IN 4 BUSINESS DAYS OR LESS                              :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS SENT IN 5 BUSINESS DAYS OR LESS                              :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS SENT IN 6 - 10 BUSINESS DAYS                                 :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(10), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS SENT IN OVER 10 BUSINESS DAYS                                :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue(11), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pforms_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL FORMS)",NEWLINE,"TOTAL FORMS SENT                                                         :",ldaCwfl3802.getPnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("I"))))
            {
                decideConditionsMet751++;
                ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr()),                  //Natural: COMPUTE #TINQUIRE-CTR = #INQUIRE-CTR ( 3 ) + #INQUIRE-CTR ( 4 ) + #INQUIRE-CTR ( 5 ) + #INQUIRE-CTR ( 10 ) + #INQUIRE-CTR ( 11 )
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(3).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(4)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(5)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(10)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(11)));
                if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr().greater(getZero())))                                                                       //Natural: IF #TINQUIRE-CTR GT 0
                {
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 3 ) = ( #INQUIRE-CTR ( 3 ) / #TINQUIRE-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 4 ) = ( #INQUIRE-CTR ( 4 ) / #TINQUIRE-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 5 ) = ( #INQUIRE-CTR ( 5 ) / #TINQUIRE-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 10 ) = ( #INQUIRE-CTR ( 10 ) / #TINQUIRE-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #PINQUIRE-CTR ( 11 ) = ( #INQUIRE-CTR ( 11 ) / #TINQUIRE-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tinquire_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL INQUIRIES COMPLETED IN 3 BUSINESS DAYS OR LESS                     :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL INQUIRIES COMPLETED IN 3 BUSINESS DAYS OR LESS                     :' #INQUIRE-CTR ( 3 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES COMPLETED IN 4 BUSINESS DAYS OR LESS                     :' #INQUIRE-CTR ( 4 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES COMPLETED IN 5 BUSINESS DAYS OR LESS                     :' #INQUIRE-CTR ( 5 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES COMPLETED IN 6 - 10 BUSINESS DAYS                        :' #INQUIRE-CTR ( 10 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES COMPLETED IN OVER 10 BUSINESS DAYS                       :' #INQUIRE-CTR ( 11 ) ( AD = OU ) ' (' #PINQUIRE-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL INQUIRY)' / 'TOTAL INQUIRIES COMPLETED                                                :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES COMPLETED IN 4 BUSINESS DAYS OR LESS                     :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES COMPLETED IN 5 BUSINESS DAYS OR LESS                     :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES COMPLETED IN 6 - 10 BUSINESS DAYS                        :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(10), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES COMPLETED IN OVER 10 BUSINESS DAYS                       :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue(11), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pinquire_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL INQUIRY)",NEWLINE,"TOTAL INQUIRIES COMPLETED                                                :",ldaCwfl3802.getPnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("R"))))
            {
                decideConditionsMet751++;
                ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr()),                //Natural: COMPUTE #TRESEARCH-CTR = #RESEARCH-CTR ( 3 ) + #RESEARCH-CTR ( 4 ) + #RESEARCH-CTR ( 5 ) + #RESEARCH-CTR ( 10 ) + #RESEARCH-CTR ( 11 )
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(3).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(4)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(5)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(10)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(11)));
                if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr().greater(getZero())))                                                                      //Natural: IF #TRESEARCH-CTR GT 0
                {
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 3 ) = ( #RESEARCH-CTR ( 3 ) / #TRESEARCH-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 4 ) = ( #RESEARCH-CTR ( 4 ) / #TRESEARCH-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 5 ) = ( #RESEARCH-CTR ( 5 ) / #TRESEARCH-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 10 ) = ( #RESEARCH-CTR ( 10 ) / #TRESEARCH-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #PRESEARCH-CTR ( 11 ) = ( #RESEARCH-CTR ( 11 ) / #TRESEARCH-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tresearch_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL RESEARCH COMPLETED IN 3 BUSINESS DAYS OR LESS                      :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL RESEARCH COMPLETED IN 3 BUSINESS DAYS OR LESS                      :' #RESEARCH-CTR ( 3 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH COMPLETED IN 4 BUSINESS DAYS OR LESS                      :' #RESEARCH-CTR ( 4 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH COMPLETED IN 5 BUSINESS DAYS OR LESS                      :' #RESEARCH-CTR ( 5 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH COMPLETED IN 6 - 10 BUSINESS DAYS                         :' #RESEARCH-CTR ( 10 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH COMPLETED IN OVER 10 BUSINESS DAYS                        :' #RESEARCH-CTR ( 11 ) ( AD = OU ) ' (' #PRESEARCH-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL RESEARCH)' / 'TOTAL RESEARCH COMPLETED                                                 :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH COMPLETED IN 4 BUSINESS DAYS OR LESS                      :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH COMPLETED IN 5 BUSINESS DAYS OR LESS                      :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH COMPLETED IN 6 - 10 BUSINESS DAYS                         :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(10), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH COMPLETED IN OVER 10 BUSINESS DAYS                        :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue(11), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Presearch_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL RESEARCH)",NEWLINE,"TOTAL RESEARCH COMPLETED                                                 :",ldaCwfl3802.getPnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("T"))))
            {
                decideConditionsMet751++;
                ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr()), ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(3).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(4)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(5)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(10)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(11))); //Natural: COMPUTE #TTRANS-CTR = #TRANS-CTR ( 3 ) + #TRANS-CTR ( 4 ) + #TRANS-CTR ( 5 ) + #TRANS-CTR ( 10 ) + #TRANS-CTR ( 11 )
                if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr().greater(getZero())))                                                                         //Natural: IF #TTRANS-CTR GT 0
                {
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 3 ) = ( #TRANS-CTR ( 3 ) / #TTRANS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 4 ) = ( #TRANS-CTR ( 4 ) / #TTRANS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 5 ) = ( #TRANS-CTR ( 5 ) / #TTRANS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 10 ) = ( #TRANS-CTR ( 10 ) / #TTRANS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #PTRANS-CTR ( 11 ) = ( #TRANS-CTR ( 11 ) / #TTRANS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Ttrans_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL TRANSACTIONS COMPLETED IN 3 BUSINESS DAYS OR LESS                  :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL TRANSACTIONS COMPLETED IN 3 BUSINESS DAYS OR LESS                  :' #TRANS-CTR ( 3 ) ( AD = OU ) ' (' #PTRANS-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS COMPLETED IN 4 BUSINESS DAYS OR LESS                  :' #TRANS-CTR ( 4 ) ( AD = OU ) ' (' #PTRANS-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS COMPLETED IN 5 BUSINESS DAYS OR LESS                  :' #TRANS-CTR ( 5 ) ( AD = OU ) ' (' #PTRANS-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS COMPLETED IN 6 - 10 BUSINESS DAYS                     :' #TRANS-CTR ( 10 ) ( AD = OU ) ' (' #PTRANS-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS COMPLETED IN OVER 10 BUSINESS DAYS                    :' #TRANS-CTR ( 11 ) ( AD = OU ) ' (' #PTRANS-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL TRANSACTIONS)' / 'TOTAL TRANSACTIONS COMPLETED                                             :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS COMPLETED IN 4 BUSINESS DAYS OR LESS                  :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS COMPLETED IN 5 BUSINESS DAYS OR LESS                  :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS COMPLETED IN 6 - 10 BUSINESS DAYS                     :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(10), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS COMPLETED IN OVER 10 BUSINESS DAYS                    :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue(11), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Ptrans_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL TRANSACTIONS)",NEWLINE,"TOTAL TRANSACTIONS COMPLETED                                             :",ldaCwfl3802.getPnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("X"))))
            {
                decideConditionsMet751++;
                ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr()),              //Natural: COMPUTE #TCOMPLAINT-CTR = #COMPLAINT-CTR ( 3 ) + #COMPLAINT-CTR ( 4 ) + #COMPLAINT-CTR ( 5 ) + #COMPLAINT-CTR ( 10 ) + #COMPLAINT-CTR ( 11 )
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(3).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(4)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(5)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(10)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(11)));
                if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr().greater(getZero())))                                                                     //Natural: IF #TCOMPLAINT-CTR GT 0
                {
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 3 ) = ( #COMPLAINT-CTR ( 3 ) / #TCOMPLAINT-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 4 ) = ( #COMPLAINT-CTR ( 4 ) / #TCOMPLAINT-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 5 ) = ( #COMPLAINT-CTR ( 5 ) / #TCOMPLAINT-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 10 ) = ( #COMPLAINT-CTR ( 10 ) / #TCOMPLAINT-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #PCOMPLAINT-CTR ( 11 ) = ( #COMPLAINT-CTR ( 11 ) / #TCOMPLAINT-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tcomplaint_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL COMPLAINTS PROCESSED IN 3 BUSINESS DAYS OR LESS                    :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL COMPLAINTS PROCESSED IN 3 BUSINESS DAYS OR LESS                    :' #COMPLAINT-CTR ( 3 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS PROCESSED IN 4 BUSINESS DAYS OR LESS                    :' #COMPLAINT-CTR ( 4 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS PROCESSED IN 5 BUSINESS DAYS OR LESS                    :' #COMPLAINT-CTR ( 5 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS PROCESSED IN 6 - 10 BUSINESS DAYS                       :' #COMPLAINT-CTR ( 10 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)' / 'TOTAL COMPLAINTS PROCESSED IN OVER 10 BUSINESS DAYS                      :' #COMPLAINT-CTR ( 11 ) ( AD = OU ) ' (' #PCOMPLAINT-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL COMPLAINTS)'/ 'TOTAL COMPLAINTS PROCESSED                                               :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS PROCESSED IN 4 BUSINESS DAYS OR LESS                    :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS PROCESSED IN 5 BUSINESS DAYS OR LESS                    :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS PROCESSED IN 6 - 10 BUSINESS DAYS                       :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(10), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS PROCESSED IN OVER 10 BUSINESS DAYS                      :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue(11), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pcomplaint_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL COMPLAINTS)",NEWLINE,"TOTAL COMPLAINTS PROCESSED                                               :",ldaCwfl3802.getPnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'Z'
            else if (condition((ldaCwfl3802.getPnd_Wpid_Action().equals("Z"))))
            {
                decideConditionsMet751++;
                ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr()),                    //Natural: COMPUTE #TOTHERS-CTR = #OTHERS-CTR ( 3 ) + #OTHERS-CTR ( 4 ) + #OTHERS-CTR ( 5 ) + #OTHERS-CTR ( 10 ) + #OTHERS-CTR ( 11 )
                    ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(3).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(4)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(5)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(10)).add(ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(11)));
                if (condition(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr().greater(getZero())))                                                                        //Natural: IF #TOTHERS-CTR GT 0
                {
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(3).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(3)),  //Natural: COMPUTE ROUNDED #POTHERS-CTR ( 3 ) = ( #OTHERS-CTR ( 3 ) / #TOTHERS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(3).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(4).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(4)),  //Natural: COMPUTE ROUNDED #POTHERS-CTR ( 4 ) = ( #OTHERS-CTR ( 4 ) / #TOTHERS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(4).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(5).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(5)),  //Natural: COMPUTE ROUNDED #POTHERS-CTR ( 5 ) = ( #OTHERS-CTR ( 5 ) / #TOTHERS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(5).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(10).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(10)),  //Natural: COMPUTE ROUNDED #POTHERS-CTR ( 10 ) = ( #OTHERS-CTR ( 10 ) / #TOTHERS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(10).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr())).multiply(100));
                    ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(11).compute(new ComputeParameters(true, ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(11)),  //Natural: COMPUTE ROUNDED #POTHERS-CTR ( 11 ) = ( #OTHERS-CTR ( 11 ) / #TOTHERS-CTR ) * 100
                        (ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(11).divide(ldaCwfl3802.getPnd_Total_Ctrs_Pnd_Tothers_Ctr())).multiply(100));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL OTHERS PROCESSED IN 3 BUSINESS DAYS OR LESS                        :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(3),  //Natural: WRITE ( 1 ) / 'TOTAL OTHERS PROCESSED IN 3 BUSINESS DAYS OR LESS                        :' #OTHERS-CTR ( 3 ) ( AD = OU ) ' (' #POTHERS-CTR ( 3 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS)' / 'TOTAL OTHERS PROCESSED IN 4 BUSINESS DAYS OR LESS                        :' #OTHERS-CTR ( 4 ) ( AD = OU ) ' (' #POTHERS-CTR ( 4 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS)' / 'TOTAL OTHERS PROCESSED IN 5 BUSINESS DAYS OR LESS                        :' #OTHERS-CTR ( 5 ) ( AD = OU ) ' (' #POTHERS-CTR ( 5 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS)' / 'TOTAL OTHERS PROCESSED IN 6 - 10 BUSINESS DAYS                           :' #OTHERS-CTR ( 10 ) ( AD = OU ) ' (' #POTHERS-CTR ( 10 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS)' / 'TOTAL OTHERS PROCESSED IN OVER 10 BUSINESS DAYS                          :' #OTHERS-CTR ( 11 ) ( AD = OU ) ' (' #POTHERS-CTR ( 11 ) ( EM = ZZ9.999 ) '% OF TOTAL OTHERS)' / 'TOTAL OTHERS PROCESSED                                                   :' #SUB-COUNT ( AD = OU )
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(3), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS)",NEWLINE,"TOTAL OTHERS PROCESSED IN 4 BUSINESS DAYS OR LESS                        :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(4), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(4), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS)",NEWLINE,"TOTAL OTHERS PROCESSED IN 5 BUSINESS DAYS OR LESS                        :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(5), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(5), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS)",NEWLINE,"TOTAL OTHERS PROCESSED IN 6 - 10 BUSINESS DAYS                           :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(10), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(10), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS)",NEWLINE,"TOTAL OTHERS PROCESSED IN OVER 10 BUSINESS DAYS                          :",ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue(11), 
                    new FieldAttributes ("AD=OU")," (",ldaCwfl3802.getPnd_Percent_Ctrs_Pnd_Pothers_Ctr().getValue(11), new ReportEditMask ("ZZ9.999"),"% OF TOTAL OTHERS)",NEWLINE,"TOTAL OTHERS PROCESSED                                                   :",ldaCwfl3802.getPnd_Sub_Count(), 
                    new FieldAttributes ("AD=OU"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaCwfl3802.getPnd_Sub_Count().reset();                                                                                                                       //Natural: RESET #SUB-COUNT #PERCENT-CTRS ( * ) #ARRAY-CTRS ( * )
            ldaCwfl3802.getPnd_Percent_Ctrs().getValue("*").reset();
            ldaCwfl3802.getPnd_Array_Ctrs().getValue("*").reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaCwfl3802_getPnd_Work_Record_Tbl_MocIsBreak || ldaCwfl3802_getPnd_Work_Record_Tbl_Close_UnitIsBreak))
        {
            ldaCwfl3802.getPnd_Old_Moc().setValue(sort01Tbl_MocOld);                                                                                                      //Natural: MOVE OLD ( #WORK-RECORD.TBL-MOC ) TO #OLD-MOC
            ldaCwfl3802.getPnd_Combine_Unit().setValue(sort01Tbl_Close_UnitOld);                                                                                          //Natural: MOVE OLD ( #WORK-RECORD.TBL-CLOSE-UNIT ) TO #COMBINE-UNIT
            short decideConditionsMet832 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #OLD-MOC;//Natural: VALUE '1'
            if (condition((ldaCwfl3802.getPnd_Old_Moc().equals("1"))))
            {
                decideConditionsMet832++;
                ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc().setValue(sort01Tbl_MocCount829);                                                                        //Natural: MOVE COUNT ( #WORK-RECORD.TBL-MOC ) TO #TOTAL-MOC
                ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total()),                //Natural: COMPUTE #AVE-TOTAL = #AVE-TOTAL-NUM / #TOTAL-MOC
                    ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total_Num().divide(ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc()));
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total Internally Generated ","Requests Completed in 3 Business Days or Less:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_3(), //Natural: WRITE ( 1 ) / 'Total Internally Generated ' 'Requests Completed in 3 Business Days or Less:' #TOTAL-MOC-3 / 'Total Internally Generated ' 'Requests Completed in 4 Business Days or Less:' #TOTAL-MOC-4 / 'Total Internally Generated ' 'Requests Completed in 5 Business Days or Less:' #TOTAL-MOC-5 / 'Total Internally Generated ' 'Requests Completed in 6 - 10 Business Days...:' #TOTAL-MOC-10 / 'Total Internally Generated ' 'Requests Completed in over 10 Business Days..:' #TOTAL-MOC-OVER / 'TOTAL INTERNALLY GENERATED ' 'REQUESTS COMPLETED ..........................:' #TOTAL-MOC / 'AVERAGE CORPORATE TURNAROUND' 'FOR INTERNALLY GENERATED REQUESTS...........:' #AVE-TOTAL
                    NEWLINE,"Total Internally Generated ","Requests Completed in 4 Business Days or Less:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_4(),
                    NEWLINE,"Total Internally Generated ","Requests Completed in 5 Business Days or Less:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_5(),
                    NEWLINE,"Total Internally Generated ","Requests Completed in 6 - 10 Business Days...:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_10(),
                    NEWLINE,"Total Internally Generated ","Requests Completed in over 10 Business Days..:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_Over(),
                    NEWLINE,"TOTAL INTERNALLY GENERATED ","REQUESTS COMPLETED ..........................:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc(),
                    NEWLINE,"AVERAGE CORPORATE TURNAROUND","FOR INTERNALLY GENERATED REQUESTS...........:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total());
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((ldaCwfl3802.getPnd_Old_Moc().equals("2"))))
            {
                decideConditionsMet832++;
                ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc().setValue(sort01Tbl_MocCount829);                                                                        //Natural: MOVE COUNT ( #WORK-RECORD.TBL-MOC ) TO #TOTAL-MOC
                ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total()),                //Natural: COMPUTE #AVE-TOTAL = #AVE-TOTAL-NUM / #TOTAL-MOC
                    ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total_Num().divide(ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc()));
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total Externally Generated ","Requests Completed in 3 Business Days or Less:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_3(), //Natural: WRITE ( 1 ) / 'Total Externally Generated ' 'Requests Completed in 3 Business Days or Less:' #TOTAL-MOC-3 / 'Total Externally Generated ' 'Requests Completed in 4 Business Days or Less:' #TOTAL-MOC-4 / 'Total Externally Generated ' 'Requests Completed in 5 Business Days or Less:' #TOTAL-MOC-5 / 'Total Externally Generated ' 'Requests Completed in 6 - 10 Business Days...:' #TOTAL-MOC-10 / 'Total Externally Generated ' 'Requests Completed in over 10 Business Days..:' #TOTAL-MOC-OVER / 'TOTAL EXTERNALLY GENERATED ' 'REQUESTS COMPLETED ..........................:' #TOTAL-MOC / 'AVERAGE CORPORATE TURNAROUND' 'FOR EXTERNALLY GENERATED REQUESTS...........:' #AVE-TOTAL
                    NEWLINE,"Total Externally Generated ","Requests Completed in 4 Business Days or Less:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_4(),
                    NEWLINE,"Total Externally Generated ","Requests Completed in 5 Business Days or Less:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_5(),
                    NEWLINE,"Total Externally Generated ","Requests Completed in 6 - 10 Business Days...:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_10(),
                    NEWLINE,"Total Externally Generated ","Requests Completed in over 10 Business Days..:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_Over(),
                    NEWLINE,"TOTAL EXTERNALLY GENERATED ","REQUESTS COMPLETED ..........................:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc(),
                    NEWLINE,"AVERAGE CORPORATE TURNAROUND","FOR EXTERNALLY GENERATED REQUESTS...........:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total());
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((ldaCwfl3802.getPnd_Old_Moc().equals("3"))))
            {
                decideConditionsMet832++;
                ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc().setValue(sort01Tbl_MocCount829);                                                                        //Natural: MOVE COUNT ( #WORK-RECORD.TBL-MOC ) TO #TOTAL-MOC
                ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total().compute(new ComputeParameters(false, ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total()),                //Natural: COMPUTE #AVE-TOTAL = #AVE-TOTAL-NUM / #TOTAL-MOC
                    ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total_Num().divide(ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc()));
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total Adustments Completed ","in 3 bsnss Days or Less......................:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_3(), //Natural: WRITE ( 1 ) / 'Total Adustments Completed ' 'in 3 bsnss Days or Less......................:' #TOTAL-MOC-3 / 'Total Adustments Completed ' 'in 4 bsnss Days or Less......................:' #TOTAL-MOC-4 / 'Total Adustments Completed ' 'in 5 bsnss Days or Less......................:' #TOTAL-MOC-5 / 'Total Adustments Completed ' 'in 6 - 10 bsnss Days.........................:' #TOTAL-MOC-10 / 'Total Adustments Completed ' 'in over 10 bsnss Days........................:' #TOTAL-MOC-OVER / 'TOTAL ADJUSTMENTS COMPLETED' '.............................................:' #TOTAL-MOC / 'AVERAGE CORPORATE TURNAROUND' 'FOR ADJUSTMENTS.............................:' #AVE-TOTAL
                    NEWLINE,"Total Adustments Completed ","in 4 bsnss Days or Less......................:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_4(),
                    NEWLINE,"Total Adustments Completed ","in 5 bsnss Days or Less......................:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_5(),
                    NEWLINE,"Total Adustments Completed ","in 6 - 10 bsnss Days.........................:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_10(),
                    NEWLINE,"Total Adustments Completed ","in over 10 bsnss Days........................:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_Over(),
                    NEWLINE,"TOTAL ADJUSTMENTS COMPLETED",".............................................:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc(),
                    NEWLINE,"AVERAGE CORPORATE TURNAROUND","FOR ADJUSTMENTS.............................:",ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total());
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(ldaCwfl3802.getPnd_Combine_Unit().equals("AIIVC2A") || ldaCwfl3802.getPnd_Combine_Unit().equals("AIIVC2B") || ldaCwfl3802.getPnd_Combine_Unit().equals("APC  1A")  //Natural: IF #COMBINE-UNIT = 'AIIVC2A' OR = 'AIIVC2B' OR = 'APC  1A' OR = 'APC  1B' OR = 'APC  2A' OR = 'APC  2B'
                || ldaCwfl3802.getPnd_Combine_Unit().equals("APC  1B") || ldaCwfl3802.getPnd_Combine_Unit().equals("APC  2A") || ldaCwfl3802.getPnd_Combine_Unit().equals("APC  2B")))
            {
                DbsUtil.callnat(Cwfn3803.class , getCurrentProcessState(), ldaCwfl3802.getPnd_Gtot_Registers(), ldaCwfl3802.getPnd_Combined_Totals(),                     //Natural: CALLNAT 'CWFN3803' #GTOT-REGISTERS #COMBINED-TOTALS #OLD-MOC
                    ldaCwfl3802.getPnd_Old_Moc());
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc().reset();                                                                                                    //Natural: RESET #TOTAL-MOC #TOTAL-MOC-3 #TOTAL-MOC-4 #TOTAL-MOC-5 #TOTAL-MOC-10 #TOTAL-MOC-OVER #AVE-TOTAL #AVE-TOTAL-NUM
            ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_3().reset();
            ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_4().reset();
            ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_5().reset();
            ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_10().reset();
            ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc_Over().reset();
            ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total().reset();
            ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Ave_Total_Num().reset();
            ldaCwfl3802.getPnd_Old_Moc().setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Moc());                                                                              //Natural: MOVE #WORK-RECORD.TBL-MOC TO #OLD-MOC
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            sort01Tbl_MocCount829.setDec(new DbsDecimal(0));                                                                                                              //Natural: END-BREAK
        }
        if (condition(ldaCwfl3802_getPnd_Work_Record_Tbl_Close_UnitIsBreak))
        {
            ldaCwfl3802.getPnd_Save_Moc_1().setValue("9");                                                                                                                //Natural: MOVE '9' TO #SAVE-MOC-1
            ldaCwfl3802.getPnd_Rep_Unit_Cde().setValue(sort01Tbl_Close_UnitOld);                                                                                          //Natural: MOVE OLD ( #WORK-RECORD.TBL-CLOSE-UNIT ) TO #REP-UNIT-CDE
            if (condition(ldaCwfl3802.getPnd_Rep_Unit_Cde().equals("AIIVC2A") || ldaCwfl3802.getPnd_Rep_Unit_Cde().equals("AIIVC2B") || ldaCwfl3802.getPnd_Rep_Unit_Cde().equals("APC  1A")  //Natural: IF #REP-UNIT-CDE = 'AIIVC2A' OR = 'AIIVC2B' OR = 'APC  1A' OR = 'APC  1B' OR = 'APC  2A' OR = 'APC  2B'
                || ldaCwfl3802.getPnd_Rep_Unit_Cde().equals("APC  1B") || ldaCwfl3802.getPnd_Rep_Unit_Cde().equals("APC  2A") || ldaCwfl3802.getPnd_Rep_Unit_Cde().equals("APC  2B")))
            {
                ldaCwfl3802.getPnd_Ctot_Count_3().nadd(ldaCwfl3802.getPnd_Total_Count_3());                                                                               //Natural: COMPUTE #CTOT-COUNT-3 = #CTOT-COUNT-3 + #TOTAL-COUNT-3
                ldaCwfl3802.getPnd_Ctot_Count_4().nadd(ldaCwfl3802.getPnd_Total_Count_4());                                                                               //Natural: COMPUTE #CTOT-COUNT-4 = #CTOT-COUNT-4 + #TOTAL-COUNT-4
                ldaCwfl3802.getPnd_Ctot_Count_5().nadd(ldaCwfl3802.getPnd_Total_Count_5());                                                                               //Natural: COMPUTE #CTOT-COUNT-5 = #CTOT-COUNT-5 + #TOTAL-COUNT-5
                ldaCwfl3802.getPnd_Ctot_Count_10().nadd(ldaCwfl3802.getPnd_Total_Count_10());                                                                             //Natural: COMPUTE #CTOT-COUNT-10 = #CTOT-COUNT-10 + #TOTAL-COUNT-10
                ldaCwfl3802.getPnd_Ctot_Count_Over().nadd(ldaCwfl3802.getPnd_Total_Count_Over());                                                                         //Natural: COMPUTE #CTOT-COUNT-OVER = #CTOT-COUNT-OVER + #TOTAL-COUNT-OVER
                ldaCwfl3802.getPnd_Ctot_Count().nadd(ldaCwfl3802.getPnd_Total_Count());                                                                                   //Natural: COMPUTE #CTOT-COUNT = #CTOT-COUNT + #TOTAL-COUNT
                ldaCwfl3802.getPnd_Comp_Unit().setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Close_Unit());                                                                 //Natural: MOVE #WORK-RECORD.TBL-CLOSE-UNIT TO #COMP-UNIT
                if (condition(ldaCwfl3802.getPnd_Comp_Unit().equals("AIIVC2A") || ldaCwfl3802.getPnd_Comp_Unit().equals("AIIVC2B") || ldaCwfl3802.getPnd_Comp_Unit().equals("APC  1A")  //Natural: IF #COMP-UNIT = 'AIIVC2A' OR = 'AIIVC2B' OR = 'APC  1A' OR = 'APC  1B' OR = 'APC  2A' OR = 'APC  2B'
                    || ldaCwfl3802.getPnd_Comp_Unit().equals("APC  1B") || ldaCwfl3802.getPnd_Comp_Unit().equals("APC  2A") || ldaCwfl3802.getPnd_Comp_Unit().equals("APC  2B")))
                {
                    if (condition(ldaCwfl3802.getPnd_Comp_Unit().greater("APC  1B") && ldaCwfl3802.getPnd_Mid_Total().getBoolean()))                                      //Natural: IF #COMP-UNIT GT 'APC  1B' AND #MID-TOTAL
                    {
                        ldaCwfl3802.getPnd_Generate_Combined_Totals().setValue(true);                                                                                     //Natural: MOVE TRUE TO #GENERATE-COMBINED-TOTALS
                        ldaCwfl3802.getPnd_Mid_Total().setValue(false);                                                                                                   //Natural: MOVE FALSE TO #MID-TOTAL
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaCwfl3802.getPnd_Generate_Combined_Totals().setValue(false);                                                                                    //Natural: MOVE FALSE TO #GENERATE-COMBINED-TOTALS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(DbsUtil.maskMatches(ldaCwfl3802.getPnd_Comp_Unit(),"AA.....")))                                                                         //Natural: IF #COMP-UNIT = MASK ( AA..... )
                    {
                        ldaCwfl3802.getPnd_Generate_Combined_Totals().setValue(true);                                                                                     //Natural: MOVE TRUE TO #GENERATE-COMBINED-TOTALS
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaCwfl3802.getPnd_Generate_Combined_Totals().setValue(false);                                                                                    //Natural: MOVE FALSE TO #GENERATE-COMBINED-TOTALS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCwfl3802.getPnd_Total_Count().greater(getZero())))                                                                                           //Natural: IF #TOTAL-COUNT GT 0
            {
                                                                                                                                                                          //Natural: PERFORM COMPUTE-GTOTAL-PERCENT
                sub_Compute_Gtotal_Percent();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), ldaCwfl3802.getPnd_Rep_Unit_Cde(), ldaCwfl3802.getPnd_Rep_Unit_Nme());                             //Natural: CALLNAT 'CWFN1103' #REP-UNIT-CDE #REP-UNIT-NME
            if (condition(Global.isEscape())) return;
            if (condition(ldaCwfl3802.getPnd_Save_Moc_1().notEquals("9")))                                                                                                //Natural: IF #SAVE-MOC-1 NE '9'
            {
                ldaCwfl3802.getPnd_Save_Moc_1().setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Moc());                                                                       //Natural: MOVE #WORK-RECORD.TBL-MOC TO #SAVE-MOC-1
            }                                                                                                                                                             //Natural: END-IF
            ldaCwfl3802.getPnd_Grand_Title().setValue("         TOTALS PAGE         ");                                                                                   //Natural: MOVE '         TOTALS PAGE         ' TO #GRAND-TITLE
            getReports().eject(1, true);                                                                                                                                  //Natural: EJECT ( 1 )
            ldaCwfl3802.getPnd_Page_Num().nadd(1);                                                                                                                        //Natural: COMPUTE #PAGE-NUM = #PAGE-NUM + 1
            getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),ldaCwfl3802.getPnd_Env(),new TabSetting(52),"CORPORATE WORKFLOW FACILITIES",new                //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 52T 'CORPORATE WORKFLOW FACILITIES' 124T 'PAGE' #PAGE-NUM ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 36T 'PREMIUM DIVISION  REPORT OF PARTICIPANT CLOSED WORK REQUESTS' 124T *TIMX ( EM = HH':'II' 'AP )
                TabSetting(124),"PAGE",ldaCwfl3802.getPnd_Page_Num(), new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), 
                new ReportEditMask ("LLL' 'DD','YY"),new TabSetting(36),"PREMIUM DIVISION  REPORT OF PARTICIPANT CLOSED WORK REQUESTS",new TabSetting(124),Global.getTIMX(), 
                new ReportEditMask ("HH':'II' 'AP"));
            if (condition(Global.isEscape())) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(52),ldaCwfl3802.getPnd_Grand_Title());                                                              //Natural: WRITE ( 1 ) 52T #GRAND-TITLE
            if (condition(Global.isEscape())) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(60),ldaCwfl3802.getPnd_Grand_Title2());                                                             //Natural: WRITE ( 1 ) 60T #GRAND-TITLE2
            if (condition(Global.isEscape())) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            if (condition(ldaCwfl3802.getPnd_Rep_Unit_Cde().equals(ldaCwfl3802.getPnd_Combined_Unit().getValue("*"))))                                                    //Natural: IF #REP-UNIT-CDE = #COMBINED-UNIT ( * )
            {
                ldaCwfl3802.getPnd_Combined_Unit_Hdr().setValue(ldaCwfl3802.getPnd_Rep_Unit_Cde());                                                                       //Natural: MOVE #REP-UNIT-CDE TO #COMBINED-UNIT-HDR
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), ldaCwfl3802.getPnd_Rep_Unit_Cde(), ldaCwfl3802.getPnd_Unit_Name());                                //Natural: CALLNAT 'CWFN1103' #REP-UNIT-CDE #UNIT-NAME
            if (condition(Global.isEscape())) return;
            getReports().write(1, ReportOption.NOTITLE,"UNIT      :",ldaCwfl3802.getPnd_Rep_Unit_Nme(),"(",ldaCwfl3802.getPnd_Rep_Unit_Cde(),")");                        //Natural: WRITE ( 1 ) 'UNIT      :' #REP-UNIT-NME '(' #REP-UNIT-CDE ')'
            if (condition(Global.isEscape())) return;
            getReports().write(1, ReportOption.NOTITLE,"START DATE:",ldaCwfl3802.getPnd_Start_Dte_Tme(), new ReportEditMask ("MM'/'DD'/'YY"),NEWLINE,"  END DATE:",ldaCwfl3802.getPnd_End_Dte_Tme(),  //Natural: WRITE ( 1 ) 'START DATE:' #START-DTE-TME ( EM = MM'/'DD'/'YY ) / '  END DATE:' #END-DTE-TME ( EM = MM'/'DD'/'YY )
                new ReportEditMask ("MM'/'DD'/'YY"));
            if (condition(Global.isEscape())) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"_",new RepeatItem(132),NEWLINE,NEWLINE,"GRAND TOTALS FOR INTERNALLY GENERATED REQUESTS AND EXTERNALLY ", //Natural: WRITE ( 1 ) // '_' ( 132 ) // 'GRAND TOTALS FOR INTERNALLY GENERATED REQUESTS AND EXTERNALLY ' 'GENERATED REQUESTS & ADJUSTMENTS:'/ '==============================================================' '================================'
                "GENERATED REQUESTS & ADJUSTMENTS:",NEWLINE,"==============================================================","================================");
            if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-GTOTAL-LINES
            sub_Write_Gtotal_Lines();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaCwfl3802.getPnd_Generate_Combined_Totals().getBoolean()))                                                                                    //Natural: IF #GENERATE-COMBINED-TOTALS
            {
                ldaCwfl3802.getPnd_Grand_Title().setValue("    COMBINED TOTALS  PAGE    ");                                                                               //Natural: MOVE '    COMBINED TOTALS  PAGE    ' TO #GRAND-TITLE
                ldaCwfl3802.getPnd_Total_Count_3().setValue(ldaCwfl3802.getPnd_Ctot_Count_3());                                                                           //Natural: MOVE #CTOT-COUNT-3 TO #TOTAL-COUNT-3
                ldaCwfl3802.getPnd_Total_Count_4().setValue(ldaCwfl3802.getPnd_Ctot_Count_4());                                                                           //Natural: MOVE #CTOT-COUNT-4 TO #TOTAL-COUNT-4
                ldaCwfl3802.getPnd_Total_Count_5().setValue(ldaCwfl3802.getPnd_Ctot_Count_5());                                                                           //Natural: MOVE #CTOT-COUNT-5 TO #TOTAL-COUNT-5
                ldaCwfl3802.getPnd_Total_Count_10().setValue(ldaCwfl3802.getPnd_Ctot_Count_10());                                                                         //Natural: MOVE #CTOT-COUNT-10 TO #TOTAL-COUNT-10
                ldaCwfl3802.getPnd_Total_Count_Over().setValue(ldaCwfl3802.getPnd_Ctot_Count_Over());                                                                     //Natural: MOVE #CTOT-COUNT-OVER TO #TOTAL-COUNT-OVER
                ldaCwfl3802.getPnd_Total_Count().setValue(ldaCwfl3802.getPnd_Ctot_Count());                                                                               //Natural: MOVE #CTOT-COUNT TO #TOTAL-COUNT
                ldaCwfl3802.getPnd_Ctot_Count_3().reset();                                                                                                                //Natural: RESET #CTOT-COUNT-3 #CTOT-COUNT-4 #CTOT-COUNT-5 #CTOT-COUNT-10 #CTOT-COUNT-OVER #CTOT-COUNT
                ldaCwfl3802.getPnd_Ctot_Count_4().reset();
                ldaCwfl3802.getPnd_Ctot_Count_5().reset();
                ldaCwfl3802.getPnd_Ctot_Count_10().reset();
                ldaCwfl3802.getPnd_Ctot_Count_Over().reset();
                ldaCwfl3802.getPnd_Ctot_Count().reset();
                if (condition(ldaCwfl3802.getPnd_Total_Count().greater(getZero())))                                                                                       //Natural: IF #TOTAL-COUNT GT 0
                {
                                                                                                                                                                          //Natural: PERFORM COMPUTE-GTOTAL-PERCENT
                    sub_Compute_Gtotal_Percent();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                getReports().eject(1, true);                                                                                                                              //Natural: EJECT ( 1 )
                ldaCwfl3802.getPnd_Page_Num().nadd(1);                                                                                                                    //Natural: COMPUTE #PAGE-NUM = #PAGE-NUM + 1
                getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),ldaCwfl3802.getPnd_Env(),new TabSetting(52),"CORPORATE WORKFLOW FACILITIES",new            //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 52T 'CORPORATE WORKFLOW FACILITIES' 124T 'PAGE' #PAGE-NUM ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 36T 'PREMIUM DIVISION  REPORT OF PARTICIPANT CLOSED WORK REQUESTS' 124T *TIMX ( EM = HH':'II' 'AP )
                    TabSetting(124),"PAGE",ldaCwfl3802.getPnd_Page_Num(), new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), 
                    new ReportEditMask ("LLL' 'DD','YY"),new TabSetting(36),"PREMIUM DIVISION  REPORT OF PARTICIPANT CLOSED WORK REQUESTS",new TabSetting(124),Global.getTIMX(), 
                    new ReportEditMask ("HH':'II' 'AP"));
                if (condition(Global.isEscape())) return;
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(52),ldaCwfl3802.getPnd_Grand_Title());                                                          //Natural: WRITE ( 1 ) 52T #GRAND-TITLE
                if (condition(Global.isEscape())) return;
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(60),ldaCwfl3802.getPnd_Grand_Title2());                                                         //Natural: WRITE ( 1 ) 60T #GRAND-TITLE2
                if (condition(Global.isEscape())) return;
                getReports().skip(1, 1);                                                                                                                                  //Natural: SKIP ( 1 ) 1
                short decideConditionsMet922 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #COMBINED-UNIT-HDR;//Natural: VALUE 'AIIVC2'
                if (condition((ldaCwfl3802.getPnd_Combined_Unit_Hdr().equals("AIIVC2"))))
                {
                    decideConditionsMet922++;
                    getReports().write(1, ReportOption.NOTITLE,"UNIT      :  COMBINED AIIVC2A AND AIIVC2B");                                                              //Natural: WRITE ( 1 ) 'UNIT      :  COMBINED AIIVC2A AND AIIVC2B'
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'APC  1'
                else if (condition((ldaCwfl3802.getPnd_Combined_Unit_Hdr().equals("APC  1"))))
                {
                    decideConditionsMet922++;
                    getReports().write(1, ReportOption.NOTITLE,"UNIT      :  COMBINED APC  1A AND APC  1B");                                                              //Natural: WRITE ( 1 ) 'UNIT      :  COMBINED APC  1A AND APC  1B'
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 'APC  2'
                else if (condition((ldaCwfl3802.getPnd_Combined_Unit_Hdr().equals("APC  2"))))
                {
                    decideConditionsMet922++;
                    getReports().write(1, ReportOption.NOTITLE,"UNIT      :  COMBINED APC  2A AND APC  2B");                                                              //Natural: WRITE ( 1 ) 'UNIT      :  COMBINED APC  2A AND APC  2B'
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                getReports().write(1, ReportOption.NOTITLE,"START DATE:",ldaCwfl3802.getPnd_Start_Dte_Tme(), new ReportEditMask ("MM'/'DD'/'YY"),NEWLINE,"  END DATE:",ldaCwfl3802.getPnd_End_Dte_Tme(),  //Natural: WRITE ( 1 ) 'START DATE:' #START-DTE-TME ( EM = MM'/'DD'/'YY ) / '  END DATE:' #END-DTE-TME ( EM = MM'/'DD'/'YY )
                    new ReportEditMask ("MM'/'DD'/'YY"));
                if (condition(Global.isEscape())) return;
                getReports().skip(1, 1);                                                                                                                                  //Natural: SKIP ( 1 ) 1
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(132),NEWLINE,NEWLINE,"_",new RepeatItem(132),NEWLINE,"COMBINED GRAND TOTALS FOR INT. GENERATED REQ.  AND EXTERNALLY ", //Natural: WRITE ( 1 ) // '*' ( 132 ) // '_' ( 132 ) / 'COMBINED GRAND TOTALS FOR INT. GENERATED REQ.  AND EXTERNALLY ' 'GENERATED REQUESTS & ADJUSTMENTS:'/ '==============================================================' '================================' /
                    "GENERATED REQUESTS & ADJUSTMENTS:",NEWLINE,"==============================================================","================================",
                    NEWLINE);
                if (condition(Global.isEscape())) return;
                DbsUtil.callnat(Cwfn3804.class , getCurrentProcessState(), ldaCwfl3802.getPnd_Combined_Totals());                                                         //Natural: CALLNAT 'CWFN3804' #COMBINED-TOTALS
                if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-GTOTAL-LINES
                sub_Write_Gtotal_Lines();
                if (condition(Global.isEscape())) {return;}
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*",new RepeatItem(132),NEWLINE);                                                                      //Natural: WRITE ( 1 ) / '*' ( 132 ) /
                if (condition(Global.isEscape())) return;
                ldaCwfl3802.getPnd_Generate_Combined_Totals().reset();                                                                                                    //Natural: RESET #GENERATE-COMBINED-TOTALS #COMBINED-TOTALS
                ldaCwfl3802.getPnd_Combined_Totals().reset();
            }                                                                                                                                                             //Natural: END-IF
            ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Booklet_Ctr().getValue("*").reset();                                                                                        //Natural: RESET #BOOKLET-CTR ( * ) #FORMS-CTR ( * ) #INQUIRE-CTR ( * ) #RESEARCH-CTR ( * ) #TRANS-CTR ( * ) #COMPLAINT-CTR ( * ) #OTHERS-CTR ( * ) #SUB-COUNT #PAGE-NUM #TOTAL-COUNT-3 #TOTAL-COUNT-4 #TOTAL-COUNT-5 #TOTAL-COUNT-10 #TOTAL-COUNT-OVER #TOTAL-COUNT #TOTAL-MOC
            ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Forms_Ctr().getValue("*").reset();
            ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Inquire_Ctr().getValue("*").reset();
            ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Research_Ctr().getValue("*").reset();
            ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Trans_Ctr().getValue("*").reset();
            ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Complaint_Ctr().getValue("*").reset();
            ldaCwfl3802.getPnd_Array_Ctrs_Pnd_Others_Ctr().getValue("*").reset();
            ldaCwfl3802.getPnd_Sub_Count().reset();
            ldaCwfl3802.getPnd_Page_Num().reset();
            ldaCwfl3802.getPnd_Total_Count_3().reset();
            ldaCwfl3802.getPnd_Total_Count_4().reset();
            ldaCwfl3802.getPnd_Total_Count_5().reset();
            ldaCwfl3802.getPnd_Total_Count_10().reset();
            ldaCwfl3802.getPnd_Total_Count_Over().reset();
            ldaCwfl3802.getPnd_Total_Count().reset();
            ldaCwfl3802.getPnd_Gtot_Registers_Pnd_Total_Moc().reset();
            ldaCwfl3802.getPnd_New_Unit().setValue(true);                                                                                                                 //Natural: MOVE TRUE TO #NEW-UNIT
            ldaCwfl3802.getPnd_Save_Unit().setValue(ldaCwfl3802.getPnd_Work_Record_Tbl_Close_Unit());                                                                     //Natural: MOVE #WORK-RECORD.TBL-CLOSE-UNIT TO #SAVE-UNIT
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=142 PS=58");

        getReports().setDisplayColumns(1, "//RECEIVED/AT TIAA",
        		ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Dte(), new ReportEditMask ("MM/DD/YY"),"//DOC/REC'D",
        		ldaCwfl3802.getPnd_Return_Doc_Txt(),"//REC'D IN/UNIT",
        		ldaCwfl3802.getPnd_Return_Rcvd_Txt(),"//WORK/PROCESS",
        		ldaCwfl3802.getPnd_Wpid_Sname(), new IdenticalSuppress(true),"/PARTICIPANT/CLOSED/STATUS",
        		ldaCwfl3802.getPnd_Status_Sname(),"//STATUS/DATE",
        		ldaCwfl3802.getPnd_Work_Record_Tbl_Status_Dte(), new ReportEditMask ("MM/DD/YY"),"BUSINESS/DAYS/IN/TIAA",
        		ldaCwfl3802.getPnd_Work_Record_Tbl_Tiaa_Bsnss_Days(), new ReportEditMask ("Z,ZZ9"),"//PARTIC/NAME",
        		ldaCwfl3802.getPnd_Work_Record_Pnd_Partic_Sname(),"///PIN",
        		ldaCwfl3802.getPnd_Work_Record_Tbl_Pin(),"//CONTRACT/NUMBER",
        		ldaCwfl3802.getPnd_Work_Record_Tbl_Contracts(),"///EMPLOYEE",
        		ldaCwfl3802.getPnd_Work_Record_Tbl_Racf());
    }
}
