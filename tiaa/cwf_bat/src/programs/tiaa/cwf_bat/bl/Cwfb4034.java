/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:32:44 PM
**        * FROM NATURAL PROGRAM : Cwfb4034
************************************************************
**        * FILE NAME            : Cwfb4034.java
**        * CLASS NAME           : Cwfb4034
**        * INSTANCE NAME        : Cwfb4034
************************************************************
** IF YOU MUST REGENERATE, REMOVE THIS COMMENT,  GENERATE THE PROGRAM,
** AND DELETE THE PRIMARY VIEW (USE VIEW IN CWFL4034 IN LOCAL DATA)
** WHICH IS GENERATED /* JVH 12/18/95
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: WORK GROUPS TABLE
**SAG SYSTEM: CRPCWF
**SAG REPORT-HEADING(1): CWF WORK GROUP CODES EXTRACT REPORT
**SAG PRINT-FILE(1): 1
**SAG HEADING-LINE: 10000000
**SAG DESCS(1): THIS PROGRAM READS THE CWF-WORK-GRP-TBL  AND WRITES A
**SAG DESCS(2): WORK FILE TO BE DOWNLOADED TO THE SQL SERVER.
**SAG PRIMARY-FILE: CWF-WORK-GRP-TBL
**SAG PRIMARY-KEY: TBL-PRIME-KEY
************************************************************************
* PROGRAM  : CWFB4034
* SYSTEM   : CRPCWF
* TITLE    : WORK GROUPS TABLE
* GENERATED: DEC 18,95 AT 01:58 PM
* FUNCTION : THIS PROGRAM READS THE CWF-WORK-GRP-TBL  AND WRITES A
*            WORK FILE TO BE DOWNLOADED TO THE SQL SERVER.
*
*
* HISTORY
**SAG DEFINE EXIT CHANGE-HISTORY
* CHANGED ON DEC 18,95 BY HARGRAV FOR RELEASE ____
* >
* >
* >
**SAG END-EXIT
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4034 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private LdaCwfl4034 ldaCwfl4034;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Rex_Read;
    private DbsField pnd_Counters_Pnd_Rex_Written;

    private DbsGroup pnd_Report_Fields;
    private DbsField pnd_Report_Fields_Pnd_Error_Msg;

    private DbsGroup pnd_Contstants;
    private DbsField pnd_Contstants_Pnd_Delimiter;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_1;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;

    private DbsGroup work_Record;
    private DbsField work_Record_Wk_Code;
    private DbsField work_Record_Pnd_C1;
    private DbsField work_Record_Wk_Desc;
    private DbsField work_Record_Pnd_C2;
    private DbsField work_Record_Resp_Division;
    private DbsField work_Record_Pnd_C3;
    private DbsField work_Record_Wpid;
    private DbsField pnd_Prev_Wk_Code;
    private DbsField pnd_I;
    private DbsField pnd_Record_Written;
    private DbsField pnd_Rec_Cntr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        ldaCwfl4034 = new LdaCwfl4034();
        registerRecord(ldaCwfl4034);
        registerRecord(ldaCwfl4034.getVw_cwf_Work_Grp());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Rex_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Read", "#REX-READ", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_Rex_Written = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Written", "#REX-WRITTEN", FieldType.PACKED_DECIMAL, 5);

        pnd_Report_Fields = localVariables.newGroupInRecord("pnd_Report_Fields", "#REPORT-FIELDS");
        pnd_Report_Fields_Pnd_Error_Msg = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 60);

        pnd_Contstants = localVariables.newGroupInRecord("pnd_Contstants", "#CONTSTANTS");
        pnd_Contstants_Pnd_Delimiter = pnd_Contstants.newFieldInGroup("pnd_Contstants_Pnd_Delimiter", "#DELIMITER", FieldType.STRING, 1);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 22);

        pnd_Tbl_Prime_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_1", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);

        work_Record = localVariables.newGroupInRecord("work_Record", "WORK-RECORD");
        work_Record_Wk_Code = work_Record.newFieldInGroup("work_Record_Wk_Code", "WK-CODE", FieldType.STRING, 6);
        work_Record_Pnd_C1 = work_Record.newFieldInGroup("work_Record_Pnd_C1", "#C1", FieldType.STRING, 1);
        work_Record_Wk_Desc = work_Record.newFieldInGroup("work_Record_Wk_Desc", "WK-DESC", FieldType.STRING, 30);
        work_Record_Pnd_C2 = work_Record.newFieldInGroup("work_Record_Pnd_C2", "#C2", FieldType.STRING, 1);
        work_Record_Resp_Division = work_Record.newFieldInGroup("work_Record_Resp_Division", "RESP-DIVISION", FieldType.STRING, 6);
        work_Record_Pnd_C3 = work_Record.newFieldInGroup("work_Record_Pnd_C3", "#C3", FieldType.STRING, 1);
        work_Record_Wpid = work_Record.newFieldInGroup("work_Record_Wpid", "WPID", FieldType.STRING, 6);
        pnd_Prev_Wk_Code = localVariables.newFieldInRecord("pnd_Prev_Wk_Code", "#PREV-WK-CODE", FieldType.STRING, 6);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Record_Written = localVariables.newFieldInRecord("pnd_Record_Written", "#RECORD-WRITTEN", FieldType.BOOLEAN, 1);
        pnd_Rec_Cntr = localVariables.newFieldInRecord("pnd_Rec_Cntr", "#REC-CNTR", FieldType.NUMERIC, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCdbatxa.initializeValues();
        ldaCwfl4034.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("       CWF Work Group Codes Extract Report");
        pnd_Header1_2.setInitialValue("                        �");
        pnd_Contstants_Pnd_Delimiter.setInitialValue("@");
        pnd_Tbl_Prime_Key.setInitialValue("A WORK-GROUP-TABLE");
        pnd_Record_Written.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4034() throws Exception
    {
        super("Cwfb4034");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        ldaCwfl4034.getVw_cwf_Work_Grp().startDatabaseRead                                                                                                                //Natural: READ CWF-WORK-GRP BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ_PRIME",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(ldaCwfl4034.getVw_cwf_Work_Grp().readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            if (condition(ldaCwfl4034.getCwf_Work_Grp_Tbl_Table_Nme().notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)))                                                    //Natural: IF CWF-WORK-GRP.TBL-TABLE-NME NE #TBL-PRIME-KEY.#TBL-TABLE-NME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Counters_Pnd_Rex_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #REX-READ
            if (condition(ldaCwfl4034.getCwf_Work_Grp_Wk_Code().notEquals(pnd_Prev_Wk_Code)))                                                                             //Natural: IF CWF-WORK-GRP.WK-CODE NE #PREV-WK-CODE
            {
                pnd_Record_Written.resetInitial();                                                                                                                        //Natural: RESET INITIAL #RECORD-WRITTEN
                work_Record.reset();                                                                                                                                      //Natural: RESET WORK-RECORD #REC-CNTR
                pnd_Rec_Cntr.reset();
                work_Record.setValuesByName(ldaCwfl4034.getVw_cwf_Work_Grp());                                                                                            //Natural: MOVE BY NAME CWF-WORK-GRP TO WORK-RECORD
                pnd_Prev_Wk_Code.setValue(ldaCwfl4034.getCwf_Work_Grp_Wk_Code());                                                                                         //Natural: MOVE CWF-WORK-GRP.WK-CODE TO #PREV-WK-CODE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rec_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-CNTR
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO 36
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(36)); pnd_I.nadd(1))
            {
                if (condition(DbsUtil.maskMatches(ldaCwfl4034.getCwf_Work_Grp_Work_Prcss_Id().getValue(pnd_I),"A")))                                                      //Natural: IF CWF-WORK-GRP.WORK-PRCSS-ID ( #I ) EQ MASK ( A )
                {
                    work_Record_Wpid.setValue(ldaCwfl4034.getCwf_Work_Grp_Work_Prcss_Id().getValue(pnd_I));                                                               //Natural: MOVE CWF-WORK-GRP.WORK-PRCSS-ID ( #I ) TO WORK-RECORD.WPID
                                                                                                                                                                          //Natural: PERFORM FORMAT-RECORD
                    sub_Format_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
                    sub_Write_Work_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *SAG END-EXIT
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        if (condition(getReports().getAstLinesLeft(1).less(6)))                                                                                                           //Natural: NEWPAGE ( 1 ) IF LESS THAN 6 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total Number of Work Group Table Records Read:",pnd_Counters_Pnd_Rex_Read,NEWLINE,"Total Number of Work File Records Written    :",pnd_Counters_Pnd_Rex_Written,NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) // 'Total Number of Work Group Table Records Read:' #REX-READ / 'Total Number of Work File Records Written    :' #REX-WRITTEN //56T 'End of Report'
            TabSetting(56),"End of Report");
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-RECORD
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *****************************
        //* *************
        //* *SAG END-EXIT
    }
    private void sub_Format_Record() throws Exception                                                                                                                     //Natural: FORMAT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        work_Record_Pnd_C1.setValue(pnd_Contstants_Pnd_Delimiter);                                                                                                        //Natural: MOVE #DELIMITER TO #C1 #C2 #C3
        work_Record_Pnd_C2.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C3.setValue(pnd_Contstants_Pnd_Delimiter);
        //* *************
        //*  FORMAT-RECORD
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(1, false, work_Record);                                                                                                                      //Natural: WRITE WORK FILE 1 WORK-RECORD
        pnd_Counters_Pnd_Rex_Written.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REX-WRITTEN
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        //* ********************************
        //*  WRITE-WORK-FILE
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "TABLE/NAME",                                                                                                                             //Natural: DISPLAY ( 1 ) 'TABLE/NAME' CWF-WORK-GRP.TBL-TABLE-NME 'WORK/CODE' WORK-RECORD.WK-CODE 'WK/DESC' WORK-RECORD.WK-DESC 'RESP/DIVISION' WORK-RECORD.RESP-DIVISION '/WPID' WORK-RECORD.WPID
        		ldaCwfl4034.getCwf_Work_Grp_Tbl_Table_Nme(),"WORK/CODE",
        		work_Record_Wk_Code,"WK/DESC",
        		work_Record_Wk_Desc,"RESP/DIVISION",
        		work_Record_Resp_Division,"/WPID",
        		work_Record_Wpid);
        if (Global.isEscape()) return;
        //* *************
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),pnd_Header1_2,new 
                        TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "TABLE/NAME",
        		ldaCwfl4034.getCwf_Work_Grp_Tbl_Table_Nme(),"WORK/CODE",
        		work_Record_Wk_Code,"WK/DESC",
        		work_Record_Wk_Desc,"RESP/DIVISION",
        		work_Record_Resp_Division,"/WPID",
        		work_Record_Wpid);
    }
}
