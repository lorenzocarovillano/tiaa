/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:39:59 PM
**        * FROM NATURAL PROGRAM : Icwb5390
************************************************************
**        * FILE NAME            : Icwb5390.java
**        * CLASS NAME           : Icwb5390
**        * INSTANCE NAME        : Icwb5390
************************************************************
************************************************************************
* PROGRAM  : ICWB5390
* SYSTEM   : CRPICW
* FUNCTION : RQST LOGGED BY CRC WITH CHECKS FOR A CERTAIN DAY
*          |
* 02/07/95 | OB TOOK OUT INPUT STATEMENT/PARAMETER, USE SYSTEM DATE AS
*          |     PARAMETER
* 08/16/95 | OB READ COR INSTEAD OF PIF
* 10/11/96 | JHH CLONED FROM CWFB5390 FOR INSTITUTIONAL CWF
* 10/17/96 | JHH ADDED SUB-TOTALS BY DAY
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwb5390 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_wpid_Tbl;
    private DbsField wpid_Tbl_Work_Prcss_Id;
    private DbsField wpid_Tbl_Work_Prcss_Short_Nme;

    private DataAccessProgramView vw_master;
    private DbsField master_Rqst_Log_Dte_Tme;

    private DbsGroup master__R_Field_1;
    private DbsField master_Rqst_Log_Dte;
    private DbsField master_Rqst_Log_Unit_Cde;
    private DbsField master_Rqst_Log_Oprtr_Cde;
    private DbsField master_Ppg_Cde;
    private DbsField master_Work_Prcss_Id;
    private DbsField master_Tiaa_Rcvd_Dte_Tme;
    private DbsField master_Admin_Unit_Cde;
    private DbsField master_Admin_Status_Cde;
    private DbsField master_Status_Cde;
    private DbsField master_Check_Ind;
    private DbsField master_Crprte_Status_Ind;

    private DataAccessProgramView vw_master1;
    private DbsField master1_Rqst_Log_Dte_Tme;
    private DbsField master1_Admin_Unit_Cde;
    private DbsField pnd_Rqst_Routing_Key;

    private DbsGroup pnd_Rqst_Routing_Key__R_Field_2;
    private DbsField pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Rqst_Routing_Key_Pnd_Strtng_Event_Dte_Tme;

    private DbsGroup pnd_Vars;
    private DbsField pnd_Vars_Pnd_Ctr;
    private DbsField pnd_Vars_Pnd_Ctr1;
    private DbsField pnd_Vars_Pnd_Ctrd;
    private DbsField pnd_Vars_Pnd_Prev_Ld;
    private DbsField pnd_Vars_Pnd_Day;
    private DbsField pnd_Vars_Pnd_Dte;
    private DbsField pnd_Vars_Pnd_O_C;
    private DbsField pnd_Vars_Pnd_Rldt;

    private DbsGroup pnd_Vars__R_Field_3;
    private DbsField pnd_Vars_Pnd_Rldt_Yyyymmdd;
    private DbsField pnd_Vars_Pnd_Rld_Max;
    private DbsField pnd_Vars_Pnd_Rldt_Max;

    private DbsGroup pnd_Vars__R_Field_4;
    private DbsField pnd_Vars_Pnd_Rldt_Max_Yyyymmdd;
    private DbsField pnd_Record;
    private DbsField pnd_Env;
    private DbsField pnd_Ppg_Code_No;
    private DbsField pnd_Ppg_Name;
    private DbsField pnd_Ny_Denver_Ind;
    private DbsField pnd_Pbs_Apps_Ind;
    private DbsField pnd_Prem_Grp_Cde;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_wpid_Tbl = new DataAccessProgramView(new NameInfo("vw_wpid_Tbl", "WPID-TBL"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        wpid_Tbl_Work_Prcss_Id = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        wpid_Tbl_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        wpid_Tbl_Work_Prcss_Short_Nme = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        wpid_Tbl_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        registerRecord(vw_wpid_Tbl);

        vw_master = new DataAccessProgramView(new NameInfo("vw_master", "MASTER"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        master_Rqst_Log_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");

        master__R_Field_1 = vw_master.getRecord().newGroupInGroup("master__R_Field_1", "REDEFINE", master_Rqst_Log_Dte_Tme);
        master_Rqst_Log_Dte = master__R_Field_1.newFieldInGroup("master_Rqst_Log_Dte", "RQST-LOG-DTE", FieldType.STRING, 8);
        master_Rqst_Log_Unit_Cde = vw_master.getRecord().newFieldInGroup("master_Rqst_Log_Unit_Cde", "RQST-LOG-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_LOG_UNIT_CDE");
        master_Rqst_Log_Oprtr_Cde = vw_master.getRecord().newFieldInGroup("master_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_LOG_OPRTR_CDE");
        master_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/TIME");
        master_Ppg_Cde = vw_master.getRecord().newFieldInGroup("master_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6, RepeatingFieldStrategy.None, "PPG_CDE");
        master_Work_Prcss_Id = vw_master.getRecord().newFieldInGroup("master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        master_Tiaa_Rcvd_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE_TME");
        master_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        master_Admin_Unit_Cde = vw_master.getRecord().newFieldInGroup("master_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        master_Admin_Status_Cde = vw_master.getRecord().newFieldInGroup("master_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "ADMIN_STATUS_CDE");
        master_Status_Cde = vw_master.getRecord().newFieldInGroup("master_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        master_Check_Ind = vw_master.getRecord().newFieldInGroup("master_Check_Ind", "CHECK-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CHECK_IND");
        master_Check_Ind.setDdmHeader("CHK");
        master_Crprte_Status_Ind = vw_master.getRecord().newFieldInGroup("master_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_STATUS_IND");
        master_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        registerRecord(vw_master);

        vw_master1 = new DataAccessProgramView(new NameInfo("vw_master1", "MASTER1"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        master1_Rqst_Log_Dte_Tme = vw_master1.getRecord().newFieldInGroup("master1_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        master1_Admin_Unit_Cde = vw_master1.getRecord().newFieldInGroup("master1_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        registerRecord(vw_master1);

        pnd_Rqst_Routing_Key = localVariables.newFieldInRecord("pnd_Rqst_Routing_Key", "#RQST-ROUTING-KEY", FieldType.STRING, 22);

        pnd_Rqst_Routing_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Rqst_Routing_Key__R_Field_2", "REDEFINE", pnd_Rqst_Routing_Key);
        pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Rqst_Routing_Key__R_Field_2.newFieldInGroup("pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Rqst_Routing_Key_Pnd_Strtng_Event_Dte_Tme = pnd_Rqst_Routing_Key__R_Field_2.newFieldInGroup("pnd_Rqst_Routing_Key_Pnd_Strtng_Event_Dte_Tme", 
            "#STRTNG-EVENT-DTE-TME", FieldType.TIME);

        pnd_Vars = localVariables.newGroupInRecord("pnd_Vars", "#VARS");
        pnd_Vars_Pnd_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ctr", "#CTR", FieldType.NUMERIC, 8);
        pnd_Vars_Pnd_Ctr1 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ctr1", "#CTR1", FieldType.NUMERIC, 8);
        pnd_Vars_Pnd_Ctrd = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ctrd", "#CTRD", FieldType.NUMERIC, 8);
        pnd_Vars_Pnd_Prev_Ld = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Prev_Ld", "#PREV-LD", FieldType.STRING, 8);
        pnd_Vars_Pnd_Day = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Day", "#DAY", FieldType.STRING, 9);
        pnd_Vars_Pnd_Dte = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Dte", "#DTE", FieldType.DATE);
        pnd_Vars_Pnd_O_C = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_O_C", "#O-C", FieldType.STRING, 7);
        pnd_Vars_Pnd_Rldt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Rldt", "#RLDT", FieldType.STRING, 15);

        pnd_Vars__R_Field_3 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_3", "REDEFINE", pnd_Vars_Pnd_Rldt);
        pnd_Vars_Pnd_Rldt_Yyyymmdd = pnd_Vars__R_Field_3.newFieldInGroup("pnd_Vars_Pnd_Rldt_Yyyymmdd", "#RLDT-YYYYMMDD", FieldType.STRING, 8);
        pnd_Vars_Pnd_Rld_Max = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Rld_Max", "#RLD-MAX", FieldType.STRING, 8);
        pnd_Vars_Pnd_Rldt_Max = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Rldt_Max", "#RLDT-MAX", FieldType.STRING, 15);

        pnd_Vars__R_Field_4 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_4", "REDEFINE", pnd_Vars_Pnd_Rldt_Max);
        pnd_Vars_Pnd_Rldt_Max_Yyyymmdd = pnd_Vars__R_Field_4.newFieldInGroup("pnd_Vars_Pnd_Rldt_Max_Yyyymmdd", "#RLDT-MAX-YYYYMMDD", FieldType.STRING, 
            8);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Ppg_Code_No = localVariables.newFieldInRecord("pnd_Ppg_Code_No", "#PPG-CODE-NO", FieldType.STRING, 1);
        pnd_Ppg_Name = localVariables.newFieldInRecord("pnd_Ppg_Name", "#PPG-NAME", FieldType.STRING, 40);
        pnd_Ny_Denver_Ind = localVariables.newFieldInRecord("pnd_Ny_Denver_Ind", "#NY-DENVER-IND", FieldType.STRING, 1);
        pnd_Pbs_Apps_Ind = localVariables.newFieldInRecord("pnd_Pbs_Apps_Ind", "#PBS-APPS-IND", FieldType.STRING, 1);
        pnd_Prem_Grp_Cde = localVariables.newFieldInRecord("pnd_Prem_Grp_Cde", "#PREM-GRP-CDE", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_wpid_Tbl.reset();
        vw_master.reset();
        vw_master1.reset();

        localVariables.reset();
        pnd_Vars_Pnd_Rldt.setInitialValue("000000000000000");
        pnd_Vars_Pnd_Rldt_Max.setInitialValue("000000000000000");
        pnd_Record.setInitialValue("�E�&l1o2a5.6c72p3e66F�&a2L�(9905X�&l0L");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Icwb5390() throws Exception
    {
        super("Icwb5390");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  --------------------------------------------------------------
        st = Global.getTIMN();                                                                                                                                            //Natural: SET TIME
        getReports().getPageNumberDbs(1).reset();                                                                                                                         //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: RESET *PAGE-NUMBER ( 1 )
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                             //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        //* *** 02/07/95 OB
        pnd_Vars_Pnd_Day.setValueEdited(Global.getDATX(),new ReportEditMask("NNNNNNNNN"));                                                                                //Natural: MOVE EDITED *DATX ( EM = N ( 9 ) ) TO #DAY
        pnd_Vars_Pnd_Dte.setValue(Global.getDATX());                                                                                                                      //Natural: MOVE *DATX TO #DTE
        short decideConditionsMet85 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE OF #DAY;//Natural: VALUE 'Saturday'
        if (condition((pnd_Vars_Pnd_Day.equals("Saturday"))))
        {
            decideConditionsMet85++;
            pnd_Vars_Pnd_Dte.nsubtract(5);                                                                                                                                //Natural: SUBTRACT 5 FROM #DTE
        }                                                                                                                                                                 //Natural: VALUE 'Sunday'
        else if (condition((pnd_Vars_Pnd_Day.equals("Sunday"))))
        {
            decideConditionsMet85++;
            pnd_Vars_Pnd_Dte.nsubtract(6);                                                                                                                                //Natural: SUBTRACT 6 FROM #DTE
        }                                                                                                                                                                 //Natural: VALUE 'Monday'
        else if (condition((pnd_Vars_Pnd_Day.equals("Monday"))))
        {
            decideConditionsMet85++;
            pnd_Vars_Pnd_Dte.nsubtract(7);                                                                                                                                //Natural: SUBTRACT 7 FROM #DTE
        }                                                                                                                                                                 //Natural: VALUE 'Tuesday'
        else if (condition((pnd_Vars_Pnd_Day.equals("Tuesday"))))
        {
            decideConditionsMet85++;
            pnd_Vars_Pnd_Dte.nsubtract(8);                                                                                                                                //Natural: SUBTRACT 8 FROM #DTE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Vars_Pnd_Dte.nsubtract(6);                                                                                                                                //Natural: SUBTRACT 6 FROM #DTE
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Vars_Pnd_Rldt_Yyyymmdd.setValueEdited(pnd_Vars_Pnd_Dte,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED #DTE ( EM = YYYYMMDD ) TO #RLDT-YYYYMMDD
        pnd_Vars_Pnd_Dte.nadd(7);                                                                                                                                         //Natural: ADD 7 TO #DTE
        pnd_Vars_Pnd_Rldt_Max_Yyyymmdd.setValueEdited(pnd_Vars_Pnd_Dte,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED #DTE ( EM = YYYYMMDD ) TO #RLDT-MAX-YYYYMMDD
        pnd_Vars_Pnd_Dte.nsubtract(1);                                                                                                                                    //Natural: SUBTRACT 1 FROM #DTE
        pnd_Vars_Pnd_Rld_Max.setValueEdited(pnd_Vars_Pnd_Dte,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED #DTE ( EM = YYYYMMDD ) TO #RLD-MAX
        //*  ----------------------------------------
        vw_master.startDatabaseRead                                                                                                                                       //Natural: READ MASTER BY ACTV-UNQUE-KEY FROM #RLDT
        (
        "READ01",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pnd_Vars_Pnd_Rldt, WcType.BY) },
        new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_master.readNextRow("READ01")))
        {
            if (condition(master_Rqst_Log_Dte_Tme.greater(pnd_Vars_Pnd_Rldt_Max)))                                                                                        //Natural: IF MASTER.RQST-LOG-DTE-TME GT #RLDT-MAX
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Ctr1.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CTR1
            if (condition(master_Check_Ind.notEquals("Y")))                                                                                                               //Natural: REJECT IF MASTER.CHECK-IND NE 'Y'
            {
                continue;
            }
            //*  REJECT IF MASTER.ORGNL-UNIT-CDE NE 'CRC'
            if (condition(master_Rqst_Log_Unit_Cde.notEquals("CIRS")))                                                                                                    //Natural: REJECT IF MASTER.RQST-LOG-UNIT-CDE NE 'CIRS'
            {
                continue;
            }
            if (condition(master_Rqst_Log_Dte.notEquals(pnd_Vars_Pnd_Prev_Ld)))                                                                                           //Natural: IF MASTER.RQST-LOG-DTE NE #PREV-LD
            {
                if (condition(pnd_Vars_Pnd_Ctr.greater(getZero())))                                                                                                       //Natural: IF #CTR > 0
                {
                    //*  10/17/96 JHH
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total daily records:",pnd_Vars_Pnd_Ctrd);                                                         //Natural: WRITE ( 1 ) / 'Total daily records:' #CTRD
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Vars_Pnd_Ctrd.reset();                                                                                                                            //Natural: RESET #CTRD
                }                                                                                                                                                         //Natural: END-IF
                pnd_Vars_Pnd_Prev_Ld.setValue(master_Rqst_Log_Dte);                                                                                                       //Natural: MOVE MASTER.RQST-LOG-DTE TO #PREV-LD
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master_Crprte_Status_Ind.equals("9")))                                                                                                          //Natural: IF MASTER.CRPRTE-STATUS-IND EQ '9'
            {
                pnd_Vars_Pnd_O_C.setValue("Closed");                                                                                                                      //Natural: MOVE 'Closed' TO #O-C
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Vars_Pnd_O_C.setValue("Open");                                                                                                                        //Natural: MOVE 'Open' TO #O-C
            }                                                                                                                                                             //Natural: END-IF
            //*           READ IIS-PREM-PAYMNT-GRP
            //*   (A6)    SENT
            //*   (A1)    RECEIVED
            //*   (A40)    "
            //*   (A1)     "
            //*   (A1)     "
            //*   (N2)     "
            DbsUtil.callnat(Icwn5113.class , getCurrentProcessState(), master_Ppg_Cde, pnd_Ppg_Code_No, pnd_Ppg_Name, pnd_Ny_Denver_Ind, pnd_Pbs_Apps_Ind,                //Natural: CALLNAT 'ICWN5113' MASTER.PPG-CDE #PPG-CODE-NO #PPG-NAME #NY-DENVER-IND #PBS-APPS-IND #PREM-GRP-CDE
                pnd_Prem_Grp_Cde);
            if (condition(Global.isEscape())) return;
            pnd_Rqst_Routing_Key_Pnd_Rqst_Log_Dte_Tme.setValue(master_Rqst_Log_Dte_Tme);                                                                                  //Natural: MOVE MASTER.RQST-LOG-DTE-TME TO #RQST-LOG-DTE-TME
            pnd_Rqst_Routing_Key_Pnd_Strtng_Event_Dte_Tme.setValue(0);                                                                                                    //Natural: MOVE 0 TO #STRTNG-EVENT-DTE-TME
            vw_master1.startDatabaseRead                                                                                                                                  //Natural: READ ( 01 ) MASTER1 BY RQST-ROUTING-KEY FROM #RQST-ROUTING-KEY
            (
            "READ02",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Rqst_Routing_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
            01
            );
            READ02:
            while (condition(vw_master1.readNextRow("READ02")))
            {
                if (condition(master1_Rqst_Log_Dte_Tme.notEquals(master_Rqst_Log_Dte_Tme)))                                                                               //Natural: IF MASTER1.RQST-LOG-DTE-TME NE MASTER.RQST-LOG-DTE-TME
                {
                    master1_Admin_Unit_Cde.setValue(" ");                                                                                                                 //Natural: MOVE ' ' TO MASTER1.ADMIN-UNIT-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            vw_wpid_Tbl.startDatabaseRead                                                                                                                                 //Natural: READ ( 01 ) WPID-TBL BY WPID-UNIQ-KEY FROM MASTER.WORK-PRCSS-ID
            (
            "READ03",
            new Wc[] { new Wc("WPID_UNIQ_KEY", ">=", master_Work_Prcss_Id, WcType.BY) },
            new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") },
            01
            );
            READ03:
            while (condition(vw_wpid_Tbl.readNextRow("READ03")))
            {
                if (condition(wpid_Tbl_Work_Prcss_Id.notEquals(master_Work_Prcss_Id)))                                                                                    //Natural: IF WPID-TBL.WORK-PRCSS-ID NE MASTER.WORK-PRCSS-ID
                {
                    wpid_Tbl_Work_Prcss_Short_Nme.setValue(" ");                                                                                                          //Natural: MOVE ' ' TO WPID-TBL.WORK-PRCSS-SHORT-NME
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  10/17/96 JHH
            pnd_Vars_Pnd_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CTR
            pnd_Vars_Pnd_Ctrd.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CTRD
            getReports().write(1, ReportOption.NOTITLE,pnd_Vars_Pnd_Ctr, new ReportEditMask ("ZZZZ9"),new TabSetting(8),master_Rqst_Log_Dte,new TabSetting(18),pnd_Ppg_Name,  //Natural: WRITE ( 1 ) #CTR ( EM = ZZZZ9 ) 8T MASTER.RQST-LOG-DTE 18T #PPG-NAME ( AL = 30 ) 50T MASTER.PPG-CDE 58T MASTER.TIAA-RCVD-DTE-TME ( EM = MM/DD/YY ) 68T MASTER.RQST-LOG-OPRTR-CDE 78T WPID-TBL.WORK-PRCSS-SHORT-NME 95T MASTER1.ADMIN-UNIT-CDE 106T MASTER.STATUS-CDE 114T MASTER.ADMIN-STATUS-CDE 122T #O-C
                new AlphanumericLength (30),new TabSetting(50),master_Ppg_Cde,new TabSetting(58),master_Tiaa_Rcvd_Dte_Tme, new ReportEditMask ("MM/DD/YY"),new 
                TabSetting(68),master_Rqst_Log_Oprtr_Cde,new TabSetting(78),wpid_Tbl_Work_Prcss_Short_Nme,new TabSetting(95),master1_Admin_Unit_Cde,new 
                TabSetting(106),master_Status_Cde,new TabSetting(114),master_Admin_Status_Cde,new TabSetting(122),pnd_Vars_Pnd_O_C);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ---------------------------------------------
        //*  ---------------------------------------------                                                                                                                //Natural: AT TOP OF PAGE ( 1 )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total daily records:",pnd_Vars_Pnd_Ctrd);                                                                     //Natural: WRITE ( 1 ) / 'Total daily records:' #CTRD
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total records read :",pnd_Vars_Pnd_Ctr1);                                                                     //Natural: WRITE ( 1 ) / 'Total records read :' #CTR1
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Time Elapsed......:",st, new ReportEditMask ("99':'99':'99'.'9"));                                                    //Natural: WRITE ( 1 ) 'Time Elapsed......:' *TIMD ( ST. ) ( EM = 99':'99':'99'.'9 )
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().getPageNumberDbs(1).reset();                                                                                                                         //Natural: RESET *PAGE-NUMBER ( 1 )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(0, pnd_Record);                                                                                                                    //Natural: WRITE #RECORD
                    getReports().write(1, ReportOption.NOTITLE,"ICWB5390",pnd_Env,new TabSetting(49),"INSTITUTIONAL CORPORATE WORKFLOW",new TabSetting(119),"PAGE",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE 'ICWB5390' #ENV 49T 'INSTITUTIONAL CORPORATE WORKFLOW' 119T 'PAGE' *PAGE-NUMBER ( 1 ) ( NL = 5 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 48T 'Requests with checks logged by CIRS ' 117T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (5), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
                        TabSetting(48),"Requests with checks logged by CIRS ",new TabSetting(117),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(53),"from",pnd_Vars_Pnd_Rldt_Yyyymmdd,"to",pnd_Vars_Pnd_Rld_Max);                           //Natural: WRITE ( 1 ) 53T 'from' #RLDT-YYYYMMDD 'to' #RLD-MAX
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(8),"Log Dte ",new TabSetting(18),"   Name           ",new TabSetting(50),"PPG  ",new  //Natural: WRITE ( 1 ) / 8T 'Log Dte ' 18T '   Name           ' 50T 'PPG  ' 58T 'TIAA Dte' 68T 'LoggedBy' 78T 'WPID Short Name' 95T 'InitUnit' 105T 'Status' 113T 'AdmStat' 122T 'C/O'
                        TabSetting(58),"TIAA Dte",new TabSetting(68),"LoggedBy",new TabSetting(78),"WPID Short Name",new TabSetting(95),"InitUnit",new TabSetting(105),"Status",new 
                        TabSetting(113),"AdmStat",new TabSetting(122),"C/O");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(8),"--------",new TabSetting(18),"------------------",new TabSetting(50),"-----",new        //Natural: WRITE ( 1 ) 8T '--------' 18T '------------------' 50T '-----' 58T '--------' 68T '--------' 78T '---------------' 95T '--------' 105T '------' 113T '-------' 122T '---'
                        TabSetting(58),"--------",new TabSetting(68),"--------",new TabSetting(78),"---------------",new TabSetting(95),"--------",new TabSetting(105),"------",new 
                        TabSetting(113),"-------",new TabSetting(122),"---");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
